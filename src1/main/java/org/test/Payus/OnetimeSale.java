package org.test.Payus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnetimeSale {

@SerializedName("CreateVASOrder_Output")
@Expose
private CreateVASOrderOutput createVASOrderOutput;

public CreateVASOrderOutput getCreateVASOrderOutput() {
return createVASOrderOutput;
}

public void setCreateVASOrderOutput(CreateVASOrderOutput createVASOrderOutput) {
this.createVASOrderOutput = createVASOrderOutput;
}
}
