package org.test.Payus;

public class EmployeeDetils {

	private Integer id;
	private String empId;
	private String name;
	private String age;
	private String dateDetected;
	private String location;
	private String team;
	private String contactDetailsOfDoctor;
	private String designation;
	private String healthStatus;
	

/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
@JoinColumn(name="empd_id", referencedColumnName ="post_com_id",nullable = false)
	*/
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public String getContactDetailsOfDoctor() {
		return contactDetailsOfDoctor;
	}
	public void setContactDetailsOfDoctor(String contactDetailsOfDoctor) {
		this.contactDetailsOfDoctor = contactDetailsOfDoctor;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getDateDetected() {
		return dateDetected;
	}
	public void setDateDetected(String dateDetected) {
		this.dateDetected = dateDetected;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}
	
	
	
	
	
	
	}
