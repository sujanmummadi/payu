package org.test.Payus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateVASOrderOutput {

@SerializedName("AccountNumber")
@Expose
private String accountNumber;
@SerializedName("errorNo")
@Expose
private String errorNo;
@SerializedName("errorMessage")
@Expose
private String errorMessage;
@SerializedName("OrderId")
@Expose
private String orderId;
@SerializedName("transactionId")
@Expose
private String transactionId;

public String getAccountNumber() {
return accountNumber;
}

public void setAccountNumber(String accountNumber) {
this.accountNumber = accountNumber;
}

public String getErrorNo() {
return errorNo;
}

public void setErrorNo(String errorNo) {
this.errorNo = errorNo;
}

public String getErrorMessage() {
return errorMessage;
}

public void setErrorMessage(String errorMessage) {
this.errorMessage = errorMessage;
}

public String getOrderId() {
return orderId;
}

public void setOrderId(String orderId) {
this.orderId = orderId;
}

public String getTransactionId() {
return transactionId;
}

public void setTransactionId(String transactionId) {
this.transactionId = transactionId;
}

@Override
public String toString() {
	return "CreateVASOrderOutput [accountNumber=" + accountNumber + ", errorNo=" + errorNo + ", errorMessage="
			+ errorMessage + ", orderId=" + orderId + ", transactionId=" + transactionId + "]";
}


}