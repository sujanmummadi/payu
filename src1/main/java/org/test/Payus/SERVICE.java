
package org.test.Payus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SERVICE {

    @SerializedName("element")
    @Expose
    private Element element;

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

}
