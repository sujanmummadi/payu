package org.test.Payus;

public class HealthParameters {
	
	private Integer Id;
	private String dateDetected;
	private String symptoms;
	private String breathLessAndFatigue;
	private String feverAndTemperatureRecord;
	private String pulse;
	private String oxsygenAfterExercise;
	private String bp;
	private String familyMemebersHealth;
	private String OtherIssue;
	
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getDateDetected() {
		return dateDetected;
	}
	public void setDateDetected(String dateDetected) {
		this.dateDetected = dateDetected;
	}
	public String getSymptoms() {
		return symptoms;
	}
	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}
	public String getBreathLessAndFatigue() {
		return breathLessAndFatigue;
	}
	public void setBreathLessAndFatigue(String breathLessAndFatigue) {
		this.breathLessAndFatigue = breathLessAndFatigue;
	}
	public String getFeverAndTemperatureRecord() {
		return feverAndTemperatureRecord;
	}
	public void setFeverAndTemperatureRecord(String feverAndTemperatureRecord) {
		this.feverAndTemperatureRecord = feverAndTemperatureRecord;
	}
	public String getPulse() {
		return pulse;
	}
	public void setPulse(String pulse) {
		this.pulse = pulse;
	}
	public String getOxsygenAfterExercise() {
		return oxsygenAfterExercise;
	}
	public void setOxsygenAfterExercise(String oxsygenAfterExercise) {
		this.oxsygenAfterExercise = oxsygenAfterExercise;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getFamilyMemebersHealth() {
		return familyMemebersHealth;
	}
	public void setFamilyMemebersHealth(String familyMemebersHealth) {
		this.familyMemebersHealth = familyMemebersHealth;
	}
	public String getOtherIssue() {
		return OtherIssue;
	}
	public void setOtherIssue(String otherIssue) {
		OtherIssue = otherIssue;
	}
	
	
	
	

}
