package org.test.Payus;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CommonCheckSumHashUtilPayu {

	
	public static String checkSumHash(String input) 
	{ 
		try { 
			// getInstance() method is called with algorithm SHA-512 
			MessageDigest md = MessageDigest.getInstance("SHA-512"); 

			// digest() method is called 
			// to calculate message digest of the input string 
			// returned as array of byte 
			byte[] messageDigest = md.digest(input.getBytes()); 

			// Convert byte array into signum representation 
			BigInteger no = new BigInteger(1, messageDigest); 

			// Convert message digest into hex value 
			String hashtext = no.toString(16); 

			// Add preceding 0s to make it 32 bit 
			while (hashtext.length() < 32) { 
				hashtext = "0" + hashtext; 
			} 

			// return the HashText 
			return hashtext; 
		} 

		// For specifying wrong message digest algorithms 
		catch (NoSuchAlgorithmException e) { 
			throw new RuntimeException(e); 
		} 
	} 
	
	/*public static void main(String[] args) {
	//	String st=checkSumHash("gtKFFx|PLS-	-11|600.000|SAU Admission 2014|Vikas Kumar|vikaskumarsre@gmail.com|||||||||||eCwWELxi");
	//	String st=checkSumHash("gtKFFx|check_payment|100123|eCwWELxi");
//		:gtKFFx|36843843|50|hi this sujan|sujan|sujan@gmail.com|7568583292|https://www.google.com|https://www.google.com|||||||||||eCwWELxi

		String st=checkSumHash("gtKFFx|36843843|50|hi this sujan|sujan|sujan@gmail.com|||||||||||eCwWELxi");
		System.out.println(st);
	}*/
}
