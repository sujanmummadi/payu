
package org.test.Payus;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSiebelAgreementInfo {

    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("errorNo")
    @Expose
    private String errorNo;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("SERVICES")
    @Expose
    private List<SERVICE> sERVICES = null;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getErrorNo() {
        return errorNo;
    }

    public void setErrorNo(String errorNo) {
        this.errorNo = errorNo;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<SERVICE> getSERVICES() {
        return sERVICES;
    }

    public void setSERVICES(List<SERVICE> sERVICES) {
        this.sERVICES = sERVICES;
    }

}
