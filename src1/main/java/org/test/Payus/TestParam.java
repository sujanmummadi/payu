package org.test.Payus;

public class TestParam {
private String key;
private String command;
private String hash;
private String var1;


public TestParam(String key, String command, String hash, String var1) {
	super();
	this.key = key;
	this.command = command;
	this.hash = hash;
	this.var1 = var1;
}
public String getKey() {
	return key;
}
public void setKey(String key) {
	this.key = key;
}
public String getCommand() {
	return command;
}
public void setCommand(String command) {
	this.command = command;
}
@Override
public String toString() {
	return "TestParam [key=" + key + ", command=" + command + ", hash=" + hash + ", var1=" + var1 + "]";
}



}
