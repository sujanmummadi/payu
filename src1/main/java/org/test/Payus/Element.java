
package org.test.Payus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Element {

    @SerializedName("AGREEMENT_START_DATE")
    @Expose
    private String aGREEMENTSTARTDATE;
    @SerializedName("EXPIRY_DATE")
    @Expose
    private String eXPIRYDATE;
    @SerializedName("BILLING_CYCLE_CODE")
    @Expose
    private String bILLINGCYCLECODE;
    @SerializedName("PAY_TERM")
    @Expose
    private String pAYTERM;
    @SerializedName("CONTRACT_TYPE")
    @Expose
    private String cONTRACTTYPE;
    @SerializedName("PACKAGE_CODE")
    @Expose
    private String pACKAGECODE;
    @SerializedName("PRODUCT_DESC")
    @Expose
    private String pRODUCTDESC;
    @SerializedName("SUBSCRIPTION_PERIOD")
    @Expose
    private String sUBSCRIPTIONPERIOD;

    public String getAGREEMENTSTARTDATE() {
        return aGREEMENTSTARTDATE;
    }

    public void setAGREEMENTSTARTDATE(String aGREEMENTSTARTDATE) {
        this.aGREEMENTSTARTDATE = aGREEMENTSTARTDATE;
    }

    public String getEXPIRYDATE() {
        return eXPIRYDATE;
    }

    public void setEXPIRYDATE(String eXPIRYDATE) {
        this.eXPIRYDATE = eXPIRYDATE;
    }

    public String getBILLINGCYCLECODE() {
        return bILLINGCYCLECODE;
    }

    public void setBILLINGCYCLECODE(String bILLINGCYCLECODE) {
        this.bILLINGCYCLECODE = bILLINGCYCLECODE;
    }

    public String getPAYTERM() {
        return pAYTERM;
    }

    public void setPAYTERM(String pAYTERM) {
        this.pAYTERM = pAYTERM;
    }

    public String getCONTRACTTYPE() {
        return cONTRACTTYPE;
    }

    public void setCONTRACTTYPE(String cONTRACTTYPE) {
        this.cONTRACTTYPE = cONTRACTTYPE;
    }

    public String getPACKAGECODE() {
        return pACKAGECODE;
    }

    public void setPACKAGECODE(String pACKAGECODE) {
        this.pACKAGECODE = pACKAGECODE;
    }

    public String getPRODUCTDESC() {
        return pRODUCTDESC;
    }

    public void setPRODUCTDESC(String pRODUCTDESC) {
        this.pRODUCTDESC = pRODUCTDESC;
    }

    public String getSUBSCRIPTIONPERIOD() {
        return sUBSCRIPTIONPERIOD;
    }

    public void setSUBSCRIPTIONPERIOD(String sUBSCRIPTIONPERIOD) {
        this.sUBSCRIPTIONPERIOD = sUBSCRIPTIONPERIOD;
    }

}
