package org.test.Payus;

public class ResponseMessage {
	
	private String status;
	private String msg;
	private String transaction_details;
	private String request_id;
	private String bank_ref_num;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTransaction_details() {
		return transaction_details;
	}
	public void setTransaction_details(String transaction_details) {
		this.transaction_details = transaction_details;
	}
	public String getRequest_id() {
		return request_id;
	}
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}
	public String getBank_ref_num() {
		return bank_ref_num;
	}
	public void setBank_ref_num(String bank_ref_num) {
		this.bank_ref_num = bank_ref_num;
	}
	@Override
	public String toString() {
		return "ResponseMessage [status=" + status + ", msg=" + msg + ", transaction_details=" + transaction_details
				+ ", request_id=" + request_id + ", bank_ref_num=" + bank_ref_num + "]";
	}
	

}
