package com.cupola.fwmp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cupola.fwmp.constants.FileConstatnts;
import com.cupola.fwmp.service.device.DeviceService;
import com.cupola.fwmp.service.user.UserService;
import com.cupola.fwmp.vo.UserBulkUploadVO;


import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Controller
//@RequestMapping(value = "/customer")
public class MainController {
	
	@Autowired
	DeviceService deviceService;

	@Autowired
	UserService userService;
	
	private final String XLS_CONTENT_TYPE = "application/vnd.ms-excel";
	private final String XLSX_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	@RequestMapping(value = { "/", "/welcome.do" }, method = RequestMethod.GET)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();
		/*model.addObject("title", "Spring Security + Hibernate Example");
		model.addObject("message", "This is default page!");*/
		model.setViewName("hello");
		return model;

	}

	/*@RequestMapping(value = "/home")
	public ModelAndView homePage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security + Hibernate Example");
		model.addObject("message", "This is default page!");
		model.setViewName("home.html");
		return model;

	}*/
	
	@RequestMapping(value = "/admin.do", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security + Hibernate Example");
		model.addObject("message", "This page is for ROLE_ADMIN only!");
		model.setViewName("admin");

		return model;

	}

	@RequestMapping(value = "/login.do", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "expired", required = false) String sessionExpired, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		
		if(sessionExpired != null){
			model.addObject("msg", "Your session has been expired. Please login again");
		}
		model.setViewName("login");

		/*CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
		if (csrf != null) {
		    Cookie cookie = new Cookie("XSRF-TOKEN", csrf.getToken());
		    cookie.setPath("/");
		    response.addCookie(cookie);
		}*/
		
		return model;

	}

	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Invalid username and password!";
		}

		return error;
	}

	// for 403 access denied page
	@RequestMapping(value = "/403.do", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();

			model.addObject("username", userDetail.getUsername());

		}

		model.setViewName("403");
		return model;

	}
	
	
	/**
	 * @Author : Sharan.Shastri Date : 14-12-2017 function will generate .xlsx
	 *         format file for user-device-bulk-upload
	 */
	@RequestMapping(value = "/userDeviceBulkUploadFileFormat", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> userDeviceBulkUploadFileFormat() {

		Workbook workbook = new XSSFWorkbook();

		try {
			workbook = deviceService.userDeviceBulkUploadFileFormat(workbook);
		} catch (Exception e) {
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.set("Content-Type", FileConstatnts.XLSX_CONTENT_TYPE);
		params.set("Content-disposition", "attachment; filename=" + "UserDeviceBulkUploadFileFormat." + "xlsx");
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			workbook.write(byteArrayOutputStream);
			byte[] responseExelByteArray = byteArrayOutputStream.toByteArray();
			params.set("Content-Length", responseExelByteArray.length + "");
			byteArrayOutputStream.close();
			return new ResponseEntity<>(responseExelByteArray, params, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * @Author : Sharan.Shastri Date : 05-12-2017
	 * 
	 *         mapping user with device in bulk, accept only .xlsx format.
	 */
	@RequestMapping(value = "/userDeviceBulkUpload", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> userDeviceBulkUpload(@RequestParam("file") MultipartFile file) {

		Workbook workbook = null;

		try {
			workbook = deviceService.mapDeviceForUserInBulk(WorkbookFactory.create(file.getInputStream()));
		} catch (Exception e) {
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.set("Content-Type", file.getContentType());
		String fileExtension = file.getContentType().equals(FileConstatnts.XLS_CONTENT_TYPE) ? "xls" : "xlsx";
		params.set("Content-disposition", "attachment; filename=" + "UserDeviceMappingResult." + fileExtension);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		try {

			workbook.write(byteArrayOutputStream);
			byte[] responseExelByteArray = byteArrayOutputStream.toByteArray();
			params.set("Content-Length", responseExelByteArray.length + "");
			byteArrayOutputStream.close();
			return new ResponseEntity<>(responseExelByteArray, params, HttpStatus.OK);

		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * @author manjuprasad.nidlady
	 * 
	 * @param file
	 * @return workbook
	 */

	@RequestMapping(value = "/userbulkupload", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> uploadBulkUserDetail(@RequestParam("file") MultipartFile file,
			Authentication authentication) {
		String login_id = authentication.getName();

		Workbook workbook = null;
		try {
			if (!(file.getContentType().equals(XLS_CONTENT_TYPE) || file.getContentType().equals(XLSX_CONTENT_TYPE))) {
				return new ResponseEntity<String>("unknown file type", HttpStatus.BAD_REQUEST);
			}
			workbook = uploadBulkUserDetail(WorkbookFactory.create(file.getInputStream()), login_id);
		} catch (Exception e) {
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.set("Content-Type", file.getContentType());
		String fileExtension = file.getContentType().equals(XLS_CONTENT_TYPE) ? "xls" : "xlsx";
		params.set("Content-disposition", "attachment; filename=" + "response." + fileExtension);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			workbook.write(byteArrayOutputStream);
			byte[] responseExelByteArray = byteArrayOutputStream.toByteArray();
			params.set("Content-Length", responseExelByteArray.length + "");
			byteArrayOutputStream.close();
			return new ResponseEntity<>(responseExelByteArray, params, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/*
	 * @author manjuprasad.nidlady
	 * 
	 * @param workbook, logId
	 * 
	 * @return workbook
	 * 
	 */
	public Workbook uploadBulkUserDetail(Workbook workbook, String logId) throws Exception {

		UserBulkUploadVO userbulk = new UserBulkUploadVO();

		Iterator<Row> rowIterator = workbook.getSheetAt(0).rowIterator();
		int i = 0;
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (i == 0) {
				row.createCell(17);
				row.getCell(17).setCellValue("Bulk Upload Status");

			} else {
				try {
				DataFormatter formatter = new DataFormatter();
				String response = "";
				userbulk.setLoginId(formatter.formatCellValue(row.getCell(0)));
				userbulk.setEmpId(formatter.formatCellValue(row.getCell(1)));
				userbulk.setFirstName(formatter.formatCellValue(row.getCell(2)));
				userbulk.setLastName(formatter.formatCellValue(row.getCell(3)));
				userbulk.setEmailId(formatter.formatCellValue(row.getCell(4)));
				userbulk.setMobileNo(Long.parseLong(formatter.formatCellValue(row.getCell(5))));
				userbulk.setPassword(formatter.formatCellValue(row.getCell(6)));
				userbulk.setUserGroups(formatter.formatCellValue(row.getCell(7)));
				userbulk.setCity(formatter.formatCellValue(row.getCell(8)));
				userbulk.setBranch(formatter.formatCellValue(row.getCell(9)));
				userbulk.setArea(formatter.formatCellValue(row.getCell(10)));
				userbulk.setSubAreas(formatter.formatCellValue(row.getCell(11)));
				userbulk.setTablet(formatter.formatCellValue(row.getCell(12)));
				userbulk.setReportTo(formatter.formatCellValue(row.getCell(13)));
				userbulk.setEmployer(formatter.formatCellValue(row.getCell(14)));
				userbulk.setSkills(formatter.formatCellValue(row.getCell(15)));
				userbulk.setEnabled(Integer.parseInt(formatter.formatCellValue(row.getCell(16))));
				userbulk.setAddedBy(logId);

				try {
					response = userService.uploadDataToDatabase(row, userbulk);
				} catch (Exception e) {
					response = "Insertion_failed_please verify the data";
				} finally {
					row.createCell(17);
					row.getCell(17).setCellValue(response);
				}

			}catch(Exception e) {
				e.printStackTrace();
			}
			}
			
			i++;
		}
		/*
		 * Date date = new Date();
		 * 
		 * DateFormat sdf = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss_SSS"); String
		 * stringDate = sdf.format(date); String path = "D://"; FileOutputStream fos =
		 * new FileOutputStream(path+logId+"_"+stringDate+".xlsx"); // saves xlsx file
		 * to disk workbook.write(fos); fos.close();
		 */

		return workbook;
	}

	/**
	 * @author manjuprasad.nidlady
	 * 
	 * @param file
	 * @return workbook
	 */

	@RequestMapping(value = "/userdefaultareamapping", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> uploadBulkDefaultArea(@RequestParam("file") MultipartFile file) {

		Workbook workbook = null;
		try {
			if (!(file.getContentType().equals(XLS_CONTENT_TYPE) || file.getContentType().equals(XLSX_CONTENT_TYPE))) {
				return new ResponseEntity<String>("unknown file type", HttpStatus.BAD_REQUEST);
			}
			workbook = userService.uploadBulkDefaultAreaMapping(WorkbookFactory.create(file.getInputStream()));
		} catch (Exception e) {
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.set("Content-Type", file.getContentType());
		String fileExtension = file.getContentType().equals(XLS_CONTENT_TYPE) ? "xls" : "xlsx";
		params.set("Content-disposition", "attachment; filename=" + "AreaMappingResult." + fileExtension);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			workbook.write(byteArrayOutputStream);
			byte[] responseExelByteArray = byteArrayOutputStream.toByteArray();
			params.set("Content-Length", responseExelByteArray.length + "");
			byteArrayOutputStream.close();
			return new ResponseEntity<>(responseExelByteArray, params, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * @author manjuprasad.nidlady
	 * 
	 * 
	 * @return workbook
	 * @throws IOException
	 * @throws InvalidFormatException
	 */

	@RequestMapping(value = "/getareamastermapping", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> getAreaMaster() throws InvalidFormatException, IOException {

		Workbook workbook = new XSSFWorkbook();

		try {
			workbook = userService.getAreaMasterMapping(workbook);
		} catch (Exception e) {
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.set("Content-Type", XLSX_CONTENT_TYPE);
		params.set("Content-disposition", "attachment; filename=" + "AreaMasterMapping." + "xlsx");
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			workbook.write(byteArrayOutputStream);
			byte[] responseExelByteArray = byteArrayOutputStream.toByteArray();
			params.set("Content-Length", responseExelByteArray.length + "");
			byteArrayOutputStream.close();
			return new ResponseEntity<>(responseExelByteArray, params, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}