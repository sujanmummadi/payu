package com.cupola.fwmp.cache;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.code.ssm.Cache;
import com.google.code.ssm.CacheFactory;
import com.google.code.ssm.api.format.SerializationType;
import com.google.code.ssm.providers.CacheException;

public class CupCacheImpl implements CupCache 
{
	Logger log=Logger.getLogger(CupCacheImpl.class);
	@Autowired
	CacheFactory cacheClient;
	private final SerializationType defaultSerializationType = SerializationType.PROVIDER;
	
 
	 
//	    MemcacheClientFactoryImpl cacheClient = new MemcacheClientFactoryImpl();

	 static   Cache cache = null;
 
	  
	    
	    public void init()
	    {
	    	
	    	log.info("init()...... CupCacheImpl ");
	    	try
			{
				cache = cacheClient.getObject();
			} catch (Exception e)
			{
				log.error("Error in init CupCacheImpl");
				e.printStackTrace();
			}
	    	
	    	log.info("init()......CupCacheImpl  done"+cache);
	    	
	    }
	        
	        
	 
	
	public LinkedHashSet<Long> get(String key)
	{
		// TODO Auto-generated method stub
		
		try
		{
			return	cache.get(key,defaultSerializationType);
		} catch (TimeoutException e)
		{
			log.error("Timeout error in get cache");
			e.printStackTrace();
		} catch (CacheException e)
		{
			log.error("Error in get cache");
			e.printStackTrace();
		}
		return null;
	}

	public boolean containsKey(String key)
	{
		
		try
		{
			return  cache.get(key,defaultSerializationType) != null;
		} catch (TimeoutException e)
		{
			log.error("Timeout Error in containsKey from cache");
			e.printStackTrace();
		} catch (CacheException e)
		{
			log.error("Error in containsKey from cache");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void putAll(
			Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping)
	{
		if (tempAreaWorkOrderMapping != null)
		{
			 

				for (Map.Entry<String, LinkedHashSet<Long>> entry : tempAreaWorkOrderMapping
						.entrySet())
				{
					put(entry.getKey(), entry.getValue());
				}
		}
	}

	public void put(String key, LinkedHashSet<Long> linkedHashSet)
	{
		try
		{
			cache.add(key,0,linkedHashSet,defaultSerializationType);
		} catch (TimeoutException | CacheException e)
		{
			log.error("Error in storing into cache");
			e.printStackTrace();
		}
	}

	public void clear()
	{
//		cache.
		
	}

	public boolean isEmpty()
	{
		 
		return true;
	}

	
	
}
