package com.cupola.fwmp.cache;

import java.util.LinkedHashSet;
import java.util.Map;

public interface CupCache
{

	LinkedHashSet<Long> get(String key);

	boolean containsKey(String key);

	void putAll(Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping);

	void put(String key, LinkedHashSet<Long> linkedHashSet);

	void clear();

	boolean isEmpty();

}