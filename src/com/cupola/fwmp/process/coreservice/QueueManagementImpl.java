package com.cupola.fwmp.process.coreservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.LogManager;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.tablet.TabletDAOImpl;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.handler.MessageHandler;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ClassificationEngineCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.TicketRequestVO;
import com.cupola.fwmp.vo.UserAndRegIdDetails;
import com.cupola.fwmp.ws.TicketDetailService;

import org.apache.log4j.Logger;

public class QueueManagementImpl implements QueueManagement
{

	private static Logger log = LogManager.getLogger(QueueManagementImpl.class);

	@Autowired
	AuthUtils authUtils;

	@Autowired
	TabletDAO tabletDAO;

	@Autowired
	TicketDetailService ticketDetailService;

	@Autowired
	ClassificationEngineCoreService classificationEngineCoreService;

	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	DBUtil dbUtil;
	
	@Override
	public void addInQueue(TicketRequestVO ticketRequestVO)
	{
		// TODO Auto-generated method stub

		String key;
		Long userId = ticketRequestVO.getAssignedTo();

		try
		{

			log.info("inside in working queue");
			key = dbUtil.getKeyForQueueByUserId(userId);
			LinkedHashSet<Long> ticketNumberList = new LinkedHashSet<>();
			ticketNumberList.add(ticketRequestVO.getTicketId());

			int workingQueueDepth = globalActivities
					.calculateWorkingQueueDepth(userId);
			if (globalActivities.getWorkingQueue().containsKey(key))
			{
				if (globalActivities.getWorkingQueue().get(key).size() < workingQueueDepth)
				{

					APIResponse response = classificationEngineCoreService
							.addTicket2WorkingQueue(key, ticketNumberList, false);

					log.info("added in qorking queue" + response);

					// call GCM with ticketNumberListForWorkingQueue

					// List<UserAndRegIdDetails>
					// userAndRegIdDetails=tabletDAO.getUserAndRegIdDetails(Long.valueOf(userId));
					// log.debug("userAndRegIdDetails" +
					// userAndRegIdDetails);
					//
					// if (userAndRegIdDetails.isEmpty()) {
					//
					// log.info("registrationId not found for user:" + userId);
					// }
					// else {
					// UserAndRegIdDetails registration =
					// userAndRegIdDetails.get(0);
					// GCMMessage message = new GCMMessage();
					// MessageVO refreshTicketVO = new MessageVO();
					// refreshTicketVO.setMessageType(FWMPConstant.MessageType.SYNC_TICKETS);
					// message.setRegistrationId(registration.getRegistrationId().trim());
					// message.setMessage(convertObjectToString(refreshTicketVO));
					// log.info("**************message is =============" +
					// message);
					//
					// MessageHandler messageHandler = new MessageHandler();
					// messageHandler.addInGCMMessageQueue(message);
					// log.info("messageHandler =============" +
					// messageHandler);
					// }
				} else
				{

					log.info("Size of working queue less than depth");
				}
			} else
			{

				log.info("queue not avaialbe " + key);

				APIResponse response = classificationEngineCoreService
						.addTicket2WorkingQueue(key, ticketNumberList, false);

			}

			List<UserAndRegIdDetails> userAndRegIdDetails = tabletDAO
					.getUserAndRegIdDetails(Long.valueOf(userId));
			log.debug("userAndRegIdDetails" + userAndRegIdDetails);

			// if (userAndRegIdDetails.isEmpty()) {
			//
			// log.info("registrationId not found for user:" + userId);
			// }
			// else {
			// UserAndRegIdDetails registration = userAndRegIdDetails.get(0);
			// GCMMessage message = new GCMMessage();
			// MessageVO refreshTicketVO = new MessageVO();
			// refreshTicketVO.setMessageType(FWMPConstant.MessageType.SYNC_TICKETS);
			// message.setRegistrationId(registration.getRegistrationId().trim());
			// message.setMessage(convertObjectToString(refreshTicketVO));
			// log.info("**************message is =============" +
			// message.toString());
			//
			// MessageHandler messageHandler = new MessageHandler();
			// messageHandler.addInGCMMessageQueue(message);
			// log.info("messageHandler =============" + messageHandler);
			// }

		} catch (Exception e)
		{
			log.error("Error While adding in Queue :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String convertObjectToString(Object refreshTicketVO)
	{
		// TODO Auto-generated method stub

		ObjectMapper mapper = new ObjectMapper();
		String dataString = "";
		try
		{
			dataString = mapper.writeValueAsString(refreshTicketVO);

		} catch (JsonGenerationException e1)
		{
			log.error("Error While generating json :"+e1.getMessage());
			e1.printStackTrace();
		} catch (JsonMappingException e1)
		{
			log.error("Error WhileJsonMapping :"+e1.getMessage());
			e1.printStackTrace();
		} catch (IOException e1)
		{
			log.error("Error While convertObjectToString :"+e1.getMessage());
			e1.printStackTrace();
		}
		return dataString;

	}

}
