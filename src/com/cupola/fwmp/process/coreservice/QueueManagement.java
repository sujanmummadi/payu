package com.cupola.fwmp.process.coreservice;

import com.cupola.fwmp.vo.TicketRequestVO;

public interface QueueManagement {

	public void addInQueue(TicketRequestVO ticketRequestVO);
	
}
