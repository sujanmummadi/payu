package com.cupola.fwmp.process.flow;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/getalivestatus")
public class AliveStatusProvider {

	@POST
	@Path("status")
	@Produces(MediaType.TEXT_PLAIN)
	public String returnStatus() 
	{
		return "Alive";
	}
}
