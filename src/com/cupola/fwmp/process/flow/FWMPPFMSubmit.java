package com.cupola.fwmp.process.flow;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.process.coreservice.QueueManagement;
import com.cupola.fwmp.vo.FWMPPFMWorkderVo;
import com.cupola.fwmp.vo.TicketRequestVO;

@Path("integ/workflows")
public class FWMPPFMSubmit
{

	@Autowired
	QueueManagement queueManagement;

	private static final Logger log = LoggerFactory
			.getLogger(FWMPPFMSubmit.class);

	@POST
	@Path("submit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public FWMPPFMWorkderVo getTicketList(List<TicketRequestVO> ticketRequestVOs)
	{
		log.debug("list " + ticketRequestVOs);

		String correlationId = null;
		FWMPPFMWorkderVo fwmppfmWorrderVo = new FWMPPFMWorkderVo();

		try
		{
			for (TicketRequestVO ticketRequestVO : ticketRequestVOs)
			{

				String Workflow = ticketRequestVO.getWfName();
				correlationId = Long.toString(ticketRequestVO.getTicketId());

				if (Workflow.equalsIgnoreCase("NISales")
						|| Workflow.equalsIgnoreCase("NIDeployment")
						|| Workflow.equalsIgnoreCase("FR")
						|| Workflow.equalsIgnoreCase("Shifting")
						|| Workflow.equalsIgnoreCase("Reactivation"))
				{

					log.debug("Ticket Object recieved " + ticketRequestVO);

					fwmppfmWorrderVo.setAcknowledgement("Success");
					fwmppfmWorrderVo.setCorrelationId(correlationId);

					queueManagement.addInQueue(ticketRequestVO);

					log.debug("management " + queueManagement);

				} else
				{

					log.info("Workflow not found");
					fwmppfmWorrderVo.setAcknowledgement("Failure");
					fwmppfmWorrderVo.setCorrelationId(correlationId);
				}
			}
		} catch (Exception e)
		{

			log.error("Workflow object is null " + e.getMessage());
			e.printStackTrace();
		}

		log.debug("fwmppfmWorrderVo ===================="
				+ fwmppfmWorrderVo.getAcknowledgement() + "           "
				+ fwmppfmWorrderVo.getCorrelationId());
		return fwmppfmWorrderVo;

	}
}