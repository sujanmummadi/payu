package com.cupola.fwmp.process.flow;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.filters.TariffFilter;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.filters.VendorFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.closeticket.MQClosureHelper;
import com.cupola.fwmp.service.customer.ProspectCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ClassificationEngineCoreService;
import com.cupola.fwmp.service.domain.engines.doc.service.DocumentationCoreEngine;
import com.cupola.fwmp.service.domain.engines.gis.GISServiceIntegrationImpl;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.frMaterialRequest.MaterialRequestService;
import com.cupola.fwmp.service.integ.aat.AATCoreService;
import com.cupola.fwmp.service.location.area.AreaCoreService;
import com.cupola.fwmp.service.location.branch.BranchCoreService;
import com.cupola.fwmp.service.location.city.CityCoreService;
import com.cupola.fwmp.service.location.subArea.SubAreaService;
import com.cupola.fwmp.service.materials.MaterialsCoreService;
import com.cupola.fwmp.service.report.ReportCoreService;
import com.cupola.fwmp.service.rollback.RollBackService;
import com.cupola.fwmp.service.tablet.TabletService;
import com.cupola.fwmp.service.tariff.TariffCoreService;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreService;
import com.cupola.fwmp.service.tool.GlobalCoreService;
import com.cupola.fwmp.service.user.UserService;
import com.cupola.fwmp.service.userCaf.UserCafCoreService;
import com.cupola.fwmp.service.userGroup.UserGroupCoreService;
import com.cupola.fwmp.service.vendor.VendorCoreService;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.service.workOrderType.WorkOrderTypeCoreService;
import com.cupola.fwmp.service.workStage.WorkStageCoreService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.PagedResultsContext;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.AreaVO;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.BranchVO;
import com.cupola.fwmp.vo.CafVo;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.vo.CustomerClosures;
import com.cupola.fwmp.vo.ETRUpdatebyNE;
import com.cupola.fwmp.vo.InputVendorVO;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.RollBackVO;
import com.cupola.fwmp.vo.TabDetailsVo;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserCafVo;
import com.cupola.fwmp.vo.UserGroupVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.VendorVO;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.gis.GISFesiabilityInput;
import com.cupola.fwmp.vo.sales.ImageInput;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.ws.TicketService;
import com.cupola.fwmp.ws.sales.DocumentService;

@Path("/PFM")
public class ProcessFlow
{

	private static Logger log = LogManager.getLogger(ProcessFlow.class);

	@Autowired
	AreaCoreService areaService;

	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	TicketCoreService ticketDetailsService;

	@Autowired
	TicketService ticketService;

	@Autowired
	TicketCoreService ticketDetailService;

	@Autowired
	TariffCoreService tariffCoreService;

	@Autowired
	UserService userService;

	@Autowired
	SubAreaService subAreaService;

	@Autowired
	MaterialsCoreService materialsCoreService;
	
	@Autowired 
	private MaterialRequestService materialRequestService;

	@Autowired
	DocumentService documentService;

	@Autowired
	CityCoreService cityService;

	@Autowired
	BranchCoreService branchService;

	@Autowired
	TabletService tabletService;

	@Autowired
	AATCoreService aatCoreServiceImpl;

	@Autowired
	TicketActivityLogCoreService ticketActivityLogService;

	@Autowired
	ClassificationEngineCoreService classificationEngineCoreService;

	@Autowired
	ReportCoreService reportCoreService;

	@Autowired
	VendorCoreService vendorCoreService;

	@Autowired
	GISServiceIntegrationImpl gisService;

	@Autowired
	RollBackService rollBackService;

	@Autowired
	ProspectCoreService prospectCoreService;

	@Autowired
	AuthUtils authUtils;

	@Autowired
	WorkOrderTypeCoreService workOrderTypeService;

	@Autowired
	WorkOrderCoreService workOrderCoreService;

	@Autowired
	WorkStageCoreService workStageCoreService;

	@Autowired
	UserGroupCoreService userGroupCoreService;

	@Autowired
	TicketCoreService ticketServiceimpl;

	@Autowired
	UserCafCoreService userCafCoreService;
	@Autowired
	DBUtil dbUtil;
	@Autowired
	GlobalCoreService globalCoreService;
	
	@Autowired
	private MQClosureHelper mqClosureHelper;

	private DocumentationCoreEngine docEngine;
	
	@Autowired
	private FrTicketService frTicketService;

	// ================Ticket details service=====================

	@POST
	@Path("ticketdetails/search")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse searchTickets(TicketFilter filter)
	{
		return ticketDetailService.searchTickets(filter);
	}

	@POST
	@Path("ticketdetails/details")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketWithDetails(TicketFilter filter)
	{

		String key;
		PagedResultsContext pc = new PagedResultsContext(filter
				.getStartOffset(), filter.getPageSize());
		ApplicationUtils.savePagingContext(pc);

		log.debug("AuthUtils.getCurrentUserLoginId() "
				+ AuthUtils.getCurrentUserId());
		log.debug("globalActivities.getAllTicketFromWorkingQueueByKey "
				+ globalActivities.getMainQueue());

		key = dbUtil.getKeyForQueueByUserId(AuthUtils.getCurrentUserId());

		Set<Long> ticketListForWorkingQueue = globalActivities
				.getAllTicketFromMainQueueByKey(key);

		if (ticketListForWorkingQueue != null
				&& !ticketListForWorkingQueue.isEmpty())
		{
			log.info(ticketListForWorkingQueue.size()
					+ " Tickets found for user " + key);
			filter.setTicketIds(new ArrayList<>(ticketListForWorkingQueue));
		} else
		{
			log.info("No Tickets found for user " + key);
			return ResponseUtil.createSuccessResponse()
					.setData(new ArrayList());
		}
		if (AuthUtils.isSalesUser())
		{

			return ticketDetailsService.getPreSaleTicketWithDetails(filter);
		} else if (AuthUtils.isNIUser())
		{
			return ticketDetailsService.getTicketWithDetails(filter);
		} else
		{
			return ticketDetailsService.getTicketWithDetails(filter);
		}
	}

	// ================Ticket service=====================

	@POST
	@Path("ticket/status/counts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getCountSummary()
	{

		return ticketServiceimpl.getCountSummary();

	}

	@POST
	@Path("ticket/assignto/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse assignTickets(@PathParam("userId") long userId,
			TicketFilter filter)
	{

		return ticketService.assignTickets(userId, filter);
	}

	@POST
	@Path("ticket/assignto")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse assignTickets(AssignVO assignVO)
	{

		return ticketService.assignTickets(assignVO);
	}

	@GET
	@Path("ticket/counts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getCounts()
	{
		return ResponseUtil.createSuccessResponse();
		// return ticketServiceimpl.getTicketCount();
	}

	@POST
	@Path("ticket/etr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateTicketEtrByNEorTL(ETRUpdatebyNE etrUpdatebyNE)
	{

		return ticketService.updateTicketEtrByNEorTL(etrUpdatebyNE);

	}

	@POST
	@Path("ticket/caf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateTicketCaf(CafVo cafVo)
	{
		return ticketService.updateTicketCaf(cafVo);

	}

	// =================== Feasibility check ======================

	@Path("GIS/feasibility/checkfeasibility")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse checkFeasibilityThroughGIS(GISFesiabilityInput gisInput)
	{
		return ResponseUtil.createFeasibleSuccessResponse()
				.setData(gisService.actGISFesibility(gisInput));
	}

	// ================User Services===================
	@POST
	@Path("user/getusername")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUserName()
	{
		return userService.getUserName();
	}

	@POST
	@Path("user/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addUser(UserVo user)
	{
		return userService.addUser(user);
	}

	@POST
	@Path("user/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateUser(UserVo user)
	{
		return userService.updateUser(user);
	}

	@POST
	@Path("user/resetpassword/{oldpassword}/{newpassword}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse resetPassword(
			@PathParam("oldpassword") String oldpassword,
			@PathParam("newpassword") String newpassword)
	{

		return userService.resetPassword(oldpassword, newpassword);
	}

	@POST
	@Path("user/getallusernames")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllUserNames()
	{
		return userService.getAllUserNames(null);
	}

	@POST
	@Path("user/getAllUsers")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllUsers()
	{
		return userService.getAllUsers();
	}

	@POST
	@Path("user/deleteuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse deleUser(UserVo user)
	{
		return userService.deleteUser(user);
	}

	@POST
	@Path("user/getallelementmacs")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getAllElementMacs()
	{
		return userService.getAllElementMacs();
	}

	@POST
	@Path("user/list")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUsers(UserFilter filter)
	{

		return userService.getUsers(filter);
	}

	@POST
	@Path("user/usersuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> userSearchSuggestion(
			@PathParam("query") String userQuery)
	{

		return userService.userSearchSuggestion(userQuery);
	}

	@POST
	@Path("user/tabsuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> tabSearchSuggestion(
			@PathParam("query") String userQuery)
	{

		return userService.tabSearchSuggestion(userQuery);
	}

	@POST
	@Path("user/elementsuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> elementSearchSuggestion(
			@PathParam("query") String elementQuery)
	{

		return userService.elementSearchSuggestion(elementQuery);
	}

	@POST
	@Path("user/citysuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> citySuggestion(
			@PathParam("query") String cityQuery)
	{

		return userService.citySearchSuggestion(cityQuery);
	}

	@POST
	@Path("user/branches/{cityname}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getBranchesByCityName(
			@PathParam("cityname") String cityName)
	{

		return userService.getBranchesByCityName(cityName);
	}

	@POST
	@Path("user/areas/{branchname}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAreasByBranchName(
			@PathParam("branchname") String branchName)
	{

		return userService.getAreasByBranchName(branchName);
	}

	@POST
	@Path("user/subareas/{areaname}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getSubAreasByAreaName(
			@PathParam("areaname") String areaName)
	{

		return userService.getSubAreasByAreaName(areaName);
	}

	@POST
	@Path("user/devices/{branchid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getDevicesByBranchId(
			@PathParam("branchid") String branchId)
	{

		return userService.getDevicesByBranchId(branchId);
	}

	@POST
	@Path("user/vendors")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getVendorsByBranchName(TypeAheadVo typeAheadVo)
	{

		return userService.getVendorsByBranchName(typeAheadVo);
	}

	@POST
	@Path("user/getUserByDeviceMac/{fxName}/{role}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUserByDeviceMac(@PathParam("fxName") String fxName,
			@PathParam("role") String role)
	{

		return userService.getUserByDeviceMac(fxName, role);
	}

	@POST
	@Path("user/skills")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getSkillsByVendorName(TypeAheadVo typeAheadVo)
	{

		return userService.getSkillsByVendorName(typeAheadVo);
	}

	@POST
	@Path("user/roles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getUserRoles()
	{

		return userService.getUserRoles();
	}

	@POST
	@Path("user/associated/{includeReportTo}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAssociatedUsers(
			@DefaultValue("true") @PathParam("includeReportTo") Boolean includeReportTo)
	{

		return ResponseUtil.createSuccessResponse()
				.setData(userService.getAssociatedUsers(null, includeReportTo));

	}

	@POST
	@Path("user/associated/context/{pageContext}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAssociatedUsers(
			@DefaultValue("true") @PathParam("includeReportTo") Boolean includeReportTo,
			@PathParam("pageContext") Integer pageContext)
	{

		return ResponseUtil.createSuccessResponse().setData(userService
				.getAssociatedUsersForContext(null, includeReportTo, pageContext));

	}

	@GET
	@Path("user/current")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse currentUser()
	{
		UserDetails userDetails = (UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		return ResponseUtil.createSuccessResponse().setData(userDetails);
	}

	// ================User Group Services===================

	@POST
	@Path("usergroup/list")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUserGroupUsers(UserFilter filter)
	{

		return userGroupCoreService.getUserGroups(filter);
	}

	@POST
	@Path("usergroup/list/usergroupnames")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllUserGroupNames()
	{

		return userGroupCoreService.getAllGroupNames();
	}

	@POST
	@Path("usergroup/list/roles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllRoles()
	{

		return userGroupCoreService.getAllRoleNames();

	}

	@POST
	@Path("usergroup/list/usergroup")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllUserGroups()
	{

		return userGroupCoreService.getAllUserGroups();

	}

	@POST
	@Path("usergroup/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addNewUserGroup(UserGroupVo userGroupVo)
	{
		return userGroupCoreService.addNewUserGroup(userGroupVo);
	}

	@POST
	@Path("usergroup/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateUserGroup(UserGroupVo userGroupVo)
	{
		return userGroupCoreService.updateUserGroup(userGroupVo);
	}

	@POST
	@Path("usergroup/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deleteUserGroup(UserGroupVo userGroupVo)
	{
		return userGroupCoreService.deleteUserGroup(userGroupVo);
	}

	// ================UserCAF Services===================

	@POST
	@Path("usercaf/add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse saveAndUpdateUserCAF(UserCafVo userCafVo)
	{

		return userCafCoreService.saveAndUpdateUserCAF(userCafVo);
	}

	@POST
	@Path("usercaf/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deleteUserCAF(UserCafVo userCafVo)
	{

		return userCafCoreService.deleteUserCAF(userCafVo);
	}

	@POST
	@Path("usercaf/id")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUserCAFByUserId(UserCafVo userCafVo)
	{
		return userCafCoreService.getUserCAFByUserId(userCafVo);
	}

	@POST
	@Path("usercaf/list")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getListUserCAF()
	{

		return userCafCoreService.getListUserCAF();
	}

	// =========== Activity Services ===============

	@POST
	@Path("activity/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse handleUpdateActivity(
			TicketAcitivityDetailsVO ticketAcitivityDetailsVO)
	{
		if (!dbUtil.isTicketAvailable(Long.valueOf(ticketAcitivityDetailsVO
				.getTicketId()), AuthUtils.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();

		return ticketActivityLogService
				.addTicketActivityLog(ticketAcitivityDetailsVO);
	}

	@POST
	@Path("activity/rollback")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse handleRollbackActivityStatus(RollBackVO rollBackVO)
	{
		log.info("CAlling activity roll back " + rollBackVO);
		return rollBackService.rollBackActivity(rollBackVO);
	}

	// =========== Vendor Services ===============

	@POST
	@Path("vendor/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse handleVendor(VendorVO vendor)
	{
		return vendorCoreService.addVendor(vendor);
	}

	@POST
	@Path("vendor/all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse handlegetAllVendor()
	{
		return vendorCoreService.getAllVendors();
	}

	@POST
	@Path("vendor/counts")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse handlegetCountAllVendor()
	{
		return vendorCoreService.getCountAllVendor();
	}

	@POST
	@Path("vendor/count/{skillId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse handlegetCountAllVendorBasedonSkill(
			@PathParam("skillId") Long id)
	{
		return vendorCoreService.getCountAllVendorBySkill(id);
	}

	@POST
	@Path("vendor/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse handleupdateVendor(VendorVO vendor)
	{
		return vendorCoreService.updateVendor(vendor);
	}

	@POST
	@Path("vendor/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse handleudeleteVendor(Long vendorId)
	{
		return vendorCoreService.deleteVendor(vendorId);
	}

	@POST
	@Path("vendor/add/dailycounts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addVendorSkillAndCount(VendorVO vendorvo)
	{
		return vendorCoreService.addVendorSkillAndCount(vendorvo);
	}

	@POST
	@Path("vendor/updateCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateCountSkill(InputVendorVO inputVendorvo)
	{
		return vendorCoreService.updateCountSkill(inputVendorvo);
	}

	@POST
	@Path("vendor/assignForTL")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse vendorAssignmentForTL(InputVendorVO inputVendorvo)
	{
		return vendorCoreService.vendorAssignmentForTL(inputVendorvo);
	}

	@POST
	@Path("vendor/addUserVendor")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addUserVendorMapping(VendorVO vendorvo)
	{
		return vendorCoreService.addUserVendorMapping(vendorvo);
	}

	@POST
	@Path("vendor/assignByTLForNE")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse vendorAssignmentByTLForNE(InputVendorVO inputVendorvo)
	{
		Long userId = AuthUtils.getCurrentUserId();
		int roleId = AuthUtils.getCurrentUserRole();
		inputVendorvo.setUserId(String.valueOf(userId));
		inputVendorvo.setRoleId(String.valueOf(roleId));
		return vendorCoreService.vendorAssignmentByTLForNE(inputVendorvo);
	}

	@POST
	@Path("vendor/detailsForTL&NE")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getVendorDetails()
	{
		Long userId = AuthUtils.getCurrentUserId();
		return vendorCoreService.getVendorDetails(userId);
	}

	@POST
	@Path("vendor/detailsForAndroid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getVendorDetailsForWeb()
	{
		Long userId = AuthUtils.getCurrentUserId();
		return vendorCoreService.getVendorDetailsForAndroid(userId);
	}

	@POST
	@Path("vendor/byfilter")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getVendorsOnFilter(VendorFilter vendorFilter)
	{
		return vendorCoreService.getVendorsOnFilter(vendorFilter);
	}

	// =========== AAT Services ===============

	@POST
	@Path("aat/details")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAATDetailsforCustomer(
			CustomerClosures customerClosures)
	{

		if (customerClosures == null)
		{
			log.info("AATResponse can't be null" + customerClosures);
			return ResponseUtil.createFailureResponse();
		}

		aatCoreServiceImpl.getAATDetailsforCustomer(customerClosures);

		/*
		 * Job job = new Job();
		 * 
		 * job.setRegistrationId(aatVo.getRegistrationId());
		 * job.setExternalJobId(aatVo.getUserId());
		 * 
		 * JobDAOImpl jo = new JobDAOImpl();
		 * 
		 * jo.sendGcmNotificationToApp(job);
		 */

		// get user by mqId
		// get regKey for GCM
		// send GCM notification.

		// GCMMessage
		return aatCoreServiceImpl.getAATDetailsforCustomer(customerClosures);
	}

	// =========== WorkOrder Services ===============

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("workorder/types")
	public APIResponse getTicketTypes()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(workOrderTypeService.getAllWorkOrderTypes());
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("workorder/stages")
	public APIResponse getWorkOrderStages()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(workStageCoreService.getWorkStages());
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("workorder/status")
	public APIResponse getWorkOrderStatuses()
	{

		Map<Long, String> values = new HashMap<Long, String>();
		for (Map.Entry<Integer, String> entry : FWMPConstant.TicketStatus.statusDisplayValue
				.entrySet())
		{
			values.put(entry.getKey().longValue(), entry.getValue());
		}
		return ResponseUtil.createSuccessResponse().setData(values);
	}

	@POST
	@Path("workorder/updatepriority/{priority}/{workOrderNo}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updatePriority(@PathParam("priority") int priority,
			@PathParam("workOrderNo") String workOrderNo)
	{
		return workOrderCoreService.updatePriority(workOrderNo, priority);
	}

	// =========== Area Services ===============

	@POST
	@Path("area/add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addArea(List<AreaVO> areaVOs)
	{

		return areaService.addAreaes(areaVOs);
	}

	@POST
	@Path("area/list/{branchid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAreasByBranchId(
			@PathParam("branchid") String branchId)
	{
		return areaService.getAreasByBranchId(branchId);
	}

	// =========== Branch Services ===============

	@POST
	@Path("branch/add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addBranch(List<BranchVO> branchVOs)
	{
		return branchService.addBranches(branchVOs);
	}

	@POST
	@Path("branch/list/{cityid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getBranchesByCityId(
			@PathParam("cityid") String cityId)
	{

		return branchService.getBranchesByCityId(cityId);
	}

	// =========== City Services ===============

	@POST
	@Path("/add/cities")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addCities(List<CityVO> cityVOs)
	{
		return cityService.addCities(cityVOs);
	}

	@POST
	@Path("city/add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addCity(CityVO cityVOs)
	{
		return cityService.addCity(cityVOs);
	}

	@POST
	@Path("city/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllCity()
	{

		return cityService.getAllCity();
	}

	// =========== Document Services ===============

	@Path("doc/uploaddoc")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse createFolderForProspect(ImageInput imageInput)
	{

		if (imageInput.getPropspectNo() == null)
			return ResponseUtil.nullArgument();

		String filePath = docEngine.createFolder(imageInput.getPropspectNo());

		if (docEngine.uploadImage(imageInput, filePath))
		{

			return ResponseUtil.createUploadSuccessResponse();

		} else
		{

			return ResponseUtil.createUploadFailsResponse();

		}

	}

	@GET
	@Path("doc/{prospectId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllCustomerDocument(
			@PathParam("prospectId") String prospectId)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(docEngine.getAllDocumentByProspectNumber(prospectId));
	}

	@GET
	@Path("doc/{prospectId}/{filename}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response openCustomerDocument(
			@PathParam("prospectId") String prospectId,
			@PathParam("filename") String filename)
	{
		File file = docEngine.getDocumentByFileName(prospectId, filename);
		try
		{
			if (file != null)
			{
				String extension = FilenameUtils.getExtension(file.getName());
				return getResponse(file, filename, extension);
			}
		} catch (Exception e)
		{
			log.error("Error WhileopenCustomerDocument :" + e.getMessage());

		}
		return null;
	}

	private Response getResponse(File file, String filename, String extension)
			throws Exception
	{

		Response.ResponseBuilder responseBuilder = Response.ok((Object) file);
		responseBuilder.header("Content-Disposition", "inline; filename="
				+ filename + ";");
		if (extension.equals("pdf"))
			responseBuilder.type("application/pdf");
		else if (extension.equals("png"))
			responseBuilder.type("image/png");
		else if (extension.equals("jpg"))
			responseBuilder.type("image/jpg");
		else if (extension.equals("jpeg"))
			responseBuilder.type("image/jpeg");
		else if (extension.equals("gif"))
			responseBuilder.type("image/gif");
		else if (extension.equals("txt"))
			responseBuilder.type("text/plain");
		// fileInputStream.close();
		return responseBuilder.build();

	}

	// =========== Material Services ===============

	@POST
	@Path("material/UpdateConsumption")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse UpdateMaterialConsumption(
			TicketAcitivityDetailsVO ticketAcitivityDetailsVO)
	{
		if (!dbUtil.isTicketAvailable(Long.valueOf(ticketAcitivityDetailsVO
				.getTicketId()), AuthUtils.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();

		if( AuthUtils.isFRUser())
		{
			APIResponse apiResponse = materialRequestService.addFrMaterialConsumption(ticketAcitivityDetailsVO);
			
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				frTicketService.updateFaltRepairActivityInCRM(Long.valueOf(ticketAcitivityDetailsVO.getTicketId()), null, null, null,
						null);
			}


			return apiResponse;
		}
			
		
		return materialsCoreService
				.UpdateMaterialConsumption(ticketAcitivityDetailsVO);
	}

	// ================= Report Service ===============

	@POST
	@Path("report/ticketreport")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketReport()
	{
		return reportCoreService.getCountWorkStageType();
	}

	@POST
	@Path("report/statusreport")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getStatusReport()
	{
		return reportCoreService.getStatusReport();
		// return getStatusReport();
	}

	// =========== SubArea Services ===============

	@POST
	@Path("subarea/list/{areaid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getSubAreasByAreaId(
			@PathParam("areaid") Long areaId)
	{

		return subAreaService.getSubAreasByAreaId(areaId);
	}

	// =========== Tablet Services ===============

	@POST
	@Path("tablet/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addTablet(TabletVO tabletVo)
	{
		if (tabletVo == null)
			return ResponseUtil.createNullParameterResponse();

		return tabletService.addTablet(tabletVo);
	}

	@POST
	@Path("tablet/tab/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTabById(@PathParam("id") Long id)
	{
		if (id == null)
			return ResponseUtil.createNullParameterResponse();

		return tabletService.getTabletById(id);
	}

	@POST
	@Path("tablet/tabs")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllTabs()
	{
		return tabletService.getAllTablet();
	}

	@POST
	@Path("tablet/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllTab()
	{

		return tabletService.getAllTabs();
	}

	@POST
	@Path("tablet/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deleteTab(@PathParam("id") Long id)
	{
		if (id == null)
			return ResponseUtil.createNullParameterResponse();

		return tabletService.deleteTablet(id);
	}

	@POST
	@Path("tablet/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateTablet(TabletVO tabletVo)
	{
		if (tabletVo == null)
			return ResponseUtil.createNullParameterResponse();

		return tabletService.updateTablet(tabletVo);

	}

	@POST
	@Path("tablet/updateregistrationId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateRegistrationId(TabDetailsVo tabDetailsVo)
	{
		log.debug("iside tablet resgietraion service");
		APIResponse response = tabletService.updateRegistrationId(tabDetailsVo);
		log.debug("response in tab register is 888888888888888888888888"
				+ response);
		return response;
	}

	// =========== Tarriff Services ===============

	@POST
	@Path("tarriff/id")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTariffByCity()
	{
		TariffFilter filter = new TariffFilter();
		filter.setCityId(AuthUtils.getCurrentUserCity().getId());
		return tariffCoreService.getTariffByCity(filter);
	}

	@POST
	@Path("tarriff/cusUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse UpdateCustomerTariff(TariffPlanUpdateVO tariffVo)
	{
		return tariffCoreService.UpdateCustomerTariff(tariffVo);
	}

	// =========== Prospect Services ===============

	@POST
	@Path("prospect/create")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse createNewProspect(ProspectCoreVO prospect)
	{
		if (prospect != null && prospect.getCustomer() != null)
		{
			prospect.getCustomer().setAddedBy(AuthUtils.getCurrentUserId());
			prospect.getCustomer()
					.setCityId(AuthUtils.getCurrentUserCity().getId());
		}
		return prospectCoreService.createNewProspect(prospect, null);
	}

	@POST
	@Path("prospectbynumber/create/{prospectNumber}/{mqId}/{workOrderNo}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse createNewProspect(
			@PathParam("prospectNumber") String prospectNumber,
			@PathParam("mqId") String mqId,
			@PathParam("workOrderNo") String workOrderNo,
			@PathParam("ticketCategory") String ticketCategory)
	{
		return workOrderCoreService
				.createWorkOrderByProspect(prospectNumber, mqId, workOrderNo, ticketCategory);
	}

	@POST
	@Path("ticket/defect/{ticketId}/{defectCode}/{subDefectCode}/{status}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateTicketDefectCode(
			@PathParam("ticketId") Long ticketId,
			@PathParam("defectCode") Long defectCode,
			@PathParam("subDefectCode") Long subDefectCode,
			@PathParam("status") Long status)
	{
		return ticketDetailsService
				.updateTicketDefectCode(ticketId, defectCode, subDefectCode, status);

	}

	@POST
	@Path("ticket/defect/update")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateTicketDefectCodeAndStatus(FRDefectSubDefectsVO vo)
	{
		if( vo == null )
			return ResponseUtil.nullArgument();
		
//		vo.setRemarks(AuthUtils.getCurrentUserDisplayName());
		vo.setModifiedBy(AuthUtils.getCurrentUserId());
			mqClosureHelper.doAutoClose(vo);
		return ticketDetailsService.updateTicketDefectCode(vo);

	}
}