package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreService;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;

@Path("ticketactivity")
public class TicketActivityLogService {

	private Logger log = Logger.getLogger(TicketActivityLogService.class
			.getName());
	TicketActivityLogCoreService ticketActivityLogService;

	public void setTicketActivityLogService(
			TicketActivityLogCoreService ticketActivityLogService) {
		this.ticketActivityLogService = ticketActivityLogService;
	}

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addTicketActivityLog(
			TicketAcitivityDetailsVO ticketActivityVo) {

		log.info("TicketActivity Service :");

		return ticketActivityLogService
				.addTicketActivityLog(ticketActivityVo);
	}


}
