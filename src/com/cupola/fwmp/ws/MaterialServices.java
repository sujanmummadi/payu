package com.cupola.fwmp.ws;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.materials.MaterialCache;
import com.cupola.fwmp.service.materials.MaterialsCoreService;
import com.cupola.fwmp.vo.MaterialRequestVO;
import com.cupola.fwmp.vo.MultipleMaterialConsumptionVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;

@Path("/materials")
@Transactional
public class MaterialServices
{
	final static Logger log = Logger.getLogger(MaterialServices.class);

	private MaterialsCoreService materialsCoreService;

	public MaterialsCoreService getMaterialsCoreService()
	{
		return materialsCoreService;
	}

	public void setMaterialsCoreService(
			MaterialsCoreService materialsCoreService)
	{
		this.materialsCoreService = materialsCoreService;
	}

	@POST
	@Path("/UpdateConsumption")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)

	public APIResponse UpdateMaterialConsumption(
			TicketAcitivityDetailsVO ticketAcitivityDetailsVO)
	{
		return materialsCoreService
				.UpdateMaterialConsumption(ticketAcitivityDetailsVO);
	}

	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getListOfMaterials()
	{
		log.info("Inside getListOfMaterials() of MaterialServices");
		Long time1 = new Date().getTime();
		APIResponse apiResponse = materialsCoreService.getAllMaterials();
		log.info(new Date().getTime()
				- time1
				+ "-----------------------------------------------------Total time");
		return apiResponse;
	}

	@POST
	@Path("/resetCache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void resetMaterialCache()
	{
		log.info("Inside resetMaterialCache() of MaterialServices");
		MaterialCache.resetCache();
	}

	@POST
	@Path("/updateMultipleMaterialConsuption")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateMultipleMaterialConsumption(
			MultipleMaterialConsumptionVO multipleMaterialConsumptionList)
	{
		log.info("Inside updateMultipleMaterialConsumption() of MaterialServices");
		Long time1 = new Date().getTime();
		APIResponse apiResponse = materialsCoreService
				.updateMultipleMaterialConsumption(multipleMaterialConsumptionList);
		log.info(new Date().getTime() - time1
				+ "-----------------------------------------------------Total time taken to updateMultipleMaterialConsumption()");
		return apiResponse;
	}

	@POST
	@Path("raiseMaterialRequest")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse raiseMaterialRequest(
			List<MaterialRequestVO> materialRequestList)
	{
		log.debug("In raiseMaterialRequest(). materialRequestList i/p is  "
				+ materialRequestList);
		Long time1 = new Date().getTime();
		log.info("Inside raiseMaterialRequest() of MaterialServices");
		APIResponse apiResponse = materialsCoreService
				.raiseMaterialRequest(materialRequestList);
		log.info(new Date().getTime() - time1
				+ "-----------------------------------------------------Total time taken to raiseMaterialRequest()");
		return apiResponse;
	}

}
