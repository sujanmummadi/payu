
package com.cupola.fwmp.ws.user;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.user.UserCacheCoreService;

/**
 * @author aditya
 * @created 6:01:26 PM Apr 6, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */
@Path("integ/usercacheservice")
public class UserCacheService
{
	@Autowired
	UserCacheCoreService userCacheCoreService;
	

	@POST
	@Path("/userareacache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userAreaCache()
	{
		return userCacheCoreService.userAreaCache();
	}

	@POST
	@Path("/userbranchcache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userBranchCache()
	{
		return userCacheCoreService.userBranchCache();
	}

	@POST
	@Path("/usercitycache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userCityCache()
	{
		return userCacheCoreService.userCityCache();
	}

	@POST
	@Path("/userdevicecache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userDeviceCache()
	{
		return userCacheCoreService.userDeviceCache();
	}

	@POST
	@Path("/tabletcache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse tabletCache()
	{
		return userCacheCoreService.tabletCache();
	}

	@POST
	@Path("/usertabletcache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userTabletCache()
	{
		return userCacheCoreService.userTabletCache();
	}

	@POST
	@Path("/userskillcache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userSkillCache()
	{
		return userCacheCoreService.userSkillCache();
	}

	@POST
	@Path("/useremployeercache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userEmployeerCache()
	{
		return userCacheCoreService.userEmployeerCache();
	}

	@POST
	@Path("/devicecache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deviceCache()
	{
		return userCacheCoreService.deviceCache();
	}

	@POST
	@Path("/userusergroupcache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse userUserGroupCache()
	{
		return userCacheCoreService.userUserGroupCache();
	}

}
