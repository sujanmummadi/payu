package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.MessagePlaceHolder;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.MessageNotificationVo;

@Path("notifycustomer")
@Produces(MediaType.APPLICATION_JSON)
public class CustomerNotificationService
{

	@Autowired
	NotificationService notificationService;

	@Autowired
	DefinitionCoreService coreService;
	
	@Autowired
	CustomerDAO customerDAO;

	private static final Logger LOGGER = LogManager
			.getLogger(CustomerNotificationService.class.getName());

	@POST
	@Path("sendnotification")
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse sendCustomerNotification(
			MessageNotificationVo notificationVo)
	{

		LOGGER.info("notificationVo contains information=========== "
				+ notificationVo);

		String statusMessageCode = coreService
				.getNotificationMessageFromProperties("111");

		String messageToSend = coreService
				.getMessagesForCustomerNotificationFromProperties(statusMessageCode);

		if(messageToSend != null)
		{	
			CustomerVO customer = customerDAO.getCustomerByTicketId(Long.valueOf(notificationVo.getTicketId()));
			
			String helpLineNo = 
					coreService.getNIHelpLineNumber(
							LocationCache.getCityCodeById(customer.getCityId()));
			String portalUrl = coreService
					.getPortalPageUrl(LocationCache
							.getCityCodeById(customer.getCityId()));

			
			messageToSend = messageToSend
					.replaceAll(MessagePlaceHolder.HELP_LINE_NUMBER, helpLineNo)
					.replaceAll(MessagePlaceHolder.PORTAL_URL, portalUrl);
			
			
		
		}
		
		

		if (notificationVo != null && notificationVo.getMobileNumber() != null)
		{
			notificationVo.setMessage(messageToSend);
		  notificationService
					.sendNotification(notificationVo);


		return ResponseUtil.createSuccessResponse();

		}

		else
		{

			LOGGER.info("notificationVo is null");
			return ResponseUtil.createFailureResponse();
		}
	}

}
