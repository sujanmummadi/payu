package com.cupola.fwmp.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FrEscalation;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.Escalation;
import com.cupola.fwmp.FWMPConstant.PriorityFilter;
import com.cupola.fwmp.FWMPConstant.ProspectType;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.activity.ActivityCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.service.workOrderType.WorkOrderTypeCoreService;
import com.cupola.fwmp.service.workStage.WorkStageCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
//import com.cupola.fwmp.vo.AppointmentVO;
import com.cupola.fwmp.vo.TypeAheadVo;

@Path("/workorder")
public class WorkOrderService
{

	@Autowired
	WorkOrderTypeCoreService workOrderTypeService;

	@Autowired
	WorkOrderCoreService workOrderCoreService;

	@Autowired
	WorkStageCoreService workStageCoreService;

	@Autowired
	DefinitionCoreService definitionService;

	@Autowired
	ActivityCoreService activityCoreService;
	
	@Autowired
	private FrTicketService frTicketService;
	
	@Autowired
	DBUtil dbUtil;

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("types")
	public APIResponse getTicketTypes()
	{

		return ResponseUtil.createSuccessResponse().setData(
				workOrderTypeService.getAllWorkOrderTypes());
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("stages")
	public APIResponse getWorkOrderStages()
	{

		List<TypeAheadVo> stages = new ArrayList<TypeAheadVo>();
		TypeAheadVo vo = new TypeAheadVo(-1l, "All");
		stages.add(vo);
		vo = new TypeAheadVo(WorkStageType.FIBER, WorkStageName.FIBER);
		stages.add(vo);
		vo = new TypeAheadVo(WorkStageType.COPPER, WorkStageName.COPPER);
		stages.add(vo);

		return ResponseUtil.createSuccessResponse().setData(stages);
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("priorities")
	public APIResponse getWorkOrderPriorities()
	{
		return ResponseUtil.createSuccessResponse().setData(
				CommonUtil.xformToTypeAheadFormat(PriorityFilter.definition));
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("prospecttypes")
	public APIResponse getProspectTypes()
	{
		List<TypeAheadVo> options = new ArrayList<TypeAheadVo>();
		options.add(new TypeAheadVo(-1l, "All"));
		options.addAll(CommonUtil.xformToTypeAheadFormat(ProspectType.def));

		return ResponseUtil.createSuccessResponse().setData(options);
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("status/{page}")
	public APIResponse getWorkOrderStatuses(@PathParam("page") Integer page)
	{
		if( AuthUtils.isFRUser() )
			return getFrFilterWorkOrderStatus();
		
		List<TypeAheadVo> statusList = new ArrayList<TypeAheadVo>();
		TypeAheadVo vo = new TypeAheadVo(-1l, "All");
		statusList.add(vo);
		Set<Integer> codeDefinition = definitionService
				.getStatusFilterForCurrentUser(true, page);
		for (Integer id : codeDefinition)
		{
			vo = new TypeAheadVo(Long.valueOf(id),
					TicketStatus.statusCodeValue.get(id));
			statusList.add(vo);
		}
		return ResponseUtil.createSuccessResponse().setData(statusList);
	}

	private APIResponse getFrFilterWorkOrderStatus()
	{
		List<TypeAheadVo> statusList = new ArrayList<TypeAheadVo>();
		TypeAheadVo vo = new TypeAheadVo(-1l, "All");
		statusList.add(vo);
		
		Set<Integer> codeDefinition = definitionService
				.getFrCurrentUserStatusFilterForSearch();
		
		for (Integer id : codeDefinition) 
		{
			vo = new TypeAheadVo(Long.valueOf(id),
					definitionService.getFrStatusStringByStatusId(Long
							.valueOf(id)));
			statusList.add(vo);
		}
		
		return ResponseUtil.createSuccessResponse().setData(statusList);
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("subcategories")
	public APIResponse getWorkOrderSubCategories()
	{
		Map<Long, String> subCategory = new HashMap<Long, String>();
		subCategory.putAll(TicketSubCategory.subCategoryCodeValue);
		subCategory.put(-1l, "Select");

		return ResponseUtil.createSuccessResponse().setData(
				CommonUtil.xformToTypeAheadFormat(subCategory));
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("activities")
	public APIResponse getAllActivity()
	{

		return ResponseUtil.createSuccessResponse().setData(
				activityCoreService.getAllActivity());
	}

	@POST
	@Path("updatepriority/{priority}/{workOrderNo}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updatePriority(@PathParam("priority") int priority,
			@PathParam("workOrderNo") String workOrderNo)
	{

		return workOrderCoreService.updatePriority(workOrderNo, priority);
	}

	@POST
	@Path("updatepriority/{priority}/{workOrderNo}/{escalationType}/{escalatedValue}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updatePriority(@PathParam("priority") int priority,
			@PathParam("workOrderNo") String workOrderNo,
			@PathParam("escalationType") Integer escalationType,
			@PathParam("escalatedValue") Integer escalatedValue)
	{
		if( AuthUtils.isFRUser() )
			return updateFrPriority(priority,workOrderNo,escalationType
					,escalatedValue);
		
		if (escalationType != null && escalationType > 0)
		{
			escalatedValue = Escalation.values.get(escalationType.longValue());
			if (escalatedValue != null)
				priority = priority + escalatedValue;
		}

		return workOrderCoreService.updatePriority(workOrderNo, priority,
				escalationType, escalatedValue);
	}

	

	private APIResponse updateFrPriority(int priority, String workOrderNo,
			Integer escalationType, Integer escalatedValue)
	{
		if ( escalationType != null && escalationType > 0 )
		{
			escalatedValue = FrEscalation.values.get(escalationType.longValue());
			if (escalatedValue != null)
				priority = priority + escalatedValue;
		}else
			escalationType = 0;
		
		APIResponse apiResponse = workOrderCoreService.updateFrPriority(workOrderNo, priority,
				escalationType, escalatedValue);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			frTicketService.updateFaltRepairActivityInCRM(null, null, workOrderNo+"", null, null);
		}
		
		return apiResponse;
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("escalations")
	public APIResponse getEscalationsPriorities()
	{
		if( AuthUtils.isFRUser() )
			return ResponseUtil.createSuccessResponse().setData(
					CommonUtil.xformToTypeAheadFormat(FrEscalation.def));
		
		return ResponseUtil.createSuccessResponse().setData(
				CommonUtil.xformToTypeAheadFormat(Escalation.def));
	}
	
	
	/*@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("update/appointment")
	public APIResponse updateAppointmentDate(AppointmentVO appointmentVO)
	{

		return ResponseUtil.createSuccessResponse().setData(
				workOrderCoreService.updateAppointmentDate(appointmentVO));
	}*/

	
	
	
	
	
	
	
}
