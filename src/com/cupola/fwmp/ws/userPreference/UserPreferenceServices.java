package com.cupola.fwmp.ws.userPreference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.UserPreferenceType;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.user.UserPreference;
import com.cupola.fwmp.service.user.UserPreferenceService;
import com.cupola.fwmp.service.user.UserPreferenceVO;
import com.cupola.fwmp.service.workOrderType.WorkOrderTypeCoreService;
import com.cupola.fwmp.service.workStage.WorkStageCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.NotificationOptions;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("userPref")
public class UserPreferenceServices {
	
	@Autowired
	UserPreferenceService userPrefService;
	
	@Autowired
	WorkStageCoreService workStageCoreService;
	@Autowired
	DefinitionCoreService  definitionService;
	
	@Autowired
	WorkOrderTypeCoreService workOrderTypeService;
	private static Logger logger = Logger.getLogger(UserPreferenceServices.class
			.getName());
	@POST
	@Path("add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse createUserPreference (UserPreferenceVO userPrefVO)
	{
		Long userGuid =AuthUtils.getCurrentUserId();
		return ResponseUtil.createSaveSuccessResponse().setData(userPrefService.createUserPreference(userGuid,userPrefVO));
	}
	
	@GET
	@Path("findByUserId")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserPreference> getUserPreferencesByUserId(){
		Long userId =AuthUtils.getCurrentUserId();
		return userPrefService.getUserPreferencesByUserId(userId);
	}
	
	@GET
	@Path("findByUserLoginId")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserPreference> getUserPreferencesByUserLoginId(){
 		String userLoginId = AuthUtils.getCurrentUserLoginId();
		return userPrefService.getUserPreferencesByUserLoginId(userLoginId);
	}
	
	@POST
	@Path("notification/update")
	@Produces(MediaType.APPLICATION_JSON)	public APIResponse updateNotificationPreference(NotificationOptions options){
 		return userPrefService.createOrUpdateNotificationPreference(options);
	}

	
	
	@POST
	@Path("notification")
	@Produces(MediaType.APPLICATION_JSON)	public APIResponse getNotificationPreference( ){
 		
 		UserPreference uf = userPrefService
				.getUserPreferencesByPreferenceName(AuthUtils.getCurrentUserId(), UserPreferenceType.NOTIFICATION_BAR);
		 if (uf != null && uf.getPrefStringValue() != null)
		{
			return ResponseUtil.createSuccessResponse().setData(uf.getPrefStringValue());
		}
		 else
			 return ResponseUtil.createFailureResponse();
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("notificationoptions")
	public APIResponse getNotificationOptions()
	{

		return userPrefService.getNotificationOption();
	}


	
}
