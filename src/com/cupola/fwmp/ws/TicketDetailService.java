package com.cupola.fwmp.ws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.mongo.logger.LoggerDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.handler.FrTicketHandler;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.PagedResultsContext;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.fr.FRVendorFilter;

@Path("ticketdetails")
public class TicketDetailService
{

	@Autowired
	TicketCoreService ticketService;
	@Autowired
	FrTicketService frTicketService;
	
	@Autowired
	GlobalActivities globalActivities;
	@Autowired
	TicketCoreService ticketCoreService;
	@Autowired
	DefinitionCoreService definitionService;
	@Autowired
	LoggerDao loggerDao;
	@Autowired
	DBUtil dbUtil;
	
	@Autowired
	private FrTicketHandler frTicketHandler;
	
	/*@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTickets(TicketFilter filter)
	{

		return ticketService.getTickets(filter);
	}*/

	private static Logger log = LogManager.getLogger(TicketDetailService.class);

	@POST
	@Path("search")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse searchTickets(TicketFilter filter)
	{
		if(filter.getTicketeCategory() != null && TicketCategory.FAULT_REPAIR_BOTH.contains(filter.getTicketeCategory()))
		{
			return frTicketService.searchFrTicketDetailsBy(filter);
		}
		else if (AuthUtils.isCFEUser()) {
			return getCFETickets(filter, null);
		}
		else
			return ticketService.searchTickets(filter);
	}

	@POST
	@Path("details")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketWithDetails(TicketFilter filter)
	{
		log.debug("################## @@@@@@@@@@@@@@@@@@ Ticket details are " + filter);
		
		PagedResultsContext pc = new PagedResultsContext(filter.getStartOffset(), filter
				.getPageSize());
		ApplicationUtils.savePagingContext(pc);
		
		if ( AuthUtils.isFRGroupUser() )
			return frTicketHandler.getFrTicketDetail(filter);
		
		Set<Long> ticketListForWorkingQueue = null;
		if(AuthUtils.isCCUser())
		{
			return	getAllTickets(filter, null);
		}
		
		if (filter.getTicketStatus() == null || filter.getTicketStatus() <= 0)
		{
			filter.setTicketStatus(-1);
			if(filter.getTicketStatusList() != null && filter.getTicketStatusList().size() > 0)
			{
				
			}
			else if(filter.getTicketIds() == null || filter.getTicketIds().size() == 0)
			{
				if (filter.getPageContextId() != null
						&& filter.getPageContextId() > 0 && filter.getPageContextId().intValue()  != DefinitionCoreService.MQ_FAILURE_RELATED_STATUS && filter.getPageContextId().intValue() != DefinitionCoreService.ECAF_RELATED_STATUS )
					filter.setTicketStatusList(new ArrayList<Integer>(definitionService
							.getStatusFilterForCurrentUser(true, filter
									.getPageContextId())));
				else
					filter.setTicketStatusList(new ArrayList<Integer>(definitionService
							.getStatusFilterForCurrentUser(true, null)));
			}
		}
		

		if (AuthUtils.isCFEUser() || AuthUtils.isSalesManager())
		{
			log.info(" Sales loginId :" + AuthUtils.getCurrentUserLoginId());

			if (AuthUtils.isCFEUser())
			{
				filter.setBranchId(AuthUtils.getCurrentUserBranch().getId());
				return getCFETickets(filter,null);
			}
			if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_BM)
				filter.setBranchId(AuthUtils.getCurrentUserBranch().getId());
			else if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_AM)
//				filter.setAreaId(AuthUtils.getCurrentUserArea().getId());
			{
				if (AuthUtils.getCurrentUsersAllArea() != null)
					filter.setAreaIds(new ArrayList<>(AuthUtils.getCurrentUsersAllArea().keySet()));
				else
					filter.setAreaIds(Arrays.asList(new Long[]{1l}));
			}
			else if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_CM)
				filter.setCityId(AuthUtils.getCurrentUserCity().getId());
			if(filter.getAssignedUserIds() != null
					&& filter.getAssignedUserIds().size() > 0)
					{
						Set<Long> ticketIds = getAllTicketsForUsers(filter.getAssignedUserIds());
						if(ticketIds == null)
							return ResponseUtil.createSuccessResponse()	.setData(null);
						
						filter.setTicketIds( new ArrayList<Long>(ticketIds));
					}
			return getSalesTickets(filter, null);
		}

		if (AuthUtils.isNIManager())
		{
			log.info(" NI Executives   " + AuthUtils.getCurrentUserLoginId());

			if (AuthUtils.getIfExecutiveRole() == UserRole.NI_BM)
				filter.setBranchId(AuthUtils.getCurrentUserBranch().getId());
			else if (AuthUtils.getIfExecutiveRole() == UserRole.NI_AM)
			{
				if (AuthUtils.getCurrentUsersAllArea() != null)
					filter.setAreaIds(new ArrayList<>(AuthUtils.getCurrentUsersAllArea().keySet()));
				else
					filter.setAreaIds(Arrays.asList(new Long[]{1l}));
				
			}
			else if (AuthUtils.getIfExecutiveRole() == UserRole.NI_CM)
				filter.setCityId(AuthUtils.getCurrentUserCity().getId());
			if (filter.getTicketType() == null || filter.getTicketType() <= 0)
				filter.setTicketType(TicketCategory.NIDEPLOYMENT.intValue());
			
			if(filter.getAssignedUserIds() != null
					&& filter.getAssignedUserIds().size() > 0)
				
			{
				Set<Long> ticketIds = getAllTicketsForUsers(filter.getAssignedUserIds());
				if(ticketIds == null)
					return ResponseUtil.createSuccessResponse()	.setData(null);
				filter.setTicketIds( new ArrayList<Long>(ticketIds));
			}
			return getAllTickets(filter, null);
		}
		if (AuthUtils.isFrManager())
		{
			log.info(" FR Executives   " + AuthUtils.getCurrentUserLoginId());

			if (filter.getTicketIds() != null
					&& filter.getTicketIds().size() > 0)
				;
			else
			{
				if (AuthUtils.getIfExecutiveRole() == UserRole.FR_BM)
					filter.setBranchId(AuthUtils.getCurrentUserBranch().getId());
				else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_AM)
				{
					if (AuthUtils.getCurrentUsersAllArea() != null)
						filter.setAreaIds(new ArrayList<>(AuthUtils.getCurrentUsersAllArea().keySet()));
				}
				else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_CM)
					filter.setCityId(AuthUtils.getCurrentUserCity().getId());
			}
			if(filter.getAssignedUserIds() != null
					&& filter.getAssignedUserIds().size() > 0)
					{
					Set<Long> ticketIds = getAllTicketsForUsers(filter.getAssignedUserIds());
					if(ticketIds == null)
						return ResponseUtil.createSuccessResponse()	.setData(null);
					filter.setTicketIds( new ArrayList<Long>(ticketIds));
					}
			return getFrTicketDetails(filter);
		}
		
		if (AuthUtils.isCCNRUser() || AuthUtils.isLegalTeamUser() ||
				AuthUtils.isFinanceTeamUser() || AuthUtils.isChurnTeamUser() || AuthUtils.isNOCTeamUser())
		{
			filter.setCityId(AuthUtils.getCurrentUserCity().getId());
			return getFrTicketDetails(filter);
		}
		
		Long userId = AuthUtils.getCurrentUserId();
		if (filter.getAssignedUserId() != null
				&& filter.getAssignedUserId() > 0)
			userId = filter.getAssignedUserId();

		String key = dbUtil.getKeyForQueueByUserId(userId);
		
		if(AuthUtils.isFRUser())
			ticketListForWorkingQueue = globalActivities
			.getAllFrTicketFromMainQueueByKey(key);
		else
			ticketListForWorkingQueue = globalActivities
			.getAllTicketFromMainQueueByKey(key);
	
		if (ticketListForWorkingQueue != null
				&& !ticketListForWorkingQueue.isEmpty())
		{
			log.info(ticketListForWorkingQueue.size()
					+ " Tickets found for user " + key + " username "
					+ AuthUtils.getCurrentUserLoginId());
			List<Long> tIds = new ArrayList<>(ticketListForWorkingQueue);

			if (filter.getTicketStatus() != null
					&& filter.getTicketStatus().intValue() == TicketStatus.ETR_APPROVAL_PENDING)
			{
				tIds.retainAll(ticketCoreService.getAllETRApprovalTickets());
				filter.setTicketStatus(-1);
			}

			if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0)
				;
			else
			filter.setTicketIds(tIds);
		} else
		{
			log.info("  Tickets not found for user " + key + " username "
					+ AuthUtils.getCurrentUserLoginId());
			return ResponseUtil.createSuccessResponse()
					.setData(new ArrayList());
		}

		if(AuthUtils.isFRUser())
		{
			log.debug(" ##################### FR   " + AuthUtils.getCurrentUserLoginId() + " filter.getTicketIds " + filter.getTicketIds());
			
			return getFrTicketDetails(filter);
		}
		
		if (AuthUtils.isSalesUser())
		{
			log.debug(" ##################### Sales  " + AuthUtils.getCurrentUserLoginId() + " filter.getTicketIds " + filter.getTicketIds());
			return getSalesTickets(filter , ticketListForWorkingQueue);
		} else if (AuthUtils.isNIUser())
		{
			log.debug("######################## isNIUser  " + AuthUtils.getCurrentUserLoginId()+ " filter.getTicketIds " + filter.getTicketIds());
			return getAllTickets(filter,ticketListForWorkingQueue);
		} else
		{
			return getAllTickets(filter,ticketListForWorkingQueue);
		}

		/*
		 * if(TicketStatus.DOCUMENTS_APPROVED == filter.getTicketStatus() ||
		 * TicketStatus.ACTIVATION_COMPLETED == filter.getTicketStatus()) {
		 * return ticketService.getTicketWithDetails(filter); }else { return
		 * ticketService.getPreSaleTicketWithDetails(filter); }
		 */
	}
	
	
	private Set<Long> getAllTicketsForUsers(List <Long> userIds)
	{
		Set<Long> allTicketIds = new HashSet<Long>();
		for(long user : userIds )
		{
			Set<Long> 	ticketIds = populateTicketIdsForUser(user);
			if(ticketIds != null )
			{
				allTicketIds.addAll(ticketIds);
			}
		}
		if(allTicketIds.size() == 0) 
			return null;
		
		return allTicketIds;
	}
	
	private Set<Long> populateTicketIdsForUser(long userId)
	{
		String key = dbUtil.getKeyForQueueByUserId(userId);
		Set<Long>  ticketListForWorkingQueue = null;
		if(AuthUtils.isFRUser())
			ticketListForWorkingQueue = globalActivities
			.getAllFrTicketFromMainQueueByKey(key);
		else
			ticketListForWorkingQueue = globalActivities
			.getAllTicketFromMainQueueByKey(key);
		return ticketListForWorkingQueue;
	}

	private APIResponse getFrTicketDetails(TicketFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(frTicketService
						.getFrTicketDetails(filter));
	}

	@POST
	@Path("logs/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketLogs(@PathParam("id") Long ticketId)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(loggerDao.getLog(ticketId));
	}

	private APIResponse getSalesTickets(TicketFilter filter,Set<Long> ticketListForWorkingQueue)
	{ 
		log.debug("filter value is ::"+filter);
		 APIResponse r = ticketService.getPreSaleTicketWithDetails(filter);
	 log.debug("API response is ::"+r);
		 
		 if(ApplicationUtils.isAppRequest())
			 r.setQueueData(ticketListForWorkingQueue);
		 
		 return r.setTransactionId(GenericUtil.createOrUpdateRequestKey(filter.getRequestKey()));
	}

	private APIResponse getAllTickets(TicketFilter filter,Set<Long>  ticketListForWorkingQueue)
	{
		APIResponse r =  ticketService.getTicketWithDetails(filter);
		if(ApplicationUtils.isAppRequest())
			r.setQueueData(ticketListForWorkingQueue);
		return r.setTransactionId(GenericUtil.createOrUpdateRequestKey(filter.getRequestKey()));
	}
	
	private APIResponse getCFETickets(TicketFilter filter,Set<Long> ticketListForWorkingQueue)
	{ 
	 APIResponse r = ticketService.getCFEicketWithDetails(filter);
	 
	 if(ApplicationUtils.isAppRequest())
		 r.setQueueData(ticketListForWorkingQueue);
		return r.setTransactionId(GenericUtil.createOrUpdateRequestKey(filter.getRequestKey()));
	}

	@POST
	@Path("sync/details")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketWithDetailsOnManualSync(TicketFilter filter)
	{
		if (AuthUtils.isSalesUser())
		{
			log.info(" Sales in  manual sync "
					+ AuthUtils.getCurrentUserLoginId());
			return getSalesTickets(filter, null);
		} else if (AuthUtils.isNIUser())
		{
			log.info(" NIUser in manual sync  "
					+ AuthUtils.getCurrentUserLoginId());
			return getAllTickets(filter, null);
		} else
		{
			return getAllTickets(filter, null);
		}
	}
	
	@POST
	@Path("for/vendors")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getReportingVendors(FRVendorFilter filter)
	{
		filter.setReportToUserId(AuthUtils.getCurrentUserId());
		return frTicketService.getFrVendorTicket(filter);
	}
}
