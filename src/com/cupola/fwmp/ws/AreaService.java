package com.cupola.fwmp.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.area.AreaCoreService;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.AreaVO;
import com.cupola.fwmp.vo.TypeAheadVo;

@Path("area")
public class AreaService
{

	AreaCoreService areaService;

	public void setAreaService(AreaCoreService areaService)
	{
		this.areaService = areaService;
	}

	@POST
	@Path("add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addAreaes(List<AreaVO> areaVOs)
	{

		return areaService.addAreaes(areaVOs);
	}
	
	@POST
	@Path("add/area")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addAreaUI(AreaVO areaVOs)
	{

		return areaService.addArea(areaVOs);
	}

	@POST
	@Path("list/{branchid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAreasByBranchId(
			@PathParam("branchid") String branchId)
	{

		return areaService.getAreasByBranchId(branchId);
	}

	@POST
	@Path("app/list/{branchid}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAreasByBranchIdForApp(
			@PathParam("branchid") String branchId)
	{

		if ("null".equalsIgnoreCase(branchId) || branchId.isEmpty()
				|| branchId.equalsIgnoreCase(null))
		{

			return ResponseUtil.createNullParameterResponse();

		} else
		{
			return ResponseUtil.createSuccessResponse()
					.setData(areaService.getAreasByBranchId(branchId));
		}
	}
	
	@POST
	@Path("/areaidlist")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Long> getAreaIds()
	{
		return areaService.getAreaIds();
	}
}
