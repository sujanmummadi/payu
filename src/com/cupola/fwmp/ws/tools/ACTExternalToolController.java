/**
* @Author aditya  28-Dec-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.ws.tools;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.tool.ACTExternalToolServices;
import com.cupola.fwmp.vo.tools.dashboard.PerformaceStasInput;

/**
 * @Author aditya 28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Path("act/external/services")
public class ACTExternalToolController {
	
	@Autowired
	ACTExternalToolServices actExternalToolController;
	
	@POST
	@Path("fr/performancestats")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	
	public APIResponse populateFrPerformaceStas(PerformaceStasInput performaceStasInput) {
		return actExternalToolController.getFrPerformaceStas(performaceStasInput);
	}
	
	@POST
	@Path("sd/performancestats")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	
	public APIResponse populateSdPerformaceStas(PerformaceStasInput performaceStasInput) {
		return actExternalToolController.getSdPerformaceStas(performaceStasInput);
	}
	
	@POST
	@Path("sales/performancestats")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	
	public APIResponse populateSalesPerformaceStas(PerformaceStasInput performaceStasInput) {
		return actExternalToolController.getSalesPerformaceStas(performaceStasInput);
	}
}
