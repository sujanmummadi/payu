/**
 * 
 */
package com.cupola.fwmp.ws.tools;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.tool.CRMCoreServices;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * @author aditya
 *
 */
@Path("/crm")
public class CRMWebServices
{
	@Autowired
	CRMCoreServices crmCoreServices;

	@POST
	@Path("complete/query")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCrmResponseByCompleteQuery(
			@QueryParam("query") String query,
			@QueryParam("cityName") String cityName)
	{

		if (cityName == null || cityName.isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("City name could not be null or empty");

		if (query == null || query.isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("Where clause query could not be null or empty");

		if (query.equalsIgnoreCase("query") || !query.startsWith("where"))
			return ResponseUtil.createNullParameterResponse()
					.setData("Invalid query passed, Please write proper query or give"
							+ " try to where clause service, insted of \"complete/query\" "
							+ "try \"where/clause/query\"");

		Map<Long, String> cityMap = LocationCache.getAllCity();

		boolean existanceFlag = false;

		for (Map.Entry<Long, String> entry : cityMap.entrySet())
		{
			if (entry.getValue().equalsIgnoreCase(cityName))
			{
				existanceFlag = true;
				break;
			}
		}

		if (!existanceFlag)
			return ResponseUtil.createNullParameterResponse()
					.setData("City Does not exist in FWMP");

		return crmCoreServices.getCrmResponseByCompleteQuery(query, cityName);
	}

	@POST
	@Path("where/clause/query")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCrmResponseByWhereClauseQuery(
			@QueryParam("query") String query,
			@QueryParam("cityName") String cityName)
	{

		if (cityName == null || cityName.isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("City name could not be null or empty");

		if (query == null || query.isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("Where clause query could not be null or empty");

		if (query.equalsIgnoreCase("query") || !query.startsWith("where"))
			return ResponseUtil.createNullParameterResponse()
					.setData("Invalid query passed, Please put proper where clause");

		Map<Long, String> cityMap = LocationCache.getAllCity();

		boolean existanceFlag = false;

		for (Map.Entry<Long, String> entry : cityMap.entrySet())
		{
			if (entry.getValue().equalsIgnoreCase(cityName))
			{
				existanceFlag = true;
				break;
			}
		}

		if (!existanceFlag)
			return ResponseUtil.createNullParameterResponse()
					.setData("City Does not exist in FWMP");

		return crmCoreServices
				.getCrmResponseByWhereClauseQuery(query, cityName);
	}
	
	@POST
	@Path("fr/where/clause/query")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCrmResponseByWhereClauseForFrQuery(
			@QueryParam("query") String query,
			@QueryParam("cityName") String cityName)
	{

		if (cityName == null || cityName.isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("City name could not be null or empty");

		if (query == null || query.isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("Where clause query could not be null or empty");

		if (query.equalsIgnoreCase("query") || !query.startsWith("where"))
			return ResponseUtil.createNullParameterResponse()
					.setData("Invalid query passed, Please put proper where clause");

		Map<Long, String> cityMap = LocationCache.getAllCity();

		boolean existanceFlag = false;

		for (Map.Entry<Long, String> entry : cityMap.entrySet())
		{
			if (entry.getValue().equalsIgnoreCase(cityName))
			{
				existanceFlag = true;
				break;
			}
		}

		if (!existanceFlag)
			return ResponseUtil.createNullParameterResponse()
					.setData("City Does not exist in FWMP");

		return crmCoreServices
				.getCrmResponseByWhereClauseQueryForFr(query, cityName);
	}

}
