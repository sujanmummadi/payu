package com.cupola.fwmp.ws.tools;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.TicketSource;
import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.persistance.entities.Router;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.configuration.ConfigurationCoreService;
import com.cupola.fwmp.service.customer.ProspectCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.location.area.AreaCoreService;
import com.cupola.fwmp.service.location.branch.BranchCoreService;
import com.cupola.fwmp.service.location.city.CityCoreService;
import com.cupola.fwmp.service.router.RouterCoreService;
import com.cupola.fwmp.service.tariff.TariffCoreService;
import com.cupola.fwmp.service.tool.ToolCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.tools.DeviceToolVo;
import com.cupola.fwmp.vo.tools.ToolAreaVO;
import com.cupola.fwmp.vo.tools.ToolBranchVO;
import com.cupola.fwmp.vo.tools.ToolCityVO;

@Path("integ/tool")
public class ToolService
{
	@Autowired
	CityCoreService cityService;

	@Autowired
	AreaCoreService areaService;

	@Autowired
	BranchCoreService branchService;
	
	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	ConfigurationCoreService configurationCoreService;

	@Autowired
	ProspectCoreService prospectCoreService;

	@Autowired
	TariffCoreService tariffCoreService;

	@Autowired
	RouterCoreService routerCoreService;
	
	@Autowired
	ToolCoreService toolCoreService;
	
	private static Logger LOGGER = Logger.getLogger(ToolService.class);

	@POST
	@Path("/cityidlist")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Long> getAllCityIds()
	{

		return cityService.getAllCityIds();
	}

	@POST
	@Path("/areaidlist")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Long> getAreaIds()
	{
		LocationCache.getAllAreaWithId();
		return areaService.getAreaIds();
	}

	@POST
	@Path("/all/areas")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<Long, String> getAllArea()
	{
		return LocationCache.getAllAreaWithId();
	}

	
	@POST
	@Path("/createbulk")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse createBulkProspect(List<ProspectCoreVO> prospects)
	{
		APIResponse apiResponse = null;

		LOGGER.info(" Adding Bulk Prospect with data " + prospects);
		for (Iterator<ProspectCoreVO> iterator = prospects.iterator(); iterator
				.hasNext();)
		{

			ProspectCoreVO prospectCoreVO = (ProspectCoreVO) iterator.next();

			if (prospectCoreVO != null && prospectCoreVO.getCustomer() != null)
			{

				prospectCoreVO.getCustomer()
						.setAddedBy(FWMPConstant.SYSTEM_ENGINE);

				prospectCoreVO.setCreationSource(TicketSource.TOOL);
			}
			
			String source = null;
			if (prospectCoreVO.getCreationSource() != null && prospectCoreVO.getCreationSource()
					.equalsIgnoreCase(CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL))
				source = prospectCoreVO.getCreationSource();

			apiResponse = prospectCoreService.addNewProspect(prospectCoreVO, source);
		}

		return ResponseUtil.createSaveSuccessResponse().setData(apiResponse);
	}

	@POST
	@Path("/citylist")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllCity()
	{

		return cityService.getAllCity();
	}

	@POST
	@Path("/arealist")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllAreas() {

		return areaService.getAllAreas();
	}
	
	
	@POST
	@Path("/branchlist")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllBranch()
	{

		return branchService.getAllBranches();
	}
	
	
	@POST
	@Path("/source")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getMediaSource()
	{
		return definitionCoreService.getMediaSource(null).getData();
	}

	@POST
	@Path("/inquiry")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object getInquiry()
	{
		return definitionCoreService.getInquiry().getData();
	}

	@POST
	@Path("tariff/bulk/upload")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse bulkTariffPlanUpload(List<TariffPlan> tariffPlans)
	{
		return tariffCoreService.bulkTariffPlanUpload(tariffPlans);
	}

	@POST
	@Path("router/bulk/upload")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse bulkRouterUpload(List<Router> routers)
	{
		return routerCoreService.bulkRouterUpload(routers);
	}

	@POST
	@Path("devices/names")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getAllDeviceNames()
	{

		return configurationCoreService.getAllDeviceNames();
	}

	@POST
	@Path("devices/bulk/upload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void addBulkDevice(Set<DeviceToolVo> deviceToolVos)
	{
		configurationCoreService.addBulkDevice(deviceToolVos);
	}


	@POST
	@Path("app/list/{branchid}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAreasByBranchIdForApp(
			@PathParam("branchid") String branchId)
	{

		if ("null".equalsIgnoreCase(branchId) || branchId.isEmpty()
				|| branchId.equalsIgnoreCase(null))
		{

			return ResponseUtil.createNullParameterResponse();

		} else
		{
			return ResponseUtil.createSuccessResponse()
					.setData(areaService.getAreasByBranchId(branchId));
		}
	}
	
	@POST
	@Path("app/list/")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getBranchesByCityId()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(branchService.getBranchesByCityId(AuthUtils
						.getCurrentUserCity().getId() + ""));
	}
	
	@POST
	@Path("/locations")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ToolCityVO> getAllLocations()
	{
//		String cityName = "HYDERABAD";
//		String branchName = "TARNAKA";
//		String areaName = "test";
//
//		List<ToolCityVO> allCityDetails = toolCoreService.getAllLocations();
//		
//		if (allCityDetails != null && !allCityDetails.isEmpty()) {
//			for (ToolCityVO toolCityVO : allCityDetails) {
//
//				if (toolCityVO != null && toolCityVO.getCityName().equalsIgnoreCase(cityName)) {
//
//					List<ToolBranchVO> branchList = toolCityVO.getBranchList();
//
//					if (branchList != null && !branchList.isEmpty()) {
//
//						for (ToolBranchVO toolBranchVO : branchList) {
//							if (toolBranchVO != null && toolBranchVO.getBranchName().equalsIgnoreCase(branchName)) {
//
//								List<ToolAreaVO> areaList = toolBranchVO.getAreaList();
//								
//								if(areaList != null & !areaList.isEmpty()) {
//									
//									for (ToolAreaVO toolAreaVO : areaList) {
//										
//										if( toolAreaVO != null && toolAreaVO.getAreaName().equalsIgnoreCase(areaName))
//										{
//											System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$ All condition matched $$$$$$$$$$$$$$");
//											
//										}
//										
//									}
//									
//									
//								} else {
//									System.out.println("Areas for branch " + toolBranchVO.getBranchName() +" are empty");
//								}
//									
//									
//								
//								
//							}  
//						}
//					} else {
//						System.out.println("Branch is empty for city " + toolCityVO.getCityName());
//					}
//
//				}  
//
//			}
//		} else {
//			System.out.println("City List is empty ");
//		}

		return toolCoreService.getAllLocations();
	}
	@GET
	@Path("bulk/fr/closure/{filePath: .*}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse closeWOinBulk4Fr(@PathParam("filePath") String filePath)
	{
		int result = toolCoreService.closeWOinBulk4Fr(filePath);

		if (result == 90000000)
			return ResponseUtil.createFailureResponse().setData(
					"Please check file path and make sure it contains column name called \"COMPLAINT_NO\" which is treated as work order for closure.");
		else if (result == 90000001)
			return ResponseUtil.createSuccessResponse()
					.setData("Total updated rows are " + 0 + " Please check all work orders are valid and present in FWMP database.");
		else if (result == 90000002)
			return ResponseUtil.createSuccessResponse()
					.setData("File not found at given location " + filePath + " Please check path is valid.");
		else
			return ResponseUtil.createSuccessResponse().setData("Total updated rows are " + result);
	}	
	@GET
	@Path("bulk/wo/closure/{filePath: .*}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse closeWOinBulk(@PathParam("filePath") String filePath)
	{
		int result = toolCoreService.closeWOinBulk(filePath);

		if (result == 90000000)
			return ResponseUtil.createFailureResponse().setData(
					"Please check file path and make sure it contains column name called \"COMPLAINT_NO\" which is treated as work order for closure.");
		else if (result == 90000001)
			return ResponseUtil.createSuccessResponse()
					.setData("Total updated rows are " + 0 + " Please check all work orders are valid and present in FWMP database.");
		else if (result == 90000002)
			return ResponseUtil.createSuccessResponse()
					.setData("File not found at given location " + filePath + " Please check path is valid.");
		else
			return ResponseUtil.createSuccessResponse().setData("Total updated rows are " + result);
	}	

}
