package com.cupola.fwmp.ws.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;

@Transactional
@Path("command")
@Produces(MediaType.APPLICATION_JSON)
public class CommandExecuterService
{

	@Autowired
	HibernateTemplate hibernateTemplate;

	private static Logger LOGGER = Logger
			.getLogger(CommandExecuterService.class);

	@POST
	@Path("/execute/query")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse executeQuery(String queryString)
	{
		LOGGER.info("Query " + queryString);

		try {
			Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery(queryString);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			
			if (queryString.contains("select") || queryString.contains("SELECT")) {
				List<Map<String, Object>> result = query.list();

				return ResponseUtil.createSuccessResponse().setData(result);
			}

			else {

				int result = query.executeUpdate();

				return ResponseUtil.createSuccessResponse().setData(result);
			}
		} catch (HibernateException e) {
			return ResponseUtil.createFailureResponse()
					.setData(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}

	}

	@GET
	@Path("/existing/queries/refresh")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse refreshPreQueries()
	{
		preQueries.clear();
		readProperties();
		return ResponseUtil.createSuccessResponse().setData(CommonUtil.xformToNodeValueFormat(preQueries));
	}

	@POST
	@Path("/existing/queries")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getPreQueries()
	{
		if (preQueries.size() == 0)
			readProperties();
		return ResponseUtil.createSuccessResponse().setData(CommonUtil.xformToNodeValueFormat(preQueries));
	}

	static Map<String, String> preQueries = new HashMap<String, String>();

	private void readProperties()
	{
		String filePath = "properties/debugging_prequeries.properties";
		Map<String, String> messsage = new LinkedHashMap<String, String>();

		ClassLoader classLoader = this.getClass().getClassLoader();
		Properties properties = new Properties();
		InputStream input = classLoader.getResourceAsStream(filePath);
		try
		{
			if (preQueries.size() == 0)
			{

				if (input.available() <= 0)
				{
					LOGGER.error("preQueries file does not exist ");
					return;
				}

			}

			properties.load(input);

			Set<Object> test = properties.keySet();

			for (Iterator iterator = test.iterator(); iterator.hasNext();)
			{
				String object = (String) iterator.next();
				messsage.put(object, properties.getProperty(object));
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				if (input != null)
					input.close();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		preQueries = messsage;
	}
}
