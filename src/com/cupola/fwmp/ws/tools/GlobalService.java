/**
 * 
 */
package com.cupola.fwmp.ws.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.LocationType;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.fr.FrDeploymentSettingDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.controllers.ClassificationController;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.ticketgc.TicketsForGCCoreService;
import com.cupola.fwmp.service.tool.GlobalCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.LocationTypeVo;
import com.cupola.fwmp.vo.MainQueueVo;

/**
 * @author aditya
 *
 */
@Path("integ/global/service")
public class GlobalService
{
	private static Logger log = LogManager
			.getLogger(GlobalService.class.getName());
	@Autowired
	GlobalCoreService globalCoreService;
	@Autowired
	ClassificationController classificationController;

	@Autowired
	TicketsForGCCoreService ticketsForGCCoreService;
	@Autowired
	DefinitionCoreService definitionCoreService;
	@Autowired
	AreaDAO areaDao;
	@Autowired
	CityDAO cityDao;
	@Autowired
	BranchDAO branchDao;
	@Autowired
	FrDeploymentSettingDao deploymentSettingDao;
	
	private String sharedPath;
	private String apkFileName;
	
	@Autowired
	DBUtil dbUtil;

	@Autowired
	GlobalActivities globalActivities;

	// @POST
	// @Path("/mergemainqueue")
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes({ "application/json" })
	// public APIResponse mergeMainQueue(
	// Map<String, LinkedHashSet<Long>> mainQueue)
	// {
	// if (mainQueue == null)
	// return ResponseUtil.createNullParameterResponse();
	//
	// if (mainQueue.isEmpty())
	// return ResponseUtil.createNullParameterResponse();
	//
	// return globalCoreService.mergeMainQueue(mainQueue);
	// }

	@POST
	@Path("/mergemainqueue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse mergeMainQueue(MainQueueVo mainQueueVo)
	{
		if (mainQueueVo == null)
			return ResponseUtil.createNullParameterResponse();

		if (mainQueueVo.getMainQueue() == null
				|| mainQueueVo.getMainQueue().isEmpty())
			return ResponseUtil.createNullParameterResponse();

		Map<String, LinkedHashSet<Long>> mainQueue = mainQueueVo.getMainQueue();

		return globalCoreService.mergeMainQueue(mainQueue);
	}

	@POST
	@Path("/refresh/sales/queue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse refreshSalesQueue()
	{
		classificationController.getAllOpenProspectTicket();
		return ResponseUtil.createSuccessResponse();
	}

	@POST
	@Path("/refresh/deployment/queue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse refreshDeploymentQueue()
	{
		classificationController.getAllOpenWorkOrderTicket();
		return ResponseUtil.createSuccessResponse();
	}

	@POST
	@Path("/reset/queue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse resetQueue()
	{
		return globalCoreService.resetQueue();
	}

	@POST
	@Path("/remove/sales/completed/ticket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse removeSalesTicket()
	{
		return ticketsForGCCoreService.removeSalesTicketFromQueue();
	}

	@POST
	@Path("/remove/deployment/completed/ticket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse removeDeploymentTicket()
	{
		return ticketsForGCCoreService.removeDeploymentTicketFromQueue();

	}

	@GET
	@Path("downloadapk")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response testDownloadApk()
	{
		apkFileName = definitionCoreService.getDeploymentPropertyValue(DefinitionCoreService.APK_NAME);
		
		java.nio.file.Path path = Paths
				.get(sharedPath + File.separator + apkFileName);
		Long userId = AuthUtils.getCurrentUserId();
		String loginId = AuthUtils.getCurrentUserLoginId();
		byte[] data;
		try
		{
			data = Files.readAllBytes(path);

			log.info("User " + loginId + "(" + userId + ") got latest apk "
					+ apkFileName + " on " + new Date());

			return Response.status(Response.Status.OK).entity(data)
					.header("Content-Disposition", "attachment; filename="
							+ apkFileName)
					.build();

		} catch (IOException e)
		{
			e.printStackTrace();

			log.info("Error while downloading latest APK fo User " + loginId
					+ "(" + userId + ") " + apkFileName + " on " + new Date());

			return Response.status(Response.Status.NO_CONTENT).build();
		}
	}

	@POST
	@Path("/reloadAll")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse reloadAll()
	{
		LocationCache.reFreshCache();
		WorkOrderDefinitionCache.reFreshCache();
		definitionCoreService.reloadDataFromProperties();
		deploymentSettingDao.resetCache();
		return ResponseUtil.createSuccessResponse();
	}

	@POST
	@Path("/locationcache/add2Cache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateAreaCache(LocationTypeVo locationType)
	{
		if (locationType != null)
		{
			LocationCache.updateLocationCache(locationType);
			if (locationType.getUpdateCacheCode() == LocationType.city)
				cityDao.updateCityCache(locationType.getCity());
			if (locationType.getUpdateCacheCode() == LocationType.branch)
				branchDao.updateBranchCache(locationType.getBranch());
			if (locationType.getUpdateCacheCode() == LocationType.area)
				areaDao.updateAreaCache(locationType.getArea());
			return ResponseUtil.createSuccessResponse();
		}

		return ResponseUtil.createFailureResponse();

	}

	public String getSharedPath()
	{
		return sharedPath;
	}

	public void setSharedPath(String sharedPath)
	{
		this.sharedPath = sharedPath;
	}

	public String getApkFileName()
	{
		return apkFileName;
	}

	public void setApkFileName(String apkFileName)
	{
		this.apkFileName = apkFileName;
	}

	@POST
	@Path("/remove/fr/completed/ticket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse removeFrTicketFromQueue()
	{
		return ticketsForGCCoreService.removeFrTicketFromQueue();

	}

	@POST
	@Path("/reset/frqueue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse resetFrQueue()
	{
		return globalCoreService.resetFrQueue();
	}

	@POST
	@Path("/merge/frqueue")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse mergeFrMainQueue(MainQueueVo mainFRQueueVo)
	{
		if (mainFRQueueVo == null)
			return ResponseUtil.createNullParameterResponse();

		if (mainFRQueueVo.getMainFrQueue() == null
				|| mainFRQueueVo.getMainFrQueue().isEmpty())
			return ResponseUtil.createNullParameterResponse();

		Map<String, LinkedHashSet<Long>> mainFRQueue = mainFRQueueVo
				.getMainFrQueue();

		return globalCoreService.mergeFrMainQueue(mainFRQueue);

	}

	@POST
	@Path("get/fr/queue/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse getFrQueueForUser(@PathParam("userId") Long userId)
	{

		String neKey = dbUtil.getKeyForQueueByUserId(userId);
		return ResponseUtil.createSuccessResponse()
				.setData(globalActivities.getFrMainQueue().get(neKey));
	}

	@POST
	@Path("get/sd/queue/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes({ "application/json" })
	public APIResponse getSdQueueForUser(@PathParam("userId") Long userId)
	{

		String neKey = dbUtil.getKeyForQueueByUserId(userId);
		return ResponseUtil.createSuccessResponse()
				.setData(globalActivities.getMainQueue().get(neKey));
	}

	@POST
	@Path("add2frqueue/{userId}/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse add2FrQueueForUser(@PathParam("userId") Long userId,
			@PathParam("ticketId")  Long ticketId)
	{
		LinkedHashSet<Long> setValue = new 	LinkedHashSet<Long>();
		globalCoreService.addFrTicket2MainQueue(setValue, userId);
		return ResponseUtil.createSuccessResponse();

	}

	@POST
	@Path("add2sdqueue/{userId}/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse add2SdQueueForUser(@PathParam("userId") Long userId,
			@PathParam("ticketId")  Long ticketId)
	{
		LinkedHashSet<Long> setValue = new 	LinkedHashSet<Long>();
		globalCoreService.addTicket2MainQueue(setValue, userId);
		return ResponseUtil.createSuccessResponse();
	}
	
}
