/**
 * 
 */
package com.cupola.fwmp.ws.tools;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.tool.AppCrashServcie;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.integ.app.FinahubLog;
import com.cupola.fwmp.vo.tools.AppCrashVo;

/**
 * @author aditya
 *
 */
@Path("integ/app/crash")
public class AndroidCrashServices {
	private static Logger log = LogManager.getLogger(AndroidCrashServices.class);

	@Autowired
	AppCrashServcie appCrashServcie;
	@Autowired
	MongoTemplate mongoTemplate;

	@Path("/report")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse sendCrashReport(AppCrashVo appCrashVo) {

		log.debug("androidCrashVo " + appCrashVo);

		appCrashServcie.recordAppCrashServcie(appCrashVo);

		return ResponseUtil.createSuccessResponse();
	}

	@Path("/finahublogger")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse generateFinahubLog(FinahubLog finahubLog) {

		log.info("Putting finahub log with details  " + finahubLog);

		if (finahubLog == null)
			return ResponseUtil.nullArgument();

		if (finahubLog.getActivity() == null || finahubLog.getActivity().isEmpty())
			return ResponseUtil.nullArgument().setData("Finahub package calling activity is null");

//		if (finahubLog.getTicketId() == null || finahubLog.getTicketId().isEmpty())
//			return ResponseUtil.nullArgument().setData("Finahub package calling ticket is null");

		if (finahubLog.getFinahubPackage() == null || finahubLog.getFinahubPackage().isEmpty())
			return ResponseUtil.nullArgument().setData("Finahub package is null or empty");

//		if (finahubLog.getRequestTime() == null)
//			return ResponseUtil.nullArgument().setData("Finahub package call time is null");

		finahubLog = setAllValues(finahubLog);

		mongoTemplate.save(finahubLog);

		log.info("Put finahub log with details  " + finahubLog);

		return ResponseUtil.createSuccessResponse();
	}

	/**
	 * @author aditya FinahubLog
	 * @param finahubLog
	 * @return
	 */
	private FinahubLog setAllValues(FinahubLog finahubLog) {

		FinahubLog finahubLogger = new FinahubLog();

		finahubLogger.setLoginId(AuthUtils.getCurrentUserLoginId());
		finahubLogger.setDisplayName(AuthUtils.getCurrentUserDisplayName());
		finahubLogger.setFinahubLogId(StrictMicroSecondTimeBasedGuid.newGuid());
		finahubLogger.setArea(AuthUtils.getCurrentUserArea().getName());
		finahubLogger.setBranch(AuthUtils.getCurrentUserBranch().getName());
		finahubLogger.setCity(AuthUtils.getCurrentUserCity().getName());
		finahubLogger.setActivity(finahubLog.getActivity());
		finahubLogger.setFinahubPackage(finahubLog.getFinahubPackage());
		finahubLogger.setRequestString(finahubLog.getRequestString());
		finahubLogger.setRequestTime(finahubLog.getRequestTime());
		finahubLogger.setResponseString(finahubLog.getResponseString());
		finahubLogger.setResponseTime(finahubLog.getResponseTime());
		finahubLogger.setTicketId(finahubLog.getTicketId());
		
		return finahubLogger;
	}

}
