package com.cupola.fwmp.ws;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.user.ResetUserVo;
import com.cupola.fwmp.service.user.UserService;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.LoginPojo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;

@Path("user")
public class UserServices
{

	final static Logger log = Logger.getLogger(UserServices.class);
	@Context
	HttpServletRequest request;
	UserService userService;

	public void setUserService(UserService userService)
	{
		this.userService = userService;
	}

	@POST
	@Path("loginforapp")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse login(LoginPojo loginPojo)
	{

		log.info("credentials::" + loginPojo);

		APIResponse response = userService.loginForApp(loginPojo);
		log.info("response of login=======" + response);
		return response;

	}

	@GET
	@Path("app")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse loginapp(
	/*
	 * @FormParam("username") String userName,
	 * 
	 * @FormParam("password") String password
	 * 
	 * @FormParam("deviceId") String deviceId
	 */)
	{

		log.debug("$$$$$$$$$ " + request.getSession().getAttribute("username"));

		LoginPojo loginPojo = new LoginPojo();
		loginPojo.setDeviceId((String) request.getSession()
				.getAttribute("deviceId"));
		loginPojo.setUsername((String) request.getSession()
				.getAttribute("username"));
		loginPojo.setPassword((String) request.getSession()
				.getAttribute("password"));
		/*
		 * CommonUtil.printHeaders(request);
		 * 
		 * StringBuffer jb = new StringBuffer(); String line = null; try {
		 * BufferedReader reader = request.getReader(); while ((line =
		 * reader.readLine()) != null) jb.append(line); } catch (Exception e) {
		 * report an error }
		 * 
		 * try { Enumeration<String> parameterNames =
		 * request.getParameterNames();
		 * 
		 * while (parameterNames.hasMoreElements()) {
		 * 
		 * String paramName = parameterNames.nextElement();
		 * 
		 * String[] paramValues = request.getParameterValues(paramName); for
		 * (int i = 0; i < paramValues.length; i++) { String paramValue =
		 * paramValues[i]; log.debug(paramName+" t " + paramValue); }
		 * 
		 * } log.debug("Request Body "+jb.toString()); } catch ( Exception e) {
		 * // crash and burn }
		 */
		// log.info("credentials::" + loginPojo);

		APIResponse response = userService.loginForApp(loginPojo);
		log.info("response of login=======" + response);
		return response;

	}

	@POST
	@Path("checkcurrentuser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse checkCurrentUser(LoginPojo loginPojo)
	{

		log.info("credentials::" + loginPojo);

		APIResponse response = userService.checkCurrentUser(loginPojo);
		log.info("response of login=======" + response);
		return response;

	}

	@GET
	@Path("current")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse currentUser()
	{

		UserDetails userDetails = (UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		return ResponseUtil.createSuccessResponse().setData(userDetails);

	}

	@POST
	@Path("associated/{includeReportTo}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAssociatedUsers(
			@DefaultValue("true") @PathParam("includeReportTo") Boolean includeReportTo)
	{

		return ResponseUtil.createSuccessResponse()
				.setData(userService.getAssociatedUsers(null, includeReportTo));

	}

	@POST
	@Path("associated/area/{areaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAssociatedUsers(@PathParam("areaId") Long areaId)
	{

		return ResponseUtil.createSuccessResponse()
				.setData(userService.getAssociatedUsers(areaId, true));

	}

	@POST
	@Path("associated/forgroup/{groupCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAssociatedUsersByGroup(
			@PathParam("areaId") Long areaId,
			@PathParam("groupCode") Long groupCode)
	{

		return ResponseUtil.createSuccessResponse().setData(userService
				.getAssociatedUsersByGroup(areaId, null, groupCode));

	}

	@POST
	@Path("associated/area/{areaId}/{groupCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAssociatedUsersForGroup(
			@PathParam("areaId") Long areaId,
			@PathParam("groupCode") Long groupCode)
	{

		return ResponseUtil.createSuccessResponse().setData(userService
				.getAssociatedUsersByGroup(areaId, null, groupCode));

	}

	@POST
	@Path("associated/{branchId}/{areaId}/{groupCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAssociatedUsers(@PathParam("branchId") Long branchId,
			@PathParam("areaId") Long areaId,
			@PathParam("groupCode") Long groupCode)
	{

		return ResponseUtil.createSuccessResponse().setData(userService
				.getAssociatedUsersByGroup(areaId, branchId, groupCode));

	}

	@POST
	@Path("getusername")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUserName()
	{

		return userService.getUserName();
	}

	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addUser(UserVo user)
	{

		log.info("User#######" + user);

		return userService.addUser(user);
	}

	@POST
	@Path("update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateUser(UserVo user)
	{

		return userService.updateUser(user);
	}

	@POST
	@Path("createnewpassword/{userId}/{newPassword}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse createNewPassword(
			@PathParam("newPassword") String newPassword,
			@PathParam("userId") Long userId)
	{

		return userService.createNewPassword(userId, newPassword);
	}

	@POST
	@Path("deleteuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse deleUser(UserVo user)
	{

		log.info("User#######" + user);

		return userService.deleteUser(user);
	}

	/*
	 * @POST
	 * 
	 * @Path("pageIndex/{pageIndex}/{itemPrePage}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public APIResponse
	 * getUserListByPageIndex(
	 * 
	 * @PathParam("pageIndex") int pageIndex, @PathParam("itemPrePage") int
	 * itemPrePage) { log.info("get user details By page Index:"); return
	 * userService.getUserListByPageIndex(pageIndex,itemPrePage); }
	 */

	@POST
	@Path("resetpassword/{oldpassword}/{newpassword}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse resetPassword(
			@PathParam("oldpassword") String oldpassword,
			@PathParam("newpassword") String newpassword)
	{

		log.info("resetPassword@@@@@@@@@s " + oldpassword + " " + newpassword);
		return userService.resetPassword(oldpassword, newpassword);
	}

	@POST
	@Path("getallusernames")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllUserNames()
	{

		log.debug("get all user names");

		return userService.getAllUserNames(null);
	}

	@POST
	@Path("getusers/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getUserNames(@PathParam("type") Long userType)
	{

		log.debug("get all user names");

		return userService.getAllUserNames(userType);
	}

	/*
	 * @POST
	 * 
	 * @Path("getallskills")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public List<TypeAheadVo>
	 * getAllSkillNames() {
	 * 
	 * log.debug("get all skills");
	 * 
	 * return userService.getAllSkillNames(); }
	 */

	@POST
	@Path("getallelementmacs")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getAllElementMacs()
	{
		log.info("get all Element Macs.......");
		return userService.getAllElementMacs();
	}

	@POST
	@Path("getAllUsers")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllUsers()
	{

		log.debug("get all users");

		return userService.getAllUsers();
	}

	@POST
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUsers(UserFilter filter)
	{

		return userService.getUsers(filter);
	}


	@POST
	@Path("usersuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> userSearchSuggestion(
			@PathParam("query") String userQuery)
	{

		return userService.userSearchSuggestion(userQuery);
	}

	@POST
	@Path("tabsuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> tabSearchSuggestion(
			@PathParam("query") String userQuery)
	{

		return userService.tabSearchSuggestion(userQuery);
	}

	@POST
	@Path("roles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getUserRoles()
	{

		return userService.getUserRoles();
	}

	@POST
	@Path("elementsuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> elementSearchSuggestion(
			@PathParam("query") String elementQuery)
	{

		return userService.elementSearchSuggestion(elementQuery);
	}

	@POST
	@Path("citysuggestion/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> citySuggestion(
			@PathParam("query") String cityQuery)
	{

		return userService.citySearchSuggestion(cityQuery);
	}

	@POST
	@Path("branches/{cityname}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getBranchesByCityName(
			@PathParam("cityname") String cityName)
	{

		return userService.getBranchesByCityName(cityName);
	}

	@POST
	@Path("areas/{branchname}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAreasByBranchName(
			@PathParam("branchname") String branchName)
	{

		return userService.getAreasByBranchName(branchName);
	}

	@POST
	@Path("subareas/{areaname}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getSubAreasByAreaName(
			@PathParam("areaname") String areaName)
	{

		return userService.getSubAreasByAreaName(areaName);
	}

	@POST
	@Path("devices/{branchid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getDevicesByBranchId(
			@PathParam("branchid") String branchId)
	{

		return userService.getDevicesByBranchId(branchId);
	}

	@POST
	@Path("vendors")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getVendorsByBranchName(TypeAheadVo typeAheadVo)
	{

		return userService.getVendorsByBranchName(typeAheadVo);
	}

	@POST
	@Path("getUserByDeviceMac/{fxName}/{role}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUserByDeviceMac(@PathParam("fxName") String fxName,
			@PathParam("role") String role)
	{
		return userService.getUserByDeviceMac(fxName, role);
	}

	/*
	 * @POST
	 * 
	 * @Path("vendortypes/{vendorname}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public List<String>
	 * getVendorTypesByVendorName(@PathParam("vendorname") String vendorName){
	 * 
	 * return userService.getVendorTypesByVendorName(vendorName); }
	 */

	@POST
	@Path("skills")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getSkillsByVendorName(TypeAheadVo typeAheadVo)
	{
		return userService.getSkillsByVendorName(typeAheadVo);
	}

	@POST
	@Path("reassign")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse reAssignedDeviceToUser(@QueryParam("userId") long userId,
			@QueryParam("fxName") String fxName)
	{
		return userService.reAssignedDeviceToUser(userId, fxName);
	}

	@POST
	@Path("resetusers")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse resetCachedUsers()
	{
		return userService.resetCachedUsers();
	}

	@POST
	@Path("list/areas/default/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getDefaultAreasForUser(@PathParam("userId") Long userId)
	{

		return userService.getDefaultAreasForUser(userId);
	}

	@POST
	@Path("list/areas/all/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllAreasForUser(@PathParam("userId") Long userId)
	{

		return userService.getAllAreasForUser(userId);
	}

	@POST
	@Path("map/areas/default/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse mapDefaultAreaForUser(List<TypeAheadVo> areaIds,
			@PathParam("userId") Long userId)
	{

		return userService.mapDefaultAreaForUser(areaIds, userId);
	}

	@POST
	@Path("map/areas/all/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse mapDevicesForUser(List<TypeAheadVo> areaIds,
			@PathParam("userId") Long userId)
	{

		return userService.mapAllAreaForUser(areaIds, userId);
	}

	@POST
	@Path("reset/mapping/byid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse mapDevicesForUser(ResetUserVo resetUserVo)
	{
		return userService.restUserById(resetUserVo);
	}

	@POST
	@Path("reset/password/byid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse resetPassword(ResetUserVo resetUserVo)
	{
		return userService.restPassword(resetUserVo);
	}

	@POST
	@Path("add2Cache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addToCache(UserVo userVo)
	{
		return userService.addToCache(userVo);
	}
	

}
