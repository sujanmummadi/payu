/**
 * 
 */
package com.cupola.fwmp.ws.sales;

import java.util.Iterator;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.AddressType;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.sales.PaymentUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.ws.customer.ProspectService;

/**
 * @author aditya
 */

@Path("/sales")
public class SalesServices
{
	@Autowired
	SalesCoreServcie salesCoreServcie;

	@Autowired
	DBUtil dbUtil;
	
//	@Autowired
//	GlobalCoreService globalCoreService;
	
	private static Logger LOGGER = Logger.getLogger(SalesServices.class);

	@Path("count")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCount()
	{
		// return salesCoreServcie.getCount();
		return salesCoreServcie.getSalesCounts();
	}
	
	@Path("count/details")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCountDetails(TicketFilter filter)
	{
		ApplicationUtils.saveTicketFilter(filter);
		// return salesCoreServcie.getCount();
		return salesCoreServcie.getSalesCounts();
	}

	
	@Path("update/basicinfo")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateBasicInfo(UpdateProspectVO updateProspectVO)
	{
		if (!dbUtil.isTicketAvailable(Long.valueOf(updateProspectVO.getTicketId()), AuthUtils.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();
		
		LOGGER.info(" $$$$$$$$$$$$%%%%%%%%%%%%%% updating basic of info Prospect with data "+updateProspectVO.getProspectNo());
		
		 List<CustomerAddressVO> customeraddressvos=updateProspectVO.getCustomerAddressVOs();
		 
		 LOGGER.info(" $$$$$$$$$$$$%%%%%%%%%%%%%% customeraddressvos  " + customeraddressvos);
			 
		 for (Iterator<CustomerAddressVO> iterator = customeraddressvos.iterator(); iterator.hasNext();) {
			 
			try {
				
				CustomerAddressVO customerAddressVO = (CustomerAddressVO) iterator.next();

				ObjectMapper mapper = new ObjectMapper();
				String addressJsonString = mapper.writeValueAsString(customerAddressVO);

				if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.CURRENT_ADDRESS))
					updateProspectVO.setCurrentAddress(addressJsonString);
				
				LOGGER.info("addressJsonString"+addressJsonString);
			     
				if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.PERMANENT_ADDRESS))
					updateProspectVO.setPermanentAddress(addressJsonString);

				if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.COMMUNICATION_ADDRESS))
					updateProspectVO.setCommunicationAdderss(addressJsonString);

			}   catch (Exception e) {
				
				LOGGER.error("Error while converting address into json format " + e.getMessage());
				e.printStackTrace();
				continue;
			}
		}

		APIResponse apiResponse = salesCoreServcie.updateBasicInfo(updateProspectVO,true,null,null);
		

		return apiResponse;
	}

	@Path("update/tariff")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateTariff(TariffPlanUpdateVO planUpdateVO)
	{
		if (!dbUtil.isTicketAvailable(Long
				.valueOf(planUpdateVO.getTicketId()), AuthUtils
						.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();

		
		return salesCoreServcie.updateTariff(planUpdateVO);
	}

	@Path("update/payment")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updatePayment(PaymentUpdateVO paymentUpdateVO)
	{
		if (!dbUtil.isTicketAvailable(Long
				.valueOf(paymentUpdateVO.getTicketId()), AuthUtils
						.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();

		
		return salesCoreServcie.updatePayment(paymentUpdateVO);
	}

	@Path("get/router")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getRouter()
	{
		Long cityId = AuthUtils.getCurrentUserCity().getId();
		System.out.println("cityId " + cityId);

		return salesCoreServcie.getRouter(cityId);
	}

	@Path("update/propspecttype")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updatePropspectType(UpdateProspectVO updateProspectVO)
	{
		if (AuthUtils.isSalesExecutiveUser() && !dbUtil.isTicketAvailable(Long
				.valueOf(updateProspectVO.getTicketId()), AuthUtils
						.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();

		return salesCoreServcie.updatePropspectType(updateProspectVO);
	}

	@POST
	@Path("se/summary")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketSummaryByWorkOrderType()
	{
		return salesCoreServcie.seSummary();
	}

	@POST
	@Path("se/summary/details")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketSummaryByWorkOrderTypeDetails(TicketFilter filter)
	{
		ApplicationUtils.saveTicketFilter(filter);
		return salesCoreServcie.seSummary();
	}
	
	@Path("update/ticket/jump/status")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updatePermission(UpdateProspectVO updateProspectVO)
	{
		if (!dbUtil.isTicketAvailable(Long
				.valueOf(updateProspectVO.getTicketId()), AuthUtils
						.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();

		
		return salesCoreServcie.updatePermission(updateProspectVO);
	}
}