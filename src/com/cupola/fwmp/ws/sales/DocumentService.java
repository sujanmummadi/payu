/**
 * 
 */
package com.cupola.fwmp.ws.sales;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.AadhaarTransactionType;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.DBActiveStatus;
import com.cupola.fwmp.dao.adhaar.AdhaarDAO;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.mongo.documents.CustomerDocument;
import com.cupola.fwmp.dao.mongo.documents.DocumentDao;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.persistance.entities.Adhaar;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.doc.service.DocumentationCoreEngine;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.tool.GlobalCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CustomerAadhaarMappingVo;
import com.cupola.fwmp.vo.sales.ImageInput;

/**
 * @author aditya
 * 
 */
@Path("docengine")
public class DocumentService
{
	private Logger log = LogManager.getLogger(DocumentService.class.getName());
	private DocumentationCoreEngine docEngine;

	@Autowired
	SalesCoreServcie salesCoreServcie;
	
	@Autowired
	DocumentDao documentDao;
	@Autowired
	DBUtil dbUtil;
	@Autowired
	GlobalCoreService globalCoreService;
	@Autowired
	CustomerDAO customerDAO;
	@Autowired
	TicketDAO ticketDAO;
	@Autowired
	AdhaarDAO adhaarDao;
	
	public void setDocEngine(DocumentationCoreEngine docEngine)
	{
		this.docEngine = docEngine;
	}

	@Path("uploaddoc")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse createFolderForProspect(ImageInput imageInput)
	{
		
		if(imageInput == null)
			return ResponseUtil.createNullParameterResponse();
	
		log.debug("Doc update request" + imageInput);
		
		if(imageInput.getTicketId() == null )
			return ResponseUtil.createNullParameterResponse();
		
		if(imageInput.getTicketId().isEmpty())
			return ResponseUtil.createNullParameterResponse();
		
		if (!dbUtil.isTicketAvailable(Long
				.valueOf(imageInput.getTicketId()), AuthUtils
						.getCurrentUserId()))
			return ResponseUtil.createTicketReassigned();
	
		try {

			if (imageInput.getKycData() != null && imageInput.getKycData().length() > 0) {
				
				if (imageInput.getAadhaarTransactionType() == null || (imageInput.getAadhaarTransactionType().isEmpty()))
					return ResponseUtil.createAadhaarTransactionTypeIsNull();
				
				if (imageInput.getTransactionId() == null || (imageInput.getTransactionId().isEmpty()))
					return ResponseUtil.createAadhaarTransactionIdIsNull();
				
				
				log.info("aadhar details for ticket " + imageInput.getTicketId() + " are "
						+ imageInput.getTransactionId() + " , " + imageInput.getAadhaarTransactionType());

				int result = 0;

				if (imageInput.getActivityId() == Activity.POA_DOCUMENT_UPDATE) {
					result = customerDAO.updatePOIOrPOAWhileActivity(Long.valueOf(imageInput.getTicketId()), 0,
							DBActiveStatus.ACTIVE);

				} else if (imageInput.getActivityId() == Activity.POI_DOCUMENT_UPDATE) {
					result = customerDAO.updatePOIOrPOAWhileActivity(Long.valueOf(imageInput.getTicketId()),
							DBActiveStatus.ACTIVE, 0);

				}

				if (result > 0 || imageInput.getActivityId() == Activity.PAYMENT_UPDATE) {
					log.info("Populating customer aadhaar mapping " + imageInput.getTicketId());

					CustomerAadhaarMapping aadhaarMapping = getAadhaarMappingFromImageInput(imageInput);

					log.debug("got customer aadhaar mapping and details are " + aadhaarMapping);

					if (aadhaarMapping != null) {
						
						aadhaarMapping.setTicketId(Long.valueOf(imageInput.getTicketId()));
						aadhaarMapping.setAdhaarTrnId(imageInput.getTransactionId());
						aadhaarMapping.setAddedOn(new Date());
						aadhaarMapping.setActivityId(imageInput.getActivityId());
						
						log.info("got customer aadhaar mapping " + imageInput.getTicketId());

						CustomerAadhaarMappingVo aadhaarMappingVo = customerDAO
								.populateCustomerAadhaarMapping(aadhaarMapping);

						Adhaar adhaar = adhaarDao.saveAdhar(docEngine.parseKycDetail(imageInput.getKycData()));

						// update customer
						
						int aadharUpdateResult = customerDAO.updateAadhaarDetails(aadhaarMapping.getCustomerId(),adhaar.getId(),aadhaarMapping.getTicketId());
						
						log.debug("CustomerAadhaarMappingVo " + aadhaarMappingVo + " for " + imageInput.getTicketId()
								+ " and aadhar details are " + adhaar.getId() + " aadharUpdateResult " + aadharUpdateResult);

					} else {
						return ResponseUtil.createAadhaarTransactionIdIsNull();
					}

				} else {
					log.info("Customer Aadhaar Mapping is not done in createFolderForProspect for ticketId :"
							+ imageInput.getTicketId());
				}

				return docEngine.uploadKycImageData(imageInput);
			} else {
				return docEngine.uploadDoucuments(imageInput);
			}

		} catch (Exception e) {

			log.info("Customer document upload failed for ticket :" + imageInput.getTicketId());
			e.printStackTrace();

			return ResponseUtil.createFailureResponse();
		}

	}

	@POST
	@Path("{prospectId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllCustomerDocument(
			@PathParam("prospectId") String prospectId)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(docEngine.getAllDocumentByProspectNumber(prospectId));
	}

	@POST
	@Path("forticket/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllCustomerDocumentByTicket(
			@PathParam("ticketId") Long ticketId)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(documentDao.getCustomeDocument(ticketId));
	}

	@POST
	@Path("update")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateCustomerDocumentByTicket(
			List<CustomerDocument> documents)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(documentDao.updateCustomerDocuments(documents));
	}

	@GET
	@Path("{prospectId}/{fileanme}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response openCustomerDocument(
			@PathParam("prospectId") String prospectId,
			@PathParam("fileanme") String filename)
	{

		File file = docEngine.getDocumentByFileName(prospectId, filename);
		try
		{
			if (file != null)
			{
				String extension = FilenameUtils.getExtension(file.getName());
				return getResponse(file, filename, extension);
			}
		} catch (Exception e)
		{

		}
		return null;
	}

	private Response getResponse(File file, String filename, String extension)
	{
		FileInputStream fileInputStream = null;
		try
		{
			Response.ResponseBuilder responseBuilder = Response
					.ok((Object) file);
			responseBuilder.header("Content-Disposition", "inline; filename="
					+ filename + ";");
			if (extension.equals("pdf"))
				responseBuilder.type("application/pdf");
			else if (extension.equals("png"))
				responseBuilder.type("image/png");
			else if (extension.equals("jpg"))
				responseBuilder.type("image/jpg");
			else if (extension.equals("jpeg"))
				responseBuilder.type("image/jpeg");
			else if (extension.equals("gif"))
				responseBuilder.type("image/gif");
			else if (extension.equals("txt"))
				responseBuilder.type("text/plain");
			//fileInputStream.close();
			return responseBuilder.build();
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			if (fileInputStream != null)
			{/*
				
				try {
					fileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}*/
				 
			}
		}
		return null;

	}
	
	/**@author noor
	 * CustomerAadhaarMapping
	 * @param imageInput
	 * @return
	 */
	private CustomerAadhaarMapping getAadhaarMappingFromImageInput(ImageInput imageInput) {

		CustomerAadhaarMapping aadhaarMapping = new CustomerAadhaarMapping();

		// log.debug("Image Input : "+ imageInput);
		//
		try {
			Ticket ticket = ticketDAO.getTicketById(Long.valueOf(imageInput.getTicketId()));

			aadhaarMapping.setCustomerId(ticket.getCustomer().getId());

			aadhaarMapping.setTicketId(ticket.getId());
			
			aadhaarMapping.setAddedBy(AuthUtils.getCurrentUserId());
			
			aadhaarMapping.setAdhaarTrnId(imageInput.getTransactionId());
			
			aadhaarMapping.setAddedOn(new Date());
			
			aadhaarMapping.setActivityId(imageInput.getActivityId());
			
			if (imageInput.getAadhaarTransactionType().equalsIgnoreCase(AadhaarTransactionType._PRIMARY))
				aadhaarMapping.setLabel(Long.valueOf(AadhaarTransactionType.PRIMARY));
			
			else if (imageInput.getAadhaarTransactionType().equalsIgnoreCase(AadhaarTransactionType._SECONDARY))
				aadhaarMapping.setLabel(Long.valueOf(AadhaarTransactionType.SECONDARY));
			
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Error while converting AadhaarMappingFromImageInput for ticket " + imageInput.getTicketId()
					+ " and message is " + e.getMessage());
		}
		return aadhaarMapping;

	}
	
}
