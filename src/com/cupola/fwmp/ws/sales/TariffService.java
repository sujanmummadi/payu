package com.cupola.fwmp.ws.sales;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cupola.fwmp.filters.TariffFilter;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.tariff.TariffCoreServiceImpl;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;

@Path("tariff")
public class TariffService
{

	final static Logger log = Logger.getLogger(TariffService.class);

	TariffCoreServiceImpl tariffCoreService;

	public void setTariffCoreService(TariffCoreServiceImpl tariffCoreService)
	{
		this.tariffCoreService = tariffCoreService;
	}

	@POST
	@Path("/id")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTariffByCity(TariffFilter filter)
	{

		log.info("get tariffPlan By city:");
		if (filter.getCityId() <= 0)
			filter.setCityId(AuthUtils.getCurrentUserCity().getId());

		return tariffCoreService.getTariffByCity(filter);
	}

	@POST
	@Path("/updatecustomertariff")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse UpdateCustomerTariff(TariffPlanUpdateVO tariffVo)
	{

		log.info("updateCustomer TariffPlan:");
		return tariffCoreService.UpdateCustomerTariff(tariffVo);

	}

	@POST
	@Path("/list/web")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTariff(TariffFilter filter)
	{
		log.info("Get Tariff plans");
		/*
		 * if(filter.getCityId() <= 0)
		 * filter.setCityId(AuthUtils.getCurrentUserCity().getId());
		 */
		return tariffCoreService.getTariffByCity(filter);

	}
	
	@POST
	@Path("getplancategoerybycity")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getPlanCategoeryByCity(TariffFilter filter)
	{
		log.info("Get Tariff plans");
		/*
		 * if(filter.getCityId() <= 0)
		 * filter.setCityId(AuthUtils.getCurrentUserCity().getId());
		 */
		return tariffCoreService.getPlanCategoeryByCity(filter);

	}

	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTariff()
	{
		log.info("Get Tariff plans");
		return tariffCoreService.getTariffByCity();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addTariff(TariffPlan tariffPlan)
	{
		log.info("Add Tariff plans");
		return tariffCoreService.addTariffPlan(tariffPlan);
	}

	@POST
	@Path("/bulk/upload")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse bulkTariffPlanUpload(List<TariffPlan> tariffPlans)
	{
		log.info("Add bulkTariffPlanUpload ");
		return tariffCoreService.bulkTariffPlanUpload(tariffPlans);
	}
	@POST
	@Path("/tariffUpdate")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateTariff(TariffPlan tariffPlan)
	{
		return tariffCoreService.updateTariff(tariffPlan);
	}
	
	@POST
	@Path("/tariffDelete")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deleteTariff(TariffPlan tariffPlan)
	{
		return tariffCoreService.deleteTariff(tariffPlan);
	}

}
