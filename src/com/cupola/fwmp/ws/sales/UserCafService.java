package com.cupola.fwmp.ws.sales;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.userCaf.UserCafCoreService;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CafDetailsVo;
import com.cupola.fwmp.vo.ReassignCAF;
import com.cupola.fwmp.vo.UserCafVo;

@Path("usercaf")
public class UserCafService
{

	final static Logger log = Logger.getLogger(UserCafService.class);

	@Autowired
	UserCafCoreService userCafCoreService;

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse saveAndUpdateUserCAF(UserCafVo userCafVo)
	{

		return userCafCoreService.saveAndUpdateUserCAF(userCafVo);
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deleteUserCAF(UserCafVo userCafVo)
	{

		return userCafCoreService.deleteUserCAF(userCafVo);
	}

	@POST
	@Path("/getcafbyid")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUserCAFByUserId(UserCafVo userCafVo)
	{
		return userCafCoreService.getUserCAFByUserId(userCafVo);
	}

	@POST
	@Path("/alldetails")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getListUserCAF()
	{

		return userCafCoreService.getListUserCAF();
	}

	@POST
	@Path("/insert")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addCafDetails(List<CafDetailsVo> userCafVo)
	{
		
		if(userCafVo == null)
			return ResponseUtil.createNullParameterResponse();

		return userCafCoreService.addCafDetails(userCafVo);

	}

	@POST
	@Path("/getcaflist")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse gerCafDetailsByUserId()
	{
		return userCafCoreService.gerCafDetailsByUserId();
	}

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateUserCafByUserId(List<CafDetailsVo> vo)

	{
		return userCafCoreService.updateUserCafByUserId(vo);
	}
	
	@POST
	@Path("/reassign")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse cafReassignment(ReassignCAF reassignCAF)
	{
		if (reassignCAF.getCafNumber() == null
				|| reassignCAF.getDestinationUserId() == null
				|| reassignCAF.getSourceUserId() == null)
			
			return ResponseUtil.createNullParameterResponse();

		return userCafCoreService.cafReassignment(reassignCAF);
	}
	
	@POST
	@Path("/search/{cafNo}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse searchCAF(@PathParam("cafNo") String cafNo)
	{
		return userCafCoreService.searchCAF(cafNo);
	}

}
