/**
 * 
 */
package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.deploymentSetting.DeploymentSettingCoreService;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

/**
 * @author aditya
 * 
 */
@Path("setting")
public class DeploymentSettingServices
{

	@Autowired
	DeploymentSettingCoreService deploymentSettingCoreService;

	@Autowired
	SalesCoreServcie salesCoreServcie;

	@POST
	@Path("getallsetting")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSetting()
	{

		return deploymentSettingCoreService
				.getAllDeploymentSettingFromGlobalCache();

	}

	@POST
	@Path("getsettingbykey/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getSettingBasedOnKey(@PathParam("key") String key)
	{

		return deploymentSettingCoreService
				.getAllDeploymentSettingBasedOnKey(key);

	}

	@Path("update/sales/permission")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updatePermission(UpdateProspectVO updateProspectVO)
	{
		return salesCoreServcie.updatePermission(updateProspectVO);
	}

	@Path("update/ne/feasibility")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateFeasibility(UpdateProspectVO updateProspectVO)
	{
		return salesCoreServcie.updatePermission(updateProspectVO);
	}

}
