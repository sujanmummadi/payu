package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.releases.ReleaseCoreService;
import com.cupola.fwmp.util.AuthUtils;

@Path("release")
public class ReleaseService
{
	private static Logger log = LogManager.getLogger(ReleaseService.class.getName());
	@Autowired
	ReleaseCoreService releaseService;

	@POST
	@Path("current/{version}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse release(@PathParam("version") String version)
	{
		
		log.info("Service version  " + "V " + version + " for user " + AuthUtils.getCurrentUserDisplayName());
		return releaseService.getReleases("V " + version);

	}

	@POST
	@Path("update/{cityId}/{newVersion}/{branchId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateRelease(@PathParam("cityId") Long cityId,
			@PathParam("newVersion") String newVersion,
			@PathParam("branchId") Long branchId)
	{
		return releaseService.updateRelease(cityId, newVersion, branchId);

	}
	@GET
	@Path("reset/versioncache")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse resetVersionCache()
	{
		return releaseService.resetVersionCache();
	}

}
