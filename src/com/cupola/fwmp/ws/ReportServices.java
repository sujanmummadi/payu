package com.cupola.fwmp.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cupola.charts.Highchart;
import com.cupola.charts.HighchartData;
import com.cupola.charts.HighchartSeries;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.report.ReportCoreService;
import com.cupola.fwmp.util.ResponseUtil;

@Path("/reports")
public class ReportServices {
	private Logger logger = Logger.getLogger(ReportServices.class);

	private ReportCoreService reportCoreService;

	public void setReportCoreService(ReportCoreService reportCoreService) {
		this.reportCoreService = reportCoreService;
	}

	@POST
	@Path("/ticketreport")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketReport() {
		return reportCoreService.getCountWorkStageType();
	}

	@POST
	@Path("/statusreport")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getStatusReport() {
		return reportCoreService.getStatusReport();// /reportDAO.getStatusReport();
	}

	/*
	 * public APIResponse getTicketReportDummy() { Highchart highchart = new
	 * Highchart(); highchart.setChartype("pie"); highchart.setTitleText("");
	 * highchart.setSubtitleText(""); List<HighchartData> dataList = new
	 * ArrayList<HighchartData>(); HighchartData data1 = new HighchartData();
	 * data1.setName("CAT5"); data1.setY(5.0); data1.setColor("#0040ff");
	 * dataList.add(data1); HighchartData data2 = new HighchartData();
	 * data2.setName("FIBER"); data2.setY(10.0); data2.setColor("#ff8000");
	 * dataList.add(data2); HighchartSeries series = new HighchartSeries();
	 * series.setData(dataList); List<HighchartSeries> seriesList = new
	 * ArrayList<HighchartSeries>(); seriesList.add(series);
	 * highchart.setSeries(seriesList); highchart .setTooltipHeaderFormat(
	 * "<span style=\"font-size: 10px\">{point.key} : <b> {point.y}</b></span> <span style=\"font-size: 15px;color:{point.color} \"><b></b> </span><br/>"
	 * ); highchart.setTooltipPointFormat("");
	 * highchart.setExportingButtonsContextButtonEnabled(false);
	 * highchart.setLegendAlign("right");
	 * highchart.setLegendVerticalAlign("top");
	 * highchart.setLegendLayout("vertical"); highchart.setLegendX(0);
	 * highchart.setLegendY(80); highchart.setSizeHeight(160);
	 * highchart.setTitleStyleFontSize("14px");
	 * 
	 * highchart.setLegendEnabled(false); highchart.setCreditsEnabled(false);
	 * APIResponse apiResponce = new APIResponse();
	 * apiResponce.setData(highchart);
	 * 
	 * return apiResponce; }
	 */

	public APIResponse getStatusReportDummy() {
		Highchart highchart = new Highchart();
		highchart.setChartype("pie");
		highchart.setTitleText("");
		highchart.setSubtitleText("");
		List<HighchartData> dataList = new ArrayList<HighchartData>();
		HighchartData data1 = new HighchartData();
		data1.setName("INPROGRESS");
		data1.setY(10.0);
		data1.setColor("#0A9CD6");
		dataList.add(data1);
		HighchartData data2 = new HighchartData();
		data2.setName("COMPLETED");
		data2.setY(4.0);
		data2.setColor("#71C755");
		dataList.add(data2);
		HighchartData data3 = new HighchartData();
		data3.setName("REASSIGNED");
		data3.setY(2.0);
		data3.setColor("#1B3366");
		dataList.add(data3);
		/*
		 * highchart.setLegendEnabled(false);
		 */HighchartSeries series = new HighchartSeries();
		series.setData(dataList);
		List<HighchartSeries> seriesList = new ArrayList<HighchartSeries>();
		seriesList.add(series);
		highchart.setSeries(seriesList);
		highchart.setExportingButtonsContextButtonEnabled(false);
		highchart
				.setTooltipHeaderFormat("<span style=\"font-size: 10px\">{point.key} : <b> {point.y}</b></span> <span style=\"font-size: 15px;color:{point.color} \"><b></b> </span><br/>");
		highchart.setTooltipPointFormat("");
		highchart.setLegendAlign("right");
		highchart.setLegendVerticalAlign("top");
		highchart.setLegendLayout("vertical");
		highchart.setLegendX(0);
		highchart.setLegendY(70);
		highchart.setTitleStyleFontSize("14px");
		highchart.setCreditsEnabled(false);
		highchart.setSizeHeight(160);
		APIResponse apiResponce = new APIResponse();
		apiResponce.setData(highchart);

		return apiResponce;
	}

}
