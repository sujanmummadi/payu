package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.ticket.app.AppSummaryService;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * 
 * @author ashraf
 * 
 */

@Path("app/ticket")
public class AppSummaryServices
{

	@Autowired
	AppSummaryService summaryService;

	@POST
	@Path("summary/{workOrderTypeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketSummaryByWorkOrderType(
			@PathParam("workOrderTypeId") Long workOrderTypeId)
	{
		Long userId = AuthUtils.getCurrentUserId();
		return ResponseUtil
				.createSuccessResponse()
				.setData(summaryService.getTicketSummaryCount(userId, workOrderTypeId));
	}

	@POST
	@Path("ne/summary")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketSummaryByWorkOrderType()
	{
		return summaryService.getNeTicketCount();
	}
	@POST
	@Path("ne/summary/details")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketSummaryByWorkOrderTypeDetails(TicketFilter filter)
	{
		if(filter != null)
			ApplicationUtils.saveTicketFilter(filter);

		return summaryService.getNeTicketCount();
	}
}
