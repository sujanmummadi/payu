package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.rollback.RollBackService;
import com.cupola.fwmp.vo.RollBackVO;

@Path("rollback")
public class RollBack {
	
    @Autowired
	RollBackService rollBackService;
	
	@POST
	@Path("activity")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse rollBackActivity(RollBackVO confirmRollBackVO) {
		return rollBackService.rollBackActivity(confirmRollBackVO);
	}
}



