package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.integ.aat.AATCoreServiceImpl;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CustomerClosures;
import com.cupola.fwmp.vo.aat.AATEligibilityInput;

@Path("integ/aat")
public class AATService
{

	private static Logger log = LogManager
			.getLogger(AATService.class.getName());

	@Autowired
	AATCoreServiceImpl aatCoreServiceImpl;

	@POST
	@Path("/customerclosures")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAATDetailsforCustomer(
			CustomerClosures customerClosures)
	{
		log.info("CustomerClosures : " + customerClosures);

		if (customerClosures == null)
		{
			log.info("AATResponse can't be null" + customerClosures);
			return ResponseUtil.createFailureResponse();
		}
		return aatCoreServiceImpl.getAATDetailsforCustomer(customerClosures);
	}

	@POST
	@Path("/eligibleforaat")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse isAllFWMPActivityDone(
			AATEligibilityInput aatEligibilityInput)
	{
		log.info("isAllFWMPActivityDone service called " + aatEligibilityInput );
		
		if (aatEligibilityInput == null)
			return ResponseUtil.createNullParameterResponse();

		if (aatEligibilityInput.getConnectionType() == null
				|| aatEligibilityInput.getConnectionType().equals(null))
			return ResponseUtil.createNullParameterResponse()
					.setData("Connection type could not be null");

		if (aatEligibilityInput.getMqId() == null
				|| aatEligibilityInput.getMqId().equals(null))
			return ResponseUtil.createNullParameterResponse()
					.setData("MqId could not be null");
		

		if (aatEligibilityInput.getCityCode() == null
				|| aatEligibilityInput.getCityCode().equals(null))
			return ResponseUtil.createNullParameterResponse()
					.setData("City could not be null");

		return aatCoreServiceImpl.isAllFWMPActivityDone(aatEligibilityInput);
	}

}
