package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.ticket.TicketUpdateCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.vo.StatusVO;

@Path("ticketupdate")
public class TicketUpdateService
{

	private static Logger LOGGER = Logger.getLogger (TicketUpdateService.class.getName ());
	
	@Autowired
	TicketUpdateCoreService ticketUpdateCoreService;
	
	@Autowired
	private FrTicketService frService;
	
	@POST
	@Path("status")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateTicketStatus(StatusVO status)
	{
		
		LOGGER.info("updating status "+status + " for ticket "+status );
		
		if ( AuthUtils.isCCNRUser() )
			return frService.updateCCNRCompletedStatus( status );
		
		return ticketUpdateCoreService.updateTicketStatus(status);
	}
	
	@POST
	@Path("status/bulk")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateBulkTicketStatus(StatusVO status)
	{
		
		LOGGER.info("updating status "+status + " for ticket "+status );
		
		return ticketUpdateCoreService.updateTicketStatus(status);
	}
}
