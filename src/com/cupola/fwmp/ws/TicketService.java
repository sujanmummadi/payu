package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.MessageType;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.service.ticket.TicketCoreServiceImpl;
import com.cupola.fwmp.service.user.UserPreferenceService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.CafVo;
import com.cupola.fwmp.vo.ETRUpdatebyNE;
import com.cupola.fwmp.vo.GxCxPermission;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.RemarksVO;
import com.cupola.fwmp.vo.fr.FrEtrUpdateVo;

@Path("/ticket")
public class TicketService
{

	TicketCoreServiceImpl ticketService;

	@Autowired
	TicketDAO ticketdao;
	@Autowired
	UserPreferenceService userPreferenceService;
	@Autowired
	NotificationService notificationService;
	@Autowired
	DBUtil dbUtil;
	
	@Autowired
	GlobalActivities globalActivities;
	
	@Autowired
	private FrTicketService frService;
	
	private static Logger logger = Logger.getLogger(TicketService.class
			.getName());

	public TicketCoreServiceImpl getTicketService()
	{
		return ticketService;
	}

	public void setTicketService(TicketCoreServiceImpl ticketService)
	{
		this.ticketService = ticketService;
	}
	
	@GET
	@Path("status/{ticket}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketStatus(@PathParam("ticket") Long ticketId)
	{
		return ticketService.getTicketStatusById(ticketId);
	}
	
	
	@POST
	@Path("/ticketDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTickets()
	{

		TicketFilter filter = new TicketFilter();
		return ticketService.getTickets(filter);
	}

	@POST
	@Path("/assignto/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse assignTickets(@PathParam("userId") long userId,
			TicketFilter filter)
	{

		return ticketService.getTickets(filter);
	}

	@POST
	@Path("/assignto")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse assignTickets(AssignVO assignVO)
	{
		if( !AuthUtils.isFRUser() )
		return ticketService.assignTickets(assignVO);
		else 
			return frService.assignFrTickets(assignVO);
	}



	@POST
	@Path("/remark")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateRemarks(RemarksVO remark)
	{

		remark.setUserName(AuthUtils.getCurrentUserLoginId());
		return ticketService.updateRemarks(remark);

	}

	@POST
	@Path("/etr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateTicketEtrByNEorTL(ETRUpdatebyNE etrUpdatebyNE)
	{
		if( AuthUtils.isFRUser() )
		{
			if( etrUpdatebyNE == null )
				return ResponseUtil.nullArgument();
			
			FrEtrUpdateVo etrVo = new FrEtrUpdateVo();
			etrVo.setCurrentETR(etrUpdatebyNE.getCurrentETR());
//			etrVo.setCurrentProgress("");
			etrVo.setNewETR(etrUpdatebyNE.getNewETR());
			etrVo.setReasonCode(etrUpdatebyNE.getReasonCode());
			etrVo.setRemarks(etrUpdatebyNE.getRemarks());
			etrVo.setTicketId(etrUpdatebyNE.getTicketId());
			
			APIResponse apiResponse = frService.updateFrTicketEtrByNeOrTl(etrVo);
			
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				frService.updateFaltRepairActivityInCRM(Long.valueOf(etrUpdatebyNE.getTicketId()), null, null, null, null);
			}
			
			return apiResponse;
		}
		return ticketService.updateTicketEtrByNEorTL(etrUpdatebyNE);

	}

	@POST
	@Path("/caf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateTicketCaf(CafVo cafVo)
	{
		return ticketService.updateTicketCaf(cafVo);

	}

	@GET
	@Path("/counts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getCounts()
	{
		return ticketService.getTicketCount();

	}

	@GET
	@Path("status/counts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getCountSummary()
	{

		return ticketService.getCountSummary();
	}

	@POST
	@Path("status/counts/details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getCountSummaryDetails(TicketFilter filter)
	{
		if(filter != null)
			ApplicationUtils.saveTicketFilter(filter);


		return ticketService.getCountSummary();
	}
	
	@POST
	@Path("/reassignment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse reassignTicket(ReassignTicketVo reassignTicketVo)
	{
		if (reassignTicketVo == null)
			return ResponseUtil.createNullParameterResponse();

		if (reassignTicketVo.getTicketIds() == null
				|| reassignTicketVo.getTicketIds().isEmpty())
			return ResponseUtil
					.createNullParameterResponse("Ticket IDs could not be null");

		if (reassignTicketVo.getRecipientUserId() == null)
			return ResponseUtil
					.createNullParameterResponse("RecipientUserId IDs could not be null");

		if (!dbUtil.isTicketAvailable(Long
				.valueOf(reassignTicketVo.getTicketIds().iterator().next()
						.longValue()), AuthUtils.getCurrentUserId()))
			return ResponseUtil.createUnAutorizedFailureResponse();

		return ticketService.reassignTicket(reassignTicketVo);
	}

	@POST
	@Path("ribbon/counts/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getCountSummaryGropedByNE(
			@PathParam("type") Long initialWorkStagetype)
	{
		return ticketService.getCountSummaryGropedByNE(initialWorkStagetype);
	}

	@POST
	@Path("/updategxcx")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateGxCXPermission(GxCxPermission gxCxPermission)
	{

		return ticketService.updateGxCXPermission(gxCxPermission);

	}

	@POST
	@Path("/ticketNo/{ticketNo}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse statusNotActivated(@PathParam("ticketNo") long ticketNo)
	{
		MessageVO vo = new MessageVO();
		vo.setMessageType(MessageType.AUTO_CLOSURE_CUSTOMER_RESPONSE_NO);
		vo.setMessage(ticketNo+"");
		
		return ticketService.autoClosureStatusUpdateToUser(ticketNo, vo);

	}
	
	@GET
	@Path("/thresholdpriority")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getThresholdPriorityValue()
	{
		return ResponseUtil.createSuccessResponse().setData(FWMPConstant.THRESHOLD_VALUE_FOR_PRIORITY_TICKET);

	}
	
	@POST
	@Path("/recheck/activation/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse recheckActivation(@PathParam("ticketId") long ticketId)
	{

		MessageNotificationVo notificationVo = new MessageNotificationVo();
		notificationVo.setMessage(FWMPConstant.CustomerMessages.RECHECK_MESSAGE);
		notificationVo.setSubject("ACT Notification");
		notificationService.sendNotificationToCustomTicketId(ticketId, notificationVo);
		return ResponseUtil.createSuccessResponse();

	}

	@POST
	@Path("/ticketids")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTicketsIds()
	{
		if( AuthUtils.isFRCXDownNEUser() || AuthUtils.isFRCxUpNEUser() )
		return ResponseUtil.createSuccessResponse()
				.setData(globalActivities
							.getAllFrTicketFromMainQueueByKey(
									dbUtil.getKeyForQueueByUserId(AuthUtils.getCurrentUserId())));
		else
		return ResponseUtil.createSuccessResponse()
				.setData(globalActivities
							.getAllTicketFromMainQueueByKey(
									dbUtil.getKeyForQueueByUserId(AuthUtils.getCurrentUserId())));
		
	}
}
