package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.closeticket.MQSCloseTicketService;

@Path("/mq")
public class CloseTicketService
{

	@Autowired
	MQSCloseTicketService closeTicketService;

	@POST
	@Path("closeticket")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse closeTicket(TicketClosureDataVo closeTicketVo)
	{

		return closeTicketService.closeTicket(closeTicketVo);
	}

}
