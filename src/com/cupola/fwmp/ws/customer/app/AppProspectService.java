package com.cupola.fwmp.ws.customer.app;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.ApiAction;
import com.cupola.fwmp.FWMPConstant.CustomerAppFeasibilityType;
import com.cupola.fwmp.handler.custom.user.CustomerUserAuth;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.integ.app.AppPropspectCoreService;
import com.cupola.fwmp.service.tariff.TariffCoreService;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.integ.app.ProspectVO;

/**
 * @author aditya
 * @created 12:37:03 PM Apr 7, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */

@Path("integ/app/prospect")
public class AppProspectService
{

	private static Logger log = LogManager
			.getLogger(AppProspectService.class.getName());

	@Autowired
	AppPropspectCoreService appPropspectCoreService;

	// @Autowired(required = true)
	// private HttpServletRequest request;

	@Autowired
	CustomerUserAuth customerUserAuth;

	@Autowired
	TariffCoreService tariffCoreService;

	@Path("createProspect")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse createProspect(ProspectVO prospectVO)
	{
		// customerUserAuth.doAutoLogin(request, "admin", "admin123");

		// System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ " +
		// AuthUtils.getCurrentUserLoginId());
		// System.exit(0);
		if (prospectVO == null)
			return ResponseUtil.createNullParameterResponse();

		if (prospectVO.getAction() == null || prospectVO.getAction().isEmpty())
			return ResponseUtil.createInvalidActionType();

		if (prospectVO.getProspectNumber() == null
				|| prospectVO.getProspectNumber().isEmpty())
			return ResponseUtil.createNullProspectNumberFromCustomerApp();

		if (prospectVO.getAction().equalsIgnoreCase(ApiAction.CREATE))
		{
			if (prospectVO.getTitle() == null
					|| prospectVO.getTitle().isEmpty())
			{
				return ResponseUtil.createNoTiltle();
			}
			if (prospectVO.getFirstName() == null
					|| prospectVO.getFirstName().isEmpty())
			{
				return ResponseUtil.createNoFirstName();
			}
			if (prospectVO.getLastName() == null
					|| prospectVO.getLastName().isEmpty())
			{
				return ResponseUtil.createNoLastName();
			}
			if (prospectVO.getPrefferedCallDate() == null)
			{
				return ResponseUtil.createNoPrefferedCallDate();
			}
			if (prospectVO.getMobileNumber() == null
					|| prospectVO.getMobileNumber().isEmpty())
			{
				return ResponseUtil.createNoMobileNumber();
			}
			if (prospectVO.getEmailId() == null
					|| prospectVO.getEmailId().isEmpty())
			{
				return ResponseUtil.createNoEmailId();
			}
			if (prospectVO.getCurrentAddress() == null
					|| prospectVO.getCurrentAddress().isEmpty())
			{
				return ResponseUtil.createNoCurrentAddressFromCustomerApp();
			}
			if (prospectVO.getCommunicationAdderss() == null
					|| prospectVO.getCommunicationAdderss().isEmpty())
			{
				return ResponseUtil
						.createNoCommunicationAddressFromCustomerApp();
			}
			if (prospectVO.getPermanentAddress() == null
					|| prospectVO.getPermanentAddress().isEmpty())
			{
				return ResponseUtil.createNoPermanentAddressFromCustomerApp();
			}
			if (prospectVO.getCityCode() == null
					|| prospectVO.getCityCode().isEmpty())
			{
				return ResponseUtil.createNoCityCodeFromCustomerApp();
			}
			if (prospectVO.getBranchCode() == null
					|| prospectVO.getBranchCode().isEmpty())
			{
				return ResponseUtil.createNoBranchCodeFromCustomerApp();
			}
			if (prospectVO.getAreaCode() == null
					|| prospectVO.getAreaCode().isEmpty())
			{
				return ResponseUtil.createNoAreaCodeFromCustomerApp();
			}

			if (prospectVO.getCityCode() == null
					|| prospectVO.getCurrentAddress().isEmpty())
			{
				return ResponseUtil.createNoCityCodeFromCustomerApp();
			}

			if (prospectVO.getProspectType() == null
					|| prospectVO.getProspectType().isEmpty())
			{
				return ResponseUtil.createNoProspectTypeFromCustomerApp();
			}

			if (prospectVO.getLongitude() == null
					|| prospectVO.getLongitude().isEmpty())
			{
				return ResponseUtil.createNoLongitudeFromCustomerApp();
			}

			if (prospectVO.getLattitude() == null
					|| prospectVO.getLattitude().isEmpty())
			{
				return ResponseUtil.createNoLattitudeFromCustomerApp();
			}

			if (prospectVO.getConnectionType() == null
					|| prospectVO.getConnectionType().isEmpty())
			{
				return ResponseUtil.createNoConnectionTypeFromCustomerApp();

			} else if (prospectVO.getConnectionType()
					.equalsIgnoreCase(CustomerAppFeasibilityType.NON_FEASIBLE)
					|| prospectVO.getConnectionType()
							.equalsIgnoreCase(CustomerAppFeasibilityType.OTHERS)
					|| prospectVO.getConnectionType()
							.equalsIgnoreCase(CustomerAppFeasibilityType.FIBER_FX_CHOKE))
			{

			} else
			{
				if (prospectVO.getFxName() == null
						|| prospectVO.getFxName().isEmpty())
				{
					return ResponseUtil.createFxNameIsEmptyOrNull();

				}
			}

			if (prospectVO.getPincode() == null
					|| prospectVO.getPincode().isEmpty())
			{
				return ResponseUtil.createPincodeIsEmptyOrNull();

			}

		} else if (prospectVO.getAction().equalsIgnoreCase(ApiAction.UPDATE))
		{
			if (prospectVO.getUpdateType() == null)
				return ResponseUtil.createUpdateTypeCouldNotBeNull();

			if (prospectVO.getUpdateType() != null
					&& prospectVO.getUpdateType().isEmpty())
				return ResponseUtil.createUpdateTypeCouldNotBeEmpty();

			if (prospectVO.getUpdateType()
					.equalsIgnoreCase(ApiAction.UPDATE_TYPE_TARIFF))
			{
				if (prospectVO.getPlanCode() == null
						|| "null".equals(prospectVO.getPlanCode()))
				{
					return ResponseUtil.createPackageNameCouldNotBeNull();

				}

				if (prospectVO.getPlanCode() != null
						&& prospectVO.getPlanCode().isEmpty())
				{
					return ResponseUtil.createPackageNameCouldNotBeEmpty();

				} else
				{
					if (!tariffCoreService
							.getTariffByPlanCode(prospectVO.getPlanCode()))
					{
						return ResponseUtil.createPackageNotFound();
					}

				}

				if (prospectVO.getTariffPlanType() == null
						|| "null".equals(prospectVO.getTariffPlanType()))
				{
					return ResponseUtil.createTariffPlanTypeCouldNotBeNull();

				}
				if (prospectVO.getTariffPlanType() != null
						&& prospectVO.getTariffPlanType().isEmpty())
				{
					return ResponseUtil.createTariffPlanTypeCouldNotBeNull();

				}

			} else if (prospectVO.getUpdateType()
					.equalsIgnoreCase(ApiAction.UPDATE_TYPE_DOCUMENT))
			{
				if (prospectVO.getDocPath() == null
						|| "null".equals(prospectVO.getDocPath()))
				{
					return ResponseUtil.createDocumentPathCouldNotBeNull();

				} else if (prospectVO.getDocPath() != null
						&& prospectVO.getDocPath().isEmpty())
				{
					return ResponseUtil.createDocumnetPathCouldNotBeEmpty();

				}

			} else if (prospectVO.getUpdateType()
					.equalsIgnoreCase(ApiAction.UPDATE_TYPE_PAYMENT))
			{
				if (prospectVO.getPaymentTransactionNo() == null
						|| "null".equals(prospectVO.getPaymentTransactionNo()))
				{
					return ResponseUtil
							.createPaymentTransactionNoCouldNotBeNull();

				} else if (prospectVO.getPaymentTransactionNo() != null
						&& prospectVO.getPaymentTransactionNo().isEmpty())
				{
					return ResponseUtil
							.createPaymentTransactionNoCouldNotBeEmpty();
				}

				if (prospectVO.getAmountPaid() == null
						|| "null".equals(prospectVO.getAmountPaid()))
				{
					return ResponseUtil.createPaidAmmountCouldNotBeNull();

				} else if (prospectVO.getAmountPaid() != null
						&& prospectVO.getAmountPaid().isEmpty())
				{
					return ResponseUtil.createPaidAmmountCouldNotBeEmpty();
				}

				if (prospectVO.getInstallationCharge() == null
						|| "null".equals(prospectVO.getInstallationCharge()))
				{
					return ResponseUtil
							.createInstallationChargeCouldNotBeNull();

				} else if (prospectVO.getInstallationCharge() != null
						&& prospectVO.getInstallationCharge().isEmpty())
				{
					return ResponseUtil.createInstallationChargeNotBeEmpty();
				}

			} else
			{
				return ResponseUtil.createInvalidUpdateType();
			}

		} else
		{
			return ResponseUtil.createInvalidActionType();
		}

		log.info("Prospect VO from App side is  " + prospectVO);
		return appPropspectCoreService.createProspect(prospectVO);

	}

	@Path("getprospect")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getProspect(@QueryParam("id") Long id)
	{
		log.debug("Getting Prospect VO from App side for id  " + id);
		return appPropspectCoreService.getPropspectById(id);

	}

}
