package com.cupola.fwmp.ws.customer;

import java.util.Iterator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.AddressType;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.customer.ProspectCoreService;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.ProspectUpdateVO;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;

@Path("prospect")
public class ProspectService {
	@Autowired
	ProspectCoreService prospectCoreService;

	@Autowired
	CustomerCoreService customerCoreService;
	
	@Autowired
	SalesCoreServcie salesCoreServcie;
	
	
	private static Logger LOGGER = Logger.getLogger(ProspectService.class);
	
	@POST
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse createNewProspect(ProspectCoreVO prospect) {

		LOGGER.info(" $$$$$$$$$$$$%%%%%%%%%%%%%% Adding New Prospect with data " + prospect);

		List<CustomerAddressVO> customeraddressvos = prospect.getCustomer().getCustomerAddressVOs();

		LOGGER.info(" $$$$$$$$$$$$%%%%%%%%%%%%%% customeraddressvos  " + customeraddressvos);

		if (customeraddressvos != null) {

			for (Iterator<CustomerAddressVO> iterator = customeraddressvos.iterator(); iterator.hasNext();) {

				try {

					CustomerAddressVO customerAddressVO = (CustomerAddressVO) iterator.next();

					ObjectMapper mapper = new ObjectMapper();
					String addressJsonString = mapper.writeValueAsString(customerAddressVO);
					
					if (customerAddressVO.getCustomerAddressType() != null
							&& (customerAddressVO.getArea() != null || customerAddressVO.getAreaId() > 0)
							&& (customerAddressVO.getOperationalEntity() != null
									|| customerAddressVO.getOperationalEntityId() > 0)
							&& (customerAddressVO.getSubArea() != null || customerAddressVO.getSubAreaId() > 0))

					{
						if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.CURRENT_ADDRESS))
							prospect.getCustomer().setCurrentAddress(addressJsonString);
						LOGGER.info("addressJsonString" + addressJsonString);

						if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.PERMANENT_ADDRESS))
							prospect.getCustomer().setPermanentAddress(addressJsonString);

						if (customerAddressVO.getCustomerAddressType()
								.equalsIgnoreCase(AddressType.COMMUNICATION_ADDRESS))
							prospect.getCustomer().setCommunicationAdderss(addressJsonString);
					} else {
						LOGGER.info("Invalid Address Type !!");
						return ResponseUtil.createInvalidAddress();
					}
				} catch (Exception e) {
					
					LOGGER.error("Error while converting address into json format " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} else {
			LOGGER.info("Invalid Address Type !!");
			return ResponseUtil.createInvalidAddress();
		}

		LOGGER.info(" Adding New Prospect with data " + prospect);

		if (prospect != null && prospect.getCustomer() != null) {
			prospect.getCustomer().setAddedBy(AuthUtils.getCurrentUserId());
			if (prospect.getCustomer().getCityId() == null)
				prospect.getCustomer().setCityId(AuthUtils.getCurrentUserCity().getId());
		}

		LOGGER.info("prospect details from app is :::" + prospect);

		return prospectCoreService.createNewProspect(prospect, null);
	}

	@POST
	@Path("retry")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse retryProspectCreation(ProspectCoreVO prospect) {

		LOGGER.info(" Retrying Prospect creation in MQ with data  " + prospect);
		return prospectCoreService.retryProspectCreation(prospect);
	}

	@POST
	@Path("update/{ticketId}/{prospectNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateProspectNumber(@PathParam("ticketId") Long ticketId,
			@PathParam("prospectNumber") Long prospectNumber) {
		LOGGER.info(" Updating Prospect no. with data ticketid " + ticketId + " prospectNumber" + prospectNumber);
		if (AuthUtils.isCFEUser() && prospectNumber != null && prospectNumber > 0) {
			return prospectCoreService.updateNewProspect(ticketId, prospectNumber);
		} else {
			return ResponseUtil.createUnAutorizedFailureResponse();
		}

	}

	@POST
	@Path("createbulk")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse createBulkProspect(List<ProspectCoreVO> prospects) {

		LOGGER.info(" Adding Bulk Prospect with data " + prospects);
		for (Iterator iterator = prospects.iterator(); iterator.hasNext();) {
			ProspectCoreVO prospectCoreVO = (ProspectCoreVO) iterator.next();
			if (prospectCoreVO != null && prospectCoreVO.getCustomer() != null) {
				prospectCoreVO.getCustomer().setAddedBy(AuthUtils.getCurrentUserId());
				prospectCoreVO.getCustomer().setCityId(AuthUtils.getCurrentUserCity().getId());
			}
			prospectCoreService.createNewProspect(prospectCoreVO, null);
		}

		return ResponseUtil.createSaveSuccessResponse();
	}

	@POST
	@Path("update")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateNewProspect(ProspectUpdateVO prospect) {

		LOGGER.info(" Updating New Prospect with data " + prospect);
		////
		return prospectCoreService.updateNewProspect(prospect);
	}

	@GET
	@Path("convert/{prospectId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse appProspectWrapper(@PathParam("prospectId") String prospectId) {

		LOGGER.info(" Updating New Prospect with data " + prospectId);
		////
		return prospectCoreService.appProspectWrapper(Long.valueOf(prospectId));
	}

	@POST
	@Path("customer/update")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateCustomerDetails(CustomerVO customerVo) {

		LOGGER.info(" Updating Customer Details with data ");
		////
		return customerCoreService.updateCustomer(customerVo);

	}

}
