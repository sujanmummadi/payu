package com.cupola.fwmp.ws.customer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.vo.MessageNotificationVo;

@Path("/notification")
public class PushNotificationService
{
	
	@Autowired
	MailService mailService;
	
	private static Logger LOGGER = Logger.getLogger(PushNotificationService.class);
	
	@POST
	@Path("notifycustomer")
	@Consumes(MediaType.APPLICATION_JSON)
	public void sendMessage(MessageNotificationVo notificationVo){/*
		
		if(notificationVo.getMobileNumber() != null){
			
			UserDataFromMq dataFromMq = new UserDataFromMq(notificationVo.getMobileNumber(), notificationVo.getMessage(), "1");
			MQResponse response = UserAPI.getMqResponseForRoi(dataFromMq);
			LOGGER.info("mq response after sending notification to "+notificationVo.getMobileNumber()+" is "+response);
		}	
		
		if(notificationVo.getEmail() != null){
			
			mailService.sendSimpleMail(notificationVo.getTo(), notificationVo.getSubject(), notificationVo.getMessage());
		}
	*/}
}
