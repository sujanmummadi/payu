package com.cupola.fwmp.ws.device;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.device.DeviceService;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.TypeAheadVo;
@Path("/userdevice")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserDeviceService
{

	@Autowired
	DeviceService deviceService;
	@POST
	@Path("add/devices")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse  addDevices(List<DeviceVO> devices) 
	{
		
		return deviceService.addDevices(devices);
	}
	

	 
	@POST
	@Path("list/devices/{cityId}/{branchId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse  getDevices( @PathParam("cityId") Long cityId, @PathParam("branchId") Long branchId) 
	{
		
		return  deviceService.getDevices(cityId,branchId); 
	}
	
	@POST
	@Path("similar/devices/{cityId}/{branchId}/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse  getDevicesByKey( @PathParam("cityId") Long cityId, @PathParam("branchId") Long branchId,@PathParam("key") String key) 
	{
		
		return  deviceService.getDevicesByKey( cityId,branchId,key); 
	}
	
	@POST
	@Path("list/user/devices/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse  getDevicesForUser( @PathParam("userId") Long userId ) 
	{
		
		return  deviceService.getDevicesForUser(userId); 
	}
	
	@POST
	@Path("map/user/devices/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse  mapDevicesForUser(List<TypeAheadVo> deviceIds  ,@PathParam("userId") Long userId ) 
	{
		
		return  deviceService.mapDevicesForUser(deviceIds,userId); 
	}
}
