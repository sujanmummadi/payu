package com.cupola.fwmp.ws.frMaterialRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;

import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.frMaterialRequest.MaterialRequestService;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.fr.MaterialRequestInputVo;
import com.cupola.fwmp.vo.fr.MaterialRequestVo;

@Path("/material")
public class MaterialRequestServices
{

	@Autowired
	MaterialRequestService materialRequestService;
	
	@Autowired
	private FrTicketService frTicketService;
	
	@Autowired
	private DBUtil dbUtil;

	private static Logger LOGGER = Logger
			.getLogger(MaterialRequestServices.class.getName());

	@Path("/request")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addMaterialRequest(MaterialRequestInputVo materialRequestInputVo)
	{

		LOGGER.info("In MaterialRequestServices , MaterialRequestVo is ::::"
				+ materialRequestInputVo);

		if (materialRequestInputVo != null)
		{

//			return materialRequestService.addMaterialRequest(materialRequestInputVo);
			APIResponse apiResponse = materialRequestService.addFrMaterialRequest(materialRequestInputVo);
			
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				frTicketService.updateFaltRepairActivityInCRM(Long.valueOf(materialRequestInputVo.getTicketId()), null, null, null, null);
			}
			
			
			return apiResponse;
			
		} else
		{

			return ResponseUtil.createSaveFailedResponse();
		}

	}

	@Path("/details/{id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getMaterialRequestDetails(
			@PathParam("id") String ticketId )
	{
		LOGGER.info("In materialRequest web services ticketId is::: "+ticketId);
		
		
			return ResponseUtil
					.recordFoundSucessFully()
					.setData(materialRequestService.getMaterialRequestDetails(Long.valueOf(ticketId)));
		
	}

	@Path("/reqandconsump/detail/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getMaterialDetailsByTicket(
			@PathParam("id") Long ticketId )
	{
		LOGGER.info("Getting Material Request and Consume Detail : "+ticketId);
		
		if( ticketId == null || ticketId.longValue() <= 0)
			return ResponseUtil.nullArgument();
		
			return ResponseUtil
					.createSuccessResponse()
					.setData(materialRequestService.getMaterialDetailsByTicket(ticketId));
	}
	
	@Path("/list")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getMaterialList()
	{
		LOGGER.info("*************  Getting Material List *********** ");
		return ResponseUtil.createSuccessResponse()
					.setData(materialRequestService.getAllMaterialForFr());
	}
}
