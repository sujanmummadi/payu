 /**
 * @Author kiran  Sep 11, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.ecaf.EcafDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.EcafVo;

/**
 * @Author kiran  Sep 11, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Path("ecaf")
public class EcafService {
	
	@Autowired
	EcafDAO eCAFDao;
	
	
	/**
	 * @author kiran
	 * APIResponse
	 * @param ticketId
	 * @return
	 */
	@POST
	@Path("details/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse geteCAFDetails(@PathParam("ticketId") Long ticketId) {
		
		EcafVo eCAFVo = eCAFDao.geteCAFDetails(ticketId);
		
		return ResponseUtil.createSuccessResponse().setData(eCAFVo);
	}

}
