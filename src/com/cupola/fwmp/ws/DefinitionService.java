package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.WorkProgressCode;

@Path("definition")
public class DefinitionService
{
	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	WorkProgressCode workProgressCode;
	
	@Autowired
	private FrTicketService frService;

	@POST
	@Path("/reload")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse reloadDataFromProperties()
	{
		try
		{
			definitionCoreService.reloadDataFromProperties();
		} catch (Exception e)
		{
			e.printStackTrace();

			return ResponseUtil.createFailureResponse();
		}
		return ResponseUtil.createSuccessResponse();

	}

	@POST
	@Path("/rolestatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getRoleStatusDifinition()
	{

		return definitionCoreService.getRoleStatusDifinition();
	}

	@POST
	@Path("/source")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getMediaSource()
	{
		return definitionCoreService
				.getMediaSource(AuthUtils.getCurrentUserCity().getId());
	}

	@POST
	@Path("/inquiry")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getInquiry()
	{
		return definitionCoreService.getInquiry();
	}

	@POST
	@Path("get/customertype")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCustomerType()
	{
		return definitionCoreService.getCustomerType();
	}

	@POST
	@Path("/reasoncode")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getEtrReasonCode()
	{
		return definitionCoreService.getEtrReasonCode();
	}

	@POST
	@Path("/reassignmentstatus")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse readReassignmentReasonCode()
	{
		return definitionCoreService.readReassignmentReasonCode();

	}

	@POST
	@Path("/paymentstatus")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse readPaymentreasonCode()
	{
		return definitionCoreService.readPaymentreasonCode();
	}

	@POST
	@Path("/documentstatus")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse readDocumentReasonCode()
	{

		return definitionCoreService.readDocumentReasonCode();

	}

	@POST
	@Path("hot/prospect/reason")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getHotProspectReasoncode()
	{
		return definitionCoreService.getHotProspectReasoncode();
	}

	@POST
	@Path("cold/prospect/reason")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getColdProspectReasoncode()
	{
		return definitionCoreService.getColdProspectReasoncode();
	}

	@POST
	@Path("medium/prospect/reason")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getMediumProspectReasoncode()
	{
		return definitionCoreService.getMediumProspectReasoncode();
	}

	@POST
	@Path("sales/customer/denied/permission/reason")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getSalesCustomerDeniedReasons()
	{
		return definitionCoreService.getSalesCustomerDeniedReasons();
	}

	@POST
	@Path("deployment/customer/denied/permission/reason")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getDeploymentCustomerDeniedReasons()
	{
		return definitionCoreService.getDeploymentCustomerDeniedReasons();
	}

	@POST
	@Path("copper2fiber")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCopper2FiberReasons()
	{
		return definitionCoreService.getCopper2FiberReasons();
	}

	@POST
	@Path("feasibility/rejected/by/ne")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getFeasibilityRejectedByNeReasons()
	{
		return definitionCoreService.getFeasibilityRejectedByNeReasons();
	}

	@POST
	@Path("/resolve")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getResolveStatusCode()
	{
		return definitionCoreService.readResolveStatusCode();
	}

	@POST
	@Path("/code/{groupId}/{statusId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getReasonCode(@PathParam("groupId") Long groupId,
			@PathParam("statusId") Long statusId)
	{
		return definitionCoreService.getReasonCode(groupId, statusId);
	}

	@POST
	@Path("feasibiltyreasoncode")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse readfeasibilityReasonCode()
	{
		return definitionCoreService.readfeasibilityReasonCode();
	}

	@POST
	@Path("fibertypes")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getFiberTypes()
	{
		return definitionCoreService.getFiberTypes();
	}

	@POST
	@Path("workprogresscode")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getWorkProgressCode()
	{
		return workProgressCode.getWorkProgressCode();
	}

	@POST
	@Path("element/denial/reason/blr")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getElementDenialReasonCodeBlr()
	{
		String cityName = AuthUtils.getCurrentUserCity().getName();

		if (cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY))
		{
			return definitionCoreService.getElementDenialReasonCodeHyd();

		} else
		{
			return definitionCoreService.getElementDenialReasonCodeBlr();
		}

	}

	@POST
	@Path("element/denial/reason/hyd")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getElementDenialReasonCodeHyd()
	{
		return definitionCoreService.getElementDenialReasonCodeHyd();
	}

	@POST
	@Path("profession")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getProfession()
	{
		return definitionCoreService.getProfession();
	}

	@POST
	@Path("title")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTitle()
	{
		return definitionCoreService.getTitle();
	}

	@POST
	@Path("prospect/types")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getProspectTypes()
	{
		return definitionCoreService.getProspectTypes();
	}

	@POST
	@Path("sales/dropdowns")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getSalesDropdown()
	{
		return definitionCoreService.getSalesDropdown();
	}

	@GET
	@Path("status/edtitable")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getEditableStatus()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(definitionCoreService
						.getEditableStatusFilterForCurrentUser());
	}

	@GET
	@Path("customer/remarks")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCustomerRemarks()
	{
		return definitionCoreService.getCustomerRemarks();
	}

	@GET
	@Path("customer/remarks/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCustomerRemarksByKey(@PathParam("key") String key)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(definitionCoreService.getCustomerRemarksByKey(key));
	}

	@GET
	@Path("getallstates")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllStates()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(definitionCoreService.getAllStates());
	}
	
	@GET
	@Path("reload/inteandclassi")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse reloadInteAndClassification()
	{
		definitionCoreService.reloadClassificationAndIntegration();
		return ResponseUtil.createSuccessResponse();
	}
	@GET
	@Path("getNationality")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getNationality()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(definitionCoreService.getNationality());
	}
	
	@GET
	@Path("getExistingISP")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getExistingISP()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(definitionCoreService.getExistingISP());
	}
	@GET
	@Path("getUinType")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUinType()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(definitionCoreService.getUinType());
	}
	@GET
	@Path("reload/flowidmapping")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse reloadFlowIdMappingBlrAndHyd()
	{
		return frService.reloadSymptomFlowIdMappingFile();
	}
	
}
