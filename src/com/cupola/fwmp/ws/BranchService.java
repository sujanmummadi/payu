package com.cupola.fwmp.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.branch.BranchCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.BranchVO;
import com.cupola.fwmp.vo.TypeAheadVo;

@Path("branch")
public class BranchService
{

	BranchCoreService branchService;

	public void setBranchService(BranchCoreService branchService)
	{
		this.branchService = branchService;
	}

	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addCity(List<BranchVO> branchVOs)
	{
		return branchService.addBranches(branchVOs);
	}
	
	@POST
	@Path("add/branch")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addBranchUI(BranchVO branchVOs)
	{
		return branchService.addBranch(branchVOs);
	}

	@POST
	@Path("list/{cityid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getBranchesByCityId(
			@PathParam("cityid") String cityId)
	{

		return branchService.getBranchesByCityId(cityId);
	}

	@POST
	@Path("app/list/")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getBranchesByCityId()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(branchService.getBranchesByCityId(AuthUtils
						.getCurrentUserCity().getId() + ""));
	}
	@POST
	@Path("flowswitch/{branchId}/{value}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse flowSwitchOnOff(@PathParam("branchId") Long branchId 
			,@PathParam("value") Long value)
	{
		if( branchId == null || branchId <= 0 
				|| value == null )
			return ResponseUtil.nullArgument();
		
		return branchService.manageFlowByBranchId(branchId,value );
	}
	
	@POST
	@Path("flowswitch/detail/{filterByCityOrBranch}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getBranchFlowSwitchDetail(@PathParam("filterByCityOrBranch") Long branchIdOrCityId )
	{
		if( branchIdOrCityId == null || branchIdOrCityId <= 0 )
			return ResponseUtil.nullArgument();
		
		return branchService.getBranchDetailBy(branchIdOrCityId);
	}
}
