package com.cupola.fwmp.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.filters.TabFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.TabDetailsVo;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.TypeAheadVo;

@Transactional
@Path("/tablet")
public class TabletService
{
	final static Logger log = Logger.getLogger(TabletService.class);

	private com.cupola.fwmp.service.tablet.TabletService tabletService;

	public void setTabletService(
			com.cupola.fwmp.service.tablet.TabletService tabletService) {
		this.tabletService = tabletService;
	}

	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addTablet(TabletVO tabletVo)
	{
		log.debug("add Tablet tablet :" + tabletVo);
		if (tabletVo == null)
			return ResponseUtil.createNullParameterResponse();
		
		return tabletService.addTablet(tabletVo);
	}

	@POST
	@Path("/tab/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getTabById(@PathParam("id") Long id)
	{
		log.debug("Get Tab By Id " + id);
		if (id == null)
			return ResponseUtil.createNullParameterResponse();

		return tabletService.getTabletById(id);
	}

	@POST
	@Path("tabs")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllTabs()
	{
		log.debug("Get All Tabs ");
		return tabletService.getAllTablet();
	}
	
	@POST
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllTab() {

		return tabletService.getAllTabs();
	}

	
	@POST
	@Path("delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deleteTab(@PathParam("id") Long id)
	{
		log.debug("Delete Tab id :"+id);
		if (id == null)
			return ResponseUtil.createNullParameterResponse();

		return tabletService.deleteTablet(id);
	}
	
	
	@POST
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateTablet(TabletVO tabletVo){
		log.debug("Update Tablet tablet :" + tabletVo);
		if (tabletVo == null)
			return ResponseUtil.createNullParameterResponse();
		
//		return ResponseUtil.createUpdateSuccessResponse();
		return tabletService.updateTablet(tabletVo);
		
	}
	
	@POST
	@Path("/updateregistrationId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateRegistrationId(TabDetailsVo tabDetailsVo)
	{
		log.info("updateRegistrationId  ::" + tabDetailsVo);
		
		APIResponse response = tabletService.updateRegistrationId(tabDetailsVo);
		
		log.info("response of updateRegistrationId : "+response);
			
		return response;
	}
	
	@POST
	@Path("/pagination")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse tabPagination(TabFilter tf)
	{
			
		return tabletService.tabPagination(tf);
	}
	
	@POST
	@Path("/gettab/by/{macAddress}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse tabPagination(@PathParam("macAddress")  String macAddress)
	{
		return tabletService.getTabletByMacAddress(macAddress);
	}
	
}
