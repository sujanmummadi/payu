package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cupola.fwmp.dao.integ.customerAppNotification.CustomerActivationVO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.customerApp.CustomerAppCoreServiceInteg;
import com.cupola.fwmp.util.ResponseUtil;

@Path("/integ/customer")
public class CustomerAppActivationServices
{

	private CustomerAppCoreServiceInteg customerAppCoreService;

	public void setCustomerAppCoreService(
			CustomerAppCoreServiceInteg customerAppCoreService)
	{
		this.customerAppCoreService = customerAppCoreService;
	}

	@POST
	@Path("/activation")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse customerActivation(
			CustomerActivationVO customerActivation)
	{

		String responseToCustomer = customerAppCoreService
				.customerActivation(customerActivation);

		if (responseToCustomer != null && responseToCustomer.equals("success"))
		{
			return ResponseUtil.createSuccessResponse()
					.setData(responseToCustomer);

		} else
		{
			return ResponseUtil.createFailureResponse()
					.setData(responseToCustomer);
		}

	}

}
