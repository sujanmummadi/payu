package com.cupola.fwmp.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.sms.AutoClosureCoreService;
import com.cupola.fwmp.util.ResponseUtil;

@Path("integ/closure")
public class AutoClosureService
{
	@Autowired
	AutoClosureCoreService autoClosureCoreService;

	private static final Logger LOGGER = LogManager
			.getLogger(AutoClosureService.class.getName());

	@GET
	@Path("bysms")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse ticketAutoClosureBySms(
			@QueryParam("message") String message,
			@QueryParam("mobileNo") String mobileNo)
	{
		LOGGER.info(" entered into  smsActivation() :: with this message : "
				+ message + " And Mobile NO : " + mobileNo);

		if (message == null || mobileNo == null
				|| (mobileNo != null && mobileNo.isEmpty())
				|| (message != null && message.isEmpty()))
			return ResponseUtil.createNullParameterResponse();

		return autoClosureCoreService
				.autoClosureTicket(message, mobileNo, null);
	}

	@GET
	@Path("byweb")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse ticketAutoClosureByWeb(
			@QueryParam("message") String message,
			@QueryParam("mobileNo") String mobileNo,
			@QueryParam("prospectNo") String prospectNo)
	{
		LOGGER.info(" entered into  smsActivation() by web :: with this message : "
				+ message + " And Mobile NO : " + mobileNo);

		if (message == null || mobileNo == null
				|| (mobileNo != null && mobileNo.isEmpty())
				|| (message != null && message.isEmpty())
				|| (prospectNo != null && prospectNo.isEmpty()))
			return ResponseUtil.createNullParameterResponse();

		APIResponse response = autoClosureCoreService
				.autoClosureTicket(message, mobileNo, prospectNo);
		
		LOGGER.info("Result " + response);
		
		return response;
	}

	@GET
	@Path("byapp")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse ticketAutoClosureByApp(
			@QueryParam("message") String message,
			@QueryParam("mobileNo") String mobileNo)
	{

		LOGGER.info(" entered into  smsActivation() :: with this message byapp : "
				+ message + " And Mobile NO : " + mobileNo);

		if (message == null || mobileNo == null
				|| (mobileNo != null && mobileNo.isEmpty())
				|| (message != null && message.isEmpty()))
			return ResponseUtil.createNullParameterResponse();

		return autoClosureCoreService
				.autoClosureTicket(message, mobileNo, null);
	}
}
