package com.cupola.fwmp.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import com.cupola.fwmp.auth.AppUser;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.filters.VendorFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.vendor.VendorCoreService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.InputVendorVO;
import com.cupola.fwmp.vo.VendorVO;

@Path("/vendors")
public class VendorServices
{

	static final Logger log = Logger.getLogger(VendorServices.class);

	private VendorCoreService vendorService;

	public void setVendorService(VendorCoreService vendorService)
	{

		this.vendorService = vendorService;
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addVendor(VendorVO vendor)
	{

		APIResponse api= vendorService.addVendor(vendor);
	    return api;
		
		
		
	}

	@POST
	@Path("/allvendor")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllVendors()
	{
		long startTime=System.currentTimeMillis();
		log.info("start time is ::"+startTime);
		APIResponse apGetAllVendor =vendorService.getAllVendors();
		

		long endTime=System.currentTimeMillis();
		
		long finalTime=endTime-startTime;
		
		log.info("end time is ::"+endTime+", final time is ::"+finalTime);
		
		return apGetAllVendor;

	}

	@POST
	@Path("/counts")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCountAllVendor()
	{

		return vendorService.getCountAllVendor();

	}

	@POST
	@Path("/count/{skillId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCountAllVendorBySkill(@PathParam("skillId") Long id)
	{

		return vendorService.getCountAllVendorBySkill(id);

	}

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateVendor(VendorVO vendor)
	{

		log.debug("vendor id in vendorServices updateVendor:" + vendor.getId());

		return vendorService.updateVendor(vendor);
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse deleteVendor(Long vendorId)
	{

		// log.debug("vendor id in vendorServices updateVendor:" +
		// vendor.getId());
		log.info("&&&&&&&&" + vendorId);

		return vendorService.deleteVendor(Long.valueOf(vendorId));
	}

	@POST
	@Path("/add/dailycounts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addVendorSkillAndCount(VendorVO vendorvo)
	{

		return vendorService.addVendorSkillAndCount(vendorvo);
	}

	@POST
	@Path("/updateCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateCountSkill(InputVendorVO inputVendorvo)
	{

		return vendorService.updateCountSkill(inputVendorvo);

	}

	@POST
	@Path("/assignForTL")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse vendorAssignmentForTL(InputVendorVO inputVendorvo)
	{

		return vendorService.vendorAssignmentForTL(inputVendorvo);

	}

	@POST
	@Path("/addUserVendor")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addUserVendorMapping(VendorVO vendorvo)
	{

		return vendorService.addUserVendorMapping(vendorvo);

	}

	@POST
	@Path("/assignByTLForNE")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse vendorAssignmentByTLForNE(InputVendorVO inputVendorvo)
	{

		/*
		 * AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
		 * .getAuthentication().getPrincipal();
		 */
		// log.debug("AppUser user Details::"+userDetails);

		// Long userId =AuthUtils.getCurrentUserId();

		// log.debug("getCurrentUserId::"+userId);

		// int roleId=AuthUtils.getCurrentUserRole();

		// log.debug("getCurrentUserRole::"+roleId);

		// inputVendorvo.setUserId(String.valueOf(userId));

		// inputVendorvo.setRoleId(String.valueOf(roleId));

		return vendorService.vendorAssignmentByTLForNE(inputVendorvo);

	}

	@POST
	@Path("/detailsForTL&NE")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getVendorDetails()

	{

		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		Long userId = AuthUtils.getCurrentUserId();

		log.debug("AuthUtils.getCurrentUserId()" + userId);

		String loginName = AuthUtils.getCurrentUserLoginId();

		log.debug("AuthUtils.getCurrentUserId()" + loginName);

		int roleid = AuthUtils.getCurrentUserRole();

		log.debug("AuthUtils.getCurrentUserId()" + roleid);

		return vendorService.getVendorDetails(userId);

	}

	@POST
	@Path("/detailsForAndroid")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getVendorDetailsForAndroid()

	{

		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		Long userId = AuthUtils.getCurrentUserId();

		log.debug("userid number::: in web services" + userId);

		String loginName = AuthUtils.getCurrentUserLoginId();

		int roleid = AuthUtils.getCurrentUserRole();

		return vendorService.getVendorDetailsForAndroid(userId);
	}

	@POST
	@Path("/detailsForWebUI")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getVendorDetailsForWebUI()

	{

		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		Long userId = AuthUtils.getCurrentUserId();

		String loginName = AuthUtils.getCurrentUserLoginId();

		int roleid = AuthUtils.getCurrentUserRole();

		return vendorService.getVendorDetailsForUI(userId);
	}

	@POST
	@Path("/byfilter")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getVendorsOnFilter(VendorFilter vendorFilter)
	{

		log.info("vendorFilter  valueeeeeeeeee" + vendorFilter);
		/*
		 * try {
		 * 
		 * SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 * 
		 * java.util.Date validstartdate =
		 * df.parse(vendorFilter.getFromDate()+""+"00:00:00");
		 * 
		 * vendorFilter.setFromDate(new Timestamp(validstartdate.getTime()));
		 * 
		 * java.util.Date validenddate =
		 * df.parse(vendorFilter.getToDate()+""+"23:59:59");
		 * 
		 * vendorFilter.setToDate(new Timestamp(validenddate.getTime()));
		 * 
		 * return vendorService.getVendorsOnFilter(vendorFilter);
		 * 
		 * 
		 * } catch (Exception e) { e.printStackTrace();
		 * 
		 * return ResponseUtil.NoRecordFound();
		 * 
		 * 
		 * APIResponse getVendorsOnUserAndLocationFilter( VendorFilter
		 * vendorFilter) { }
		 */

		return vendorService.getVendorsOnFilter(vendorFilter);

	}

	@POST
	@Path("/byuserLocationfilter")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getVendorsOnUserAndLocationFilter(
			VendorFilter vendorFilter)
	{

		return vendorService.getVendorsOnUserAndLocationFilter(vendorFilter);
	}

	@POST
	@Path("/vendorlist")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse vendorList()
	{

		return vendorService.vendorList();

	}

	@POST
	@Path("/skilllist")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse skillList()
	{

		return vendorService.skillList();

	}
	
	@POST
	@Path("/tlvendors")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getVendorSkillDetailsByTl(TicketFilter filter)
	{
		if(filter != null)
			ApplicationUtils.saveTicketFilter(filter);
		return vendorService.getVendorSkillDetailsByTl();
	}

}
