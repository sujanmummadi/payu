package com.cupola.fwmp.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.configuration.ConfigurationCoreService;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.UserVo;

@Path("setup")
public class ConfigurationService
{
	@Autowired
	ConfigurationCoreService configurationCoreService;
	
	
	@POST
	@Path("devices/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addDevices(List<DeviceVO> deviceVOs){
		
		return configurationCoreService.addDevices(deviceVOs);
	}
	
	@POST
	@Path("tabs/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addTablets(List<TabletVO> tabletVOs){
		
		return configurationCoreService.addTablets(tabletVOs);
	}
	
	@POST
	@Path("users/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addUsers(List<UserVo> userVOs){
		
		return configurationCoreService.addUsers(userVOs);
	}
	
	

	@POST
	@Path("devices/names")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getAllDeviceNames(){
		
		return configurationCoreService.getAllDeviceNames();
	}
	
	@POST
	@Path("users/add/defaults")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addDefaultSalesUserForAreas(List<UserVo> userVOs){
		
		return configurationCoreService.addDefaultSalesUserForAreas(userVOs);
	}
	
}
