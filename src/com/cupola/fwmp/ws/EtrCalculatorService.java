package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculatorCoreService;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculatorPojo;

@Path("etrcal")
public class EtrCalculatorService {
	private EtrCalculatorCoreService eteCalculatorCoreService;

	public void setEteCalculatorCoreService(
			EtrCalculatorCoreService eteCalculatorCoreService) {
		this.eteCalculatorCoreService = eteCalculatorCoreService;
	}

	@Path("activityPer")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse calculateActivityPercentage(
			EtrCalculatorPojo etrCalculatorPojo) {
		return eteCalculatorCoreService
				.calculateActivityPercentage(etrCalculatorPojo);

	}
}
