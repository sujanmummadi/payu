package com.cupola.fwmp.ws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.userGroup.UserGroupCoreService;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserGroupVo;

@Path("usergroup")
public class UserGroupService
{

	final static Logger log = Logger.getLogger(UserGroupService.class);

	UserGroupCoreService userGroupCoreService;

	public void setUserGroupCoreService(UserGroupCoreService userGroupCoreService)
	{
		this.userGroupCoreService = userGroupCoreService;
	}
	
	@POST
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getUsers(UserFilter filter) {

		return userGroupCoreService.getUserGroups(filter);
	}
	
	@POST
	@Path("list/usergroupnames")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllUserGroupNames() {

		log.debug("get all usergroup");

		return userGroupCoreService.getAllGroupNames();
	}
	

	@POST
	@Path("list/roles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllRoles() {
		
		log.info("get all roles.......");
		return userGroupCoreService.getAllRoleNames();
		
	}
	
	@POST
	@Path("list/usergroup")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllUserGroups() {

		log.debug("get all users groups");
		return userGroupCoreService.getAllUserGroups();
	
	}
	
	@POST
	@Path("list/usergroup/editable")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getEditableGroupByCurrentUser() {

		log.debug("getEditableGroupByCurrentUser all users groups");
		return userGroupCoreService.getEditableGroupByCurrentUser();
	
	}
	
	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addNewUserGroup(UserGroupVo userGroupVo) {

		log.info("user userGroups:"+userGroupVo);

		return userGroupCoreService.addNewUserGroup(userGroupVo);
	}
	
	@POST
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateUserGroup(UserGroupVo userGroupVo) {

		log.info("updating userGroup:"+userGroupVo);

		return userGroupCoreService.updateUserGroup(userGroupVo);
	}
	
	@POST
	@Path("delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse deleteUserGroup(UserGroupVo userGroupVo) {

		log.info("deleting userGroup:"+userGroupVo);

		return userGroupCoreService.deleteUserGroup(userGroupVo);
	}
	
	@POST
	@Path("immediate/manager/list")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getImmediateManagerList()
	{
		return userGroupCoreService.getImmediateManagerList();
	}
}
