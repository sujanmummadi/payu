package com.cupola.fwmp.ws.fr;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.dao.fr.FrDeploymentSettingDao;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.dao.fr.vendor.SubTicketDAO;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.closeticket.MQClosureHelper;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrIntegrationCoreService;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.integ.crm.CrmServiceCall;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.fr.FRVendorVO;
import com.cupola.fwmp.vo.fr.FlowActionData;
import com.cupola.fwmp.vo.fr.FrDefectRequestVo;
import com.cupola.fwmp.vo.fr.FrEtrUpdateVo;
import com.cupola.fwmp.vo.fr.FrTicketClosureVo;
import com.cupola.fwmp.vo.fr.FrTicketPushBackVo;
import com.cupola.fwmp.vo.fr.FrUpdateRequestVO;
import com.cupola.fwmp.vo.fr.ShiftingRequestVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting.Last3ServiceImpactingResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting.ListOfFwmpqueryio;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting.ListOfServiceRequest_ACTPriotization;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting.QueryLast3SR_Output;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting.ServiceRequest_ACTPriotization;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.last3ServiceImpacting.Last3ServiceImpactingRequest;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.last3ServiceImpacting.QueryLast3SR_Input;
import com.sun.jersey.impl.ApiMessages;

@Path("frticket")
public class FrTicketDetailsService
{
	@Autowired
	private FrTicketService frTicketService;
	
	@Autowired
	SubTicketDAO frVendorDao;
	
	@Autowired
	private MQClosureHelper mqClosureHelper;
	
	@Autowired
	private FrDeploymentSettingDao frDeploymentSettingDao;
	
	@Autowired
	private CrmServiceCall crmServiceCall;
	
	@Autowired
	DBUtil dbUtil;
	
	@POST
	@Path("details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getFrTicketDetails(TicketFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(frTicketService
						.getFrTicketDetails(filter));
	}
	
	@POST
	@Path("search")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse searchFrTicket(TicketFilter filter)
	{
		return frTicketService
				.searchFrTicketDetailsBy(filter);
	}
	
	@GET
	@Path("/ne")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getAllFrTicketByEngineer()
	{
		return null;
	}
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getAllFrTicket()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(frTicketService.getAllFrTicket());
	}
	

	@GET
	@Path("/symptomcategory")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getSymptomNames()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(FrTicketUtil.getFrSymptomCategories());
	}
	
	@GET
	@Path("/allsymptoms/{flowId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSymptomNamesByFlowId(@PathParam("flowId") Long flowId)
	{
		return frDeploymentSettingDao.getAllFrSymptomNameByFlowId(flowId);
	}
	
	@POST
	@Path("/update/flowId")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateFrTicketFlowId(FrUpdateRequestVO updateRequestVo)
	{
		if (updateRequestVo != null)
		{
			if( updateRequestVo.getFlowId() != null && updateRequestVo.getTicketId() != null)
			{	
				frTicketService.updateFlowId(updateRequestVo.getTicketId(),
					updateRequestVo.getSymptomName(),updateRequestVo.getFlowId());
				
				String currentCRM = dbUtil.getCurrentCrmName(null, null);

				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
					
					frTicketService.updateFaltRepairActivityInCRM(updateRequestVo.getTicketId(), null, null, null, null);
				}

				return ResponseUtil.createUpdateSuccessResponse();
			}
			return ResponseUtil.createUpdateFailedResponse();
		}
		return ResponseUtil.createUpdateFailedResponse();
	}
	
	@POST
	@Path("/updatelockstatus/{userId}/{ticketId}/{lockstatus}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse recheckActivation(@PathParam("userId") long userId, 
			@PathParam("ticketId") long ticketId, @PathParam("lockstatus")  boolean lockStatus)
	{

		return frTicketService.updateTicketLock(userId,ticketId, lockStatus);

	}
	
	

	@POST
	@Path("/submit/action")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse submitFrActionData(FlowActionData actionData)
	{
		APIResponse apiResponse = frTicketService.submitFrActionData(actionData);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			frTicketService.updateFaltRepairActivityInCRM(Long.valueOf(actionData.getTicketId()), null, null, null, null);
		}

		return apiResponse;

	}
	
	@POST
	@Path("/shifting/permission")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getShiftingPermission( ShiftingRequestVo actionData)
	{
		if( actionData == null )
			return ResponseUtil.nullArgument();
		
		APIResponse apiResponse = frTicketService.getShiftingPermission(actionData);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			frTicketService.updateFaltRepairActivityInCRM(Long.valueOf(actionData.getTicketId()), null, null, null, null);
		}
				
		return apiResponse;

	}
	
	@POST
	@Path("/request/billingteam")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse tlRaiseSubTicketToBillingTean( ShiftingRequestVo actionData)
	{
		if( actionData == null )
			return ResponseUtil.nullArgument();
		
		APIResponse apiResponse = frTicketService.tlRaiseSubTicketToBillingTeam(actionData);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			frTicketService.updateFaltRepairActivityInCRM(Long.valueOf(actionData.getTicketId()), null, null, null, null);
		}
		
		return apiResponse;

	}
	
	@POST
	@Path("/etr/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateFrTicketEtrByNeOrTl(FrEtrUpdateVo updateRequest)
	{
		if( updateRequest == null  )
			return ResponseUtil.nullArgument();
		
		APIResponse apiResponse = frTicketService.updateFrTicketEtrByNeOrTl(updateRequest);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			frTicketService.updateFaltRepairActivityInCRM(Long.valueOf(updateRequest.getTicketId()), null, null, null, null);
		}
		
		
		return apiResponse;
	}

	@POST
	@Path("/etr/update/bulk")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateMultiFrTicketEtrByNeOrTl(List<FrEtrUpdateVo> updateRequest)
	{
		if( updateRequest == null || updateRequest.isEmpty())
			return ResponseUtil.nullArgument();
		
		return frTicketService.updateFrTicketEtrApp(updateRequest);
	}
	
	@POST
	@Path("/flow/defectcodes")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getAllDefectSubDefectCode()
	{
		return frTicketService.getDefectSubDefectCode();
	}

	@POST
	@Path("/bulk/close")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse closeFrTicketInBulk(FrTicketClosureVo ticketTobeClosed)
	{
		if( ticketTobeClosed == null 
				|| ticketTobeClosed.getTicketIds() == null 
				|| ticketTobeClosed.getTicketIds().size() <= 0 )
			
		return ResponseUtil.createTicketClosureFailedInMQ().setData("failed");
		
		else
		{
			FRDefectSubDefectsVO closeVo = null;
			for( Long ticketId : ticketTobeClosed.getTicketIds() )
			{
				closeVo = new FRDefectSubDefectsVO();
				closeVo.setTicketId(ticketId);
				closeVo.setDefectCode(ticketTobeClosed.getDefectCode());
				closeVo.setSubDefectCode(ticketTobeClosed.getSubDefectCode());
//				closeVo.setRemarks(AuthUtils.getCurrentUserDisplayName());
				closeVo.setModifiedBy(AuthUtils.getCurrentUserId());
				List<FRVendorVO> subTicketsForRelatedMainTicket =frVendorDao.getVendorsForTicket(ticketId);
				for (FRVendorVO frVendorVO : subTicketsForRelatedMainTicket)
				{
					if(ActionTypeAttributeDefinitions.FR_ACTION_PENDING_STATUS_LIST.contains( frVendorVO.getStatus().longValue()))
					{
					 frVendorDao.updateTicketStatus(ticketId, ActionTypeAttributeDefinitions.FR_ACTION_PENDING_STATUS_MAP.get( frVendorVO.getStatus().longValue()).intValue());
						
					}
				
				}				
					mqClosureHelper.handleTicket(closeVo);				
				
			}
			return ResponseUtil.createTicketClosureSuccessInMQ();
		}
	}
	
	@POST
	@Path("/permission/{ticketId}/{status}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse managePermission(@PathParam("ticketId") Long ticketId ,@PathParam("status") Long status)
	{
		if( status == null || ticketId == null
				|| ticketId <= 0 )
			
			return ResponseUtil.nullArgument();
		
		APIResponse apiResponse = frTicketService.managePermission( ticketId ,status );
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			frTicketService.updateFaltRepairActivityInCRM(ticketId, null, null, null, null);
		}
		
		return apiResponse;
	}
	@POST
	@Path("/reportingUserList/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getAllTlForUser(@PathParam("userId") Long userId)
	{
		
		if(userId == -1)
		{
			userId =	AuthUtils.getCurrentUserId();
		}
		return ResponseUtil.createSuccessResponse().setData(frTicketService.getAllTlForUser(userId));
	}
	
	@POST
	@Path("/reportingTlList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getAllReportingTlList()
	{
		return ResponseUtil.createSuccessResponse().setData(frTicketService.getAllReportingTlList(AuthUtils.getCurrentUserId()));
	}
	
	@POST
	@Path("/pushback")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse pushBackNeTicket(FrTicketPushBackVo pushBackVo)
	{
		if( pushBackVo == null )
			return ResponseUtil.nullArgument();
		else
		{
			APIResponse apiResponse = frTicketService.pushBackFrTicketForUser( pushBackVo );
			 
			 String currentCRM = dbUtil.getCurrentCrmName(null, null);

				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
					
					frTicketService.updateFaltRepairActivityInCRM(Long.valueOf(pushBackVo.getTicketId()), null, null, null, null);
				}
			 
			
			return apiResponse;
		}
			
	}
	
	@POST
	@Path("/defect/subdefect/createorrefresh")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse createDefectSubDefect(FrDefectRequestVo requestVo)
	{
		if( requestVo == null || requestVo.getAddOrRefreshOption() == null )
			return ResponseUtil.nullArgument().setData("Provide add or refresh option...");
		return frTicketService.createDefectSubDefectWithMQClosureCode(
				requestVo.getFileName(), requestVo.getFilePath(), requestVo.getAddOrRefreshOption());
	}
	
	@POST
	@Path("/reopen/{symptom}/{workOrderNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse reopenFrTicket( @PathParam("workOrderNumber") Long workOrderNumber,
			@PathParam("symptom") String symptom)
	{
		if( workOrderNumber == null || workOrderNumber.longValue() <=0 )
			return ResponseUtil.nullArgument();
		else
			{
				 MQWorkorderVO workOrder = new MQWorkorderVO();
				 workOrder.setWorkOrderNumber(workOrderNumber+"");
				 workOrder.setSymptom(symptom);
				 return frTicketService.reopenFrWorkOrder(workOrder);
			}
	}
	
	@POST
	@Path("/qdata/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getFrUserQData( @PathParam("userId") Long userId )
	{
		if( userId == null || userId.longValue() <= 0 )
			return ResponseUtil.nullArgument().setData("Invalid user Id : "+userId);
		
		else
			return FrTicketUtil.getFrUserQData( userId );
	}
	
	@POST
	@Path("/last3services/{customerId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getLast3Services( @PathParam("customerId") String customerId )
	{
		
//		return ResponseUtil.createSuccessResponse().setData(getLast3CommomData());
		
		if( customerId != null && !customerId.isEmpty()){
			QueryLast3SR_Input queryLast3SR_Input = new QueryLast3SR_Input();
			queryLast3SR_Input.setCustomerNo(customerId);
			Last3ServiceImpactingRequest serviceImpactingRequest = new Last3ServiceImpactingRequest();
			serviceImpactingRequest.setQueryLast3SR_Input(queryLast3SR_Input);
			
			return crmServiceCall.callSiebelServiceForLast3ServiceImpactingTickets(serviceImpactingRequest);
			
		}else{
			return ResponseUtil.createNullParameterResponse().setData("Customer Id is null");
		}
		
	}
	
	/**
	 * Implemented for local hard-coded response generated if siebel is not up
	 * @author noor
	 * Last3ServiceImpactingResponseFromSiebel
	 * @return
	 */
	private  Last3ServiceImpactingResponseFromSiebel getLast3CommomData(){
		
		Last3ServiceImpactingResponseFromSiebel fromSiebel = new Last3ServiceImpactingResponseFromSiebel();
		QueryLast3SR_Output last3sr_Output = new QueryLast3SR_Output();
		last3sr_Output.setError_spcCode("123");
		last3sr_Output.setError_spcMessage("ABC");
		
		List<ListOfFwmpqueryio> fwmpqueryios = new ArrayList<>();
		fwmpqueryios.add(getListOfFwmpQ("ABC"));
		fwmpqueryios.add(getListOfFwmpQ("Mounika"));
		last3sr_Output.setListOfFwmpqueryio(fwmpqueryios);
		
		fromSiebel.setQueryLast3SR_Output(last3sr_Output);
		
		return fromSiebel;
	}
	
	private ListOfFwmpqueryio getListOfFwmpQ(String ownerName)
	{
		ListOfFwmpqueryio listofFwmp = new ListOfFwmpqueryio();
		listofFwmp.setCategory("ABCdasfkljsdklfjlkasdjflkjadsABCdasfkljsdklfjlkasdjflkjadsABCdasfkljsdklfjlkasdjflkjadsABCdasfkljsdklfjlkasdjflkjads");
		listofFwmp.setOwner(ownerName);
		listofFwmp.setClosedDate("ABCasdfvjhgfjkhadsfkjsdakjfABCdasfkljsdklfjlkasdjflkjads");
		listofFwmp.setETR("ABCasdfvjhgfjkhadsfkjsdakjfABCdasfkljsdklfjlkasdjflkjads");
		List<ListOfServiceRequest_ACTPriotization> actPriotizationList = new ArrayList<>();
		ListOfServiceRequest_ACTPriotization actPriotization = new ListOfServiceRequest_ACTPriotization();
		ServiceRequest_ACTPriotization srActPriotization = new ServiceRequest_ACTPriotization();
		srActPriotization.setACTPrioritization("MONIKAMONIKAMONIKAMONIKAMONIKAMONIKAMONIKA");
		actPriotization.setServiceRequest_ACTPriotization(srActPriotization);
		actPriotizationList.add(actPriotization);
		listofFwmp.setListOfServiceRequest_ACTPriotization(actPriotizationList);
		listofFwmp.setNature("ABCasdfvjhgfjkhadsfkjsdakjfABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setPriority("ABCasdfvABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setResolutionCode("ABCasdfvjhgfjkhadsfkjsdakjfABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setResolutionDescription("ABCasdfvjhgfjkhadsfkjsdakjfABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setSRNumber("ABCasdfvjABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setStatus("ABCasdfvjhgfjkhadsfkjsdakjfABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setSubResolutionCode("ABCasdfvjhgfjkhadsfkjsdakjfABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setTicketDescription("ABCasdfvjhgfjkhadsfkjsdakjfABCasdfvjhgfjkhadsfkjsdakjf");
		listofFwmp.setVOC("ABCasdfvjhgfjkhadsfkjsdakjfABCasdfvjhgfjkhadsfkjsdakjfC");
		return listofFwmp;
	}
	
}
