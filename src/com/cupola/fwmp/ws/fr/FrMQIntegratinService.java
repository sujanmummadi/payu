package com.cupola.fwmp.ws.fr;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.fr.FrIntegrationCoreService;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.fr.FrIntegrationVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;

@Path("frmq/inte")
public class FrMQIntegratinService
{
	@Autowired
	private FrIntegrationCoreService actForceService;
	
	@POST
	@Path("/getdevicedata")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getDeviceData(FrIntegrationVo pushBackVo)
	{
		if( pushBackVo == null )
			return ResponseUtil.nullArgument();
		else
			return actForceService.getDeviceInfo(pushBackVo);
	}
	
	@GET
	@Path("/update/etr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getEtrInMq()
	{
		TicketModifyInMQVo etrVo = new TicketModifyInMQVo();
		/*etrVo.setWorkOrderNumber("1028766635");
		etrVo.setEtrValue(GenericUtil.convertToUiDateFormat(new Date()));
		etrVo.setReasonCode("Fiber puller not available...");
		etrVo.setRemarks("Fiber puller not available...");
		etrVo.setTicketId(1494225400850720l);*/
		return actForceService.updateEtrInMQ(etrVo);
	}
	
	@GET
	@Path("/update/statusorcomment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getStatusOrCommentInMq()
	{
		TicketModifyInMQVo etrVo = new TicketModifyInMQVo();
		/*etrVo.setWorkOrderNumber("1028792111");
		etrVo.setEtrValue(GenericUtil.convertToUiDateFormat(new Date()));
		etrVo.setReasonCode("Fiber puller not available...");
		etrVo.setRemarks("Fiber puller not available...");
		etrVo.setTicketId(1494225412753591l);*/
		return actForceService.updateCommentOrStatusInMQ(etrVo);
	}
}
