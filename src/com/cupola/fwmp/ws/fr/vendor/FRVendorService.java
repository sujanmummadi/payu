package com.cupola.fwmp.ws.fr.vendor;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.SubTickets;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.fr.vendor.FRVendorCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FrFlowRequestVo;
import com.cupola.fwmp.vo.fr.VendorNEAssignVO;
import com.cupola.fwmp.vo.fr.VendorTicketUpdateVO;

@Path("fr/vendor")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FRVendorService
{
	@Autowired
	FRVendorCoreService frVendorCoreService;
	
	@Autowired
	private FrTicketService frTicketService;

	@GET
	@Path("associated/{skillType}")
	public APIResponse getAssociatedVendors(
			@PathParam("skillType") Long skillType)
	{
		FRVendorFilter filter = new FRVendorFilter();
		if (AuthUtils.isFRTLUser())
			filter.setReportToUserId(AuthUtils.getCurrentUserId());
		else
			filter.setAssocatedToUserId(AuthUtils.getCurrentUserId());
		filter.setSkillId(skillType);
		return frVendorCoreService.getVendors(filter);

	}
	
	@GET
	@Path("associated/withdetails/{skillType}")
	public APIResponse getAssociatedVendorsWithDetails(
			@PathParam("skillType") Long skillType)
	{
		FRVendorFilter filter = new FRVendorFilter();
		if (AuthUtils.isFRTLUser())
			filter.setReportToUserId(AuthUtils.getCurrentUserId());
		else
			filter.setAssocatedToUserId(AuthUtils.getCurrentUserId());
		filter.setSkillId(skillType);
		return frVendorCoreService.getVendorsWithDetails(filter);

	}


	@GET
	@Path("skills")
	public APIResponse getAllSkills()
	{
		return frVendorCoreService.getAllSkills();

	}

	@GET
	@Path("schedule/{vendorId}")
	public APIResponse getVendorSchedule(@PathParam("vendorId") Long vendorId)
	{
		return frVendorCoreService.getVendorUserSchedule(vendorId);

	}

	@POST
	@Path("update/estimatedtime")
	public APIResponse updateEstimatedTimeToTicketByVendor(
			SubTickets ticketData)
	{
		return frVendorCoreService
				.updateEstimatedTimeToTicketByVendor(ticketData);
	}

	@POST
	@Path("update/actualtime")
	public APIResponse updateActualTimeToTicketByVendor(SubTickets ticketData)
	{
		return frVendorCoreService.updateActualTimeToTicketByVendor(ticketData);
	}

	@POST
	@Path("assign/ticket")
	public APIResponse assignTicketToVendor(SubTickets vendorTicket)
			
	{
//		return frVendorCoreService.assignTicketToVendor(vendorTicket,null);
		
		/*
		 * if ( vendorTicket == null) return ResponseUtil.nullArgument();
		 * 
		 * FrFlowRequestVo requestVo = new FrFlowRequestVo();
		 * requestVo.setActionType(0); 
		 * return frVendorCoreService.createSubTicketTo(requestVo);
		 */
		
		return null;
	}

	@POST
	@Path("assign/ticket/{layoutId}")
	public APIResponse assignTicketToVendorByLayout(SubTickets vendorTicket,@PathParam("layoutId")Long layoutId)
			
	{
//		return frVendorCoreService.assignTicketToVendor(vendorTicket,layoutId);
		
		if ( vendorTicket == null) 
			return ResponseUtil.nullArgument();
		
		FrFlowRequestVo requestVo = new FrFlowRequestVo();
		requestVo.setActionType(layoutId);
		requestVo.setTicketId(vendorTicket.getTicketId());
		requestVo.setStatus(vendorTicket.getStatus());
		requestVo.setUserId(vendorTicket.getUserId());
		
		return frVendorCoreService.createSubTicketTo(requestVo);
	}
	
	@POST
	@Path("associate/nes")
	public APIResponse associateVendorUserToNE(
			List<VendorNEAssignVO> vendorNEAssignVO)
	{
		return frVendorCoreService
				.associateVendorUserToMultipleNE(vendorNEAssignVO);
	}

	@POST
	@Path("ticket/status/{ticketId}/{status}")
	public APIResponse updateTicketStatus(@PathParam("ticketId") long ticketId,
			@PathParam("status") int status)
	{
		return frVendorCoreService.updateTicketStatus(ticketId, status);
	}

	@POST
	@Path("reporting/{userId}")
	public APIResponse getReportingVendors(@PathParam("userId") Long userId)
	{
		FRVendorFilter filter = new FRVendorFilter();
		/**
		 * @author kiran
		 *  from ui tl id is getting means it will use user id if it comes null means it will take current userId
		 */
		if(userId != null && userId  > 0)
			filter.setReportToUserId(userId);
		else
		    filter.setReportToUserId(AuthUtils.getCurrentUserId());
		return frVendorCoreService.getReportingVendors(filter);
	}

	@POST
	@Path("allFrnes/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllFrEngineers(@PathParam("userId") Long userId)
	{
		return frVendorCoreService.getAllTlEngineers(userId);
	}
	
	
	@POST
	@Path("ticket/update")
	public	APIResponse assignOrUpdateVendorTicket(VendorTicketUpdateVO vo)
	{
		return frVendorCoreService.assignOrUpdateVendorTicket(vo);

	}
	
	@POST
	@Path("details")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getFrVendorTicketDetails(FRVendorFilter filter)
	{
		return frTicketService
						.getFrVendorTicket(filter);
	}
	
	@POST
	@Path("ticket/summary")
	public	APIResponse getVendorTicketSummary()
	{
		return frVendorCoreService.getVendorTicketSummary();
	}
	
	 
}
