package com.cupola.fwmp.ws.fr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FrReportConstant;
import com.cupola.fwmp.dao.reports.fr.FrReportDao;
import com.cupola.fwmp.reports.jasper.JasperManager;
import com.cupola.fwmp.reports.util.NamingConstants;
import com.cupola.fwmp.reports.util.PropLoader;
import com.cupola.fwmp.reports.vo.CafVerficationReport;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.reports.caf.CafReportService;
import com.cupola.fwmp.service.reports.fr.FrAllocationReport;
import com.cupola.fwmp.service.reports.fr.FrClosedComplaintsReport;
import com.cupola.fwmp.service.reports.fr.FrEtrReport;
import com.cupola.fwmp.service.reports.fr.FrNewLightWeight;
import com.cupola.fwmp.service.reports.fr.FrProductivityReport;
import com.cupola.fwmp.service.reports.fr.FrReAssignmentHistoryReport;
import com.cupola.fwmp.service.reports.fr.FrReOpenHistoryReport;
import com.cupola.fwmp.service.reports.fr.FrReport;
import com.cupola.fwmp.service.reports.fr.FrReportService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.google.gson.Gson;

@Path("frreport")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FrReportServices 
{
	@Autowired
	private FrReportService frReportService;
	
	@Autowired
	private FrReportDao frReportDao;
	
	 @Context
	    HttpServletRequest request;
		 
	    @Context
	    HttpServletResponse response;
	    
	    @Context
	    SecurityContext securityContext;
	    
	    public static Logger log = LogManager.getLogger(FrReportServices.class);
	    private static String JRXML_HOST_PATH = "jrxml.path";
	
	@GET
	@Path("etrbytl")
	public APIResponse  getFrEtrReportByTl() throws IOException
	{
		
		log.info("Inside getFrEtrReportByTl %%%%%%%%%%%%%%%%%%%%%%%%");
		
		List<FrEtrReport> frReportList = frReportService.getFrEtrReportByTlNew(AuthUtils.getCurrentUserId());
		
		log.info("List size of etrtl : "+frReportList.size());
		 
		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.ETRREPORTJRXML + ".jrxml";
            //log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :"+e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = "EtrTemplateTL.xls";

		String fullPath = appPath + fileName;
		//log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "EtrTemplateTL" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			outStream.close();
			}
		
		return null;
		
		
	}
	
	@GET
	@Path("etrbyam")
	public APIResponse  getFrEtrReportByAm() throws IOException
	{

		log.info("Inside getFrEtrReportByAm %%%%%%%%%%%%%%%%%%%%%%%%");
		
		List<FrEtrReport> frReportList = frReportService.getFrEtrReportByAMNew(AuthUtils.getCurrentUserId());
		
		log.info("List size of etram : "+frReportList.size());
		 
		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.ETRREPORTJRXML + ".jrxml";
            //log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :"+e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = "EtrTemplateAM.xls";

		String fullPath = appPath + fileName;
		//log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "EtrTemplateAM" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			outStream.close();
			}
		
		return null;
		 
	}
	
	 
	
	@GET
	@Path("productivitybytl")
	public APIResponse  getFrProductivityReportByTl() throws IOException
	{
		 
		log.info("Inside getFrProductivityReportByTl %%%%%%%%%%%%%%%%%%%%%%%%");
		
		List<FrProductivityReport> frReportList = frReportService.getFrProductivityReportByTlNew(AuthUtils.getCurrentUserId());
		
		log.info("List size of productivitybytl : "+frReportList.size());
		 
		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.PRODUCTIVITYREPORTJRXML + ".jrxml";
            //log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :"+e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = "ProductivityTemplateTL.xls";

		String fullPath = appPath + fileName;
		//log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "ProductivityTemplateTL" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			outStream.close();
			}
		
		return null;
	}
	
	@GET
	@Path("productivitybyam")
	public APIResponse  getFrProductivityReportByAm() throws IOException
	{
		 
	 log.info("Inside getFrProductivityReportByAm %%%%%%%%%%%%%%%%%%%%%%%%");
		
		List<FrProductivityReport> frReportList = frReportService.getFrProductivityReportByAmNew(AuthUtils.getCurrentUserId());
		
		log.info("List size productivitybyam : "+frReportList.size());
		 
		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.PRODUCTIVITYREPORTAMJRXML + ".jrxml";
            //log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :"+e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = "ProductivityTemplateAM.xls";

		String fullPath = appPath + fileName;
		//log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "ProductivityTemplateAM" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			outStream.close();
			}
		
		return null;
	 
	}
	
	
	@GET
	@Path("allocationbytl")
	public APIResponse  getFrAllocationReportByTl() throws IOException
      {
		log.info("Inside getFrAllocationReportByTl %%%%%%%%%%%%%%%%%%%%%%%%");

		List<FrAllocationReport> frReportList = frReportService
				.getFrAllocationReportByTl(AuthUtils.getCurrentUserId());

		log.info("List size of allocationbytl : " + frReportList.size());

		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.ALLOCATIONREPORTTLJRXML + ".jrxml";
			// log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :" + e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = "AllocationTemplateTL.xls";

		String fullPath = appPath + fileName;
		// log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "AllocationTemplateTL" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			outStream.close();
		}

		return null;

	}
	
	@GET
	@Path("allocationbyam")
	public APIResponse  getFrAllocationReportByAm() throws IOException
      {
		log.info("Inside getFrAllocationReportByAm %%%%%%%%%%%%%%%%%%%%%%%%");

		List<FrAllocationReport> frReportList = frReportService
				.getFrAllocationReportByAM(AuthUtils.getCurrentUserId());

		log.info("List size of allocationbyam : " + frReportList.size());

		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.ALLOCATIONREPORTAMJRXML + ".jrxml";
			// log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :" + e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = "AllocationTemplateAM.xls";

		String fullPath = appPath + fileName;
		// log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "AllocationTemplateAM" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			outStream.close();
		}

		return null;
		 
	}
	
	
	/*@GET
	@Path("closedcomplaints/{fromDate}/{toDate}/{branchId}/{areaId}")
	public APIResponse getFrClosedComplaintsReport(@PathParam("fromDate") String fromDate,@PathParam("toDate") String toDate,@PathParam("branchId") String branchId,@PathParam("areaId") String areaId) throws IOException {

		log.info("Inside getFrClosedComplaintsReport %%%%%%%%%%%%%%%%%%%%%%%%");
		
		log.info("@@@@@@@@@  fromDate(" +fromDate + "), toDate("+toDate+"),branchId("+branchId+"),areaId("+areaId+")");
		
		List<FrClosedComplaintsReport> frReportList = frReportDao.getFrClosedComplaintsReport(fromDate, toDate,branchId, areaId ,null);
		
		log.info("List size of ClosedComplaints : " + frReportList.size());

		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.CLOSEDCOMPLAINTSJRXML + ".jrxml";
			// log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :" + e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = "ClosedComplaintsTL.xls";

		String fullPath = appPath + fileName;
		// log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "ClosedComplaintsTL" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			outStream.close();
		}

		return null;

	}

	@GET
	@Path("netlightweight/{fromDate}/{toDate}/{branchId}/{areaId}")
	public APIResponse getFrNewLightWeightReport(@PathParam("fromDate") String fromDate,@PathParam("toDate") String toDate,@PathParam("branchId") String branchId,@PathParam("areaId") String areaId) throws IOException {

		log.info("Inside getFrNewLightWeightReport %%%%%%%%%%%%%%%%%%%%%%%%");

		log.info("@@@@@@@@@  fromDate(" +fromDate + "), toDate("+toDate+"),branchId("+branchId+"),areaId("+areaId+")");
		
		List<FrNewLightWeight> frReportList = frReportDao
				.getFrNewLightWeightReport(fromDate, toDate,branchId, areaId ,null);
		 
		log.info("List size of NewLightWeight : " + frReportList.size());

		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.NETLIGHTWEIGHTJRXML + ".jrxml";
			// log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :" + e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = " NetLightWeightTL.xls";

		String fullPath = appPath + fileName;
		// log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "NetLightWeightTL" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			outStream.close();
		}

		return null;

	}
	
	@GET
	@Path("reassignmenthistory/{fromDate}/{toDate}/{branchId}/{areaId}")
	public APIResponse getFrReassignmentHistoryReport(@PathParam("fromDate") String fromDate,@PathParam("toDate") String toDate,@PathParam("branchId") String branchId,@PathParam("areaId") String areaId) throws IOException {

		log.info("Inside getFrReassignmentHistoryReport %%%%%%%%%%%%%%%%%%%%%%%%");

		log.info("@@@@@@@@@  fromDate(" +fromDate + "), toDate("+toDate+"),branchId("+branchId+"),areaId("+areaId+")");
		
		List<FrReAssignmentHistoryReport> frReportList = frReportDao
				.getFrReassignmentHistoryReport(fromDate, toDate,branchId, areaId ,null);
		 
		log.info("List size of FrReassignmentHistoryReport : " + frReportList.size());

		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.REASSIGNMENTJRXML + ".jrxml";
			// log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :" + e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = " ReAssignmentHistory.xls";

		String fullPath = appPath + fileName;
		// log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "ReAssignmentHistory" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			outStream.close();
		}

		return null;

	}
	
	
	@GET
	@Path("reopenhistory/{fromDate}/{toDate}/{branchId}/{areaId}")
	public APIResponse getFrReopenHistoryReport(@PathParam("fromDate") String fromDate,@PathParam("toDate") String toDate,@PathParam("branchId") String branchId,@PathParam("areaId") String areaId) throws IOException {

		log.info("Inside getFrReopenHistoryReport %%%%%%%%%%%%%%%%%%%%%%%%");

		log.info("@@@@@@@@@  fromDate(" +fromDate + "), toDate("+toDate+"),branchId("+branchId+"),areaId("+areaId+")");
		
		List<FrReOpenHistoryReport> frReportList = frReportDao
				.getFrReopenHistoryReport(fromDate, toDate,branchId, areaId ,null);
		 
		log.info("List size of FrReopenHistoryReport : " + frReportList.size());

		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;

		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.REOPENJRXML + ".jrxml";
			// log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :" + e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = " ReopenHistory.xls";

		String fullPath = appPath + fileName;
		// log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "ReopenHistory" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			outStream.close();
		}

		return null;

	}*/
	
	@POST
	@Path("downtime/opentickets")
	public APIResponse  getDownTimeReportOnOpenTicket()
	{
		return ResponseUtil.createSaveSuccessResponse()
				.setData(frReportService.getDownTimeReportOnOpenTicket());
	}
}
