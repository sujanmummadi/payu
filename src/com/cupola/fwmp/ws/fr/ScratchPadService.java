/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.ws.fr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cupola.fwmp.dao.fr.ScratchPadDAO;
import com.cupola.fwmp.dao.fr.vendor.SubTicketDAO;
import com.cupola.fwmp.persistance.entities.ControlBlock;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.service.domain.TicketScratchPadHandler;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.fr.ActWorkingHoursVo;
import com.cupola.fwmp.vo.fr.Activity;
import com.cupola.fwmp.vo.fr.AndroidFlowCxDown;
import com.cupola.fwmp.vo.fr.Decision;
import com.cupola.fwmp.vo.fr.DefectCode;
import com.cupola.fwmp.vo.fr.ETRDetails;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FRVendorVO;
import com.cupola.fwmp.vo.fr.FlowData;
import com.cupola.fwmp.vo.fr.ScratchPadLog;
import com.cupola.fwmp.vo.fr.ScratchPadVo;
import com.cupola.fwmp.vo.fr.ScratchVo;
import com.cupola.fwmp.vo.fr.SubDefectCode;

/**
 * 
 * @author kiran
 *
 */
@Path("scratchPad")
public class ScratchPadService {
	
	@Autowired
	ScratchPadDAO scratchPadDAO;
	
	@Autowired
	SubTicketDAO frVendorDao;
	
	@Autowired
	FrTicketService frTicketService;
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	@Autowired
	TicketScratchPadHandler ticketScratchPadHandler;
	
	
	@GET
	@Path("/decisionList")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllDecisions()
	{
		
		return ResponseUtil.createSuccessResponse().setData(xformToListOfDecisions(scratchPadDAO.getAllDecisions()));
	}

	private List<Decision> xformToListOfDecisions(
			Map<Integer, Decision> allDecisions) {
		List<Decision> returnValue = new ArrayList<Decision>();
		if (allDecisions == null)
			return new ArrayList<Decision>();
		for (Entry<Integer, Decision> entry : allDecisions.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<Decision>(returnValue);
	}
	
	@GET
	@Path("/activityList")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllActivities()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfActivities(scratchPadDAO.getAllActivities()));
	}

	private List<Activity> xformToListOfActivities(Map<Integer, Activity> allActivities) {
		List<Activity> returnValue = new ArrayList<Activity>();
		if (allActivities == null)
			return new ArrayList<Activity>();
		for (Entry<Integer, Activity> entry : allActivities.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<Activity>(returnValue);
	}
	
	@GET
	@Path("/defectCodeList")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllDefectCodes()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfDefectCodes(scratchPadDAO.getAllDefectCodes()));
	}
	
	private List<DefectCode> xformToListOfDefectCodes(
			Map<Integer, DefectCode> allDefectCodes) {
		List<DefectCode> returnValue = new ArrayList<DefectCode>();
		if (allDefectCodes == null)
			return new ArrayList<DefectCode>();
		for (Entry<Integer, DefectCode> entry : allDefectCodes.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<DefectCode>(returnValue);
	}
	
	@GET
	@Path("/subDefectCodeList")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSubDefectCodes()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfSubDefectCodes(scratchPadDAO.getAllSubDefectCodes()));
	}
	

	private List<SubDefectCode> xformToListOfSubDefectCodes(
			Map<Integer, SubDefectCode> allSubDefectCodes) {
		List<SubDefectCode> returnValue = new ArrayList<SubDefectCode>();
		if (allSubDefectCodes == null)
			return new ArrayList<SubDefectCode>();
		for (Entry<Integer, SubDefectCode> entry : allSubDefectCodes.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<SubDefectCode>(returnValue);
	}

	@POST
	@Path("/spapi")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse spAPI(ScratchVo scratchVo)
	{
		if(scratchVo != null)
		{
			scratchVo.setAddedBy(AuthUtils.getCurrentUserId());
			ticketScratchPadHandler.handleScratchPad(scratchVo);
			return ResponseUtil.createSuccessResponse();
		}
		else
		return ResponseUtil.createFailureResponse();
	}
	@POST
	@Path("/splog/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getScratchPadByTicketId(@PathParam("ticketId") long ticketId)
	{/*
		if(ticketId > 0)
		{*/
			List<ScratchPadLog> resut = scratchPadDAO.getScratchPadByTicketId(ticketId);
			if(resut != null)
			{
				return ResponseUtil.createSuccessResponse().setData(resut);
			}
			else {
				return ResponseUtil.createFailureResponse();
			}
		/*}else
		return ResponseUtil.createFailureResponse();*/
	}
	
	@GET
	@Path("/checkVersion")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getVersionDetails()
	{
		return scratchPadDAO.getVersionDetails();
	}
	
	@GET
	@Path("/etrList")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllETRDetails()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfETRDetails(scratchPadDAO.getAllETRDetails()));
	}

	private List<ETRDetails> xformToListOfETRDetails(
			Map<Integer, ETRDetails> allETRDetails) {
		List<ETRDetails> returnValue = new ArrayList<ETRDetails>();
		if (allETRDetails == null)
			return new ArrayList<ETRDetails>();
		for (Entry<Integer, ETRDetails> entry : allETRDetails.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<ETRDetails>(returnValue);
	}
	
	@GET
	@Path("/etrStepID/{stepID}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getETRDetailsByStepID(@PathParam("stepID") int stepID)
	{
		return ResponseUtil.createSuccessResponse().setData(scratchPadDAO.getETRDetailsByStepID(stepID));
	}
	
	@GET
	@Path("/resetCache")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse resetScratchPadCache()
	{
		return scratchPadDAO.resetScratchPadCache();
	}
	
	@GET
	@Path("/cxdownflow")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllCxDownAndroidFlow()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfCxDownFlowDetails(scratchPadDAO.getAllCxDownAndroidFlow()));
	}

	private List<AndroidFlowCxDown> xformToListOfCxDownFlowDetails(
			Map<Integer, AndroidFlowCxDown> cxDownFlowDetails) {
		List<AndroidFlowCxDown> returnValue = new ArrayList<AndroidFlowCxDown>();
		if (cxDownFlowDetails == null)
			return new ArrayList<AndroidFlowCxDown>();
		for (Entry<Integer, AndroidFlowCxDown> entry : cxDownFlowDetails.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<AndroidFlowCxDown>(returnValue);
	}
	
	@GET
	@Path("/slowspeedflow")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSlowSpeedAndroidFlow()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfSlowSpeedFlowDetails(scratchPadDAO.getAllSlowSpeedAndroidFlow()));
	}

	private List<AndroidFlowCxDown> xformToListOfSlowSpeedFlowDetails(
			Map<Integer, AndroidFlowCxDown> allSlowSpeedAndroidFlow) {
		List<AndroidFlowCxDown> returnValue = new ArrayList<AndroidFlowCxDown>();
		if (allSlowSpeedAndroidFlow == null)
			return new ArrayList<AndroidFlowCxDown>();
		for (Entry<Integer, AndroidFlowCxDown> entry : allSlowSpeedAndroidFlow.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<AndroidFlowCxDown>(returnValue);
	}
	
	@GET
	@Path("/fdflow")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllFDAndroidFlow()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfFDFlowDetails(scratchPadDAO.getAllFDAndroidFlow()));
	}

	private List<AndroidFlowCxDown> xformToListOfFDFlowDetails(
			Map<Integer, AndroidFlowCxDown> allFDAndroidFlow) {
		List<AndroidFlowCxDown> returnValue = new ArrayList<AndroidFlowCxDown>();
		if (allFDAndroidFlow == null)
			return new ArrayList<AndroidFlowCxDown>();
		for (Entry<Integer, AndroidFlowCxDown> entry : allFDAndroidFlow.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<AndroidFlowCxDown>(returnValue);
	}

	@GET
	@Path("/cxupflow")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllCxUpAndroidFlow()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfCxUpFlowDetails(scratchPadDAO.getAllCxUpAndroidFlow()));
	}

	private List<AndroidFlowCxDown> xformToListOfCxUpFlowDetails(
			Map<Integer, AndroidFlowCxDown> allCxUpAndroidFlow) {
		List<AndroidFlowCxDown> returnValue = new ArrayList<AndroidFlowCxDown>();
		if (allCxUpAndroidFlow == null)
			return new ArrayList<AndroidFlowCxDown>();
		for (Entry<Integer, AndroidFlowCxDown> entry : allCxUpAndroidFlow.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<AndroidFlowCxDown>(returnValue);
	}
	
	@POST
	@Path("/contextrule/{ticketId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getContextRuleByTicketId(@PathParam("ticketId") long ticketId)
	{
			List<ScratchPadVo> resut = scratchPadDAO.getContextRuleByTicketId(ticketId);
			if(resut != null)
			{
				return ResponseUtil.createSuccessResponse().setData(resut);
			}
			else {
				return ResponseUtil.createFailureResponse();
			}
		
	}
	
	@GET
	@Path("/srelementflow")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSeniorElementAndroidFlow()
	{
		return ResponseUtil.createSuccessResponse().setData(xformToListOfSeniorElementFlowDetails(scratchPadDAO.getAllSeniorElementAndroidFlow()));
	}

	private List<AndroidFlowCxDown> xformToListOfSeniorElementFlowDetails(
			Map<Integer, AndroidFlowCxDown> allSeniorElementAndroidFlow) {
		List<AndroidFlowCxDown> returnValue = new ArrayList<AndroidFlowCxDown>();
		if (allSeniorElementAndroidFlow == null)
			return new ArrayList<AndroidFlowCxDown>();
		for (Entry<Integer, AndroidFlowCxDown> entry : allSeniorElementAndroidFlow.entrySet())
		{
			returnValue.add(entry.getValue());
		}

		return new ArrayList<AndroidFlowCxDown>(returnValue);
	}
	
	@GET
	@Path("/flow")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllAndroidFlows()
	{
		FlowData flowData = new FlowData();
		flowData.setDecisionList((List<Decision>) getAllDecisions().getData());
		flowData.setActivityList( (List<Activity>) getAllActivities().getData());
		flowData.setDefectCodeList((List<DefectCode>) getAllDefectCodes().getData());
		flowData.setSubDefectCodeList((List<SubDefectCode>) getAllSubDefectCodes().getData());
		flowData.setEtrList((List<ETRDetails>) getAllETRDetails().getData());
		flowData.setCxDown((List<AndroidFlowCxDown>) getAllCxDownAndroidFlow().getData());
		flowData.setCxUp((List<AndroidFlowCxDown>) getAllCxUpAndroidFlow().getData());
		flowData.setSlowPeed((List<AndroidFlowCxDown>) getAllSlowSpeedAndroidFlow().getData());
		flowData.setFrequentDisconnection((List<AndroidFlowCxDown>) getAllFDAndroidFlow().getData());
		flowData.setSeniourElement((List<AndroidFlowCxDown>) getAllSeniorElementAndroidFlow().getData());
		flowData.setFiberTypes(CommonUtil.xformToTypeAheadFormat(DefinitionCoreServiceImpl.fiberTypes));
		flowData.setPushBackReasonCode(definitionCoreService.getFrBushBackReasonCode());
		FRVendorFilter filter = new FRVendorFilter();
		filter.setAssocatedToUserId(AuthUtils.getCurrentUserId());
		filter.setSkillId(0);
		List<FRVendorVO> results = frVendorDao.getVendorsWithDetails(filter);
		
		if (results == null || results.isEmpty())
			flowData.setVendorDetails(new ArrayList<FRVendorVO>());
			
		flowData.setVendorDetails(results);
		APIResponse response = frTicketService.getDefectSubDefectCode();
		
		flowData.setFlowDefectSubDefectCodes((Map<String, List<String>>) response.getData());
		
		ActWorkingHoursVo actWorkingHoursVo = new ActWorkingHoursVo();
		actWorkingHoursVo.setBusinessStartHour(Integer.valueOf(DefinitionCoreServiceImpl.workingHoursDef.get("act.business.start.hour")));
		actWorkingHoursVo.setBusinessEndHour(Integer.valueOf(DefinitionCoreServiceImpl.workingHoursDef.get("act.business.end.hour")));
		actWorkingHoursVo.setWorkingMinsPerDay(Integer.valueOf(DefinitionCoreServiceImpl.workingHoursDef.get("act.working.mins.per.day")));
		flowData.setActWorkingHoursVo(actWorkingHoursVo);
		
		flowData.setFlowWiseDefectAndSubDefectCodeData(frTicketService.getFrDeploymentSetting());
		
		return ResponseUtil.createSuccessResponse().setData(flowData);
	}



}
