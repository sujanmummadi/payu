package com.cupola.fwmp.ws.fr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.mongo.fr.TicketIncidentDao;
import com.cupola.fwmp.dao.mongo.vo.fr.AssetsVo;
import com.cupola.fwmp.dao.mongo.vo.fr.FlowRequestDataVo;
import com.cupola.fwmp.dao.mongo.vo.fr.TicketIncidentVo;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;

@Path("frflow")
public class FrIncidentReportService
{
	@Autowired
	TicketIncidentDao incidentDao;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private UserDao userDao;
	
	@POST
	@Path("/request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse submitFlowRequest(FlowRequestDataVo value)
	{
		incidentDao.saveOrUpdateFlowRequest(value);
		return ResponseUtil.createSuccessResponse();
	}
	
	@POST
	@Path("raise/incident")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse raiseIncident(TicketIncidentVo voObje)
	{
		incidentDao.createIncident(voObje);
		return ResponseUtil.createSuccessResponse();
	}

	@POST
	@Path("assign/assets")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse assignAssetsToUser(List<AssetsVo> assets)
	{
		incidentDao.assignAssets(assets);
		return ResponseUtil.createSuccessResponse();
	}
	
	@POST
	@Path("/assigned/assets")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getAllAssetsByAssignedBy(TicketFilter filter)
	{
		List<Long> reportToId = new ArrayList<Long>();
		if(filter != null )
		{
			if(filter.getAssignedByUserId()  !=  null && filter.getAssignedByUserId() > 0)
			{
				if(!userHelper.isFRTLUser(filter.getAssignedByUserId()) )
				{
					Map<Long, String> reportedUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(), null, null, null);
					for( Map.Entry<Long ,String> entry : reportedUsers.entrySet() )
					{
						if(userHelper.isFRTLUser(entry.getKey()))
						{
							reportToId.add(entry.getKey());
						}
					}
				}else {
					reportToId.add(filter.getAssignedByUserId() );
				}
			}
		}
		
		if(AuthUtils.isFRTLUser())
		{
			Long userId = AuthUtils.getCurrentUserId();
			 reportToId.add(userId);
		}
		return ResponseUtil.createSuccessResponse()
				.setData(incidentDao.getAllAssetsByAssignedUser(reportToId));
	}
}
