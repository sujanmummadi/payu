 /**
 * @Author snoor  Nov 27, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.ws.fr.configuration;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.persistance.entities.FlowType;
import com.cupola.fwmp.persistance.entities.FrDeploymentSetting;
import com.cupola.fwmp.persistance.entities.FrSymptomNames;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.fr.configuration.FRConfigurationService;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.BranchFlowConfigVO;
import com.cupola.fwmp.vo.FrSymptomCodesVo;

/**
 * @Author snoor  Nov 27, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Path("frConfig")
public class FRConfiguration {
	
	@Autowired
	private FRConfigurationService frConfigurationService;
	
	private static Logger log = Logger.getLogger(FRConfiguration.class.getName());
	
	@POST
	@Path("crmSetting/level/{mqLevel}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getMQLevelData(@PathParam("mqLevel") String mqLevel)
	{
		List<FrDeploymentSetting> deploymentSettings = frConfigurationService.getMQLevelData(mqLevel);
		if( deploymentSettings != null)
			return ResponseUtil.createSuccessResponse().setData(deploymentSettings);
		else
			return ResponseUtil.createInvalidReponse();
	}
	
	@POST
	@Path("crmSetting/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addCRMLevelData(FrDeploymentSetting deploymentSetting)
	{
		return frConfigurationService.addCRMData(deploymentSetting);
	}
	
	@POST
	@Path("crmSetting/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateCRMLevelData(FrDeploymentSetting deploymentSetting)
	{
		return frConfigurationService.updateCRMData(deploymentSetting);
	}
	
	@POST
	@Path("crmSetting/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse deleteCRMLevelData(FrDeploymentSetting deploymentSetting)
	{
		return frConfigurationService.deleteCRMData(deploymentSetting);
	}
	
	@POST
	@Path("branchFlow/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllbranchFlowConfigData()
	{
			List<BranchFlowConfigVO> branchFlowConfigVOs = frConfigurationService.getAllbranchFlowData();
			if( branchFlowConfigVOs != null)
				return ResponseUtil.createSuccessResponse().setData(branchFlowConfigVOs);
			else
				return ResponseUtil.createInvalidReponse();
	}
	
	@POST
	@Path("branchFlow/get/{cityId}/{branchId: .*}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getbranchFlowConfigData(@PathParam("cityId") String cityId, @PathParam("branchId") String branchId)
	{
		if( cityId == null && branchId == null) {
			return getAllbranchFlowConfigData();
		}
		else{
				List<BranchFlowConfigVO> branchFlowConfigVOs = frConfigurationService.getbranchFlowData(cityId,branchId);
				if( branchFlowConfigVOs != null)
					return ResponseUtil.createSuccessResponse().setData(branchFlowConfigVOs);
				else
					return ResponseUtil.createInvalidReponse();
		}
		
		
	}
	
	@POST
	@Path("branchFlow/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addbranchFlowConfigData(BranchFlowConfigVO branchFlowConfigVO)
	{
		return frConfigurationService.addbranchFlowData(branchFlowConfigVO);
	}
	
	@POST
	@Path("branchFlow/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updatebranchFlowConfigData(BranchFlowConfigVO branchFlowConfigVO)
	{
		return frConfigurationService.updatebranchFlowData(branchFlowConfigVO);
	}
	
	@POST
	@Path("branchFlow/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse deletebranchFlowData(BranchFlowConfigVO branchFlowConfigVO)
	{
		if( branchFlowConfigVO != null)
			return frConfigurationService.deletebranchFlowData(branchFlowConfigVO.getId());
		else
			return ResponseUtil.createNullParameterResponse();
	}
	
	@POST
	@Path("flowData/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllFlowTypes()
	{
			List<FlowType> flowTypes = frConfigurationService.getAllFlowTypeData();
			if( flowTypes != null)
				return ResponseUtil.createSuccessResponse().setData(flowTypes);
			else
				return ResponseUtil.createInvalidReponse();
	}
	
	@POST
	@Path("symptomData/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSymptomTypes()
	{
			List<FrSymptomCodesVo> frSymptomCodes = frConfigurationService.getAllSymptomTypes();
			if( frSymptomCodes != null)
				return ResponseUtil.createSuccessResponse().setData(frSymptomCodes);
			else
				return ResponseUtil.createInvalidReponse();
	}
	
	@POST
	@Path("symptomData/getAllSymptomNames")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSymptomNames()
	{
			List<FrSymptomNames> frSymptomCodes = frConfigurationService.getAllSymptomNames();
			if( frSymptomCodes != null)
				return ResponseUtil.createSuccessResponse().setData(frSymptomCodes);
			else
				return ResponseUtil.createInvalidReponse();
	}
	
	@POST
	@Path("symptomData/getSymptomForFlowId/{flowId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getSymptomForFlowId(@PathParam("flowId") long flowid)
	{
			List<FrSymptomNames> frSymptomCodes = frConfigurationService.getSymptomForFlowId(flowid);
			if( frSymptomCodes != null)
				return ResponseUtil.createSuccessResponse().setData(frSymptomCodes);
			else
				return ResponseUtil.createInvalidReponse();
	}
	
	@POST
	@Path("symptomData/getSymptomNamesForFlowId")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getSymptomNamesForFlowId()
	{
		Map<Integer, List<String>> frSymptomCodes = frConfigurationService.getSymptomNamesForFlowId();
			if( frSymptomCodes != null)
				return ResponseUtil.createSuccessResponse().setData(frSymptomCodes);
			else
				return ResponseUtil.createInvalidReponse();
	}
	
	@POST
	@Path("symptomData/updateSymptoms/{flowId}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse updateSymptomNames( @PathParam("flowId") Integer flowId, List<FrSymptomNames> symptomNames ) {
		return frConfigurationService.updateSymtoms(flowId, symptomNames);
	}
	
	@POST
	@Path("symptomData/addSymptom")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addSymptomName( @QueryParam("symptomName") String symptomName ) {
		return frConfigurationService.addSymptomName(symptomName);
	}
}
