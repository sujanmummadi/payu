package com.cupola.fwmp.ws.fr;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.ClassificationCategories;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.TypeAheadVo;

@Path("filters")
public class FilterService
{

	@Autowired
	DefinitionCoreService definitionCoreService;
	
	@Autowired
	private FrTicketService frService;

	@POST
	@Path("flow/options")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getFlowsOptions()
	{
		List<TypeAheadVo> list = CommonUtil
				.xformToTypeAheadFormat(FrTicketUtil.getFrFlowTypeForFilter());
		list.add(0, new TypeAheadVo(-1l, "All"));
		return ResponseUtil.createSuccessResponse().setData(list);
	}

	@POST
	@Path("naturecodes/options")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getNatureCodeOptions()
	{
//		return definitionCoreService.getAllfrNatureCodes();
		return frService.getAllFxByTlUser(AuthUtils.getCurrentUserId());
	}

	@POST
	@Path("symptoms/options")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllSymptomsName()
	{
		return ResponseUtil.createSuccessResponse().setData(
				FrTicketUtil.getAllFrSymptomsName());
	}

	@POST
	@Path("closure/remarks")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getfrClosureRemarks()
	{
		return definitionCoreService.getFrClosureRemarks();
	}

}
