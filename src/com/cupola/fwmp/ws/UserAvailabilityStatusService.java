package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.userAvailabilityStatus.UserAvailabilityStatusCoreService;
import com.cupola.fwmp.vo.UserAvailabilityStatusVo;

@Path("useravailability")
public class UserAvailabilityStatusService
{

	@Autowired
	UserAvailabilityStatusCoreService userAvailabilityStatusService;

	@POST
	@Path("/status")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addAndupdateUserAvailabilityStatus(
			UserAvailabilityStatusVo userStatus)
	{

		return userAvailabilityStatusService
				.addAndupdateUserAvailabilityStatus(userStatus);
	}

}
