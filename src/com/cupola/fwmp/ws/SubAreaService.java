package com.cupola.fwmp.ws;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.TypeAheadVo;

@Path("subarea")
public class SubAreaService {

	private com.cupola.fwmp.service.location.subArea.SubAreaService subAreaService;

	public void setSubAreaService(com.cupola.fwmp.service.location.subArea.SubAreaService subAreaService) {
		this.subAreaService = subAreaService;
	}

	@POST
	@Path("list/{areaid}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getSubAreasByAreaId(@PathParam("areaid") Long areaId) {

//		System.out.println("Test dkljfkdsnfdskjfdkjsjf areaId" + areaId);

		return subAreaService.getSubAreasByAreaId(areaId);
	}

	@POST
	@Path("app/list/{areaid}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getSubAreasByAreaIdForApp(@PathParam("areaid") String areaid) {
		
//		System.out.println("afkjdshfkjdshf ##########$444444444444444 " + areaid);
		
		if ("null".equalsIgnoreCase(areaid) || areaid.isEmpty() || areaid.equalsIgnoreCase(null)) {

			return ResponseUtil.createNullParameterResponse();

		} else {
			
			Long aId = Long.valueOf(areaid);
			return ResponseUtil.createSuccessResponse().setData(subAreaService.getSubAreasByAreaId(aId));
		}
	}

}
