package com.cupola.fwmp.ws.dashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FWMPConstant.PriorityFilter;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.counts.dashboard.CountRequestVO;
import com.cupola.fwmp.vo.counts.dashboard.FRDashBoardCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRSummaryCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRUserSummaryVO;

@Path("dashboard")
public class DashBoardService
{

	@Autowired
	TicketCoreService ticketService;
	
	@Autowired
	private FrTicketService  frTicketSercice;
	
	@Autowired
	DefinitionCoreService definitionCoreService;
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("summary/{ticketType}")
	public APIResponse getSummaryCounts(@PathParam("ticketType") Long ticketType)
	{

		return ticketService.calculateDashBoardStatus(ticketType);

		// return getDummySummary();

	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("summary/details/{ticketType}")
	public APIResponse getSummaryCountsWithDetails(@PathParam("ticketType") Long ticketType,TicketFilter filter)
	{
		if(filter != null)
			ApplicationUtils.saveTicketFilter(filter);

		return ticketService.calculateDashBoardStatus(ticketType);

		// return getDummySummary();

	}

	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("summary/fr")
	public APIResponse getFRSummaryCounts(CountRequestVO countVo)
	{
		countVo = null;
		
		if ( countVo == null )
			countVo = new CountRequestVO(false, 0l);
		
		return ResponseUtil.createSuccessResponse().
				setData(frTicketSercice.getFrSummaryCounts(countVo));

	}

	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("summary/fr/details")
	public APIResponse getFRSummaryCounts(TicketFilter filter)
	{
		CountRequestVO countVo = null;
		if(filter != null)
			ApplicationUtils.saveTicketFilter(filter);
		if ( countVo == null )
			countVo = new CountRequestVO(true, 0l);
		
		return ResponseUtil.createSuccessResponse().
				setData(frTicketSercice.getFrSummaryCounts(countVo));

	}

	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("summary/fr/tickets")
	public APIResponse getFrSummaryCounts()
	{
		return ResponseUtil.createSuccessResponse().setData(frTicketSercice.getFrUserSummaryCount());
	}
	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("summary/fr/tickets/ui")
	public APIResponse getFrSummaryCountsForUI(TicketFilter filter)
	{
		if(filter != null)
		{
			ApplicationUtils.saveTicketFilter(filter);
		}
		return ResponseUtil.createSuccessResponse().setData(frTicketSercice.getFrUserSummaryCount());
	}
	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("summary/fr/nedetails")
	public APIResponse getFrTlneDetailCounts()
	{
		if(AuthUtils.isFRCXDownNEUser())
			return	ResponseUtil.createSuccessResponse()
			.setData(frTicketSercice.getTlneDetailCounts().getCxDownCountSummary().get(0));
		else if (AuthUtils.isFRCxUpNEUser())
			return	ResponseUtil.createSuccessResponse()
			.setData(frTicketSercice.getTlneDetailCounts().getCxUpCountSummary().get(0));
		
		else if (AuthUtils.isFRSRNEUser())
		{
			return	ResponseUtil.createSuccessResponse()
					.setData(frTicketSercice.getTlneDetailCounts().getSrElementUserDetails().get(0));
		}
		
		return ResponseUtil.createSuccessResponse()
				.setData(frTicketSercice.getTlneDetailCounts());
	}
	
	public APIResponse getDummySummary(Long flowType)
	{
//		FRDashBoardCountVO vo = new FRDashBoardCountVO();
//		createDummySummary(vo,flowType );
		List<FRSummaryCountVO> summary = new ArrayList<>();
		createSummaryVo(summary);
		return ResponseUtil.createSuccessResponse().setData(summary);
	}

	Random r = new Random();
	int Low = 10;
	int High = 100;

	private void createDummySummary(FRDashBoardCountVO vo,Long flowType)
	{
		List<FRSummaryCountVO> countSummary = new CopyOnWriteArrayList<FRSummaryCountVO>(); // left
																					// status
		List<FRUserSummaryVO> cxUpUserSummaryCount = new CopyOnWriteArrayList<FRUserSummaryVO>();
		List<FRUserSummaryVO> cxDownUserSummaryCount = new CopyOnWriteArrayList<FRUserSummaryVO>();

		vo.setCountSummary(countSummary);
		// vo.setCxDownuserCount(cxDownuserCount);
		// vo.setCxUpuserCount(cxUpuserCount);

		createSummaryVo(countSummary);
		if(flowType == 2 )
			createUserSummaryVo(cxUpUserSummaryCount, "CxUp");
		else	if(flowType == 1  )
			createUserSummaryVo(cxDownUserSummaryCount, "CxDown");
		else	
		{
			createUserSummaryVo(cxUpUserSummaryCount, "CxUp");
			createUserSummaryVo(cxDownUserSummaryCount, "CxDown");
		}
//		vo.setCxDownUserSummaryCount(cxDownUserSummaryCount);
//		vo.setCxUpUserSummaryCount(cxUpUserSummaryCount);
		
	
		
		
		
	}
	

	private void createUserSummaryVo(List<FRUserSummaryVO> summary,
			String neSuffix)
	{ 

		for (int i = 18; i < 20; i++)
		{
			FRUserSummaryVO vo = new FRUserSummaryVO();
			
			vo.setUserId(i*10l);
			vo.setUserName("NE_"+i);
			
			Map<String, FRSummaryCountVO> countSummary = new HashMap<String, FRSummaryCountVO>();
			vo.setCountSummary(countSummary); 
			Set<String> symptoms = null;
			if("Priority".equals(neSuffix))
				{
				symptoms = new HashSet<String>( PriorityFilter.definition.values());
				symptoms.remove("All");
				}
			else
				 symptoms = "CxUp".equals(neSuffix) ? definitionCoreService.getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_CX_UP_SYMPTOMS) : definitionCoreService.getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_CX_DOWN_SYMPTOMS) ;
			
			int count = 0;
			for (Iterator iterator = symptoms.iterator(); iterator.hasNext();)
			{
				String string = (String) iterator.next();
				int k = getrandomNumber().intValue();
				count +=k;
				countSummary.put(string, new FRSummaryCountVO(k, Arrays.asList(new Long[] { getrandomNumber()}),string.toUpperCase()));
			}
			vo.setTotalCount(count);
			summary.add(vo);
		}

	 }

	private void createSummaryVo(List<FRSummaryCountVO> summary)
	{

		for (Map.Entry<Long, String> entry : FRStatus.definition.entrySet())
		{
			FRSummaryCountVO s = new FRSummaryCountVO(entry.getValue(), entry.getKey(), Arrays
					.asList(new Long[] { getrandomNumber(), getrandomNumber() }), getrandomNumber()
					.intValue());
			summary.add(s);
		}

	}

	private Long getrandomNumber()
	{
		return Long.valueOf(r.nextInt(High - Low) + Low);// TODO Auto-generated
															// method stub

	}
}
