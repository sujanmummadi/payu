/**
 * 
 */
package com.cupola.fwmp.ws;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.mongo.gis.GISDataDao;
import com.cupola.fwmp.dao.mongo.vo.fr.GISData;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.gis.GISServiceIntegrationImpl;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.tool.GlobalCoreService;
import com.cupola.fwmp.service.user.UserService;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.gis.GISFesiabilityInput;
import com.cupola.fwmp.vo.gis.GISNotFeasibleVO;
import com.cupola.fwmp.vo.gis.GISResponseVO;
import com.cupola.fwmp.vo.gis.GisCxVO;
import com.cupola.fwmp.vo.gis.GisFxVO;
import com.cupola.fwmp.vo.gis.GisMultipleFeasibilityVO;

/**
 * @author aditya
 */
@Path("/gis")
public class GISService
{
	@Autowired
	DBUtil dbUtil;

	@Autowired
	UserService userService;
	@Autowired
	GlobalCoreService globalCoreService;
	@Autowired
	CityDAO cityDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	GISDataDao gisDataDao;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;
	
	@Autowired
	WorkOrderDAO workOrderDAO;
	
	@Autowired
	SalesCoreServcie salesCoreServcie;

	private GISServiceIntegrationImpl gisServiceIntegration;

	public void setGisServiceIntegration(
			GISServiceIntegrationImpl gisServiceIntegration)
	{
		this.gisServiceIntegration = gisServiceIntegration;
	}

	@Path("/feasibility/checkfeasibility")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse checkFeasibility(GISFesiabilityInput gisInput)
	{
		return ResponseUtil.createFeasibleSuccessResponse()
				.setData(gisServiceIntegration.actGISFesibility(gisInput));
	}

	@Path("dummy/feasibility/checkfeasibility")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse checkDummyFeasibility(GISFesiabilityInput gisInput)
	{
		if (AuthUtils.getCurrentUserCity().getName()
				.equalsIgnoreCase(CityName.HYDERABAD_CITY))
		{
			return ResponseUtil.createFeasibleSuccessResponse()
					.setData(createGISDummyResponseHyd());
		} else
		{

//			return ResponseUtil.createFeasibleSuccessResponse()
//					.setData(createGISDummyResponseBlr());
			
			return ResponseUtil.createFeasibleSuccessResponse()
							.setData(createGISDummyNonFeasibleResponseHyd());
		}

	}

	@Path("/feasibility/save")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addGISInfoOfTicket2GisTable(GISPojo gisPojo)
	{
		if (gisPojo == null)
			return ResponseUtil.createNullParameterResponse();
		
		if (!dbUtil.isTicketAvailable(Long
				.valueOf(gisPojo.getTicketNo()), AuthUtils
						.getCurrentUserId()))
			return ResponseUtil.createUnAutorizedFailureResponse();

		if (gisPojo == null)
			return ResponseUtil.createNullParameterResponse();
		

		if (gisPojo != null && gisPojo.getTicketNo() == null
				|| gisPojo.getTicketNo() <= 0)
			return ResponseUtil.createNullParameterResponse()
					.setData("Ticket id could be null or empty");

		gisPojo.setFeasibilityType(FeasibilityType.PHYSICAL_FEASIBILITY_BY_NE);
		if (WorkStageName.FIBER.equalsIgnoreCase(gisPojo.getConnectionType()))
		{
			gisPojo.setWorkstageType((int) WorkStageType.FIBER);

		} else if (WorkStageName.COPPER
				.equalsIgnoreCase(gisPojo.getConnectionType()))
		{
			gisPojo.setWorkstageType((int) WorkStageType.COPPER);
		}

		if (gisPojo.getCity() != null)
			gisPojo.setCity(cityDAO.getCityIdByName(gisPojo.getCity()) + "");

		if (gisPojo.getBranch() != null)
			gisPojo.setBranch(branchDAO.getIdByCode(gisPojo.getBranch()) + "");

		if (gisPojo.getArea() != null)
			gisPojo.setArea(areaDAO.getIdByCode(gisPojo.getArea()) + "");
		
		APIResponse response =  gisServiceIntegration.addGISInfoOfTicket2GisTable(gisPojo);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			String workOrderNumber = workOrderDAO
					.getWoNumberForTicket(gisPojo.getTicketNo());
			
			if(workOrderNumber == null)
				salesCoreServcie.updateSalesActivityInCRM(gisPojo.getTicketNo(),  null  , null);
		}
		

		return response;
	}

	@Path("/feasibility/getgisinfo4ticketno")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getGisInfo4TicketNo(String ticketNo)
	{
		Long ticketId = Long.valueOf(ticketNo);
		return gisServiceIntegration.getGisInfo4TicketNo(ticketId);
	}

	@Path("/corresponding/user")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getGisInfo4TicketNo(GISNotFeasibleVO gisNotFeasibleVO)
	{
		if (gisNotFeasibleVO == null)
			return ResponseUtil.createNullParameterResponse();

		if (gisNotFeasibleVO.getTicketId() == null
				|| gisNotFeasibleVO.getTicketId() <= 0)
			return ResponseUtil.createNullParameterResponse()
					.setData("Ticket ID could not be null or empty");

		return userService.getUsersOfAssosiatedDevice(gisNotFeasibleVO);
	}

	@Path("/feasibility/multiple")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse multipleFeasibility(GISFesiabilityInput gisInput)
	{
		return ResponseUtil.createFeasibleSuccessResponse()
				.setData(gisServiceIntegration.multipleGISFesibility(gisInput));
	}

	@Path("/update/gisData")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse multipleFeasibility(GISData gisData)
	{
		gisDataDao.storeGISData(gisData);
		return ResponseUtil.createUpdateSuccessResponse();
	}

	@Path("/dummy/feasibility/multiple")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse multipleDummyFeasibility(GISFesiabilityInput gisInput)
	{
		if (AuthUtils.getCurrentUserCity().getName()
				.equalsIgnoreCase(CityName.HYDERABAD_CITY))
		{
			return ResponseUtil.createFeasibleSuccessResponse()
					.setData(dummyResponseHyd());
		} else
		{

			return ResponseUtil.createFeasibleSuccessResponse()
					.setData(dummyResponseBlr());
		}
	}

	private GisMultipleFeasibilityVO dummyResponseBlr()
	{
		GisMultipleFeasibilityVO feasibilityVO = new GisMultipleFeasibilityVO();

		Map<String, GisCxVO> cxNameCxDetailsMap = new LinkedHashMap<String, GisCxVO>();
		Map<String, GisFxVO> fxNameFxDetailsMap = new LinkedHashMap<String, GisFxVO>();
		Map<String, List<String>> fxNameListCxMap = new LinkedHashMap<String, List<String>>();

		String fxMacId = "00:06:19:14:4f:70";
		String fxName = "BLR-BHO-AX1-9-10-BOO5";
		String fxPorts = "0";

		GisFxVO gisFxVO = new GisFxVO(fxName, null, fxMacId, fxPorts);
		fxNameFxDetailsMap.put(fxName, gisFxVO);

		List<String> cxList = new ArrayList<String>();

		String cxMacId = "00:06:19:1b:d5:f4";
		String cxName = "BOO5-13-HB5";
		String cxPorts = "4";

		GisCxVO gisCxVO = new GisCxVO(cxName, null, cxMacId, cxPorts);

		cxNameCxDetailsMap.put(cxName, gisCxVO);

		cxList.add(cxName);

		feasibilityVO.setType("COPPER");
		feasibilityVO.setErrorNo(0 + "");
		feasibilityVO.setMessage("Success");
		feasibilityVO.setTransactionNo("323232");
		feasibilityVO.setClusterName("Test");

		fxNameListCxMap.put(fxName, cxList);

		feasibilityVO.setCxNameCxDetailsMap(cxNameCxDetailsMap);
		feasibilityVO.setFxNameFxDetailsMap(fxNameFxDetailsMap);
		feasibilityVO.setFxNameListCxMap(fxNameListCxMap);

		return feasibilityVO;
	}

	private GisMultipleFeasibilityVO dummyResponseHyd()
	{
		GisMultipleFeasibilityVO feasibilityVO = new GisMultipleFeasibilityVO();

		Map<String, GisCxVO> cxNameCxDetailsMap = new LinkedHashMap<String, GisCxVO>();
		Map<String, GisFxVO> fxNameFxDetailsMap = new LinkedHashMap<String, GisFxVO>();
		Map<String, List<String>> fxNameListCxMap = new LinkedHashMap<String, List<String>>();

		String fxMacId = "00:06:19:11:9B:18";
		String fxName = "SCBD5-SAIRN-BHASW-10-11-150.8";
		String fxPorts = "0";

		GisFxVO gisFxVO = new GisFxVO(fxName, null, fxMacId, fxPorts);
		fxNameFxDetailsMap.put(fxName, gisFxVO);

		List<String> cxList = new ArrayList<String>();

		String cxMacId = "00:06:19:11:7F:4F";
		String cxName = "SCBD6-BHASW-7F:4F-00-19-150.128";
		String cxPorts = "4";

		GisCxVO gisCxVO = new GisCxVO(cxName, null, cxMacId, cxPorts);

		cxNameCxDetailsMap.put(cxName, gisCxVO);

		cxList.add(cxName);

		feasibilityVO.setType("COPPER");
		feasibilityVO.setErrorNo(0 + "");
		feasibilityVO.setMessage("Success");
		feasibilityVO.setTransactionNo("323232");
		feasibilityVO.setClusterName("Test");

		fxNameListCxMap.put(fxName, cxList);

		feasibilityVO.setCxNameCxDetailsMap(cxNameCxDetailsMap);
		feasibilityVO.setFxNameFxDetailsMap(fxNameFxDetailsMap);
		feasibilityVO.setFxNameListCxMap(fxNameListCxMap);

		return feasibilityVO;
	}

	private GISResponseVO createGISDummyResponseBlr()
	{
		GISResponseVO gisResponseVO = new GISResponseVO();
		gisResponseVO.setArea("SHNGR_DEFAULT");
		gisResponseVO.setBranch("SHNGR");
		gisResponseVO.setCity("BANGALORE");
		gisResponseVO.setClusterName("Test");
		gisResponseVO.setType("COPPER");
		gisResponseVO.setMessage("Success");
		gisResponseVO.setTransactionNo("1482345590130511");
		gisResponseVO.setErrorNo("0");

		gisResponseVO.setFxIpAddress("");
		gisResponseVO.setFxMacAddress("00:06:19:18:bb:f8");
		gisResponseVO.setFxName("BLR-HSR-AX2-7-8-IRAV");
		List list = new ArrayList<>();
		list.add("0");
		gisResponseVO.setFxPorts(list);

		gisResponseVO.setCxIpAddress("");
		gisResponseVO.setCxMacAddress("00:06:19:19:c0:c5");
		gisResponseVO.setCxName("IRAV-24-HIV");
		gisResponseVO.setCxPorts(list);

		return gisResponseVO;

	}

	private GISResponseVO createGISDummyResponseHyd()
	{
		GISResponseVO gisResponseVO = new GISResponseVO();
		gisResponseVO.setArea("SECD_DEFAULT");
		gisResponseVO.setBranch("SECD");
		gisResponseVO.setCity("HYDERABAD");
		gisResponseVO.setClusterName("Test");
		gisResponseVO.setType("COPPER");
		gisResponseVO.setMessage("Success");
		gisResponseVO.setTransactionNo("1482345590130511");
		gisResponseVO.setErrorNo("0");

		gisResponseVO.setFxIpAddress("");
		gisResponseVO.setFxMacAddress("00:06:19:18:bb:f8");
		gisResponseVO.setFxName("SCBD5-SAIRN-BHASW-10-11-150.8");
		List list = new ArrayList<>();
		list.add("0");
		gisResponseVO.setFxPorts(list);

		gisResponseVO.setCxIpAddress("");
		gisResponseVO.setCxMacAddress("00:06:19:19:c0:c5");
		gisResponseVO.setCxName("SCBD6-BHASW-7F:4F-00-19-150.128");
		gisResponseVO.setCxPorts(list);

		return gisResponseVO;

	}
	
	private GISResponseVO createGISDummyNonFeasibleResponseHyd()
	{
		GISResponseVO gisResponseVO = new GISResponseVO();
		gisResponseVO.setArea("SECD_DEFAULT");
		gisResponseVO.setBranch("SECD");
		gisResponseVO.setCity("HYDERABAD");
		gisResponseVO.setClusterName("Test");
		gisResponseVO.setType("NON_FEASIBLE");
		gisResponseVO.setMessage("Success");
		gisResponseVO.setTransactionNo("1482345590130511");
		gisResponseVO.setErrorNo("0");

		gisResponseVO.setFxIpAddress("");
		gisResponseVO.setFxMacAddress("00:06:19:18:bb:f8");
		gisResponseVO.setFxName("SCBD5-SAIRN-BHASW-10-11-150.8");
		List list = new ArrayList<>();
		list.add("0");
		gisResponseVO.setFxPorts(list);

		gisResponseVO.setCxIpAddress("");
		gisResponseVO.setCxMacAddress("00:06:19:19:c0:c5");
		gisResponseVO.setCxName("SCBD6-BHASW-7F:4F-00-19-150.128");
		gisResponseVO.setCxPorts(list);

		return gisResponseVO;

	}
}
