package com.cupola.fwmp.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;

@Path("integ/tablet/check")
public class TabletCheckService
{
	final static Logger log = Logger.getLogger(TabletCheckService.class);

	@Autowired
	com.cupola.fwmp.service.tablet.TabletService tabletService;


	@POST
	@Path("/gettab/by")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse tabPagination(@QueryParam("macAddress") String macAddress)
	{
		if(macAddress == null)
			return ResponseUtil.createNullParameterResponse();
	
		return tabletService.getTabletByMacAddress(macAddress);
	}
	
}
