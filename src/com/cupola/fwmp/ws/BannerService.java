package com.cupola.fwmp.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.doc.service.DocumentationCoreEngine;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;

@Path("banners")
public class BannerService
{

	@Autowired
	private DocumentationCoreEngine docEngine;
	
	@GET
	@Path("version")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCurrentBannerVersion()
	{
		
		return ResponseUtil.createSuccessResponse().setData(docEngine.readCurrentBannerVersion());
		
	}
	
	
	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getCurrentBanners()
	{
		
		return	ResponseUtil.createSuccessResponse().setData(docEngine.getAllBanners());
	}
}
