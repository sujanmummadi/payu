package com.cupola.fwmp.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.fr.FrDeploymentSettingDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;

@Path("cache")
public class CacheService {
	
	@Autowired
	DefinitionCoreService  definitionCoreService;
	
	@Autowired
	FrDeploymentSettingDao deploymentSettingDao;
	
	@GET
	@Path("/reloadAll")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse reloadAll(){
		
		if(AuthUtils.isAdmin())
		{
			LocationCache.reFreshCache();
			WorkOrderDefinitionCache.reFreshCache();
			definitionCoreService.reloadDataFromProperties();
			deploymentSettingDao.resetCache();
			
		}
		return ResponseUtil.createSuccessResponse();
		
		
	}
	
	@GET
	@Path("/reload/{cacheName}")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse reloadByCacheName(@PathParam("cacheName") String cacheName){

		if(AuthUtils.isAdmin())
		{
		if(cacheName != null && !cacheName.isEmpty())
		{
			if(cacheName.equalsIgnoreCase("location"))
			{
				LocationCache.reFreshCache();
			}
			else if(cacheName.equalsIgnoreCase("workorder"))
			{
				WorkOrderDefinitionCache.reFreshCache();
			}
			else if(cacheName.equalsIgnoreCase("workorder"))
			{
				WorkOrderDefinitionCache.reFreshCache();
				
			}
			else if(cacheName.equalsIgnoreCase("defnition"))
			{
				definitionCoreService.reloadDataFromProperties();
				
			}
			else if(cacheName.equalsIgnoreCase("frDeploymentSettings")) {
				deploymentSettingDao.resetCache();
			}
		}
		}
		  return ResponseUtil.createSuccessResponse();
	}

}
