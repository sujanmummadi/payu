package com.cupola.fwmp.ws;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.city.CityCoreService;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.vo.TypeAheadVo;

@Path("city")
public class CityService {

	CityCoreService cityService;

	public void setCityService(CityCoreService cityService) {
		this.cityService = cityService;
	}

	@POST
	@Path("add")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addCity(List<CityVO> cityVOs) {

		return cityService.addCities(cityVOs);
	}
	
	@POST
	@Path("add/city")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse addCityUI(CityVO cityVOs) {

		return cityService.addCity(cityVOs);
	}
	
	@POST
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllCity() {

		return cityService.getAllCity();
	}
	

	@POST
	@Path("corresponding/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<TypeAheadVo> getAllCitiesCorrespondingToState() {

		return cityService.getAllCitiesCorrespondingToState();
	}

	@POST
	@Path("idlist")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Long> getAllCityIds() {

		return cityService.getAllCityIds();
	}
	
	
}
