package com.cupola.fwmp.ws;
import javax.ws.rs.Path;

@Path("/tabservice")
public class TabService
{/*
	final static Logger log = Logger.getLogger(TabService.class);

	
	private TabDao tabDao;

	public void setTabDao(TabDao tabDao) {
		this.tabDao = tabDao;
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse addtab(Tab tab)
	{
		log.debug("Adding New tab  : " + tab);
		APIResponse apiResponse = new APIResponse();

		if (tab == null)
			return ResponseUtil.createNullParameterResponse();
		
		if (tab.getMacAddress() == null ||  tab.getMacAddress().isEmpty())
			return	ResponseUtil.createNullParameterResponse("Mac Address is null");

		try
		{
			apiResponse = tabDAO.addTab(tab);

			log.debug("tab:" + tab + " is saved into DB successfully");

			return ResponseUtil.createSaveSuccessResponse();

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return apiResponse;
	}

	@POST
	@Path("/tab/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getTabById(@PathParam("id") Long id)
	{
		log.debug("Get Tab By Id " + id);
		if (id == null)
			return ResponseUtil.createNullParameterResponse();

		return tabDAO.getTabById(id);
	}

	


	@POST
	@Path("tabs")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllTabs()
	{
		log.debug("Get All Tabs ");

		return tabDAO.getAllTabs();
	}

	@POST
	@Path("dropdownlist")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllTabOptions()
	{
		log.debug("Fetching all Tab details");
		return tabDAO.getAllTabOptions();
	}
	
	@POST
	@Path("delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse deleteTab(@PathParam("id") Long id)
	{
		if (id == null)
			return ResponseUtil.createNullParameterResponse();

		return tabDAO.deleteTab(id);
	}
	@POST
	@Path("engineers")
	@Produces(MediaType.APPLICATION_JSON)
	public APIResponse getAllEngineerDetails()
	{
		log.debug("Fetching all Engineer details");
		return tabDAO.getAllEngineerDetails();
	}
	
	
	
	@POST
	@Path("/ticket/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse getTicketDetailsByEngineerId(@PathParam("id") Long id)
	{
		log.debug("Get TicketDetails By Engineer Id " + id);
		if (id == null)
			return ResponseUtil.createNullParameterResponse();

		return tabDAO.getTicketDetailsByEngineerId(id);
	}
	
	
	public void setTabDAO(TabDAO tabDAO)
	{
		this.tabDAO = tabDAO;
	}
	
	
	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateZone(Tab tab)
	{
		log.debug("Update Tab : " + tab);
		
			return ResponseUtil.createSuccessResponse();
		
		
	}
	
	@POST
	@Path("/updateregistrationId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse updateRegistrationId(TabDetailsVo tabDetailsVo)
	{
		log.debug("updateRegistrationId  ::" + tabDetailsVo);
		
			return tabDao.updateRegistrationId(tabDetailsVo);
	}
	
*/}
