package com.cupola.fwmp.dispatcher.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.cupola.fwmp.dispatcher.model.Job;
import com.cupola.fwmp.dispatcher.service.JobService;
import com.cupola.fwmp.dispatcher.vo.JobVo;

@Path("job")
public class JobServices {

	public static Logger logger = LogManager.getLogger(JobServices.class);
	
	JobService jobService;

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	
	@POST
	@Path("notifyapp")
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendGcmNotification(Job job){
		
		logger.info("Job send by App server is:"+job);
		return jobService.sendGcmNotificationToApp(job);
	}
	
	@POST
	@Path("notifyappserver")
	@Consumes(MediaType.APPLICATION_JSON)
	public String notifyAppServer(JobVo jobVo){
		
		logger.info("JobVo send by App is:"+jobVo);
		return jobService.notifyAppServer(jobVo);
	}
	
}
