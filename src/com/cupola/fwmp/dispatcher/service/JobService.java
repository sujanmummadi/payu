package com.cupola.fwmp.dispatcher.service;

import com.cupola.fwmp.dispatcher.model.Job;
import com.cupola.fwmp.dispatcher.vo.JobVo;

public interface JobService {

	String sendGcmNotificationToApp(Job job);
	
	String notifyAppServer(JobVo jobVo);
}
