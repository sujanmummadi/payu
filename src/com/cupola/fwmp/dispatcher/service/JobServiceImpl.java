package com.cupola.fwmp.dispatcher.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.cupola.fwmp.dispatcher.dao.JobDAO;
import com.cupola.fwmp.dispatcher.model.Job;
import com.cupola.fwmp.dispatcher.vo.JobVo;

public class JobServiceImpl implements JobService{

	public static Logger logger = LogManager.getLogger(JobServiceImpl.class);
	
	JobDAO jobDAO;

	public void setJobDAO(JobDAO jobDAO) {
		this.jobDAO = jobDAO;
	}
	
	public String notifyAppServer(JobVo jobVo){
		
		return jobDAO.notifyAppServer(jobVo);
	}
	
	public String sendGcmNotificationToApp(Job job){
		
		return jobDAO.sendGcmNotificationToApp(job);
	}
	
}
