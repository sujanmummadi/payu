package com.cupola.fwmp.dispatcher.vo;

public class JobVo {

	private Long tokenId;
	private String status;
	
	public Long getTokenId() {
		return tokenId;
	}
	public void setTokenId(Long tokenId) {
		this.tokenId = tokenId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "JobVo [tokenId=" + tokenId + ", status=" + status + "]";
	}
	
	
}
