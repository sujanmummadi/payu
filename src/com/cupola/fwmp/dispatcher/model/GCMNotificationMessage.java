package com.cupola.fwmp.dispatcher.model;

public class GCMNotificationMessage {

	private String registrationId;
	private String message;

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GCMNotificationMessage [registrationId=" + registrationId
				+ ", message=" + message + "]";
	}

}
