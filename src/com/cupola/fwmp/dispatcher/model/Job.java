package com.cupola.fwmp.dispatcher.model;

public class Job {

	private String registrationId;
	private Long externalJobId;
	
	public String getRegistrationId() {
		return registrationId;
	}
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}
	public Long getExternalJobId() {
		return externalJobId;
	}
	public void setExternalJobId(Long externalJobId) {
		this.externalJobId = externalJobId;
	}
	@Override
	public String toString() {
		return "Job [registrationId=" + registrationId + ", externalJobId="
				+ externalJobId + "]";
	}
	
	
}
