package com.cupola.fwmp.dispatcher.dao;

import com.cupola.fwmp.dispatcher.model.Job;
import com.cupola.fwmp.dispatcher.vo.JobVo;

public interface JobDAO {

	String sendGcmNotificationToApp(Job job);
	
	String notifyAppServer(JobVo jobVo);
}
