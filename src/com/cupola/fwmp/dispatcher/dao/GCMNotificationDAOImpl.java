package com.cupola.fwmp.dispatcher.dao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.cupola.fwmp.dispatcher.model.GCMNotificationMessage;


public class GCMNotificationDAOImpl {

	public static Logger logger = LogManager
			.getLogger(GCMNotificationDAOImpl.class);

	public String sendGcmNotificationToApp(GCMNotificationMessage notification) {

		String response = null;

		logger.info("notification is::" + notification);
		
		 String API_KEY = "AIzaSyC_wo57AKZnbcWXy8hFMhna3Jrq99CvF4c";
		 String URL = "https://android.googleapis.com/gcm/send";
		 OutputStream outputStream = null;
		 InputStream inputStream  = null;

		try {
			// Prepare JSON containing the GCM message content. What to send and
			// where to send.
			JSONObject jGcmData = new JSONObject();
			JSONObject jData = new JSONObject();
			try {
				jData.put("message",notification.getMessage());

				// Where to send GCM message.
				jGcmData.put("to",notification.getRegistrationId());

				// What to send in GCM message.
				jGcmData.put("data", jData);

			} catch (JSONException e) {
				logger.error("Error While sending GcmNotificationToApp :"+e.getMessage());
				e.printStackTrace();
			}

			
			// Create connection to send GCM Message request.
			/* URL url = new URL(CommonUtil.getPropertyByName("GCM_URL")); */
			// HttpURLConnection conn = (HttpURLConnection)
			// url.openConnection();

			/*
			 * conn.setRequestProperty("Authorization", "key=" +
			 * CommonUtil.getPropertyByName("API_KEY"));
			 */

			URL url = new URL(URL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// conn.setRequestProperty("Authorization", "key=" +
			// CommonUtil.getPropertyByName("API_KEY"));
			conn.setRequestProperty("Authorization", "key=" + API_KEY);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			// Send GCM message content.
			 outputStream = conn.getOutputStream();
			outputStream.write(jGcmData.toString().getBytes());

			// Read GCM response.
			 inputStream = conn.getInputStream();
			response = IOUtils.toString(inputStream);
			logger.info("GCM Sever Response : " + response);

			Thread.sleep(60000);
			/*
			 * if(jobIdAndStatusMap.containsKey(notification.getExternalJobId()))
			 * return ""; else sendGcmNotificationToApp(notification);
			 */
			logger.info("Check your device/emulator for notification or loggercat for "
					+ "confirmation of the receipt of the GCM message.");
		} catch (IOException e) {
			logger.error("Unable to send GCM message.");
			logger.error("Please ensure that API_KEY has been replaced by the server "
					+ "API key, and that the device's registration token is correct (if specified).");
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Error While sending gsm notification interruption occurred :"+e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				if(outputStream != null)
				outputStream.close();
				if(inputStream != null)
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error("Error While streams closing :"+e.getMessage());
				e.printStackTrace();
			}
		}

		return response;
	}
}
