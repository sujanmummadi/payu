package com.cupola.fwmp.dispatcher.dao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import com.cupola.fwmp.dispatcher.model.Job;
import com.cupola.fwmp.dispatcher.vo.JobVo;
import com.cupola.fwmp.util.CommonUtil;

public class JobDAOImpl implements JobDAO
{

	public static Logger logger = LogManager.getLogger(JobDAOImpl.class);

	public static Map<Long, String> jobIdAndStatusMap = 
			new ConcurrentHashMap<Long, String>();

	public String notifyAppServer(JobVo jobVo)
	{
		if( jobVo == null )
			return "Not notified to App server";

		logger.info("In side notifyAppServer " +jobVo.getTokenId());

		CloseableHttpResponse httpResponse = null;
		CloseableHttpClient httpclient = null;

		try
		{
			 httpclient = HttpClients.createDefault();

			jobIdAndStatusMap.put(jobVo.getTokenId(), jobVo.getStatus());

			ObjectMapper mapper = new ObjectMapper();

			String jsonData = mapper.writeValueAsString(jobVo);

			String appServer = CommonUtil.getPropertyByName("APP_SERVER");

			HttpPost postRequest = new HttpPost(appServer
					+ "FFA/rest/ffaservices/updatestatusandgetnewjob");

			logger.debug("##########postRequest.getURI()#############"
					+ postRequest.getURI());

			StringEntity se = new StringEntity(jsonData);

			postRequest.setEntity(se);

			postRequest.setHeader("Content-type", "application/json");

			httpResponse = httpclient.execute(postRequest);

			logger.debug("Response::" + httpResponse);

		} catch (MalformedURLException e)
		{

			logger.error("Given URL is not present " + e.getMessage());

			e.printStackTrace();

		} catch (IOException e)
		{

			logger.error("Exception " + e.getMessage());

			e.printStackTrace();

		} finally
		{
			try
			{
				if(httpclient != null)
				httpResponse.close();
				if(httpResponse != null)
				httpclient.close();
				

			} catch (IOException e)
			{
				logger.error("Error in finally block, while closing httpresponse for notifyAppServer."
						+ e.getMessage());
				e.printStackTrace();
			}
		}
		logger.debug("Exitting from  notifyAppServer ");

		return "Notified to app server successfully";
	}

	public String sendGcmNotificationToApp(Job job)
	{

		String response = null;
		if(job == null)
			return response;

		logger.info(" inside sendGcmNotificationToApp job is:: "+job);
		 
		OutputStream outputStream = null;
		InputStream inputStream = null;

		try
		{
			// Prepare JSON containing the GCM message content. What to send and
			// where to send.
			JSONObject jGcmData = new JSONObject();
			JSONObject jData = new JSONObject();
			try
			{
				jData.put("message", job.getExternalJobId());

				// Where to send GCM message.
				jGcmData.put("to", job.getRegistrationId());

				// What to send in GCM message.
				jGcmData.put("data", jData);

			} catch (JSONException e)
			{
				logger.error("Error While creating json object :"+e.getMessage());
				e.printStackTrace();
			}

			// Create connection to send GCM Message request.
			URL url = new URL(CommonUtil.getPropertyByName("GCM_URL"));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Authorization", "key="
					+ CommonUtil.getPropertyByName("API_KEY"));
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			// Send GCM message content.
			 outputStream = conn.getOutputStream();
			outputStream.write(jGcmData.toString().getBytes());

			// Read GCM response.
			 inputStream = conn.getInputStream();
			response = IOUtils.toString(inputStream);
			logger.info("GCM Sever Response : " + response);

			Thread.sleep(60000);

			if (jobIdAndStatusMap!=null && jobIdAndStatusMap.containsKey(job.getExternalJobId()))
			{
				outputStream.close();
				inputStream.close();
				return "";
			}
			else
				sendGcmNotificationToApp(job);

			logger.info("Check your device/emulator for notification or loggercat for "
					+ "confirmation of the receipt of the GCM message.");
		} catch (IOException e)
		{
			logger.error("Unable to send GCM message.");
			logger.error("Please ensure that API_KEY has been replaced by the server "
					+ "API key, and that the device's registration token is correct (if specified).");
			e.printStackTrace();
		} catch (InterruptedException e)
		{
			logger.error("Error While sending gsm notification to app interruption occurred :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				if(inputStream != null)
				inputStream.close();
				if(outputStream != null)
				outputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		logger.debug(" Exitting from  sendGcmNotificationToApp  "+job);
		
		return response;
	}
}
