package com.cupola.fwmp.constants;

public interface NamingConstants {
	
	
	String CEO_ESCALATION = "ceoescalation";
	String NODAL_ESCALATION = "nodalescalation";
	String ORM  = "orm"; 
	String FOLLOW_UP  = "followup"; 
	String PRIVIOUS_DAY_CUSTOMER  = "previousdaycustomer"; 
	String HIGH_REVENUE_CUSTOMER  = "highrevenuecustomer"; 
	String FT_CUSTOMER  = "ftcustomer"; 
	String RED_ALERT  = "redalert"; 
	String FAST_TRACK_CUSTOMER  = "fastrackcustomer"; 
	String HOURS_EXECUTION = "hoursexecution"; 	
	String NO_OF_HOURS_PER_POINTS = "numberofhoursperpoints";
	String NO_OF_POINTS_PER_HOURS = "numberofpointsperhours";
	String NO_OF_NE_MEMS = "numberofnemems";
	String NO_OF_TL_MEMS = "numberoftlmems";
	String NO_OF_EVENT_HOURS = "numberofeventhours";
	String NO_OF_TICKETS_FOR_NE = "numberofTicketsAssignedForNE";
	String NO_OF_TICKETS_FOR_TL = "numberofTicketsAssignedForTL";
	String TIMER_SCHEDULER_TIME = "timerscheduletime";
  //String SCHEDULER_RUNNING_HOUR = "timerscheduletime";
	String DAILY_POINTS_FOR_TICKETS = "dailypointsfortickets";
	String SCHEDULER_RUNNING_HOUR = "schedulerrunninghour";
	String Timer_Schedule_Time_FOR_Global_Priority="timerScheduleTimeForGlobalPriority";
	String Base_GlobalPriority = "baseGlobalPriority";
	String Number_Of_Points_PerHour_For_GlobalPriority = "numberOfPointsPerHourForGlobalPriority";
	
	 	

}
