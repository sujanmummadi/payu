/**
* @Author aditya  30-Aug-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.controller.integ;

import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDaoImpl;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.WorkOrder;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.customer.ProspectCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.jobs.CrmJobHelper;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.service.tool.CRMCoreServices;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.util.tools.CommonToolUtil;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateProspectVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateWorkOrderVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckResponse;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckVO;

/**
 * @Author aditya 30-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CrmRequestHandler {

	static final Logger logger = Logger.getLogger(CrmRequestHandler.class);

	@Autowired
	ProspectCoreService prospectCoreService;

	@Autowired
	SalesCoreServcie salesCoreServcie;

	@Autowired
	WorkOrderDAO workOrderDAO;
	
	//added by manjuprasad for static removal
	@Autowired
	CommonToolUtil commonToolUtil;

	Thread crmProspectCreateThread = null;
	BlockingQueue<CreateUpdateProspectVo> crmProspectCreateQueue;

	Thread crmProspectUpdateThread = null;
	BlockingQueue<CreateUpdateProspectVo> crmProspectUpdateQueue;

	Thread crmWorkOrderCreateThread = null;
	BlockingQueue<CreateUpdateWorkOrderVo> crmWorkOrderCreateQueue;

	Thread crmWorkOrderUpdateThread = null;
	BlockingQueue<CreateUpdateWorkOrderVo> crmWorkOrderUpdateQueue;

	Thread crmFrUpdateThread = null;
	BlockingQueue<CreateUpdateFrVo> crmUpdateFrQueue;

	Thread crmCreateFrThread = null;
	BlockingQueue<CreateUpdateFrVo> crmCreateFrQueue;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	TicketCoreService ticketCoreService;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	CRMCoreServices coreServices;

	@Autowired
	CrmJobHelper crmJobHelper;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	FrTicketDao frTicketDao;

	public void init() {
		logger.info("CrmRequestHandler init method called..");

		crmProspectCreateQueue = new LinkedBlockingQueue<CreateUpdateProspectVo>(100000);
		crmProspectUpdateQueue = new LinkedBlockingQueue<CreateUpdateProspectVo>(100000);

		if (crmProspectUpdateThread == null) {
			crmProspectUpdateThread = new Thread(crmProspectUpdateThreadRun, "crmProspectUpdateHandlerService");
			crmProspectUpdateThread.start();
		}

		if (crmProspectCreateThread == null) {
			crmProspectCreateThread = new Thread(crmProspectCreateThreadRun, "crmProspectCreateHandlerService");
			crmProspectCreateThread.start();
		}

		crmWorkOrderUpdateQueue = new LinkedBlockingQueue<CreateUpdateWorkOrderVo>(100000);
		crmWorkOrderCreateQueue = new LinkedBlockingQueue<CreateUpdateWorkOrderVo>(100000);

		if (crmWorkOrderUpdateThread == null) {
			crmWorkOrderUpdateThread = new Thread(crmWorkOrderUpdateThreadRun, "crmWorkOrderUpdateHsndlerService");
			crmWorkOrderUpdateThread.start();
		}

		if (crmWorkOrderCreateThread == null) {
			crmWorkOrderCreateThread = new Thread(crmWorkOrderCreateThreadRun, "crmWorkOrderCreateHandlerService");
			crmWorkOrderCreateThread.start();
		}

		crmUpdateFrQueue = new LinkedBlockingQueue<CreateUpdateFrVo>(100000);
		crmCreateFrQueue = new LinkedBlockingQueue<CreateUpdateFrVo>(100000);

		if (crmFrUpdateThread == null) {
			crmFrUpdateThread = new Thread(crmFrUpdateThreadRun, "crmFrUpdateThreadHsndlerService");
			crmFrUpdateThread.start();
		}

		if (crmCreateFrThread == null) {
			crmCreateFrThread = new Thread(crmFrCreateThreadRun, "crmCreateFrThreadHandlerService");
			crmCreateFrThread.start();
		}

		logger.info("CrmRequestHandler init method executed.");
	}

	public void addNewCrmCreateProspect(CreateUpdateProspectVo prospect) {

		try {

			logger.info("Adding new crmCreateProspectQueue  request for customer " + prospect.getMobileNumber());

			if (prospect.getStatus() != null && prospect.getStatus().equalsIgnoreCase("open")) {

				logger.info(
						"crmCreateProspectQueue Ticket from Server start up and adding this ticket at the time of server start "
								+ prospect);

			} else {

				logger.info("Marking crmCreateProspect as open " + prospect);

				prospect.setStatus("open");
				prospect.setCreateUpdateProspectId(StrictMicroSecondTimeBasedGuid.newGuid());

				mongoTemplate.insert(prospect);
			}

			crmProspectCreateQueue.add(prospect);

			logger.info("Depth of crmCreateProspectQueue after adding  " + prospect + " is "
					+ crmProspectCreateQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmCreateProspectQueue " + e.getMessage());
		}
	}

	public void addNewCrmUpdateProspect(CreateUpdateProspectVo prospect) {
		try {
			logger.info("Adding new addNewCrmUpdateProspect request for customer " + prospect.getMobileNumber());

			if (prospect.getStatus() != null && prospect.getStatus().equalsIgnoreCase("open")) {
				logger.info(
						"addNewCrmUpdateProspect Ticket from Server start up and adding this ticket at the time of server start "
								+ prospect);

			} else {

				logger.info("Marking pospect as open for addNewCrmUpdateProspect " + prospect.getMobileNumber());
				prospect.setStatus("open");
				prospect.setCreateUpdateProspectId(StrictMicroSecondTimeBasedGuid.newGuid());
				
//Commented By Manjuprasad For Seibel Source Issue
//				prospect.setSource(CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL);
				mongoTemplate.insert(prospect);
			}

			crmProspectUpdateQueue.add(prospect);

			logger.info("Depth of addNewCrmUpdateProspect queue after adding " + prospect.getMobileNumber() + " is "
					+ crmProspectUpdateQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmProspectUpdateQueue " + e.getMessage());
		}
	}

	public void addNewCrmCreateWorkOrder(CreateUpdateWorkOrderVo workOrder) {
		try {
			logger.info("Adding new crmWorkOrderCreateQueue  request for customer " + workOrder.getCustomerMobile());

			if (workOrder.getStatus() != null && workOrder.getStatus().equalsIgnoreCase("open")) {
				logger.info(
						"crmWorkOrderCreateQueue Ticket from Server start up and adding this ticket at the time of server start "
								+ workOrder.getCustomerMobile());

			} else {
				logger.info("Marking crmWorkOrderCreate as open " + workOrder.getCustomerMobile());

				workOrder.setStatus("open");
				workOrder.setCreateUpdateWorkOrderId(StrictMicroSecondTimeBasedGuid.newGuid());

				mongoTemplate.insert(workOrder);
			}

			crmWorkOrderCreateQueue.add(workOrder);

			logger.info("Depth of crmWorkOrderCreateQueue after adding  " + workOrder.getCustomerMobile() + " is "
					+ crmWorkOrderCreateQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmWorkOrderCreateQueue " + e.getMessage());
		}
	}

	public void addNewCrmUpdateWorkOrder(CreateUpdateWorkOrderVo createUpdateWorkOrderVo) {
		try {
			logger.info("Adding new crmWorkOrderUpdateQueue  request for customer "
					+ createUpdateWorkOrderVo.getCustomerMobile());

			if (createUpdateWorkOrderVo.getStatus() != null
					&& createUpdateWorkOrderVo.getStatus().equalsIgnoreCase("open")) {
				logger.info(
						"crmWorkOrderUpdateQueue Ticket from Server start up and adding this ticket at the time of server start "
								+ createUpdateWorkOrderVo.getCustomerMobile());

			} else {

				logger.info("Marking crmWorkOrderUpdate as open " + createUpdateWorkOrderVo.getCustomerMobile());

				createUpdateWorkOrderVo.setStatus("open");

				createUpdateWorkOrderVo.setCreateUpdateWorkOrderId(StrictMicroSecondTimeBasedGuid.newGuid());

				mongoTemplate.insert(createUpdateWorkOrderVo);
			}

			crmWorkOrderUpdateQueue.add(createUpdateWorkOrderVo);

			logger.info("Depth of crmWorkOrderUpdateQueue after adding  " + createUpdateWorkOrderVo.getCustomerMobile()
					+ " is " + crmWorkOrderUpdateQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmWorkOrderUpdateQueue " + e.getMessage());
		}
	}

	public void addNewCrmCreateFr(CreateUpdateFrVo faultRepair) {
		try {
			logger.info("Adding new crmCreateFrQueue  request for customer " + faultRepair.getMobileNumber());

			if (faultRepair.getMongoStatus() != null && faultRepair.getMongoStatus().equalsIgnoreCase("open")) {
				logger.info(
						"crmCreateFrQueue Ticket from Server start up and adding this ticket at the time of server start "
								+ faultRepair.getMobileNumber());

			} else {
				logger.info("Marking crmCreateFrQueue as open " + faultRepair.getMobileNumber());

				faultRepair.setMongoStatus("open");

				faultRepair.setCreateUpdateFrWorkOrderId(StrictMicroSecondTimeBasedGuid.newGuid());

				mongoTemplate.insert(faultRepair);
			}

			crmCreateFrQueue.add(faultRepair);

			logger.info("Depth of crmCreateFrQueue after adding  " + faultRepair.getMobileNumber() + " is "
					+ crmCreateFrQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmCreateFrQueue " + e.getMessage());
		}
	}

	public void addNewCrmUpdateFr(CreateUpdateFrVo faultRepair) {
		try {
			logger.info("Adding new crmUpdateFrQueue  request for customer " + faultRepair.getMobileNumber());

			if (faultRepair.getMongoStatus() != null && faultRepair.getMongoStatus().equalsIgnoreCase("open")) {
				logger.info(
						"crmUpdateFrQueue Ticket from Server start up and adding this ticket at the time of server start "
								+ faultRepair.getMobileNumber());

			} else {
				logger.info("Marking crmUpdateFr as open " + faultRepair.getMobileNumber());

				faultRepair.setMongoStatus("open");

				faultRepair.setCreateUpdateFrWorkOrderId(StrictMicroSecondTimeBasedGuid.newGuid());

				mongoTemplate.insert(faultRepair);
			}

			crmUpdateFrQueue.add(faultRepair);

			logger.info("Depth of crmUpdateFrQueue after adding  " + faultRepair.getMobileNumber() + " is "
					+ crmUpdateFrQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmUpdateFrQueue " + e.getMessage());
		}
	}

	Runnable crmWorkOrderCreateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_CREATE_WORK_ORDER_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final CreateUpdateWorkOrderVo workOrder = crmWorkOrderCreateQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (workOrder != null) {
										logger.info(workOrder
												+ " and current depth crmWorkOrderCreateQueue after processing is "
												+ crmWorkOrderCreateQueue.size());

										// Create Prospect
										crmWorkOrderCreate(workOrder);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmWorkOrderCreateQueue " + " " + workOrder
											+ "" + e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmWorkOrderCreateQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmWorkOrderCreateQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmWorkOrderCreateQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void crmWorkOrderCreate(CreateUpdateWorkOrderVo workOrder) throws Exception {
			logger.info("Creating WO for customer for crmWorkOrderCreateQueue" + workOrder.getWorkOrderNumber());

			MQWorkorderVO mqWorkorderVO = CommonToolUtil.convertWorkOrderDataToFWMPFormate(workOrder);

			APIResponse apiResponse = coreServices.populateWorkOrder(workOrder.getCityName(),
					Collections.singletonList(mqWorkorderVO));

			if (apiResponse != null)
				updateStatusInMongo(workOrder);

			logger.info("WO creation result for customer for crmWorkOrderCreateQueue" + workOrder.getWorkOrderNumber()
					+ " :: " + apiResponse);

		}

		private CreateUpdateWorkOrderVo updateStatusInMongo(CreateUpdateWorkOrderVo workOrder) {

			Query query = new Query();

			query.addCriteria(Criteria.where("createUpdateWorkOrderId").is(workOrder.getCreateUpdateWorkOrderId()));

			CreateUpdateWorkOrderVo createUpdateWorkOrderVo = mongoTemplate.findAndRemove(query,
					CreateUpdateWorkOrderVo.class);

			return createUpdateWorkOrderVo;

		}

	};

	Runnable crmWorkOrderUpdateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_UPDATE_WORK_ORDER_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {

						final CreateUpdateWorkOrderVo createUpdateWorkOrderVo = crmWorkOrderUpdateQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (createUpdateWorkOrderVo != null) {
										logger.info(createUpdateWorkOrderVo
												+ " and current depth crmWorkOrderUpdateQueue after processing is "
												+ crmWorkOrderUpdateQueue.size());

										// Create Prospect
										crmWorkOrderUpdate(createUpdateWorkOrderVo);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmWorkOrderUpdateQueue " + " "
											+ createUpdateWorkOrderVo + "" + e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmWorkOrderUpdateQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmWorkOrderUpdateQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmWorkOrderUpdateQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void crmWorkOrderUpdate(CreateUpdateWorkOrderVo createUpdateWorkOrderVo) throws Exception {
			logger.info("Updating WO for customer for crmWorkOrderUpdateQueue"
					+ createUpdateWorkOrderVo.getWorkOrderNumber());

			WorkOrder workOrder = workOrderDAO.getWorkOrderByWoNo(createUpdateWorkOrderVo.getWorkOrderNumber());

			// WorkOrderActivityUpdateInCRMVo activityUpdateInCRMVo =
			// CommonToolUtil.convert2WO(createUpdateWorkOrderVo);
			// activityUpdateInCRMVo.setWorkOrderActivityUpdateInCRMVoId(workOrderActivityUpdateInCRMVo);

			int result = 0;

			if (workOrder != null) {
				createUpdateWorkOrderVo.setStatus(workOrder.getTicket().getStatus() + "");
				result = workOrderDAO.updateWOByCRM(createUpdateWorkOrderVo);

			} else {

				logger.info("During wo update, wo not found, hence Creating WO for customer for crmWorkOrderCreateQueue"
						+ createUpdateWorkOrderVo.getWorkOrderNumber());

				MQWorkorderVO mqWorkorderVO = CommonToolUtil.convertWorkOrderDataToFWMPFormate(createUpdateWorkOrderVo);

				APIResponse apiResponse = coreServices.populateWorkOrder(createUpdateWorkOrderVo.getCityName(),
						Collections.singletonList(mqWorkorderVO));

				logger.info("WO creation result for customer for crmWorkOrderCreateQueue"
						+ createUpdateWorkOrderVo.getWorkOrderNumber() + " :: " + apiResponse);

				if (apiResponse != null)
					result = workOrderDAO.updateWOByCRM(createUpdateWorkOrderVo);
			}

			if (result > 0)
				updateStatusInMongo(createUpdateWorkOrderVo);

			logger.info("WO Updating result for customer for crmWorkOrderUpdateQueue"
					+ createUpdateWorkOrderVo.getWorkOrderNumber() + " :: " + result);

		}

		private CreateUpdateWorkOrderVo updateStatusInMongo(CreateUpdateWorkOrderVo workOrder) {

			Query query = new Query();

			query.addCriteria(Criteria.where("createUpdateWorkOrderId").is(workOrder.getCreateUpdateWorkOrderId()));

			CreateUpdateWorkOrderVo createUpdateWorkOrderVo = mongoTemplate.findAndRemove(query,
					CreateUpdateWorkOrderVo.class);

			return createUpdateWorkOrderVo;

		}

	};

	Runnable crmFrUpdateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_UPDATE_FR_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final CreateUpdateFrVo prospect = crmUpdateFrQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (prospect != null) {
										logger.info(
												prospect + " and current depth crmFrUpdateQueue after processing is "
														+ crmUpdateFrQueue.size());

										// Update FR Ticket
										crmFrUpdate(prospect);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmFrUpdateQueue " + " " + prospect + ""
											+ e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmFrUpdateQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmFrUpdateQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmFrUpdateQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void crmFrUpdate(CreateUpdateFrVo faultRepair) throws Exception {

			logger.info("Creating FR WO for customer for crmFrUpdateQueue" + faultRepair);

			FrTicket frTicket = frTicketDao.getFrTicketByWorkOrder(faultRepair.getTicketNo());
		
			int result = 0;

			if (frTicket != null) {
				//faultRepair.setStatus(frTicket.getStatus() + "");
				result = frTicketDao.updateFrWOByCRM(faultRepair);
				updateStatusInMongo(faultRepair);

			} else {

				APIResponse apiResponse = crmFrCreate(faultRepair);

				if (apiResponse != null)
					result = frTicketDao.updateFrWOByCRM(faultRepair);

			}

			logger.info("Result for Creating FR WO for customer for crmFrUpdateQueue" + faultRepair + " is " + result);
		}

		/**
		 * @param faultRepair
		 * @throws Exception
		 */
		private APIResponse crmFrCreate(CreateUpdateFrVo faultRepair) {

			logger.info("Creating FR WO for customer for crmCreateFrQueue" + faultRepair);

			MQWorkorderVO mqWorkorderVO = CommonToolUtil.convertFRWorkOrderDataToFWMPFormate(faultRepair);

			logger.info(" mqWorkorderVO ::  " + mqWorkorderVO);

			logger.info("Creating FR WO for customer for mqWorkorderVO" + mqWorkorderVO);

			APIResponse apiResponse = crmJobHelper.createDetail(Collections.singletonList(mqWorkorderVO),LocationCache.getCityNameById(Long.valueOf(mqWorkorderVO.getCityId())));

			logger.info("FR WO creation result for customer crmCreateFrQueue" + faultRepair.getTicketNo() + " :: "
					+ apiResponse);

			return apiResponse;

		}

		private CreateUpdateFrVo updateStatusInMongo(CreateUpdateFrVo faultRepair) {

			Query query = new Query();

			query.addCriteria(
					Criteria.where("createUpdateFrWorkOrderId").is(faultRepair.getCreateUpdateFrWorkOrderId()));

			CreateUpdateFrVo createUpdateFrVo = mongoTemplate.findAndRemove(query, CreateUpdateFrVo.class);

			return createUpdateFrVo;

		}

	};

	Runnable crmFrCreateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_CREATE_FR_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final CreateUpdateFrVo faultRepair = crmCreateFrQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (faultRepair != null) {
										logger.info(
												faultRepair + " and current depth crmCreateFrQueue after processing is "
														+ crmCreateFrQueue.size());

										// Create FR Ticket
										crmFrCreate(faultRepair);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmCreateFrQueue " + " " + faultRepair + ""
											+ e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmCreateFrQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmCreateFrQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmCreateFrQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param faultRepair
		 * @throws Exception
		 */
		private void crmFrCreate(CreateUpdateFrVo faultRepair) throws Exception {

			logger.info("Creating FR WO for customer for crmCreateFrQueue" + faultRepair.getTicketNo());

			MQWorkorderVO mqWorkorderVO = CommonToolUtil.convertFRWorkOrderDataToFWMPFormate(faultRepair);

			logger.info(" mqWorkorderVO ::  " + mqWorkorderVO);

			APIResponse apiResponse = crmJobHelper.createDetail(Collections.singletonList(mqWorkorderVO),LocationCache.getCityNameById(Long.valueOf(mqWorkorderVO.getCityId())));


			if (apiResponse != null)
				updateStatusInMongo(faultRepair);

			logger.info("FR WO creation result for customer crmCreateFrQueue" + faultRepair.getTicketNo() + " :: ");

		}

		private CreateUpdateFrVo updateStatusInMongo(CreateUpdateFrVo faultRepair) {

			Query query = new Query();

			query.addCriteria(
					Criteria.where("createUpdateFrWorkOrderId").is(faultRepair.getCreateUpdateFrWorkOrderId()));

			CreateUpdateFrVo createUpdateFrVo = mongoTemplate.findAndRemove(query, CreateUpdateFrVo.class);

			return createUpdateFrVo;

		}

	};

	Runnable crmProspectUpdateThreadRun = new Runnable() {

		@Override
		public void run() {

			try {

				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_UPDATE_PROSPECT_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {

					try {

						final CreateUpdateProspectVo prospect = crmProspectUpdateQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (prospect != null) {
										logger.info(prospect
												+ " and current depth crmProspectUpdateQueue after processing is "
												+ crmProspectUpdateQueue.size());

										// Update Prospect
										updateProspect(prospect);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmProspectUpdateQueue " + " " + prospect
											+ "" + e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmProspectUpdateQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmProspectUpdateQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmProspectUpdateQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void updateProspect(CreateUpdateProspectVo prospect) throws Exception {

			logger.info("Updating prospect for customer for crmProspectUpdateQueue " + prospect);

			Ticket ticket = null;

			String prospectNo = prospect.getProspectNumber();
			logger.info("prospect number is ::" + prospectNo);
			logger.info("createUpdateprospectnumber is ::" + prospect.getCreateUpdateProspectId());
			ticket = ticketDAO.getTicketByProspectNumber(prospect.getProspectNumber());

			logger.info("ticket result after fetching query::" + ticket);

			String ticketId = null;
			String customerId = null;

			if (ticket != null) {

				ticketId = String.valueOf(ticket.getId());
				customerId = ticket.getCustomer() != null ? String.valueOf(ticket.getCustomer().getId()) : null;

			} else {

				logger.info("During prospect update, prospect not found, hence " + prospect.getProspectNumber()
						+ " Hence creating prospect " + prospect);

				APIResponse result = crmCreateProspectIfNotPresent(prospect);

				if (result != null) {
					ticket = ticketDAO.getTicketByProspectNumber(prospect.getProspectNumber());

					if (ticket != null) {

						ticketId = String.valueOf(ticket.getId());
						customerId = ticket.getCustomer() != null ? String.valueOf(ticket.getCustomer().getId()) : null;

					}
				}
			}

			logger.info("Updating prospect for customer " + customerId + " and Ticket " + ticketId + " . " + prospect);

			UpdateProspectVO updateProspectVO = CommonToolUtil.convertCreateUpdateProspectVo2UpdateProspectVO(prospect);

//Commented by MAnjuprasad for the issue where we change the Ticket Source
//			updateProspectVO.setSource(CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL);

			updateProspectVO.setCustomerId(customerId);
			updateProspectVO.setTicketId(ticketId);
			
			Long assignToFromCRM = null;
			
			if(UserDaoImpl.cachedUsersByLoginId.containsKey(prospect.getAssignedTo().toUpperCase()))
			{
				assignToFromCRM = UserDaoImpl.cachedUsersByLoginId.get(prospect.getAssignedTo().toUpperCase()).getId();
			}

			APIResponse apiResponse = salesCoreServcie.updateBasicInfo(updateProspectVO,false,assignToFromCRM,prospect.getDeDupFlag());
			CreateUpdateProspectVo createUpdateProspectVo = null;
			if (apiResponse.getStatusCode() == StatusCodes.SAVED_SUCCESSFULLY && apiResponse.getData() != null)
				createUpdateProspectVo = updateStatusInMongo(prospect);

			logger.info("Prospect creation result for customer crmProspectUpdateQueue " + prospect.getProspectNumber()
					+ " :: " + apiResponse + " createUpdateProspectVo " + createUpdateProspectVo);
		}

		private CreateUpdateProspectVo updateStatusInMongo(CreateUpdateProspectVo prospect) {

			Query query = new Query();

			query.addCriteria(Criteria.where("createUpdateProspectId").is(prospect.getCreateUpdateProspectId()));

			CreateUpdateProspectVo createUpdateProspectVo = mongoTemplate.findAndRemove(query,
					CreateUpdateProspectVo.class);

			return createUpdateProspectVo;

		}

		private APIResponse crmCreateProspectIfNotPresent(CreateUpdateProspectVo prospect) throws Exception {
			APIResponse result = null;
			String source = CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL;

			logger.info("Creating prospect for customer for crmCreateProspectIfNotPresent " + prospect
					+ "######### Source" + source);

			ProspectCoreVO prospectCoreVo = commonToolUtil.convertProspectDataToFWMPFormate(prospect);

			result = prospectCoreService.createNewProspect(prospectCoreVo, source);

			logger.info("Prospect creation result for customer " + prospect.getProspectNumber()
					+ " :: from crmCreateProspectIfNotPresent is " + result);

			return result;
		}

	};

	Runnable crmProspectCreateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_CREATE_PROSPECT_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final CreateUpdateProspectVo prospect = crmProspectCreateQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (prospect != null) {
										logger.info(prospect
												+ " and current depth crmCreateProspectQueue after processing is "
												+ crmProspectCreateQueue.size());

										// Create Prospect
										crmCreateProspect(prospect);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmCreateProspectQueue " + " " + prospect
											+ "" + e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmCreateProspectQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmCreateProspectQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmCreateProspectQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void crmCreateProspect(CreateUpdateProspectVo prospect) throws Exception {

			String source = CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL;

			logger.info("Creating prospect for customer for crmCreateProspectQueue " + prospect + "######### Source"
					+ source);

			ProspectCoreVO prospectCoreVo = commonToolUtil.convertProspectDataToFWMPFormate(prospect);
			prospectCoreVo.setDeDupFlag(prospect.getDeDupFlag());
			
			APIResponse result = prospectCoreService.createNewProspect(prospectCoreVo, source);

			if (result.getStatusCode() == StatusCodes.SAVED_SUCCESSFULLY)
				updateSuccessStatusInMongo(prospect);

			logger.info("Prospect creation result for customer " + prospect.getProspectNumber()
					+ " :: from crmCreateProspectQueue is " + result);
		}

		private void updateSuccessStatusInMongo(CreateUpdateProspectVo prospect) {

			Query query = new Query();

			query.addCriteria(Criteria.where("createUpdateProspectId").is(prospect.getCreateUpdateProspectId()));

			mongoTemplate.findAndRemove(query, CreateUpdateProspectVo.class);

		}

	};

	/**
	 * @author kiran void
	 * @param woEligibilityCheckVO
	 * @param cityId
	 */
	public WOEligibilityCheckResponse isEligibleForWOCreation(WOEligibilityCheckVO woEligibilityCheckVO, Long cityId) {

		logger.info("isEligibleForWOCreation called in Handler" + woEligibilityCheckVO);

		WOEligibilityCheckResponse eligibilityCheckResponse = new WOEligibilityCheckResponse();

		eligibilityCheckResponse = ticketCoreService.isEligibleForWOCreation(woEligibilityCheckVO, cityId);

		logger.info("isEligibleForWOCreation response in Handler" + eligibilityCheckResponse);

		return eligibilityCheckResponse;
	}

}
