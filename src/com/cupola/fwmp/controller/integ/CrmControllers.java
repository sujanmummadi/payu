/**
* @Author aditya  29-Aug-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.controller.integ;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.beans.BeanUtils;

import com.cupola.fwmp.FWMPConstant.ApiAction;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.service.tool.CRMCoreServices;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.util.tools.CommonToolUtil;
import com.cupola.fwmp.vo.tools.CRMFrLogger;
import com.cupola.fwmp.vo.tools.CRMProspectLogger;
import com.cupola.fwmp.vo.tools.CRMWorkOrderLogger;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateProspectVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateWorkOrderVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckResponse;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckVO;
import com.cupola.fwmp.vo.tools.siebel.response2siebel.WorkOrderResponse2siebel;

/**
 * @Author aditya 29-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

@Path("/external/crm")
public class CrmControllers {

	private static Logger log = LogManager.getLogger(CrmControllers.class.getName());

	@Autowired
	CRMCoreServices crmCoreServices;

	@Autowired
	CrmRequestHandler crmRequestHandler;

	@Autowired
	TicketCoreService ticketCoreService;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	CommonToolUtil commonToolUtil;

	@POST
	@Path("create/prospect")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse createProspect(CreateUpdateProspectVo createUpdateProspectVo) {

		log.info("create and update prospect from Siebel called"+createUpdateProspectVo);

		if (createUpdateProspectVo != null) {

			CRMProspectLogger logger = new CRMProspectLogger();
			logger.setAddedOnDate(new Date());
			logger.setTransactionId(createUpdateProspectVo.getTransactionId());

			APIResponse response = commonToolUtil.validateSiebelProspectData(createUpdateProspectVo);

			if (response != null) {

				logger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				logger.setCreateUpdateProspectVo(createUpdateProspectVo);
				logger.setValidatorResponse(response);

				response.setTransactionId(createUpdateProspectVo.getTransactionId());

				ticketCoreService.prospectLoggerFromSiebel(logger);
				return response;

			} else {

				logger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				logger.setCreateUpdateProspectVo(createUpdateProspectVo);
				logger.setValidatorResponse(response);
				ticketCoreService.prospectLoggerFromSiebel(logger);

			}

			if (createUpdateProspectVo.getAction().equalsIgnoreCase(ApiAction.CREATE)
					&& (createUpdateProspectVo.getPreferredCallDate() == null
							|| createUpdateProspectVo.getPreferredCallDate().isEmpty())) {

				createUpdateProspectVo.setPreferredCallDate(GenericUtil.convertToMqDateFormat(new Date()));
			}

			if (createUpdateProspectVo.getAction().equalsIgnoreCase(ApiAction.CREATE))
				crmRequestHandler.addNewCrmCreateProspect(createUpdateProspectVo);

			else if (createUpdateProspectVo.getAction().equalsIgnoreCase(ApiAction.UPDATE))
				crmRequestHandler.addNewCrmUpdateProspect(createUpdateProspectVo);

		}

		return ResponseUtil.createSuccessResponse().setTransactionId(createUpdateProspectVo.getTransactionId())
				.setData("Saved Successfuly");
	}

	@POST
	@Path("isEligibleForWOCreation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse isEligibleForWOCreation(WOEligibilityCheckVO woEligibilityCheckVO) {

		log.info("isEligibleForWOCreation called from Siebel called " + woEligibilityCheckVO);

		woEligibilityCheckVO.setId(StrictMicroSecondTimeBasedGuid.newGuid());

		APIResponse response = isEligibleForWOCreationValidator(woEligibilityCheckVO);

		woEligibilityCheckVO.setValidatorResponse(response);

		if (response != null) {
			log.info("Inside Response is not Null :"+response);
			response.setTransactionId(woEligibilityCheckVO.getTransactionId());
			mongoTemplate.insert(woEligibilityCheckVO);
			WOEligibilityCheckResponse eligibilityCheckResponse = new WOEligibilityCheckResponse();
			eligibilityCheckResponse.setCityName(woEligibilityCheckVO.getCityName());
			eligibilityCheckResponse.setEligibleForWorkOrderCreation(false);
			eligibilityCheckResponse.setProspectNo(woEligibilityCheckVO.getProspectNo());
			return response.setData(eligibilityCheckResponse);

		} else {

			Long cityId = cityDAO.getCityIdByName(woEligibilityCheckVO.getCityName()) == null
					? cityDAO.getIdByCode(woEligibilityCheckVO.getCityName())
					: null;
			WOEligibilityCheckResponse eligibilityCheckResponse = crmRequestHandler
					.isEligibleForWOCreation(woEligibilityCheckVO, cityId);
			if(eligibilityCheckResponse.isEligibleForWorkOrderCreation() == false)
			{
				eligibilityCheckResponse = new WOEligibilityCheckResponse();
				eligibilityCheckResponse.setCityName(woEligibilityCheckVO.getCityName());
				eligibilityCheckResponse.setEligibleForWorkOrderCreation(false);
				eligibilityCheckResponse.setProspectNo(woEligibilityCheckVO.getProspectNo());
			}
			
			woEligibilityCheckVO.setResponse(eligibilityCheckResponse);

			log.info("isEligibleForWOCreation response to Siebel " + eligibilityCheckResponse);

			mongoTemplate.insert(woEligibilityCheckVO);

			return ResponseUtil.createSuccessResponse().setTransactionId(woEligibilityCheckVO.getTransactionId())
					.setData(eligibilityCheckResponse);

		}
	}

	/**
	 * @author aditya String
	 * @param woEligibilityCheckVO
	 * @return
	 */
	private APIResponse isEligibleForWOCreationValidator(WOEligibilityCheckVO woEligibilityCheckVO) {

		if (woEligibilityCheckVO == null)
			return ResponseUtil.createNullParameterResponse();

		if (woEligibilityCheckVO.getProspectNo() == null || woEligibilityCheckVO.getProspectNo().equals(null)
				|| woEligibilityCheckVO.getProspectNo().isEmpty())
			return ResponseUtil.createNOProspectNumberFromSiebel();

		if (woEligibilityCheckVO.getTransactionId() == null || woEligibilityCheckVO.getTransactionId().isEmpty())
			return ResponseUtil.createNoTransactionId();
		else if (woEligibilityCheckVO.getTransactionId() != null || !woEligibilityCheckVO.getTransactionId().isEmpty())
			return crmCoreServices.getTransactionById(woEligibilityCheckVO.getTransactionId());

		if (woEligibilityCheckVO.getCityName() == null || woEligibilityCheckVO.getCityName().isEmpty())
			return ResponseUtil.createNoCityNameFromSiebel();

		Long cityId = cityDAO.getCityIdByName(woEligibilityCheckVO.getCityName().toUpperCase()) == null
				? cityDAO.getIdByCode(woEligibilityCheckVO.getCityName().toUpperCase())
				: cityDAO.getCityIdByName(woEligibilityCheckVO.getCityName().toUpperCase());

		log.info("isEligibleForWOCreation request to Siebel " + cityId + " for " + woEligibilityCheckVO);

		if (cityId == null || cityId <= 0)
			return ResponseUtil.createInvalidCityName();

		return null;
	}

	@POST
	@Path("create/workorder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WorkOrderResponse2siebel createWorkorder(CreateUpdateWorkOrderVo createUpdateWorkOrderVo) {

		log.info("create workorder from Siebel called " + createUpdateWorkOrderVo);

		WorkOrderResponse2siebel orderResponse2siebel = new WorkOrderResponse2siebel();

		if (createUpdateWorkOrderVo == null) {
			
			APIResponse apiResponse = ResponseUtil.createNullParameterResponse();
			BeanUtils.copyProperties(apiResponse, orderResponse2siebel);
			return orderResponse2siebel;
		}
		
		orderResponse2siebel.setWorkOrderNumber(createUpdateWorkOrderVo.getWorkOrderNumber());
		
		if (createUpdateWorkOrderVo != null) {

			APIResponse response = commonToolUtil.validateSiebelWorkOrderData(createUpdateWorkOrderVo);

			CRMWorkOrderLogger logger = new CRMWorkOrderLogger();
			logger.setAddedOnDate(new Date());
			logger.setTransactionId(createUpdateWorkOrderVo.getTransactionId());

			if (response != null) {

				logger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				logger.setCreateUpdateWorkOrderVo(createUpdateWorkOrderVo);
				logger.setValidatorResponse(response);
				response.setTransactionId(createUpdateWorkOrderVo.getTransactionId());

				ticketCoreService.workOrderLoggerFromSiebel(logger);
				BeanUtils.copyProperties(response, orderResponse2siebel);

				return orderResponse2siebel;

			} else {

				logger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				logger.setCreateUpdateWorkOrderVo(createUpdateWorkOrderVo);
				logger.setValidatorResponse(response);

				ticketCoreService.workOrderLoggerFromSiebel(logger);
			}

			if (createUpdateWorkOrderVo.getAction().equalsIgnoreCase(ApiAction.CREATE))
				crmRequestHandler.addNewCrmCreateWorkOrder(createUpdateWorkOrderVo);

			else if (createUpdateWorkOrderVo.getAction().equalsIgnoreCase(ApiAction.UPDATE))
				crmRequestHandler.addNewCrmUpdateWorkOrder(createUpdateWorkOrderVo);

		}

		APIResponse apiResponse = ResponseUtil.createSuccessResponse()
				.setTransactionId(createUpdateWorkOrderVo.getTransactionId()).setData("Saved Successfuly");
		orderResponse2siebel.setData("Saved Successfuly");
		BeanUtils.copyProperties(apiResponse, orderResponse2siebel);

		return orderResponse2siebel;
	}

	@POST
	@Path("create/faultrepair")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public APIResponse createFaultReapir(CreateUpdateFrVo createUpdateFrVo) {

		log.info("create fault repair from Siebel called");

		if (createUpdateFrVo != null) {

			APIResponse response = commonToolUtil.validateSiebelFRWorkOrderData(createUpdateFrVo);

			CRMFrLogger logger = new CRMFrLogger();
			logger.setAddedOnDate(new Date());
			logger.setTransactionId(createUpdateFrVo.getTransactionId());

			if (response != null) {

				logger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				logger.setCreateUpdateFrVo(createUpdateFrVo);
				logger.setValidatorResponse(response);
				response.setTransactionId(createUpdateFrVo.getTransactionId());

				ticketCoreService.frLoggerFromSiebel(logger);

				return response;

			} else {

				logger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				logger.setCreateUpdateFrVo(createUpdateFrVo);
				logger.setValidatorResponse(response);

				ticketCoreService.frLoggerFromSiebel(logger);

			}

			if (createUpdateFrVo.getAction().equalsIgnoreCase(ApiAction.CREATE))
				crmRequestHandler.addNewCrmCreateFr(createUpdateFrVo);

			else if (createUpdateFrVo.getAction().equalsIgnoreCase(ApiAction.UPDATE))
				crmRequestHandler.addNewCrmUpdateFr(createUpdateFrVo);

		}

		return ResponseUtil.createSuccessResponse().setTransactionId(createUpdateFrVo.getTransactionId())
				.setData("Saved Successfuly");
	}

}