/**
* @Author aditya  30-Aug-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.controller.integ;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.FWMPConstant.FWMP_Flows.SiebelWorkOrderStatus;
import com.cupola.fwmp.dao.tool.CRMDao;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.integ.crm.CrmServiceCall;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.CreateUpdateFaultRepairResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.from.vo.prospect.ProspectResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.from.vo.workorder.CreateUpdateWorkOrderResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.FaultRepairActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.CreateUpdateFaultRepairRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.Action;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.CreateUpdateWorkOrderRequestInSiebel;

/**
 * @Author aditya 30-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

public class CRMUpdateRequestHandler {

	static final Logger logger = Logger.getLogger(CRMUpdateRequestHandler.class);

	@Autowired
	SalesCoreServcie salesCoreServcie;

	@Autowired
	CRMDao crmDao;

	@Autowired
	CrmServiceCall crmServiceCall;

	@Autowired
	UserDao userDao;

	private String siebelURI;

	/**
	 * @return the siebelURI
	 */
	public String getSiebelURI() {
		return siebelURI;
	}

	/**
	 * @param siebelURI
	 *            the siebelURI to set
	 */
	public void setSiebelURI(String siebelURI) {
		this.siebelURI = siebelURI;
	}

	Thread crmProspectUpdateThread = null;
	BlockingQueue<SalesActivityUpdateInCRMVo> crmProspectUpdateQueue;

	Thread crmWorkOrderUpdateThread = null;
	BlockingQueue<WorkOrderActivityUpdateInCRMVo> crmWorkOrderUpdateQueue;

	Thread crmFrUpdateThread = null;
	BlockingQueue<FaultRepairActivityUpdateInCRMVo> crmFRUpdateQueue;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	TicketCoreService ticketCoreService;

	@Autowired
	DefinitionCoreService definitionCoreService;

	public void init() {

		logger.info("CrmRequestHandler init method called..");

		crmProspectUpdateQueue = new LinkedBlockingQueue<SalesActivityUpdateInCRMVo>(100000);

		if (crmProspectUpdateThread == null) {
			crmProspectUpdateThread = new Thread(crmProspectUpdateThreadRun, "crmProspectUpdateHandlerService");
			crmProspectUpdateThread.start();
		}

		crmWorkOrderUpdateQueue = new LinkedBlockingQueue<WorkOrderActivityUpdateInCRMVo>(100000);

		if (crmWorkOrderUpdateThread == null) {
			crmWorkOrderUpdateThread = new Thread(crmWorkOrderUpdateThreadRun, "crmWorkOrderUpdateHsndlerService");
			crmWorkOrderUpdateThread.start();
		}

		crmFRUpdateQueue = new LinkedBlockingQueue<FaultRepairActivityUpdateInCRMVo>(100000);

		if (crmFrUpdateThread == null) {
			crmFrUpdateThread = new Thread(crmFrUpdateThreadRun, "crmFrUpdateThreadHsndlerService");
			crmFrUpdateThread.start();
		}

		logger.info("CrmRequestHandler init method executed.");
	}

	public void salesActivityUpdateInCRM(SalesActivityUpdateInCRMVo prospect) {
		try {

			logger.info("Adding new addNewCrmUpdateProspect request fthunor customer " + prospect.getCustomerId());
			logger.info("SalesActivityUpdateInCRMVo " + prospect);

			if (prospect.getStatus() != null && prospect.getStatus().equalsIgnoreCase("open")) {
				logger.info(
						"addNewCrmUpdateProspect Ticket from Server start up and adding this ticket at the time of server start "
								+ prospect);

			} else {

				logger.info("Marking pospect as open for addNewCrmUpdateProspect " + prospect.getCustomerId());

				prospect.setStatus("open");
				prospect.setSalesActivityUpdateInCRMId(StrictMicroSecondTimeBasedGuid.newGuid());
				prospect.setTransactionId(prospect.getSalesActivityUpdateInCRMId() + "");

				CreateUpdateProspectRequestInSiebel createUpdateProspectInSiebel = getSiebelFormatForSales(prospect);

				logger.info("CreateUpdateProspectRequestInSiebel" + createUpdateProspectInSiebel);

				createUpdateProspectInSiebel.getCreateUpdateProspect_Input()
						.setTransation_spcId(prospect.getSalesActivityUpdateInCRMId() + "");

				prospect.setCreateUpdateProspectInSiebel(createUpdateProspectInSiebel);

				mongoTemplate.insert(prospect);
			}

			crmProspectUpdateQueue.add(prospect);

			logger.info("Depth of addNewCrmUpdateProspect queue after adding " + prospect.getCustomerId() + " is "
					+ crmProspectUpdateQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmProspectUpdateQueue " + e.getMessage());
		}
	}

	public void workOrderActivityUpdateInCRM(WorkOrderActivityUpdateInCRMVo workOrder , String activityId) {
		try {
			logger.info("Adding new crmWorkOrderUpdateQueue  request for customer " + workOrder.getWorkOrderNumber());

			if (workOrder.getStatus() != null && workOrder.getStatus().equalsIgnoreCase("open")) {
				logger.info(
						"crmWorkOrderUpdateQueue Ticket from Server start up and adding this ticket at the time of server start "
								+ workOrder.getCustomerId());

			} else {
				logger.info("Marking crmWorkOrderUpdate as open " + workOrder.getWorkOrderNumber());

				workOrder.setStatus("open");

				workOrder.setWorkOrderActivityUpdateInCRMVoId(StrictMicroSecondTimeBasedGuid.newGuid());
				workOrder.setTransactionId(workOrder.getWorkOrderActivityUpdateInCRMVoId() + "");

				CreateUpdateWorkOrderRequestInSiebel createUpdateWorkOrderRequestInSiebel = getSiebelFormatForWorkOrder(
						workOrder,activityId);
				createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input()
						.setTransactionID(workOrder.getWorkOrderActivityUpdateInCRMVoId() + "");

				workOrder.setCreateUpdateWorkOrderRequestInSiebel(createUpdateWorkOrderRequestInSiebel);

				mongoTemplate.insert(workOrder);
			}

			crmWorkOrderUpdateQueue.add(workOrder);

			logger.info("Depth of crmWorkOrderUpdateQueue after adding  " + workOrder.getWorkOrderNumber() + " is "
					+ crmWorkOrderUpdateQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmWorkOrderUpdateQueue " + e.getMessage());
		}
	}

	public void faultRepairActivityUpdateInCRM(FaultRepairActivityUpdateInCRMVo faultRepair) {
		try {
			logger.info("Adding new crmUpdateFrQueue  request for customer " + faultRepair.getTicketId());

			if (faultRepair.getStatus() != null && faultRepair.getStatus().equalsIgnoreCase("open")) {
				logger.info(
						"crmUpdateFrQueue Ticket from Server start up and adding this ticket at the time of server start "
								+ faultRepair.getTicketId());

			} else {
				logger.info("Marking crmUpdateFr as open " + faultRepair.getWorkOrderNumber());

				faultRepair.setStatus("open");

				faultRepair.setFaultRepairActivityUpdateInCRMVoId(StrictMicroSecondTimeBasedGuid.newGuid());
				faultRepair.setTransactionId(faultRepair.getFaultRepairActivityUpdateInCRMVoId() + "");

				CreateUpdateFaultRepairRequestInSiebel createUpdateFaultRepairRequestInSiebel = getSiebelFormatForFaultRepair(
						faultRepair);
				createUpdateFaultRepairRequestInSiebel.getFWMPSRUpsert_Input()
						.setTransaction_spcID(faultRepair.getFaultRepairActivityUpdateInCRMVoId() + "");

				faultRepair.setCreateUpdateFaultRepairRequestInSiebel(createUpdateFaultRepairRequestInSiebel);

				mongoTemplate.insert(faultRepair);
			}

			crmFRUpdateQueue.add(faultRepair);

			logger.info("Depth of crmUpdateFrQueue after adding  " + faultRepair.getWorkOrderNumber() + " is "
					+ crmFRUpdateQueue.size());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error crmUpdateFrQueue " + e.getMessage());
		}
	}

	Runnable crmWorkOrderUpdateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_UPDATE_WORK_ORDER_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final WorkOrderActivityUpdateInCRMVo workOrder = crmWorkOrderUpdateQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (workOrder != null) {
										logger.info(workOrder
												+ " and current depth crmWorkOrderUpdateQueue after processing is "
												+ crmWorkOrderUpdateQueue.size());

										// Create Prospect
										crmWorkOrderUpdate(workOrder);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmWorkOrderUpdateQueue " + " "
											+ workOrder.getWorkOrderNumber() + "" + e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmWorkOrderUpdateQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmWorkOrderUpdateQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmWorkOrderUpdateQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void crmWorkOrderUpdate(WorkOrderActivityUpdateInCRMVo workOrder) throws Exception {
			logger.info("Creating WO for customer for crmWorkOrderUpdateQueue" + workOrder);

			CreateUpdateWorkOrderResponseFromSiebel fromSiebel = null;

			try {
				logger.info("Creating prospect for customer for crmWorkOrderUpdateQueue" + workOrder);

				APIResponse apiResponse = crmServiceCall.callSiebelServiceForCreateUpdateWorkOrder(workOrder);

				if (apiResponse != null && apiResponse.getData() != null) {

					ObjectMapper mapper = new ObjectMapper();

					String stringResponse = apiResponse.getData().toString();

					fromSiebel = mapper.readValue(stringResponse, CreateUpdateWorkOrderResponseFromSiebel.class);

				}

				logger.info("Prospect creation result for customer crmWorkOrderUpdateQueue " + workOrder + " :: "
						+ apiResponse + " fromSiebel " + fromSiebel);

			} catch (Exception e) {
				e.printStackTrace();
			}

			logger.info("WO creation result for customer for crmWorkOrderUpdateQueue" + workOrder.getWorkOrderNumber()
					+ " :: ");

		}

	};

	Runnable crmFrUpdateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_UPDATE_FR_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final FaultRepairActivityUpdateInCRMVo prospect = crmFRUpdateQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (prospect != null) {
										logger.info(
												prospect + " and current depth crmFrUpdateQueue after processing is "
														+ crmFRUpdateQueue.size());

										// Create Prospect
										crmFrUpdate(prospect);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmFrUpdateQueue " + " " + prospect + ""
											+ e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmFrUpdateQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmFrUpdateQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmFrUpdateQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void crmFrUpdate(FaultRepairActivityUpdateInCRMVo prospect) throws Exception {
			logger.info("Creating FR WO for customer for crmFrUpdateQueue" + prospect);

			CreateUpdateFaultRepairResponseFromSiebel fromSiebel = null;

			APIResponse apiResponse = crmServiceCall.callSiebelServiceForCreateUpdateFaultRepair(prospect);

			if (apiResponse != null && apiResponse.getData() != null) {

				ObjectMapper mapper = new ObjectMapper();

				String stringResponse = apiResponse.getData().toString();

				fromSiebel = mapper.readValue(stringResponse, CreateUpdateFaultRepairResponseFromSiebel.class);

			}

			logger.info("Prospect creation result for customer crmWorkOrderUpdateQueue " + prospect + " :: "
					+ apiResponse + " fromSiebel " + fromSiebel);

		}

	};

	Runnable crmProspectUpdateThreadRun = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_EXTERNAL_UPDATE_PROSPECT_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final SalesActivityUpdateInCRMVo prospect = crmProspectUpdateQueue.take();

						logger.info("SalesActivityUpdateInCRMVo in thread " + prospect);

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (prospect != null) {
										logger.info(
												prospect + " and current depth crmProspectUpdateQueue after processing is "
														+ crmProspectUpdateQueue.size());

										// Create Prospect
										updateProspect(prospect);

									}

								} catch (Exception e) {
									e.printStackTrace();
									logger.error("Error in prospect thread crmProspectUpdateQueue " + " " + prospect
											+ "" + e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						logger.error("Error in prospect main thread crmProspectUpdateQueue " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				logger.error("error in crmProspectUpdateQueue Processor main loop- msg= " + e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor crmProspectUpdateQueue thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private ProspectResponseFromSiebel updateProspect(SalesActivityUpdateInCRMVo prospect) throws Exception {
			ProspectResponseFromSiebel fromSiebel = null;

			try {
				logger.info("Creating prospect for customer for crmProspectUpdateQueue" + prospect);

				APIResponse apiResponse = crmServiceCall.callSiebelServiceForCreateUpdateProspect(prospect);

				if (apiResponse != null && apiResponse.getData() != null) {

					ObjectMapper mapper = new ObjectMapper();

					String stringResponse = apiResponse.getData().toString();

					fromSiebel = mapper.readValue(stringResponse, ProspectResponseFromSiebel.class);

				}

				logger.info("Prospect updation result for customer crmProspectUpdateQueue " + prospect + " :: "
						+ apiResponse + " fromSiebel " + fromSiebel);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return fromSiebel;
		}

	};

	private CreateUpdateProspectRequestInSiebel getSiebelFormatForSales(SalesActivityUpdateInCRMVo prospect) {

		CreateUpdateProspectRequestInSiebel createUpdateProspectInSiebel = null;

		try {

			logger.info("SalesActivityUpdateInCRMVo value in CRMUpdateRequestHandler" + prospect);

			createUpdateProspectInSiebel = crmDao.getSalesSiebelFormat(prospect);

			Log.info("createUpdateProspectInSiebel result comes from commontoolutil in crmupdaterequsthandler::"
					+ createUpdateProspectInSiebel);

			if (createUpdateProspectInSiebel.getCreateUpdateProspect_Input().getListOfActcreateprospectthinio()
					.getActListMgmtProspectiveContactBc().getProspectAssignedTo() != null) {

				String assignedToId = createUpdateProspectInSiebel.getCreateUpdateProspect_Input()
						.getListOfActcreateprospectthinio().getActListMgmtProspectiveContactBc()
						.getProspectAssignedTo();

				String assignedToName = userDao.getUserNameByUserId(Long.valueOf(assignedToId));

				createUpdateProspectInSiebel.getCreateUpdateProspect_Input().getListOfActcreateprospectthinio()
						.getActListMgmtProspectiveContactBc().setProspectAssignedTo(assignedToName.toUpperCase());
			}
			logger.info("Response from crmdao for CreateUpdateProspectRequestInSiebel " + createUpdateProspectInSiebel);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return createUpdateProspectInSiebel;
	}

	private CreateUpdateWorkOrderRequestInSiebel getSiebelFormatForWorkOrder(WorkOrderActivityUpdateInCRMVo prospect, String activityId) {

		CreateUpdateWorkOrderRequestInSiebel createUpdateWorkOrderRequestInSiebel = null;

		try {

			createUpdateWorkOrderRequestInSiebel = crmDao.getWorkOrderSiebelFormat(prospect);

			Long prioritySouceLong = createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input()
					.getListOfComworkorderio().getComWorkOrder_Orders().getListOfComWorkOrder_Orders_ACTPriotization()
					.getComWorkOrder_Orders_ACTPriotization().getPrioritySource() != null
							? Long.valueOf(
									createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input()
											.getListOfComworkorderio()
											.getComWorkOrder_Orders().getListOfComWorkOrder_Orders_ACTPriotization()
											.getComWorkOrder_Orders_ACTPriotization().getPrioritySource())
							: null;

		
			logger.info("prioritySouceLong value is coming::" + prioritySouceLong);
			
			String loginId = null;
			if (createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input().getListOfComworkorderio()
					.getComWorkOrder_Orders().getAssignedTo() != null) {

				// UserVo uservo= UserDaoImpl.cachedUsersById
				// .get(createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input()
				// .getListOfComworkorderio().getComWorkOrder_Orders().getAssignedTo());

				UserVo uservo = userDao
						.getUserById(Long.valueOf(createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input()
								.getListOfComworkorderio().getComWorkOrder_Orders().getAssignedTo()));

				logger.info("uservouservouservo:::" + uservo);

				loginId = uservo.getLoginId();

			}

			createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input().getListOfComworkorderio()
					.getComWorkOrder_Orders().setAssignedTo(loginId.toUpperCase());


			if (prospect.getActivityStage() != null && prospect.getActivityStage().equalsIgnoreCase(TicketUpdateConstant.ACTIVITY_ROLLBACK)) {
				for (Action action : createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input()
						.getListOfComworkorderio().getComWorkOrder_Orders().getListOfAction().getAction()) {
					action.setStatus(SiebelWorkOrderStatus.ROLLBACK);
					if(activityId != null)
					action.setActivityType(activityId);
				}
			}
			
			
			if (prioritySouceLong != null && prioritySouceLong > 0 ) {
				
				String prioritySouce = definitionCoreService.getEscalationTypeByKey(prioritySouceLong);
				
				createUpdateWorkOrderRequestInSiebel.getUpdateWorkOrder_Input().getListOfComworkorderio()
						.getComWorkOrder_Orders().getListOfComWorkOrder_Orders_ACTPriotization()
						.getComWorkOrder_Orders_ACTPriotization().setPrioritySource(prioritySouce);
			}
			logger.info("consolidated assignedto user id:::" + createUpdateWorkOrderRequestInSiebel
					.getUpdateWorkOrder_Input().getListOfComworkorderio().getComWorkOrder_Orders().getAssignedTo());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return createUpdateWorkOrderRequestInSiebel;
	}

	private CreateUpdateFaultRepairRequestInSiebel getSiebelFormatForFaultRepair(
			FaultRepairActivityUpdateInCRMVo faultRepair) {

		logger.info("Getting fr details from DB " + faultRepair.getTicketId());

		CreateUpdateFaultRepairRequestInSiebel createUpdateProspectInSiebel = null;

		try {
			createUpdateProspectInSiebel = crmDao.getFaultRepairsSiebelFormat(faultRepair);
			logger.info("got fr details from DB " + createUpdateProspectInSiebel + " for " + faultRepair.getTicketId());

			/*Long prioritySouceLong = createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio()
					.getServiceRequest_Lightweight().getListOfServiceRequest_Lightweight_ACTPriotization()
					.getServiceRequest_Lightweight_ACTPriotization().getPrioritySource() != null
							? Long.valueOf(createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio()
									.getServiceRequest_Lightweight()
									.getListOfServiceRequest_Lightweight_ACTPriotization()
									.getServiceRequest_Lightweight_ACTPriotization().getPrioritySource())
							: null;*/
			
			if(createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio()
					.getServiceRequest_Lightweight().getListOfServiceRequest_Lightweight_ACTPriotization()
					.getServiceRequest_Lightweight_ACTPriotization().getPrioritySource() != null){
				
				Long prioritySouceLong=Long.valueOf(createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio()
						.getServiceRequest_Lightweight().getListOfServiceRequest_Lightweight_ACTPriotization()
						.getServiceRequest_Lightweight_ACTPriotization().getPrioritySource());
				
				logger.info("prioritySouceLong value in crm update request handler::"+prioritySouceLong);
				
				String prioritySouce = definitionCoreService.getEscalationTypeByKey(prioritySouceLong);
				
				logger.info("prioritySouce value in crmupdaterequest handler::"+prioritySouce);
				
				createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio().getServiceRequest_Lightweight()
						.getListOfServiceRequest_Lightweight_ACTPriotization()
						.getServiceRequest_Lightweight_ACTPriotization().setPrioritySource(prioritySouce);
				
			}
							
		if (createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio()
					.getServiceRequest_Lightweight().getOwner() != null) {
				logger.info("consolidated assignedto user id:::" + createUpdateProspectInSiebel.getFWMPSRUpsert_Input()
						.getListOfFwmpsrthinio().getServiceRequest_Lightweight().getOwner());

				String OwnerId = createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio()
						.getServiceRequest_Lightweight().getOwner();

				logger.info("Fr request is responsible for owner::" + OwnerId);

				String OwnerName = userDao.getUserNameByUserId(Long.valueOf(OwnerId));

				createUpdateProspectInSiebel.getFWMPSRUpsert_Input().getListOfFwmpsrthinio()
						.getServiceRequest_Lightweight().setOwner(OwnerName.toUpperCase());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return createUpdateProspectInSiebel;
	}

}
