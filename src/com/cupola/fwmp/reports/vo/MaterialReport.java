package com.cupola.fwmp.reports.vo;

public class MaterialReport
{
	public String customerName;
	private String customerAddress;
	private String phoneMobile;
	private String area;
	private String subStatusId;
	private String approachDate;
	private String workOrderNo;
	private String mqId;
	private String woType;
	private String caf;
	private String tariffPlan;
	private String ne;

	private String woDate;

	private String branchName;
	private String cityName;

	private String batteryFixing = "NA";
	private String cx = "NA";
	private String copperCablePulledFromCxtoCPE = "NA";
	private String fibre = "NA";
	private String imsCabinet = "NA";
	private String flexiblePipe = "NA";
	private String coreElectricWire = "NA";
	private String femaleSocket = "NA";
	private String cxRack = "NA";
	private String drum = "NA";
	private String router = "NA";
	private String rj45Boots = "NA";
	private String padLock = "NA";

	private String fiberType = "NA";
	private String rj45Connectors = "NA";
	private String sfp = "NA";
	private String patchChord = "NA";

	public String getCustomerName()
	{
		return customerName;
	}

	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}

	public String getCustomerAddress()
	{
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress)
	{
		this.customerAddress = customerAddress;
	}

	public String getPhoneMobile()
	{
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile)
	{
		this.phoneMobile = phoneMobile;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public String getSubStatusId()
	{
		return subStatusId;
	}

	public void setSubStatusId(String subStatusId)
	{
		this.subStatusId = subStatusId;
	}

	public String getApproachDate()
	{
		return approachDate;
	}

	public void setApproachDate(String approachDate)
	{
		this.approachDate = approachDate;
	}

	public String getWorkOrderNo()
	{
		return workOrderNo;
	}

	public void setWorkOrderNo(String workOrderNo)
	{
		this.workOrderNo = workOrderNo;
	}

	public String getMqId()
	{
		return mqId;
	}

	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}

	public String getWoType()
	{
		return woType;
	}

	public void setWoType(String woType)
	{
		this.woType = woType;
	}

	public String getNe()
	{
		return ne;
	}

	public void setNe(String ne)
	{
		this.ne = ne;
	}

	public String getWoDate()
	{
		return woDate;
	}

	public void setWoDate(String woDate)
	{
		this.woDate = woDate;
	}

	public String getBranchName()
	{
		return branchName;
	}

	public void setBranchName(String branchName)
	{
		this.branchName = branchName;
	}

	public String getCityName()
	{
		return cityName;
	}

	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}

	public String getBatteryFixing()
	{
		return batteryFixing;
	}

	public void setBatteryFixing(String batteryFixing)
	{
		this.batteryFixing = batteryFixing;
	}

	public String getCx()
	{
		return cx;
	}

	public void setCx(String cx)
	{
		this.cx = cx;
	}

	public String getCopperCablePulledFromCxtoCPE()
	{
		return copperCablePulledFromCxtoCPE;
	}

	public void setCopperCablePulledFromCxtoCPE(
			String copperCablePulledFromCxtoCPE)
	{
		this.copperCablePulledFromCxtoCPE = copperCablePulledFromCxtoCPE;
	}

	public String getFibre()
	{
		return fibre;
	}

	public void setFibre(String fibre)
	{
		this.fibre = fibre;
	}

	public String getImsCabinet()
	{
		return imsCabinet;
	}

	public void setImsCabinet(String imsCabinet)
	{
		this.imsCabinet = imsCabinet;
	}

	public String getFlexiblePipe()
	{
		return flexiblePipe;
	}

	public void setFlexiblePipe(String flexiblePipe)
	{
		this.flexiblePipe = flexiblePipe;
	}

	 

	public String getFemaleSocket()
	{
		return femaleSocket;
	}

	public void setFemaleSocket(String femaleSocket)
	{
		this.femaleSocket = femaleSocket;
	}

	public String getCxRack()
	{
		return cxRack;
	}

	public void setCxRack(String cxRack)
	{
		this.cxRack = cxRack;
	}

	public String getDrum()
	{
		return drum;
	}

	public void setDrum(String drum)
	{
		this.drum = drum;
	}

	public String getRouter()
	{
		return router;
	}

	public void setRouter(String router)
	{
		this.router = router;
	}

	public String getRj45Boots()
	{
		return rj45Boots;
	}

	public void setRj45Boots(String rj45Boots)
	{
		this.rj45Boots = rj45Boots;
	}

	public String getPadLock()
	{
		return padLock;
	}

	public void setPadLock(String padLock)
	{
		this.padLock = padLock;
	}

	 
	public String getRj45Connectors()
	{
		return rj45Connectors;
	}

	public void setRj45Connectors(String rj45Connectors)
	{
		this.rj45Connectors = rj45Connectors;
	}

	public String getSfp()
	{
		return sfp;
	}

	public void setSfp(String sfp)
	{
		this.sfp = sfp;
	}

	public String getPatchChord()
	{
		return patchChord;
	}

	public void setPatchChord(String patchChord)
	{
		this.patchChord = patchChord;
	}

	public String getCoreElectricWire()
	{
		return coreElectricWire;
	}

	public void setCoreElectricWire(String coreElectricWire)
	{
		this.coreElectricWire = coreElectricWire;
	}

	public String getFiberType()
	{
		return fiberType;
	}

	public void setFiberType(String fiberType)
	{
		this.fiberType = fiberType;
	}

	public String getCaf()
	{
		return caf;
	}

	public void setCaf(String caf)
	{
		this.caf = caf;
	}

	public String getTariffPlan()
	{
		return tariffPlan;
	}

	public void setTariffPlan(String tariffPlan)
	{
		this.tariffPlan = tariffPlan;
	}

}
