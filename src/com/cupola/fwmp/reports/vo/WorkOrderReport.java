package com.cupola.fwmp.reports.vo;

public class WorkOrderReport
{
	public String dcrNo;
	String customerName;
	String customerAddress;
	String phoneMobile;
	String area;
	String branch;
	String city;
	String substatusId;
	String workOrderNo;
	String mqId;
	String approachDate;
	String woAssignedTo;
	String fxPortNumber;
	String cxName;
	String cxPortNumber;
	String feasibilityGivenBy;
	String feasibilityDate;
	String woGeneratedDate;
	String woClosedDate;
	String alName;                      
	String woType;
	String rackFixedBy;
	String noOfSplicing;                 
	String splicingDoneBy;              
	String fiberDoneBy;                 
	String batteryInstalled;
	String batteryOneDetails;
	String modifiedFxList;
	String modifiedCxList;
	String accountActivation;
	String demo;
	String fxName;
	String remarks;

	public String getDcrNo()
	{
		return dcrNo;
	}

	public void setDcrNo(String dcrNo)
	{
		this.dcrNo = dcrNo;
	}

	public String getCustomerName()
	{
		return customerName;
	}

	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}

	public String getCustomerAddress()
	{
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress)
	{
		this.customerAddress = customerAddress;
	}

	public String getPhoneMobile()
	{
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile)
	{
		this.phoneMobile = phoneMobile;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public String getSubstatusId()
	{
		return substatusId;
	}

	public void setSubstatusId(String substatusId)
	{
		this.substatusId = substatusId;
	}

	public String getWorkOrderNo()
	{
		return workOrderNo;
	}

	public void setWorkOrderNo(String workOrderNo)
	{
		this.workOrderNo = workOrderNo;
	}

	public String getMqId()
	{
		return mqId;
	}

	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}

	public String getApproachDate()
	{
		return approachDate;
	}

	public void setApproachDate(String approachDate)
	{
		this.approachDate = approachDate;
	}

	public String getWoAssignedTo()
	{
		return woAssignedTo;
	}

	public void setWoAssignedTo(String woAssignedTo)
	{
		this.woAssignedTo = woAssignedTo;
	}

	public String getFxPortNumber()
	{
		return fxPortNumber;
	}

	public void setFxPortNumber(String fxPortNumber)
	{
		this.fxPortNumber = fxPortNumber;
	}

	public String getCxName()
	{
		return cxName;
	}

	public void setCxName(String cxName)
	{
		this.cxName = cxName;
	}

	public String getCxPortNumber()
	{
		return cxPortNumber;
	}

	public void setCxPortNumber(String cxPortNumber)
	{
		this.cxPortNumber = cxPortNumber;
	}

	public String getFeasibilityGivenBy()
	{
		return feasibilityGivenBy;
	}

	public void setFeasibilityGivenBy(String feasibilityGivenBy)
	{
		this.feasibilityGivenBy = feasibilityGivenBy;
	}

	public String getFeasibilityDate()
	{
		return feasibilityDate;
	}

	public void setFeasibilityDate(String feasibilityDate)
	{
		this.feasibilityDate = feasibilityDate;
	}

	public String getWoGeneratedDate()
	{
		return woGeneratedDate;
	}

	public void setWoGeneratedDate(String woGeneratedDate)
	{
		this.woGeneratedDate = woGeneratedDate;
	}

	public String getWoClosedDate()
	{
		return woClosedDate;
	}

	public void setWoClosedDate(String woClosedDate)
	{
		this.woClosedDate = woClosedDate;
	}

	public String getAlName()
	{
		return alName;
	}

	public void setAlName(String alName)
	{
		this.alName = alName;
	}

	public String getWoType()
	{
		return woType;
	}

	public void setWoType(String woType)
	{
		this.woType = woType;
	}

	public String getRackFixedBy()
	{
		return rackFixedBy;
	}

	public void setRackFixedBy(String rackFixedBy)
	{
		this.rackFixedBy = rackFixedBy;
	}

	public String getNoOfSplicing()
	{
		return noOfSplicing;
	}

	public void setNoOfSplicing(String noOfSplicing)
	{
		this.noOfSplicing = noOfSplicing;
	}

	public String getSplicingDoneBy()
	{
		return splicingDoneBy;
	}

	public void setSplicingDoneBy(String splicingDoneBy)
	{
		this.splicingDoneBy = splicingDoneBy;
	}

	public String getFiberDoneBy()
	{
		return fiberDoneBy;
	}

	public void setFiberDoneBy(String fiberDoneBy)
	{
		this.fiberDoneBy = fiberDoneBy;
	}

	public String getBatteryInstalled()
	{
		return batteryInstalled;
	}

	public void setBatteryInstalled(String batteryInstalled)
	{
		this.batteryInstalled = batteryInstalled;
	}

	public String getBatteryOneDetails()
	{
		return batteryOneDetails;
	}

	public void setBatteryOneDetails(String batteryOneDetails)
	{
		this.batteryOneDetails = batteryOneDetails;
	}

	public String getModifiedFxList()
	{
		return modifiedFxList;
	}

	public void setModifiedFxList(String modifiedFxList)
	{
		this.modifiedFxList = modifiedFxList;
	}

	public String getModifiedCxList()
	{
		return modifiedCxList;
	}

	public void setModifiedCxList(String modifiedCxList)
	{
		this.modifiedCxList = modifiedCxList;
	}

	public String getAccountActivation()
	{
		return accountActivation;
	}

	public void setAccountActivation(String accountActivation)
	{
		this.accountActivation = accountActivation;
	}

	public String getDemo()
	{
		return demo;
	}

	public void setDemo(String demo)
	{
		this.demo = demo;
	}

	public String getFxName()
	{
		return fxName;
	}

	public void setFxName(String fxName)
	{
		this.fxName = fxName;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public  String getBranch()
	{
		return branch;
	}

	public  void setBranch(String branch)
	{
		this.branch = branch;
	}

	public  String getCity()
	{
		return city;
	}

	public  void setCity(String city)
	{
		this.city = city;
	}

}
