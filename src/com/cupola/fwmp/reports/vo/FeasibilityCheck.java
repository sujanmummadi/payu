package com.cupola.fwmp.reports.vo;

public class FeasibilityCheck
{
	private String dcrNO;
	private String currentPhoneMob;
	private String currentPhoneRes;
	private String currentPhoneOff;
	private String areaName;
	private String branchName;
	private String cityName;
	private String commitmentDays;
	
	private String seName;
	private String neName;
	
	private String fxNameByse;
	private String cxNameByse;
	private String fxPortByse;
	private String cxPortByse;
	private String connectionTypeByse;
	private String feasibilityDateByse;
	
	private String fxNameByne;
	private String cxNameByne;
	private String fxPortByne;
	private String cxPortByne;
	private String connectionTypeByne;
	private String feasibilityDateByne;
	
	private String fxNameByaat;
	private String cxNameByaat;
	private String fxPortByaat;
	private String cxPortByaat;
	private String conaatctionTypeByaat;
	private String feasibilityDateByaat;
	private String remarks;
	
	public  String getDcrNO()
	{
		return dcrNO;
	}
	public  void setDcrNO(String dcrNO)
	{
		this.dcrNO = dcrNO;
	}
	public  String getCurrentPhoneMob()
	{
		return currentPhoneMob;
	}
	public  void setCurrentPhoneMob(String currentPhoneMob)
	{
		this.currentPhoneMob = currentPhoneMob;
	}
	public  String getCurrentPhoneRes()
	{
		return currentPhoneRes;
	}
	public  void setCurrentPhoneRes(String currentPhoneRes)
	{
		this.currentPhoneRes = currentPhoneRes;
	}
	public  String getCurrentPhoneOff()
	{
		return currentPhoneOff;
	}
	public  void setCurrentPhoneOff(String currentPhoneOff)
	{
		this.currentPhoneOff = currentPhoneOff;
	}
	public  String getAreaName()
	{
		return areaName;
	}
	public  void setAreaName(String areaName)
	{
		this.areaName = areaName;
	}
	public  String getBranchName()
	{
		return branchName;
	}
	public  void setBranchName(String branchName)
	{
		this.branchName = branchName;
	}
	public  String getCityName()
	{
		return cityName;
	}
	public  void setCityName(String cityName)
	{
		this.cityName = cityName;
	}
	public  String getCommitmentDays()
	{
		return commitmentDays;
	}
	public  void setCommitmentDays(String commitmentDays)
	{
		this.commitmentDays = commitmentDays;
	}
	public  String getSeName()
	{
		return seName;
	}
	public  void setSeName(String seName)
	{
		this.seName = seName;
	}
	public  String getNeName()
	{
		return neName;
	}
	public  void setNeName(String neName)
	{
		this.neName = neName;
	}
	public  String getFxNameByse()
	{
		return fxNameByse;
	}
	public  void setFxNameByse(String fxNameByse)
	{
		this.fxNameByse = fxNameByse;
	}
	public  String getCxNameByse()
	{
		return cxNameByse;
	}
	public  void setCxNameByse(String cxNameByse)
	{
		this.cxNameByse = cxNameByse;
	}
	public  String getFxPortByse()
	{
		return fxPortByse;
	}
	public  void setFxPortByse(String fxPortByse)
	{
		this.fxPortByse = fxPortByse;
	}
	public  String getCxPortByse()
	{
		return cxPortByse;
	}
	public  void setCxPortByse(String cxPortByse)
	{
		this.cxPortByse = cxPortByse;
	}
	public  String getConnectionTypeByse()
	{
		return connectionTypeByse;
	}
	public  void setConnectionTypeByse(String connectionTypeByse)
	{
		this.connectionTypeByse = connectionTypeByse;
	}
	public  String getFeasibilityDateByse()
	{
		return feasibilityDateByse;
	}
	public  void setFeasibilityDateByse(String feasibilityDateByse)
	{
		this.feasibilityDateByse = feasibilityDateByse;
	}
	public  String getFxNameByne()
	{
		return fxNameByne;
	}
	public  void setFxNameByne(String fxNameByne)
	{
		this.fxNameByne = fxNameByne;
	}
	public  String getCxNameByne()
	{
		return cxNameByne;
	}
	public  void setCxNameByne(String cxNameByne)
	{
		this.cxNameByne = cxNameByne;
	}
	public  String getFxPortByne()
	{
		return fxPortByne;
	}
	public  void setFxPortByne(String fxPortByne)
	{
		this.fxPortByne = fxPortByne;
	}
	public  String getCxPortByne()
	{
		return cxPortByne;
	}
	public  void setCxPortByne(String cxPortByne)
	{
		this.cxPortByne = cxPortByne;
	}
	public  String getConnectionTypeByne()
	{
		return connectionTypeByne;
	}
	public  void setConnectionTypeByne(String connectionTypeByne)
	{
		this.connectionTypeByne = connectionTypeByne;
	}
	public  String getFeasibilityDateByne()
	{
		return feasibilityDateByne;
	}
	public  void setFeasibilityDateByne(String feasibilityDateByne)
	{
		this.feasibilityDateByne = feasibilityDateByne;
	}
	public  String getFxNameByaat()
	{
		return fxNameByaat;
	}
	public  void setFxNameByaat(String fxNameByaat)
	{
		this.fxNameByaat = fxNameByaat;
	}
	public  String getCxNameByaat()
	{
		return cxNameByaat;
	}
	public  void setCxNameByaat(String cxNameByaat)
	{
		this.cxNameByaat = cxNameByaat;
	}
	public  String getFxPortByaat()
	{
		return fxPortByaat;
	}
	public  void setFxPortByaat(String fxPortByaat)
	{
		this.fxPortByaat = fxPortByaat;
	}
	public  String getCxPortByaat()
	{
		return cxPortByaat;
	}
	public  void setCxPortByaat(String cxPortByaat)
	{
		this.cxPortByaat = cxPortByaat;
	}
	public  String getConaatctionTypeByaat()
	{
		return conaatctionTypeByaat;
	}
	public  void setConaatctionTypeByaat(String conaatctionTypeByaat)
	{
		this.conaatctionTypeByaat = conaatctionTypeByaat;
	}
	public  String getFeasibilityDateByaat()
	{
		return feasibilityDateByaat;
	}
	public  void setFeasibilityDateByaat(String feasibilityDateByaat)
	{
		this.feasibilityDateByaat = feasibilityDateByaat;
	}
	public String getRemarks()
	{
		return remarks;
	}
	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}
	@Override
	public String toString()
	{
		return "FeasibilityCheck [dcrNO=" + dcrNO + ", currentPhoneMob="
				+ currentPhoneMob + ", currentPhoneRes=" + currentPhoneRes
				+ ", currentPhoneOff=" + currentPhoneOff + ", areaName="
				+ areaName + ", branchName=" + branchName + ", cityName="
				+ cityName + ", commitmentDays=" + commitmentDays + ", seName="
				+ seName + ", neName=" + neName + ", fxNameByse=" + fxNameByse
				+ ", cxNameByse=" + cxNameByse + ", fxPortByse=" + fxPortByse
				+ ", cxPortByse=" + cxPortByse + ", connectionTypeByse="
				+ connectionTypeByse + ", feasibilityDateByse="
				+ feasibilityDateByse + ", fxNameByne=" + fxNameByne
				+ ", cxNameByne=" + cxNameByne + ", fxPortByne=" + fxPortByne
				+ ", cxPortByne=" + cxPortByne + ", connectionTypeByne="
				+ connectionTypeByne + ", feasibilityDateByne="
				+ feasibilityDateByne + ", fxNameByaat=" + fxNameByaat
				+ ", cxNameByaat=" + cxNameByaat + ", fxPortByaat="
				+ fxPortByaat + ", cxPortByaat=" + cxPortByaat
				+ ", conaatctionTypeByaat=" + conaatctionTypeByaat
				+ ", feasibilityDateByaat=" + feasibilityDateByaat
				+ ", remarks=" + remarks + "]";
	}
 
}
