package com.cupola.fwmp.reports.vo;

public class SalesDcrDetails
{
	public String dcrNo;
	public String seName;
	public String source;
	String customerName;
	String completeAddress;
	String currentStatus;
	String contactNumber;
	String espTariff;
	String emailId;
	String areaName;
	String branch;
	String city;
	String remarks;
	String cafNo;
	String dcrCreactionDate; 
	String connectionType; 
	String customerType; 
	
	
	
	String feasibilityRaisedTime; //gis se feasibity type 2
	String feasibilityGivenTime;//gis se feasibity type 2

	String docCollectionTime;// uploaded time
	String cafLoginTime; //payment update time
	
	String accountActivationTime; //CAA time

	String commitmentDays;  // initial wo type
	
	
	
	String enquiry;
	String prefferedCallDate;
	String modifiedBy;
	String modifiedOn;
	   
	

	public String getDcrNo()
	{
		return dcrNo;
	}

	public void setDcrNo(String dcrNo)
	{
		this.dcrNo = dcrNo;
	}

	public String getCustomerName()
	{
		return customerName;
	}

	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}

	public String getCompleteAddress()
	{
		return completeAddress;
	}

	public void setCompleteAddress(String completeAddress)
	{
		this.completeAddress = completeAddress;
	}

	public String getCurrentStatus()
	{
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus)
	{
		this.currentStatus = currentStatus;
	}

	public String getFeasibilityRaisedTime()
	{
		return feasibilityRaisedTime;
	}

	public void setFeasibilityRaisedTime(String feasibilityRaisedTime)
	{
		this.feasibilityRaisedTime = feasibilityRaisedTime;
	}

	public String getFeasibilityGivenTime()
	{
		return feasibilityGivenTime;
	}

	public void setFeasibilityGivenTime(String feasibilityGivenTime)
	{
		this.feasibilityGivenTime = feasibilityGivenTime;
	}

	public String getContactNumber()
	{
		return contactNumber;
	}

	public void setContactNumber(String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	public String getDocCollectionTime()
	{
		return docCollectionTime;
	}

	public void setDocCollectionTime(String docCollectionTime)
	{
		this.docCollectionTime = docCollectionTime;
	}

	public String getCafLoginTime()
	{
		return cafLoginTime;
	}

	public void setCafLoginTime(String cafLoginTime)
	{
		this.cafLoginTime = cafLoginTime;
	}

	public String getEspTariff()
	{
		return espTariff;
	}

	public void setEspTariff(String espTariff)
	{
		this.espTariff = espTariff;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	public String getAccountActivationTime()
	{
		return accountActivationTime;
	}

	public void setAccountActivationTime(String accountActivationTime)
	{
		this.accountActivationTime = accountActivationTime;
	}

	public String getAreaName()
	{
		return areaName;
	}

	public void setAreaName(String areaName)
	{
		this.areaName = areaName;
	}

	public String getDcrCreactionDate()
	{
		return dcrCreactionDate;
	}

	public void setDcrCreactionDate(String dcrCreactionDate)
	{
		this.dcrCreactionDate = dcrCreactionDate;
	}

	public String getCommitmentDays()
	{
		return commitmentDays;
	}

	public void setCommitmentDays(String commitmentDays)
	{
		this.commitmentDays = commitmentDays;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getCafNo()
	{
		return cafNo;
	}

	public void setCafNo(String cafNo)
	{
		this.cafNo = cafNo;
	}

	public synchronized String getConnectionType()
	{
		return connectionType;
	}

	public  void setConnectionType(String connectionType)
	{
		this.connectionType = connectionType;
	}

	public  String getCustomerType()
	{
		return customerType;
	}

	public  void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}

	public  String getSeName()
	{
		return seName;
	}

	public  void setSeName(String seName)
	{
		this.seName = seName;
	}

	public  String getBranch()
	{
		return branch;
	}

	public  void setBranch(String branch)
	{
		this.branch = branch;
	}

	public  String getCity()
	{
		return city;
	}

	public  void setCity(String city)
	{
		this.city = city;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getEnquiry() {
		return enquiry;
	}

	public void setEnquiry(String enquiry) {
		this.enquiry = enquiry;
	}

	public String getPrefferedCallDate() {
		return prefferedCallDate;
	}

	public void setPrefferedCallDate(String prefferedCallDate) {
		this.prefferedCallDate = prefferedCallDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	
	
	
}
