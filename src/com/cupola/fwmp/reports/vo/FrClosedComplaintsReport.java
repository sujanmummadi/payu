 /**
 * @Author leela  Jun 7, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.reports.vo;

import java.util.Date;

 /**
 * @Author leela  Jun 7, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrClosedComplaintsReport {
	  
	private Date valueDate;
	private Date modifiedOn;
	private String workOrderNumber;
	private String mqId;
	private String areaName;
	private String fxName;
	private String cxIp;
	private String communicationAdderss;
	private String empName;
	private String custName;
	private String branchName;
	private String ticketCategory;
	private String resCode;
	private String problemDesc;
	private String duration;
	private String aging;
	private String voc;
	private String closedBy;
	private String reopenCount;
	private Date communicationETR;
	private Date ticketClosedTime;
	private String defectCode;
	private String subDefectCode;
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getFxName() {
		return fxName;
	}
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	 
	public String getCxIp() {
		return cxIp;
	}
	public void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getTicketCategory() {
		return ticketCategory;
	}
	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}
	public String getResCode() {
		return resCode;
	}
	public void setResCode(String resCode) {
		this.resCode = resCode;
	}
	public String getProblemDesc() {
		return problemDesc;
	}
	public void setProblemDesc(String problemDesc) {
		this.problemDesc = problemDesc;
	}
	 
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getAging() {
		return aging;
	}
	public void setAging(String aging) {
		this.aging = aging;
	}
	public String getVoc() {
		return voc;
	}
	public void setVoc(String voc) {
		this.voc = voc;
	}
	public String getClosedBy() {
		return closedBy;
	}
	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}
	public String getReopenCount() {
		return reopenCount;
	}
	public void setReopenCount(String reopenCount) {
		this.reopenCount = reopenCount;
	}
	public Date getCommunicationETR() {
		return communicationETR;
	}
	public void setCommunicationETR(Date communicationETR) {
		this.communicationETR = communicationETR;
	}
	public Date getTicketClosedTime() {
		return ticketClosedTime;
	}
	public void setTicketClosedTime(Date ticketClosedTime) {
		this.ticketClosedTime = ticketClosedTime;
	}
	public String getDefectCode() {
		return defectCode;
	}
	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}
	public String getSubDefectCode() {
		return subDefectCode;
	}
	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	@Override
	public String toString() {
		return "FrClosedComplaintsReport [valueDate=" + valueDate
				+ ", modifiedOn=" + modifiedOn + ", workOrderNumber="
				+ workOrderNumber + ", mqId=" + mqId + ", areaName=" + areaName
				+ ", fxName=" + fxName + ", cxIp=" + cxIp
				+ ", communicationAdderss=" + communicationAdderss
				+ ", empName=" + empName + ", custName=" + custName
				+ ", branchName=" + branchName + ", ticketCategory="
				+ ticketCategory + ", resCode=" + resCode + ", problemDesc="
				+ problemDesc + ", duration=" + duration + ", aging=" + aging
				+ ", voc=" + voc + ", closedBy=" + closedBy + ", reopenCount="
				+ reopenCount + ", communicationETR=" + communicationETR
				+ ", ticketClosedTime=" + ticketClosedTime + ", defectCode="
				+ defectCode + ", subDefectCode=" + subDefectCode + "]";
	}
	 
	 
	 

}
