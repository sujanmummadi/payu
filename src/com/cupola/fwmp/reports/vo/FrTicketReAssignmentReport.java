 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.reports.vo;

import java.util.Date;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrTicketReAssignmentReport {
	
	private String ticketNo;
	private String city;
	private String branch;
	private String area;
	private String subArea;
	private String ticketCategory;
	private String ticketNature;
	
	private String assignedFromName;
	private String assignedFromUid;
	
	private String assignedToName;
	private String assignedToUid;
	
	private String assignedByName;
	private String assignedByUid;
	
	private Date addedOn;

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSubArea() {
		return subArea;
	}

	public void setSubArea(String subArea) {
		this.subArea = subArea;
	}

	public String getTicketCategory() {
		return ticketCategory;
	}

	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}

	public String getTicketNature() {
		return ticketNature;
	}

	public void setTicketNature(String ticketNature) {
		this.ticketNature = ticketNature;
	}

	public String getAssignedFromName() {
		return assignedFromName;
	}

	public void setAssignedFromName(String assignedFromName) {
		this.assignedFromName = assignedFromName;
	}

	public String getAssignedFromUid() {
		return assignedFromUid;
	}

	public void setAssignedFromUid(String assignedFromUid) {
		this.assignedFromUid = assignedFromUid;
	}

	public String getAssignedToName() {
		return assignedToName;
	}

	public void setAssignedToName(String assignedToName) {
		this.assignedToName = assignedToName;
	}

	public String getAssignedToUid() {
		return assignedToUid;
	}

	public void setAssignedToUid(String assignedToUid) {
		this.assignedToUid = assignedToUid;
	}

	public String getAssignedByName() {
		return assignedByName;
	}

	public void setAssignedByName(String assignedByName) {
		this.assignedByName = assignedByName;
	}

	public String getAssignedByUid() {
		return assignedByUid;
	}

	public void setAssignedByUid(String assignedByUid) {
		this.assignedByUid = assignedByUid;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	@Override
	public String toString() {
		return "FrTicketReAssignmentReport [ticketNo=" + ticketNo + ", city="
				+ city + ", branch=" + branch + ", area=" + area + ", subArea="
				+ subArea + ", ticketCategory=" + ticketCategory
				+ ", ticketNature=" + ticketNature + ", assignedFromName="
				+ assignedFromName + ", assignedFromUid=" + assignedFromUid
				+ ", assignedToName=" + assignedToName + ", assignedToUid="
				+ assignedToUid + ", assignedByName=" + assignedByName
				+ ", assignedByUid=" + assignedByUid + ", addedOn=" + addedOn
				+ "]";
	}
	 
	 
	  
	
}
