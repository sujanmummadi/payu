 /**
 * @Author leela  Jun 8, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.reports.vo;

import java.util.Date;

 /**
 * @Author leela  Jun 8, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrNewLightWeight {
	
	private Date valueDate;
	private Date modifiedOn;
	private String workOrderNumber;
	private String mqId;
	private String areaName;
	private String fxName;
	private String cxName;
	private String communicationAdderss;
	private String status;
	private String empName;
	private String custName;
	private String mobileNumber;
	private String branchName;
	private String complaintDesc;
	private String problemDesc;
	private String duration;
	private String aging;
	private String voc;
	private String reopenCount;
	private Date  communicationETR;
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getFxName() {
		return fxName;
	}
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	public String getCxName() {
		return cxName;
	}
	public void setCxName(String cxName) {
		this.cxName = cxName;
	}
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getComplaintDesc() {
		return complaintDesc;
	}
	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}
	public String getProblemDesc() {
		return problemDesc;
	}
	public void setProblemDesc(String problemDesc) {
		this.problemDesc = problemDesc;
	}
	 
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getAging() {
		return aging;
	}
	public void setAging(String aging) {
		this.aging = aging;
	}
	public String getVoc() {
		return voc;
	}
	public void setVoc(String voc) {
		this.voc = voc;
	}
	public String getReopenCount() {
		return reopenCount;
	}
	public void setReopenCount(String reopenCount) {
		this.reopenCount = reopenCount;
	}
	public Date getCommunicationETR() {
		return communicationETR;
	}
	public void setCommunicationETR(Date communicationETR) {
		this.communicationETR = communicationETR;
	}
	@Override
	public String toString() {
		return "FrNewLightWeight [valueDate=" + valueDate + ", modifiedOn="
				+ modifiedOn + ", workOrderNumber=" + workOrderNumber
				+ ", mqId=" + mqId + ", areaName=" + areaName + ", fxName="
				+ fxName + ", cxName=" + cxName + ", communicationAdderss="
				+ communicationAdderss + ", status=" + status + ", empName="
				+ empName + ", custName=" + custName + ", mobileNumber="
				+ mobileNumber + ", branchName=" + branchName
				+ ", complaintDesc=" + complaintDesc + ", problemDesc="
				+ problemDesc + ", duration=" + duration + ", aging=" + aging
				+ ", voc=" + voc + ", reopenCount=" + reopenCount
				+ ", communicationETR=" + communicationETR + "]";
	}
	 
	 

}
