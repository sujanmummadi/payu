package com.cupola.fwmp.reports.vo;
public class CafVerficationReport {
	private String mqId;
	private String dcrNo ;
	private String customerName; 
	private String phoneMobile;
	private String seName;
	private String area;
	private String branch;
	private String city;
	private String subStatusId;
	private String cafNumber ;
	private String approachDate;
	private String tariffPlanName;
	private String totalSubcPaid ;
	private String docCollectedDate ;
	private String docAccRejectDate;
	private String apDocType;
	private String idpDocType;
	private String cafStatusDate;
	
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public String getDcrNo() {
		return dcrNo;
	}
	public void setDcrNo(String dcrNo) {
		this.dcrNo = dcrNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPhoneMobile() {
		return phoneMobile;
	}
	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getSubStatusId() {
		return subStatusId;
	}
	public void setSubStatusId(String subStatusId) {
		this.subStatusId = subStatusId;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}
	public String getApproachDate() {
		return approachDate;
	}
	public void setApproachDate(String approachDate) {
		this.approachDate = approachDate;
	}
	public String getTariffPlanName() {
		return tariffPlanName;
	}
	public void setTariffPlanName(String tariffPlanName) {
		this.tariffPlanName = tariffPlanName;
	}
	public String getTotalSubcPaid() {
		return totalSubcPaid;
	}
	public void setTotalSubcPaid(String totalSubcPaid) {
		this.totalSubcPaid = totalSubcPaid;
	}
	public String getDocCollectedDate() {
		return docCollectedDate;
	}
	public void setDocCollectedDate(String docCollectedDate) {
		this.docCollectedDate = docCollectedDate;
	}
	public String getDocAccRejectDate() {
		return docAccRejectDate;
	}
	public void setDocAccRejectDate(String docAccRejectDate) {
		this.docAccRejectDate = docAccRejectDate;
	}
	public String getApDocType() {
		return apDocType;
	}
	public void setApDocType(String apDocType) {
		this.apDocType = apDocType;
	}
	public String getIdpDocType() {
		return idpDocType;
	}
	public void setIdpDocType(String idpDocType) {
		this.idpDocType = idpDocType;
	}
	public String getCafStatusDate() {
		return cafStatusDate;
	}
	public void setCafStatusDate(String cafStatusDate) {
		this.cafStatusDate = cafStatusDate;
	}
	@Override
	public String toString() {
		return "CafVerficationReport [mqId=" + mqId + ", dcrNo=" + dcrNo
				+ ", customerName=" + customerName + ", phoneMobile="
				+ phoneMobile + ", area=" + area + ", subStatusId="
				+ subStatusId + ", cafNumber=" + cafNumber + ", approachDate="
				+ approachDate + ", tariffPlanName=" + tariffPlanName
				+ ", totalSubcPaid=" + totalSubcPaid + ", docCollectedDate="
				+ docCollectedDate + ", docAccRejectDate=" + docAccRejectDate
				+ ", apDocType=" + apDocType + ", idpDocType=" + idpDocType
				+ ", cafStatusDate=" + cafStatusDate + "]";
	}
	public  String getBranch()
	{
		return branch;
	}
	public  void setBranch(String branch)
	{
		this.branch = branch;
	}
	public  String getCity()
	{
		return city;
	}
	public  void setCity(String city)
	{
		this.city = city;
	}
	public  String getSeName()
	{
		return seName;
	}
	public  void setSeName(String seName)
	{
		this.seName = seName;
	}
	
	
	
}
