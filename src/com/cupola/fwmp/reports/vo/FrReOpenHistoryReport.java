 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.reports.vo;

import java.util.Date;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrReOpenHistoryReport {
	
	private String mqId;
	private String workOrderNumber;
	private Date ticketCreationDate;
	private String status;
	private String ticketCategory;
	private String currentSymptomName;
	private Date reopenDateTime;
	private String assignedEmpId;
	private String assignedEmpName;
	private String closedByUId;
	private String closedByUName;
	private Date ticketClosedTime;
	private String reopenCount;
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public Date getTicketCreationDate() {
		return ticketCreationDate;
	}
	public void setTicketCreationDate(Date ticketCreationDate) {
		this.ticketCreationDate = ticketCreationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTicketCategory() {
		return ticketCategory;
	}
	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}
	public String getCurrentSymptomName() {
		return currentSymptomName;
	}
	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}
	public Date getReopenDateTime() {
		return reopenDateTime;
	}
	public void setReopenDateTime(Date reopenDateTime) {
		this.reopenDateTime = reopenDateTime;
	}
	public String getAssignedEmpId() {
		return assignedEmpId;
	}
	public void setAssignedEmpId(String assignedEmpId) {
		this.assignedEmpId = assignedEmpId;
	}
	public String getAssignedEmpName() {
		return assignedEmpName;
	}
	public void setAssignedEmpName(String assignedEmpName) {
		this.assignedEmpName = assignedEmpName;
	}
	public String getClosedByUId() {
		return closedByUId;
	}
	public void setClosedByUId(String closedByUId) {
		this.closedByUId = closedByUId;
	}
	public String getClosedByUName() {
		return closedByUName;
	}
	public void setClosedByUName(String closedByUName) {
		this.closedByUName = closedByUName;
	}
	public Date getTicketClosedTime() {
		return ticketClosedTime;
	}
	public void setTicketClosedTime(Date ticketClosedTime) {
		this.ticketClosedTime = ticketClosedTime;
	}
	public String getReopenCount() {
		return reopenCount;
	}
	public void setReopenCount(String reopenCount) {
		this.reopenCount = reopenCount;
	}
	@Override
	public String toString() {
		return "FrReOpenHistoryReport [mqId=" + mqId + ", workOrderNumber="
				+ workOrderNumber + ", ticketCreationDate="
				+ ticketCreationDate + ", status=" + status
				+ ", ticketCategory=" + ticketCategory
				+ ", currentSymptomName=" + currentSymptomName
				+ ", reopenDateTime=" + reopenDateTime + ", assignedEmpId="
				+ assignedEmpId + ", assignedEmpName=" + assignedEmpName
				+ ", closedByUId=" + closedByUId + ", closedByUName="
				+ closedByUName + ", ticketClosedTime=" + ticketClosedTime
				+ ", reopenCount=" + reopenCount + "]";
	}
	
	 
	

}
