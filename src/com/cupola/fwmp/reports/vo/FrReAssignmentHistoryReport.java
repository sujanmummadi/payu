 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.reports.vo;

import java.util.Date;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrReAssignmentHistoryReport {
	
	private Date ticketCreationDate;
	private String currentSymptomName;
	private String workOrderNumber;
	private String mqId;
	private Date communicationETR;
	private String mobileNumber;
	public Date getTicketCreationDate() {
		return ticketCreationDate;
	}
	public void setTicketCreationDate(Date ticketCreationDate) {
		this.ticketCreationDate = ticketCreationDate;
	}
	public String getCurrentSymptomName() {
		return currentSymptomName;
	}
	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public Date getCommunicationETR() {
		return communicationETR;
	}
	public void setCommunicationETR(Date communicationETR) {
		this.communicationETR = communicationETR;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	@Override
	public String toString() {
		return "FrReAssignmentHistoryReport [ticketCreationDate="
				+ ticketCreationDate + ", currentSymptomName="
				+ currentSymptomName + ", workOrderNumber=" + workOrderNumber
				+ ", mqId=" + mqId + ", communicationETR=" + communicationETR
				+ ", mobileNumber=" + mobileNumber + "]";
	}
	 
	 
	
	  
	
	

}
