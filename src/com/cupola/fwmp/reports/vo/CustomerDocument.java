package com.cupola.fwmp.reports.vo;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.util.GenericUtil;


@Document(collection = "CustomerDocument")
public class CustomerDocument
{
	@Id
	private Long documentId;
	private Long ticketId;
	private Long customerId;
	private String displayName;
	private String documentName;
	private int docType;
	private int docSubType;
	private String docNumber;
	private String expireOn;
	private int docApprovalStatus;
	private String remark;
	private int remarkCode;
	private Long modifiedBy;
	private String modifiedOn;
	
	
	public CustomerDocument()
	{
		
	}

	public Long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}
	public Long getCustomerId()
	{
		return customerId;
	}
	public void setCustomerId(Long customerId)
	{
		this.customerId = customerId;
	}
	public String getDisplayName()
	{
		return displayName;
	}
	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
	public int getDocType()
	{
		return docType;
	}
	public void setDocType(int docType)
	{
		this.docType = docType;
	}
	public int getDocSubType()
	{
		return docSubType;
	}
	public void setDocSubType(int docSubType)
	{
		this.docSubType = docSubType;
	}
	public String getDocNumber()
	{
		return docNumber;
	}
	public void setDocNumber(String docNumber)
	{
		this.docNumber = docNumber;
	}
	public String getExpireOn()
	{
		return expireOn;
	}
	public void setExpireOn(String expireOn)
	{
		this.expireOn = expireOn;
	}
	public int getDocApprovalStatus()
	{
		return docApprovalStatus;
	}
	public void setDocApprovalStatus(int docApprovalStatus)
	{
		this.docApprovalStatus = docApprovalStatus;
	}
	public String getRemark()
	{
		return remark;
	}
	public void setRemark(String remark)
	{
		this.remark = remark;
	}
	public int getRemarkCode()
	{
		return remarkCode;
	}
	public void setRemarkCode(int remarkCode)
	{
		this.remarkCode = remarkCode;
	}
	public Long getModifiedBy()
	{
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedOn()
	{
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public CustomerDocument(Long ticketId, Long customerId, int docType,
			int docSubType, Long modifiedBy)
	{
		super();
		this.ticketId = ticketId;
		this.customerId = customerId;
		this.docType = docType;
		this.docSubType = docSubType;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = GenericUtil.convertToUiDateFormat((new Date()));
	}

	public Long getDocumentId()
	{
		return documentId;
	}

	public void setDocumentId(Long documentId)
	{
		this.documentId = documentId;
	}

	public String getDocumentName()
	{
		return documentName;
	}

	public void setDocumentName(String documentName)
	{
		this.documentName = documentName;
	}

	@Override
	public String toString()
	{
		return "CustomerDocument [documentId=" + documentId + ", ticketId="
				+ ticketId + ", customerId=" + customerId + ", displayName="
				+ displayName + ", documentName=" + documentName + "]";
	}
	
}
