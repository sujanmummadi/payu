package com.cupola.fwmp.reports.jasper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

import com.cupola.fwmp.reports.util.PropLoader;
import com.cupola.fwmp.reports.vo.CafVerficationReport;
import com.cupola.fwmp.reports.vo.FeasibilityCheck;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JasperManager {

	private static String JRXML_HOST_PATH = "jrxml.path";
	
	private static String XLS_OUTPUT_PATH = "xls.output.path";
	
	private static Logger log = Logger.getLogger(JasperManager.class
			.getName());

	public static void exportData(String reportData, String reportName,String reportOutputDir)
			throws IOException {

		JasperReport jasperReport = null;
		
		List<Object> objectList = null;

		try {
			
			objectList = createJsonToReportListData(reportData);
			
			String URL= PropLoader.getPropertyForReport(JRXML_HOST_PATH)+ reportName +".jrxml";
			
			jasperReport = JasperCompileManager.compileReport(URL);
			
		} catch (JRException e1) {
			// TODO Auto-generated catch block
			log.error("Error While compiling jasper report :"+e1.getMessage());
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
			
		} catch (JRException e) {
			// TODO Auto-generated catch block
			log.error("Error While jasper print :"+e.getMessage());
			e.printStackTrace();
		}

		// Make sure the output directory exists.
		File outDir = new File(" ");
		outDir.mkdirs();

		// Export to PDF.
		
		FileOutputStream fos = null;
		try {
			// JasperExportManager.exportReportToPdfFile(jasperPrint,"/home/dsingh/Documents/CafVerficationReport_output.pdf");

			String outputURL= PropLoader.getPropertyForReport(reportOutputDir);
			
			File file = File.createTempFile(reportName, ".xls",
					new File(reportOutputDir));
			
			 fos = new FileOutputStream(file);
			
			JRXlsExporter exporterXLS = new JRXlsExporter();
			 
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, fos);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_FILE,file.getAbsolutePath());
			exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS,Boolean.FALSE);
			
			
			exporterXLS.exportReport();

		} catch (JRException e) {
			// TODO Auto-generated catch block
			log.error("Error While exporting :"+e.getMessage());
			e.printStackTrace();
		}finally{
			if(fos != null)
			fos.close();
		}

		log.info("Reports Done!!!!!!!!!");

	}
	
	
	public static List<Object> createJsonToReportListData(String reportData){
		
		List<Object> objectData = new ArrayList<Object>();
			
			ObjectMapper mapper = new ObjectMapper();
			
			if(!"".equals(reportData)){
				
				try {
					
					objectData = mapper.readValue(reportData, mapper.getTypeFactory().constructCollectionType(List.class, Object.class));
				} catch (JsonParseException e) {
					log.error("Error While parsing json  :"+e.getMessage());
					e.printStackTrace();
					
				} catch (JsonMappingException e) {
					log.error("Error While json mapping  :"+e.getMessage());
					e.printStackTrace();
					
				} catch (IOException e) {
					log.error("Error While executing  createJsonToReportListData :"+e.getMessage());
					e.printStackTrace();
					
				}
										
			}
			return objectData;
			
		}

}
