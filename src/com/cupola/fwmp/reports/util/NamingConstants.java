package com.cupola.fwmp.reports.util;

public class NamingConstants {

	public static final  String CAFREPORTPHASE = "-cafverfication";
	public static final  String MATERIALREPORTPHASE = "-material";
	public static final  String SALESREPORTPHASE = "-salesdcrdetails";
	public static final  String WORKORDERREPORTPHASE = "-workorder";
	public static final  String FEASIBILITYREPORTPHASE = "-feasibilitycheck";
	
	public static final  String CAFREPORTJRXML = "CafVerficationReport";
	public static final  String MATERIALREPORTJRXML = "Material_report";
	public static final  String SALESREPORTJRXML = "SalesDcrDetailsReport";
	public static final  String WORKORDERREPORTJRXML = "WorkOrderReport";
	public static final  String FEASIBILITYREPORTJRXML = "FeasibilityCheck";
	public static final  String PRODUCTIVITYREPORTJRXML = "ProductivityTemplateTL";
	public static final  String PRODUCTIVITYREPORTAMJRXML = "ProductivityTemplateAM";
	public static final  String ETRREPORTJRXML = "ETRTemplate";
	public static final  String ALLOCATIONREPORTTLJRXML = "AllocationTemplateTL";
	public static final  String ALLOCATIONREPORTAMJRXML = "AllocationTemplateAM";
	
	public static final  String CLOSEDCOMPLAINTSJRXML = "ActClosedComplaints";
	public static final  String CLOSEDCOMPLAINTS30MinSJRXML = "Act30MinsClosedComplaints";
	public static final  String NETLIGHTWEIGHTJRXML = "ActNewLightWeight";
	public static final  String REASSIGNMENTJRXML = "ActReassignmentHistory";
	public static final  String REOPENJRXML = "ActReopenHistory";
	public static final  String TICKETREASSIGNMENTJRXML = "TicketReassignmentReport";
}
