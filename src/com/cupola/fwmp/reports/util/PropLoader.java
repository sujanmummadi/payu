package com.cupola.fwmp.reports.util;

import java.io.File;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class PropLoader
{

	public static final Logger log = LogManager.getLogger(PropLoader.class);
	static String filePath = null;
	public static String getPropertyForReport(String key)
	{
		
		if(filePath != null)
			return filePath;
		try
		{
			ClassLoader classLoader = PropLoader.class.getClassLoader();
			  filePath = classLoader.getResource("").getPath();
			 
			filePath = filePath.concat("jrxml").concat(File.separator);
			return filePath;

		} catch (Exception ex)
		{
			log.error("Error While executing getPropertyForReport :"
					+ ex.getMessage());
			ex.printStackTrace();

		}
		return filePath;
	}

}
