package com.cupola.fwmp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public interface FWMPConstant
{

	public static final String REALTY="Community";
	
	public static final int MINUS_MINUTES_FROM_CURRENT_TIME = 5;
	public static final int MINUS_DAYS_FROM_CURRENT_TIME = 20;

	public static final int THRESHOLD_VALUE_FOR_PRIORITY_TICKET = 50;

	public static final Long SYSTEM_ENGINE = 1l;

	public static final int COUNT_ETR = 2;

	public static final String COUNTRY_INDIA = "India";

	public static final Integer ANDRIOD_PAGE_CONTEXT = 100;
	public static final Integer CAF_ALLOCATION = 300;

	public class UserPreferenceType
	{
		public static String NOTIFICATION_BAR = "NOTIFICATION_BAR";

	}

	public class DBActiveStatus
	{
		public static final int ACTIVE = 1;

	}

	public class TicketNotificationStatus
	{
		public static final String CUSTOMER_NOTIFICATION_STATUS = "status.when.customer.get.notification";
		public static final String USER_NOTIFICATION_STATUS = "status.when.user.get.notification";

	}

	public class ActivityNotificationStatus
	{
		public static final String ACTIVITY_NOTIFICATION_STATUS = "activities.when.customer.get.notification";

	}

	public class CountStatusCode
	{
		public static final int REACTIVATION = 1000;
		public static final int SHIFTING = 2000;
		public static final int NEW_INSTALLATION = 3000;

		public static final int FIBER = 6000;
		public static final int COPPER = 7000;

		public static final int FEASIBILITY = 4000;
		public static final int PRIORTY = 5000;

		public static final int TOTAL = 8000;

		public static final String REACTIVATION_STRING = "Re-Activation";
		public static final String SHIFTING_STRING = "Shifting";
		public static final String NEW_INSTALLATION_STRING = "New Installation";
		public static final String FEASIBILITY_STRING = "Feasibility";
		public static final String PRIORTY_STRING = "Priority";
		public static final String FIBER_STRING = "Fiber";

		public static final String COPPER_STRING = "Copper";
		public static final String TOTAL_STRING = "Total";

		public static final Long PRIORITY_THRESOLD = 70l;
		public static final Long FEASIBILITY_ID = 1l;

	}

	public class MessageType
	{
		public static final int ETR_APPROVAL = 10001;
		public static final int SYNC_TICKETS = 10002;
		public static final int REASSIGNED_TICKET = 10003;

		public static final int AAT_ACTIVATION = 10092;

		public static final int ETR_UPDATED = 10050;

		public static final int ETR_UPDATION_REQUESTED = 10051;

		public static final int ETR_REJECTED = 10052;

		public static final int ETR_CHANGE_APPROVED = 10053;

		public static final int USER_AVAILABILITY_STATUS = 10054;

		public static final String ETR_REJECTED_MESSAGE = "Etr changed rejected";
		public static final String ETR_CHANGE_APPROVED__MESSAGE = "Etr changed approved";

		public static final int ACTIVITY_UPDATED = 10098;

		public static final int CUSTOMER_ACTIVATION_SUCCESS = 10093;

		public static final int CUSTOMER_ACTIVATION_FAILURE = 10094;

		public static final int DOCUMENTS_APPROVAL_STATUS_CHANGED = 10095;

		public static final int DOCUMENTS__STATUS = 10096;

		public static final int PAYMENT__STATUS = 10097;

		public static final int AUTO_CLOSURE_CUSTOMER_RESPONSE_YES = 10098;
		public static final int AUTO_CLOSURE_CUSTOMER_RESPONSE_NO = 10099;

		public static final String AUTO_CLOSURE_CUSTOMER_RESPONSE_NO_MESSAGE = "Auto Closure Of Ticket with id : _id_ Failed";

		public static final String AUTO_CLOSURE_CUSTOMER_RESPONSE_YES_MESSAGE = "Auto Closure Of Ticket with id : _id_ Success";

		public static final int DIFF_BETWEEN_CURRENT_TIME_AND_COMMITTED_ETR = 6;

		public static final int TIME_INTERVAL_BETWEEN_CURRENT_AND_LAST_SEND_IN_HOUR = 2;

		public static final int NO_OF_TIMES_THE_MESSAGE_SHOULD_BE_SENT = 2;
	}

	public class TariffPlanStatus
	{
		public static String Active = "Active";
		public static Integer Active_ID = 8789;
		public static String INACTIVE = "INACTIVE";
		public static Integer INACTIVE_ID = 8788;
		public static Map<String, Integer> tariffStatus = new HashMap<String, Integer>();
		static
		{
			tariffStatus.put(Active, Active_ID);
			tariffStatus.put(INACTIVE, INACTIVE_ID);
		}

	}

	public class CustomerType
	{
		public static final String HOME = "Retail";
		public static final String CORPORATE = "Sme";
		public static final String REALITY = "Community";
		public static final String SOHO = "Soho";
		public static final String COMMUNITY = "Community";
		public static final String RETAIL = "Retail";
		
	}

	public class ProspectType
	{
		public static final String HOT = "Hot";
		public static final String MEDIUM = "Medium";
		public static final String COLD = "Cold";
		public static final String REJECTED = "Rejected";

		public static final int HOT_ID = 8001;
		public static final int MEDIUM_ID = 8002;
		public static final int COLD_ID = 8003;

		public static Map<Long, String> def = new HashMap<Long, String>();

		static
		{
			def.put(Long.valueOf(HOT_ID), HOT);
			def.put(Long.valueOf(MEDIUM_ID), MEDIUM);
			def.put(Long.valueOf(COLD_ID), COLD);
		}
	}

	public class Escalation
	{
		static ResourceBundle rb = ResourceBundle
				.getBundle("com.cupola.fwmp.properties.priority");

		public static final String CEO_TYPE = "ceoescalation";
		public static final String NODAL_TYPE = "nodalescalation";
		public static final String SOCIAL_MEDIA_TYPE = "social";
		public static final String OTHERS_TYPE = "others";

		public static final long OTHERS = -1;
		public static final long CEO = 1;
		public static final long NODAL = 2;
		public static final long SOCIAL_MEDIA = 3;
		public static Map<Long, String> def = new HashMap<Long, String>();
		static
		{
			def.put(CEO, CEO_TYPE);
			def.put(NODAL, NODAL_TYPE);
			def.put(OTHERS, OTHERS_TYPE);
			def.put(SOCIAL_MEDIA, SOCIAL_MEDIA_TYPE);
		}
		public static Map<Long, Integer> values = new HashMap<Long, Integer>();
		static
		{
			values.put(CEO, Integer.parseInt(rb.getString(CEO_TYPE)));
			values.put(NODAL, Integer.parseInt(rb.getString(NODAL_TYPE)));
			values.put(OTHERS, Integer.parseInt(rb.getString(OTHERS_TYPE)));
			values.put(SOCIAL_MEDIA, Integer
					.parseInt(rb.getString(SOCIAL_MEDIA_TYPE)));
		}

	}

	public class TicketStatus
	{
		public static List<Integer> COMPLETED_DEF = Arrays
				.asList(new Integer[] { 103, 101});

		public static List<Integer> NEW_TICKET_DEF = Arrays
				.asList(new Integer[] { 102 });
		public static List<Integer> IN_PROGRESS_TICKET_DEF = Arrays
				.asList(new Integer[] { 100 });

		public static final int DIFF_BETWEEN_MODIFIED_ON_AND_NOW_IN_HOURS = 8;
		public static final int DIFF_BETWEEN_TICKET_TIME_AND_LAST_SEND_IN_HOURS = 4;
		public static final int NO_OF_TIMES_THE_MESSAGE_SHOULD_BE_SENT = 2;
		public static final int START_TIME_OF_WORKING_DAY_IN_HOURS = 4;
		public static final int END_TIME_OF_WORKING_DAY_IN_HOURS = 22; // 24
																		// hour
		public static final int FR_MQ_CLOSURE_FAILED = 104;																// format

		public static final int OPEN = 102;
		public static final int CLOSED = 103;

		public static final int NOT_STARTED = 201;
		public static final int HALTED = 202;
		public static final int COMPLETED = 101;
		public static final int IN_PROGRESS = 100;

		public static final int PRE_SALE = 168;
		public static final int PROSPECT_CREATED = 158;

		public static final int ASSIGNED = 163;//
		public static final int NOT_ASSIGNED = 164;//

		public static final int GIS_DATA_NOT_AVAILABLE = 268;//
		public static final int MQ_INSERTION_FAILED = 269;//

		public static final int CUSTOMER_BASIC_INFO_UPDATED = 170;

		public static final int TARIFF_PLAN_UPDATED = 349;

		public static final int PAYMENT_STILL_PENDING = 105;
		public static final int PAYMENT_APPROVED = 161;
		public static final int PAYMENT_REJECTED = 165;//
		public static final int PAYMENT_RECEIVED = 302;
		public static final int PAYMENT_REFUND_INITIATED = 303;
		public static final int WAITINING_FOR_PAYMENT_REFUND = 312;
		public static final int WAITINING_FOR_PAYMENT_REFUND_APPROVAL = 324;
		
		public static final int POI_DOCUMENTS_APPROVED = 1050;
		public static final int POI_DOCUMENTS_REJECTED = 1051;
		public static final int POI_DOCUMENTS_UPLOADED = 1052;
		
		public static final int POA_DOCUMENTS_APPROVED = 1053;
		public static final int POA_DOCUMENTS_REJECTED = 1054;
		public static final int POA_DOCUMENTS_UPLOADED = 1055;
		
//		public static final int DOCUMENTS_APPROVED = 166;
//		public static final int DOCUMENTS_REJECTED = 167;
//		public static final int DOCUMENTS_UPLOADED = 169;

		public static final int FEASIBILITY_UPDATED_DURING_SALE = 171;
		public static final int FEASIBILITY_UPDATED_DURING_MANUAL_FEASIBILITY = 172;
		public static final int FEASIBILITY_UPDATED_DURING_DEPLOYMENT = 173;
		public static final int PORT_ACTIVATION_SUCCESSFULL = 140;

		public static final int ACTIVATION_COMPLETED = 250;
		public static final int ACTIVATION_PENDING = 110;

		public static final int ETR_APPROVAL_PENDING = 253;
		public static final int ETR_APPROVED = 251;
		public static final int ETR_REJECTED = 252;
		public static final int ETR_UPDATE_AUTO_MODE = 300;
		public static final int ETR_UPDATE_MANUAL_MODE = 301;

		public static final int REACH_TEAM_ACTION_PENDING = 246;
		public static final int REACH_TEAM_ACTION_COMPLETED = 247;
		public static final int REACH_TEAM_ACTION_REJECTED = 248;
		public static final int ACTIVATION_COMPLETED_VERIFIED = 249;

		public static final Integer PERMISSION_PENDING_BY_SALES = 304;
		public static final Integer PERMISSION_GRANTED_BY_SALES = 306;
		public static final Integer PERMISSION_REJECTED_BY_SALES = 307;
		public static final Integer FEASIBILITY_PENDING_BY_NE = 305;
		public static final Integer FEASIBILITY_GRANTED_BY_NE = 308;
		public static final Integer FEASIBILITY_REJECTED_BY_NE = 309;

		public static final Integer FEASIBILITY_REJECTED_BY_NE_DURING_DEPLOYMENT = 313;

		public static final Integer PHYSICAL_FEASIBILITY = 310;
		public static final Integer CUSTOMER_REJECTED_BY_SALES = 311;

		public static final int SALES_UNASSIGNED_TICKET = 254;
		public static final int WO_UNASSIGNED_TICKET = 325;

		public static final int CUSTOMER_ACCOUNT__ACTIVATE_SUCCESSFULLY = 111;

		public static final Integer CX_RACK_FIXING_DONE = 314;
		public static final Integer COPPER_LAYING_DONE = 315;
		public static final Integer FIBER_LAYING_DONE = 316;
		public static final Integer SPLICING_DONE = 317;
		public static final Integer MATERIAL_CONSUMPTION_UPDATED = 318;
		public static final Integer CUSTOMER_ACCOUNT_ACTIVATION_DONE = 319;
		public static final Integer PORT_ACTIVATION_DONE = 320;
		public static final Integer PHYSICAL_FEASIBILITY_DONE = 321;
		public static final Integer WAITING_FOR_WO_GENERATION = 322;
		public static final Integer WO_GENERATED = 323;
		public static Map<Integer, String> statusCodeValue = new HashMap<Integer, String>();

		public static final List<Integer> SALES_STATUS_CODE_DEF = Arrays
				.asList(OPEN, PAYMENT_REJECTED, PAYMENT_APPROVED, PAYMENT_STILL_PENDING,
						POI_DOCUMENTS_APPROVED, POI_DOCUMENTS_REJECTED, POI_DOCUMENTS_REJECTED, POA_DOCUMENTS_REJECTED,
						POA_DOCUMENTS_REJECTED, POA_DOCUMENTS_UPLOADED);

		public static final List<Integer> COMPLETE_SALES_STATUS = Arrays
				.asList(OPEN, POI_DOCUMENTS_APPROVED, POI_DOCUMENTS_REJECTED, POI_DOCUMENTS_REJECTED, POA_DOCUMENTS_REJECTED,
						POA_DOCUMENTS_REJECTED, POA_DOCUMENTS_UPLOADED, PAYMENT_REJECTED, PAYMENT_APPROVED, PAYMENT_STILL_PENDING);

		
		public static final List<Integer> NI_STATUS_CODE_DEF = Arrays.asList(ACTIVATION_COMPLETED,
				POI_DOCUMENTS_APPROVED, POA_DOCUMENTS_APPROVED, ETR_APPROVAL_PENDING, ETR_REJECTED, ETR_APPROVED);

		public static final List<Integer> TL_NI_STATUS_CODE_DEF = Arrays.asList(ACTIVATION_COMPLETED,
				 POI_DOCUMENTS_APPROVED, POA_DOCUMENTS_APPROVED, ETR_APPROVAL_PENDING, ETR_REJECTED,
				ETR_APPROVED);

		public static final List<Integer> FEASIBILITY_STATUS_LIST = Arrays
				.asList(FEASIBILITY_PENDING_BY_NE, FEASIBILITY_GRANTED_BY_NE, FEASIBILITY_REJECTED_BY_NE);

		public static Map<Integer, String> statusDisplayValue = new HashMap<Integer, String>();

		public static final List<Integer> CLOSED_TICKET_STATUS = Arrays
				.asList(new Integer[] { ACTIVATION_COMPLETED_VERIFIED, CUSTOMER_REJECTED_BY_SALES });

		static
		{

		}

	}

	/*
	 * public class UserRole { public static final int DOC_APPROVER = 102;
	 * public static final int TL = 99; public static final int TL_NI = 100;
	 * public static final int NE_NI = 101; }
	 */
	public class TicketCategory
	{
		public static String NEW_INSTALLATION = "NEW_INSTALLATION";
		public static String FAULT_REPAIR = "BROADBAND-COMPLAINTS";
		public static String SHIFTING_STR = "SHIFTING";
		public static String RE_ACTIVATION_STRING = "RE ACTIVATION";
		
		public static List<String> FAULT_REPAIR_HYD = new ArrayList<String>();
		public static List<String> FAULT_REPAIR_ROI = new ArrayList<String>();
		public static List<String> FAULT_REPAIR_BOTH = new ArrayList<String>();

		public static final Long NI_SALES = 1l;
		public static final Long NIDEPLOYMENT = 2l;
		public static final Long SHIFTING = 3l;
		public static final Long RE_ACTIVATION = 4l;
		public static final Long FR = 5l;
	}

	public class TicketSubCategory
	{

		public static final String GIS_FEASIBILITY_FAILURE = "GIS_FEASIBILITY_FAILURE";
		public static final Long FEASIBILITY = 305l;
		public static final Long PRIORITY = 11l;
		public static final long FIBER_ID = 1l;
		public static final long COPPER_ID = 2l;
		public static final long PERMISSION_ID = 20l;

		public static final String FIBER = "Fiber";
		public static final String COPPER = "Copper";
		public static final String FISIBLE = "Feasibility";
		public static final String PERMISSION = "PERMISSION";

		public static Map<Long, String> subCategoryCodeValue = new HashMap<Long, String>();
		static
		{
			subCategoryCodeValue.put(FEASIBILITY, "FEASIBILITY");

		}

	}

	public class UserAvailabilityStatus
	{
		public static final int USER_AVAILABILITY_STATUS = 1;
		public static final int USER_UNAVAILABILITY_STATUS = 0;

		public static final String USER_ONLINE_STATUS = "Online Successful ";

		public static final String USER_OFFLINE_STATUS = "Offline Successful";
	}

	public interface TicketSource
	{
		String FWMP = "Call Centre";
		String CUSTOMER_APP = "CustomerApp";
		String TOOL = "Tool";
		String DOOR_KNOCK = "Door Knock";
		
		//added by Manjuprasad , a new constant for Door Knocks
		String DOOR_KNOCKS = "Door Knocks";
	}
	/*public interface TicketSubAttribute{
		
		String DOOR_KNOCK="Door Knock";
	}*/

	public class WorkStageName
	{
		public static String FIBER = "Fiber";
		public static String COPPER = "Copper";
		public static String CXDOWN = "CXDown";
		public static String CXUP = "CXUp";

	}

	public interface ReportStatusType
	{
		String COMPLETED = "Completed";
		String IN_PROGRESS = "In Progress";
		String NEW_TICKET = "New Ticket";

	}

	public class WorkStageType
	{
		public static final long FIBER = 1;
		public static final long COPPER = 2;
		public static final long SALES = 5;
	}

	public interface Activity
	{
		List<Long> SALES_ACTIVITIES_LIST = Arrays
				.asList(new Long[] { 12l, 13l, 14l, 15l ,16l ,17l});
		long BASIC_INFO_UPDATE = 12;
		long PAYMENT_UPDATE = 13;
		long DOCUMENT_UPDATE = 14;
		long TARIFF_UPDATE = 15;
		long PORT_ACTIVATION = 4;
		long CUSTOMER_ACCOUNT_ACTIVATION = 2;
		long CX_RACK_FIXING = 3;
		long COPPER_LAYING = 5;
		long FIBER_LAYING = 6;
		long SPLICING = 7;
		long PHYSICAL_FEASIBILITY = 9;
		long MATERIAL_CONSUMPTION = 8;
		long TICKET_CONVERTED_FROM_COPPER_TO_FIBER = 20;
		long POA_DOCUMENT_UPDATE = 16;
		long POI_DOCUMENT_UPDATE = 17;
		
		String POI_STATUS = "POI";
		String POA_STATUS = "POA";
		String PAYMENT_STATUS = "VERIFICATION";
	}

	public interface SubActivity
	{
		long CX_IP = 31;
		long CX_PORT = 17;
		long FEASIBILITY_CONNECTION_TYPE = 605;

	}

	public interface UserCaf
	{
		public static final int USED = 0;
		public static final int UN_USED = 1;
	}

	public class PortActivation
	{

		public static long PORT_ACTIVATION = 4;
		public static long ENTER_CX_PORT_Id = 17;
		public static long ENTER_CX_IP_Id = 31;
		public static String ENTER_CX_PORT_NO = "enterCxPortNo";

		public static String ENTER_CX_IP = "entercxIp";
		public static Map<Long, String> activityPort = new HashMap<Long, String>();
		static
		{
			activityPort.put(ENTER_CX_PORT_Id, ENTER_CX_PORT_NO);
			activityPort.put(ENTER_CX_IP_Id, ENTER_CX_IP);
		}

	}

	public class CutomerAccountActivation
	{
		public static long CUSTOMER_ACCOUNT_ACTIVATION = 2;
	}

	public class CustomerAppNotification
	{
		public static String CUSTOMER_APP_NOTIFICATION_SUCCESS = "success";

		public static String CUSTOMER_APP_NOTIFICATION_FAILURE = "failure";

		public static int CUSTOMER_ACTIVITYID = 2;

		public static String CUSTOMER_SUB_ACTIVITYID = "7";

		public static String CUSTOMER_SUB_ACTIVITYID_TRUE_VALUE = "true";

		public static String CUSTOMER_SUB_ACTIVITYID_FALSE_VALUE = "false";

		public static String CUSTOMER_SERVER_ADDRESS = "CUSTOMER_SERVER_ADDRESS";

		public static String SMS_GATEWAY_ADDRESS = "SMS_GATEWAY_ADDRESS";

		// **************Query for HYD**************************
		public static String QUERY_FOR_HYD = "QUERY_FOR_HYD";
		public static String QUERY_FOR_HYD_WITH_INTERVAL = "QUERY_FOR_HYD_WITH_INTERVAL";

		// **************Query for ROI**************************
		public static String QUERY_FOR_ROI = "QUERY_FOR_ROI";
		public static String QUERY_FOR_ROI_WITH_INTERVAL = "QUERY_FOR_ROI_WITH_INTERVAL";

		public static String BASE_PATH_FOR_CUSTOMER_MOBILE_DETAILS = "BASE_PATH_FOR_CUSTOMER_MOBILE_DETAILS";
		public static String FILE_NAME = "FILE_NAME";
		public static String BASE_PATH_FOR_ARCHIVE_CUSTOMER_MOBILE_DETAILS = "BASE_PATH_FOR_ARCHIVE_CUSTOMER_MOBILE_DETAILS";

		public static String TOTAL_NO_COL_IN_CUSTOMER_SHEET = "TOTAL_NO_COL_IN_CUSTOMER_SHEET";

		public static String ROI = "ROI";
		public static String HYD = "HYD";

	}

	public static List<Long> WorkStageActivitySequence = Arrays
			.asList(new Long[] { 12l, 15l, 14l, 13l, 10l, 9l, 11l, 3l, 6l, 7l,
					5l, 8l, 4l, 2l, 1l });

	public class DashBoardStatusDefinition
	{
		public static final List<Long> FIBER_TYPE_ACTIVATION_PENDING = Arrays
				.asList(6l, 7l, 3l, 5l, 9l);
		public static final List<Long> FIBER_TYPE_COPPER_PENDING = Arrays
				.asList(6l, 7l, 3l, 9l);

		public static final List<Long> FIBER_LAID_RACK_FIXED_SPLICING_ACTIVATION_PENDING = Arrays
				.asList(6l, 3l, 9l);

		public static final List<Long> FIBER_DONE_RACK_SPLICING_PENDING = Arrays
				.asList(6l, 9l);

		public static final List<Long> FIBER_TYPE_ONLY_RACK_FIXED = Arrays
				.asList(3l, 9l);

		public static final List<Long> FIBER_TYPE_FIBER_PENDING = Arrays
				.asList(9l);

		public static final List<Long> COPPER_TYPE_ACTIVATION_PENDING = Arrays
				.asList(5l, 9l);
		public static final List<Long> COPPER_TYPE_COPPER_PENDING = Arrays
				.asList(9l);

	}

	public class ActivityStatusDefinition
	{
		public static final List<Long> COPPER_PENDING = Arrays
				.asList(6l, 7l, 3l);
		public static final List<Long> FIBER_DONE_RACK_SPLICING_PENDING = Arrays
				.asList(6l);
		public static final List<Long> FIBER_LAID_RACK_FIXED_SPLICING_ACTIVATION_PENDING = Arrays
				.asList(6l, 3l);
		public static final List<Long> FIBER_YET_TO_START = null;

		public static final List<Integer> COMPLETED_DEF = Arrays
				.asList(TicketStatus.CLOSED, TicketStatus.COMPLETED, TicketStatus.ACTIVATION_COMPLETED_VERIFIED);
		public static final List<Integer> INPROGRESS_DEF = Arrays
				.asList(TicketStatus.IN_PROGRESS, TicketStatus.HALTED);

		public static final List<Integer> SALES_RELATED_ISSUE_DEF = Arrays
				.asList(404);
		public static final List<Integer> REACH_REVERT_DEF = Arrays.asList(400);

		public static final List<Integer> LCO_ISSUE_DEF = Arrays.asList(401);
		public static final List<Integer> CUSTOMER_END_ISSUE_DEF = Arrays
				.asList(405);
		public static final List<Integer> CUSTOMER_APPOINT_ISSUE_DEF = Arrays
				.asList(406);

	}

	public class DocumentType
	{
		public static final int ID_PROOF = 1;
		public static final int ADDRESS_PROOF = 2;
		public static final int PERMISSION = 3;
		public static final int AADHAR = 4;
		public static final String ID_PROOF_STRING = "IDENTITY";
		public static final String ADDRESS_PROOF_STRING = "ADDRESS";
		public static final String PERMISSION_STRING = "PERMISSION";
		public static final String AADHAR_STRING = "AADHAAR";
		public static Map<Integer, String> DOCUMENT_TYPE_STRING = new HashMap<Integer, String>();
		static
		{
			DOCUMENT_TYPE_STRING.put(ID_PROOF, "ID_PROOF");
			DOCUMENT_TYPE_STRING.put(ADDRESS_PROOF, "ADDRESS_PROOF");
			DOCUMENT_TYPE_STRING.put(PERMISSION, "PERMISSION");
			DOCUMENT_TYPE_STRING.put(AADHAR, "AADHAAR");
		}
	}

	public class DocumentSubType
	{
		public static final int ADHAAR = 11;
		public static final int PASSPORT = 12;
		public static final int ARMS_LICENSE = 13;
		public static final int DRIVING_LICENSE = 14;
		public static final int ELECTION_COMMISSION_CARD = 15;
		public static final int RATION_CARD_WITH_ADDRESS = 16;
		public static final int CGHS_ECHS_CARD = 17;
		public static final int CERTIFICATE_OF_ADDRESS_BY_GOVT_INSTITUTE = 18;
		public static final int CERTIFICATE_OF_ADDRESS_BY_PANCHAYAT = 19;
		public static final int INCOME_TAX_PAN_CARD = 110;
		public static final int PHOTO_CREDIT_CARD = 111;
		public static final int ADDRESS_CARD_BY_GOI = 112;
		public static final int SMART_CARD_BY_CSD_DEFENCE = 113;
		public static final int CURRENT_PASSBOOK_OF_POST_OR_BANK = 114;
		public static final int PHOTO_IDENTITY_CARD_BY_PSU_OR_GOVT = 115;
		public static final int PHOTO_IDENTITY_CARD_BY_GOVT_INSTITUTE = 116;
		public static final int CAST_AND_DOMICILE_CERTIFICATE = 117;
		public static final int PENSIONERS_CARD_WITH_ADDRESS = 118;
		public static final int FREEDOM_FIGHTER_CARD_WITH_ADDRESS = 119;

		// Address
		public static final int CERTIFICATE_OF_ADDRESS_BY_GAZETTED_OFFICER = 28;
		public static final int WATER_BILL = 211;
		public static final int TELEPHONE_BILL = 212;
		public static final int ELECTRICITY_BILL = 213;
		public static final int INCOME_TAX_ASSESSMENT_ORDER = 214;
		public static final int VEHICAL_REGISTRATION_CERTIFICATE = 215;
		public static final int NOTARIZED_SALE_LEASE_AGREEMENT = 216;
		public static final int CREDIT_CARD_STATEMENT = 220;
		public static final int KISAN_PASSBOOK_HAVING_ADDRESS = 224;
		public static final int RENTAL_AGREEMENT = 225;
		public static final int MARRIAGE_CERTIFICATE = 226;
		public static final int PERMISSION_FORM = 227;
		public static final int AADHAAR_VERIFICATION = 228;

		public static Map<Integer, String> DOCUMENT_SUBTYPE_VALUE_STRING = new HashMap<Integer, String>();
		public static Map<String, Integer> DOCUMENT_SUBTYPE_STRING_VALUE = new HashMap<String, Integer>();

		static
		{

			DOCUMENT_SUBTYPE_VALUE_STRING.put(PASSPORT, "PASSPORT");
			DOCUMENT_SUBTYPE_VALUE_STRING.put(ADHAAR, "AADHAAR");
			DOCUMENT_SUBTYPE_VALUE_STRING.put(ARMS_LICENSE, "OTHERS");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(DRIVING_LICENSE, "DRIVING_LICENSE");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(ELECTION_COMMISSION_CARD, "ELECTION_COMMISSION_CARD");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(RATION_CARD_WITH_ADDRESS, "RATION_CARD_WITH_ADDRESS");
			DOCUMENT_SUBTYPE_VALUE_STRING.put(CGHS_ECHS_CARD, "CGHS_ECHS_CARD");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(CERTIFICATE_OF_ADDRESS_BY_GOVT_INSTITUTE, "CERTIFICATE_OF_ADDRESS_BY_GOVT_INSTITUTE");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(CERTIFICATE_OF_ADDRESS_BY_PANCHAYAT, "CERTIFICATE_OF_ADDRESS_BY_PANCHAYAT");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(INCOME_TAX_PAN_CARD, "INCOME_TAX_PAN_CARD");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(PHOTO_CREDIT_CARD, "PHOTO_CREDIT_CARD");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(ADDRESS_CARD_BY_GOI, "ADDRESS_CARD_BY_GOI");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(SMART_CARD_BY_CSD_DEFENCE, "SMART_CARD_BY_CSD_DEFENCE");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(CURRENT_PASSBOOK_OF_POST_OR_BANK, "CURRENT_PASSBOOK_OF_POST_OR_BANK");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(PHOTO_IDENTITY_CARD_BY_PSU_OR_GOVT, "PHOTO_IDENTITY_CARD_BY_PSU_OR_GOVT");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(PHOTO_IDENTITY_CARD_BY_GOVT_INSTITUTE, "PHOTO_IDENTITY_CARD_BY_GOVT_INSTITUTE");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(CAST_AND_DOMICILE_CERTIFICATE, "CAST_AND_DOMICILE_CERTIFICATE");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(PENSIONERS_CARD_WITH_ADDRESS, "PENSIONERS_CARD_WITH_ADDRESS");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(FREEDOM_FIGHTER_CARD_WITH_ADDRESS, "FREEDOM_FIGHTER_CARD_WITH_ADDRESS");

			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(CERTIFICATE_OF_ADDRESS_BY_GAZETTED_OFFICER, "CERTIFICATE_OF_ADDRESS_BY_GAZETTED_OFFICER");
			DOCUMENT_SUBTYPE_VALUE_STRING.put(WATER_BILL, "WATER_BILL");
			DOCUMENT_SUBTYPE_VALUE_STRING.put(TELEPHONE_BILL, "TELEPHONE_BILL");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(ELECTRICITY_BILL, "ELECTRICITY_BILL");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(INCOME_TAX_ASSESSMENT_ORDER, "INCOME_TAX_ASSESSMENT_ORDER");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(VEHICAL_REGISTRATION_CERTIFICATE, "VEHICAL_REGISTRATION_CERTIFICATE");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(NOTARIZED_SALE_LEASE_AGREEMENT, "NOTARIZED_SALE_LEASE_AGREEMENT");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(CREDIT_CARD_STATEMENT, "CREDIT_CARD_STATEMENT");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(KISAN_PASSBOOK_HAVING_ADDRESS, "KISAN_PASSBOOK_HAVING_ADDRESS");

			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(RENTAL_AGREEMENT, "RENTAL_AGREEMENT");
			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(MARRIAGE_CERTIFICATE, "MARRIAGE_CERTIFICATE");

			DOCUMENT_SUBTYPE_VALUE_STRING
					.put(PERMISSION_FORM, "PERMISSION_FORM");

			DOCUMENT_SUBTYPE_VALUE_STRING.put(AADHAAR_VERIFICATION, "VERIFICATION");
	
			for (Map.Entry<Integer, String> entry : DOCUMENT_SUBTYPE_VALUE_STRING
					.entrySet())
			{
				DOCUMENT_SUBTYPE_STRING_VALUE
						.put(entry.getValue(), entry.getKey());
			}
		}

	}

	public interface FeasibilityType
	{
		Long GIS_AUTO = 1l;
		Long GIS_SALES = 2l;
		Long PHYSICAL_FEASIBILITY_BY_NE = 3l;
		Long DEPLOYMENT_AAT = 4l;
		Long GIS_CLASSIFICATION = 5l;
		Long GIS_CUSTOMER_APP = 6l;
	}

	public class ClassificationCategories
	{
		public static String NEW_INSTALLATION_CATEG = "NEW_INSTALLATION";
		public static String SHIFTING_CATEG = "SHIFTING";
		public static String RE_ACTIVATION_CATEG = "COMBO_RE_ACTIVATION";
		public static String FR_CATEG = "BROADBAND-COMPLAINTS";

		public static List<String> FR_SYMPTOM = Arrays
				.asList("CX Down", "CX UP", "Frequent Disconnection", "Slow Speed", "Senior Element down");

		public static Long FLOW_ID_CX_DOWN = 1l;
		public static Long FLOW_ID_CX_UP = 2l;
		public static Long FLOW_ID_FREQUENT_DISCONNECTION = 3l;
		public static Long FLOW_ID_SLOW_SPEED = 4l;
		public static Long FLOW_ID_SENIOR_ELEMENT_DOWM = 5l;
		public static Long FLOW_ID_OTHERS = 0l;
		public static Long FLOW_ID_DISCARD = 6l;
		public static Long FLOW_ID_NONE = -1l;

		public static Map<Long, String> symptomFlowIdMap = new ConcurrentHashMap<Long, String>();
		static
		{
			symptomFlowIdMap.put(FLOW_ID_CX_DOWN, "CX Down");
			symptomFlowIdMap.put(FLOW_ID_CX_UP, "CX UP");
			symptomFlowIdMap
					.put(FLOW_ID_FREQUENT_DISCONNECTION, "Frequent Disconnection");
			symptomFlowIdMap.put(FLOW_ID_SLOW_SPEED, "Slow Speed");
			symptomFlowIdMap
					.put(FLOW_ID_SENIOR_ELEMENT_DOWM, "Senior Element down");
			symptomFlowIdMap.put(FLOW_ID_OTHERS, "Others");
			symptomFlowIdMap.put(FLOW_ID_NONE, "None");
			symptomFlowIdMap.put(FLOW_ID_DISCARD, "Discard");
		}
	}

	public class FrTicketStatus
	{
		public static Integer CLOSED = 700;
		public static Integer OPENED = 701;
		public static Integer RESOLVED = 702;
		public static Integer ACTIVE = 703;

		public static Integer SYMPTOM_CODE = 703;
	}

	public class CustomerMessages
	{
		public static String RECHECK_MESSAGE = "Please recheck Portal page and Reply with 'Yes' or 'NO'";
	}

	public class SalesCount
	{
		public static final int DOOR_KNOCK_ID = 110001;
		public static final int NEW_ID = 110002;
		public static final int FEASIBILITY_PENDING_ID = 110003;
		public static final int FEASIBILITY_APPROVED_ID = 110004;
		public static final int DOCUMENT_PENDING_WITH_CFE_ID = 110005;
		public static final int PAYMENT_PENDING_WITH_CFE_ID = 110006;
		public static final int PERMISSION_ID = 110007;
		public static final int TOTAL_ID = 110008;
		public static final int CAF_ACCEPTED_BY_CFE_ID = 110009;
		public static final int POI_DOCUMENT_PENDING_WITH_CFE_ID = 110010;
		public static final int POA_DOCUMENT_PENDING_WITH_CFE_ID = 110011;

		public static final String DOOR_KNOCK = "DOOR KNOCK";
		public static final String NEW = "NEW";
		public static final String FEASIBILITY_PENDING = "FEASIBILITY PENDING";
		public static final String FEASIBILITY_APPROVED = "FEASIBILITY APPROVED";
		public static final String DOCUMENT_PENDING_WITH_CFE = "DOCUMENT PENDING WITH CFE";
		public static final String POA_DOCUMENT_PENDING_WITH_CFE = "POA DOCUMENT PENDING WITH CFE";
		public static final String POI_DOCUMENT_PENDING_WITH_CFE = "POI DOCUMENT PENDING WITH CFE";
		public static final String PAYMENT_PENDING_WITH_CFE = "PAYMENT PENDING WITH CFE";
		public static final String PERMISSION = "PERMISSION";
		public static final String TOTAL = "TOTAL";
		public static final String CAF_ACCEPTED_BY_CFE = "CAF ACCEPTED BY CFE";
	}

	public class PriorityFilter
	{
		public static final long VERY_HIGH = 1;
		public static final long HIGH = 2;
		public static final long MEDIUM = 3;
		public static final long LOW = 4;
		public static final String VERY_HIGH_STRING = "VERY_HIGH";
		public static final String HIGH_STRING = "HIGH";
		public static final String MEDIUM_STRING = "MEDIUM";
		public static final String LOW_STRING = "LOW";

		public static Map<Long, String> definition = new LinkedHashMap<Long, String>();
		static
		{
			definition.put(-1l, "All");
			definition.put(LOW, LOW_STRING);
			definition.put(MEDIUM, MEDIUM_STRING);
			definition.put(HIGH, HIGH_STRING);
			definition.put(VERY_HIGH, VERY_HIGH_STRING);

		}

	}

	public class BooleanValue
	{
		public static final String TRUE = "true";
		public static final String FALSE = "false";

		public static final String FALSE_ID = "0";
		public static final String TRUE_ID = "1";
	}

	public class ControlBlockStatus
	{
		public static int ADD = 1;
		public static int UPDATE = 2;
		public static int DELETE = 3;
		public static int CLEAR = 4;

		public static int YES = 1;
		public static int NO = 2;

		public static int SCRATCH_TYPE = 1;
		public static int CONTEXT_TYPE = 2;

	}

	public class EmailType
	{
		public static int LEGAL = 1;
		public static int CORPORATE = 2;

	}

	public interface EmailStatus
	{
		public static int SENT = 1;
		public static int NOT_SENT = 2;
		public static int NOT_YET_SENT = 3;
		public static int YES = 1;
		public static int NO = 2;

		public static int CX_DOWN = 1;
		public static int FLOW_ID_CX_UP = 2;
		public static int FLOW_ID_FREQUENT_DISCONNECTION = 3;
		public static int FLOW_ID_SLOW_SPEED = 4;
		public static int FLOW_ID_SENIOR_ELEMENT_DOWM = 5;
		public static int FLOW_ID_OTHERS = 0;
		public static int FLOW_ID_DISCARD = 6;
		public static int FLOW_ID_NONE = -1;

	}

	public interface GisFailed
	{
		String GIS_RESPONSE_FAILED_NON_FEASIBLE = "NON_FEASIBLE";
		String GIS_RESPONSE_FAILED_ERROE = "999999";
		String GIS_RESPONSE_FAILED_TRANSACTION_NO = "999998";
		String GIS_RESPONSE_FAILED_MESSAGE = "Gis Response Failed";
	}

	public class FilterType
	{
		public static int WORK_ORDER_TYPE = 1;
		public static int PRIORITY = 2;
		public static int STATUS = 3;
		public static int INITIAL_WORK_STAGE = 4;

	}

	public class UserGroupCodes
	{
		public static String SALES_CM = "SALES_CM";
		public static String SALES_BM = "SALES_BM";
		public static String SALES_AM = "SALES_AM";

		public static String NI_CM = "NI_CM";
		public static String NI_BM = "NI_BM";
		public static String NI_AM = "NI_AM";

	}

	public interface CityName
	{
		public String HYDERABAD_CITY = "HYDERABAD";
		public String HYDERABAD_CITY_CODE = "HYD";
		public String BANGALORE_CITY = "BANGALORE";
		public String CHENNAI_CITY = "CHENNAI";
	}

	public interface MQStatus
	{
		public Integer SUCCESS = 1;
		public Integer FAILED = 0;
	}

	public interface Status
	{
		public Integer ACTIVE = 1;
		public Integer IN_ACTIVE = 0;
	}

	public interface DeviceType
	{
		public Integer CX = 1;
		public Integer FX = 2;

		public interface DeviceTypeStatusString
		{
			public String ACTIVE = "ACTIVE";
			public String IN_ACTIVE = "IN_ACTIVE";
		}
	}

	public interface ApiAction
	{
		public String CREATE = "CREATE";
		public String UPDATE = "UPDATE";

		public String UPDATE_TYPE_TARIFF = "TARIFF_UPDATE";
		public String UPDATE_TYPE_DOCUMENT = "DOCUMENT_UPDATE";
		public String UPDATE_TYPE_PAYMENT = "PAYMENT_UPDATE";

	}

	public interface MessagePlaceHolder
	{
		String DATE = "_DATE_";
		String MQID = "_MQID_";
		String PROSPECT_NUMBER = "_PROSPECT_NUMBER_";
		String HELP_LINE_NUMBER = "_HELP_LINE_NUMBER_";
		String DOC_REJECT_CODE = "_DOC_REJECT_CODE_";
		String PAYMENT_REJECT_CODE = "_PAYMENT_REJECT_CODE_";
		String WORK_ORDER_NO = "_WORK_ORDER_NO_";
		String PORTAL_URL = "_PORTAL_URL_";
		String CUSTOMER_NAME = "_CUST_NAME_";
		String MOBILE = "_MOBILE_";
		String AREA = "_AREA_";
		String ADDRESS = "_ADDRESS_";

	}


	public interface AAT
	{
		boolean ELIGIBILE = true;
		boolean NON_ELIGIBILE = false;

		int CONNECTION_TYPE_MISMATCH_ID = 56500;
		int AAT_ELIGIBILE_ID = 56501;
		int AAT_NON_ELIGIBILE_ID = 56502;

		String CONNECTION_TYPE_MISMATCH_MESSAGE = "CONNECTION_TYPE_MISMATCH";
		String ALL_ACTIVITY_NOT_COMPLETED = "ALL_ACTIVITY_NOT_COMPLETED";
		String ALL_ACTIVITY_COMPLETED = "ALL_ACTIVITY_COMPLETED";

		Set<Long> FIBER_ACTIVITIES = new HashSet<Long>(Arrays
				.asList(9l,3l,6l,7l,8l,5l));
		
		Set<Long> COPPER_ACTIVITIES = new HashSet<Long>(Arrays
				.asList(9l,5l,8l));
	}
	
	public interface WOConversionCheck
	{
		
		boolean ELIGIBILE = true;
		boolean NON_ELIGIBILE = false;

		String ALL_ACTIVITY_NOT_COMPLETED = "ALL_ACTIVITY_NOT_COMPLETED";
		String ALL_ACTIVITY_COMPLETED = "ALL_ACTIVITY_COMPLETED";

		Set<Long> SALES_MQ_ACTIVITIES = new HashSet<Long>(Arrays
				.asList(12l,13l,14l,15l));
		
		Set<Long> SALES_SIEBEL_ACTIVITIES = new HashSet<Long>(Arrays
				.asList(12l,13l,15l,16l,17l));
	}

	public interface ReportType
	{
		String CAF = "CAF";
		String FEASIBILITY = "Feasibility";
		String MATERIAL = "Material";
		String SALESDCR = "SalesDCR";
		String WORKORDER = "WorkOrder";
		
		String CLOSEDCOMPLAINTS = "ClosedComplaints";
		String CLOSEDCOMPLAINTS30MINS = "ClosedComplaints30Mins";
		String LIGHTWEIGHT = "LightWeight";
		String REASSIGNMENT = "ReAssignment";
		String REOPEN = "ReOpen";
		String TICKETREASSIGNMENT = "TicketReAssignment";
	}
	
	public interface CustomerAppFeasibilityType
	{
		String FIBER = "Fiber";
		String COPPER = "Copper";
		String NON_FEASIBLE = "Non_Feasible";
		String OTHERS = "Others";
		String FIBER_FX_CHOKE = "Fiber_Fx_Choke";

	}
	
	public interface LocationType
	{
		int city = 1;
		int branch = 2;
		int area = 3;
	}
	public interface DefaultThereadPoolConfig
	{
		int DEFAULT_THEREAD_POOL_SIZE = 30;
	
	}
	
	public interface RemarkType
	{
		int POA = 1;
		int POI = 2;
	}
	
	public interface AadhaarTransactionType 
	{
		int PRIMARY = 20001;
		int SECONDARY = 20002;
		String _PRIMARY = "PRIMARY";
		String _SECONDARY = "SECONDARY";
	}
	
	public interface PdfCoOrdinates
	{
		
		public static final String SRC = "/usr/share/fwmp/PDFEDITOR/EcafTemplate_in.pdf";
		public static final String DEST = "//usr/share/fwmp/PDFEDITOR/EcafTemplate_out.pdf";
		public static final String TICK = "/usr/share/fwmp/tick.png";
		
		String DRIVING = "DRIVING";
		String PASSPORT = "PASSPORT";
		String PANCARD = "PANCARD";
		String BILL = "BILL";
		
		String ONLINE = "ONLINE";
		String CASH = "CASH";
		String CHEQUE = "CHEQUE";
		String DEMAND = "DEMAND";
		
		String HOME = "HOME";
		String SOHO = "SOHO";
		String CORPORATE = "CORPORATE";
		String CYBER = "CYBER";
		
		String COPPER = "COPPER";
		String FIBER = "FIBER";
		
		
		int PIC_MAX_WIDTH = 100;
		int PIC_MAX_HEIGHT = 150;
		int TICK_MAX_WIDTH = 8;
		int TICK_MAX_HEIGHT = 8;
		int IMAGE_X = 464;
		int IMAGE_Y = 628;
		int CAF_NO_X = 84;
		int CAF_NO_Y = 675;
		String CAF_NO = "123456";
		int CREATION_DATE_X = 210;
		int CREATION_DATE_Y = 675;
		String CREATION_DATE = "08-17-2017";
		int PLACE_X = 330;
		int PLACE_Y = 675;
		String PLACE = "BANGLORE";
		int CUSTOMER_NAME_X = 128;
		int CUSTOMER_NAME_Y = 614;
		String CUSTOMER_NAME = "MALLA SRINIVAS KIRAN";
		int PRIMARY_TX_X = 130;
		int PRIMARY_TX_Y = 570;
		String PRIMARY_TX = "ekyc_5189427971512563472373";
		int PRIMARY_TX_DATE_X = 399;
		int PRIMARY_TX_DATE_Y = 570;
		String PRIMARY_TX_DATE = "06/12/2017";
		int PRIMARY_TX_TIME_X = 489;
		int PRIMARY_TX_TIME_Y = 570;
		String PRIMARY_TX_TIME = "18:02:56";
		int PROFESSION_X = 130;
		int PROFESSION_Y = 551;
		String PROFESSION = "BUSINESSMAN";
		int CARE_OF_X = 140;
		int CARE_OF_Y = 530;
		String CARE_OF = "S/O Rajulu";
		int GENDER_X = 370;
		int GENDER_Y = 530;
		String GENDER = "M";
		int AADHAR_ADDRESS_X = 119;
		int AADHAR_ADDRESS_Y = 509;
		String AADHAR_ADDRESS = "86-18-31, pedda veedhi, Rajahmundry urban, V L Puram, East Godavari, Andhra Pradesh, 533101,";
		int VTCNAME_X = 69;
		int VTCNAME_Y = 468;
		String VTCNAME = "Rajahmundry";
		int AADHAR_STATE_X = 238;
		int AADHAR_STATE_Y = 468;
		String AADHAR_STATE = "Andhra Pradesh";
		int AADHAR_PINCODE_X = 420;
		int AADHAR_PINCODE_Y = 468;
		String AADHAR_PINCODE = "560017";
		int AADHAR_MOBILE_NUMBER_X = 326;
		int AADHAR_MOBILE_NUMBER_Y = 448;
		String AADHAR_MOBILE_NUMBER = "9738434213";
		int ADDRESS_X = 119;
		int ADDRESS_Y = 425;
		String ADDRESS = "HSR layout";
		int CITY_X = 69;
		int CITY_Y = 386;
		String CITY = "Banglore";
		int STATE_X = 238;
		int STATE_Y = 386;
		String STATE = "Karnataka";
		int PINCODE_X = 420;
		int PINCODE_Y = 386;
		String PINCODE = "560017";
		int MOBILE_NUMBER_X = 326;
		int MOBILE_NUMBER_Y = 365;
		String MOBILE_NUMBER = "9738434213";
		int ALTERNATE_MOBILE_NUMBER_X = 474;
		int ALTERNATE_MOBILE_NUMBER_Y = 365;
		String ALTERNATE_MOBILE_NUMBER = "9090909090";
		int DRIVING_X = 231;
		int DRIVING_Y = 321;
		int PASSPORT_X = 312;
		int PASSPORT_Y = 321;
		int PANCARD_X = 393;
		int PANCARD_Y = 321;
		int RATION_CARD_X = 520;
		int RATION_CARD_Y = 321;
		int EMAIL_ADDRESS_X = 412;
		int EMAIL_ADDRESS_Y = 264;
		String EMAIL_ADDRESS = "mallasrinivaskiran@gmail.com";
		int STATIC_IP_YES_X = 205;
		int STATIC_IP_YES_Y = 177;
		int STATIC_IP_NO_X = 365;
		int STATIC_IP_NO_Y = 177;
		int CX_PERMISSION_YES_X = 205;
		int CX_PERMISSION_YES_Y = 154;
		int CX_PERMISSION_NO_X = 365;
		int CX_PERMISSION_NO_Y = 154;
		int TARIFF_PLAN_X = 148;
		int TARIFF_PLAN_Y = 757;
		String TARIFF_PLAN = "ACT Blaze 6 M + 1 M_FT";
		int TARIFF_PLAN_DURATION_1_X = 382;
		int TARIFF_PLAN_DURATION_1_Y = 756;
		int TARIFF_PLAN_DURATION_3_X = 436;
		int TARIFF_PLAN_DURATION_3_Y = 756;
		int TARIFF_PLAN_DURATION_6_X = 491;
		int TARIFF_PLAN_DURATION_6_Y = 756;
		int TARIFF_PLAN_DURATION_12_X = 549;
		int TARIFF_PLAN_DURATION_12_Y = 756;
		int ROUTER_YES_X = 437;
		int ROUTER_YES_Y = 733;
		int ROUTER_NO_X = 488;
		int ROUTER_NO_Y = 733;
		int INSTALLATION_CHARGES_X = 62;
		int INSTALLATION_CHARGES_Y = 714;
		String INSTALLATION_CHARGES = "434.78";
		int TOTAL_CHARGES_X = 437;
		int TOTAL_CHARGES_Y = 714;
		String TOTAL_CHARGES = "9426.0";
		int ONLINE_X = 227;
		int ONLINE_Y = 675;
		int CASH_X = 314;
		int CASH_Y = 675;
		int CHEQUE_X = 410;
		int CHEQUE_Y = 675;
		int DRAFT_X = 526;
		int DRAFT_Y = 675;
		int SIGN_MAX_WIDTH = 60;
		int SIGN_MAX_HEIGHT = 25;
		int SIGN_X = 53;
		int SIGN_Y = 415;
		int SIGN_DATE_X = 227;
		int SIGN_DATE_Y = 416;
		String SIGN_DATE = "06.12.2017";
		int SIGN_PLACE_X = 397;
		int SIGN_PLACE_Y = 416;
		String SIGN_PLACE = "BANGLORE";
		int SECONDRY_TX_X = 130;
		int SECONDRY_TX_Y = 380;
		String SECONDRY_TX = "ekyc_5189427971512563472373";
		int SECONDRY_TX_DATE_X = 399;
		int SECONDRY_TX_DATE_Y = 380;
		String SECONDRY_TX_DATE = "06/12/2017";
		int SECONDRY_TX_TIME_X = 489;
		int SECONDRY_TX_TIME_Y = 380;
		String SECONDRY_TX_TIME = "18:02:56";
		int HOME_X = 175;
		int HOME_Y = 275;
		int SOHO_X = 289;
		int SOHO_Y = 275;
		int CORPORATE_X = 417;
		int CORPORATE_Y = 275;
		int CYBER_CAFE_X = 549;
		int CYBER_CAFE_Y = 275;
		int INBOUND_X = 177;
		int INBOUND_Y = 252;
		int CALL_X = 293;
		int CALL_Y = 252;
		int REFERRAL_X = 419;
		int REFERRAL_Y = 252;
		int CAT5_X = 177;
		int CAT5_Y = 230;
		int FIBER_X = 294;
		int FIBER_Y = 230;
		int SE_NAME_X = 52;
		int SE_NAME_Y = 201;
		String SE_NAME = "Venkateshappa - SE";
		int CFE_NAME_X = 52;
		int CFE_NAME_Y = 160;
		String CFE_NAME = "Sridhar - CFE";
		int D_1_X = 108;
		int D_1_Y = 590;
		String D_1 = "1";
		int D_2_X = 121;
		int D_2_Y = 590;
		String D_2 = "1";
		int M_1_X = 134;
		int M_1_Y = 590;
		String M_1 = "1";
		int M_2_X = 147;
		int M_2_Y = 590;
		String M_2 = "1";
		int Y_1_X = 160;
		int Y_1_Y = 590;
		String Y_1 = "1";
		int Y_2_X = 173;
		int Y_2_Y = 590;
		String Y_2 = "1";
		int Y_3_X = 186;
		int Y_3_Y = 590;
		String Y_3 = "1";
		int Y_4_X = 199;
		int Y_4_Y = 590;
		String Y_4 = "1";
		int DECLARATION_SIGN_X = 52;
		int DECLARATION_SIGN_Y = 788;
		int DECLARATION_SIGN_DATE_X = 227;
		int DECLARATION_SIGN_DATE_Y = 788;
		int DECLARATION_SIGN_PLACE_X = 397;
		int DECLARATION_SIGN_PLACE_Y = 788;
	}
	
	public interface CurrentCrmNames
	{
		String CRM_MQ = "MQ";
		String CRM_SIEBEL = "SIEBEL";
		
		String DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL = "SIEBEL";
		
	}

	public interface AddressLinesSeparator {
		
		String ADDRESS_SEPARATOR = ":-:-:";
		
	}
	
	public interface AddressType {
		
		String CURRENT_ADDRESS = "currentAddress";
		String COMMUNICATION_ADDRESS = "communicationAddress";
		String PERMANENT_ADDRESS = "permanentAddress";
		
	}
	public interface DeDupFlag
	{
		int  deDupFlagSuccess = 3001; // cleanProspect
		int  deDupFlagFailure = 3002; // duplicatedProspect
		int  cleanProspect = 0;
		int  duplicatedProspect = 1;
		String cleanProspectString = "Clean Prospect";
		String duplicatedProspectString  = "Duplicated Prospect";
	}
	
	public interface Country_Name {
		String INDIA="India";
	}
	
	public interface ProductName {
		
		String BROADBAND="Broadband";
	}
	
   public interface SubSource{
		
		String CRP="crp";
		String FWMP="fwmp";
		
	}
   
   public interface LineOfBusiness{
		
		String INDIVIDUAL="Individual";
	}
	
	public interface SiebelAddressType {

		String INSTALLATION = "Installation";
		String BILL_TO = "Bill To";
		String AADHAR = "Aadhar";
	}

	public interface FrStatusForSiebel {

		String COMPLETED = "Closed";
		String ASSIGNED = "Assigned";
		String REOPEN = "Reopen";
	}
	
	public interface SiebelTicketStatus{
		
		public static final String CUSTOMER_BASIC_INFO_UPDATED = "CUSTOMER_BASIC_INFO_UPDATED";

		public static final String TARIFF_PLAN_UPDATED = "TARIFF_PLAN_UPDATED";

		public static final String PAYMENT_STILL_PENDING = "PAYMENT_STILL_PENDING";
		public static final String PAYMENT_APPROVED = "PAYMENT_APPROVED";
		public static final String PAYMENT_REJECTED = "PAYMENT_REJECTED";
		public static final String PAYMENT_RECEIVED = "PAYMENT_RECEIVED";
		public static final String PAYMENT_REFUND_INITIATED = "PAYMENT_REFUND_INITIATED";
		public static final String WAITINING_FOR_PAYMENT_REFUND = "WAITINING_FOR_PAYMENT_REFUND";
		public static final String WAITINING_FOR_PAYMENT_REFUND_APPROVAL = "WAITINING_FOR_PAYMENT_REFUND_APPROVAL";

		public static final String POI_DOCUMENTS_APPROVED = "POI_DOCUMENTS_APPROVED";
		public static final String POI_DOCUMENTS_REJECTED = "POI_DOCUMENTS_REJECTED";
		public static final String POI_DOCUMENTS_UPLOADED = "POI_DOCUMENTS_UPLOADED";

		public static final String POA_DOCUMENTS_APPROVED = "POA_DOCUMENTS_APPROVED";
		public static final String POA_DOCUMENTS_REJECTED = "POA_DOCUMENTS_REJECTED";
		public static final String POA_DOCUMENTS_UPLOADED = "POA_DOCUMENTS_UPLOADED";

		public static final String DOCUMENTS_APPROVED = "DOCUMENTS_APPROVED";
		public static final String DOCUMENTS_REJECTED = "DOCUMENTS_REJECTED";
		public static final String DOCUMENTS_UPLOADED = "DOCUMENTS_UPLOADED";

		public static final String FEASIBILITY_UPDATED_DURING_SALE = "FEASIBILITY_UPDATED_DURING_SALE";
		public static final String FEASIBILITY_UPDATED_DURING_MANUAL_FEASIBILITY = "FEASIBILITY_UPDATED_DURING_MANUAL_FEASIBILITY";
		public static final String FEASIBILITY_UPDATED_DURING_DEPLOYMENT = "FEASIBILITY_UPDATED_DURING_DEPLOYMENT";
		public static final String PORT_ACTIVATION_SUCCESSFULL = "PORT_ACTIVATION_SUCCESSFULL";

		public static final String ACTIVATION_COMPLETED = "ACTIVATION_COMPLETED";
		public static final String ACTIVATION_PENDING = "ACTIVATION_PENDING";

		public static final String ETR_APPROVAL_PENDING = "ETR_APPROVAL_PENDING";
		public static final String ETR_APPROVED = "ETR_APPROVED";
		public static final String ETR_REJECTED = "ETR_REJECTED";
		public static final String ETR_UPDATE_AUTO_MODE = "ETR_UPDATE_AUTO_MODE";
		public static final String ETR_UPDATE_MANUAL_MODE = "ETR_UPDATE_MANUAL_MODE";

		public static final String REACH_TEAM_ACTION_PENDING = "REACH_TEAM_ACTION_PENDING";
		public static final String REACH_TEAM_ACTION_COMPLETED = "REACH_TEAM_ACTION_COMPLETED";
		public static final String REACH_TEAM_ACTION_REJECTED = "REACH_TEAM_ACTION_REJECTED";

		public static final String ACTIVATION_COMPLETED_VERIFIED = "ACTIVATION_COMPLETED_VERIFIED";

		public static final String PERMISSION_PENDING_BY_SALES = "PERMISSION_PENDING_BY_SALES";
		public static final String PERMISSION_GRANTED_BY_SALES = "PERMISSION_GRANTED_BY_SALES";
		public static final String PERMISSION_REJECTED_BY_SALES = "PERMISSION_REJECTED_BY_SALES";
		public static final String FEASIBILITY_PENDING_BY_NE = "FEASIBILITY_PENDING_BY_NE";
		public static final String FEASIBILITY_GRANTED_BY_NE = "FEASIBILITY_GRANTED_BY_NE";
		public static final String FEASIBILITY_REJECTED_BY_NE = "FEASIBILITY_REJECTED_BY_NE";
		public static final String FEASIBILITY_REJECTED_BY_NE_DURING_DEPLOYMENT = "FEASIBILITY_REJECTED_BY_NE_DURING_DEPLOYMENT";

		public static final String  PHYSICAL_FEASIBILITY = "PHYSICAL_FEASIBILITY";
		public static final String  CUSTOMER_REJECTED_BY_SALES = "CUSTOMER_REJECTED_BY_SALES";

		public static final String  SALES_UNASSIGNED_TICKET = "SALES_UNASSIGNED_TICKET";
		public static final String  WO_UNASSIGNED_TICKET = "WO_UNASSIGNED_TICKET";

		public static final String  CUSTOMER_ACCOUNT__ACTIVATE_SUCCESSFULLY ="CUSTOMER_ACCOUNT__ACTIVATE_SUCCESSFULLY";

		public static final String CX_RACK_FIXING_DONE = "CX_RACK_FIXING_DONE";
		public static final String COPPER_LAYING_DONE ="COPPER_LAYING_DONE";
		public static final String FIBER_LAYING_DONE = "FIBER_LAYING_DONE";
		public static final String SPLICING_DONE = "SPLICING_DONE";
		public static final String MATERIAL_CONSUMPTION_UPDATED = "MATERIAL_CONSUMPTION_UPDATED";
		public static final String CUSTOMER_ACCOUNT_ACTIVATION_DONE = "CUSTOMER_ACCOUNT_ACTIVATION_DONE";
		public static final String PORT_ACTIVATION_DONE = "PORT_ACTIVATION_DONE";
		public static final String PHYSICAL_FEASIBILITY_DONE = "PHYSICAL_FEASIBILITY_DONE";
		public static final String WAITING_FOR_WO_GENERATION = "WAITING_FOR_WO_GENERATION";
		public static final String WO_GENERATED = "WO_GENERATED";
	}
	
	
	public interface SiebelACTETRFormat{
		
		String READ_ETR_FORMAT_FROM_DB="yyyy-mm-dd HH:MM:SS";
		String SIEBEL_ACT_ETR_DATE_FORMAT ="mm/dd/yyyy HH:MM:SS";
	}
		
	public interface SiebelWorkOrderActivityType {

		String CustomerAccountActivation = "Customer Account Activation";
		String CX_RackFixing = "CX RackFixing";
		String PortActivation = "Port Activation";
		String CopperLaying ="Copper Laying";
		String FiberLaying ="Fiber Laying";
		String Splicing = "Splicing";
		String MaterialConsumption = "Material Consumption";
		String PhysicalFeasibility = "Physical Feasibility";

	}
	
	public interface FWMP_Flows
	{
		public interface Fiber_Flow {

			String PHYSICAL_FEASIBILTIY = "physical feasibiltiy";
//Changed by manjuprasad for cx_rack_fixing issue			
//			String CX_RACK_FIXING = "Cx Rack fixing";
			String CX_RACK_FIXING = "Cx Rack Fixing";
			String SPLICING = "Splicing";
			String FIBER_LAYING = "Fiber Laying";
			String MATERIAL_CONSUMPTION = "Material Consumption";
			String PORT_ACTIVATION = "Port Activation";
			String CUSTOMER_ACCOUNT_ACTIVATION = "Customer Account Activation";
		}

		public interface Copper_Flow {
			
			String COPPER_LAYING = "Copper Laying";
			String PHYSICAL_FEASIBILTIY = "Physical Feasibility";
			String MATERIAL_CONSUMPTION = "Material Consumption";
			String PORT_ACTIVATION = "Port Activation";
			String CUSTOMER_ACCOUNT_ACTIVATION = "Customer Account Activation";

		}
		
		public interface SiebelWorkOrderStatus{
			
			String INPROGRESS="In Progress";
			String COMPLETED="Closed";
			String ROLLBACK ="Rollback";
		}
		
		
		public interface SiebelConnectionType{
			
			String COPPER="Copper";
			String FIBER="Fiber";
		}
		
		
		public interface Sales_Flow {
			
			String CUSTOMER_BASIC_INFO_UPDATED = "Basic Info Updated";
			String TARIFF_PLAN_UPDATED = "Tariff Updated";
			String POA_DOCUMENT_UPDATED = "Poa_Document_Updated";
			String POI_DOCUMENT_UPDATED="Poi_Document_Updated";
			String PAYMENT_UPDATED="Payment_Updated";
	}
		
		public interface MileStoneStatus {
			
			String APPROVED = "Approved";
			String REJECTED = "Rejected";
			String COMPLETED = "Completed";
			String ROLLBACK = "Rollback";
			String UPLOADED = "Uploaded";
			
			
		}
		public interface SibelWoCloserCodes {
			String RESOLUTION_CODE = "INSTALLATION DONE";
			String SUB_RESOLUTION_CODE = "INSTALLATION DONE";
			
			String REFUND_RESOLUTION_CODE = "INS Not Done-Payment Reversal";
			String REFUND_SUB_RESOLUTION_CODE = "Subscriber Not Interested";
		}
		
	}
	
	


}
