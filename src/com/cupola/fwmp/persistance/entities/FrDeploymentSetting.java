package com.cupola.fwmp.persistance.entities;

public class FrDeploymentSetting
{
	private long id;
	private String defectCode;
	private String subDefectCode;
	private String mqClosureCode;
	private String defectSubDefectLogicalCode;
	private String subDefectCodeValue;
	private String defectCodeValue;
	private String cityName;
	private String resolutionDescription;
	private String flowId;
	
	/**
	 * @return the flowId
	 */
	public String getFlowId() {
		return flowId;
	}
	/**
	 * @param flowId the flowId to set
	 */
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getCityName()
	{
		return cityName;
	}
	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public String getDefectCode()
	{
		return defectCode;
	}
	public void setDefectCode(String defectCode)
	{
		this.defectCode = defectCode;
	}
	public String getSubDefectCode()
	{
		return subDefectCode;
	}
	public void setSubDefectCode(String subDefectCode)
	{
		this.subDefectCode = subDefectCode;
	}
	public String getMqClosureCode()
	{
		return mqClosureCode;
	}
	
	public void setMqClosureCode(String mqClosureCode)
	{
		this.mqClosureCode = mqClosureCode;
	}
	public String getDefectSubDefectLogicalCode()
	{
		return defectSubDefectLogicalCode;
	}
	public void setDefectSubDefectLogicalCode(String defectSubDefectLogicalCode)
	{
		this.defectSubDefectLogicalCode = defectSubDefectLogicalCode;
	}
	public String getSubDefectCodeValue()
	{
		return subDefectCodeValue;
	}
	public void setSubDefectCodeValue(String subDefectCodeValue)
	{
		this.subDefectCodeValue = subDefectCodeValue;
	}
	public String getDefectCodeValue()
	{
		return defectCodeValue;
	}
	public void setDefectCodeValue(String defectCodeValue)
	{
		this.defectCodeValue = defectCodeValue;
	}
	public String getResolutionDescription()
	{
		return resolutionDescription;
	}
	public void setResolutionDescription(String resolutionDescription)
	{
		this.resolutionDescription = resolutionDescription;
	}
	@Override
	public String toString() {
		return "FrDeploymentSetting [id=" + id + ", defectCode=" + defectCode + ", subDefectCode=" + subDefectCode
				+ ", mqClosureCode=" + mqClosureCode + ", defectSubDefectLogicalCode=" + defectSubDefectLogicalCode
				+ ", subDefectCodeValue=" + subDefectCodeValue + ", defectCodeValue=" + defectCodeValue + ", cityName="
				+ cityName + ", resolutionDescription=" + resolutionDescription + ", flowId=" + flowId + "]";
	}
	
	
}
