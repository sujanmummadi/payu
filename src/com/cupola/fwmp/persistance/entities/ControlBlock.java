package com.cupola.fwmp.persistance.entities;

// Generated Oct 24, 2016 1:17:20 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * ControlBlock generated by hbm2java
 */
public class ControlBlock implements java.io.Serializable {

	private long ticketId;
	private Integer cbStatus;
	private Integer status;
	private Date addedOn;
	private Date updatedOn;
	private Set scratchPads = new HashSet(0);

	public ControlBlock() {
	}

	public ControlBlock(long ticketId) {
		this.ticketId = ticketId;
	}

	public ControlBlock(long ticketId, Integer cbStatus, Integer status,
			Date addedOn, Date updatedOn, Set scratchPads) {
		this.ticketId = ticketId;
		this.cbStatus = cbStatus;
		this.status = status;
		this.addedOn = addedOn;
		this.updatedOn = updatedOn;
		this.scratchPads = scratchPads;
	}

	public long getTicketId() {
		return this.ticketId;
	}

	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getCbStatus() {
		return this.cbStatus;
	}

	public void setCbStatus(Integer cbStatus) {
		this.cbStatus = cbStatus;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getAddedOn() {
		return this.addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Set getScratchPads() {
		return this.scratchPads;
	}

	public void setScratchPads(Set scratchPads) {
		this.scratchPads = scratchPads;
	}

}
