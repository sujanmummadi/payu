package com.cupola.fwmp.persistance.entities;

// Generated Aug 4, 2016 3:14:14 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * City generated by hbm2java
 */
public class City implements java.io.Serializable
{

	private long id;
	private String cityName;
	private String cityCode;
	private String country;
	private String state;
	private String cityDiscription;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;
	private Set customers = new HashSet(0);
	private Set vendors = new HashSet(0);
	private Set branches = new HashSet(0);
	private Set users = new HashSet(0);
	private Set devices = new HashSet(0);
	private Set vendorDetailses = new HashSet(0);

	public City()
	{
	}

	public City(long id, String cityName, String cityCode)
	{
		this.id = id;
		this.cityName = cityName;
		this.cityCode = cityCode;
	}

	public City(long id, String cityName, String cityCode, String country,
			String state, String cityDiscription, Long addedBy, Date addedOn,
			Long modifiedBy, Date modifiedOn, Integer status, Set customers,
			Set vendors, Set branches, Set users, Set devices,
			Set vendorDetailses)
	{
		this.id = id;
		this.cityName = cityName;
		this.cityCode = cityCode;
		this.country = country;
		this.state = state;
		this.cityDiscription = cityDiscription;
		this.addedBy = addedBy;
		this.addedOn = addedOn;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.status = status;
		this.customers = customers;
		this.vendors = vendors;
		this.branches = branches;
		this.users = users;
		this.devices = devices;
		this.vendorDetailses = vendorDetailses;
	}

	public long getId()
	{
		return this.id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getCityName()
	{
		return this.cityName;
	}

	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}

	public String getCityCode()
	{
		return this.cityCode;
	}

	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}

	public String getCountry()
	{
		return this.country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getState()
	{
		return this.state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getCityDiscription()
	{
		return this.cityDiscription;
	}

	public void setCityDiscription(String cityDiscription)
	{
		this.cityDiscription = cityDiscription;
	}

	public Long getAddedBy()
	{
		return this.addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Date getAddedOn()
	{
		return this.addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getModifiedBy()
	{
		return this.modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return this.modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus()
	{
		return this.status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Set getCustomers()
	{
		return this.customers;
	}

	public void setCustomers(Set customers)
	{
		this.customers = customers;
	}

	public Set getVendors()
	{
		return this.vendors;
	}

	public void setVendors(Set vendors)
	{
		this.vendors = vendors;
	}

	public Set getBranches()
	{
		return this.branches;
	}

	public void setBranches(Set branches)
	{
		this.branches = branches;
	}

	public Set getUsers()
	{
		return this.users;
	}

	public void setUsers(Set users)
	{
		this.users = users;
	}

	public Set getDevices()
	{
		return this.devices;
	}

	public void setDevices(Set devices)
	{
		this.devices = devices;
	}

	public Set getVendorDetailses()
	{
		return this.vendorDetailses;
	}

	public void setVendorDetailses(Set vendorDetailses)
	{
		this.vendorDetailses = vendorDetailses;
	}

}
