package com.cupola.fwmp.persistance.entities;

// Generated Jul 21, 2016 12:03:01 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * SubActivity generated by hbm2java
 */
public class SubActivity implements java.io.Serializable
{

	private Long id;
	private Activity activity;
	private String subActivityName;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;
	private Integer sequence;
	private Integer skip;
	private Set ticketActivityLogs = new HashSet(0);

	public SubActivity()
	{
	}

	public SubActivity(String subActivityName)
	{
		this.subActivityName = subActivityName;
	}

	public SubActivity(Activity activity, String subActivityName, Long addedBy,
			Date addedOn, Long modifiedBy, Date modifiedOn, Integer status,
			Integer sequence, Integer skip, Set ticketActivityLogs)
	{
		this.activity = activity;
		this.subActivityName = subActivityName;
		this.addedBy = addedBy;
		this.addedOn = addedOn;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.status = status;
		this.sequence = sequence;
		this.skip = skip;
		this.ticketActivityLogs = ticketActivityLogs;
	}

	public Long getId()
	{
		return this.id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Activity getActivity()
	{
		return this.activity;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	public String getSubActivityName()
	{
		return this.subActivityName;
	}

	public void setSubActivityName(String subActivityName)
	{
		this.subActivityName = subActivityName;
	}

	public Long getAddedBy()
	{
		return this.addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Date getAddedOn()
	{
		return this.addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getModifiedBy()
	{
		return this.modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return this.modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus()
	{
		return this.status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Integer getSequence()
	{
		return this.sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}

	public Integer getSkip()
	{
		return this.skip;
	}

	public void setSkip(Integer skip)
	{
		this.skip = skip;
	}

	public Set getTicketActivityLogs()
	{
		return this.ticketActivityLogs;
	}

	public void setTicketActivityLogs(Set ticketActivityLogs)
	{
		this.ticketActivityLogs = ticketActivityLogs;
	}

}
