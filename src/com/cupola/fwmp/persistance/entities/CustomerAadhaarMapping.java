/**
* @Author noor  Oct 30, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.persistance.entities;

import java.util.Date;

/**
 * @Author noor Oct 30, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CustomerAadhaarMapping implements java.io.Serializable {

	private Long customerId;

	private String adhaarTrnId;

	private Long ticketId;

	private Long label;
	private Long activityId;

	private Long addedBy;

	private Date addedOn;

	/**
	 * 
	 */
	public CustomerAadhaarMapping() {
		super();
	}

	/**
	 * @param customerId
	 * @param adhaarTrnId
	 * @param ticketId
	 * @param label
	 * @param addedBy
	 * @param addedOn
	 */
	public CustomerAadhaarMapping(Long customerId, String adhaarTrnId, Long ticketId, Long label, Long activityId,
			Long addedBy, Date addedOn) {
		super();
		this.customerId = customerId;
		this.adhaarTrnId = adhaarTrnId;
		this.ticketId = ticketId;
		this.label = label;
		this.activityId = activityId;
		this.addedBy = addedBy;
		this.addedOn = addedOn;
	}

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
<<<<<<< HEAD
<<<<<<< HEAD
<<<<<<< HEAD
	 * @param customerId
	 *            the customerId to set
=======
	 * @param customerId the customerId to set
>>>>>>> 386149739... Dual Aadhaar authentication implementation
=======
	 * @param customerId
	 *            the customerId to set
>>>>>>> e895eeade... Dual aadhar varification and aadhaar related changes
=======
	 * @param customerId
	 *            the customerId to set
>>>>>>> fwmp_server_rel_ver_2_1_support
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the adhaarTrnId
	 */
	public String getAdhaarTrnId() {
		return adhaarTrnId;
	}

	/**
	 * @param adhaarTrnId
	 *            the adhaarTrnId to set
	 */
	public void setAdhaarTrnId(String adhaarTrnId) {
		this.adhaarTrnId = adhaarTrnId;
	}

	/**
	 * @return the ticketId
	 */
	public Long getTicketId() {
		return ticketId;
	}

	/**
	 * @param ticketId
	 *            the ticketId to set
	 */
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * @return the label
	 */
	public Long getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(Long label) {
		this.label = label;
	}

	/**
	 * @return the addedBy
	 */
	public Long getAddedBy() {
		return addedBy;
	}

	/**
	 * @param addedBy
	 *            the addedBy to set
	 */
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	/**
	 * @return the addedOn
	 */
	public Date getAddedOn() {
		return addedOn;
	}

	/**
	 * @param addedOn
	 *            the addedOn to set
	 */
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	/**
	 * @return the activityId
	 */
	public Long getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            the activityId to set
	 */
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerAadhaarMapping [customerId=" + customerId + ", adhaarTrnId=" + adhaarTrnId + ", ticketId="
				+ ticketId + ", label=" + label + ", activityId=" + activityId + ", addedBy=" + addedBy + ", addedOn="
				+ addedOn + "]";
	}

}
