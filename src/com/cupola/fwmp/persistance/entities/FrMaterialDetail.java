package com.cupola.fwmp.persistance.entities;

import java.util.Date;

public class FrMaterialDetail implements java.io.Serializable
{
	private long id;
	private long ticket;
	private String materialId;
	private String materialRequestQty;
	
	private String materialUnit;
	private String materialRequested;
	private String materialConsumedQty;
	
	private String materialUniqNumber;
	private String materialFiberType;
	private String materialDrumNumber;
	private String materialStartEndPoint;
	private String notifiedToBOR;
	
	private Date addedOn;
	private String addedBy;
	private String modifiedBy;
	private Date modifiedOn;
	
	private Date consumptionAddedOn;
	private String consumptionAddedBy;
	private String consumptionModifiedBy;
	private Date consumptionModifiedOn;
	private String consumedMaterial;
	
	public FrMaterialDetail()
	{
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getTicket()
	{
		return ticket;
	}

	public void setTicket(long ticket)
	{
		this.ticket = ticket;
	}

	public String getMaterialId()
	{
		return materialId;
	}

	public void setMaterialId(String materialId)
	{
		this.materialId = materialId;
	}

	public String getMaterialRequestQty()
	{
		return materialRequestQty;
	}

	public void setMaterialRequestQty(String materialRequestQty)
	{
		this.materialRequestQty = materialRequestQty;
	}

	public String getMaterialUnit()
	{
		return materialUnit;
	}

	public void setMaterialUnit(String materialUnit)
	{
		this.materialUnit = materialUnit;
	}

	public String getMaterialRequested()
	{
		return materialRequested;
	}

	public void setMaterialRequested(String materialRequested)
	{
		this.materialRequested = materialRequested;
	}

	public String getMaterialConsumedQty()
	{
		return materialConsumedQty;
	}

	public void setMaterialConsumedQty(String materialConsumedQty)
	{
		this.materialConsumedQty = materialConsumedQty;
	}

	public String getMaterialUniqNumber()
	{
		return materialUniqNumber;
	}

	public void setMaterialUniqNumber(String materialUniqNumber)
	{
		this.materialUniqNumber = materialUniqNumber;
	}

	public String getMaterialFiberType()
	{
		return materialFiberType;
	}

	public void setMaterialFiberType(String materialFiberType)
	{
		this.materialFiberType = materialFiberType;
	}

	public String getMaterialDrumNumber()
	{
		return materialDrumNumber;
	}

	public void setMaterialDrumNumber(String materialDrumNumber)
	{
		this.materialDrumNumber = materialDrumNumber;
	}

	public String getMaterialStartEndPoint()
	{
		return materialStartEndPoint;
	}

	public void setMaterialStartEndPoint(String materialStartEndPoint)
	{
		this.materialStartEndPoint = materialStartEndPoint;
	}

	public String getNotifiedToBOR()
	{
		return notifiedToBOR;
	}

	public void setNotifiedToBOR(String notifiedToBOR)
	{
		this.notifiedToBOR = notifiedToBOR;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public String getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(String addedBy)
	{
		this.addedBy = addedBy;
	}

	public String getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Date getConsumptionAddedOn()
	{
		return consumptionAddedOn;
	}

	public void setConsumptionAddedOn(Date consumptionAddedOn)
	{
		this.consumptionAddedOn = consumptionAddedOn;
	}

	public String getConsumptionAddedBy()
	{
		return consumptionAddedBy;
	}

	public void setConsumptionAddedBy(String consumptionAddedBy)
	{
		this.consumptionAddedBy = consumptionAddedBy;
	}

	public Date getConsumptionModifiedOn()
	{
		return consumptionModifiedOn;
	}

	public void setConsumptionModifiedOn(Date consumptionModifiedOn)
	{
		this.consumptionModifiedOn = consumptionModifiedOn;
	}

	public String getConsumptionModifiedBy()
	{
		return consumptionModifiedBy;
	}

	public void setConsumptionModifiedBy(String consumptionModifiedBy)
	{
		this.consumptionModifiedBy = consumptionModifiedBy;
	}

	public String getConsumedMaterial()
	{
		return consumedMaterial;
	}

	public void setConsumedMaterial(String consumedMaterial)
	{
		this.consumedMaterial = consumedMaterial;
	}
	
}
