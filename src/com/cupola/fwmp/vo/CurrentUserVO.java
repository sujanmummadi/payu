/**
 * 
 */
package com.cupola.fwmp.vo;

/**
 * @author aditya
 * 
 */
public class CurrentUserVO
{
	private Long assignedBy;
	private Long currentAssignedTo;
	private Long assignedTo;
	private Long ticketId;

	public Long getAssignedBy()
	{
		return assignedBy;
	}

	public void setAssignedBy(Long assignedBy)
	{
		this.assignedBy = assignedBy;
	}

	public Long getCurrentAssignedTo()
	{
		return currentAssignedTo;
	}

	public void setCurrentAssignedTo(Long currentAssignedTo)
	{
		this.currentAssignedTo = currentAssignedTo;
	}

	public Long getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(Long assignedTo)
	{
		this.assignedTo = assignedTo;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	@Override
	public String toString()
	{
		return "CurrentUserVO [assignedBy=" + assignedBy
				+ ", currentAssignedTo=" + currentAssignedTo + ", assignedTo="
				+ assignedTo + ", ticketId=" + ticketId + "]";
	}

}
