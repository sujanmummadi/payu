package com.cupola.fwmp.vo;

import java.util.List;


public class VendorDetailSummaryVO
{
	private String vendorName;
	private Long vendorId;
	private String skillName;
	private VendorQueueData nextJobInQueue;
	private List<VendorQueueData> vendorQueueData;
	
	public VendorDetailSummaryVO(){
	}
	
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public List<VendorQueueData> getVendorQueueData() {
		return vendorQueueData;
	}

	public void setVendorQueueData(List<VendorQueueData> vendorQueueData) {
		this.vendorQueueData = vendorQueueData;
	}

	public VendorQueueData getNextJobInQueue()
	{
		return nextJobInQueue;
	}

	public void setNextJobInQueue(VendorQueueData nextJobInQueue)
	{
		this.nextJobInQueue = nextJobInQueue;
	}
	
}
