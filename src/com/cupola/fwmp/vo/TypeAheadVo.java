package com.cupola.fwmp.vo;

import java.io.Serializable;

public class TypeAheadVo implements Comparable<TypeAheadVo>,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private int status;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public TypeAheadVo(Long id, String name)
	{
		super();
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString()
	{
		return "TypeAheadVo [id=" + id + ", name=" + name + "]";
	}

	public TypeAheadVo()
	{
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeAheadVo other = (TypeAheadVo) obj;
		if (id == null)
		{
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(TypeAheadVo value)
	{
		if (this.name == null || value == null)
			return 0;
			
		return this.getName().compareToIgnoreCase(value.getName());
	}

}
