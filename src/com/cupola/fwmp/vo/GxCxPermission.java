/**
 * 
 */
package com.cupola.fwmp.vo;

/**
 * @author aditya
 *
 */
public class GxCxPermission
{
	private String ticketId;
	private String gxPermission;
	private String cxPermission;
	
	
	public String getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}
	public String getGxPermission()
	{
		return gxPermission;
	}
	public void setGxPermission(String gxPermission)
	{
		this.gxPermission = gxPermission;
	}
	public String getCxPermission()
	{
		return cxPermission;
	}
	public void setCxPermission(String cxPermission)
	{
		this.cxPermission = cxPermission;
	}
	@Override
	public String toString()
	{
		return "GxCxPermission [ticketId=" + ticketId + ", gxPermission="
				+ gxPermission + ", cxPermission=" + cxPermission + "]";
	}

}
