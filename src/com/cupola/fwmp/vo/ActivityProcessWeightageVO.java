package com.cupola.fwmp.vo;

public class ActivityProcessWeightageVO {
	private long id;
	private long activityId;
	private long progressWeightage;
	private long workOrderTypeId;
	private int status;
	private long workStageId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getActivityId() {
		return activityId;
	}
	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}
	public long getProgressWeightage() {
		return progressWeightage;
	}
	public void setProgressWeightage(long progressWeightage) {
		this.progressWeightage = progressWeightage;
	}
	public long getWorkOrderTypeId() {
		return workOrderTypeId;
	}
	public void setWorkOrderTypeId(long workOrderTypeId) {
		this.workOrderTypeId = workOrderTypeId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getWorkStageId() {
		return workStageId;
	}
	public void setWorkStageId(long workStageId) {
		this.workStageId = workStageId;
	}
	@Override
	public String toString() {
		return "ActivityProcessWeightageVO [id=" + id + ", activityId="
				+ activityId + ", progressWeightage=" + progressWeightage
				+ ", workOrderTypeId=" + workOrderTypeId + ", status=" + status
				+ ", workStageId=" + workStageId + "]";
	}
	
}