package com.cupola.fwmp.vo;

import java.util.Date;

public class WorkOrderVo {

	private long id;
	private Long ticketId;
	private Long assignedTo;
	private Long assignedBy;
	private String workOrderNo;
	private Long workOrderTypeId;
	private String currentEtr;

	private Long initialWorkStageId;
	private Long currentWorkStageId;
	private Long activityId;
	private Long gisId;

	private Integer totalPriority;

	private Long currentProgress;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Long getAssignedBy() {
		return assignedBy;
	}

	public void setAssignedBy(Long assignedBy) {
		this.assignedBy = assignedBy;
	}

	public String getWorkOrderNo() {
		return workOrderNo;
	}

	public void setWorkOrderNo(String workOrderNo) {
		this.workOrderNo = workOrderNo;
	}

	public Long getWorkOrderTypeId() {
		return workOrderTypeId;
	}

	public void setWorkOrderTypeId(Long workOrderTypeId) {
		this.workOrderTypeId = workOrderTypeId;
	}

	public String getCurrentEtr() {
		return currentEtr;
	}

	public void setCurrentEtr(String currentEtr) {
		this.currentEtr = currentEtr;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getInitialWorkStageId() {
		return initialWorkStageId;
	}

	public void setInitialWorkStageId(Long initialWorkStageId) {
		this.initialWorkStageId = initialWorkStageId;
	}

	public Long getCurrentWorkStageId() {
		return currentWorkStageId;
	}

	public void setCurrentWorkStageId(Long currentWorkStageId) {
		this.currentWorkStageId = currentWorkStageId;
	}

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public Long getGisId() {
		return gisId;
	}

	public void setGisId(Long gisId) {
		this.gisId = gisId;
	}

	public Integer getTotalPriority() {
		return totalPriority;
	}

	public void setTotalPriority(Integer totalPriority) {
		this.totalPriority = totalPriority;
	}

	public Long getCurrentProgress() {
		return currentProgress;
	}

	public void setCurrentProgress(Long currentProgress) {
		this.currentProgress = currentProgress;
	}

	public Long getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "WorkOrderVo [id=" + id + ", ticketId=" + ticketId
				+ ", assignedTo=" + assignedTo + ", assignedBy=" + assignedBy
				+ ", workOrderNo=" + workOrderNo + ", workOrderTypeId="
				+ workOrderTypeId + ", currentEtr=" + currentEtr
				+ ", initialWorkStageId=" + initialWorkStageId
				+ ", currentWorkStageId=" + currentWorkStageId
				+ ", activityId=" + activityId + ", gisId=" + gisId
				+ ", totalPriority=" + totalPriority + ", currentProgress="
				+ currentProgress + ", addedBy=" + addedBy + ", addedOn="
				+ addedOn + ", modifiedBy=" + modifiedBy + ", modifiedOn="
				+ modifiedOn + ", status=" + status + "]";
	}

}
