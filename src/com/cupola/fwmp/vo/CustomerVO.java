package com.cupola.fwmp.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;

public class CustomerVO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private Long tariffId;
	private Long branchId;
	private Long areaId;
	private Long cityId;
	private Long subAreaId;
	private Long subscriptionTypeId;
	private String companyName;
	private String notes;
	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private Date prefferedCallDate;
	
	private Long prefferedCallTimeStamp;
	private String customerName;
	private String customerType;
	private String mobileNumber;
	private String alternativeMobileNo;
	private String officeNumber;
	private String emailId;
	private String customerProfession;
	private String alternativeEmailId;
	private String crpMobileNo;
	private String crpAccountNo;
	private String mqId;
	private String currentAddress;
	private String communicationAdderss;
	private String permanentAddress;
	private String lat;
	private String lon;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;
	private Integer pincode;
	private String enquiry;
	private String knownSource;
	private Long sourceModeId;
	private Long enquiryModeId;
	private KycDetails kycDetails;
	private boolean aadhaarForPOI;
	private boolean aadhaarForPOA;
	private String finahubTransactionId;
	private Long adhaarId;
	private String couponCode;
	private String nationality;
	private String existingIsp;
	private Boolean isStaticIPRequired;
	private List<CustomerAddressVO> customerAddressVOs;
	private String customerProfile;
	private String uinType;
	private String uin;
	private String subAreaName;
	
	private String aadhaarNumber;
	private String aadhaarTransactionType;
	
	public CustomerVO()
	{

	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the tariffId
	 */
	public Long getTariffId() {
		return tariffId;
	}

	/**
	 * @param tariffId the tariffId to set
	 */
	public void setTariffId(Long tariffId) {
		this.tariffId = tariffId;
	}

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the areaId
	 */
	public Long getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId the areaId to set
	 */
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the cityId
	 */
	public Long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the subAreaId
	 */
	public Long getSubAreaId() {
		return subAreaId;
	}

	/**
	 * @param subAreaId the subAreaId to set
	 */
	public void setSubAreaId(Long subAreaId) {
		this.subAreaId = subAreaId;
	}

	/**
	 * @return the subscriptionTypeId
	 */
	public Long getSubscriptionTypeId() {
		return subscriptionTypeId;
	}

	/**
	 * @param subscriptionTypeId the subscriptionTypeId to set
	 */
	public void setSubscriptionTypeId(Long subscriptionTypeId) {
		this.subscriptionTypeId = subscriptionTypeId;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the prefferedCallDate
	 */
	public Date getPrefferedCallDate() {
		return prefferedCallDate;
	}

	/**
	 * @param prefferedCallDate the prefferedCallDate to set
	 */
	public void setPrefferedCallDate(Date prefferedCallDate) {
		this.prefferedCallDate = prefferedCallDate;
	}

	/**
	 * @return the prefferedCallTimeStamp
	 */
	public Long getPrefferedCallTimeStamp() {
		return prefferedCallTimeStamp;
	}

	/**
	 * @param prefferedCallTimeStamp the prefferedCallTimeStamp to set
	 */
	public void setPrefferedCallTimeStamp(Long prefferedCallTimeStamp) {
		this.prefferedCallTimeStamp = prefferedCallTimeStamp;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the alternativeMobileNo
	 */
	public String getAlternativeMobileNo() {
		return alternativeMobileNo;
	}

	/**
	 * @param alternativeMobileNo the alternativeMobileNo to set
	 */
	public void setAlternativeMobileNo(String alternativeMobileNo) {
		this.alternativeMobileNo = alternativeMobileNo;
	}

	/**
	 * @return the officeNumber
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * @param officeNumber the officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the customerProfession
	 */
	public String getCustomerProfession() {
		return customerProfession;
	}

	/**
	 * @param customerProfession the customerProfession to set
	 */
	public void setCustomerProfession(String customerProfession) {
		this.customerProfession = customerProfession;
	}

	/**
	 * @return the alternativeEmailId
	 */
	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}

	/**
	 * @param alternativeEmailId the alternativeEmailId to set
	 */
	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}

	/**
	 * @return the crpMobileNo
	 */
	public String getCrpMobileNo() {
		return crpMobileNo;
	}

	/**
	 * @param crpMobileNo the crpMobileNo to set
	 */
	public void setCrpMobileNo(String crpMobileNo) {
		this.crpMobileNo = crpMobileNo;
	}

	/**
	 * @return the crpAccountNo
	 */
	public String getCrpAccountNo() {
		return crpAccountNo;
	}

	/**
	 * @param crpAccountNo the crpAccountNo to set
	 */
	public void setCrpAccountNo(String crpAccountNo) {
		this.crpAccountNo = crpAccountNo;
	}

	/**
	 * @return the mqId
	 */
	public String getMqId() {
		return mqId;
	}

	/**
	 * @param mqId the mqId to set
	 */
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}

	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}

	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	/**
	 * @return the communicationAdderss
	 */
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}

	/**
	 * @param communicationAdderss the communicationAdderss to set
	 */
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}

	/**
	 * @return the permanentAddress
	 */
	public String getPermanentAddress() {
		return permanentAddress;
	}

	/**
	 * @param permanentAddress the permanentAddress to set
	 */
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	/**
	 * @return the lat
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * @param lat the lat to set
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}

	/**
	 * @return the lon
	 */
	public String getLon() {
		return lon;
	}

	/**
	 * @param lon the lon to set
	 */
	public void setLon(String lon) {
		this.lon = lon;
	}

	/**
	 * @return the addedBy
	 */
	public Long getAddedBy() {
		return addedBy;
	}

	/**
	 * @param addedBy the addedBy to set
	 */
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	/**
	 * @return the addedOn
	 */
	public Date getAddedOn() {
		return addedOn;
	}

	/**
	 * @param addedOn the addedOn to set
	 */
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	/**
	 * @return the modifiedBy
	 */
	public Long getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the pincode
	 */
	public Integer getPincode() {
		return pincode;
	}

	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the enquiry
	 */
	public String getEnquiry() {
		return enquiry;
	}

	/**
	 * @param enquiry the enquiry to set
	 */
	public void setEnquiry(String enquiry) {
		this.enquiry = enquiry;
	}

	/**
	 * @return the knownSource
	 */
	public String getKnownSource() {
		return knownSource;
	}

	/**
	 * @param knownSource the knownSource to set
	 */
	public void setKnownSource(String knownSource) {
		this.knownSource = knownSource;
	}

	/**
	 * @return the sourceModeId
	 */
	public Long getSourceModeId() {
		return sourceModeId;
	}

	/**
	 * @param sourceModeId the sourceModeId to set
	 */
	public void setSourceModeId(Long sourceModeId) {
		this.sourceModeId = sourceModeId;
	}

	/**
	 * @return the enquiryModeId
	 */
	public Long getEnquiryModeId() {
		return enquiryModeId;
	}

	/**
	 * @param enquiryModeId the enquiryModeId to set
	 */
	public void setEnquiryModeId(Long enquiryModeId) {
		this.enquiryModeId = enquiryModeId;
	}

	/**
	 * @return the kycDetails
	 */
	public KycDetails getKycDetails() {
		return kycDetails;
	}

	/**
	 * @param kycDetails the kycDetails to set
	 */
	public void setKycDetails(KycDetails kycDetails) {
		this.kycDetails = kycDetails;
	}

	/**
	 * @return the aadhaarForPOI
	 */
	public boolean isAadhaarForPOI() {
		return aadhaarForPOI;
	}

	/**
	 * @param aadhaarForPOI the aadhaarForPOI to set
	 */
	public void setAadhaarForPOI(boolean aadhaarForPOI) {
		this.aadhaarForPOI = aadhaarForPOI;
	}

	/**
	 * @return the aadhaarForPOA
	 */
	public boolean isAadhaarForPOA() {
		return aadhaarForPOA;
	}

	/**
	 * @param aadhaarForPOA the aadhaarForPOA to set
	 */
	public void setAadhaarForPOA(boolean aadhaarForPOA) {
		this.aadhaarForPOA = aadhaarForPOA;
	}

	/**
	 * @return the finahubTransactionId
	 */
	public String getFinahubTransactionId() {
		return finahubTransactionId;
	}

	/**
	 * @param finahubTransactionId the finahubTransactionId to set
	 */
	public void setFinahubTransactionId(String finahubTransactionId) {
		this.finahubTransactionId = finahubTransactionId;
	}

	/**
	 * @return the adhaarId
	 */
	public Long getAdhaarId() {
		return adhaarId;
	}

	/**
	 * @param adhaarId the adhaarId to set
	 */
	public void setAdhaarId(Long adhaarId) {
		this.adhaarId = adhaarId;
	}
	
	

	/**
	 * @return the aadhaarTransactionType
	 */
	public String getAadhaarTransactionType() {
		return aadhaarTransactionType;
	}

	/**
	 * @param aadhaarTransactionType the aadhaarTransactionType to set
	 */
	public void setAadhaarTransactionType(String aadhaarTransactionType) {
		this.aadhaarTransactionType = aadhaarTransactionType;
	}

	/**
	 * @return the couponCode
	 */
	public String getCouponCode() {
		return couponCode;
	}

	/**
	 * @param couponCode the couponCode to set
	 */
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the existingIsp
	 */
	public String getExistingIsp() {
		return existingIsp;
	}

	/**
	 * @param existingIsp the existingIsp to set
	 */
	public void setExistingIsp(String existingIsp) {
		this.existingIsp = existingIsp;
	}

	/**
	 * @return the customerAddressVOs
	 */
	public List<CustomerAddressVO> getCustomerAddressVOs() {
		return customerAddressVOs;
	}

	/**
	 * @param customerAddressVOs the customerAddressVOs to set
	 */
	public void setCustomerAddressVOs(List<CustomerAddressVO> customerAddressVOs) {
		this.customerAddressVOs = customerAddressVOs;
	}

	/**
	 * @return the customerProfile
	 */
	public String getCustomerProfile() {
		return customerProfile;
	}

	/**
	 * @param customerProfile the customerProfile to set
	 */
	public void setCustomerProfile(String customerProfile) {
		this.customerProfile = customerProfile;
	}

	/**
	 * @return the uinType
	 */
	public String getUinType() {
		return uinType;
	}

	/**
	 * @param uinType the uinType to set
	 */
	public void setUinType(String uinType) {
		this.uinType = uinType;
	}

	/**
	 * @return the uin
	 */
	public String getUin() {
		return uin;
	}

	/**
	 * @param uin the uin to set
	 */
	public void setUin(String uin) {
		this.uin = uin;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	

	/**
	 * @return the isStaticIPRequired
	 */
	public Boolean getIsStaticIPRequired() {
		return isStaticIPRequired;
	}

	
	
	
	/**
	 * @return the subAreaName
	 */
	public String getSubAreaName() {
		return subAreaName;
	}

	/**
	 * @param subAreaName the subAreaName to set
	 */
	public void setSubAreaName(String subAreaName) {
		this.subAreaName = subAreaName;
	}

	/**
	 * @param isStaticIPRequired the isStaticIPRequired to set
	 */
	public void setIsStaticIPRequired(Boolean isStaticIPRequired) {
		this.isStaticIPRequired = isStaticIPRequired;
	}

	public CustomerVO(long id, Long tariffId, Long branchId, Long areaId, Long cityId, Long subAreaId,
			Long subscriptionTypeId, String companyName, String notes, String title, String firstName, String lastName,
			String middleName, Date prefferedCallDate, Long prefferedCallTimeStamp, String customerName,
			String customerType, String mobileNumber, String alternativeMobileNo, String officeNumber, String emailId,
			String customerProfession, String alternativeEmailId, String crpMobileNo, String crpAccountNo, String mqId,
			String currentAddress, String communicationAdderss, String permanentAddress, String lat, String lon,
			Long addedBy, Date addedOn, Long modifiedBy, Date modifiedOn, Integer status, Integer pincode,
			String enquiry, String knownSource, Long sourceModeId, Long enquiryModeId, KycDetails kycDetails,
			boolean aadhaarForPOI, boolean aadhaarForPOA, String finahubTransactionId, Long adhaarId, String couponCode,
			String nationality, String existingIsp, Boolean isStaticIPRequired, List<CustomerAddressVO> customerAddressVOs,
			String customerProfile, String uinType, String uin) {
		super();
		this.id = id;
		this.tariffId = tariffId;
		this.branchId = branchId;
		this.areaId = areaId;
		this.cityId = cityId;
		this.subAreaId = subAreaId;
		
		this.subscriptionTypeId = subscriptionTypeId;
		this.companyName = companyName;
		this.notes = notes;
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.prefferedCallDate = prefferedCallDate;
		this.prefferedCallTimeStamp = prefferedCallTimeStamp;
		this.customerName = customerName;
		this.customerType = customerType;
		this.mobileNumber = mobileNumber;
		this.alternativeMobileNo = alternativeMobileNo;
		this.officeNumber = officeNumber;
		this.emailId = emailId;
		this.customerProfession = customerProfession;
		this.alternativeEmailId = alternativeEmailId;
		this.crpMobileNo = crpMobileNo;
		this.crpAccountNo = crpAccountNo;
		this.mqId = mqId;
		this.currentAddress = currentAddress;
		this.communicationAdderss = communicationAdderss;
		this.permanentAddress = permanentAddress;
		this.lat = lat;
		this.lon = lon;
		this.addedBy = addedBy;
		this.addedOn = addedOn;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.status = status;
		this.pincode = pincode;
		this.enquiry = enquiry;
		this.knownSource = knownSource;
		this.sourceModeId = sourceModeId;
		this.enquiryModeId = enquiryModeId;
		this.kycDetails = kycDetails;
		this.aadhaarForPOI = aadhaarForPOI;
		this.aadhaarForPOA = aadhaarForPOA;
		this.finahubTransactionId = finahubTransactionId;
		this.adhaarId = adhaarId;
		this.couponCode = couponCode;
		this.nationality = nationality;
		this.existingIsp = existingIsp;
		this.isStaticIPRequired = isStaticIPRequired;
		this.customerAddressVOs = customerAddressVOs;
		this.customerProfile = customerProfile;
		this.uinType = uinType;
		this.uin = uin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerVO [id=" + id + ", tariffId=" + tariffId + ", branchId=" + branchId + ", areaId=" + areaId
				+ ", cityId=" + cityId + ", subAreaId=" + subAreaId + ", subscriptionTypeId=" + subscriptionTypeId
				+ ", companyName=" + companyName + ", notes=" + notes + ", title=" + title + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", middleName=" + middleName + ", prefferedCallDate=" + prefferedCallDate
				+ ", prefferedCallTimeStamp=" + prefferedCallTimeStamp + ", customerName=" + customerName
				+ ", customerType=" + customerType + ", mobileNumber=" + mobileNumber + ", alternativeMobileNo="
				+ alternativeMobileNo + ", officeNumber=" + officeNumber + ", emailId=" + emailId
				+ ", customerProfession=" + customerProfession + ", alternativeEmailId=" + alternativeEmailId
				+ ", crpMobileNo=" + crpMobileNo + ", crpAccountNo=" + crpAccountNo + ", mqId=" + mqId
				+ ", currentAddress=" + currentAddress + ", communicationAdderss=" + communicationAdderss
				+ ", permanentAddress=" + permanentAddress + ", lat=" + lat + ", lon=" + lon + ", addedBy=" + addedBy
				+ ", addedOn=" + addedOn + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + ", status="
				+ status + ", pincode=" + pincode + ", enquiry=" + enquiry + ", knownSource=" + knownSource
				+ ", sourceModeId=" + sourceModeId + ", enquiryModeId=" + enquiryModeId + ", kycDetails=" + kycDetails
				+ ", aadhaarForPOI=" + aadhaarForPOI + ", aadhaarForPOA=" + aadhaarForPOA + ", finahubTransactionId="
				+ finahubTransactionId + ", adhaarId=" + adhaarId + ", couponCode=" + couponCode + ", nationality="
				+ nationality + ", existingIsp=" + existingIsp + ", isStaticIPRequired=" + isStaticIPRequired
				+ ", customerAddressVOs=" + customerAddressVOs + ", customerProfile=" + customerProfile + ", uinType="
				+ uinType + ", uin=" + uin + ", subAreaName=" + subAreaName + "]";
	}

	/**
	 * @return the aadhaarNumber
	 */
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	/**
	 * @param aadhaarNumber the aadhaarNumber to set
	 */
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	
	
}

	
	