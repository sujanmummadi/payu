package com.cupola.fwmp.vo;

import java.util.Date;
import java.util.List;

public class UserGroupVo {

	private Long id;
	private String userGroupName;
	private String userGroupDesc;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private String status;
	private String immediateManagerGroup;
	
	private List<TypeAheadVo> roles;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserGroupName() {
		return userGroupName;
	}
	public void setUserGroupName(String userGroupName) {
		this.userGroupName = userGroupName;
	}
	public String getUserGroupDesc() {
		return userGroupDesc;
	}
	public void setUserGroupDesc(String userGroupDesc) {
		this.userGroupDesc = userGroupDesc;
	}
	public Long getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<TypeAheadVo> getRoles() {
		return roles;
	}
	public void setRoles(List<TypeAheadVo> roles) {
		this.roles = roles;
	}
	
	public UserGroupVo() {

	}
	@Override
	public String toString() {
		return "UserGroupVo [id=" + id + ", userGroupName=" + userGroupName
				+ ", userGroupDesc=" + userGroupDesc + ", addedBy=" + addedBy
				+ ", addedOn=" + addedOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + ", status=" + status
				+ ", roles=" + roles + "]";
	}
	/**
	 * @return the immediateManagerGroup
	 */
	public String getImmediateManagerGroup() {
		return immediateManagerGroup;
	}
	/**
	 * @param immediateManagerGroup the immediateManagerGroup to set
	 */
	public void setImmediateManagerGroup(String immediateManagerGroup) {
		this.immediateManagerGroup = immediateManagerGroup;
	}
}
