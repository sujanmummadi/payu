package com.cupola.fwmp.vo;


public class ETRUpdatebyNE
{
	private String ticketId;
	private String currentETR;
	private String newETR;
	private String remarks;
	private String reasonCode;
	private String othersReason;

	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getCurrentETR()
	{
		return currentETR;
	}

	public void setCurrentETR(String currentETR)
	{
		this.currentETR = currentETR;
	}

	public String getNewETR()
	{
		return newETR;
	}

	public void setNewETR(String newETR)
	{
		this.newETR = newETR;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getReasonCode()
	{
		return reasonCode;
	}

	public void setReasonCode(String reasonCode)
	{
		this.reasonCode = reasonCode;
	}

	public String getOthersReason()
	{
		return othersReason;
	}

	public void setOthersReason(String othersReason)
	{
		this.othersReason = othersReason;
	}

	@Override
	public String toString()
	{
		return "ETRUpdatebyNE [ticketId=" + ticketId + ", currentETR="
				+ currentETR + ", newETR=" + newETR + ", remarks=" + remarks
				+ ", reasonCode=" + reasonCode + ", othersReason="
				+ othersReason + "]";
	}

}
