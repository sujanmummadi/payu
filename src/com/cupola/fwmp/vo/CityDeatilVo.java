package com.cupola.fwmp.vo;

import java.util.List;

public class CityDeatilVo
{
	private long cityId;
	private String cityFlowSwitch;
	private List<BranchDetailVo> branchDetail;
	
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public String getCityFlowSwitch() {
		return cityFlowSwitch;
	}
	public void setCityFlowSwitch(String cityFlowSwitch) {
		this.cityFlowSwitch = cityFlowSwitch;
	}
	public List<BranchDetailVo> getBranchDetail() {
		return branchDetail;
	}
	public void setBranchDetail(List<BranchDetailVo> branchDetail) {
		this.branchDetail = branchDetail;
	}
	
	
}
