package com.cupola.fwmp.vo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


public class TicketPriorityVo implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date ticketCreationDate;
	private long ticketId;
	//private String prospectNo;
	private Long assignedTo;
	private String workorderNumber;
	private BigInteger workorderId;
	private Integer priorityValue;
	
	private Integer escalationType;
	private Integer escalatedValue;
	private Integer updatedCount;
	private Long priorityModifiedBy;
	private Long priorityId;
	private Date priorityModifiedOn;
	
	
	public Long getPriorityModifiedBy()
	{
		return priorityModifiedBy;
	}
	
	public void setPriorityModifiedBy(Long priorityModifiedBy)
	{
		this.priorityModifiedBy = priorityModifiedBy;
	}
	
	public Date getPriorityModifiedOn()
	{
		return priorityModifiedOn;
	}
	
	public void setPriorityModifiedOn(Date priorityModifiedOn)
	{
		this.priorityModifiedOn = priorityModifiedOn;
	}
	
	public Integer getEscalationType()
	{
		return escalationType;
	}
	public void setEscalationType(Integer escalationType)
	{
		this.escalationType = escalationType;
	}
	
	public Integer getEscalatedValue()
	{
		return escalatedValue;
	}
	
	public void setEscalatedValue(Integer escalatedValue)
	{
		this.escalatedValue = escalatedValue;
	}
	public Integer getUpdatedCount()
	{
		return updatedCount;
	}
	
	public void setUpdatedCount(Integer updatedCount)
	{
		this.updatedCount = updatedCount;
	}
	
	public Long getPriorityId()
	{
		return priorityId;
	}
	public void setPriorityId(Long priorityId)
	{
		this.priorityId = priorityId;
	}
	
	public Integer getPriorityValue()
	{
		return priorityValue;
	}
	public void setPriorityValue(Integer priority)
	{
		this.priorityValue = priority;
	}
	public Date getTicketCreationDate()
	{
		return ticketCreationDate;
	}
	public void setTicketCreationDate(Date ticketCreationDate)
	{
		this.ticketCreationDate = ticketCreationDate;
	}
	public long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}
	/*public String getProspectNo()
	{
		return prospectNo;
	}
	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}*/
	public Long getAssignedTo()
	{
		return assignedTo;
	}
	public void setAssignedTo(Long assignedTo)
	{
		this.assignedTo = assignedTo;
	}
	public String getWorkorderNumber()
	{
		return workorderNumber;
	}
	public void setWorkorderNumber(String workorderNumber)
	{
		this.workorderNumber = workorderNumber;
	}
	public BigInteger getWorkorderId()
	{
		return workorderId;
	}
	public void setWorkorderId(BigInteger workorderId)
	{
		this.workorderId = workorderId;
	}
	@Override
	public String toString()
	{
		return "TicketPriorityVo [ticketCreationDate=" + ticketCreationDate
				+ ", ticketId=" + ticketId + ", assignedTo=" + assignedTo
				+ ", workorderNumber=" + workorderNumber + ", workorderId="
				+ workorderId + ", priority=" + priorityValue + "]";
	}
	
	
	
}
