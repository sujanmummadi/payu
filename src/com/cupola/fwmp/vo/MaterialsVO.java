package com.cupola.fwmp.vo;

/**
 * 
 * @author Mahesh Chouhan
 * 
 * */

public class MaterialsVO
{

	private Long id;
	private String name;

	public MaterialsVO()
	{
	}

	public MaterialsVO(String name)
	{
		this.name = name;
	}

	public Long getId()
	{
		return this.id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString() {
		return "MaterialsVO [id=" + id + ", name=" + name + "]";
	}

}
