package com.cupola.fwmp.vo.counts;

public class RibbonCountSummary
{
	private Long userId;
	private String firstName;
	private String lastName;
	private Long ticketId;
	private Long initialWorkStage;
	private Long assignedTo;
	private Integer ticketStatus;
	private String ticketSubCategory;
	private Long priorityId;
	private Integer tickePriority;
	
	public RibbonCountSummary(){
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public Long getInitialWorkStage()
	{
		return initialWorkStage;
	}

	public void setInitialWorkStage(Long initialWorkStage)
	{
		this.initialWorkStage = initialWorkStage;
	}

	public Long getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(Long assignedTo)
	{
		this.assignedTo = assignedTo;
	}

	public Integer getTicketStatus()
	{
		return ticketStatus;
	}

	public void setTicketStatus(Integer ticketStatus)
	{
		this.ticketStatus = ticketStatus;
	}

	public String getTicketSubCategory()
	{
		return ticketSubCategory;
	}

	public void setTicketSubCategory(String ticketSubCategory)
	{
		this.ticketSubCategory = ticketSubCategory;
	}

	
	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}

	public Integer getTickePriority() {
		return tickePriority;
	}

	public void setTickePriority(Integer tickePriority) {
		this.tickePriority = tickePriority;
	}

	@Override
	public String toString()
	{
		return "RibbonCountSummary [userId=" + userId + ", firstName="
				+ firstName + ", lastName=" + lastName + ", ticketId="
				+ ticketId + ", initialWorkStage=" + initialWorkStage
				+ ", assignedTo=" + assignedTo + ", ticketStatus="
				+ ticketStatus + ", ticketSubCategory=" + ticketSubCategory
				+ "]";
	}

	
}
