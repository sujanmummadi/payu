/**
 * 
 */
package com.cupola.fwmp.vo.counts;

/**
 * @author aditya
 * 
 */
public class SalesTypeCountVO
{
	private long customerId;
	private int prospectStatus;
	private long ticketId;
	private int ticketStatus;
	private int paymentStatus;
	private int documentStatus;
	private int poaDocumentStatus;
	private int poiDocumentStatus;
	private long assignedTo;
	private String ticketSource;

	public long getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(long customerId)
	{
		this.customerId = customerId;
	}

	public int getProspectStatus()
	{
		return prospectStatus;
	}

	public void setProspectStatus(int prospectStatus)
	{
		this.prospectStatus = prospectStatus;
	}

	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public int getTicketStatus()
	{
		return ticketStatus;
	}

	public void setTicketStatus(int ticketStatus)
	{
		this.ticketStatus = ticketStatus;
	}

	public int getPaymentStatus()
	{
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	public int getDocumentStatus()
	{
		return documentStatus;
	}

	public void setDocumentStatus(int documentStatus)
	{
		this.documentStatus = documentStatus;
	}

	public long getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(long assignedTo)
	{
		this.assignedTo = assignedTo;
	}

	public String getTicketSource()
	{
		return ticketSource;
	}

	public void setTicketSource(String ticketSource)
	{
		this.ticketSource = ticketSource;
	}
	
	

	public int getPoaDocumentStatus() {
		return poaDocumentStatus;
	}

	public void setPoaDocumentStatus(int poaDocumentStatus) {
		this.poaDocumentStatus = poaDocumentStatus;
	}

	public int getPoiDocumentStatus() {
		return poiDocumentStatus;
	}

	public void setPoiDocumentStatus(int poiDocumentStatus) {
		this.poiDocumentStatus = poiDocumentStatus;
	}

	@Override
	public String toString()
	{
		return "SalesTypeCountVO [customerId=" + customerId
				+ ", prospectStatus=" + prospectStatus + ", ticketId="
				+ ticketId + ", ticketStatus=" + ticketStatus
				+ ", paymentStatus=" + paymentStatus + ", documentStatus="
				+ documentStatus + ", assignedTo=" + assignedTo
				+ ", ticketSource=" + ticketSource + "]";
	}
	
	

}
