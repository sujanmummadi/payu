package com.cupola.fwmp.vo.counts;

public class TypeBasedCountVo
{
	private Long id;
	private String name;
	private String title;
	private Integer count;
	
	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public Integer getCount()
	{
		return count;
	}
	public void setCount(Integer count)
	{
		this.count = count;
	}
	public TypeBasedCountVo(Long id, String name, String title, Integer count)
	{
		super();
		this.id = id;
		this.name = name;
		this.title = title;
		this.count = count;
	}
	@Override
	public String toString()
	{
		return "TypeBasedCountVo [id=" + id + ", name=" + name + ", title="
				+ title + ", count=" + count + "]";
	}
}
