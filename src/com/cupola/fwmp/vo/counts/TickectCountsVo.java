package com.cupola.fwmp.vo.counts;

import java.util.List;

public class TickectCountsVo
{
	private List<StatusBasedCountVo> statusCounts;
	private List<TypeBasedCountVo> typeCounts;
	private List<WSBasedCountVo> wsCounts;
	private List<ActivityBasedCountVo> activitiesCounts;
	
	@Override
	public String toString()
	{
		return "TickectCountsVo [statusCounts=" + statusCounts
				+ ", typeCounts=" + typeCounts + ", wsCounts=" + wsCounts
				+ "]\n";
	}

	public List<StatusBasedCountVo> getStatusCounts()
	{
		return statusCounts;
	}

	public void setStatusCounts(List<StatusBasedCountVo> statusCounts)
	{
		this.statusCounts = statusCounts;
	}

	public List<TypeBasedCountVo> getTypeCounts()
	{
		return typeCounts;
	}

	public void setTypeCounts(List<TypeBasedCountVo> typeCounts)
	{
		this.typeCounts = typeCounts;
	}

	public List<WSBasedCountVo> getWsCounts()
	{
		return wsCounts;
	}

	public void setWsCounts(List<WSBasedCountVo> wsCounts)
	{
		this.wsCounts = wsCounts;
	}

	public List<ActivityBasedCountVo> getActivitiesCounts()
	{
		return activitiesCounts;
	}

	public void setActivitiesCounts(List<ActivityBasedCountVo> activitiesCounts)
	{
		this.activitiesCounts = activitiesCounts;
	}
}
