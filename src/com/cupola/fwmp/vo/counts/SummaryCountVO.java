package com.cupola.fwmp.vo.counts;

import java.util.List;


public class SummaryCountVO
{
	private String statusName;
	private Long filterValue;
	private int totalCounts;
	private int pendingCounts;
	private int completedCounts;
	private int filterType;
	private int statusCode;
	private List<Long> ticketIds;
	
	public SummaryCountVO()
	{
		// TODO Auto-generated constructor stub
	}
	
	
	public String getStatusName()
	{
		return statusName;
	}
	public void setStatusName(String statusName)
	{
		this.statusName = statusName;
	}
	public int getTotalCounts()
	{
		return totalCounts;
	}
	public void setTotalCounts(int totalCounts)
	{
		this.totalCounts = totalCounts;
	}
	public int getPendingCounts()
	{
		return pendingCounts;
	}
	public void setPendingCounts(int pendingCounts)
	{
		this.pendingCounts = pendingCounts;
	}
	public int getCompletedCounts()
	{
		return completedCounts;
	}
	public void setCompletedCounts(int completedCounts)
	{
		this.completedCounts = completedCounts;
	}


	public SummaryCountVO(String statusName, Long filterValue,int filterType, int totalCounts,
			int pendingCounts, int completedCounts,int statusCode,List<Long> ticketIds)
	{
		super();
		this.statusName = statusName;
		this.filterValue = filterValue;
		this.totalCounts = totalCounts;
		this.pendingCounts = pendingCounts;
		this.completedCounts = completedCounts;
		this.filterType = filterType;
		this.statusCode = statusCode;
		this.ticketIds = ticketIds;
	}


	public List<Long> getTicketIds() {
		return ticketIds;
	}


	public void setTicketIds(List<Long> ticketIds) {
		this.ticketIds = ticketIds;
	}


	public int getFilterType()
	{
		return filterType;
	}


	public void setFilterType(int filterType)
	{
		this.filterType = filterType;
	}


	public Long getFilterValue()
	{
		return filterValue;
	}


	public void setFilterValue(Long filterValue)
	{
		this.filterValue = filterValue;
	}


	public int getStatusCode()
	{
		return statusCode;
	}


	public void setStatusCode(int statusCode)
	{
		this.statusCode = statusCode;
	}





}
