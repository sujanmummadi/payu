package com.cupola.fwmp.vo.counts.dashboard;

import java.util.List;

public class FRDashBoardCountVO
{
	private List<FRSummaryCountVO> countSummary; 
	private List<FRUserSummaryVO> cxUpUserSummaryCount;
	private List<FRUserSummaryVO> cxDownUserSummaryCount;
	
	public FRDashBoardCountVO(){
	}
		public List<FRSummaryCountVO> getCountSummary()
	{
		return countSummary;
	}
	public void setCountSummary(List<FRSummaryCountVO> countSummary)
	{
		this.countSummary = countSummary;
	}
	public List<FRUserSummaryVO> getCxUpUserSummaryCount() {
		return cxUpUserSummaryCount;
	}
	public void setCxUpUserSummaryCount(List<FRUserSummaryVO> cxUpUserSummaryCount) {
		this.cxUpUserSummaryCount = cxUpUserSummaryCount;
	}
	public List<FRUserSummaryVO> getCxDownUserSummaryCount() {
		return cxDownUserSummaryCount;
	}
	public void setCxDownUserSummaryCount(
			List<FRUserSummaryVO> cxDownUserSummaryCount) {
		this.cxDownUserSummaryCount = cxDownUserSummaryCount;
	}

}
