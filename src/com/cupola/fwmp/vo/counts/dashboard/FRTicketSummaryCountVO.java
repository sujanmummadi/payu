package com.cupola.fwmp.vo.counts.dashboard;

import java.util.List;
import java.util.Set;

public class FRTicketSummaryCountVO
{
	private List<FRUserSummaryVO> cxUpCountSummary;
	private List<FRUserSummaryVO> cxDownCountSummary;
	private List<FRUserSummaryVO> priorityCountSummary;
	private List<FRSummaryCountVO> frHeadCountSummary;
	private List<FRUserSummaryVO> srElementUserDetails;
	private Set<String> cxUpHeader;
	private Set<String> cxDownHeader;
	private Set<String> cxSrElementHeader;
	private Set<String> priorityHeader;

	public List<FRUserSummaryVO> getSrElementUserDetails() {
		return srElementUserDetails;
	}
	public void setSrElementUserDetails(List<FRUserSummaryVO> srElementUserDetails) {
		this.srElementUserDetails = srElementUserDetails;
	}
	public FRTicketSummaryCountVO()
	{
		// TODO Auto-generated constructor stub
	}
	public List<FRUserSummaryVO> getCxUpCountSummary()
	{
		return cxUpCountSummary;
	}

	public void setCxUpCountSummary(List<FRUserSummaryVO> cxUpCountSummary)
	{
		this.cxUpCountSummary = cxUpCountSummary;
	}

	public List<FRUserSummaryVO> getCxDownCountSummary()
	{
		return cxDownCountSummary;
	}

	public void setCxDownCountSummary(List<FRUserSummaryVO> cxDownCountSummary)
	{
		this.cxDownCountSummary = cxDownCountSummary;
	}

	public List<FRUserSummaryVO> getPriorityCountSummary()
	{
		return priorityCountSummary;
	}

	public void setPriorityCountSummary(
			List<FRUserSummaryVO> priorityCountSummary)
	{
		this.priorityCountSummary = priorityCountSummary;
	}

	public FRTicketSummaryCountVO(List<FRUserSummaryVO> cxUpCountSummary,
			List<FRUserSummaryVO> cxDownCountSummary,
			List<FRUserSummaryVO> priorityCountSummary)
	{
		super();
		this.cxUpCountSummary = cxUpCountSummary;
		this.cxDownCountSummary = cxDownCountSummary;
		this.priorityCountSummary = priorityCountSummary;
	}
	
	public List<FRSummaryCountVO> getFrHeadCountSummary() {
		return frHeadCountSummary;
	}
	
	public void setFrHeadCountSummary(List<FRSummaryCountVO> frHeadCountSummary) {
		this.frHeadCountSummary = frHeadCountSummary;
	}
	
	public FRTicketSummaryCountVO(List<FRUserSummaryVO> cxUpCountSummary,
			List<FRUserSummaryVO> cxDownCountSummary,
			List<FRUserSummaryVO> priorityCountSummary,
			List<FRSummaryCountVO> frHeadCountSummary) {
		super();
		this.cxUpCountSummary = cxUpCountSummary;
		this.cxDownCountSummary = cxDownCountSummary;
		this.priorityCountSummary = priorityCountSummary;
		this.frHeadCountSummary = frHeadCountSummary;
	}
	public Set<String> getCxUpHeader()
	{
		return cxUpHeader;
	}
	public void setCxUpHeader(Set<String> cxUpHeader)
	{
		this.cxUpHeader = cxUpHeader;
	}
	public Set<String> getCxDownHeader()
	{
		return cxDownHeader;
	}
	public void setCxDownHeader(Set<String> cxDownHeader)
	{
		this.cxDownHeader = cxDownHeader;
	}
	public Set<String> getCxSrElementHeader()
	{
		return cxSrElementHeader;
	}
	public void setCxSrElementHeader(Set<String> cxSrElementHeader)
	{
		this.cxSrElementHeader = cxSrElementHeader;
	}
	
	public Set<String> getPriorityHeader()
	{
		return priorityHeader;
	}
	public void setPriorityHeader(Set<String> priorityHeader)
	{
		this.priorityHeader = priorityHeader;
	}
	
}
