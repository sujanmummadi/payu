package com.cupola.fwmp.vo.counts.dashboard;

import java.util.ArrayList;
import java.util.List;

public class FRSummaryCountVO
{
	
	private long statusCode;
	
	private int pendingCounts;
	private List<Long> ticketIds;
	private String statusName;
	
	private int totalCounts;
	private int completedCounts;
	
	
	
	public FRSummaryCountVO(){
		this.statusCode = 0l;
		this.pendingCounts = 0;
		this.totalCounts = 0;
		this.completedCounts = 0;
		this.ticketIds = new ArrayList<>();
	}
	
	
	
	public FRSummaryCountVO(String statusName, long statusCode)
	{
		this.statusName = statusName;
		this.statusCode = statusCode;
		this.pendingCounts = 0;
		this.totalCounts = 0;
		this.completedCounts = 0;
		this.ticketIds = new ArrayList<>();
	}



	public FRSummaryCountVO(List<Long> ticketIds, int pendingCounts,
			int totalCounts, int completedCounts) {
		super();
		this.ticketIds = ticketIds;
		this.pendingCounts = pendingCounts;
		this.totalCounts = totalCounts;
		this.completedCounts = completedCounts;
	}

	public FRSummaryCountVO(String statusName, long statusCode,
			List<Long> ticketIds, int pendingCounts, int totalCounts,
			int completedCounts) {
		super();
		this.statusName = statusName;
		this.statusCode = statusCode;
		this.ticketIds = ticketIds;
		this.pendingCounts = pendingCounts;
		this.totalCounts = totalCounts;
		this.completedCounts = completedCounts;
	}

	public String getStatusName()
	{
		return statusName;
	}
	public void setStatusName(String statusName)
	{
		this.statusName = statusName;
	}
	public long getStatusCode()
	{
		return statusCode;
	}
	public void setStatusCode(long statusCode)
	{
		this.statusCode = statusCode;
	}
	public int getTotalCounts()
	{
		return totalCounts;
	}
	public void setTotalCounts(int totalCounts)
	{
		this.totalCounts = totalCounts;
	}
	public int getPendingCounts()
	{
		return pendingCounts;
	}
	public void setPendingCounts(int pendingCounts)
	{
		this.pendingCounts = pendingCounts;
	}
	public int getCompletedCounts()
	{
		return completedCounts;
	}
	public void setCompletedCounts(int completedCounts)
	{
		this.completedCounts = completedCounts;
	}
	public List<Long> getTicketIds()
	{
		return ticketIds;
	}
	public void setTicketIds(List<Long> ticketIds)
	{
		this.ticketIds = ticketIds;
	}
	public FRSummaryCountVO(String statusName, long statusCode,
			List<Long> ticketIds, int pendingCounts)
	{
		super();
		this.statusName = statusName;
		this.statusCode = statusCode;
		this.ticketIds = ticketIds;
		this.pendingCounts = pendingCounts;
	}



	public FRSummaryCountVO(int totalCounts, List<Long> ticketIds,
			String statusName)
	{
		super();
		this.ticketIds = ticketIds;
		this.statusName = statusName;
		this.totalCounts = totalCounts;
	}
}
