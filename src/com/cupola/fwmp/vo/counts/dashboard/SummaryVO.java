package com.cupola.fwmp.vo.counts.dashboard;

import java.util.Map;

public class SummaryVO
{
	private Map<Long, WiPSummaryVO> priorityWiseWiPSummary; // priority and WiPSummary
	// others here

	public Map<Long, WiPSummaryVO> getPriorityWiseWiPSummary()
	{
		return priorityWiseWiPSummary;
	}

	public void setPriorityWiseWiPSummary(
			Map<Long, WiPSummaryVO> priorityWiseWiPSummary)
	{
		this.priorityWiseWiPSummary = priorityWiseWiPSummary;
	}

	@Override
	public String toString()
	{
		return "SummaryVO [priorityWiseWiPSummary=" + priorityWiseWiPSummary
				+ "]";
	}

}
