package com.cupola.fwmp.vo.counts.dashboard;

import java.util.Date;
import java.util.List;

public class FRTicketCountSummaryVO 
{
	private Long ticketId;
	private Date currentEtr;
	private Date commitedEtr;
	private Integer ticketStatus;
	
	private String userName;
	private Long userId;
	
	private List<Long> statusList;
	private Long currentflowId;
	private String currentSymptomName;
	private boolean isLocked;
	
	private Integer priorityValue;
	private Integer escalatedValue;
	private Integer updatedCount;
	private Date ticketCreationDate;
	private Date ticketClosedDate;
	
	private Integer reopenTicket;
	
	public FRTicketCountSummaryVO(){
	}

	public Integer getReopenTicket() {
		return reopenTicket;
	}


	public void setReopenTicket(Integer reopenTicket) {
		this.reopenTicket = reopenTicket;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Long> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<Long> statusList) {
		this.statusList = statusList;
	}

	public Long getCurrentflowId() {
		return currentflowId;
	}


	public void setCurrentflowId(Long currentflowId) {
		this.currentflowId = currentflowId;
	}

	public Date getCurrentEtr() {
		return currentEtr;
	}


	public void setCurrentEtr(Date currentEtr) {
		this.currentEtr = currentEtr;
	}

	public Date getCommitedEtr() {
		return commitedEtr;
	}


	public void setCommitedEtr(Date commitedEtr) {
		this.commitedEtr = commitedEtr;
	}

	public Integer getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(Integer ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getCurrentSymptomName() {
		return currentSymptomName;
	}

	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public Integer getPriorityValue() {
		return priorityValue;
	}

	public void setPriorityValue(Integer priorityValue) {
		this.priorityValue = priorityValue;
	}

	public Integer getEscalatedValue() {
		return escalatedValue;
	}

	public void setEscalatedValue(Integer escalatedValue) {
		this.escalatedValue = escalatedValue;
	}

	public Integer getUpdatedCount() {
		return updatedCount;
	}

	public void setUpdatedCount(Integer updatedCount) {
		this.updatedCount = updatedCount;
	}
	
	public Date getTicketCreationDate() {
		return ticketCreationDate;
	}

	public void setTicketCreationDate(Date ticketCreationDate) {
		this.ticketCreationDate = ticketCreationDate;
	}
	
	public Date getTicketClosedDate() {
		return ticketClosedDate;
	}

	public void setTicketClosedDate(Date ticketClosedDate) {
		this.ticketClosedDate = ticketClosedDate;
	}

	@Override
	public String toString() {
		return "FRTicketCountSummaryVO [ticketId=" + ticketId + ", currentEtr="
				+ currentEtr + ", commitedEtr=" + commitedEtr
				+ ", ticketStatus=" + ticketStatus + ", userName=" + userName
				+ ", userId=" + userId + ", statusList=" + statusList
				+ ", currentflowId=" + currentflowId + ", currentSymptomName="
				+ currentSymptomName + ", isLocked=" + isLocked
				+ ", priorityValue=" + priorityValue + ", escalatedValue="
				+ escalatedValue + ", updatedCount=" + updatedCount + "]";
	}
	
}
