package com.cupola.fwmp.vo.counts.dashboard;

import java.util.List;

public class SEDetails
{
	private String seName;
	private String seId;
	private boolean seStatus;
	private String prospectCount;
	private List<Long> ticketId;

	public String getSeName()
	{
		return seName;
	}

	public void setSeName(String seName)
	{
		this.seName = seName;
	}

	public String getSeId()
	{
		return seId;
	}

	public void setSeId(String seId)
	{
		this.seId = seId;
	}

	public boolean isSeStatus()
	{
		return seStatus;
	}

	public void setSeStatus(boolean seStatus)
	{
		this.seStatus = seStatus;
	}

	public String getProspectCount()
	{
		return prospectCount;
	}

	public void setProspectCount(String prospectCount)
	{
		this.prospectCount = prospectCount;
	}

	public List<Long> getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(List<Long> ticketId)
	{
		this.ticketId = ticketId;
	}

	@Override
	public String toString()
	{
		return "SEDetails [seName=" + seName + ", seId=" + seId + ", seStatus="
				+ seStatus + ", prospectCount=" + prospectCount + ", ticketId="
				+ ticketId + "]";
	}

}
