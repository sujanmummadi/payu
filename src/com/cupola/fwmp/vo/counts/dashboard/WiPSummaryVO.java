package com.cupola.fwmp.vo.counts.dashboard;

import java.util.ArrayList;
import java.util.List;

public class WiPSummaryVO
{
	private long cxRackFixedOtherPending;
	private long coperPending;
	private long fiberDoneRackSplicingPending;
	private long fiberLaidRackFixedSplActiPending;
	private long fiberYetToStart;
	private long activationPending;

	private List<Long> coperPendingTicketIds;
	private List<Long> fiberDoneRackSplicingPendingTicketIds;
	private List<Long> fiberLaidRackFixedSplActiPendingTicketIds;
	private List<Long> fiberYetToStartTicketIds;
	private List<Long> activationPendingTicketIds;
	private List<Long> cxRackFixedOtherPendingTicketIds;

	@Override
	public String toString()
	{
		return "WiPSummaryVO [cxRackFixedOtherPending=" + cxRackFixedOtherPending
				+ ", coperPending=" + coperPending
				+ ", fiberDoneRackSplicingPending="
				+ fiberDoneRackSplicingPending
				+ ", fiberLaidRackFixedSplActiPending="
				+ fiberLaidRackFixedSplActiPending + ", fiberYetToStart="
				+ fiberYetToStart + ", activationPending=" + activationPending
				+ ", coperPendingTicketIds=" + coperPendingTicketIds
				+ ", fiberDoneRackSplicingPendingTicketIds="
				+ fiberDoneRackSplicingPendingTicketIds
				+ ", fiberLaidRackFixedSplActiPendingTicketIds="
				+ fiberLaidRackFixedSplActiPendingTicketIds
				+ ", fiberYetToStartTicketIds=" + fiberYetToStartTicketIds
				+ ", activationPendingTicketIds=" + activationPendingTicketIds
				+ ", cxRackFixedOtherPendingTicketIds=" + cxRackFixedOtherPendingTicketIds + "]";
	}

	public long getCxRackFixedOtherPending()
	{
		return cxRackFixedOtherPending;
	}

	public void setCxRackFixedOtherPending(long cxRackFixedOtherPending)
	{
		this.cxRackFixedOtherPending = cxRackFixedOtherPending;
	}

	public void addToCxRackFixedOtherPending()
	{
		this.cxRackFixedOtherPending++;
	}

	public long getCoperPending()
	{
		return coperPending;
	}

	public void setCoperPending(long coperPending)
	{
		this.coperPending = coperPending;
	}

	public void addToCoperPending()
	{
		this.coperPending++;
	}

	public long getFiberDoneRackSplicingPending()
	{
		return fiberDoneRackSplicingPending;
	}

	public void setFiberDoneRackSplicingPending(
			long fiberDoneRackSplicingPending)
	{
		this.fiberDoneRackSplicingPending = fiberDoneRackSplicingPending;
	}

	public void addToFiberDoneRackSplicingPending()
	{
		this.fiberDoneRackSplicingPending++;
	}

	public long getFiberLaidRackFixedSplActiPending()
	{
		return fiberLaidRackFixedSplActiPending;
	}

	public void setFiberLaidRackFixedSplActiPending(
			long fiberLaidRackFixedSplActiPending)
	{
		this.fiberLaidRackFixedSplActiPending = fiberLaidRackFixedSplActiPending;
	}

	public void addToFiberLaidRackFixedSplActiPending()
	{
		this.fiberLaidRackFixedSplActiPending++;
	}

	public long getFiberYetToStart()
	{
		return fiberYetToStart;
	}

	public void setFiberYetToStart(long fiberYetToStart)
	{
		this.fiberYetToStart = fiberYetToStart;
	}

	public void addToFiberYetToStart()
	{
		this.fiberYetToStart++;
	}

	public long getActivationPending()
	{
		return activationPending;
	}

	public void setActivationPending(long activationPending)
	{
		this.activationPending = activationPending;
	}

	public void addToActivationPending()
	{
		this.activationPending++;
	}

	public List<Long> getCoperPendingTicketIds()
	{
		return coperPendingTicketIds;
	}

	public void setCoperPendingTicketIds(List<Long> coperPendingTicketIds)
	{
		this.coperPendingTicketIds = coperPendingTicketIds;
	}

	public void addToCoperPendingTicketIds(Long ticketId)
	{
		if (this.coperPendingTicketIds == null)
			this.coperPendingTicketIds = new ArrayList<Long>();

		this.coperPendingTicketIds.add(ticketId);
	}

	public List<Long> getFiberDoneRackSplicingPendingTicketIds()
	{
		return fiberDoneRackSplicingPendingTicketIds;
	}

	public void setFiberDoneRackSplicingPendingTicketIds(
			List<Long> fiberDoneRackSplicingPendingTicketIds)
	{
		this.fiberDoneRackSplicingPendingTicketIds = fiberDoneRackSplicingPendingTicketIds;
	}

	public void addToFiberDoneRackSplicingPendingTicketIds(Long ticketId)
	{
		if (this.fiberDoneRackSplicingPendingTicketIds == null)
			this.fiberDoneRackSplicingPendingTicketIds = new ArrayList<Long>();

		this.fiberDoneRackSplicingPendingTicketIds.add(ticketId);
	}
	
	public void addToCxRackFixedOtherPendingTicketIds(Long ticketId)
	{
		if (this.cxRackFixedOtherPendingTicketIds == null)
			this.cxRackFixedOtherPendingTicketIds = new ArrayList<Long>();

		this.cxRackFixedOtherPendingTicketIds.add(ticketId);
	}
	public List<Long> getFiberLaidRackFixedSplActiPendingTicketIds()
	{
		return fiberLaidRackFixedSplActiPendingTicketIds;
	}

	public void setFiberLaidRackFixedSplActiPendingTicketIds(
			List<Long> fiberLaidRackFixedSplActiPendingTicketIds)
	{
		this.fiberLaidRackFixedSplActiPendingTicketIds = fiberLaidRackFixedSplActiPendingTicketIds;
	}

	public void addToFiberLaidRackFixedSplActiPendingTicketIds(Long ticketId)
	{
		if (this.fiberLaidRackFixedSplActiPendingTicketIds == null)
			this.fiberLaidRackFixedSplActiPendingTicketIds = new ArrayList<Long>();

		this.fiberLaidRackFixedSplActiPendingTicketIds.add(ticketId);
	}

	public List<Long> getFiberYetToStartTicketIds()
	{
		return fiberYetToStartTicketIds;
	}

	public void setFiberYetToStartTicketIds(List<Long> fiberYetToStartTicketIds)
	{
		this.fiberYetToStartTicketIds = fiberYetToStartTicketIds;
	}

	public void addToFiberYetToStartTicketIds(Long ticketId)
	{
		if (this.fiberYetToStartTicketIds == null)
			this.fiberYetToStartTicketIds = new ArrayList<Long>();

		this.fiberYetToStartTicketIds.add(ticketId);
	}

	public List<Long> getActivationPendingTicketIds()
	{
		return activationPendingTicketIds;
	}

	public void setActivationPendingTicketIds(
			List<Long> activationPendingTicketIds)
	{
		this.activationPendingTicketIds = activationPendingTicketIds;
	}

	public void addToActivationPendingTicketIds(Long ticketId)
	{
		if (this.activationPendingTicketIds == null)
			this.activationPendingTicketIds = new ArrayList<Long>();

		this.activationPendingTicketIds.add(ticketId);
	}

	public List<Long> getCxRackFixedOtherPendingTicketIds()
	{
		return cxRackFixedOtherPendingTicketIds;
	}

	public void setCxRackFixedOtherPendingTicketIds(
			List<Long> cxRackFixedOtherPendingTicketIds)
	{
		this.cxRackFixedOtherPendingTicketIds = cxRackFixedOtherPendingTicketIds;
	}

	 
}
