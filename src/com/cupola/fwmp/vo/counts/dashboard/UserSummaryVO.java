package com.cupola.fwmp.vo.counts.dashboard;

import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.counts.dashboard.SummaryVO;

public class UserSummaryVO
{
	TypeAheadVo user;
	SummaryVO summary;
	public UserSummaryVO()
	{
		// TODO Auto-generated constructor stub
	}
	public TypeAheadVo getUser()
	{
		return user;
	}
	public void setUser(TypeAheadVo user)
	{
		this.user = user;
	}
	public SummaryVO getSummary()
	{
		return summary;
	}
	public void setSummary(SummaryVO summary)
	{
		this.summary = summary;
	}
	public UserSummaryVO(TypeAheadVo user, SummaryVO summary)
	{
		super();
		this.user = user;
		this.summary = summary;
	}
	@Override
	public String toString()
	{
		return "UserSummaryVO [user=" + user + ", summary=" + summary + "]";
	}
}
