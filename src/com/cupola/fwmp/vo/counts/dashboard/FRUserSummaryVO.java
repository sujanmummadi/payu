package com.cupola.fwmp.vo.counts.dashboard;

import java.util.Map;

public class FRUserSummaryVO
{
	private String userName;
	private Long userId;
	private int online;
	private int totalCount;
	private Map<String, FRSummaryCountVO> countSummary;

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public int getOnline()
	{
		return online;
	}

	public void setOnline(int online)
	{
		this.online = online;
	}

	public int getTotalCount()
	{
		return totalCount;
	}

	public void setTotalCount(int totalCount)
	{
		this.totalCount = totalCount;
	}

	public Map<String, FRSummaryCountVO> getCountSummary()
	{
		return countSummary;
	}

	public void setCountSummary(Map<String, FRSummaryCountVO> countSummary)
	{
		this.countSummary = countSummary;
	}

}
