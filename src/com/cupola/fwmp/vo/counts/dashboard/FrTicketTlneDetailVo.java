package com.cupola.fwmp.vo.counts.dashboard;

public class FrTicketTlneDetailVo
{
	private Long userId;
	private String userName;
	private Long ticketId;
	private String userGroupCode; 
	private Long currentFlowId;
	private String currentSymptomName;
	private String lastName;
	private Integer ticketStatus;
	
	private Long vendorTicketId;
	private Integer vendorTicketStatus;
	private Long requestedBy;
	private Long userType;
	
	private Integer outStatus;
	
	public Integer getOutStatus() {
		return outStatus;
	}

	public void setOutStatus(Integer outStatus) {
		this.outStatus = outStatus;
	}

	public Long getVendorTicketId() {
		return vendorTicketId;
	}

	public void setVendorTicketId(Long vendorTicketId) {
		this.vendorTicketId = vendorTicketId;
	}

	public Integer getVendorTicketStatus() {
		return vendorTicketStatus;
	}

	public void setVendorTicketStatus(Integer vendorTicketStatus) {
		this.vendorTicketStatus = vendorTicketStatus;
	}

	public Long getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(Long requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Integer getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(Integer ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public FrTicketTlneDetailVo(){}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getUserGroupCode() {
		return userGroupCode;
	}

	public void setUserGroupCode(String userGroupCode) {
		this.userGroupCode = userGroupCode;
	}

	public Long getCurrentFlowId() {
		return currentFlowId;
	}

	public void setCurrentFlowId(Long currentFlowId) {
		this.currentFlowId = currentFlowId;
	}

	public String getCurrentSymptomName() {
		return currentSymptomName;
	}

	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}

	public Long getUserType() {
		return userType;
	}

	public void setUserType(Long userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		return "FrTicketTlneDetailVo [userId=" + userId + ", userName="
				+ userName + ", ticketId=" + ticketId + ", userGroupCode="
				+ userGroupCode + ", currentFlowId=" + currentFlowId
				+ ", currentSymptomName=" + currentSymptomName + ", lastName="
				+ lastName + ", ticketStatus=" + ticketStatus
				+ ", vendorTicketId=" + vendorTicketId
				+ ", vendorTicketStatus=" + vendorTicketStatus
				+ ", requestedBy=" + requestedBy + "]";
	}

}
