package com.cupola.fwmp.vo.counts.dashboard;

import java.util.List;

public class DashBoardSummaryVO
{
	private List<UserSummaryVO> userWiseSummary;

	public List<UserSummaryVO> getUserWiseSummary()
	{
		return userWiseSummary;
	}

	public void setUserWiseSummary(List<UserSummaryVO> userWiseSummary)
	{
		this.userWiseSummary = userWiseSummary;
	}
	
	 
	
}
