package com.cupola.fwmp.vo.counts.dashboard;

public class CountRequestVO 
{
	private boolean modified;
	private Long flowType;
	
	public CountRequestVO (){
	}
	 
	public CountRequestVO(boolean isModified, Long flowType) {
		super();
		this.modified = isModified;
		this.flowType = flowType;
	}

	public boolean isModified() {
		return modified;
	}
	
	public void setModified(boolean isModified) {
		this.modified = isModified;
	}
	
	public Long getFlowType() {
		return flowType;
	}
	
	public void setFlowType(Long flowType) {
		this.flowType = flowType;
	}

	@Override
	public String toString() {
		return "CountRequestVO [modified=" + modified + ", flowType="
				+ flowType + "]";
	}
	
}
