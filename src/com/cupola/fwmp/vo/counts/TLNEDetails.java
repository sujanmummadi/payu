package com.cupola.fwmp.vo.counts;

import java.util.List;

public class TLNEDetails
{
	private String neName;
	private long neUserId;
	private long fiberTicketsCount;
	private long copperTicketsCount;
	private long feasibilityTicketsCount;
	private long priorityTicketsCount;
	private long totalTicketsCount;
	private int neStatus;
	private List<Long> totalTicketIds;
	private List<Long> copperTicketIds;
	private List<Long> fiberTicketIds;
	private List<Long> priorityTicketIds;
	private List<Long> feasibilityTicketIds;

	public TLNEDetails(String neName,Long neUserId,
			Long fiberTicketsCount,Long copperTicketsCount,
			Long pendingTicketsCount,Long priorityTicketsCount,
			Long totalTicketsCount, int neStatus)
	{
		this.neName = neName;
		this.neUserId = neUserId;
		this.fiberTicketsCount = fiberTicketsCount;
		this.copperTicketsCount = copperTicketsCount;
		this.priorityTicketsCount = priorityTicketsCount;
		this.totalTicketsCount = totalTicketsCount;
		this.neStatus = neStatus;
	}
	

	public TLNEDetails(String neName, long neUserId, long fiberTicketsCount,
			Long copperTicketsCount, long feasibilityTicketsCount,
			Long priorityTicketsCount, long totalTicketsCount, int neStatus,
			List<Long> totalTicketIds, List<Long> copperTicketIds,
			List<Long> fiberTicketIds, List<Long> priorityTicketIds,
			List<Long> feasibilityTicketIds) {
		super();
		this.neName = neName;
		this.neUserId = neUserId;
		this.fiberTicketsCount = fiberTicketsCount;
		this.copperTicketsCount = copperTicketsCount;
		this.feasibilityTicketsCount = feasibilityTicketsCount;
		this.priorityTicketsCount = priorityTicketsCount;
		this.totalTicketsCount = totalTicketsCount;
		this.neStatus = neStatus;
		this.totalTicketIds = totalTicketIds;
		this.copperTicketIds = copperTicketIds;
		this.fiberTicketIds = fiberTicketIds;
		this.priorityTicketIds = priorityTicketIds;
		this.feasibilityTicketIds = feasibilityTicketIds;
	}

	public List<Long> getTotalTicketIds() {
		return totalTicketIds;
	}

	public void setTotalTicketIds(List<Long> totalTicketIds) {
		this.totalTicketIds = totalTicketIds;
	}

	public List<Long> getCopperTicketIds() {
		return copperTicketIds;
	}

	public void setCopperTicketIds(List<Long> copperTicketIds) {
		this.copperTicketIds = copperTicketIds;
	}

	public List<Long> getFiberTicketIds() {
		return fiberTicketIds;
	}

	public void setFiberTicketIds(List<Long> fiberTicketIds) {
		this.fiberTicketIds = fiberTicketIds;
	}

	public List<Long> getPriorityTicketIds() {
		return priorityTicketIds;
	}

	public void setPriorityTicketIds(List<Long> priorityTicketIds) {
		this.priorityTicketIds = priorityTicketIds;
	}

	public List<Long> getFeasibilityTicketIds() {
		return feasibilityTicketIds;
	}

	public void setFeasibilityTicketIds(List<Long> feasibilityTicketIds) {
		this.feasibilityTicketIds = feasibilityTicketIds;
	}

	public TLNEDetails()
	{
	}

	public String getNeName()
	{
		return neName;
	}

	public void setNeName(String neName)
	{
		this.neName = neName;
	}

	public long getNeUserId()
	{
		return neUserId;
	}

	public void setNeUserId(Long neUserId)
	{
		this.neUserId = neUserId;
	}

	public long getFiberTicketsCount()
	{
		return fiberTicketsCount;
	}

	public void setFiberTicketsCount(Long fiberTicketsCount)
	{
		this.fiberTicketsCount = fiberTicketsCount;
	}

	public long getCopperTicketsCount()
	{
		return copperTicketsCount;
	}

	public void setCopperTicketsCount(Long copperTicketsCount)
	{
		this.copperTicketsCount = copperTicketsCount;
	}

 
	public long getPriorityTicketsCount()
	{
		return priorityTicketsCount;
	}

	public void setPriorityTicketsCount(Long priorityTicketsCount)
	{
		this.priorityTicketsCount = priorityTicketsCount;
	}

	public long getTotalTicketsCount()
	{
		return totalTicketsCount;
	}

	public void setTotalTicketsCount(Long totalTicketsCount)
	{
		this.totalTicketsCount = totalTicketsCount;
	}

	public int getNeStatus()
	{
		return neStatus;
	}

	public void setNeStatus(int neStatus)
	{
		this.neStatus = neStatus;
	}

	@Override
	public String toString()
	{
		return "TLNEDetails [neName=" + neName + ", neUserId=" + neUserId
				+ ", fiberTicketsCount=" + fiberTicketsCount
				+ ", copperTicketsCount=" + copperTicketsCount
				+ ", priorityTicketsCount=" + priorityTicketsCount
				+ ", totalTicketsCount=" + totalTicketsCount + ", neStatus="
				+ neStatus + "]";
	}

	public long getFeasibilityTicketsCount()
	{
		return feasibilityTicketsCount;
	}

	public void setFeasibilityTicketsCount(Long feasibilityTicketsCount)
	{
		this.feasibilityTicketsCount = feasibilityTicketsCount;
	}

}
