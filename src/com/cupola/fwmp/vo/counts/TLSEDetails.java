/**
 * 
 */
package com.cupola.fwmp.vo.counts;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aditya
 * 
 */
public class TLSEDetails
{
	private String seName;
	private Long seUserId;

	private int hotCustomerCount;
	private int coldCustomerCount;
	private int mediumCustomerCount;
	private int pendingProspectCount;

	private List<Long> hotCustomerTicketIds;
	private List<Long> coldCustomerTicketIds;
	private List<Long> mediumCustomerTicketIds;
	private List<Long> pendingProspectTicketIds;

	private int seStatus;

	public TLSEDetails()
	{
	}

	public TLSEDetails(int hotCustomerCount, int coldCustomerCount,
			int mediumCustomerCount, int pendingProspectCount, int seStatus)
	{
		this.hotCustomerCount = hotCustomerCount;
		this.coldCustomerCount = coldCustomerCount;
		this.mediumCustomerCount = mediumCustomerCount;
		this.pendingProspectCount = pendingProspectCount;
		this.seStatus = seStatus;

	}

	public TLSEDetails(String seName, Long seUserId, int hotCustomerCount,
			int coldCustomerCount, int mediumCustomerCount,
			int pendingProspectCount, int seStatus)
	{
		this.seName = seName;
		this.seUserId = seUserId;
		this.hotCustomerCount = hotCustomerCount;
		this.coldCustomerCount = coldCustomerCount;
		this.mediumCustomerCount = mediumCustomerCount;
		this.pendingProspectCount = pendingProspectCount;
		this.seStatus = seStatus;
	}

	public int getHotCustomerCount()
	{
		return hotCustomerCount;
	}

	public void addHotCustomerCount()
	{
		this.hotCustomerCount++;
	}

	public int getColdCustomerCount()
	{
		return coldCustomerCount;
	}

	public void addColdCustomerCount()
	{
		this.coldCustomerCount++;
	}

	public int getMediumCustomerCount()
	{
		return mediumCustomerCount;
	}

	public void addMediumCustomerCount()
	{
		this.mediumCustomerCount++;
	}

	public int getPendingProspectCount()
	{
		return pendingProspectCount;
	}

	public void addPendingProspectCount()
	{
		this.pendingProspectCount++;
	}

	public int getSeStatus()
	{
		return seStatus;
	}

	@Override
	public String toString()
	{
		return "TLSEDetails [seName=" + seName + ", seUserId=" + seUserId
				+ ", hotCustomerCount=" + hotCustomerCount
				+ ", coldCustomerCount=" + coldCustomerCount
				+ ", mediumCustomerCount=" + mediumCustomerCount
				+ ", pendingProspectCount=" + pendingProspectCount
				+ ", seStatus=" + seStatus + "]";
	}

	public List<Long> getHotCustomerTicketIds()
	{
		return hotCustomerTicketIds;
	}

	public void addHotCustomerTicketIds(Long ticketId)
	{
		if (this.hotCustomerTicketIds == null)
			this.hotCustomerTicketIds = new ArrayList<Long>();
		this.hotCustomerTicketIds.add(ticketId);
	}

	public List<Long> getColdCustomerTicketIds()
	{
		return coldCustomerTicketIds;
	}

	public void addColdCustomerTicketIds(Long ticketId)
	{
		if (this.coldCustomerTicketIds == null)
			this.coldCustomerTicketIds = new ArrayList<Long>();
		this.coldCustomerTicketIds.add(ticketId);
	}

	public List<Long> getMediumCustomerTicketIds()
	{
		return mediumCustomerTicketIds;
	}

	public void addMediumCustomerTicketIds(Long ticketId)
	{
		if (this.mediumCustomerTicketIds == null)
			this.mediumCustomerTicketIds = new ArrayList<Long>();
		this.mediumCustomerTicketIds.add(ticketId);
	}

	public List<Long> getPendingProspectTicketIds()
	{
		return pendingProspectTicketIds;
	}

	public void addPendingProspectTicketIds(Long ticketId)
	{
		if (this.pendingProspectTicketIds == null)
			this.pendingProspectTicketIds = new ArrayList<Long>();
		this.pendingProspectTicketIds.add(ticketId);
	}

	public String getSeName()
	{
		return seName;
	}

	public void setSeName(String seName)
	{
		this.seName = seName;
	}

	public Long getSeUserId()
	{
		return seUserId;
	}

	public void setSeUserId(Long seUserId)
	{
		this.seUserId = seUserId;
	}

	public void setHotCustomerCount(int hotCustomerCount)
	{
		this.hotCustomerCount = hotCustomerCount;
	}

	public void setColdCustomerCount(int coldCustomerCount)
	{
		this.coldCustomerCount = coldCustomerCount;
	}

	public void setMediumCustomerCount(int mediumCustomerCount)
	{
		this.mediumCustomerCount = mediumCustomerCount;
	}

	public void setPendingProspectCount(int pendingProspectCount)
	{
		this.pendingProspectCount = pendingProspectCount;
	}

	public void setHotCustomerTicketIds(List<Long> hotCustomerTicketIds)
	{
		this.hotCustomerTicketIds = hotCustomerTicketIds;
	}

	public void setColdCustomerTicketIds(List<Long> coldCustomerTicketIds)
	{
		this.coldCustomerTicketIds = coldCustomerTicketIds;
	}

	public void setMediumCustomerTicketIds(List<Long> mediumCustomerTicketIds)
	{
		this.mediumCustomerTicketIds = mediumCustomerTicketIds;
	}

	public void setPendingProspectTicketIds(List<Long> pendingProspectTicketIds)
	{
		this.pendingProspectTicketIds = pendingProspectTicketIds;
	}

	public void setSeStatus(int seStatus)
	{
		this.seStatus = seStatus;
	}

}
