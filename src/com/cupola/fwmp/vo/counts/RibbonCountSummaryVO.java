package com.cupola.fwmp.vo.counts;

public class RibbonCountSummaryVO
{
	private String userName;
	private long userId;

	private int workInProgress;
	private int salesIssues;
	private int reachReverted;
	private int lcoIssues;
	private int customerEndIssues;
	private int customerAppoinmentIssues;
	private int others;

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public int getWorkInProgress()
	{
		return workInProgress;
	}

	public void setWorkInProgress(int workInProgress)
	{
		this.workInProgress = workInProgress;
	}

	public int getSalesIssues()
	{
		return salesIssues;
	}

	public void setSalesIssues(int salesIssues)
	{
		this.salesIssues = salesIssues;
	}

	public int getReachReverted()
	{
		return reachReverted;
	}

	public void setReachReverted(int reachReverted)
	{
		this.reachReverted = reachReverted;
	}

	public int getLcoIssues()
	{
		return lcoIssues;
	}

	public void setLcoIssues(int lcoIssues)
	{
		this.lcoIssues = lcoIssues;
	}

	public int getCustomerEndIssues()
	{
		return customerEndIssues;
	}

	public void setCustomerEndIssues(int customerEndIssues)
	{
		this.customerEndIssues = customerEndIssues;
	}

	public int getCustomerAppoinmentIssues()
	{
		return customerAppoinmentIssues;
	}

	public void setCustomerAppoinmentIssues(int customerAppoinmentIssues)
	{
		this.customerAppoinmentIssues = customerAppoinmentIssues;
	}

	public int getOthers()
	{
		return others;
	}

	public void setOthers(int others)
	{
		this.others = others;
	}

	public RibbonCountSummaryVO(String userName, long userId,
			int workInProgress, int salesIssues, int reachReverted,
			int lcoIssues, int customerEndIssues, int customerAppoinmentIssues,
			int others)
	{
		super();
		this.userName = userName;
		this.userId = userId;
		this.workInProgress = workInProgress;
		this.salesIssues = salesIssues;
		this.reachReverted = reachReverted;
		this.lcoIssues = lcoIssues;
		this.customerEndIssues = customerEndIssues;
		this.customerAppoinmentIssues = customerAppoinmentIssues;
		this.others = others;
	}
	
	public RibbonCountSummaryVO(){}

	@Override
	public String toString()
	{
		return "RibbonCountSummaryVO [userName=" + userName + ", userId="
				+ userId + ", workInProgress=" + workInProgress
				+ ", salesIssues=" + salesIssues + ", reachReverted="
				+ reachReverted + ", lcoIssues=" + lcoIssues
				+ ", customerEndIssues=" + customerEndIssues
				+ ", customerAppoinmentIssues=" + customerAppoinmentIssues
				+ ", others=" + others + "]";
	}

}
