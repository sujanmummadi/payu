package com.cupola.fwmp.vo;

import java.util.List;

import com.cupola.fwmp.vo.sales.RouterVo;
import com.cupola.fwmp.vo.sales.TariffPlanVo;

public class SalesDropdownVo
{
	private List<TypeAheadVo> titles;
	private List<TypeAheadVo> inquiry;
	private List<TypeAheadVo> customerType;
	private List<TypeAheadVo> reassignmentStatus;
	private List<TypeAheadVo> paymentStatus;
	private List<TypeAheadVo> documentStatus;
	private List<TypeAheadVo> hotProspectReason;
	private List<TypeAheadVo> coldProspectReason;
	private List<TypeAheadVo> mediumProspectReason;
	private List<TypeAheadVo> salesCustomerDeniedPermissionReason;
	private List<TypeAheadVo> profession;
	private List<TypeAheadVo> prospectTypes;
	private List<TypeAheadVo> source;
	private List<TypeAheadVo> paymentMode;
	private List<TariffPlanVo> tariffList;
	private List<RouterVo> routerVoList;
	private List<TypeAheadVo> branchList;
	private List<TypeAheadVo> nationalityList;
	private List<TypeAheadVo> ispList;
	private List<TypeAheadVo> uinTypesList;

	public List<TypeAheadVo> getTitles()
	{
		return titles;
	}

	public void setTitles(List<TypeAheadVo> titles)
	{
		this.titles = titles;
	}

	public List<TypeAheadVo> getInquiry()
	{
		return inquiry;
	}

	public void setInquiry(List<TypeAheadVo> inquiry)
	{
		this.inquiry = inquiry;
	}

	public List<TypeAheadVo> getCustomerType()
	{
		return customerType;
	}

	public void setCustomerType(List<TypeAheadVo> customerType)
	{
		this.customerType = customerType;
	}

	public List<TypeAheadVo> getReassignmentStatus()
	{
		return reassignmentStatus;
	}

	public void setReassignmentStatus(List<TypeAheadVo> reassignmentStatus)
	{
		this.reassignmentStatus = reassignmentStatus;
	}

	public List<TypeAheadVo> getPaymentStatus()
	{
		return paymentStatus;
	}

	public void setPaymentStatus(List<TypeAheadVo> paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	public List<TypeAheadVo> getDocumentStatus()
	{
		return documentStatus;
	}

	public void setDocumentStatus(List<TypeAheadVo> documentStatus)
	{
		this.documentStatus = documentStatus;
	}

	public List<TypeAheadVo> getHotProspectReason()
	{
		return hotProspectReason;
	}

	public void setHotProspectReason(List<TypeAheadVo> hotProspectReason)
	{
		this.hotProspectReason = hotProspectReason;
	}

	public List<TypeAheadVo> getColdProspectReason()
	{
		return coldProspectReason;
	}

	public void setColdProspectReason(List<TypeAheadVo> coldProspectReason)
	{
		this.coldProspectReason = coldProspectReason;
	}

	public List<TypeAheadVo> getMediumProspectReason()
	{
		return mediumProspectReason;
	}

	public void setMediumProspectReason(List<TypeAheadVo> mediumProspectReason)
	{
		this.mediumProspectReason = mediumProspectReason;
	}

	public List<TypeAheadVo> getSalesCustomerDeniedPermissionReason()
	{
		return salesCustomerDeniedPermissionReason;
	}

	public void setSalesCustomerDeniedPermissionReason(
			List<TypeAheadVo> salesCustomerDeniedPermissionReason)
	{
		this.salesCustomerDeniedPermissionReason = salesCustomerDeniedPermissionReason;
	}

	public List<TypeAheadVo> getProfession()
	{
		return profession;
	}

	public void setProfession(List<TypeAheadVo> profession)
	{
		this.profession = profession;
	}

	public List<TypeAheadVo> getProspectTypes()
	{
		return prospectTypes;
	}

	public void setProspectTypes(List<TypeAheadVo> prospectTypes)
	{
		this.prospectTypes = prospectTypes;
	}

	public List<TypeAheadVo> getSource()
	{
		return source;
	}

	public void setSource(List<TypeAheadVo> source)
	{
		this.source = source;
	}

	public List<TypeAheadVo> getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(List<TypeAheadVo> paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	public List<TariffPlanVo> getTariffList()
	{
		return tariffList;
	}

	public void setTariffList(List<TariffPlanVo> tariffList)
	{
		this.tariffList = tariffList;
	}

	public List<RouterVo> getRouterVoList()
	{
		return routerVoList;
	}

	public void setRouterVoList(List<RouterVo> routerVoList)
	{
		this.routerVoList = routerVoList;
	}

	public List<TypeAheadVo> getBranchList()
	{
		return branchList;
	}

	public void setBranchList(List<TypeAheadVo> branchList)
	{
		this.branchList = branchList;
	}

	
	
	
	
	
	

	/**
	 * @return the nationalityList
	 */
	public List<TypeAheadVo> getNationalityList() {
		return nationalityList;
	}

	/**
	 * @param nationalityList the nationalityList to set
	 */
	public void setNationalityList(List<TypeAheadVo> nationalityList) {
		this.nationalityList = nationalityList;
	}

	public List<TypeAheadVo> getIspList() {
		return ispList;
	}

	public void setIspList(List<TypeAheadVo> ispList) {
		this.ispList = ispList;
	}

	public List<TypeAheadVo> getUinTypesList() {
		return uinTypesList;
	}

	public void setUinTypesList(List<TypeAheadVo> uinTypesList) {
		this.uinTypesList = uinTypesList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SalesDropdownVo [titles=" + titles + ", inquiry=" + inquiry + ", customerType=" + customerType
				+ ", reassignmentStatus=" + reassignmentStatus + ", paymentStatus=" + paymentStatus
				+ ", documentStatus=" + documentStatus + ", hotProspectReason=" + hotProspectReason
				+ ", coldProspectReason=" + coldProspectReason + ", mediumProspectReason=" + mediumProspectReason
				+ ", salesCustomerDeniedPermissionReason=" + salesCustomerDeniedPermissionReason + ", profession="
				+ profession + ", prospectTypes=" + prospectTypes + ", source=" + source + ", paymentMode="
				+ paymentMode + ", tariffList=" + tariffList + ", routerVoList=" + routerVoList + ", branchList="
				+ branchList + ", nationalityList=" + nationalityList + ", ispList=" + ispList + ", uinTypesList="
				+ uinTypesList + "]";
	}


	
	

	
}