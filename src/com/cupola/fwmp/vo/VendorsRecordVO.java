package com.cupola.fwmp.vo;

import java.io.Serializable;

public class VendorsRecordVO implements Serializable{

	private Long id;
	private String name;
	private long branchId;
	private long cityId;
	private long areaId;
	private int skillId;
	private int count;
	private String branchName;
	private String cityName;
	private String areaName;
	private int status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getBranchId() {
		return branchId;
	}
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public long getAreaId() {
		return areaId;
	}
	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	
	public int getSkillId() {
		return skillId;
	}
	public void setSkillId(int skillId) {
		this.skillId = skillId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "VendorsRecordVO [id=" + id + ", name=" + name + ", branchId="
				+ branchId + ", cityId=" + cityId + ", areaId=" + areaId
				+ ", skillId=" + skillId + ", count=" + count + ", branchName="
				+ branchName + ", cityName=" + cityName + ", areaName="
				+ areaName + ", status=" + status + "]";
	}
	
	
}
