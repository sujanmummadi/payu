package com.cupola.fwmp.vo;

public class TabletFilterVO
{
	private String currentUserName;
	private String assignedTo;
	private String macAddress;
	private String brandName;
	private String status;
	private String tabName;
	private String currentUserId;
	private String assignedToId;

	public String getCurrentUserName()
	{
		return currentUserName;
	}

	public void setCurrentUserName(String currentUserName)
	{
		this.currentUserName = currentUserName;
	}

	public String getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo)
	{
		this.assignedTo = assignedTo;
	}

	public String getMacAddress()
	{
		return macAddress;
	}

	public void setMacAddress(String macAddress)
	{
		this.macAddress = macAddress;
	}

	public String getBrandName()
	{
		return brandName;
	}

	public void setBrandName(String brandName)
	{
		this.brandName = brandName;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getTabName()
	{
		return tabName;
	}

	public void setTabName(String tabName)
	{
		this.tabName = tabName;
	}

	public String getCurrentUserId()
	{
		return currentUserId;
	}

	public void setCurrentUserId(String currentUserId)
	{
		this.currentUserId = currentUserId;
	}

	public String getAssignedToId()
	{
		return assignedToId;
	}

	public void setAssignedToId(String assignedToId)
	{
		this.assignedToId = assignedToId;
	}

	@Override
	public String toString()
	{
		return "TabletFilterVO [currentUserName=" + currentUserName
				+ ", assignedTo=" + assignedTo + ", macAddress=" + macAddress
				+ ", brandName=" + brandName + ", status=" + status
				+ ", tabName=" + tabName + ", currentUserId=" + currentUserId
				+ ", assignedToId=" + assignedToId + "]";
	}
}
