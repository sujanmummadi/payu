package com.cupola.fwmp.vo;

public class TabDetailsVo {

	private String deviceId;
	private String registrationId;
	
	public TabDetailsVo() {
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getRegistrationId() {
		return registrationId;
	}
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}
	public TabDetailsVo(String deviceId, String registrationId) {
		super();
		this.deviceId = deviceId;
		this.registrationId = registrationId;
	}
	@Override
	public String toString() {
		return "TabDetailsVo [deviceId=" + deviceId + ", registrationId="
				+ registrationId + "]";
	}
	
	
}
