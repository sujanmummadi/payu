package com.cupola.fwmp.vo;

public class UserAvailabilityStatusVo {
	
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "UserAvailabilityStatusVo [status=" + status + "]";
	}
	
	

}
