package com.cupola.fwmp.vo;

public class TabletInfoVo {

	private Long ticketId;
	private String mqId;
	private String registrationId;
	private Long userId;

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getMqId() {
		return mqId;
	}

	public void setMqId(String mqId) {
		this.mqId = mqId;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "GcmFilter [ticketId=" + ticketId + ", mqId=" + mqId
				+ ", registrationId=" + registrationId + ", userId=" + userId
				+ "]";
	}

}
