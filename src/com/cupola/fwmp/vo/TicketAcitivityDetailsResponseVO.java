package com.cupola.fwmp.vo;

public class TicketAcitivityDetailsResponseVO {

	private String ticketId;
	private Long progressPercentage;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public Long getProgressPercentage() {
		return progressPercentage;
	}

	public void setProgressPercentage(Long progressPercentage) {
		this.progressPercentage = progressPercentage;
	}

	@Override
	public String toString() {
		return "TicketAcitivityDetailsResponseVO [ticketId=" + ticketId
				+ ", progressPercentage=" + progressPercentage + "]";
	}

}
