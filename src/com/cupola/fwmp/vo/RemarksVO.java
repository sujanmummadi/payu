package com.cupola.fwmp.vo;

public class RemarksVO
{
	private Long userId;
	private Long ticketId;
	private String remark;
	private String userName;
	private Integer remarkType;
	
	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public Integer getRemarkType() {
		return remarkType;
	}

	public void setRemarkType(Integer remarkType) {
		this.remarkType = remarkType;
	}
	
	
}
