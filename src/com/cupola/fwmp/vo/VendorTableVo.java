package com.cupola.fwmp.vo;

public class VendorTableVo {

	private long vendorId;
	private String vendorName;
	private int cxRackFixerCount;
	private int fLCount;
	private int cLCount;
	private int splicerCount;
	
	public long getVendorId() {
		return vendorId;
	}
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public int getCxRackFixerCount() {
		return cxRackFixerCount;
	}
	public void setCxRackFixerCount(int cxRackFixerCount) {
		this.cxRackFixerCount = cxRackFixerCount;
	}
	public int getfLCount() {
		return fLCount;
	}
	public void setfLCount(int fLCount) {
		this.fLCount = fLCount;
	}
	public int getcLCount() {
		return cLCount;
	}
	public void setcLCount(int cLCount) {
		this.cLCount = cLCount;
	}
	public int getSplicerCount() {
		return splicerCount;
	}
	public void setSplicerCount(int splicerCount) {
		this.splicerCount = splicerCount;
	}
	@Override
	public String toString() {
		return "VendorTableVo [vendorId=" + vendorId + ", vendorName="
				+ vendorName + ", cxRackFixerCount=" + cxRackFixerCount
				+ ", fLCount=" + fLCount + ", cLCount=" + cLCount
				+ ", splicerCount=" + splicerCount + "]";
	}
	
	
	
}
