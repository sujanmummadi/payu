package com.cupola.fwmp.vo;

public class CafDetailsVo
{
	private String cafNumber;
	private Integer status;
	private Long userId;

	public CafDetailsVo()
	{

	}
	

	public CafDetailsVo(String cafNumber, Integer status, Long userId)
	{
		this.cafNumber = cafNumber;
		this.status = status;
		this.userId = userId;
	}



	public String getCafNumber()
	{
		return cafNumber;
	}

	public void setCafNumber(String cafNumber)
	{
		this.cafNumber = cafNumber;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	@Override
	public String toString()
	{
		return "CafDetailsVo [cafNumber=" + cafNumber + ", status=" + status
				+ ", userId=" + userId + "]";
	}

	

}
