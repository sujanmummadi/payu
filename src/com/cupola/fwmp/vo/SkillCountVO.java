/**
 * 
 */
package com.cupola.fwmp.vo;

import java.io.Serializable;

/**
 * @author pawan
 * 
 */
public class SkillCountVO implements Serializable
{

	private long skillId;
	private String skillName;
	private int count;
	public long getSkillId() {
		return skillId;
	}
	public void setSkillId(long skillId) {
		this.skillId = skillId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "SkillCountVO [skillId=" + skillId + ", skillName=" + skillName
				+ ", count=" + count + "]";
	}
	

	
	
	
	
	
	
}
