package com.cupola.fwmp.vo;

public class ReassignCAF
{
	private Long sourceUserId;
	private Long destinationUserId;
	private String cafNumber;
	
	public Long getSourceUserId()
	{
		return sourceUserId;
	}
	public void setSourceUserId(Long sourceUserId)
	{
		this.sourceUserId = sourceUserId;
	}
	public Long getDestinationUserId()
	{
		return destinationUserId;
	}
	public void setDestinationUserId(Long destinationUserId)
	{
		this.destinationUserId = destinationUserId;
	}
	public String getCafNumber()
	{
		return cafNumber;
	}
	public void setCafNumber(String cafNumber)
	{
		this.cafNumber = cafNumber;
	}
	
	@Override
	public String toString()
	{
		return "ReassignCAF [sourceUserId=" + sourceUserId
				+ ", destinationUserId=" + destinationUserId + ", cafNumber="
				+ cafNumber + "]";
	}
	
}
