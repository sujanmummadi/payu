/**
 * 
 */
package com.cupola.fwmp.vo;

/**
 * @author aditya
 * 
 */
public class TicketsForGC
{
	private Long ticketId;
	private int ticketStatus;
	private Long currentAssignedTo;
	private int paymentStatus;
	private int documentStatus;
	private int joiningTableStatus;

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public int getTicketStatus()
	{
		return ticketStatus;
	}

	public void setTicketStatus(int ticketStatus)
	{
		this.ticketStatus = ticketStatus;
	}

	public Long getCurrentAssignedTo()
	{
		return currentAssignedTo;
	}

	public void setCurrentAssignedTo(Long currentAssignedTo)
	{
		this.currentAssignedTo = currentAssignedTo;
	}

	public int getPaymentStatus()
	{
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	public int getDocumentStatus()
	{
		return documentStatus;
	}

	public void setDocumentStatus(int documentStatus)
	{
		this.documentStatus = documentStatus;
	}

	public int getJoiningTableStatus()
	{
		return joiningTableStatus;
	}

	public void setJoiningTableStatus(int joiningTableStatus)
	{
		this.joiningTableStatus = joiningTableStatus;
	}

	@Override
	public String toString()
	{
		return "TicketsForGC [ticketId=" + ticketId + "]";
	}

}
