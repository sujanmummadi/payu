package com.cupola.fwmp.vo;

import java.util.Map;

public class WorkStageTypeCountVo {

	private String workStageName;
	private Integer count;

	public String getWorkStageName() {
		return workStageName;
	}

	public void setWorkStageName(String workStageName) {
		this.workStageName = workStageName;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "WorkStageTypeCountVo [workStageName=" + workStageName
				+ ", count=" + count + "]";
	}

}
