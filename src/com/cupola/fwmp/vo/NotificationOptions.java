package com.cupola.fwmp.vo;

import java.util.ArrayList;
import java.util.List;

public class NotificationOptions
{
	List<TypeAheadVo> activities = new ArrayList<TypeAheadVo>();
	List<TypeAheadVo> stages = new ArrayList<TypeAheadVo>();;
	List<TypeAheadVo> types = new ArrayList<TypeAheadVo>();;
	List<TypeAheadVo> status = new ArrayList<TypeAheadVo>();

	public List<TypeAheadVo> getActivities()
	{
		return activities;
	}

	public void setActivities(List<TypeAheadVo> activities)
	{
		this.activities = activities;
	}

	public List<TypeAheadVo> getStages()
	{
		return stages;
	}

	public void setStages(List<TypeAheadVo> stages)
	{
		this.stages = stages;
	}

	public List<TypeAheadVo> getTypes()
	{
		return types;
	}

	public void setTypes(List<TypeAheadVo> types)
	{
		this.types = types;
	}

	public List<TypeAheadVo> getStatus()
	{
		return status;
	}

	public void setStatus(List<TypeAheadVo> status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "NotificationOptions [activities=" + activities + ", stages="
				+ stages + ", types=" + types + ", status=" + status + "]";
	};

}