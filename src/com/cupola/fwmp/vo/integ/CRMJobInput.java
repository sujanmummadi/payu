/**
* @Author aditya  30-Dec-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.integ;

/**
 * @Author aditya 30-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CRMJobInput {

	private String city;
	private String branch;
	private String area;
	private String subArea;

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch
	 *            the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the subArea
	 */
	public String getSubArea() {
		return subArea;
	}

	/**
	 * @param subArea
	 *            the subArea to set
	 */
	public void setSubArea(String subArea) {
		this.subArea = subArea;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CRMJobInput [city=" + city + ", branch=" + branch + ", area=" + area + ", subArea=" + subArea + "]";
	}
}
