package com.cupola.fwmp.vo.integ.app;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ProspectByApp")
public class ProspectVO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String action;// CREATE,UPDATE. required in all the cases

	private Long id; // for internal Use only

	private String title;// mandatory if Action = CREATE
	private String firstName;// mandatory if Action = CREATE
	private String lastName;// mandatory if Action = CREATE
	private String middleName;// mandatory if Action = CREATE
	private Date prefferedCallDate;// mandatory if Action = CREATE

	private String prospectNumber;// mandatory if Action = CREATE

	private String customerType;

	private String mobileNumber;// mandatory if Action = CREATE
	private String alternativeMobileNo;
	private String officeNumber;
	private String emailId;// mandatory if Action = CREATE
	private String customerProfession;// mandatory if Action = CREATE
	private String alternativeEmailId;

	private String currentAddress;// mandatory if Action = CREATE
	private String communicationAdderss;// mandatory if Action = CREATE
	private String permanentAddress;// mandatory if Action = CREATE
	private String pincode;// mandatory if Action = CREATE
	
	private String connectionType;// mandatory if Action = CREATE "Domestic"

	private String fxName;// mandatory if Action = CREATE
	private String fxIpAddress;// mandatory if Action = CREATE
	private String fxMacAddress;// mandatory if Action = CREATE
	private String fxPorts;// mandatory if Action = CREATE

	private String cxName;// mandatory if Action = CREATE
	private String cxIpAddress;// mandatory if Action = CREATE
	private String cxMacAddress;// mandatory if Action = CREATE
	private String cxPorts;// mandatory if Action = CREATE

	private String cityCode;// mandatory if Action = CREATE
	private String branchCode;// mandatory if Action = CREATE
	private String areaCode;// mandatory if Action = CREATE
	private String clusterName;

	private String enquiryType; // domestic, lease line etc // mandatory if
								// Action = CREATE
	private String adSubCategories;// how do u come to know about act
									// //mandatory if Action = CREATE
	private String prospectColdReason;
	private String prospectType; // hot, cold, medium // mandatory if Action =
									// CREATE

	private String longitude;// mandatory if Action = CREATE
	private String lattitude;// mandatory if Action = CREATE

	private String remarks;// mandatory if Action = CREATE
	private String source;// Source = App mandatory if Action = CREATE

	private String updateType;// mandatory if Action = UPDATE

	private String planCode;// mandatory if Action = UPDATE Tariff
							// planCode eg AMX1050_12
	private String tariffPlanType; // mandatory if Action = UPDATE Tariff
	// Like soho home etc
	private String routerFreeWithPlanModel;// mandatory if Action = UPDATE
											// Tariff Plan put Y for Yes N for
											// No
	private boolean cxPermission;// mandatory if Action = UPDATE
	private boolean gxPermission;// mandatory if Action = UPDATE

	private List<String> docPath;// mandatory if Action = UPDATE

	private String paymentTransactionNo;// mandatory if Action = UPDATE
	private String amountPaid;// mandatory if Action = UPDATE
	private String installationCharge;// mandatory if Action = UPDATE

	private String paymentMode;// mandatory if Action = Payment UPDATE
	private String cafNo;// mandatory if Action = Payment UPDATE
	private String referenceNo;// mandatory if Action = PaymentUPDATE
	private String paidAmount;// mandatory if Action = PaymentUPDATE
	private String installationCharges;// mandatory if Action =Payment UPDATE
	private String paidConnection;// mandatory if Action = Payment UPDATE yes
									// for paid comnnection

	public String getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	public String getCafNo()
	{
		return cafNo;
	}

	public void setCafNo(String cafNo)
	{
		this.cafNo = cafNo;
	}

	public String getReferenceNo()
	{
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo)
	{
		this.referenceNo = referenceNo;
	}

	public String getPaidAmount()
	{
		return paidAmount;
	}

	public void setPaidAmount(String paidAmount)
	{
		this.paidAmount = paidAmount;
	}

	public String getInstallationCharges()
	{
		return installationCharges;
	}

	public void setInstallationCharges(String installationCharges)
	{
		this.installationCharges = installationCharges;
	}

	public String getPaidConnection()
	{
		return paidConnection;
	}

	public void setPaidConnection(String paidConnection)
	{
		this.paidConnection = paidConnection;
	}

	private Date addedOn;
	private Date modifiedOn;

	private String externalRouterRequired;// mandatory if Action = UPDATE Tariff
											// Plan put Y for Yes N for No
	private String routerModel;// mandatory if Action = UPDATE Tariff
								// router model
	private String externalRouterAmount;// mandatory if Action = UPDATE Tariff
										// router price

	public String getAction()
	{
		return action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public Date getPrefferedCallDate()
	{
		return prefferedCallDate;
	}

	public void setPrefferedCallDate(Date prefferedCallDate)
	{
		this.prefferedCallDate = prefferedCallDate;
	}

	public String getProspectNumber()
	{
		return prospectNumber;
	}

	public void setProspectNumber(String prospectNumber)
	{
		this.prospectNumber = prospectNumber;
	}

	public String getCustomerType()
	{
		return customerType;
	}

	public void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String getAlternativeMobileNo()
	{
		return alternativeMobileNo;
	}

	public void setAlternativeMobileNo(String alternativeMobileNo)
	{
		this.alternativeMobileNo = alternativeMobileNo;
	}

	public String getOfficeNumber()
	{
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber)
	{
		this.officeNumber = officeNumber;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	public String getCustomerProfession()
	{
		return customerProfession;
	}

	public void setCustomerProfession(String customerProfession)
	{
		this.customerProfession = customerProfession;
	}

	public String getAlternativeEmailId()
	{
		return alternativeEmailId;
	}

	public void setAlternativeEmailId(String alternativeEmailId)
	{
		this.alternativeEmailId = alternativeEmailId;
	}

	public String getCurrentAddress()
	{
		return currentAddress;
	}

	public void setCurrentAddress(String currentAddress)
	{
		this.currentAddress = currentAddress;
	}

	public String getCommunicationAdderss()
	{
		return communicationAdderss;
	}

	public void setCommunicationAdderss(String communicationAdderss)
	{
		this.communicationAdderss = communicationAdderss;
	}

	public String getPermanentAddress()
	{
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress)
	{
		this.permanentAddress = permanentAddress;
	}

	public String getConnectionType()
	{
		return connectionType;
	}

	public void setConnectionType(String connectionType)
	{
		this.connectionType = connectionType;
	}

	public String getFxName()
	{
		return fxName;
	}

	public void setFxName(String fxName)
	{
		this.fxName = fxName;
	}

	public String getFxIpAddress()
	{
		return fxIpAddress;
	}

	public void setFxIpAddress(String fxIpAddress)
	{
		this.fxIpAddress = fxIpAddress;
	}

	public String getFxMacAddress()
	{
		return fxMacAddress;
	}

	public void setFxMacAddress(String fxMacAddress)
	{
		this.fxMacAddress = fxMacAddress;
	}

	public String getFxPorts()
	{
		return fxPorts;
	}

	public void setFxPorts(String fxPorts)
	{
		this.fxPorts = fxPorts;
	}

	public String getCxName()
	{
		return cxName;
	}

	public void setCxName(String cxName)
	{
		this.cxName = cxName;
	}

	public String getCxIpAddress()
	{
		return cxIpAddress;
	}

	public void setCxIpAddress(String cxIpAddress)
	{
		this.cxIpAddress = cxIpAddress;
	}

	public String getCxMacAddress()
	{
		return cxMacAddress;
	}

	public void setCxMacAddress(String cxMacAddress)
	{
		this.cxMacAddress = cxMacAddress;
	}

	public String getCxPorts()
	{
		return cxPorts;
	}

	public void setCxPorts(String cxPorts)
	{
		this.cxPorts = cxPorts;
	}

	public String getCityCode()
	{
		return cityCode;
	}

	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}

	public String getBranchCode()
	{
		return branchCode;
	}

	public void setBranchCode(String branchCode)
	{
		this.branchCode = branchCode;
	}

	public String getAreaCode()
	{
		return areaCode;
	}

	public void setAreaCode(String areaCode)
	{
		this.areaCode = areaCode;
	}

	public String getClusterName()
	{
		return clusterName;
	}

	public void setClusterName(String clusterName)
	{
		this.clusterName = clusterName;
	}

	public String getEnquiryType()
	{
		return enquiryType;
	}

	public void setEnquiryType(String enquiryType)
	{
		this.enquiryType = enquiryType;
	}

	public String getAdSubCategories()
	{
		return adSubCategories;
	}

	public void setAdSubCategories(String adSubCategories)
	{
		this.adSubCategories = adSubCategories;
	}

	public String getProspectColdReason()
	{
		return prospectColdReason;
	}

	public void setProspectColdReason(String prospectColdReason)
	{
		this.prospectColdReason = prospectColdReason;
	}

	public String getProspectType()
	{
		return prospectType;
	}

	public void setProspectType(String prospectType)
	{
		this.prospectType = prospectType;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getLattitude()
	{
		return lattitude;
	}

	public void setLattitude(String lattitude)
	{
		this.lattitude = lattitude;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getUpdateType()
	{
		return updateType;
	}

	public void setUpdateType(String updateType)
	{
		this.updateType = updateType;
	}

	public String getRouterFreeWithPlanModel()
	{
		return routerFreeWithPlanModel;
	}

	public void setRouterFreeWithPlanModel(String routerFreeWithPlanModel)
	{
		this.routerFreeWithPlanModel = routerFreeWithPlanModel;
	}

	public boolean isCxPermission()
	{
		return cxPermission;
	}

	public void setCxPermission(boolean cxPermission)
	{
		this.cxPermission = cxPermission;
	}

	public boolean isGxPermission()
	{
		return gxPermission;
	}

	public void setGxPermission(boolean gxPermission)
	{
		this.gxPermission = gxPermission;
	}

	public List<String> getDocPath()
	{
		return docPath;
	}

	public void setDocPath(List<String> docPath)
	{
		this.docPath = docPath;
	}

	public String getPaymentTransactionNo()
	{
		return paymentTransactionNo;
	}

	public void setPaymentTransactionNo(String paymentTransactionNo)
	{
		this.paymentTransactionNo = paymentTransactionNo;
	}

	public String getAmountPaid()
	{
		return amountPaid;
	}

	public void setAmountPaid(String amountPaid)
	{
		this.amountPaid = amountPaid;
	}

	public String getInstallationCharge()
	{
		return installationCharge;
	}

	public void setInstallationCharge(String installationCharge)
	{
		this.installationCharge = installationCharge;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public String getExternalRouterRequired()
	{
		return externalRouterRequired;
	}

	public void setExternalRouterRequired(String externalRouterRequired)
	{
		this.externalRouterRequired = externalRouterRequired;
	}

	public String getRouterModel()
	{
		return routerModel;
	}

	public void setRouterModel(String externalRouterModel)
	{
		this.routerModel = externalRouterModel;
	}

	public String getExternalRouterAmount()
	{
		return externalRouterAmount;
	}

	public void setExternalRouterAmount(String externalRouterAmount)
	{
		this.externalRouterAmount = externalRouterAmount;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	@Override
	public String toString()
	{
		return "ProspectVO [action=" + action + ", id=" + id + ", title="
				+ title + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", middleName=" + middleName + ", prefferedCallDate="
				+ prefferedCallDate + ", prospectNumber=" + prospectNumber
				+ ", customerType=" + customerType + ", mobileNumber="
				+ mobileNumber + ", alternativeMobileNo=" + alternativeMobileNo
				+ ", officeNumber=" + officeNumber + ", emailId=" + emailId
				+ ", customerProfession=" + customerProfession
				+ ", alternativeEmailId=" + alternativeEmailId
				+ ", currentAddress=" + currentAddress
				+ ", communicationAdderss=" + communicationAdderss
				+ ", permanentAddress=" + permanentAddress + ", connectionType="
				+ connectionType + ", fxName=" + fxName + ", fxIpAddress="
				+ fxIpAddress + ", fxMacAddress=" + fxMacAddress + ", fxPorts="
				+ fxPorts + ", cxName=" + cxName + ", cxIpAddress="
				+ cxIpAddress + ", cxMacAddress=" + cxMacAddress + ", cxPorts="
				+ cxPorts + ", cityCode=" + cityCode + ", branchCode="
				+ branchCode + ", areaCode=" + areaCode + ", clusterName="
				+ clusterName + ", enquiryType=" + enquiryType
				+ ", adSubCategories=" + adSubCategories
				+ ", prospectColdReason=" + prospectColdReason
				+ ", prospectType=" + prospectType + ", longitude=" + longitude
				+ ", lattitude=" + lattitude + ", remarks=" + remarks
				+ ", source=" + source + ", updateType=" + updateType
				+ ", planCode=" + planCode + ", tariffPlanType="
				+ tariffPlanType + ", routerFreeWithPlanModel="
				+ routerFreeWithPlanModel + ", cxPermission=" + cxPermission
				+ ", gxPermission=" + gxPermission + ", docPath=" + docPath
				+ ", paymentTransactionNo=" + paymentTransactionNo
				+ ", amountPaid=" + amountPaid + ", installationCharge="
				+ installationCharge + ", paymentMode=" + paymentMode
				+ ", cafNo=" + cafNo + ", referenceNo=" + referenceNo
				+ ", paidAmount=" + paidAmount + ", installationCharges="
				+ installationCharges + ", paidConnection=" + paidConnection
				+ ", addedOn=" + addedOn + ", modifiedOn=" + modifiedOn
				+ ", externalRouterRequired=" + externalRouterRequired
				+ ", routerModel=" + routerModel + ", externalRouterAmount="
				+ externalRouterAmount + "]";
	}

	public String getPlanCode()
	{
		return planCode;
	}

	public void setPlanCode(String planCode)
	{
		this.planCode = planCode;
	}

	public String getTariffPlanType()
	{
		return tariffPlanType;
	}

	public void setTariffPlanType(String tariffPlanType)
	{
		this.tariffPlanType = tariffPlanType;
	}

	public String getPincode()
	{
		return pincode;
	}

	public void setPincode(String pincode)
	{
		this.pincode = pincode;
	}
}
