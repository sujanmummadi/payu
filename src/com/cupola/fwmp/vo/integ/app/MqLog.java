/**
 * 
 */
package com.cupola.fwmp.vo.integ.app;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectResponse;
import com.cupola.fwmp.vo.ProspectCoreVO;

/**
 * @author aditya
 * 
 */
@Document(collection = "MqLog")
public class MqLog
{
	@Id
	private Long mqLogId;
	private Long addedBy;

	private String mqUrl;
	private String requestTime;
	private String requestString;

	private String responseTime;
	private String responseString;

	private MqProspectResponse mqProspectResponse;
	private ProspectCoreVO prospect;
	private String prospectNo;
	
	private String mqErrorCode;
	private String mqErrorMessage;

	public Long getMqLogId()
	{
		return mqLogId;
	}

	public void setMqLogId(Long mqLogId)
	{
		this.mqLogId = mqLogId;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public String getMqUrl()
	{
		return mqUrl;
	}

	public void setMqUrl(String mqUrl)
	{
		this.mqUrl = mqUrl;
	}

	public String getRequestTime()
	{
		return requestTime;
	}

	public void setRequestTime(String requestTime)
	{
		this.requestTime = requestTime;
	}

	public String getRequestString()
	{
		return requestString;
	}

	public void setRequestString(String requestString)
	{
		this.requestString = requestString;
	}

	public String getResponseTime()
	{
		return responseTime;
	}

	public void setResponseTime(String responseTime)
	{
		this.responseTime = responseTime;
	}

	public String getResponseString()
	{
		return responseString;
	}

	public void setResponseString(String responseString)
	{
		this.responseString = responseString;
	}

	public MqProspectResponse getMqProspectResponse()
	{
		return mqProspectResponse;
	}

	public void setMqProspectResponse(MqProspectResponse mqProspectResponse)
	{
		this.mqProspectResponse = mqProspectResponse;
	}

	public ProspectCoreVO getProspect()
	{
		return prospect;
	}

	public void setProspect(ProspectCoreVO prospect)
	{
		this.prospect = prospect;
	}

	public String getProspectNo()
	{
		return prospectNo;
	}

	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}

	public String getMqErrorCode()
	{
		return mqErrorCode;
	}

	public void setMqErrorCode(String mqErrorCode)
	{
		this.mqErrorCode = mqErrorCode;
	}

	public String getMqErrorMessage()
	{
		return mqErrorMessage;
	}

	public void setMqErrorMessage(String mqErrorMessage)
	{
		this.mqErrorMessage = mqErrorMessage;
	}

	@Override
	public String toString()
	{
		return "MqLog [mqLogId=" + mqLogId + ", addedBy=" + addedBy + ", mqUrl="
				+ mqUrl + ", requestTime=" + requestTime + ", requestString="
				+ requestString + ", responseTime=" + responseTime
				+ ", responseString=" + responseString + ", mqProspectResponse="
				+ mqProspectResponse + ", prospect=" + prospect
				+ ", prospectNo=" + prospectNo + ", mqErrorCode=" + mqErrorCode
				+ ", mqErrorMessage=" + mqErrorMessage + "]";
	}

}
