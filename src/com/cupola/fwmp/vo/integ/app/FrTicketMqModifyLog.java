package com.cupola.fwmp.vo.integ.app;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "FrTicketMqModifyLog")
public class FrTicketMqModifyLog
{
	@Id
	private Long mqLogId;
	private Long addedBy;

	private String mqUrl;
	private String requestTime;
	private String requestString;

	private String responseTime;
	private String responseString;
	private String workOrderNumber;
	
	private String requestType;

	public Long getMqLogId()
	{
		return mqLogId;
	}

	public void setMqLogId(Long mqLogId)
	{
		this.mqLogId = mqLogId;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public String getMqUrl()
	{
		return mqUrl;
	}

	public void setMqUrl(String mqUrl)
	{
		this.mqUrl = mqUrl;
	}

	public String getRequestTime()
	{
		return requestTime;
	}

	public void setRequestTime(String requestTime)
	{
		this.requestTime = requestTime;
	}

	public String getRequestString()
	{
		return requestString;
	}

	public void setRequestString(String requestString)
	{
		this.requestString = requestString;
	}

	public String getResponseTime()
	{
		return responseTime;
	}

	public void setResponseTime(String responseTime)
	{
		this.responseTime = responseTime;
	}

	public String getResponseString()
	{
		return responseString;
	}

	public void setResponseString(String responseString)
	{
		this.responseString = responseString;
	}

	public String getWorkOrderNumber()
	{
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber)
	{
		this.workOrderNumber = workOrderNumber;
	}

	public String getRequestType()
	{
		return requestType;
	}

	public void setRequestType(String requestType)
	{
		this.requestType = requestType;
	}
}
