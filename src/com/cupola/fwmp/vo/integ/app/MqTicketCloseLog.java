/**
 * 
 */
package com.cupola.fwmp.vo.integ.app;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author aditya
 * 
 */
@Document(collection = "MqTicketCloseLog")
public class MqTicketCloseLog
{
	@Id
	private Long mqLogId;
	private Long addedBy;

	private String mqUrl;
	private String requestTime;
	private String requestString;

	private String responseTime;
	private String responseString;

	public Long getMqLogId()
	{
		return mqLogId;
	}

	public void setMqLogId(Long mqLogId)
	{
		this.mqLogId = mqLogId;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public String getMqUrl()
	{
		return mqUrl;
	}

	public void setMqUrl(String mqUrl)
	{
		this.mqUrl = mqUrl;
	}

	public String getRequestTime()
	{
		return requestTime;
	}

	public void setRequestTime(String requestTime)
	{
		this.requestTime = requestTime;
	}

	public String getRequestString()
	{
		return requestString;
	}

	public void setRequestString(String requestString)
	{
		this.requestString = requestString;
	}

	public String getResponseTime()
	{
		return responseTime;
	}

	public void setResponseTime(String responseTime)
	{
		this.responseTime = responseTime;
	}

	public String getResponseString()
	{
		return responseString;
	}

	public void setResponseString(String responseString)
	{
		this.responseString = responseString;
	}

	@Override
	public String toString()
	{
		return "MqLog [mqLogId=" + mqLogId + ", addedBy=" + addedBy + ", mqUrl="
				+ mqUrl + ", requestTime=" + requestTime + ", requestString="
				+ requestString + ", responseTime=" + responseTime
				+ ", responseString=" + responseString + "]";
	}

}
