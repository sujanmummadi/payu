/**
* @Author aditya  11-Jan-2018
* @Copyright (c) 2018 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.integ.app;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @Author aditya 11-Jan-2018
 * @Copyright (c) 2018 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "FinahubLog")
public class FinahubLog {

	@Id
	private Long finahubLogId;

	private String loginId;
	private String displayName;
	private String area;
	private String branch;
	private String city;
	private String activity;

	private String finahubPackage;
	private Date requestTime;
	private String requestString;
	private String ticketId;

	private Date responseTime;
	private String responseString;

	/**
	 * @return the finahubLogId
	 */
	public Long getFinahubLogId() {
		return finahubLogId;
	}

	/**
	 * @param finahubLogId
	 *            the finahubLogId to set
	 */
	public void setFinahubLogId(Long finahubLogId) {
		this.finahubLogId = finahubLogId;
	}

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId
	 *            the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName
	 *            the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch
	 *            the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the activity
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * @param activity
	 *            the activity to set
	 */
	public void setActivity(String activity) {
		this.activity = activity;
	}

	/**
	 * @return the finahubPackage
	 */
	public String getFinahubPackage() {
		return finahubPackage;
	}

	/**
	 * @param finahubPackage
	 *            the finahubPackage to set
	 */
	public void setFinahubPackage(String finahubPackage) {
		this.finahubPackage = finahubPackage;
	}

	/**
	 * @return the requestTime
	 */
	public Date getRequestTime() {
		return requestTime;
	}

	/**
	 * @param requestTime
	 *            the requestTime to set
	 */
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	/**
	 * @return the requestString
	 */
	public String getRequestString() {
		return requestString;
	}

	/**
	 * @param requestString
	 *            the requestString to set
	 */
	public void setRequestString(String requestString) {
		this.requestString = requestString;
	}

	/**
	 * @return the ticketId
	 */
	public String getTicketId() {
		return ticketId;
	}

	/**
	 * @param ticketId
	 *            the ticketId to set
	 */
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * @return the responseTime
	 */
	public Date getResponseTime() {
		return responseTime;
	}

	/**
	 * @param responseTime
	 *            the responseTime to set
	 */
	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}

	/**
	 * @return the responseString
	 */
	public String getResponseString() {
		return responseString;
	}

	/**
	 * @param responseString
	 *            the responseString to set
	 */
	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FinahubLog [finahubLogId=" + finahubLogId + ", loginId=" + loginId + ", displayName=" + displayName
				+ ", area=" + area + ", branch=" + branch + ", city=" + city + ", activity=" + activity
				+ ", finahubPackage=" + finahubPackage + ", requestTime=" + requestTime + ", requestString="
				+ requestString + ", ticketId=" + ticketId + ", responseTime=" + responseTime + ", responseString="
				+ responseString + "]";
	}
}
