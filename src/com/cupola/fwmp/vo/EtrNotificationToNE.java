package com.cupola.fwmp.vo;

import java.util.Date;

public class EtrNotificationToNE
{
	// Etr Request for NE
	private String ticketId;
	private String workOrderNumber;
	private Date newEtr;
	private int messageCode;
	private String message;

	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	public int getMessageCode()
	{
		return messageCode;
	}

	public void setMessageCode(int messageCode)
	{
		this.messageCode = messageCode;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getWorkOrderNumber()
	{
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber)
	{
		this.workOrderNumber = workOrderNumber;
	}

	public Date getNewEtr()
	{
		return newEtr;
	}

	public void setNewEtr(Date newEtr)
	{
		this.newEtr = newEtr;
	}

	@Override
	public String toString()
	{
		return "EtrNotificationToNE [ticketId=" + ticketId
				+ ", workOrderNumber=" + workOrderNumber + ", newEtr=" + newEtr
				+ ", messageCode=" + messageCode + ", message=" + message + "]";
	}

	// private message;

}
