/**
 * 
 */
package com.cupola.fwmp.vo.user;

/**
 * @author aditya
 *
 */
public class UserVoModified
{
	// select distinct u.id as userId,u.empId as empId, u.loginId as
	// loginId,u.password as password, u.firstName as firstName, u.lastName as
	// lastName,u.emailId as emailId, u.mobileNo as mobileNo, tab.id as
	// tabletId, tab.macAddress as tabMac, dev.id as devcieId, dev.deviceName as
	// deviceName, b.id as branchId, b.branchName as branchName, c.id as cityId,
	// c.cityName as cityName, a.id as areaId, a.areaName as areaName, u1.id as
	// reportToId, u1.loginId reportToLoginId, ug.id as userGroupId,
	// ug.userGroupName as userGroup, r.id as roleId, r.roleCode as roleCode

	private String userId;
	private String empId;
	private String loginId;
	private String password;
	private String firstName;
	private String lastName;
	private String emailId;
	private String mobileNo;
	private String enabled;

	private String tabletId;
	private String tabMac;

	private String deviceId;
	private String deviceName;

	private String branchId;
	private String branchName;
	private String branchCode;

	private String cityId;
	private String cityName;
	private String cityCode;

	private String areaId;
	private String areaName;
	private String areaCode;

	private String reportToId;
	private String reportToLoginId;

	private String userGroupId;
	private String userGroup;

	private String roleId;
	private String roleCode;
	
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public String getEmpId()
	{
		return empId;
	}
	public void setEmpId(String empId)
	{
		this.empId = empId;
	}
	public String getLoginId()
	{
		return loginId;
	}
	public void setLoginId(String loginId)
	{
		this.loginId = loginId;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	public String getEmailId()
	{
		return emailId;
	}
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}
	public String getMobileNo()
	{
		return mobileNo;
	}
	public void setMobileNo(String mobileNo)
	{
		this.mobileNo = mobileNo;
	}
	public String getTabletId()
	{
		return tabletId;
	}
	public void setTabletId(String tabletId)
	{
		this.tabletId = tabletId;
	}
	public String getTabMac()
	{
		return tabMac;
	}
	public void setTabMac(String tabMac)
	{
		this.tabMac = tabMac;
	}
	public String getDeviceId()
	{
		return deviceId;
	}
	public void setDeviceId(String deviceId)
	{
		this.deviceId = deviceId;
	}
	public String getDeviceName()
	{
		return deviceName;
	}
	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}
	public String getBranchId()
	{
		return branchId;
	}
	public void setBranchId(String branchId)
	{
		this.branchId = branchId;
	}
	public String getBranchName()
	{
		return branchName;
	}
	public void setBranchName(String branchName)
	{
		this.branchName = branchName;
	}
	public String getBranchCode()
	{
		return branchCode;
	}
	public void setBranchCode(String branchCode)
	{
		this.branchCode = branchCode;
	}
	public String getCityId()
	{
		return cityId;
	}
	public void setCityId(String cityId)
	{
		this.cityId = cityId;
	}
	public String getCityName()
	{
		return cityName;
	}
	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}
	public String getCityCode()
	{
		return cityCode;
	}
	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}
	public String getAreaId()
	{
		return areaId;
	}
	public void setAreaId(String areaId)
	{
		this.areaId = areaId;
	}
	public String getAreaName()
	{
		return areaName;
	}
	public void setAreaName(String areaName)
	{
		this.areaName = areaName;
	}
	public String getAreaCode()
	{
		return areaCode;
	}
	public void setAreaCode(String areaCode)
	{
		this.areaCode = areaCode;
	}
	public String getReportToId()
	{
		return reportToId;
	}
	public void setReportToId(String reportToId)
	{
		this.reportToId = reportToId;
	}
	public String getReportToLoginId()
	{
		return reportToLoginId;
	}
	public void setReportToLoginId(String reportToLoginId)
	{
		this.reportToLoginId = reportToLoginId;
	}
	public String getUserGroupId()
	{
		return userGroupId;
	}
	public void setUserGroupId(String userGroupId)
	{
		this.userGroupId = userGroupId;
	}
	public String getUserGroup()
	{
		return userGroup;
	}
	public void setUserGroup(String userGroup)
	{
		this.userGroup = userGroup;
	}
	public String getRoleId()
	{
		return roleId;
	}
	public void setRoleId(String roleId)
	{
		this.roleId = roleId;
	}
	public String getRoleCode()
	{
		return roleCode;
	}
	public void setRoleCode(String roleCode)
	{
		this.roleCode = roleCode;
	}
	
	public String getEnabled()
	{
		return enabled;
	}
	public void setEnabled(String enabled)
	{
		this.enabled = enabled;
	}
	
	@Override
	public String toString()
	{
		return "UserVoModified [userId=" + userId + ", empId=" + empId
				+ ", loginId=" + loginId + ", password=" + password
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", emailId=" + emailId + ", mobileNo=" + mobileNo
				+ ", tabletId=" + tabletId + ", tabMac=" + tabMac
				+ ", deviceId=" + deviceId + ", deviceName=" + deviceName
				+ ", branchId=" + branchId + ", branchName=" + branchName
				+ ", branchCode=" + branchCode + ", cityId=" + cityId
				+ ", cityName=" + cityName + ", cityCode=" + cityCode
				+ ", areaId=" + areaId + ", areaName=" + areaName
				+ ", areaCode=" + areaCode + ", reportToId=" + reportToId
				+ ", reportToLoginId=" + reportToLoginId + ", userGroupId="
				+ userGroupId + ", userGroup=" + userGroup + ", roleId="
				+ roleId + ", roleCode=" + roleCode + "]";
	}

}
