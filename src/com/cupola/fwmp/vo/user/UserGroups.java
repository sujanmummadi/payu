/**
 * 
 */
package com.cupola.fwmp.vo.user;

/**
 * @author aditya
 *
 */
public class UserGroups
{
	// ug.id,ug.userGroupName,r.id,r.roleName,r.roleCode,r.roleDesc
	private Long userGroupId;
	private String userGroupName;
	private Long roleId;
	private String roleName;
	private String roleCode;
	private String roleDesc;

	public Long getUserGroupId()
	{
		return userGroupId;
	}

	public void setUserGroupId(Long userGroupId)
	{
		this.userGroupId = userGroupId;
	}

	public String getUserGroupName()
	{
		return userGroupName;
	}

	public void setUserGroupName(String userGroupName)
	{
		this.userGroupName = userGroupName;
	}

	public Long getRoleId()
	{
		return roleId;
	}

	public void setRoleId(Long roleId)
	{
		this.roleId = roleId;
	}

	public String getRoleName()
	{
		return roleName;
	}

	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}

	public String getRoleCode()
	{
		return roleCode;
	}

	public void setRoleCode(String roleCode)
	{
		this.roleCode = roleCode;
	}

	public String getRoleDesc()
	{
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc)
	{
		this.roleDesc = roleDesc;
	}

	@Override
	public String toString()
	{
		return "UserGroups [userGroupId=" + userGroupId + ", userGroupName="
				+ userGroupName + ", roleId=" + roleId + ", roleName="
				+ roleName + ", roleCode=" + roleCode + ", roleDesc=" + roleDesc
				+ "]";
	}

}
