package com.cupola.fwmp.vo;

import java.util.Date;
import java.util.Map;

public class TicketAcitivityDetailsVO
{

	private String ticketId;
	private String activityId;
	private String activityStatus;
	private String vendorId;
	private Date activityCompletedTime;
	private String reasons;
	
	private Map<String, String> subActivityDetail;

	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getActivityId()
	{
		return activityId;
	}

	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}

	public String getActivityStatus()
	{
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus)
	{
		this.activityStatus = activityStatus;
	}

	public String getVendorId()
	{
		return vendorId;
	}

	public void setVendorId(String vendorId)
	{
		this.vendorId = vendorId;
	}

	public Date getActivityCompletedTime()
	{
		return activityCompletedTime;
	}

	public void setActivityCompletedTime(Date activityCompletedTime)
	{
		this.activityCompletedTime = activityCompletedTime;
	}

	public Map<String, String> getSubActivityDetail()
	{
		return subActivityDetail;
	}

	public void setSubActivityDetail(Map<String, String> subActivityDetail)
	{
		this.subActivityDetail = subActivityDetail;
	}

	@Override
	public String toString()
	{
		return "TicketAcitivityDetailsVO [ticketId=" + ticketId
				+ ", activityId=" + activityId + ", activityStatus="
				+ activityStatus + ", vendorId=" + vendorId
				+ ", activityCompletedTime=" + activityCompletedTime
				+ ", subActivityDetail=" + subActivityDetail + "]";
	}

	public String getReasons()
	{
		return reasons;
	}

	public void setReasons(String reasons)
	{
		this.reasons = reasons;
	}

}
