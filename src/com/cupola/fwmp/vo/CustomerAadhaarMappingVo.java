 /**
 * @Author noor  Oct 30, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author noor  Oct 30, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CustomerAadhaarMappingVo implements Serializable{

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerAadhaarMappingVo [customerId=" + customerId + ", adhaarTrnId=" + adhaarTrnId + ", ticketId="
				+ ticketId + ", label=" + label + ", addedBy=" + addedBy + ", addedOn=" + addedOn + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long customerId;
	
	private String adhaarTrnId;
	
	private Long ticketId;
	
	private Long label;
	
	private Long addedBy;
	
	private Date addedOn;

	/**
	 * 
	 */
	public CustomerAadhaarMappingVo() {
		super();
	}

	/**
	 * @param customerId
	 * @param adhaarTrnId
	 * @param ticketId
	 * @param label
	 * @param addedBy
	 * @param addedOn
	 */
	public CustomerAadhaarMappingVo(Long customerId, String adhaarTrnId, Long ticketId, Long label, Long addedBy,
			Date addedOn) {
		super();
		this.customerId = customerId;
		this.adhaarTrnId = adhaarTrnId;
		this.ticketId = ticketId;
		this.label = label;
		this.addedBy = addedBy;
		this.addedOn = addedOn;
	}

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the adhaarTrnId
	 */
	public String getAdhaarTrnId() {
		return adhaarTrnId;
	}

	/**
	 * @param adhaarTrnId the adhaarTrnId to set
	 */
	public void setAdhaarTrnId(String adhaarTrnId) {
		this.adhaarTrnId = adhaarTrnId;
	}

	/**
	 * @return the ticketId
	 */
	public Long getTicketId() {
		return ticketId;
	}

	/**
	 * @param ticketId the ticketId to set
	 */
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * @return the label
	 */
	public Long getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(Long label) {
		this.label = label;
	}

	/**
	 * @return the addedBy
	 */
	public Long getAddedBy() {
		return addedBy;
	}

	/**
	 * @param addedBy the addedBy to set
	 */
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	/**
	 * @return the addedOn
	 */
	public Date getAddedOn() {
		return addedOn;
	}

	/**
	 * @param addedOn the addedOn to set
	 */
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	
	
}
