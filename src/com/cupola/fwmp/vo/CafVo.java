package com.cupola.fwmp.vo;

public class CafVo {

	private long ticketId;
	private String cafNumber;

	public long getTicketId() {
		return ticketId;
	}

	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}

	public String getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}

	@Override
	public String toString() {
		return "CafVo [ticketId=" + ticketId + ", cafNumber=" + cafNumber + "]";
	}

}
