/**
 * 
 */
package com.cupola.fwmp.vo.tools;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @Author aditya 22-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

public class CustomerAddressVO {

	@JsonIgnore
	private String customerAddressType;
	private String address1;
	private String address2;
	private String landmark;
	private String pincode;
	private String others;
	private String area;
	private String subArea;
	private String apartmentName;
	private String country;
	private String city;
	private String projectType;
	private String operationalEntity;
	private String postalCode;
	private String state;
	private String latitude;
	private String addressType;
	private String longitude;
	private String landMark;

	@JsonIgnore
	private long areaId;
	@JsonIgnore
	private long subAreaId;
	@JsonIgnore
	private long operationalEntityId;

	/**
	 * @return the customerAddressType
	 */
	public String getCustomerAddressType() {
		return customerAddressType;
	}

	/**
	 * @param customerAddressType
	 *            the customerAddressType to set
	 */
	public void setCustomerAddressType(String customerAddressType) {
		this.customerAddressType = customerAddressType;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the landmark
	 */
	public String getLandmark() {
		return landmark;
	}

	/**
	 * @param landmark
	 *            the landmark to set
	 */
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * @param pincode
	 *            the pincode to set
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the others
	 */
	public String getOthers() {
		return others;
	}

	/**
	 * @param others
	 *            the others to set
	 */
	public void setOthers(String others) {
		this.others = others;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the subArea
	 */
	public String getSubArea() {
		return subArea;
	}

	/**
	 * @param subArea
	 *            the subArea to set
	 */
	public void setSubArea(String subArea) {
		this.subArea = subArea;
	}

	/**
	 * @return the apartmentName
	 */
	public String getApartmentName() {
		return apartmentName;
	}

	/**
	 * @param apartmentName
	 *            the apartmentName to set
	 */
	public void setApartmentName(String apartmentName) {
		this.apartmentName = apartmentName;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the projectType
	 */
	public String getProjectType() {
		return projectType;
	}

	/**
	 * @param projectType
	 *            the projectType to set
	 */
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	/**
	 * @return the operationalEntity
	 */
	public String getOperationalEntity() {
		return operationalEntity;
	}

	/**
	 * @param operationalEntity
	 *            the operationalEntity to set
	 */
	public void setOperationalEntity(String operationalEntity) {
		this.operationalEntity = operationalEntity;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the addressType
	 */
	public String getAddressType() {
		return addressType;
	}

	/**
	 * @param addressType
	 *            the addressType to set
	 */
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the landMark
	 */
	public String getLandMark() {
		return landMark;
	}

	/**
	 * @param landMark
	 *            the landMark to set
	 */
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}

	/**
	 * @return the areaId
	 */
	public long getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId
	 *            the areaId to set
	 */
	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the subAreaId
	 */
	public long getSubAreaId() {
		return subAreaId;
	}

	/**
	 * @param subAreaId
	 *            the subAreaId to set
	 */
	public void setSubAreaId(long subAreaId) {
		this.subAreaId = subAreaId;
	}

	/**
	 * @return the operationalEntityId
	 */
	public long getOperationalEntityId() {
		return operationalEntityId;
	}

	/**
	 * @param operationalEntityId
	 *            the operationalEntityId to set
	 */
	public void setOperationalEntityId(long operationalEntityId) {
		this.operationalEntityId = operationalEntityId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerAddressVO [customerAddressType=" + customerAddressType + ", address1=" + address1
				+ ", address2=" + address2 + ", landmark=" + landmark + ", pincode=" + pincode + ", others=" + others
				+ ", area=" + area + ", subArea=" + subArea + ", apartmentName=" + apartmentName + ", country="
				+ country + ", city=" + city + ", projectType=" + projectType + ", operationalEntity="
				+ operationalEntity + ", postalCode=" + postalCode + ", state=" + state + ", latitude=" + latitude
				+ ", addressType=" + addressType + ", longitude=" + longitude + ", landMark=" + landMark + ", areaId="
				+ areaId + ", subAreaId=" + subAreaId + ", operationalEntityId=" + operationalEntityId + "]";
	}
}
