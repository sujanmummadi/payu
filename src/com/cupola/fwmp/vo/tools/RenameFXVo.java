package com.cupola.fwmp.vo.tools;

public class RenameFXVo
{
	private String oldFxName;
	private String newFxName;

	public String getOldFxName()
	{
		return oldFxName;
	}

	public void setOldFxName(String oldFxName)
	{
		this.oldFxName = oldFxName;
	}

	public String getNewFxName()
	{
		return newFxName;
	}

	public void setNewFxName(String newFxName)
	{
		this.newFxName = newFxName;
	}

	@Override
	public String toString()
	{
		return "RenameFXVo [oldFxName=" + oldFxName + ", newFxName=" + newFxName
				+ "]";
	}

}
