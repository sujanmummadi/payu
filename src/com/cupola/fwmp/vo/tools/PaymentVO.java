/**
 * 
 */
package com.cupola.fwmp.vo.tools;

/**
 * @author pawan
 *
 */
public class PaymentVO {
	
	private String cafNo;
	private String paymentMode;
	private String referenceNo;
	private String paidAmount;
	private String installationCharges;
	private String paidConnection;
	private String seName;
	private String cfeName;
	
	public String getCafNo() {
		return cafNo;
	}
	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getInstallationCharges() {
		return installationCharges;
	}
	public void setInstallationCharges(String installationCharges) {
		this.installationCharges = installationCharges;
	}
	public String getPaidConnection() {
		return paidConnection;
	}
	public void setPaidConnection(String paidConnection) {
		this.paidConnection = paidConnection;
	}
	
	/**
	 * @return the seName
	 */
	public String getSeName() {
		return seName;
	}
	/**
	 * @param seName the seName to set
	 */
	public void setSeName(String seName) {
		this.seName = seName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentVO [cafNo=" + cafNo + ", paymentMode=" + paymentMode + ", referenceNo=" + referenceNo
				+ ", paidAmount=" + paidAmount + ", installationCharges=" + installationCharges + ", paidConnection="
				+ paidConnection + ", seName=" + seName + "]";
	}
	/**
	 * @return the cfeName
	 */
	public String getCfeName() {
		return cfeName;
	}
	/**
	 * @param cfeName the cfeName to set
	 */
	public void setCfeName(String cfeName) {
		this.cfeName = cfeName;
	}

}
