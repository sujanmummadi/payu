/**
 * 
 */
package com.cupola.fwmp.vo.tools;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author pawan
 *
 */
@Document(collection = "CreateUpdateFr")
public class CreateUpdateFrVo {
	
	@Id
	private Long CreateUpdateFrWorkOrderId;
	private String status;
	
	private String action;
	private String prospectNumber;
	private String customerNumber;
	private String ticketNo;
	private String categoryName;
	private String voc;
	private String reopenStatus;
	private String symptomName;
	private Date serviceRequestDate;
	private String commentCode;
	private String comments;
	private String cxIp;
	private String cxName;
	private String cxPort;
	private String cxMac;
	private String fxName;
	private String fxIp;
	private String fxPort;
	private String fxMac;
	private String customerName;
	private CustomerAddressVO address;
	private String mobileNumber;
	private String alternativeMobileNo;
	private String officeNumber;
	private String area;
	private String city;
	private String branch;
	private PriorityVO priorityVO;
	public Long getCreateUpdateFrWorkOrderId() {
		return CreateUpdateFrWorkOrderId;
	}
	public void setCreateUpdateFrWorkOrderId(Long createUpdateFrWorkOrderId) {
		CreateUpdateFrWorkOrderId = createUpdateFrWorkOrderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getProspectNumber() {
		return prospectNumber;
	}
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getVoc() {
		return voc;
	}
	public void setVoc(String voc) {
		this.voc = voc;
	}
	public String getReopenStatus() {
		return reopenStatus;
	}
	public void setReopenStatus(String reopenStatus) {
		this.reopenStatus = reopenStatus;
	}
	public String getSymptomName() {
		return symptomName;
	}
	public void setSymptomName(String symptomName) {
		this.symptomName = symptomName;
	}
	public Date getServiceRequestDate() {
		return serviceRequestDate;
	}
	public void setServiceRequestDate(Date serviceRequestDate) {
		this.serviceRequestDate = serviceRequestDate;
	}
	public String getCommentCode() {
		return commentCode;
	}
	public void setCommentCode(String commentCode) {
		this.commentCode = commentCode;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getCxIp() {
		return cxIp;
	}
	public void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}
	public String getCxName() {
		return cxName;
	}
	public void setCxName(String cxName) {
		this.cxName = cxName;
	}
	public String getCxPort() {
		return cxPort;
	}
	public void setCxPort(String cxPort) {
		this.cxPort = cxPort;
	}
	public String getCxMac() {
		return cxMac;
	}
	public void setCxMac(String cxMac) {
		this.cxMac = cxMac;
	}
	public String getFxName() {
		return fxName;
	}
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	public String getFxIp() {
		return fxIp;
	}
	public void setFxIp(String fxIp) {
		this.fxIp = fxIp;
	}
	public String getFxPort() {
		return fxPort;
	}
	public void setFxPort(String fxPort) {
		this.fxPort = fxPort;
	}
	public String getFxMac() {
		return fxMac;
	}
	public void setFxMac(String fxMac) {
		this.fxMac = fxMac;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public CustomerAddressVO getAddress() {
		return address;
	}
	public void setAddress(CustomerAddressVO address) {
		this.address = address;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAlternativeMobileNo() {
		return alternativeMobileNo;
	}
	public void setAlternativeMobileNo(String alternativeMobileNo) {
		this.alternativeMobileNo = alternativeMobileNo;
	}
	public String getOfficeNumber() {
		return officeNumber;
	}
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public PriorityVO getPriority() {
		return priorityVO;
	}
	public void setPriority(PriorityVO priorityVO) {
		this.priorityVO = priorityVO;
	}
	@Override
	public String toString() {
		return "CreateUpdateFrVo [CreateUpdateFrWorkOrderId="
				+ CreateUpdateFrWorkOrderId + ", status=" + status
				+ ", action=" + action + ", prospectNumber=" + prospectNumber
				+ ", customerNumber=" + customerNumber + ", ticketNo="
				+ ticketNo + ", categoryName=" + categoryName + ", voc=" + voc
				+ ", reopenStatus=" + reopenStatus + ", symptomName="
				+ symptomName + ", serviceRequestDate=" + serviceRequestDate
				+ ", commentCode=" + commentCode + ", comments=" + comments
				+ ", cxIp=" + cxIp + ", cxName=" + cxName + ", cxPort="
				+ cxPort + ", cxMac=" + cxMac + ", fxName=" + fxName
				+ ", fxIp=" + fxIp + ", fxPort=" + fxPort + ", fxMac=" + fxMac
				+ ", customerName=" + customerName + ", address=" + address
				+ ", mobileNumber=" + mobileNumber + ", alternativeMobileNo="
				+ alternativeMobileNo + ", officeNumber=" + officeNumber
				+ ", area=" + area + ", city=" + city + ", branch=" + branch
				+ ", priority=" + priorityVO + "]";
	}

	
}
