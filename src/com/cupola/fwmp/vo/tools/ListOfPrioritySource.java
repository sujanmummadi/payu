/**
 * 
 */
package com.cupola.fwmp.vo.tools;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author pawan
 *
 */
public class ListOfPrioritySource {
	
	@JsonProperty("Priority")
	 PriorityVO Priority;

	/**
	 * @return the priority
	 */
	@JsonProperty("Priority")
	public PriorityVO getPriority() {
		return Priority;
	}

	/**
	 * @param priority the priority to set
	 */
	@JsonProperty("Priority")
	public void setPriority(PriorityVO priority) {
		Priority = priority;
	}

	 
}
