/**
 * 
 */
package com.cupola.fwmp.vo.tools;

import java.util.Date;

import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.SubArea;

/**
 * @author aditya
 *
 */
public class DeviceToolVo
{
	private long id;
	private String branch;
	private String area;
	private String city;
	private String subArea;
	private String deviceName;
	private String ipAddress;
	private String macAddress;
	private String deviceType;
	private String parentDeviceId;
	private String availablePort;
	private String totalPort;
	private String usedPort;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getBranch()
	{
		return branch;
	}

	public void setBranch(String branch)
	{
		this.branch = branch;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getSubArea()
	{
		return subArea;
	}

	public void setSubArea(String subArea)
	{
		this.subArea = subArea;
	}

	public String getDeviceName()
	{
		return deviceName;
	}

	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}

	public String getIpAddress()
	{
		return ipAddress;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	public String getMacAddress()
	{
		return macAddress;
	}

	public void setMacAddress(String macAddress)
	{
		this.macAddress = macAddress;
	}

	public String getDeviceType()
	{
		return deviceType;
	}

	public void setDeviceType(String deviceType)
	{
		this.deviceType = deviceType;
	}

	public String getParentDeviceId()
	{
		return parentDeviceId;
	}

	public void setParentDeviceId(String parentDeviceId)
	{
		this.parentDeviceId = parentDeviceId;
	}

	public String getAvailablePort()
	{
		return availablePort;
	}

	public void setAvailablePort(String availablePort)
	{
		this.availablePort = availablePort;
	}

	public String getTotalPort()
	{
		return totalPort;
	}

	public void setTotalPort(String totalPort)
	{
		this.totalPort = totalPort;
	}

	public String getUsedPort()
	{
		return usedPort;
	}

	public void setUsedPort(String usedPort)
	{
		this.usedPort = usedPort;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "DeviceToolVo [id=" + id + ", branch=" + branch + ", area="
				+ area + ", city=" + city + ", subArea=" + subArea
				+ ", deviceName=" + deviceName + ", ipAddress=" + ipAddress
				+ ", macAddress=" + macAddress + ", deviceType=" + deviceType
				+ ", parentDeviceId=" + parentDeviceId + ", availablePort="
				+ availablePort + ", totalPort=" + totalPort + ", usedPort="
				+ usedPort + ", addedBy=" + addedBy + ", addedOn=" + addedOn
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn
				+ ", status=" + status + "]";
	}

}
