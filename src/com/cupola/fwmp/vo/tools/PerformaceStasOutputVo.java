/**
* @Author aditya  02-Jan-2018
* @Copyright (c) 2018 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools;

import java.util.List;
import java.util.Map;

/**
 * @Author aditya 02-Jan-2018
 * @Copyright (c) 2018 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class PerformaceStasOutputVo {

	private Map<String, List<PerformaceStasOutput>> userAndReportingSubordinateMap;

	/**
	 * @return the userAndReportingSubordinateMap
	 */
	public Map<String, List<PerformaceStasOutput>> getUserAndReportingSubordinateMap() {
		return userAndReportingSubordinateMap;
	}

	/**
	 * @param userAndReportingSubordinateMap
	 *            the userAndReportingSubordinateMap to set
	 */
	public void setUserAndReportingSubordinateMap(
			Map<String, List<PerformaceStasOutput>> userAndReportingSubordinateMap) {
		this.userAndReportingSubordinateMap = userAndReportingSubordinateMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PerformaceStasOutputVo [userAndReportingSubordinateMap=" + userAndReportingSubordinateMap + "]";
	}

}
