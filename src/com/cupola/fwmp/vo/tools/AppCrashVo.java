/**
 * 
 */
package com.cupola.fwmp.vo.tools;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author aditya
 *
 */
@Document(collection = "AppCrashLog")
public class AppCrashVo
{
	private long id;
	private String log;
	private String serverIp;
	private String userId;
	private String appVersion;
	private Date addedOn;
	private String prospectNo;
	private String loginId;
	private String ticketId;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getLog()
	{
		return log;
	}

	public void setLog(String log)
	{
		this.log = log;
	}

	public String getServerIp()
	{
		return serverIp;
	}

	public void setServerIp(String serverIp)
	{
		this.serverIp = serverIp;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getAppVersion()
	{
		return appVersion;
	}

	public void setAppVersion(String appVersion)
	{
		this.appVersion = appVersion;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public String getProspectNo()
	{
		return prospectNo;
	}

	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}

	public String getLoginId()
	{
		return loginId;
	}

	public void setLoginId(String loginId)
	{
		this.loginId = loginId;
	}

	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	@Override
	public String toString()
	{
		return "AppCrashVo [id=" + id + ", log=" + log + ", serverIp="
				+ serverIp + ", userId=" + userId + ", appVersion=" + appVersion
				+ ", addedOn=" + addedOn + ", prospectNo=" + prospectNo
				+ ", loginId=" + loginId + ", ticketId=" + ticketId + "]";
	}
}
