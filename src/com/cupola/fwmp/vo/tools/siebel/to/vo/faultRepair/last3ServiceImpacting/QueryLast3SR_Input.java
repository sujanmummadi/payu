/**
* @Author kiran  Oct 12, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.last3ServiceImpacting;

/**
 * @Author kiran Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class QueryLast3SR_Input {

	private String CustomerNo;

	public String getCustomerNo() {
		return CustomerNo;
	}

	public void setCustomerNo(String CustomerNo) {
		this.CustomerNo = CustomerNo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QueryLast3SR_Input [CustomerNo=" + CustomerNo + "]";
	}

	

}
