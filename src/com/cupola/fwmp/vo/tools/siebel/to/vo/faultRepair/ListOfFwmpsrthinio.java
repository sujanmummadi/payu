/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ListOfFwmpsrthinio {
	
	private ServiceRequest_Lightweight ServiceRequest_Lightweight;

	/**
	 * @return the serviceRequest_Lightweight
	 */
	public ServiceRequest_Lightweight getServiceRequest_Lightweight() {
		return ServiceRequest_Lightweight;
	}

	/**
	 * @param serviceRequest_Lightweight the serviceRequest_Lightweight to set
	 */
	public void setServiceRequest_Lightweight(ServiceRequest_Lightweight serviceRequest_Lightweight) {
		ServiceRequest_Lightweight = serviceRequest_Lightweight;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfFwmpsrthinio [ServiceRequest_Lightweight=" + ServiceRequest_Lightweight + "]";
	}

	

	
	
}
