/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

import java.util.List;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ListMgmtProspectiveContact {

	private String RouterModelCode;

	private String CXPort;

	private String MilestoneStage;

	private String ProspectAssignedTo;

	private String ConnectionType;

	private String FXPort;

	private String HowdidgettoknowaboutACT;

	private String RefereeAccountNumber;

	private String ProductType;

	private String Latitude;

	private String RefereeMobileNumber;

	private String PlanCode;

	private String WifiRequired;

	private String MileStoneStatus;

	private ListOfListMgmtProspectiveContact_ACTContactListMVL ListOfListMgmtProspectiveContact_ACTContactListMVL;

	private String ActionName;

	private String CustomerType;

	private String SubAttribute;

	private String ProspectNum;

	private String ProspectRejectionReason;

	private String SourceType;

	private String CXIP;

	private String FXName;

	private String Notes;

	private String Status;

	private String PlanName;

	private String ProspectNumber;

	private String SchemeCode;

	private String LineofBusiness;

	private ListOfActCutAddress ListOfActCutAddress;

	private String Longitude;

	private String SubSource;

	/**
	 * @return the routerModelCode
	 */
	public String getRouterModelCode() {
		return RouterModelCode;
	}

	/**
	 * @param routerModelCode the routerModelCode to set
	 */
	public void setRouterModelCode(String routerModelCode) {
		RouterModelCode = routerModelCode;
	}

	/**
	 * @return the cXPort
	 */
	public String getCXPort() {
		return CXPort;
	}

	/**
	 * @param cXPort the cXPort to set
	 */
	public void setCXPort(String cXPort) {
		CXPort = cXPort;
	}

	/**
	 * @return the milestoneStage
	 */
	public String getMilestoneStage() {
		return MilestoneStage;
	}

	/**
	 * @param milestoneStage the milestoneStage to set
	 */
	public void setMilestoneStage(String milestoneStage) {
		MilestoneStage = milestoneStage;
	}

	/**
	 * @return the prospectAssignedTo
	 */
	public String getProspectAssignedTo() {
		return ProspectAssignedTo;
	}

	/**
	 * @param prospectAssignedTo the prospectAssignedTo to set
	 */
	public void setProspectAssignedTo(String prospectAssignedTo) {
		ProspectAssignedTo = prospectAssignedTo;
	}

	/**
	 * @return the connectionType
	 */
	public String getConnectionType() {
		return ConnectionType;
	}

	/**
	 * @param connectionType the connectionType to set
	 */
	public void setConnectionType(String connectionType) {
		ConnectionType = connectionType;
	}

	/**
	 * @return the fXPort
	 */
	public String getFXPort() {
		return FXPort;
	}

	/**
	 * @param fXPort the fXPort to set
	 */
	public void setFXPort(String fXPort) {
		FXPort = fXPort;
	}

	/**
	 * @return the howdidgettoknowaboutACT
	 */
	public String getHowdidgettoknowaboutACT() {
		return HowdidgettoknowaboutACT;
	}

	/**
	 * @param howdidgettoknowaboutACT the howdidgettoknowaboutACT to set
	 */
	public void setHowdidgettoknowaboutACT(String howdidgettoknowaboutACT) {
		HowdidgettoknowaboutACT = howdidgettoknowaboutACT;
	}

	/**
	 * @return the refereeAccountNumber
	 */
	public String getRefereeAccountNumber() {
		return RefereeAccountNumber;
	}

	/**
	 * @param refereeAccountNumber the refereeAccountNumber to set
	 */
	public void setRefereeAccountNumber(String refereeAccountNumber) {
		RefereeAccountNumber = refereeAccountNumber;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return ProductType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		ProductType = productType;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return Latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	/**
	 * @return the refereeMobileNumber
	 */
	public String getRefereeMobileNumber() {
		return RefereeMobileNumber;
	}

	/**
	 * @param refereeMobileNumber the refereeMobileNumber to set
	 */
	public void setRefereeMobileNumber(String refereeMobileNumber) {
		RefereeMobileNumber = refereeMobileNumber;
	}

	/**
	 * @return the planCode
	 */
	public String getPlanCode() {
		return PlanCode;
	}

	/**
	 * @param planCode the planCode to set
	 */
	public void setPlanCode(String planCode) {
		PlanCode = planCode;
	}

	/**
	 * @return the wifiRequired
	 */
	public String getWifiRequired() {
		return WifiRequired;
	}

	/**
	 * @param wifiRequired the wifiRequired to set
	 */
	public void setWifiRequired(String wifiRequired) {
		WifiRequired = wifiRequired;
	}

	/**
	 * @return the mileStoneStatus
	 */
	public String getMileStoneStatus() {
		return MileStoneStatus;
	}

	/**
	 * @param mileStoneStatus the mileStoneStatus to set
	 */
	public void setMileStoneStatus(String mileStoneStatus) {
		MileStoneStatus = mileStoneStatus;
	}

	/**
	 * @return the listOfListMgmtProspectiveContact_ACTContactListMVL
	 */
	public ListOfListMgmtProspectiveContact_ACTContactListMVL getListOfListMgmtProspectiveContact_ACTContactListMVL() {
		return ListOfListMgmtProspectiveContact_ACTContactListMVL;
	}

	/**
	 * @param listOfListMgmtProspectiveContact_ACTContactListMVL the listOfListMgmtProspectiveContact_ACTContactListMVL to set
	 */
	public void setListOfListMgmtProspectiveContact_ACTContactListMVL(
			ListOfListMgmtProspectiveContact_ACTContactListMVL listOfListMgmtProspectiveContact_ACTContactListMVL) {
		ListOfListMgmtProspectiveContact_ACTContactListMVL = listOfListMgmtProspectiveContact_ACTContactListMVL;
	}

	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return ActionName;
	}

	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		ActionName = actionName;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return CustomerType;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		CustomerType = customerType;
	}

	/**
	 * @return the subAttribute
	 */
	public String getSubAttribute() {
		return SubAttribute;
	}

	/**
	 * @param subAttribute the subAttribute to set
	 */
	public void setSubAttribute(String subAttribute) {
		SubAttribute = subAttribute;
	}

	/**
	 * @return the prospectNum
	 */
	public String getProspectNum() {
		return ProspectNum;
	}

	/**
	 * @param prospectNum the prospectNum to set
	 */
	public void setProspectNum(String prospectNum) {
		ProspectNum = prospectNum;
	}

	/**
	 * @return the prospectRejectionReason
	 */
	public String getProspectRejectionReason() {
		return ProspectRejectionReason;
	}

	/**
	 * @param prospectRejectionReason the prospectRejectionReason to set
	 */
	public void setProspectRejectionReason(String prospectRejectionReason) {
		ProspectRejectionReason = prospectRejectionReason;
	}

	/**
	 * @return the sourceType
	 */
	public String getSourceType() {
		return SourceType;
	}

	/**
	 * @param sourceType the sourceType to set
	 */
	public void setSourceType(String sourceType) {
		SourceType = sourceType;
	}

	/**
	 * @return the cXIP
	 */
	public String getCXIP() {
		return CXIP;
	}

	/**
	 * @param cXIP the cXIP to set
	 */
	public void setCXIP(String cXIP) {
		CXIP = cXIP;
	}

	/**
	 * @return the fXName
	 */
	public String getFXName() {
		return FXName;
	}

	/**
	 * @param fXName the fXName to set
	 */
	public void setFXName(String fXName) {
		FXName = fXName;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return Notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		Notes = notes;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}

	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return PlanName;
	}

	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		PlanName = planName;
	}

	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return ProspectNumber;
	}

	/**
	 * @param prospectNumber the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		ProspectNumber = prospectNumber;
	}

	/**
	 * @return the schemeCode
	 */
	public String getSchemeCode() {
		return SchemeCode;
	}

	/**
	 * @param schemeCode the schemeCode to set
	 */
	public void setSchemeCode(String schemeCode) {
		SchemeCode = schemeCode;
	}

	/**
	 * @return the lineofBusiness
	 */
	public String getLineofBusiness() {
		return LineofBusiness;
	}

	/**
	 * @param lineofBusiness the lineofBusiness to set
	 */
	public void setLineofBusiness(String lineofBusiness) {
		LineofBusiness = lineofBusiness;
	}

	/**
	 * @return the listOfActCutAddress
	 */
	public ListOfActCutAddress getListOfActCutAddress() {
		return ListOfActCutAddress;
	}

	/**
	 * @param listOfActCutAddress the listOfActCutAddress to set
	 */
	public void setListOfActCutAddress(ListOfActCutAddress listOfActCutAddress) {
		ListOfActCutAddress = listOfActCutAddress;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return Longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	/**
	 * @return the subSource
	 */
	public String getSubSource() {
		return SubSource;
	}

	/**
	 * @param subSource the subSource to set
	 */
	public void setSubSource(String subSource) {
		SubSource = subSource;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListMgmtProspectiveContact [RouterModelCode=" + RouterModelCode + ", CXPort=" + CXPort
				+ ", MilestoneStage=" + MilestoneStage + ", ProspectAssignedTo=" + ProspectAssignedTo
				+ ", ConnectionType=" + ConnectionType + ", FXPort=" + FXPort + ", HowdidgettoknowaboutACT="
				+ HowdidgettoknowaboutACT + ", RefereeAccountNumber=" + RefereeAccountNumber + ", ProductType="
				+ ProductType + ", Latitude=" + Latitude + ", RefereeMobileNumber=" + RefereeMobileNumber
				+ ", PlanCode=" + PlanCode + ", WifiRequired=" + WifiRequired + ", MileStoneStatus=" + MileStoneStatus
				+ ", ListOfListMgmtProspectiveContact_ACTContactListMVL="
				+ ListOfListMgmtProspectiveContact_ACTContactListMVL + ", ActionName=" + ActionName + ", CustomerType="
				+ CustomerType + ", SubAttribute=" + SubAttribute + ", ProspectNum=" + ProspectNum
				+ ", ProspectRejectionReason=" + ProspectRejectionReason + ", SourceType=" + SourceType + ", CXIP="
				+ CXIP + ", FXName=" + FXName + ", Notes=" + Notes + ", Status=" + Status + ", PlanName=" + PlanName
				+ ", ProspectNumber=" + ProspectNumber + ", SchemeCode=" + SchemeCode + ", LineofBusiness="
				+ LineofBusiness + ", ListOfActCutAddress=" + ListOfActCutAddress + ", Longitude=" + Longitude
				+ ", SubSource=" + SubSource + "]";
	}

	

}
