/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

import java.util.List;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ListOfActCreateProspectIo {

	private ListMgmtProspectiveContact ListMgmtProspectiveContact;

	/**
	 * @return the listMgmtProspectiveContact
	 */
	public ListMgmtProspectiveContact getListMgmtProspectiveContact() {
		return ListMgmtProspectiveContact;
	}

	/**
	 * @param listMgmtProspectiveContact the listMgmtProspectiveContact to set
	 */
	public void setListMgmtProspectiveContact(ListMgmtProspectiveContact listMgmtProspectiveContact) {
		ListMgmtProspectiveContact = listMgmtProspectiveContact;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfActCreateProspectIo [ListMgmtProspectiveContact=" + ListMgmtProspectiveContact + "]";
	}

	

	
}
