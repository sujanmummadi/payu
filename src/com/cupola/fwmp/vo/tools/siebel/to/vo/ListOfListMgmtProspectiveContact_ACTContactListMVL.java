/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ListOfListMgmtProspectiveContact_ACTContactListMVL {
	private ListMgmtProspectiveContact_ACTContactListMVL ListMgmtProspectiveContact_ACTContactListMVL;

	public ListMgmtProspectiveContact_ACTContactListMVL getListMgmtProspectiveContact_ACTContactListMVL() {
		return ListMgmtProspectiveContact_ACTContactListMVL;
	}

	public void setListMgmtProspectiveContact_ACTContactListMVL(
			ListMgmtProspectiveContact_ACTContactListMVL ListMgmtProspectiveContact_ACTContactListMVL) {
		this.ListMgmtProspectiveContact_ACTContactListMVL = ListMgmtProspectiveContact_ACTContactListMVL;
	}

	@Override
	public String toString() {
		return "ClassPojo [ListMgmtProspectiveContact_ACTContactListMVL = "
				+ ListMgmtProspectiveContact_ACTContactListMVL + "]";
	}

}
