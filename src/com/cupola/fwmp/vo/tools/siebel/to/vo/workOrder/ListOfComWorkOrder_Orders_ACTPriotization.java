/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ListOfComWorkOrder_Orders_ACTPriotization {

	private  ComWorkOrder_Orders_ACTPriotization ComWorkOrder_Orders_ACTPriotization;

	/**
	 * @return the comWorkOrder_Orders_ACTPriotization
	 */
	public ComWorkOrder_Orders_ACTPriotization getComWorkOrder_Orders_ACTPriotization() {
		return ComWorkOrder_Orders_ACTPriotization;
	}

	/**
	 * @param comWorkOrder_Orders_ACTPriotization the comWorkOrder_Orders_ACTPriotization to set
	 */
	public void setComWorkOrder_Orders_ACTPriotization(
			 ComWorkOrder_Orders_ACTPriotization comWorkOrder_Orders_ACTPriotization) {
		ComWorkOrder_Orders_ACTPriotization = comWorkOrder_Orders_ACTPriotization;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfComWorkOrder_Orders_ACTPriotization [ComWorkOrder_Orders_ACTPriotization="
				+ ComWorkOrder_Orders_ACTPriotization + "]";
	}

	

	
}
