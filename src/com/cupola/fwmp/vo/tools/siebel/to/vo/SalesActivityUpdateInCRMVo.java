/**
* @Author aditya  10-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;

/**
 * @Author aditya 10-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

@Document(collection = "SalesActivityUpdateInCRMVo")
public class SalesActivityUpdateInCRMVo {

	@Id
	private Long salesActivityUpdateInCRMId;
	private long customerId;
	private long ticketId;
	private String prospectNumber;
	private long cityId;
	private long branchId;
	private String status;
	private String transactionId;
	private CreateUpdateProspectRequestInSiebel createUpdateProspectInSiebel;
	
	/**
	 * @return the salesActivityUpdateInCRMId
	 */
	public Long getSalesActivityUpdateInCRMId() {
		return salesActivityUpdateInCRMId;
	}
	/**
	 * @param salesActivityUpdateInCRMId the salesActivityUpdateInCRMId to set
	 */
	public void setSalesActivityUpdateInCRMId(Long salesActivityUpdateInCRMId) {
		this.salesActivityUpdateInCRMId = salesActivityUpdateInCRMId;
	}
	/**
	 * @return the customerId
	 */
	public long getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the ticketId
	 */
	public long getTicketId() {
		return ticketId;
	}
	/**
	 * @param ticketId the ticketId to set
	 */
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return prospectNumber;
	}
	/**
	 * @param prospectNumber the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}
	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}
	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	/**
	 * @return the branchId
	 */
	public long getBranchId() {
		return branchId;
	}
	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the createUpdateProspectInSiebel
	 */
	public CreateUpdateProspectRequestInSiebel getCreateUpdateProspectInSiebel() {
		return createUpdateProspectInSiebel;
	}
	/**
	 * @param createUpdateProspectInSiebel the createUpdateProspectInSiebel to set
	 */
	public void setCreateUpdateProspectInSiebel(CreateUpdateProspectRequestInSiebel createUpdateProspectInSiebel) {
		this.createUpdateProspectInSiebel = createUpdateProspectInSiebel;
	}
	@Override
	public String toString() {
		return "SalesActivityUpdateInCRMVo [salesActivityUpdateInCRMId=" + salesActivityUpdateInCRMId + ", customerId="
				+ customerId + ", ticketId=" + ticketId + ", prospectNumber=" + prospectNumber + ", cityId=" + cityId
				+ ", branchId=" + branchId + ", status=" + status + ", transactionId=" + transactionId
				+ ", createUpdateProspectInSiebel=" + createUpdateProspectInSiebel + "]";
	}
	
}
