/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ListOfComworkorderio {

	private ComWorkOrder_Orders ComWorkOrder_Orders;

	/**
	 * @return the comWorkOrder_Orders
	 */
	public ComWorkOrder_Orders getComWorkOrder_Orders() {
		return ComWorkOrder_Orders;
	}

	/**
	 * @param comWorkOrder_Orders
	 *            the comWorkOrder_Orders to set
	 */
	public void setComWorkOrder_Orders(ComWorkOrder_Orders comWorkOrder_Orders) {
		ComWorkOrder_Orders = comWorkOrder_Orders;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfComworkorderio [ComWorkOrder_Orders=" + ComWorkOrder_Orders + "]";
	}

}
