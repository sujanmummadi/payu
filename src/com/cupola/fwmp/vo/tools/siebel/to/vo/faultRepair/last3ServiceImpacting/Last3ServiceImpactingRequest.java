/**
 * 
 */
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.last3ServiceImpacting;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class Last3ServiceImpactingRequest {
	
	private QueryLast3SR_Input QueryLast3SR_Input;

    public QueryLast3SR_Input getQueryLast3SR_Input ()
    {
        return QueryLast3SR_Input;
    }

    public void setQueryLast3SR_Input (QueryLast3SR_Input QueryLast3SR_Input)
    {
        this.QueryLast3SR_Input = QueryLast3SR_Input;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Last3ServiceImpactingRequest [QueryLast3SR_Input=" + QueryLast3SR_Input + "]";
	}

   
}
