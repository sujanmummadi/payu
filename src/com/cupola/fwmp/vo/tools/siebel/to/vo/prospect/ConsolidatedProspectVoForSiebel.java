/**
* @Author kiran  Oct 12, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

import java.util.Date;

/**
 * @Author kiran Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ConsolidatedProspectVoForSiebel {

	private String Others;

	private String Area;

	private String SubArea;

	private String ApartmentName;

	private String Country;

	private String City;

	private String ProjectType;

	private String OperationalEntity;

	private String PostalCode;

	private String State;

	private String Address2;

	private String Address1;

	private String Latitude;

	private String AddressType;

	private String Longitude;

	private String LandMark;

	private String ContactName;

	private String AlternateMobileNumber;

	private String EmailAddress2;

	private String CompanyName;

	private String LastName;

	private String Title;

	private String Nationality;

	private String CellularPhone;

	private String AadharNumber;

	private String MiddleName;

	private String _IsPrimaryMVG;

	private String UINType;

	private String HOMEPHNUM;

	private String EmailAddress;

	private String FirstName;

	private String UIN;

	private String ContactIntegrationId;

	private String CXPort;

	private String RouterModelCode;

	private String MilestoneStage;

	private String ProspectAssignedTo;

	private String ConnectionType;

	private String CustomerProfile;

	private String ACTCAFSerialNumber;

	private String FXPort;

	private String HowdidgettoknowaboutACT;

	private String ProductType;

	private String RefereeAccountNumber;

	private String PlanCode;

	private String RefereeMobileNumber;

	private String WifiRequired;

	private String MileStoneStatus;

	private String ACTGxRequired;

	private String ActionName;

	private String CustomerType;

	private String SubAttribute;

	private String ProspectRejectionReason;

	private String SourceType;

	private String CXIP;

	private String FXName;

	private String Notes;

	private String ACTCxPermission;

	private String Status;

	private String PlanName;

	private String ProspectNumber;

	private String SchemeCode;

	private String LineofBusiness;

	private String ACTCurrentISP;

	private String SubSource;
	
	private String ActDescription;

	private String Transation_spcId;

	private String PreferredCallDateTime;
	
	private String currentAddress;
	
	private String isStaticIPRequired;
	
	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}

	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	/**
	 * @return the communicationAddress
	 */
	public String getCommunicationAddress() {
		return communicationAddress;
	}

	/**
	 * @param communicationAddress the communicationAddress to set
	 */
	public void setCommunicationAddress(String communicationAddress) {
		this.communicationAddress = communicationAddress;
	}

	/**
	 * @return the permanentAddress
	 */
	public String getPermanentAddress() {
		return permanentAddress;
	}

	/**
	 * @param permanentAddress the permanentAddress to set
	 */
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	private String communicationAddress;
	private String permanentAddress;

	/**
	 * @return the others
	 */
	public String getOthers() {
		return Others;
	}

	/**
	 * @param others the others to set
	 */
	public void setOthers(String others) {
		Others = others;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return Area;
	}

	/**
	 * @param area the area to set
	 */
	public void setArea(String area) {
		Area = area;
	}

	/**
	 * @return the subArea
	 */
	public String getSubArea() {
		return SubArea;
	}

	/**
	 * @param subArea the subArea to set
	 */
	public void setSubArea(String subArea) {
		SubArea = subArea;
	}

	/**
	 * @return the apartmentName
	 */
	public String getApartmentName() {
		return ApartmentName;
	}

	/**
	 * @param apartmentName the apartmentName to set
	 */
	public void setApartmentName(String apartmentName) {
		ApartmentName = apartmentName;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return Country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		Country = country;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return City;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		City = city;
	}

	/**
	 * @return the projectType
	 */
	public String getProjectType() {
		return ProjectType;
	}

	/**
	 * @param projectType the projectType to set
	 */
	public void setProjectType(String projectType) {
		ProjectType = projectType;
	}

	/**
	 * @return the operationalEntity
	 */
	public String getOperationalEntity() {
		return OperationalEntity;
	}

	/**
	 * @param operationalEntity the operationalEntity to set
	 */
	public void setOperationalEntity(String operationalEntity) {
		OperationalEntity = operationalEntity;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return PostalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return State;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		State = state;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return Address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		Address2 = address2;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return Address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		Address1 = address1;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return Latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	/**
	 * @return the addressType
	 */
	public String getAddressType() {
		return AddressType;
	}

	/**
	 * @param addressType the addressType to set
	 */
	public void setAddressType(String addressType) {
		AddressType = addressType;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return Longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	/**
	 * @return the landMark
	 */
	public String getLandMark() {
		return LandMark;
	}

	/**
	 * @param landMark the landMark to set
	 */
	public void setLandMark(String landMark) {
		LandMark = landMark;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return ContactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		ContactName = contactName;
	}

	/**
	 * @return the alternateMobileNumber
	 */
	public String getAlternateMobileNumber() {
		return AlternateMobileNumber;
	}

	/**
	 * @param alternateMobileNumber the alternateMobileNumber to set
	 */
	public void setAlternateMobileNumber(String alternateMobileNumber) {
		AlternateMobileNumber = alternateMobileNumber;
	}

	/**
	 * @return the emailAddress2
	 */
	public String getEmailAddress2() {
		return EmailAddress2;
	}

	/**
	 * @param emailAddress2 the emailAddress2 to set
	 */
	public void setEmailAddress2(String emailAddress2) {
		EmailAddress2 = emailAddress2;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return CompanyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return LastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		LastName = lastName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return Title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		Title = title;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return Nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		Nationality = nationality;
	}

	/**
	 * @return the cellularPhone
	 */
	public String getCellularPhone() {
		return CellularPhone;
	}

	/**
	 * @param cellularPhone the cellularPhone to set
	 */
	public void setCellularPhone(String cellularPhone) {
		CellularPhone = cellularPhone;
	}

	/**
	 * @return the aadharNumber
	 */
	public String getAadharNumber() {
		return AadharNumber;
	}

	/**
	 * @param aadharNumber the aadharNumber to set
	 */
	public void setAadharNumber(String aadharNumber) {
		AadharNumber = aadharNumber;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return MiddleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}

	/**
	 * @return the _IsPrimaryMVG
	 */
	public String get_IsPrimaryMVG() {
		return _IsPrimaryMVG;
	}

	/**
	 * @param _IsPrimaryMVG the _IsPrimaryMVG to set
	 */
	public void set_IsPrimaryMVG(String _IsPrimaryMVG) {
		this._IsPrimaryMVG = _IsPrimaryMVG;
	}

	/**
	 * @return the uINType
	 */
	public String getUINType() {
		return UINType;
	}

	/**
	 * @param uINType the uINType to set
	 */
	public void setUINType(String uINType) {
		UINType = uINType;
	}

	/**
	 * @return the hOMEPHNUM
	 */
	public String getHOMEPHNUM() {
		return HOMEPHNUM;
	}

	/**
	 * @param hOMEPHNUM the hOMEPHNUM to set
	 */
	public void setHOMEPHNUM(String hOMEPHNUM) {
		HOMEPHNUM = hOMEPHNUM;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return EmailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return FirstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	/**
	 * @return the uIN
	 */
	public String getUIN() {
		return UIN;
	}

	/**
	 * @param uIN the uIN to set
	 */
	public void setUIN(String uIN) {
		UIN = uIN;
	}

	/**
	 * @return the contactIntegrationId
	 */
	public String getContactIntegrationId() {
		return ContactIntegrationId;
	}

	/**
	 * @param contactIntegrationId the contactIntegrationId to set
	 */
	public void setContactIntegrationId(String contactIntegrationId) {
		ContactIntegrationId = contactIntegrationId;
	}

	/**
	 * @return the cXPort
	 */
	public String getCXPort() {
		return CXPort;
	}

	/**
	 * @param cXPort the cXPort to set
	 */
	public void setCXPort(String cXPort) {
		CXPort = cXPort;
	}

	/**
	 * @return the routerModelCode
	 */
	public String getRouterModelCode() {
		return RouterModelCode;
	}

	/**
	 * @param routerModelCode the routerModelCode to set
	 */
	public void setRouterModelCode(String routerModelCode) {
		RouterModelCode = routerModelCode;
	}

	/**
	 * @return the milestoneStage
	 */
	public String getMilestoneStage() {
		return MilestoneStage;
	}

	/**
	 * @param milestoneStage the milestoneStage to set
	 */
	public void setMilestoneStage(String milestoneStage) {
		MilestoneStage = milestoneStage;
	}

	/**
	 * @return the prospectAssignedTo
	 */
	public String getProspectAssignedTo() {
		return ProspectAssignedTo;
	}

	/**
	 * @param prospectAssignedTo the prospectAssignedTo to set
	 */
	public void setProspectAssignedTo(String prospectAssignedTo) {
		ProspectAssignedTo = prospectAssignedTo;
	}

	/**
	 * @return the connectionType
	 */
	public String getConnectionType() {
		return ConnectionType;
	}

	/**
	 * @param connectionType the connectionType to set
	 */
	public void setConnectionType(String connectionType) {
		ConnectionType = connectionType;
	}

	/**
	 * @return the customerProfile
	 */
	public String getCustomerProfile() {
		return CustomerProfile;
	}

	/**
	 * @param customerProfile the customerProfile to set
	 */
	public void setCustomerProfile(String customerProfile) {
		CustomerProfile = customerProfile;
	}

	/**
	 * @return the aCTCAFSerialNumber
	 */
	public String getACTCAFSerialNumber() {
		return ACTCAFSerialNumber;
	}

	/**
	 * @param aCTCAFSerialNumber the aCTCAFSerialNumber to set
	 */
	public void setACTCAFSerialNumber(String aCTCAFSerialNumber) {
		ACTCAFSerialNumber = aCTCAFSerialNumber;
	}

	/**
	 * @return the fXPort
	 */
	public String getFXPort() {
		return FXPort;
	}

	/**
	 * @param fXPort the fXPort to set
	 */
	public void setFXPort(String fXPort) {
		FXPort = fXPort;
	}

	/**
	 * @return the howdidgettoknowaboutACT
	 */
	public String getHowdidgettoknowaboutACT() {
		return HowdidgettoknowaboutACT;
	}

	/**
	 * @param howdidgettoknowaboutACT the howdidgettoknowaboutACT to set
	 */
	public void setHowdidgettoknowaboutACT(String howdidgettoknowaboutACT) {
		HowdidgettoknowaboutACT = howdidgettoknowaboutACT;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return ProductType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		ProductType = productType;
	}

	/**
	 * @return the refereeAccountNumber
	 */
	public String getRefereeAccountNumber() {
		return RefereeAccountNumber;
	}

	/**
	 * @param refereeAccountNumber the refereeAccountNumber to set
	 */
	public void setRefereeAccountNumber(String refereeAccountNumber) {
		RefereeAccountNumber = refereeAccountNumber;
	}

	/**
	 * @return the planCode
	 */
	public String getPlanCode() {
		return PlanCode;
	}

	/**
	 * @param planCode the planCode to set
	 */
	public void setPlanCode(String planCode) {
		PlanCode = planCode;
	}

	/**
	 * @return the refereeMobileNumber
	 */
	public String getRefereeMobileNumber() {
		return RefereeMobileNumber;
	}

	/**
	 * @param refereeMobileNumber the refereeMobileNumber to set
	 */
	public void setRefereeMobileNumber(String refereeMobileNumber) {
		RefereeMobileNumber = refereeMobileNumber;
	}

	/**
	 * @return the wifiRequired
	 */
	public String getWifiRequired() {
		return WifiRequired;
	}

	/**
	 * @param wifiRequired the wifiRequired to set
	 */
	public void setWifiRequired(String wifiRequired) {
		WifiRequired = wifiRequired;
	}

	/**
	 * @return the mileStoneStatus
	 */
	public String getMileStoneStatus() {
		return MileStoneStatus;
	}

	/**
	 * @param mileStoneStatus the mileStoneStatus to set
	 */
	public void setMileStoneStatus(String mileStoneStatus) {
		MileStoneStatus = mileStoneStatus;
	}

	/**
	 * @return the aCTGxRequired
	 */
	public String getACTGxRequired() {
		return ACTGxRequired;
	}

	/**
	 * @param aCTGxRequired the aCTGxRequired to set
	 */
	public void setACTGxRequired(String aCTGxRequired) {
		ACTGxRequired = aCTGxRequired;
	}

	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return ActionName;
	}

	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		ActionName = actionName;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return CustomerType;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		CustomerType = customerType;
	}

	/**
	 * @return the subAttribute
	 */
	public String getSubAttribute() {
		return SubAttribute;
	}

	/**
	 * @param subAttribute the subAttribute to set
	 */
	public void setSubAttribute(String subAttribute) {
		SubAttribute = subAttribute;
	}

	/**
	 * @return the prospectRejectionReason
	 */
	public String getProspectRejectionReason() {
		return ProspectRejectionReason;
	}

	/**
	 * @param prospectRejectionReason the prospectRejectionReason to set
	 */
	public void setProspectRejectionReason(String prospectRejectionReason) {
		ProspectRejectionReason = prospectRejectionReason;
	}

	/**
	 * @return the sourceType
	 */
	public String getSourceType() {
		return SourceType;
	}

	/**
	 * @param sourceType the sourceType to set
	 */
	public void setSourceType(String sourceType) {
		SourceType = sourceType;
	}

	/**
	 * @return the cXIP
	 */
	public String getCXIP() {
		return CXIP;
	}

	/**
	 * @param cXIP the cXIP to set
	 */
	public void setCXIP(String cXIP) {
		CXIP = cXIP;
	}

	/**
	 * @return the fXName
	 */
	public String getFXName() {
		return FXName;
	}

	/**
	 * @param fXName the fXName to set
	 */
	public void setFXName(String fXName) {
		FXName = fXName;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return Notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		Notes = notes;
	}

	/**
	 * @return the aCTCxPermission
	 */
	public String getACTCxPermission() {
		return ACTCxPermission;
	}

	/**
	 * @param aCTCxPermission the aCTCxPermission to set
	 */
	public void setACTCxPermission(String aCTCxPermission) {
		ACTCxPermission = aCTCxPermission;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}

	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return PlanName;
	}

	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		PlanName = planName;
	}

	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return ProspectNumber;
	}

	/**
	 * @param prospectNumber the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		ProspectNumber = prospectNumber;
	}

	/**
	 * @return the schemeCode
	 */
	public String getSchemeCode() {
		return SchemeCode;
	}

	/**
	 * @param schemeCode the schemeCode to set
	 */
	public void setSchemeCode(String schemeCode) {
		SchemeCode = schemeCode;
	}

	/**
	 * @return the lineofBusiness
	 */
	public String getLineofBusiness() {
		return LineofBusiness;
	}

	/**
	 * @param lineofBusiness the lineofBusiness to set
	 */
	public void setLineofBusiness(String lineofBusiness) {
		LineofBusiness = lineofBusiness;
	}

	/**
	 * @return the aCTCurrentISP
	 */
	public String getACTCurrentISP() {
		return ACTCurrentISP;
	}

	/**
	 * @param aCTCurrentISP the aCTCurrentISP to set
	 */
	public void setACTCurrentISP(String aCTCurrentISP) {
		ACTCurrentISP = aCTCurrentISP;
	}

	/**
	 * @return the subSource
	 */
	public String getSubSource() {
		return SubSource;
	}

	/**
	 * @param subSource the subSource to set
	 */
	public void setSubSource(String subSource) {
		SubSource = subSource;
	}

	/**
	 * @return the actDescription
	 */
	public String getActDescription() {
		return ActDescription;
	}

	/**
	 * @param actDescription the actDescription to set
	 */
	public void setActDescription(String actDescription) {
		ActDescription = actDescription;
	}

	/**
	 * @return the transation_spcId
	 */
	public String getTransation_spcId() {
		return Transation_spcId;
	}

	/**
	 * @param transation_spcId the transation_spcId to set
	 */
	public void setTransation_spcId(String transation_spcId) {
		Transation_spcId = transation_spcId;
	}

	/**
	 * @return the preferredCallDateTime
	 */
	public String getPreferredCallDateTime() {
		return PreferredCallDateTime;
	}

	/**
	 * @param preferredCallDateTime the preferredCallDateTime to set
	 */
	public void setPreferredCallDateTime(String  preferredCallDateTime) {
		PreferredCallDateTime = preferredCallDateTime;
	}
	
	

	public String getIsStaticIPRequired() {
		return isStaticIPRequired;
	}

	public void setIsStaticIPRequired(String isStaticIPRequired) {
		this.isStaticIPRequired = isStaticIPRequired;
	}

	@Override
	public String toString() {
		return "ConsolidatedProspectVoForSiebel [Others=" + Others + ", Area=" + Area + ", SubArea=" + SubArea
				+ ", ApartmentName=" + ApartmentName + ", Country=" + Country + ", City=" + City + ", ProjectType="
				+ ProjectType + ", OperationalEntity=" + OperationalEntity + ", PostalCode=" + PostalCode + ", State="
				+ State + ", Address2=" + Address2 + ", Address1=" + Address1 + ", Latitude=" + Latitude
				+ ", AddressType=" + AddressType + ", Longitude=" + Longitude + ", LandMark=" + LandMark
				+ ", ContactName=" + ContactName + ", AlternateMobileNumber=" + AlternateMobileNumber
				+ ", EmailAddress2=" + EmailAddress2 + ", CompanyName=" + CompanyName + ", LastName=" + LastName
				+ ", Title=" + Title + ", Nationality=" + Nationality + ", CellularPhone=" + CellularPhone
				+ ", AadharNumber=" + AadharNumber + ", MiddleName=" + MiddleName + ", _IsPrimaryMVG=" + _IsPrimaryMVG
				+ ", UINType=" + UINType + ", HOMEPHNUM=" + HOMEPHNUM + ", EmailAddress=" + EmailAddress
				+ ", FirstName=" + FirstName + ", UIN=" + UIN + ", ContactIntegrationId=" + ContactIntegrationId
				+ ", CXPort=" + CXPort + ", RouterModelCode=" + RouterModelCode + ", MilestoneStage=" + MilestoneStage
				+ ", ProspectAssignedTo=" + ProspectAssignedTo + ", ConnectionType=" + ConnectionType
				+ ", CustomerProfile=" + CustomerProfile + ", ACTCAFSerialNumber=" + ACTCAFSerialNumber + ", FXPort="
				+ FXPort + ", HowdidgettoknowaboutACT=" + HowdidgettoknowaboutACT + ", ProductType=" + ProductType
				+ ", RefereeAccountNumber=" + RefereeAccountNumber + ", PlanCode=" + PlanCode + ", RefereeMobileNumber="
				+ RefereeMobileNumber + ", WifiRequired=" + WifiRequired + ", MileStoneStatus=" + MileStoneStatus
				+ ", ACTGxRequired=" + ACTGxRequired + ", ActionName=" + ActionName + ", CustomerType=" + CustomerType
				+ ", SubAttribute=" + SubAttribute + ", ProspectRejectionReason=" + ProspectRejectionReason
				+ ", SourceType=" + SourceType + ", CXIP=" + CXIP + ", FXName=" + FXName + ", Notes=" + Notes
				+ ", ACTCxPermission=" + ACTCxPermission + ", Status=" + Status + ", PlanName=" + PlanName
				+ ", ProspectNumber=" + ProspectNumber + ", SchemeCode=" + SchemeCode + ", LineofBusiness="
				+ LineofBusiness + ", ACTCurrentISP=" + ACTCurrentISP + ", SubSource=" + SubSource + ", ActDescription="
				+ ActDescription + ", Transation_spcId=" + Transation_spcId + ", PreferredCallDateTime="
				+ PreferredCallDateTime + ", currentAddress=" + currentAddress + ", isStaticIPRequired="
				+ isStaticIPRequired + ", communicationAddress=" + communicationAddress + ", permanentAddress="
				+ permanentAddress + "]";
	}
	
	

}
