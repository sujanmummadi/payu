/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ACTListMgmtProspectiveContactBC_ACTContactListMVL {

	private String ContactName;

	private String AlternateMobileNumber;

	private String EmailAddress2;

	private String CompanyName;

	private String LastName;

	private String Title;

	private String Nationality;

	private String CellularPhone;

	private String AadharNumber;

	private String MiddleName;

	private String _IsPrimaryMVG;

	private String UINType;

	private String HOMEPHNUM;

	private String EmailAddress;

	private String FirstName;

	private String UIN;

	private String ContactIntegrationId;

	public String getContactName() {
		return ContactName;
	}

	public void setContactName(String ContactName) {
		this.ContactName = ContactName;
	}

	public String getAlternateMobileNumber() {
		return AlternateMobileNumber;
	}

	public void setAlternateMobileNumber(String AlternateMobileNumber) {
		this.AlternateMobileNumber = AlternateMobileNumber;
	}

	public String getEmailAddress2() {
		return EmailAddress2;
	}

	public void setEmailAddress2(String EmailAddress2) {
		this.EmailAddress2 = EmailAddress2;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String CompanyName) {
		this.CompanyName = CompanyName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String Title) {
		this.Title = Title;
	}

	public String getNationality() {
		return Nationality;
	}

	public void setNationality(String Nationality) {
		this.Nationality = Nationality;
	}

	public String getCellularPhone() {
		return CellularPhone;
	}

	public void setCellularPhone(String CellularPhone) {
		this.CellularPhone = CellularPhone;
	}

	public String getAadharNumber() {
		return AadharNumber;
	}

	public void setAadharNumber(String AadharNumber) {
		this.AadharNumber = AadharNumber;
	}

	public String getMiddleName() {
		return MiddleName;
	}

	public void setMiddleName(String MiddleName) {
		this.MiddleName = MiddleName;
	}

	public String get_IsPrimaryMVG() {
		return _IsPrimaryMVG;
	}

	public void set_IsPrimaryMVG(String _IsPrimaryMVG) {
		this._IsPrimaryMVG = _IsPrimaryMVG;
	}

	public String getUINType() {
		return UINType;
	}

	public void setUINType(String UINType) {
		this.UINType = UINType;
	}

	public String getHOMEPHNUM() {
		return HOMEPHNUM;
	}

	public void setHOMEPHNUM(String HOMEPHNUM) {
		this.HOMEPHNUM = HOMEPHNUM;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String EmailAddress) {
		this.EmailAddress = EmailAddress;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}

	public String getUIN() {
		return UIN;
	}

	public void setUIN(String UIN) {
		this.UIN = UIN;
	}

	public String getContactIntegrationId() {
		return ContactIntegrationId;
	}

	public void setContactIntegrationId(String ContactIntegrationId) {
		this.ContactIntegrationId = ContactIntegrationId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ACTListMgmtProspectiveContactBC_ACTContactListMVL [ContactName=" + ContactName
				+ ", AlternateMobileNumber=" + AlternateMobileNumber + ", EmailAddress2=" + EmailAddress2
				+ ", CompanyName=" + CompanyName + ", LastName=" + LastName + ", Title=" + Title + ", Nationality="
				+ Nationality + ", CellularPhone=" + CellularPhone + ", AadharNumber=" + AadharNumber + ", MiddleName="
				+ MiddleName + ", _IsPrimaryMVG=" + _IsPrimaryMVG + ", UINType=" + UINType + ", HOMEPHNUM=" + HOMEPHNUM
				+ ", EmailAddress=" + EmailAddress + ", FirstName=" + FirstName + ", UIN=" + UIN
				+ ", ContactIntegrationId=" + ContactIntegrationId + "]";
	}

	
}
