/**
* @Author kiran  Oct 12, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

/**
 * @Author kiran Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ConsolidatedWorkOrderVoForSiebel {

	private String ACTGxId;

	private String LastUpdatedBy;

	private String ACTCATFIVEStartReading;

	private String CompletionDate;

	private String ACTCATFIVEEndReading;

	private String ACTCxMACId;

	private String ACTFxMACId;

	private String ActivityType;

	private String ACTRouterMacId;

	private String ACTCxName;

	private String Status;

	private String VendorName;

	private String ACTFxName;

	private String ACTFxPortNumber;

	private String ACTFxIP;

	private String ACTFiberCableStartReading;

	private String ACTCxIP;

	private String ACTDescription;

	private String ACTFiberCableEndReading;

	private String ACTCxPortNumber;
	private String PrioritySource;

	private String CancellationReason;

	private String ACTETR;

	private String WorkOrderNumber;

	private String AssignedTo;

	private String PriorityValue;

	private String ACTResolutionCode;

	private String WorkOrderCompletionDate;

	private String ACTSubResolutionCode;

	private String ACTConnectionType;

	private String TransactionID;

	/**
	 * @return the aCTGxId
	 */
	public String getACTGxId() {
		return ACTGxId;
	}

	/**
	 * @param aCTGxId
	 *            the aCTGxId to set
	 */
	public void setACTGxId(String aCTGxId) {
		ACTGxId = aCTGxId;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy
	 *            the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	/**
	 * @return the aCTCATFIVEStartReading
	 */
	public String getACTCATFIVEStartReading() {
		return ACTCATFIVEStartReading;
	}

	/**
	 * @param aCTCATFIVEStartReading
	 *            the aCTCATFIVEStartReading to set
	 */
	public void setACTCATFIVEStartReading(String aCTCATFIVEStartReading) {
		ACTCATFIVEStartReading = aCTCATFIVEStartReading;
	}

	/**
	 * @return the completionDate
	 */
	public String getCompletionDate() {
		return CompletionDate;
	}

	/**
	 * @param completionDate
	 *            the completionDate to set
	 */
	public void setCompletionDate(String completionDate) {
		CompletionDate = completionDate;
	}

	/**
	 * @return the aCTCATFIVEEndReading
	 */
	public String getACTCATFIVEEndReading() {
		return ACTCATFIVEEndReading;
	}

	/**
	 * @param aCTCATFIVEEndReading
	 *            the aCTCATFIVEEndReading to set
	 */
	public void setACTCATFIVEEndReading(String aCTCATFIVEEndReading) {
		ACTCATFIVEEndReading = aCTCATFIVEEndReading;
	}

	/**
	 * @return the aCTCxMACId
	 */
	public String getACTCxMACId() {
		return ACTCxMACId;
	}

	/**
	 * @param aCTCxMACId
	 *            the aCTCxMACId to set
	 */
	public void setACTCxMACId(String aCTCxMACId) {
		ACTCxMACId = aCTCxMACId;
	}

	/**
	 * @return the aCTFxMACId
	 */
	public String getACTFxMACId() {
		return ACTFxMACId;
	}

	/**
	 * @param aCTFxMACId
	 *            the aCTFxMACId to set
	 */
	public void setACTFxMACId(String aCTFxMACId) {
		ACTFxMACId = aCTFxMACId;
	}

	/**
	 * @return the activityType
	 */
	public String getActivityType() {
		return ActivityType;
	}

	/**
	 * @param activityType
	 *            the activityType to set
	 */
	public void setActivityType(String activityType) {
		ActivityType = activityType;
	}

	/**
	 * @return the aCTRouterMacId
	 */
	public String getACTRouterMacId() {
		return ACTRouterMacId;
	}

	/**
	 * @param aCTRouterMacId
	 *            the aCTRouterMacId to set
	 */
	public void setACTRouterMacId(String aCTRouterMacId) {
		ACTRouterMacId = aCTRouterMacId;
	}

	/**
	 * @return the aCTCxName
	 */
	public String getACTCxName() {
		return ACTCxName;
	}

	/**
	 * @param aCTCxName
	 *            the aCTCxName to set
	 */
	public void setACTCxName(String aCTCxName) {
		ACTCxName = aCTCxName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return VendorName;
	}

	/**
	 * @param vendorName
	 *            the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		VendorName = vendorName;
	}

	/**
	 * @return the aCTFxName
	 */
	public String getACTFxName() {
		return ACTFxName;
	}

	/**
	 * @param aCTFxName
	 *            the aCTFxName to set
	 */
	public void setACTFxName(String aCTFxName) {
		ACTFxName = aCTFxName;
	}

	/**
	 * @return the aCTFxPortNumber
	 */
	public String getACTFxPortNumber() {
		return ACTFxPortNumber;
	}

	/**
	 * @param aCTFxPortNumber
	 *            the aCTFxPortNumber to set
	 */
	public void setACTFxPortNumber(String aCTFxPortNumber) {
		ACTFxPortNumber = aCTFxPortNumber;
	}

	/**
	 * @return the aCTFxIP
	 */
	public String getACTFxIP() {
		return ACTFxIP;
	}

	/**
	 * @param aCTFxIP
	 *            the aCTFxIP to set
	 */
	public void setACTFxIP(String aCTFxIP) {
		ACTFxIP = aCTFxIP;
	}

	/**
	 * @return the aCTFiberCableStartReading
	 */
	public String getACTFiberCableStartReading() {
		return ACTFiberCableStartReading;
	}

	/**
	 * @param aCTFiberCableStartReading
	 *            the aCTFiberCableStartReading to set
	 */
	public void setACTFiberCableStartReading(String aCTFiberCableStartReading) {
		ACTFiberCableStartReading = aCTFiberCableStartReading;
	}

	/**
	 * @return the aCTCxIP
	 */
	public String getACTCxIP() {
		return ACTCxIP;
	}

	/**
	 * @param aCTCxIP
	 *            the aCTCxIP to set
	 */
	public void setACTCxIP(String aCTCxIP) {
		ACTCxIP = aCTCxIP;
	}

	/**
	 * @return the aCTDescription
	 */
	public String getACTDescription() {
		return ACTDescription;
	}

	/**
	 * @param aCTDescription
	 *            the aCTDescription to set
	 */
	public void setACTDescription(String aCTDescription) {
		ACTDescription = aCTDescription;
	}

	/**
	 * @return the aCTFiberCableEndReading
	 */
	public String getACTFiberCableEndReading() {
		return ACTFiberCableEndReading;
	}

	/**
	 * @param aCTFiberCableEndReading
	 *            the aCTFiberCableEndReading to set
	 */
	public void setACTFiberCableEndReading(String aCTFiberCableEndReading) {
		ACTFiberCableEndReading = aCTFiberCableEndReading;
	}

	/**
	 * @return the aCTCxPortNumber
	 */
	public String getACTCxPortNumber() {
		return ACTCxPortNumber;
	}

	/**
	 * @param aCTCxPortNumber
	 *            the aCTCxPortNumber to set
	 */
	public void setACTCxPortNumber(String aCTCxPortNumber) {
		ACTCxPortNumber = aCTCxPortNumber;
	}

	/**
	 * @return the prioritySource
	 */
	public String getPrioritySource() {
		return PrioritySource;
	}

	/**
	 * @param prioritySource
	 *            the prioritySource to set
	 */
	public void setPrioritySource(String prioritySource) {
		PrioritySource = prioritySource;
	}

	/**
	 * @return the cancellationReason
	 */
	public String getCancellationReason() {
		return CancellationReason;
	}

	/**
	 * @param cancellationReason
	 *            the cancellationReason to set
	 */
	public void setCancellationReason(String cancellationReason) {
		CancellationReason = cancellationReason;
	}

	/**
	 * @return the aCTETR
	 */
	public String getACTETR() {
		return ACTETR;
	}

	/**
	 * @param aCTETR
	 *            the aCTETR to set
	 */
	public void setACTETR(String aCTETR) {
		ACTETR = aCTETR;
	}

	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return WorkOrderNumber;
	}

	/**
	 * @param workOrderNumber
	 *            the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		WorkOrderNumber = workOrderNumber;
	}

	/**
	 * @return the assignedTo
	 */
	public String getAssignedTo() {
		return AssignedTo;
	}

	/**
	 * @param assignedTo
	 *            the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		AssignedTo = assignedTo;
	}

	/**
	 * @return the priorityValue
	 */
	public String getPriorityValue() {
		return PriorityValue;
	}

	/**
	 * @param priorityValue
	 *            the priorityValue to set
	 */
	public void setPriorityValue(String priorityValue) {
		PriorityValue = priorityValue;
	}

	/**
	 * @return the aCTResolutionCode
	 */
	public String getACTResolutionCode() {
		return ACTResolutionCode;
	}

	/**
	 * @param aCTResolutionCode
	 *            the aCTResolutionCode to set
	 */
	public void setACTResolutionCode(String aCTResolutionCode) {
		ACTResolutionCode = aCTResolutionCode;
	}

	/**
	 * @return the workOrderCompletionDate
	 */
	public String getWorkOrderCompletionDate() {
		return WorkOrderCompletionDate;
	}

	/**
	 * @param workOrderCompletionDate
	 *            the workOrderCompletionDate to set
	 */
	public void setWorkOrderCompletionDate(String workOrderCompletionDate) {
		WorkOrderCompletionDate = workOrderCompletionDate;
	}

	/**
	 * @return the aCTSubResolutionCode
	 */
	public String getACTSubResolutionCode() {
		return ACTSubResolutionCode;
	}

	/**
	 * @param aCTSubResolutionCode
	 *            the aCTSubResolutionCode to set
	 */
	public void setACTSubResolutionCode(String aCTSubResolutionCode) {
		ACTSubResolutionCode = aCTSubResolutionCode;
	}

	/**
	 * @return the aCTConnectionType
	 */
	public String getACTConnectionType() {
		return ACTConnectionType;
	}

	/**
	 * @param aCTConnectionType
	 *            the aCTConnectionType to set
	 */
	public void setACTConnectionType(String aCTConnectionType) {
		ACTConnectionType = aCTConnectionType;
	}

	/**
	 * @return the transactionID
	 */
	public String getTransactionID() {
		return TransactionID;
	}

	/**
	 * @param transactionID
	 *            the transactionID to set
	 */
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConsolidatedWorkOrderVoForSiebel [ACTGxId=" + ACTGxId + ", LastUpdatedBy=" + LastUpdatedBy
				+ ", ACTCATFIVEStartReading=" + ACTCATFIVEStartReading + ", CompletionDate=" + CompletionDate
				+ ", ACTCATFIVEEndReading=" + ACTCATFIVEEndReading + ", ACTCxMACId=" + ACTCxMACId + ", ACTFxMACId="
				+ ACTFxMACId + ", ActivityType=" + ActivityType + ", ACTRouterMacId=" + ACTRouterMacId + ", ACTCxName="
				+ ACTCxName + ", Status=" + Status + ", VendorName=" + VendorName + ", ACTFxName=" + ACTFxName
				+ ", ACTFxPortNumber=" + ACTFxPortNumber + ", ACTFxIP=" + ACTFxIP + ", ACTFiberCableStartReading="
				+ ACTFiberCableStartReading + ", ACTCxIP=" + ACTCxIP + ", ACTDescription=" + ACTDescription
				+ ", ACTFiberCableEndReading=" + ACTFiberCableEndReading + ", ACTCxPortNumber=" + ACTCxPortNumber
				+ ", PrioritySource=" + PrioritySource + ", CancellationReason=" + CancellationReason + ", ACTETR="
				+ ACTETR + ", WorkOrderNumber=" + WorkOrderNumber + ", AssignedTo=" + AssignedTo + ", PriorityValue="
				+ PriorityValue + ", ACTResolutionCode=" + ACTResolutionCode + ", WorkOrderCompletionDate="
				+ WorkOrderCompletionDate + ", ACTSubResolutionCode=" + ACTSubResolutionCode + ", ACTConnectionType="
				+ ACTConnectionType + ", TransactionID=" + TransactionID + "]";
	}

}
