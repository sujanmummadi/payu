/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ComWorkOrder_Orders {

	private ListOfAction ListOfAction;

	private String CancellationReason;

	private ListOfComWorkOrder_Orders_ACTPriotization ListOfComWorkOrder_Orders_ACTPriotization;//

	private String ACTETR;

	private String WorkOrderNumber;

	private String AssignedTo;

	private String PriorityValue;

	private String ACTResolutionCode;

	private String WorkOrderCompletionDate;

	private String ACTSubResolutionCode;

	private String ACTConnectionType;

	public ListOfAction getListOfAction() {
		return ListOfAction;
	}

	public void setListOfAction(ListOfAction ListOfAction) {
		this.ListOfAction = ListOfAction;
	}

	public String getCancellationReason() {
		return CancellationReason;
	}

	public void setCancellationReason(String CancellationReason) {
		this.CancellationReason = CancellationReason;
	}

	public ListOfComWorkOrder_Orders_ACTPriotization getListOfComWorkOrder_Orders_ACTPriotization() {
		return ListOfComWorkOrder_Orders_ACTPriotization;
	}

	public void setListOfComWorkOrder_Orders_ACTPriotization(
			ListOfComWorkOrder_Orders_ACTPriotization ListOfComWorkOrder_Orders_ACTPriotization) {
		this.ListOfComWorkOrder_Orders_ACTPriotization = ListOfComWorkOrder_Orders_ACTPriotization;
	}

	public String getACTETR() {
		return ACTETR;
	}

	public void setACTETR(String ACTETR) {
		this.ACTETR = ACTETR;
	}

	public String getWorkOrderNumber() {
		return WorkOrderNumber;
	}

	public void setWorkOrderNumber(String WorkOrderNumber) {
		this.WorkOrderNumber = WorkOrderNumber;
	}

	public String getAssignedTo() {
		return AssignedTo;
	}

	public void setAssignedTo(String AssignedTo) {
		this.AssignedTo = AssignedTo;
	}

	public String getPriorityValue() {
		return PriorityValue;
	}

	public void setPriorityValue(String PriorityValue) {
		this.PriorityValue = PriorityValue;
	}

	public String getACTResolutionCode() {
		return ACTResolutionCode;
	}

	public void setACTResolutionCode(String ACTResolutionCode) {
		this.ACTResolutionCode = ACTResolutionCode;
	}

	public String getWorkOrderCompletionDate() {
		return WorkOrderCompletionDate;
	}

	public void setWorkOrderCompletionDate(String WorkOrderCompletionDate) {
		this.WorkOrderCompletionDate = WorkOrderCompletionDate;
	}

	public String getACTSubResolutionCode() {
		return ACTSubResolutionCode;
	}

	public void setACTSubResolutionCode(String ACTSubResolutionCode) {
		this.ACTSubResolutionCode = ACTSubResolutionCode;
	}

	public String getACTConnectionType() {
		return ACTConnectionType;
	}

	public void setACTConnectionType(String ACTConnectionType) {
		this.ACTConnectionType = ACTConnectionType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ComWorkOrder_Orders [ListOfAction=" + ListOfAction + ", CancellationReason=" + CancellationReason
				+ ", ListOfComWorkOrder_Orders_ACTPriotization=" + ListOfComWorkOrder_Orders_ACTPriotization
				+ ", ACTETR=" + ACTETR + ", WorkOrderNumber=" + WorkOrderNumber + ", AssignedTo=" + AssignedTo
				+ ", PriorityValue=" + PriorityValue + ", ACTResolutionCode=" + ACTResolutionCode
				+ ", WorkOrderCompletionDate=" + WorkOrderCompletionDate + ", ACTSubResolutionCode="
				+ ACTSubResolutionCode + ", ACTConnectionType=" + ACTConnectionType + "]";
	}

	
}
