/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ActCutAddress {

	private String ProjectType;

	private String OperationalEntity;

	private String PostalCode;

	private String State;

	private String Others;

	private String Address2;

	private String Address1;

	private String Area;

	private String ApartmentName;

	private String SubArea;

	private String AddressType;

	private String Country;

	private String City;

	private String LandMark;

	public String getProjectType() {
		return ProjectType;
	}

	public void setProjectType(String projectType) {
		ProjectType = projectType;
	}

	public String getOperationalEntity() {
		return OperationalEntity;
	}

	public void setOperationalEntity(String operationalEntity) {
		OperationalEntity = operationalEntity;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getOthers() {
		return Others;
	}

	public void setOthers(String others) {
		Others = others;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String area) {
		Area = area;
	}

	public String getApartmentName() {
		return ApartmentName;
	}

	public void setApartmentName(String apartmentName) {
		ApartmentName = apartmentName;
	}

	public String getSubArea() {
		return SubArea;
	}

	public void setSubArea(String subArea) {
		SubArea = subArea;
	}

	public String getAddressType() {
		return AddressType;
	}

	public void setAddressType(String addressType) {
		AddressType = addressType;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getLandMark() {
		return LandMark;
	}

	public void setLandMark(String landMark) {
		LandMark = landMark;
	}

	@Override
	public String toString() {
		return "ActCutAddress [ProjectType=" + ProjectType + ", OperationalEntity=" + OperationalEntity
				+ ", PostalCode=" + PostalCode + ", State=" + State + ", Others=" + Others + ", Address2=" + Address2
				+ ", Address1=" + Address1 + ", Area=" + Area + ", ApartmentName=" + ApartmentName + ", SubArea="
				+ SubArea + ", AddressType=" + AddressType + ", Country=" + Country + ", City=" + City + ", LandMark="
				+ LandMark + "]";
	}
	
	

}
