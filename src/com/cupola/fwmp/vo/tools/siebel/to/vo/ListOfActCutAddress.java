/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ListOfActCutAddress {

	private ActCutAddress ActCutAddress;

	/**
	 * @return the actCutAddress
	 */
	public ActCutAddress getActCutAddress() {
		return ActCutAddress;
	}

	/**
	 * @param actCutAddress the actCutAddress to set
	 */
	public void setActCutAddress(ActCutAddress actCutAddress) {
		ActCutAddress = actCutAddress;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfActCutAddress [ActCutAddress=" + ActCutAddress + "]";
	}
	
	

}
