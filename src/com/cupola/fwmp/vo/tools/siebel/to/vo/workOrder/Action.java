/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class Action {

	private String ACTGxId;

	private String LastUpdatedBy;

	private String ACTCATFIVEStartReading;

	private String CompletionDate;

	private String ACTCATFIVEEndReading;

	private String ACTCxMACId;

	private String ACTFxMACId;

	private String ActivityType;

	private String ACTRouterMacId;

	private String ACTCxName;

	private String Status;

	private String VendorName;

	private String ACTFxName;

	private String ACTFxPortNumber;

	private String ACTFxIP;

	private String ACTFiberCableStartReading;

	private String ACTCxIP;

	private String ACTDescription;

	private String ACTFiberCableEndReading;

	private String ACTCxPortNumber;

	public String getACTGxId() {
		return ACTGxId;
	}

	public void setACTGxId(String ACTGxId) {
		this.ACTGxId = ACTGxId;
	}

	public String getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(String LastUpdatedBy) {
		this.LastUpdatedBy = LastUpdatedBy;
	}

	public String getACTCATFIVEStartReading() {
		return ACTCATFIVEStartReading;
	}

	public void setACTCATFIVEStartReading(String ACTCATFIVEStartReading) {
		this.ACTCATFIVEStartReading = ACTCATFIVEStartReading;
	}

	public String getCompletionDate() {
		return CompletionDate;
	}

	public void setCompletionDate(String CompletionDate) {
		this.CompletionDate = CompletionDate;
	}

	public String getACTCATFIVEEndReading() {
		return ACTCATFIVEEndReading;
	}

	public void setACTCATFIVEEndReading(String ACTCATFIVEEndReading) {
		this.ACTCATFIVEEndReading = ACTCATFIVEEndReading;
	}

	public String getACTCxMACId() {
		return ACTCxMACId;
	}

	public void setACTCxMACId(String ACTCxMACId) {
		this.ACTCxMACId = ACTCxMACId;
	}

	public String getACTFxMACId() {
		return ACTFxMACId;
	}

	public void setACTFxMACId(String ACTFxMACId) {
		this.ACTFxMACId = ACTFxMACId;
	}

	public String getActivityType() {
		return ActivityType;
	}

	public void setActivityType(String ActivityType) {
		this.ActivityType = ActivityType;
	}

	public String getACTRouterMacId() {
		return ACTRouterMacId;
	}

	public void setACTRouterMacId(String ACTRouterMacId) {
		this.ACTRouterMacId = ACTRouterMacId;
	}

	public String getACTCxName() {
		return ACTCxName;
	}

	public void setACTCxName(String ACTCxName) {
		this.ACTCxName = ACTCxName;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String Status) {
		this.Status = Status;
	}

	public String getVendorName() {
		return VendorName;
	}

	public void setVendorName(String VendorName) {
		this.VendorName = VendorName;
	}

	public String getACTFxName() {
		return ACTFxName;
	}

	public void setACTFxName(String ACTFxName) {
		this.ACTFxName = ACTFxName;
	}

	public String getACTFxPortNumber() {
		return ACTFxPortNumber;
	}

	public void setACTFxPortNumber(String ACTFxPortNumber) {
		this.ACTFxPortNumber = ACTFxPortNumber;
	}

	public String getACTFxIP() {
		return ACTFxIP;
	}

	public void setACTFxIP(String ACTFxIP) {
		this.ACTFxIP = ACTFxIP;
	}

	public String getACTFiberCableStartReading() {
		return ACTFiberCableStartReading;
	}

	public void setACTFiberCableStartReading(String ACTFiberCableStartReading) {
		this.ACTFiberCableStartReading = ACTFiberCableStartReading;
	}

	public String getACTCxIP() {
		return ACTCxIP;
	}

	public void setACTCxIP(String ACTCxIP) {
		this.ACTCxIP = ACTCxIP;
	}

	public String getACTDescription() {
		return ACTDescription;
	}

	public void setACTDescription(String ACTDescription) {
		this.ACTDescription = ACTDescription;
	}

	public String getACTFiberCableEndReading() {
		return ACTFiberCableEndReading;
	}

	public void setACTFiberCableEndReading(String ACTFiberCableEndReading) {
		this.ACTFiberCableEndReading = ACTFiberCableEndReading;
	}

	public String getACTCxPortNumber() {
		return ACTCxPortNumber;
	}

	public void setACTCxPortNumber(String ACTCxPortNumber) {
		this.ACTCxPortNumber = ACTCxPortNumber;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Action [ACTGxId=" + ACTGxId + ", LastUpdatedBy=" + LastUpdatedBy + ", ACTCATFIVEStartReading="
				+ ACTCATFIVEStartReading + ", CompletionDate=" + CompletionDate + ", ACTCATFIVEEndReading="
				+ ACTCATFIVEEndReading + ", ACTCxMACId=" + ACTCxMACId + ", ACTFxMACId=" + ACTFxMACId + ", ActivityType="
				+ ActivityType + ", ACTRouterMacId=" + ACTRouterMacId + ", ACTCxName=" + ACTCxName + ", Status="
				+ Status + ", VendorName=" + VendorName + ", ACTFxName=" + ACTFxName + ", ACTFxPortNumber="
				+ ACTFxPortNumber + ", ACTFxIP=" + ACTFxIP + ", ACTFiberCableStartReading=" + ACTFiberCableStartReading
				+ ", ACTCxIP=" + ACTCxIP + ", ACTDescription=" + ACTDescription + ", ACTFiberCableEndReading="
				+ ACTFiberCableEndReading + ", ACTCxPortNumber=" + ACTCxPortNumber + "]";
	}

	
}
