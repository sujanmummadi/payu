/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ServiceRequest_Lightweight {

	private String TicketDescription;

	private String Owner;

	private String CallType;

	private String ClosedDate;

	private String ResolutionCode;

	private String VOC;

	private String TicketSource;

	private String Nature;

	private ListOfServiceRequest_Lightweight_ACTPriotization ListOfServiceRequest_Lightweight_ACTPriotization;

	private String Priority;

	private String Status;

	private String SRNumber;

	private String ETR;

	private String Category;

	private String CustomerNumber;

	private String ResolutionDescription;

	private String SubResolutionCode;

	public String getTicketDescription() {
		return TicketDescription;
	}

	public void setTicketDescription(String TicketDescription) {
		this.TicketDescription = TicketDescription;
	}

	public String getOwner() {
		return Owner;
	}

	public void setOwner(String Owner) {
		this.Owner = Owner;
	}

	public String getCallType() {
		return CallType;
	}

	public void setCallType(String CallType) {
		this.CallType = CallType;
	}

	public String getClosedDate() {
		return ClosedDate;
	}

	public void setClosedDate(String ClosedDate) {
		this.ClosedDate = ClosedDate;
	}

	public String getResolutionCode() {
		return ResolutionCode;
	}

	public void setResolutionCode(String ResolutionCode) {
		this.ResolutionCode = ResolutionCode;
	}

	public String getVOC() {
		return VOC;
	}

	public void setVOC(String VOC) {
		this.VOC = VOC;
	}

	public String getTicketSource() {
		return TicketSource;
	}

	public void setTicketSource(String TicketSource) {
		this.TicketSource = TicketSource;
	}

	public String getNature() {
		return Nature;
	}

	public void setNature(String Nature) {
		this.Nature = Nature;
	}

	public String getPriority() {
		return Priority;
	}

	public void setPriority(String Priority) {
		this.Priority = Priority;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String Status) {
		this.Status = Status;
	}

	public String getSRNumber() {
		return SRNumber;
	}

	public void setSRNumber(String SRNumber) {
		this.SRNumber = SRNumber;
	}

	public String getETR() {
		return ETR;
	}

	public void setETR(String ETR) {
		this.ETR = ETR;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String Category) {
		this.Category = Category;
	}

	public String getCustomerNumber() {
		return CustomerNumber;
	}

	public void setCustomerNumber(String CustomerNumber) {
		this.CustomerNumber = CustomerNumber;
	}

	public String getResolutionDescription() {
		return ResolutionDescription;
	}

	public void setResolutionDescription(String ResolutionDescription) {
		this.ResolutionDescription = ResolutionDescription;
	}

	public String getSubResolutionCode() {
		return SubResolutionCode;
	}

	public void setSubResolutionCode(String SubResolutionCode) {
		this.SubResolutionCode = SubResolutionCode;
	}

	/**
	 * @return the listOfServiceRequest_Lightweight_ACTPriotization
	 */
	public ListOfServiceRequest_Lightweight_ACTPriotization getListOfServiceRequest_Lightweight_ACTPriotization() {
		return ListOfServiceRequest_Lightweight_ACTPriotization;
	}

	/**
	 * @param listOfServiceRequest_Lightweight_ACTPriotization
	 *            the listOfServiceRequest_Lightweight_ACTPriotization to set
	 */
	public void setListOfServiceRequest_Lightweight_ACTPriotization(
			ListOfServiceRequest_Lightweight_ACTPriotization listOfServiceRequest_Lightweight_ACTPriotization) {
		ListOfServiceRequest_Lightweight_ACTPriotization = listOfServiceRequest_Lightweight_ACTPriotization;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServiceRequest_Lightweight [TicketDescription=" + TicketDescription + ", Owner=" + Owner + ", CallType="
				+ CallType + ", ClosedDate=" + ClosedDate + ", ResolutionCode=" + ResolutionCode + ", VOC=" + VOC
				+ ", TicketSource=" + TicketSource + ", Nature=" + Nature
				+ ", ListOfServiceRequest_Lightweight_ACTPriotization="
				+ ListOfServiceRequest_Lightweight_ACTPriotization + ", Priority=" + Priority + ", Status=" + Status
				+ ", SRNumber=" + SRNumber + ", ETR=" + ETR + ", Category=" + Category + ", CustomerNumber="
				+ CustomerNumber + ", ResolutionDescription=" + ResolutionDescription + ", SubResolutionCode="
				+ SubResolutionCode + "]";
	}

}
