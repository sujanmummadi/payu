
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ActListMgmtProspectiveContactBc {
	
	 private String CXPort;

	    private String RouterModelCode;

	    private String MilestoneStage;

	    private String ProspectAssignedTo;

	    private String ConnectionType;

	    private String CustomerProfile;

	    private String ACTCAFSerialNumber;

	    private String FXPort;

	    private String HowdidgettoknowaboutACT;

	    private String ProductType;

	    private String RefereeAccountNumber;

	    private String PlanCode;

	    private String RefereeMobileNumber;

	    private String WifiRequired;

	    private String MileStoneStatus;

	    private String ACTGxRequired;

	    private ListOfActCutAddressThinBc ListOfActCutAddressThinBc;

	    private String ActionName;

	    private String CustomerType;

	    private ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL;

	    private String SubAttribute;

	    private String ProspectRejectionReason;

	    private String SourceType;

	    private String CXIP;

	    private String FXName;

	    private String Notes;

	    private String ACTCxPermission;

	    private String Status;

	    private String PlanName;

	    private String ProspectNumber;

	    private String SchemeCode;

	    private String LineofBusiness;

	    private String ACTCurrentISP;

	    private String SubSource;

	    public String getCXPort ()
	    {
	        return CXPort;
	    }

	    public void setCXPort (String CXPort)
	    {
	        this.CXPort = CXPort;
	    }

	    public String getRouterModelCode ()
	    {
	        return RouterModelCode;
	    }

	    public void setRouterModelCode (String RouterModelCode)
	    {
	        this.RouterModelCode = RouterModelCode;
	    }

	    public String getMilestoneStage ()
	    {
	        return MilestoneStage;
	    }

	    public void setMilestoneStage (String MilestoneStage)
	    {
	        this.MilestoneStage = MilestoneStage;
	    }

	    public String getProspectAssignedTo ()
	    {
	        return ProspectAssignedTo;
	    }

	    public void setProspectAssignedTo (String ProspectAssignedTo)
	    {
	        this.ProspectAssignedTo = ProspectAssignedTo;
	    }

	    public String getConnectionType ()
	    {
	        return ConnectionType;
	    }

	    public void setConnectionType (String ConnectionType)
	    {
	        this.ConnectionType = ConnectionType;
	    }

	    public String getCustomerProfile ()
	    {
	        return CustomerProfile;
	    }

	    public void setCustomerProfile (String CustomerProfile)
	    {
	        this.CustomerProfile = CustomerProfile;
	    }

	    public String getACTCAFSerialNumber ()
	    {
	        return ACTCAFSerialNumber;
	    }

	    public void setACTCAFSerialNumber (String ACTCAFSerialNumber)
	    {
	        this.ACTCAFSerialNumber = ACTCAFSerialNumber;
	    }

	    public String getFXPort ()
	    {
	        return FXPort;
	    }

	    public void setFXPort (String FXPort)
	    {
	        this.FXPort = FXPort;
	    }

	    public String getHowdidgettoknowaboutACT ()
	    {
	        return HowdidgettoknowaboutACT;
	    }

	    public void setHowdidgettoknowaboutACT (String HowdidgettoknowaboutACT)
	    {
	        this.HowdidgettoknowaboutACT = HowdidgettoknowaboutACT;
	    }

	    public String getProductType ()
	    {
	        return ProductType;
	    }

	    public void setProductType (String ProductType)
	    {
	        this.ProductType = ProductType;
	    }

	    public String getRefereeAccountNumber ()
	    {
	        return RefereeAccountNumber;
	    }

	    public void setRefereeAccountNumber (String RefereeAccountNumber)
	    {
	        this.RefereeAccountNumber = RefereeAccountNumber;
	    }

	    public String getPlanCode ()
	    {
	        return PlanCode;
	    }

	    public void setPlanCode (String PlanCode)
	    {
	        this.PlanCode = PlanCode;
	    }

	    public String getRefereeMobileNumber ()
	    {
	        return RefereeMobileNumber;
	    }

	    public void setRefereeMobileNumber (String RefereeMobileNumber)
	    {
	        this.RefereeMobileNumber = RefereeMobileNumber;
	    }

	    public String getWifiRequired ()
	    {
	        return WifiRequired;
	    }

	    public void setWifiRequired (String WifiRequired)
	    {
	        this.WifiRequired = WifiRequired;
	    }

	    public String getMileStoneStatus ()
	    {
	        return MileStoneStatus;
	    }

	    public void setMileStoneStatus (String MileStoneStatus)
	    {
	        this.MileStoneStatus = MileStoneStatus;
	    }

	    public String getACTGxRequired ()
	    {
	        return ACTGxRequired;
	    }

	    public void setACTGxRequired (String ACTGxRequired)
	    {
	        this.ACTGxRequired = ACTGxRequired;
	    }

	    public ListOfActCutAddressThinBc getListOfActCutAddressThinBc ()
	    {
	        return ListOfActCutAddressThinBc;
	    }

	    public void setListOfActCutAddressThinBc (ListOfActCutAddressThinBc ListOfActCutAddressThinBc)
	    {
	        this.ListOfActCutAddressThinBc = ListOfActCutAddressThinBc;
	    }

	    public String getActionName ()
	    {
	        return ActionName;
	    }

	    public void setActionName (String ActionName)
	    {
	        this.ActionName = ActionName;
	    }

	    public String getCustomerType ()
	    {
	        return CustomerType;
	    }

	    public void setCustomerType (String CustomerType)
	    {
	        this.CustomerType = CustomerType;
	    }

	    public ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL getListOfACTListMgmtProspectiveContactBC_ACTContactListMVL ()
	    {
	        return ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL;
	    }

	    public void setListOfACTListMgmtProspectiveContactBC_ACTContactListMVL (ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL)
	    {
	        this.ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL = ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL;
	    }

	    public String getSubAttribute ()
	    {
	        return SubAttribute;
	    }

	    public void setSubAttribute (String SubAttribute)
	    {
	        this.SubAttribute = SubAttribute;
	    }

	    public String getProspectRejectionReason ()
	    {
	        return ProspectRejectionReason;
	    }

	    public void setProspectRejectionReason (String ProspectRejectionReason)
	    {
	        this.ProspectRejectionReason = ProspectRejectionReason;
	    }

	    public String getSourceType ()
	    {
	        return SourceType;
	    }

	    public void setSourceType (String SourceType)
	    {
	        this.SourceType = SourceType;
	    }

	    public String getCXIP ()
	    {
	        return CXIP;
	    }

	    public void setCXIP (String CXIP)
	    {
	        this.CXIP = CXIP;
	    }

	    public String getFXName ()
	    {
	        return FXName;
	    }

	    public void setFXName (String FXName)
	    {
	        this.FXName = FXName;
	    }

	    public String getNotes ()
	    {
	        return Notes;
	    }

	    public void setNotes (String Notes)
	    {
	        this.Notes = Notes;
	    }

	    public String getACTCxPermission ()
	    {
	        return ACTCxPermission;
	    }

	    public void setACTCxPermission (String ACTCxPermission)
	    {
	        this.ACTCxPermission = ACTCxPermission;
	    }

	    public String getStatus ()
	    {
	        return Status;
	    }

	    public void setStatus (String Status)
	    {
	        this.Status = Status;
	    }

	    public String getPlanName ()
	    {
	        return PlanName;
	    }

	    public void setPlanName (String PlanName)
	    {
	        this.PlanName = PlanName;
	    }

	    public String getProspectNumber ()
	    {
	        return ProspectNumber;
	    }

	    public void setProspectNumber (String ProspectNumber)
	    {
	        this.ProspectNumber = ProspectNumber;
	    }

	    public String getSchemeCode ()
	    {
	        return SchemeCode;
	    }

	    public void setSchemeCode (String SchemeCode)
	    {
	        this.SchemeCode = SchemeCode;
	    }

	    public String getLineofBusiness ()
	    {
	        return LineofBusiness;
	    }

	    public void setLineofBusiness (String LineofBusiness)
	    {
	        this.LineofBusiness = LineofBusiness;
	    }

	    public String getACTCurrentISP ()
	    {
	        return ACTCurrentISP;
	    }

	    public void setACTCurrentISP (String ACTCurrentISP)
	    {
	        this.ACTCurrentISP = ACTCurrentISP;
	    }

	    public String getSubSource ()
	    {
	        return SubSource;
	    }

	    public void setSubSource (String SubSource)
	    {
	        this.SubSource = SubSource;
	    }

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ActListMgmtProspectiveContactBc [CXPort=" + CXPort + ", RouterModelCode=" + RouterModelCode
					+ ", MilestoneStage=" + MilestoneStage + ", ProspectAssignedTo=" + ProspectAssignedTo
					+ ", ConnectionType=" + ConnectionType + ", CustomerProfile=" + CustomerProfile
					+ ", ACTCAFSerialNumber=" + ACTCAFSerialNumber + ", FXPort=" + FXPort + ", HowdidgettoknowaboutACT="
					+ HowdidgettoknowaboutACT + ", ProductType=" + ProductType + ", RefereeAccountNumber="
					+ RefereeAccountNumber + ", PlanCode=" + PlanCode + ", RefereeMobileNumber=" + RefereeMobileNumber
					+ ", WifiRequired=" + WifiRequired + ", MileStoneStatus=" + MileStoneStatus + ", ACTGxRequired="
					+ ACTGxRequired + ", ListOfActCutAddressThinBc=" + ListOfActCutAddressThinBc + ", ActionName="
					+ ActionName + ", CustomerType=" + CustomerType
					+ ", ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL="
					+ ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL + ", SubAttribute=" + SubAttribute
					+ ", ProspectRejectionReason=" + ProspectRejectionReason + ", SourceType=" + SourceType + ", CXIP="
					+ CXIP + ", FXName=" + FXName + ", Notes=" + Notes + ", ACTCxPermission=" + ACTCxPermission
					+ ", Status=" + Status + ", PlanName=" + PlanName + ", ProspectNumber=" + ProspectNumber
					+ ", SchemeCode=" + SchemeCode + ", LineofBusiness=" + LineofBusiness + ", ACTCurrentISP="
					+ ACTCurrentISP + ", SubSource=" + SubSource + "]";
		}

	    
}
