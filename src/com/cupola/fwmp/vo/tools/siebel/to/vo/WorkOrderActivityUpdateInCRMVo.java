/**
* @Author aditya  10-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.CreateUpdateWorkOrderRequestInSiebel;

/**
 * @Author aditya 10-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

@Document(collection = "WorkOrderActivityUpdateInCRMVo")
public class WorkOrderActivityUpdateInCRMVo {

	@Id
	private Long workOrderActivityUpdateInCRMVoId;
	private long customerId;
	private long ticketId;
	private String prospectNumber;
	private String workOrderNumber;
	private long cityId;
	private long branchId;
	private String status;
	private String transactionId;
	private String activityStage;
	private CreateUpdateWorkOrderRequestInSiebel createUpdateWorkOrderRequestInSiebel;

	/**
	 * @return the workOrderActivityUpdateInCRMVo
	 */
	public Long getWorkOrderActivityUpdateInCRMVoId() {
		return workOrderActivityUpdateInCRMVoId;
	}

	/**
	 * @param workOrderActivityUpdateInCRMVo
	 *            the workOrderActivityUpdateInCRMVo to set
	 */
	public void setWorkOrderActivityUpdateInCRMVoId(Long workOrderActivityUpdateInCRMVo) {
		this.workOrderActivityUpdateInCRMVoId = workOrderActivityUpdateInCRMVo;
	}

	/**
	 * @return the customerId
	 */
	public long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the ticketId
	 */
	public long getTicketId() {
		return ticketId;
	}

	/**
	 * @param ticketId
	 *            the ticketId to set
	 */
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return prospectNumber;
	}

	/**
	 * @param prospectNumber
	 *            the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}

	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	/**
	 * @param workOrderNumber
	 *            the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the branchId
	 */
	public long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId
	 *            the branchId to set
	 */
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the createUpdateWorkOrderRequestInSiebel
	 */
	public CreateUpdateWorkOrderRequestInSiebel getCreateUpdateWorkOrderRequestInSiebel() {
		return createUpdateWorkOrderRequestInSiebel;
	}

	/**
	 * @param createUpdateWorkOrderRequestInSiebel
	 *            the createUpdateWorkOrderRequestInSiebel to set
	 */
	public void setCreateUpdateWorkOrderRequestInSiebel(
			CreateUpdateWorkOrderRequestInSiebel createUpdateWorkOrderRequestInSiebel) {
		this.createUpdateWorkOrderRequestInSiebel = createUpdateWorkOrderRequestInSiebel;
	}
	

	/**
	 * @return the activityStage
	 */
	public String getActivityStage() {
		return activityStage;
	}

	/**
	 * @param activityStage the activityStage to set
	 */
	public void setActivityStage(String activityStage) {
		this.activityStage = activityStage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WorkOrderActivityUpdateInCRMVo [workOrderActivityUpdateInCRMVoID=" + workOrderActivityUpdateInCRMVoId
				+ ", customerId=" + customerId + ", ticketId=" + ticketId + ", prospectNumber=" + prospectNumber
				+ ", workOrderNumber=" + workOrderNumber + ", cityId=" + cityId + ", branchId=" + branchId + ", status="
				+ status + ", transactionId=" + transactionId + ", createUpdateWorkOrderRequestInSiebel="
				+ createUpdateWorkOrderRequestInSiebel + "]";
	}

}
