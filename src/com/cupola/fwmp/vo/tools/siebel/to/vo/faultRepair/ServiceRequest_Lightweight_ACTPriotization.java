 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair;

 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ServiceRequest_Lightweight_ACTPriotization {
	
	private String PrioritySource;

    public String getPrioritySource ()
    {
        return PrioritySource;
    }

    public void setPrioritySource (String PrioritySource)
    {
        this.PrioritySource = PrioritySource;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServiceRequest_Lightweight_ACTPriotization [PrioritySource=" + PrioritySource + "]";
	}

   
}
