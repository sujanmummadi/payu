/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL {

	private List<ACTListMgmtProspectiveContactBC_ACTContactListMVL> ACTListMgmtProspectiveContactBC_ACTContactListMVL;

	/**
	 * @return the aCTListMgmtProspectiveContactBC_ACTContactListMVL
	 */
	public List<ACTListMgmtProspectiveContactBC_ACTContactListMVL> getACTListMgmtProspectiveContactBC_ACTContactListMVL() {
		return ACTListMgmtProspectiveContactBC_ACTContactListMVL;
	}

	/**
	 * @param aCTListMgmtProspectiveContactBC_ACTContactListMVL
	 *            the aCTListMgmtProspectiveContactBC_ACTContactListMVL to set
	 */
	public void setACTListMgmtProspectiveContactBC_ACTContactListMVL(
			List<ACTListMgmtProspectiveContactBC_ACTContactListMVL> aCTListMgmtProspectiveContactBC_ACTContactListMVL) {
		ACTListMgmtProspectiveContactBC_ACTContactListMVL = aCTListMgmtProspectiveContactBC_ACTContactListMVL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL [ACTListMgmtProspectiveContactBC_ACTContactListMVL="
				+ ACTListMgmtProspectiveContactBC_ACTContactListMVL + "]";
	}

}
