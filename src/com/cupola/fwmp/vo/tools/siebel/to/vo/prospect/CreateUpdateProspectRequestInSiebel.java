/**
* @Author aditya  10-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class CreateUpdateProspectRequestInSiebel {

	 private CreateUpdateProspect_Input CreateUpdateProspect_Input;

	    public CreateUpdateProspect_Input getCreateUpdateProspect_Input ()
	    {
	        return CreateUpdateProspect_Input;
	    }

	    public void setCreateUpdateProspect_Input (CreateUpdateProspect_Input CreateUpdateProspect_Input)
	    {
	        this.CreateUpdateProspect_Input = CreateUpdateProspect_Input;
	    }

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "CreateUpdateProspectRequestInSiebel [CreateUpdateProspect_Input=" + CreateUpdateProspect_Input
					+ "]";
		}

	   

}
