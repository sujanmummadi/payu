/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

/**
 * @Author pawan Oct 11, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved. All the
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ActCutAddressThinBc {

	private String Others;

	private String Area;

	private String SubArea;

	private String ApartmentName;

	private String Country;

	private String City;

	private String ProjectType;

	private String OperationalEntity;

	private String PostalCode;

	private String State;

	private String Address2;

	private String Address1;

	private String Latitude;

	private String AddressType;

	private String Longitude;

	private String LandMark;

	public String getOthers() {
		return Others;
	}

	public void setOthers(String Others) {
		this.Others = Others;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String Area) {
		this.Area = Area;
	}

	public String getSubArea() {
		return SubArea;
	}

	public void setSubArea(String SubArea) {
		this.SubArea = SubArea;
	}

	public String getApartmentName() {
		return ApartmentName;
	}

	public void setApartmentName(String ApartmentName) {
		this.ApartmentName = ApartmentName;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String Country) {
		this.Country = Country;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String City) {
		this.City = City;
	}

	public String getProjectType() {
		return ProjectType;
	}

	public void setProjectType(String ProjectType) {
		this.ProjectType = ProjectType;
	}

	public String getOperationalEntity() {
		return OperationalEntity;
	}

	public void setOperationalEntity(String OperationalEntity) {
		this.OperationalEntity = OperationalEntity;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String PostalCode) {
		this.PostalCode = PostalCode;
	}

	public String getState() {
		return State;
	}

	public void setState(String State) {
		this.State = State;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String Address2) {
		this.Address2 = Address2;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String Address1) {
		this.Address1 = Address1;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String Latitude) {
		this.Latitude = Latitude;
	}

	public String getAddressType() {
		return AddressType;
	}

	public void setAddressType(String AddressType) {
		this.AddressType = AddressType;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String Longitude) {
		this.Longitude = Longitude;
	}

	public String getLandMark() {
		return LandMark;
	}

	public void setLandMark(String LandMark) {
		this.LandMark = LandMark;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActCutAddressThinBc [Others=" + Others + ", Area=" + Area + ", SubArea=" + SubArea + ", ApartmentName="
				+ ApartmentName + ", Country=" + Country + ", City=" + City + ", ProjectType=" + ProjectType
				+ ", OperationalEntity=" + OperationalEntity + ", PostalCode=" + PostalCode + ", State=" + State
				+ ", Address2=" + Address2 + ", Address1=" + Address1 + ", Latitude=" + Latitude + ", AddressType="
				+ AddressType + ", Longitude=" + Longitude + ", LandMark=" + LandMark + "]";
	}

	
}
