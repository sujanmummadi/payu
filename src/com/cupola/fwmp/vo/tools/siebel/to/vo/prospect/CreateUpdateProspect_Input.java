/**
* @Author kiran  Oct 12, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @Author kiran Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class CreateUpdateProspect_Input {

	@JsonProperty("ActDescription")
	private String ActDescription;
	
	@JsonProperty("Transation_spcId")
	private String Transation_spcId;
	
	@JsonProperty("PreferredCallDateTime")
	private String PreferredCallDateTime;
	
	@JsonProperty("ListOfActcreateprospectthinio")
	private ListOfActcreateprospectthinio ListOfActcreateprospectthinio;

	@JsonProperty("ActDescription")
	public String getActDescription() {
		return ActDescription;
	}

	@JsonProperty("ActDescription")
	public void setActDescription(String ActDescription) {
		this.ActDescription = ActDescription;
	}

	/**
	 * @return the transation_spcId
	 */
	@JsonProperty("Transation_spcId")
	public String getTransation_spcId() {
		return Transation_spcId;
	}

	/**
	 * @param transation_spcId
	 *            the transation_spcId to set
	 */
	@JsonProperty("Transation_spcId")
	public void setTransation_spcId(String transation_spcId) {
		Transation_spcId = transation_spcId;
	}

	/**
	 * @return the preferredCallDateTime
	 */
	@JsonProperty("PreferredCallDateTime")
	public String getPreferredCallDateTime() {
		return PreferredCallDateTime;
	}

	/**
	 * @param preferredCallDateTime
	 *            the preferredCallDateTime to set
	 */
	@JsonProperty("PreferredCallDateTime")
	public void setPreferredCallDateTime(String preferredCallDateTime) {
		PreferredCallDateTime = preferredCallDateTime;
	}

	/**
	 * @return the listOfActcreateprospectthinio
	 */
	@JsonProperty("ListOfActcreateprospectthinio")
	public ListOfActcreateprospectthinio getListOfActcreateprospectthinio() {
		return ListOfActcreateprospectthinio;
	}

	/**
	 * @param listOfActcreateprospectthinio
	 *            the listOfActcreateprospectthinio to set
	 */
	@JsonProperty("ListOfActcreateprospectthinio")
	public void setListOfActcreateprospectthinio(ListOfActcreateprospectthinio listOfActcreateprospectthinio) {
		ListOfActcreateprospectthinio = listOfActcreateprospectthinio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateProspect_Input [ActDescription=" + ActDescription + ", Transation_spcId=" + Transation_spcId
				+ ", PreferredCallDateTime=" + PreferredCallDateTime + ", ListOfActcreateprospectthinio="
				+ ListOfActcreateprospectthinio + "]";
	}

}
