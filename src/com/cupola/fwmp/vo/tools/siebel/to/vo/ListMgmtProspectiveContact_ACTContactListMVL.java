/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ListMgmtProspectiveContact_ACTContactListMVL {

	private String ContactName;

	private String AlternateMobileNumber;

	private String EmailAddress2;

	private String CompanyName;

	private String LastName;

	private String Title;

	private String Nationality;

	private String CellularPhone;

	private String AadharNumber;

	private String MiddleName;

	private String _IsPrimaryMVG;

	private String UINType;

	private String HOMEPHNUM;

	private String EmailAddress;

	private String FirstName;

	private String UIN;

	private String ContactIntegrationId;

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return ContactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		ContactName = contactName;
	}

	/**
	 * @return the alternateMobileNumber
	 */
	public String getAlternateMobileNumber() {
		return AlternateMobileNumber;
	}

	/**
	 * @param alternateMobileNumber the alternateMobileNumber to set
	 */
	public void setAlternateMobileNumber(String alternateMobileNumber) {
		AlternateMobileNumber = alternateMobileNumber;
	}

	/**
	 * @return the emailAddress2
	 */
	public String getEmailAddress2() {
		return EmailAddress2;
	}

	/**
	 * @param emailAddress2 the emailAddress2 to set
	 */
	public void setEmailAddress2(String emailAddress2) {
		EmailAddress2 = emailAddress2;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return CompanyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return LastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		LastName = lastName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return Title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		Title = title;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return Nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		Nationality = nationality;
	}

	/**
	 * @return the cellularPhone
	 */
	public String getCellularPhone() {
		return CellularPhone;
	}

	/**
	 * @param cellularPhone the cellularPhone to set
	 */
	public void setCellularPhone(String cellularPhone) {
		CellularPhone = cellularPhone;
	}

	/**
	 * @return the aadharNumber
	 */
	public String getAadharNumber() {
		return AadharNumber;
	}

	/**
	 * @param aadharNumber the aadharNumber to set
	 */
	public void setAadharNumber(String aadharNumber) {
		AadharNumber = aadharNumber;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return MiddleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}

	/**
	 * @return the _IsPrimaryMVG
	 */
	public String get_IsPrimaryMVG() {
		return _IsPrimaryMVG;
	}

	/**
	 * @param _IsPrimaryMVG the _IsPrimaryMVG to set
	 */
	public void set_IsPrimaryMVG(String _IsPrimaryMVG) {
		this._IsPrimaryMVG = _IsPrimaryMVG;
	}

	/**
	 * @return the uINType
	 */
	public String getUINType() {
		return UINType;
	}

	/**
	 * @param uINType the uINType to set
	 */
	public void setUINType(String uINType) {
		UINType = uINType;
	}

	/**
	 * @return the hOMEPHNUM
	 */
	public String getHOMEPHNUM() {
		return HOMEPHNUM;
	}

	/**
	 * @param hOMEPHNUM the hOMEPHNUM to set
	 */
	public void setHOMEPHNUM(String hOMEPHNUM) {
		HOMEPHNUM = hOMEPHNUM;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return EmailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return FirstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	/**
	 * @return the uIN
	 */
	public String getUIN() {
		return UIN;
	}

	/**
	 * @param uIN the uIN to set
	 */
	public void setUIN(String uIN) {
		UIN = uIN;
	}

	/**
	 * @return the contactIntegrationId
	 */
	public String getContactIntegrationId() {
		return ContactIntegrationId;
	}

	/**
	 * @param contactIntegrationId the contactIntegrationId to set
	 */
	public void setContactIntegrationId(String contactIntegrationId) {
		ContactIntegrationId = contactIntegrationId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListMgmtProspectiveContact_ACTContactListMVL [ContactName=" + ContactName + ", AlternateMobileNumber="
				+ AlternateMobileNumber + ", EmailAddress2=" + EmailAddress2 + ", CompanyName=" + CompanyName
				+ ", LastName=" + LastName + ", Title=" + Title + ", Nationality=" + Nationality + ", CellularPhone="
				+ CellularPhone + ", AadharNumber=" + AadharNumber + ", MiddleName=" + MiddleName + ", _IsPrimaryMVG="
				+ _IsPrimaryMVG + ", UINType=" + UINType + ", HOMEPHNUM=" + HOMEPHNUM + ", EmailAddress=" + EmailAddress
				+ ", FirstName=" + FirstName + ", UIN=" + UIN + ", ContactIntegrationId=" + ContactIntegrationId + "]";
	}

	

}
