/**
* @Author aditya  10-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.CreateUpdateFaultRepairRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;

/**
 * @Author aditya 10-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

@Document(collection = "FaultRepairActivityUpdateInCRMVo")
public class FaultRepairActivityUpdateInCRMVo {

	@Id
	private Long faultRepairActivityUpdateInCRMVoId;
	private long customerId;
	private long ticketId;
	private String prospectNumber;
	private String workOrderNumber;
	private long cityId;
	private long branchId;
	private String status;
	private String transactionId;
	private CreateUpdateFaultRepairRequestInSiebel createUpdateFaultRepairRequestInSiebel;

	/**
	 * @return the faultRepairActivityUpdateInCRMVo
	 */
	public Long getFaultRepairActivityUpdateInCRMVoId() {
		return faultRepairActivityUpdateInCRMVoId;
	}

	/**
	 * @param faultRepairActivityUpdateInCRMVo
	 *            the faultRepairActivityUpdateInCRMVo to set
	 */
	public void setFaultRepairActivityUpdateInCRMVoId(Long faultRepairActivityUpdateInCRMVo) {
		this.faultRepairActivityUpdateInCRMVoId = faultRepairActivityUpdateInCRMVo;
	}

	/**
	 * @return the customerId
	 */
	public long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the ticketId
	 */
	public long getTicketId() {
		return ticketId;
	}

	/**
	 * @param ticketId
	 *            the ticketId to set
	 */
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return prospectNumber;
	}

	/**
	 * @param prospectNumber
	 *            the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}

	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	/**
	 * @param workOrderNumber
	 *            the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the branchId
	 */
	public long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId
	 *            the branchId to set
	 */
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the createUpdateFaultRepairRequestInSiebel
	 */
	public CreateUpdateFaultRepairRequestInSiebel getCreateUpdateFaultRepairRequestInSiebel() {
		return createUpdateFaultRepairRequestInSiebel;
	}

	/**
	 * @param createUpdateFaultRepairRequestInSiebel
	 *            the createUpdateFaultRepairRequestInSiebel to set
	 */
	public void setCreateUpdateFaultRepairRequestInSiebel(
			CreateUpdateFaultRepairRequestInSiebel createUpdateFaultRepairRequestInSiebel) {
		this.createUpdateFaultRepairRequestInSiebel = createUpdateFaultRepairRequestInSiebel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FaultRepairActivityUpdateInCRMVo [faultRepairActivityUpdateInCRMVoId =" + faultRepairActivityUpdateInCRMVoId
				+ ", customerId=" + customerId + ", ticketId=" + ticketId + ", prospectNumber=" + prospectNumber
				+ ", workOrderNumber=" + workOrderNumber + ", cityId=" + cityId + ", branchId=" + branchId + ", status="
				+ status + ", transactionId=" + transactionId + ", createUpdateFaultRepairRequestInSiebel="
				+ createUpdateFaultRepairRequestInSiebel + "]";
	}
}
