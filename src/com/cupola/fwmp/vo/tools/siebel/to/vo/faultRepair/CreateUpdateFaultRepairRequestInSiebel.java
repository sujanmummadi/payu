/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class CreateUpdateFaultRepairRequestInSiebel {
	
	private FWMPSRUpsert_Input FWMPSRUpsert_Input;

    public FWMPSRUpsert_Input getFWMPSRUpsert_Input ()
    {
        return FWMPSRUpsert_Input;
    }

    public void setFWMPSRUpsert_Input (FWMPSRUpsert_Input FWMPSRUpsert_Input)
    {
        this.FWMPSRUpsert_Input = FWMPSRUpsert_Input;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateFaultRepairRequestInSiebel [FWMPSRUpsert_Input=" + FWMPSRUpsert_Input + "]";
	}

    
}
