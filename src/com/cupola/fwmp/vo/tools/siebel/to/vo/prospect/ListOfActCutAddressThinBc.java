/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.prospect;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ListOfActCutAddressThinBc {

	private List<ActCutAddressThinBc> ActCutAddressThinBc;

	/**
	 * @return the actCutAddressThinBc
	 */
	public List<ActCutAddressThinBc> getActCutAddressThinBc() {
		return ActCutAddressThinBc;
	}

	/**
	 * @param actCutAddressThinBc
	 *            the actCutAddressThinBc to set
	 */
	public void setActCutAddressThinBc(List<ActCutAddressThinBc> actCutAddressThinBc) {
		ActCutAddressThinBc = actCutAddressThinBc;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfActCutAddressThinBc [ActCutAddressThinBc=" + ActCutAddressThinBc + "]";
	}

	

}
