/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ComWorkOrder_Orders_ACTPriotization {

	private String PrioritySource;

	public String getPrioritySource() {
		return PrioritySource;
	}

	public void setPrioritySource(String PrioritySource) {
		this.PrioritySource = PrioritySource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ComWorkOrder_Orders_ACTPriotization [PrioritySource=" + PrioritySource + "]";
	}

}
