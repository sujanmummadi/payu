 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair;

import java.util.List;

/**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ListOfServiceRequest_Lightweight_ACTPriotization {
	
	private ServiceRequest_Lightweight_ACTPriotization ServiceRequest_Lightweight_ACTPriotization;

	/**
	 * @return the serviceRequest_Lightweight_ACTPriotization
	 */
	public ServiceRequest_Lightweight_ACTPriotization getServiceRequest_Lightweight_ACTPriotization() {
		return ServiceRequest_Lightweight_ACTPriotization;
	}

	/**
	 * @param serviceRequest_Lightweight_ACTPriotization the serviceRequest_Lightweight_ACTPriotization to set
	 */
	public void setServiceRequest_Lightweight_ACTPriotization(
			ServiceRequest_Lightweight_ACTPriotization serviceRequest_Lightweight_ACTPriotization) {
		ServiceRequest_Lightweight_ACTPriotization = serviceRequest_Lightweight_ACTPriotization;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfServiceRequest_Lightweight_ACTPriotization [ServiceRequest_Lightweight_ACTPriotization="
				+ ServiceRequest_Lightweight_ACTPriotization + "]";
	}

	
	
   

}
