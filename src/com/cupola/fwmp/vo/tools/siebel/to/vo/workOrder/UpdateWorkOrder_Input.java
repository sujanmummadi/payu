/**
* @Author kiran  Oct 12, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

/**
 * @Author kiran Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class UpdateWorkOrder_Input {

	private String TransactionID;

	private ListOfComworkorderio ListOfComworkorderio;

	public String getTransactionID() {
		return TransactionID;
	}

	public void setTransactionID(String TransactionID) {
		this.TransactionID = TransactionID;
	}

	public ListOfComworkorderio getListOfComworkorderio() {
		return ListOfComworkorderio;
	}

	public void setListOfComworkorderio(ListOfComworkorderio ListOfComworkorderio) {
		this.ListOfComworkorderio = ListOfComworkorderio;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UpdateWorkOrder_Input [TransactionID=" + TransactionID + ", ListOfComworkorderio="
				+ ListOfComworkorderio + "]";
	}

	
}
