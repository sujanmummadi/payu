 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair;

import java.util.List;

/**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FWMPSRUpsert_Input {
	
	 private String Transaction_spcID;

	    private  ListOfFwmpsrthinio ListOfFwmpsrthinio;

	    public String getTransaction_spcID ()
	    {
	        return Transaction_spcID;
	    }

	    /**
		 * @return the listOfFwmpsrthinio
		 */
		public ListOfFwmpsrthinio getListOfFwmpsrthinio() {
			return ListOfFwmpsrthinio;
		}

		/**
		 * @param listOfFwmpsrthinio the listOfFwmpsrthinio to set
		 */
		public void setListOfFwmpsrthinio(ListOfFwmpsrthinio listOfFwmpsrthinio) {
			ListOfFwmpsrthinio = listOfFwmpsrthinio;
		}

		public void setTransaction_spcID (String Transaction_spcID)
	    {
	        this.Transaction_spcID = Transaction_spcID;
	    }

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "FWMPSRUpsert_Input [Transaction_spcID=" + Transaction_spcID + ", ListOfFwmpsrthinio="
					+ ListOfFwmpsrthinio + "]";
		}

	  

	  
}
