/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CreateProspectAPI_Input {

	private String ActDescription;

	private String PreferredCallDateTime;

	private ListOfActCreateProspectIo ListOfActCreateProspectIo;

	/**
	 * @return the actDescription
	 */
	public String getActDescription() {
		return ActDescription;
	}

	/**
	 * @param actDescription the actDescription to set
	 */
	public void setActDescription(String actDescription) {
		ActDescription = actDescription;
	}

	/**
	 * @return the preferredCallDateTime
	 */
	public String getPreferredCallDateTime() {
		return PreferredCallDateTime;
	}

	/**
	 * @param preferredCallDateTime the preferredCallDateTime to set
	 */
	public void setPreferredCallDateTime(String preferredCallDateTime) {
		PreferredCallDateTime = preferredCallDateTime;
	}

	/**
	 * @return the listOfActCreateProspectIo
	 */
	public ListOfActCreateProspectIo getListOfActCreateProspectIo() {
		return ListOfActCreateProspectIo;
	}

	/**
	 * @param listOfActCreateProspectIo the listOfActCreateProspectIo to set
	 */
	public void setListOfActCreateProspectIo(ListOfActCreateProspectIo listOfActCreateProspectIo) {
		ListOfActCreateProspectIo = listOfActCreateProspectIo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateProspectAPI_Input [ActDescription=" + ActDescription + ", PreferredCallDateTime="
				+ PreferredCallDateTime + ", ListOfActCreateProspectIo=" + ListOfActCreateProspectIo + "]";
	}
	
	

	

}
