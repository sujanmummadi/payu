/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ListOfAction {

	private List<Action> Action;

	/**
	 * @return the action
	 */
	public List<Action> getAction() {
		return Action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(List<Action> action) {
		Action = action;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfAction [Action=" + Action + "]";
	}

}
