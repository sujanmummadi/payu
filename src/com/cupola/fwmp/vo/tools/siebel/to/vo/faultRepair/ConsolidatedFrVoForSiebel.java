/**
* @Author kiran  Oct 12, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair;

/**
 * @Author kiran Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ConsolidatedFrVoForSiebel {

	private String Transaction_spcID;

	private String PrioritySource;

	private String TicketDescription;

	private String Owner;

	private String CallType;

	private String ClosedDate;

	private String ResolutionCode;

	private String VOC;

	private String TicketSource;

	private String Nature;

	private String Priority;

	private String Status;

	private String SRNumber;

	private String ETR;

	private String Category;

	private String CustomerNumber;

	private String ResolutionDescription;

	private String SubResolutionCode;

	/**
	 * @return the transaction_spcID
	 */
	public String getTransaction_spcID() {
		return Transaction_spcID;
	}

	/**
	 * @param transaction_spcID the transaction_spcID to set
	 */
	public void setTransaction_spcID(String transaction_spcID) {
		Transaction_spcID = transaction_spcID;
	}

	/**
	 * @return the prioritySource
	 */
	public String getPrioritySource() {
		return PrioritySource;
	}

	/**
	 * @param prioritySource the prioritySource to set
	 */
	public void setPrioritySource(String prioritySource) {
		PrioritySource = prioritySource;
	}

	/**
	 * @return the ticketDescription
	 */
	public String getTicketDescription() {
		return TicketDescription;
	}

	/**
	 * @param ticketDescription the ticketDescription to set
	 */
	public void setTicketDescription(String ticketDescription) {
		TicketDescription = ticketDescription;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return Owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		Owner = owner;
	}

	/**
	 * @return the callType
	 */
	public String getCallType() {
		return CallType;
	}

	/**
	 * @param callType the callType to set
	 */
	public void setCallType(String callType) {
		CallType = callType;
	}

	/**
	 * @return the closedDate
	 */
	public String getClosedDate() {
		return ClosedDate;
	}

	/**
	 * @param closedDate the closedDate to set
	 */
	public void setClosedDate(String closedDate) {
		ClosedDate = closedDate;
	}

	/**
	 * @return the resolutionCode
	 */
	public String getResolutionCode() {
		return ResolutionCode;
	}

	/**
	 * @param resolutionCode the resolutionCode to set
	 */
	public void setResolutionCode(String resolutionCode) {
		ResolutionCode = resolutionCode;
	}

	/**
	 * @return the vOC
	 */
	public String getVOC() {
		return VOC;
	}

	/**
	 * @param vOC the vOC to set
	 */
	public void setVOC(String vOC) {
		VOC = vOC;
	}

	/**
	 * @return the ticketSource
	 */
	public String getTicketSource() {
		return TicketSource;
	}

	/**
	 * @param ticketSource the ticketSource to set
	 */
	public void setTicketSource(String ticketSource) {
		TicketSource = ticketSource;
	}

	/**
	 * @return the nature
	 */
	public String getNature() {
		return Nature;
	}

	/**
	 * @param nature the nature to set
	 */
	public void setNature(String nature) {
		Nature = nature;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return Priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		Priority = priority;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}

	/**
	 * @return the sRNumber
	 */
	public String getSRNumber() {
		return SRNumber;
	}

	/**
	 * @param sRNumber the sRNumber to set
	 */
	public void setSRNumber(String sRNumber) {
		SRNumber = sRNumber;
	}

	/**
	 * @return the eTR
	 */
	public String getETR() {
		return ETR;
	}

	/**
	 * @param eTR the eTR to set
	 */
	public void setETR(String eTR) {
		ETR = eTR;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return Category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		Category = category;
	}

	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber() {
		return CustomerNumber;
	}

	/**
	 * @param customerNumber the customerNumber to set
	 */
	public void setCustomerNumber(String customerNumber) {
		CustomerNumber = customerNumber;
	}

	/**
	 * @return the resolutionDescription
	 */
	public String getResolutionDescription() {
		return ResolutionDescription;
	}

	/**
	 * @param resolutionDescription the resolutionDescription to set
	 */
	public void setResolutionDescription(String resolutionDescription) {
		ResolutionDescription = resolutionDescription;
	}

	/**
	 * @return the subResolutionCode
	 */
	public String getSubResolutionCode() {
		return SubResolutionCode;
	}

	/**
	 * @param subResolutionCode the subResolutionCode to set
	 */
	public void setSubResolutionCode(String subResolutionCode) {
		SubResolutionCode = subResolutionCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConsolidatedFrVoForSiebel [Transaction_spcID=" + Transaction_spcID + ", PrioritySource="
				+ PrioritySource + ", TicketDescription=" + TicketDescription + ", Owner=" + Owner + ", CallType="
				+ CallType + ", ClosedDate=" + ClosedDate + ", ResolutionCode=" + ResolutionCode + ", VOC=" + VOC
				+ ", TicketSource=" + TicketSource + ", Nature=" + Nature + ", Priority=" + Priority + ", Status="
				+ Status + ", SRNumber=" + SRNumber + ", ETR=" + ETR + ", Category=" + Category + ", CustomerNumber="
				+ CustomerNumber + ", ResolutionDescription=" + ResolutionDescription + ", SubResolutionCode="
				+ SubResolutionCode + "]";
	}
	
	

}
