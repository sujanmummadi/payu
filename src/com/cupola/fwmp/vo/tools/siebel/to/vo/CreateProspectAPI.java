/**
* @Author kiran  Sep 18, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo;

/**
 * @Author kiran Sep 18, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CreateProspectAPI {

	private CreateProspectAPI_Input CreateProspectAPI_Input;

	/**
	 * @return the createProspectAPI_Input
	 */
	public CreateProspectAPI_Input getCreateProspectAPI_Input() {
		return CreateProspectAPI_Input;
	}

	/**
	 * @param createProspectAPI_Input the createProspectAPI_Input to set
	 */
	public void setCreateProspectAPI_Input(CreateProspectAPI_Input createProspectAPI_Input) {
		CreateProspectAPI_Input = createProspectAPI_Input;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateProspectAPI [CreateProspectAPI_Input=" + CreateProspectAPI_Input + "]";
	}
	
	

}
