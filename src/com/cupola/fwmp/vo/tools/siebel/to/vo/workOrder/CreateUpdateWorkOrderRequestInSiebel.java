/**
* @Author pawan  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class CreateUpdateWorkOrderRequestInSiebel {

	private UpdateWorkOrder_Input UpdateWorkOrder_Input;

	public UpdateWorkOrder_Input getUpdateWorkOrder_Input() {
		return UpdateWorkOrder_Input;
	}

	public void setUpdateWorkOrder_Input(UpdateWorkOrder_Input UpdateWorkOrder_Input) {
		this.UpdateWorkOrder_Input = UpdateWorkOrder_Input;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateWorkOrderRequestInSiebel [UpdateWorkOrder_Input=" + UpdateWorkOrder_Input + "]";
	}

	
}
