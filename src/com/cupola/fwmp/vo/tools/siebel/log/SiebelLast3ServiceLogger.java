/**
* @Author aditya  26-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.log;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting.Last3ServiceImpactingResponseFromSiebel;

/**
 * @Author aditya 26-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "SiebelLast3ServiceLogger")
public class SiebelLast3ServiceLogger {

	@Id
	private Long id;
	private String request;
	private String requestTime;
	private String response;
	private String responseTime;
	private String uri;
	private Last3ServiceImpactingResponseFromSiebel fromSiebel;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * @param request
	 *            the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}

	/**
	 * @return the requestTime
	 */
	public String getRequestTime() {
		return requestTime;
	}

	/**
	 * @param requestTime
	 *            the requestTime to set
	 */
	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

	/**
	 * @return the responseTime
	 */
	public String getResponseTime() {
		return responseTime;
	}

	/**
	 * @param responseTime
	 *            the responseTime to set
	 */
	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SiebelLast3ServiceLogger [id=" + id + ", request=" + request + ", requestTime=" + requestTime
				+ ", response=" + response + ", responseTime=" + responseTime + ", uri=" + uri + ", fromSiebel="
				+ fromSiebel + "]";
	}

	/**
	 * @return the fromSiebel
	 */
	public Last3ServiceImpactingResponseFromSiebel getFromSiebel() {
		return fromSiebel;
	}

	/**
	 * @param fromSiebel
	 *            the fromSiebel to set
	 */
	public void setFromSiebel(Last3ServiceImpactingResponseFromSiebel fromSiebel) {
		this.fromSiebel = fromSiebel;
	}

}