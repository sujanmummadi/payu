/**
* @Author aditya  09-Nov-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.response2siebel;

import java.util.Set;

/**
 * @Author aditya 09-Nov-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class WorkOrderResponse2siebel {
	private int statusCode;
	private String statusMessage;
	private String transactionId;
	private String workOrderNumber;
	private Object data;
	private Set<Long> queueData;

	/**
	 * @return the queueData
	 */
	public Set<Long> getQueueData() {
		return queueData;
	}

	/**
	 * @param queueData
	 *            the queueData to set
	 */
	public void setQueueData(Set<Long> queueData) {
		this.queueData = queueData;
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage
	 *            the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	/**
	 * @param workOrderNumber
	 *            the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WorkOrderResponse2siebel [statusCode=" + statusCode + ", statusMessage=" + statusMessage
				+ ", transactionId=" + transactionId + ", workOrderNumber=" + workOrderNumber + ", data=" + data
				+ ", queueData=" + queueData + "]";
	}

}
