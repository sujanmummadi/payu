/**
* @Author aditya  10-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author aditya 10-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved. All the
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */

public class CreateUpdateFaultRepairResponseFromSiebel {
	@JsonProperty("FWMPSRUpsert_Output")
	private FWMPSRUpsert_Output FWMPSRUpsert_Output;
	@JsonProperty("FWMPSRUpsert_Output")
    public FWMPSRUpsert_Output getFWMPSRUpsert_Output ()
    {
        return FWMPSRUpsert_Output;
    }

    @JsonProperty("FWMPSRUpsert_Output")
    public void setFWMPSRUpsert_Output (FWMPSRUpsert_Output FWMPSRUpsert_Output)
    {
        this.FWMPSRUpsert_Output = FWMPSRUpsert_Output;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateFaultRepairResponseFromSiebel [FWMPSRUpsert_Output=" + FWMPSRUpsert_Output + "]";
	}

   
}
