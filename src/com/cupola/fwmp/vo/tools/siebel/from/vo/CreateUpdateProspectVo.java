 /**
 * @Author aditya  29-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo;


import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.FeasibilityVO;
import com.cupola.fwmp.vo.tools.PaymentVO;
import com.cupola.fwmp.vo.tools.PriorityVO;
import com.cupola.fwmp.vo.tools.TariffVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
 /**
 * @Author aditya  29-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "CreateUpdateProspect")
public class CreateUpdateProspectVo 
{
	@JsonIgnore
	@Id
	private Long createUpdateProspectId;
	@JsonIgnore
	private String customerProfession;
	@JsonIgnore
	private FeasibilityVO feasibilityVO ;
	@JsonIgnore
	private PaymentVO paymentVO;
	@JsonIgnore
	private TariffVO tariffVO;
	@JsonIgnore
	private List<PriorityVO> priorityVOList;
	@JsonIgnore
	private String status;
	
	private String action;
	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private String preferredCallDate;
	private String prospectNumber;
	private String customerType;
	private String mobileNumber;
	private String alternativeMobileNo;
	private String officeNumber;
	private String emailId;
	private String customerProfile;
	private String alternativeEmailId;
	private CustomerAddressVO currentAddress;
	private CustomerAddressVO communicationAddress;
	private CustomerAddressVO permanentAddress;
	private String notes;
	private String cityName;
	private String branchName;
	private String areaName;
	private String subAreaName;
	private String enquiryType;
	private String source;
	
	@JsonProperty("How_To_Know_ACT")
	private String How_To_Know_ACT;
	
	private String subAttribute;
	
	@JsonProperty("CRPAccountNumber")
	private String CRPAccountNumber;
	
	@JsonProperty("CRPMobileNumber")
	private String CRPMobileNumber;
	
	private String existingISP;
	private String prospectType;
	private String reason;
	private String deDupFlag;
	private String assignedTo;
	
	@JsonProperty("TransactionId")
	private String TransactionId;
	
	/**
	 * @return the createUpdateProspectId
	 */
	public Long getCreateUpdateProspectId() {
		return createUpdateProspectId;
	}
	/**
	 * @param createUpdateProspectId the createUpdateProspectId to set
	 */
	public void setCreateUpdateProspectId(Long createUpdateProspectId) {
		this.createUpdateProspectId = createUpdateProspectId;
	}
	/**
	 * @return the customerProfession
	 */
	public String getCustomerProfession() {
		return customerProfession;
	}
	/**
	 * @param customerProfession the customerProfession to set
	 */
	public void setCustomerProfession(String customerProfession) {
		this.customerProfession = customerProfession;
	}
	/**
	 * @return the feasibilityVO
	 */
	public FeasibilityVO getFeasibilityVO() {
		return feasibilityVO;
	}
	/**
	 * @param feasibilityVO the feasibilityVO to set
	 */
	public void setFeasibilityVO(FeasibilityVO feasibilityVO) {
		this.feasibilityVO = feasibilityVO;
	}
	/**
	 * @return the paymentVO
	 */
	public PaymentVO getPaymentVO() {
		return paymentVO;
	}
	/**
	 * @param paymentVO the paymentVO to set
	 */
	public void setPaymentVO(PaymentVO paymentVO) {
		this.paymentVO = paymentVO;
	}
	/**
	 * @return the tariffVO
	 */
	public TariffVO getTariffVO() {
		return tariffVO;
	}
	/**
	 * @param tariffVO the tariffVO to set
	 */
	public void setTariffVO(TariffVO tariffVO) {
		this.tariffVO = tariffVO;
	}
	/**
	 * @return the priorityVOList
	 */
	public List<PriorityVO> getPriorityVOList() {
		return priorityVOList;
	}
	/**
	 * @param priorityVOList the priorityVOList to set
	 */
	public void setPriorityVOList(List<PriorityVO> priorityVOList) {
		this.priorityVOList = priorityVOList;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the preferredCallDate
	 */
	public String getPreferredCallDate() {
		return preferredCallDate;
	}
	/**
	 * @param preferredCallDate the preferredCallDate to set
	 */
	public void setPreferredCallDate(String preferredCallDate) {
		this.preferredCallDate = preferredCallDate;
	}
	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return prospectNumber;
	}
	/**
	 * @param prospectNumber the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}
	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * @return the alternativeMobileNo
	 */
	public String getAlternativeMobileNo() {
		return alternativeMobileNo;
	}
	/**
	 * @param alternativeMobileNo the alternativeMobileNo to set
	 */
	public void setAlternativeMobileNo(String alternativeMobileNo) {
		this.alternativeMobileNo = alternativeMobileNo;
	}
	/**
	 * @return the officeNumber
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}
	/**
	 * @param officeNumber the officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the customerProfile
	 */
	public String getCustomerProfile() {
		return customerProfile;
	}
	/**
	 * @param customerProfile the customerProfile to set
	 */
	public void setCustomerProfile(String customerProfile) {
		this.customerProfile = customerProfile;
	}
	/**
	 * @return the alternativeEmailId
	 */
	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}
	/**
	 * @param alternativeEmailId the alternativeEmailId to set
	 */
	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}
	/**
	 * @return the currentAddress
	 */
	public CustomerAddressVO getCurrentAddress() {
		return currentAddress;
	}
	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(CustomerAddressVO currentAddress) {
		this.currentAddress = currentAddress;
	}
	/**
	 * @return the communicationAddress
	 */
	public CustomerAddressVO getCommunicationAddress() {
		return communicationAddress;
	}
	/**
	 * @param communicationAddress the communicationAddress to set
	 */
	public void setCommunicationAddress(CustomerAddressVO communicationAddress) {
		this.communicationAddress = communicationAddress;
	}
	/**
	 * @return the permanentAddress
	 */
	public CustomerAddressVO getPermanentAddress() {
		return permanentAddress;
	}
	/**
	 * @param permanentAddress the permanentAddress to set
	 */
	public void setPermanentAddress(CustomerAddressVO permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}
	/**
	 * @param branchName the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}
	/**
	 * @param areaName the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	/**
	 * @return the subAreaName
	 */
	public String getSubAreaName() {
		return subAreaName;
	}
	/**
	 * @param subAreaName the subAreaName to set
	 */
	public void setSubAreaName(String subAreaName) {
		this.subAreaName = subAreaName;
	}
	/**
	 * @return the enquiryType
	 */
	public String getEnquiryType() {
		return enquiryType;
	}
	/**
	 * @param enquiryType the enquiryType to set
	 */
	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the how_To_Know_ACT
	 */
	public String getHow_To_Know_ACT() {
		return How_To_Know_ACT;
	}
	/**
	 * @param how_To_Know_ACT the how_To_Know_ACT to set
	 */
	public void setHow_To_Know_ACT(String How_To_Know_ACT) {
		this.How_To_Know_ACT = How_To_Know_ACT;
	}
	/**
	 * @return the subAttribute
	 */
	public String getSubAttribute() {
		return subAttribute;
	}
	/**
	 * @param subAttribute the subAttribute to set
	 */
	public void setSubAttribute(String subAttribute) {
		this.subAttribute = subAttribute;
	}
	/**
	 * @return the cRPAccountNumber
	 */
	public String getCRPAccountNumber() {
		return CRPAccountNumber;
	}
	/**
	 * @param cRPAccountNumber the cRPAccountNumber to set
	 */
	public void setCRPAccountNumber(String cRPAccountNumber) {
		CRPAccountNumber = cRPAccountNumber;
	}
	/**
	 * @return the cRPMobileNumber
	 */
	public String getCRPMobileNumber() {
		return CRPMobileNumber;
	}
	/**
	 * @param cRPMobileNumber the cRPMobileNumber to set
	 */
	public void setCRPMobileNumber(String cRPMobileNumber) {
		CRPMobileNumber = cRPMobileNumber;
	}
	/**
	 * @return the existingISP
	 */
	public String getExistingISP() {
		return existingISP;
	}
	/**
	 * @param existingISP the existingISP to set
	 */
	public void setExistingISP(String existingISP) {
		this.existingISP = existingISP;
	}
	/**
	 * @return the prospectType
	 */
	public String getProspectType() {
		return prospectType;
	}
	/**
	 * @param prospectType the prospectType to set
	 */
	public void setProspectType(String prospectType) {
		this.prospectType = prospectType;
	}
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	/**
	 * @return the deDupFlag
	 */
	public String getDeDupFlag() {
		return deDupFlag;
	}
	/**
	 * @param deDupFlag the deDupFlag to set
	 */
	public void setDeDupFlag(String deDupFlag) {
		this.deDupFlag = deDupFlag;
	}
	/**
	 * @return the assignedTo
	 */
	public String getAssignedTo() {
		return assignedTo;
	}
	/**
	 * @param assignedTo the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return TransactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateProspectVo [createUpdateProspectId=" + createUpdateProspectId + ", customerProfession="
				+ customerProfession + ", feasibilityVO=" + feasibilityVO + ", paymentVO=" + paymentVO + ", tariffVO="
				+ tariffVO + ", priorityVOList=" + priorityVOList + ", status=" + status + ", action=" + action
				+ ", title=" + title + ", firstName=" + firstName + ", lastName=" + lastName + ", middleName="
				+ middleName + ", preferredCallDate=" + preferredCallDate + ", prospectNumber=" + prospectNumber
				+ ", customerType=" + customerType + ", mobileNumber=" + mobileNumber + ", alternativeMobileNo="
				+ alternativeMobileNo + ", officeNumber=" + officeNumber + ", emailId=" + emailId + ", customerProfile="
				+ customerProfile + ", alternativeEmailId=" + alternativeEmailId + ", currentAddress=" + currentAddress
				+ ", communicationAddress=" + communicationAddress + ", permanentAddress=" + permanentAddress
				+ ", notes=" + notes + ", cityName=" + cityName + ", branchName=" + branchName + ", areaName="
				+ areaName + ", subAreaName=" + subAreaName + ", enquiryType=" + enquiryType + ", source=" + source
				+ ", How_To_Know_ACT=" + How_To_Know_ACT + ", subAttribute=" + subAttribute + ", CRPAccountNumber="
				+ CRPAccountNumber + ", CRPMobileNumber=" + CRPMobileNumber + ", existingISP=" + existingISP
				+ ", prospectType=" + prospectType + ", reason=" + reason + ", deDupFlag=" + deDupFlag + ", assignedTo="
				+ assignedTo + ", TransactionId=" + TransactionId + "]";
	}
	
	
	
}
