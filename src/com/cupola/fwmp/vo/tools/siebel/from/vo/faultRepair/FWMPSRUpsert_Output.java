 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair;

 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FWMPSRUpsert_Output {

	private String Transaction_spcID;

    private String SR_spcNumber;

    private String Error_spcMessage;

    private String Error_spcCode;

    public String getTransaction_spcID ()
    {
        return Transaction_spcID;
    }

    public void setTransaction_spcID (String Transaction_spcID)
    {
        this.Transaction_spcID = Transaction_spcID;
    }

    public String getSR_spcNumber ()
    {
        return SR_spcNumber;
    }

    public void setSR_spcNumber (String SR_spcNumber)
    {
        this.SR_spcNumber = SR_spcNumber;
    }

    public String getError_spcMessage ()
    {
        return Error_spcMessage;
    }

    public void setError_spcMessage (String Error_spcMessage)
    {
        this.Error_spcMessage = Error_spcMessage;
    }

    public String getError_spcCode ()
    {
        return Error_spcCode;
    }

    public void setError_spcCode (String Error_spcCode)
    {
        this.Error_spcCode = Error_spcCode;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FWMPSRUpsert_Output [Transaction_spcID=" + Transaction_spcID + ", SR_spcNumber=" + SR_spcNumber
				+ ", Error_spcMessage=" + Error_spcMessage + ", Error_spcCode=" + Error_spcCode + "]";
	}

    
}
