/**
* @Author kiran  Sep 26, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.from.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.response.APIResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @Author kiran Sep 26, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "WOEligibilityCheckVO")
public class WOEligibilityCheckVO {
	@Id
	@JsonIgnore
	private Long id;
	private String prospectNo;
	private String cityName;
	private String transactionId;
	private APIResponse validatorResponse;
	private WOEligibilityCheckResponse response;

	/**
	 * @return the validatorResponse
	 */
	public APIResponse getValidatorResponse() {
		return validatorResponse;
	}

	/**
	 * @param validatorResponse the validatorResponse to set
	 */
	public void setValidatorResponse(APIResponse validatorResponse) {
		this.validatorResponse = validatorResponse;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the prospectNo
	 */
	public String getProspectNo() {
		return prospectNo;
	}

	/**
	 * @param prospectNo
	 *            the prospectNo to set
	 */
	public void setProspectNo(String prospectNo) {
		this.prospectNo = prospectNo;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the response
	 */
	public WOEligibilityCheckResponse getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(WOEligibilityCheckResponse response) {
		this.response = response;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WOEligibilityCheckVO [id=" + id + ", prospectNo=" + prospectNo + ", cityName=" + cityName
				+ ", transactionId=" + transactionId + ", validatorResponse=" + validatorResponse + ", response="
				+ response + "]";
	}

}
