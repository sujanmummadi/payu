 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting;

 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ServiceRequest_ACTPriotization {
	
	
	private String ACTPrioritization;

    public String getACTPrioritization ()
    {
        return ACTPrioritization;
    }

    public void setACTPrioritization (String ACTPrioritization)
    {
        this.ACTPrioritization = ACTPrioritization;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServiceRequest_ACTPriotization [ACTPrioritization=" + ACTPrioritization + "]";
	}

   

}
