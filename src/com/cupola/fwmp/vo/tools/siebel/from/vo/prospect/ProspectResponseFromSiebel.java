/**
* @Author aditya  10-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.from.vo.prospect;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @Author aditya 10-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved. All the
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */

public class ProspectResponseFromSiebel {
	
	@JsonProperty("CreateProspectAPI_Output")
	private CreateProspectAPI_Output CreateProspectAPI_Output;

	public CreateProspectAPI_Output getCreateProspectAPI_Output() {
		return CreateProspectAPI_Output;
	}

	public void setCreateProspectAPI_Output(CreateProspectAPI_Output CreateProspectAPI_Output) {
		this.CreateProspectAPI_Output = CreateProspectAPI_Output;
	}

	@Override
	public String toString() {
		return "ClassPojo [CreateProspectAPI_Output = " + CreateProspectAPI_Output + "]";
	}
}
