 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo.workorder;

 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class UpdateWorkOrder_Output {

	private String TransactionID;

    private String WO_spcNumber;

    private String Error_spcMessage;

    private String Error_spcCode;

    public String getTransactionID ()
    {
        return TransactionID;
    }

    public void setTransactionID (String TransactionID)
    {
        this.TransactionID = TransactionID;
    }

    public String getWO_spcNumber ()
    {
        return WO_spcNumber;
    }

    public void setWO_spcNumber (String WO_spcNumber)
    {
        this.WO_spcNumber = WO_spcNumber;
    }

    public String getError_spcMessage ()
    {
        return Error_spcMessage;
    }

    public void setError_spcMessage (String Error_spcMessage)
    {
        this.Error_spcMessage = Error_spcMessage;
    }

    public String getError_spcCode ()
    {
        return Error_spcCode;
    }

    public void setError_spcCode (String Error_spcCode)
    {
        this.Error_spcCode = Error_spcCode;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UpdateWorkOrder_Output [TransactionID=" + TransactionID + ", WO_spcNumber=" + WO_spcNumber
				+ ", Error_spcMessage=" + Error_spcMessage + ", Error_spcCode=" + Error_spcCode + "]";
	}

    
}
