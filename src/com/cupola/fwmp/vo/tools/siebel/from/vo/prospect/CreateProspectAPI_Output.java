/**
* @Author kiran  Oct 12, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.from.vo.prospect;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @Author kiran Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class CreateProspectAPI_Output {
	@JsonProperty("ProspectNumber")
	private String ProspectNumber;

	@JsonProperty("DeDupFlag")
	private String DeDupFlag;
	
	@JsonIgnore
	@JsonProperty("Transation_spcId")
	private String Transation_spcId;

	@JsonIgnore
	@JsonProperty("Error_spcMessage")
	private String Error_spcMessage;
	
	@JsonProperty("Error_spcCode")
	private String Error_spcCode;

	@JsonProperty("ProspectNumber")
	public String getProspectNumber() {
		return ProspectNumber;
	}

	@JsonProperty("ProspectNumber")
	public void setProspectNumber(String ProspectNumber) {
		this.ProspectNumber = ProspectNumber;
	}

	@JsonProperty("DeDupFlag")
	public String getDeDupFlag() {
		return DeDupFlag;
	}

	@JsonProperty("DeDupFlag")
	public void setDeDupFlag(String DeDupFlag) {
		this.DeDupFlag = DeDupFlag;
	}

	public String getTransation_spcId() {
		return Transation_spcId;
	}

	public void setTransation_spcId(String Transation_spcId) {
		this.Transation_spcId = Transation_spcId;
	}

	@JsonProperty("Error_spcMessage")
	public String getError_spcMessage() {
		return Error_spcMessage;
	}

	@JsonProperty("Error_spcMessage")
	public void setError_spcMessage(String Error_spcMessage) {
		this.Error_spcMessage = Error_spcMessage;
	}

	@JsonProperty("Error_spcCode")
	public String getError_spcCode() {
		return Error_spcCode;
	}

	@JsonProperty("Error_spcCode")
	public void setError_spcCode(String Error_spcCode) {
		this.Error_spcCode = Error_spcCode;
	}

	@Override
	public String toString() {
		return "CreateProspectAPI_Output [ProspectNumber = " + ProspectNumber + ", DeDupFlag = " + DeDupFlag
				+ ", Transation_spcId = " + Transation_spcId + ", Error_spcMessage = " + Error_spcMessage
				+ ", Error_spcCode = " + Error_spcCode + "]";
	}
}
