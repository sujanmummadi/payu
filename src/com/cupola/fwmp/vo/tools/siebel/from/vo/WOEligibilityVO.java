/**
* @Author aditya  11-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.from.vo;

/**
 * @Author aditya 11-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class WOEligibilityVO {

	// "select distinct c.firstName as customerName,t.prospectNo as prospectNo,"
	// + "tal.activityId as activityId, city.cityName as cityName, city.id as cityId
	// "

	private String customerName;
	private String prospectNo;
	private Long activityId;
	private String cityName;
	private Long cityId;

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the prospectNo
	 */
	public String getProspectNo() {
		return prospectNo;
	}

	/**
	 * @param prospectNo
	 *            the prospectNo to set
	 */
	public void setProspectNo(String prospectNo) {
		this.prospectNo = prospectNo;
	}

	/**
	 * @return the activityId
	 */
	public Long getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            the activityId to set
	 */
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the cityId
	 */
	public Long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WOEligibilityVO [customerName=" + customerName + ", prospectNo=" + prospectNo + ", activityId="
				+ activityId + ", cityName=" + cityName + ", cityId=" + cityId + "]";
	}
}
