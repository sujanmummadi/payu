/**
 * 
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class Last3ServiceImpactingResponseFromSiebel {
	private QueryLast3SR_Output QueryLast3SR_Output;

    public QueryLast3SR_Output getQueryLast3SR_Output ()
    {
        return QueryLast3SR_Output;
    }

    public void setQueryLast3SR_Output (QueryLast3SR_Output QueryLast3SR_Output)
    {
        this.QueryLast3SR_Output = QueryLast3SR_Output;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [QueryLast3SR_Output = "+QueryLast3SR_Output+"]";
    }
}
