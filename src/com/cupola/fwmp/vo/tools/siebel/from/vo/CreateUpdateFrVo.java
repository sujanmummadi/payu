/**
 * 
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.ListOfPrioritySource;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author pawan
 *
 */
@Document(collection = "CreateUpdateFr")
public class CreateUpdateFrVo {

	@JsonIgnore
	@Id
	private Long createUpdateFrWorkOrderId;
	@JsonIgnore
	private String reopenStatus;
	@JsonIgnore
	private String customerName;
	@JsonIgnore
	private String prospectNumber;
	@JsonIgnore
	private String mongoStatus;


	private String action;
	private String customerNumber;
	private String ticketNo;
	private String categoryName;
	private String ticketSource;
	private String voc;
	private String reopenCount;//
	@JsonProperty("Status")
	private String Status;//
	private String symptomName;
	private String serviceRequestDate;
	private String comments;//
	private String cxIp;
	private String cxPort;
	private String fxName;
	private String fxIp;
	private String fxPort;
	private String firstName;
	private String middleName;
	private String lastName;
	private CustomerAddressVO currentAddress;
	private String mobileNumber;
	private String alternativeMobileNo;
	private String area;
	private String branch;
	private String city;
	private String priorityValue;//
	private ListOfPrioritySource listOfPrioritySource;//
	private String assignedTo;//
	private String closedByUserId;
	private String defectCode;//
	private String subDefectCode;//
	@JsonProperty("ETR")
	private String ETR;
	private String transactionId;
	/**
	 * @return the createUpdateFrWorkOrderId
	 */
	public Long getCreateUpdateFrWorkOrderId() {
		return createUpdateFrWorkOrderId;
	}
	/**
	 * @param createUpdateFrWorkOrderId the createUpdateFrWorkOrderId to set
	 */
	public void setCreateUpdateFrWorkOrderId(Long createUpdateFrWorkOrderId) {
		this.createUpdateFrWorkOrderId = createUpdateFrWorkOrderId;
	}
	/**
	 * @return the reopenStatus
	 */
	public String getReopenStatus() {
		return reopenStatus;
	}
	/**
	 * @param reopenStatus the reopenStatus to set
	 */
	public void setReopenStatus(String reopenStatus) {
		this.reopenStatus = reopenStatus;
	}
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return prospectNumber;
	}
	/**
	 * @param prospectNumber the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber() {
		return customerNumber;
	}
	/**
	 * @param customerNumber the customerNumber to set
	 */
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	/**
	 * @return the ticketNo
	 */
	public String getTicketNo() {
		return ticketNo;
	}
	/**
	 * @param ticketNo the ticketNo to set
	 */
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/**
	 * @return the ticketSource
	 */
	public String getTicketSource() {
		return ticketSource;
	}
	/**
	 * @param ticketSource the ticketSource to set
	 */
	public void setTicketSource(String ticketSource) {
		this.ticketSource = ticketSource;
	}
	/**
	 * @return the voc
	 */
	public String getVoc() {
		return voc;
	}
	/**
	 * @param voc the voc to set
	 */
	public void setVoc(String voc) {
		this.voc = voc;
	}
	/**
	 * @return the reopenCount
	 */
	public String getReopenCount() {
		return reopenCount;
	}
	/**
	 * @param reopenCount the reopenCount to set
	 */
	public void setReopenCount(String reopenCount) {
		this.reopenCount = reopenCount;
	}
	/**
	 * @return the status
	 */
	@JsonProperty("Status")
	public String getStatus() {
		return Status;
	}
	/**
	 * @param status the status to set
	 */
	@JsonProperty("Status")
	public void setStatus(String status) {
		this.Status = status;
	}
	/**
	 * @return the symptomName
	 */
	public String getSymptomName() {
		return symptomName;
	}
	/**
	 * @param symptomName the symptomName to set
	 */
	public void setSymptomName(String symptomName) {
		this.symptomName = symptomName;
	}
	/**
	 * @return the serviceRequestDate
	 */
	public String getServiceRequestDate() {
		return serviceRequestDate;
	}
	/**
	 * @param serviceRequestDate the serviceRequestDate to set
	 */
	public void setServiceRequestDate(String serviceRequestDate) {
		this.serviceRequestDate = serviceRequestDate;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the cxIp
	 */
	public String getCxIp() {
		return cxIp;
	}
	/**
	 * @param cxIp the cxIp to set
	 */
	public void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}
	/**
	 * @return the cxPort
	 */
	public String getCxPort() {
		return cxPort;
	}
	/**
	 * @param cxPort the cxPort to set
	 */
	public void setCxPort(String cxPort) {
		this.cxPort = cxPort;
	}
	/**
	 * @return the fxName
	 */
	public String getFxName() {
		return fxName;
	}
	/**
	 * @param fxName the fxName to set
	 */
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	/**
	 * @return the fxIp
	 */
	public String getFxIp() {
		return fxIp;
	}
	/**
	 * @param fxIp the fxIp to set
	 */
	public void setFxIp(String fxIp) {
		this.fxIp = fxIp;
	}
	/**
	 * @return the fxPort
	 */
	public String getFxPort() {
		return fxPort;
	}
	/**
	 * @param fxPort the fxPort to set
	 */
	public void setFxPort(String fxPort) {
		this.fxPort = fxPort;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the currentAddress
	 */
	public CustomerAddressVO getCurrentAddress() {
		return currentAddress;
	}
	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(CustomerAddressVO currentAddress) {
		this.currentAddress = currentAddress;
	}
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * @return the alternativeMobileNo
	 */
	public String getAlternativeMobileNo() {
		return alternativeMobileNo;
	}
	/**
	 * @param alternativeMobileNo the alternativeMobileNo to set
	 */
	public void setAlternativeMobileNo(String alternativeMobileNo) {
		this.alternativeMobileNo = alternativeMobileNo;
	}
	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}
	/**
	 * @param area the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}
	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}
	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the priorityValue
	 */
	public String getPriorityValue() {
		return priorityValue;
	}
	/**
	 * @param priorityValue the priorityValue to set
	 */
	public void setPriorityValue(String priorityValue) {
		this.priorityValue = priorityValue;
	}
	 
	/**
	 * @return the listOfPrioritySource
	 */
	public ListOfPrioritySource getListOfPrioritySource() {
		return listOfPrioritySource;
	}
	/**
	 * @param listOfPrioritySource the listOfPrioritySource to set
	 */
	public void setListOfPrioritySource(ListOfPrioritySource listOfPrioritySource) {
		this.listOfPrioritySource = listOfPrioritySource;
	}
	/**
	 * @return the assignedTo
	 */
	public String getAssignedTo() {
		return assignedTo;
	}
	/**
	 * @param assignedTo the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	/**
	 * @return the closedByUserId
	 */
	public String getClosedByUserId() {
		return closedByUserId;
	}
	/**
	 * @param closedByUserId the closedByUserId to set
	 */
	public void setClosedByUserId(String closedByUserId) {
		this.closedByUserId = closedByUserId;
	}
	/**
	 * @return the defectCode
	 */
	public String getDefectCode() {
		return defectCode;
	}
	/**
	 * @param defectCode the defectCode to set
	 */
	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}
	/**
	 * @return the subDefectCode
	 */
	public String getSubDefectCode() {
		return subDefectCode;
	}
	/**
	 * @param subDefectCode the subDefectCode to set
	 */
	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	/**
	 * @return the eTR
	 */
	@JsonProperty("ETR")
	public String getETR() {
		return ETR;
	}
	/**
	 * @param eTR the eTR to set
	 */
	@JsonProperty("ETR")
	public void setETR(String eTR) {
		ETR = eTR;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	
	public String getMongoStatus() {
		return mongoStatus;
	}
	public void setMongoStatus(String mongoStatus) {
		this.mongoStatus = mongoStatus;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateFrVo [createUpdateFrWorkOrderId=" + createUpdateFrWorkOrderId + ", reopenStatus="
				+ reopenStatus + ", customerName=" + customerName + ", prospectNumber=" + prospectNumber + ", action="
				+ action + ", customerNumber=" + customerNumber + ", ticketNo=" + ticketNo + ", categoryName="
				+ categoryName + ", ticketSource=" + ticketSource + ", voc=" + voc + ", reopenCount=" + reopenCount
				+ ", status=" + Status + ", symptomName=" + symptomName + ", serviceRequestDate=" + serviceRequestDate
				+ ", comments=" + comments + ", cxIp=" + cxIp + ", cxPort=" + cxPort + ", fxName=" + fxName + ", fxIp="
				+ fxIp + ", fxPort=" + fxPort + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", currentAddress=" + currentAddress + ", mobileNumber=" + mobileNumber
				+ ", alternativeMobileNo=" + alternativeMobileNo + ", area=" + area + ", branch=" + branch + ", city="
				+ city + ", priorityValue=" + priorityValue + ", listOfPrioritySource=" + listOfPrioritySource
				+ ", assignedTo=" + assignedTo + ", closedByUserId=" + closedByUserId + ", defectCode=" + defectCode
				+ ", subDefectCode=" + subDefectCode + ", ETR=" + ETR + ", transactionId=" + transactionId + "]";
	}
}
