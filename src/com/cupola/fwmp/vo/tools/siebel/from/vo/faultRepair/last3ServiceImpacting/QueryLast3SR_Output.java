 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting;

import java.util.List;

/**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class QueryLast3SR_Output {
	
	private List<ListOfFwmpqueryio> ListOfFwmpqueryio;

    private String Error_spcMessage;

    private String Error_spcCode;

	/**
	 * @return the listOfFwmpqueryio
	 */
	public List<ListOfFwmpqueryio> getListOfFwmpqueryio() {
		return ListOfFwmpqueryio;
	}

	/**
	 * @param listOfFwmpqueryio the listOfFwmpqueryio to set
	 */
	public void setListOfFwmpqueryio(List<ListOfFwmpqueryio> listOfFwmpqueryio) {
		ListOfFwmpqueryio = listOfFwmpqueryio;
	}

	/**
	 * @return the error_spcMessage
	 */
	public String getError_spcMessage() {
		return Error_spcMessage;
	}

	/**
	 * @param error_spcMessage the error_spcMessage to set
	 */
	public void setError_spcMessage(String error_spcMessage) {
		Error_spcMessage = error_spcMessage;
	}

	/**
	 * @return the error_spcCode
	 */
	public String getError_spcCode() {
		return Error_spcCode;
	}

	/**
	 * @param error_spcCode the error_spcCode to set
	 */
	public void setError_spcCode(String error_spcCode) {
		Error_spcCode = error_spcCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QueryLast3SR_Output [ListOfFwmpqueryio=" + ListOfFwmpqueryio + ", Error_spcMessage=" + Error_spcMessage
				+ ", Error_spcCode=" + Error_spcCode + "]";
	}
    
    

}
