/**
 * 
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting;

import java.util.List;

/**
 * @Author pawan Oct 10, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */
public class ListOfFwmpqueryio {

	private String TicketDescription;

	private String Owner;

	private String ClosedDate;

	private String ResolutionCode;

	private String VOC;

	private List<ListOfServiceRequest_ACTPriotization> ListOfServiceRequest_ACTPriotization;

	private String Nature;

	private String Priority;

	private String ETR;

	private String SRNumber;

	private String Status;

	private String Category;

	private String ResolutionDescription;

	private String SubResolutionCode;
	
	/**
	 * @return the ticketDescription
	 */
	public String getTicketDescription() {
		return TicketDescription;
	}



	/**
	 * @param ticketDescription the ticketDescription to set
	 */
	public void setTicketDescription(String ticketDescription) {
		TicketDescription = ticketDescription;
	}



	/**
	 * @return the owner
	 */
	public String getOwner() {
		return Owner;
	}



	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		Owner = owner;
	}



	/**
	 * @return the closedDate
	 */
	public String getClosedDate() {
		return ClosedDate;
	}



	/**
	 * @param closedDate the closedDate to set
	 */
	public void setClosedDate(String closedDate) {
		ClosedDate = closedDate;
	}



	/**
	 * @return the resolutionCode
	 */
	public String getResolutionCode() {
		return ResolutionCode;
	}



	/**
	 * @param resolutionCode the resolutionCode to set
	 */
	public void setResolutionCode(String resolutionCode) {
		ResolutionCode = resolutionCode;
	}



	/**
	 * @return the vOC
	 */
	public String getVOC() {
		return VOC;
	}



	/**
	 * @param vOC the vOC to set
	 */
	public void setVOC(String vOC) {
		VOC = vOC;
	}



	/**
	 * @return the listOfServiceRequest_ACTPriotization
	 */
	public List<ListOfServiceRequest_ACTPriotization> getListOfServiceRequest_ACTPriotization() {
		return ListOfServiceRequest_ACTPriotization;
	}



	/**
	 * @param listOfServiceRequest_ACTPriotization the listOfServiceRequest_ACTPriotization to set
	 */
	public void setListOfServiceRequest_ACTPriotization(
			List<ListOfServiceRequest_ACTPriotization> listOfServiceRequest_ACTPriotization) {
		ListOfServiceRequest_ACTPriotization = listOfServiceRequest_ACTPriotization;
	}



	/**
	 * @return the nature
	 */
	public String getNature() {
		return Nature;
	}



	/**
	 * @param nature the nature to set
	 */
	public void setNature(String nature) {
		Nature = nature;
	}



	/**
	 * @return the priority
	 */
	public String getPriority() {
		return Priority;
	}



	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		Priority = priority;
	}



	/**
	 * @return the eTR
	 */
	public String getETR() {
		return ETR;
	}



	/**
	 * @param eTR the eTR to set
	 */
	public void setETR(String eTR) {
		ETR = eTR;
	}



	/**
	 * @return the sRNumber
	 */
	public String getSRNumber() {
		return SRNumber;
	}



	/**
	 * @param sRNumber the sRNumber to set
	 */
	public void setSRNumber(String sRNumber) {
		SRNumber = sRNumber;
	}



	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}



	/**
	 * @return the category
	 */
	public String getCategory() {
		return Category;
	}



	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		Category = category;
	}



	/**
	 * @return the resolutionDescription
	 */
	public String getResolutionDescription() {
		return ResolutionDescription;
	}



	/**
	 * @param resolutionDescription the resolutionDescription to set
	 */
	public void setResolutionDescription(String resolutionDescription) {
		ResolutionDescription = resolutionDescription;
	}



	/**
	 * @return the subResolutionCode
	 */
	public String getSubResolutionCode() {
		return SubResolutionCode;
	}



	/**
	 * @param subResolutionCode the subResolutionCode to set
	 */
	public void setSubResolutionCode(String subResolutionCode) {
		SubResolutionCode = subResolutionCode;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfFwmpqueryio [TicketDescription=" + TicketDescription + ", Owner=" + Owner + ", ClosedDate="
				+ ClosedDate + ", ResolutionCode=" + ResolutionCode + ", VOC=" + VOC
				+ ", ListOfServiceRequest_ACTPriotization=" + ListOfServiceRequest_ACTPriotization + ", Nature="
				+ Nature + ", Priority=" + Priority + ", ETR=" + ETR + ", SRNumber=" + SRNumber + ", Status=" + Status
				+ ", Category=" + Category + ", ResolutionDescription=" + ResolutionDescription + ", SubResolutionCode="
				+ SubResolutionCode + "]";
	}

}
