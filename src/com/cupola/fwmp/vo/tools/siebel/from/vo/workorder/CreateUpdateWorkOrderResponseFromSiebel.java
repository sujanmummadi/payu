/**
* @Author aditya  10-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.from.vo.workorder;

/**
 * @Author aditya 10-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved. All the
 *            variables are starting with upper case because of Siebel
 *            dependency. These files are defined in .net and used in java.
 */

public class CreateUpdateWorkOrderResponseFromSiebel {
	private UpdateWorkOrder_Output UpdateWorkOrder_Output;

	public UpdateWorkOrder_Output getUpdateWorkOrder_Output() {
		return UpdateWorkOrder_Output;
	}

	public void setUpdateWorkOrder_Output(UpdateWorkOrder_Output UpdateWorkOrder_Output) {
		this.UpdateWorkOrder_Output = UpdateWorkOrder_Output;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateWorkOrderResponseFromSiebel [UpdateWorkOrder_Output=" + UpdateWorkOrder_Output + "]";
	}

	
}
