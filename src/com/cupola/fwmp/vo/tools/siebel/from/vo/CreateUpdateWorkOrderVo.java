/**
 * 
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.ListOfPrioritySource;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author pawan
 *
 */
@Document(collection = "CreateUpdateWorkOrder")
public class CreateUpdateWorkOrderVo {
	@JsonIgnore
	@Id
	private Long createUpdateWorkOrderId;
	@JsonIgnore
	private String customerName;
	@JsonIgnore
	private String fxName;
	@JsonIgnore
	private String reopenStatus;
	@JsonIgnore
	private String cxIp;
	
	  private String action;
	  private String workOrderNumber;
	  private String prospectNumber;
	  private String customerNumber;
	  private String firstName;
	  private String middleName;
	  private String lastName;
	  private CustomerAddressVO currentAddress;
	  private String customerMobile;
	  private String ticketCategory;
	  private String symptom;
	  private String requestDate;
	  private String comment;
	  private String areaName;
	  private String branchName;
	  private String cityName;
	  private String assignedTo;//
	  private String appointmentDate;//
	  private String priorityValue;//
	  private ListOfPrioritySource listOfPrioritySource;//
	  private String status;//
	  private String closedByUserId;//
	  private String defectCode;//
	  private String subDefectCode;//
	  private String transactionId;//
	  
	/**
	 * @return the createUpdateWorkOrderId
	 */
	public Long getCreateUpdateWorkOrderId() {
		return createUpdateWorkOrderId;
	}
	/**
	 * @param createUpdateWorkOrderId the createUpdateWorkOrderId to set
	 */
	public void setCreateUpdateWorkOrderId(Long createUpdateWorkOrderId) {
		this.createUpdateWorkOrderId = createUpdateWorkOrderId;
	}
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return the fxName
	 */
	public String getFxName() {
		return fxName;
	}
	/**
	 * @param fxName the fxName to set
	 */
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	/**
	 * @return the reopenStatus
	 */
	public String getReopenStatus() {
		return reopenStatus;
	}
	/**
	 * @param reopenStatus the reopenStatus to set
	 */
	public void setReopenStatus(String reopenStatus) {
		this.reopenStatus = reopenStatus;
	}
	/**
	 * @return the cxIp
	 */
	public String getCxIp() {
		return cxIp;
	}
	/**
	 * @param cxIp the cxIp to set
	 */
	public void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	/**
	 * @param workOrderNumber the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return prospectNumber;
	}
	/**
	 * @param prospectNumber the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}
	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber() {
		return customerNumber;
	}
	/**
	 * @param customerNumber the customerNumber to set
	 */
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the currentAddress
	 */
	public CustomerAddressVO getCurrentAddress() {
		return currentAddress;
	}
	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(CustomerAddressVO currentAddress) {
		this.currentAddress = currentAddress;
	}
	/**
	 * @return the customerMobile
	 */
	public String getCustomerMobile() {
		return customerMobile;
	}
	/**
	 * @param customerMobile the customerMobile to set
	 */
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	/**
	 * @return the ticketCategory
	 */
	public String getTicketCategory() {
		return ticketCategory;
	}
	/**
	 * @param ticketCategory the ticketCategory to set
	 */
	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}
	/**
	 * @return the symptom
	 */
	public String getSymptom() {
		return symptom;
	}
	/**
	 * @param symptom the symptom to set
	 */
	public void setSymptom(String symptom) {
		this.symptom = symptom;
	}
	/**
	 * @return the requestDate
	 */
	public String getRequestDate() {
		return requestDate;
	}
	/**
	 * @param requestDate the requestDate to set
	 */
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}
	/**
	 * @param areaName the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}
	/**
	 * @param branchName the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	/**
	 * @return the assignedTo
	 */
	public String getAssignedTo() {
		return assignedTo;
	}
	/**
	 * @param assignedTo the assignedTo to set
	 */
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	/**
	 * @return the appointmentDate
	 */
	public String getAppointmentDate() {
		return appointmentDate;
	}
	/**
	 * @param appointmentDate the appointmentDate to set
	 */
	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	/**
	 * @return the priorityValue
	 */
	public String getPriorityValue() {
		return priorityValue;
	}
	/**
	 * @param priorityValue the priorityValue to set
	 */
	public void setPriorityValue(String priorityValue) {
		this.priorityValue = priorityValue;
	}
	 
	/**
	 * @return the listOfPrioritySource
	 */
	public ListOfPrioritySource getListOfPrioritySource() {
		return listOfPrioritySource;
	}
	/**
	 * @param listOfPrioritySource the listOfPrioritySource to set
	 */
	public void setListOfPrioritySource(ListOfPrioritySource listOfPrioritySource) {
		this.listOfPrioritySource = listOfPrioritySource;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the closedByUserId
	 */
	public String getClosedByUserId() {
		return closedByUserId;
	}
	/**
	 * @param closedByUserId the closedByUserId to set
	 */
	public void setClosedByUserId(String closedByUserId) {
		this.closedByUserId = closedByUserId;
	}
	/**
	 * @return the defectCode
	 */
	public String getDefectCode() {
		return defectCode;
	}
	/**
	 * @param defectCode the defectCode to set
	 */
	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}
	/**
	 * @return the subDefectCode
	 */
	public String getSubDefectCode() {
		return subDefectCode;
	}
	/**
	 * @param subDefectCode the subDefectCode to set
	 */
	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateWorkOrderVo [CreateUpdateWorkOrderId=" + createUpdateWorkOrderId + ", customerName="
				+ customerName + ", fxName=" + fxName + ", reopenStatus=" + reopenStatus + ", cxIp=" + cxIp
				+ ", action=" + action + ", workOrderNumber=" + workOrderNumber + ", prospectNumber=" + prospectNumber
				+ ", customerNumber=" + customerNumber + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", currentAddress=" + currentAddress + ", customerMobile=" + customerMobile
				+ ", ticketCategory=" + ticketCategory + ", symptom=" + symptom + ", requestDate=" + requestDate
				+ ", comment=" + comment + ", areaName=" + areaName + ", branchName=" + branchName + ", cityName="
				+ cityName + ", assignedTo=" + assignedTo + ", appointmentDate=" + appointmentDate + ", priorityValue="
				+ priorityValue + ", listOfPrioritySource=" + listOfPrioritySource + ", status=" + status
				+ ", closedByUserId=" + closedByUserId + ", defectCode=" + defectCode + ", subDefectCode="
				+ subDefectCode + ", transactionId=" + transactionId + "]";
	}
	  
	  
	
}