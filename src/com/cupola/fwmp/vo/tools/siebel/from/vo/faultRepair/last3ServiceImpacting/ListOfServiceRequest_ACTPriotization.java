 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting;

 /**
 * @Author kiran  Oct 12, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ListOfServiceRequest_ACTPriotization {

	private ServiceRequest_ACTPriotization ServiceRequest_ACTPriotization;

    public ServiceRequest_ACTPriotization getServiceRequest_ACTPriotization ()
    {
        return ServiceRequest_ACTPriotization;
    }

    public void setServiceRequest_ACTPriotization (ServiceRequest_ACTPriotization ServiceRequest_ACTPriotization)
    {
        this.ServiceRequest_ACTPriotization = ServiceRequest_ACTPriotization;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ListOfServiceRequest_ACTPriotization [ServiceRequest_ACTPriotization=" + ServiceRequest_ACTPriotization
				+ "]";
	}

    
}
