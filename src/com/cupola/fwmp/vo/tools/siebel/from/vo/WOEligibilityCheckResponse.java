/**
* @Author kiran  Sep 26, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.siebel.from.vo;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @Author kiran Sep 26, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "WOEligibilityCheckResponse")
@JsonInclude(Include.NON_NULL)
public class WOEligibilityCheckResponse {
	@Id
	@JsonIgnore
	private Long id;
	private String prospectNo;
	private String cityName;
	private boolean isEligibleForWorkOrderCreation;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the prospectNo
	 */
	public String getProspectNo() {
		return prospectNo;
	}

	/**
	 * @param prospectNo
	 *            the prospectNo to set
	 */
	public void setProspectNo(String prospectNo) {
		this.prospectNo = prospectNo;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the isEligibleForWorkOrderCreation
	 */
	public boolean isEligibleForWorkOrderCreation() {
		return isEligibleForWorkOrderCreation;
	}

	/**
	 * @param isEligibleForWorkOrderCreation
	 *            the isEligibleForWorkOrderCreation to set
	 */
	public void setEligibleForWorkOrderCreation(boolean isEligibleForWorkOrderCreation) {
		this.isEligibleForWorkOrderCreation = isEligibleForWorkOrderCreation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WOEligibilityCheckResponse [id=" + id + ", prospectNo=" + prospectNo + ", cityName=" + cityName
				+ ", isEligibleForWorkOrderCreation=" + isEligibleForWorkOrderCreation + "]";
	}

}
