/**
* @Author aditya  24-Jul-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools;

/**
 * @Author aditya 24-Jul-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ToolAreaVO {

	private Long areaId;

	private String areaName;

	/**
	 * 
	 */
	public ToolAreaVO() {
		super();
	}

	/**
	 * @param areaId
	 * @param areaName
	 */
	public ToolAreaVO(Long areaId, String areaName) {
		super();
		this.areaId = areaId;
		this.areaName = areaName;
	}

	/**
	 * @return the areaId
	 */
	public Long getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId
	 *            the areaId to set
	 */
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}

	/**
	 * @param areaName
	 *            the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "_AreaVO [areaId=" + areaId + ", areaName=" + areaName + "]";
	}

}
