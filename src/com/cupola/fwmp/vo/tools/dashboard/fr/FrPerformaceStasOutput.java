/**
* @Author aditya  28-Dec-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.dashboard.fr;

import java.util.Date;

/**
 * @Author aditya 28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrPerformaceStasOutput {
	private String loginid;
	private String tlId;
	private String amId;
	private String bmId;
	private String cmId;
	private String usergroupname;
	private String closedTktsMtd;
	private String closedGartMtd;
	private String openTktsMtd;
	private String openGartMtd;
	private String closedTktsFtd;
	private String closedGartFtd;
	private String openTktsFtd;
	private String openGartFtd;
	private String csatMtd;
	private String csatFtd;
	private Date addedOn;
	private String addedBy;
	private String cssClass;
	private String firstName;
	private String lastName;

	public FrPerformaceStasOutput() {
	}

	public FrPerformaceStasOutput(Date addedOn) {
		this.addedOn = addedOn;
	}

	public FrPerformaceStasOutput(String loginid, String tlId, String amId, String bmId, String cmId,
			String usergroupname, String closedTktsMtd, String closedGartMtd, String openTktsMtd, String openGartMtd,
			String closedTktsFtd, String closedGartFtd, String openTktsFtd, String openGartFtd, String csatMtd,
			String csatFtd, Date addedOn, String addedBy) {
		this.loginid = loginid;
		this.tlId = tlId;
		this.amId = amId;
		this.bmId = bmId;
		this.cmId = cmId;
		this.usergroupname = usergroupname;
		this.closedTktsMtd = closedTktsMtd;
		this.closedGartMtd = closedGartMtd;
		this.openTktsMtd = openTktsMtd;
		this.openGartMtd = openGartMtd;
		this.closedTktsFtd = closedTktsFtd;
		this.closedGartFtd = closedGartFtd;
		this.openTktsFtd = openTktsFtd;
		this.openGartFtd = openGartFtd;
		this.csatMtd = csatMtd;
		this.csatFtd = csatFtd;
		this.addedOn = addedOn;
		this.addedBy = addedBy;
	}

	public String getLoginid() {
		return this.loginid;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public String getTlId() {
		return this.tlId;
	}

	public void setTlId(String tlId) {
		this.tlId = tlId;
	}

	public String getAmId() {
		return this.amId;
	}

	public void setAmId(String amId) {
		this.amId = amId;
	}

	public String getBmId() {
		return this.bmId;
	}

	public void setBmId(String bmId) {
		this.bmId = bmId;
	}

	public String getCmId() {
		return this.cmId;
	}

	public void setCmId(String cmId) {
		this.cmId = cmId;
	}

	public String getUsergroupname() {
		return this.usergroupname;
	}

	public void setUsergroupname(String usergroupname) {
		this.usergroupname = usergroupname;
	}

	public String getClosedTktsMtd() {
		return this.closedTktsMtd;
	}

	public void setClosedTktsMtd(String closedTktsMtd) {
		this.closedTktsMtd = closedTktsMtd;
	}

	public String getClosedGartMtd() {
		return this.closedGartMtd;
	}

	public void setClosedGartMtd(String closedGartMtd) {
		this.closedGartMtd = closedGartMtd;
	}

	public String getOpenTktsMtd() {
		return this.openTktsMtd;
	}

	public void setOpenTktsMtd(String openTktsMtd) {
		this.openTktsMtd = openTktsMtd;
	}

	public String getOpenGartMtd() {
		return this.openGartMtd;
	}

	public void setOpenGartMtd(String openGartMtd) {
		this.openGartMtd = openGartMtd;
	}

	public String getClosedTktsFtd() {
		return this.closedTktsFtd;
	}

	public void setClosedTktsFtd(String closedTktsFtd) {
		this.closedTktsFtd = closedTktsFtd;
	}

	public String getClosedGartFtd() {
		return this.closedGartFtd;
	}

	public void setClosedGartFtd(String closedGartFtd) {
		this.closedGartFtd = closedGartFtd;
	}

	public String getOpenTktsFtd() {
		return this.openTktsFtd;
	}

	public void setOpenTktsFtd(String openTktsFtd) {
		this.openTktsFtd = openTktsFtd;
	}

	public String getOpenGartFtd() {
		return this.openGartFtd;
	}

	public void setOpenGartFtd(String openGartFtd) {
		this.openGartFtd = openGartFtd;
	}

	public String getCsatMtd() {
		return this.csatMtd;
	}

	public void setCsatMtd(String csatMtd) {
		this.csatMtd = csatMtd;
	}

	public String getCsatFtd() {
		return this.csatFtd;
	}

	public void setCsatFtd(String csatFtd) {
		this.csatFtd = csatFtd;
	}

	public Date getAddedOn() {
		return this.addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public String getAddedBy() {
		return this.addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	/**
	 * @return the cssClass
	 */
	public String getCssClass() {
		return cssClass;
	}

	/**
	 * @param cssClass
	 *            the cssClass to set
	 */
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "FrPerformaceStasOutput [loginid=" + loginid + ", tlId=" + tlId + ", amId=" + amId + ", bmId=" + bmId
				+ ", cmId=" + cmId + ", usergroupname=" + usergroupname + ", closedTktsMtd=" + closedTktsMtd
				+ ", closedGartMtd=" + closedGartMtd + ", openTktsMtd=" + openTktsMtd + ", openGartMtd=" + openGartMtd
				+ ", closedTktsFtd=" + closedTktsFtd + ", closedGartFtd=" + closedGartFtd + ", openTktsFtd="
				+ openTktsFtd + ", openGartFtd=" + openGartFtd + ", csatMtd=" + csatMtd + ", csatFtd=" + csatFtd
				+ ", addedOn=" + addedOn + ", addedBy=" + addedBy + ", cssClass=" + cssClass + ", firstName="
				+ firstName + ", lastName=" + lastName + "]";
	}
}
