/**
* @Author aditya  02-Jan-2018
* @Copyright (c) 2018 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools.dashboard.fr;

import java.util.Map;
import java.util.Set;

/**
 * @Author aditya 02-Jan-2018
 * @Copyright (c) 2018 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrPerformaceStasOutputVo {

	private Map<String, FrPerformaceStasOutput> userAndReportingSubordinateMap;
	private Map<String, Set<String>> userAndReportingSubordinateIdMap;

	/**
	 * @return the userAndReportingSubordinateMap
	 */
	public Map<String, FrPerformaceStasOutput> getUserAndReportingSubordinateMap() {
		return userAndReportingSubordinateMap;
	}

	/**
	 * @param userAndReportingSubordinateMap
	 *            the userAndReportingSubordinateMap to set
	 */
	public void setUserAndReportingSubordinateMap(Map<String, FrPerformaceStasOutput> userAndReportingSubordinateMap) {
		this.userAndReportingSubordinateMap = userAndReportingSubordinateMap;
	}

	/**
	 * @return the userAndReportingSubordinateIdMap
	 */
	public Map<String, Set<String>> getUserAndReportingSubordinateIdMap() {
		return userAndReportingSubordinateIdMap;
	}

	/**
	 * @param userAndReportingSubordinateIdMap
	 *            the userAndReportingSubordinateIdMap to set
	 */
	public void setUserAndReportingSubordinateIdMap(Map<String, Set<String>> userAndReportingSubordinateIdMap) {
		this.userAndReportingSubordinateIdMap = userAndReportingSubordinateIdMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PerformaceStasOutputVo [userAndReportingSubordinateMap=" + userAndReportingSubordinateMap
				+ ", userAndReportingSubordinateIdMap=" + userAndReportingSubordinateIdMap + "]";
	}

}
