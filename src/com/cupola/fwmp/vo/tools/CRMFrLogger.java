/**
* @Author kiran  Oct 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo;

/**
 * @Author kiran Oct 11, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "CRMFrLogger")
public class CRMFrLogger {

	@Id
	private Long id;
	private CreateUpdateFrVo createUpdateFrVo;
	private APIResponse validatorResponse;
	private Date addedOnDate;
	private String transactionId;

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the addedOnDate
	 */
	public Date getAddedOnDate() {
		return addedOnDate;
	}

	/**
	 * @param addedOnDate
	 *            the addedOnDate to set
	 */
	public void setAddedOnDate(Date addedOnDate) {
		this.addedOnDate = addedOnDate;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the createUpdateFrVo
	 */
	public CreateUpdateFrVo getCreateUpdateFrVo() {
		return createUpdateFrVo;
	}

	/**
	 * @param createUpdateFrVo
	 *            the createUpdateFrVo to set
	 */
	public void setCreateUpdateFrVo(CreateUpdateFrVo createUpdateFrVo) {
		this.createUpdateFrVo = createUpdateFrVo;
	}

	/**
	 * @return the validatorResponse
	 */
	public APIResponse getValidatorResponse() {
		return validatorResponse;
	}

	/**
	 * @param validatorResponse
	 *            the validatorResponse to set
	 */
	public void setValidatorResponse(APIResponse validatorResponse) {
		this.validatorResponse = validatorResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CRMFrLogger [id=" + id + ", createUpdateFrVo=" + createUpdateFrVo + ", validatorResponse="
				+ validatorResponse + ", addedOnDate=" + addedOnDate + ", transactionId=" + transactionId + "]";
	}

}
