/**
 * 
 */
package com.cupola.fwmp.vo.tools;

/**
 * @author pawan
 *
 */
public class TariffVO {

	private String planId;
	private String tariffId;
	private String planType;
	private String planCategory;
	private Boolean isRouterFree;
	private String cityName;
	private String externalRouter;
	private String planAmount;
	
	
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getTariffId() {
		return tariffId;
	}
	public void setTariffId(String tariffId) {
		this.tariffId = tariffId;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getPlanCategory() {
		return planCategory;
	}
	public void setPlanCategory(String planCategory) {
		this.planCategory = planCategory;
	}
	public Boolean getIsRouterFree() {
		return isRouterFree;
	}
	public void setIsRouterFree(Boolean isRouterFree) {
		this.isRouterFree = isRouterFree;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getExternalRouter() {
		return externalRouter;
	}
	public void setExternalRouter(String externalRouter) {
		this.externalRouter = externalRouter;
	}
	public String getPlanAmount() {
		return planAmount;
	}
	public void setPlanAmount(String planAmount) {
		this.planAmount = planAmount;
	}
	@Override
	public String toString() {
		return "TariffVO [planId=" + planId + ", tariffId=" + tariffId
				+ ", planType=" + planType + ", planCategory=" + planCategory
				+ ", isRouterFree=" + isRouterFree + ", cityName=" + cityName
				+ ", externalRouter=" + externalRouter + ", planAmount="
				+ planAmount + "]";
	}
	
	
}