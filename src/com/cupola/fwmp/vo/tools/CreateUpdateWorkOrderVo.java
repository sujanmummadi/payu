/**
 * 
 */
package com.cupola.fwmp.vo.tools;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author pawan
 *
 */
@Document(collection = "CreateUpdateWorkOrder")
public class CreateUpdateWorkOrderVo {
	
	@Id
	private Long CreateUpdateWorkOrderId;
	private String status;
	
	private String action;
	private String prospectNumber;
	private String customerNumber;
	private String customerName;
	private CustomerAddressVO customerAddressVO;
	private String customerMobile;
	private String ticketCategory;
	private String reopenStatus;
	private String symptom;
	private String reqDate;
	private String comment;
	private String commentCode;
	private String cxIp;
	private String fxName;
	private String areaName;
	private String branchName;
	private String cityName;
	private String assignedTo;
	private PriorityVO priorityVO;
	
	public Long getCreateUpdateWorkOrderId() {
		return CreateUpdateWorkOrderId;
	}
	public void setCreateUpdateWorkOrderId(Long createUpdateWorkOrderId) {
		CreateUpdateWorkOrderId = createUpdateWorkOrderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getProspectNumber() {
		return prospectNumber;
	}
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public CustomerAddressVO getCustomerAddress() {
		return customerAddressVO;
	}
	public void setCustomerAddress(CustomerAddressVO customerAddressVO) {
		this.customerAddressVO = customerAddressVO;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public String getTicketCategory() {
		return ticketCategory;
	}
	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}
	public String getReopenStatus() {
		return reopenStatus;
	}
	public void setReopenStatus(String reopenStatus) {
		this.reopenStatus = reopenStatus;
	}
	public String getSymptom() {
		return symptom;
	}
	public void setSymptom(String symptom) {
		this.symptom = symptom;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommentCode() {
		return commentCode;
	}
	public void setCommentCode(String commentCode) {
		this.commentCode = commentCode;
	}
	public String getCxIp() {
		return cxIp;
	}
	public void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}
	public String getFxName() {
		return fxName;
	}
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public PriorityVO getPriority() {
		return priorityVO;
	}
	public void setPriority(PriorityVO priorityVO) {
		this.priorityVO = priorityVO;
	}
	@Override
	public String toString() {
		return "CreateUpdateWorkOrderVo [CreateUpdateWorkOrderId="
				+ CreateUpdateWorkOrderId + ", status=" + status + ", action="
				+ action + ", prospectNumber=" + prospectNumber
				+ ", customerNumber=" + customerNumber + ", customerName="
				+ customerName + ", customerAddress=" + customerAddressVO
				+ ", customerMobile=" + customerMobile + ", ticketCategory="
				+ ticketCategory + ", reopenStatus=" + reopenStatus
				+ ", symptom=" + symptom + ", reqDate=" + reqDate
				+ ", comment=" + comment + ", commentCode=" + commentCode
				+ ", cxIp=" + cxIp + ", fxName=" + fxName + ", areaName="
				+ areaName + ", branchName=" + branchName + ", cityName="
				+ cityName + ", assignedTo=" + assignedTo + ", priority="
				+ priorityVO + "]";
	}
	
	
}