/**
 * 
 */
package com.cupola.fwmp.vo.tools;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author pawan
 *
 */
public class PriorityVO {

	private String priorityType;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PriorityVO [priorityType=" + priorityType + "]";
	}

	/**
	 * @return the priorityType
	 */
	public String getPriorityType() {
		return priorityType;
	}

	/**
	 * @param priorityType the priorityType to set
	 */
	public void setPriorityType(String priorityType) {
		this.priorityType = priorityType;
	}
 
	 
	
}
