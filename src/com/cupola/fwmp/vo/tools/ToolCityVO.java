/**
* @Author aditya  24-Jul-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools;

import java.util.List;

/**
 * @Author aditya 24-Jul-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ToolCityVO {
	
	private Long cityId;

	private String cityName;

	private List<ToolBranchVO> branchList;

	/**
	 * 
	 */
	public ToolCityVO() {
		super();
	}

	/**
	 * @param cityId
	 * @param cityName
	 */
	public ToolCityVO(Long cityId, String cityName) {
		super();
		this.cityId = cityId;
		this.cityName = cityName;
	}

	/**
	 * @return the cityId
	 */
	public Long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the branchList
	 */
	public List<ToolBranchVO> getBranchList() {
		return branchList;
	}

	/**
	 * @param branchList
	 *            the branchList to set
	 */
	public void setBranchList(List<ToolBranchVO> branchList) {
		this.branchList = branchList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "_CityVO [cityId=" + cityId + ", cityName=" + cityName + ", branchList=" + branchList + "]";
	}

}
