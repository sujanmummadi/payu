 /**
 * @Author aditya  29-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.tools;


import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
 /**
 * @Author aditya  29-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "CreateUpdateProspect")
public class CreateUpdateProspectVo 
{
	@Id
	private Long createUpdateProspectId;
	private String status;
	
	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private Date preferedCallDate;
	private String prospectNumber;
	private String customerType;
	private String mobileNumber;
	private String alternativeMobileNo;
	private String officeNumber;
	private String emailId;
	private String alternativeEmailId;
	private String customerProfession;
	private List<CustomerAddressVO> listOfAddress;
	private String notes;
	private String cityName;
	private String branchName;
	private String areaName;
	private String enquiryType;
	private String source;
	private String How_To_Know_ACT;
	private String prospectType;
	private FeasibilityVO feasibilityVO ;
	private PaymentVO paymentVO;
	private TariffVO tariffVO;
	private PriorityVO priorityVO;

	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the preferedCallDate
	 */
	public Date getPreferedCallDate() {
		return preferedCallDate;
	}

	/**
	 * @param preferedCallDate the preferedCallDate to set
	 */
	public void setPreferedCallDate(Date preferedCallDate) {
		this.preferedCallDate = preferedCallDate;
	}

	/**
	 * @return the prospectNumber
	 */
	public String getProspectNumber() {
		return prospectNumber;
	}

	/**
	 * @param prospectNumber the prospectNumber to set
	 */
	public void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the alternativeMobileNo
	 */
	public String getAlternativeMobileNo() {
		return alternativeMobileNo;
	}

	/**
	 * @param alternativeMobileNo the alternativeMobileNo to set
	 */
	public void setAlternativeMobileNo(String alternativeMobileNo) {
		this.alternativeMobileNo = alternativeMobileNo;
	}

	/**
	 * @return the officeNumber
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * @param officeNumber the officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the alternativeEmailId
	 */
	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}

	/**
	 * @param alternativeEmailId the alternativeEmailId to set
	 */
	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}

	/**
	 * @return the customerProfession
	 */
	public String getCustomerProfession() {
		return customerProfession;
	}

	/**
	 * @param customerProfession the customerProfession to set
	 */
	public void setCustomerProfession(String customerProfession) {
		this.customerProfession = customerProfession;
	}

	/**
	 * @return the listOfAddress
	 */
	public List<CustomerAddressVO> getListOfAddress() {
		return listOfAddress;
	}

	/**
	 * @param listOfAddress the listOfAddress to set
	 */
	public void setListOfAddress(List<CustomerAddressVO> listOfAddress) {
		this.listOfAddress = listOfAddress;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}

	/**
	 * @param areaName the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/**
	 * @return the enquiryType
	 */
	public String getEnquiryType() {
		return enquiryType;
	}

	/**
	 * @param enquiryType the enquiryType to set
	 */
	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the how_To_Know_ACT
	 */
	public String getHow_To_Know_ACT() {
		return How_To_Know_ACT;
	}

	/**
	 * @param how_To_Know_ACT the how_To_Know_ACT to set
	 */
	public void setHow_To_Know_ACT(String how_To_Know_ACT) {
		How_To_Know_ACT = how_To_Know_ACT;
	}

	/**
	 * @return the prospectType
	 */
	public String getProspectType() {
		return prospectType;
	}

	/**
	 * @param prospectType the prospectType to set
	 */
	public void setProspectType(String prospectType) {
		this.prospectType = prospectType;
	}

	/**
	 * @return the feasibility
	 */
	public FeasibilityVO getFeasibility() {
		return feasibilityVO;
	}

	/**
	 * @param feasibility the feasibility to set
	 */
	public void setFeasibility(FeasibilityVO feasibility) {
		this.feasibilityVO = feasibility;
	}

	/**
	 * @return the payment
	 */
	public PaymentVO getPayment() {
		return paymentVO;
	}

	/**
	 * @param payment the payment to set
	 */
	public void setPayment(PaymentVO payment) {
		this.paymentVO = payment;
	}

	/**
	 * @return the tariff
	 */
	public TariffVO getTariff() {
		return tariffVO;
	}

	/**
	 * @param tariff the tariff to set
	 */
	public void setTariff(TariffVO tariff) {
		this.tariffVO = tariff;
	}

	

	/**
	 * @return the createUpdateProspectId
	 */
	public Long getCreateUpdateProspectId() {
		return createUpdateProspectId;
	}

	/**
	 * @return the priority
	 */
	public PriorityVO getPriority() {
		return priorityVO;
	}

	/**
	 * @param priorityVO the priority to set
	 */
	public void setPriority(PriorityVO priorityVO) {
		this.priorityVO = priorityVO;
	}

	/**
	 * @param createUpdateProspectId the createUpdateProspectId to set
	 */
	public void setCreateUpdateProspectId(Long createUpdateProspectId) {
		this.createUpdateProspectId = createUpdateProspectId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUpdateProspectVo [createUpdateProspectId="
				+ createUpdateProspectId + ", status=" + status + ", title="
				+ title + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", middleName=" + middleName + ", preferedCallDate="
				+ preferedCallDate + ", prospectNumber=" + prospectNumber
				+ ", customerType=" + customerType + ", mobileNumber="
				+ mobileNumber + ", alternativeMobileNo=" + alternativeMobileNo
				+ ", officeNumber=" + officeNumber + ", emailId=" + emailId
				+ ", alternativeEmailId=" + alternativeEmailId
				+ ", customerProfession=" + customerProfession
				+ ", listOfAddress=" + listOfAddress + ", notes=" + notes
				+ ", cityName=" + cityName + ", branchName=" + branchName
				+ ", areaName=" + areaName + ", enquiryType=" + enquiryType
				+ ", source=" + source + ", How_To_Know_ACT=" + How_To_Know_ACT
				+ ", prospectType=" + prospectType + ", feasibility="
				+ feasibilityVO + ", payment=" + paymentVO + ", tariff=" + tariffVO
				+ ", priority=" + priorityVO + "]";
	}
	
	
}
