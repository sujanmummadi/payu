/**
* @Author aditya  28-Dec-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools;

/**
 * @Author aditya 28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class PerformaceStasInput {

	private String loginId;
	private String role;
	private String reportTo;
	private String city;
	private String branch;
	private String area;
	private String subArea;

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId
	 *            the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the reportTo
	 */
	public String getReportTo() {
		return reportTo;
	}

	/**
	 * @param reportTo
	 *            the reportTo to set
	 */
	public void setReportTo(String reportTo) {
		this.reportTo = reportTo;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch
	 *            the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the subArea
	 */
	public String getSubArea() {
		return subArea;
	}

	/**
	 * @param subArea
	 *            the subArea to set
	 */
	public void setSubArea(String subArea) {
		this.subArea = subArea;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PerformaceStasInput [loginId=" + loginId + ", role=" + role + ", reportTo=" + reportTo + ", city="
				+ city + ", branch=" + branch + ", area=" + area + ", subArea=" + subArea + "]";
	}
}
