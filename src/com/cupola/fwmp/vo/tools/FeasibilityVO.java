/**
 * 
 */
package com.cupola.fwmp.vo.tools;



/**
 * @author pawan
 *
 */
public class FeasibilityVO {
	
	private String connectionType;
	private String cxMac;
	private String cxPort;
	private String cxName;
	private String cxIpAddress;
	private String fxName;
	private String fxMac;
	private String fxport;
	private String fxIpAddress;
	
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}
	public String getCxMac() {
		return cxMac;
	}
	public void setCxMac(String cxMac) {
		this.cxMac = cxMac;
	}
	public  String getCxPort() {
		return cxPort;
	}
	public void setCxPort(String cxPort) {
		this.cxPort = cxPort;
	}
	public String getCxName() {
		return cxName;
	}
	public void setCxName(String cxName) {
		this.cxName = cxName;
	}
	public String getCxIpAddress() {
		return cxIpAddress;
	}
	public void setCxIpAddress(String cxIpAddress) {
		this.cxIpAddress = cxIpAddress;
	}
	public String getFxName() {
		return fxName;
	}
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	public String getFxMac() {
		return fxMac;
	}
	public void setFxMac(String fxMac) {
		this.fxMac = fxMac;
	}
	public String getFxport() {
		return fxport;
	}
	public void setFxport(String fxport) {
		this.fxport = fxport;
	}
	public String getFxIpAddress() {
		return fxIpAddress;
	}
	public void setFxIpAddress(String fxIpAddress) {
		this.fxIpAddress = fxIpAddress;
	}
	@Override
	public String toString() {
		return "FeasibilityVO [connectionType=" + connectionType + ", cxMac="
				+ cxMac + ", cxPort=" + cxPort + ", cxName=" + cxName
				+ ", cxIpAddress=" + cxIpAddress + ", fxName=" + fxName
				+ ", fxMac=" + fxMac + ", fxport=" + fxport + ", fxIpAddress="
				+ fxIpAddress + "]";
	}

	
	
}
