/**
* @Author aditya  24-Jul-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.tools;

import java.util.List;

import com.cupola.fwmp.vo.TypeAheadVo;

/**
 * @Author aditya 24-Jul-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ToolBranchVO {

	private Long branchId;

	private String branchName;

	private List<ToolAreaVO> areaList;

	/**
	 * 
	 */
	public ToolBranchVO() {
		super();
	}

	/**
	 * @param branchId
	 * @param branchName
	 */
	public ToolBranchVO(Long branchId, String branchName) {
		super();
		this.branchId = branchId;
		this.branchName = branchName;
	}
	
	/**
	 * @param branchId
	 * @param branchName
	 * @param areaList
	 */
	public ToolBranchVO(Long branchId, String branchName, List<ToolAreaVO> areaList) {
		super();
		this.branchId = branchId;
		this.branchName = branchName;
		this.areaList = areaList;
	}

	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId
	 *            the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName
	 *            the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the areaList
	 */
	public List<ToolAreaVO> getAreaList() {
		return areaList;
	}

	/**
	 * @param areaList
	 *            the areaList to set
	 */
	public void setAreaList(List<ToolAreaVO> areaList) {
		this.areaList = areaList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "_BranchVO [branchId=" + branchId + ", branchName=" + branchName + ", areaList=" + areaList + "]";
	}

}
