package com.cupola.fwmp.vo;

import java.util.Date;

public class TicketCustomerDetailsVO
{

	private long ticketId;
	private String workOrderNumber;
	private Date currentEtr;
	private Date commitedEtr;
	
	private Date modifiedOn;
	
	private long customerId;
	private String mqId;
	private String mobileNumber;
	private String customerEmail;
	
	
	public long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}
	public String getWorkOrderNumber()
	{
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber)
	{
		this.workOrderNumber = workOrderNumber;
	}
	
	public Date getModifiedOn()
	{
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}
	public long getCustomerId()
	{
		return customerId;
	}
	public void setCustomerId(long customerId)
	{
		this.customerId = customerId;
	}
	public String getMqId()
	{
		return mqId;
	}
	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}
	public String getMobileNumber()
	{
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}
	public String getCustomerEmail()
	{
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail)
	{
		this.customerEmail = customerEmail;
	}
	@Override
	public String toString()
	{
		return "TicketCustomerDetailsVO [ticketId=" + ticketId
				+ ", workOrderNumber=" + workOrderNumber + 
				", modifiedOn=" + modifiedOn + ", customerId="
				+ customerId + ", mqId=" + mqId + ", mobileNumber="
				+ mobileNumber + ", customerEmail=" + customerEmail + "]";
	}
	public Date getCurrentEtr()
	{
		return currentEtr;
	}
	public void setCurrentEtr(Date currentEtr)
	{
		this.currentEtr = currentEtr;
	}
	public Date getCommitedEtr()
	{
		return commitedEtr;
	}
	public void setCommitedEtr(Date commitedEtr)
	{
		this.commitedEtr = commitedEtr;
	}
	
	
	
}
