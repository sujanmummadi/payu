package com.cupola.fwmp.vo;

import java.util.List;

public class UserDeviceVO
{

	private Long userId;
	private List<Long> devices;
	
	public Long getUserId()
	{
		return userId;
	}
	public void setUserId(Long userId)
	{
		this.userId = userId;
	}
	public List<Long> getDevices()
	{
		return devices;
	}
	public void setDevices(List<Long> devices)
	{
		this.devices = devices;
	}
	 
}
