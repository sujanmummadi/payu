package com.cupola.fwmp.vo;

import java.util.List;

public class AssignVO
{

	private List<Long> ticketId;
	private List<String> workOrderNo;
	private int reasonCode;
	private String reasonValue;
	private long assignedToId;
	private long assignedById;
	private String remark;
	public long getAssignedById()
	{
		return assignedById;
	}
	public void setAssignedById(long assignedById)
	{
		this.assignedById = assignedById;
	}
	public List<Long> getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(List<Long> ticketId)
	{
		this.ticketId = ticketId;
	}
	public long getAssignedToId()
	{
		return assignedToId;
	}
	public void setAssignedToId(long assignedToId)
	{
		this.assignedToId = assignedToId;
	}
	public List<String> getWorkOrderNo()
	{
		return workOrderNo;
	}
	public void setWorkOrderNo(List<String> workOrderNo)
	{
		this.workOrderNo = workOrderNo;
	}
	public int getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getReasonValue() {
		return reasonValue;
	}
	public void setReasonValue(String reasonValue) {
		this.reasonValue = reasonValue;
	}
	public String getRemark()
	{
		return remark;
	}
	public void setRemark(String remark)
	{
		this.remark = remark;
	}
	
	
}
