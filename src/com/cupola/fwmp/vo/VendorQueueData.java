package com.cupola.fwmp.vo;


public class VendorQueueData
{
	private Long ticketId;
	private String workOrderNumber;
	private String estimatedStartTime;
	private String estimatedEndTime;
	
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getEstimatedStartTime() {
		return estimatedStartTime;
	}
	public void setEstimatedStartTime(String estimatedStartTime) {
		this.estimatedStartTime = estimatedStartTime;
	}
	public String getEstimatedEndTime() {
		return estimatedEndTime;
	}
	public void setEstimatedEndTime(String estimatedEndTime) {
		this.estimatedEndTime = estimatedEndTime;
	}
}
