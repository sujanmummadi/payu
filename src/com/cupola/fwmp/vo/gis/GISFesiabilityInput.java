/**
 * 
 */
package com.cupola.fwmp.vo.gis;

/**
 * @author aditya
 * 
 */
public class GISFesiabilityInput {
	
	private String customerId;
	private String phoneNo;
	private String latitude;
	private String longitude;
	private String transactionNo;
	private String city;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "GISFesiabilityInput [customerId=" + customerId + ", phoneNo="
				+ phoneNo + ", latitude=" + latitude + ", longitude="
				+ longitude + ", transactionNo=" + transactionNo + ", city="
				+ city + "]";
	}

}
