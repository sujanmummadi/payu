/**
 * 
 */
package com.cupola.fwmp.vo.gis;

import java.util.List;

/**
 * @author aditya
 * 
 */
public class GISResponseVO
{

	private String type;
	private String city;
	private String branch;
	private String area;
	private String clusterName;
	private String transactionNo;
	private String message;
	private String errorNo;

	private String fxName;
	private String fxIpAddress;
	private String fxMacAddress;
	private List<String> fxPorts;

	private String cxName;
	private String cxIpAddress;
	private String cxMacAddress;
	private List<String> cxPorts;
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getBranch()
	{
		return branch;
	}
	public void setBranch(String branch)
	{
		this.branch = branch;
	}
	public String getArea()
	{
		return area;
	}
	public void setArea(String area)
	{
		this.area = area;
	}
	public String getClusterName()
	{
		return clusterName;
	}
	public void setClusterName(String clusterName)
	{
		this.clusterName = clusterName;
	}
	public String getTransactionNo()
	{
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo)
	{
		this.transactionNo = transactionNo;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getErrorNo()
	{
		return errorNo;
	}
	public void setErrorNo(String errorNo)
	{
		this.errorNo = errorNo;
	}
	public String getFxName()
	{
		return fxName;
	}
	public void setFxName(String fxName)
	{
		this.fxName = fxName;
	}
	public String getFxIpAddress()
	{
		return fxIpAddress;
	}
	public void setFxIpAddress(String fxIpAddress)
	{
		this.fxIpAddress = fxIpAddress;
	}
	public String getFxMacAddress()
	{
		return fxMacAddress;
	}
	public void setFxMacAddress(String fxMacAddress)
	{
		this.fxMacAddress = fxMacAddress;
	}
	public List<String> getFxPorts()
	{
		return fxPorts;
	}
	public void setFxPorts(List<String> fxPorts)
	{
		this.fxPorts = fxPorts;
	}
	public String getCxName()
	{
		return cxName;
	}
	public void setCxName(String cxName)
	{
		this.cxName = cxName;
	}
	public String getCxIpAddress()
	{
		return cxIpAddress;
	}
	public void setCxIpAddress(String cxIpAddress)
	{
		this.cxIpAddress = cxIpAddress;
	}
	public String getCxMacAddress()
	{
		return cxMacAddress;
	}
	public void setCxMacAddress(String cxMacAddress)
	{
		this.cxMacAddress = cxMacAddress;
	}
	public List<String> getCxPorts()
	{
		return cxPorts;
	}
	public void setCxPorts(List<String> cxPorts)
	{
		this.cxPorts = cxPorts;
	}
	@Override
	public String toString()
	{
		return "GISResponseVO [type=" + type + ", city=" + city + ", branch="
				+ branch + ", area=" + area + ", clusterName=" + clusterName
				+ ", transactionNo=" + transactionNo + ", message=" + message
				+ ", errorNo=" + errorNo + ", fxName=" + fxName
				+ ", fxIpAddress=" + fxIpAddress + ", fxMacAddress="
				+ fxMacAddress + ", fxPorts=" + fxPorts + ", cxName=" + cxName
				+ ", cxIpAddress=" + cxIpAddress + ", cxMacAddress="
				+ cxMacAddress + ", cxPorts=" + cxPorts + "]";
	}

}
