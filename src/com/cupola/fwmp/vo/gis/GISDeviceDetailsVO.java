/**
 * 
 */
package com.cupola.fwmp.vo.gis;

import java.util.List;

/**
 * @author aditya
 * 
 */
public class GISDeviceDetailsVO {

	private String name;
	private String ipAddress;
	private String macAddress;
	private List<String> availablePorts;

	public GISDeviceDetailsVO() {
	}

	public GISDeviceDetailsVO(String name, String ipAddress, String macAddress,
			List<String> availablePorts) {
		this.name = name;
		this.ipAddress = ipAddress;
		this.macAddress = macAddress;
		this.availablePorts = availablePorts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public List<String> getAvailablePorts() {
		return availablePorts;
	}

	public void setAvailablePorts(List<String> availablePorts) {
		this.availablePorts = availablePorts;
	}

	@Override
	public String toString() {
		return "GISDeviceDetailsVO [name=" + name + ", ipAddress=" + ipAddress
				+ ", macAddress=" + macAddress + ", availablePorts="
				+ availablePorts + "]";
	}

}
