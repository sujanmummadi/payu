/**
 * 
 */
package com.cupola.fwmp.vo.gis;

/**
 * @author aditya
 * 
 */
public class GisFxVO
{
	private String fxName;
	private String fxIpAddress;
	private String fxMacAddress;
	private String fxPorts;

	public GisFxVO()
	{
	}

	public GisFxVO(String fxName, String fxIpAddress, String fxMacAddress,
			String fxPorts)
	{
		this.fxName = fxName;
		this.fxIpAddress = fxIpAddress;
		this.fxMacAddress = fxMacAddress;
		this.fxPorts = fxPorts;
	}

	public String getFxName()
	{
		return fxName;
	}

	public void setFxName(String fxName)
	{
		this.fxName = fxName;
	}

	public String getFxIpAddress()
	{
		return fxIpAddress;
	}

	public void setFxIpAddress(String fxIpAddress)
	{
		this.fxIpAddress = fxIpAddress;
	}

	public String getFxMacAddress()
	{
		return fxMacAddress;
	}

	public void setFxMacAddress(String fxMacAddress)
	{
		this.fxMacAddress = fxMacAddress;
	}

	public String getFxPorts()
	{
		return fxPorts;
	}

	public void setFxPorts(String fxPorts)
	{
		this.fxPorts = fxPorts;
	}

	@Override
	public String toString()
	{
		return "GisFxVO [fxName=" + fxName + ", fxIpAddress=" + fxIpAddress
				+ ", fxMacAddress=" + fxMacAddress + ", fxPorts=" + fxPorts
				+ "]";
	}

}
