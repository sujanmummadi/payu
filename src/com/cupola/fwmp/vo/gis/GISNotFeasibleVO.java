/**
 * 
 */
package com.cupola.fwmp.vo.gis;

/**
 * @author aditya
 * 
 */
public class GISNotFeasibleVO
{
	private Long ticketId;
	private String remarks;
	private String prospectType;
	private String longitude;
	private String lattitude;

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getProspectType()
	{
		return prospectType;
	}

	public void setProspectType(String prospectType)
	{
		this.prospectType = prospectType;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getLattitude()
	{
		return lattitude;
	}

	public void setLattitude(String lattitude)
	{
		this.lattitude = lattitude;
	}

	@Override
	public String toString()
	{
		return "GISNotFeasibleVO [ticketId=" + ticketId + ", remarks="
				+ remarks + ", prospectType=" + prospectType + ", longitude="
				+ longitude + ", lattitude=" + lattitude + "]";
	}
}
