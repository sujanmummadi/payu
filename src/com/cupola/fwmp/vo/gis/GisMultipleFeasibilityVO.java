/**
 * 
 */
package com.cupola.fwmp.vo.gis;

import java.util.List;
import java.util.Map;

/**
 * @author aditya
 * 
 */
public class GisMultipleFeasibilityVO
{
	private String type;
	private String city;
	private String branch;
	private String area;
	private String clusterName;
	private String transactionNo;
	private String message;
	private String errorNo;
	private Map<String, List<String>> fxNameListCxMap;
	private Map<String, GisCxVO> cxNameCxDetailsMap;
	private Map<String, GisFxVO> fxNameFxDetailsMap;
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getBranch()
	{
		return branch;
	}
	public void setBranch(String branch)
	{
		this.branch = branch;
	}
	public String getArea()
	{
		return area;
	}
	public void setArea(String area)
	{
		this.area = area;
	}
	public String getClusterName()
	{
		return clusterName;
	}
	public void setClusterName(String clusterName)
	{
		this.clusterName = clusterName;
	}
	public String getTransactionNo()
	{
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo)
	{
		this.transactionNo = transactionNo;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getErrorNo()
	{
		return errorNo;
	}
	public void setErrorNo(String errorNo)
	{
		this.errorNo = errorNo;
	}
	public Map<String, List<String>> getFxNameListCxMap()
	{
		return fxNameListCxMap;
	}
	public void setFxNameListCxMap(Map<String, List<String>> fxNameListCxMap)
	{
		this.fxNameListCxMap = fxNameListCxMap;
	}
	public Map<String, GisCxVO> getCxNameCxDetailsMap()
	{
		return cxNameCxDetailsMap;
	}
	public void setCxNameCxDetailsMap(Map<String, GisCxVO> cxNameCxDetailsMap)
	{
		this.cxNameCxDetailsMap = cxNameCxDetailsMap;
	}
	public Map<String, GisFxVO> getFxNameFxDetailsMap()
	{
		return fxNameFxDetailsMap;
	}
	public void setFxNameFxDetailsMap(Map<String, GisFxVO> fxNameFxDetailsMap)
	{
		this.fxNameFxDetailsMap = fxNameFxDetailsMap;
	}
	@Override
	public String toString()
	{
		return "GisMultipleFeasibilityVO [type=" + type + ", city=" + city
				+ ", branch=" + branch + ", area=" + area + ", clusterName="
				+ clusterName + ", transactionNo=" + transactionNo
				+ ", message=" + message + ", errorNo=" + errorNo
				+ ", fxNameListCxMap=" + fxNameListCxMap
				+ ", cxNameCxDetailsMap=" + cxNameCxDetailsMap
				+ ", fxNameFxDetailsMap=" + fxNameFxDetailsMap + "]";
	}
}
