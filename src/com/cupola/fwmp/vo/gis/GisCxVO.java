/**
 * 
 */
package com.cupola.fwmp.vo.gis;

/**
 * @author aditya
 * 
 */
public class GisCxVO
{

	private String cxName;
	private String cxIpAddress;
	private String cxMacAddress;
	private String cxPorts;

	public GisCxVO()
	{
	}

	public GisCxVO(String cxName, String cxIpAddress, String cxMacAddress,
			String cxPorts)
	{
		this.cxName = cxName;
		this.cxIpAddress = cxIpAddress;
		this.cxMacAddress = cxMacAddress;
		this.cxPorts = cxPorts;
	}

	public String getCxName()
	{
		return cxName;
	}

	public void setCxName(String cxName)
	{
		this.cxName = cxName;
	}

	public String getCxIpAddress()
	{
		return cxIpAddress;
	}

	public void setCxIpAddress(String cxIpAddress)
	{
		this.cxIpAddress = cxIpAddress;
	}

	public String getCxMacAddress()
	{
		return cxMacAddress;
	}

	public void setCxMacAddress(String cxMacAddress)
	{
		this.cxMacAddress = cxMacAddress;
	}

	public String getCxPorts()
	{
		return cxPorts;
	}

	public void setCxPorts(String cxPorts)
	{
		this.cxPorts = cxPorts;
	}

	@Override
	public String toString()
	{
		return "GisCxVO [cxName=" + cxName + ", cxIpAddress=" + cxIpAddress
				+ ", cxMacAddress=" + cxMacAddress + ", cxPorts=" + cxPorts
				+ "]";
	}

}
