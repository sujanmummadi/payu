/**
 * 
 */
package com.cupola.fwmp.vo.gis;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author aditya
 * 
 */
@Document(collection = "GisLog")
public class GisLog
{
	@Id
	private Long gisLogId;
	private String customerId;
	private String phoneNo;
	private String latitude;
	private String longitude;
	private String transactionNo;
	private String city;
	private Long addedBy;
	private String url;

	private String requestTime;
	private String requestString;

	private String responseTime;
	private String responseString;

	public Long getGisLogId()
	{
		return gisLogId;
	}

	public void setGisLogId(Long gisLogId)
	{
		this.gisLogId = gisLogId;
	}

	public String getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(String customerId)
	{
		this.customerId = customerId;
	}

	public String getPhoneNo()
	{
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo)
	{
		this.phoneNo = phoneNo;
	}

	public String getLatitude()
	{
		return latitude;
	}

	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getTransactionNo()
	{
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo)
	{
		this.transactionNo = transactionNo;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public String getRequestTime()
	{
		return requestTime;
	}

	public void setRequestTime(String requestTime)
	{
		this.requestTime = requestTime;
	}

	public String getRequestString()
	{
		return requestString;
	}

	public void setRequestString(String requestString)
	{
		this.requestString = requestString;
	}

	public String getResponseTime()
	{
		return responseTime;
	}

	public void setResponseTime(String responseTime)
	{
		this.responseTime = responseTime;
	}

	public String getResponseString()
	{
		return responseString;
	}

	public void setResponseString(String responseString)
	{
		this.responseString = responseString;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	@Override
	public String toString()
	{
		return "GisLog [gisLogId=" + gisLogId + ", customerId=" + customerId
				+ ", phoneNo=" + phoneNo + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", transactionNo="
				+ transactionNo + ", city=" + city + ", addedBy=" + addedBy
				+ ", url=" + url + ", requestTime=" + requestTime
				+ ", requestString=" + requestString + ", responseTime="
				+ responseTime + ", responseString=" + responseString + "]";
	}

}
