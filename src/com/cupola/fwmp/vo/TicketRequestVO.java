package com.cupola.fwmp.vo;

public class TicketRequestVO implements java.io.Serializable  {
		private String wfName;
	    private long workOrderNumber;
	    private int workStageId;
	    private long userId;
	    private String userName;
	    private int priority;
	    private String cityName;
	    private String branchName;
	    private String areaName;
	    private long areaId;
	    private String ack;    
	    private int currentPhase;
	    private long ticketId;
		private int currentProgress;
		private String prospectNo;
		private String workOrderType;
		private String status;
		private String source;
		private String commitedEtr;
		private String customerId;
		private String mqId;
		private String customerName;
		private String customerAddress;
		private String mobileNumber;
		private String subAreaId;
		private String subAreaName;
		private long assignedBy;
		private long assignedTo;
		private String phaseName;
		public String getWfName() {
			return wfName;
		}
		public void setWfName(String wfName) {
			this.wfName = wfName;
		}
		public long getWorkOrderNumber() {
			return workOrderNumber;
		}
		public void setWorkOrderNumber(long workOrderNumber) {
			this.workOrderNumber = workOrderNumber;
		}
		public int getWorkStageId() {
			return workStageId;
		}
		public void setWorkStageId(int workStageId) {
			this.workStageId = workStageId;
		}
		public long getUserId() {
			return userId;
		}
		public void setUserId(long userId) {
			this.userId = userId;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public int getPriority() {
			return priority;
		}
		public void setPriority(int priority) {
			this.priority = priority;
		}
		public String getCityName() {
			return cityName;
		}
		public void setCityName(String cityName) {
			this.cityName = cityName;
		}
		public String getBranchName() {
			return branchName;
		}
		public void setBranchName(String branchName) {
			this.branchName = branchName;
		}
		public String getAreaName() {
			return areaName;
		}
		public void setAreaName(String areaName) {
			this.areaName = areaName;
		}
		public long getAreaId() {
			return areaId;
		}
		public void setAreaId(long areaId) {
			this.areaId = areaId;
		}
		public String getAck() {
			return ack;
		}
		public void setAck(String ack) {
			this.ack = ack;
		}
		public int getCurrentPhase() {
			return currentPhase;
		}
		public void setCurrentPhase(int currentPhase) {
			this.currentPhase = currentPhase;
		}
		public long getTicketId() {
			return ticketId;
		}
		public void setTicketId(long ticketId) {
			this.ticketId = ticketId;
		}
		public int getCurrentProgress() {
			return currentProgress;
		}
		public void setCurrentProgress(int currentProgress) {
			this.currentProgress = currentProgress;
		}
		public String getProspectNo() {
			return prospectNo;
		}
		public void setProspectNo(String prospectNo) {
			this.prospectNo = prospectNo;
		}
		public String getWorkOrderType() {
			return workOrderType;
		}
		public void setWorkOrderType(String workOrderType) {
			this.workOrderType = workOrderType;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getSource() {
			return source;
		}
		public void setSource(String source) {
			this.source = source;
		}
		public String getCommitedEtr() {
			return commitedEtr;
		}
		public void setCommitedEtr(String commitedEtr) {
			this.commitedEtr = commitedEtr;
		}
		public String getCustomerId() {
			return customerId;
		}
		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}
		public String getMqId() {
			return mqId;
		}
		public void setMqId(String mqId) {
			this.mqId = mqId;
		}
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		public String getCustomerAddress() {
			return customerAddress;
		}
		public void setCustomerAddress(String customerAddress) {
			this.customerAddress = customerAddress;
		}
		public String getMobileNumber() {
			return mobileNumber;
		}
		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}
		public String getSubAreaId() {
			return subAreaId;
		}
		public void setSubAreaId(String subAreaId) {
			this.subAreaId = subAreaId;
		}
		public String getSubAreaName() {
			return subAreaName;
		}
		public void setSubAreaName(String subAreaName) {
			this.subAreaName = subAreaName;
		}
		public long getAssignedBy() {
			return assignedBy;
		}
		public void setAssignedBy(long assignedBy) {
			this.assignedBy = assignedBy;
		}
		public long getAssignedTo() {
			return assignedTo;
		}
		public void setAssignedTo(long assignedTo) {
			this.assignedTo = assignedTo;
		}
		public String getPhaseName() {
			return phaseName;
		}
		public void setPhaseName(String phaseName) {
			this.phaseName = phaseName;
		}
		@Override
		public String toString() {
			return "TicketRequestVO [wfName=" + wfName + ", workOrderNumber="
					+ workOrderNumber + ", workStageId=" + workStageId
					+ ", userId=" + userId + ", userName=" + userName
					+ ", priority=" + priority + ", cityName=" + cityName
					+ ", branchName=" + branchName + ", areaName=" + areaName
					+ ", areaId=" + areaId + ", ack=" + ack + ", currentPhase="
					+ currentPhase + ", ticketId=" + ticketId
					+ ", currentProgress=" + currentProgress + ", prospectNo="
					+ prospectNo + ", workOrderType=" + workOrderType
					+ ", status=" + status + ", source=" + source
					+ ", commitedEtr=" + commitedEtr + ", customerId="
					+ customerId + ", mqId=" + mqId + ", customerName="
					+ customerName + ", customerAddress=" + customerAddress
					+ ", mobileNumber=" + mobileNumber + ", subAreaId="
					+ subAreaId + ", subAreaName=" + subAreaName
					+ ", assignedBy=" + assignedBy + ", assignedTo="
					+ assignedTo + ", phaseName=" + phaseName
					+ ", getWfName()=" + getWfName()
					+ ", getWorkOrderNumber()=" + getWorkOrderNumber()
					+ ", getWorkStageId()=" + getWorkStageId()
					+ ", getUserId()=" + getUserId() + ", getUserName()="
					+ getUserName() + ", getPriority()=" + getPriority()
					+ ", getCityName()=" + getCityName() + ", getBranchName()="
					+ getBranchName() + ", getAreaName()=" + getAreaName()
					+ ", getAreaId()=" + getAreaId() + ", getAck()=" + getAck()
					+ ", getCurrentPhase()=" + getCurrentPhase()
					+ ", getTicketId()=" + getTicketId()
					+ ", getCurrentProgress()=" + getCurrentProgress()
					+ ", getProspectNo()=" + getProspectNo()
					+ ", getWorkOrderType()=" + getWorkOrderType()
					+ ", getStatus()=" + getStatus() + ", getSource()="
					+ getSource() + ", getCommitedEtr()=" + getCommitedEtr()
					+ ", getCustomerId()=" + getCustomerId() + ", getMqId()="
					+ getMqId() + ", getCustomerName()=" + getCustomerName()
					+ ", getCustomerAddress()=" + getCustomerAddress()
					+ ", getMobileNumber()=" + getMobileNumber()
					+ ", getSubAreaId()=" + getSubAreaId()
					+ ", getSubAreaName()=" + getSubAreaName()
					+ ", getAssignedBy()=" + getAssignedBy()
					+ ", getAssignedTo()=" + getAssignedTo()
					+ ", getPhaseName()=" + getPhaseName() + ", getClass()="
					+ getClass() + ", hashCode()=" + hashCode()
					+ ", toString()=" + super.toString() + "]";
		}
		
}