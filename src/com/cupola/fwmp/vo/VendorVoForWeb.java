/**
 * 
 */
package com.cupola.fwmp.vo;

import java.util.List;
import java.util.Map;

/**
 * @author pawan
 * 
 */
public class VendorVoForWeb {

	private long vendorId;
	private String vendorName;
	
	private List<SkillCountVO> skillCountList;
	


	public long getVendorId() {
		return vendorId;
	}

	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public List<SkillCountVO> getSkillCountList() {
		return skillCountList;
	}

	public void setSkillCountList(List<SkillCountVO> skillCountList) {
		this.skillCountList = skillCountList;
	}

	@Override
	public String toString() {
		return "VendorVoForWeb [vendorId=" + vendorId + ", vendorName="
				+ vendorName + ", skillCountList=" + skillCountList + "]";
	}



	

	

}
