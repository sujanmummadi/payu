package com.cupola.fwmp.vo;

public class FWMPPFMWorkderVo {
	private String acknowledgement;
	private String correlationId;
	public String getAcknowledgement() {
		return acknowledgement;
	}
	public void setAcknowledgement(String acknowledgement) {
		this.acknowledgement = acknowledgement;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}


	

}
