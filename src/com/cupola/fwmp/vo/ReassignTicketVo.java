/**
 * 
 */
package com.cupola.fwmp.vo;

import java.util.List;

/**
 * @author aditya
 */

public class ReassignTicketVo
{
	private List<Long> ticketIds;
	private String recipientUserId;
	private String donnerUserId;
	private String reason;
	private String remark;
	private int status;
	private boolean workOrderGenerated;
	
	public List<Long> getTicketIds()
	{
		return ticketIds;
	}
	public void setTicketIds(List<Long> ticketIds)
	{
		this.ticketIds = ticketIds;
	}
	public String getRecipientUserId()
	{
		return recipientUserId;
	}
	public void setRecipientUserId(String recipientUserId)
	{
		this.recipientUserId = recipientUserId;
	}
	public String getDonnerUserId()
	{
		return donnerUserId;
	}
	public void setDonnerUserId(String donnerUserId)
	{
		this.donnerUserId = donnerUserId;
	}
	public String getReason()
	{
		return reason;
	}
	public void setReason(String reason)
	{
		this.reason = reason;
	}
	public String getRemark()
	{
		return remark;
	}
	public void setRemark(String remark)
	{
		this.remark = remark;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	public boolean isWorkOrderGenerated()
	{
		return workOrderGenerated;
	}
	public void setWorkOrderGenerated(boolean workOrderGenerated)
	{
		this.workOrderGenerated = workOrderGenerated;
	}
	@Override
	public String toString()
	{
		return "ReassignTicketVo [ticketIds=" + ticketIds + ", recipientUserId="
				+ recipientUserId + ", donnerUserId=" + donnerUserId
				+ ", reason=" + reason + ", remark=" + remark + ", status="
				+ status + ", workOrderGenerated=" + workOrderGenerated + "]";
	}

}
