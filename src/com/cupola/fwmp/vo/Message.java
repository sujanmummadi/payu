package com.cupola.fwmp.vo;

public class Message
{
	long userId;
	String message;
	
	
	public Message(){}
	
	
	public Message(long userId, String message)
	{
		super();
		this.userId = userId;
		this.message = message;
	}
	
	
	public long getUserId()
	{
		return userId;
	}
	public void setUserId(long userId)
	{
		this.userId = userId;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	@Override
	public String toString()
	{
		return "Message [userId=" + userId + ", message=" + message + "]";
	}
	
}
