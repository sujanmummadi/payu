package com.cupola.fwmp.vo;

import java.util.Map;

public class MaterialConsumptionVO
{

	private String ticketId;
	
	private Map<String, String> subActivityDetail;
	
	private String totalConsumption;

	
	
	
	public String getTotalConsumption()
	{
		return totalConsumption;
	}

	public void setTotalConsumption(String totalConsumption)
	{
		this.totalConsumption = totalConsumption;
	}

	
	public Map<String, String> getSubActivityDetail()
	{
		return subActivityDetail;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public void setSubActivityDetail(Map<String, String> subActivityDetail)
	{
		this.subActivityDetail = subActivityDetail;
	}

	@Override
	public String toString() {
		return "MaterialConsumptionVO [ticketId=" + ticketId
				+ ", subActivityDetail=" + subActivityDetail
				+ ", totalConsumption=" + totalConsumption + "]";
	}

	
	

	
	

}
