package com.cupola.fwmp.vo;

import java.util.Date;

public class AreaVO
{

	private long id;
	private Long branchId;
	private String branchName;
	private String branchCode;
	private String areaName;
	private String areaCode;
	private String cityCode;
	private String areaDescription;
	private Long addedBy;
	private Date addedOn;
	private Date modifiedOn;
	private Long modifiedBy;
	private Integer status;
	private String remarks;
	
	public String getRemarks()
	{
		return remarks;
	}
	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public String getBranchName()
	{
		return branchName;
	}
	public void setBranchName(String branchName)
	{
		this.branchName = branchName;
	}
	public String getAreaName()
	{
		return areaName;
	}
	public void setAreaName(String areaName)
	{
		this.areaName = areaName;
	}
	public String getAreaDescription()
	{
		return areaDescription;
	}
	public void setAreaDescription(String areaDescription)
	{
		this.areaDescription = areaDescription;
	}
	public Long getAddedBy()
	{
		return addedBy;
	}
	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}
	public Date getAddedOn()
	{
		return addedOn;
	}
	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}
	public Date getModifiedOn()
	{
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}
	public Long getModifiedBy()
	{
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	public Integer getStatus()
	{
		return status;
	}
	public void setStatus(Integer status)
	{
		this.status = status;
	}
	@Override
	public String toString()
	{
		return "AreaVO [id=" + id + ", branchId=" + branchId + ", branchName="
				+ branchName + ", branchCode=" + branchCode + ", areaName="
				+ areaName + ", areaCode=" + areaCode + ", cityCode="
				+ cityCode + "]";
	}
	public String getBranchCode()
	{
		return branchCode;
	}
	public void setBranchCode(String branchCode)
	{
		this.branchCode = branchCode;
	}
	public String getAreaCode()
	{
		return areaCode;
	}
	public void setAreaCode(String areaCode)
	{
		this.areaCode = areaCode;
	}
	public String getCityCode()
	{
		return cityCode;
	}
	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}
	public Long getBranchId()
	{
		return branchId;
	}
	public void setBranchId(Long branchId)
	{
		this.branchId = branchId;
	}
}
