package com.cupola.fwmp.vo;

public class TicketApprovalVo
{

	private String ticketId;
	private int approvalStatus;

	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	public int getApprovalStatus()
	{
		return approvalStatus;
	}

	public void setApprovalStatus(int approvalStatus)
	{
		this.approvalStatus = approvalStatus;
	}

	@Override
	public String toString()
	{
		return "TicketApprovalVo [ticketId=" + ticketId + ", approvalStatus="
				+ approvalStatus + "]";
	}

}
