package com.cupola.fwmp.vo;

public class FrSymptomCodesVo {

	private Long id;
	private Long symptomCodes;
	private int flowId;
	private String symptomNames;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSymptomCodes() {
		return symptomCodes;
	}
	public void setSymptomCodes(Long symptomCodes) {
		this.symptomCodes = symptomCodes;
	}
	public int getFlowId() {
		return flowId;
	}
	public void setFlowId(int flowId) {
		this.flowId = flowId;
	}
	public String getSymptomNames() {
		return symptomNames;
	}
	public void setSymptomNames(String symptomNames) {
		this.symptomNames = symptomNames;
	}
	@Override
	public String toString() {
		return "FrSymptomCodesVo [id=" + id + ", symptomCodes=" + symptomCodes + ", flowId=" + flowId
				+ ", symptomNames=" + symptomNames + "]";
	}
	
}
