package com.cupola.fwmp.vo;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class TicketDetailVo
{

	

	// "SELECT wo.id as woId, wo.workOrderTypeId, wo.status, wo.totalPriority,
	// wo.currentProgress,"
	// + " t.source,t.commitedETR, c.id as cId"
	// +
	// " FROM workorder wo, ticket t, customer c where t.id = wo.ticketId and
	// t.customerId = c.id ";
	//
	@JsonFormat(shape=Shape.STRING)
	private String id;
	private long workOrderId;
	private String workOrderNumber;
	private String cafNumber;
	private long workOrderTypeId;
	private long statusId;

	private int currentProgress;
	private String prospectNo;
	private String ticketCategory;
	private String ticketSubCategory;
	private String prospectType;
	private String prospectColdReason;

	private String workOrderType;
	private String status;
	private int documentStatus = TicketStatus.OPEN;
	private int paymentStatus = TicketStatus.OPEN;

	private String remarks;
	private String source;
	private String commitedEtr;
	private String currentEtr;
	private String preferedDate;
	private String customerId;
	private String accountNumber;
	private int mqStatus;
	private int mqRetryCount;
	private String customerName;
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String currentAddress;// Customer current address
	private String permanentAddress;// Customer Aadhaar address,per
	private String communicationAddress;// Customer biling address,commu
	private String customerProfile;//profile
	private String officeNumber;
	private String pincode;
	private String city;
	private long cityId;
	private long branchId;
	private String branch;
	private String mobileNumber;
	private String customerAltMobile;
	private String customerEmail;
	private String customerAltEmail;

	private String longitude;
	private String lattitude;

	private String planName;
	private long planId = TicketStatus.OPEN;
	private String planType;
	private String planAmount;
	private String planCategoery;

	private boolean routerRequired;
	private String routerDescription;
	private boolean externalRouterRequired;
	private String externalRouterDescription;
	private String externalRouterAmount;

	private long areaId;
	private String areaName;

	private String subAreaId;
	private String subAreaName;
	private long initialWorkStageId;
	private String initialWorkStage; // workstage name started with
	private List<ActivityVo> activities;

	private String assignedToDisplayName;
	private long assignedToId;

	private boolean etrApprovalPending;
	private String etrApprovalValue;

	private String createdDate;

	private String paymentRefNumber;
	private String paidAmount;

	private String sourceModeId;
	private String enquiryModeId;
	private Long ticketSubCategoryId;

	private String installationCharges;// need to add
	private String additionalCharges;// need to add
	private String paymentMode;// need to add
	private String cxPermission;// need to add
	private String gxPermission;// need

	private int priority;
	private int updatedCount;
	private int escalatedValue;
	private int sliderEscalatedValue;
	private int priorityEscalationType;

	private boolean fiberFeasible = false;

	private String connectionType;
	private String fxName;
	private String fxIpAddress;
	private String fxMacAddress;
	private String fxPorts;
	private String cxName;
	private String cxIpAddress;
	private String cxMacAddress;
	private String cxPorts;
	private String paymentRemark;
	private String documentRemark;
	private Set<TypeAheadVo> userRoles;
	private String crpMobileNumber;
	private String crpAccountNumber;
	private String couponCode;
	private String nationality;
	private String existingIsp;
	private Boolean isStaticIPRequired;
	private List<CustomerAddressVO> customerAddressesList;
	private String uinType;
	private String uin;

	private Map<Integer, List<Byte>> ticketStatusMap;
	private Map<Long, GISPojo> feasibilityTypeDetails;
	
	private String neComment;
	private String seComment;
	private boolean isEditable;

	private String paidConnection;
	private String assignedBy;
	
	private String modifiedOn;
	

	private boolean aadhaarForPOI;
	private boolean aadhaarForPOA;
	
	private long poiStatusId;
	private long poaStatusId;
	
//	@JsonIgnore
	private KycDetails adhaarDetails;
	
	private String poaRemarks;
	private String poiRemarks;
	
	private String finahubTransactionId;
	
	private String schemeCode;
	private String discountAmount;
	private String poaDocumentRemark;
	private String poiDocumentRemark;	
	
	private String staticIpPriceWithTax;
	
	//private String staticIpPriceWithoutTax;
	
	private int deDupFlag;
	private String deDupMessage;
	
	private String aadhaarNumber;
	
	String appointmentDate;

	/**
	 * @return the deDupFlag
	 */
	public int getDeDupFlag() {
		return deDupFlag;
	}

	/**
	 * @param deDupFlag the deDupFlag to set
	 */
	public void setDeDupFlag(int deDupFlag) {
		this.deDupFlag = deDupFlag;
	}

	/**
	 * @return the deDupMessage
	 */
	public String getDeDupMessage() {
		return deDupMessage;
	}

	/**
	 * @param deDupMessage the deDupMessage to set
	 */
	public void setDeDupMessage(String deDupMessage) {
		this.deDupMessage = deDupMessage;
	}

	public TicketDetailVo(){
		
	}

	/**
	 * @param workOrderId
	 * @param workOrderTypeId
	 * @param statusId
	 * @param currentProgress
	 * @param priority
	 */
	public TicketDetailVo(long workOrderId, long workOrderTypeId, long statusId, int currentProgress, int priority) {
		super();
		this.workOrderId = workOrderId;
		this.workOrderTypeId = workOrderTypeId;
		this.statusId = statusId;
		this.currentProgress = currentProgress;
		this.priority = priority;
	}
	/**
	 * @return the workOrderId
	 */
	public long getWorkOrderId() {
		return workOrderId;
	}

	/**
	 * @param workOrderId the workOrderId to set
	 */
	public void setWorkOrderId(long workOrderId) {
		this.workOrderId = workOrderId;
	}

	/**
	 * @return the workOrderNumber
	 */
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	/**
	 * @param workOrderNumber the workOrderNumber to set
	 */
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	/**
	 * @return the cafNumber
	 */
	public String getCafNumber() {
		return cafNumber;
	}

	/**
	 * @param cafNumber the cafNumber to set
	 */
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}

	/**
	 * @return the workOrderTypeId
	 */
	public long getWorkOrderTypeId() {
		return workOrderTypeId;
	}

	/**
	 * @param workOrderTypeId the workOrderTypeId to set
	 */
	public void setWorkOrderTypeId(long workOrderTypeId) {
		this.workOrderTypeId = workOrderTypeId;
	}

	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the currentProgress
	 */
	public int getCurrentProgress() {
		return currentProgress;
	}

	/**
	 * @param currentProgress the currentProgress to set
	 */
	public void setCurrentProgress(int currentProgress) {
		this.currentProgress = currentProgress;
	}

	/**
	 * @return the prospectNo
	 */
	public String getProspectNo() {
		return prospectNo;
	}

	/**
	 * @param prospectNo the prospectNo to set
	 */
	public void setProspectNo(String prospectNo) {
		this.prospectNo = prospectNo;
	}

	/**
	 * @return the ticketCategory
	 */
	public String getTicketCategory() {
		return ticketCategory;
	}

	/**
	 * @param ticketCategory the ticketCategory to set
	 */
	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}

	/**
	 * @return the ticketSubCategory
	 */
	public String getTicketSubCategory() {
		return ticketSubCategory;
	}

	/**
	 * @param ticketSubCategory the ticketSubCategory to set
	 */
	public void setTicketSubCategory(String ticketSubCategory) {
		this.ticketSubCategory = ticketSubCategory;
	}

	/**
	 * @return the prospectType
	 */
	public String getProspectType() {
		return prospectType;
	}

	/**
	 * @param prospectType the prospectType to set
	 */
	public void setProspectType(String prospectType) {
		this.prospectType = prospectType;
	}

	/**
	 * @return the prospectColdReason
	 */
	public String getProspectColdReason() {
		return prospectColdReason;
	}

	/**
	 * @param prospectColdReason the prospectColdReason to set
	 */
	public void setProspectColdReason(String prospectColdReason) {
		this.prospectColdReason = prospectColdReason;
	}

	/**
	 * @return the workOrderType
	 */
	public String getWorkOrderType() {
		return workOrderType;
	}

	/**
	 * @param workOrderType the workOrderType to set
	 */
	public void setWorkOrderType(String workOrderType) {
		this.workOrderType = workOrderType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the documentStatus
	 */
	public int getDocumentStatus() {
		return documentStatus;
	}

	/**
	 * @param documentStatus the documentStatus to set
	 */
	public void setDocumentStatus(int documentStatus) {
		this.documentStatus = documentStatus;
	}

	/**
	 * @return the paymentStatus
	 */
	public int getPaymentStatus() {
		return paymentStatus;
	}

	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the commitedEtr
	 */
	public String getCommitedEtr() {
		return commitedEtr;
	}

	/**
	 * @param commitedEtr the commitedEtr to set
	 */
	public void setCommitedEtr(String commitedEtr) {
		this.commitedEtr = commitedEtr;
	}

	/**
	 * @return the currentEtr
	 */
	public String getCurrentEtr() {
		return currentEtr;
	}

	/**
	 * @param currentEtr the currentEtr to set
	 */
	public void setCurrentEtr(String currentEtr) {
		this.currentEtr = currentEtr;
	}

	/**
	 * @return the preferedDate
	 */
	public String getPreferedDate() {
		return preferedDate;
	}

	/**
	 * @param preferedDate the preferedDate to set
	 */
	public void setPreferedDate(String preferedDate) {
		this.preferedDate = preferedDate;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the mqStatus
	 */
	public int getMqStatus() {
		return mqStatus;
	}

	/**
	 * @param mqStatus the mqStatus to set
	 */
	public void setMqStatus(int mqStatus) {
		this.mqStatus = mqStatus;
	}

	/**
	 * @return the mqRetryCount
	 */
	public int getMqRetryCount() {
		return mqRetryCount;
	}

	/**
	 * @param mqRetryCount the mqRetryCount to set
	 */
	public void setMqRetryCount(int mqRetryCount) {
		this.mqRetryCount = mqRetryCount;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}

	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	/**
	 * @return the permanentAddress
	 */
	public String getPermanentAddress() {
		return permanentAddress;
	}

	/**
	 * @param permanentAddress the permanentAddress to set
	 */
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	/**
	 * @return the communicationAddress
	 */
	public String getCommunicationAddress() {
		return communicationAddress;
	}

	/**
	 * @param communicationAddress the communicationAddress to set
	 */
	public void setCommunicationAddress(String communicationAddress) {
		this.communicationAddress = communicationAddress;
	}

	

	/**
	 * @return the customerProfile
	 */
	public String getCustomerProfile() {
		return customerProfile;
	}

	/**
	 * @param customerProfile the customerProfile to set
	 */
	public void setCustomerProfile(String customerProfile) {
		this.customerProfile = customerProfile;
	}

	/**
	 * @return the officeNumber
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * @param officeNumber the officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the branchId
	 */
	public long getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the customerAltMobile
	 */
	public String getCustomerAltMobile() {
		return customerAltMobile;
	}

	/**
	 * @param customerAltMobile the customerAltMobile to set
	 */
	public void setCustomerAltMobile(String customerAltMobile) {
		this.customerAltMobile = customerAltMobile;
	}

	/**
	 * @return the customerEmail
	 */
	public String getCustomerEmail() {
		return customerEmail;
	}

	/**
	 * @param customerEmail the customerEmail to set
	 */
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	/**
	 * @return the customerAltEmail
	 */
	public String getCustomerAltEmail() {
		return customerAltEmail;
	}

	/**
	 * @param customerAltEmail the customerAltEmail to set
	 */
	public void setCustomerAltEmail(String customerAltEmail) {
		this.customerAltEmail = customerAltEmail;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the lattitude
	 */
	public String getLattitude() {
		return lattitude;
	}

	/**
	 * @param lattitude the lattitude to set
	 */
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}

	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}

	/**
	 * @return the planId
	 */
	public long getPlanId() {
		return planId;
	}

	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(long planId) {
		this.planId = planId;
	}

	/**
	 * @return the planType
	 */
	public String getPlanType() {
		return planType;
	}

	/**
	 * @param planType the planType to set
	 */
	public void setPlanType(String planType) {
		this.planType = planType;
	}

	/**
	 * @return the planAmount
	 */
	public String getPlanAmount() {
		return planAmount;
	}

	/**
	 * @param planAmount the planAmount to set
	 */
	public void setPlanAmount(String planAmount) {
		this.planAmount = planAmount;
	}

	/**
	 * @return the planCategoery
	 */
	public String getPlanCategoery() {
		return planCategoery;
	}

	/**
	 * @param planCategoery the planCategoery to set
	 */
	public void setPlanCategoery(String planCategoery) {
		this.planCategoery = planCategoery;
	}

	/**
	 * @return the routerRequired
	 */
	public boolean isRouterRequired() {
		return routerRequired;
	}

	/**
	 * @param routerRequired the routerRequired to set
	 */
	public void setRouterRequired(boolean routerRequired) {
		this.routerRequired = routerRequired;
	}

	/**
	 * @return the routerDescription
	 */
	public String getRouterDescription() {
		return routerDescription;
	}

	/**
	 * @param routerDescription the routerDescription to set
	 */
	public void setRouterDescription(String routerDescription) {
		this.routerDescription = routerDescription;
	}

	/**
	 * @return the externalRouterRequired
	 */
	public boolean isExternalRouterRequired() {
		return externalRouterRequired;
	}

	/**
	 * @param externalRouterRequired the externalRouterRequired to set
	 */
	public void setExternalRouterRequired(boolean externalRouterRequired) {
		this.externalRouterRequired = externalRouterRequired;
	}

	/**
	 * @return the externalRouterDescription
	 */
	public String getExternalRouterDescription() {
		return externalRouterDescription;
	}

	/**
	 * @param externalRouterDescription the externalRouterDescription to set
	 */
	public void setExternalRouterDescription(String externalRouterDescription) {
		this.externalRouterDescription = externalRouterDescription;
	}

	/**
	 * @return the externalRouterAmount
	 */
	public String getExternalRouterAmount() {
		return externalRouterAmount;
	}

	/**
	 * @param externalRouterAmount the externalRouterAmount to set
	 */
	public void setExternalRouterAmount(String externalRouterAmount) {
		this.externalRouterAmount = externalRouterAmount;
	}

	/**
	 * @return the areaId
	 */
	public long getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId the areaId to set
	 */
	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}

	/**
	 * @param areaName the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/**
	 * @return the subAreaId
	 */
	public String getSubAreaId() {
		return subAreaId;
	}

	/**
	 * @param subAreaId the subAreaId to set
	 */
	public void setSubAreaId(String subAreaId) {
		this.subAreaId = subAreaId;
	}

	/**
	 * @return the subAreaName
	 */
	public String getSubAreaName() {
		return subAreaName;
	}

	/**
	 * @param subAreaName the subAreaName to set
	 */
	public void setSubAreaName(String subAreaName) {
		this.subAreaName = subAreaName;
	}

	/**
	 * @return the initialWorkStageId
	 */
	public long getInitialWorkStageId() {
		return initialWorkStageId;
	}

	/**
	 * @param initialWorkStageId the initialWorkStageId to set
	 */
	public void setInitialWorkStageId(long initialWorkStageId) {
		this.initialWorkStageId = initialWorkStageId;
	}

	/**
	 * @return the initialWorkStage
	 */
	public String getInitialWorkStage() {
		return initialWorkStage;
	}

	/**
	 * @param initialWorkStage the initialWorkStage to set
	 */
	public void setInitialWorkStage(String initialWorkStage) {
		this.initialWorkStage = initialWorkStage;
	}

	/**
	 * @return the activities
	 */
	public List<ActivityVo> getActivities() {
		return activities;
	}

	/**
	 * @param activities the activities to set
	 */
	public void setActivities(List<ActivityVo> activities) {
		this.activities = activities;
	}

	/**
	 * @return the assignedToDisplayName
	 */
	public String getAssignedToDisplayName() {
		return assignedToDisplayName;
	}

	/**
	 * @param assignedToDisplayName the assignedToDisplayName to set
	 */
	public void setAssignedToDisplayName(String assignedToDisplayName) {
		this.assignedToDisplayName = assignedToDisplayName;
	}

	/**
	 * @return the assignedToId
	 */
	public long getAssignedToId() {
		return assignedToId;
	}

	/**
	 * @param assignedToId the assignedToId to set
	 */
	public void setAssignedToId(long assignedToId) {
		this.assignedToId = assignedToId;
	}

	/**
	 * @return the etrApprovalPending
	 */
	public boolean isEtrApprovalPending() {
		return etrApprovalPending;
	}

	/**
	 * @param etrApprovalPending the etrApprovalPending to set
	 */
	public void setEtrApprovalPending(boolean etrApprovalPending) {
		this.etrApprovalPending = etrApprovalPending;
	}

	/**
	 * @return the etrApprovalValue
	 */
	public String getEtrApprovalValue() {
		return etrApprovalValue;
	}

	/**
	 * @param etrApprovalValue the etrApprovalValue to set
	 */
	public void setEtrApprovalValue(String etrApprovalValue) {
		this.etrApprovalValue = etrApprovalValue;
	}

	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the paymentRefNumber
	 */
	public String getPaymentRefNumber() {
		return paymentRefNumber;
	}

	/**
	 * @param paymentRefNumber the paymentRefNumber to set
	 */
	public void setPaymentRefNumber(String paymentRefNumber) {
		this.paymentRefNumber = paymentRefNumber;
	}

	/**
	 * @return the paidAmount
	 */
	public String getPaidAmount() {
		return paidAmount;
	}

	/**
	 * @param paidAmount the paidAmount to set
	 */
	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}

	/**
	 * @return the sourceModeId
	 */
	public String getSourceModeId() {
		return sourceModeId;
	}

	/**
	 * @param sourceModeId the sourceModeId to set
	 */
	public void setSourceModeId(String sourceModeId) {
		this.sourceModeId = sourceModeId;
	}

	/**
	 * @return the enquiryModeId
	 */
	public String getEnquiryModeId() {
		return enquiryModeId;
	}

	/**
	 * @param enquiryModeId the enquiryModeId to set
	 */
	public void setEnquiryModeId(String enquiryModeId) {
		this.enquiryModeId = enquiryModeId;
	}

	/**
	 * @return the ticketSubCategoryId
	 */
	public Long getTicketSubCategoryId() {
		return ticketSubCategoryId;
	}

	/**
	 * @param ticketSubCategoryId the ticketSubCategoryId to set
	 */
	public void setTicketSubCategoryId(Long ticketSubCategoryId) {
		this.ticketSubCategoryId = ticketSubCategoryId;
	}

	/**
	 * @return the installationCharges
	 */
	public String getInstallationCharges() {
		return installationCharges;
	}

	/**
	 * @param installationCharges the installationCharges to set
	 */
	public void setInstallationCharges(String installationCharges) {
		this.installationCharges = installationCharges;
	}

	/**
	 * @return the additionalCharges
	 */
	public String getAdditionalCharges() {
		return additionalCharges;
	}

	/**
	 * @param additionalCharges the additionalCharges to set
	 */
	public void setAdditionalCharges(String additionalCharges) {
		this.additionalCharges = additionalCharges;
	}

	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode() {
		return paymentMode;
	}

	/**
	 * @param paymentMode the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	/**
	 * @return the cxPermission
	 */
	public String getCxPermission() {
		return cxPermission;
	}

	/**
	 * @param cxPermission the cxPermission to set
	 */
	public void setCxPermission(String cxPermission) {
		this.cxPermission = cxPermission;
	}

	/**
	 * @return the gxPermission
	 */
	public String getGxPermission() {
		return gxPermission;
	}

	/**
	 * @param gxPermission the gxPermission to set
	 */
	public void setGxPermission(String gxPermission) {
		this.gxPermission = gxPermission;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * @return the updatedCount
	 */
	public int getUpdatedCount() {
		return updatedCount;
	}

	/**
	 * @param updatedCount the updatedCount to set
	 */
	public void setUpdatedCount(int updatedCount) {
		this.updatedCount = updatedCount;
	}

	/**
	 * @return the escalatedValue
	 */
	public int getEscalatedValue() {
		return escalatedValue;
	}

	/**
	 * @param escalatedValue the escalatedValue to set
	 */
	public void setEscalatedValue(int escalatedValue) {
		this.escalatedValue = escalatedValue;
	}

	/**
	 * @return the sliderEscalatedValue
	 */
	public int getSliderEscalatedValue() {
		return sliderEscalatedValue;
	}

	/**
	 * @param sliderEscalatedValue the sliderEscalatedValue to set
	 */
	public void setSliderEscalatedValue(int sliderEscalatedValue) {
		this.sliderEscalatedValue = sliderEscalatedValue;
	}

	/**
	 * @return the priorityEscalationType
	 */
	public int getPriorityEscalationType() {
		return priorityEscalationType;
	}

	/**
	 * @param priorityEscalationType the priorityEscalationType to set
	 */
	public void setPriorityEscalationType(int priorityEscalationType) {
		this.priorityEscalationType = priorityEscalationType;
	}

	/**
	 * @return the fiberFeasible
	 */
	public boolean isFiberFeasible() {
		return fiberFeasible;
	}

	/**
	 * @param fiberFeasible the fiberFeasible to set
	 */
	public void setFiberFeasible(boolean fiberFeasible) {
		this.fiberFeasible = fiberFeasible;
	}

	/**
	 * @return the connectionType
	 */
	public String getConnectionType() {
		return connectionType;
	}

	/**
	 * @param connectionType the connectionType to set
	 */
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	/**
	 * @return the fxName
	 */
	public String getFxName() {
		return fxName;
	}

	/**
	 * @param fxName the fxName to set
	 */
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}

	/**
	 * @return the fxIpAddress
	 */
	public String getFxIpAddress() {
		return fxIpAddress;
	}

	/**
	 * @param fxIpAddress the fxIpAddress to set
	 */
	public void setFxIpAddress(String fxIpAddress) {
		this.fxIpAddress = fxIpAddress;
	}

	/**
	 * @return the fxMacAddress
	 */
	public String getFxMacAddress() {
		return fxMacAddress;
	}

	/**
	 * @param fxMacAddress the fxMacAddress to set
	 */
	public void setFxMacAddress(String fxMacAddress) {
		this.fxMacAddress = fxMacAddress;
	}

	/**
	 * @return the fxPorts
	 */
	public String getFxPorts() {
		return fxPorts;
	}

	/**
	 * @param fxPorts the fxPorts to set
	 */
	public void setFxPorts(String fxPorts) {
		this.fxPorts = fxPorts;
	}

	/**
	 * @return the cxName
	 */
	public String getCxName() {
		return cxName;
	}

	/**
	 * @param cxName the cxName to set
	 */
	public void setCxName(String cxName) {
		this.cxName = cxName;
	}

	/**
	 * @return the cxIpAddress
	 */
	public String getCxIpAddress() {
		return cxIpAddress;
	}

	/**
	 * @param cxIpAddress the cxIpAddress to set
	 */
	public void setCxIpAddress(String cxIpAddress) {
		this.cxIpAddress = cxIpAddress;
	}

	/**
	 * @return the cxMacAddress
	 */
	public String getCxMacAddress() {
		return cxMacAddress;
	}

	/**
	 * @param cxMacAddress the cxMacAddress to set
	 */
	public void setCxMacAddress(String cxMacAddress) {
		this.cxMacAddress = cxMacAddress;
	}

	/**
	 * @return the cxPorts
	 */
	public String getCxPorts() {
		return cxPorts;
	}

	/**
	 * @param cxPorts the cxPorts to set
	 */
	public void setCxPorts(String cxPorts) {
		this.cxPorts = cxPorts;
	}

	/**
	 * @return the paymentRemark
	 */
	public String getPaymentRemark() {
		return paymentRemark;
	}

	/**
	 * @param paymentRemark the paymentRemark to set
	 */
	public void setPaymentRemark(String paymentRemark) {
		this.paymentRemark = paymentRemark;
	}

	/**
	 * @return the documentRemark
	 */
	public String getDocumentRemark() {
		return documentRemark;
	}

	/**
	 * @param documentRemark the documentRemark to set
	 */
	public void setDocumentRemark(String documentRemark) {
		this.documentRemark = documentRemark;
	}

	/**
	 * @return the userRoles
	 */
	public Set<TypeAheadVo> getUserRoles() {
		return userRoles;
	}

	/**
	 * @param userRoles the userRoles to set
	 */
	public void setUserRoles(Set<TypeAheadVo> userRoles) {
		this.userRoles = userRoles;
	}

	/**
	 * @return the crpMobileNumber
	 */
	public String getCrpMobileNumber() {
		return crpMobileNumber;
	}

	/**
	 * @param crpMobileNumber the crpMobileNumber to set
	 */
	public void setCrpMobileNumber(String crpMobileNumber) {
		this.crpMobileNumber = crpMobileNumber;
	}

	/**
	 * @return the crpAccountNumber
	 */
	public String getCrpAccountNumber() {
		return crpAccountNumber;
	}

	/**
	 * @param crpAccountNumber the crpAccountNumber to set
	 */
	public void setCrpAccountNumber(String crpAccountNumber) {
		this.crpAccountNumber = crpAccountNumber;
	}

	/**
	 * @return the couponCode
	 */
	public String getCouponCode() {
		return couponCode;
	}

	/**
	 * @param couponCode the couponCode to set
	 */
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the existingIsp
	 */
	public String getExistingIsp() {
		return existingIsp;
	}

	/**
	 * @param existingIsp the existingIsp to set
	 */
	public void setExistingIsp(String existingIsp) {
		this.existingIsp = existingIsp;
	}

	

	/**
	 * @return the customerAddressesList
	 */
	public List<CustomerAddressVO> getCustomerAddressesList() {
		return customerAddressesList;
	}

	/**
	 * @param customerAddressesList the customerAddressesList to set
	 */
	public void setCustomerAddressesList(List<CustomerAddressVO> customerAddressesList) {
		this.customerAddressesList = customerAddressesList;
	}

	/**
	 * @return the uinType
	 */
	public String getUinType() {
		return uinType;
	}

	/**
	 * @param uinType the uinType to set
	 */
	public void setUinType(String uinType) {
		this.uinType = uinType;
	}

	/**
	 * @return the uin
	 */
	public String getUin() {
		return uin;
	}

	/**
	 * @param uin the uin to set
	 */
	public void setUin(String uin) {
		this.uin = uin;
	}

	/**
	 * @return the ticketStatusMap
	 */
	public Map<Integer, List<Byte>> getTicketStatusMap() {
		return ticketStatusMap;
	}

	/**
	 * @param ticketStatusMap the ticketStatusMap to set
	 */
	public void setTicketStatusMap(Map<Integer, List<Byte>> ticketStatusMap) {
		this.ticketStatusMap = ticketStatusMap;
	}

	/**
	 * @return the feasibilityTypeDetails
	 */
	public Map<Long, GISPojo> getFeasibilityTypeDetails() {
		return feasibilityTypeDetails;
	}

	/**
	 * @param feasibilityTypeDetails the feasibilityTypeDetails to set
	 */
	public void setFeasibilityTypeDetails(Map<Long, GISPojo> feasibilityTypeDetails) {
		this.feasibilityTypeDetails = feasibilityTypeDetails;
	}

	/**
	 * @return the neComment
	 */
	public String getNeComment() {
		return neComment;
	}

	/**
	 * @param neComment the neComment to set
	 */
	public void setNeComment(String neComment) {
		this.neComment = neComment;
	}

	/**
	 * @return the seComment
	 */
	public String getSeComment() {
		return seComment;
	}

	/**
	 * @param seComment the seComment to set
	 */
	public void setSeComment(String seComment) {
		this.seComment = seComment;
	}

	/**
	 * @return the isEditable
	 */
	public boolean isEditable() {
		return isEditable;
	}

	/**
	 * @param isEditable the isEditable to set
	 */
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * @return the paidConnection
	 */
	public String getPaidConnection() {
		return paidConnection;
	}

	/**
	 * @param paidConnection the paidConnection to set
	 */
	public void setPaidConnection(String paidConnection) {
		this.paidConnection = paidConnection;
	}

	/**
	 * @return the assignedBy
	 */
	public String getAssignedBy() {
		return assignedBy;
	}

	/**
	 * @param assignedBy the assignedBy to set
	 */
	public void setAssignedBy(String assignedBy) {
		this.assignedBy = assignedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	public String getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the aadhaarForPOI
	 */
	public boolean isAadhaarForPOI() {
		return aadhaarForPOI;
	}

	/**
	 * @param aadhaarForPOI the aadhaarForPOI to set
	 */
	public void setAadhaarForPOI(boolean aadhaarForPOI) {
		this.aadhaarForPOI = aadhaarForPOI;
	}

	/**
	 * @return the aadhaarForPOA
	 */
	public boolean isAadhaarForPOA() {
		return aadhaarForPOA;
	}

	/**
	 * @param aadhaarForPOA the aadhaarForPOA to set
	 */
	public void setAadhaarForPOA(boolean aadhaarForPOA) {
		this.aadhaarForPOA = aadhaarForPOA;
	}

	/**
	 * @return the poiStatusId
	 */
	public long getPoiStatusId() {
		return poiStatusId;
	}

	/**
	 * @param poiStatusId the poiStatusId to set
	 */
	public void setPoiStatusId(long poiStatusId) {
		this.poiStatusId = poiStatusId;
	}

	/**
	 * @return the poaStatusId
	 */
	public long getPoaStatusId() {
		return poaStatusId;
	}

	/**
	 * @param poaStatusId the poaStatusId to set
	 */
	public void setPoaStatusId(long poaStatusId) {
		this.poaStatusId = poaStatusId;
	}

	/**
	 * @return the adhaarDetails
	 */
	public KycDetails getAdhaarDetails() {
		return adhaarDetails;
	}

	/**
	 * @param adhaarDetails the adhaarDetails to set
	 */
	public void setAdhaarDetails(KycDetails adhaarDetails) {
		this.adhaarDetails = adhaarDetails;
	}

	/**
	 * @return the poaRemarks
	 */
	public String getPoaRemarks() {
		return poaRemarks;
	}

	/**
	 * @param poaRemarks the poaRemarks to set
	 */
	public void setPoaRemarks(String poaRemarks) {
		this.poaRemarks = poaRemarks;
	}

	/**
	 * @return the poiRemarks
	 */
	public String getPoiRemarks() {
		return poiRemarks;
	}

	/**
	 * @param poiRemarks the poiRemarks to set
	 */
	public void setPoiRemarks(String poiRemarks) {
		this.poiRemarks = poiRemarks;
	}

	/**
	 * @return the finahubTransactionId
	 */
	public String getFinahubTransactionId() {
		return finahubTransactionId;
	}

	/**
	 * @param finahubTransactionId the finahubTransactionId to set
	 */
	public void setFinahubTransactionId(String finahubTransactionId) {
		this.finahubTransactionId = finahubTransactionId;
	}

	/**
	 * @return the schemeCode
	 */
	public String getSchemeCode() {
		return schemeCode;
	}

	/**
	 * @param schemeCode the schemeCode to set
	 */
	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	/**
	 * @return the discountAmount
	 */
	public String getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}

	/**
	 * @return the poaDocumentRemark
	 */
	public String getPoaDocumentRemark() {
		return poaDocumentRemark;
	}

	/**
	 * @param poaDocumentRemark the poaDocumentRemark to set
	 */
	public void setPoaDocumentRemark(String poaDocumentRemark) {
		this.poaDocumentRemark = poaDocumentRemark;
	}

	/**
	 * @return the poiDocumentRemark
	 */
	public String getPoiDocumentRemark() {
		return poiDocumentRemark;
	}

	/**
	 * @param poiDocumentRemark the poiDocumentRemark to set
	 */
	public void setPoiDocumentRemark(String poiDocumentRemark) {
		this.poiDocumentRemark = poiDocumentRemark;
	}

	/**
	 * @return the aadhaarNumber
	 */
	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	/**
	 * @param aadhaarNumber the aadhaarNumber to set
	 */
	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}
	

	/**
	 * @return the isStaticIPRequired
	 */
	public Boolean getIsStaticIPRequired() {
		return isStaticIPRequired;
	}

	/**
	 * @param isStaticIPRequired the isStaticIPRequired to set
	 */
	public void setIsStaticIPRequired(Boolean isStaticIPRequired) {
		this.isStaticIPRequired = isStaticIPRequired;
	}
	
	
	

	/**
	 * @return the staticIpPriceWithTax
	 */
	public String getStaticIpPriceWithTax() {
		return staticIpPriceWithTax;
	}

	/**
	 * @param staticIpPriceWithTax the staticIpPriceWithTax to set
	 */
	public void setStaticIpPriceWithTax(String staticIpPriceWithTax) {
		this.staticIpPriceWithTax = staticIpPriceWithTax;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TicketDetailVo [id=" + id + ", workOrderId=" + workOrderId + ", workOrderNumber=" + workOrderNumber
				+ ", cafNumber=" + cafNumber + ", workOrderTypeId=" + workOrderTypeId + ", statusId=" + statusId
				+ ", currentProgress=" + currentProgress + ", prospectNo=" + prospectNo + ", ticketCategory="
				+ ticketCategory + ", ticketSubCategory=" + ticketSubCategory + ", prospectType=" + prospectType
				+ ", prospectColdReason=" + prospectColdReason + ", workOrderType=" + workOrderType + ", status="
				+ status + ", documentStatus=" + documentStatus + ", paymentStatus=" + paymentStatus + ", remarks="
				+ remarks + ", source=" + source + ", commitedEtr=" + commitedEtr + ", currentEtr=" + currentEtr
				+ ", preferedDate=" + preferedDate + ", customerId=" + customerId + ", accountNumber=" + accountNumber
				+ ", mqStatus=" + mqStatus + ", mqRetryCount=" + mqRetryCount + ", customerName=" + customerName
				+ ", title=" + title + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", currentAddress=" + currentAddress + ", permanentAddress=" + permanentAddress
				+ ", communicationAddress=" + communicationAddress + ", customerProfile=" + customerProfile
				+ ", officeNumber=" + officeNumber + ", pincode=" + pincode + ", city=" + city + ", cityId=" + cityId
				+ ", branchId=" + branchId + ", branch=" + branch + ", mobileNumber=" + mobileNumber
				+ ", customerAltMobile=" + customerAltMobile + ", customerEmail=" + customerEmail
				+ ", customerAltEmail=" + customerAltEmail + ", longitude=" + longitude + ", lattitude=" + lattitude
				+ ", planName=" + planName + ", planId=" + planId + ", planType=" + planType + ", planAmount="
				+ planAmount + ", planCategoery=" + planCategoery + ", routerRequired=" + routerRequired
				+ ", routerDescription=" + routerDescription + ", externalRouterRequired=" + externalRouterRequired
				+ ", externalRouterDescription=" + externalRouterDescription + ", externalRouterAmount="
				+ externalRouterAmount + ", areaId=" + areaId + ", areaName=" + areaName + ", subAreaId=" + subAreaId
				+ ", subAreaName=" + subAreaName + ", initialWorkStageId=" + initialWorkStageId + ", initialWorkStage="
				+ initialWorkStage + ", activities=" + activities + ", assignedToDisplayName=" + assignedToDisplayName
				+ ", assignedToId=" + assignedToId + ", etrApprovalPending=" + etrApprovalPending
				+ ", etrApprovalValue=" + etrApprovalValue + ", createdDate=" + createdDate + ", paymentRefNumber="
				+ paymentRefNumber + ", paidAmount=" + paidAmount + ", sourceModeId=" + sourceModeId
				+ ", enquiryModeId=" + enquiryModeId + ", ticketSubCategoryId=" + ticketSubCategoryId
				+ ", installationCharges=" + installationCharges + ", additionalCharges=" + additionalCharges
				+ ", paymentMode=" + paymentMode + ", cxPermission=" + cxPermission + ", gxPermission=" + gxPermission
				+ ", priority=" + priority + ", updatedCount=" + updatedCount + ", escalatedValue=" + escalatedValue
				+ ", sliderEscalatedValue=" + sliderEscalatedValue + ", priorityEscalationType="
				+ priorityEscalationType + ", fiberFeasible=" + fiberFeasible + ", connectionType=" + connectionType
				+ ", fxName=" + fxName + ", fxIpAddress=" + fxIpAddress + ", fxMacAddress=" + fxMacAddress
				+ ", fxPorts=" + fxPorts + ", cxName=" + cxName + ", cxIpAddress=" + cxIpAddress + ", cxMacAddress="
				+ cxMacAddress + ", cxPorts=" + cxPorts + ", paymentRemark=" + paymentRemark + ", documentRemark="
				+ documentRemark + ", userRoles=" + userRoles + ", crpMobileNumber=" + crpMobileNumber
				+ ", crpAccountNumber=" + crpAccountNumber + ", couponCode=" + couponCode + ", nationality="
				+ nationality + ", existingIsp=" + existingIsp + ", isStaticIPRequired=" + isStaticIPRequired
				+ ", customerAddressesList=" + customerAddressesList + ", uinType=" + uinType + ", uin=" + uin
				+ ", ticketStatusMap=" + ticketStatusMap + ", feasibilityTypeDetails=" + feasibilityTypeDetails
				+ ", neComment=" + neComment + ", seComment=" + seComment + ", isEditable=" + isEditable
				+ ", paidConnection=" + paidConnection + ", assignedBy=" + assignedBy + ", modifiedOn=" + modifiedOn
				+ ", aadhaarForPOI=" + aadhaarForPOI + ", aadhaarForPOA=" + aadhaarForPOA + ", poiStatusId="
				+ poiStatusId + ", poaStatusId=" + poaStatusId + ", adhaarDetails=" + adhaarDetails + ", poaRemarks="
				+ poaRemarks + ", poiRemarks=" + poiRemarks + ", finahubTransactionId=" + finahubTransactionId
				+ ", schemeCode=" + schemeCode + ", discountAmount=" + discountAmount + ", poaDocumentRemark="
				+ poaDocumentRemark + ", poiDocumentRemark=" + poiDocumentRemark + ", staticIpPriceWithTax="
				+ staticIpPriceWithTax + ", deDupFlag=" + deDupFlag + ", deDupMessage=" + deDupMessage + "]";
	}
	

	
	
	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	
	}