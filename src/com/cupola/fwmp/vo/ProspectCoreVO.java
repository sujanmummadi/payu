package com.cupola.fwmp.vo;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ProspectRequest")
public class ProspectCoreVO implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private CustomerVO customer;
	private TicketVo ticket;
	private String creationSource;
	private String status;
	private String deDupFlag;
	
	/**
	 * @return the deDupFlag
	 */
	public String getDeDupFlag() {
		return deDupFlag;
	}

	/**
	 * @param deDupFlag the deDupFlag to set
	 */
	public void setDeDupFlag(String deDupFlag) {
		this.deDupFlag = deDupFlag;
	}

	public ProspectCoreVO(){
	}
	
	public ProspectCoreVO(CustomerVO customer, TicketVo ticket) {
		super();
		this.customer = customer;
		this.ticket = ticket;
	}

	public CustomerVO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerVO customer) {
		this.customer = customer;
	}

	public TicketVo getTicket() {
		return ticket;
	}

	public void setTicket(TicketVo ticket) {
		this.ticket = ticket;
	}

	public String getCreationSource()
	{
		return creationSource;
	}

	public void setCreationSource(String creationSource)
	{
		this.creationSource = creationSource;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "ProspectCoreVO [customer=" + customer + ", ticket=" + ticket
				+ ", creationSource=" + creationSource + ", status=" + status
				+ "]";
	}
	
	
	
}
