package com.cupola.fwmp.vo;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author manjuprasad.nidlady
 */
public class UserBulkUploadVO {
	
	private long id;
	private String empId;
	private String loginId;
	private String password;
	private String firstName;
	private String lastName;
	private String emailId;
	private long mobileNo;
	private String tablet; 
	private String branch;
	private String employer; 
	private String city;
	private String area;
	private String subAreas;
	private String skills;
	private String reportTo;
	private String userGroups;
	private String userRoles; 
	private int enabled;
	private String addedBy;
	private String modifiedBy;
	private String permissions;
	private List<String> arealist = new ArrayList<String>();
	private List<String> usergrouplist = new ArrayList<String>();
	private List<String> subarealist = new ArrayList<String>();
	private List<String> skilllist = new ArrayList<String>();
	
	public UserBulkUploadVO() {
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String userLoginId) {
		this.loginId = userLoginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String email) {
		this.emailId = email;
	}

	public long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(long mobile) {
		this.mobileNo = mobile;
	}

	public String getTablet() {
		return tablet;
	}

	public void setTablet(String tablet) {
		this.tablet = tablet;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}
	

	public String getReportTo() {
		return reportTo;
	}

	public void setReportTo(String reportTo) {
		this.reportTo = reportTo;
	}

	
	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}


	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}


	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	

	public List<String> getSubAreas() {
		return subarealist;
	}

	public void setSubAreas(String subAreas) {
		this.subAreas = subAreas;
		this.subarealist=Arrays.asList(this.subAreas.split(","));
	}

	public List<String> getSkills() {
		return skilllist;
	}

	public void setSkills(String skills) {
		this.skills = skills;
		this.skilllist=Arrays.asList(this.skills.split(","));
	}

	public List<String> getUserGroups() {
		return usergrouplist;
	}

	public void setUserGroups(String userGroups) {
		this.userGroups = userGroups;
		this.usergrouplist=Arrays.asList(this.userGroups.split(","));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public String getUserRoles()
	{
		return userRoles;
	}

	public void setUserRoles(String userRoles)
	{
		this.userRoles = userRoles;
	}

	public String getEmpId()
	{
		return empId;
	}

	public void setEmpId(String empId)
	{
		this.empId = empId;
	}

	

	public String getPermissions()
	{
		return permissions;
	}

	public void setPermissions(String permissions)
	{
		this.permissions = permissions;
	}

	public List<String> getArea()
	{
		return arealist;
	}

	public void setArea(String area)
	{
		this.area = area;
		this.arealist=Arrays.asList(this.area.split(","));
	}
	
	

	
}
