package com.cupola.fwmp.vo;

import java.io.Serializable;
import java.util.Date;

public class AuditLogVo implements Serializable
{
	private static final long serialVersionUID = 1L;

	private long cityId;
	private long ticketId;
	private long customerId;
	private String prospectNo;
	private String cityCode;
	private String cityName;
	private String customerName;
	private String custMobileNumber;
	
	private String 	packageName;
	private String planCode;
	private String planType;
	private String planCategoery;
	
	private String paymentMode;
	private String paidAmount;
	private String paymentReferenceNo;
	
	private String paymentRemark;
	private int ticketStatus;
	private int mqStatus;
	
	private int paymentStatus;
	
	private String documentRemark;
	private int documentStatus;
	private String ticketCreationDate;
	private String addedOn ;
	
	private String emailId;
	private String mqId;
	private Date communicationETR;
	private Date preferedDate;
	
	private String branchName;
	private String areaName;
	private String assignedToName;
	private long currentAssignedToId;
	private String assignedMobileNumber;
	
	private String communicationAdderss;
	
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getProspectNo() {
		return prospectNo;
	}
	public void setProspectNo(String prospectNo) {
		this.prospectNo = prospectNo;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustMobileNumber() {
		return custMobileNumber;
	}
	public void setCustMobileNumber(String custMobileNumber) {
		this.custMobileNumber = custMobileNumber;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPlanCode() {
		return planCode;
	}
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getPlanCategoery() {
		return planCategoery;
	}
	public void setPlanCategoery(String planCategoery) {
		this.planCategoery = planCategoery;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getPaymentReferenceNo() {
		return paymentReferenceNo;
	}
	public void setPaymentReferenceNo(String paymentReferenceNo) {
		this.paymentReferenceNo = paymentReferenceNo;
	}
	public String getPaymentRemark() {
		return paymentRemark;
	}
	public void setPaymentRemark(String paymentRemark) {
		this.paymentRemark = paymentRemark;
	}
	public int getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(int ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	public int getMqStatus() {
		return mqStatus;
	}
	public void setMqStatus(int mqStatus) {
		this.mqStatus = mqStatus;
	}
	public int getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getDocumentRemark() {
		return documentRemark;
	}
	public void setDocumentRemark(String documentRemark) {
		this.documentRemark = documentRemark;
	}
	public int getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(int documentStatus) {
		this.documentStatus = documentStatus;
	}
	public String getTicketCreationDate() {
		return ticketCreationDate;
	}
	public void setTicketCreationDate(String ticketCreationDate) {
		this.ticketCreationDate = ticketCreationDate;
	}
	public String getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(String addedOn) {
		this.addedOn = addedOn;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public Date getCommunicationETR() {
		return communicationETR;
	}
	public void setCommunicationETR(Date communicationETR) {
		this.communicationETR = communicationETR;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getAssignedToName() {
		return assignedToName;
	}
	public void setAssignedToName(String assignedToName) {
		this.assignedToName = assignedToName;
	}
	public long getCurrentAssignedToId() {
		return currentAssignedToId;
	}
	public void setCurrentAssignedToId(long currentAssignedToId) {
		this.currentAssignedToId = currentAssignedToId;
	}
	public String getAssignedMobileNumber() {
		return assignedMobileNumber;
	}
	public void setAssignedMobileNumber(String assignedMobileNumber) {
		this.assignedMobileNumber = assignedMobileNumber;
	}
	public Date getPreferedDate() {
		return preferedDate;
	}
	public void setPreferedDate(Date preferedDate) {
		this.preferedDate = preferedDate;
	}
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}
	@Override
	public String toString() {
		return "AuditLogVo [cityId=" + cityId + ", ticketId=" + ticketId + ", customerId=" + customerId
				+ ", prospectNo=" + prospectNo + ", cityCode=" + cityCode + ", cityName=" + cityName + ", customerName="
				+ customerName + ", custMobileNumber=" + custMobileNumber + ", packageName=" + packageName
				+ ", planCode=" + planCode + ", planType=" + planType + ", planCategoery=" + planCategoery
				+ ", paymentMode=" + paymentMode + ", paidAmount=" + paidAmount + ", paymentReferenceNo="
				+ paymentReferenceNo + ", paymentRemark=" + paymentRemark + ", ticketStatus=" + ticketStatus
				+ ", mqStatus=" + mqStatus + ", paymentStatus=" + paymentStatus + ", documentRemark=" + documentRemark
				+ ", documentStatus=" + documentStatus + ", ticketCreationDate=" + ticketCreationDate + ", addedOn="
				+ addedOn + ", emailId=" + emailId + ", mqId=" + mqId + ", communicationETR=" + communicationETR
				+ ", preferedDate=" + preferedDate + ", branchName=" + branchName + ", areaName=" + areaName
				+ ", assignedToName=" + assignedToName + ", currentAssignedToId=" + currentAssignedToId
				+ ", assignedMobileNumber=" + assignedMobileNumber + ", communicationAdderss=" + communicationAdderss
				+ "]";
	}
	
	
}
