package com.cupola.fwmp.vo;

public class WorkStageStausCountVo {

	private Integer status;
	private Integer statusCount;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatusCount() {
		return statusCount;
	}

	public void setStatusCount(Integer statusCount) {
		this.statusCount = statusCount;
	}

	@Override
	public String toString() {
		return "WorkStageStausCount [status=" + status + ", statusCount="
				+ statusCount + "]";
	}

}
