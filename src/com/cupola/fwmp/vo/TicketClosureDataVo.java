package com.cupola.fwmp.vo;

public class TicketClosureDataVo
{

	private String workOrderNo;
	private String ticketResolutionCode;
	private String ticketResolutionNotes;
	private Long ticketId;
	private int status;
	private String remarks;
	private String cityName;
	private String prospectNo;
	private String mobileNo;
	private String workorderId;
	private String customerId;
	private String mqID;
	private String currentAssignedTo;

	public TicketClosureDataVo()
	{
	}

	/**
	 * @param workOrderNo
	 * @param ticketId
	 * @param cityName
	 * @param prospectNo
	 * @param mobileNo
	 * @param workorderId
	 * @param customerId
	 * @param mqID
	 * @param currentAssignedTo
	 */
	public TicketClosureDataVo(String workOrderNo, Long ticketId,
			String cityName, String prospectNo, String mobileNo,
			String workorderId, String customerId, String mqID,
			String currentAssignedTo)
	{
		super();
		this.workOrderNo = workOrderNo;
		this.ticketId = ticketId;
		this.cityName = cityName;
		this.prospectNo = prospectNo;
		this.mobileNo = mobileNo;
		this.workorderId = workorderId;
		this.customerId = customerId;
		this.mqID = mqID;
		this.currentAssignedTo = currentAssignedTo;
	}

	public String getWorkOrderNo()
	{
		return workOrderNo;
	}

	public void setWorkOrderNo(String workOrderNo)
	{
		this.workOrderNo = workOrderNo;
	}

	public String getTicketResolutionCode()
	{
		return ticketResolutionCode;
	}

	public void setTicketResolutionCode(String ticketResolutionCode)
	{
		this.ticketResolutionCode = ticketResolutionCode;
	}

	public String getTicketResolutionNotes()
	{
		return ticketResolutionNotes;
	}

	public void setTicketResolutionNotes(String ticketResolutionNotes)
	{
		this.ticketResolutionNotes = ticketResolutionNotes;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public String getCityName()
	{
		return cityName;
	}

	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}

	public String getProspectNo()
	{
		return prospectNo;
	}

	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(String mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getWorkorderId()
	{
		return workorderId;
	}

	public void setWorkorderId(String workorderId)
	{
		this.workorderId = workorderId;
	}

	public String getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(String customerId)
	{
		this.customerId = customerId;
	}

	public String getMqID()
	{
		return mqID;
	}

	public void setMqID(String mqID)
	{
		this.mqID = mqID;
	}

	public String getCurrentAssignedTo()
	{
		return currentAssignedTo;
	}

	public void setCurrentAssignedTo(String currentAssignedTo)
	{
		this.currentAssignedTo = currentAssignedTo;
	}

	@Override
	public String toString()
	{
		return "TicketClosureDataVo [workOrderNo=" + workOrderNo
				+ ", ticketResolutionCode=" + ticketResolutionCode
				+ ", ticketResolutionNotes=" + ticketResolutionNotes
				+ ", ticketId=" + ticketId + ", status=" + status + ", remarks="
				+ remarks + ", cityName=" + cityName + ", prospectNo="
				+ prospectNo + ", mobileNo=" + mobileNo + ", workorderId="
				+ workorderId + ", customerId=" + customerId + ", mqID=" + mqID
				+ ", currentAssignedTo=" + currentAssignedTo + "]";
	}

}
