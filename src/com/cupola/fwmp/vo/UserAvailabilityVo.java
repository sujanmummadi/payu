package com.cupola.fwmp.vo;

public class UserAvailabilityVo
{

	private Long userId;
	private String userName;
	private int status;

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "UserAvailabilityGcmNotification [userId=" + userId
				+ ", userName=" + userName + ", status=" + status + "]";
	}

}
