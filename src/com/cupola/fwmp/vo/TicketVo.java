package com.cupola.fwmp.vo;

import java.io.Serializable;
import java.util.Date;

public class TicketVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private long areaId;
	private long assignTo;
	private long assignedBy;
	private long customerId;
	private long subAreaId;
	private String brandObjId;
	private Long serviceRequestId;
	private String ticketNo;
	private Date ticketCreationDate;
	private String ticketCategory;
	private String ticketSubCategory;
	private long ticketSubCategoryId;
	private String ticketShortDescription;
	private String ticketDescriptionVoc;
	private String ticketNature;
	private String currentEtr;
	private String commitedEtr;
	private String actualTime;
	private String status;
	private String mqStatus;
	private String priority;
	private Date preferedDate;
	private String scheduledDate;
	private Long teamId;
	private long updatedBy;
	private String teamCode;
	private String teamName;
	private String employeeCode;
	private String technicianName;
	private Integer csrId;
	private String followUpCalls;
	private String etrDate;
	private Date colclosedByFieldTime;
	private String resCode;
	private String resNotes;
	private String accountNo;
	private String cxName;
	private String fxName;
	private String priorityUpdatedTime;
	private String department;
	private String symptomCode;
	private String reason;
	private String networkElement;
	private String areaCode;
	private String network;
	private String remarks;
	private String source;
	private String subAttribute = "Door Knock";
	private Date ticketClosedTime;
	private String ticketCreatedBy;
	private Date slaTime;
	private String responseTime;
	private String referenceSource;
	private String defectCode;
	private String subDefectCode;
	private String prospectNo;
	private String mqErrorCode;
	private String currentUpdateCategory; // Used by Ticket update gateway

	
	private String currentActivityId;
	private String currentActivityValue;
	private Date crmCreatedOn;
	private Date crmModifiedOn;
	
	private int deDupFlag;
	private String deDupMessage;
	
	/**
	 * @return the deDupMessage
	 */
	public String getDeDupMessage() {
		return deDupMessage;
	}


	/**
	 * @param deDupMessage the deDupMessage to set
	 */
	public void setDeDupMessage(String deDupMessage) {
		this.deDupMessage = deDupMessage;
	}


	public TicketVo()
	{
		// TODO Auto-generated constructor stub
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public long getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(long assignTo) {
		this.assignTo = assignTo;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getSubAreaId() {
		return subAreaId;
	}

	public void setSubAreaId(long subAreaId) {
		this.subAreaId = subAreaId;
	}

	public String getBrandObjId() {
		return brandObjId;
	}

	public void setBrandObjId(String brandObjId) {
		this.brandObjId = brandObjId;
	}

	public Long getServiceRequestId() {
		return serviceRequestId;
	}

	public void setServiceRequestId(Long serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public Date getTicketCreationDate() {
		return ticketCreationDate;
	}

	public void setTicketCreationDate(Date ticketCreationDate) {
		this.ticketCreationDate = ticketCreationDate;
	}

	public String getTicketCategory() {
		return ticketCategory;
	}

	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}

	public String getTicketSubCategory() {
		return ticketSubCategory;
	}

	public void setTicketSubCategory(String ticketSubCategory) {
		this.ticketSubCategory = ticketSubCategory;
	}

	public String getTicketShortDescription() {
		return ticketShortDescription;
	}

	public void setTicketShortDescription(String ticketShortDescription) {
		this.ticketShortDescription = ticketShortDescription;
	}

	public String getTicketDescriptionVoc() {
		return ticketDescriptionVoc;
	}

	public void setTicketDescriptionVoc(String ticketDescriptionVoc) {
		this.ticketDescriptionVoc = ticketDescriptionVoc;
	}

	public String getTicketNature() {
		return ticketNature;
	}

	public void setTicketNature(String ticketNature) {
		this.ticketNature = ticketNature;
	}

	public String getCurrentEtr() {
		return currentEtr;
	}

	public void setCurrentEtr(String currentEtr) {
		this.currentEtr = currentEtr;
	}

	public String getCommitedEtr() {
		return commitedEtr;
	}

	public void setCommitedEtr(String commitedEtr) {
		this.commitedEtr = commitedEtr;
	}

	public String getActualTime() {
		return actualTime;
	}

	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Date getPreferedDate() {
		return preferedDate;
	}

	public void setPreferedDate(Date preferedDate) {
		this.preferedDate = preferedDate;
	}

	public String getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public String getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(String teamCode) {
		this.teamCode = teamCode;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getTechnicianName() {
		return technicianName;
	}

	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}

	public Integer getCsrId() {
		return csrId;
	}

	public void setCsrId(Integer csrId) {
		this.csrId = csrId;
	}

	public String getFollowUpCalls() {
		return followUpCalls;
	}

	public void setFollowUpCalls(String followUpCalls) {
		this.followUpCalls = followUpCalls;
	}

	public String getEtrDate() {
		return etrDate;
	}

	public void setEtrDate(String etrDate) {
		this.etrDate = etrDate;
	}

	public Date getColclosedByFieldTime() {
		return colclosedByFieldTime;
	}

	public void setColclosedByFieldTime(Date colclosedByFieldTime) {
		this.colclosedByFieldTime = colclosedByFieldTime;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResNotes() {
		return resNotes;
	}

	public void setResNotes(String resNotes) {
		this.resNotes = resNotes;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getCxName() {
		return cxName;
	}

	public void setCxName(String cxName) {
		this.cxName = cxName;
	}

	public String getFxName() {
		return fxName;
	}

	public void setFxName(String fxName) {
		this.fxName = fxName;
	}

	public String getPriorityUpdatedTime() {
		return priorityUpdatedTime;
	}

	public void setPriorityUpdatedTime(String priorityUpdatedTime) {
		this.priorityUpdatedTime = priorityUpdatedTime;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSymptomCode() {
		return symptomCode;
	}

	public void setSymptomCode(String symptomCode) {
		this.symptomCode = symptomCode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getNetworkElement() {
		return networkElement;
	}

	public void setNetworkElement(String networkElement) {
		this.networkElement = networkElement;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getTicketClosedTime() {
		return ticketClosedTime;
	}

	public void setTicketClosedTime(Date ticketClosedTime) {
		this.ticketClosedTime = ticketClosedTime;
	}

	public String getTicketCreatedBy() {
		return ticketCreatedBy;
	}

	public void setTicketCreatedBy(String ticketCreatedBy) {
		this.ticketCreatedBy = ticketCreatedBy;
	}

	public Date getSlaTime() {
		return slaTime;
	}

	public void setSlaTime(Date slaTime) {
		this.slaTime = slaTime;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	public String getReferenceSource() {
		return referenceSource;
	}

	public void setReferenceSource(String referenceSource) {
		this.referenceSource = referenceSource;
	}

	public String getDefectCode() {
		return defectCode;
	}

	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}

	public String getSubDefectCode() {
		return subDefectCode;
	}

	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	
	

	public String getProspectNo()
	{
		return prospectNo;
	}


	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}


	public String getCurrentUpdateCategory()
	{
		return currentUpdateCategory;
	}

	public void setCurrentUpdateCategory(String currentUpdateCategory)
	{
		this.currentUpdateCategory = currentUpdateCategory;
	}

	public long getAssignedBy()
	{
		return assignedBy;
	}

	public void setAssignedBy(long assignedBy)
	{
		this.assignedBy = assignedBy;
	}

	public long getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(long updatedBy)
	{
		this.updatedBy = updatedBy;
	}


	public TicketVo(long id, String currentUpdateCategory)
	{
		super();
		this.id = id;
		this.currentUpdateCategory = currentUpdateCategory;
	}


	

	public String getCurrentActivityId()
	{
		return currentActivityId;
	}


	public void setCurrentActivityId(String currentActivityId)
	{
		this.currentActivityId = currentActivityId;
	}


	public String getCurrentActivityValue()
	{
		return currentActivityValue;
	}


	public void setCurrentActivityValue(String currentActivityValue)
	{
		this.currentActivityValue = currentActivityValue;
	}


	public long getTicketSubCategoryId()
	{
		return ticketSubCategoryId;
	}


	public void setTicketSubCategoryId(long ticketSubCategoryId)
	{
		this.ticketSubCategoryId = ticketSubCategoryId;
	}


	public String getMqStatus()
	{
		return mqStatus;
	}


	public void setMqStatus(String mqStatus)
	{
		this.mqStatus = mqStatus;
	}


	public String getMqErrorCode()
	{
		return mqErrorCode;
	}


	public void setMqErrorCode(String mqErrorCode)
	{
		this.mqErrorCode = mqErrorCode;
	}
	/**
	 * @return the crmCreatedOn
	 */
	public Date getCrmCreatedOn() {
		return crmCreatedOn;
	}


	/**
	 * @param crmCreatedOn the crmCreatedOn to set
	 */
	public void setCrmCreatedOn(Date crmCreatedOn) {
		this.crmCreatedOn = crmCreatedOn;
	}

	public String getSubAttribute() {
		return subAttribute;
	}


	public void setSubAttribute(String subAttribute) {
		this.subAttribute = subAttribute;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TicketVo [id=" + id + ", areaId=" + areaId + ", assignTo=" + assignTo + ", assignedBy=" + assignedBy
				+ ", customerId=" + customerId + ", subAreaId=" + subAreaId + ", brandObjId=" + brandObjId
				+ ", serviceRequestId=" + serviceRequestId + ", ticketNo=" + ticketNo + ", ticketCreationDate="
				+ ticketCreationDate + ", ticketCategory=" + ticketCategory + ", ticketSubCategory=" + ticketSubCategory
				+ ", ticketSubCategoryId=" + ticketSubCategoryId + ", ticketShortDescription=" + ticketShortDescription
				+ ", ticketDescriptionVoc=" + ticketDescriptionVoc + ", ticketNature=" + ticketNature + ", currentEtr="
				+ currentEtr + ", commitedEtr=" + commitedEtr + ", actualTime=" + actualTime + ", status=" + status
				+ ", mqStatus=" + mqStatus + ", priority=" + priority + ", preferedDate=" + preferedDate
				+ ", scheduledDate=" + scheduledDate + ", teamId=" + teamId + ", updatedBy=" + updatedBy + ", teamCode="
				+ teamCode + ", teamName=" + teamName + ", employeeCode=" + employeeCode + ", technicianName="
				+ technicianName + ", csrId=" + csrId + ", followUpCalls=" + followUpCalls + ", etrDate=" + etrDate
				+ ", colclosedByFieldTime=" + colclosedByFieldTime + ", resCode=" + resCode + ", resNotes=" + resNotes
				+ ", accountNo=" + accountNo + ", cxName=" + cxName + ", fxName=" + fxName + ", priorityUpdatedTime="
				+ priorityUpdatedTime + ", department=" + department + ", symptomCode=" + symptomCode + ", reason="
				+ reason + ", networkElement=" + networkElement + ", areaCode=" + areaCode + ", network=" + network
				+ ", remarks=" + remarks + ", source=" + source + ", subAttribute=" + subAttribute
				+ ", ticketClosedTime=" + ticketClosedTime + ", ticketCreatedBy=" + ticketCreatedBy + ", slaTime="
				+ slaTime + ", responseTime=" + responseTime + ", referenceSource=" + referenceSource + ", defectCode="
				+ defectCode + ", subDefectCode=" + subDefectCode + ", prospectNo=" + prospectNo + ", mqErrorCode="
				+ mqErrorCode + ", currentUpdateCategory=" + currentUpdateCategory + ", currentActivityId="
				+ currentActivityId + ", currentActivityValue=" + currentActivityValue + "]";
	}


	/**
	 * @return the deDupFlag
	 */
	public int getDeDupFlag() {
		return deDupFlag;
	}


	/**
	 * @param deDupFlag the deDupFlag to set
	 */
	public void setDeDupFlag(int deDupFlag) {
		this.deDupFlag = deDupFlag;
	}

	/**
	 * @return the crmModifiedOn
	 */
	public Date getCrmModifiedOn() {
		return crmModifiedOn;
	}


	/**
	 * @param crmModifiedOn the crmModifiedOn to set
	 */
	public void setCrmModifiedOn(Date crmModifiedOn) {
		this.crmModifiedOn = crmModifiedOn;
	}
	
	
}
