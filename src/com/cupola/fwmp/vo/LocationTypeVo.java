 /**
 * @author kiran
 * @copyright Cupola Technology
 */
package com.cupola.fwmp.vo;

import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;

/**
 * @author kiran
 * @copyright Cupola Technology
 */
public class LocationTypeVo
{
	private City city;
	private Branch branch;
	private Area area;
	private int updateCacheCode;
	public City getCity()
	{
		return city;
	}
	public void setCity(City city)
	{
		this.city = city;
	}
	public Branch getBranch()
	{
		return branch;
	}
	public void setBranch(Branch branch)
	{
		this.branch = branch;
	}
	public Area getArea()
	{
		return area;
	}
	public void setArea(Area area)
	{
		this.area = area;
	}
	public int getUpdateCacheCode()
	{
		return updateCacheCode;
	}
	public void setUpdateCacheCode(int updateCacheCode)
	{
		this.updateCacheCode = updateCacheCode;
	}
	
	

}
