/**
 * 
 */
package com.cupola.fwmp.vo.sales;

/**
 * @author aditya
 * 
 */
public class TariffPlanUpdateVO
{
	private long ticketId;
	private String prospectNo;
	private long tariffPlanId;
	private String tariffPlanName;
	private String routerModel;
	private String routerRequired;
	private String customerTypeId;

	private String externalRouterRequired;
	private String externalRouterModel;
	private String externalRouterAmount;
	private String staticIPRequired;

	
	
	public String getCustomerTypeId()
	{
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId)
	{
		this.customerTypeId = customerTypeId;
	}

	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getProspectNo()
	{
		return prospectNo;
	}

	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}

	public long getTariffPlanId()
	{
		return tariffPlanId;
	}

	

	/**
	 * @return the staticIPRequired
	 */
	public String getStaticIPRequired() {
		return staticIPRequired;
	}

	/**
	 * @param staticIPRequired the staticIPRequired to set
	 */
	public void setStaticIPRequired(String staticIPRequired) {
		this.staticIPRequired = staticIPRequired;
	}

	public void setTariffPlanId(long tariffPlanId)
	{
		this.tariffPlanId = tariffPlanId;
	}

	public String getTariffPlanName()
	{
		return tariffPlanName;
	}

	public void setTariffPlanName(String tariffPlanName)
	{
		this.tariffPlanName = tariffPlanName;
	}

	public String getRouterModel()
	{
		return routerModel;
	}

	public void setRouterModel(String routerModel)
	{
		this.routerModel = routerModel;
	}

	public String getRouterRequired()
	{
		return routerRequired;
	}

	public void setRouterRequired(String routerRequired)
	{
		this.routerRequired = routerRequired;
	}

	public String getExternalRouterRequired()
	{
		return externalRouterRequired;
	}

	public void setExternalRouterRequired(String externalRouterRequired)
	{
		this.externalRouterRequired = externalRouterRequired;
	}

	public String getExternalRouterModel()
	{
		return externalRouterModel;
	}

	public void setExternalRouterModel(String externalRouterModel)
	{
		this.externalRouterModel = externalRouterModel;
	}

	public String getExternalRouterAmount()
	{
		return externalRouterAmount;
	}

	public void setExternalRouterAmount(String externalRouterAmount)
	{
		this.externalRouterAmount = externalRouterAmount;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TariffPlanUpdateVO [ticketId=" + ticketId + ", prospectNo=" + prospectNo + ", tariffPlanId="
				+ tariffPlanId + ", tariffPlanName=" + tariffPlanName + ", routerModel=" + routerModel
				+ ", routerRequired=" + routerRequired + ", customerTypeId=" + customerTypeId
				+ ", externalRouterRequired=" + externalRouterRequired + ", externalRouterModel=" + externalRouterModel
				+ ", externalRouterAmount=" + externalRouterAmount + ", staticIPRequired=" + staticIPRequired + "]";
	}

	
	
	
	
	
}
