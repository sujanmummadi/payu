/**
 * 
 */
package com.cupola.fwmp.vo.sales;

/**
 * @author aditya
 * 
 */
public class RouterVo
{
	private Long routerId;
	private String routerName;
	private Long cityId;
	private String routerCost;
	private String routerModel;
	private String status;

	public RouterVo()
	{
	}

	public RouterVo(Long routerId, String routerName, String routerCost,
			Long cityId, String routerModel, String status)
	{
		super();
		this.routerId = routerId;
		this.routerName = routerName;
		this.routerCost = routerCost;
		this.routerModel = routerModel;
		this.status = status;
		this.cityId = cityId;
	}

	public Long getRouterId()
	{
		return routerId;
	}

	public void setRouterId(Long routerId)
	{
		this.routerId = routerId;
	}

	public String getRouterName()
	{
		return routerName;
	}

	public void setRouterName(String routerName)
	{
		this.routerName = routerName;
	}

	public String getRouterCost()
	{
		return routerCost;
	}

	public void setRouterCost(String routerCost)
	{
		this.routerCost = routerCost;
	}

	public String getRouterModel()
	{
		return routerModel;
	}

	public void setRouterModel(String routerModel)
	{
		this.routerModel = routerModel;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Long getCityId()
	{
		return cityId;
	}

	public void setCityId(Long cityId)
	{
		this.cityId = cityId;
	}

	@Override
	public String toString()
	{
		return "RouterVo [routerId=" + routerId + ", routerName=" + routerName
				+ ", cityId=" + cityId + ", routerCost=" + routerCost
				+ ", routerModel=" + routerModel + ", status=" + status + "]";
	}

}
