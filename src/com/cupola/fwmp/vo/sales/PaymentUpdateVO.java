/**
 * 
 */
package com.cupola.fwmp.vo.sales;

/**
 * @author aditya
 * 
 */
public class PaymentUpdateVO
{
	private long ticketId;
	private String cafNo;

	private String paymentMode;
	private String referenceNo;
	private String paidAmount;
	private String installationCharges;
	private String paidConnection;
	private String schemeCode;
	private String discountAmount;

	private String kycData;
	private String transactionId;
	private String aadhaarTransactionType;

	/**
	 * @return the kycData
	 */
	public String getKycData() {
		return kycData;
	}

	/**
	 * @param kycData the kycData to set
	 */
	public void setKycData(String kycData) {
		this.kycData = kycData;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the aadhaarTransactionType
	 */
	public String getAadhaarTransactionType() {
		return aadhaarTransactionType;
	}

	/**
	 * @param aadhaarTransactionType the aadhaarTransactionType to set
	 */
	public void setAadhaarTransactionType(String aadhaarTransactionType) {
		this.aadhaarTransactionType = aadhaarTransactionType;
	}

	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getCafNo()
	{
		return cafNo;
	}

	public void setCafNo(String cafNo)
	{
		this.cafNo = cafNo;
	}

	public String getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	public String getReferenceNo()
	{
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo)
	{
		this.referenceNo = referenceNo;
	}

	public String getPaidAmount()
	{
		return paidAmount;
	}

	public void setPaidAmount(String paidAmount)
	{
		this.paidAmount = paidAmount;
	}

	public String getInstallationCharges()
	{
		return installationCharges;
	}

	public void setInstallationCharges(String installationCharges)
	{
		this.installationCharges = installationCharges;
	}

	public String getPaidConnection()
	{
		return paidConnection;
	}

	public void setPaidConnection(String paidConnection)
	{
		this.paidConnection = paidConnection;
	}

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	public String getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Override
	public String toString() {
		return "PaymentUpdateVO [ticketId=" + ticketId + ", cafNo=" + cafNo + ", paymentMode=" + paymentMode
				+ ", referenceNo=" + referenceNo + ", paidAmount=" + paidAmount + ", installationCharges="
				+ installationCharges + ", paidConnection=" + paidConnection + ", schemeCode=" + schemeCode
				+ ", discountAmount=" + discountAmount + "]";
	}
	
	

}
