package com.cupola.fwmp.vo.sales;

import java.util.List;
import java.util.Map;

public class TariffDetailsVO
{
	private List<TariffPlanVo> tariffPlanVoList;
	private Map<String, List<String>> mapOfTariffPackageList;

	public List<TariffPlanVo> getTariffPlanVoList()
	{
		return tariffPlanVoList;
	}

	public void setTariffPlanVoList(List<TariffPlanVo> tariffPlanVoList)
	{
		this.tariffPlanVoList = tariffPlanVoList;
	}

	public Map<String, List<String>> getMapOfTariffPackageList()
	{
		return mapOfTariffPackageList;
	}

	public void setMapOfTariffPackageList(
			Map<String, List<String>> mapOfTariffPackageList)
	{
		this.mapOfTariffPackageList = mapOfTariffPackageList;
	}

}
