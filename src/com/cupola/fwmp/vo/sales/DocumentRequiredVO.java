/**
 * 
 */
package com.cupola.fwmp.vo.sales;

import java.util.Date;

import com.cupola.fwmp.persistance.entities.AddressDocument;
import com.cupola.fwmp.persistance.entities.PhotoDocument;
import com.cupola.fwmp.persistance.entities.User;

/**
 * @author aditya
 * 
 */
public class DocumentRequiredVO
{
	private Long id;
	private PhotoDocument photoDocument;
	private AddressDocument addressDocument;
	private String documentName;
	private Long documentType;
	private String documentPath;
	private Date issueDate;
	private Date validFrom;
	private Date validTo;
	private String isValid;
	private String documentNumber;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;
	private Byte physicaCopyAvailable;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public PhotoDocument getPhotoDocument()
	{
		return photoDocument;
	}

	public void setPhotoDocument(PhotoDocument photoDocument)
	{
		this.photoDocument = photoDocument;
	}

	public AddressDocument getAddressDocument()
	{
		return addressDocument;
	}

	public void setAddressDocument(AddressDocument addressDocument)
	{
		this.addressDocument = addressDocument;
	}

	public String getDocumentName()
	{
		return documentName;
	}

	public void setDocumentName(String documentName)
	{
		this.documentName = documentName;
	}

	public Long getDocumentType()
	{
		return documentType;
	}

	public void setDocumentType(Long documentType)
	{
		this.documentType = documentType;
	}

	public String getDocumentPath()
	{
		return documentPath;
	}

	public void setDocumentPath(String documentPath)
	{
		this.documentPath = documentPath;
	}

	public Date getIssueDate()
	{
		return issueDate;
	}

	public void setIssueDate(Date issueDate)
	{
		this.issueDate = issueDate;
	}

	public Date getValidFrom()
	{
		return validFrom;
	}

	public void setValidFrom(Date validFrom)
	{
		this.validFrom = validFrom;
	}

	public Date getValidTo()
	{
		return validTo;
	}

	public void setValidTo(Date validTo)
	{
		this.validTo = validTo;
	}

	public String getIsValid()
	{
		return isValid;
	}

	public void setIsValid(String isValid)
	{
		this.isValid = isValid;
	}

	public String getDocumentNumber()
	{
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber)
	{
		this.documentNumber = documentNumber;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Byte getPhysicaCopyAvailable()
	{
		return physicaCopyAvailable;
	}

	public void setPhysicaCopyAvailable(Byte physicaCopyAvailable)
	{
		this.physicaCopyAvailable = physicaCopyAvailable;
	}

	@Override
	public String toString()
	{
		return "DocumentVO [id=" + id + ", photoDocument=" + photoDocument
				+ ", addressDocument=" + addressDocument + ", documentName="
				+ documentName + ", documentType=" + documentType
				+ ", documentPath=" + documentPath + ", issueDate=" + issueDate
				+ ", validFrom=" + validFrom + ", validTo=" + validTo
				+ ", isValid=" + isValid + ", documentNumber=" + documentNumber
				+ ", addedBy=" + addedBy + ", addedOn=" + addedOn
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn
				+ ", status=" + status + ", physicaCopyAvailable="
				+ physicaCopyAvailable + "]";
	}

}
