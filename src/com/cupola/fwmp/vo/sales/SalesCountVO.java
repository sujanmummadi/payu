/**
 * 
 */
package com.cupola.fwmp.vo.sales;

import java.util.List;

import com.cupola.fwmp.vo.counts.dashboard.SEDetails;

/**
 * @author aditya
 * 
 */
public class SalesCountVO
{
	private String statusName;
	private int statusCode;
	private int statusCounts;
	private List<Long> ticketIds;
	private List<SEDetails> seIdandCount;

	public SalesCountVO()
	{
	}


	public SalesCountVO(String statusName, int statusCode, int statusCounts)
	{
		super();
		this.statusName = statusName;
		this.statusCode = statusCode;
		this.statusCounts = statusCounts;
	}


	public SalesCountVO(String statusName, int statusCode, int statusCounts,
			List<Long> ticketIds)
	{
		this.statusName = statusName;
		this.statusCode = statusCode;
		this.statusCounts = statusCounts;
		this.ticketIds = ticketIds;
	}


	public String getStatusName()
	{
		return statusName;
	}

	public void setStatusName(String statusName)
	{
		this.statusName = statusName;
	}

	public int getStatusCode()
	{
		return statusCode;
	}

	public void setStatusCode(int statusCode)
	{
		this.statusCode = statusCode;
	}

	public int getStatusCounts()
	{
		return statusCounts;
	}

	public void setStatusCounts(int statusCounts)
	{
		this.statusCounts = statusCounts;
	}

	public List<Long> getTicketIds()
	{
		return ticketIds;
	}

	public void setTicketIds(List<Long> ticketIds)
	{
		this.ticketIds = ticketIds;
	}

	 

	@Override
	public String toString()
	{
		return "SalesCountVO [statusName=" + statusName + ", statusCode="
				+ statusCode + ", statusCounts=" + statusCounts + ", ticketIds="
				+ ticketIds + ", seIdandCound=" + seIdandCount + "]";
	}



	public List<SEDetails> getSeIdandCount()
	{
		return seIdandCount;
	}


	public void setSeIdandCount(List<SEDetails> seIdandCount)
	{
		this.seIdandCount = seIdandCount;
	}


	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + statusCode;
		result = prime * result + statusCounts;
		result = prime * result
				+ ((statusName == null) ? 0 : statusName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesCountVO other = (SalesCountVO) obj;
		if (statusCode != other.statusCode)
			return false;
		if (statusCounts != other.statusCounts)
			return false;
		if (statusName == null)
		{
			if (other.statusName != null)
				return false;
		} else if (!statusName.equals(other.statusName))
			return false;
		return true;
	}

}
