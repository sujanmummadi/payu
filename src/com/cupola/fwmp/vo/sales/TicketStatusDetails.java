/**
 * 
 */
package com.cupola.fwmp.vo.sales;

/**
 * @author aditya
 * 
 */
public class TicketStatusDetails
{
	private Long ticketId;

	private Integer status;

	public TicketStatusDetails(Long ticketId, Integer status)
	{
		this.ticketId = ticketId;
		this.status = status;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "TicketStatusDetails [ticketId=" + ticketId + ", status="
				+ status + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((ticketId == null) ? 0 : ticketId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketStatusDetails other = (TicketStatusDetails) obj;
		if (status == null)
		{
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (ticketId == null)
		{
			if (other.ticketId != null)
				return false;
		} else if (!ticketId.equals(other.ticketId))
			return false;
		return true;
	}

}
