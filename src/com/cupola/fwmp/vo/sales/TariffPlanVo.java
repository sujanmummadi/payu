package com.cupola.fwmp.vo.sales;

import java.util.Date;

public class TariffPlanVo
{
	private String id;
	private String planType;
	private String planCategoery;
	private String planCode;
	private String packageName;
	private String netopCode;
	private String description;
	private String tenure;
	private String OriginalPriceWithoutTax;
	private String OfferOriginalPrice;
	private String priceWithTax;
	private String offerPriceWithTax;
	private String installCharges;
	private String installChargesWithTax;
	private String planQuota;
	private String usageCategory;
	private String currentSpeed;
	private String currentSpeedUnit;
	private String speedBreakerSpeed;
	private String isRouterFree;
	private String status;
	private String cityName;
	private String cityCode;
	private String cityId;
	private String startDate;
	private String endDate;
	private String staticIpPriceWithTax ;   
	private String staticIpPriceWithoutTax;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getPlanType()
	{
		return planType;
	}

	public void setPlanType(String planType)
	{
		this.planType = planType;
	}

	public String getPlanCategoery()
	{
		return planCategoery;
	}

	public void setPlanCategoery(String planCategoery)
	{
		this.planCategoery = planCategoery;
	}

	public String getPlanCode()
	{
		return planCode;
	}

	public void setPlanCode(String planCode)
	{
		this.planCode = planCode;
	}

	public String getPackageName()
	{
		return packageName;
	}

	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}

	public String getNetopCode()
	{
		return netopCode;
	}

	public void setNetopCode(String netopCode)
	{
		this.netopCode = netopCode;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getTenure()
	{
		return tenure;
	}

	public void setTenure(String tenure)
	{
		this.tenure = tenure;
	}

	public String getOriginalPriceWithoutTax()
	{
		return OriginalPriceWithoutTax;
	}

	public void setOriginalPriceWithoutTax(String originalPriceWithoutTax)
	{
		OriginalPriceWithoutTax = originalPriceWithoutTax;
	}

	public String getOfferOriginalPrice()
	{
		return OfferOriginalPrice;
	}

	public void setOfferOriginalPrice(String offerOriginalPrice)
	{
		OfferOriginalPrice = offerOriginalPrice;
	}

	public String getPriceWithTax()
	{
		return priceWithTax;
	}

	public void setPriceWithTax(String priceWithTax)
	{
		this.priceWithTax = priceWithTax;
	}

	public String getOfferPriceWithTax()
	{
		return offerPriceWithTax;
	}

	public void setOfferPriceWithTax(String offerPriceWithTax)
	{
		this.offerPriceWithTax = offerPriceWithTax;
	}

	public String getInstallCharges()
	{
		return installCharges;
	}

	public void setInstallCharges(String installCharges)
	{
		this.installCharges = installCharges;
	}

	public String getInstallChargesWithTax()
	{
		return installChargesWithTax;
	}

	public void setInstallChargesWithTax(String installChargesWithTax)
	{
		this.installChargesWithTax = installChargesWithTax;
	}

	public String getPlanQuota()
	{
		return planQuota;
	}

	public void setPlanQuota(String planQuota)
	{
		this.planQuota = planQuota;
	}

	public String getUsageCategory()
	{
		return usageCategory;
	}

	public void setUsageCategory(String usageCategory)
	{
		this.usageCategory = usageCategory;
	}

	public String getCurrentSpeed()
	{
		return currentSpeed;
	}

	public void setCurrentSpeed(String currentSpeed)
	{
		this.currentSpeed = currentSpeed;
	}

	public String getCurrentSpeedUnit()
	{
		return currentSpeedUnit;
	}

	public void setCurrentSpeedUnit(String currentSpeedUnit)
	{
		this.currentSpeedUnit = currentSpeedUnit;
	}

	public String getSpeedBreakerSpeed()
	{
		return speedBreakerSpeed;
	}

	public void setSpeedBreakerSpeed(String speedBreakerSpeed)
	{
		this.speedBreakerSpeed = speedBreakerSpeed;
	}

	public String getIsRouterFree()
	{
		return isRouterFree;
	}

	public void setIsRouterFree(String isRouterFree)
	{
		this.isRouterFree = isRouterFree;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getCityName()
	{
		return cityName;
	}

	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}

	public String getCityCode()
	{
		return cityCode;
	}

	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}

	public String getCityId()
	{
		return cityId;
	}

	public void setCityId(String cityId)
	{
		this.cityId = cityId;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}
	
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	/**
	 * @return the staticIpPriceWithTax
	 */
	public String getStaticIpPriceWithTax() {
		return staticIpPriceWithTax;
	}

	/**
	 * @param staticIpPriceWithTax the staticIpPriceWithTax to set
	 */
	public void setStaticIpPriceWithTax(String staticIpPriceWithTax) {
		this.staticIpPriceWithTax = staticIpPriceWithTax;
	}

	/**
	 * @return the staticIpPriceWithoutTax
	 */
	public String getStaticIpPriceWithoutTax() {
		return staticIpPriceWithoutTax;
	}

	/**
	 * @param staticIpPriceWithoutTax the staticIpPriceWithoutTax to set
	 */
	public void setStaticIpPriceWithoutTax(String staticIpPriceWithoutTax) {
		this.staticIpPriceWithoutTax = staticIpPriceWithoutTax;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TariffPlanVo [id=" + id + ", planType=" + planType + ", planCategoery=" + planCategoery + ", planCode="
				+ planCode + ", packageName=" + packageName + ", netopCode=" + netopCode + ", description="
				+ description + ", tenure=" + tenure + ", OriginalPriceWithoutTax=" + OriginalPriceWithoutTax
				+ ", OfferOriginalPrice=" + OfferOriginalPrice + ", priceWithTax=" + priceWithTax
				+ ", offerPriceWithTax=" + offerPriceWithTax + ", installCharges=" + installCharges
				+ ", installChargesWithTax=" + installChargesWithTax + ", planQuota=" + planQuota + ", usageCategory="
				+ usageCategory + ", currentSpeed=" + currentSpeed + ", currentSpeedUnit=" + currentSpeedUnit
				+ ", speedBreakerSpeed=" + speedBreakerSpeed + ", isRouterFree=" + isRouterFree + ", status=" + status
				+ ", cityName=" + cityName + ", cityCode=" + cityCode + ", cityId=" + cityId + ", startDate="
				+ startDate + ", endDate=" + endDate + ", staticIpPriceWithTax=" + staticIpPriceWithTax
				+ ", staticIpPriceWithoutTax=" + staticIpPriceWithoutTax + "]";
	}

	



}
