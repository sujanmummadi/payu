/**
 * 
 */
package com.cupola.fwmp.vo.sales;

import java.util.Map;

/**
 * @author aditya
 * 
 */
public class ImageInput {

	private String ticketId;

	private String propspectNo;

	private Map<String, String> imageNameBase64ValueMap;

	private boolean permissionDoc;

	private String kycData;
	
	private String transactionId;
	

	private long activityId;

	private String uinType;
	
	private String aadhaarTransactionType;

	private String uin;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getPropspectNo() {
		return propspectNo;
	}

	public void setPropspectNo(String propspectNo) {
		this.propspectNo = propspectNo;
	}

	public Map<String, String> getImageNameBase64ValueMap() {
		return imageNameBase64ValueMap;
	}

	public void setImageNameBase64ValueMap(Map<String, String> imageNameBase64ValueMap) {
		this.imageNameBase64ValueMap = imageNameBase64ValueMap;
	}

	public boolean isPermissionDoc() {
		return permissionDoc;
	}

	public void setPermissionDoc(boolean permissionDoc) {
		this.permissionDoc = permissionDoc;
	}

	public String getKycData() {
		return kycData;
	}

	public void setKycData(String kycData) {
		this.kycData = kycData;
	}

	/**
	 * @return the activityId
	 */
	public long getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            the activityId to set
	 */
	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}

	public String getUinType() {
		return uinType;
	}

	public void setUinType(String uinType) {
		this.uinType = uinType;
	}

	public String getUin() {
		return uin;
	}

	public void setUin(String uin) {
		this.uin = uin;
	}
	
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	/**
	 * @return the aadhaarTransactionType
	 */
	public String getAadhaarTransactionType() {
		return aadhaarTransactionType;
	}

	/**
	 * @param aadhaarTransactionType the aadhaarTransactionType to set
	 */
	public void setAadhaarTransactionType(String aadhaarTransactionType) {
		this.aadhaarTransactionType = aadhaarTransactionType;
	}

	@Override
	public String toString() {
		return "ImageInput [ticketId=" + ticketId + ", propspectNo=" + propspectNo + ", permissionDoc=" + permissionDoc
				+ ", kycData=" + kycData + ", activityId=" + activityId + ", uinType=" + uinType + ", uin=" + uin + "]";
	}
}
