package com.cupola.fwmp.vo.sales;

/**
 * 
 */

import java.util.Date;
import java.util.List;

import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;

/**
 * @author aditya
 * 
 */
public class UpdateProspectVO {
	private String ticketId;
	private String cafNo;
	private String prospectNo;

	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private String fullName;

	private Long prefferedCallDate;

	private String customerType;
	private String tariffType;
	private String customerId;

	private String mobileNumber;
	private String alternativeMobileNo;
	private String officeNumber;
	private String emailId;
	private String customerProfile;
	private String alternativeEmailId;

	private String currentAddress;// Customer current address
	private String communicationAdderss;// Customer biling address
	private String permanentAddress;// Customer Aadhaar address

	private String pinCode;

	private String cxPermission;
	private String gxPermission;

	// start of must to set during basic info update, get this values from gis
	// response
	private String connectionType;
	private String transactionNo;
	private String errorNo;
	private String message;
	// End of must to set during basic info update, got values from gis response
	private String cityId;
	private String branchId;
	private String areaId;
	private String clusterName;
	private String subAreaId;
	private String areaName;
	private String branchName;
	private String cityName;

	private String fxName;
	private String fxIpAddress;
	private String fxMacAddress;
	private List<String> fxPorts;

	private String cxName;
	private String cxIpAddress;
	private String cxMacAddress;
	private List<String> cxPorts;

	private String remarks;
	private String source;

	private Date addedOn;

	private String status;

	private String statusId;

	private String enquiryType;
	private String sourceOfEnquiry;
	private String prospectColdReason;
	private String prospectType;
	private String prospectUpdateStatusId;

	private String longitude;
	private String lattitude;

	private String permissionFeasibilityStatus;
	private String permissionRejectionReason;
	private boolean nonFeasible;

	private boolean feasibilityRejectedDuringDeployment;

	private String copperPermission;

	private boolean fromFwmpApp;

	private boolean workOrderGenerated;

	private String modifiedOn;

	private KycDetails kycDetails;

	private boolean aadhaarForPOI;

	private boolean aadhaarForPOA;

	private String finahubTransactionId;

	private String customerPhoto;

	private String customerEsign;

	private String crpMobileNumber;

	private String crpAccountNumber;

	private String nationality;

	private String existingIsp;

	private List<CustomerAddressVO> customerAddressVOs;

	private String subAreaName;

	private String aadhaarTransactionType;

	/**
	 * @return the ticketId
	 */
	public String getTicketId() {
		return ticketId;
	}

	/**
	 * @param ticketId
	 *            the ticketId to set
	 */
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * @return the cafNo
	 */
	public String getCafNo() {
		return cafNo;
	}

	/**
	 * @param cafNo
	 *            the cafNo to set
	 */
	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	/**
	 * @return the prospectNo
	 */
	public String getProspectNo() {
		return prospectNo;
	}

	/**
	 * @param prospectNo
	 *            the prospectNo to set
	 */
	public void setProspectNo(String prospectNo) {
		this.prospectNo = prospectNo;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName
	 *            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the prefferedCallDate
	 */
	public Long getPrefferedCallDate() {
		return prefferedCallDate;
	}

	/**
	 * @param prefferedCallDate
	 *            the prefferedCallDate to set
	 */
	public void setPrefferedCallDate(Long prefferedCallDate) {
		this.prefferedCallDate = prefferedCallDate;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the tariffType
	 */
	public String getTariffType() {
		return tariffType;
	}

	/**
	 * @param tariffType
	 *            the tariffType to set
	 */
	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the alternativeMobileNo
	 */
	public String getAlternativeMobileNo() {
		return alternativeMobileNo;
	}

	/**
	 * @param alternativeMobileNo
	 *            the alternativeMobileNo to set
	 */
	public void setAlternativeMobileNo(String alternativeMobileNo) {
		this.alternativeMobileNo = alternativeMobileNo;
	}

	/**
	 * @return the officeNumber
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * @param officeNumber
	 *            the officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the customerProfile
	 */
	public String getCustomerProfile() {
		return customerProfile;
	}

	/**
	 * @param customerProfile
	 *            the customerProfile to set
	 */
	public void setCustomerProfile(String customerProfile) {
		this.customerProfile = customerProfile;
	}

	/**
	 * @return the alternativeEmailId
	 */
	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}

	/**
	 * @param alternativeEmailId
	 *            the alternativeEmailId to set
	 */
	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}

	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}

	/**
	 * @param currentAddress
	 *            the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	/**
	 * @return the communicationAdderss
	 */
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}

	/**
	 * @param communicationAdderss
	 *            the communicationAdderss to set
	 */
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}

	/**
	 * @return the permanentAddress
	 */
	public String getPermanentAddress() {
		return permanentAddress;
	}

	/**
	 * @param permanentAddress
	 *            the permanentAddress to set
	 */
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	/**
	 * @return the pinCode
	 */
	public String getPinCode() {
		return pinCode;
	}

	/**
	 * @param pinCode
	 *            the pinCode to set
	 */
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	/**
	 * @return the cxPermission
	 */
	public String getCxPermission() {
		return cxPermission;
	}

	/**
	 * @param cxPermission
	 *            the cxPermission to set
	 */
	public void setCxPermission(String cxPermission) {
		this.cxPermission = cxPermission;
	}

	/**
	 * @return the gxPermission
	 */
	public String getGxPermission() {
		return gxPermission;
	}

	/**
	 * @param gxPermission
	 *            the gxPermission to set
	 */
	public void setGxPermission(String gxPermission) {
		this.gxPermission = gxPermission;
	}

	/**
	 * @return the connectionType
	 */
	public String getConnectionType() {
		return connectionType;
	}

	/**
	 * @param connectionType
	 *            the connectionType to set
	 */
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	/**
	 * @return the transactionNo
	 */
	public String getTransactionNo() {
		return transactionNo;
	}

	/**
	 * @param transactionNo
	 *            the transactionNo to set
	 */
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	/**
	 * @return the errorNo
	 */
	public String getErrorNo() {
		return errorNo;
	}

	/**
	 * @param errorNo
	 *            the errorNo to set
	 */
	public void setErrorNo(String errorNo) {
		this.errorNo = errorNo;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the cityId
	 */
	public String getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the branchId
	 */
	public String getBranchId() {
		return branchId;
	}

	/**
	 * @param branchId
	 *            the branchId to set
	 */
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the areaId
	 */
	public String getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId
	 *            the areaId to set
	 */
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the clusterName
	 */
	public String getClusterName() {
		return clusterName;
	}

	/**
	 * @param clusterName
	 *            the clusterName to set
	 */
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	/**
	 * @return the subAreaId
	 */
	public String getSubAreaId() {
		return subAreaId;
	}

	/**
	 * @param subAreaId
	 *            the subAreaId to set
	 */
	public void setSubAreaId(String subAreaId) {
		this.subAreaId = subAreaId;
	}

	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}

	/**
	 * @param areaName
	 *            the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName
	 *            the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the fxName
	 */
	public String getFxName() {
		return fxName;
	}

	/**
	 * @param fxName
	 *            the fxName to set
	 */
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}

	/**
	 * @return the fxIpAddress
	 */
	public String getFxIpAddress() {
		return fxIpAddress;
	}

	/**
	 * @param fxIpAddress
	 *            the fxIpAddress to set
	 */
	public void setFxIpAddress(String fxIpAddress) {
		this.fxIpAddress = fxIpAddress;
	}

	/**
	 * @return the fxMacAddress
	 */
	public String getFxMacAddress() {
		return fxMacAddress;
	}

	/**
	 * @param fxMacAddress
	 *            the fxMacAddress to set
	 */
	public void setFxMacAddress(String fxMacAddress) {
		this.fxMacAddress = fxMacAddress;
	}

	/**
	 * @return the fxPorts
	 */
	public List<String> getFxPorts() {
		return fxPorts;
	}

	/**
	 * @param fxPorts
	 *            the fxPorts to set
	 */
	public void setFxPorts(List<String> fxPorts) {
		this.fxPorts = fxPorts;
	}

	/**
	 * @return the cxName
	 */
	public String getCxName() {
		return cxName;
	}

	/**
	 * @param cxName
	 *            the cxName to set
	 */
	public void setCxName(String cxName) {
		this.cxName = cxName;
	}

	/**
	 * @return the cxIpAddress
	 */
	public String getCxIpAddress() {
		return cxIpAddress;
	}

	/**
	 * @param cxIpAddress
	 *            the cxIpAddress to set
	 */
	public void setCxIpAddress(String cxIpAddress) {
		this.cxIpAddress = cxIpAddress;
	}

	/**
	 * @return the cxMacAddress
	 */
	public String getCxMacAddress() {
		return cxMacAddress;
	}

	/**
	 * @param cxMacAddress
	 *            the cxMacAddress to set
	 */
	public void setCxMacAddress(String cxMacAddress) {
		this.cxMacAddress = cxMacAddress;
	}

	/**
	 * @return the cxPorts
	 */
	public List<String> getCxPorts() {
		return cxPorts;
	}

	/**
	 * @param cxPorts
	 *            the cxPorts to set
	 */
	public void setCxPorts(List<String> cxPorts) {
		this.cxPorts = cxPorts;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source
	 *            the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the addedOn
	 */
	public Date getAddedOn() {
		return addedOn;
	}

	/**
	 * @param addedOn
	 *            the addedOn to set
	 */
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusId
	 */
	public String getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId
	 *            the statusId to set
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the enquiryType
	 */
	public String getEnquiryType() {
		return enquiryType;
	}

	/**
	 * @param enquiryType
	 *            the enquiryType to set
	 */
	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}

	/**
	 * @return the sourceOfEnquiry
	 */
	public String getSourceOfEnquiry() {
		return sourceOfEnquiry;
	}

	/**
	 * @param sourceOfEnquiry
	 *            the sourceOfEnquiry to set
	 */
	public void setSourceOfEnquiry(String sourceOfEnquiry) {
		this.sourceOfEnquiry = sourceOfEnquiry;
	}

	/**
	 * @return the prospectColdReason
	 */
	public String getProspectColdReason() {
		return prospectColdReason;
	}

	/**
	 * @param prospectColdReason
	 *            the prospectColdReason to set
	 */
	public void setProspectColdReason(String prospectColdReason) {
		this.prospectColdReason = prospectColdReason;
	}

	/**
	 * @return the prospectType
	 */
	public String getProspectType() {
		return prospectType;
	}

	/**
	 * @param prospectType
	 *            the prospectType to set
	 */
	public void setProspectType(String prospectType) {
		this.prospectType = prospectType;
	}

	/**
	 * @return the prospectUpdateStatusId
	 */
	public String getProspectUpdateStatusId() {
		return prospectUpdateStatusId;
	}

	/**
	 * @param prospectUpdateStatusId
	 *            the prospectUpdateStatusId to set
	 */
	public void setProspectUpdateStatusId(String prospectUpdateStatusId) {
		this.prospectUpdateStatusId = prospectUpdateStatusId;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the lattitude
	 */
	public String getLattitude() {
		return lattitude;
	}

	/**
	 * @param lattitude
	 *            the lattitude to set
	 */
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	/**
	 * @return the permissionFeasibilityStatus
	 */
	public String getPermissionFeasibilityStatus() {
		return permissionFeasibilityStatus;
	}

	/**
	 * @param permissionFeasibilityStatus
	 *            the permissionFeasibilityStatus to set
	 */
	public void setPermissionFeasibilityStatus(String permissionFeasibilityStatus) {
		this.permissionFeasibilityStatus = permissionFeasibilityStatus;
	}

	/**
	 * @return the permissionRejectionReason
	 */
	public String getPermissionRejectionReason() {
		return permissionRejectionReason;
	}

	/**
	 * @param permissionRejectionReason
	 *            the permissionRejectionReason to set
	 */
	public void setPermissionRejectionReason(String permissionRejectionReason) {
		this.permissionRejectionReason = permissionRejectionReason;
	}

	/**
	 * @return the nonFeasible
	 */
	public boolean isNonFeasible() {
		return nonFeasible;
	}

	/**
	 * @param nonFeasible
	 *            the nonFeasible to set
	 */
	public void setNonFeasible(boolean nonFeasible) {
		this.nonFeasible = nonFeasible;
	}

	/**
	 * @return the feasibilityRejectedDuringDeployment
	 */
	public boolean isFeasibilityRejectedDuringDeployment() {
		return feasibilityRejectedDuringDeployment;
	}

	/**
	 * @param feasibilityRejectedDuringDeployment
	 *            the feasibilityRejectedDuringDeployment to set
	 */
	public void setFeasibilityRejectedDuringDeployment(boolean feasibilityRejectedDuringDeployment) {
		this.feasibilityRejectedDuringDeployment = feasibilityRejectedDuringDeployment;
	}

	/**
	 * @return the copperPermission
	 */
	public String getCopperPermission() {
		return copperPermission;
	}

	/**
	 * @param copperPermission
	 *            the copperPermission to set
	 */
	public void setCopperPermission(String copperPermission) {
		this.copperPermission = copperPermission;
	}

	/**
	 * @return the fromFwmpApp
	 */
	public boolean isFromFwmpApp() {
		return fromFwmpApp;
	}

	/**
	 * @param fromFwmpApp
	 *            the fromFwmpApp to set
	 */
	public void setFromFwmpApp(boolean fromFwmpApp) {
		this.fromFwmpApp = fromFwmpApp;
	}

	/**
	 * @return the workOrderGenerated
	 */
	public boolean isWorkOrderGenerated() {
		return workOrderGenerated;
	}

	/**
	 * @param workOrderGenerated
	 *            the workOrderGenerated to set
	 */
	public void setWorkOrderGenerated(boolean workOrderGenerated) {
		this.workOrderGenerated = workOrderGenerated;
	}

	/**
	 * @return the modifiedOn
	 */
	public String getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * @return the kycDetails
	 */
	public KycDetails getKycDetails() {
		return kycDetails;
	}

	/**
	 * @param kycDetails
	 *            the kycDetails to set
	 */
	public void setKycDetails(KycDetails kycDetails) {
		this.kycDetails = kycDetails;
	}

	/**
	 * @return the aadhaarForPOI
	 */
	public boolean isAadhaarForPOI() {
		return aadhaarForPOI;
	}

	/**
	 * @param aadhaarForPOI
	 *            the aadhaarForPOI to set
	 */
	public void setAadhaarForPOI(boolean aadhaarForPOI) {
		this.aadhaarForPOI = aadhaarForPOI;
	}

	/**
	 * @return the aadhaarForPOA
	 */
	public boolean isAadhaarForPOA() {
		return aadhaarForPOA;
	}

	/**
	 * @param aadhaarForPOA
	 *            the aadhaarForPOA to set
	 */
	public void setAadhaarForPOA(boolean aadhaarForPOA) {
		this.aadhaarForPOA = aadhaarForPOA;
	}

	/**
	 * @return the finahubTransactionId
	 */
	public String getFinahubTransactionId() {
		return finahubTransactionId;
	}

	/**
	 * @param finahubTransactionId
	 *            the finahubTransactionId to set
	 */
	public void setFinahubTransactionId(String finahubTransactionId) {
		this.finahubTransactionId = finahubTransactionId;
	}

	/**
	 * @return the customerPhoto
	 */
	public String getCustomerPhoto() {
		return customerPhoto;
	}

	/**
	 * @param customerPhoto
	 *            the customerPhoto to set
	 */
	public void setCustomerPhoto(String customerPhoto) {
		this.customerPhoto = customerPhoto;
	}

	/**
	 * @return the customerEsign
	 */
	public String getCustomerEsign() {
		return customerEsign;
	}

	/**
	 * @param customerEsign
	 *            the customerEsign to set
	 */
	public void setCustomerEsign(String customerEsign) {
		this.customerEsign = customerEsign;
	}
	
	

	/**
	 * @return the aadhaarTransactionType
	 */
	public String getAadhaarTransactionType() {
		return aadhaarTransactionType;
	}

	/**
	 * @param aadhaarTransactionType the aadhaarTransactionType to set
	 */
	public void setAadhaarTransactionType(String aadhaarTransactionType) {
		this.aadhaarTransactionType = aadhaarTransactionType;
	}

	/**
	 * @return the crpMobileNumber
	 */
	public String getCrpMobileNumber() {
		return crpMobileNumber;
	}

	/**
	 * @param crpMobileNumber
	 *            the crpMobileNumber to set
	 */
	public void setCrpMobileNumber(String crpMobileNumber) {
		this.crpMobileNumber = crpMobileNumber;
	}

	/**
	 * @return the crpAccountNumber
	 */
	public String getCrpAccountNumber() {
		return crpAccountNumber;
	}

	/**
	 * @param crpAccountNumber
	 *            the crpAccountNumber to set
	 */
	public void setCrpAccountNumber(String crpAccountNumber) {
		this.crpAccountNumber = crpAccountNumber;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the existingIsp
	 */
	public String getExistingIsp() {
		return existingIsp;
	}

	/**
	 * @param existingIsp
	 *            the existingIsp to set
	 */
	public void setExistingIsp(String existingIsp) {
		this.existingIsp = existingIsp;
	}

	/**
	 * @return the customerAddressVOs
	 */
	public List<CustomerAddressVO> getCustomerAddressVOs() {
		return customerAddressVOs;
	}

	/**
	 * @param customerAddressVOs
	 *            the customerAddressVOs to set
	 */
	public void setCustomerAddressVOs(List<CustomerAddressVO> customerAddressVOs) {
		this.customerAddressVOs = customerAddressVOs;
	}

	/**
	 * @return the subAreaName
	 */
	public String getSubAreaName() {
		return subAreaName;
	}

	/**
	 * @param subAreaName
	 *            the subAreaName to set
	 */
	public void setSubAreaName(String subAreaName) {
		this.subAreaName = subAreaName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UpdateProspectVO [ticketId=" + ticketId + ", cafNo=" + cafNo + ", prospectNo=" + prospectNo + ", title="
				+ title + ", firstName=" + firstName + ", lastName=" + lastName + ", middleName=" + middleName
				+ ", fullName=" + fullName + ", prefferedCallDate=" + prefferedCallDate + ", customerType="
				+ customerType + ", tariffType=" + tariffType + ", customerId=" + customerId + ", mobileNumber="
				+ mobileNumber + ", alternativeMobileNo=" + alternativeMobileNo + ", officeNumber=" + officeNumber
				+ ", emailId=" + emailId + ", customerProfile=" + customerProfile + ", alternativeEmailId="
				+ alternativeEmailId + ", currentAddress=" + currentAddress + ", communicationAdderss="
				+ communicationAdderss + ", permanentAddress=" + permanentAddress + ", pinCode=" + pinCode
				+ ", cxPermission=" + cxPermission + ", gxPermission=" + gxPermission + ", connectionType="
				+ connectionType + ", transactionNo=" + transactionNo + ", errorNo=" + errorNo + ", message=" + message
				+ ", cityId=" + cityId + ", branchId=" + branchId + ", areaId=" + areaId + ", clusterName="
				+ clusterName + ", subAreaId=" + subAreaId + ", areaName=" + areaName + ", branchName=" + branchName
				+ ", cityName=" + cityName + ", fxName=" + fxName + ", fxIpAddress=" + fxIpAddress + ", fxMacAddress="
				+ fxMacAddress + ", fxPorts=" + fxPorts + ", cxName=" + cxName + ", cxIpAddress=" + cxIpAddress
				+ ", cxMacAddress=" + cxMacAddress + ", cxPorts=" + cxPorts + ", remarks=" + remarks + ", source="
				+ source + ", addedOn=" + addedOn + ", status=" + status + ", statusId=" + statusId + ", enquiryType="
				+ enquiryType + ", sourceOfEnquiry=" + sourceOfEnquiry + ", prospectColdReason=" + prospectColdReason
				+ ", prospectType=" + prospectType + ", prospectUpdateStatusId=" + prospectUpdateStatusId
				+ ", longitude=" + longitude + ", lattitude=" + lattitude + ", permissionFeasibilityStatus="
				+ permissionFeasibilityStatus + ", permissionRejectionReason=" + permissionRejectionReason
				+ ", nonFeasible=" + nonFeasible + ", feasibilityRejectedDuringDeployment="
				+ feasibilityRejectedDuringDeployment + ", copperPermission=" + copperPermission + ", fromFwmpApp="
				+ fromFwmpApp + ", workOrderGenerated=" + workOrderGenerated + ", modifiedOn=" + modifiedOn
				+ ", kycDetails=" + kycDetails + ", aadhaarForPOI=" + aadhaarForPOI + ", aadhaarForPOA=" + aadhaarForPOA
				+ ", finahubTransactionId=" + finahubTransactionId + ", crpMobileNumber=" + crpMobileNumber
				+ ", crpAccountNumber=" + crpAccountNumber + ", nationality=" + nationality + ", existingIsp="
				+ existingIsp + ", customerAddressVOs=" + customerAddressVOs + ", subAreaName=" + subAreaName + "]";
	}
}
