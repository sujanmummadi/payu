package com.cupola.fwmp.vo;

import java.util.Date;
import java.util.List;

public class StatusVO
{
	private long ticketId;
	private List<Long> ticketList;

	private String propspectNo;
	private List<String> propspectNoList;
	private String remarks;
	private int status;
	private int reasonCode;
	private String reasonValue;
	private Long addedBy;
	private String addedByUserName;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private String workOrderNumber;
	
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public String getAddedByUserName()
	{
		return addedByUserName;
	}

	public void setAddedByUserName(String addedByUserName)
	{
		this.addedByUserName = addedByUserName;
	}

	public List<Long> getTicketList()
	{
		return ticketList;
	}

	public void setTicketList(List<Long> ticketList)
	{
		this.ticketList = ticketList;
	}

	public int getReasonCode()
	{
		return reasonCode;
	}

	public void setReasonCode(int reasonCode)
	{
		this.reasonCode = reasonCode;
	}

	public String getPropspectNo()
	{
		return propspectNo;
	}

	public void setPropspectNo(String propspectNo)
	{
		this.propspectNo = propspectNo;
	}

	public List<String> getPropspectNoList()
	{
		return propspectNoList;
	}

	public void setPropspectNoList(List<String> propspectNoList)
	{
		this.propspectNoList = propspectNoList;
	}


	public String getReasonValue()
	{
		return reasonValue;
	}

	public void setReasonValue(String reasonValue)
	{
		this.reasonValue = reasonValue;
	}

	@Override
	public String toString()
	{
		return "StatusVO [ticketId=" + ticketId + ", propspectNo="
				+ propspectNo + ", status=" + status + ", reasonCode="
				+ reasonCode + "]";
	}

	 

}