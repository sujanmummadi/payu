package com.cupola.fwmp.vo;

public class CustomerClosures
{

	private String mqId;
	private String customerId;
	private String userName;
	private String city;
	private String activationStatus;
	private String mqClosureStatus;
	private String fxName;
	private String fxIpAddress;
	private String fxMacAddress;
	private String fxPorts;
	private String cxName;
	private String cxIpAddress;
	private boolean gx;
	private String cxMacAddress;
	private String cxPorts;
	private String secretKey;

	public String getMqId()
	{
		return mqId;
	}

	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}

	public String getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(String customerId)
	{
		this.customerId = customerId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getActivationStatus()
	{
		return activationStatus;
	}

	public void setActivationStatus(String activationStatus)
	{
		this.activationStatus = activationStatus;
	}

	public String getMqClosureStatus()
	{
		return mqClosureStatus;
	}

	public void setMqClosureStatus(String mqClosureStatus)
	{
		this.mqClosureStatus = mqClosureStatus;
	}

	public String getFxIpAddress()
	{
		return fxIpAddress;
	}

	public void setFxIpAddress(String fxIpAddress)
	{
		this.fxIpAddress = fxIpAddress;
	}

	public String getFxMacAddress()
	{
		return fxMacAddress;
	}

	public void setFxMacAddress(String fxMacAddress)
	{
		this.fxMacAddress = fxMacAddress;
	}

	public String getFxPorts()
	{
		return fxPorts;
	}

	public void setFxPorts(String fxPorts)
	{
		this.fxPorts = fxPorts;
	}

	public String getCxName()
	{
		return cxName;
	}

	public void setCxName(String cxName)
	{
		this.cxName = cxName;
	}

	public String getCxIpAddress()
	{
		return cxIpAddress;
	}

	public void setCxIpAddress(String cxIpAddress)
	{
		this.cxIpAddress = cxIpAddress;
	}

	public String getCxMacAddress()
	{
		return cxMacAddress;
	}

	public void setCxMacAddress(String cxMacAddress)
	{
		this.cxMacAddress = cxMacAddress;
	}

	public String getCxPorts()
	{
		return cxPorts;
	}

	public void setCxPorts(String cxPorts)
	{
		this.cxPorts = cxPorts;
	}

	public String getSecretKey()
	{
		return secretKey;
	}

	public void setSecretKey(String secretKey)
	{
		this.secretKey = secretKey;
	}

	public String getFxName()
	{
		return fxName;
	}

	public boolean isGx()
	{
		return gx;
	}

	public void setGx(boolean gx)
	{
		this.gx = gx;
	}

	public void setFxName(String fxName)
	{
		this.fxName = fxName;
	}

	@Override
	public String toString()
	{
		return "CustomerClosures [mqId=" + mqId + ", customerId=" + customerId
				+ ", userName=" + userName + ", city=" + city
				+ ", activationStatus=" + activationStatus
				+ ", mqClosureStatus=" + mqClosureStatus + ", fxName=" + fxName
				+ ", fxIpAddress=" + fxIpAddress + ", fxMacAddress="
				+ fxMacAddress + ", fxPorts=" + fxPorts + ", cxName=" + cxName
				+ ", cxIpAddress=" + cxIpAddress + ", isGx=" + gx
				+ ", cxMacAddress=" + cxMacAddress + ", cxPorts=" + cxPorts
				+ ", secretKey=" + secretKey + "]";
	}

}