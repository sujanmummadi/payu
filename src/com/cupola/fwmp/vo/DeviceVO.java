package com.cupola.fwmp.vo;

import java.util.Date;

public class DeviceVO
{
	private long id;
	private long cityId;
	private long branchId;
	private long areaId;
	private String branchName;
	private String areaName;
	private String cityName;
	private String subAreaName;
	private String deviceName;
	private String ipAddress;
	private String macAddress;
	private Integer deviceType;
	private Long parentDeviceId;
	private Integer availablePort;
	private Integer totalPort;
	private Integer usedPort;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;
	
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public String getBranchName()
	{
		return branchName;
	}
	public void setBranchName(String branchName)
	{
		this.branchName = branchName;
	}
	public String getAreaName()
	{
		return areaName;
	}
	public void setAreaName(String areaName)
	{
		this.areaName = areaName;
	}
	public String getCityName()
	{
		return cityName;
	}
	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}
	public String getSubAreaName()
	{
		return subAreaName;
	}
	public void setSubAreaName(String subAreaName)
	{
		this.subAreaName = subAreaName;
	}
	public String getDeviceName()
	{
		return deviceName;
	}
	public void setDeviceName(String deviceName)
	{
		this.deviceName = deviceName;
	}
	public String getIpAddress()
	{
		return ipAddress;
	}
	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}
	public String getMacAddress()
	{
		return macAddress;
	}
	public void setMacAddress(String macAddress)
	{
		this.macAddress = macAddress;
	}
	public Integer getDeviceType()
	{
		return deviceType;
	}
	public void setDeviceType(Integer deviceType)
	{
		this.deviceType = deviceType;
	}
	public Long getParentDeviceId()
	{
		return parentDeviceId;
	}
	public void setParentDeviceId(Long parentDeviceId)
	{
		this.parentDeviceId = parentDeviceId;
	}
	public Integer getAvailablePort()
	{
		return availablePort;
	}
	public void setAvailablePort(Integer availablePort)
	{
		this.availablePort = availablePort;
	}
	public Integer getTotalPort()
	{
		return totalPort;
	}
	public void setTotalPort(Integer totalPort)
	{
		this.totalPort = totalPort;
	}
	public Integer getUsedPort()
	{
		return usedPort;
	}
	public void setUsedPort(Integer usedPort)
	{
		this.usedPort = usedPort;
	}
	public Long getAddedBy()
	{
		return addedBy;
	}
	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}
	public Date getAddedOn()
	{
		return addedOn;
	}
	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}
	public Long getModifiedBy()
	{
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn()
	{
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}
	public Integer getStatus()
	{
		return status;
	}
	public void setStatus(Integer status)
	{
		this.status = status;
	}
	@Override
	public String toString()
	{
		return "DeviceVO [id=" + id + ", branchName=" + branchName
				+ ", areaName=" + areaName + ", cityName=" + cityName
				+ ", subAreaName=" + subAreaName + ", deviceName=" + deviceName
				+ ", ipAddress=" + ipAddress + ", macAddress=" + macAddress
				+ ", deviceType=" + deviceType + ", parentDeviceId="
				+ parentDeviceId + ", availablePort=" + availablePort
				+ ", totalPort=" + totalPort + ", usedPort=" + usedPort
				+ ", status=" + status + "]";
	}
	public long getCityId()
	{
		return cityId;
	}
	public void setCityId(long cityId)
	{
		this.cityId = cityId;
	}
	public long getBranchId()
	{
		return branchId;
	}
	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}
	public long getAreaId()
	{
		return areaId;
	}
	public void setAreaId(long areaId)
	{
		this.areaId = areaId;
	}
}
