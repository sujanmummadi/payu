package com.cupola.fwmp.vo;

public class ETRUpdateByNEtoTLVo {
	private String ticketId;
	private String workOrderNumber;
	private String userId;
	private String userName;
	private String previousETR;
	private String newETR;
	private String remarks;
	private String reasonCode;
	private String othersReason;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPreviousETR() {
		return previousETR;
	}

	public void setPreviousETR(String previousETR) {
		this.previousETR = previousETR;
	}

	public String getNewETR() {
		return newETR;
	}

	public void setNewETR(String newETR) {
		this.newETR = newETR;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOthersReason() {
		return othersReason;
	}

	public void setOthersReason(String othersReason) {
		this.othersReason = othersReason;
	}

	@Override
	public String toString() {
		return "ETRUpdateByNEtoTLVo [ticketId=" + ticketId
				+ ", workOrderNumber=" + workOrderNumber + ", userId=" + userId
				+ ", userName=" + userName + ", previousETR=" + previousETR
				+ ", newETR=" + newETR + ", remarks=" + remarks
				+ ", reasonCode=" + reasonCode + ", othersReason="
				+ othersReason + "]";
	}

	

}
