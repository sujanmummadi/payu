package com.cupola.fwmp.vo.fr;

import org.codehaus.jackson.annotate.JsonIgnore;


public class FRDefectSubDefectsVO
{
	 
	private Long ticketId;
	private String defectCode;
	private String subDefectCode;
	private String remarks;
	private Long status;
	
	@JsonIgnore
	private Long modifiedBy;
	
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	public String getDefectCode() {
		return defectCode;
	}
	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}
	public String getSubDefectCode() {
		return subDefectCode;
	}
	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	public Long getModifiedBy()
	{
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
}
