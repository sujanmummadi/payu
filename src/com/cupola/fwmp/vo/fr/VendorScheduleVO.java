package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class VendorScheduleVO
{
	private Long ticketId;
	private String estimatedStartTime;
	private Date estimatedEndTime;
	private Date actualStartTime;
	private Date actualEndTime;
	private Long addedBy;
	private Date addedOn;
	private Integer status;
	
	public Long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}
	public String getEstimatedStartTime()
	{
		return estimatedStartTime;
	}
	public void setEstimatedStartTime(String estimatedStartTime)
	{
		this.estimatedStartTime = estimatedStartTime;
	}
	public Date getEstimatedEndTime()
	{
		return estimatedEndTime;
	}
	public void setEstimatedEndTime(Date estimatedEndTime)
	{
		this.estimatedEndTime = estimatedEndTime;
	}
	public Date getActualStartTime()
	{
		return actualStartTime;
	}
	public void setActualStartTime(Date actualStartTime)
	{
		this.actualStartTime = actualStartTime;
	}
	public Date getActualEndTime()
	{
		return actualEndTime;
	}
	public void setActualEndTime(Date actualEndTime)
	{
		this.actualEndTime = actualEndTime;
	}
	public Long getAddedBy()
	{
		return addedBy;
	}
	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}
	public Date getAddedOn()
	{
		return addedOn;
	}
	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}
	public Integer getStatus()
	{
		return status;
	}
	public void setStatus(Integer status)
	{
		this.status = status;
	}
}
