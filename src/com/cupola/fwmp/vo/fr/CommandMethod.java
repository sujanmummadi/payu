package com.cupola.fwmp.vo.fr;

public enum CommandMethod {
	START(1),
	STOP(2),
	JUMP(3);
	
	private final int typeVal;

	CommandMethod(int val) {
        typeVal = val;
    }
	
	/*
     * Get the value for the header field of this particular Command Method
     * return the Command Method suitible for bitwise combination
     */
    public int getValue() {
        return typeVal;
    }

    /*
     * Get the CommandClass enum from the type field integer
     * return the appropriate CommandClass
     */
   public static CommandMethod getCommandMethod(int typeField) {
        switch (typeField) {
        
        case 1:
            return START;
        case 2:
            return STOP;
        case 3:
            return JUMP;

            // Not a valid Decision Method type
        default:
                return null;
        
        }
    }

}
