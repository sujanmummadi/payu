package com.cupola.fwmp.vo.fr;

public class Activity {
	private int activityId;
	private String activityShortName;
	private String activityName;
	public int getActivityId() {
		return activityId;
	}
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
	public String getActivityShortName() {
		return activityShortName;
	}
	public void setActivityShortName(String activityShortName) {
		this.activityShortName = activityShortName;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	@Override
	public String toString() {
		return "Activity [activityId=" + activityId + ", activityShortName="
				+ activityShortName + ", activityName=" + activityName + "]";
	}
	

}
