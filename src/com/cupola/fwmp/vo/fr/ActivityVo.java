package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class ActivityVo {
	private int activityId;
	private int status;
	private int reasonCode;
	private String remarks;
	private Date timeStamp;
	public int getActivityId() {
		return activityId;
	}
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	@Override
	public String toString() {
		return "ActivityVo [activityId=" + activityId + ", status=" + status
				+ ", reasonCode=" + reasonCode + ", remarks=" + remarks
				+ ", timeStamp=" + timeStamp + "]";
	}
	
	

}
