package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class FrTicketEtrVo
{
	private Long ticketId;
	private Date currentEtr;
	private Date committedEtr;
	private Integer updatedCount;
	private Integer etrUpdateMode;
	private Date approvalEtr;
	private String resonCode;
	private String remarks;
	private Integer status;
	private Long modifiedBy;
	private Date modifiedOn;
	private Long addedBy;
	private Date addedOn;
	private String currentProgress;
	
	public FrTicketEtrVo(){
	}
	
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	public Date getCurrentEtr() {
		return currentEtr;
	}
	public void setCurrentEtr(Date currentEtr) {
		this.currentEtr = currentEtr;
	}
	public Date getCommittedEtr() {
		return committedEtr;
	}
	public void setCommittedEtr(Date committedEtr) {
		this.committedEtr = committedEtr;
	}
	public Integer getUpdatedCount() {
		return updatedCount;
	}
	public void setUpdatedCount(Integer updatedCount) {
		this.updatedCount = updatedCount;
	}
	public Integer getEtrUpdateMode() {
		return etrUpdateMode;
	}
	public void setEtrUpdateMode(Integer etrUpdateMode) {
		this.etrUpdateMode = etrUpdateMode;
	}
	public Date getApprovalEtr() {
		return approvalEtr;
	}
	public void setApprovalEtr(Date approvalEtr) {
		this.approvalEtr = approvalEtr;
	}
	public String getResonCode() {
		return resonCode;
	}
	public void setResonCode(String resonCode) {
		this.resonCode = resonCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Long getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public String getCurrentProgress() {
		return currentProgress;
	}

	public void setCurrentProgress(String currentProgress) {
		this.currentProgress = currentProgress;
	}
	
}	
