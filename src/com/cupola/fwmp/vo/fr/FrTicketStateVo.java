package com.cupola.fwmp.vo.fr;

public class FrTicketStateVo
{
	private Long ticketId;
	private Long userId;
	private boolean reassigned;
	private boolean locked;
	private boolean blocked;
	
	public FrTicketStateVo(){}
	
	public FrTicketStateVo(Long ticketId, Long userId, boolean locked,
			boolean blocked) {
		super();
		this.ticketId = ticketId;
		this.userId = userId;
		this.locked = locked;
		this.blocked = blocked;
	}




	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public boolean isReassigned() {
		return reassigned;
	}

	public void setReassigned(boolean reassigned) {
		this.reassigned = reassigned;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	
}
