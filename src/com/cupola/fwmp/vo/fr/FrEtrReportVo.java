package com.cupola.fwmp.vo.fr;

import java.util.Date;

import com.cupola.fwmp.service.reports.fr.FrReport;

public class FrEtrReportVo implements FrReport
{
	private Long ticketId;
	private Integer ticketStatus;
	private Long userId;
	private String userName;
	private Long reportTo;
	private String reportToName;
	
	private Date currentEtr;
	private Date commitedEtr;
	private Integer updatedCount;
	private Long currentFlowId;
	
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	public Integer getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(Integer ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getReportTo() {
		return reportTo;
	}
	public void setReportTo(Long reportTo) {
		this.reportTo = reportTo;
	}
	public String getReportToName() {
		return reportToName;
	}
	public void setReportToName(String reportToName) {
		this.reportToName = reportToName;
	}
	public Date getCurrentEtr() {
		return currentEtr;
	}
	public void setCurrentEtr(Date currentEtr) {
		this.currentEtr = currentEtr;
	}
	public Date getCommitedEtr() {
		return commitedEtr;
	}
	public void setCommitedEtr(Date commitedEtr) {
		this.commitedEtr = commitedEtr;
	}
	public Integer getUpdatedCount() {
		return updatedCount;
	}
	public void setUpdatedCount(Integer updatedCount) {
		this.updatedCount = updatedCount;
	}
	public Long getCurrentFlowId() {
		return currentFlowId;
	}
	public void setCurrentFlowId(Long currentFlowId) {
		this.currentFlowId = currentFlowId;
	}
}
