package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class EmailCorporateVo {
	
	private long id;
	private long ticketId;
	private Long cityId;
	private Integer eventId;
	private Integer emailType;
	private String attachments;
	private String mails;
	private Date addedOn;
	private Date updatedOn;
	private Integer status;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Integer getEmailType() {
		return emailType;
	}
	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}
	public String getAttachments() {
		return attachments;
	}
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}
	public String getMails() {
		return mails;
	}
	public void setMails(String mails) {
		this.mails = mails;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "EmailCorporateVo [id=" + id + ", ticketId=" + ticketId
				+ ", cityId=" + cityId + ", eventId=" + eventId
				+ ", emailType=" + emailType + ", attachments=" + attachments
				+ ", mails=" + mails + ", addedOn=" + addedOn + ", updatedOn="
				+ updatedOn + ", status=" + status + "]";
	}
	
	

}
