package com.cupola.fwmp.vo.fr;

public class TicketModifyInMQVo
{
	private long ticketId;
	private String workOrderNumber;
	private String etrValue;
	private String status;
	private String reasonCode;
	private String remarks;
	private String assignedToUser;
	private String defectCode;
	private String subDefectCode;
	private String cityName;
	private String requestParam;
	private long mqModifyRequestType;
	
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getEtrValue() {
		return etrValue;
	}
	public void setEtrValue(String etrValue) {
		this.etrValue = etrValue;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getAssignedToUser() {
		return assignedToUser;
	}
	public void setAssignedToUser(String assignedToUser) {
		this.assignedToUser = assignedToUser;
	}
	public String getDefectCode() {
		return defectCode;
	}
	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}
	public String getSubDefectCode() {
		return subDefectCode;
	}
	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	public String getCityName()
	{
		return cityName;
	}
	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}
	public String getRequestParam()
	{
		return requestParam;
	}
	public void setRequestParam(String requestParam)
	{
		this.requestParam = requestParam;
	}
	public long getMqModifyRequestType()
	{
		return mqModifyRequestType;
	}
	public void setMqModifyRequestType(long mqModifyRequestType)
	{
		this.mqModifyRequestType = mqModifyRequestType;
	}
	
}
