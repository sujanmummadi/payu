package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class ScratchPadLog {
	private long id;
	private long ticketId;
	private int typeId;
	private String typeName;
	private int decisionId;
	private String decisionName;
	private int noOfOptions;
	private int selectedOption;
	private String selectedOptionName;
	private Date timeStamp;
	private int activityId;
	private String activityName;
	private int status;
	private int reasonCode;
	private int scbStatus;
	private String remarks;
	private int cmdId;
	private String cmdName;
	private Date addedOn;
	private Date modifiedOn;
	private String message;
	private long addedBy;
	private String reasionName;
	
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public int getDecisionId() {
		return decisionId;
	}
	public void setDecisionId(int decisionId) {
		this.decisionId = decisionId;
	}
	public int getNoOfOptions() {
		return noOfOptions;
	}
	public void setNoOfOptions(int noOfOptions) {
		this.noOfOptions = noOfOptions;
	}
	public int getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public int getActivityId() {
		return activityId;
	}
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public int getScbStatus() {
		return scbStatus;
	}
	public void setScbStatus(int scbStatus) {
		this.scbStatus = scbStatus;
	}
	public String getDecisionName() {
		return decisionName;
	}
	public void setDecisionName(String decisionName) {
		this.decisionName = decisionName;
	}
	public String getSelectedOptionName() {
		return selectedOptionName;
	}
	public void setSelectedOptionName(String selectedOptionName) {
		this.selectedOptionName = selectedOptionName;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	public int getCmdId() {
		return cmdId;
	}
	public void setCmdId(int cmdId) {
		this.cmdId = cmdId;
	}
	public String getCmdName() {
		return cmdName;
	}
	public void setCmdName(String cmdName) {
		this.cmdName = cmdName;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public long getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(long addedBy) {
		this.addedBy = addedBy;
	}
	
	
	public String getReasionName() {
		return reasionName;
	}
	public void setReasionName(String reasionName) {
		this.reasionName = reasionName;
	}
	@Override
	public String toString() {
		return "ScratchPadLog [id=" + id + ", ticketId=" + ticketId
				+ ", typeId=" + typeId + ", typeName=" + typeName
				+ ", decisionId=" + decisionId + ", decisionName="
				+ decisionName + ", noOfOptions=" + noOfOptions
				+ ", selectedOption=" + selectedOption
				+ ", selectedOptionName=" + selectedOptionName + ", timeStamp="
				+ timeStamp + ", activityId=" + activityId + ", activityName="
				+ activityName + ", status=" + status + ", reasonCode="
				+ reasonCode + ", scbStatus=" + scbStatus + ", remarks="
				+ remarks + ", cmdId=" + cmdId + ", cmdName=" + cmdName
				+ ", addedOn=" + addedOn + ", modifiedOn=" + modifiedOn
				+ ", message=" + message + ", addedBy=" + addedBy
				+ ", reasionName=" + reasionName + "]";
	}
	
	
	
	
	
	

}
