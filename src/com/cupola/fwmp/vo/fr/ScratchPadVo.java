/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class ScratchPadVo {
	private long id;
	private long ticketId;
	private int type;
	private int idDetails;
	private Date addedOn;
	private Date updatedOn;
	private int status;
	private int selectedOption;
	private Integer jumpId;
	private String remarks;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getIdDetails() {
		return idDetails;
	}
	public void setIdDetails(int idDetails) {
		this.idDetails = idDetails;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	public int getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
	}
	
	public Integer getJumpId()
	{
		return jumpId;
	}
	public void setJumpId(Integer jumpId)
	{
		this.jumpId = jumpId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Override
	public String toString() {
		return "ScratchPadVo [id=" + id + ", ticketId=" + ticketId + ", type=" + type + ", idDetails=" + idDetails
				+ ", addedOn=" + addedOn + ", updatedOn=" + updatedOn + ", status=" + status + ", selectedOption="
				+ selectedOption + ", jumpId=" + jumpId + ", remarks=" + remarks + "]";
	}
	
	
	
	
	

}
