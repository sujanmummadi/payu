/**
* @Author aditya  31-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo.fr;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @Author aditya 31-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Document(collection = "UtilityLogger")
public class UtilityLogger {
	@Id
	private Long utilityLoggerId;
	private String utilityServerURI;
	private String utilityServerRequestTime;
	private String utilityServerResponseTime;
	private String utilityServerResponse;

	/**
	 * @return the utilityLoggerId
	 */
	public Long getUtilityLoggerId() {
		return utilityLoggerId;
	}

	/**
	 * @param utilityLoggerId
	 *            the utilityLoggerId to set
	 */
	public void setUtilityLoggerId(Long utilityLoggerId) {
		this.utilityLoggerId = utilityLoggerId;
	}

	/**
	 * @return the utilityServerURI
	 */
	public String getUtilityServerURI() {
		return utilityServerURI;
	}

	/**
	 * @param utilityServerURI
	 *            the utilityServerURI to set
	 */
	public void setUtilityServerURI(String utilityServerURI) {
		this.utilityServerURI = utilityServerURI;
	}

	/**
	 * @return the utilityServerRequestTime
	 */
	public String getUtilityServerRequestTime() {
		return utilityServerRequestTime;
	}

	/**
	 * @param utilityServerRequestTime
	 *            the utilityServerRequestTime to set
	 */
	public void setUtilityServerRequestTime(String utilityServerRequestTime) {
		this.utilityServerRequestTime = utilityServerRequestTime;
	}

	/**
	 * @return the utilityServerResponseTime
	 */
	public String getUtilityServerResponseTime() {
		return utilityServerResponseTime;
	}

	/**
	 * @param utilityServerResponseTime
	 *            the utilityServerResponseTime to set
	 */
	public void setUtilityServerResponseTime(String utilityServerResponseTime) {
		this.utilityServerResponseTime = utilityServerResponseTime;
	}

	/**
	 * @return the utilityServerResponse
	 */
	public String getUtilityServerResponse() {
		return utilityServerResponse;
	}

	/**
	 * @param utilityServerResponse
	 *            the utilityServerResponse to set
	 */
	public void setUtilityServerResponse(String utilityServerResponse) {
		this.utilityServerResponse = utilityServerResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UtilityLogger [utilityLoggerId=" + utilityLoggerId + ", utilityServerURI=" + utilityServerURI
				+ ", utilityServerRequestTime=" + utilityServerRequestTime + ", utilityServerResponseTime="
				+ utilityServerResponseTime + ", utilityServerResponse=" + utilityServerResponse + "]";
	}
}
