package com.cupola.fwmp.vo.fr;

import org.codehaus.jackson.annotate.JsonIgnore;

public class FrTicketPushBackVo 
{
	private String ticketId;
	private String reason;
	private String remarks;
	private String resMessage;
	private String resStatus;

	@JsonIgnore
	private Long reportTo;
	
	@JsonIgnore
	private boolean locked;
	
	@JsonIgnore
	private String updateType;

	public FrTicketPushBackVo (){}


	public String getTicketId() {
		return ticketId;
	}


	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}


	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public String getResMessage() {
		return resMessage;
	}


	public void setResMessage(String resMessage) {
		this.resMessage = resMessage;
	}


	public String getResStatus() {
		return resStatus;
	}


	public void setResStatus(String resStatus) {
		this.resStatus = resStatus;
	}
	

	public boolean isLocked() {
		return locked;
	}


	public void setLocked(boolean locked) {
		this.locked = locked;
	}


	public String getUpdateType() {
		return updateType;
	}


	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}


	public Long getReportTo() {
		return reportTo;
	}


	public void setReportTo(Long reportTo) {
		this.reportTo = reportTo;
	}
	
}
