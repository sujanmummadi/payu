package com.cupola.fwmp.vo.fr;

public class FrFlowRequestVo 
{
	private long ticketId; // for ticket id
	private long requestedBy; // ne request for ccnr or splicer
	private long actionType; // electrician, splicer, ccnr
	private Integer status; // electrician pending ,ccnr pending
	private long userId; // user id or ccnr id or vendor id
	
	
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public long getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(long requestedBy) {
		this.requestedBy = requestedBy;
	}
	public long getActionType() {
		return actionType;
	}
	public void setActionType(long actionType) {
		this.actionType = actionType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
}
