package com.cupola.fwmp.vo.fr;

public class ETRDetails {
	private int stepID;
	private String stepName;
	private int etr;
	private int progressPercentage;
	public int getStepID() {
		return stepID;
	}
	public void setStepID(int stepID) {
		this.stepID = stepID;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public int getEtr() {
		return etr;
	}
	public void setEtr(int etr) {
		this.etr = etr;
	}
	public int getProgressPercentage() {
		return progressPercentage;
	}
	public void setProgressPercentage(int progressPercentage) {
		this.progressPercentage = progressPercentage;
	}
	@Override
	public String toString() {
		return "ETRDetatils [stepID=" + stepID + ", stepName=" + stepName
				+ ", etr=" + etr + ", progressPercentage=" + progressPercentage
				+ "]";
	}
	

}
