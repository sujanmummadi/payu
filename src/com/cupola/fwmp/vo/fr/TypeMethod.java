package com.cupola.fwmp.vo.fr;

public enum TypeMethod {
	DECISION(1),
	ACTIVITY(2),
	COMMAND(3),
	DEFECTCODE(4),
	SUBDEFECTCODE(5);

    private final int typeVal;

    TypeMethod(int val) {
        typeVal = val;
    }

    /*
     * Get the value for the header field of this particular Type Method
     * return the Type Method suitible for bitwise combination
     */
    public int getValue() {
        return typeVal;
    }

    /*
     * Get the TypeClass enum from the type field integer
     * return the appropriate TypeClass
     */
   public static TypeMethod getTypeMethod(int typeField) {
        switch (typeField) {
        
        case 1:
            return DECISION;
        case 2:
            return ACTIVITY;
        case 3:
            return COMMAND;
        case 4:
            return DEFECTCODE;
        case 5:
            return SUBDEFECTCODE;

            // Not a valid Decision Method type
        default:
                return null;
        
        }
    }
	
        

}
