package com.cupola.fwmp.vo.fr;

public class ShiftingRequestVo
{
	private String oldCustomerAcc;
	private String newCustomerAcc;
	private String cxIp;
	private String cxMac;
	private String ticketId;
	
	public ShiftingRequestVo(){}
	
	public String getOldCustomerAcc() {
		return oldCustomerAcc;
	}
	public void setOldCustomerAcc(String oldCustomerAcc) {
		this.oldCustomerAcc = oldCustomerAcc;
	}
	public String getNewCustomerAcc() {
		return newCustomerAcc;
	}
	public void setNewCustomerAcc(String newCustomerAcc) {
		this.newCustomerAcc = newCustomerAcc;
	}
	public String getCxIp() {
		return cxIp;
	}
	public void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}
	public String getCxMac() {
		return cxMac;
	}
	public void setCxMac(String cxMac) {
		this.cxMac = cxMac;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	
	
}
