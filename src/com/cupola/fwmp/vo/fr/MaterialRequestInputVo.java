package com.cupola.fwmp.vo.fr;

import java.util.Map;

public class MaterialRequestInputVo
{

	private String ticketId;
	private Map<String, String> subActivityDetail;
	private String materialQty;
	private boolean editable;
	
	public MaterialRequestInputVo(){
		
	}
	
	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	public Map<String, String> getSubActivityDetail()
	{
		return subActivityDetail;
	}

	public void setSubActivityDetail(Map<String, String> subActivityDetail)
	{
		this.subActivityDetail = subActivityDetail;
	}

	public String getMaterialQty()
	{
		return materialQty;
	}

	public void setMaterialQty(String materialQty)
	{
		this.materialQty = materialQty;
	}

	@Override
	public String toString()
	{
		return "MaterialRequestInputVo [ticketId=" + ticketId
				+ ", subActivityDetail=" + subActivityDetail + ", materialQty="
				+ materialQty + ", getTicketId()=" + getTicketId()
				+ ", getSubActivityDetail()=" + getSubActivityDetail()
				+ ", getMaterialQty()=" + getMaterialQty() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	public boolean isEditable()
	{
		return editable;
	}

	public void setEditable(boolean editable)
	{
		this.editable = editable;
	}
	
}
