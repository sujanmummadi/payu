package com.cupola.fwmp.vo.fr;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.vo.TypeAheadVo;

public class FlowData {
	private List<Decision> decisionList;
	private List<Activity> activityList;
	private List<DefectCode> defectCodeList;
	private List<SubDefectCode>subDefectCodeList;
	private List<ETRDetails> etrList;
	private List<AndroidFlowCxDown> cxDown;
	private List<AndroidFlowCxDown> cxUp;
	private List<AndroidFlowCxDown> slowPeed;
	private List<AndroidFlowCxDown> frequentDisconnection;
	private List<AndroidFlowCxDown> seniourElement;
	private List<TypeAheadVo> fiberTypes;
	private List<FRVendorVO> vendorDetails;
	private Map<String,List<String>> flowDefectSubDefectCodes;
	private List<TypeAheadVo> pushBackReasonCode;
	private ActWorkingHoursVo actWorkingHoursVo;
	private FlowWiseDefectAndSubDefectCodeData flowWiseDefectAndSubDefectCodeData;
	
	
	public List<Decision> getDecisionList() {
		return decisionList;
	}
	public void setDecisionList(List<Decision> decisionList) {
		this.decisionList = decisionList;
	}
	public List<Activity> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<Activity> activityList) {
		this.activityList = activityList;
	}
	public List<DefectCode> getDefectCodeList() {
		return defectCodeList;
	}
	public void setDefectCodeList(List<DefectCode> defectCodeList) {
		this.defectCodeList = defectCodeList;
	}
	public List<SubDefectCode> getSubDefectCodeList() {
		return subDefectCodeList;
	}
	public void setSubDefectCodeList(List<SubDefectCode> subDefectCodeList) {
		this.subDefectCodeList = subDefectCodeList;
	}
	public List<ETRDetails> getEtrList() {
		return etrList;
	}
	public void setEtrList(List<ETRDetails> etrList) {
		this.etrList = etrList;
	}
	public List<AndroidFlowCxDown> getCxDown() {
		return cxDown;
	}
	public void setCxDown(List<AndroidFlowCxDown> cxDown) {
		this.cxDown = cxDown;
	}
	public List<AndroidFlowCxDown> getCxUp() {
		return cxUp;
	}
	public void setCxUp(List<AndroidFlowCxDown> cxUp) {
		this.cxUp = cxUp;
	}
	public List<AndroidFlowCxDown> getSlowPeed() {
		return slowPeed;
	}
	public void setSlowPeed(List<AndroidFlowCxDown> slowPeed) {
		this.slowPeed = slowPeed;
	}
	public List<AndroidFlowCxDown> getFrequentDisconnection() {
		return frequentDisconnection;
	}
	public void setFrequentDisconnection(List<AndroidFlowCxDown> frequentDisconnection) {
		this.frequentDisconnection = frequentDisconnection;
	}
	public List<AndroidFlowCxDown> getSeniourElement() {
		return seniourElement;
	}
	public void setSeniourElement(List<AndroidFlowCxDown> seniourElement) {
		this.seniourElement = seniourElement;
	}
	
	public List<TypeAheadVo> getFiberTypes() {
		return fiberTypes;
	}
	public void setFiberTypes(List<TypeAheadVo> fiberTypes) {
		this.fiberTypes = fiberTypes;
	}
	
	public List<FRVendorVO> getVendorDetails() {
		return vendorDetails;
	}
	public void setVendorDetails(List<FRVendorVO> vendorDetails) {
		this.vendorDetails = vendorDetails;
	}
	
	
	public Map<String, List<String>> getFlowDefectSubDefectCodes() {
		return flowDefectSubDefectCodes;
	}
	public void setFlowDefectSubDefectCodes(Map<String, List<String>> flowDefectSubDefectCodes) {
		this.flowDefectSubDefectCodes = flowDefectSubDefectCodes;
	}
	 
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FlowData [decisionList=" + decisionList + ", activityList=" + activityList + ", defectCodeList="
				+ defectCodeList + ", subDefectCodeList=" + subDefectCodeList + ", etrList=" + etrList + ", cxDown="
				+ cxDown + ", cxUp=" + cxUp + ", slowPeed=" + slowPeed + ", frequentDisconnection="
				+ frequentDisconnection + ", seniourElement=" + seniourElement + ", fiberTypes=" + fiberTypes
				+ ", vendorDetails=" + vendorDetails + ", flowDefectSubDefectCodes=" + flowDefectSubDefectCodes
				+ ", pushBackReasonCode=" + pushBackReasonCode + ", actWorkingHoursVo=" + actWorkingHoursVo
				+ ", flowWiseDefectAndSubDefectCodeData=" + flowWiseDefectAndSubDefectCodeData + "]";
	}
	public List<TypeAheadVo> getPushBackReasonCode() {
		return pushBackReasonCode;
	}
	public void setPushBackReasonCode(List<TypeAheadVo> pushBackReasonCode) {
		this.pushBackReasonCode = pushBackReasonCode;
	}
	public ActWorkingHoursVo getActWorkingHoursVo()
	{
		return actWorkingHoursVo;
	}
	public void setActWorkingHoursVo(ActWorkingHoursVo actWorkingHoursVo)
	{
		this.actWorkingHoursVo = actWorkingHoursVo;
	}
	/**
	 * @return the flowWiseDefectAndSubDefectCodeData
	 */
	public FlowWiseDefectAndSubDefectCodeData getFlowWiseDefectAndSubDefectCodeData() {
		return flowWiseDefectAndSubDefectCodeData;
	}
	/**
	 * @param flowWiseDefectAndSubDefectCodeData the flowWiseDefectAndSubDefectCodeData to set
	 */
	public void setFlowWiseDefectAndSubDefectCodeData(FlowWiseDefectAndSubDefectCodeData flowWiseDefectAndSubDefectCodeData) {
		this.flowWiseDefectAndSubDefectCodeData = flowWiseDefectAndSubDefectCodeData;
	}
	
	
	
	
	
	}
