package com.cupola.fwmp.vo.fr;

public class FrDefectRequestVo
{
	private String fileName;
	private String filePath;
	private String addOrRefreshOption;
	
	public String getFileName()
	{
		return fileName;
	}
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	public String getFilePath()
	{
		return filePath;
	}
	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}
	public String getAddOrRefreshOption()
	{
		return addOrRefreshOption;
	}
	public void setAddOrRefreshOption(String addOrRefreshOption)
	{
		this.addOrRefreshOption = addOrRefreshOption;
	}
	
	
}
