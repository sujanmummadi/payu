package com.cupola.fwmp.vo.fr;

public class AndroidFlowCxDown {
	private int id;
	private String type;
	private String typeId;
	private String selectedOption;
	private String jumpId;
	private String androidId;
	private String stepId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}
	public String getJumpId() {
		return jumpId;
	}
	public void setJumpId(String jumpId) {
		this.jumpId = jumpId;
	}
	public String getAndroidId() {
		return androidId;
	}
	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	@Override
	public String toString() {
		return "AndroidFlowCxDown [id=" + id + ", type=" + type + ", typeId="
				+ typeId + ", selectedOption=" + selectedOption + ", jumpId="
				+ jumpId + ", androidId=" + androidId + ", stepId=" + stepId
				+ "]";
	}
	
	

}
