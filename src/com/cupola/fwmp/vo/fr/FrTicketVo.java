package com.cupola.fwmp.vo.fr;


public class FrTicketVo
{
	private Long id;
	private long userByAssignedBy;
	private long userByAssignedTo;
	private long ticketId;
	private String category;
	private String subCategory;
	private Integer symptomCode;
	private Integer updatedSymptomCode;
	private String currentProgress;
	private String natureCode;
	private String addedOn;
	private Long addedBy;
	private Long modifiedBy;
	private String modifiedOn;
	private Integer status;
	private String voc;
	private Integer subCode;
	private String reqStatus;
	private Long flowId;
	private String workOrderNumber;
	private Long deviceId;
	private boolean isLocked;
	private boolean blocked;
	
	private Long currentFlowId;
	private String currentSymptomName;
	private String fxName;
	
	private String source;
	
	public FrTicketVo(){}

	public FrTicketVo(Long id, long userByAssignedBy,
			long userByAssignedTo, long ticketId, String category,
			String subCategory, Integer symptomCode,
			Integer updatedSymptomCode, String currentProgress,
			String natureCode, String addedOn, Long addedBy, Long modifiedBy,
			String modifiedOn, Integer status, String voc, Integer subCode,
			String reqStatus,Long deviceId,String workOrderNumber,
			boolean isLocked,Long currentFlowId,String currentSympotmName,boolean blocked,String source)
	{
		super();
		this.id = id;
		this.userByAssignedBy = userByAssignedBy;
		this.userByAssignedTo = userByAssignedTo;
		this.ticketId = ticketId;
		this.category = category;
		this.subCategory = subCategory;
		this.symptomCode = symptomCode;
		this.updatedSymptomCode = updatedSymptomCode;
		this.currentProgress = currentProgress;
		this.natureCode = natureCode;
		this.addedOn = addedOn;
		this.addedBy = addedBy;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.status = status;
		this.voc = voc;
		this.subCode = subCode;
		this.reqStatus = reqStatus;
		this.deviceId = deviceId;
		this.workOrderNumber = workOrderNumber;
		this.isLocked = isLocked;
		this.currentFlowId = currentFlowId;
		this.currentSymptomName = currentSympotmName;
		this.blocked = blocked;
		this.source=source;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public Long getCurrentFlowId() {
		return currentFlowId;
	}

	public void setCurrentFlowId(Long currentFlowId) {
		this.currentFlowId = currentFlowId;
	}

	public String getCurrentSymptomName() {
		return currentSymptomName;
	}

	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public long getUserByAssignedBy()
	{
		return userByAssignedBy;
	}

	public void setUserByAssignedBy(long userByAssignedBy)
	{
		this.userByAssignedBy = userByAssignedBy;
	}

	public long getUserByAssignedTo()
	{
		return userByAssignedTo;
	}

	public void setUserByAssignedTo(long userByAssignedTo)
	{
		this.userByAssignedTo = userByAssignedTo;
	}
	
	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}

	public String getSubCategory()
	{
		return subCategory;
	}

	public void setSubCategory(String subCategory)
	{
		this.subCategory = subCategory;
	}

	public Integer getSymptomCode()
	{
		return symptomCode;
	}

	public void setSymptomCode(Integer symptomCode)
	{
		this.symptomCode = symptomCode;
	}

	public Integer getUpdatedSymptomCode()
	{
		return updatedSymptomCode;
	}

	public void setUpdatedSymptomCode(Integer updatedSymptomCode)
	{
		this.updatedSymptomCode = updatedSymptomCode;
	}

	public String getCurrentProgress()
	{
		return currentProgress;
	}

	public void setCurrentProgress(String currentProgress)
	{
		this.currentProgress = currentProgress;
	}

	public String getNatureCode()
	{
		return natureCode;
	}

	public void setNatureCode(String natureCode)
	{
		this.natureCode = natureCode;
	}

	public String getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(String addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedOn()
	{
		return modifiedOn;
	}

	public String getReqStatus()
	{
		return reqStatus;
	}

	public void setReqStatus(String reqStatus)
	{
		this.reqStatus = reqStatus;
	}

	public void setModifiedOn(String modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public String getVoc()
	{
		return voc;
	}

	public void setVoc(String voc)
	{
		this.voc = voc;
	}

	public Integer getSubCode()
	{
		return subCode;
	}

	public void setSubCode(Integer subCode)
	{
		this.subCode = subCode;
	}

	public Long getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(Long deviceId)
	{
		this.deviceId = deviceId;
	}
	
	public Long getFlowId()
	{
		return flowId;
	}

	public void setFlowId(Long flowId)
	{
		this.flowId = flowId;
	}

	public String getFxName() {
		return fxName;
	}

	public void setFxName(String fxName) {
		this.fxName = fxName;
	}

	
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FrTicketVo [id=" + id + ", userByAssignedBy=" + userByAssignedBy + ", userByAssignedTo="
				+ userByAssignedTo + ", ticketId=" + ticketId + ", category=" + category + ", subCategory="
				+ subCategory + ", symptomCode=" + symptomCode + ", updatedSymptomCode=" + updatedSymptomCode
				+ ", currentProgress=" + currentProgress + ", natureCode=" + natureCode + ", addedOn=" + addedOn
				+ ", addedBy=" + addedBy + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + ", status="
				+ status + ", voc=" + voc + ", subCode=" + subCode + ", reqStatus=" + reqStatus + ", flowId=" + flowId
				+ ", workOrderNumber=" + workOrderNumber + ", deviceId=" + deviceId + ", isLocked=" + isLocked
				+ ", blocked=" + blocked + ", currentFlowId=" + currentFlowId + ", currentSymptomName="
				+ currentSymptomName + ", fxName=" + fxName + ", source=" + source + "]";
	}

	

}
