package com.cupola.fwmp.vo.fr;

import java.util.Date;
import java.util.List;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class FrTicketDetailsVO {


	/* ************************ TICKET RELATED INFO ***************************/
	
	@JsonFormat(shape=Shape.STRING)
	private String id;
	
	private long  assignedToId;
	@JsonFormat(shape=Shape.STRING)
	private String ticketId;
	private String assignedToDisplayName;
	private String assignedNEMobileNumber;
	private String prospectNo;

	private String category;
	private String subCategory;

	private String workOrderNumber;
	private String commitedEtr;
	private String currentEtr;
	private String communicatedEtr;

	private String status;
	private long statusId;
	private String createdDate;

	private String cxIp;
	private Integer wipCode;
	private Integer reopenCount;
	private Integer pushBackCount;

	private boolean isLocked;
	private boolean blocked;
	private boolean editable;

	private String customerId;
	private String mqId;

	private Integer priority;
	private Integer updatedCount;
	private Integer escalatedValue;
	private Integer sliderEscalatedValue;
	private Integer priorityEscalationType;

	private String oldCustAccNo;
	private String newCustAccNo;
	private String attributeValue;

	private String reason;
	private String remarks;

	private String defectCode;
	private String subDefectCode;
	private String modifiedDisplayName;

	/*
	 * ************************ TICKET RELATED INFO END
	 ***************************/

	private long frId;
	private String symptomCode;
	private String updatedSymptomCode;
	private String currentProgress;
	private String natureCode;
	private String addedOn;
	private String statusList;

	private Long addedBy;
	private Long modifiedBy;
	private Date modifiedOn;

	private String voc;
	private Integer subCode;
	private String reqStatus;

	private Long flowId;
	private String flowName;

	private Long currentFlowId;
	private String currentSymptomName;
	private String currentFlowName;
	private String flowSwitch;

	/* ************************ DEVICE RELATED INFO ***************************/

	private Long deviceId;
	private String deviceName;
	private String ipAddress;
	private String macAddress;
	private Long deviceType;
	private Long availablePort;
	private String deviceStatus;

	/*
	 * ************************ FX & CX RELATED INFO
	 ***************************/
	private String fxName;
	private String fxIp;
	private String fxMac;
	private long fxPorts;

	private String cxName;
	private String cxMac;
	private long cxPorts;
	private String ticketGART;

	/*
	 * ************************* CUSTOMER RELATED INFO
	 ***************************/

	private String customerName;
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String customerAddress;// Customer current address
	private String customerPermanentAddress;// Customer Aadhaar address
	private String customerBillingAddress;// Customer biling address
	private List<CustomerAddressVO> customerAddressesList;
	private String profession;
	private String officeNumber;
	private String pincode;
	private String city;
	private long cityId;
	private long branchId;
	private String branch;
	private String mobileNumber;
	private String customerAltMobile;
	private String customerEmail;
	private String customerAltEmail;

	private long areaId;
	private String areaName;
	
	private Long subAreaId;
	private String subAreaName;

	private String longitude;
	private String lattitude;

	private String planName;
	private long planId = TicketStatus.OPEN;
	private String planType;
	private String planAmount;
	private String planCategoery;

	private List<FRVendorVO> vendors;

	private FRVendorVO vendor;
	private int proceedStatus;
	private String source;

	/*
	 * ************************* CUSTOMER RELATED INFO END
	 ***************************/


	public boolean isLocked() {
		return isLocked;
	}

	public String getFlowSwitch() {
		return flowSwitch;
	}

	public void setFlowSwitch(String flowSwitch) {
		this.flowSwitch = flowSwitch;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public String getCxIp() {
		return cxIp;
	}

	public void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}

	public Integer getWipCode() {
		return wipCode;
	}

	public void setWipCode(Integer wipCode) {
		this.wipCode = wipCode;
	}

	public Integer getReopenCount() {
		return reopenCount;
	}

	public void setReopenCount(Integer reopenCount) {
		this.reopenCount = reopenCount;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getUpdatedCount() {
		return updatedCount;
	}

	public void setUpdatedCount(Integer updatedCount) {
		this.updatedCount = updatedCount;
	}

	public Integer getEscalatedValue() {
		return escalatedValue;
	}

	public void setEscalatedValue(Integer escalatedValue) {
		this.escalatedValue = escalatedValue;
	}

	public Integer getSliderEscalatedValue() {
		return sliderEscalatedValue;
	}

	public void setSliderEscalatedValue(Integer sliderEscalatedValue) {
		this.sliderEscalatedValue = sliderEscalatedValue;
	}

	public Integer getPriorityEscalationType() {
		return priorityEscalationType;
	}

	public void setPriorityEscalationType(Integer priorityEscalationType) {
		this.priorityEscalationType = priorityEscalationType;
	}

	public Long getCurrentFlowId() {
		return currentFlowId;
	}

	public void setCurrentFlowId(Long currentFlowId) {
		this.currentFlowId = currentFlowId;
	}

	public String getCurrentSymptomName() {
		return currentSymptomName;
	}

	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}

	public String getCurrentFlowName() {
		return currentFlowName;
	}

	public void setCurrentFlowName(String currentFlowName) {
		this.currentFlowName = currentFlowName;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public String getNatureCode() {
		return natureCode;
	}

	public void setNatureCode(String natureCode) {
		this.natureCode = natureCode;
	}

	public String getWorkOrderNumber() {
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	public long getAssignedToId() {
		return assignedToId;
	}

	public void setAssignedToId(long assignedToId) {
		this.assignedToId = assignedToId;
	}

	public String getAssignedToDisplayName() {
		return assignedToDisplayName;
	}

	public void setAssignedToDisplayName(String assignedToDisplayName) {
		this.assignedToDisplayName = assignedToDisplayName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getSymptomCode() {
		return symptomCode;
	}

	public void setSymptomCode(String symptomCode) {
		this.symptomCode = symptomCode;
	}

	public String getUpdatedSymptomCode() {
		return updatedSymptomCode;
	}

	public void setUpdatedSymptomCode(String updatedSymptomCode) {
		this.updatedSymptomCode = updatedSymptomCode;
	}

	public String getCurrentProgress() {
		return currentProgress;
	}

	public void setCurrentProgress(String currentProgress) {
		this.currentProgress = currentProgress;
	}

	public String getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(String addedOn) {
		this.addedOn = addedOn;
	}

	public Long getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVoc() {
		return voc;
	}

	public void setVoc(String voc) {
		this.voc = voc;
	}

	public Integer getSubCode() {
		return subCode;
	}

	public void setSubCode(Integer subCode) {
		this.subCode = subCode;
	}

	public String getReqStatus() {
		return reqStatus;
	}

	public void setReqStatus(String reqStatus) {
		this.reqStatus = reqStatus;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getProspectNo() {
		return prospectNo;
	}

	public void setProspectNo(String prospectNo) {
		this.prospectNo = prospectNo;
	}

	public Long getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(Long deviceType) {
		this.deviceType = deviceType;
	}

	public Long getAvailablePort() {
		return availablePort;
	}

	public void setAvailablePort(Long availablePort) {
		this.availablePort = availablePort;
	}

	public String getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getCommitedEtr() {
		return commitedEtr;
	}

	public void setCommitedEtr(String commitedEtr) {
		this.commitedEtr = commitedEtr;
	}

	public String getCurrentEtr() {
		return currentEtr;
	}

	public void setCurrentEtr(String currentEtr) {
		this.currentEtr = currentEtr;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public long getFrId() {
		return frId;
	}

	public void setFrId(long frId) {
		this.frId = frId;
	}

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getMqId() {
		return mqId;
	}

	public void setMqId(String mqId) {
		this.mqId = mqId;
	}

	public String getStatusList() {
		return statusList;
	}

	public void setStatusList(String statusList) {
		this.statusList = statusList;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerPermanentAddress() {
		return customerPermanentAddress;
	}

	public void setCustomerPermanentAddress(String customerPermanentAddress) {
		this.customerPermanentAddress = customerPermanentAddress;
	}

	public String getCustomerBillingAddress() {
		return customerBillingAddress;
	}

	public void setCustomerBillingAddress(String customerBillingAddress) {
		this.customerBillingAddress = customerBillingAddress;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCustomerAltMobile() {
		return customerAltMobile;
	}

	public void setCustomerAltMobile(String customerAltMobile) {
		this.customerAltMobile = customerAltMobile;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerAltEmail() {
		return customerAltEmail;
	}

	public void setCustomerAltEmail(String customerAltEmail) {
		this.customerAltEmail = customerAltEmail;
	}

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public long getPlanId() {
		return planId;
	}

	public void setPlanId(long planId) {
		this.planId = planId;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(String planAmount) {
		this.planAmount = planAmount;
	}

	public String getPlanCategoery() {
		return planCategoery;
	}

	public void setPlanCategoery(String planCategoery) {
		this.planCategoery = planCategoery;
	}
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}

	public List<FRVendorVO> getVendors() {
		return vendors;
	}

	public void setVendors(List<FRVendorVO> vendors) {
		this.vendors = vendors;
	}

	public int getProceedStatus() {
		return proceedStatus;
	}

	public void setProceedStatus(int proceedStatus) {
		this.proceedStatus = proceedStatus;
	}

	public FRVendorVO getVendor() {
		return vendor;
	}

	public void setVendor(FRVendorVO vendor) {
		this.vendor = vendor;
	}

	public String getAssignedNEMobileNumber() {
		return assignedNEMobileNumber;
	}

	public void setAssignedNEMobileNumber(String assignedNEMobileNumber) {
		this.assignedNEMobileNumber = assignedNEMobileNumber;
	}

	public String getFxName() {
		return fxName;
	}

	public void setFxName(String fxName) {
		this.fxName = fxName;
	}

	public String getFxIp() {
		return fxIp;
	}

	public void setFxIp(String fxIp) {
		this.fxIp = fxIp;
	}

	public String getFxMac() {
		return fxMac;
	}

	public void setFxMac(String fxMac) {
		this.fxMac = fxMac;
	}

	public long getFxPorts() {
		return fxPorts;
	}

	public void setFxPorts(long fxPorts) {
		this.fxPorts = fxPorts;
	}

	public String getCxName() {
		return cxName;
	}

	public void setCxName(String cxName) {
		this.cxName = cxName;
	}

	public String getCxMac() {
		return cxMac;
	}

	public void setCxMac(String cxMac) {
		this.cxMac = cxMac;
	}

	public long getCxPorts() {
		return cxPorts;
	}

	public void setCxPorts(long cxPorts) {
		this.cxPorts = cxPorts;
	}

	public String getTicketGART() {
		return ticketGART;
	}

	public void setTicketGART(String ticketGART) {
		this.ticketGART = ticketGART;
	}

	public String getOldCustAccNo() {
		return oldCustAccNo;
	}

	public void setOldCustAccNo(String oldCustAccNo) {
		this.oldCustAccNo = oldCustAccNo;
	}

	public String getNewCustAccNo() {
		return newCustAccNo;
	}

	public void setNewCustAccNo(String newCustAccNo) {
		this.newCustAccNo = newCustAccNo;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public Integer getPushBackCount() {
		return pushBackCount;
	}

	public void setPushBackCount(Integer pushBackCount) {
		this.pushBackCount = pushBackCount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDefectCode() {
		return defectCode;
	}

	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}

	public String getSubDefectCode() {
		return subDefectCode;
	}

	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}

	public String getModifiedDisplayName() {
		return modifiedDisplayName;
	}

	public void setModifiedDisplayName(String modifiedDisplayName) {
		this.modifiedDisplayName = modifiedDisplayName;
	}

	public String getCommunicatedEtr() {
		return communicatedEtr;
	}

	public void setCommunicatedEtr(String communicatedEtr) {
		this.communicatedEtr = communicatedEtr;
	}

	/**
	 * @return the customerAddressesList
	 */
	public List<CustomerAddressVO> getCustomerAddressesList() {
		return customerAddressesList;
	}

	/**
	 * @param customerAddressesList
	 *            the customerAddressesList to set
	 */
	public void setCustomerAddressesList(List<CustomerAddressVO> customerAddressesList) {
		this.customerAddressesList = customerAddressesList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FrTicketDetailsVO [id=" + id + ", assignedToId=" + assignedToId + ", ticketId=" + ticketId
				+ ", assignedToDisplayName=" + assignedToDisplayName + ", assignedNEMobileNumber="
				+ assignedNEMobileNumber + ", prospectNo=" + prospectNo + ", category=" + category + ", subCategory="
				+ subCategory + ", workOrderNumber=" + workOrderNumber + ", commitedEtr=" + commitedEtr
				+ ", currentEtr=" + currentEtr + ", communicatedEtr=" + communicatedEtr + ", status=" + status
				+ ", statusId=" + statusId + ", createdDate=" + createdDate + ", cxIp=" + cxIp + ", wipCode=" + wipCode
				+ ", reopenCount=" + reopenCount + ", pushBackCount=" + pushBackCount + ", isLocked=" + isLocked
				+ ", blocked=" + blocked + ", editable=" + editable + ", customerId=" + customerId + ", mqId=" + mqId
				+ ", priority=" + priority + ", updatedCount=" + updatedCount + ", escalatedValue=" + escalatedValue
				+ ", sliderEscalatedValue=" + sliderEscalatedValue + ", priorityEscalationType="
				+ priorityEscalationType + ", oldCustAccNo=" + oldCustAccNo + ", newCustAccNo=" + newCustAccNo
				+ ", attributeValue=" + attributeValue + ", reason=" + reason + ", remarks=" + remarks + ", defectCode="
				+ defectCode + ", subDefectCode=" + subDefectCode + ", modifiedDisplayName=" + modifiedDisplayName
				+ ", frId=" + frId + ", symptomCode=" + symptomCode + ", updatedSymptomCode=" + updatedSymptomCode
				+ ", currentProgress=" + currentProgress + ", natureCode=" + natureCode + ", addedOn=" + addedOn
				+ ", statusList=" + statusList + ", addedBy=" + addedBy + ", modifiedBy=" + modifiedBy + ", modifiedOn="
				+ modifiedOn + ", voc=" + voc + ", subCode=" + subCode + ", reqStatus=" + reqStatus + ", flowId="
				+ flowId + ", flowName=" + flowName + ", currentFlowId=" + currentFlowId + ", currentSymptomName="
				+ currentSymptomName + ", currentFlowName=" + currentFlowName + ", flowSwitch=" + flowSwitch
				+ ", deviceId=" + deviceId + ", deviceName=" + deviceName + ", ipAddress=" + ipAddress + ", macAddress="
				+ macAddress + ", deviceType=" + deviceType + ", availablePort=" + availablePort + ", deviceStatus="
				+ deviceStatus + ", fxName=" + fxName + ", fxIp=" + fxIp + ", fxMac=" + fxMac + ", fxPorts=" + fxPorts
				+ ", cxName=" + cxName + ", cxMac=" + cxMac + ", cxPorts=" + cxPorts + ", ticketGART=" + ticketGART
				+ ", customerName=" + customerName + ", title=" + title + ", firstName=" + firstName + ", middleName="
				+ middleName + ", lastName=" + lastName + ", customerAddress=" + customerAddress
				+ ", customerPermanentAddress=" + customerPermanentAddress + ", customerBillingAddress="
				+ customerBillingAddress + ", profession=" + profession + ", officeNumber=" + officeNumber
				+ ", pincode=" + pincode + ", city=" + city + ", cityId=" + cityId + ", branchId=" + branchId
				+ ", branch=" + branch + ", mobileNumber=" + mobileNumber + ", customerAltMobile=" + customerAltMobile
				+ ", customerEmail=" + customerEmail + ", customerAltEmail=" + customerAltEmail + ", areaId=" + areaId
				+ ", areaName=" + areaName + ", longitude=" + longitude + ", lattitude=" + lattitude + ", planName="
				+ planName + ", planId=" + planId + ", planType=" + planType + ", planAmount=" + planAmount
				+ ", planCategoery=" + planCategoery + ", vendors=" + vendors + ", vendor=" + vendor
				+ ", proceedStatus=" + proceedStatus + ", customerAddressesList=" + customerAddressesList + "]";
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source
	 *            the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public Long getSubAreaId() {
		return subAreaId;
	}

	public void setSubAreaId(long subAreaId) {
		this.subAreaId = subAreaId;
	}

	public String getSubAreaName() {
		return subAreaName;
	}

	public void setSubAreaName(String subAreaName) {
		this.subAreaName = subAreaName;
	}
	
	

}
