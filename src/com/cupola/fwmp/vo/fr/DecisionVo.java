package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class DecisionVo {
	private int decisionId;
	private int noOfOptions;
	private int selectedOption;
	private Date timeStamp;
	public int getDecisionId() {
		return decisionId;
	}
	public void setDecisionId(int decisionId) {
		this.decisionId = decisionId;
	}
	public int getNoOfOptions() {
		return noOfOptions;
	}
	public void setNoOfOptions(int noOfOptions) {
		this.noOfOptions = noOfOptions;
	}
	public int getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	@Override
	public String toString() {
		return "DecisionVo [decisionId=" + decisionId + ", noOfOptions="
				+ noOfOptions + ", selectedOption=" + selectedOption
				+ ", timeStamp=" + timeStamp + "]";
	}
	
	

}
