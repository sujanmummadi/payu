package com.cupola.fwmp.vo.fr;

public class SubDefectCode {
	private int subDefectCodeId;
	private String subDefectCodeName;
	public int getSubDefectCodeId() {
		return subDefectCodeId;
	}
	public void setSubDefectCodeId(int subDefectCodeId) {
		this.subDefectCodeId = subDefectCodeId;
	}
	public String getSubDefectCodeName() {
		return subDefectCodeName;
	}
	public void setSubDefectCodeName(String subDefectCodeName) {
		this.subDefectCodeName = subDefectCodeName;
	}
	@Override
	public String toString() {
		return "SubDefectCode [subDefectCodeId=" + subDefectCodeId
				+ ", subDefectCodeName=" + subDefectCodeName + "]";
	}
	

}
