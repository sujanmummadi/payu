package com.cupola.fwmp.vo.fr;

public enum ActivityMethod {
	AP_THEFT_REPORT(0),
	AP_MATERIAL_REQUEST(1),
	AP_MATERIAL_CONSUMED(2),
	AP_INSTALL_NEW_CX(3),
	AP_CLOSE_TICKET(4),
	AP_REQUEST_CX_UP(5),
	AP_FAULTY_BATERY(6),
	AP_SUB_ACTIVITY_TICKET(7),
	AP_REQUEST_RENTAL(8),
	AP_SHIFTING_CX(9),
	AP_NEW_LOCATION_DETAILS_CX(10),
	AP_UPDATE_INTERNAL_CX(11),
	AP_CABLING_REQUIRED(12),
	AP_NEW_CUSTOMER_DETAILS(13),
	AP_ALERT_FINANCE_TEAM(14),
	AP_LCO_TEAM(15),
	AP_CHECK_VLAN_MISMATCH(16),
	AP_SOFT_REBOOT(17),
	AP_FAULTY_CX(18),
	AP_REQUEST_ROUTER_REPLACEMENT(19),
	AP_REPLACE_ROUTER(20),
	AP_DUPLEX_SETTING(21);

	
	

    private final int typeVal;

	ActivityMethod(int val) {
        typeVal = val;
    }

    /*
     * Get the value for the header field of this particular Activity Method
     * return the Activity Method suitible for bitwise combination
     */
    public int getValue() {
        return typeVal;
    }

    /*
     * Get the ActivityClass enum from the type field integer
     * return the appropriate ActivityClass
     */
   public static ActivityMethod getActivityMethod(int typeField) {
        switch (typeField) {
            // Had to hard code these constants
            case 0:
                return AP_THEFT_REPORT;
            case 1:
                return AP_MATERIAL_REQUEST;
            
            case 2:
                return AP_MATERIAL_CONSUMED;
            case 3:
                return AP_INSTALL_NEW_CX;
            case 4:
                return AP_CLOSE_TICKET;
            case 5:
                return AP_REQUEST_CX_UP;
            case 6:
                return AP_FAULTY_BATERY;
            case 7:
                return AP_SUB_ACTIVITY_TICKET;
            case 8:
                return AP_REQUEST_RENTAL;
            case 9:
                return AP_SHIFTING_CX;
            case 10:
                return AP_NEW_LOCATION_DETAILS_CX;
            case 11:
                return AP_UPDATE_INTERNAL_CX;
            case 12:
                return AP_CABLING_REQUIRED;
            case 13:
                return AP_NEW_CUSTOMER_DETAILS;
            case 14:
                return AP_ALERT_FINANCE_TEAM;
            case 15:
                return AP_LCO_TEAM;
            case 16:
                return AP_CHECK_VLAN_MISMATCH;
            case 17:
                return AP_SOFT_REBOOT;
            case 18:
                return AP_FAULTY_CX;
            case 19:
                return AP_REQUEST_ROUTER_REPLACEMENT;
            case 20:
                return AP_REPLACE_ROUTER;
            case 21:
                return AP_DUPLEX_SETTING;

            // Not a valid Decision Method type
            default:
                return null;
        }

    }

	

}
