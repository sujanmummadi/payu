package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class FRVendorFilter
{
	private long reportToUserId;
	private long assocatedToUserId;
	private long skillId;
	private long ticketId;
	private Long status;
	private int startOffset;
	private int pageSize;
	private Date toDate;
	private Date fromDate;
	private long vendorUserId;
	public long getReportToUserId()
	{
		return reportToUserId;
	}

	public void setReportToUserId(long reportToUserId)
	{
		this.reportToUserId = reportToUserId;
	}

	public long getAssocatedToUserId()
	{
		return assocatedToUserId;
	}

	public void setAssocatedToUserId(long assocatedToUserId)
	{
		this.assocatedToUserId = assocatedToUserId;
	}

	public long getSkillId()
	{
		return skillId;
	}

	public void setSkillId(long skillId)
	{
		this.skillId = skillId;
	}

	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	@Override
	public String toString()
	{
		return "FRVendorFilter [reportToUserId=" + reportToUserId
				+ ", assocatedToUserId=" + assocatedToUserId + ", skillId="
				+ skillId + ", ticketId=" + ticketId + "]";
	}

	public Long getStatus()
	{
		return status;
	}

	public void setStatus(Long status)
	{
		this.status = status;
	}

	public int getStartOffset()
	{
		return startOffset;
	}

	public void setStartOffset(int startOffset)
	{
		this.startOffset = startOffset;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}

	public Date getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate)
	{
		this.fromDate = fromDate;
	}

	public long getVendorUserId()
	{
		return vendorUserId;
	}

	public void setVendorUserId(long vendorUserId)
	{
		this.vendorUserId = vendorUserId;
	}

	 
}
