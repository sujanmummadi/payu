package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class FrAllocationReportVo 
{
	private Long ticketId;
	private Integer ticketStatus;
	private Long userId;
	private String userName;
	private Long reportTo;
	private String reportToName;
	private Long currentFlowId;
	
	private Long currentAssignedTo;
	private Integer reassignedCount;
	
	private Date ticketCreationDate;
	private Date ticketModifiedDate;
	
	public FrAllocationReportVo(){
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(Integer ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getReportTo() {
		return reportTo;
	}

	public void setReportTo(Long reportTo) {
		this.reportTo = reportTo;
	}

	public String getReportToName() {
		return reportToName;
	}

	public void setReportToName(String reportToName) {
		this.reportToName = reportToName;
	}

	public Long getCurrentFlowId() {
		return currentFlowId;
	}

	public void setCurrentFlowId(Long currentFlowId) {
		this.currentFlowId = currentFlowId;
	}

	public Date getTicketCreationDate() {
		return ticketCreationDate;
	}

	public void setTicketCreationDate(Date ticketCreationDate) {
		this.ticketCreationDate = ticketCreationDate;
	}

	public Date getTicketModifiedDate() {
		return ticketModifiedDate;
	}

	public void setTicketModifiedDate(Date ticketModifiedDate) {
		this.ticketModifiedDate = ticketModifiedDate;
	}

	public Long getCurrentAssignedTo() {
		return currentAssignedTo;
	}

	public void setCurrentAssignedTo(Long currentAssignedTo) {
		this.currentAssignedTo = currentAssignedTo;
	}

	public Integer getReassignedCount() {
		return reassignedCount;
	}

	public void setReassignedCount(Integer reassignedCount) {
		this.reassignedCount = reassignedCount;
	}

	@Override
	public String toString() {
		return "FrAllocationReportVo [ticketId=" + ticketId + ", ticketStatus="
				+ ticketStatus + ", userId=" + userId + ", userName="
				+ userName + ", reportTo=" + reportTo + ", reportToName="
				+ reportToName + ", currentFlowId=" + currentFlowId
				+ ", currentAssignedTo=" + currentAssignedTo
				+ ", reassignedCount=" + reassignedCount
				+ ", ticketCreationDate=" + ticketCreationDate
				+ ", ticketModifiedDate=" + ticketModifiedDate + "]";
	}
	
}
