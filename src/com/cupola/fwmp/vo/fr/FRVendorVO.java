package com.cupola.fwmp.vo.fr;

import java.util.List;

import com.cupola.fwmp.vo.TypeAheadVo;


public class FRVendorVO
{
	private long vendorId;
	private String vendorName;
	private String vendorSkill;
	private Long vendorSkillId;
	
	private Long ticketId;
	private String estimatedStartTime;
	private String estimatedEndTime;
	private String actualStartTime;
	private String actualEndTime;
	private Integer status;
	private String currentStatus;
	
	private List<TypeAheadVo> associatedUsers;

	public FRVendorVO()
	{
		// TODO Auto-generated constructor stub
	}
	public long getVendorId()
	{
		return vendorId;
	}

	public void setVendorId(long vendorId)
	{
		this.vendorId = vendorId;
	}

	public String getVendorName()
	{
		return vendorName;
	}

	public void setVendorName(String vendorName)
	{
		this.vendorName = vendorName;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getEstimatedStartTime()
	{
		return estimatedStartTime;
	}

	public void setEstimatedStartTime(String estimatedStartTime)
	{
		this.estimatedStartTime = estimatedStartTime;
	}

	public String getEstimatedEndTime()
	{
		return estimatedEndTime;
	}

	public void setEstimatedEndTime(String estimatedEndTime)
	{
		this.estimatedEndTime = estimatedEndTime;
	}

	public String getActualStartTime()
	{
		return actualStartTime;
	}

	public void setActualStartTime(String actualStartTime)
	{
		this.actualStartTime = actualStartTime;
	}

	public String getActualEndTime()
	{
		return actualEndTime;
	}

	public void setActualEndTime(String actualEndTime)
	{
		this.actualEndTime = actualEndTime;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public String getVendorSkill()
	{
		return vendorSkill;
	}

	public void setVendorSkill(String vendorSkill)
	{
		this.vendorSkill = vendorSkill;
	}

	public Long getVendorSkillId()
	{
		return vendorSkillId;
	}

	public void setVendorSkillId(Long vendorSkillId)
	{
		this.vendorSkillId = vendorSkillId;
	}

	public List<TypeAheadVo> getAssociatedUsers()
	{
		return associatedUsers;
	}

	public void setAssociatedUsers(List<TypeAheadVo> associatedUsers)
	{
		this.associatedUsers = associatedUsers;
	}

	@Override
	public String toString()
	{
		return "FRVendorVO [vendorId=" + vendorId + ", vendorName="
				+ vendorName + ", vendorSkill=" + vendorSkill
				+ ", vendorSkillId=" + vendorSkillId + ", ticketId=" + ticketId
				+ ", estimatedStartTime=" + estimatedStartTime
				+ ", estimatedEndTime=" + estimatedEndTime
				+ ", actualStartTime=" + actualStartTime + ", actualEndTime="
				+ actualEndTime + ", status=" + status + ", associatedUsers="
				+ associatedUsers + "]";
	}

	public FRVendorVO(long vendorId, String vendorName, String vendorSkill,
			Long vendorSkillId)
	{
		super();
		this.vendorId = vendorId;
		this.vendorName = vendorName;
		this.vendorSkill = vendorSkill;
		this.vendorSkillId = vendorSkillId;
	}
	public String getCurrentStatus()
	{
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus)
	{
		this.currentStatus = currentStatus;
	}
}
