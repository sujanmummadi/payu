package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class ETRExpireVo
{
	private Long ticketId;
	private Long currentFlowId;
	private String mobileNumber;
	private Date currentETR;
	private Long mqId;
	private Integer etrElapsedCount;
	private Long cityId;
	private Long branchId;
	
	public Long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}
	public Long getCurrentFlowId()
	{
		return currentFlowId;
	}
	public void setCurrentFlowId(Long currentFlowId)
	{
		this.currentFlowId = currentFlowId;
	}
	public String getMobileNumber()
	{
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}
	public Date getCurrentETR()
	{
		return currentETR;
	}
	public void setCurrentETR(Date currentETR)
	{
		this.currentETR = currentETR;
	}
	public Long getMqId()
	{
		return mqId;
	}
	public void setMqId(Long mqId)
	{
		this.mqId = mqId;
	}
	
	
	public Integer getEtrElapsedCount()
	{
		return etrElapsedCount;
	}
	public void setEtrElapsedCount(Integer etrElapsedCount)
	{
		this.etrElapsedCount = etrElapsedCount;
	}
	public Long getCityId()
	{
		return cityId;
	}
	public void setCityId(Long cityId)
	{
		this.cityId = cityId;
	}
	
	public Long getBranchId()
	{
		return branchId;
	}
	public void setBranchId(Long branchId)
	{
		this.branchId = branchId;
	}
	@Override
	public String toString()
	{
		return "ETRExpireVo [ticketId=" + ticketId + ", currentFlowId="
				+ currentFlowId + ", mobileNumber=" + mobileNumber
				+ ", currentETR=" + currentETR + ", mqId=" + mqId
				+ ", etrElapsedCount=" + etrElapsedCount + ", cityId=" + cityId
				+ "]";
	}

	
	
	

}
