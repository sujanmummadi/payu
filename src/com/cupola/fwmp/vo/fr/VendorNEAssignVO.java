package com.cupola.fwmp.vo.fr;

import java.util.List;

public class VendorNEAssignVO
{
	private long vendorId;
	private List<Long> userIds;// NE userId

	public long getVendorId()
	{
		return vendorId;
	}

	public void setVendorId(long vendorId)
	{
		this.vendorId = vendorId;
	}

	public List<Long> getUserIds()
	{
		return userIds;
	}

	public void setUserIds(List<Long> userIds)
	{
		this.userIds = userIds;
	}

	@Override
	public String toString()
	{
		return "VendorNEAssignVO [vendorId=" + vendorId + ", userIds="
				+ userIds + "]";
	}

}
