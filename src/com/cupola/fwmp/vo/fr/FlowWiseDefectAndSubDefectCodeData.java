/**
 * @Author swapnil  17-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo.fr;

import java.util.List;
import java.util.Map;

/**
 * @Author swapnil 17-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FlowWiseDefectAndSubDefectCodeData
{
	private Map<String, List<String>> cxDownDefectSubDefectCodeMap;

	private Map<String, List<String>> cxUpDefectSubDefectCodeMap;

	private Map<String, List<String>> slowSpeedDefectSubDefectCodeMap;

	private Map<String, List<String>> frequentDisconnectionDefectSubDefectCodeMap;

	private Map<String, List<String>> seniorElementDefectSubDefectCodeMap;

	private Map<String, List<String>> otherDefectSubDefectCodeMap;

	public Map<String, List<String>> getCxDownDefectSubDefectCodeMap()
	{
		return cxDownDefectSubDefectCodeMap;
	}

	public void setCxDownDefectSubDefectCodeMap(
			Map<String, List<String>> cxDownDefectSubDefectCodeMap)
	{
		this.cxDownDefectSubDefectCodeMap = cxDownDefectSubDefectCodeMap;
	}

	public Map<String, List<String>> getCxUpDefectSubDefectCodeMap()
	{
		return cxUpDefectSubDefectCodeMap;
	}

	public void setCxUpDefectSubDefectCodeMap(
			Map<String, List<String>> cxUpDefectSubDefectCodeMap)
	{
		this.cxUpDefectSubDefectCodeMap = cxUpDefectSubDefectCodeMap;
	}

	public Map<String, List<String>> getSlowSpeedDefectSubDefectCodeMap()
	{
		return slowSpeedDefectSubDefectCodeMap;
	}

	public void setSlowSpeedDefectSubDefectCodeMap(
			Map<String, List<String>> slowSpeedDefectSubDefectCodeMap)
	{
		this.slowSpeedDefectSubDefectCodeMap = slowSpeedDefectSubDefectCodeMap;
	}

	public Map<String, List<String>> getFrequentDisconnectionDefectSubDefectCodeMap()
	{
		return frequentDisconnectionDefectSubDefectCodeMap;
	}

	public void setFrequentDisconnectionDefectSubDefectCodeMap(
			Map<String, List<String>> frequentDisconnectionDefectSubDefectCodeMap)
	{
		this.frequentDisconnectionDefectSubDefectCodeMap = frequentDisconnectionDefectSubDefectCodeMap;
	}

	public Map<String, List<String>> getSeniorElementDefectSubDefectCodeMap()
	{
		return seniorElementDefectSubDefectCodeMap;
	}

	public void setSeniorElementDefectSubDefectCodeMap(
			Map<String, List<String>> seniorElementDefectSubDefectCodeMap)
	{
		this.seniorElementDefectSubDefectCodeMap = seniorElementDefectSubDefectCodeMap;
	}

	public Map<String, List<String>> getOtherDefectSubDefectCodeMap()
	{
		return otherDefectSubDefectCodeMap;
	}

	public void setOtherDefectSubDefectCodeMap(
			Map<String, List<String>> otherDefectSubDefectCodeMap)
	{
		this.otherDefectSubDefectCodeMap = otherDefectSubDefectCodeMap;
	}

	@Override
	public String toString()
	{
		return "FlowWiseDefectAndSubDefectCodeData [cxDownDefectSubDefectCodeMap="
				+ cxDownDefectSubDefectCodeMap
				+ ", cxUpDefectSubDefectCodeMap="
				+ cxUpDefectSubDefectCodeMap
				+ ", slowSpeedDefectSubDefectCodeMap="
				+ slowSpeedDefectSubDefectCodeMap
				+ ", frequentDisconnectionDefectSubDefectCodeMap="
				+ frequentDisconnectionDefectSubDefectCodeMap
				+ ", seniorElementDefectSubDefectCodeMap="
				+ seniorElementDefectSubDefectCodeMap
				+ ", otherDefectSubDefectCodeMap="
				+ otherDefectSubDefectCodeMap + "]";
	}

}
