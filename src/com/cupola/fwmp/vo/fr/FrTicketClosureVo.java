package com.cupola.fwmp.vo.fr;

import java.util.List;

public class FrTicketClosureVo
{
	private List<Long> ticketIds;
	private String defectCode;
	private String subDefectCode;
	private String remarks;
	private Long status;
	
	public List<Long> getTicketIds() {
		return ticketIds;
	}
	public void setTicketIds(List<Long> ticketIds) {
		this.ticketIds = ticketIds;
	}
	public String getDefectCode() {
		return defectCode;
	}
	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}
	public String getSubDefectCode() {
		return subDefectCode;
	}
	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	
}
