 /**
 * @author kiran
 * @copyright Cupola Technology
 */
package com.cupola.fwmp.vo.fr;

 /**
 * @author kiran
 * @copyright Cupola Technology
 */
public class ActWorkingHoursVo
{
	private int businessStartHour;
	private int businessEndHour;
	private int workingMinsPerDay;
	public int getBusinessStartHour()
	{
		return businessStartHour;
	}
	public void setBusinessStartHour(int businessStartHour)
	{
		this.businessStartHour = businessStartHour;
	}
	public int getBusinessEndHour()
	{
		return businessEndHour;
	}
	public void setBusinessEndHour(int businessEndHour)
	{
		this.businessEndHour = businessEndHour;
	}
	public int getWorkingMinsPerDay()
	{
		return workingMinsPerDay;
	}
	public void setWorkingMinsPerDay(int workingMinsPerDay)
	{
		this.workingMinsPerDay = workingMinsPerDay;
	}
	
	

}
