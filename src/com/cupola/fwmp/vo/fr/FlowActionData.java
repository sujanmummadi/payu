package com.cupola.fwmp.vo.fr;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "FlowActionData")
public class FlowActionData
{
	@Id
	private long id;
	
	private long ticketId;
	private long actionType;
	private long userId;
	private Map<String, String> additionalAttributes;
	private String remarks;

	public FlowActionData()
	{
	}
	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public long getActionType()
	{
		return actionType;
	}

	public void setActionType(long actionType)
	{
		this.actionType = actionType;
	}

	public Map<String, String> getAdditionalAttributes()
	{
		return additionalAttributes;
	}

	public void setAdditionalAttributes(Map<String, String> additionalAttributes)
	{
		this.additionalAttributes = additionalAttributes;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	@Override
	public String toString()
	{
		return "FlowActionData [ticketId=" + ticketId + ", userId=" + userId
				+ ", actionType=" + actionType + ", additionalAttributes="
				+ additionalAttributes + ", remarks=" + remarks + "]";
	}

	public FlowActionData(long ticketId, long actionType)
	{
		super();
		this.ticketId = ticketId;
		this.actionType = actionType;
	}
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}

}
