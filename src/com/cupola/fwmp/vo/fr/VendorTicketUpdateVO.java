package com.cupola.fwmp.vo.fr;

import java.util.Date;

public class VendorTicketUpdateVO
{
	private long vendorUserId;
	private Date estimatedStartTime;
	private long ticketId;
	private long oldVendorId;
	public long getVendorUserId()
	{
		return vendorUserId;
	}
	public void setVendorUserId(long vendorUserId)
	{
		this.vendorUserId = vendorUserId;
	}
	public Date getEstimatedStartTime()
	{
		return estimatedStartTime;
	}
	public void setEstimatedStartTime(Date estimatedStartTime)
	{
		this.estimatedStartTime = estimatedStartTime;
	}
	public long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}
	public long getOldVendorId()
	{
		return oldVendorId;
	}
	public void setOldVendorId(long oldVendorId)
	{
		this.oldVendorId = oldVendorId;
	}
}
