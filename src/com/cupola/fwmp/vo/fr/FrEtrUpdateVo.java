package com.cupola.fwmp.vo.fr;

public class FrEtrUpdateVo 
{
	private String ticketId;
	private String currentETR;
	private String newETR;
	private String remarks;
	private String reasonCode;
	private String currentProgress;
	
	public FrEtrUpdateVo(){
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getCurrentETR() {
		return currentETR;
	}

	public void setCurrentETR(String currentETR) {
		this.currentETR = currentETR;
	}

	public String getNewETR() {
		return newETR;
	}

	public void setNewETR(String newETR) {
		this.newETR = newETR;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getCurrentProgress() {
		return currentProgress;
	}

	public void setCurrentProgress(String currentProgress) {
		this.currentProgress = currentProgress;
	}

	public FrEtrUpdateVo(String ticketId, String currentETR, String newETR,
			String remarks, String reasonCode, String currentProgress) {
		super();
		this.ticketId = ticketId;
		this.currentETR = currentETR;
		this.newETR = newETR;
		this.remarks = remarks;
		this.reasonCode = reasonCode;
		this.currentProgress = currentProgress;
	}
	public FrEtrUpdateVo(String ticketId, String currentETR, String newETR,
			String remarks, String reasonCode) {
		super();
		this.ticketId = ticketId;
		this.currentETR = currentETR;
		this.newETR = newETR;
		this.remarks = remarks;
		this.reasonCode = reasonCode;
	}
}
