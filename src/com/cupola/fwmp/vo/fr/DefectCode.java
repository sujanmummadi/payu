package com.cupola.fwmp.vo.fr;

public class DefectCode {
	private int defectCodeId;
	private String defectCodeName;
	public int getDefectCodeId() {
		return defectCodeId;
	}
	public void setDefectCodeId(int defectCodeId) {
		this.defectCodeId = defectCodeId;
	}
	public String getDefectCodeName() {
		return defectCodeName;
	}
	public void setDefectCodeName(String defectCodeName) {
		this.defectCodeName = defectCodeName;
	}
	@Override
	public String toString() {
		return "DefectCode [defectCodeId=" + defectCodeId + ", defectCodeName="
				+ defectCodeName + "]";
	}
	

}
