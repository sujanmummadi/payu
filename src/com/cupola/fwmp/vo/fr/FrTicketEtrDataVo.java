package com.cupola.fwmp.vo.fr;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="SymptomFlowMappingLog")
public class FrTicketEtrDataVo
{
	@Id
	private long ticketId;
	private String workOrderNumber;
	private String mqId;
	private String mobileNumber;
	private String symptomName;
	private String cityCode;
	private String branchCode;
	private Long flowId;
	private Date committedEtr;
	
	public long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}
	public String getWorkOrderNumber()
	{
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber)
	{
		this.workOrderNumber = workOrderNumber;
	}
	public String getMqId()
	{
		return mqId;
	}
	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}
	public String getMobileNumber()
	{
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}
	public String getSymptomName()
	{
		return symptomName;
	}
	public void setSymptomName(String symptomName)
	{
		this.symptomName = symptomName;
	}
	public String getCityCode()
	{
		return cityCode;
	}
	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}
	public String getBranchCode()
	{
		return branchCode;
	}
	public void setBranchCode(String branchCode)
	{
		this.branchCode = branchCode;
	}
	public Long getFlowId()
	{
		return flowId;
	}
	public void setFlowId(Long flowId)
	{
		this.flowId = flowId;
	}
	public Date getCommittedEtr()
	{
		return committedEtr;
	}
	public void setCommittedEtr(Date committedEtr)
	{
		this.committedEtr = committedEtr;
	}
	
}
