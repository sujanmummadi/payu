/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.vo.fr;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ScratchVo {
	private long ticketId;
	private int cbStatus;
	private int status;
	private Date addedOn;
	private Date updatedOn;
	private Set<ScratchPadVo> scratchPads;
	private Set<ScratchPadVo> contexts;
	
	@JsonIgnore
	private Long addedBy;
	
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public int getCbStatus() {
		return cbStatus;
	}
	public void setCbStatus(int cbStatus) {
		this.cbStatus = cbStatus;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Set<ScratchPadVo> getScratchPads() {
		return scratchPads;
	}
	public void setScratchPads(Set<ScratchPadVo> scratchPads) {
		this.scratchPads = scratchPads;
	}
	
	
	public Set<ScratchPadVo> getContexts() {
		return contexts;
	}
	public void setContexts(Set<ScratchPadVo> contexts) {
		this.contexts = contexts;
	}
	
	
	public Long getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}
	
	@Override
	public String toString() {
		return "ScratchVo [ticketId=" + ticketId + ", cbStatus=" + cbStatus
				+ ", status=" + status + ", addedOn=" + addedOn
				+ ", updatedOn=" + updatedOn + ", scratchPads=" + scratchPads
				+ ", contexts=" + contexts + "]";
	}
	
	
	
}
