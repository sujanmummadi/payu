package com.cupola.fwmp.vo.fr;

public class AppResponseVo
{
	private String message;
	private String status;
	
	public AppResponseVo(){}
	
	public AppResponseVo(String status,String message)
	{
		this.status = status;
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
