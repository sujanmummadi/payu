package com.cupola.fwmp.vo.fr;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MaterialRequest")

public class MaterialRequestVo
{
	@Id
	private Long id;
	private Long ticketId;
	private Integer materialRequestId;
	private boolean materialRequestValue;
    private Long materialRequestBy;
    private Date materialRequestOn;
    private Integer status;
   
	
	public MaterialRequestVo(){
	}


	
	public long getId()
	{
		return id;
	}


	public void setId(long id)
	{
		this.id = id;
	}
	
	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}
	
	


	public Integer getMaterialRequestId()
	{
		return materialRequestId;
	}


	public void setMaterialRequestId(Integer materialRequestId)
	{
		this.materialRequestId = materialRequestId;
	}


	public boolean isMaterialRequestValue()
	{
		return materialRequestValue;
	}


	public void setMaterialRequestValue(boolean materialRequestValue)
	{
		this.materialRequestValue = materialRequestValue;
	}


	public Long getMaterialRequestBy()
	{
		return materialRequestBy;
	}


	public void setMaterialRequestBy(Long materialRequestBy)
	{
		this.materialRequestBy = materialRequestBy;
	}


	public Date getMaterialRequestOn()
	{
		return materialRequestOn;
	}


	public void setMaterialRequestOn(Date materialRequestOn)
	{
		this.materialRequestOn = materialRequestOn;
	}



	public Integer getStatus()
	{
		return status;
	}



	public void setStatus(Integer status)
	{
		this.status = status;
	}



	@Override
	public String toString()
	{
		return "MaterialRequestVo [id=" + id + ", ticketId=" + ticketId
				+ ", materialRequestId=" + materialRequestId
				+ ", materialRequestValue=" + materialRequestValue
				+ ", materialRequestBy=" + materialRequestBy
				+ ", materialRequestOn=" + materialRequestOn + "]";
	}


	


	
}
