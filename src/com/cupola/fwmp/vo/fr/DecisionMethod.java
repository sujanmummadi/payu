package com.cupola.fwmp.vo.fr;

public enum DecisionMethod {
	DP_AVAILABLE(1),
	DP_ISSUE_RESOLVED(2),
	DP_CX_WORKING(3),
	DP_POWER_CUT(4),
	DP_BATTERY_AVAILABLE(5),
	DP_BATTERY_DRAINED(6),
	DP_CONNECTIVITY_FINE(7),
	DP_BATTERY_FAULTY(8),
	DP_SWITCHED_OFF(9),
	DP_CABLE_DAMAGED(10),
	DP_CX_POWER_OFF_BY_CUSTOMER(11),
	DP_DISCOUNT_ISSUE(12),
	DP_CUSTOMER_CONVENCIED(13),
	DP_UNSTRUCTURED_CABLE(14),
	DP_SERVICE_ISSUE(15),
	DP_CUSTOMER_VACATE(16),
	DP_PORT_BLINKING(17),
	DP_CORD_FAULTY(18),
	DP_FIBER_CUT(19),
	DP_LOW_LAYING(20),
	DP_LCO(21),
	DP_CONFIGURATION_ISSUE(22),
	DP_VLAN_MISMATCH(23),
	DP_SWITCH_HANGING(24),
	DP_SWITCH_WORKING(25),
	DP_SFP_OVERHEATED(26),
	DP_ELEMENT_UP(27),
	DP_CONNECTED_LAN_CABLE(28),
	DP_RJ45_BROKEN(29),
	DP_ABLE_TO_BROWSE(30),
	DP_ROUTER_HANGING(31),
	DP_ACT_ROUTER(32),
	DP_CAT5_DAMAGED(33),
	DP_PORT_MISMATCH(34),
	DP_ROUTER_WARANTY(35),
	DP_SERVICES_WORKING(36),
	DP_MAX_LIMIT(37);
	
	

    private final int typeVal;

    DecisionMethod(int val) {
        typeVal = val;
    }

    /*
     * Get the value for the header field of this particular Message Method
     * return the Decision Method suitible for bitwise combination
     */
    public int getValue() {
        return typeVal;
    }

    /*
     * Get the DecisionClass enum from the type field integer
     * return the appropriate DecisionClass
     */
  public  static DecisionMethod getDecisionMethod(int typeField) {
        switch (typeField) {
            // Had to hard code these constants
            case 1:
                return DP_AVAILABLE;
            case 2:
                return DP_ISSUE_RESOLVED;
            
            case 3:
                return DP_CX_WORKING;
            case 4:
                return DP_POWER_CUT;
            case 5:
                return DP_BATTERY_AVAILABLE;
            case 6:
                return DP_BATTERY_DRAINED;
            case 7:
                return DP_CONNECTIVITY_FINE;
            case 8:
                return DP_BATTERY_FAULTY;
            case 9:
                return DP_SWITCHED_OFF;
            case 10:
                return DP_CABLE_DAMAGED;
            case 11:
                return DP_CX_POWER_OFF_BY_CUSTOMER;
            case 12:
                return DP_DISCOUNT_ISSUE;
            case 13:
                return DP_CUSTOMER_CONVENCIED;
            case 14:
                return DP_UNSTRUCTURED_CABLE;
            case 15:
                return DP_SERVICE_ISSUE;
            case 16:
                return DP_CUSTOMER_VACATE;
            case 17:
                return DP_PORT_BLINKING;
            case 18:
                return DP_CORD_FAULTY;
            case 19:
                return DP_FIBER_CUT;
            case 20:
                return DP_LOW_LAYING;
            case 21:
                return DP_LCO;
            case 22:
                return DP_CONFIGURATION_ISSUE;
            case 23:
                return DP_VLAN_MISMATCH;
            case 24:
                return DP_SWITCH_HANGING;
            case 25:
                return DP_SWITCH_WORKING;
            case 26:
                return DP_SFP_OVERHEATED;
            case 27:
                return DP_ELEMENT_UP;
            case 28:
                return DP_CONNECTED_LAN_CABLE;
            case 29:
                return DP_RJ45_BROKEN;
            case 30:
                return DP_ABLE_TO_BROWSE;
            case 31:
                return DP_ROUTER_HANGING;
            case 32:
                return DP_ACT_ROUTER;
            case 33:
                return DP_CAT5_DAMAGED;
            case 34:
                return DP_PORT_MISMATCH;
            case 35:
                return DP_ROUTER_WARANTY;
            case 36:
                return DP_SERVICES_WORKING;
            case 37:
                return DP_MAX_LIMIT;

            // Not a valid Decision Method type
            default:
                return null;
        }

    }

	

}
