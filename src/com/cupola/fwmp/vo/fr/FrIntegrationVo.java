package com.cupola.fwmp.vo.fr;

public class FrIntegrationVo 
{
	private long actionType;
	private String deviceIp;
	private String port;
	private String deviceType;
	
	public long getActionType() {
		return actionType;
	}
	
	public void setActionType(long actionType) {
		this.actionType = actionType;
	}
	
	public String getDeviceIp() {
		return deviceIp;
	}
	public void setDeviceIp(String deviceIp) {
		this.deviceIp = deviceIp;
	}
	
	public String getPort() {
		return port;
	}
	
	public void setPort(String port) {
		this.port = port;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
}
