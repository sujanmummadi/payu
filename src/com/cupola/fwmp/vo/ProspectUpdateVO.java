package com.cupola.fwmp.vo;

import java.util.Date;
import java.util.List;

public class ProspectUpdateVO
{
	private Long ticketId; // for internal Use only

	private String title;
	private String firstName;
	private String lastName;
	private String middleName;
	private Date prefferedCallDate;

	private String customerType;

	private String mobileNumber;
	private String alternativeMobileNo;
	private String officeNumber;
	private String emailId;
	private String customerProfession;
	private String alternativeEmailId;

	private String currentAddress;
	private String communicationAdderss;
	private String permanentAddress;

	private String docPath;
	private String tariffId;
	private boolean cxPermission;
	private boolean gxPermission;

	private String connectionType;
	private String city;
	private String branch;
	private String area;
	private String clusterName;
	private String subAreaId;
	private String areaCode;

	private String fxName;
	private String fxIpAddress;
	private String fxMacAddress;
	private List<String> fxPorts;

	private String cxName;
	private String cxIpAddress;
	private String cxMacAddress;
	private List<String> cxPorts;

	private String commitedEtr;
	private String prospectNumber;

	private String remarks;
	private String source;

	private Date addedOn;

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public Date getPrefferedCallDate()
	{
		return prefferedCallDate;
	}

	public void setPrefferedCallDate(Date prefferedCallDate)
	{
		this.prefferedCallDate = prefferedCallDate;
	}

	public String getCustomerType()
	{
		return customerType;
	}

	public void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String getAlternativeMobileNo()
	{
		return alternativeMobileNo;
	}

	public void setAlternativeMobileNo(String alternativeMobileNo)
	{
		this.alternativeMobileNo = alternativeMobileNo;
	}

	public String getOfficeNumber()
	{
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber)
	{
		this.officeNumber = officeNumber;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	public String getCustomerProfession()
	{
		return customerProfession;
	}

	public void setCustomerProfession(String customerProfession)
	{
		this.customerProfession = customerProfession;
	}

	public String getAlternativeEmailId()
	{
		return alternativeEmailId;
	}

	public void setAlternativeEmailId(String alternativeEmailId)
	{
		this.alternativeEmailId = alternativeEmailId;
	}

	public String getCurrentAddress()
	{
		return currentAddress;
	}

	public void setCurrentAddress(String currentAddress)
	{
		this.currentAddress = currentAddress;
	}

	public String getCommunicationAdderss()
	{
		return communicationAdderss;
	}

	public void setCommunicationAdderss(String communicationAdderss)
	{
		this.communicationAdderss = communicationAdderss;
	}

	public String getPermanentAddress()
	{
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress)
	{
		this.permanentAddress = permanentAddress;
	}

	public String getDocPath()
	{
		return docPath;
	}

	public void setDocPath(String docPath)
	{
		this.docPath = docPath;
	}

	public String getTariffId()
	{
		return tariffId;
	}

	public void setTariffId(String tariffId)
	{
		this.tariffId = tariffId;
	}

	public boolean isCxPermission()
	{
		return cxPermission;
	}

	public void setCxPermission(boolean cxPermission)
	{
		this.cxPermission = cxPermission;
	}

	public boolean isGxPermission()
	{
		return gxPermission;
	}

	public void setGxPermission(boolean gxPermission)
	{
		this.gxPermission = gxPermission;
	}

	public String getConnectionType()
	{
		return connectionType;
	}

	public void setConnectionType(String connectionType)
	{
		this.connectionType = connectionType;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getBranch()
	{
		return branch;
	}

	public void setBranch(String branch)
	{
		this.branch = branch;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public String getClusterName()
	{
		return clusterName;
	}

	public void setClusterName(String clusterName)
	{
		this.clusterName = clusterName;
	}

	public String getSubAreaId()
	{
		return subAreaId;
	}

	public void setSubAreaId(String subAreaId)
	{
		this.subAreaId = subAreaId;
	}

	public String getAreaCode()
	{
		return areaCode;
	}

	public void setAreaCode(String areaCode)
	{
		this.areaCode = areaCode;
	}

	public String getFxName()
	{
		return fxName;
	}

	public void setFxName(String fxName)
	{
		this.fxName = fxName;
	}

	public String getFxIpAddress()
	{
		return fxIpAddress;
	}

	public void setFxIpAddress(String fxIpAddress)
	{
		this.fxIpAddress = fxIpAddress;
	}

	public String getFxMacAddress()
	{
		return fxMacAddress;
	}

	public void setFxMacAddress(String fxMacAddress)
	{
		this.fxMacAddress = fxMacAddress;
	}

	public List<String> getFxPorts()
	{
		return fxPorts;
	}

	public void setFxPorts(List<String> fxPorts)
	{
		this.fxPorts = fxPorts;
	}

	public String getCxName()
	{
		return cxName;
	}

	public void setCxName(String cxName)
	{
		this.cxName = cxName;
	}

	public String getCxIpAddress()
	{
		return cxIpAddress;
	}

	public void setCxIpAddress(String cxIpAddress)
	{
		this.cxIpAddress = cxIpAddress;
	}

	public String getCxMacAddress()
	{
		return cxMacAddress;
	}

	public void setCxMacAddress(String cxMacAddress)
	{
		this.cxMacAddress = cxMacAddress;
	}

	public List<String> getCxPorts()
	{
		return cxPorts;
	}

	public void setCxPorts(List<String> cxPorts)
	{
		this.cxPorts = cxPorts;
	}

	public String getCommitedEtr()
	{
		return commitedEtr;
	}

	public void setCommitedEtr(String commitedEtr)
	{
		this.commitedEtr = commitedEtr;
	}

	public String getProspectNumber()
	{
		return prospectNumber;
	}

	public void setProspectNumber(String prospectNumber)
	{
		this.prospectNumber = prospectNumber;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	@Override
	public String toString()
	{
		return "ProspectCoreUpdateVO [ticketId=" + ticketId + ", title="
				+ title + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", middleName=" + middleName + ", prefferedCallDate="
				+ prefferedCallDate + ", customerType=" + customerType
				+ ", mobileNumber=" + mobileNumber + ", alternativeMobileNo="
				+ alternativeMobileNo + ", officeNumber=" + officeNumber
				+ ", emailId=" + emailId + ", customerProfession="
				+ customerProfession + ", alternativeEmailId="
				+ alternativeEmailId + ", currentAddress=" + currentAddress
				+ ", communicationAdderss=" + communicationAdderss
				+ ", permanentAddress=" + permanentAddress + ", docPath="
				+ docPath + ", tariffId=" + tariffId + ", cxPermission="
				+ cxPermission + ", gxPermission=" + gxPermission
				+ ", connectionType=" + connectionType + ", city=" + city
				+ ", branch=" + branch + ", area=" + area + ", clusterName="
				+ clusterName + ", subAreaId=" + subAreaId + ", areaCode="
				+ areaCode + ", fxName=" + fxName + ", fxIpAddress="
				+ fxIpAddress + ", fxMacAddress=" + fxMacAddress + ", fxPorts="
				+ fxPorts + ", cxName=" + cxName + ", cxIpAddress="
				+ cxIpAddress + ", cxMacAddress=" + cxMacAddress + ", cxPorts="
				+ cxPorts + ", commitedEtr=" + commitedEtr
				+ ", prospectNumber=" + prospectNumber + ", remarks=" + remarks
				+ ", source=" + source + ", addedOn=" + addedOn + "]";
	}
	
}
