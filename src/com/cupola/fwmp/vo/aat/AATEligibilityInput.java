package com.cupola.fwmp.vo.aat;

/**
 * @author aditya
 *
 */

public class AATEligibilityInput
{
	private String mqId;
	private String connectionType;
	private String cityCode;

	public String getMqId()
	{
		return mqId;
	}

	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}

	public String getConnectionType()
	{
		return connectionType;
	}

	public void setConnectionType(String connectionType)
	{
		this.connectionType = connectionType;
	}

	public String getCityCode()
	{
		return cityCode;
	}

	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}

	@Override
	public String toString()
	{
		return "AATEligibilityInput [mqId=" + mqId + ", connectionType="
				+ connectionType + ", cityCode=" + cityCode + "]";
	}

}
