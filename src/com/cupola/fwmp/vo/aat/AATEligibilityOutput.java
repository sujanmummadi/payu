/**
 * 
 */
package com.cupola.fwmp.vo.aat;

/**
 * @author aditya
 *
 */
public class AATEligibilityOutput
{
	boolean status;
	String message;
	int code;

	public boolean isStatus()
	{
		return status;
	}

	public void setStatus(boolean status)
	{
		this.status = status;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	@Override
	public String toString()
	{
		return "AATEligibilityOutput [status=" + status + ", message=" + message
				+ ", code=" + code + "]";
	}

}
