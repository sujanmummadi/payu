package com.cupola.fwmp.vo;

import java.util.Date;

import com.cupola.fwmp.persistance.entities.WorkOrder;

public class TicketMaterialMappingVO
{
	
	private Long workOrderId;
	private Integer materialId;
	private Long ticketId;
	
	private String totalConsumption;
	private String macid;
	private String serialNo;
	private String startPoint;
	
	private String endPoint;
	private String uoMid;
	private String drumNo;
	private String fiberType;
	
	public Long getWorkOrderId()
	{
		return workOrderId;
	}
	public void setWorkOrderId(Long workOrderId)
	{
		this.workOrderId = workOrderId;
	}
	public Integer getMaterialId()
	{
		return materialId;
	}
	public void setMaterialId(Integer materialId)
	{
		this.materialId = materialId;
	}
	public Long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}
	public String getTotalConsumption()
	{
		return totalConsumption;
	}
	public void setTotalConsumption(String totalConsumption)
	{
		this.totalConsumption = totalConsumption;
	}
	public String getMacid()
	{
		return macid;
	}
	public void setMacid(String macid)
	{
		this.macid = macid;
	}
	public String getSerialNo()
	{
		return serialNo;
	}
	public void setSerialNo(String serialNo)
	{
		this.serialNo = serialNo;
	}
	public String getStartPoint()
	{
		return startPoint;
	}
	public void setStartPoint(String startPoint)
	{
		this.startPoint = startPoint;
	}
	public String getEndPoint()
	{
		return endPoint;
	}
	public void setEndPoint(String endPoint)
	{
		this.endPoint = endPoint;
	}
	public String getUoMid()
	{
		return uoMid;
	}
	public void setUoMid(String uoMid)
	{
		this.uoMid = uoMid;
	}
	public String getDrumNo()
	{
		return drumNo;
	}
	public void setDrumNo(String drumNo)
	{
		this.drumNo = drumNo;
	}
	public String getFiberType()
	{
		return fiberType;
	}
	public void setFiberType(String fiberType)
	{
		this.fiberType = fiberType;
	}
	public Long getAddedBy()
	{
		return addedBy;
	}
	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}
	public Date getAddedOn()
	{
		return addedOn;
	}
	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}
	public Long getModifiedBy()
	{
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn()
	{
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}
	public String getActivityId()
	{
		return activityId;
	}
	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}
	public String getActivityStatus()
	{
		return activityStatus;
	}
	public void setActivityStatus(String activityStatus)
	{
		this.activityStatus = activityStatus;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	
	private String activityId;
	private String activityStatus;
	private String status;

	@Override
	public String toString()
	{
		return "TicketMaterialMappingVO [workOrderId=" + workOrderId
				+ ", materialId=" + materialId + ", ticketId=" + ticketId
				+ ", totalConsumption=" + totalConsumption + ", macid=" + macid
				+ ", serialNo=" + serialNo + ", startPoint=" + startPoint
				+ ", endPoint=" + endPoint + ", uoMid=" + uoMid + ", drumNo="
				+ drumNo + ", fiberType=" + fiberType + ", addedBy=" + addedBy
				+ ", addedOn=" + addedOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + ", activityId=" + activityId
				+ ", activityStatus=" + activityStatus + ", status=" + status
				+ "]";
	}
	
		
}
