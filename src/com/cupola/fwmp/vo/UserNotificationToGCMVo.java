package com.cupola.fwmp.vo;

public class UserNotificationToGCMVo
{
	private Long ticketId;
	private int statusId;
	private String message;
	private String remarks;

	private int reasonCode;
	private String reasonValue;

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public int getStatusId()
	{
		return statusId;
	}

	public void setStatusId(int statusId)
	{
		this.statusId = statusId;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public int getReasonCode()
	{
		return reasonCode;
	}

	public void setReasonCode(int reasonCode)
	{
		this.reasonCode = reasonCode;
	}

	public String getReasonValue()
	{
		return reasonValue;
	}

	public void setReasonValue(String reasonValue)
	{
		this.reasonValue = reasonValue;
	}

	@Override
	public String toString()
	{
		return "UserNotificationToGCMVo [ticketId=" + ticketId + ", statusId="
				+ statusId + ", message=" + message + ", remarks=" + remarks
				+ ", reasonCode=" + reasonCode + ", reasonValue=" + reasonValue
				+ "]";
	}

}
