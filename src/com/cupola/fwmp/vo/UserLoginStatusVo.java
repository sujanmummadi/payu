package com.cupola.fwmp.vo;

import java.util.Date;


public class UserLoginStatusVo
{
 
	private long id;
	private long userId;
	private Date loginTime;
	private Date logoutTime;
	private Integer status;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public Date getLoginTime()
	{
		return loginTime;
	}

	public void setLoginTime(Date loginTime)
	{
		this.loginTime = loginTime;
	}

	public Date getLogoutTime()
	{
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime)
	{
		this.logoutTime = logoutTime;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "UserLoginStatusVo [id=" + id + ", userId=" + userId
				+ ", loginTime=" + loginTime + ", logoutTime=" + logoutTime
				+ ", status=" + status + "]";
	}

}
