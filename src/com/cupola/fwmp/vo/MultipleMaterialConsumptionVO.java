package com.cupola.fwmp.vo;

/**
 * 
 * @author Mahesh Chouhan
 * 
 * */

import java.util.List;

public class MultipleMaterialConsumptionVO {
Long ticketId;
List<TicketMaterialMappingVO> ticketMaterialMappingList;
public Long getTicketId() {
	return ticketId;
}
public void setTicketId(Long ticketId) {
	this.ticketId = ticketId;
}
public List<TicketMaterialMappingVO> getTicketMaterialMappingList() {
	return ticketMaterialMappingList;
}
public void setTicketMaterialMappingList(List<TicketMaterialMappingVO> ticketMaterialMappingList) {
	this.ticketMaterialMappingList = ticketMaterialMappingList;
}

}
