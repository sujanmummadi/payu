package com.cupola.fwmp.vo;

public class SkillCountVOForWebUI {
	
	private long skillId;
	private String skillName;
	private int count1;
	
	private int count2;
	
	private int count3;
	
	private int count4;

	public long getSkillId() {
		return skillId;
	}

	public void setSkillId(long skillId) {
		this.skillId = skillId;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public int getCount1() {
		return count1;
	}

	public void setCount1(int count1) {
		this.count1 = count1;
	}

	public int getCount2() {
		return count2;
	}

	public void setCount2(int count2) {
		this.count2 = count2;
	}

	public int getCount3() {
		return count3;
	}

	public void setCount3(int count3) {
		this.count3 = count3;
	}

	public int getCount4() {
		return count4;
	}

	public void setCount4(int count4) {
		this.count4 = count4;
	}

	@Override
	public String toString() {
		return "SkillCountVOForWebUI [skillId=" + skillId + ", skillName="
				+ skillName + ", count1=" + count1 + ", count2=" + count2
				+ ", count3=" + count3 + ", count4=" + count4 + "]";
	}
	
	
	

}
