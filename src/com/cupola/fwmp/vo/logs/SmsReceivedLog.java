/**
 * 
 */
package com.cupola.fwmp.vo.logs;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.WorkOrder;

/**
 * @author aditya
 *
 */
@Document(collection = "SmsReceivedLog")
public class SmsReceivedLog
{
	@Id
	private Long smsGatewayLogId;
	private String mobileNumber;
	private String message;
	private String requestTime;

	private String ticketId;
	private String workOrderId;

	private String requestSource;
	private String prospectNo;
	private String mqRequestTime;
	private String mqResponseTime;
	private String mqResponse;
	
	public Long getSmsGatewayLogId()
	{
		return smsGatewayLogId;
	}
	public void setSmsGatewayLogId(Long smsGatewayLogId)
	{
		this.smsGatewayLogId = smsGatewayLogId;
	}
	public String getMobileNumber()
	{
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getRequestTime()
	{
		return requestTime;
	}
	public void setRequestTime(String requestTime)
	{
		this.requestTime = requestTime;
	}
	public String getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}
	public String getWorkOrderId()
	{
		return workOrderId;
	}
	public void setWorkOrderId(String workOrderId)
	{
		this.workOrderId = workOrderId;
	}
	public String getRequestSource()
	{
		return requestSource;
	}
	public void setRequestSource(String requestSource)
	{
		this.requestSource = requestSource;
	}
	public String getProspectNo()
	{
		return prospectNo;
	}
	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}
	public String getMqRequestTime()
	{
		return mqRequestTime;
	}
	public void setMqRequestTime(String mqRequestTime)
	{
		this.mqRequestTime = mqRequestTime;
	}
	public String getMqResponseTime()
	{
		return mqResponseTime;
	}
	public void setMqResponseTime(String mqResponseTime)
	{
		this.mqResponseTime = mqResponseTime;
	}
	public String getMqResponse()
	{
		return mqResponse;
	}
	public void setMqResponse(String mqResponse)
	{
		this.mqResponse = mqResponse;
	}
	@Override
	public String toString()
	{
		return "SmsReceivedLog [smsGatewayLogId=" + smsGatewayLogId
				+ ", mobileNumber=" + mobileNumber + ", message=" + message
				+ ", requestTime=" + requestTime + ", ticketId=" + ticketId
				+ ", workOrderId=" + workOrderId + ", requestSource="
				+ requestSource + ", prospectNo=" + prospectNo
				+ ", mqRequestTime=" + mqRequestTime + ", mqResponseTime="
				+ mqResponseTime + ", mqResponse=" + mqResponse + "]";
	}

}
