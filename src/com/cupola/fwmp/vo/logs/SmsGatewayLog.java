/**
 * 
 */
package com.cupola.fwmp.vo.logs;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author aditya
 *
 */
@Document(collection = "SmsGatewayLog")
public class SmsGatewayLog
{
	@Id
	private Long smsGatewayLogId;
	private String mobileNumber;
	private String email;
	private String subject;
	private String message;
	private String ticketId;
	private String activityId;
	private String requestTime;
	private String responseTime;
	private String url;
	private String request;
	private String response;
	
	
	public Long getSmsGatewayLogId()
	{
		return smsGatewayLogId;
	}

	public void setSmsGatewayLogId(Long smsGatewayLogId)
	{
		this.smsGatewayLogId = smsGatewayLogId;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getActivityId()
	{
		return activityId;
	}

	public void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}

	public String getRequestTime()
	{
		return requestTime;
	}

	public void setRequestTime(String requestTime)
	{
		this.requestTime = requestTime;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getRequest()
	{
		return request;
	}

	public void setRequest(String request)
	{
		this.request = request;
	}

	public String getResponse()
	{
		return response;
	}

	public void setResponse(String response)
	{
		this.response = response;
	}

	public String getResponseTime()
	{
		return responseTime;
	}

	public void setResponseTime(String responseTime)
	{
		this.responseTime = responseTime;
	}

	@Override
	public String toString()
	{
		return "SmsGatewayLog [smsGatewayLogId=" + smsGatewayLogId
				+ ", mobileNumber=" + mobileNumber + ", email=" + email
				+ ", subject=" + subject + ", message=" + message
				+ ", ticketId=" + ticketId + ", activityId=" + activityId
				+ ", requestTime=" + requestTime + ", responseTime="
				+ responseTime + ", url=" + url + ", request=" + request
				+ ", response=" + response + "]";
	}

}
