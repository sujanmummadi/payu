package com.cupola.fwmp.vo;

import java.util.Date;

public class BranchVO
{

	private long id;
	private Long cityId;
	private String cityName;
	private String cityCode;
	private String branchName;
	private String branchCode;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;
	private String remarks;
	private String flowSwitch;
	
	public String getFlowSwitch()
	{
		return flowSwitch;
	}
	public void setFlowSwitch(String flowSwitch)
	{
		this.flowSwitch = flowSwitch;
	}
	public String getRemarks()
	{
		return remarks;
	}
	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public String getCityName()
	{
		return cityName;
	}
	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}
	public String getBranchName()
	{
		return branchName;
	}
	public void setBranchName(String branchName)
	{
		this.branchName = branchName;
	}
	public Long getAddedBy()
	{
		return addedBy;
	}
	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}
	public Date getAddedOn()
	{
		return addedOn;
	}
	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}
	public Long getModifiedBy()
	{
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn()
	{
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}
	public Integer getStatus()
	{
		return status;
	}
	public void setStatus(Integer status)
	{
		this.status = status;
	}
	@Override
	public String toString()
	{
		return "BranchVO [id=" + id + ", cityId=" + cityId + ", cityName="
				+ cityName + ", cityCode=" + cityCode + ", branchName="
				+ branchName + ", branchCode=" + branchCode + "]";
	}
	public String getCityCode()
	{
		return cityCode;
	}
	public void setCityCode(String cityCode)
	{
		this.cityCode = cityCode;
	}
	public String getBranchCode()
	{
		return branchCode;
	}
	public void setBranchCode(String branchCode)
	{
		this.branchCode = branchCode;
	}
	public Long getCityId()
	{
		return cityId;
	}
	public void setCityId(Long cityId)
	{
		this.cityId = cityId;
	}
}
