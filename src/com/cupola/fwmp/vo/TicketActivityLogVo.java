package com.cupola.fwmp.vo;

import java.util.Date;

public class TicketActivityLogVo
{

	private Long id;
	private Long activityId;
	private Long subActivityId;
	private Long ticketId;
	private String workOrderNumber;
	private Long currentProgress;
	private Long workOrderTypeId;
	private Long vendorId;
	private String vendorDetails;
	private Integer count;
	private String value;
	private Long assignedTo;
	private Date addedOn;
	private Long addedBy;
	private Date modifiedOn;
	private Integer status;
	private Long modifiedBy;
	private String remarks;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getActivityId()
	{
		return activityId;
	}

	public void setActivityId(Long activityId)
	{
		this.activityId = activityId;
	}

	public Long getSubActivityId()
	{
		return subActivityId;
	}

	public void setSubActivityId(Long subActivityId)
	{
		this.subActivityId = subActivityId;
	}

	public String getWorkOrderNumber()
	{
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber)
	{
		this.workOrderNumber = workOrderNumber;
	}

	public Long getWorkOrderTypeId()
	{
		return workOrderTypeId;
	}

	public void setWorkOrderTypeId(Long workOrderTypeId)
	{
		this.workOrderTypeId = workOrderTypeId;
	}

	public Long getVendorId()
	{
		return vendorId;
	}

	public void setVendorId(Long vendorId)
	{
		this.vendorId = vendorId;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getVendorDetails()
	{
		return vendorDetails;
	}

	public void setVendorDetails(String vendorDetails)
	{
		this.vendorDetails = vendorDetails;
	}

	public Integer getCount()
	{
		return count;
	}

	public void setCount(Integer count)
	{
		this.count = count;
	}

	public String getValue()
	{
		return value;
	}

	public Long getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(Long assignedTo)
	{
		this.assignedTo = assignedTo;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public Long getCurrentProgress()
	{
		return currentProgress;
	}

	public void setCurrentProgress(Long currentProgress)
	{
		this.currentProgress = currentProgress;
	}

	@Override
	public String toString()
	{
		return "TicketActivityLogVo [id=" + id + ", activityId=" + activityId
				+ ", subActivityId=" + subActivityId + ", ticketId=" + ticketId
				+ ", workOrderNumber=" + workOrderNumber + ", currentProgress="
				+ currentProgress + ", workOrderTypeId=" + workOrderTypeId
				+ ", vendorId=" + vendorId + ", vendorDetails=" + vendorDetails
				+ ", count=" + count + ", value=" + value + ", assignedTo="
				+ assignedTo + ", addedOn=" + addedOn + ", addedBy=" + addedBy
				+ ", modifiedOn=" + modifiedOn + ", status=" + status
				+ ", modifiedBy=" + modifiedBy + ", remarks=" + remarks + "]";
	}

}
