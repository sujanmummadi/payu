package com.cupola.fwmp.vo;

import java.util.Date;
import java.util.Map;

public class CxRackFixingActivityVO {

	private String workOrderNo;

	private Map<String, String> subActivityDetail;

	private String vendorId;

	private Date date;

	private String activityId;

	public String getWorkOrderNo() {
		return workOrderNo;
	}

	public void setWorkOrderNo(String workOrderNo) {
		this.workOrderNo = workOrderNo;
	}

	public Map<String, String> getSubActivityDetail() {
		return subActivityDetail;
	}

	public void setSubActivityDetail(Map<String, String> subActivityDetail) {
		this.subActivityDetail = subActivityDetail;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	@Override
	public String toString() {
		return "CxRackFixingActivityVO [workOrderNo=" + workOrderNo
				+ ", subActivityDetail=" + subActivityDetail + ", vendorId="
				+ vendorId + ", date=" + date + ", activityId=" + activityId
				+ "]";
	}

}
