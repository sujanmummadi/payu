package com.cupola.fwmp.vo;

public class MessageVO
{
	private int messageType;
	
	private String message;

	public int getMessageType()
	{
		return messageType;
	}

	public void setMessageType(int messageType)
	{
		this.messageType = messageType;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}


	@Override
	public String toString()
	{
		return "MessageVO [messageType=" + messageType + ", message=" + message
				+ "]";
	}

}
