package com.cupola.fwmp.vo;

public class RollBackVO {
	
	private String ticketId;
	private String activityId;
	
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	@Override
	public String toString() {
		return "ConfirmRollBackVO [ticketId=" + ticketId + ", activityId="
				+ activityId + "]";
	}
	
	
	
	
}
