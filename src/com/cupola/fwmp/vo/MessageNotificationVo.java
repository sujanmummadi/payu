package com.cupola.fwmp.vo;

public class MessageNotificationVo
{

	private String mobileNumber;
	private String email;
	private String subject;
	private String message;
	private String ticketId;
	private String activityId;

	 
	@Override
	public String toString()
	{
		return "MessageNotificationVo [mobileNumber=" + mobileNumber
				+ ", email=" + email + ", subject=" + subject + ", message="
				+ message + ", ticketId=" + ticketId + ", activityId="
				+ activityId + "]";
	}


	public  String getMobileNumber()
	{
		return mobileNumber;
	}


	public  void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}


	public  String getEmail()
	{
		return email;
	}


	public  void setEmail(String email)
	{
		this.email = email;
	}


	public  String getSubject()
	{
		return subject;
	}


	public  void setSubject(String subject)
	{
		this.subject = subject;
	}


	public  String getMessage()
	{
		return message;
	}


	public  void setMessage(String message)
	{
		this.message = message;
	}


	public  String getTicketId()
	{
		return ticketId;
	}


	public  void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}


	public  String getActivityId()
	{
		return activityId;
	}


	public  void setActivityId(String activityId)
	{
		this.activityId = activityId;
	}
	
}
