package com.cupola.fwmp.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.Skill;
import com.cupola.fwmp.persistance.entities.Vendor;
import com.cupola.fwmp.persistance.entities.VendorDetails;


public class VendorVO implements Serializable{

	private long id;
	private long vendorId;
	private String name;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private int status;
	private String branchName;
	private String cityName;
	private String areaName;
	private long branchId;
	private long cityId;
	private long areaId;
	private String skillName;
	private long skillId;
	private int count;
	private Long userId;
	private Date date;
	private String enable;
	private Vendor vendor;
	private int roleid;
	private long neUserId;
	private Map<String, String> vendorSkillIdAndCountMap;
	private boolean firstEntry;

	public VendorVO() {}
	
	public int getCount() {
		return count;
	}

	public long getVendorId() {
		return vendorId;
	}
    public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}


	public void setCount(int count) {
		this.count = count;
	}

	public long getId() {
		return id;
	}

	public long getSkillId() {
		return skillId;
	}

	public void setSkillId(long skillId) {
		this.skillId = skillId;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	public String getBranchName() {
		return branchName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Map<String, String> getVendorSkillIdAndCountMap() {
		return vendorSkillIdAndCountMap;
	}

	public void setVendorSkillIdAndCountMap(
			Map<String, String> vendorSkillIdAndCountMap) {
		this.vendorSkillIdAndCountMap = vendorSkillIdAndCountMap;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	
	
	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}
  public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	
	
	
	

	public boolean isFirstEntry() {
		return firstEntry;
	}

	public void setFirstEntry(boolean firstEntry) {
		this.firstEntry = firstEntry;
	}

	

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}
   
	
	
	
	
	public long getNeUserId() {
		return neUserId;
	}

	public void setNeUserId(long neUserId) {
		this.neUserId = neUserId;
	}

	@Override
	public String toString() {
		return "VendorVO [id=" + id + ", vendorId=" + vendorId + ", name="
				+ name + ", addedBy=" + addedBy + ", addedOn=" + addedOn
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn
				+ ", status=" + status + ", branchName=" + branchName
				+ ", cityName=" + cityName + ", areaName=" + areaName
				+ ", branchId=" + branchId + ", cityId=" + cityId + ", areaId="
				+ areaId + ", skillName=" + skillName + ", skillId=" + skillId
				+ ", count=" + count + ", userId=" + userId + ", date=" + date
				+ ", enable=" + enable + ", vendor=" + vendor + ", roleid="
				+ roleid + ", neUserId=" + neUserId
				+ ", vendorSkillIdAndCountMap=" + vendorSkillIdAndCountMap
				+ ", firstEntry=" + firstEntry + "]";
	}

	
	
}
