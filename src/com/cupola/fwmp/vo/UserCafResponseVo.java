package com.cupola.fwmp.vo;

import java.util.Date;

import com.cupola.fwmp.persistance.entities.User;

public class UserCafResponseVo {

	private long id;
	private Long userId;
	private String range;
	private Date addedOn;
	private Long addedBy;
	private Date modifiedOn;
	private Long modifiedBy;
	private Integer status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public Long getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "UserCafResponseVo [id=" + id + ", userId=" + userId
				+ ", range=" + range + ", addedOn=" + addedOn + ", addedBy="
				+ addedBy + ", modifiedOn=" + modifiedOn + ", modifiedBy="
				+ modifiedBy + ", status=" + status + "]";
	}

}
