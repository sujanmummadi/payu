package com.cupola.fwmp.vo;

// Generated May 31, 2016 4:42:34 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

/**
 * Tablet generated by hbm2java
 */
public class TabletVO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private TypeAheadVo userByAssignedTo;
	private TypeAheadVo userByCurrentUser;
	private String currentUserName;
	private String assignedTo;
	private String tabName;
	private String macAddress;
	private String brandName;
	private String registrationId;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private int status;

	public TabletVO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TypeAheadVo getUserByAssignedTo() {
		return userByAssignedTo;
	}

	public void setUserByAssignedTo(TypeAheadVo userByAssignedTo) {
		this.userByAssignedTo = userByAssignedTo;
	}

	public TypeAheadVo getUserByCurrentUser() {
		return userByCurrentUser;
	}

	public void setUserByCurrentUser(TypeAheadVo userByCurrentUser) {
		this.userByCurrentUser = userByCurrentUser;
	}

	public String getCurrentUserName() {
		return currentUserName;
	}

	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}
	
	
	public String getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo)
	{
		this.assignedTo = assignedTo;
	}


	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public Long getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TabletVO [id=" + id + ", userByAssignedTo=" + userByAssignedTo
				+ ", userByCurrentUser=" + userByCurrentUser
				+ ", currentUserName=" + currentUserName + ", tabName="
				+ tabName + ", macAddress=" + macAddress + ", brandName="
				+ brandName + ", registrationId=" + registrationId
				+ ", status=" + status + "]";
	}

	public TabletVO(long id, TypeAheadVo userByAssignedTo,
			TypeAheadVo userByCurrentUser, String currentUserName,
			String tabName, String macAddress, String brandName,
			String registrationId, Long addedBy, Date addedOn, Long modifiedBy,
			Date modifiedOn, int status)
	{
		super();
		this.id = id;
		this.userByAssignedTo = userByAssignedTo;
		this.userByCurrentUser = userByCurrentUser;
		this.currentUserName = currentUserName;
		this.tabName = tabName;
		this.macAddress = macAddress;
		this.brandName = brandName;
		this.registrationId = registrationId;
		this.addedBy = addedBy;
		this.addedOn = addedOn;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.status = status;
	}

	public TabletVO(long id, String tabName, String macAddress,
			String brandName, String registrationId, Long addedBy,
			Date addedOn, Long modifiedBy, Date modifiedOn, int status)
	{
		super();
		this.id = id;
		this.tabName = tabName;
		this.macAddress = macAddress;
		this.brandName = brandName;
		this.registrationId = registrationId;
		this.addedBy = addedBy;
		this.addedOn = addedOn;
		this.modifiedBy = modifiedBy;
		this.modifiedOn = modifiedOn;
		this.status = status;
	}

	
}
