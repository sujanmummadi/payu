package com.cupola.fwmp.vo;

import java.io.Serializable;

/**
 * 
 * @author G Ashraf
 * 
 * */

public class TicketCountTlneVo implements Serializable
{
	private Long ticketId;
	private Integer ticketStatus;
	private Long subCategoryId;
	private Long currentAssignedTo;
	private String ticketCategory;
	private String ticketSubCategory;
	private Long priorityId;
	private Integer priorityValue;
	private Integer currentWorkStage;
	private Long workOrderTypeId;
	private String workStageName;
	private String workOrderType;
	
	public Long getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	public Integer getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(Integer ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	public Long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(Long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public Long getCurrentAssignedTo() {
		return currentAssignedTo;
	}
	public void setCurrentAssignedTo(Long currentAssignedTo) {
		this.currentAssignedTo = currentAssignedTo;
	}
	public String getTicketCategory() {
		return ticketCategory;
	}
	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}
	public String getTicketSubCategory() {
		return ticketSubCategory;
	}
	public void setTicketSubCategory(String ticketSubCategory) {
		this.ticketSubCategory = ticketSubCategory;
	}
	public Long getPriorityId() {
		return priorityId;
	}
	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}
	public Integer getPriorityValue() {
		return priorityValue;
	}
	public void setPriorityValue(Integer priorityValue) {
		this.priorityValue = priorityValue;
	}
	public Integer getCurrentWorkStage() {
		return currentWorkStage;
	}
	public void setCurrentWorkStage(Integer currentWorkStage) {
		this.currentWorkStage = currentWorkStage;
	}
	public Long getWorkOrderTypeId() {
		return workOrderTypeId;
	}
	public void setWorkOrderTypeId(Long workOrderTypeId) {
		this.workOrderTypeId = workOrderTypeId;
	}
	public String getWorkStageName() {
		return workStageName;
	}
	public void setWorkStageName(String workStageName) {
		this.workStageName = workStageName;
	}
	public String getWorkOrderType() {
		return workOrderType;
	}
	public void setWorkOrderType(String workOrderType) {
		this.workOrderType = workOrderType;
	}
}
