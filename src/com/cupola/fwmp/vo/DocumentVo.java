package com.cupola.fwmp.vo;


public class DocumentVo
{
	private String documentDisplayName;
	private String documentName;
	private String createdOn;
	
	public DocumentVo()
	{
		// TODO Auto-generated constructor stub
	}
	public String getDocumentDisplayName()
	{
		return documentDisplayName;
	}
	public void setDocumentDisplayName(String documentDisplayName)
	{
		this.documentDisplayName = documentDisplayName;
	}
	public String getDocumentName()
	{
		return documentName;
	}
	public void setDocumentName(String documentName)
	{
		this.documentName = documentName;
	}
	public String getCreatedOn()
	{
		return createdOn;
	}
	public void setCreatedOn(String createdOn)
	{
		this.createdOn = createdOn;
	}
	public DocumentVo(String documentDisplayName, String documentName,
			String createdOn)
	{
		super();
		this.documentDisplayName = documentDisplayName;
		this.documentName = documentName;
		this.createdOn = createdOn;
	}

}
