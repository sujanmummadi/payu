 /**
 * @Author snoor  Nov 29, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.vo;

import java.util.Date;

/**
 * @Author snoor  Nov 29, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class BranchFlowConfigVO {
	private Long id;
	private Long branchId;
	private Long cityId;
	private Long flowId;
	private Long modifiedBy;
	private Long addedBy;
	private Integer bufferTime;
	private Integer averageCloseTime;
	private Date addedOn;
	private Date modifiedOn;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the branchId
	 */
	public Long getBranchId() {
		return branchId;
	}
	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	/**
	 * @return the cityId
	 */
	public Long getCityId() {
		return cityId;
	}
	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	/**
	 * @return the flowId
	 */
	public Long getFlowId() {
		return flowId;
	}
	/**
	 * @param flowId the flowId to set
	 */
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	/**
	 * @return the modifiedBy
	 */
	public Long getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the addedBy
	 */
	public Long getAddedBy() {
		return addedBy;
	}
	/**
	 * @param addedBy the addedBy to set
	 */
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}
	/**
	 * @return the bufferTime
	 */
	public Integer getBufferTime() {
		return bufferTime;
	}
	/**
	 * @param bufferTime the bufferTime to set
	 */
	public void setBufferTime(Integer bufferTime) {
		this.bufferTime = bufferTime;
	}
	/**
	 * @return the averageCloseTime
	 */
	public Integer getAverageCloseTime() {
		return averageCloseTime;
	}
	/**
	 * @param averageCloseTime the averageCloseTime to set
	 */
	public void setAverageCloseTime(Integer averageCloseTime) {
		this.averageCloseTime = averageCloseTime;
	}
	/**
	 * @return the addedOn
	 */
	public Date getAddedOn() {
		return addedOn;
	}
	/**
	 * @param addedOn the addedOn to set
	 */
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
	/**
	 * @return the modifiedOn
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BranchFlowConfigVO [id=" + id + ", branchId=" + branchId + ", cityId=" + cityId + ", flowId=" + flowId
				+ ", modifiedBy=" + modifiedBy + ", addedBy=" + addedBy + ", bufferTime=" + bufferTime
				+ ", averageCloseTime=" + averageCloseTime + ", addedOn=" + addedOn + ", modifiedOn=" + modifiedOn
				+ "]";
	}
	
}
