package com.cupola.fwmp.vo;

public class UserAndRegIdDetails
{
	String registrationId;
	long mobileNo;
	String emailId;

	public UserAndRegIdDetails(){}

	@Override
	public String toString()
	{
		return "UserAndRegistationDetails [registrationId=" + registrationId
				+ ", mobileNo=" + mobileNo + ", emailId=" + emailId + "]";
	}

	public String getRegistrationId()
	{
		return registrationId;
	}

	public void setRegistrationId(String registrationId)
	{
		this.registrationId = registrationId;
	}

	public long getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(long mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	public UserAndRegIdDetails(String registrationId, long mobileNo,
			String emailId)
	{
		super();
		this.registrationId = registrationId;
		this.mobileNo = mobileNo;
		this.emailId = emailId;
	}
	
	

}
