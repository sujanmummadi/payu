package com.cupola.fwmp.vo;

public class GCMMessage
{
	long id;
	String apiKey;
	String registrationId;
	String message;
	String to;
	String from;
	long mobileNo;
	String emailId;

	public GCMMessage()
	{
	}


	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getApiKey()
	{
		return apiKey;
	}

	public void setApiKey(String apiKey)
	{
		this.apiKey = apiKey;
	}

	public String getRegistrationId()
	{
		return registrationId;
	}

	public void setRegistrationId(String registrationId)
	{
		this.registrationId = registrationId;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getTo()
	{
		return to;
	}

	public void setTo(String to)
	{
		this.to = to;
	}

	public String getFrom()
	{
		return from;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public long getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(long mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}

	@Override
	public String toString()
	{
		return "GCMMessage [id=" + id + ", apiKey=" + apiKey
				+ ", registrationId=" + registrationId + ", message=" + message
				+ ", to=" + to + ", from=" + from
				+ ", mobileNo=" + mobileNo + ", emailId=" + emailId + "]";
	}
}
