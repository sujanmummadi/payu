package com.cupola.fwmp.vo;

public class MapUserDeviceVO {

	private Long userId;
	private Long userGroupId;
	private Long userBranchId;
	private Long deviceId;
	private Long deviceBranchId;
	private int isActive;
	
	private String status;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}

	public Long getUserBranchId() {
		return userBranchId;
	}

	public void setUserBranchId(Long userBranchId) {
		this.userBranchId = userBranchId;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Long getDeviceBranchId() {
		return deviceBranchId;
	}

	public void setDeviceBranchId(Long deviceBranchId) {
		this.deviceBranchId = deviceBranchId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	
	

}
