package com.cupola.fwmp.vo;

public class UserNotificationVo
{
	private Long ticketId;
	private String status;

	private String remarks;

	private int reasonCode;
	private String reasonValue;

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	@Override
	public String toString()
	{
		return "UserNotificationVo [ticketId=" + ticketId + ", status="
				+ status + ", remarks=" + remarks + "]";
	}

	public int getReasonCode()
	{
		return reasonCode;
	}

	public void setReasonCode(int reasonCode)
	{
		this.reasonCode = reasonCode;
	}

	public String getReasonValue()
	{
		return reasonValue;
	}

	public void setReasonValue(String reasonValue)
	{
		this.reasonValue = reasonValue;
	}

}
