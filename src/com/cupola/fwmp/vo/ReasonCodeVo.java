package com.cupola.fwmp.vo;

import java.util.List;

public class ReasonCodeVo
{
	private List<TypeAheadVo> copperReasonCode;
	private List<TypeAheadVo> fiberReasonCode;
	private List<TypeAheadVo> nonFeasibleReasonCode;
	private List<TypeAheadVo> permissionDenialReasonCode;
	private List<TypeAheadVo> elementRacFixingDenialReasons;
	
	public List<TypeAheadVo> getCopperReasonCode()
	{
		return copperReasonCode;
	}

	public void setCopperReasonCode(List<TypeAheadVo> copperReasonCode)
	{
		this.copperReasonCode = copperReasonCode;
	}

	public List<TypeAheadVo> getFiberReasonCode()
	{
		return fiberReasonCode;
	}

	public void setFiberReasonCode(List<TypeAheadVo> fiberReasonCode)
	{
		this.fiberReasonCode = fiberReasonCode;
	}

	public List<TypeAheadVo> getNonFeasibleReasonCode()
	{
		return nonFeasibleReasonCode;
	}

	public void setNonFeasibleReasonCode(List<TypeAheadVo> nonFeasibleReasonCode)
	{
		this.nonFeasibleReasonCode = nonFeasibleReasonCode;
	}

	public List<TypeAheadVo> getPermissionDenialReasonCode()
	{
		return permissionDenialReasonCode;
	}

	public void setPermissionDenialReasonCode(
			List<TypeAheadVo> permissionDenialReasonCode)
	{
		this.permissionDenialReasonCode = permissionDenialReasonCode;
	}

	public List<TypeAheadVo> getElementRacFixingDenialReasons()
	{
		return elementRacFixingDenialReasons;
	}

	public void setElementRacFixingDenialReasons(
			List<TypeAheadVo> elementRacFixingDenialReasons)
	{
		this.elementRacFixingDenialReasons = elementRacFixingDenialReasons;
	}

}