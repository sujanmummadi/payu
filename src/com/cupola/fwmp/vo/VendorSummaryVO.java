package com.cupola.fwmp.vo;

import java.util.List;
import java.util.Map;

public class VendorSummaryVO {
  
	private long vendorId;
	private String vendorName;
	private int skillId;
	private int count;
	private String skillName;
	
	
	
	public long getVendorId() {
		return vendorId;
	}
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	public int getSkillId() {
		return skillId;
	}
	public void setSkillId(int skillId) {
		this.skillId = skillId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	@Override
	public String toString() {
		return "VendorSummaryVO [vendorId=" + vendorId + ", vendorName="
				+ vendorName + ", skillId=" + skillId + ", count=" + count
				+ ", skillName=" + skillName + "]";
	}
	
	
	
	
	
}
