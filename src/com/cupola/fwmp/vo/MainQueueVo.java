/**
 * 
 */
package com.cupola.fwmp.vo;

import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author aditya
 *
 */
public class MainQueueVo
{
	private Map<String, LinkedHashSet<Long>> mainQueue;
	private Map<String, LinkedHashSet<Long>> mainFrQueue;

	public Map<String, LinkedHashSet<Long>> getMainQueue()
	{
		return mainQueue;
	}

	public void setMainQueue(Map<String, LinkedHashSet<Long>> mainQueue)
	{
		this.mainQueue = mainQueue;
	}


	public Map<String, LinkedHashSet<Long>> getMainFrQueue() {
		return mainFrQueue;
	}

	public void setMainFrQueue(Map<String, LinkedHashSet<Long>> mainFrQueue) {
		this.mainFrQueue = mainFrQueue;
	}

	@Override
	public String toString()
	{
		return "MainQueueVo [mainQueue=" + mainQueue + ", mainFrQueue="
				+ mainFrQueue + "]";
	}
	
}
