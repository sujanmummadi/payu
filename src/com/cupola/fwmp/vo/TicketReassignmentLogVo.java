package com.cupola.fwmp.vo;

import java.util.Date;
import java.util.List;

/**
 * @Author aditya 25-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class TicketReassignmentLogVo {

	private long id;
	private Long ticketId;
	private Long assignedFrom;
	private Long assignedTo;
	private Long assignedBy;
	private String ticketCategory;
	private Date addedOn;

	public TicketReassignmentLogVo() {
	}

	public TicketReassignmentLogVo(long id) {
		this.id = id;
	}

	/**
	 * @param id
	 * @param ticketId
	 * @param ticketIds
	 * @param assignedFrom
	 * @param assignedTo
	 * @param assignedBy
	 * @param prospectNo
	 * @param addedOn
	 */
	public TicketReassignmentLogVo(long id, Long ticketId, List<Long> ticketIds, Long assignedFrom, Long assignedTo,
			Long assignedBy, String ticketCategory, Date addedOn) {
		super();
		this.id = id;
		this.ticketId = ticketId;
		this.assignedFrom = assignedFrom;
		this.assignedTo = assignedTo;
		this.assignedBy = assignedBy;
		this.ticketCategory = ticketCategory;
		this.addedOn = addedOn;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the ticketId
	 */
	public Long getTicketId() {
		return ticketId;
	}

	/**
	 * @param ticketId
	 *            the ticketId to set
	 */
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * @return the assignedFrom
	 */
	public Long getAssignedFrom() {
		return assignedFrom;
	}

	/**
	 * @param assignedFrom
	 *            the assignedFrom to set
	 */
	public void setAssignedFrom(Long assignedFrom) {
		this.assignedFrom = assignedFrom;
	}

	/**
	 * @return the assignedTo
	 */
	public Long getAssignedTo() {
		return assignedTo;
	}

	/**
	 * @param assignedTo
	 *            the assignedTo to set
	 */
	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}

	/**
	 * @return the assignedBy
	 */
	public Long getAssignedBy() {
		return assignedBy;
	}

	/**
	 * @param assignedBy
	 *            the assignedBy to set
	 */
	public void setAssignedBy(Long assignedBy) {
		this.assignedBy = assignedBy;
	}


	/**
	 * @return the addedOn
	 */
	public Date getAddedOn() {
		return addedOn;
	}

	/**
	 * @param addedOn
	 *            the addedOn to set
	 */
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	/**
	 * @return the ticketCategory
	 */
	public String getTicketCategory() {
		return ticketCategory;
	}

	/**
	 * @param ticketCategory the ticketCategory to set
	 */
	public void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TicketReassignmentLogVo [id=" + id + ", ticketId=" + ticketId + ", assignedFrom=" + assignedFrom
				+ ", assignedTo=" + assignedTo + ", assignedBy=" + assignedBy + ", ticketCategory=" + ticketCategory
				+ ", addedOn=" + addedOn + "]";
	}

}
