package com.cupola.fwmp.vo;


public class BranchDetailVo 
{
	private long id;
	private Long cityId;
	private String cityName;
	private String cityCode;
	private String branchName;
	private String branchCode;
	private String flowSwitch;
	private String branchManager;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getFlowSwitch() {
		return flowSwitch;
	}
	public void setFlowSwitch(String flowSwitch) {
		this.flowSwitch = flowSwitch;
	}
	public String getBranchManager() {
		return branchManager;
	}
	public void setBranchManager(String branchManager) {
		this.branchManager = branchManager;
	}
	
	
}
