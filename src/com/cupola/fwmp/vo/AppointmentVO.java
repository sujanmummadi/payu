package com.cupola.fwmp.vo;

import java.util.Date;

public class AppointmentVO {

	private String workOrderNo;
	private String ticketNo;
	private Date appointmentDate;
	
	public AppointmentVO(){
		
	}
	
	/**
	 * @param workOrderNo
	 * @param ticketNo
	 * @param appointmentDate
	 */
	public AppointmentVO(String workOrderNo, String ticketNo, Date appointmentDate) {
		super();
		this.workOrderNo = workOrderNo;
		this.ticketNo = ticketNo;
		this.appointmentDate = appointmentDate;
	}
	/**
	 * @return the workOrderNo
	 */
	public String getWorkOrderNo() {
		return workOrderNo;
	}
	/**
	 * @param workOrderNo the workOrderNo to set
	 */
	public void setWorkOrderNo(String workOrderNo) {
		this.workOrderNo = workOrderNo;
	}
	/**
	 * @return the ticketNo
	 */
	public String getTicketNo() {
		return ticketNo;
	}
	/**
	 * @param ticketNo the ticketNo to set
	 */
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	/**
	 * @return the appointmentDate
	 */
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	/**
	 * @param appointmentDate the appointmentDate to set
	 */
	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppointmentVO [workOrderNo=" + workOrderNo + ", ticketNo=" + ticketNo + ", appointmentDate="
				+ appointmentDate + "]";
	}
	
	
	
}
