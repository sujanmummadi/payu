package com.cupola.fwmp.vo;

import java.util.Date;

public class TicketEtrVo
{

	private long id;
	private Long ticketId;
	private Long userId;
	private Date currentETR;
	private Date newETR;
	private Integer approvalStatus;
	private String reasonCode;
	private String remarks;
	private Integer count;
	private Integer status;
	private Integer etrMode;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public Integer getApprovalStatus()
	{
		return approvalStatus;
	}

	public void setApprovalStatus(Integer approvalStatus)
	{
		this.approvalStatus = approvalStatus;
	}

	public String getReasonCode()
	{
		return reasonCode;
	}

	public void setReasonCode(String reasonCode)
	{
		this.reasonCode = reasonCode;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public Integer getCount()
	{
		return count;
	}

	public void setCount(Integer count)
	{
		this.count = count;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Date getCurrentETR()
	{
		return currentETR;
	}

	public void setCurrentETR(Date currentETR)
	{
		this.currentETR = currentETR;
	}

	public Date getNewETR()
	{
		return newETR;
	}

	public void setNewETR(Date newETR)
	{
		this.newETR = newETR;
	}

	public Integer getEtrMode()
	{
		return etrMode;
	}

	public void setEtrMode(Integer etrMode)
	{
		this.etrMode = etrMode;
	}

	@Override
	public String toString()
	{
		return "TicketEtrVo [id=" + id + ", ticketId=" + ticketId + ", userId="
				+ userId + ", currentETR=" + currentETR + ", newETR=" + newETR
				+ ", approvalStatus=" + approvalStatus + ", reasonCode="
				+ reasonCode + ", remarks=" + remarks + ", count=" + count
				+ ", status=" + status + "]";
	}

}