package com.cupola.fwmp.vo;

public class NotificationMessage
{
	long id;
	String from; 
	String to;
	long mobileNo;
	String emailId;
	String subject;
	String message;

	
	public NotificationMessage(){}
	
	
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public String getFrom()
	{
		return from;
	}
	public void setFrom(String from)
	{
		this.from = from;
	}
	public String getTo()
	{
		return to;
	}
	public void setTo(String to)
	{
		this.to = to;
	}
	public long getMobileNo()
	{
		return mobileNo;
	}
	public void setMobileNo(long mobileNo)
	{
		this.mobileNo = mobileNo;
	}
	public String getEmailId()
	{
		return emailId;
	}
	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	@Override
	public String toString()
	{
		return "NotificationMessage [id=" + id + ", from=" + from + ", to="
				+ to + ", mobileNo=" + mobileNo + ", emailId=" + emailId
				+ ", subject=" + subject + ", message=" + message + "]";
	}
}
