package com.cupola.fwmp.vo;

public class GCMNotificationMessage {

	private Long userId;// who sends notification
	private Long ticketId;
	private String message;
	private String currentEtr;
	private String updateEtr;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCurrentEtr() {
		return currentEtr;
	}

	public void setCurrentEtr(String currentEtr) {
		this.currentEtr = currentEtr;
	}

	public String getUpdateEtr() {
		return updateEtr;
	}

	public void setUpdateEtr(String updateEtr) {
		this.updateEtr = updateEtr;
	}

	@Override
	public String toString() {
		return "GCMNotificationMessage [userId=" + userId + ", ticketId="
				+ ticketId + ", message=" + message + ", currentEtr="
				+ currentEtr + ", updateEtr=" + updateEtr + "]";
	}

}
