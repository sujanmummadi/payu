package com.cupola.fwmp.vo;


import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class InputVendorVO implements Serializable {

	private String userId;
	private String cityId;
	private String branchId;
	private String areaId;
	private String roleId;
	private String vendorId;
	private String neUserId;
	private Map<String, String> vendorSkillIdAndCountMap;
	private boolean firstEntry;
	private Date date;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public Map<String, String> getVendorSkillIdAndCountMap() {
		return vendorSkillIdAndCountMap;
	}

	public void setVendorSkillIdAndCountMap(
			Map<String, String> vendorSkillIdAndCountMap) {
		this.vendorSkillIdAndCountMap = vendorSkillIdAndCountMap;
	}

	public boolean isFirstEntry() {
		return firstEntry;
	}

	public void setFirstEntry(boolean firstEntry) {
		this.firstEntry = firstEntry;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	
	
	public String getNeUserId() {
		return neUserId;
	}

	public void setNeUserId(String neUserId) {
		this.neUserId = neUserId;
	}

	@Override
	public String toString() {
		return "InputVendorVO [userId=" + userId + ", vendorId=" + vendorId
				+ ", neUserId=" + neUserId + ", vendorSkillIdAndCountMap="
				+ vendorSkillIdAndCountMap + "]";
	}

	

	
}
