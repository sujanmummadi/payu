package com.cupola.fwmp.vo;

public class UserCafVo {

	private Long userId;
	private String cafDetails;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCafDetails() {
		return cafDetails;
	}

	public void setCafDetails(String cafDetails) {
		this.cafDetails = cafDetails;
	}

	@Override
	public String toString() {
		return "UserCAFVo [userId=" + userId + ", cafDetails=" + cafDetails
				+ "]";
	}

}
