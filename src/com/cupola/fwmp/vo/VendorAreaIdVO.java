package com.cupola.fwmp.vo;

public class VendorAreaIdVO {

	
	private String areaId;
	private String userId;
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "VendorAreaIdVO [areaId=" + areaId + ", userId=" + userId + "]";
	}
	
	
	
}
