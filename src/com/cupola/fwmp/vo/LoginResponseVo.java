package com.cupola.fwmp.vo;

import java.util.List;

public class LoginResponseVo
{

	private Long userId;

	private TypeAheadVo cityId;

	private String displayName;

	private List<String> roles;

	private List<TypeAheadVo> branches;

	private List<TypeAheadVo> userGroups;

	private Integer status;

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public List<String> getRoles()
	{
		return roles;
	}

	public void setRoles(List<String> roles)
	{
		this.roles = roles;
	}

	public List<TypeAheadVo> getBranches()
	{
		return branches;
	}

	public void setBranches(List<TypeAheadVo> branches)
	{
		this.branches = branches;
	}

	public List<TypeAheadVo> getUserGroups()
	{
		return userGroups;
	}

	public void setUserGroups(List<TypeAheadVo> userGroups)
	{
		this.userGroups = userGroups;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public TypeAheadVo getCityId()
	{
		return cityId;
	}

	public void setCityId(TypeAheadVo cityId)
	{
		this.cityId = cityId;
	}

}
