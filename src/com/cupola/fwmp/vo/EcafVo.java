/**
* @Author kiran  Sep 1, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.vo;

import java.util.List;

import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.mongo.documents.CustomerDocument;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.vo.sales.TariffPlanVo;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.PaymentVO;

/**
 * @Author kiran Sep 1, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class EcafVo {

	private String cafNo;
	private String createdDate;
	private String place;
	private String dob;
	private String customerPhoto;
	private String customerName;
	private String customerSign;
	private KycDetails adhaar;
	private String customerProfession;
	private String state;
	private String permanentAddress;
	private String communicationAdderss;
	private String currentAddress;
	private String mobileNumber;
	private String officeNumber;
	private String pincode;
	private TariffPlanVo tariffPlanVo;
	private List<CustomerDocument> documents;
	private String emailId;
	private String cxPermission;
	private String externalRouterRequired;
	private PaymentVO paymentVO;
	private boolean isCustomerViaAadhaar;
	private String primaryTransactionId;
	private String secoundryTransactionId;
	private String primaryDate;
	private String secondaryDate;
	private String alternativeMobileNo;
	private GISPojo gisPojo;
	private CustomerAddressVO current;
	private CustomerAddressVO communication;
	private CustomerAddressVO perminent;

	public String getCafNo() {
		return cafNo;
	}

	public void setCafNo(String cafNo) {
		this.cafNo = cafNo;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getCustomerPhoto() {
		return customerPhoto;
	}

	public void setCustomerPhoto(String customerPhoto) {
		this.customerPhoto = customerPhoto;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the customerSign
	 */
	public String getCustomerSign() {
		return customerSign;
	}

	/**
	 * @param customerSign
	 *            the customerSign to set
	 */
	public void setCustomerSign(String customerSign) {
		this.customerSign = customerSign;
	}

	/**
	 * @return the adhaar
	 */
	public KycDetails getAdhaar() {
		return adhaar;
	}

	/**
	 * @param adhaar the adhaar to set
	 */
	public void setAdhaar(KycDetails adhaar) {
		this.adhaar = adhaar;
	}
	
	

	/**
	 * @return the customerProfession
	 */
	public String getCustomerProfession() {
		return customerProfession;
	}

	/**
	 * @param customerProfession the customerProfession to set
	 */
	public void setCustomerProfession(String customerProfession) {
		this.customerProfession = customerProfession;
	}
	
	

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	

	/**
	 * @return the permanentAddress
	 */
	public String getPermanentAddress() {
		return permanentAddress;
	}

	/**
	 * @param permanentAddress the permanentAddress to set
	 */
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	/**
	 * @return the communicationAdderss
	 */
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}

	/**
	 * @param communicationAdderss the communicationAdderss to set
	 */
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}

	/**
	 * @return the currentAddress
	 */
	public String getCurrentAddress() {
		return currentAddress;
	}

	/**
	 * @param currentAddress the currentAddress to set
	 */
	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}
	
	

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the officeNumber
	 */
	public String getOfficeNumber() {
		return officeNumber;
	}

	/**
	 * @param officeNumber the officeNumber to set
	 */
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}
	
	

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	

	/**
	 * @return the tariffPlanVo
	 */
	public TariffPlanVo getTariffPlanVo() {
		return tariffPlanVo;
	}

	/**
	 * @param tariffPlanVo the tariffPlanVo to set
	 */
	public void setTariffPlanVo(TariffPlanVo tariffPlanVo) {
		this.tariffPlanVo = tariffPlanVo;
	}

	/**
	 * @return the documents
	 */
	public List<CustomerDocument> getDocuments() {
		return documents;
	}

	/**
	 * @param documents the documents to set
	 */
	public void setDocuments(List<CustomerDocument> documents) {
		this.documents = documents;
	}
	
	

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	

	/**
	 * @return the cxPermission
	 */
	public String getCxPermission() {
		return cxPermission;
	}

	/**
	 * @param cxPermission the cxPermission to set
	 */
	public void setCxPermission(String cxPermission) {
		this.cxPermission = cxPermission;
	}
	
	

	/**
	 * @return the externalRouterRequired
	 */
	public String getExternalRouterRequired() {
		return externalRouterRequired;
	}

	/**
	 * @param externalRouterRequired the externalRouterRequired to set
	 */
	public void setExternalRouterRequired(String externalRouterRequired) {
		this.externalRouterRequired = externalRouterRequired;
	}
	
	

	/**
	 * @return the paymentVO
	 */
	public PaymentVO getPaymentVO() {
		return paymentVO;
	}

	/**
	 * @param paymentVO the paymentVO to set
	 */
	public void setPaymentVO(PaymentVO paymentVO) {
		this.paymentVO = paymentVO;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EcafVo [cafNo=" + cafNo + ", createdDate=" + createdDate + ", place=" + place + ", dob=" + dob
				+ ", customerPhoto=" + customerPhoto + ", customerName=" + customerName + ", customerSign="
				+ customerSign + ", adhaar=" + adhaar + "]";
	}

	/**
	 * @return the isCustomerViaAadhaar
	 */
	public boolean isCustomerViaAadhaar() {
		return isCustomerViaAadhaar;
	}

	/**
	 * @param isCustomerViaAadhaar the isCustomerViaAadhaar to set
	 */
	public void setCustomerViaAadhaar(boolean isCustomerViaAadhaar) {
		this.isCustomerViaAadhaar = isCustomerViaAadhaar;
	}

	/**
	 * @return the primaryTransactionId
	 */
	public String getPrimaryTransactionId() {
		return primaryTransactionId;
	}

	/**
	 * @param primaryTransactionId the primaryTransactionId to set
	 */
	public void setPrimaryTransactionId(String primaryTransactionId) {
		this.primaryTransactionId = primaryTransactionId;
	}

	/**
	 * @return the secoundryTransactionId
	 */
	public String getSecoundryTransactionId() {
		return secoundryTransactionId;
	}

	/**
	 * @param secoundryTransactionId the secoundryTransactionId to set
	 */
	public void setSecoundryTransactionId(String secoundryTransactionId) {
		this.secoundryTransactionId = secoundryTransactionId;
	}

	/**
	 * @return the secondaryDate
	 */
	public String getSecondaryDate() {
		return secondaryDate;
	}

	/**
	 * @param secondaryDate the secondaryDate to set
	 */
	public void setSecondaryDate(String secondaryDate) {
		this.secondaryDate = secondaryDate;
	}

	/**
	 * @return the primaryDate
	 */
	public String getPrimaryDate() {
		return primaryDate;
	}

	/**
	 * @param primaryDate the primaryDate to set
	 */
	public void setPrimaryDate(String primaryDate) {
		this.primaryDate = primaryDate;
	}

	/**
	 * @return the alternativeMobileNo
	 */
	public String getAlternativeMobileNo() {
		return alternativeMobileNo;
	}

	/**
	 * @param alternativeMobileNo the alternativeMobileNo to set
	 */
	public void setAlternativeMobileNo(String alternativeMobileNo) {
		this.alternativeMobileNo = alternativeMobileNo;
	}

	/**
	 * @return the gisPojo
	 */
	public GISPojo getGisPojo() {
		return gisPojo;
	}

	/**
	 * @param gisPojo the gisPojo to set
	 */
	public void setGisPojo(GISPojo gisPojo) {
		this.gisPojo = gisPojo;
	}

	public CustomerAddressVO getCurrent() {
		return current;
	}

	public void setCurrent(CustomerAddressVO current) {
		this.current = current;
	}

	public CustomerAddressVO getCommunication() {
		return communication;
	}

	public void setCommunication(CustomerAddressVO communication) {
		this.communication = communication;
	}

	public CustomerAddressVO getPerminent() {
		return perminent;
	}

	public void setPerminent(CustomerAddressVO perminent) {
		this.perminent = perminent;
	}
	
	

}
