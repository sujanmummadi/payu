package com.cupola.fwmp.mail;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.cupola.fwmp.service.definition.DefinitionCoreService;

public class MailService
{
	final static Logger logger = Logger.getLogger(MailService.class);
	@Autowired
	JavaMailSender mailSender;

	@Autowired
	VelocityEngine velocityEngine;
	
	@Autowired
	DefinitionCoreService definitionCoreService;

	static String templateBasePath = "properties/velocity/";

	
	private JavaMailSenderImpl mailProperties()
	{
		logger.info("Initializing mail properties " );
		
		JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();

		String from = definitionCoreService.getMailConfig(DefinitionCoreService.EmailProperties.FROM);
		String password = definitionCoreService.getMailConfig(DefinitionCoreService.EmailProperties.PASSWORD);
		String host = definitionCoreService.getMailConfig(DefinitionCoreService.EmailProperties.HOST);
		String portStr = definitionCoreService.getMailConfig(DefinitionCoreService.EmailProperties.PORT);
		int port = portStr != null && !portStr.isEmpty() ? Integer.valueOf(portStr) : 587;
		String mailSmtpHost = definitionCoreService.getMailConfig(DefinitionCoreService.EmailProperties.MAIL_SMTP_HOST);
		String mailSmtpAuth = definitionCoreService.getMailConfig(DefinitionCoreService.EmailProperties.MAIL_SMTP_AUTH);
		String mailSmtpStarttlsEnable = definitionCoreService
				.getMailConfig(DefinitionCoreService.EmailProperties.MAIL_SMTP_STARTTLS_ENABLE);
		String mailSmtpSslTrust = definitionCoreService
				.getMailConfig(DefinitionCoreService.EmailProperties.MAIL_SMTP_SSL_TRUST);

		
		logger.info("Email Properties are :: from " + from + " password " + password + "  host " + host + " portStr " + portStr
				+ " port " + port + " mailSmtpHost " + mailSmtpHost + " mailSmtpAuth " + mailSmtpAuth
				+ " mailSmtpStarttlsEnable " + mailSmtpStarttlsEnable + " mailSmtpSslTrust " + mailSmtpSslTrust);

		mailSenderImpl.setUsername(from);
		mailSenderImpl.setPassword(password);
		mailSenderImpl.setHost(host);
		mailSenderImpl.setPort(port);

		Properties javaMailProperties = new Properties();

		javaMailProperties.setProperty(DefinitionCoreService.EmailProperties.MAIL_SMTP_HOST, mailSmtpHost);
		javaMailProperties.setProperty(DefinitionCoreService.EmailProperties.MAIL_SMTP_AUTH, mailSmtpAuth);
		javaMailProperties.setProperty(DefinitionCoreService.EmailProperties.MAIL_SMTP_STARTTLS_ENABLE,
				mailSmtpStarttlsEnable);
		javaMailProperties.setProperty(DefinitionCoreService.EmailProperties.MAIL_SMTP_SSL_TRUST, mailSmtpSslTrust);

		mailSenderImpl.setJavaMailProperties(javaMailProperties);

		logger.debug("mailSenderImpl " + mailSenderImpl.toString());

		return mailSenderImpl;
	}
	
	public void sendMiMeMail(String sub, String to, Map cc, Map bcc,
			String templateName, Object mailVO)
	{
		logger.info("Inside sendMiMeMail");
		
		mailSender = mailProperties();

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		
		try
		{
			String from = definitionCoreService.getMailConfig(DefinitionCoreService.EmailProperties.FROM);
			
			message.setFrom(new InternetAddress(from, "NoReply-ACT"));

			helper = new MimeMessageHelper(message, true);

			helper = new MimeMessageHelper(message, true);
			
			helper.setSubject(sub);

			if (to != null && !to.isEmpty())
			{
				String[] toList = to.split(",");
				for (int i = 0; i < toList.length; i++)
				{
					helper.addTo(toList[i]);
				}

			}

			// add the 'to'
			if (to != null && !to.isEmpty())
			{
				/*
				 * Iterator itr = to.keySet().iterator(); while(itr.hasNext()) {
				 * String name = (String) itr.next(); String recipient =
				 * (String) to.get(name);
				 */
				// helper.addTo(to);
				// }
			} else
			{
				logger.error(" To is not specify");
				return; //
			}
			// add the 'cc'
			if (cc != null && !cc.isEmpty())
			{
				Iterator itr = cc.keySet().iterator();

				while (itr.hasNext())
				{

					String name = (String) itr.next();
					String recipient = (String) cc.get(name);
					helper.addCc(recipient, name);
				}
			}

			// add the 'bcc'
			if (bcc != null && !bcc.isEmpty())
			{
				Iterator itr = bcc.keySet().iterator();
				while (itr.hasNext())
				{
					String name = (String) itr.next();
					String recipient = (String) bcc.get(name);
					logger.info(" recipient : " + recipient + " , name :"
							+ name);
					helper.addBcc(recipient, name);
				}
			}

			Map model = new HashMap();
			model.put("mailVO", mailVO);

			if (templateName != null && !templateName.isEmpty())
			{
				String text = VelocityEngineUtils
						.mergeTemplateIntoString(velocityEngine, templateBasePath
								+ templateName, "UTF-8", model);
				helper.setText(text, true);
			} else
			{
				String text = VelocityEngineUtils
						.mergeTemplateIntoString(velocityEngine, templateBasePath
								+ "vct.vm", "UTF-8", model);
				helper.setText(text, true);
			}

			// helper.addInline("image", new ClassPathResource("/image.png"));

		} catch (UnsupportedEncodingException e)
		{
			logger.error("Error While doing Encoding :"+e.getMessage());
			e.printStackTrace();
		} catch (MessagingException e)
		{
			logger.error("Error While sending Mail :"+e.getMessage());
			e.printStackTrace();
		}
		mailSender.send(message);
		logger.info("mail send successfully");
	}

	public void sendSimpleMail(String to, String sub, String msg)
	{
		logger.info(" Sending mail to  " + to);


		mailSender = mailProperties();
		
		MimeMessage message = mailSender.createMimeMessage();

		MimeMessageHelper helper;
		try
		{
			String from = definitionCoreService.getMailConfig("from");
			
			message.setFrom(new InternetAddress(from, "NoReply-ACT"));

			helper = new MimeMessageHelper(message, true);

			helper.setTo(to);
			helper.setSubject(sub);
			helper.setText(msg);

			mailSender.send(message);

			logger.info(" Sent mail to  " + to);

		} catch (Exception e)
		{
			logger.error(" Exception in sending mail " + e.getMessage());
		}

	}

	public void sendSimpleMailWithAttachment(String to, String subject,
			String msg, String attachmentDocName, String attachmentLoc)
	{
		logger.info("Inside sendSimpleMailWithAttachment");
		mailSender = mailProperties();

		MimeMessage message = mailSender.createMimeMessage();
		try
		{
			
			String from = definitionCoreService.getMailConfig("from");
		
			
		        message.setFrom(new InternetAddress(from, "NoReply-ACT"));
		        message.setRecipients(Message.RecipientType.TO,
		                InternetAddress.parse(to));
		        message.setSubject(subject);
		        message.setText(msg);

		        MimeBodyPart messageBodyPart = new MimeBodyPart();

		        Multipart multipart = new MimeMultipart();

		        messageBodyPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(attachmentLoc);
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        messageBodyPart.setFileName(attachmentDocName);
		        multipart.addBodyPart(messageBodyPart);

		        message.setContent(multipart);

		        mailSender.send(message);

		        logger.info("Email successfully sent with attachement for emailId " + to);
			
		} catch (Exception e)
		{
			logger.error("Error While sending SimpleMailWithAttachment for emailId " + to + " . " + e.getMessage());
			e.printStackTrace();
		}
		
	}

}
