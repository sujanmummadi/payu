package com.cupola.fwmp.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;

public class WorkProgressCode
{
	private static final Logger LOGGER = LogManager
			.getLogger(WorkProgressCode.class.getName());

	public APIResponse getWorkProgressCode()
	{
		LOGGER.info("get WorkProgress Order::::");
		return ResponseUtil
				.createSuccessResponse()
				.setData(
						CommonUtil
								.xformToTypeAheadFormat(DefinitionCoreServiceImpl.workProgressCode));
	}

	public String getWorkProgressCodeByKey(Long key)
	{
		LOGGER.info("get WorkProgress Order By Key:" + key);
		return DefinitionCoreServiceImpl.workProgressCode.get(key);
	}

}
