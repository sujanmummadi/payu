package com.cupola.fwmp.util;

public interface UserRoles {
	
	String ROLE_NI = "ROLE_NI";
	String ROLE_FR = "ROLE_FR";
	String ROLE_SALES_REPRESENTATIVE= "ROLE_SALES_REPRESENTATIVE";
	String ROLE_TEAM_LEAD = "ROLE_TEAM_LEAD";
	String ROLE_NETWORK_ENGINEER = "ROLE_NETWORK_ENGINEER";
	String ROLE_CX_DOWN = "ROLE_CX_DOWN";
	String ROLE_CX_UP = "ROLE_CX_UP";

}
