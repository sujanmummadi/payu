 
package com.cupola.fwmp.util;

import java.util.List;

public interface PagedResultSet<E>
{

	public int getPageSize ();

	public String getSortByField ();

	public int getStartOffset ();

	public int getTotalRecords ();

	public List<E> getResults ();

	public List<Integer> getStartOffsetByPageNumberList ();

}