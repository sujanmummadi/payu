package com.cupola.fwmp.util;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class StrictMicroSecondTimeBasedGuid
{
	private final static Logger logger = Logger
			.getLogger(StrictMicroSecondTimeBasedGuid.class);

	private static final long MICRO_IN_MILL = 10000;
	private static final long NANO_IN_MICRO = 10000;


	private static long baseNanoTime;
	private static long baseTimeInMicro;
	private static long lastGuid;
	
	private static final long MICRO_IN_MILL_FOR_TICKET = 10000;
	private static final long NANO_IN_MICRO_FOR_TICKET = 10000;

	private static long baseNanoTimeForTicket;
	private static long baseTimeInMicroForTicket;
	private static long lastGuidForTicket;
	

	static
	{
		/*
		 * Nanosecond time's reference is not known, therfore following logic is
		 * needed to calculate time in micro without knowing refrence point of
		 * nano time*
		 */
		baseNanoTime = System.nanoTime();
		baseTimeInMicro = System.currentTimeMillis() * MICRO_IN_MILL;
		lastGuid = baseTimeInMicro;
		
		baseNanoTimeForTicket = System.nanoTime();
		baseTimeInMicroForTicket = System.currentTimeMillis() * MICRO_IN_MILL_FOR_TICKET;
		lastGuidForTicket = baseTimeInMicroForTicket;
	}

	public static synchronized Long newGuid()
	{
		long newGuid;

		while ((newGuid = calNewTimeInMicro()) <= lastGuid)
		{
			/** we have to check for this log, we don't want to see log. */

			logger.info("wait of 10-microsecond is introduced to get new guid");

			try
			{
				TimeUnit.MICROSECONDS.sleep(100) ;
			} catch (InterruptedException e)
			{
				logger.error("Error", e);
			}
		}

		lastGuid = newGuid;
		return (long) (newGuid* Math.random());
	}
	
	public static synchronized Long newGuidForTicket()
	{
		long newGuid;


		synchronized (StrictMicroSecondTimeBasedGuid.class) {

			while ((newGuid = calNewTimeInMicroForTicket()) <= lastGuidForTicket) {
				/** we have to check for this log, we don't want to see log. */

				logger.info("wait of 100-microsecond is introduced to get new guid");

				try {
					TimeUnit.MICROSECONDS.sleep(800);
				} catch (InterruptedException e) {
					logger.error("Error", e);
				}
			}
			lastGuidForTicket = newGuid;
			return (long) (newGuid * Math.random());
		}

	}

	private static long calNewTimeInMicro()
	{
		return baseTimeInMicro
				+ ((System.nanoTime() - baseNanoTime) / NANO_IN_MICRO);
	}
	private static long calNewTimeInMicroForTicket()
	{
		return baseTimeInMicroForTicket
				+ ((System.nanoTime() - baseNanoTimeForTicket) / NANO_IN_MICRO_FOR_TICKET);
	}
}
