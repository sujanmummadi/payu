package com.cupola.fwmp.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;

public class FTPUtil
{
	public static Logger logger = Logger.getLogger(FTPUtil.class);

	private static String filePath = null;
	private static  String remoteFilePath =null;
	static FTPClient  ftpClient = createFTPClient();

	public static FTPClient getFTPClient()
	{
		return ftpClient;

	}

	private FTPUtil()
	{
	}
	
	
	public static String getlocalBasePath()
	{
		return filePath;
	}
	public static String getRemoteBasePath()
	{
		return remoteFilePath;
	}
	
	private static FTPClient createFTPClient()
	{

	

			final String server = DefinitionCoreServiceImpl.ftpClientProperties.get("server");
			final int port = Integer.parseInt( DefinitionCoreServiceImpl.ftpClientProperties.get("port"));
			final String user =  DefinitionCoreServiceImpl.ftpClientProperties.get("username");
	
			final String pass =  DefinitionCoreServiceImpl.ftpClientProperties.get("password");
			  
			filePath = DefinitionCoreServiceImpl.ftpClientProperties.get("file_path_download");
			 remoteFilePath = DefinitionCoreServiceImpl.ftpClientProperties.get("remote_file_path_download");
			ftpClient = new FTPClient();
			InputStream inputStream;

			try
			{
				ftpClient.connect(server, port);
				ftpClient.login(user, pass);
				ftpClient.enterLocalPassiveMode();
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

				logger.info("Start uploading file");
				logger.info("Name and path of file in local " + filePath);
				logger.info("Name and path of remote file " + remoteFilePath);

				 

			} catch (IOException ex)
			{

				ex.printStackTrace();
			} finally
			{ 
			}

		
		return ftpClient;

	}
	

}
