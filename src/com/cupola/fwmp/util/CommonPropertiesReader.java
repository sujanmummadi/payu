package com.cupola.fwmp.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class CommonPropertiesReader
{

	private static final Logger logger = LogManager
			.getLogger(CommonPropertiesReader.class.getName());

	public Map<String, String> readMessageProperties()
	{
		return readProperties("properties/etr_change_reason_code.properties");
	}

	public Map<Long, String> readCustomerTypeProperties()
	{
		return readPropertiesLongAsKey("properties/customer_type.properties");
	}

	private Map<String, String> readProperties(String filePath)
	{
		Map<String, String> messsage = new LinkedHashMap<String, String>();

		ClassLoader classLoader = this.getClass().getClassLoader();
		InputStream input = classLoader.getResourceAsStream(filePath);
		Properties properties = new Properties();
		try
		{
			properties.load(input);

			Set<Object> test = properties.keySet();

			for (Iterator iterator = test.iterator(); iterator.hasNext();)
			{
				String object = (String) iterator.next();
				messsage.put(object, properties.getProperty(object));
			}

		} catch (IOException e)
		{
			logger.error("Error while reading property files "
					+ e.getMessage());
			e.printStackTrace();
		} finally
		{
			try
			{
				if (input != null)
					input.close();
			} catch (IOException e)
			{
				logger.error("Error while closing streams " + e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return messsage;
	}

	private Map<Long, String> readPropertiesLongAsKey(String filePath)
	{

		Map<Long, String> messsage = new LinkedHashMap<Long, String>();

		ClassLoader classLoader = this.getClass().getClassLoader();
		InputStream input = classLoader.getResourceAsStream(filePath);
		Properties properties = new Properties();
		try
		{
			properties.load(input);

			Set<Object> test = properties.keySet();

			for (Iterator iterator = test.iterator(); iterator.hasNext();)
			{
				String object = (String) iterator.next();
				messsage.put(Long.valueOf(object), properties
						.getProperty(object));
			}

		} catch (IOException e)
		{
			logger.error("Error while reading property files "
					+ e.getMessage());
			e.printStackTrace();
		} finally
		{
			try
			{
				input.close();
			} catch (IOException e)
			{
				logger.error("Error while closing streams " + e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return messsage;
	}

	public Map<String, String> readStatusDefProperties()
	{
		return readProperties("properties/roleFilterStatusDef.properties");

	}

	public Map<Long, String> readHydMediaSource()

	{
		return readPropertiesLongAsKey("properties/hyd_media_source.properties");
	}

	public Map<Long, String> readRoiMediaSource()

	{
		return readPropertiesLongAsKey("properties/roi_media_source.properties");
	}

	public Map<Long, String> readInquiry()

	{

		return readPropertiesLongAsKey("properties/inquiry.properties");
	}

	public Map<String, String> readCustomerNotificationStatusDefProperties()
	{
		return readProperties("properties/notification_configuration.properties");
	}

	public Map<String, String> readNotificationMessageDefProperties()
	{
		return readProperties("properties/notification_mesaages_contents.properties");
	}

	public Map<String, String> readCustomerNotificationMessageDefProperties()
	{
		return readProperties("properties/customer_notification_messages.properties");
	}

	public Map<Long, String> readPaymentAcceptResoncode()

	{
		return readPropertiesLongAsKey("properties/payment_accept_reasoncode.properties");
	}

	public Map<Long, String> readPaymentRejectResoncode()

	{
		return readPropertiesLongAsKey("properties/payment_reject_reasoncode.properties");
	}

	public Map<Long, String> readPaymentRefundResoncode()

	{
		return readPropertiesLongAsKey("properties/payment_refund_reasoncode.properties");
	}

	public Map<Long, String> readDocumentRejectReasonCode()

	{
		return readPropertiesLongAsKey("properties/document_reject_reasoncode.properties");
	}

	public Map<Long, String> readDocumentAcceptReasonCode()

	{
		return readPropertiesLongAsKey("properties/document_accept_reasoncode.properties");
	}

	public Map<Long, String> readReassignRejectReasoncode()

	{
		return readPropertiesLongAsKey("properties/reassign_reject_reasoncode.properties");
	}

	public Map<Long, String> readReassignAcceptReasoncode()

	{
		return readPropertiesLongAsKey("properties/reassign_accept_reasoncode.properties");
	}

	public Map<Long, String> readHotProspectReasoncode()

	{
		return readPropertiesLongAsKey("properties/hot_prospect_reasoncode.properties");
	}

	public Map<Long, String> readResolveStatusCode()

	{
		return readPropertiesLongAsKey("properties/resolve_status_code.properties");

	}

	public Map<String, String> readSchedularProperties()
	{

		return readProperties("scheduler.properties");
	}

	public Map<String, String> readClassificationCategoryDefProperties()
	{
		return readProperties("properties/classification.properties");
	}

	public Map<String, String> readMqErrors()
	{
		return readProperties("properties/mq_error_message.properties");
	}

	public Map<Long, String> readfeasibilityReasonCode()
	{
		return readPropertiesLongAsKey("properties/feasibility_reasoncode.properties");
	}

	public Map<Long, String> readColdProspectReasoncode()
	{
		return readPropertiesLongAsKey("properties/cold_prospect_reasoncode.properties");
	}

	public Map<Long, String> readMediumProspectReasoncode()
	{
		return readPropertiesLongAsKey("properties/medium_prospect_reasoncode.properties");
	}

	public Map<Long, String> readFeasibilityRejectedByNeReasons()
	{
		return readPropertiesLongAsKey("properties/feasibilityRejectedByNeReasons.properties");
	}

	public Map<Long, String> readCopper2fiber()
	{
		return readPropertiesLongAsKey("properties/copper2fiberreasoncode.properties");
	}

	public Map<Long, String> readDeploymentCustomerDeniedReasons()
	{
		return readPropertiesLongAsKey("properties/deploymentCustomerDeniedReasons.properties");
	}

	public Map<Long, String> readSalesCustomerDeniedReasons()
	{
		return readPropertiesLongAsKey("properties/salesCustomerDeniedReasons.properties");
	}

	public Map<Long, String> readFiberTypes()
	{
		return readPropertiesLongAsKey("properties/fiberTypes.properties");
	}

	public Map<String, String> readEmailCorporateConfigurationProperties()
	{
		return readProperties("properties/email_corporate_configuration.properties");

	}

	public Map<String, String> readVersionDetails()
	{
		return readProperties("properties/version.properties");
	}

	public Map<String, String> readFrNatureCodes()

	{
		Map<String, String> frNatureCodes = new LinkedHashMap<String, String>();
		Map<String, String> result = readProperties("properties/fr.natures.properties");
		for (String keys : result.keySet())
			frNatureCodes.put(result.get(keys), keys);
		return frNatureCodes;
	}

	public Map<String, String> readFrSymptomNames()
	{
		return readProperties("properties/fr_symptom_name_flowId_mapping.properties");
	}

	public Map<Long, String> readFrClosureRemarks()
	{
		return readPropertiesLongAsKey("properties/fr_closure_remarks.properties");
	}

	public Map<Long, String> readWorkProgressCode()
	{
		return readPropertiesLongAsKey("properties/work_progress_code.properties");
	}

	public Map<Long, String> readElementCopperDenialReasonCodeBlr()
	{
		return readPropertiesLongAsKey("properties/element_copper_denial_reasons_blr.properties");
	}

	public Map<Long, String> readElementCopperDenialReasonCodeHyd()
	{
		return readPropertiesLongAsKey("properties/element_copper_denial_reasons_hyd.properties");
	}

	public Map<Long, String> readElementFiberDenialReasonCodeHyd()
	{
		return readPropertiesLongAsKey("properties/element_fiber_denial_reasons_hyd.properties");
	}

	public Map<Long, String> readElementFiberDenialReasonCodeBlr()
	{
		return readPropertiesLongAsKey("properties/element_fiber_denial_reasons_blr.properties");
	}

	public Map<Long, String> readMqErrorMessage()
	{
		return readPropertiesLongAsKey("properties/mq_error_message.properties");
	}

	public Map<String, String> readIntegrationProperties()
	{
		return readProperties("properties/integrations.properties");
	}

	public Map<String, String> readDevcieToolProperties()
	{
		return readProperties("properties/tools.properties");
	}

	public Map<Long, String> readProfessions()
	{
		return readPropertiesLongAsKey("properties/profession.properties");
	}

	public Map<Long, String> readTitle()
	{
		return readPropertiesLongAsKey("properties/titles.properties");
	}

	public Map<Long, String> readProspectTypes()
	{
		return readPropertiesLongAsKey("properties/prospect_types.properties");
	}

	public Map<Long, String> readElementCopperDenialNonFeasibleReasonCodeHyd()
	{
		return readPropertiesLongAsKey("properties/element_feasibility_denial_reasons_hyd.properties");
	}

	public Map<Long, String> readElementCopperDenialNonFeasibleReasonCodeBlr()
	{
		return readPropertiesLongAsKey("properties/element_feasibility_denial_reasons_blr.properties");
	}

	public Map<Long, String> readElementPermissionReasonCodeHyd()
	{
		return readPropertiesLongAsKey("properties/element_permission_denial_reasons_hyd.properties");
	}

	public Map<Long, String> readElementPermissionReasonCodeBlr()
	{
		return readPropertiesLongAsKey("properties/element_permission_denial_reasons_blr.properties");
	}

	public Map<String, String> readhelpLineNUmbersMessageDefProperties()
	{
		return readProperties("properties/helpline_number_definition.properties");
	}

	public Map<Long, String> readCxDenialReasonCodeBlr()
	{
		return readPropertiesLongAsKey("properties/element_rac_fixing_denial_reasons_blr.properties");
	}

	public Map<Long, String> readCxDenialReasonCodeHyd()
	{
		return readPropertiesLongAsKey("properties/element_rac_fixing_denial_reasons_hyd.properties");
	}

	public Map<String, String> readClassificationQueryProperties()
	{
		return readProperties("properties/classification.properties");
	}

	public Map<String, String> readThreadPoolInfoProperties()
	{
		return readProperties("properties/thread_pool_Info.properties");
	}
	public Map<Long, String> readPaymentMode()
	{
		return readPropertiesLongAsKey("properties/paymentMode.properties");
	}
	
	public Map<String, String> readDefectAndSubDefectCode()
	{
		return readProperties("properties/fr_defect_subDefect_code_mapping.properties");
	}
	
	public Map<String, String> readFRDefaultETR()
	{
		return readProperties("properties/defaultETR.properties") ;
	}

	public Map<String, String> getFrStatusMap()
	{
		return readProperties("properties/frStatus.properties");
	}

	public Map<Long, String> getFrPushBackCount()
	{
		return readPropertiesLongAsKey("properties/fr_push_back_reason_code.properties");
	}

	public Map<String, String> readeDefectCodeNameAndValue()
	{
		return readProperties("properties/fr_defect_code.properties");
	}

	public Map<String, String> readFrMqClosureCodeWithDefectSubDefectCode() 
	{
		return readProperties("properties/fr_defect_subdefect_mq_closur_code_mapping.properties");
	}
	
	public Map<String, String> readFrMqClosureCodeHydWithDefectSubDefectCode() 
	{
		return readProperties("properties/fr_defect_sub_mqcode_hyd_mapping.properties");
	}

	public Map<String, String> getIntegrationMap()
	{
		return readProperties("properties/integrations.properties");
	}
	
	
	public Map<String, String> readFTPProperties()
	{
		return readProperties("properties/ftpconfig.properties");
	}

	public Map<Long, String> allStates()
	{
		return readPropertiesLongAsKey("properties/states.properties");
	}
	
	public Map<Long, String> getnationality()
	{
		return readPropertiesLongAsKey("properties/nationality.properties");
	}
	
	public Map<Long, String> getExistingISP()
	{
		return readPropertiesLongAsKey("properties/existingISP.properties");
	}
	
	public Map<Long, String> getUinType()
	{
		return readPropertiesLongAsKey("properties/uinType.properties");
	}
	
	
	public Map<String, String> readBufferTimeProperties()
	{
		return readProperties("properties/buffer_time_branchwise.properties");
	}
	
	public Map<String, String> readWorkingHoursProperties()
	{
		return readProperties("properties/working_hours.properties");
	}

	/**@author kiran
	 * Map<String,String>
	 * @return
	 */
	public Map<String, String> readMergingQueueProperties()
	{
		return readProperties("properties/mergemainqueue.properties");
	}

	/**@author kiran
	 * Map<String,String>
	 * @return
	 */
	public Map<String, String> readFRCityWiseDefaultETR()
	{
		return readProperties("properties/cityWiseDefaultETR.properties") ;
	}

	/**@author aditya
	 * Map<String,String>
	 * @return
	 */
	public Map<String, String> readMailConfig()
	{
		return readProperties("properties/mail_config.properties") ;
	}
	/**@author aditya
	 * Map<String,String>
	 * @return
	 */
	public Map<String, String> readDeploymentProperties() 
	{
		return readProperties("properties/deployment.properties") ;
	}

	/**@author aditya
	 * Map<Long,String>
	 * @return
	 */
	public Map<Long, String> readEscalationType() {
		 
		return readPropertiesLongAsKey("properties/escalation_type.properties");	 
	}

}
