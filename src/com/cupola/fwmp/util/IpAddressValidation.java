package com.cupola.fwmp.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IpAddressValidation {

	public static boolean ipValidation(String ip) {

		String pattern = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);

		// Now create matcher object.
		Matcher m = r.matcher(ip);

		if (m.matches()) {
			return true;
		}
		return false;
	}
}
