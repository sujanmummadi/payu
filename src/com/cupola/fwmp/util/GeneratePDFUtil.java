/**
* @Author kiran  Dec 11, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.PdfCoOrdinates;
import com.cupola.fwmp.dao.mongo.documents.CustomerDocument;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.vo.EcafVo;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import sun.misc.BASE64Decoder;

/**
 * @Author kiran Dec 11, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class GeneratePDFUtil {

	@Autowired
	DefinitionCoreService definitionCoreService;

	private static Logger log = LogManager.getLogger(GeneratePDFUtil.class.getName());

	/***
	 * @author kiran void
	 * @param src
	 * @param dest
	 * @param ecafVo
	 */
	public void manipulateECAFPdf(String src, String dest, EcafVo ecafVo) {

		try {

			PdfReader reader = new PdfReader(src);

			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));

			PdfContentByte contentOfFirstPage = stamper.getOverContent(1);
			renderingDataInFirstPage(stamper, contentOfFirstPage, ecafVo);

			PdfContentByte contentOfSecondPage = stamper.getOverContent(2);
			renderingDataInSecondPage(stamper, contentOfSecondPage, ecafVo);

			PdfContentByte contentOfSexthPage = stamper.getOverContent(6);
			renderingDataInSixthPage(stamper, contentOfSexthPage, ecafVo);

			stamper.close();
			reader.close();

		} catch (Exception e) {

			log.error("Error while manipulateECAFPdf ::" + e.getMessage());
			e.printStackTrace();

		}

	}

	/***
	 * 
	 * @param stamper
	 * @param contentOfSexthPage
	 * @param ecafVo
	 */
	private void renderingDataInSixthPage(PdfStamper stamper, PdfContentByte contentOfSexthPage, EcafVo ecafVo) {

		try {
			
			if (ecafVo.getCustomerSign() != null)
				addCustomerSignature(stamper, contentOfSexthPage, ecafVo.getCustomerSign(),PdfCoOrdinates.DECLARATION_SIGN_X,
						PdfCoOrdinates.DECLARATION_SIGN_Y);
			
			if (ecafVo.getCreatedDate() != null)
			addText(contentOfSexthPage, ecafVo.getCreatedDate(), PdfCoOrdinates.DECLARATION_SIGN_DATE_X,
					PdfCoOrdinates.DECLARATION_SIGN_DATE_Y);
			
			if (ecafVo.getPlace() != null)
			addText(contentOfSexthPage, ecafVo.getPlace(),
					PdfCoOrdinates.DECLARATION_SIGN_PLACE_X, PdfCoOrdinates.DECLARATION_SIGN_PLACE_Y);
		} catch (Exception e) {

			log.error("Error while manipulateECAFPdf ::" + e.getMessage());
			e.printStackTrace();

		}
	}

	/**
	 * @author kiran void
	 * @param stamper
	 * @param contentOfFirstPage
	 * @param ecafVo
	 */
	private void renderingDataInFirstPage(PdfStamper stamper, PdfContentByte contentOfFirstPage, EcafVo ecafVo) {

		try {

			if (ecafVo.getAdhaar().getPhoto() != null)
				addCustomerPhoto(stamper, contentOfFirstPage, ecafVo.getAdhaar().getPhoto());
			else {
				if (ecafVo.getCustomerPhoto() != null)
					addCustomerPhoto(stamper, contentOfFirstPage, ecafVo.getCustomerPhoto());
			}

			if( ecafVo.getCafNo() != null)
			   addText(contentOfFirstPage, ecafVo.getCafNo(), PdfCoOrdinates.CAF_NO_X, PdfCoOrdinates.CAF_NO_Y);

			if (ecafVo.getCreatedDate() != null) {
				String[] creationDate = StringUtils.split(ecafVo.getCreatedDate());
				addText(contentOfFirstPage, creationDate[0], PdfCoOrdinates.CREATION_DATE_X,
						PdfCoOrdinates.CREATION_DATE_Y);
			}
			if( ecafVo.getCreatedDate() != null)
			addText(contentOfFirstPage, ecafVo.getCreatedDate(), PdfCoOrdinates.CREATION_DATE_X,
					PdfCoOrdinates.CREATION_DATE_Y);

			if( ecafVo.getPlace() != null)
			addText(contentOfFirstPage, ecafVo.getPlace(), PdfCoOrdinates.PLACE_X, PdfCoOrdinates.PLACE_Y);
			if( ecafVo.getCustomerName() != null)
			addText(contentOfFirstPage, ecafVo.getCustomerName(), PdfCoOrdinates.CUSTOMER_NAME_X,
					PdfCoOrdinates.CUSTOMER_NAME_Y);

			if (ecafVo.getAdhaar().getDob() != null) {
				String dob = ecafVo.getAdhaar().getDob();

				String[] ary = dob.split("");

				for (int i = 0; i < ary.length; i++) {

					if (i == 0)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.D_1_X, PdfCoOrdinates.D_1_Y);

					if (i == 1)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.D_2_X, PdfCoOrdinates.D_2_Y);

					if (i == 3)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.M_1_X, PdfCoOrdinates.M_1_Y);

					if (i == 4)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.M_2_X, PdfCoOrdinates.M_2_Y);

					if (i == 6)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.Y_1_X, PdfCoOrdinates.Y_1_Y);

					if (i == 7)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.Y_2_X, PdfCoOrdinates.Y_2_Y);

					if (i == 8)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.Y_3_X, PdfCoOrdinates.Y_3_Y);

					if (i == 9)
						addText(contentOfFirstPage, ary[i], PdfCoOrdinates.Y_4_X, PdfCoOrdinates.Y_4_Y);
				}

			}

			if (ecafVo.getPrimaryTransactionId() != null)
				addText(contentOfFirstPage, ecafVo.getPrimaryTransactionId(), PdfCoOrdinates.PRIMARY_TX_X,
						PdfCoOrdinates.PRIMARY_TX_Y);

			if (ecafVo.getPrimaryDate() != null) {
				String[] splitedPrimaryDate = StringUtils.split(ecafVo.getPrimaryDate());
				addText(contentOfFirstPage, splitedPrimaryDate[0], PdfCoOrdinates.PRIMARY_TX_DATE_X,
						PdfCoOrdinates.PRIMARY_TX_DATE_Y);
				addText(contentOfFirstPage, splitedPrimaryDate[1], PdfCoOrdinates.PRIMARY_TX_TIME_X,
						PdfCoOrdinates.PRIMARY_TX_TIME_Y);
			}

			if( ecafVo.getCustomerProfession() != null)
			addText(contentOfFirstPage, ecafVo.getCustomerProfession(), PdfCoOrdinates.PROFESSION_X,
					PdfCoOrdinates.PROFESSION_Y);

			if (ecafVo.getAdhaar().getCareof() != null)
				addText(contentOfFirstPage, ecafVo.getAdhaar().getCareof(), PdfCoOrdinates.CARE_OF_X,
						PdfCoOrdinates.CARE_OF_Y);

			if (ecafVo.getAdhaar().getGender() != null)
				addText(contentOfFirstPage, ecafVo.getAdhaar().getGender(), PdfCoOrdinates.GENDER_X,
						PdfCoOrdinates.GENDER_Y);

			if (ecafVo.getAdhaar().getCompleteAddress() != null)
				addText(contentOfFirstPage, ecafVo.getAdhaar().getCompleteAddress(), PdfCoOrdinates.AADHAR_ADDRESS_X,
						PdfCoOrdinates.AADHAR_ADDRESS_Y);

			if (ecafVo.getAdhaar().getVtcname() != null)
				addText(contentOfFirstPage, ecafVo.getAdhaar().getVtcname(), PdfCoOrdinates.VTCNAME_X,
						PdfCoOrdinates.VTCNAME_Y);

			if (ecafVo.getAdhaar().getState() != null)
				addText(contentOfFirstPage, ecafVo.getAdhaar().getState(), PdfCoOrdinates.AADHAR_STATE_X,
						PdfCoOrdinates.AADHAR_STATE_Y);

			if (ecafVo.getAdhaar().getPincode() != null)
				addText(contentOfFirstPage, ecafVo.getAdhaar().getPincode(), PdfCoOrdinates.AADHAR_PINCODE_X,
						PdfCoOrdinates.AADHAR_PINCODE_Y);

			if (ecafVo.getMobileNumber() != null)
				addText(contentOfFirstPage, ecafVo.getMobileNumber(), PdfCoOrdinates.AADHAR_MOBILE_NUMBER_X,
						PdfCoOrdinates.AADHAR_MOBILE_NUMBER_Y);

			if(ecafVo.getCommunication() != null)
			{
				if(ecafVo.getCommunication().getAddress1() != null)
				addText(contentOfFirstPage, ecafVo.getCommunication().getAddress1(), PdfCoOrdinates.ADDRESS_X,
						PdfCoOrdinates.ADDRESS_Y);
			}else
			{
				if( ecafVo.getCommunicationAdderss() != null)
				addText(contentOfFirstPage, ecafVo.getCommunicationAdderss(), PdfCoOrdinates.ADDRESS_X,
						PdfCoOrdinates.ADDRESS_Y);
			}
				
			
			
			if( ecafVo.getPlace() != null)
			addText(contentOfFirstPage, ecafVo.getPlace(), PdfCoOrdinates.CITY_X, PdfCoOrdinates.CITY_Y);
			if( ecafVo.getState() != null)
			addText(contentOfFirstPage, ecafVo.getState(), PdfCoOrdinates.STATE_X, PdfCoOrdinates.STATE_Y);
			if( ecafVo.getPincode() != null)
			addText(contentOfFirstPage, ecafVo.getPincode(), PdfCoOrdinates.PINCODE_X, PdfCoOrdinates.PINCODE_Y);
			if( ecafVo.getMobileNumber() != null)
			addText(contentOfFirstPage, ecafVo.getMobileNumber(), PdfCoOrdinates.MOBILE_NUMBER_X,
					PdfCoOrdinates.MOBILE_NUMBER_Y);

			if (ecafVo.getAlternativeMobileNo() != null)
				addText(contentOfFirstPage, ecafVo.getAlternativeMobileNo(), PdfCoOrdinates.ALTERNATE_MOBILE_NUMBER_X,
						PdfCoOrdinates.ALTERNATE_MOBILE_NUMBER_Y);

			if (ecafVo.getDocuments() != null) {
				List<CustomerDocument> documents = ecafVo.getDocuments();

				for (CustomerDocument customerDocument : documents) {

					if (customerDocument.getDisplayName().equalsIgnoreCase(PdfCoOrdinates.DRIVING))
						addTick(contentOfFirstPage, stamper, PdfCoOrdinates.DRIVING_X, PdfCoOrdinates.DRIVING_Y);

					if (customerDocument.getDisplayName().equalsIgnoreCase(PdfCoOrdinates.PASSPORT))
						addTick(contentOfFirstPage, stamper, PdfCoOrdinates.PASSPORT_X, PdfCoOrdinates.PASSPORT_Y);

					if (customerDocument.getDisplayName().equalsIgnoreCase(PdfCoOrdinates.PANCARD))
						addTick(contentOfFirstPage, stamper, PdfCoOrdinates.PANCARD_X, PdfCoOrdinates.PANCARD_Y);

					if (customerDocument.getDisplayName().equalsIgnoreCase(PdfCoOrdinates.BILL))
						addTick(contentOfFirstPage, stamper, PdfCoOrdinates.RATION_CARD_X,
								PdfCoOrdinates.RATION_CARD_Y);
				}
			}

			if( ecafVo.getEmailId() != null)
			addText(contentOfFirstPage, ecafVo.getEmailId(), PdfCoOrdinates.EMAIL_ADDRESS_X,
					PdfCoOrdinates.EMAIL_ADDRESS_Y);

			// for Release 3.0
			/*
			 * addTick(contentOfFirstPage, stamper, PdfCoOrdinates.STATIC_IP_YES_X,
			 * PdfCoOrdinates.STATIC_IP_YES_Y); addTick(contentOfFirstPage, stamper,
			 * PdfCoOrdinates.STATIC_IP_NO_X, PdfCoOrdinates.STATIC_IP_NO_Y);
			 */

			if (ecafVo.getCxPermission() != null) {
				boolean cxPermission = ecafVo.getCxPermission().equalsIgnoreCase("true");

				if (cxPermission)
					addTick(contentOfFirstPage, stamper, PdfCoOrdinates.CX_PERMISSION_YES_X,
							PdfCoOrdinates.CX_PERMISSION_YES_Y);
				else
					addTick(contentOfFirstPage, stamper, PdfCoOrdinates.CX_PERMISSION_NO_X,
							PdfCoOrdinates.CX_PERMISSION_NO_Y);

			}

		} catch (Exception e) {

			log.error("Error while rendaring data in first page ::" + e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * @author kiran void
	 * @param stamper
	 * @param over
	 * @throws IOException
	 * @throws DocumentException
	 */
	private void addCustomerPhoto(PdfStamper stamper, PdfContentByte over, String customerPhoto)
			throws IOException, DocumentException {
		Image image = null;
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] imageByte = decoder.decodeBuffer(customerPhoto);
		ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
		BufferedImage bimg = ImageIO.read(bis);

		int width = bimg.getWidth();
		int height = bimg.getHeight();

		if (width > PdfCoOrdinates.PIC_MAX_WIDTH) {
			width = PdfCoOrdinates.PIC_MAX_WIDTH;
		}
		if (height > PdfCoOrdinates.PIC_MAX_HEIGHT) {
			height = PdfCoOrdinates.PIC_MAX_HEIGHT;
		}

		if (height == PdfCoOrdinates.PIC_MAX_HEIGHT || width == PdfCoOrdinates.PIC_MAX_WIDTH) {

			bimg = scaleImage(width, height, imageByte, null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bimg, "png", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();

			image = Image.getInstance(bimg, null);
			baos.close();
		}

		PdfImage stream = new PdfImage(image, "", null);
		stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
		PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
		image.setDirectReference(ref.getIndirectReference());
		// image.setAbsolutePosition(80, 600);
		over.addImage(image, width, 0, 0, height, PdfCoOrdinates.IMAGE_X, PdfCoOrdinates.IMAGE_Y);

		bis.close();
	}

	/**
	 * @author kiran BufferedImage
	 * @param WIDTH
	 * @param HEIGHT
	 * @param imageByte
	 * @return
	 */
	public BufferedImage scaleImage(int WIDTH, int HEIGHT, byte[] imageByte, String filename) {
		BufferedImage bi = null;
		try {
			ImageIcon ii;
			if (imageByte != null)
				ii = new ImageIcon(imageByte);// path to image
			else
				ii = new ImageIcon(filename);

			bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = (Graphics2D) bi.createGraphics();
			g2d.addRenderingHints(
					new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
			g2d.drawImage(ii.getImage(), 0, 0, WIDTH, HEIGHT, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return bi;
	}

	/**
	 * @author kiran void
	 * @param cb
	 * @param text
	 * @param x
	 * @param y
	 */
	public static void addText(PdfContentByte pdfContentByte, String text, int x, int y) {

		try {

			BaseFont baseFont = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			pdfContentByte.saveState();
			pdfContentByte.beginText();
			pdfContentByte.moveText(x, y);
			pdfContentByte.setFontAndSize(baseFont, 8);
			pdfContentByte.showText(text);
			pdfContentByte.endText();
			pdfContentByte.restoreState();

		} catch (Exception e) {

			log.error("Error while addText ::" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * @author kiran void
	 * @param over
	 * @param stamper
	 * @param x
	 * @param y
	 * @throws IOException
	 * @throws DocumentException
	 */
	private void addTick(PdfContentByte over, PdfStamper stamper, int x, int y) throws IOException, DocumentException {

		String tickPath = definitionCoreService.getIntegrationFilePropertiesByKey(DefinitionCoreService.TICK_KEY);

		Image image = Image.getInstance(tickPath);
		BufferedImage bimg = ImageIO.read(new File(tickPath));
		int width = bimg.getWidth();
		int height = bimg.getHeight();

		if (width > PdfCoOrdinates.TICK_MAX_WIDTH) {
			width = PdfCoOrdinates.TICK_MAX_WIDTH;
		}
		if (height > PdfCoOrdinates.TICK_MAX_HEIGHT) {
			height = PdfCoOrdinates.TICK_MAX_HEIGHT;
		}

		if (height == PdfCoOrdinates.TICK_MAX_HEIGHT || width == PdfCoOrdinates.TICK_MAX_WIDTH) {

			bimg = scaleImage(width, height, null, tickPath);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bimg, "png", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			image = Image.getInstance(imageInByte);
		}

		PdfImage stream = new PdfImage(image, "", null);
		stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
		PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
		image.setDirectReference(ref.getIndirectReference());
		over.addImage(image, width, 0, 0, height, x, y);
	}

	/**
	 * @author kiran void
	 * @param stamper
	 * @param contentOfSecondPage
	 * @param ecafVo
	 */
	private void renderingDataInSecondPage(PdfStamper stamper, PdfContentByte contentOfSecondPage, EcafVo ecafVo) {

		try {

			if( ecafVo.getTariffPlanVo().getPackageName() != null)
			addText(contentOfSecondPage, ecafVo.getTariffPlanVo().getPackageName(), PdfCoOrdinates.TARIFF_PLAN_X,
					PdfCoOrdinates.TARIFF_PLAN_Y);

			if (ecafVo.getTariffPlanVo().getTenure() != null) {
				int duration = Integer.valueOf(ecafVo.getTariffPlanVo().getTenure());

				if (duration == 1)
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.TARIFF_PLAN_DURATION_1_X,
							PdfCoOrdinates.TARIFF_PLAN_DURATION_1_Y);

				if (duration == 3)
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.TARIFF_PLAN_DURATION_3_X,
							PdfCoOrdinates.TARIFF_PLAN_DURATION_3_Y);

				if (duration == 6)
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.TARIFF_PLAN_DURATION_6_X,
							PdfCoOrdinates.TARIFF_PLAN_DURATION_6_Y);

				if (duration == 12)
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.TARIFF_PLAN_DURATION_12_X,
							PdfCoOrdinates.TARIFF_PLAN_DURATION_12_Y);
			}

			if (ecafVo.getExternalRouterRequired() != null) {

				int routerRequired = Integer.valueOf(ecafVo.getExternalRouterRequired());

				if (routerRequired == 1)
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.ROUTER_YES_X, PdfCoOrdinates.ROUTER_YES_Y);
				else
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.ROUTER_NO_X, PdfCoOrdinates.ROUTER_NO_Y);

			}

			if( ecafVo.getTariffPlanVo().getInstallCharges() != null)
			addText(contentOfSecondPage, ecafVo.getTariffPlanVo().getInstallCharges(),
					PdfCoOrdinates.INSTALLATION_CHARGES_X, PdfCoOrdinates.INSTALLATION_CHARGES_Y);
			
			if( ecafVo.getPaymentVO().getPaidAmount() != null)
			addText(contentOfSecondPage, ecafVo.getPaymentVO().getPaidAmount(), PdfCoOrdinates.TOTAL_CHARGES_X,
					PdfCoOrdinates.TOTAL_CHARGES_Y);

			if(ecafVo.getPaymentVO().getPaymentMode() != null)
			{
				String paymentMode = ecafVo.getPaymentVO().getPaymentMode();

				if (paymentMode.equalsIgnoreCase(PdfCoOrdinates.ONLINE))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.ONLINE_X, PdfCoOrdinates.ONLINE_Y);

				if (paymentMode.equalsIgnoreCase(PdfCoOrdinates.CASH))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.CASH_X, PdfCoOrdinates.CASH_Y);

				if (paymentMode.equalsIgnoreCase(PdfCoOrdinates.CHEQUE))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.CHEQUE_X, PdfCoOrdinates.CHEQUE_Y);

				if (paymentMode.equalsIgnoreCase(PdfCoOrdinates.DEMAND))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.DRAFT_X, PdfCoOrdinates.DRAFT_Y);

				if (ecafVo.getCustomerSign() != null)
					addCustomerSignature(stamper, contentOfSecondPage, ecafVo.getCustomerSign(),PdfCoOrdinates.SIGN_X, PdfCoOrdinates.SIGN_Y);
			}
			

			if(ecafVo.getCreatedDate() != null)
			addText(contentOfSecondPage, ecafVo.getCreatedDate(), PdfCoOrdinates.SIGN_DATE_X,
					PdfCoOrdinates.SIGN_DATE_Y);
			if(ecafVo.getPlace() != null)
			addText(contentOfSecondPage, ecafVo.getPlace(), PdfCoOrdinates.SIGN_PLACE_X, PdfCoOrdinates.SIGN_PLACE_Y);

			if (ecafVo.getSecoundryTransactionId() != null)
				addText(contentOfSecondPage, ecafVo.getSecoundryTransactionId(), PdfCoOrdinates.SECONDRY_TX_X,
						PdfCoOrdinates.SECONDRY_TX_Y);

			if (ecafVo.getSecondaryDate() != null) {
				String[] secondaryDate = StringUtils.split(ecafVo.getSecondaryDate());

				addText(contentOfSecondPage, secondaryDate[0], PdfCoOrdinates.SECONDRY_TX_DATE_X,
						PdfCoOrdinates.SECONDRY_TX_DATE_Y);
				addText(contentOfSecondPage, secondaryDate[1], PdfCoOrdinates.SECONDRY_TX_TIME_X,
						PdfCoOrdinates.SECONDRY_TX_TIME_Y);

			}

			if(ecafVo.getTariffPlanVo().getPlanType() != null)
			{
				String planType = ecafVo.getTariffPlanVo().getPlanType();

				if (planType.equalsIgnoreCase(PdfCoOrdinates.HOME))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.HOME_X, PdfCoOrdinates.HOME_Y);

				if (planType.equalsIgnoreCase(PdfCoOrdinates.SOHO))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.SOHO_X, PdfCoOrdinates.SOHO_Y);

				if (planType.equalsIgnoreCase(PdfCoOrdinates.CORPORATE))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.CORPORATE_X, PdfCoOrdinates.CORPORATE_Y);

				if (planType.equalsIgnoreCase(PdfCoOrdinates.CYBER))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.CYBER_CAFE_X, PdfCoOrdinates.CYBER_CAFE_Y);


			}
						// enquiry id need to be add in ecaf sevice
			/*
			 * addTick(contentOfSecondPage, stamper, PdfCoOrdinates.INBOUND_X,
			 * PdfCoOrdinates.INBOUND_Y); addTick(contentOfSecondPage, stamper,
			 * PdfCoOrdinates.CALL_X, PdfCoOrdinates.CALL_Y); addTick(contentOfSecondPage,
			 * stamper, PdfCoOrdinates.REFERRAL_X, PdfCoOrdinates.REFERRAL_Y);
			 */

			if (ecafVo.getGisPojo() != null) {
				String connectionType = ecafVo.getGisPojo().getConnectionType();

				if (connectionType.equalsIgnoreCase(PdfCoOrdinates.COPPER))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.CAT5_X, PdfCoOrdinates.CAT5_Y);

				if (connectionType.equalsIgnoreCase(PdfCoOrdinates.FIBER))
					addTick(contentOfSecondPage, stamper, PdfCoOrdinates.FIBER_X, PdfCoOrdinates.FIBER_Y);

			}

			if (ecafVo.getPaymentVO().getSeName() != null)
				addText(contentOfSecondPage, ecafVo.getPaymentVO().getSeName(), PdfCoOrdinates.SE_NAME_X,
						PdfCoOrdinates.SE_NAME_Y);

			if (ecafVo.getPaymentVO().getCfeName() != null)
				addText(contentOfSecondPage, ecafVo.getPaymentVO().getCfeName(), PdfCoOrdinates.CFE_NAME_X,
						PdfCoOrdinates.CFE_NAME_Y);

		} catch (Exception e) {

			log.error("Error while rendering Data In SecondPage ::" + e.getMessage());
			e.printStackTrace();
		}
	}

	/***
	 * 
	 * @param stamper
	 * @param contentOfSecondPage
	 * @param customerSign
	 * @param x
	 * @param y
	 * @throws IOException
	 * @throws DocumentException
	 */
	private void addCustomerSignature(PdfStamper stamper, PdfContentByte contentOfSecondPage, String customerSign, int x ,int y)
			throws IOException, DocumentException {
		Image image = null;
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] imageByte = decoder.decodeBuffer(customerSign);
		ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
		BufferedImage bimg = ImageIO.read(bis);
		bis.close();
		int width = bimg.getWidth();
		int height = bimg.getHeight();

		if (width > PdfCoOrdinates.SIGN_MAX_WIDTH) {
			width = PdfCoOrdinates.SIGN_MAX_WIDTH;
		}
		if (height > PdfCoOrdinates.SIGN_MAX_HEIGHT) {
			height = PdfCoOrdinates.SIGN_MAX_HEIGHT;
		}

		if (height == PdfCoOrdinates.SIGN_MAX_HEIGHT || width == PdfCoOrdinates.SIGN_MAX_WIDTH) {

			bimg = scaleImage(width, height, imageByte, null);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bimg, "png", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			image = Image.getInstance(imageInByte);
		}

		PdfImage stream = new PdfImage(image, "", null);
		stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
		PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
		image.setDirectReference(ref.getIndirectReference());
		contentOfSecondPage.addImage(image, width, 0, 0, height, x, y);
	}

}
