package com.cupola.fwmp.util;

public interface MaterialName
{
	
	 int COPPER_CABLE=1;
	 int FIBRE_CABLE=2;
	 int BATTERY=3;
	 int CX=4;
	 int CORE_ELECTRIC_WIRE=5;
	 int FEMALE_SOCKET=6;
	 int LMS_CABINET=7;
	 int FLEXIBLE_PIPE=8;
	 int TERMINATION_TRAY=9;
	 int PATCH_CHORD=10;
	 int SFP=11;
	 int PAD_LOCK=12;
	 int RJ45_BOOTS=13;
	 int RJ45_CONNECTORS=14;
	 int FIBRETYPE=15;
	 int DRUM=16;
	 int CX_RACK=17;
	 int ROUTER=18;
	
 
	
}


