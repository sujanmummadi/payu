package com.cupola.fwmp.util;

public interface DBStatus
{
	Integer DELETED = 0;

	Integer ACTIVE = 1;

}
