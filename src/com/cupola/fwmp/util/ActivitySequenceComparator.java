package com.cupola.fwmp.util;

import java.util.Comparator;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.vo.ActivityVo;

public class ActivitySequenceComparator implements Comparator<ActivityVo>
{

	@Override
	public int compare(ActivityVo o1, ActivityVo o2)
	{
		if(o1 == null || o2 == null)
			return 1;
		if(FWMPConstant.WorkStageActivitySequence.indexOf(o1.getId()) < 0 || 
				FWMPConstant.WorkStageActivitySequence.indexOf(o2.getId()) < 0)
			return 1;
		
		return FWMPConstant.WorkStageActivitySequence.indexOf(o1.getId()) - FWMPConstant.WorkStageActivitySequence.indexOf(o2.getId());
	}

}
