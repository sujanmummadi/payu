package com.cupola.fwmp.util;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 *
 * @param <K> Key to map objects by
 * @param <T> Type of Object to cache
 * @author Charles Chappell
 */
public class ExpiringCache<K extends Object, T extends Object> implements Map<K, T>
{

    public ExpiringCache()
    {
        cacheMap = new ConcurrentHashMap<K, CachedObject<T>>();
        initialize();
    }

    public ExpiringCache(long ttl, long ato, long tiv)
    {
        this.ttl = ttl;
        this.ato = ato;
        this.tiv = tiv;

        cacheMap = new ConcurrentHashMap<K, CachedObject<T>>();
        initialize();
    }
    
    private static final Logger logger = LogManager.getLogger(ExpiringCache.class.getName());
	
   // static Logger logger = Logger.getLogger(ExpiringCache.class.getPackage().getName());
    
    public static final long DEFAULT_TIME_TO_LIVE = 60 * 6 * 60000;
    public static final long DEFAULT_ACCESS_TIMEOUT = 60 * 6 * 60000;
    public static final long DEFAULT_TIMER_INTERVAL = 60 * 6 * 60000;
    
    long ttl = DEFAULT_TIME_TO_LIVE;
    long ato = DEFAULT_ACCESS_TIMEOUT;
    long tiv = DEFAULT_TIMER_INTERVAL;
    
    boolean mChangeAccessTime = true;
    
//    LRUMap cacheMap;
    ConcurrentHashMap<K, CachedObject<T>> cacheMap;
    Timer cacheManager;

    @Override
    protected void finalize() throws Throwable
    {
        if (cacheManager != null)
        {
            cacheManager.cancel();
        }
    }

    @SuppressWarnings("unchecked")
    public Map<K, T> getMap()
    {
        HashMap<K, T> hm = new HashMap<K, T>();

        for (Entry<K,CachedObject<T>> e : cacheMap.entrySet())
        {
            hm.put(e.getKey(), e.getValue().cachedData);
        }

        return hm;
    }

    public void setTimeToLive(long milliSecs)
    {
        ttl = milliSecs;
        initialize();
    }
    /* Note:- 
    *  We have stored keep-live packet in cache & we do not require to change 
    *  access time when portal agent is accessing this object
    */
    public void changeAccessTime(boolean changeAccessTime)
    {
        mChangeAccessTime  = changeAccessTime;
    }

    public void setAccessTimeout(long milliSecs)
    {
        ato = milliSecs;
        initialize();
    }

    public void setCleaningInterval(long milliSecs) 
    {
        tiv = milliSecs;
        initialize();
    }

    public void initialize()
    {
        if (cacheManager != null)
        {
            cacheManager.cancel();
        }

        cacheManager = new Timer(true);
        
        cacheManager.schedule(new TimerTask()
                    {
                       public void run()
                       {
                            long now = System.currentTimeMillis();
                            try
                            {
                                @SuppressWarnings("unchecked")
                                Iterator<Entry<K, CachedObject<T>>> itr = cacheMap.entrySet().iterator();
                                while (itr.hasNext()) 
                                {
                                    Entry<K, CachedObject<T>> entry = itr.next();
                                    K key = entry.getKey();
                                    CachedObject<T> cobj = entry.getValue();

//                                    logger.info("ExpiringCache:- Key Found ;- " + key);
                                    if (cobj == null || cobj.hasExpired(now))
                                    {
//                                	logger.info("ExpiringCache :- Removing " + key + ": Idle time=" + (now - cobj.timeAccessedLast) + "; Stale time:" + (now - cobj.timeCached));
                                        itr.remove();
                                        Thread.yield();
                                    }
                                }
                                
                            }
                            catch (ConcurrentModificationException cme)
                            {
                                /*
                                    Ignorable.  This is just a timer cleaning up.
                                    It will catchup on cleaning next time it runs.
                                */
                        	logger.info("Ignorable ConcurrentModificationException"+ cme);
                            }
                        //   logger.info("TimerTask run()");
                        }
                    },
                    0,
                    tiv);
        }

    public int howManyObjects() {
        return cacheMap.size();
    }

    public void clear() {
        cacheMap.clear();
    }

    /**
    If the given key already maps to an existing object and the new object
    is not equal to the existing object, existing object is overwritten
    and the existing object is returned; otherwise null is returned.
    You may want to check the return value for null-ness to make sure you
    are not overwriting a previously cached object.  May be you can use a
    different key for your object if you do not intend to overwrite.
     * @param key
     * @param dataToCache
     * @return Object replaced by the just submitted object, or null if none
     */
    public T admit(K key, T dataToCache)
    {
        //cacheMap.put(key, new CachedObject<T>(dataToCache));
        //return null;

        CachedObject<T> cobj = cacheMap.get(key);
        if (cobj == null) 
        {
            cacheMap.put(key, new CachedObject<T>(dataToCache));
            return null;
        }
        else 
        {
            T obj = cobj.getCachedData(key);
            if (obj == null)
            {
                if (dataToCache == null)
                {
                    // Avoids creating unnecessary new cachedObject
                    // Number of accesses is not reset because object is the same
                    cobj.timeCached = cobj.timeAccessedLast = System.currentTimeMillis();
                    return null;
                } 
                else
                {
                    cacheMap.put(key, new CachedObject<T>(dataToCache));
                    return null;
                }
            } 
            else if (obj.equals(dataToCache))
            {
                // Avoids creating unnecessary new cachedObject
                // Number of accesses is not reset because object is the same
                cobj.timeCached = cobj.timeAccessedLast = System.currentTimeMillis();
                return null;
            }
            else
            {
                cacheMap.put(key, new CachedObject<T>(dataToCache));
                return obj;
            }
        }
    }

    public T admit(K key, T dataToCache, long objectTimeToLive, long objectIdleTimeout)
    {
        //cacheMap.put(key, new CachedObject<T>(dataToCache));
        //return null;

        CachedObject<T> cobj = cacheMap.get(key);
        if (cobj == null)
        {
            cacheMap.put(key, new CachedObject<T>(dataToCache, objectTimeToLive, objectIdleTimeout));
            return null;
        }
        else
        {
            T obj = cobj.getCachedData(key);
            if (obj == null)
            {
                if (dataToCache == null)
                {
                    // Avoids creating unnecessary new cachedObject
                    // Number of accesses is not reset because object is the same
                    cobj.timeCached = cobj.timeAccessedLast = System.currentTimeMillis();
                    cobj.objectTTL = objectTimeToLive;
                    cobj.objectIdleTimeout = objectIdleTimeout;
                    cobj.userTimeouts = true;
                    return null;
                } 
                else
                {
                    cacheMap.put(key, new CachedObject<T>(dataToCache, objectTimeToLive, objectIdleTimeout));
                    return null;
                }
            }
            else if (obj.equals(dataToCache))
            {
                // Avoids creating unnecessary new cachedObject
                // Number of accesses is not reset because object is the same
                cobj.timeCached = cobj.timeAccessedLast = System.currentTimeMillis();
                cobj.objectTTL = objectTimeToLive;
                cobj.objectIdleTimeout = objectIdleTimeout;
                cobj.userTimeouts = true;
                return null;
            } 
            else 
            {
                cacheMap.put(key, new CachedObject<T>(dataToCache, objectTimeToLive, objectIdleTimeout));
                return obj;
            }
        }
    }

    public T recover(Object key)
    {
        CachedObject<T> cobj = cacheMap.get((K)key);
        if (cobj == null)
        {
            return null;
        } 
        else 
        {
            return cobj.getCachedData(key);
        }
    }

    public void discard(Object key)
    {
        cacheMap.remove((K)key);
    }

    public long whenCached(Object key)
    {
        CachedObject<T> cobj = cacheMap.get((K)key);
        if (cobj == null)
        {
            return 0;
        }
        return cobj.timeCached;
    }

    public long whenLastAccessed(Object key)
    {
        CachedObject<T> cobj = cacheMap.get((K)key);
        if (cobj == null)
        {
            return 0;
        }
        return cobj.timeAccessedLast;
    }

    public int howManyTimesAccessed(Object key) 
    {
        CachedObject<T> cobj = cacheMap.get((K)key);
        if (cobj == null)
        {
            return 0;
        }
        return cobj.numberOfAccesses;
    }

    public int size()
    {
        return cacheMap.size();
    }

    public boolean isEmpty()
    {
        return cacheMap.isEmpty();
    }

    public boolean containsKey(Object key)
    {
        return cacheMap.containsKey((K)key);
    }

    public boolean containsValue(Object value)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public T get(Object key)
    {
        return recover(key);
    }

    public T put(K key, T value)
    {
        return admit(key, value);
    }

    public T remove(Object key)
    {
        discard(key);
        return null;
    }

    @SuppressWarnings("unchecked")
    public void putAll(Map t)
    {
        Iterator<Entry<K, T>> i = t.entrySet().iterator();
        while (i.hasNext())
        {
            Entry<? extends K, ? extends T> e = i.next();
            admit(e.getKey(), e.getValue());
        }
    }

    public Set<K> keySet()
    {
        Set<K> keys =  cacheMap.keySet();
        return keys;
    }

    public Collection<T> values()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Set<Entry<K, T>> entrySet()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * A cached object, needed to store attributes such as the last time
     * it was accessed.
     * @param <T>
     */
    protected class CachedObject<T extends Object>
    {
        T cachedData;
        long timeCached;
        long timeAccessedLast;
        int numberOfAccesses;
        long objectTTL;
        long objectIdleTimeout;
        boolean userTimeouts;

        CachedObject(T cachedData) 
        {
            long now = System.currentTimeMillis();
            this.cachedData = cachedData;
            timeCached = now;
            timeAccessedLast = now;
            ++numberOfAccesses;
            userTimeouts = false;
        }

        CachedObject(T cachedData, long timeToLive, long idleTimeout) 
        {
            long now = System.currentTimeMillis();
            this.cachedData = cachedData;
            objectTTL = timeToLive;
            objectIdleTimeout = idleTimeout;
            userTimeouts = true;
            timeCached = now;
            timeAccessedLast = now;
            ++numberOfAccesses;
        }

        T getCachedData(Object key) 
        {
            long now = System.currentTimeMillis();
            if (hasExpired(now)) 
            {
                cachedData = null;
                cacheMap.remove((K)key);
                return null;
            }
            if(mChangeAccessTime)
            {
                timeAccessedLast = now;
            }
            ++numberOfAccesses;
            return cachedData;
        }

        boolean hasExpired(long now)
        {
            long usedTTL = userTimeouts ? objectTTL : ttl;
            long usedATO = userTimeouts ? objectIdleTimeout : ato;

            if (now > timeAccessedLast + usedATO ||
                    now > timeCached + usedTTL) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
