package com.cupola.fwmp.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface MaterialConstants
{

	int Battery_SERIAL_N0 = 1003;
	int CX_MAC_ID = 1004;
	int FIBRETYPE = 1015;
	int DRUM = 1016;
	int CX_RACK = 1017;
	int COPPER_CABLE = 1001;
	int FIBER_CABLE = 1002;
	int CORE_ELECTRIC_WIRE = 1005;
	int FEMALE_SOCKET = 1006;
	int LMS_CABINET = 1007;
	int FLEXIBLE_PIPE = 1008;
	int TERMINATION_TRAY = 1009;
	int PATCH_CHORD = 1010;
	int SFP = 1011;
	int PAD_LOCK = 1012;
	int RJ45_BOOTS = 1013;
	int RJ45_CONNECTORS = 1014;
	int FIBER_TYPE = 1015;
	int DRUM_NO = 1016;
	int CX_RACK_ID = 1017;
	int ROUTER = 1018;
	int ELEMENT_ID = 1024;
	int GX_SERIAL_NUMBER = 1025;
	
	List<Integer> FR_MATERIAL= new ArrayList<Integer>(Arrays.asList(Battery_SERIAL_N0,CX_MAC_ID,FIBRETYPE,DRUM,CX_RACK,COPPER_CABLE,
			FIBER_CABLE,CORE_ELECTRIC_WIRE,FEMALE_SOCKET,LMS_CABINET,FLEXIBLE_PIPE,PATCH_CHORD,
			SFP,PAD_LOCK,RJ45_BOOTS,RJ45_CONNECTORS,CX_RACK_ID,ROUTER));
	
	String MATERIA_REQ = "REQ";
	String MATERIA_CON = "CON";
}
