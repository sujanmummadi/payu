/**
 * 
 */
package com.cupola.fwmp.util;

import javax.xml.soap.SOAPException;

import org.apache.axis.message.SOAPHeaderElement;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.service.definition.DefinitionCoreService;

/**
 * @author aditya
 *
 */
public class CrmUtil
{
	@Autowired
	DefinitionCoreService definitionCoreService;

	public SOAPHeaderElement createMqAuth() throws SOAPException
	{
		String url = definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_URI);

		SOAPHeaderElement authentication = new SOAPHeaderElement(url, "MQUserNameToken");

		SOAPHeaderElement user = new SOAPHeaderElement(url, "User_id", definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_APP_USER_NAME));

		SOAPHeaderElement password = new SOAPHeaderElement(url, "Password", definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_APP_PASSWORD));

		authentication.addChild(user);
		authentication.addChild(password);

		return authentication;
	}
}
