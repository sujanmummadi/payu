package com.cupola.fwmp.util;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.service.domain.HttpRequestInfo;
import com.cupola.fwmp.service.domain.HttpRequestInfoImpl;

public class ApplicationUtils
	
{
	private static final Logger LOGGER = Logger.getLogger (ApplicationUtils.class);
	private static  String userAgentKey = "User-Agent";
	private static ThreadLocal<PagedResultsContext> pagingContexts = new ThreadLocal<PagedResultsContext> ();
	private static ThreadLocal<Boolean> requestContext = new ThreadLocal<Boolean> ();
	private static ThreadLocal<TicketFilter> ticketFilters = new ThreadLocal<TicketFilter> ();
	private static ThreadLocal<HttpRequestInfo> httpRequestInfos = new ThreadLocal<HttpRequestInfo> ();
	public static void savePagingContext (PagedResultsContext pageCtx)
	{
		pagingContexts.set (pageCtx);
	}

	public static PagedResultsContext retrievePagingContext ()
	{
		return (PagedResultsContext) pagingContexts.get ();
	}
	
	public static void saveTicketFilter (TicketFilter filter)
	{
		ticketFilters.set (filter);
	}public static Boolean isAppRequest ()
	{
		return requestContext.get();
	}

	public static void saveRequestContext (Boolean flag)
	{
		requestContext.set (flag);
	}

	public static TicketFilter retrieveTicketFilter ()
	{
		return (TicketFilter) ticketFilters.get ();
	}
	public static  void removeTicketFilter()
	{
		ticketFilters.remove();
	}
	
	public static HttpRequestInfo saveHttpRequestInfo (HttpServletRequest request)
	{
		HttpRequestInfoImpl reqInfo = new HttpRequestInfoImpl ();
		reqInfo.setContextPath (request.getContextPath ());
		reqInfo.setPathInfo (request.getPathInfo ());
		reqInfo.setRemoteHost (request.getRemoteHost ());
		reqInfo.setRemoteAddr (request.getRemoteAddr ());
		// Get only the already established session (won't create one if one
		// isn't already established)
		boolean isMobileDevice = isRequestFromMobileDevice(request);
		saveRequestContext(isMobileDevice);
		HttpSession session = request.getSession (false);
		if (session != null) {
			session.setAttribute("isMobileDevice", isMobileDevice);
			reqInfo.setRequestedSessionId (request.getRequestedSessionId ());
			reqInfo.setRequestedSessionIdValid (request.isRequestedSessionIdValid ());
			reqInfo.setHttpSession (session);
		}
		reqInfo.setRequestSecure (request.isSecure ());
		reqInfo.setRequestURI (request.getRequestURI ());
		reqInfo.setRequestURL (request.getRequestURL ().toString ());
		reqInfo.setBaseUrl (reqInfo.getRequestURL ().substring (0,
			reqInfo.getRequestURL ().length () - reqInfo.getRequestURI ().length ()));
		reqInfo.setRequestedFromMobile(isMobileDevice);
		httpRequestInfos.set (reqInfo);
		return reqInfo;
	}

	public static HttpRequestInfo retrieveHttpRequestInfo ()
	{
		return httpRequestInfos.get ();
	}
	
	private static boolean isRequestFromMobileDevice(HttpServletRequest request)
	{
		String andriodStr1 =  "dalvik";
		String andriodStr2 =  "android";
		String andriodStr3 =  "art";
		String andriodStr4 =  "apache-httpclient";
		 HttpServletRequest httpRequest = (HttpServletRequest) request;
		 
		 
	        Enumeration<String> headerNames = httpRequest.getHeaderNames();
	        if (headerNames != null) {
	        	String value = httpRequest.getHeader(userAgentKey);
	        	
	        	if(value != null &&(value.toLowerCase().indexOf(andriodStr2) >=0 ||
	        			value.toLowerCase().indexOf(andriodStr1) >=0 ||
	        					value.toLowerCase().indexOf(andriodStr4) >=0 ||
	        			value.toLowerCase().indexOf(andriodStr3) >=0 ))
	        	{
	        		return true;
	        	}
	        }
			return false;
		
	}

}
