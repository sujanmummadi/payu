package com.cupola.fwmp.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class FieldValidatorUtil
{
	private static Logger LOGGER = Logger.getLogger(FieldValidatorUtil.class.getName());
	private static final String IPADDRESS_PATTERN =
				"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	 
	 private static Pattern compilePattern( final String PATTERN )
	 {
		 if( PATTERN == null || PATTERN.isEmpty() )
			 return null;
		 
		 return Pattern.compile(PATTERN);
	 }

	  /**
	   * Validate ip address with regular expression.
	   * @param ip ip address for validation.
	   * @return true valid ip address, false invalid ip address.
	   */
	  public static boolean isValidIp( final String IP )
	  {
		  LOGGER.info("************* Validating IP **************** "+IP);
		  if( IP == null || IP.isEmpty() )
			  return false;
		  
		  try 
		  {
			  Pattern pattern = compilePattern( IPADDRESS_PATTERN );
			  if( pattern == null )
				  return false;
			  
			  Matcher  matcher = pattern.matcher(IP);
			  return matcher.matches();
		  } 
		  catch (Exception e) 
		  {
			  e.printStackTrace();
			  LOGGER.error("Error while validating IP address in FieldValidatorUtil isValidIp(IP)");
			  return false;
		  }
	  }
	  
}
