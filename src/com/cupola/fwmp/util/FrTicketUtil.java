package com.cupola.fwmp.util;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.configuration.PropertyConverter;
import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.cupola.fwmp.FWMPConstant.ClassificationCategories;
import com.cupola.fwmp.dao.fr.FrDeploymentSettingDaoImpl;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.persistance.entities.FrDeploymentSetting;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.vo.TypeAheadVo;

/**
 * 
 * @author G Ashraf
 * 
 * */

public class FrTicketUtil
{
	private static Logger LOGGER = Logger.getLogger(FrTicketUtil.class);
	
	private static final String DB_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String UI_DATE_FORMAT = "dd/MM/yyyy hh:mm:ss";
	
	static String customerFormatPropValue = "dd/MM/YYYY";
	private static String baseKeyPrefix = "afp.fr.ticket.unlock.size.";
	
	private static Map<String,Long> symptomNameToFlowIdMapping = null;

	private static DefinitionCoreService definitionCoreService;
	private static FrTicketDao frDao;
	private static DBUtil dbUtil;
	private static GlobalActivities globalActivities;
	
	private static Map<String ,List<Long>> fxNameAndFlowMap = new HashMap<String ,List<Long>>();
	
	static{
		LOGGER.info("Initializing symptom name flow id mapping map...");
		symptomNameToFlowIdMapping = getMapObjectOfSymptomName();
	}
	

	public static boolean isVicinityTicket( String fxName, Long flowId )
	{
		if( fxName == null || fxName.isEmpty() 
				|| flowId == null || flowId.longValue() <= -1 )
			
			return false;
		
		else 
		{
			List<Long> value = null;
			if( fxNameAndFlowMap != null && fxNameAndFlowMap.containsKey( fxName ) ) 
			{
				value = fxNameAndFlowMap.get(fxName);
				if( value != null && value.contains(flowId) )
					return true;
				else
				{
					value = new ArrayList<Long>();
					value.add(flowId);
					fxNameAndFlowMap.put(fxName, value);
					return false;
				}
			}
			else
			{
				value = new ArrayList<Long>();
				value.add(flowId);
				fxNameAndFlowMap.put(fxName, value);
				return false;
			}
		}
	}
	
	public static Integer reloadFlowIdSymptomMapping()
	{
		int result = 0 ;
		symptomNameToFlowIdMapping = getMapObjectOfSymptomName();
		
		if( symptomNameToFlowIdMapping != null 
				&& !symptomNameToFlowIdMapping.isEmpty())
			result++;
		
		return result;
	}
	
	public static Long getUserIdFromKey(String key)
	{
		Long userId =  null;
		try{
			String[] value = key.split("_");
			userId = Long.valueOf(value[value.length-1]);
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return userId;
	}
	
	public static Long getUnlockLimitByCity()
	{
		TypeAheadVo userCity = AuthUtils.getCurrentUserCity();
		String count = null;
		if(userCity != null)
			count =  definitionCoreService.getPropertyValue(baseKeyPrefix +userCity.getName().toLowerCase());
		else if(count == null)
			 count =  definitionCoreService.getPropertyValue(baseKeyPrefix +"default");
		return Long.valueOf(count.trim());
		 
	}
	
	private static Map<String,Long> getMapObjectOfSymptomName()
	{
		LOGGER.info("Reading properties from Db for initialization of map ");
		
		Map<String,Long> frSymptomName = new ConcurrentHashMap<String, Long>();
		try{
			
			for(Integer key : FrDeploymentSettingDaoImpl.frFlowNamesAndFlowIdMap.keySet()){
				List<String> listSymptoms = FrDeploymentSettingDaoImpl.frFlowNamesAndFlowIdMap.get(key);
				for(String mapKeyValue : listSymptoms){
					Long mapValue = key.longValue();
					if(mapKeyValue.trim().length() > 0)
						frSymptomName.put(mapKeyValue, mapValue);
				}
			}
			
			
		}catch(Exception e){
			LOGGER.error("Error occure while reading properties file ",e);
		}
		Log.info("Fr TIcket Util frSymptomName map size:"+frSymptomName.size());
		return frSymptomName;
	}
	
	private static List<String> getListOfMsg(String msgWithCommaSeparated)
	{
		List<String> list = null;
		if(msgWithCommaSeparated != null)
		{
			list = PropertyConverter.split(msgWithCommaSeparated, ',');
			return list;
		}
		return null;
	}
	
	public static Long findFlowIdforMatchingSymptomName(String symptomName)
	{
		if(symptomName == null)
			return ClassificationCategories.FLOW_ID_DISCARD;
		
		if(symptomNameToFlowIdMapping == null || symptomNameToFlowIdMapping.isEmpty())
			symptomNameToFlowIdMapping = getMapObjectOfSymptomName();
		
		Long flowId;
		try{
			flowId = symptomNameToFlowIdMapping.get(symptomName);
			
			/*if( flowId == null && symptomName.contains("Fiber Cut") 
					&& symptomName.contains("Slow Issue"))
			{
				symptomName = "Fiber Cut-Slow Issue";
				return symptomNameToFlowIdMapping.get(symptomName);
			}*/
			
		}catch(NullPointerException e){
			LOGGER.info("Found unknown symptom name : "+symptomName);
			flowId = ClassificationCategories.FLOW_ID_DISCARD;
		}
		return flowId;
	}
	
	public static boolean isTicketHavingFrSymptom(String symptom)
	{
		if(symptomNameToFlowIdMapping == null || symptomNameToFlowIdMapping.isEmpty())
			symptomNameToFlowIdMapping = getMapObjectOfSymptomName();
		
		return symptomNameToFlowIdMapping.containsKey(symptom);
	}
	
	public static List<TypeAheadVo> getFrSymptomCategories()
	{
		return CommonUtil.xformToTypeAheadFormat(
				ClassificationCategories.symptomFlowIdMap);
	}
	
	public static Map<Long, String> getFrFlowTypeForFilter()
	{
		Map<Long, String> symptomFlowIdMap = new HashMap<>(ClassificationCategories.symptomFlowIdMap);
		symptomFlowIdMap.remove(ClassificationCategories.FLOW_ID_NONE);
		symptomFlowIdMap.remove(ClassificationCategories.FLOW_ID_DISCARD);
		return symptomFlowIdMap;
	}
	
	public static Set<String> getAllFrSymptomsName()
	{
		if(symptomNameToFlowIdMapping == null || symptomNameToFlowIdMapping.isEmpty())
			symptomNameToFlowIdMapping = getMapObjectOfSymptomName();
		
		return symptomNameToFlowIdMapping.keySet();
	}
	
	public static String findSymptomCategoryForFlowId(Long flowId)
	{
		if( flowId == null || flowId < 0 )
			return ClassificationCategories
					.symptomFlowIdMap.get(ClassificationCategories.FLOW_ID_NONE);
		
		return ClassificationCategories.symptomFlowIdMap.get(flowId);
	}
	
	public static List<Long> convertStatusStringToLong(String commaSeparatedValue)
	{
		List<Long> values = new ArrayList<Long>();
		if( commaSeparatedValue == null )
			return values;
		else if( commaSeparatedValue != null 
				&& commaSeparatedValue.trim().length() <= 0)
			return values;
		
		try{
			String[] status = commaSeparatedValue.split(",");
			for( String value : status)
				if(value.trim().length() > 0)
					values.add(Long.valueOf(value));
		}catch(Exception exception){
			LOGGER.error("Exception occure while converting into Long ...",exception);
		}
		return values;
	}
	
	public static APIResponse getFrUserQData( Long userId ) 
	{
		if( userId == null || userId.longValue() <= 0 )
			return ResponseUtil.nullArgument().setData("Invalid user Id : "+userId);
		
		String keyValue = dbUtil.getKeyForQueueByUserId(userId);
		LOGGER.info("User Q Key : "+keyValue);
		Set<Long> QData = globalActivities.getAllFrTicketFromMainQueueByKey( keyValue );
		
		return ResponseUtil.createSuccessResponse().setData(QData);
	}

	public static String getFullName(String firstName, String middleName, String lastName )
	{
		if( middleName != null && !middleName.isEmpty())
			firstName = firstName +" "+middleName;
		
		if( lastName != null && !lastName.isEmpty())
			firstName = firstName +" "+lastName;
		
		return firstName;
	}
	
	public static String convertDbDateToUiStrngDate(Date date)
	{
		if( date == null)
			return null;
		
		SimpleDateFormat format = new SimpleDateFormat(UI_DATE_FORMAT);
		String string = format.format(date.getTime());
		return string;
	}
	
	public static Date convertStringToDbDate(String string)
	{
		if( string == null)
			return null;
		
		SimpleDateFormat format = new SimpleDateFormat(DB_DATE_FORMAT);
		Date date = null;
		try
		{
			date = format.parse(string);
		} 
		catch (ParseException e)
		{
			LOGGER.error("Error occure while parsing string to date  : ",e);
		}
		return date;
	}
	
	public static String convertDbDateToDbStringDate(Date dbDate)
	{
		if(dbDate == null)
			return null;
		
		SimpleDateFormat format = new SimpleDateFormat(DB_DATE_FORMAT);
		String string = format.format(dbDate.getTime());
		
		return string;
	}
	
	public static Long objectToLong(Object obj)
	{
		if(obj == null)
			return 0l;
		return Long.valueOf(obj.toString());
	}
	
	public static Integer objectToInteger(Object obj)
	{
		if(obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}
	
	public static String objectToString(Object obj)
	{
		if(obj == null)
			return null;
		return obj.toString();
	}
	
	public static Integer objectToInt(Object obj)
	{
		if(obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}
	
	public static boolean objectToBoolean(Object value)
	{
		if(value == null)return false;
		int val = Integer.valueOf(value.toString());
		if( val == 1)
			return true;
		else
			return false;
	}
	
	public static Date convertObjectToDate(Object obj)
	{
		Date formattedDateInSiebel = null;
		SimpleDateFormat comminicationFormatter = new SimpleDateFormat(UI_DATE_FORMAT);
		
		try {

			SimpleDateFormat dbFormatDateETR = new SimpleDateFormat(DB_DATE_FORMAT);

			String date = dbFormatDateETR.format(obj);

			formattedDateInSiebel = comminicationFormatter.parse (date.toString());

			return formattedDateInSiebel;

		} catch (Exception e) {
			
			try {
				formattedDateInSiebel = comminicationFormatter.parse(GenericUtil.getDateBy5Days(new Date()));
				
			} catch (ParseException e1) {
				
				formattedDateInSiebel = new Date(GenericUtil.getDateBy5Days(new Date()));
			}
			
			return formattedDateInSiebel;
		}
	}
	
	public static Date objectToDbDate(Object obj)
	{
		if( obj == null)
			return null;
		
		String dateString = FrTicketUtil.objectToString(obj);
		Date date = FrTicketUtil.convertStringToDbDate(dateString);
		
		return date ;
	}
	
	

	public static void setFrDao(FrTicketDao frDao) {
		FrTicketUtil.frDao = frDao;
	}

	public static void setDbUtil(DBUtil dbUtil) {
		FrTicketUtil.dbUtil = dbUtil;
	}

	public static DefinitionCoreService getDefinitionCoreService() {
		return definitionCoreService;
	}

	public static void setDefinitionCoreService(
			DefinitionCoreService definitionCoreService) {
		FrTicketUtil.definitionCoreService = definitionCoreService;
	}

	public static void setGlobalActivities(GlobalActivities globalActivities) {
		FrTicketUtil.globalActivities = globalActivities;
	}


}