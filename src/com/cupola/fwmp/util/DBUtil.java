package com.cupola.fwmp.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.WorkOrderVo;

public class DBUtil
{
	private static final Logger log = LogManager
			.getLogger(DBUtil.class.getName());

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	UserDao userDao;
	@Autowired
	GlobalActivities globalActivities;
	
	@Autowired
	DefinitionCoreService definitionCoreService;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{

		this.jdbcTemplate = jdbcTemplate;

	}

	private static String TICKET_HAS_WORKORDER_BASE_QUERY = "SELECT initialWorkStage FROM WorkOrder where ticketId =?";

	public boolean isTicketAvailable(Long ticketId,
			Long requesterId)
	{
		try
		{
			/*Long currentUser = jdbcTemplate
					.queryForLong("select currentAssignedTo from Ticket where id="
							+ ticketId);*/
			
			Long currentUser = null;

			if( AuthUtils.isFRUser() )
				currentUser = jdbcTemplate
					.queryForObject("select currentAssignedTo from FrTicket where id="
							+ ticketId,Long.class);
			else
				currentUser = jdbcTemplate
					.queryForLong("select currentAssignedTo from Ticket where id="
							+ ticketId);
				
			if (requesterId.equals(currentUser))
				return true;
			else
				return false;

		} catch (Exception e)
		{
			log.error("Error while getting isCurrentAssignedSameAsReuester for ticket "
					+ ticketId + " and requesterId " + requesterId + " "
					+ e.getMessage());
			return false;
		}
	}

	public Boolean ticketIDHasWorkOrder(long ticketId)
	{
		if (ticketId > 0)
		{
			List<WorkOrderVo> workOrderVOs = jdbcTemplate
					.query(TICKET_HAS_WORKORDER_BASE_QUERY, new Object[] {
							ticketId }, new WorkOrderMapper());

			if (workOrderVOs != null)
				return true;
			else
				return false;
		} else
			return false;
	}

	public String getKeyForQueueByUserId(Long userLoginId)
	{
		String fullyQualifiedNameKey = null;

		UserVo usrvo = userDao.getUserById(userLoginId);
//		String areaId = "UNKNOWN";
		String branchId = "UNKNOWN";
		String cityId = "UNKNOWN";

		if (usrvo != null)
		{
//			if (usrvo.getArea() != null && usrvo.getArea().getId() != null)
//				areaId = usrvo.getArea().getId().toString();

			if (usrvo.getBranch() != null && usrvo.getBranch().getId() != null)
				branchId = usrvo.getBranch().getId().toString();

			if (usrvo.getCity() != null && usrvo.getCity().getId() != null)
				cityId = usrvo.getCity().getId().toString();

			fullyQualifiedNameKey = cityId + "_" + branchId +/* "_" + areaId
					+*/ "_" + userLoginId;
		} else
		{
			fullyQualifiedNameKey = cityId + "_" + branchId + "_" /*+ areaId
					+ "_"*/ + userLoginId;
		}
		
		return fullyQualifiedNameKey;
	}

	public boolean removeOneTicketFromQueue(Long userId, Long ticketId)
	{

		String sourceKey = getKeyForQueueByUserId(userId);

		boolean result = globalActivities
				.removeTicketFromMainQueueByKeyAndTicketNumber(sourceKey, ticketId);

		long reportTo = userDao.getReportToUser(userId);

		if (reportTo > 0)
		{
			String tlKey = getKeyForQueueByUserId(userId);

			globalActivities
					.removeTicketFromMainQueueByKeyAndTicketNumber(tlKey, ticketId);

		}

		if (result)
			return true;
		else
			return false;

	}

	public void addOneTicketToQueue(Long userId, Long ticketId)
	{
		LinkedHashSet<Long> ticketIds = new LinkedHashSet<>();
		ticketIds.add(ticketId);
		
		String sourceKey = getKeyForQueueByUserId(userId);

//		System.out.println("Before Adding for SE "
//				+ globalActivities.getMainQueue().get(sourceKey));
		
		globalActivities.addTicket2MainQueue(sourceKey, ticketIds);
		
//		System.out.println("After Adding for SE "
//				+ globalActivities.getMainQueue().get(sourceKey));
		
		long reportTo = userDao.getReportToUser(userId);

		if (reportTo > 0)
		{
			String tlKey = getKeyForQueueByUserId(userId);
			
//			System.out.println("Before Adding for SE TL "
//					+ globalActivities.getMainQueue().get(tlKey));
			
			globalActivities.addTicket2MainQueue(tlKey, ticketIds);
			
//			System.out.println("After Adding for SE TL "
//					+ globalActivities.getMainQueue().get(tlKey));

		}
	}
	
	public void addOneFrTicketToQueue(Long userId, Long ticketId)
	{
		LinkedHashSet<Long> ticketIds = new LinkedHashSet<>();
		ticketIds.add(ticketId);
		
		String sourceKey = getKeyForQueueByUserId(userId);

		globalActivities.addFrTicket2MainQueue(sourceKey, ticketIds);
		
		long reportTo = userDao.getReportToUser(userId);
		if (reportTo > 0)
		{
			String tlKey = getKeyForQueueByUserId(userId);
			globalActivities.addFrTicket2MainQueue(tlKey, ticketIds);
		}
	}
	
	public boolean removeOneFrTicketFromQueue(Long userId, Long ticketId)
	{

		String sourceKey = getKeyForQueueByUserId(userId);

		boolean result = globalActivities
				.removeFrTicketFromMainQueueByKeyAndTicketNumber(sourceKey, ticketId);

		long reportTo = userDao.getReportToUser(userId);
		if (reportTo > 0)
		{
			String tlKey = getKeyForQueueByUserId(userId);
			globalActivities
					.removeFrTicketFromMainQueueByKeyAndTicketNumber(tlKey, ticketId);
		}

		if (result)
			return true;
		else
			return false;

	}

	public String getCurrentCrmName(String prospectNo, String workOrderNo) {

		log.info("Getting CRM for prospect " + prospectNo + " or workOrderNo " + workOrderNo);

		String crmName = null;

		if (prospectNo == null || prospectNo.isEmpty()) {
		
			log.info("All Values are null from CRM for prospect " + prospectNo + " or workOrderNo " + workOrderNo);

			if (workOrderNo == null || workOrderNo.isEmpty()) {

				// get crm name from properties file
				crmName = definitionCoreService.getDeploymentPropertyValue(DefinitionCoreService.CURRENT_APPLICATION_CRM_NAME);
			
			} else {

				// get branch from work order and check the crm name
			}

		} else {
			
			log.info("All Values are not null from CRM for prospect " + prospectNo + " or workOrderNo " + workOrderNo);

			// get branch from prospect and check the crm name
		}

		log.info("Got CRM " + crmName + " for prospect " + prospectNo + " or workOrderNo " + workOrderNo);

		return crmName;
	}

	
	public Long getFrTicketIdByWorkOrder(String workOrder)
	{
		return jdbcTemplate.queryForObject("select id from FrTicket where workOrderNumber = " + workOrder, Long.class);
	}
}

class WorkOrderMapper implements RowMapper<WorkOrderVo>
{

	@Override
	public WorkOrderVo mapRow(ResultSet resultSet, int rowNumber)
			throws SQLException
	{

		WorkOrderVo workOrder = new WorkOrderVo();

		workOrder.setInitialWorkStageId(resultSet.getLong("initialWorkStage"));

		return workOrder;
	}
}
