/**
 * 
 */
package com.cupola.fwmp.util;

import com.cupola.fwmp.response.APIResponse;

/**
 * @author aditya
 * 
 */
public class ResponseUtil {
	private ResponseUtil() {

	}

	public static APIResponse recordFoundSucessFully() {
		return new APIResponse(StatusCodes.RECORD_FOUND_SUCCESSFULLY, StatusMessages.RECORD_FOUND_SUCCESSFULLY);
	}

	public static APIResponse ticketClosedSucessFully() {
		return new APIResponse(StatusCodes.TICKET_CLOSED_SUCCESSFULLY, StatusMessages.TICKET_CLOSED_SUCCESSFULLY);
	}

	public static APIResponse ticketCloseExceptionResponse() {
		return new APIResponse(StatusCodes.CLOSE_TICKET_FAILED, StatusMessages.CLOSE_TICKET_FAILED);
	}

	public static APIResponse noResponseFromMqServer() {
		return new APIResponse(StatusCodes.NO_RESPONSE_FROM_MQ_SERVER, StatusMessages.NO_RESPONSE_FROM_MQ_SERVER);
	}

	public static APIResponse createMqServerTicketCtreationFailed() {
		return new APIResponse(StatusCodes.MQ_SERVER_TICKET_CREATION_FAILED,
				StatusMessages.MQ_SERVER_TICKET_CREATION_FAILED);
	}

	public static APIResponse createMqServerTicketCtreationSuccess(String prospectNumber) {
		return new APIResponse(StatusCodes.MQ_SERVER_TICKET_CREATION_SUCCESS,
				StatusMessages.MQ_SERVER_TICKET_CREATION_SUCCESS.replace("_PROSPECT_", prospectNumber));
	}

	public static APIResponse createDBExceptionResponse() {
		return new APIResponse(StatusCodes.MONGO_CONNECTION_FAILED, StatusMessages.MONGO_CONNECTION_FAILED);
	}

	public static APIResponse createBadCredentialsExceptionResponse() {
		return new APIResponse(StatusCodes.BAD_CREDENTIALS, StatusMessages.BAD_CREDENTIALS);
	}
	
	// added by manjuprasad
	
	public static APIResponse defaultAreaMappingResponse(String result)
	{
		return new APIResponse(StatusCodes.SAVED_SUCCESSFULLY, StatusMessages.SAVED_SUCCESSFULLY,result);
	}

	public static APIResponse multipleRecordFound() {
		return new APIResponse(StatusCodes.MORE_THAN_ONE_RECORD, StatusMessages.MORE_THAN_ONE_RECORD);
	}

	public static APIResponse NoRecordFound() {
		return new APIResponse(StatusCodes.NO_RECORD_FOUND, StatusMessages.NO_RECORD_FOUND);
	}

	public static APIResponse databaseUpdateException() {
		return new APIResponse(StatusCodes.DATABASE_RECORD_UPDATE_FAILED, StatusMessages.DATABASE_RECORD_UPDATE_FAILED);
	}

	public static APIResponse databaseUpdateSuccess() {
		return new APIResponse(StatusCodes.DATABASE_RECORD_UPDATE_SUCESSFULL,
				StatusMessages.DATABASE_RECORD_UPDATE_SUCESSFULL);
	}

	public static APIResponse multipleRecordUpdated() {
		return new APIResponse(StatusCodes.MULTIPLE_RECORD_UPDATED, StatusMessages.MULTIPLE_RECORD_UPDATED);
	}

	public static APIResponse noRecordUpdated() {
		return new APIResponse(StatusCodes.NO_RECORD_UPDATED, StatusMessages.NO_RECORD_UPDATED);
	}

	public static APIResponse recordAddedSucessfully() {
		return new APIResponse(StatusCodes.RECORD_ADD_SUCESSFUL, StatusMessages.RECORD_ADD_SUCESSFUL);
	}

	public static APIResponse recordDeleteSucessful() {
		return new APIResponse(StatusCodes.RECORD_DELETE_SUCESSFUL, StatusMessages.RECORD_DELETE_SUCESSFUL);
	}

	public static APIResponse nullArgument() {
		return new APIResponse(StatusCodes.NULL_ARGUMENT_PASSED, StatusMessages.NULL_ARGUMENT_PASSED);
	}

	public static APIResponse createSaveSuccessResponse() {
		return new APIResponse(StatusCodes.SAVED_SUCCESSFULLY, StatusMessages.SAVED_SUCCESSFULLY);
	}

	public static APIResponse createSaveFailedResponse() {
		return new APIResponse(StatusCodes.INSERTION_FAILED, StatusMessages.INSERTION_FAILED);
	}

	public static APIResponse createNullParameterResponse() {
		return new APIResponse(StatusCodes.INPUT_NULL, StatusMessages.NULL_MASSAGE);
	}

	public static APIResponse createNullParameterResponse(String message) {
		return new APIResponse(StatusCodes.INPUT_NULL, message);
	}

	public static APIResponse createRecordNotFoundResponse() {
		return new APIResponse(StatusCodes.NO_RECORD_FOUND, StatusMessages.NO_RECORD_FOUND);
	}

	public static APIResponse createUpdateSuccessResponse() {
		return new APIResponse(StatusCodes.UPDATED_SUCCESSFULLY, StatusMessages.UPDATED_SUCCESSFULLY);
	}

	public static APIResponse createUpdateFailedResponse() {
		return new APIResponse(StatusCodes.UPDATE_FAILED, StatusMessages.UPDATE_FAILED);
	}

	public static APIResponse createSuccessResponse() {
		return new APIResponse(StatusCodes.SUCCESS, StatusMessages.SUCCESS);
	}

	public static APIResponse createDuplicateResponse() {
		return new APIResponse(StatusCodes.DUPLICATE_RECORD, StatusMessages.DUPLICATE_RECORD);
	}

	public static APIResponse createServerErrorResponse() {
		return new APIResponse(StatusCodes.SERVER_ERROR, StatusMessages.SERVER_ERROR);
	}

	public static APIResponse createDeleteSuccessResponse() {
		return new APIResponse(StatusCodes.DELETE_SUCCESSFULLY, StatusMessages.DELETED_SUCCESSFULLY);
	}

	public static APIResponse createDeleteFailedResponse() {
		return new APIResponse(StatusCodes.DELETE_FAILED, StatusMessages.DELETE_FAILED);
	}

	public static APIResponse createClusterOutSideZoneResponse() {
		return new APIResponse(StatusCodes.CLUSTER_OUTSIDE_ZONE, StatusMessages.CLUSTER_OUTSIDE_ZONE);
	}

	public static APIResponse createClusterOverLapResponse(String clusterName) {
		return new APIResponse(StatusCodes.CLUSTER_OVERLAP, StatusMessages.CLUSTER_OVERLAP + " " + clusterName);
	}

	public static APIResponse createZoneOverLapResponse(String zoneName) {
		return new APIResponse(StatusCodes.ZONE_OVERLAP, StatusMessages.ZONE_OVERLAP + " " + zoneName);
	}

	public static APIResponse deviceTaggingDetailsNotAvailable() {
		return new APIResponse(StatusCodes.DEVICE_TAGGING_DETAILS_NOT_AVAILABLE,
				StatusMessages.DEVICE_TAGGING_DETAILS_NOT_AVAILABLE);
	}

	public static APIResponse createDeleteFailedResponseForDeleteZone() {
		return new APIResponse(StatusCodes.DELETE_FAILED_ZONE_CONTAINS_CLUSTER,
				StatusMessages.DELETE_FAILED_ZONE_CONTAINS_CLUSTER);

	}

	public static APIResponse createDeleteFailedResponseForDeleteCluster() {
		return new APIResponse(StatusCodes.DELETE_FAILED_CLUSTER_CONTAINS_HUB,
				StatusMessages.DELETE_FAILED_ZONE_CONTAINS_CLUSTER);
	}

	public static APIResponse createUserAssociatedWithTabTrueResponse() {
		return new APIResponse(StatusCodes.USER_ASSOCIATED_WITH_TAB_TRUE, StatusMessages.USER_ASSOCIATED_WITH_TAB_TRUE);
	}

	public static APIResponse createUserAssociatedWithTabFalseResponse() {
		return new APIResponse(StatusCodes.USER_ASSOCIATED_WITH_TAB_FALSE,
				StatusMessages.USER_ASSOCIATED_WITH_TAB_FALSE);

	}

	public static APIResponse createUploadSuccessResponse() {
		return new APIResponse(StatusCodes.IMAGE_UPLOAD_SUCCESS, StatusMessages.IMAGE_UPLOAD_SUCCESS);
	}

	public static APIResponse createUploadFailsResponse() {
		return new APIResponse(StatusCodes.IMAGE_UPLOAD_FAIL, StatusMessages.IMAGE_UPLOAD_FAIL);
	}

	public static APIResponse createLoginFailureResponse(String message) {
		return new APIResponse(StatusCodes.AUTHENTICATION_FAILED, message);
	}

	public static APIResponse createFailureResponse() {
		return new APIResponse(StatusCodes.FAILURE, StatusMessages.FAILURE);
	}

	public static APIResponse createFeasibleSuccessResponse() {
		return new APIResponse(StatusCodes.SUCCESS, StatusMessages.SUCCESS);
	}

	public static APIResponse createFeasibleFailureResponse() {
		return new APIResponse(StatusCodes.FAILURE, StatusMessages.FAILURE);
	}

	public static APIResponse createDeviceAssociatedWithOtherUserResponse() {
		return new APIResponse(StatusCodes.DEVICE_ASSOCIATED_WITH_OTHER_USER,
				StatusMessages.DEVICE_ASSOCIATED_WITH_OTHER_USER);
	}

	public static APIResponse createUnAutorizedFailureResponse() {
		return new APIResponse(StatusCodes.AUTHENTICATION_FAILED, StatusMessages.UNAUTHORISED_FAILED);
	}

	public static APIResponse createAssignFailureResponse() {
		return new APIResponse(StatusCodes.SUCCESS, StatusMessages.UNAUTHORISED_FAILED);
	}

	public static APIResponse createTicketRemoveFromQueueSuccessResponse() {
		return new APIResponse(StatusCodes.SUCCESS, StatusMessages.TICKET_REMOVE_FROM_QUEUE_SUCCESS);
	}

	public static APIResponse createTicketRemoveFromQueueFailResponse() {
		return new APIResponse(StatusCodes.FAILURE, StatusMessages.TICKET_REMOVE_FROM_QUEUE_FAILED);
	}

	public static APIResponse createETRSuccessResponse() {
		return new APIResponse(StatusCodes.ETR_SUCCESS, StatusMessages.ETR_SUCCESS);
	}

	public static APIResponse createETRLimitREachedResponse() {
		return new APIResponse(StatusCodes.ETR_LIMIT_REACHED, StatusMessages.ETR_LIMIT_REACHED);
	}

	public static APIResponse createSuccessRollBack() {
		return new APIResponse(StatusCodes.SUCCESS_ROLL_BACK, StatusMessages.SUCCESS_ROLL_BACK);
	}

	public static APIResponse createFailureRollBack() {
		return new APIResponse(StatusCodes.NOT_ROLL_BACK_YET, StatusMessages.NOT_ROLL_BACK_YET);
	}

	public static APIResponse createTicketClosureFailedInMQ() {
		return new APIResponse(StatusCodes.MQ_SERVER_TICKET_CLOSURE_FAILED,
				StatusMessages.MQ_SERVER_TICKET_CLOSURE_FAILED);
	}

	public static APIResponse createTicketClosureSuccessInMQ() {
		return new APIResponse(StatusCodes.MQ_SERVER_TICKET_CLOSURE_SUCCESS,
				StatusMessages.MQ_SERVER_TICKET_CLOSURE_SUCCESS);
	}

	public static APIResponse createFrClosureFailedInMQ() {
		return new APIResponse(StatusCodes.FR_MQ_CLOSURE_FAILED, StatusMessages.MQ_SERVER_TICKET_CLOSURE_FAILED);
	}

	public static APIResponse createFrClosureSuccessInMQ() {
		return new APIResponse(StatusCodes.FRM_MQ_CLOSURE_SUCCESS, StatusMessages.MQ_SERVER_TICKET_CLOSURE_SUCCESS);
	}

	public static APIResponse createPortalPageNotReachable() {
		return new APIResponse(StatusCodes.PORTAL_PAGE_NOT_REACHABLE, StatusMessages.PORTAL_PAGE_NOT_REACHABLE);
	}

	public static APIResponse createInvalidReponse() {
		return new APIResponse(StatusCodes.INVALID_REPONSE, StatusMessages.INVALID_REPONSE);
	}

	public static APIResponse createInvalidActionType() {
		return new APIResponse(StatusCodes.INVALID_CUSTOMER_APP_ACTION, StatusMessages.INVALID_CUSTOMER_APP_ACTION);
	}

	public static APIResponse createNullProspectNumberFromCustomerApp() {
		return new APIResponse(StatusCodes.PROSPECT_NUMBER_NULL_FROM_CUSTOMER_APP,
				StatusMessages.PROSPECT_NUMBER_NULL_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoConnectionTypeFromCustomerApp() {
		return new APIResponse(StatusCodes.CONENCTION_TYPE_NOT_PROVIDED_BY_CUSTOMER_APP,
				StatusMessages.CONENCTION_TYPE_NOT_PROVIDED_BY_CUSTOMER_APP);
	}

	public static APIResponse createPaymentNotApproved() {
		return new APIResponse(StatusCodes.PAYMENT_NOT_APPROVED, StatusMessages.PAYMENT_NOT_APPROVED);
	}

	public static APIResponse createDocumentNotApproved() {
		return new APIResponse(StatusCodes.DOCUMENT_NOT_APPROVED, StatusMessages.DOCUMENT_NOT_APPROVED);
	}

	public static APIResponse createPOADocumentNotApproved() {
		return new APIResponse(StatusCodes.POA_DOCUMENT_NOT_APPROVED, StatusMessages.POA_DOCUMENT_NOT_APPROVED);
	}

	public static APIResponse createPOIDocumentNotApproved() {
		return new APIResponse(StatusCodes.POI_DOCUMENT_NOT_APPROVED, StatusMessages.POI_DOCUMENT_NOT_APPROVED);
	}

	public static APIResponse createPackageNameCouldNotBeNull() {
		return new APIResponse(StatusCodes.PACKAGE_NAME_COULD_NOT_BE_NULL,
				StatusMessages.PACKAGE_NAME_COULD_NOT_BE_NULL);
	}

	public static APIResponse createPackageNameCouldNotBeEmpty() {
		return new APIResponse(StatusCodes.PACKAGE_NAME_COULD_NOT_BE_EMPTY,
				StatusMessages.PACKAGE_NAME_COULD_NOT_BE_EMPTY);
	}

	public static APIResponse createPackageNotFound() {
		return new APIResponse(StatusCodes.PACKAGE_NAME_NOT_FOUND, StatusMessages.PACKAGE_NAME_NOT_FOUND);
	}

	public static APIResponse createDocumentPathCouldNotBeNull() {
		return new APIResponse(StatusCodes.DOCUMENT_PATH_COULD_NOT_BE_NULL,
				StatusMessages.DOCUMENT_PATH_COULD_NOT_BE_NULL);
	}

	public static APIResponse createDocumnetPathCouldNotBeEmpty() {
		return new APIResponse(StatusCodes.DOCUMENT_PATH_COULD_NOT_BE_EMPTY,
				StatusMessages.DOCUMENT_PATH_COULD_NOT_BE_EMPTY);
	}

	public static APIResponse createPaymentTransactionNoCouldNotBeNull() {
		return new APIResponse(StatusCodes.PAYMENT_TRANSACTION_NO_COULD_NOTBE_NULL,
				StatusMessages.PAYMENT_TRANSACTION_NO_COULD_NOTBE_NULL);
	}

	public static APIResponse createPaymentTransactionNoCouldNotBeEmpty() {
		return new APIResponse(StatusCodes.PAYMENT_TRANSACTION_NO_COULD_NOT_BE_EMPTY,
				StatusMessages.PAYMENT_TRANSACTION_NO_COULD_NOT_BE_EMPTY);
	}

	public static APIResponse createPaidAmmountCouldNotBeNull() {
		return new APIResponse(StatusCodes.PAID_AMMOUNT_COULD_NOT_BE_NULL,
				StatusMessages.PAID_AMMOUNT_COULD_NOT_BE_NULL);
	}

	public static APIResponse createPaidAmmountCouldNotBeEmpty() {
		return new APIResponse(StatusCodes.PAID_AMMOUNT_COULD_NOT_BE_EMPTY,
				StatusMessages.PAID_AMMOUNT_COULD_NOT_BE_EMPTY);
	}

	public static APIResponse createInstallationChargeCouldNotBeNull() {
		return new APIResponse(StatusCodes.INSTALLATION_CHARGE_COULD_NOT_BE_NULL,
				StatusMessages.INSTALLATION_CHARGE_COULD_NOT_BE_NULL);
	}

	public static APIResponse createInstallationChargeNotBeEmpty() {
		return new APIResponse(StatusCodes.INSTALLATION_CHARGE_COULD_NOT_BE_EMPTY,
				StatusMessages.INSTALLATION_CHARGE_COULD_NOT_BE_EMPTY);
	}

	public static APIResponse createInvalidUpdateType() {
		return new APIResponse(StatusCodes.INVALID_UPDATE_TYPE, StatusMessages.INVALID_UPDATE_TYPE);
	}

	public static APIResponse createUpdateTypeCouldNotBeNull() {
		return new APIResponse(StatusCodes.UPDATE_TYPE_COULD_NOT_BE_NULL, StatusMessages.UPDATE_TYPE_COULD_NOT_BE_NULL);
	}

	public static APIResponse createUpdateTypeCouldNotBeEmpty() {
		return new APIResponse(StatusCodes.UPDATE_TYPE_COULD_NOT_BE_EMPTY,
				StatusMessages.UPDATE_TYPE_COULD_NOT_BE_EMPTY);
	}

	public static APIResponse createTicketReassigned() {
		return new APIResponse(StatusCodes.UPDATE_TICKET_REASSIGNED, StatusMessages.UPDATE_TICKET_REASSIGNED);
	}

	public static APIResponse createInvalidFxName() {
		return new APIResponse(StatusCodes.INVALID_FX_NAME, StatusMessages.INVALID_FX_NAME);
	}

	public static APIResponse createFxNameIsEmptyOrNull() {
		return new APIResponse(StatusCodes.FX_NAME_COULD_NOT_BE_EMPTY_OR_NULL,
				StatusMessages.FX_NAME_COULD_NOT_BE_EMPTY_OR_NULL);
	}

	public static APIResponse createTariffPlanTypeCouldNotBeNull() {
		return new APIResponse(StatusCodes.TARIFF_PLAN_TYPE_COULD_NOT_BE_NULL,
				StatusMessages.TARIFF_PLAN_TYPE_COULD_NOT_BE_NULL);
	}

	public static APIResponse createNoTiltle() {
		return new APIResponse(StatusCodes.NO_TITLE, StatusMessages.NO_TITLE);
	}

	public static APIResponse createInValidTitle() {
		return new APIResponse(StatusCodes.INVALID_TITLE, StatusMessages.INVALID_TITLE);
	}

	public static APIResponse createNoFirstName() {
		return new APIResponse(StatusCodes.NO_FIRST_NAME, StatusMessages.NO_FIRST_NAME);
	}

	public static APIResponse createNoLastName() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_LAST_NAME, StatusMessages.NO_LAST_NAME);
	}

	public static APIResponse createNoPrefferedCallDate() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_PREFFERED_CALL_DATE, StatusMessages.NO_PREFFERED_CALL_DATE);
	}

	public static APIResponse createNoMobileNumber() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_MOBILE_NUMBER, StatusMessages.NO_MOBILE_NUMBER);
	}

	public static APIResponse createNoEmailId() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_EMAILID, StatusMessages.NO_EMAILID);
	}

	public static APIResponse createNoCurrentAddressFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_CURRENT_ADDRESS_FROM_CUSTOMER_APP,
				StatusMessages.NO_CURRENT_ADDRESS_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoPermanentAddressFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_PERMANENT_ADDRESS_FROM_CUSTOMER_APP,
				StatusMessages.NO_PERMANENT_ADDRESS_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoCommunicationAddressFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_COMMUNICATION_ADDRESS_FROM_CUSTOMER_APP,
				StatusMessages.NO_COMMUNICATION_ADDRESS_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoCityCodeFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_CITY_CODE_FROM_CUSTOMER_APP,
				StatusMessages.NO_CITY_CODE_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoBranchCodeFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_BRANCH_CODE_FROM_CUSTOMER_APP,
				StatusMessages.NO_BRANCH_CODE_FROM_CUSTOMER_APP);
	}
	
	
	
	
	

	public static APIResponse createNoAreaCodeFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_AREA_CODE_FROM_CUSTOMER_APP,
				StatusMessages.NO_AREA_CODE_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoProspectTypeFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_PROSPECT_TYPE_FROM_CUSTOMER_APP,
				StatusMessages.NO_PROSPECT_TYPE_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoLongitudeFromCustomerApp() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_LONGITUDE_FROM_CUSTOMER_APP,
				StatusMessages.NO_LONGITUDE_FROM_CUSTOMER_APP);
	}

	public static APIResponse createNoLattitudeFromCustomerApp() {
		return new APIResponse(StatusCodes.NO_LATTITUDE_FROM_CUSTOMER_APP,
				StatusMessages.NO_LATTITUDE_FROM_CUSTOMER_APP);
	}

	public static APIResponse createPincodeIsEmptyOrNull() {
		return new APIResponse(StatusCodes.NO_PINCODE_FROM_CUSTOMER_APP, StatusMessages.NO_PINCODE_FROM_CUSTOMER_APP);
	}
	
	/**@author aditya
	 * APIResponse
	 * @return
	 */
	public static APIResponse createAadhaarTransactionIdIsNull() {
		return new APIResponse(StatusCodes.AADHAAR_TRANSACTIONID_IS_NULL, StatusMessages.AADHAAR_TRANSACTIONID_IS_NULL);
		
	}
	
	public static APIResponse createAadhaarTransactionTypeIsNull() {
		return new APIResponse(StatusCodes.AADHAAR_TRANSACTION_TYPE_IS_NULL, StatusMessages.AADHAAR_TRANSACTION_TYPE_IS_NULL);
		
	}

	public static APIResponse createNoCityNameFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_CITY_NAME_FROM_SIEBEL, StatusMessages.NO_CITY_NAME_FROM_SIEBEL);
	}

	public static APIResponse createNoCityNameForCurrentAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_CITY_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL, StatusMessages.NO_CITY_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL);
	}

	
	public static APIResponse createNoCityNameForCommunicationAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_CITY_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL, StatusMessages.NO_CITY_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoStateNameForCurrentAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_STATE_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL, StatusMessages.NO_STATE_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL);
	}
	
	public static APIResponse createNoStateNameForCommunicationAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_STATE_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL, StatusMessages.NO_STATE_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoCountryNameForCurrentAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_COUNTRY_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL, StatusMessages.NO_COUNTRY_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoCountryNameForCommunicationAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_COUNTRY_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL, StatusMessages.NO_COUNTRY_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL);
	}
	
	
	
	public static APIResponse createNoStateNameFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_STATE_NAME_FROM_SIEBEL, StatusMessages.NO_STATE_NAME_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoCountryNameFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_COUNTRY_NAME_FROM_SIEBEL, StatusMessages.NO_COUNTRY_NAME_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoBranchNameFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_BRANCH_NAME_FROM_SIEBEL, StatusMessages.NO_BRANCH_NAME_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoBranchNameForCurrentAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_BRANCH_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL, StatusMessages.NO_BRANCH_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL);
	}
	

	public static APIResponse createNoBranchNameForCommunicationAddressFromSiebel() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_BRANCH_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL, StatusMessages.NO_BRANCH_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoCityNameMapping() {
		// TODO Auto-generated method stub
		return new APIResponse(StatusCodes.NO_CITY_NAME_MAPPING, StatusMessages.NO_CITY_NAME_MAPPING);
	}

	public static APIResponse createNoBranchNameMapping() {
		return new APIResponse(StatusCodes.NO_BRANCH_NAME_MAPPING, StatusMessages.NO_BRANCH_NAME_MAPPING);
	}

	public static APIResponse createNoAreaNameMapping() {
		return new APIResponse(StatusCodes.NO_AREA_NAME_MAPPING, StatusMessages.NO_AREA_NAME_MAPPING);
	}

	public static APIResponse createNoAreaNameFromSiebel() {
		return new APIResponse(StatusCodes.NO_AREA_NAME_FROM_SIEBEL, StatusMessages.NO_AREA_NAME_FROM_SIEBEL);
	}

	
	public static APIResponse createNoAreaNameForCurrentAddressFromSiebel() {
		return new APIResponse(StatusCodes.NO_AREA_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL, StatusMessages.NO_AREA_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL);
	}
	
	public static APIResponse createNoAreaNameForCommunicationAddressFromSiebel() {
		return new APIResponse(StatusCodes.NO_AREA_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL, StatusMessages.NO_AREA_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL);
	}
	
	public static APIResponse createNoActionFromSiebel() {
		return new APIResponse(StatusCodes.NO_ACTION_FROM_SIEBEL, StatusMessages.NO_ACTION_FROM_SIEBEL);
	}

	public static APIResponse createNoKnownSourceFROMSiebel() {
		return new APIResponse(StatusCodes.NO_KNOWN_SOURCE_FROM_SIEBEL, StatusMessages.NO_KNOWN_SOURCE_FROM_SIEBEL);
	}

	public static APIResponse createInValidgetHowToKnowACT() {
		return new APIResponse(StatusCodes.INVALID_KNOWN_SOURCE_FROM_SIEBEL,
				StatusMessages.INVALID_KNOWN_SOURCE_FROM_SIEBEL);
	}

	public static APIResponse createNOProspectNumberFromSiebel() {
		return new APIResponse(StatusCodes.NO_PROSPECT_NUMBER_FROM_SIEBEL,
				StatusMessages.NO_PROSPECT_NUMBER_FROM_SIEBEL);
	}

	public static APIResponse createNOCustomerType() {
		return new APIResponse(StatusCodes.NO_CUSTOMER_TYPE_FROM_SIEBEL, StatusMessages.NO_CUSTOMER_TYPE_FROM_SIEBEL);
	}

	public static APIResponse createNoEnquiryType() {
		return new APIResponse(StatusCodes.NO_ENQUIRY_TYPE, StatusMessages.NO_ENQUIRY_TYPE);
	}

	public static APIResponse createInvalidEnquiryType() {
		return new APIResponse(StatusCodes.INVALID_ENQUIRY_TYPE, StatusMessages.INVALID_ENQUIRY_TYPE);
	}

	public static APIResponse createNOSource() {
		return new APIResponse(StatusCodes.NO_SOURCE, StatusMessages.NO_SOURCE);
	}

	public static APIResponse createNOAddressFound() {
		return new APIResponse(StatusCodes.NO_ADDRESS_FOUND, StatusMessages.NO_ADDRESS_FOUND);
	}
	
	public static APIResponse createNOCurrentAddressFound() {
		return new APIResponse(StatusCodes.NO_CURRENT_ADDRESS_FOUND, StatusMessages.NO_CURRENT_ADDRESS_FOUND);
	}

	public static APIResponse createNOCommunicationAddressFound() {
		return new APIResponse(StatusCodes.NO_COMMUNICATION_ADDRESS_FOUND, StatusMessages.NO_COMMUNICATION_ADDRESS_FOUND);
	}
	
	public static APIResponse createNOPermanentAddressFound() {
		return new APIResponse(StatusCodes.NO_PERMANENT_ADDRESS_FOUND, StatusMessages.NO_PERMANENT_ADDRESS_FOUND);
	}

	
	public static APIResponse createInvalidPreferedCallDate() {
		return new APIResponse(StatusCodes.INVALID_PREFEREDCALL_DATE, StatusMessages.INVALID_PREFEREDCALL_DATE);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoTransactionId() {

		return new APIResponse(StatusCodes.NO_TRANSACTION_ID, StatusMessages.NO_TRANSACTION_ID);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoWoNumber() {

		return new APIResponse(StatusCodes.NO_WO_NUMBER, StatusMessages.NO_WO_NUMBER);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNOCusmoterNumber() {

		return new APIResponse(StatusCodes.NO_CUSMOTER_NUMBER, StatusMessages.NO_CUSMOTER_NUMBER);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoTicketCategory() {

		return new APIResponse(StatusCodes.NO_TICKET_CATEGORY, StatusMessages.NO_TICKET_CATEGORY);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoSymptom() {

		return new APIResponse(StatusCodes.NO_SYMPTOM, StatusMessages.NO_SYMPTOM);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoRequestDate() {

		return new APIResponse(StatusCodes.NO_REQUEST_DATE, StatusMessages.NO_REQUEST_DATE);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoAssignedTo() {

		return new APIResponse(StatusCodes.NO_ASSIGNED_TO, StatusMessages.NO_ASSIGNED_TO);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNOStatus() {

		return new APIResponse(StatusCodes.NO_STATUS, StatusMessages.NO_STATUS);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoComments() {

		return new APIResponse(StatusCodes.NO_COMMENTS, StatusMessages.NO_COMMENTS);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoCxIp() {

		return new APIResponse(StatusCodes.NO_CXIP, StatusMessages.NO_CXIP);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoCxPort() {

		return new APIResponse(StatusCodes.NO_CXPORT, StatusMessages.NO_CXPORT);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoFxName() {

		return new APIResponse(StatusCodes.NO_FXNAME, StatusMessages.NO_FXNAME);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoFxIp() {

		return new APIResponse(StatusCodes.NO_FXIP, StatusMessages.NO_FXIP);
	}

	/**
	 * @author kiran APIResponse
	 * @return
	 */
	public static APIResponse createNoFxPort() {

		return new APIResponse(StatusCodes.NO_FXPORT, StatusMessages.NO_FXPORT);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createInvalidCityName() {
		return new APIResponse(StatusCodes.INVALID_CITY_NAME, StatusMessages.INVALID_CITY_NAME);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoUserFound() {
		return new APIResponse(StatusCodes.NO_USER_FOUND, StatusMessages.NO_USER_FOUND);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createDuplicateTransaction() {
		return new APIResponse(StatusCodes.DUPLICATE_TRANSACTION, StatusMessages.DUPLICATE_TRANSACTION);
	}

	/**
	 * @author snoor
	 * @return APIResponse
	 */
	public static APIResponse createInvalidTransactionID() {
		return new APIResponse(StatusCodes.INVALID_TRANSACTION_ID, StatusMessages.INVALID_TRANSACTION_ID);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoCxPortFromSiebel() {

		return new APIResponse(StatusCodes.NO_CX_PORT_FROM_SIEBEL, StatusMessages.NO_CX_PORT_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoFxPortFromSiebel() {

		return new APIResponse(StatusCodes.NO_FX_PORT_FROM_SIEBEL, StatusMessages.NO_FX_PORT_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoFxNameFromSiebel() {
		return new APIResponse(StatusCodes.NO_FX_NAME_FROM_SIEBEL, StatusMessages.NO_FX_NAME_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNocxNameFromSiebel() {
		return new APIResponse(StatusCodes.NO_CX_NAME_FROM_SIEBEL, StatusMessages.NO_CX_NAME_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createInvalidAddress() {
		return new APIResponse(StatusCodes.INVALID_ADDRESS, StatusMessages.INVALID_ADDRESS);
	}
	
	public static APIResponse createInvalidAddress1ForCurrentAddress() {
		return new APIResponse(StatusCodes.INVALID_ADDRESS_1_FOR_CURRENT_ADDRESS, StatusMessages.INVALID_ADDRESS_1_FOR_CURRENT_ADDRESS);
	}
	
	
	public static APIResponse createInvalidAddress2ForCurrentAddress() {
		return new APIResponse(StatusCodes.INVALID_ADDRESS_2_FOR_CURRENT_ADDRESS, StatusMessages.INVALID_ADDRESS_2_FOR_CURRENT_ADDRESS);
	}
	
	
	
	public static APIResponse createInvalidAddress1ForCommunicationAddress() {
		return new APIResponse(StatusCodes.INVALID_ADDRESS_1_FOR_COMMUNICATION_ADDRESS, StatusMessages.INVALID_ADDRESS_1_FOR_COMMUNICATION_ADDRESS);
	}
	
	
	public static APIResponse createInvalidAddress2ForCommunicationAddress() {
		return new APIResponse(StatusCodes.INVALID_ADDRESS_2_FOR_COMMUNICATION_ADDRESS, StatusMessages.INVALID_ADDRESS_2_FOR_COMMUNICATION_ADDRESS);
	}
	
	public static APIResponse createInvalidPincodeForCurrentAddress() {
		return new APIResponse(StatusCodes.INVALID_PINCODE_FOR_CURRENT_ADDESS, StatusMessages.INVALID_PINCODE_FOR_CURRENT_ADDRESS);
	}

	public static APIResponse createInvalidPincodeForCommunicationAddress() {
		return new APIResponse(StatusCodes.INVALID_PINCODE_FOR_COMMUNICATION_ADDESS, StatusMessages.INVALID_PINCODE_FOR_COMMINICATION_ADDRESS);
	}
	
	public static APIResponse createNoAddressTypeForCurrentAddress() {
		return new APIResponse(StatusCodes.NO_ADDRESS_TYPE_OF_CURRENT_ADDRESS_FROM_SIEBEL, StatusMessages.NO_ADDRESS_TYPE_OF_CURRENT_ADDRESS_FROM_SIEBEL);
	}
	
	public static APIResponse createNoAddressTypeForCommunicatonAddress() {
		return new APIResponse(StatusCodes.NO_ADDRESS_TYPE_OF_COMMUNICATION_ADDRESS_FROM_SIEBEL, StatusMessages.NO_ADDRESS_TYPE_OF_COMMUNICATION_ADDRESS_FROM_SIEBEL);
	}
	
	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoSubAreaNameFromSiebel() {
		return new APIResponse(StatusCodes.NO_SUB_AREA_NAME_FROM_SIEBEL, StatusMessages.NO_SUB_AREA_NAME_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoSubAreaNameForCurrentAddressFromSiebel() {
		return new APIResponse(StatusCodes.NO_SUB_AREA_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL, StatusMessages.NO_SUB_AREA_NAME_FOR_CURRENT_ADDRESS_FROM_SIEBEL);
	}
	
	
	public static APIResponse createNoSubAreaNameForCommunicationAddressFromSiebel() {
		return new APIResponse(StatusCodes.NO_SUB_AREA_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL, StatusMessages.NO_SUB_AREA_NAME_FOR_COMMUNICATION_ADDRESS_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoSubAttributeeFromSiebel() {
		return new APIResponse(StatusCodes.NO_SUB_ATTRIBUTE_FROM_SIEBEL, StatusMessages.NO_SUB_ATTRIBUTE_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoProspectTypeFromSiebel() {
		return new APIResponse(StatusCodes.NO_PROSPECT_TYPE_FROM_SIEBEL, StatusMessages.NO_PROSPECT_TYPE_FROM_SIEBEL);
	}

	public static APIResponse createInvalidProspectTypeFromSiebel() {
		return new APIResponse(StatusCodes.INVALID_PROSPECT_TYPE_FROM_SIEBEL,
				StatusMessages.INVALID_PROSPECT_TYPE_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createNoReopenCount() {
		return new APIResponse(StatusCodes.NO_REOPEN_COUNT_FROM_SIEBEL, StatusMessages.NO_REOPEN_COUNT_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createInvalidDeDupFlagFromSiebel() {
		return new APIResponse(StatusCodes.INVALID_DEDUPFLAG_FROM_SIEBEL, StatusMessages.INVALID_DEDUPFLAG_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createDuplicateWorkOrder() {
		return new APIResponse(StatusCodes.DUPLICATE_WORKORDER_FROM_SIEBEL,
				StatusMessages.DUPLICATE_WORKORDER_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createDuplicateSR() {
		return new APIResponse(StatusCodes.DUPLICATE_SR_FROM_SIEBEL, StatusMessages.DUPLICATE_SR_FROM_SIEBEL);
	}

	/**
	 * @author aditya APIResponse
	 * @return
	 */
	public static APIResponse createpDuplicateProspect() {
		return new APIResponse(StatusCodes.DUPLICATE_PROSPECT_FROM_SIEBEL,
				StatusMessages.DUPLICATE_PROSPECT_FROM_SIEBEL);
	}

	/**
	 * @author snoor APIResponse
	 * @return
	 */
	public static APIResponse createNoCustomerProfession() {
		return new APIResponse(StatusCodes.NO_CUSTOMER_PROFESSION, StatusMessages.NO_CUSTOMER_PROFESSION);
	}

	/**@author snoor
	 * APIResponse
	 * @return
	 */
	public static APIResponse createInvalidCustomerProfession() {
		return new APIResponse(StatusCodes.INVALID_CUSTOMER_PROFESSION, StatusMessages.INVALID_CUSTOMER_PROFESSION);
	}

	/**@author snoor
	 * APIResponse
	 * @return
	 */
	public static APIResponse createInvalidMobileNumber() {
		return new APIResponse(StatusCodes.INVALID_MOBILE_NUMBER, StatusMessages.INVALID_MOBILE_NUMBER);
	}

	/**@author snoor
	 * APIResponse
	 * @return
	 */
	public static APIResponse createInvalidEmailId() {
		return new APIResponse(StatusCodes.INVALID_EMAIL_ID, StatusMessages.INVALID_EMAIL_ID);
	}

	/**@author snoor
	 * APIResponse
	 * @return
	 */
	public static APIResponse createInvalidCxIp() {
		return new APIResponse(StatusCodes.INVALID_CXIP, StatusMessages.INVALID_CXIP);
	}

	/**@author snoor
	 * APIResponse
	 * @return
	 */
	public static APIResponse createInvalidFxIp() {
		return new APIResponse(StatusCodes.INVALID_FXIP, StatusMessages.INVALID_FXIP);
	}
	
	public static APIResponse createNoSubAreaNameMapping() {
		return new APIResponse(StatusCodes.NO_SUB_AREA_NAME_MAPPING, StatusMessages.NO_SUB_AREA_NAME_MAPPING);
	}

}
