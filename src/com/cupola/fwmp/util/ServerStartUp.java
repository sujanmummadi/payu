package com.cupola.fwmp.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ServerStartUp implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
		System.out
				.println("\t\t\t****************************************************************************************");
		System.out
				.println("\t\t\t************************** FWMP SERVER DESTROYED SUCCESSFULL***************************");
		System.out
				.println("\t\t\t****************************************************************************************\n\n");

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {

//		BranchDAOImpl branchDaoImpl = new BranchDAOImpl();
//		branchDaoImpl.init();
		
		System.out
				.println("\n\n");
		System.out
				.println("\t\t\t****************************************************************************************");
		System.out
				.println("\t\t\t************************ FWMP WEB SERVER INITIALIZATION SUCCESSFULL**********************");
		System.out
				.println("\t\t\t****************************************************************************************\n\n");
	}

}
