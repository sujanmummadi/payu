package com.cupola.fwmp.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import com.cupola.fwmp.FRConstant.FrFlowStatus;
import com.cupola.fwmp.auth.AppUser;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;

public class AuthUtils
{
	static UserDao userDao;

	static final Logger log = Logger.getLogger(AuthUtils.class);

	public static List<String> TAB_ELIGIBLE_ROLES = Arrays
			.asList(new String[] { "ROLE_TL", "ROLE_NE", "ROLE_EX" });
	
	
	public static List<String> NI_DEF = Arrays		
			.asList(new String[] { "ROLE_NI" });
	public static List<String> SALES_DEF = Arrays		
			.asList(new String[] { "ROLE_SALES" });
	public static List<String> FR_DEF = Arrays		
			.asList(new String[] { "ROLE_FR" });
	

	public final static List<String> VENDOR_NI_ELIGIBLE_ROLES = Arrays
			.asList(new String[] { "ROLE_TL", "ROLE_NI" });

	public final static long VENDOR_ELIGIBLE_USER_TYPE = 1l;
	public final static long TAB_ELIGIBLE_USER_TYPE = 2l;

	private final static List<String> TL_DEF = Arrays
			.asList(new String[] { "ROLE_TL", "ROLE_NE", "ROLE_FR" });

	public final static List<String> TL_NI_DEF = Arrays
			.asList(new String[] { "ROLE_NI", "ROLE_TL" });
	
	private final static List<String> TL_FR_DEF = Arrays
			.asList(new String[] { "ROLE_FR", "ROLE_TL" });

	public final static List<String> NE_NI_DEF = Arrays
			.asList(new String[] { "ROLE_NI", "ROLE_NE", "ROLE_NE_COPPER","ROLE_NE_FIBER"});

	public final static List<String> NE_NI_DEF_STRICT = Arrays
			.asList(new String[] { "ROLE_NI", "ROLE_NE_COPPER","ROLE_NE_FIBER"});

	//changed by Sharan for Work order reassignment issue. added a new role ROLE_NI
	public final static List<String> NE_COMMON_DEF = Arrays
			.asList(new String[] { "ROLE_NE_COPPER", "ROLE_NE","ROLE_NE_FIBER","ROLE_NI" });

	
	public final static List<String> NE_NI_COPPER = Arrays
			.asList(new String[] { "ROLE_NE_COPPER" });
	public final static List<String> NE_NI_DEFAULT = Arrays
			.asList(new String[] {"ROLE_NI", "ROLE_NE"});

	public final static List<String> NE_NI_FIBER = Arrays
			.asList(new String[] { "ROLE_NE_FIBER" });

	public final static List<String> EXECUTIVE_DEF = Arrays
			.asList(new String[] { "ROLE_AM", "ROLE_BM", "ROLE_CM" });

	public final static List<String> NE_FR_DEF = Arrays.asList(new String[] {
			"ROLE_FR", "ROLE_NE", "ROLE_CX_UP", "ROLE_CX_DOWN" });

	public final static List<String> NE_FR_CXUP_DEF = Arrays
			.asList(new String[] { "ROLE_FR", "ROLE_NE", "ROLE_CX_UP" });

	public final static List<String> NE_FR_CXDOWN_DEF = Arrays
			.asList(new String[] { "ROLE_FR", "ROLE_NE", "ROLE_CX_DOWN" });

	private final static List<String> FR_CXUP_DEF = Arrays
			.asList(new String[] { "ROLE_CX_UP" });
	
	private final static List<String> FR_CXDOWN_DEF = Arrays
			.asList(new String[] {  "ROLE_CX_DOWN" });
	
	private final static List<String> SR_CXDOWN_DEF = Arrays.asList(new String[] {
			"ROLE_NE_SR_ELEMENT"});
	
	
	public final static List<String> NE_SR_CXDOWN_DEF = Arrays.asList(new String[] {
			"ROLE_FR", "ROLE_NE_SR_ELEMENT"});

	private final static List<String> DOC_APPROVER_DEF = Arrays
			.asList(new String[] { "ROLE_CFE" });

	public final static List<String> SALES_TL_DEF = Arrays
			.asList(new String[] { "ROLE_TL", "ROLE_SALES" });

	public final static List<String> SALES_EX_DEF = Arrays
			.asList(new String[] { "ROLE_EX", "ROLE_SALES" });

	private final static List<String> CFE_DEF = Arrays
			.asList(new String[] { "ROLE_CFE" });

	private static List<String> FR_GROUP_ROLE_DEF = Arrays.asList("ROLE_FR","ROLE_LEGAL_TEAM",
			"ROLE_NOC","ROLE_CCNR","ROLE_FINANCE","ROLE_VENDOR","ROLE_CHURN");
	
	public static int getIfExecutiveRole()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		List<String> roles = userDetails.getRoles();
		if (roles != null)
		{
			if (roles.contains("ROLE_SALES") && roles.contains("ROLE_AM"))
				return UserRole.SALES_AM;
			else if (roles.contains("ROLE_SALES") && roles.contains("ROLE_BM"))
				return UserRole.SALES_BM;
			else if (roles.contains("ROLE_SALES") && roles.contains("ROLE_CM"))
				return UserRole.SALES_CM;
			else if (roles.contains("ROLE_SALES") && roles.contains("ROLE_TL"))
				return UserRole.SALES_TL;

			else if (roles.contains("ROLE_NI") && roles.contains("ROLE_AM"))
				return UserRole.NI_AM;
			else if (roles.contains("ROLE_NI") && roles.contains("ROLE_BM"))
				return UserRole.NI_BM;
			else if (roles.contains("ROLE_NI") && roles.contains("ROLE_CM"))
				return UserRole.NI_CM;
			else if (roles.contains("ROLE_NI") && roles.contains("ROLE_TL"))
				return UserRole.NI_TL;

			else if (roles.contains("ROLE_FR") && roles.contains("ROLE_AM"))
				return UserRole.FR_AM;
			else if (roles.contains("ROLE_FR") && roles.contains("ROLE_BM"))
				return UserRole.FR_BM;
			else if (roles.contains("ROLE_FR") && roles.contains("ROLE_CM"))
				return UserRole.FR_CM;
			else if (roles.contains("ROLE_FR") && roles.contains("ROLE_TL"))
				return UserRole.FR_TL;
		}

		return -1;
	}

	public static boolean isFrManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		List<String> roles = userDetails.getRoles();
		if (roles != null)
		{
			return (roles.contains("ROLE_FR") && (roles.contains("ROLE_AM")
					|| roles.contains("ROLE_BM") || roles.contains("ROLE_CM")));
		}
		return false;
	}
	
	public static boolean isFrCityManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		List<String> roles = userDetails.getRoles();
		if (roles != null)
		{
			return (roles.contains("ROLE_FR") && (roles.contains("ROLE_CM")));
		}
		return false;
	}
	
	public static boolean isFrAreaManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		List<String> roles = userDetails.getRoles();
		if (roles != null)
		{
			return (roles.contains("ROLE_FR") && (roles.contains("ROLE_AM")));
		}
		return false;
	}
	
	public static boolean isFrBranchManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		List<String> roles = userDetails.getRoles();
		if (roles != null)
		{
			return (roles.contains("ROLE_FR") && ( roles.contains("ROLE_BM")));
		}
		return false;
	}

	public static boolean isSalesUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_SALES"));
	}

	public static boolean isSalesExecutiveUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_EX"));
	}
	public static boolean isFRGroupUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		boolean isRoleFound = false;
		for( String role : roles )
		{
			isRoleFound = FR_GROUP_ROLE_DEF.contains(role);
			if( isRoleFound )
				break;
		}
		return  isRoleFound;
	}
	
	public static boolean isFRUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_FR"));
	}

	public static boolean isCCNRUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_CCNR"));
	}

	public static boolean isNOCTeamUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_NOC"));
	}

	public static boolean isLegalTeamUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_LEGAL_TEAM"));
	}

	public static boolean isFinanceTeamUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_FINANCE"));
	}

	public static boolean isChurnTeamUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_CHURN"));
	}

	public static boolean isVendorUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_VENDOR"));
	}

	public static boolean isSalesManager()
	{
		int executiveValue = AuthUtils.getIfExecutiveRole();
		return (executiveValue == UserRole.SALES_AM
				|| executiveValue == UserRole.SALES_BM
				|| executiveValue == UserRole.SALES_CM);

	}

	public static boolean isNIManager()
	{
		int executiveValue = AuthUtils.getIfExecutiveRole();
		return (executiveValue == UserRole.NI_AM
				|| executiveValue == UserRole.NI_BM
				|| executiveValue == UserRole.NI_CM);

	}

	public static boolean isManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();

		if (roles != null)
		{

			return roles.contains("ROLE_AM") || roles.contains("ROLE_BM")
					|| roles.contains("ROLE_CM");
		}
		return false;

	}

	public static boolean isAreaManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();

		if (roles != null)
		{

			return roles.contains("ROLE_AM");
		}
		return false;

	}

	public static boolean isBranchManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();

		if (roles != null)
		{

			return roles.contains("ROLE_BM");
		}
		return false;

	}

	public static boolean isCityManager()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();

		if (roles != null)
		{

			return roles.contains("ROLE_CM");
		}
		return false;

	}

	public static boolean isCFEUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_CFE"));
	}

	public static boolean isCCUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_CC"));
	}

	public static boolean isNIUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_NI"));
	}

	public static boolean isNITLUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.containsAll(TL_NI_DEF));
	}
	public static boolean isTLUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_TL"));
	}
	
	public static boolean isNEUser()
	{
		try {
			AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();

			List<String> roles = userDetails.getRoles();
			for (String role : roles)
			{
				if (NE_COMMON_DEF.contains(role))
					return true;
			}
			
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public static boolean isFRTLUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.containsAll(TL_FR_DEF));
	}

	public static boolean isFRNEUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.containsAll(NE_FR_DEF));
	}

	public static boolean isFRSRNEUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.containsAll(NE_SR_CXDOWN_DEF));
	}
	
	public static boolean isFRCxUpNEUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.containsAll(NE_FR_CXUP_DEF));
	}

	public static boolean isFRCXDownNEUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.containsAll(NE_FR_CXDOWN_DEF));
	}

	public static boolean isFrCxDownNEUser(long userId)
	{
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if (roles == null)
			return false;
		return (roles.containsAll(NE_FR_CXDOWN_DEF));
	}
	public static boolean isSRNEUser(long userId)
	{
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if(roles == null)
			return false;
		return (roles.containsAll(NE_SR_CXDOWN_DEF));
	}

	public static boolean isFrCxUpNEUser(long userId)
	{
		List<String> roles = userDao.getAllRolesByUserId(userId);
		return (roles.containsAll(NE_FR_CXUP_DEF));
	}

	public static boolean isSalesTlUser()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.containsAll(SALES_TL_DEF));
	}

	public static boolean isAdmin()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_ADMIN"));
	}

	public static boolean isUserAdmin()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		List<String> roles = userDetails.getRoles();
		return (roles.contains("ROLE_USER_ADMIN"));
	}


	public static int getCurrentUserRole()
	{
		try {
			AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();

			List<String> roles = userDetails.getRoles();

			if (roles.containsAll(TL_NI_DEF))
				return UserRole.TL_NI;
			
			if( roles.containsAll(TL_FR_DEF))
				return UserRole.TL;
			
			if (roles.containsAll(TL_DEF))
				return UserRole.TL;

			else if (roles.containsAll(NE_NI_DEF) ||roles.containsAll(NE_NI_DEFAULT) || roles.containsAll(NE_NI_COPPER)
					|| roles.containsAll(NE_NI_FIBER))
				return UserRole.NE_NI;

			else if (roles.containsAll(NE_NI_DEF) ||roles.containsAll(NE_NI_DEFAULT) || roles.containsAll(NE_FR_DEF)
					|| roles.containsAll(NE_NI_COPPER)
					|| roles.containsAll(NE_NI_FIBER))
				return UserRole.NE;

			else if(roles.containsAll(NE_FR_CXDOWN_DEF) || roles.containsAll(NE_FR_CXUP_DEF))
				return UserRole.NE_FR;

			else if (roles.containsAll(DOC_APPROVER_DEF))
				return UserRole.DOC_APPROVER;

			else if (roles.containsAll(SALES_TL_DEF))
				return UserRole.SALES_TL;
			else if (roles.containsAll(SALES_EX_DEF))
				return UserRole.SALES_EX;

			return -1;
		} catch (Exception e) {
			return -1;
		}
	}

	public static long getCurrentUserId()
	{
		if (SecurityContextHolder.getContext() == null
				|| SecurityContextHolder.getContext()
						.getAuthentication() == null
				|| SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal() == null
				|| "anonymousUser"
						.equalsIgnoreCase(SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal().toString()))
			return 1l;

		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		return userDetails.getId();
	}
	
	public static String getCurrentUserDisplayName()
	{
		if (SecurityContextHolder.getContext() == null
				|| SecurityContextHolder.getContext()
						.getAuthentication() == null
				|| SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal() == null
				|| "anonymousUser"
						.equalsIgnoreCase(SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal().toString()))
			return "admin";

		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		return userDetails.getUserDisplayName();
	}

	public static TypeAheadVo getCurrentUserCity()
	{
		if (SecurityContextHolder.getContext() == null
				|| SecurityContextHolder.getContext()
						.getAuthentication() == null
				|| SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal() == null
				|| "anonymousUser"
						.equalsIgnoreCase(SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal().toString()))
		{

			return null;
		}
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		TypeAheadVo vo = new TypeAheadVo();
		
		if (userDetails.getCities() != null
				&& userDetails.getCities().entrySet().iterator().hasNext())
		{
			Entry<Long, String> entry = userDetails.getCities().entrySet()
					.iterator().next();

			vo.setId(entry.getKey());
			vo.setName(entry.getValue());
		}

		return vo;
	}

	public static TypeAheadVo getCurrentUserBranch()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		TypeAheadVo vo = new TypeAheadVo();

		if (userDetails.getCities() != null
				&& userDetails.getBranches().entrySet().iterator().hasNext())
		{
			Entry<Long, String> entry = userDetails.getBranches().entrySet()
					.iterator().next();

			vo.setId(entry.getKey());
			vo.setName(entry.getValue());
		}

		return vo;
	}

	public static TypeAheadVo getCurrentUserArea()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		TypeAheadVo vo = new TypeAheadVo();

		if (userDetails.getCities() != null
				&& userDetails.getAreas().entrySet().iterator().hasNext())
		{
			Entry<Long, String> entry = userDetails.getAreas().entrySet()
					.iterator().next();

			vo.setId(entry.getKey());
			vo.setName(entry.getValue());
		}

		return vo;
	}
	public static Map<Long, String> getCurrentUsersAllArea()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		Map<Long, String> areaIds = new HashMap<Long, String>();
		if (userDetails.getAreas() != null
				&& userDetails.getAreas().entrySet().iterator().hasNext())
		{
			areaIds.putAll(userDetails.getAreas());
			 
		 
		}
		if(areaIds.size() > 0)
			return areaIds;
		
		return null;
	}
	public static TypeAheadVo getCurrentUserGroup()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		TypeAheadVo vo = new TypeAheadVo();

		if (userDetails.getUserGroup() != null
				&& userDetails.getUserGroup().entrySet().iterator().hasNext())
		{
			Entry<Long, String> entry = userDetails.getUserGroup().entrySet()
					.iterator().next();

			vo.setId(entry.getKey());
			vo.setName(entry.getValue());
		}

		return vo;
	}

	public static String getCurrentUserLoginId()
	{
		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		return userDetails.getUsername();
	}

	public class UserRole
	{
		public static final int DOC_APPROVER = 102;
		public static final int TL = 99;
		public static final int TL_NI = 100;
		public static final int NE = 101;
		public static final int NE_NI = 102;
		public static final int NE_FR = 103;
		public static final int SALES_TL = 200;
		public static final int SALES_EX = 201;
		public static final int CFE = 202;

		public static final int SALES_AM = 500;
		public static final int SALES_BM = 501;
		public static final int SALES_CM = 502;
		
		public static final int NI_AM = 600;
		public static final int NI_BM = 601;
		public static final int NI_CM = 602;
		public static final int NI_TL = 603;
		public static final int NI_NE = 604;
		
		public static final int FR_AM = 700;
		public static final int FR_BM = 701;
		public static final int FR_CM = 702;
		public static final int FR_TL = 703;
		public static final int FR_NE = 704;
		
		public static final String ROLE_NI = "ROLE_NI";
		public static final String ROLE_NI_FIBER = "ROLE_NI_FIBER";
		public static final String ROLE_NI_COPPER = "ROLE_NI_COPPER";
		public static final String ROLE_SALES = "ROLE_SALES";
		public static final String ROLE_FR = "ROLE_FR";
		public static final String ROLE_EX = "ROLE_EX";
		public static final String ROLE_NE = "ROLE_NE";
		
		public static final String ROLE_NE_FIBER = "ROLE_NE_FIBER";
		public static final String ROLE_NE_COPPER = "ROLE_NE_COPPER";

		public static final String ROLE_AM = "ROLE_AM";
		public static final String ROLE_BM = "ROLE_BM";
		public static final String ROLE_CM = "ROLE_CM";

	}

//	public static String getKeyForQueueByUserId(Long userLoginId)
//	{
//
//		String fullyQualifiedNameKey = null;
//
//		String areaId = "UNKNOWN";
//		if (getCurrentUserArea() != null
//				&& getCurrentUserArea().getId() != null)
//			areaId = getCurrentUserArea().getId().toString();
//
//		String branchId = "UNKNOWN";
//		if (getCurrentUserBranch() != null
//				&& getCurrentUserBranch().getId() != null)
//			branchId = getCurrentUserBranch().getId().toString();
//
//		String cityId = "UNKNOWN";
//		if (getCurrentUserCity() != null
//				&& getCurrentUserCity().getId() != null)
//			cityId = getCurrentUserCity().getId().toString();
//
//		fullyQualifiedNameKey = cityId + "_" + branchId + "_" + areaId + "_"
//				+ userLoginId;
//
//		return fullyQualifiedNameKey;
//	}

	public static List<Long> getAreaIdsForCurrentUser()
	{
		List<Long> areaIds = new ArrayList<Long>();
		if (isAreaManager() && getCurrentUserArea() != null && getCurrentUsersAllArea() != null)
		{
			areaIds.addAll( getCurrentUsersAllArea().keySet());

		} else if (isBranchManager())
		{
			AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();
			
			if( userDetails.getBranches() != null)
			 for(Long branch : userDetails.getBranches().keySet())
					{
					 areaIds.addAll(LocationCache
								.getAreasByBranchId(branch+ "")
								.keySet());
					}
			
		} else if (isCityManager())
		{
			areaIds.addAll(LocationCache
					.getAreasByCityId(getCurrentUserCity().getId() + "")
					.keySet());
		}
		return areaIds;
	}

	public static List<String> getUserRolesByFlowId(Long flowId)
	{
		if (flowId != null)
		{
			if (flowId.equals(FrFlowStatus.CX_DOWN))
				return  FR_CXDOWN_DEF;
			else if( flowId.equals(FrFlowStatus.SENIOR_ELEMENT) )
				return  SR_CXDOWN_DEF;
			else
				return  FR_CXUP_DEF;
		}
		return FR_CXUP_DEF;
	}

	public static UserVo getCurrentCityCCNRUser()
	{
		long cityId = getCurrentUserCity().getId();
		List<UserVo> userVo = userDao.getCurrentCityTeams("CCNR", cityId);
		return ( userVo != null && userVo.size() > 0 ) ? userVo.get(0) : null;
	}
	
	public static UserVo getCurrentCityFinanceTeam()
	{
		long cityId = getCurrentUserCity().getId();
		List<UserVo> userVo = userDao.getCurrentCityTeams("FINANCE", cityId);
		return ( userVo != null && userVo.size() > 0 ) ? userVo.get(0) : null;
	}

	public static UserDao getUserDao()
	{
		return userDao;
	}

	public static void setUserDao(UserDao userDao)
	{
		AuthUtils.userDao = userDao;
	}
	
	public static boolean isWebLoginNotAllowed( )
	{
 
		return	 isSalesExecutiveUser() ;
	}
	
}
