package com.cupola.fwmp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import com.cupola.fwmp.util.FTPUtil;

public class FTPUploader
{

	FTPClient ftpClient = null;

	static Logger log=Logger.getLogger(FTPUploader.class);
			
	public FTPUploader(String host, String user, String pwd) throws Exception
	{
		try
		{
			ftpClient = new FTPClient();
			ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
			int reply;
			ftpClient.connect(host);
			reply = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply))
			{
				ftpClient.disconnect();
				throw new Exception("Exception in connecting to FTP Server");
			}
			ftpClient.login(user, pwd);
			ftpClient.enterLocalPassiveMode();
//			ftpClient.enterRemotePassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void uploadFile(String localFileFullName, String fileName,
			String hostDir) throws Exception
	{
		try (InputStream input = new FileInputStream(new File(localFileFullName)))
		{
			this.ftpClient.storeFile(hostDir + fileName, input);
			
		}
	}

	public void disconnect()
	{
		if (this.ftpClient.isConnected())
		{
			try
			{
				this.ftpClient.logout();
				this.ftpClient.disconnect();
			} catch (IOException f)
			{
				// do nothing as file is already saved to server
			}
		}
	}

	public static void main(String[] args) throws Exception
	{
		log.info("Start");
		FTPUploader ftpUploader = new FTPUploader("192.168.6.85", "aditya", "root123");
		String basefile = "/home/aditya/Projects/ACT_FWMP/LiveReport/Hyd/CAF/";

		String file = "CAF_Report.csv";

		String remoteArchPath = FTPUtil.getRemoteBasePath() + "output/";

		log.info("basee file " + basefile + " remote file "
				+ remoteArchPath);

		ftpUploader.uploadFile(basefile+file, file, remoteArchPath);
		ftpUploader.disconnect();
		log.debug("Done");
	}

}