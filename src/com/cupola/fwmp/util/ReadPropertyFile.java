package com.cupola.fwmp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ReadPropertyFile
{

	public static Logger logger = Logger.getLogger(ReadPropertyFile.class);

	public static Properties readProp()
	{
		Properties prop = new Properties();

		InputStream input = null;

		try
		{

		String currentProp = 	 "/home/aditya/Projects/ACT_FWMP/Dev/workspace/FWMP_1_2_1/src/properties/ftpconfig.properties";
		File f = new File(currentProp);
		
		if( f.exists())
		{
			 logger.info("Using ftp server config file "+currentProp) ;
			input = new FileInputStream(f)	;
		}
	
		else
			{
				String filename = "config.properties";

				input = ReadPropertyFile.class.getClassLoader()
						.getResourceAsStream(filename);

				if (input == null)
				{
					logger.info("Sorry, unable to find " + filename
							+ " in class path");
					return null;
				}
			}
			// load a properties file from class path, inside static method

			prop.load(input);
			
		} catch (IOException ex)
		{
			ex.printStackTrace();
		} finally
		{
			if (input != null)
			{
				try
				{
					input.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return prop;
	}
}
