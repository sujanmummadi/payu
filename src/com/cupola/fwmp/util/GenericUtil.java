package com.cupola.fwmp.util;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;

public class GenericUtil
{
	private static Logger logger = LogManager.getLogger(GenericUtil.class.getName());
	static String dateFormatPropValue = "dd/MM/YYYY HH:mm:ss";
	static String customerFormatPropValue = "dd/MM/YYYY";
	static String siebelDateFormatPropValue = "MM/dd/yyyy HH:mm:ss";
	static String dbFormatDate = "yyyy-MM-dd HH:mm:ss";

	public static boolean isNullOrEmpty(final Collection<?> c)
	{
		return c == null || c.isEmpty();
	}

	public static boolean isNullOrEmpty(final Map<?, ?> m)
	{
		return m == null || m.isEmpty();
	}

	public static String convertToDateFormat(Object obj)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(dbFormatDate);

		if (obj == null)
		{
			return null;
		}
		String inputDate = obj.toString().trim();
		if (inputDate.trim().length() == 0)
			return null;
		// inputDate = inputDate.substring(0,inputDate.lastIndexOf("."));
		Date ts = null;

		try
		{
			
			inputDate=formatter.format(obj); 
			ts = formatter.parse(inputDate);
		} catch (ParseException e)
		{
			logger.error("Error while parsing date " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		formatter = new SimpleDateFormat(dateFormatPropValue);
		if (ts == null)
		{
			return "";
		}
		String date = formatter.format(ts);
		return date;
	}

	public static Date convertStringToDateFormat(Object obj)
	{

		if (obj == null)
		{
			return null;
		}
		String inputDate = obj.toString().trim();
		if (inputDate.trim().length() == 0)
			return null;
		Date ts = (Date) obj;
		return ts;
	}

	public static Date addDays(Date date, int days)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}

	public static Date converDateFromString(String date)
	{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date startDate = null;
		//

		try
		{
			startDate = df.parse(date);

			// String newDateString = df.format(startDate);
			// System.out.println(newDateString);

			return startDate;

		} catch (ParseException e)
		{
			logger.error("Error while parsing date " + e.getMessage());
			e.printStackTrace();
			return startDate;
		}

	}
	
	public static Date convertToFWMPDateFormatFromSiebel(String date)
	{
//Modified by Manjuprasad for Date Correction in Seibel 
//		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date startDate = null;


		try
		{
			startDate = df.parse(date);
			return startDate;

		} catch (ParseException e)
		{
			logger.error("Error while parsing date " + e.getMessage());
			e.printStackTrace();
			return startDate;
		}
	
}

	public static Date converDateFromMQDateString(String date)
	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date startDate = null;
		//

		try
		{
			startDate = df.parse(date);

			// String newDateString = df.format(startDate);
			// System.out.println(newDateString);

			return startDate;

		} catch (ParseException e)
		{
			logger.error("Error while parsing date " + e.getMessage());
			e.printStackTrace();
			return startDate;
		}

	}

	public static String convertToUiDateFormat(Object obj)
	{

		if (obj == null)
		{
			return null;
		}
		String inputDate = obj.toString().trim();
		if (inputDate.trim().length() == 0)
			return null;
		// inputDate = inputDate.substring(0,inputDate.lastIndexOf("."));
		Date ts = (Date) obj;
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormatPropValue);

		String date = formatter.format(ts);
		return date;
	}

	public static String convertToCustomerDateFormat(Object obj)
	{

		if (obj == null)
		{
			return null;
		}
		String inputDate = obj.toString().trim();
		if (inputDate.trim().length() == 0)
			return null;
		// inputDate = inputDate.substring(0,inputDate.lastIndexOf("."));
		Date ts = (Date) obj;
		SimpleDateFormat formatter = new SimpleDateFormat(customerFormatPropValue);

		String date = formatter.format(ts);
		return date;
	}
	
	public static Date addHours(int hours)
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, hours);
		return  cal.getTime();
	}
	
	public static String convertToMqDateFormat(Object obj)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormatPropValue);
		if (obj == null)
		{
			return null;
		}
		String inputDate = obj.toString().trim();
		if (inputDate.trim().length() == 0)
			return null;
		// inputDate = inputDate.substring(0,inputDate.lastIndexOf("."));
		Date ts = (Date) obj;

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, +4);

		Date calDate = cal.getTime();
		String date = null;
		if (calDate.compareTo(ts) > 0)
		{
			logger.debug("prefered date" + ts + " is smaller to expected date "
					+ calDate);

			date = formatter.format(calDate);

		} else
		{
			date = formatter.format(ts);
		}

		logger.debug("Returing date for mq " + date);

		return date;
	}

	public static Date convertFromUiDateFormat(String obj)
	{

		if (obj == null)
		{
			return null;
		}
		String inputDate = obj.toString().trim();
		if (inputDate.trim().length() == 0)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		try
		{
			return formatter.parse(obj);
		} catch (ParseException e)
		{
			logger.error("Error while parsing date " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Long getUserIdByKey(String key)
	{

		if (key != null)
		{
			String[] ids = key.split("_");
			if (ids.length == 4)
			{
				return Long.valueOf(ids[3]);
			}
		}
		return 0l;
	}

	public static String formatRemarks(String oldRemarks, String newRemarks,
			String userName)
	{
		if (newRemarks == null || newRemarks.trim().length() == 0)
		{
			return oldRemarks;
		}
		userName = "<b>" + userName + "</b>";
		StringBuffer finalValue = new StringBuffer("");
		if (oldRemarks == null)
		{
			finalValue.append(userName).append(" : ").append(newRemarks)
					.append(" <br>");

		} else
		{
			if (oldRemarks.indexOf("<br>") < 0)
				oldRemarks = oldRemarks.concat("<br>");
			finalValue = new StringBuffer(oldRemarks);
			finalValue.append(userName).append(" : ").append(newRemarks)
					.append(" <br>");
		}

		return finalValue.toString();
	}

	public static String completeCustomerName(CustomerVO customer)
	{
		StringBuilder fullname = new StringBuilder("");
		if (customer.getTitle() != null)
			fullname.append(customer.getTitle() + " ");
		if (customer.getFirstName() != null)
			fullname.append(customer.getFirstName() + " ");
		if (customer.getMiddleName() != null)
			fullname.append(customer.getMiddleName() + " ");
		if (customer.getLastName() != null)
			fullname.append(customer.getLastName() + " ");
		return fullname.toString();

	}

	public static String completeUserName(UserVo user)
	{
		if (user == null)
			return "";
		StringBuilder fullname = new StringBuilder("");
		if (user.getFirstName() != null)
			fullname.append(user.getFirstName() + " ");
		if (user.getLastName() != null)
			fullname.append(user.getLastName() + " ");
		return fullname.toString();

	}

	/**
	 * @param user
	 * @return
	 */
	public static String buildUserSelectOption(UserVo user)
	{
		StringBuilder userDisplayValue = new StringBuilder(completeUserName(user));
		userDisplayValue.append(" - ");
		userDisplayValue.append(user.getLoginId());

		if (user.getUserGroups() != null && user.getUserGroups().size() > 0)
		{
			userDisplayValue.append(" (");

			if (ApplicationUtils.isAppRequest()) 
			{
				userDisplayValue.append(user.getLoginId());
			} else 
			{
				if (user.getUserGroups() != null
					 && !user.getUserGroups().isEmpty()) 
				{
					logger.debug("################################ " + user.getUserGroups());
					for (Iterator<TypeAheadVo> iterator = user.getUserGroups()
						.iterator(); iterator.hasNext();) 
					{
						TypeAheadVo type = (TypeAheadVo) iterator.next();
						userDisplayValue.append(type.getName());

					}
				}
			}

			userDisplayValue.append(")");
		}
		return userDisplayValue.toString();
	}

	public static Long getCategoryIdByString(String categoryString)
	{
		if (TicketCategory.NEW_INSTALLATION.equalsIgnoreCase(categoryString))
			return TicketCategory.NIDEPLOYMENT;

		else if (TicketCategory.FAULT_REPAIR_ROI.contains(categoryString))
			return TicketCategory.FR;
		
		else if (TicketCategory.FAULT_REPAIR_HYD.contains(categoryString))
			return TicketCategory.FR;

		// add others cases...

		return 0l;
	}

	public static List<Long> getUsersSelectedAreaIds()
	{
		List<Long> areaIds = null;
		if (AuthUtils.isManager() || AuthUtils.isAdmin())
		{

			TicketFilter filter = ApplicationUtils.retrieveTicketFilter();

			if (filter == null)
				return null;
			areaIds = new ArrayList<Long>();
			
			areaIds.add(-1L);

			Long areaId = filter.getAreaId();
			Long branchId = filter.getBranchId();
			Long cityId = filter.getCityId();

			if (!AuthUtils.isAdmin())
				cityId = AuthUtils.getCurrentUserCity().getId();

			if (areaId != null && areaId > 0)
				areaIds.add(areaId);
			else if (branchId != null && branchId > 0)
				areaIds.addAll(LocationCache.getAreasByBranchId(branchId + "")
						.keySet());

			else if (cityId != null && cityId > 0)
			{
				Map<Long, String> areas = LocationCache
						.getAreasByCityId(cityId + "");
				areaIds.addAll(areas.keySet());
			}

			if (AuthUtils.isAreaManager() && (areaId == null || areaId <= 0))
			{
				areaIds.clear();
				
				if (AuthUtils.getAreaIdsForCurrentUser() != null && !AuthUtils.getAreaIdsForCurrentUser().isEmpty())
				areaIds.addAll(AuthUtils.getAreaIdsForCurrentUser());
				else
					areaIds.addAll(LocationCache.getAreasByBranchId(AuthUtils.getCurrentUserBranch().getId() + "").keySet());
			} else if (AuthUtils.isBranchManager()
					&& (areaId == null || areaId <= 0))
			{
				areaIds.clear();
				areaIds.addAll(AuthUtils.getAreaIdsForCurrentUser());
			}
		}
		return areaIds;
	}

	public static boolean isSEUser(UserVo vo)
	{
		if (vo == null || vo.getUserRoles() == null)
			return false;
		else
		{
			for (TypeAheadVo ta : vo.getUserRoles())
			{
				if ("ROLE_SALES".equals(ta.getName()))
					return true;
			}
		}
		return false;
	}

	public static boolean isStringNotNull(String input)
	{
		return !(input == null || input.trim().length() == 0 || "undefined".equals(input) || "null".equals(input));
	}

	public static String createOrUpdateRequestKey(String exstingKey)
	{
		if (exstingKey != null)
		{

			exstingKey = exstingKey.replace(exstingKey.split("_")[2], System
					.currentTimeMillis() + "");
		} else
		{
			exstingKey = AuthUtils.getCurrentUserId() + "_"
					+ ApplicationUtils.retrieveHttpRequestInfo()
							.getHttpSession().getId()
					+ "_" + System.currentTimeMillis();
		}
		return exstingKey;
	}

	public static Date getLastRequestTime(String exstingKey)
	{
		if (exstingKey != null && exstingKey.split("_").length > 1)
		{
			long limeStamp = Long.valueOf(exstingKey.split("_")[2]);
			return new Date(limeStamp);
		} 
		else
			return null;
	}

	public static String dateToStringFormate(Date date)
	{
		String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(date);

	}

	public static Calendar getCalendarForNow()
	{
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		return calendar;
	}

	public static Date getFirstDateOfPrivious2PriviousMonth()
	{
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		aCalendar.set(Calendar.DATE, -30);
		aCalendar.set(Calendar.HOUR_OF_DAY, 0);
		aCalendar.set(Calendar.MINUTE, 0);
		aCalendar.set(Calendar.SECOND, 0);
		aCalendar.set(Calendar.MILLISECOND, 0);
		Date firstDateOfPreviousMonth = aCalendar.getTime();
		return firstDateOfPreviousMonth;
	}

	public static Date getFirstDateOfPriviousMonth()
	{
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.set(Calendar.HOUR_OF_DAY, 0);
		aCalendar.set(Calendar.MINUTE, 0);
		aCalendar.set(Calendar.SECOND, 0);
		aCalendar.set(Calendar.MILLISECOND, 0);
		Date firstDateOfPreviousMonth = aCalendar.getTime();
		return firstDateOfPreviousMonth;
	}

	public static Date getLastDateOfPriviousMonth()
	{
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		aCalendar.set(Calendar.HOUR_OF_DAY, 23);
		aCalendar.set(Calendar.MINUTE, 59);
		aCalendar.set(Calendar.SECOND, 59);
		aCalendar.set(Calendar.MILLISECOND, 0);
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		return lastDateOfPreviousMonth;
	}

	public static void setTimeToBeginningOfDay(Calendar calendar)
	{
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	public static void setTimeToEndofDay(Calendar calendar)
	{
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
	}

	public static String createFolder(String documentPath, String parentFolder,
			String folderName)
	{
		File basePath = new File(documentPath);
		if (!basePath.exists())
		{
			basePath.mkdir();
		}

		File filePath = new File(documentPath + File.separator + parentFolder);

		if (!filePath.exists())
		{
			filePath.mkdir();

			logger.debug("Creating folder for prospect " + parentFolder
					+ " at " + documentPath + parentFolder);

			String folder = documentPath + File.separator + parentFolder;
			return createChildFolder(folder, folderName);

		} else
		{

			logger.debug("Folder Exist with folder Name " + parentFolder
					+ " at " + documentPath + parentFolder);

			String folder = documentPath + File.separator + parentFolder;
			return createChildFolder(folder, folderName);
		}

	}

	public static String createChildFolder(String documentPath,
			String folderName)
	{
		File filePath = new File(documentPath + File.separator + folderName);

		if (!filePath.exists())
		{
			filePath.mkdir();

			logger.info("Creating folder for prospect " + folderName
					+ " at " + documentPath + folderName);

			return documentPath + File.separator + folderName;

		} else
		{

			logger.debug("Folder Exist with folder Name " + folderName
					+ " at " + documentPath + folderName);

			return documentPath + File.separator + folderName;

		}

	}

	public static String getDateBy5Days(Object obj)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormatPropValue);
		if (obj == null)
		{
			return null;
		}
		String inputDate = obj.toString().trim();
		if (inputDate.trim().length() == 0)
			return null;
		// inputDate = inputDate.substring(0,inputDate.lastIndexOf("."));
		Date ts = (Date) obj;

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, +(5*24));

		Date calDate = cal.getTime();
		String date = null;
		if (calDate.compareTo(ts) > 0)
		{
			logger.debug("prefered date" + ts + " is smaller to expected date "
					+ calDate);

			date = formatter.format(calDate);

		} else
		{
			date = formatter.format(ts);
		}

		logger.debug("Returing date for mq " + date);
		return date;
	}
	
	public static String getSiebelDateFormatter(String dateString) {

		try {

			SimpleDateFormat siebelDateFormatPropValueOfETR = new SimpleDateFormat(siebelDateFormatPropValue);
			
			SimpleDateFormat dbFormatDateETR = new SimpleDateFormat(dbFormatDate);
			
			Date date = dbFormatDateETR.parse(dateString);
			
			String formattedDateInSiebel = siebelDateFormatPropValueOfETR.format(date).toString();
			
			return formattedDateInSiebel;

		} catch (Exception e) {
			
			return "";
		}

	}	
}
