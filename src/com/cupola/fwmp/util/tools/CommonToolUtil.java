/**
* @Author kiran  Sep 22, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.util.tools;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.AddressType;
import com.cupola.fwmp.FWMPConstant.ApiAction;
import com.cupola.fwmp.FWMPConstant.CustomerType;
import com.cupola.fwmp.FWMPConstant.FWMP_Flows.Copper_Flow;
import com.cupola.fwmp.FWMPConstant.FWMP_Flows.Fiber_Flow;
import com.cupola.fwmp.FWMPConstant.FWMP_Flows.Sales_Flow;
import com.cupola.fwmp.FWMPConstant.FWMP_Flows.SibelWoCloserCodes;
import com.cupola.fwmp.FWMPConstant.FWMP_Flows.SiebelWorkOrderStatus;
import com.cupola.fwmp.FWMPConstant.SiebelAddressType;
import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.city.CityDAOImpl;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.user.UserDaoImpl;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.tool.CRMCoreServices;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.TicketVo;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.TransactionHistoryLogger;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateProspectVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateWorkOrderVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ConsolidatedFrVoForSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.CreateUpdateFaultRepairRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.FWMPSRUpsert_Input;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ListOfFwmpsrthinio;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ListOfServiceRequest_Lightweight_ACTPriotization;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ServiceRequest_Lightweight;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ServiceRequest_Lightweight_ACTPriotization;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ACTListMgmtProspectiveContactBC_ACTContactListMVL;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ActCutAddressThinBc;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ActListMgmtProspectiveContactBc;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ConsolidatedProspectVoForSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspect_Input;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ListOfActCutAddressThinBc;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ListOfActcreateprospectthinio;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.Action;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.ComWorkOrder_Orders;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.ComWorkOrder_Orders_ACTPriotization;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.ConsolidatedWorkOrderVoForSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.ListOfAction;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.ListOfComWorkOrder_Orders_ACTPriotization;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.ListOfComworkorderio;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.UpdateWorkOrder_Input;

/**
 * @Author kiran Sep 22, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CommonToolUtil {

	private static Logger log = LogManager.getLogger(CommonToolUtil.class.getName());

	@Autowired
	CRMCoreServices crmCoreServices;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	static UserDao userDao;
	
	@Autowired
	AreaDAO areaDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	CityDAO cityDAO;

	private static final String EMAIL_VALIDATION_REG_EXPRESSION = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
	private static final String MOBILE_VALIDATION_REG_EXPRESSION = "^([6-9]{1})([0-9]{9})$";
	private static final String PIN_CODE_VALIDATION_REG_EXPRESSION = "^[1-9][0-9]{5}$";
	private static final String MAC_PATTERN = "^[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}$";
	private static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-4])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-4])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-4])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-4])$";

	/**
	 * @author kiran APIResponse
	 * @param createUpdateProspectVo
	 * @return
	 */
	
	public APIResponse validateSiebelProspectData(CreateUpdateProspectVo createUpdateProspectVo) {

		log.info(" createUpdateProspectVo " + createUpdateProspectVo);

		if (createUpdateProspectVo != null) {

			try {
				if (createUpdateProspectVo.getAction() == null || createUpdateProspectVo.getAction().isEmpty())
					return ResponseUtil.createNoActionFromSiebel();

				else if (createUpdateProspectVo.getProspectNumber() == null
						|| createUpdateProspectVo.getProspectNumber().isEmpty())
					return ResponseUtil.createNOProspectNumberFromSiebel();

				else if (createUpdateProspectVo.getAction().equalsIgnoreCase(ApiAction.CREATE)) {

					log.debug("Valid action type " + ApiAction.CREATE);
					if (crmCoreServices
							.getProspectByProspectNumber(createUpdateProspectVo.getProspectNumber()) != null) {
						return ResponseUtil.createpDuplicateProspect();
					}

				} else if (createUpdateProspectVo.getAction().equalsIgnoreCase(ApiAction.UPDATE))
					log.debug("Valid action type " + ApiAction.UPDATE);
				else
					return ResponseUtil.createInvalidActionType();

				if (createUpdateProspectVo.getAction().equalsIgnoreCase(ApiAction.UPDATE)
						&& (createUpdateProspectVo.getAssignedTo() == null
						|| !UserDaoImpl.cachedUsersByLoginId
								.containsKey(createUpdateProspectVo.getAssignedTo().toUpperCase())))
					return ResponseUtil.createNoUserFound();

				 if (createUpdateProspectVo.getTransactionId() == null
						|| createUpdateProspectVo.getTransactionId().isEmpty())
					return ResponseUtil.createNoTransactionId();

				else if (createUpdateProspectVo.getDeDupFlag() == null
						|| createUpdateProspectVo.getDeDupFlag().isEmpty())
					return ResponseUtil.createInvalidDeDupFlagFromSiebel();

				else if (createUpdateProspectVo.getDeDupFlag() != null) {
					log.info("Dedup flag is  " + createUpdateProspectVo.getDeDupFlag());
					try {
						int value = Integer.valueOf(createUpdateProspectVo.getDeDupFlag());

						if (value >= 0 && value < 2) {

						} else
							return ResponseUtil.createInvalidDeDupFlagFromSiebel();

					} catch (Exception e) {
						return ResponseUtil.createInvalidDeDupFlagFromSiebel();
					}
				}

				if (createUpdateProspectVo.getCustomerType() == null
						|| createUpdateProspectVo.getCustomerType().isEmpty())
					return ResponseUtil.createNOCustomerType();

				else if (createUpdateProspectVo.getCityName() == null
						|| createUpdateProspectVo.getCityName().isEmpty()) {
					log.info("getCityName  is  " + createUpdateProspectVo.getCityName());
					return ResponseUtil.createInvalidCityName();
				} else if (createUpdateProspectVo.getBranchName() == null
						|| createUpdateProspectVo.getBranchName().isEmpty()) {
					log.info("getBranchName  is  " + createUpdateProspectVo.getBranchName());
					return ResponseUtil.createNoBranchNameFromSiebel();
				} else if (createUpdateProspectVo.getAreaName() == null
						|| createUpdateProspectVo.getAreaName().isEmpty())
					return ResponseUtil.createNoAreaNameFromSiebel();

				else if (createUpdateProspectVo.getSubAreaName() == null
						|| createUpdateProspectVo.getSubAreaName().isEmpty())
					return ResponseUtil.createNoSubAreaNameFromSiebel();

				else if (createUpdateProspectVo.getMobileNumber() == null
						|| createUpdateProspectVo.getMobileNumber().isEmpty())
					return ResponseUtil.createNoMobileNumber();
				else if (!isMobileNumberValid(createUpdateProspectVo.getMobileNumber()))
					return ResponseUtil.createInvalidMobileNumber();

				if (createUpdateProspectVo.getEmailId() == null || createUpdateProspectVo.getEmailId().isEmpty())
					return ResponseUtil.createNoEmailId();
				else if (!isEmailValid(createUpdateProspectVo.getEmailId()))
					return ResponseUtil.createInvalidEmailId();

				if (createUpdateProspectVo.getHow_To_Know_ACT() == null
						|| createUpdateProspectVo.getHow_To_Know_ACT().isEmpty())
					return ResponseUtil.createNoKnownSourceFROMSiebel();
				else if (!definitionCoreService
						.getHowDiducome2knowAbtACTByValues(createUpdateProspectVo.getHow_To_Know_ACT()))
					return ResponseUtil.createInValidgetHowToKnowACT();

				if (createUpdateProspectVo.getCustomerProfile() == null
						|| createUpdateProspectVo.getCustomerProfile().isEmpty())
					return ResponseUtil.createNoCustomerProfession();
				else if (!definitionCoreService.getProfessionByValues(createUpdateProspectVo.getCustomerProfile()))
					return ResponseUtil.createInvalidCustomerProfession();

				if (createUpdateProspectVo.getTitle() == null || createUpdateProspectVo.getTitle().isEmpty())
					return ResponseUtil.createNoTiltle();
				else if (!definitionCoreService.getTitlesByValues(createUpdateProspectVo.getTitle()))
					return ResponseUtil.createInValidTitle();

				if (createUpdateProspectVo.getFirstName() == null || createUpdateProspectVo.getFirstName().isEmpty())
					return ResponseUtil.createNoFirstName();

				else if (createUpdateProspectVo.getLastName() == null || createUpdateProspectVo.getLastName().isEmpty())
					return ResponseUtil.createNoLastName();

				else if (createUpdateProspectVo.getCustomerType() == null
						|| createUpdateProspectVo.getCustomerType().isEmpty())
					return ResponseUtil.createNOCustomerType();

				// if
				// (createUpdateProspectVo.getAction().equalsIgnoreCase(ApiAction.CREATE)
				// && createUpdateProspectVo.getPreferredCallDate() != null
				// && !createUpdateProspectVo.getPreferredCallDate().isEmpty())
				// {
				// DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy
				// HH:mm:ss");
				// Date preferedCallDate = null;
				//
				// try {
				//
				// preferedCallDate =
				// formatter.parse(createUpdateProspectVo.getPreferredCallDate());
				//
				// if (!validatePreferedCallDate(preferedCallDate))
				// return ResponseUtil.createInvalidPreferedCallDate();
				//
				// } catch (Exception e) {
				//
				// log.error("Error while parsing prefered call date from
				// validator :: " + e.getMessage());
				// return ResponseUtil.createInvalidPreferedCallDate();
				// }
				//
				// }

				if (createUpdateProspectVo.getEnquiryType() == null
						|| createUpdateProspectVo.getEnquiryType().isEmpty())
					return ResponseUtil.createNoEnquiryType();

				else if (!definitionCoreService.getInqueryTypeByValues(createUpdateProspectVo.getEnquiryType()))
					return ResponseUtil.createInvalidEnquiryType();

				if (createUpdateProspectVo.getSource() == null || createUpdateProspectVo.getSource().isEmpty())
					return ResponseUtil.createNOSource();

				else if (createUpdateProspectVo.getCurrentAddress() == null)
					return ResponseUtil.createNOAddressFound();

				else if (createUpdateProspectVo.getCurrentAddress() != null) {

					if (createUpdateProspectVo.getCurrentAddress().getAddress1() == null
							|| createUpdateProspectVo.getCurrentAddress().getAddress1().isEmpty())
						return ResponseUtil.createInvalidAddress();

					if (createUpdateProspectVo.getCurrentAddress().getPincode() == null
							|| createUpdateProspectVo.getCurrentAddress().getPincode().isEmpty())
						return ResponseUtil.createInvalidAddress();
					else if (!isZipCodeValid(createUpdateProspectVo.getCurrentAddress().getPincode()))
						return ResponseUtil.createInvalidAddress();
				}

			/*	if (createUpdateProspectVo.getSubAttribute() == null
						|| createUpdateProspectVo.getSubAttribute().isEmpty())
					return ResponseUtil.createNoSubAttributeeFromSiebel();*/

				else if (createUpdateProspectVo.getProspectType() == null
						|| createUpdateProspectVo.getProspectType().isEmpty())
					return ResponseUtil.createNoProspectTypeFromSiebel();
				else if (!definitionCoreService.getProspectTypeByValues(createUpdateProspectVo.getProspectType()))
					return ResponseUtil.createInvalidProspectTypeFromSiebel();

				if (createUpdateProspectVo.getTransactionId() != null
						|| !createUpdateProspectVo.getTransactionId().isEmpty()) {
					TransactionHistoryLogger transactionHistoryLogger = new TransactionHistoryLogger();
					transactionHistoryLogger.setAddedOnDate(new Date());
					transactionHistoryLogger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					transactionHistoryLogger.setTransactionId(createUpdateProspectVo.getTransactionId());
					APIResponse responseForValidTransaction = crmCoreServices
							.getTransactionById(transactionHistoryLogger);
					if (responseForValidTransaction != null)
						return responseForValidTransaction;
					// crmCoreServices.getTransactionById(createUpdateProspectVo.getTransactionId());
				}

				return validateCityBranchAreaMapping(createUpdateProspectVo.getCityName(),
						createUpdateProspectVo.getBranchName(), createUpdateProspectVo.getAreaName(),createUpdateProspectVo.getSubAreaName(),true);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;

	}

	/**
	 * @author kiran APIResponse
	 * @param createUpdateProspectVo
	 * @return
	 */
	private APIResponse validateCityBranchAreaMapping(String cityName, String branchName, String areaName , String subAreaName ,boolean isProspect) {

		log.info("########################## City  " + cityName + " branch " + branchName + " Area " + areaName + "Sub Area " + subAreaName);

		Long cityId = null;
		Long branchId = null;
		Long areaId = null;

		if (cityName != null) {

			cityId = LocationCache.getCityDAO().getCityIdByName(cityName.toUpperCase());
			log.info(" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ cityId " + cityId);
			if (cityId == null || cityId == 0)
				return ResponseUtil.createNoCityNameMapping();

		} else {

			return ResponseUtil.createNoCityNameFromSiebel();
		}
		if (branchName != null) {

			if (branchName.equalsIgnoreCase(FWMPConstant.REALTY)) {
				log.info("FWMPConstant.REALTY");
				branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
						branchName.toUpperCase().trim(),
						cityName.toUpperCase());	
				log.info(" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ branchId " + branchId);
				
			}else
			{
				branchId = LocationCache.getBranchDAO().getBranchIdByName(branchName.toUpperCase());
				log.info(" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ branchId " + branchId);
			}
			
			if (branchId == null || branchId == 0)
				return ResponseUtil.createNoBranchNameMapping();
			else {

				if (!LocationCache.getBranchesByCityId(cityId.toString()).containsKey(branchId))
					return ResponseUtil.createNoBranchNameMapping();
			}
		} else {

			return ResponseUtil.createNoBranchNameFromSiebel();
		}
		if (areaName != null) {

			areaId = LocationCache.getAreaDAO().getAreaIdByName(branchName+"_"+areaName);
			log.info(" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ areaId " + areaId);
			if (areaId == null || areaId == 0)
				return ResponseUtil.createNoAreaNameMapping();
			else {

				if (!LocationCache.getAreasByBranchId(branchId.toString()).containsKey(areaId))
					return ResponseUtil.createNoAreaNameMapping();
			}
		} else {
			return ResponseUtil.createNoAreaNameFromSiebel();
		}
		if(isProspect)
		{
			if (subAreaName != null) {

				Long subAreaId = LocationCache.getSubAreaDAO().getSubAreaIdByName(areaId+"_"+subAreaName);
				log.info(" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ subAreaId " + subAreaId);
				if (subAreaId == null || subAreaId == 0)
					return ResponseUtil.createNoSubAreaNameMapping();
				else {

					if (!LocationCache.getSubAreasByAreaId(areaId).containsKey(subAreaId))
						return ResponseUtil.createNoSubAreaNameMapping();
				}
			} else {
				return ResponseUtil.createNoSubAreaNameFromSiebel();
			}
		}
		
		return null;

	}

	/**
	 * @author kiran void
	 * @param preferedCallDate
	 */
	private static boolean validatePreferedCallDate(Date preferedCallDate) {

		Date currentDate = new Date();
		int PREFFERED_DATE_HOURS_DIFFERENCE = 0;

		long getDiff = preferedCallDate.getTime() - currentDate.getTime();
		long getHoursDiff = getDiff / (60 * 60 * 1000);

		log.info("Difference value :" + getHoursDiff);

		if (getHoursDiff > PREFFERED_DATE_HOURS_DIFFERENCE)
			return true;
		else
			return false;
	}

	/**
	 * @author kiran ProspectCoreVO
	 * @param createUpdateProspectVo
	 * @return
	 */
	// removed static for issue for getting from property file manjuprasad.
	public ProspectCoreVO convertProspectDataToFWMPFormate(CreateUpdateProspectVo createUpdateProspectVo) {

		ProspectCoreVO prospectVo = new ProspectCoreVO();

		CustomerVO customer = new CustomerVO();

		TicketVo ticket = new TicketVo();

		log.info("LocationCache.getCityDAO().getCityIdByName(createUpdateProspectVo.getCityName()) "
				+ LocationCache.getCityDAO().getCityIdByName(createUpdateProspectVo.getCityName().toUpperCase())
				+ "createUpdateProspectVo.getCityName().toUpperCase()" + createUpdateProspectVo.getCityName());

		customer.setCityId(
				LocationCache.getCityDAO().getCityIdByName(createUpdateProspectVo.getCityName().toUpperCase()));

		log.info("LocationCache.getBranchDAO().getBranchIdByName(createUpdateProspectVo.getBranchName())"
				+ LocationCache.getBranchDAO().getBranchIdByName(createUpdateProspectVo.getBranchName().toUpperCase()));

		customer.setBranchId(
				LocationCache.getBranchDAO().getBranchIdByName(createUpdateProspectVo.getBranchName().toUpperCase()));

		log.info("LocationCache.getAreaDAO().getAreaIdByName(createUpdateProspectVo.getAreaName())"
				+ LocationCache.getAreaDAO().getAreaIdByName(createUpdateProspectVo.getBranchName()+"_"+createUpdateProspectVo.getAreaName()));

		customer.setAreaId(LocationCache.getAreaDAO().getAreaIdByName(createUpdateProspectVo.getBranchName()+"_"+createUpdateProspectVo.getAreaName()));

		if (createUpdateProspectVo.getNotes() != null)
			customer.setNotes(createUpdateProspectVo.getNotes());

		if (createUpdateProspectVo.getTitle() != null)
			customer.setTitle(createUpdateProspectVo.getTitle());

		if (createUpdateProspectVo.getFirstName() != null)
			customer.setFirstName(createUpdateProspectVo.getFirstName());

		if (createUpdateProspectVo.getMiddleName() != null)
			customer.setMiddleName(createUpdateProspectVo.getMiddleName());

		if (createUpdateProspectVo.getLastName() != null)
			customer.setLastName(createUpdateProspectVo.getLastName());

		/*
		 * if (customer.getPrefferedCallDate() != null)
		 * customer.setPrefferedCallDate(createUpdateProspectVo.
		 * getPreferedCallDate());
		 */
		if (createUpdateProspectVo.getMobileNumber() != null)
			customer.setMobileNumber(createUpdateProspectVo.getMobileNumber());

		if (createUpdateProspectVo.getAlternativeMobileNo() != null)
			customer.setAlternativeMobileNo(createUpdateProspectVo.getAlternativeMobileNo());

		if (createUpdateProspectVo.getEmailId() != null)
			customer.setEmailId(createUpdateProspectVo.getEmailId());

		if (createUpdateProspectVo.getAlternativeEmailId() != null)
			customer.setAlternativeEmailId(createUpdateProspectVo.getAlternativeEmailId());

		if (createUpdateProspectVo.getCustomerProfile() != null)
			customer.setCustomerProfession(createUpdateProspectVo.getCustomerProfile());

		if (createUpdateProspectVo.getOfficeNumber() != null)
			customer.setOfficeNumber(createUpdateProspectVo.getOfficeNumber());

		if (createUpdateProspectVo.getCustomerType() != null)
			customer.setCustomerType(createUpdateProspectVo.getCustomerType());
		
		if(createUpdateProspectVo.getExistingISP() != null)
			customer.setExistingIsp(createUpdateProspectVo.getExistingISP());
		
		//added a new getter setter in manjuprasad
/*		if(createUpdateProspectVo.getHow_To_Know_ACT() != null)
			customer.setKnownSource(createUpdateProspectVo.getHow_To_Know_ACT());*/
		//changed for manjuprasad for getting the known source with Id
		if(createUpdateProspectVo.getHow_To_Know_ACT() != null)
			customer.setKnownSource(definitionCoreService.getSourceById(Long.parseLong(createUpdateProspectVo.getHow_To_Know_ACT()), customer.getCityId()));

		ObjectMapper mapper = new ObjectMapper();
		try {
			if (createUpdateProspectVo.getCurrentAddress() != null) {
				createUpdateProspectVo.getCurrentAddress().setCustomerAddressType("currentAddress");
				customer.setCurrentAddress(mapper.writeValueAsString(createUpdateProspectVo.getCurrentAddress()));
			}
			if (createUpdateProspectVo.getPermanentAddress() != null) {
				
				createUpdateProspectVo.getPermanentAddress().setCustomerAddressType("permanentAddress");
				customer.setPermanentAddress(mapper.writeValueAsString(createUpdateProspectVo.getPermanentAddress()));
				log.info("createUpdateProspectVo.getPermanentAddress() :: "+customer.getPermanentAddress());
			}
			if (createUpdateProspectVo.getCommunicationAddress() != null) {
				createUpdateProspectVo.getCommunicationAddress().setCustomerAddressType("communicationAddress");
				customer.setCommunicationAdderss(
						mapper.writeValueAsString(createUpdateProspectVo.getCommunicationAddress()));
				log.info("createUpdateProspectVo.getCommunicationAdderss() :: "+customer.getCommunicationAdderss());
			}
			log.info("createUpdateProspectVo.getCommunicationAdderss() :: "+createUpdateProspectVo.getCommunicationAddress());
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * if(createUpdateProspectVo.getAssignedTo() != null)
		 * ticket.setAssignTo(Long.valueOf(createUpdateProspectVo.getAssignedTo(
		 * )));
		 */

		ticket.setProspectNo(createUpdateProspectVo.getProspectNumber());

		if (createUpdateProspectVo.getDeDupFlag() != null && !createUpdateProspectVo.getDeDupFlag().isEmpty())
			ticket.setDeDupFlag(Integer.valueOf(createUpdateProspectVo.getDeDupFlag()));
		

		log.info("LocationCache.getSubAreaDAO().getSubAreasByAreaName(createUpdateProspectVo.getSubAreaName())"
				+ LocationCache.getSubAreaDAO().getSubAreasByAreaName(createUpdateProspectVo.getAreaName()));

		ticket.setAreaId(LocationCache.getAreaDAO().getAreaIdByName(createUpdateProspectVo.getBranchName()+"_"+createUpdateProspectVo.getAreaName()));
		if(createUpdateProspectVo.getSubAreaName() != null && !createUpdateProspectVo.getSubAreaName().isEmpty())
		{
			log.info("LocationCache.getSubAreaDAO().getSubAreaIdByName(createUpdateProspectVo.getSubAreaName()) :: "+LocationCache.getSubAreaDAO().getSubAreaIdByName(ticket.getAreaId()+"_"+createUpdateProspectVo.getSubAreaName()));
			ticket.setSubAreaId(LocationCache.getSubAreaDAO().getSubAreaIdByName(ticket.getAreaId()+"_"+createUpdateProspectVo.getSubAreaName()));
		}
	//modifed for source seibel issue by manjuprasad	 
		if (createUpdateProspectVo.getSource() !=null) {
			ticket.setSource(createUpdateProspectVo.getSource());
		}

		prospectVo.setCustomer(customer);
		prospectVo.setTicket(ticket);
		return prospectVo;
	}

	/**
	 * @author kiran CreateUpdateProspectVo
	 * @param prospectCoreVO
	 * @return
	 */
	public static CreateUpdateProspectVo convertProspectDataToSiebelFormate(ProspectCoreVO prospectCoreVO) {

		// reason field not set (From customer remarks)

		if (prospectCoreVO != null) {
			CreateUpdateProspectVo createUpdateProspectVo = new CreateUpdateProspectVo();

			if (prospectCoreVO.getCustomer() != null) {

				CustomerVO customerVo = prospectCoreVO.getCustomer();

				if (customerVo.getTitle() != null)
					createUpdateProspectVo.setTitle(customerVo.getTitle());

				if (customerVo.getFirstName() != null)
					createUpdateProspectVo.setFirstName(customerVo.getFirstName());

				if (customerVo.getLastName() != null)
					createUpdateProspectVo.setLastName(customerVo.getLastName());

				if (customerVo.getMiddleName() != null)
					createUpdateProspectVo.setMiddleName(customerVo.getMiddleName());

				if (customerVo.getPrefferedCallDate() != null)
					createUpdateProspectVo
							.setPreferredCallDate(GenericUtil.convertToUiDateFormat(customerVo.getPrefferedCallDate()));

				if (customerVo.getCustomerType() != null)
					createUpdateProspectVo.setCustomerType(customerVo.getCustomerType());

				if (customerVo.getMobileNumber() != null)
					createUpdateProspectVo.setMobileNumber(customerVo.getMobileNumber());

				if (customerVo.getAlternativeMobileNo() != null)
					createUpdateProspectVo.setAlternativeMobileNo(customerVo.getAlternativeMobileNo());

				if (customerVo.getOfficeNumber() != null)
					createUpdateProspectVo.setOfficeNumber(customerVo.getOfficeNumber());

				if (customerVo.getEmailId() != null)
					createUpdateProspectVo.setEmailId(customerVo.getEmailId());

				if (customerVo.getAlternativeEmailId() != null)
					createUpdateProspectVo.setAlternativeEmailId(customerVo.getAlternativeEmailId());

				if (customerVo.getCustomerProfession() != null)
					createUpdateProspectVo.setCustomerProfession(customerVo.getCustomerProfession());

				if (customerVo.getNotes() != null)
					createUpdateProspectVo.setNotes(customerVo.getNotes());

				if (customerVo.getCityId() != null && customerVo.getCityId() > 0)
					createUpdateProspectVo.setCityName(LocationCache.getCityNameById(customerVo.getCityId()));

				if (customerVo.getBranchId() != null && customerVo.getBranchId() > 0)
					createUpdateProspectVo.setBranchName(LocationCache.getBrancheNameById(customerVo.getBranchId()));

				if (customerVo.getAreaId() != null && customerVo.getAreaId() > 0)
					createUpdateProspectVo.setAreaName(LocationCache.getAreaNameById(customerVo.getAreaId()));

				if (customerVo.getKnownSource() != null)
					createUpdateProspectVo.setHow_To_Know_ACT(customerVo.getKnownSource());

				ObjectMapper mapper = new ObjectMapper();
				try {
					if (customerVo.getCurrentAddress() != null)
						createUpdateProspectVo.setCurrentAddress(
								mapper.readValue(customerVo.getCurrentAddress(), CustomerAddressVO.class));
					if (customerVo.getPermanentAddress() != null)
						createUpdateProspectVo.setPermanentAddress(
								mapper.readValue(customerVo.getPermanentAddress(), CustomerAddressVO.class));
					if (customerVo.getCommunicationAdderss() != null)
						createUpdateProspectVo.setCommunicationAddress(
								mapper.readValue(customerVo.getCommunicationAdderss(), CustomerAddressVO.class));
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (customerVo.getKnownSource() != null)
					createUpdateProspectVo.setSource(customerVo.getKnownSource());

				if (customerVo.getCustomerType() != null)
					createUpdateProspectVo.setProspectType(customerVo.getCustomerType());

				if (customerVo.getCustomerProfession() != null)
					createUpdateProspectVo.setCustomerProfession(customerVo.getCustomerProfession());

				if (customerVo.getCrpAccountNo() != null)
					createUpdateProspectVo.setCRPAccountNumber(customerVo.getCrpAccountNo());

				if (customerVo.getCrpMobileNo() != null)
					createUpdateProspectVo.setCRPMobileNumber(customerVo.getCrpMobileNo());

			}

			if (prospectCoreVO.getTicket() != null) {
				TicketVo ticketVo = prospectCoreVO.getTicket();

				if (ticketVo.getProspectNo() != null)
					createUpdateProspectVo.setProspectNumber(ticketVo.getProspectNo());

				if (ticketVo.getAssignTo() > 0)
					createUpdateProspectVo.setAssignedTo(userDao.getUserById(ticketVo.getAssignTo()).getLoginId());

				if (ticketVo.getMqStatus() != null)
					createUpdateProspectVo.setDeDupFlag(ticketVo.getMqStatus());

				if (ticketVo.getSubAttribute() != null)
					createUpdateProspectVo.setSubAttribute(ticketVo.getSubAttribute());
			}

			return createUpdateProspectVo;
		}

		else
			return new CreateUpdateProspectVo();

	}

	/**
	 * @author kiran APIResponse
	 * @param createUpdateWorkOrderVo
	 * @return
	 */
	public APIResponse validateSiebelWorkOrderData(CreateUpdateWorkOrderVo createUpdateWorkOrderVo) {

		if (createUpdateWorkOrderVo.getAction() == null || createUpdateWorkOrderVo.getAction().isEmpty())
			return ResponseUtil.createNoActionFromSiebel();

		if (createUpdateWorkOrderVo.getWorkOrderNumber() == null
				|| createUpdateWorkOrderVo.getWorkOrderNumber().isEmpty())
			return ResponseUtil.createNoWoNumber();

		if (createUpdateWorkOrderVo.getAction().equalsIgnoreCase(ApiAction.CREATE)) {
			log.debug("Valid action type " + ApiAction.CREATE);
			if (crmCoreServices.getWorkOrderByWorkOrderNumber(createUpdateWorkOrderVo.getWorkOrderNumber()) != null) {
				return ResponseUtil.createDuplicateWorkOrder();
			}

		} else if (createUpdateWorkOrderVo.getAction().equalsIgnoreCase(ApiAction.UPDATE))
			log.debug("Valid action type " + ApiAction.UPDATE);
		else
			return ResponseUtil.createInvalidActionType();

		if (createUpdateWorkOrderVo.getAction().equalsIgnoreCase(ApiAction.UPDATE)
				&& (createUpdateWorkOrderVo.getAssignedTo() == null
				|| !UserDaoImpl.cachedUsersByLoginId.containsKey(createUpdateWorkOrderVo.getAssignedTo().toUpperCase())))
			return ResponseUtil.createNoUserFound();

		if (createUpdateWorkOrderVo.getWorkOrderNumber() == null
				|| createUpdateWorkOrderVo.getWorkOrderNumber().isEmpty())
			return ResponseUtil.createNoWoNumber();

		if (createUpdateWorkOrderVo.getTransactionId() == null || createUpdateWorkOrderVo.getTransactionId().isEmpty())
			return ResponseUtil.createNoTransactionId();
		else if (createUpdateWorkOrderVo.getTransactionId() != null
				|| !createUpdateWorkOrderVo.getTransactionId().isEmpty()) {
			TransactionHistoryLogger transactionHistoryLogger = new TransactionHistoryLogger();
			transactionHistoryLogger.setAddedOnDate(new Date());
			transactionHistoryLogger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			transactionHistoryLogger.setTransactionId(createUpdateWorkOrderVo.getTransactionId());
			APIResponse responseForValidTransactionID = crmCoreServices.getTransactionById(transactionHistoryLogger);
			if (responseForValidTransactionID != null)
				return responseForValidTransactionID;// if mongo is agrred
														// uncoment this and
														// commnet other

			// return
			// crmCoreServices.getTransactionById(createUpdateWorkOrderVo.getTransactionId());
		}

		if (createUpdateWorkOrderVo.getAction() != null) {

			if (createUpdateWorkOrderVo.getProspectNumber() == null
					|| createUpdateWorkOrderVo.getProspectNumber().isEmpty())
				return ResponseUtil.createNOProspectNumberFromSiebel();

			if (createUpdateWorkOrderVo.getCustomerNumber() == null
					|| createUpdateWorkOrderVo.getCustomerNumber().isEmpty())
				return ResponseUtil.createNOCusmoterNumber();

			if (createUpdateWorkOrderVo.getCityName() == null || createUpdateWorkOrderVo.getCityName().isEmpty())
				return ResponseUtil.createInvalidCityName();

			if (createUpdateWorkOrderVo.getBranchName() == null || createUpdateWorkOrderVo.getBranchName().isEmpty())
				return ResponseUtil.createNoBranchNameFromSiebel();

			if (createUpdateWorkOrderVo.getAreaName() == null || createUpdateWorkOrderVo.getAreaName().isEmpty())
				return ResponseUtil.createNoAreaNameFromSiebel();

			if (createUpdateWorkOrderVo.getFirstName() == null || createUpdateWorkOrderVo.getFirstName().isEmpty())
				return ResponseUtil.createNoFirstName();

			if (createUpdateWorkOrderVo.getLastName() == null || createUpdateWorkOrderVo.getLastName().isEmpty())
				return ResponseUtil.createNoLastName();

			if (createUpdateWorkOrderVo.getCurrentAddress() == null)
				return ResponseUtil.createNOAddressFound();

			if (createUpdateWorkOrderVo.getCurrentAddress() != null) {

				if (createUpdateWorkOrderVo.getCurrentAddress().getAddress1() == null
						|| createUpdateWorkOrderVo.getCurrentAddress().getAddress1().isEmpty())
					return ResponseUtil.createInvalidAddress();

				if (createUpdateWorkOrderVo.getCurrentAddress().getPincode() == null
						|| createUpdateWorkOrderVo.getCurrentAddress().getPincode().isEmpty())
					return ResponseUtil.createInvalidAddress();
				else if (!isZipCodeValid(createUpdateWorkOrderVo.getCurrentAddress().getPincode()))
					return ResponseUtil.createInvalidAddress();

			}

			if (createUpdateWorkOrderVo.getCustomerMobile() == null
					|| createUpdateWorkOrderVo.getCustomerMobile().isEmpty())
				return ResponseUtil.createNoMobileNumber();
			else if (!isMobileNumberValid(createUpdateWorkOrderVo.getCustomerMobile()))
				return ResponseUtil.createInvalidMobileNumber();

			if (createUpdateWorkOrderVo.getTicketCategory() == null
					|| createUpdateWorkOrderVo.getTicketCategory().isEmpty())
				return ResponseUtil.createNoTicketCategory();

			if (createUpdateWorkOrderVo.getSymptom() == null || createUpdateWorkOrderVo.getSymptom().isEmpty())
				return ResponseUtil.createNoSymptom();

			if (createUpdateWorkOrderVo.getRequestDate() == null)
				return ResponseUtil.createNoRequestDate();

			if (createUpdateWorkOrderVo.getStatus() == null || createUpdateWorkOrderVo.getStatus().isEmpty())
				return ResponseUtil.createNOStatus();

			return validateCityBranchAreaMapping(createUpdateWorkOrderVo.getCityName(),
					createUpdateWorkOrderVo.getBranchName(), createUpdateWorkOrderVo.getAreaName(),null,false);

		}

		return null;

	}

	/**
	 * @author kiran APIResponse
	 * @param createUpdateFrVo
	 * @return
	 */
	public APIResponse validateSiebelFRWorkOrderData(CreateUpdateFrVo createUpdateFrVo) {

		if (createUpdateFrVo.getAction() == null || createUpdateFrVo.getAction().isEmpty())
			return ResponseUtil.createNoActionFromSiebel();

		if (createUpdateFrVo.getAction().equalsIgnoreCase(ApiAction.CREATE))

		{
			log.debug("Valid action type " + ApiAction.CREATE);

			if (crmCoreServices.getFrWOByFrWONumber(createUpdateFrVo.getTicketNo()) != null) {
				return ResponseUtil.createDuplicateSR();
			}
		} else if (createUpdateFrVo.getAction().equalsIgnoreCase(ApiAction.UPDATE))
			log.debug("Valid action type " + ApiAction.UPDATE);
		else
			return ResponseUtil.createInvalidActionType();

		/*if (createUpdateFrVo.getCxPort() == null || createUpdateFrVo.getCxPort().isEmpty())
			return ResponseUtil.createNoCxPortFromSiebel();

		if (createUpdateFrVo.getFxPort() == null || createUpdateFrVo.getFxPort().isEmpty())
			return ResponseUtil.createNoFxPortFromSiebel();

		if (createUpdateFrVo.getFxName() == null || createUpdateFrVo.getFxName().isEmpty())
			return ResponseUtil.createNoFxNameFromSiebel();

		if (createUpdateFrVo.getCxIp() == null || createUpdateFrVo.getCxIp().isEmpty())
			return ResponseUtil.createNoCxIp();
		else if (!isIpValid(createUpdateFrVo.getCxIp()))
			return ResponseUtil.createInvalidCxIp();

		if (createUpdateFrVo.getFxName() == null || createUpdateFrVo.getFxName().isEmpty())
			return ResponseUtil.createNoFxName();

		if (createUpdateFrVo.getFxIp() == null || createUpdateFrVo.getFxIp().isEmpty())
			return ResponseUtil.createNoFxIp();
		else if (!isIpValid(createUpdateFrVo.getFxIp()))
			return ResponseUtil.createInvalidFxIp();
*/
		if (createUpdateFrVo.getTransactionId() == null || createUpdateFrVo.getTransactionId().isEmpty())
			return ResponseUtil.createNoTransactionId();

		else if (createUpdateFrVo.getTransactionId() != null || !createUpdateFrVo.getTransactionId().isEmpty()) {
			TransactionHistoryLogger transactionHistoryLogger = new TransactionHistoryLogger();
			transactionHistoryLogger.setAddedOnDate(new Date());
			transactionHistoryLogger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			transactionHistoryLogger.setTransactionId(createUpdateFrVo.getTransactionId());
			APIResponse responseForValidTransactionID = crmCoreServices.getTransactionById(transactionHistoryLogger);
			if (responseForValidTransactionID != null)
				return responseForValidTransactionID;

			// return
			// crmCoreServices.getTransactionById(createUpdateFrVo.getTransactionId());
		}

		if (createUpdateFrVo.getTicketNo() == null || createUpdateFrVo.getTicketNo().isEmpty())
			return ResponseUtil.createNoWoNumber();

		if (createUpdateFrVo.getAction().equalsIgnoreCase(ApiAction.UPDATE)
				&& (createUpdateFrVo.getAssignedTo() == null
				|| !UserDaoImpl.cachedUsersByLoginId.containsKey(createUpdateFrVo.getAssignedTo().toUpperCase())))
			return ResponseUtil.createNoUserFound();

		if (createUpdateFrVo.getAction() != null) {

			if (createUpdateFrVo.getAction().equalsIgnoreCase(ApiAction.CREATE)
					|| createUpdateFrVo.getAction().equalsIgnoreCase(ApiAction.UPDATE)) {

				/*
				 * if (createUpdateFrVo.getProspectNumber() == null ||
				 * createUpdateFrVo.getProspectNumber().isEmpty()) return
				 * ResponseUtil.createNOProspectNumberFromSiebel();
				 */

				if (createUpdateFrVo.getCustomerNumber() == null || createUpdateFrVo.getCustomerNumber().isEmpty())
					return ResponseUtil.createNOCusmoterNumber();

				/*
				 * else if (createUpdateFrVo.getCustomerName() == null ||
				 * createUpdateFrVo.getCustomerName().isEmpty()) return
				 * ResponseUtil.createNoFirstName();
				 */

				if (createUpdateFrVo.getMobileNumber() == null || createUpdateFrVo.getMobileNumber().isEmpty())
					return ResponseUtil.createNoMobileNumber();
				else if (!isMobileNumberValid(createUpdateFrVo.getMobileNumber()))
					return ResponseUtil.createInvalidMobileNumber();

				if (createUpdateFrVo.getCategoryName() == null || createUpdateFrVo.getCategoryName().isEmpty())
					return ResponseUtil.createNoTicketCategory();

				if (createUpdateFrVo.getTicketSource() == null || createUpdateFrVo.getTicketSource().isEmpty())
					return ResponseUtil.createNOSource();

				if (createUpdateFrVo.getSymptomName() == null || createUpdateFrVo.getSymptomName().isEmpty())
					return ResponseUtil.createNoSymptom();

				if (createUpdateFrVo.getServiceRequestDate() == null)
					return ResponseUtil.createNoRequestDate();

				if (createUpdateFrVo.getCategoryName() == null || createUpdateFrVo.getCategoryName().isEmpty())
					return ResponseUtil.createNoTicketCategory();

				if (createUpdateFrVo.getComments() == null || createUpdateFrVo.getComments().isEmpty())
					return ResponseUtil.createNoComments();

				if (createUpdateFrVo.getReopenCount() == null || createUpdateFrVo.getReopenCount().isEmpty())
					return ResponseUtil.createNoReopenCount();

				if (createUpdateFrVo.getFirstName() == null || createUpdateFrVo.getFirstName().isEmpty())
					return ResponseUtil.createNoFirstName();

				if (createUpdateFrVo.getLastName() == null || createUpdateFrVo.getLastName().isEmpty())
					return ResponseUtil.createNoLastName();

				if (createUpdateFrVo.getCity() == null || createUpdateFrVo.getCity().isEmpty())
					return ResponseUtil.createInvalidCityName();

				if (createUpdateFrVo.getBranch() == null || createUpdateFrVo.getBranch().isEmpty())
					return ResponseUtil.createNoBranchNameFromSiebel();

				if (createUpdateFrVo.getArea() == null || createUpdateFrVo.getArea().isEmpty())
					return ResponseUtil.createNoAreaNameFromSiebel();

				if (createUpdateFrVo.getCurrentAddress() == null)
					return ResponseUtil.createNOAddressFound();

				if (createUpdateFrVo.getStatus() == null || createUpdateFrVo.getStatus().isEmpty())
					return ResponseUtil.createNOStatus();

				if (createUpdateFrVo.getCurrentAddress() != null) {

					if (createUpdateFrVo.getCurrentAddress().getAddress1() == null
							|| createUpdateFrVo.getCurrentAddress().getAddress1().isEmpty())
						return ResponseUtil.createInvalidAddress();

					if (createUpdateFrVo.getCurrentAddress().getPincode() == null
							|| createUpdateFrVo.getCurrentAddress().getPincode().isEmpty())
						return ResponseUtil.createInvalidAddress();
					else if (!isZipCodeValid(createUpdateFrVo.getCurrentAddress().getPincode()))
						return ResponseUtil.createInvalidAddress();

				}

				return validateCityBranchAreaMapping(createUpdateFrVo.getCity(), createUpdateFrVo.getBranch(),
						createUpdateFrVo.getArea(),null,false);

			} else {

				return ResponseUtil.createInvalidActionType();
			}
		}

		return null;

	}

	/**
	 * @author kiran MQWorkorderVO
	 * @param createUpdateWorkOrderVo
	 * @return
	 */
	public static MQWorkorderVO convertWorkOrderDataToFWMPFormate(CreateUpdateWorkOrderVo createUpdateWorkOrderVo) {

		// reqDate

		if (createUpdateWorkOrderVo != null) {
			MQWorkorderVO mqWorkorderVO = new MQWorkorderVO();

			if (createUpdateWorkOrderVo.getWorkOrderNumber() != null)
				mqWorkorderVO.setWorkOrderNumber(createUpdateWorkOrderVo.getWorkOrderNumber());

			if (createUpdateWorkOrderVo.getProspectNumber() != null)
				mqWorkorderVO.setProspectNumber(createUpdateWorkOrderVo.getProspectNumber());

			if (createUpdateWorkOrderVo.getCustomerNumber() != null)
				mqWorkorderVO.setMqId(createUpdateWorkOrderVo.getCustomerNumber());

			if (createUpdateWorkOrderVo.getFirstName() != null)
				mqWorkorderVO.setCustomerName(createUpdateWorkOrderVo.getFirstName());

			if (createUpdateWorkOrderVo.getCurrentAddress() != null) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					mqWorkorderVO
							.setCustomerAddress(mapper.writeValueAsString(createUpdateWorkOrderVo.getCurrentAddress()));
				} catch (Exception e) {
					log.error("Error While converting customer addreess :: " + e.getMessage());
				}
			}

			if (createUpdateWorkOrderVo.getTicketCategory() != null)
				mqWorkorderVO.setTicketCategory(createUpdateWorkOrderVo.getTicketCategory());

			if (createUpdateWorkOrderVo.getCxIp() != null)
				mqWorkorderVO.setCxIp(createUpdateWorkOrderVo.getCxIp());

			if (createUpdateWorkOrderVo.getFxName() != null)
				mqWorkorderVO.setFxName(createUpdateWorkOrderVo.getFxName());

			if (createUpdateWorkOrderVo.getCityName() != null)
				mqWorkorderVO.setCityId(LocationCache.getCityDAO()
						.getCityIdByName(createUpdateWorkOrderVo.getCityName().toUpperCase()).toString());

			if (createUpdateWorkOrderVo.getBranchName() != null)
				mqWorkorderVO.setBranchId(LocationCache.getBranchDAO()
						.getBranchIdByName(createUpdateWorkOrderVo.getBranchName().toUpperCase()).toString());

			if (createUpdateWorkOrderVo.getAreaName() != null)
				mqWorkorderVO.setAreaId(
						LocationCache.getAreaDAO().getAreaIdByName(createUpdateWorkOrderVo.getBranchName()+"_"+createUpdateWorkOrderVo.getAreaName()).toString());

			if (createUpdateWorkOrderVo.getSymptom() != null)
				mqWorkorderVO.setSymptom(createUpdateWorkOrderVo.getSymptom());

			if (createUpdateWorkOrderVo.getRequestDate() != null)
				mqWorkorderVO.setReqDate(createUpdateWorkOrderVo.getRequestDate());

			return mqWorkorderVO;
		}

		return null;

	}

	public static MQWorkorderVO convertFRWorkOrderDataToFWMPFormate(CreateUpdateFrVo createUpdateFrVo) {

		if (createUpdateFrVo != null) {
			MQWorkorderVO mqWorkorderVO = new MQWorkorderVO();

			FrDeviceVo deviceVo = new FrDeviceVo();

			if (createUpdateFrVo.getTicketNo() != null)
				mqWorkorderVO.setWorkOrderNumber(createUpdateFrVo.getTicketNo());

			if (createUpdateFrVo.getProspectNumber() != null)
				mqWorkorderVO.setProspectNumber(createUpdateFrVo.getProspectNumber());

			if (createUpdateFrVo.getCustomerNumber() != null)
				mqWorkorderVO.setMqId(createUpdateFrVo.getCustomerNumber());

			if (createUpdateFrVo.getFirstName() != null)
				mqWorkorderVO.setCustomerName(createUpdateFrVo.getFirstName());

			if (createUpdateFrVo.getCurrentAddress() != null) {
				ObjectMapper mapper = new ObjectMapper();
				try {

					CustomerAddressVO addressVO = createUpdateFrVo.getCurrentAddress();
					addressVO.setCustomerAddressType(AddressType.CURRENT_ADDRESS);
					mqWorkorderVO.setCustomerAddress(mapper.writeValueAsString(addressVO));
				} catch (Exception e) {
					log.error("Error While converting customer addreess :: " + e.getMessage());
				}
			}

			if (createUpdateFrVo.getSymptomName() != null)
				mqWorkorderVO.setSymptom(createUpdateFrVo.getSymptomName());

			if (createUpdateFrVo.getCxIp() != null)
				deviceVo.setCxIp(createUpdateFrVo.getCxIp());

			if (createUpdateFrVo.getFxName() != null)
				deviceVo.setFxName(createUpdateFrVo.getFxName());

			if (createUpdateFrVo.getFxIp() != null)
				deviceVo.setFxIp(createUpdateFrVo.getFxIp());

			try {
				if (createUpdateFrVo.getCxPort() != null && !createUpdateFrVo.getCxPort().isEmpty())
					deviceVo.setCxPort(Integer.valueOf(createUpdateFrVo.getCxPort()));

				if (createUpdateFrVo.getFxPort() != null && !createUpdateFrVo.getFxPort().isEmpty())
					deviceVo.setFxPort(Integer.valueOf(createUpdateFrVo.getFxPort()));
			} catch (NumberFormatException e) {
				log.info("cxport or fxport are undefined :: "+e.getMessage());
			}

			if (createUpdateFrVo.getCity() != null)
				mqWorkorderVO.setCityId(LocationCache.getCityDAO()
						.getCityIdByName(createUpdateFrVo.getCity().toUpperCase()).toString());

			if (createUpdateFrVo.getBranch() != null)
				mqWorkorderVO.setBranchId(LocationCache.getBranchDAO()
						.getBranchIdByName(createUpdateFrVo.getBranch().toUpperCase()).toString());

			if (createUpdateFrVo.getArea() != null)
				mqWorkorderVO
						.setAreaId(LocationCache.getAreaDAO().getAreaIdByName(createUpdateFrVo.getBranch()+"_"+createUpdateFrVo.getArea()).toString());

			if (deviceVo != null)
				mqWorkorderVO.setDeviceData(deviceVo);

			if (createUpdateFrVo.getServiceRequestDate() != null)
				mqWorkorderVO.setReqDate(createUpdateFrVo.getServiceRequestDate());

			if (createUpdateFrVo.getCategoryName() != null)
				mqWorkorderVO.setTicketCategory(createUpdateFrVo.getCategoryName());

			if (createUpdateFrVo.getComments() != null)
				mqWorkorderVO.setComment(createUpdateFrVo.getComments());

			if (createUpdateFrVo.getCxIp() != null)
				mqWorkorderVO.setCxIp(createUpdateFrVo.getCxIp());

			if (createUpdateFrVo.getFxName() != null)
				mqWorkorderVO.setFxName(createUpdateFrVo.getFxName());

			if (createUpdateFrVo.getMobileNumber() != null)
				mqWorkorderVO.setCustomerMobile(createUpdateFrVo.getMobileNumber());

			if (createUpdateFrVo.getAssignedTo() != null)
			{
				if(UserDaoImpl.cachedUsersByLoginId.containsKey(createUpdateFrVo.getAssignedTo().toUpperCase()))
					mqWorkorderVO.setAssignedTo(
							UserDaoImpl.cachedUsersByLoginId.get(createUpdateFrVo.getAssignedTo().toUpperCase()).getId());
			}
				

			if (createUpdateFrVo.getVoc() != null)
				mqWorkorderVO.setVoc(createUpdateFrVo.getVoc());

			if (createUpdateFrVo.getAlternativeMobileNo() != null)
				mqWorkorderVO.setAltMobileNumber(createUpdateFrVo.getAlternativeMobileNo());
			
			if(createUpdateFrVo.getStatus() != null)
				mqWorkorderVO.setResponseStatus(createUpdateFrVo.getStatus());
			
			if(createUpdateFrVo.getServiceRequestDate() != null)
				mqWorkorderVO.setReqDate(createUpdateFrVo.getServiceRequestDate());

			ObjectMapper mapper = new ObjectMapper();
			try {
				mqWorkorderVO.setCustomerAddress(mapper.writeValueAsString(createUpdateFrVo.getCurrentAddress()));
			} catch (Exception e) {
			}
			return mqWorkorderVO;
		}

		return null;

	}

	/**
	 * @author aditya UpdateProspectVO
	 * @param prospect
	 * @return
	 */
	public static UpdateProspectVO convertCreateUpdateProspectVo2UpdateProspectVO(
			CreateUpdateProspectVo createUpdateProspectVo) {

		UpdateProspectVO updateProspectVO = new UpdateProspectVO();

		if (createUpdateProspectVo.getTitle() != null) {
			updateProspectVO.setTitle(createUpdateProspectVo.getTitle());
		}
		if (createUpdateProspectVo.getFirstName() != null) {
			updateProspectVO.setFirstName(createUpdateProspectVo.getFirstName());
		}
		if (createUpdateProspectVo.getLastName() != null) {
			updateProspectVO.setLastName(createUpdateProspectVo.getLastName());
		}

		if (createUpdateProspectVo.getMiddleName() != null) {
			updateProspectVO.setMiddleName(createUpdateProspectVo.getMiddleName());
		}

		if (createUpdateProspectVo.getPreferredCallDate() != null && !createUpdateProspectVo.getPreferredCallDate().isEmpty()) {

			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date preferedCallDate = null;

			try {

				preferedCallDate = formatter.parse(createUpdateProspectVo.getPreferredCallDate());

			} catch (ParseException e) {

				log.info("Error while parsing prefered call date from validator :: " + e.getMessage());

			}

			Long l = preferedCallDate.getTime();

			updateProspectVO.setPrefferedCallDate(l);

			// updateProspectVO.setPrefferedCallDate(Long.valueOf(createUpdateProspectVo.getPreferredCallDate()));
		}
		if (createUpdateProspectVo.getProspectNumber() != null) {
			updateProspectVO.setProspectNo(createUpdateProspectVo.getProspectNumber());
		}
		if (createUpdateProspectVo.getCustomerType() != null) {
			updateProspectVO.setCustomerType(createUpdateProspectVo.getCustomerType());
		}
		if (createUpdateProspectVo.getMobileNumber() != null) {
			updateProspectVO.setMobileNumber(createUpdateProspectVo.getMobileNumber());
		}

		if (createUpdateProspectVo.getMobileNumber() != null) {
			updateProspectVO.setMobileNumber(createUpdateProspectVo.getMobileNumber());
		}

		if (createUpdateProspectVo.getAlternativeMobileNo() != null) {
			updateProspectVO.setAlternativeMobileNo(createUpdateProspectVo.getAlternativeMobileNo());
		}

		if (createUpdateProspectVo.getOfficeNumber() != null) {
			updateProspectVO.setOfficeNumber(createUpdateProspectVo.getOfficeNumber());
		}

		if (createUpdateProspectVo.getEmailId() != null) {
			updateProspectVO.setEmailId(createUpdateProspectVo.getEmailId());
		}
		if (createUpdateProspectVo.getAlternativeEmailId() != null) {
			updateProspectVO.setAlternativeEmailId(createUpdateProspectVo.getAlternativeEmailId());
		}

		if (createUpdateProspectVo.getCustomerProfile() != null) {
			updateProspectVO.setCustomerProfile(createUpdateProspectVo.getCustomerProfile());
		}

		if (createUpdateProspectVo.getCityName() != null) {
			updateProspectVO.setCityName(createUpdateProspectVo.getCityName());
			updateProspectVO.setCityId(String.valueOf(
					LocationCache.getCityDAO().getCityIdByName(createUpdateProspectVo.getCityName().toUpperCase())));
		}

		if (createUpdateProspectVo.getBranchName() != null) {
			updateProspectVO.setBranchName(createUpdateProspectVo.getBranchName());
			updateProspectVO.setBranchId(String.valueOf(LocationCache.getBranchDAO()
					.getBranchIdByName(createUpdateProspectVo.getBranchName().toUpperCase())));
		}

		if (createUpdateProspectVo.getAreaName() != null) {
			updateProspectVO.setAreaName(createUpdateProspectVo.getAreaName());
			updateProspectVO.setAreaId(
					String.valueOf(LocationCache.getAreaDAO().getAreaIdByName(createUpdateProspectVo.getBranchName()+"_"+createUpdateProspectVo.getAreaName())));
		}

		if (createUpdateProspectVo.getEnquiryType() != null) {
			updateProspectVO.setEnquiryType(createUpdateProspectVo.getEnquiryType());
		}

		if (createUpdateProspectVo.getSource() != null) {
			updateProspectVO.setSource(createUpdateProspectVo.getSource());
		}

		if (createUpdateProspectVo.getHow_To_Know_ACT() != null) {
			updateProspectVO.setSourceOfEnquiry(createUpdateProspectVo.getHow_To_Know_ACT());
		}

		if (createUpdateProspectVo.getProspectType() != null) {
			updateProspectVO.setProspectType(createUpdateProspectVo.getProspectType());
		}

		ObjectMapper mapper = new ObjectMapper();
		try {
			if (createUpdateProspectVo.getCurrentAddress() != null)
			{
				createUpdateProspectVo.getCurrentAddress().setCustomerAddressType(AddressType.CURRENT_ADDRESS);
				updateProspectVO
				.setCurrentAddress(mapper.writeValueAsString(createUpdateProspectVo.getCurrentAddress()));
			}
				
			if (createUpdateProspectVo.getPermanentAddress() != null)
			{
				createUpdateProspectVo.getPermanentAddress().setCustomerAddressType(AddressType.PERMANENT_ADDRESS);
				updateProspectVO
				.setPermanentAddress(mapper.writeValueAsString(createUpdateProspectVo.getPermanentAddress()));
			}
				
			if (createUpdateProspectVo.getCommunicationAddress() != null)
			{
				createUpdateProspectVo.getCommunicationAddress().setCustomerAddressType(AddressType.COMMUNICATION_ADDRESS);
				updateProspectVO.setCommunicationAdderss(
						mapper.writeValueAsString(createUpdateProspectVo.getCommunicationAddress()));
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ************ Feasibility informations have started **************

		// if (createUpdateProspectVo.getFeasibility() != null) {
		//
		// // ************ CX Related information starts **************
		//
		// updateProspectVO.setCxIpAddress(createUpdateProspectVo.getFeasibility().getCxIpAddress());
		// updateProspectVO.setCxMacAddress(createUpdateProspectVo.getFeasibility().getCxMac());
		// updateProspectVO.setCxName(createUpdateProspectVo.getFeasibility().getCxName());
		// List<String> cxPorts = new ArrayList<String>();
		// cxPorts.add(createUpdateProspectVo.getFeasibility().getCxPort());
		// updateProspectVO.setCxPorts(cxPorts);
		//
		// // ************ FX Related information starts **************
		//
		// updateProspectVO.setFxIpAddress(createUpdateProspectVo.getFeasibility().getFxIpAddress());
		// updateProspectVO.setFxMacAddress(createUpdateProspectVo.getFeasibility().getFxMac());
		// updateProspectVO.setFxName(createUpdateProspectVo.getFeasibility().getFxName());
		// List<String> fxPorts = new ArrayList<String>();
		// fxPorts.add(createUpdateProspectVo.getFeasibility().getFxport());
		// updateProspectVO.setFxPorts(fxPorts);
		//
		// updateProspectVO.setConnectionType(createUpdateProspectVo.getFeasibility().getConnectionType());
		//
		// }

		// ************ Feasibility informations have Done **************

		if (createUpdateProspectVo.getTransactionId() != null) {
			updateProspectVO.setTransactionNo(createUpdateProspectVo.getTransactionId());
		}

		if (createUpdateProspectVo.getCRPAccountNumber() != null) {
			updateProspectVO.setCrpAccountNumber(createUpdateProspectVo.getCRPAccountNumber());
		}

		if (createUpdateProspectVo.getCRPMobileNumber() != null) {
			updateProspectVO.setCrpMobileNumber(createUpdateProspectVo.getCRPMobileNumber());
		}

		if (createUpdateProspectVo.getExistingISP() != null) {
			updateProspectVO.setExistingIsp(createUpdateProspectVo.getExistingISP());
		}

		// *********** Customer's Address is starting ******************

		List<CustomerAddressVO> customerAddressVOsList = new ArrayList<CustomerAddressVO>();
		updateProspectVO.setCustomerAddressVOs(customerAddressVOsList);
		if (createUpdateProspectVo.getCurrentAddress() != null) {
			CustomerAddressVO currentAddress = new CustomerAddressVO();
			currentAddress = createUpdateProspectVo.getCurrentAddress();
			customerAddressVOsList.add(currentAddress);

		}

		if (createUpdateProspectVo.getCommunicationAddress() != null) {
			CustomerAddressVO communicationAddress = new CustomerAddressVO();
			communicationAddress = createUpdateProspectVo.getCommunicationAddress();
			customerAddressVOsList.add(communicationAddress);

		}

		if (createUpdateProspectVo.getPermanentAddress() != null) {
			CustomerAddressVO permanentAddress = new CustomerAddressVO();
			permanentAddress = createUpdateProspectVo.getPermanentAddress();
			customerAddressVOsList.add(permanentAddress);
		}

		if (!customerAddressVOsList.isEmpty() && customerAddressVOsList != null) {
			updateProspectVO.setCustomerAddressVOs(customerAddressVOsList);
		}

		// *********** Customer's Address has been Done ******************

		return updateProspectVO;
	}

	public  CreateUpdateProspectRequestInSiebel convertConsolidatedProspectVo2ProspectRequestInSiebel(
			ConsolidatedProspectVoForSiebel cpvo4siebel) {

		CreateUpdateProspectRequestInSiebel cuprinsiebel = getCreateUpdateProspectRequestInSiebel(cpvo4siebel); // superParent
		return cuprinsiebel;
	}

	/**
	 * @author aditya CreateUpdateProspectRequestInSiebel
	 * @param cpvo4siebel
	 * @return
	 */
	private CreateUpdateProspectRequestInSiebel getCreateUpdateProspectRequestInSiebel(
			ConsolidatedProspectVoForSiebel cpvo4siebel) {

		log.info("getCreateUpdateProspectRequestInSiebel is call for update prospect"+cpvo4siebel);
		CreateUpdateProspectRequestInSiebel createUpdateProspectRequestInSiebel = new CreateUpdateProspectRequestInSiebel();
		createUpdateProspectRequestInSiebel.setCreateUpdateProspect_Input(getCreateUpdateProspect_Input(cpvo4siebel));

		return createUpdateProspectRequestInSiebel;
	}

	/**
	 * @author aditya CreateUpdateProspect_Input
	 * @param cpvo4siebel
	 * @return
	 */
	private CreateUpdateProspect_Input getCreateUpdateProspect_Input(
			ConsolidatedProspectVoForSiebel cpvo4siebel) {

		CreateUpdateProspect_Input createUpdateProspect_Input = new CreateUpdateProspect_Input();
		createUpdateProspect_Input.setListOfActcreateprospectthinio(getListOfActcreateprospectthinio(cpvo4siebel));

		if (cpvo4siebel.getActDescription() != null)
			createUpdateProspect_Input.setActDescription(cpvo4siebel.getActDescription());

		if (cpvo4siebel.getPreferredCallDateTime() != null){
//			createUpdateProspect_Input
//					.setPreferredCallDateTime(getInSiebelDateFormatter(cpvo4siebel.getPreferredCallDateTime()));

			log.info("preffered call date is  ::"+cpvo4siebel.getPreferredCallDateTime());
//		
			createUpdateProspect_Input
			.setPreferredCallDateTime(GenericUtil.getSiebelDateFormatter(cpvo4siebel.getPreferredCallDateTime()));

		}
			
		if (cpvo4siebel.getTransation_spcId() != null)
			createUpdateProspect_Input.setTransation_spcId(cpvo4siebel.getTransation_spcId());

		return createUpdateProspect_Input;
	}

	/**
	 * @author aditya ListOfActcreateprospectthinio
	 * @param cpvo4siebel
	 * @return
	 */
	private ListOfActcreateprospectthinio getListOfActcreateprospectthinio(
			ConsolidatedProspectVoForSiebel cpvo4siebel) {

		ListOfActcreateprospectthinio actcreateprospectthinio = new ListOfActcreateprospectthinio();

		actcreateprospectthinio.setActListMgmtProspectiveContactBc(getActListMgmtProspectiveContactBc(cpvo4siebel));

		return actcreateprospectthinio;
	}

	/**
	 * @author aditya ActListMgmtProspectiveContactBc
	 * @param cpvo4siebel
	 * @return
	 */
	private ActListMgmtProspectiveContactBc getActListMgmtProspectiveContactBc(
			ConsolidatedProspectVoForSiebel cpvo4siebel) {

		ActListMgmtProspectiveContactBc actListMgmtProspectiveContactBc = new ActListMgmtProspectiveContactBc();
		actListMgmtProspectiveContactBc.setListOfActCutAddressThinBc(getListOfActCutAddressThinBc(cpvo4siebel));

		actListMgmtProspectiveContactBc.setListOfACTListMgmtProspectiveContactBC_ACTContactListMVL(
				getListOfACTListMgmtProspectiveContactBC_ACTContactListMVL(cpvo4siebel));

		if (cpvo4siebel.getACTCAFSerialNumber() != null)
			actListMgmtProspectiveContactBc.setACTCAFSerialNumber(cpvo4siebel.getACTCAFSerialNumber());

		if (cpvo4siebel.getACTCurrentISP() != null)
			actListMgmtProspectiveContactBc.setACTCurrentISP(cpvo4siebel.getACTCurrentISP());

		if (cpvo4siebel.getACTCxPermission() != null)
			actListMgmtProspectiveContactBc.setACTCxPermission(cpvo4siebel.getACTCxPermission());

		if (cpvo4siebel.getACTGxRequired() != null)
			actListMgmtProspectiveContactBc.setACTGxRequired(cpvo4siebel.getACTGxRequired());

		if (cpvo4siebel.getActionName() != null) {
			actListMgmtProspectiveContactBc.setActionName(cpvo4siebel.getActionName());
		} else {
			actListMgmtProspectiveContactBc.setActionName(FWMPConstant.ApiAction.UPDATE);
		}

		if (cpvo4siebel.getConnectionType() != null)
			actListMgmtProspectiveContactBc.setConnectionType(cpvo4siebel.getConnectionType());

		if (cpvo4siebel.getCustomerProfile() != null)
			actListMgmtProspectiveContactBc.setCustomerProfile(cpvo4siebel.getCustomerProfile());

		if (cpvo4siebel.getCustomerType() != null)
			actListMgmtProspectiveContactBc.setCustomerType(cpvo4siebel.getCustomerType());

		if (cpvo4siebel.getCXIP() != null)
			actListMgmtProspectiveContactBc.setCXIP(cpvo4siebel.getCXIP());

		if (cpvo4siebel.getCXPort() != null)
			actListMgmtProspectiveContactBc.setCXPort(cpvo4siebel.getCXPort());

		if (cpvo4siebel.getFXName() != null)
			actListMgmtProspectiveContactBc.setFXName(cpvo4siebel.getFXName());

		if (cpvo4siebel.getFXPort() != null)
			actListMgmtProspectiveContactBc.setFXPort(cpvo4siebel.getFXPort());
		// Modified by Manjuprasad For Door Knocks Issue
		
//		if (cpvo4siebel.getHowdidgettoknowaboutACT() != null)
//			actListMgmtProspectiveContactBc.setHowdidgettoknowaboutACT(cpvo4siebel.getHowdidgettoknowaboutACT());
		if (cpvo4siebel.getHowdidgettoknowaboutACT() != null) {
//			String knownsource = definitionCoreService.getSourceById(Long.valueOf(cpvo4siebel.getHowdidgettoknowaboutACT()),null);		
			if (cpvo4siebel.getHowdidgettoknowaboutACT().equalsIgnoreCase(FWMPConstant.TicketSource.DOOR_KNOCK)){
				actListMgmtProspectiveContactBc.setHowdidgettoknowaboutACT(definitionCoreService
						.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));
			}else {
				actListMgmtProspectiveContactBc.setHowdidgettoknowaboutACT(cpvo4siebel.getHowdidgettoknowaboutACT());
			}
		} else {
			actListMgmtProspectiveContactBc.setHowdidgettoknowaboutACT(definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));
		}
		

		if (cpvo4siebel.getLineofBusiness() != null)
			actListMgmtProspectiveContactBc.setLineofBusiness(cpvo4siebel.getLineofBusiness());

		if (cpvo4siebel.getMilestoneStage() != null)
		{
			String salesMileStoneName=getActivityTypeByActivityId(Long.valueOf(cpvo4siebel.getMilestoneStage()));
			actListMgmtProspectiveContactBc.setMilestoneStage(salesMileStoneName);
		}
			

		if (cpvo4siebel.getMileStoneStatus() != null)
			actListMgmtProspectiveContactBc.setMileStoneStatus(cpvo4siebel.getMileStoneStatus());

		if (cpvo4siebel.getNotes() != null)
			actListMgmtProspectiveContactBc.setNotes(cpvo4siebel.getNotes());

		if (cpvo4siebel.getPlanCode() != null)
			actListMgmtProspectiveContactBc.setPlanCode(cpvo4siebel.getPlanCode());

		if (cpvo4siebel.getPlanName() != null)
			actListMgmtProspectiveContactBc.setPlanName(cpvo4siebel.getPlanName());

		if (cpvo4siebel.getProductType() != null) {
			actListMgmtProspectiveContactBc.setProductType(cpvo4siebel.getProductType());
		} else {
			actListMgmtProspectiveContactBc.setProductType(FWMPConstant.ProductName.BROADBAND);
		}

		if (cpvo4siebel.getProspectAssignedTo() != null)
			actListMgmtProspectiveContactBc.setProspectAssignedTo(cpvo4siebel.getProspectAssignedTo());

		if (cpvo4siebel.getProspectNumber() != null)
			actListMgmtProspectiveContactBc.setProspectNumber(cpvo4siebel.getProspectNumber());

		if (cpvo4siebel.getRefereeAccountNumber() != null)
			actListMgmtProspectiveContactBc.setRefereeAccountNumber(cpvo4siebel.getRefereeAccountNumber());

		if (cpvo4siebel.getRefereeMobileNumber() != null)
			actListMgmtProspectiveContactBc.setRefereeMobileNumber(cpvo4siebel.getRefereeMobileNumber());

		if (cpvo4siebel.getRouterModelCode() != null)
			actListMgmtProspectiveContactBc.setRouterModelCode(cpvo4siebel.getRouterModelCode());

		if (cpvo4siebel.getSchemeCode() != null)
			actListMgmtProspectiveContactBc.setSchemeCode(cpvo4siebel.getSchemeCode());

// Modified by Manjuprasad For Door Knocks Issue
//		if (cpvo4siebel.getSourceType() != null)
//			actListMgmtProspectiveContactBc.setSourceType(cpvo4siebel.getSourceType());
		if (cpvo4siebel.getSourceType() != null) {
			if (cpvo4siebel.getSourceType().equalsIgnoreCase(FWMPConstant.TicketSource.DOOR_KNOCK)){
				actListMgmtProspectiveContactBc.setSourceType(definitionCoreService
						.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));
			}else {
				actListMgmtProspectiveContactBc.setSourceType(cpvo4siebel.getSourceType());
			}
			
		}else {
			actListMgmtProspectiveContactBc.setSourceType(definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));
		}
		
		
		if (cpvo4siebel.getWifiRequired() != null) {
			
			try {
				int wifiRequired = Integer.valueOf(cpvo4siebel.getWifiRequired());
				
				if( wifiRequired == Status.ACTIVE)
					actListMgmtProspectiveContactBc.setWifiRequired("Y");
				else
					actListMgmtProspectiveContactBc.setWifiRequired("N");
			} catch (NumberFormatException e) {
				log.info("Exception while setting the ipRequired :value is :"+cpvo4siebel.getWifiRequired());
			}
		}

		if (cpvo4siebel.getStatus() != null){
			
			if(Integer.parseInt(cpvo4siebel.getStatus())==FWMPConstant.ProspectType.HOT_ID)
				actListMgmtProspectiveContactBc.setStatus(FWMPConstant.ProspectType.HOT);
			
			else if(Integer.parseInt(cpvo4siebel.getStatus())==FWMPConstant.ProspectType.MEDIUM_ID) {
				actListMgmtProspectiveContactBc.setStatus(FWMPConstant.ProspectType.MEDIUM);
				try {
					if(DefinitionCoreServiceImpl.mediumProspectReasoncode.containsKey(Long.valueOf(cpvo4siebel.getNotes()))) {
						actListMgmtProspectiveContactBc.setProspectRejectionReason(DefinitionCoreServiceImpl.mediumProspectReasoncode.get(Long.valueOf(cpvo4siebel.getNotes())));
					}
				} catch (Exception e) {
					log.info("Exception while setting ProspectRejectionReason in Medium");
				}
			}
			else {
				actListMgmtProspectiveContactBc.setStatus(FWMPConstant.ProspectType.COLD);
				try {
					if(DefinitionCoreServiceImpl.coldProspectReasoncode.containsKey(Long.valueOf(cpvo4siebel.getNotes()))) {
						actListMgmtProspectiveContactBc.setProspectRejectionReason(DefinitionCoreServiceImpl.coldProspectReasoncode.get(Long.valueOf(cpvo4siebel.getNotes())));
					}
				} catch (Exception e) {
					log.info("Exception while setting ProspectRejectionReason in Cold");
				}
			}
		}

		if (cpvo4siebel.getSubAttribute() != null)
			actListMgmtProspectiveContactBc.setSubAttribute(cpvo4siebel.getSubAttribute());

		if (cpvo4siebel.getSubSource() != null)
			actListMgmtProspectiveContactBc.setSubSource(cpvo4siebel.getSubSource());

		if (cpvo4siebel.getWifiRequired() != null)
			actListMgmtProspectiveContactBc.setWifiRequired(cpvo4siebel.getWifiRequired());

		return actListMgmtProspectiveContactBc;
	}

	/**
	 * @author aditya ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL
	 * @param cpvo4siebel
	 * @return
	 */
	private static ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL getListOfACTListMgmtProspectiveContactBC_ACTContactListMVL(
			ConsolidatedProspectVoForSiebel cpvo4siebel) {

		ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL actContactListMVL = new ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL();
		actContactListMVL.setACTListMgmtProspectiveContactBC_ACTContactListMVL(
				getACTListMgmtProspectiveContactBC_ACTContactListMVL(cpvo4siebel));
		return actContactListMVL;

	}

	/**
	 * @author aditya List<ACTListMgmtProspectiveContactBC_ACTContactListMVL>
	 * @param cpvo4siebel
	 * @return
	 */
	private static List<ACTListMgmtProspectiveContactBC_ACTContactListMVL> getACTListMgmtProspectiveContactBC_ACTContactListMVL(
			ConsolidatedProspectVoForSiebel cpvo4siebel) {

		List<ACTListMgmtProspectiveContactBC_ACTContactListMVL> actContactListMVLs = new ArrayList<ACTListMgmtProspectiveContactBC_ACTContactListMVL>();

		ACTListMgmtProspectiveContactBC_ACTContactListMVL actContactListMVL = new ACTListMgmtProspectiveContactBC_ACTContactListMVL();

		if (cpvo4siebel.get_IsPrimaryMVG() != null)
			actContactListMVL.set_IsPrimaryMVG(cpvo4siebel.get_IsPrimaryMVG());

		if (cpvo4siebel.getAadharNumber() != null)
			actContactListMVL.setAadharNumber(cpvo4siebel.getAadharNumber());

		if (cpvo4siebel.getAlternateMobileNumber() != null)
			actContactListMVL.setAlternateMobileNumber(cpvo4siebel.getAlternateMobileNumber());

		if (cpvo4siebel.getCellularPhone() != null)
			actContactListMVL.setCellularPhone(cpvo4siebel.getCellularPhone());

		if (cpvo4siebel.getCompanyName() != null)
			actContactListMVL.setCompanyName(cpvo4siebel.getCompanyName());

		if (cpvo4siebel.getContactIntegrationId() != null)
			actContactListMVL.setContactIntegrationId(cpvo4siebel.getContactIntegrationId());

		if (cpvo4siebel.getContactName() != null)
			actContactListMVL.setContactName(cpvo4siebel.getContactName());

		if (cpvo4siebel.getEmailAddress() != null)
			actContactListMVL.setEmailAddress(cpvo4siebel.getEmailAddress());

		if (cpvo4siebel.getEmailAddress2() != null)
			actContactListMVL.setEmailAddress2(cpvo4siebel.getEmailAddress2());

		if (cpvo4siebel.getFirstName() != null)
			actContactListMVL.setFirstName(cpvo4siebel.getFirstName());

		if (cpvo4siebel.getHOMEPHNUM() != null)
			actContactListMVL.setHOMEPHNUM(cpvo4siebel.getHOMEPHNUM());

		if (cpvo4siebel.getLastName() != null)
			actContactListMVL.setLastName(cpvo4siebel.getLastName());

		if (cpvo4siebel.getMiddleName() != null)
			actContactListMVL.setMiddleName(cpvo4siebel.getMiddleName());

		if (cpvo4siebel.getNationality() != null)
			actContactListMVL.setNationality(cpvo4siebel.getNationality());

		if (cpvo4siebel.getTitle() != null)
			actContactListMVL.setTitle(cpvo4siebel.getTitle());

		if (cpvo4siebel.getUIN() != null)
			actContactListMVL.setUIN(cpvo4siebel.getUIN());

		if (cpvo4siebel.getUINType() != null)
			actContactListMVL.setUINType(cpvo4siebel.getUINType());

		actContactListMVLs.add(actContactListMVL);
		return actContactListMVLs;
	}

	/**
	 * @author aditya ListOfActCutAddressThinBc
	 * @param cpvo4siebel
	 * @return
	 */
	private static ListOfActCutAddressThinBc getListOfActCutAddressThinBc(ConsolidatedProspectVoForSiebel cpvo4siebel) {

		ListOfActCutAddressThinBc actCutAddressThinBc = new ListOfActCutAddressThinBc();

		actCutAddressThinBc.setActCutAddressThinBc(getActCutAddressThinBc(cpvo4siebel));

		return actCutAddressThinBc;
	}

	/**
	 * @author aditya List<ActCutAddressThinBc>
	 * @param cpvo4siebel
	 * @return
	 */
	private static List<ActCutAddressThinBc> getActCutAddressThinBc(ConsolidatedProspectVoForSiebel cpvo4siebel) {
		
		List<ActCutAddressThinBc> actCutAddressThinBcs = new ArrayList<ActCutAddressThinBc>();

		ActCutAddressThinBc actCutAddressPermanent = getActCutAddressPermanent(cpvo4siebel);
		ActCutAddressThinBc actCutAddressCurrent = getActCutAddressCurrent(cpvo4siebel);
		ActCutAddressThinBc actCutAddressCommunication = getActCutAddressCommunication(cpvo4siebel);

		actCutAddressThinBcs.add(actCutAddressCommunication);
		actCutAddressThinBcs.add(actCutAddressCurrent);
		// actCutAddressThinBcs.add(actCutAddressPermanent);

		return actCutAddressThinBcs;
	}

	/**
	 * @author aditya ActCutAddressThinBc
	 * @param cpvo4siebel
	 * @return
	 */
	private static ActCutAddressThinBc getActCutAddressCommunication(ConsolidatedProspectVoForSiebel cpvo4siebel) {

		ActCutAddressThinBc actCutAddressThinBc = new ActCutAddressThinBc();

		log.info(" getActCutAddressCommunication is called:::"+cpvo4siebel);
		actCutAddressThinBc = getActCutAddressFromCustomerAddressVO(cpvo4siebel.getCommunicationAddress());
		actCutAddressThinBc.setAddressType(SiebelAddressType.BILL_TO);
		
		if(cpvo4siebel.getCity()!=null){
			log.info("cityId in getActCutAddressCommunication"+cpvo4siebel.getCity());
			String cityName=LocationCache.getCityNameById(Long.valueOf(cpvo4siebel.getCity()));
			log.info("cityName getActCutAddressCommunication"+cityName);
			log.info("cityId in getActCutAddressCommunication"+cpvo4siebel.getCity() +"cityName *** "+cityName);
			actCutAddressThinBc.setCity(cityName);
			
		}
		
		if(cpvo4siebel.getOperationalEntity()!=null){
			log.info("BranchId in getActCutAddressCommunication "+cpvo4siebel.getOperationalEntity());
			String branchName=LocationCache.getBrancheNameById(Long.valueOf(cpvo4siebel.getOperationalEntity()));
			
			log.info("BranchId in getActCutAddressCommunication "+cpvo4siebel.getOperationalEntity()+" branchName **"+branchName);
			actCutAddressThinBc.setOperationalEntity(branchName);
			
		}
		
		if(cpvo4siebel.getArea()!=null){
			log.info("AreaId in getActCutAddressCommunication "+cpvo4siebel.getArea());
			String areaName=LocationCache.getAreaNameById(Long.valueOf(cpvo4siebel.getArea()));
			log.info("AreaId in getActCutAddressCommunication "+cpvo4siebel.getArea()+"areaName **"+areaName);
			actCutAddressThinBc.setArea(areaName);
			
		}
		actCutAddressThinBc.setCountry(FWMPConstant.COUNTRY_INDIA);
		String state=CityDAOImpl.cityIdAndStateMap.get(Long.valueOf(cpvo4siebel.getCity()));
		actCutAddressThinBc.setState(state);
		return actCutAddressThinBc;

	}

	/**
	 * @author aditya ActCutAddressThinBc
	 * @param cpvo4siebel
	 * @return
	 */
	private static ActCutAddressThinBc getActCutAddressCurrent(ConsolidatedProspectVoForSiebel cpvo4siebel) {

		ActCutAddressThinBc actCutAddressThinBc = new ActCutAddressThinBc();

		actCutAddressThinBc = getActCutAddressFromCustomerAddressVO(cpvo4siebel.getCurrentAddress());
		
		if(cpvo4siebel.getLatitude() != null)
			actCutAddressThinBc.setLatitude(cpvo4siebel.getLatitude());
		
		if(cpvo4siebel.getLongitude() != null)
			actCutAddressThinBc.setLongitude(cpvo4siebel.getLongitude());
		
		if(cpvo4siebel.getCity()!=null){
			log.info("cityId in actCutAddressThinBc"+cpvo4siebel.getCity());
			String cityName=LocationCache.getCityNameById(Long.valueOf(cpvo4siebel.getCity()));
			actCutAddressThinBc.setCity(cityName);
			
		}
		
		if(cpvo4siebel.getOperationalEntity()!=null){
//			log.info("BranchId in actCutAddressThinBc "+cpvo4siebel.getOperationalEntity());
			String branchName=LocationCache.getBrancheNameById(Long.valueOf(cpvo4siebel.getOperationalEntity()));
			actCutAddressThinBc.setOperationalEntity(branchName);
			
		}
		
		if(cpvo4siebel.getArea()!=null){
			log.info("AreaId in actCutAddressThinBc "+cpvo4siebel.getArea());
			String areaName=LocationCache.getAreaNameById(Long.valueOf(cpvo4siebel.getArea()));
			log.info("areaName to siebel"+areaName);
			actCutAddressThinBc.setArea(areaName);
			
		}
		actCutAddressThinBc.setCountry(FWMPConstant.COUNTRY_INDIA);
		String state=CityDAOImpl.cityIdAndStateMap.get(Long.valueOf(cpvo4siebel.getCity()));
		actCutAddressThinBc.setState(state);
	
		return actCutAddressThinBc;

	}

	/**
	 * @author aditya ActCutAddressThinBc
	 * @param cpvo4siebel
	 * @return
	 */
	private static ActCutAddressThinBc getActCutAddressPermanent(ConsolidatedProspectVoForSiebel cpvo4siebel) {

		ActCutAddressThinBc actCutAddressThinBc = new ActCutAddressThinBc();

		actCutAddressThinBc = getActCutAddressFromCustomerAddressVO(cpvo4siebel.getPermanentAddress());

		return actCutAddressThinBc;
	}

	/**
	 * @author kiran ActCutAddressThinBc
	 * @param permanentAddressVo
	 * @return
	 */
	private static ActCutAddressThinBc getActCutAddressFromCustomerAddressVO(String address) {

		ActCutAddressThinBc actCutAddressThinBc = new ActCutAddressThinBc();
		
		log.info("ActCutAddressThinBc for current addresss :::"+address);

		try {
			ObjectMapper mapper = new ObjectMapper();

			CustomerAddressVO addressVo = mapper.readValue(address, CustomerAddressVO.class);

			if (addressVo.getAddress1() != null)
				actCutAddressThinBc.setAddress1(addressVo.getAddress1());

			if (addressVo.getAddress2() != null)
				actCutAddressThinBc.setAddress2(addressVo.getAddress2());

			if (addressVo.getCustomerAddressType() != null)
				actCutAddressThinBc.setAddressType(addressVo.getCustomerAddressType());

			if (addressVo.getLandmark() != null)
				actCutAddressThinBc.setLandMark(addressVo.getLandmark());

			if (addressVo.getPincode() != null)
				actCutAddressThinBc.setPostalCode(addressVo.getPincode());
			if(addressVo.getSubArea() != null)
				actCutAddressThinBc.setSubArea(addressVo.getSubArea());

			/*if(addressVo.getCity()!=null){
				
				String cityName=LocationCache.getCityNameById(Long.valueOf(addressVo.getCity()));
				actCutAddressThinBc.setCity(cityName.substring(0, 1).toUpperCase()+cityName.substring(1).toLowerCase());
				
			}
			
			if(addressVo.getOperationalEntity()!=null){
				
				String branchName=LocationCache.getBrancheNameById(Long.valueOf(addressVo.getOperationalEntity()));
				actCutAddressThinBc.setCity(branchName.substring(0, 1).toUpperCase()+branchName.substring(1).toLowerCase());
				
			}
			
			if(addressVo.getArea()!=null){
				log.info("addressVo.getArea() value is *******"+addressVo.getArea());
				String areaName=LocationCache.getAreaNameById(Long.valueOf(addressVo.getArea()));
				actCutAddressThinBc.setCity(areaName.substring(0, 1).toUpperCase()+areaName.substring(1).toLowerCase());
				
			}*/
			
			
			actCutAddressThinBc.setAddressType(SiebelAddressType.INSTALLATION);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return actCutAddressThinBc;
	}

	/**
	 * @author aditya CreateUpdateProspectRequestInSiebel
	 * @param createUpdateProspectVo
	 * @return
	 */
	public CreateUpdateProspectRequestInSiebel convertCreateUpdateProspectVO2ProspectRequestInSiebel(
			ProspectCoreVO prospectCoreVO) {

		CreateUpdateProspectRequestInSiebel createUpdateProspectRequestInSiebel = new CreateUpdateProspectRequestInSiebel();

		createUpdateProspectRequestInSiebel
				.setCreateUpdateProspect_Input(getCreateUpdateProspect_Input(prospectCoreVO));

		return createUpdateProspectRequestInSiebel;
	}

	/**
	 * @author aditya CreateUpdateProspect_Input
	 * @param prospectCoreVO
	 * @return
	 */
	private CreateUpdateProspect_Input getCreateUpdateProspect_Input(ProspectCoreVO prospectCoreVO) {
		CreateUpdateProspect_Input CreateUpdateProspect_Input = new CreateUpdateProspect_Input();

		CreateUpdateProspect_Input.setActDescription(prospectCoreVO.getCustomer().getNotes());

		log.info("prospectCoreVO.getCustomer().getPrefferedCallDate() for update::"+prospectCoreVO.getCustomer().getPrefferedCallDate());
//		createUpdateProspect_Input
//				.setPreferredCallDateTime(getSiebelDateFormatter(prospectCoreVO.getCustomer().getPrefferedCallDate()));

		
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stringDate = formatter.format(prospectCoreVO.getCustomer().getPrefferedCallDate());
	
		CreateUpdateProspect_Input
		.setPreferredCallDateTime(GenericUtil.getSiebelDateFormatter(stringDate));

		CreateUpdateProspect_Input.setTransation_spcId(StrictMicroSecondTimeBasedGuid.newGuid() + "");

		CreateUpdateProspect_Input.setListOfActcreateprospectthinio(getListOfActcreateprospectthinio(prospectCoreVO));

		return CreateUpdateProspect_Input;
	}

	/**
	 * @author aditya ListOfActcreateprospectthinio
	 * @param prospectCoreVO
	 * @return
	 */
	private ListOfActcreateprospectthinio getListOfActcreateprospectthinio(ProspectCoreVO prospectCoreVO) {

		ListOfActcreateprospectthinio listOfActcreateprospectthinio = new ListOfActcreateprospectthinio();
		listOfActcreateprospectthinio
				.setActListMgmtProspectiveContactBc(getActListMgmtProspectiveContactBc(prospectCoreVO));

		return listOfActcreateprospectthinio;
	}

	/**
	 * @author aditya ActListMgmtProspectiveContactBc
	 * @param prospectCoreVO
	 * @return
	 */
	private ActListMgmtProspectiveContactBc getActListMgmtProspectiveContactBc(ProspectCoreVO prospectCoreVO) {

		ActListMgmtProspectiveContactBc actListMgmtProspectiveContactBc = new ActListMgmtProspectiveContactBc();

		actListMgmtProspectiveContactBc.setListOfActCutAddressThinBc(getListOfActCutAddressThinBc(prospectCoreVO));

		////
		actListMgmtProspectiveContactBc.setListOfACTListMgmtProspectiveContactBC_ACTContactListMVL(
				getListOfACTListMgmtProspectiveContactBC_ACTContactListMVL(prospectCoreVO));

		actListMgmtProspectiveContactBc.setACTCurrentISP(prospectCoreVO.getCustomer().getExistingIsp());

		actListMgmtProspectiveContactBc.setActionName(FWMPConstant.ApiAction.CREATE);

		actListMgmtProspectiveContactBc.setProspectAssignedTo(AuthUtils.getCurrentUserLoginId().toUpperCase());

		actListMgmtProspectiveContactBc.setProductType(FWMPConstant.ProductName.BROADBAND);

		actListMgmtProspectiveContactBc.setProspectRejectionReason(prospectCoreVO.getTicket().getReason());

		actListMgmtProspectiveContactBc.setProspectNumber(prospectCoreVO.getTicket().getProspectNo());

		// need to modify as per TCS values

		actListMgmtProspectiveContactBc.setCustomerType(null);

		actListMgmtProspectiveContactBc.setRefereeAccountNumber(prospectCoreVO.getCustomer().getCrpAccountNo());

		actListMgmtProspectiveContactBc.setRefereeMobileNumber(prospectCoreVO.getCustomer().getCrpMobileNo());

		//Modified by Manjuprasad For Door Knocks Issue
//		actListMgmtProspectiveContactBc.setSourceType(FWMPConstant.TicketSource.DOOR_KNOCK);
		actListMgmtProspectiveContactBc.setSourceType(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));

		actListMgmtProspectiveContactBc.setStatus(FWMPConstant.ProspectType.HOT);

		if (prospectCoreVO.getCustomer().getCrpAccountNo() != null
				&& prospectCoreVO.getCustomer().getCrpMobileNo() != null
				&& !prospectCoreVO.getCustomer().getCrpAccountNo().isEmpty()
				&& !prospectCoreVO.getCustomer().getCrpMobileNo().isEmpty()) {

			actListMgmtProspectiveContactBc.setSubSource(FWMPConstant.SubSource.CRP);
		} else {
			actListMgmtProspectiveContactBc.setSubSource(FWMPConstant.SubSource.FWMP);
		}

		actListMgmtProspectiveContactBc.setCustomerProfile(prospectCoreVO.getCustomer().getCustomerProfile());

		actListMgmtProspectiveContactBc.setNotes(prospectCoreVO.getCustomer().getNotes());

		actListMgmtProspectiveContactBc.setLineofBusiness(FWMPConstant.LineOfBusiness.INDIVIDUAL);

//Modified by Manjuprasad for Door Knocks issue	
//		actListMgmtProspectiveContactBc.setHowdidgettoknowaboutACT(FWMPConstant.TicketSource.DOOR_KNOCK);
		actListMgmtProspectiveContactBc.setHowdidgettoknowaboutACT(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));
		
		String branchName = LocationCache.getBrancheNameById(prospectCoreVO.getCustomer().getBranchId());
		if(branchName != null)
		{
			if(branchName.equalsIgnoreCase(FWMPConstant.REALTY))
				actListMgmtProspectiveContactBc.setCustomerType(CustomerType.COMMUNITY);
			else
				actListMgmtProspectiveContactBc.setCustomerType(CustomerType.RETAIL);
		}
		
		return actListMgmtProspectiveContactBc;
	}

	/**
	 * @author aditya ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL
	 * @param prospectCoreVO
	 * @return
	 */
	private static ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL getListOfACTListMgmtProspectiveContactBC_ACTContactListMVL(
			ProspectCoreVO prospectCoreVO) {

		ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL actContactListMVL = new ListOfACTListMgmtProspectiveContactBC_ACTContactListMVL();

		actContactListMVL.setACTListMgmtProspectiveContactBC_ACTContactListMVL(
				getACTListMgmtProspectiveContactBC_ACTContactListMVL(prospectCoreVO));
		return actContactListMVL;

	}

	/**
	 * @author aditya List<ACTListMgmtProspectiveContactBC_ACTContactListMVL>
	 * @param prospectCoreVO
	 * @return
	 */
	private static List<ACTListMgmtProspectiveContactBC_ACTContactListMVL> getACTListMgmtProspectiveContactBC_ACTContactListMVL(
			ProspectCoreVO prospectCoreVO) {

		List<ACTListMgmtProspectiveContactBC_ACTContactListMVL> actContactListMVLs = new ArrayList<ACTListMgmtProspectiveContactBC_ACTContactListMVL>();

		ACTListMgmtProspectiveContactBC_ACTContactListMVL actContactListMVL = new ACTListMgmtProspectiveContactBC_ACTContactListMVL();

		if (prospectCoreVO.getCustomer().getAlternativeMobileNo() != null) {
			actContactListMVL.setAlternateMobileNumber(prospectCoreVO.getCustomer().getAlternativeMobileNo());
		}

		if (prospectCoreVO.getCustomer().getAlternativeEmailId() != null) {
			actContactListMVL.setEmailAddress2(prospectCoreVO.getCustomer().getAlternativeEmailId());
		}

		if (prospectCoreVO.getCustomer().getCompanyName() != null) {
			actContactListMVL.setCompanyName(prospectCoreVO.getCustomer().getCompanyName());
		}

		if (prospectCoreVO.getCustomer().getLastName() != null) {
			actContactListMVL.setLastName(prospectCoreVO.getCustomer().getLastName());
		}

		if (prospectCoreVO.getCustomer().getTitle() != null) {
			actContactListMVL.setTitle(prospectCoreVO.getCustomer().getTitle());
		}

		if (prospectCoreVO.getCustomer().getNationality() != null) {
			actContactListMVL.setNationality(prospectCoreVO.getCustomer().getNationality());
		}

		if (prospectCoreVO.getCustomer().getMobileNumber() != null) {
			actContactListMVL.setCellularPhone(prospectCoreVO.getCustomer().getMobileNumber());
		}

		actContactListMVL.setAadharNumber(prospectCoreVO.getCustomer().getAadhaarNumber());

		actContactListMVL.setMiddleName(prospectCoreVO.getCustomer().getMiddleName());

		actContactListMVL.setUINType(prospectCoreVO.getCustomer().getUinType());

		actContactListMVL.setHOMEPHNUM(prospectCoreVO.getCustomer().getAlternativeMobileNo());

		actContactListMVL.setEmailAddress(prospectCoreVO.getCustomer().getEmailId());

		actContactListMVL.setFirstName(prospectCoreVO.getCustomer().getFirstName());

		actContactListMVL.setUIN(prospectCoreVO.getCustomer().getUin());

		actContactListMVLs.add(actContactListMVL);

		return actContactListMVLs;
	}

	/**
	 * @author aditya ListOfActCutAddressThinBc
	 * @param prospectCoreVO
	 * @return
	 */
	private static ListOfActCutAddressThinBc getListOfActCutAddressThinBc(ProspectCoreVO prospectCoreVO) {
		ListOfActCutAddressThinBc actCutAddressThinBc = new ListOfActCutAddressThinBc();
		actCutAddressThinBc.setActCutAddressThinBc(getActCutAddressThinBc(prospectCoreVO));
		return actCutAddressThinBc;

	}

	/**
	 * @author aditya List<ActCutAddressThinBc>
	 * @param prospectCoreVO
	 * @return
	 */
	private static List<ActCutAddressThinBc> getActCutAddressThinBc(ProspectCoreVO prospectCoreVO) {

		List<ActCutAddressThinBc> actCutAddressThinBcs = new ArrayList<ActCutAddressThinBc>();

		// ActCutAddressThinBc actCutAddressPermanent =
		// getActCutAddressPermanent(prospectCoreVO);
		ActCutAddressThinBc actCutAddressCurrent = getActCutAddressCurrent(prospectCoreVO);
		ActCutAddressThinBc actCutAddressCommunication = getActCutAddressCommunication(prospectCoreVO);

		actCutAddressThinBcs.add(actCutAddressCommunication);
		actCutAddressThinBcs.add(actCutAddressCurrent);
		// actCutAddressThinBcs.add(actCutAddressPermanent);

		return actCutAddressThinBcs;
	}

	/**
	 * @author aditya ActCutAddressThinBc
	 * @param prospectCoreVO
	 * @return
	 */
	private static ActCutAddressThinBc getActCutAddressCommunication(ProspectCoreVO prospectCoreVO) {

		// *************** Communication Addresss ***************************

		ActCutAddressThinBc actCutAddressThinBc = new ActCutAddressThinBc();

		if (prospectCoreVO.getCustomer().getCustomerAddressVOs() != null) {

			List<CustomerAddressVO> communicationCustomerAddressvoList = prospectCoreVO.getCustomer()
					.getCustomerAddressVOs();

			for (CustomerAddressVO customerAddressVO : communicationCustomerAddressvoList) {
				
				if(customerAddressVO != null)
				{
					if (customerAddressVO.getCustomerAddressType()
							.equalsIgnoreCase(FWMPConstant.AddressType.COMMUNICATION_ADDRESS)) {

						actCutAddressThinBc.setPostalCode(customerAddressVO.getPincode());

						actCutAddressThinBc.setAddress2(customerAddressVO.getAddress2());

						actCutAddressThinBc.setAddress1(customerAddressVO.getAddress1());

						actCutAddressThinBc.setAddressType(customerAddressVO.getCustomerAddressType());

						actCutAddressThinBc.setLandMark(customerAddressVO.getLandmark());

						actCutAddressThinBc.setCountry(FWMPConstant.Country_Name.INDIA);
					}
				}

			}
		}

		if (prospectCoreVO.getCustomer().getAreaId() != null) {
			actCutAddressThinBc.setArea(LocationCache.getAreaNameById(prospectCoreVO.getCustomer().getAreaId()));
		}

		if (prospectCoreVO.getCustomer().getSubAreaName() != null) {
			actCutAddressThinBc.setSubArea(prospectCoreVO.getCustomer().getSubAreaName());
		}

		if (prospectCoreVO.getCustomer().getBranchId() != null) {
			actCutAddressThinBc
					.setOperationalEntity(LocationCache.getBrancheNameById(prospectCoreVO.getCustomer().getBranchId()));
		}

		if (prospectCoreVO.getCustomer().getCityId() != null) {
			actCutAddressThinBc.setState((CityDAOImpl.cityIdAndStateMap.get(prospectCoreVO.getCustomer().getCityId())));
		}

		if (prospectCoreVO.getCustomer().getCityId() != null) {
			
			String cityName=LocationCache.getCityNameById(prospectCoreVO.getCustomer().getCityId());
			actCutAddressThinBc.setCity(cityName);
		}

		if (prospectCoreVO.getCustomer().getLat() != null) {
			actCutAddressThinBc.setLatitude(prospectCoreVO.getCustomer().getLat());
		}

		if (prospectCoreVO.getCustomer().getLon() != null) {
			actCutAddressThinBc.setLongitude(prospectCoreVO.getCustomer().getLon());
		}

		actCutAddressThinBc.setAddressType(SiebelAddressType.BILL_TO);
		return actCutAddressThinBc;
	}

	/**
	 * @author aditya ActCutAddressThinBc
	 * @param prospectCoreVO
	 * @return
	 */
	private static ActCutAddressThinBc getActCutAddressCurrent(ProspectCoreVO prospectCoreVO) {

		// ********************* Current Addresss ****************************

		ActCutAddressThinBc actCutAddressThinBc = new ActCutAddressThinBc();

		if (prospectCoreVO.getCustomer().getCustomerAddressVOs() != null) {

			List<CustomerAddressVO> currentCustomerAddressvoList = prospectCoreVO.getCustomer().getCustomerAddressVOs();

			for (CustomerAddressVO customerAddressVO : currentCustomerAddressvoList) {
				
				if(customerAddressVO != null)
				{
					if (customerAddressVO.getCustomerAddressType()
							.equalsIgnoreCase(FWMPConstant.AddressType.CURRENT_ADDRESS)) {

						actCutAddressThinBc.setPostalCode(customerAddressVO.getPincode());

						actCutAddressThinBc.setAddress2(customerAddressVO.getAddress2());

						actCutAddressThinBc.setAddress1(customerAddressVO.getAddress1());

						actCutAddressThinBc.setAddressType(customerAddressVO.getCustomerAddressType());

						actCutAddressThinBc.setLandMark(customerAddressVO.getLandmark());

						actCutAddressThinBc.setCountry(FWMPConstant.Country_Name.INDIA);
					}

				}

				
			}

		}

		if (prospectCoreVO.getCustomer().getAreaId() != null) {
			actCutAddressThinBc.setArea(LocationCache.getAreaNameById(prospectCoreVO.getCustomer().getAreaId()));
		}

		if (prospectCoreVO.getCustomer().getSubAreaName() != null) {
			actCutAddressThinBc.setSubArea(prospectCoreVO.getCustomer().getSubAreaName());
		}

		if (prospectCoreVO.getCustomer().getBranchId() != null) {
			actCutAddressThinBc
					.setOperationalEntity(LocationCache.getBrancheNameById(prospectCoreVO.getCustomer().getBranchId()));
		}

		if (prospectCoreVO.getCustomer().getCityId() != null) {
			actCutAddressThinBc.setState((CityDAOImpl.cityIdAndStateMap.get(prospectCoreVO.getCustomer().getCityId())));
		}

		if (prospectCoreVO.getCustomer().getCityId() != null) {
		
			String cityName=LocationCache.getCityNameById(prospectCoreVO.getCustomer().getCityId());
			log.info("City name passing to siebel is in actCutAddressThinBc "+cityName);
			actCutAddressThinBc.setCity(cityName);
		}

		if (prospectCoreVO.getCustomer().getLat() != null) {
			actCutAddressThinBc.setLatitude(prospectCoreVO.getCustomer().getLat());
		}

		if (prospectCoreVO.getCustomer().getLon() != null) {
			actCutAddressThinBc.setLongitude(prospectCoreVO.getCustomer().getLon());
		}

		actCutAddressThinBc.setAddressType(SiebelAddressType.INSTALLATION);

		return actCutAddressThinBc;
	}

	/**
	 * @author aditya ActCutAddressThinBc
	 * @param prospectCoreVO
	 * @return
	 */
	private static ActCutAddressThinBc getActCutAddressPermanent(ProspectCoreVO prospectCoreVO) {

		// ***************** Permanent Addresss
		// **********************************

		ActCutAddressThinBc actCutAddressThinBc = new ActCutAddressThinBc();

		if (prospectCoreVO.getCustomer().getCustomerAddressVOs() != null) {

			List<CustomerAddressVO> permanentCustomerAddressvoList = prospectCoreVO.getCustomer()
					.getCustomerAddressVOs();

			for (CustomerAddressVO customerAddressVO : permanentCustomerAddressvoList) {

				if (customerAddressVO.getCustomerAddressType()
						.equalsIgnoreCase(FWMPConstant.AddressType.PERMANENT_ADDRESS)) {

					actCutAddressThinBc.setPostalCode(customerAddressVO.getPincode());

					actCutAddressThinBc.setAddress2(customerAddressVO.getAddress2());

					actCutAddressThinBc.setAddress1(customerAddressVO.getAddress1());

					actCutAddressThinBc.setAddressType(customerAddressVO.getCustomerAddressType());

					actCutAddressThinBc.setLandMark(customerAddressVO.getLandmark());

					actCutAddressThinBc.setCountry(FWMPConstant.Country_Name.INDIA);
				}

			}
		}
		actCutAddressThinBc.setAddressType(SiebelAddressType.AADHAR);

		return actCutAddressThinBc;
	}

	/**
	 * @author aditya WorkOrderActivityUpdateInCRMVo
	 * @param createUpdateWorkOrderVo
	 * @return
	 */
	public static WorkOrderActivityUpdateInCRMVo convert2WO(CreateUpdateWorkOrderVo createUpdateWorkOrderVo) {

		WorkOrderActivityUpdateInCRMVo activityUpdateInCRMVo = new WorkOrderActivityUpdateInCRMVo();

		return activityUpdateInCRMVo;
	}

	/**
	 * @author kiran CreateUpdateFaultRepairRequestInSiebel
	 * @param consolidatedFrVoForSiebel
	 * @return
	 */
	public static CreateUpdateFaultRepairRequestInSiebel convertConsolidatedFrVoForSiebel2CreateUpdateFaultRepairRequestInSiebel(
			ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel) {

		CreateUpdateFaultRepairRequestInSiebel createUpdateFaultRepairRequestInSiebel = new CreateUpdateFaultRepairRequestInSiebel();

		createUpdateFaultRepairRequestInSiebel.setFWMPSRUpsert_Input(getfWMPSRUpsert_Input(consolidatedFrVoForSiebel));

		return createUpdateFaultRepairRequestInSiebel;
	}

	/**
	 * @author pawan FWMPSRUpsert_Input
	 * @return
	 */
	private static FWMPSRUpsert_Input getfWMPSRUpsert_Input(ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel) {

		FWMPSRUpsert_Input fWMPSRUpsert_Input = new FWMPSRUpsert_Input();

		if (consolidatedFrVoForSiebel.getTransaction_spcID() != null)
			fWMPSRUpsert_Input.setTransaction_spcID(consolidatedFrVoForSiebel.getTransaction_spcID());

		fWMPSRUpsert_Input.setListOfFwmpsrthinio(getListOfFwmpsrthinio(consolidatedFrVoForSiebel));
		;

		return fWMPSRUpsert_Input;
	}

	/**
	 * @author pawan ListOfFwmpsrthinio
	 * @param consolidatedFrVoForSiebel
	 * @return
	 */
	private static ListOfFwmpsrthinio getListOfFwmpsrthinio(ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel) {

		ListOfFwmpsrthinio listOfFwmpsrthinio = new ListOfFwmpsrthinio();

		listOfFwmpsrthinio.setServiceRequest_Lightweight(getserviceRequest_Lightweight(consolidatedFrVoForSiebel));

		return listOfFwmpsrthinio;
	}

	/**
	 * @author pawan List<ServiceRequest_Lightweight>
	 * @param consolidatedFrVoForSiebel
	 * @return
	 */
	private static ServiceRequest_Lightweight getserviceRequest_Lightweight(
			ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel) {

		ServiceRequest_Lightweight serviceRequest_Lightweight = new ServiceRequest_Lightweight();

		// List<ServiceRequest_Lightweight> listOfServiceRequest_Lightweight =
		// new
		// ArrayList<ServiceRequest_Lightweight>();

		/*if (consolidatedFrVoForSiebel.getTicketDescription() != null){
			serviceRequest_Lightweight.setTicketDescription(consolidatedFrVoForSiebel.getTicketDescription());
		}
			
		else {
			serviceRequest_Lightweight.setTicketDescription(consolidatedFrVoForSiebel.getVOC());
		}*/
//commented by manjuprasad for ticket description issue		
//		serviceRequest_Lightweight.setTicketDescription("update by tab");
		serviceRequest_Lightweight.setTicketDescription(consolidatedFrVoForSiebel.getTicketDescription());
		
		if (consolidatedFrVoForSiebel.getOwner() != null)
			serviceRequest_Lightweight.setOwner(consolidatedFrVoForSiebel.getOwner().toUpperCase());

		if (consolidatedFrVoForSiebel.getCallType() != null)
			serviceRequest_Lightweight.setCallType(consolidatedFrVoForSiebel.getCallType());

		if (consolidatedFrVoForSiebel.getClosedDate() != null)
			serviceRequest_Lightweight.setClosedDate(consolidatedFrVoForSiebel.getClosedDate());

		if (consolidatedFrVoForSiebel.getResolutionCode() != null)
			serviceRequest_Lightweight.setResolutionCode(consolidatedFrVoForSiebel.getResolutionCode());

		if (consolidatedFrVoForSiebel.getVOC() != null)
			serviceRequest_Lightweight.setVOC(consolidatedFrVoForSiebel.getVOC());

		if (consolidatedFrVoForSiebel.getTicketSource() != null)
			serviceRequest_Lightweight.setTicketSource(consolidatedFrVoForSiebel.getTicketSource());

		if (consolidatedFrVoForSiebel.getNature() != null){
			serviceRequest_Lightweight.setNature(consolidatedFrVoForSiebel.getNature());
		}
		
		if (consolidatedFrVoForSiebel.getPriority() != null)
			serviceRequest_Lightweight.setPriority(consolidatedFrVoForSiebel.getPriority());
		
		
		int status = Integer.valueOf(consolidatedFrVoForSiebel.getStatus()).intValue();

		if (consolidatedFrVoForSiebel.getStatus() != null && consolidatedFrVoForSiebel.getResolutionCode() != null
				&& !consolidatedFrVoForSiebel.getResolutionCode().isEmpty() && consolidatedFrVoForSiebel.getSubResolutionCode() != null && !consolidatedFrVoForSiebel.getSubResolutionCode().isEmpty())
			serviceRequest_Lightweight.setStatus(FWMPConstant.FrStatusForSiebel.COMPLETED);
		else if(status == FRStatus.FR_WORKORDER_REOPEN.intValue())
			serviceRequest_Lightweight.setStatus(FWMPConstant.FrStatusForSiebel.REOPEN);
		else
			serviceRequest_Lightweight.setStatus(FWMPConstant.FrStatusForSiebel.ASSIGNED);
		
		
		if (consolidatedFrVoForSiebel.getSRNumber() != null)
			serviceRequest_Lightweight.setSRNumber(consolidatedFrVoForSiebel.getSRNumber());

		if (consolidatedFrVoForSiebel.getETR() != null)
			serviceRequest_Lightweight.setETR(GenericUtil.getSiebelDateFormatter(consolidatedFrVoForSiebel.getETR()));

		if (consolidatedFrVoForSiebel.getCategory() != null)
			serviceRequest_Lightweight.setCategory(consolidatedFrVoForSiebel.getCategory());

		if (consolidatedFrVoForSiebel.getCustomerNumber() != null)
			serviceRequest_Lightweight.setCustomerNumber(consolidatedFrVoForSiebel.getCustomerNumber());

		if (consolidatedFrVoForSiebel.getResolutionDescription() != null)

			serviceRequest_Lightweight.setResolutionDescription(consolidatedFrVoForSiebel.getResolutionDescription());

		if (consolidatedFrVoForSiebel.getSubResolutionCode() != null)
			serviceRequest_Lightweight.setSubResolutionCode(consolidatedFrVoForSiebel.getSubResolutionCode());

		serviceRequest_Lightweight.setListOfServiceRequest_Lightweight_ACTPriotization(
				getListOfServiceRequest_Lightweight_ACTPriotization(consolidatedFrVoForSiebel));

		// listOfServiceRequest_Lightweight.add(serviceRequest_Lightweight);

		return serviceRequest_Lightweight;
	}

	/**
	 * @author pawan ListOfServiceRequest_Lightweight_ACTPriotization
	 * @param consolidatedFrVoForSiebel
	 * @return
	 */
	private static ListOfServiceRequest_Lightweight_ACTPriotization getListOfServiceRequest_Lightweight_ACTPriotization(
			ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel) {

		ListOfServiceRequest_Lightweight_ACTPriotization listOfServiceRequest_Lightweight_ACTPriotization = new ListOfServiceRequest_Lightweight_ACTPriotization();

		listOfServiceRequest_Lightweight_ACTPriotization.setServiceRequest_Lightweight_ACTPriotization(
				getListOfserviceRequest_Lightweight_ACTPriotization(consolidatedFrVoForSiebel));

		return listOfServiceRequest_Lightweight_ACTPriotization;
	}

	/**
	 * @author pawan List<ServiceRequest_Lightweight_ACTPriotization>
	 * @param consolidatedFrVoForSiebel
	 * @return
	 */
	private static ServiceRequest_Lightweight_ACTPriotization getListOfserviceRequest_Lightweight_ACTPriotization(
			ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel) {

		ServiceRequest_Lightweight_ACTPriotization serviceRequest_Lightweight_ACTPriotization = new ServiceRequest_Lightweight_ACTPriotization();

		// List<ServiceRequest_Lightweight_ACTPriotization>
		// listOfServiceRequest_Lightweight_ACTPriotization = new
		// ArrayList<ServiceRequest_Lightweight_ACTPriotization>();

		if (consolidatedFrVoForSiebel.getPrioritySource() != null)
			serviceRequest_Lightweight_ACTPriotization.setPrioritySource(consolidatedFrVoForSiebel.getPrioritySource());

		// listOfServiceRequest_Lightweight_ACTPriotization.add(serviceRequest_Lightweight_ACTPriotization);

		return serviceRequest_Lightweight_ACTPriotization;
	}

	/**
	 * @author kiran UpdateWorkOrder_Input
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	public static UpdateWorkOrder_Input convertConsolidatedWorkOrderVoForSiebel2UpdateWorkOrder_Input(
			ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		UpdateWorkOrder_Input updateWorkOrderInput = new UpdateWorkOrder_Input();

		updateWorkOrderInput.setTransactionID(consolidatedWorkOrderVoForSiebel.getTransactionID());
		updateWorkOrderInput.setListOfComworkorderio(getListOfComworkorderio(consolidatedWorkOrderVoForSiebel));

		return updateWorkOrderInput;

	}

	/**
	 * @author kiran ListOfComworkorderio
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	private static ListOfComworkorderio getListOfComworkorderio(
			ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		ListOfComworkorderio listOfComworkorderio = new ListOfComworkorderio();

		listOfComworkorderio.setComWorkOrder_Orders(getComWorkOrder_Orders(consolidatedWorkOrderVoForSiebel));

		return listOfComworkorderio;

	}

	/**
	 * @author kiran List<ComWorkOrder_Orders>
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	private static ComWorkOrder_Orders getComWorkOrder_Orders(
			ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		ComWorkOrder_Orders workOrder_Orders = new ComWorkOrder_Orders();

		if (consolidatedWorkOrderVoForSiebel.getACTConnectionType() != null) {
			workOrder_Orders.setACTConnectionType(
					consolidatedWorkOrderVoForSiebel.getACTConnectionType());

		}

		if (consolidatedWorkOrderVoForSiebel.getACTETR() != null) {

			workOrder_Orders
					.setACTETR(GenericUtil.getSiebelDateFormatter(consolidatedWorkOrderVoForSiebel.getACTETR()));
		}

		if (consolidatedWorkOrderVoForSiebel.getActivityType() != null) {
			
			 long id = Long.valueOf(consolidatedWorkOrderVoForSiebel.getActivityType()).longValue();
			 log.info("Activity.CUSTOMER_ACCOUNT_ACTIVATION :: "+id);
			if ( id == Activity.CUSTOMER_ACCOUNT_ACTIVATION)
			{
					workOrder_Orders.setACTResolutionCode(SibelWoCloserCodes.RESOLUTION_CODE);

					workOrder_Orders.setACTSubResolutionCode(SibelWoCloserCodes.SUB_RESOLUTION_CODE);
			}
			
			if(consolidatedWorkOrderVoForSiebel.getStatus() != null)
			{
				if (Integer.parseInt(consolidatedWorkOrderVoForSiebel.getStatus()) == FWMPConstant.TicketStatus.PAYMENT_REFUND_INITIATED) {
					
					workOrder_Orders.setACTResolutionCode(SibelWoCloserCodes.REFUND_RESOLUTION_CODE);

					workOrder_Orders.setACTSubResolutionCode(SibelWoCloserCodes.REFUND_SUB_RESOLUTION_CODE);
				}
			}
		}
		

		log.info("consolidatedWorkOrderVoForSiebel.getAssignedTo() :: "+consolidatedWorkOrderVoForSiebel.getAssignedTo());
		
		if (consolidatedWorkOrderVoForSiebel.getAssignedTo() != null)
			workOrder_Orders.setAssignedTo(consolidatedWorkOrderVoForSiebel.getAssignedTo());

		if (consolidatedWorkOrderVoForSiebel.getCancellationReason() != null)
			workOrder_Orders.setCancellationReason(consolidatedWorkOrderVoForSiebel.getCancellationReason());

		// Action
		workOrder_Orders.setListOfAction(getListOfAction(consolidatedWorkOrderVoForSiebel));

		workOrder_Orders.setListOfComWorkOrder_Orders_ACTPriotization(
				getistOfComWorkOrder_Orders_ACTPriotization(consolidatedWorkOrderVoForSiebel));

		if (consolidatedWorkOrderVoForSiebel.getPriorityValue() != null)
			workOrder_Orders.setPriorityValue(consolidatedWorkOrderVoForSiebel.getPriorityValue());

		if (consolidatedWorkOrderVoForSiebel.getWorkOrderCompletionDate() != null)
			workOrder_Orders.setWorkOrderCompletionDate(consolidatedWorkOrderVoForSiebel.getWorkOrderCompletionDate());

		if (consolidatedWorkOrderVoForSiebel.getWorkOrderNumber() != null)
			workOrder_Orders.setWorkOrderNumber(consolidatedWorkOrderVoForSiebel.getWorkOrderNumber());

		return workOrder_Orders;
	}

	/**
	 * @author kiran ListOfComWorkOrder_Orders_ACTPriotization
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	private static ListOfComWorkOrder_Orders_ACTPriotization getistOfComWorkOrder_Orders_ACTPriotization(
			ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		ListOfComWorkOrder_Orders_ACTPriotization actPriotization = new ListOfComWorkOrder_Orders_ACTPriotization();

		actPriotization.setComWorkOrder_Orders_ACTPriotization(
				getComWorkOrder_Orders_ACTPriotization(consolidatedWorkOrderVoForSiebel));

		return actPriotization;
	}

	/**
	 * @author kiran List<ComWorkOrder_Orders_ACTPriotization>
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	private static ComWorkOrder_Orders_ACTPriotization getComWorkOrder_Orders_ACTPriotization(
			ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		ComWorkOrder_Orders_ACTPriotization actPriotization = new ComWorkOrder_Orders_ACTPriotization();

		if(consolidatedWorkOrderVoForSiebel.getPrioritySource() != null)
		{
			if (Integer.parseInt(consolidatedWorkOrderVoForSiebel.getPrioritySource())==0)

				actPriotization.setPrioritySource(consolidatedWorkOrderVoForSiebel.getPrioritySource());
			
			else
				actPriotization.setPrioritySource(consolidatedWorkOrderVoForSiebel.getPrioritySource());
		}
		

		return actPriotization;

	}

	/**
	 * @author kiran ListOfAction
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	private static ListOfAction getListOfAction(ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		ListOfAction listOfAction = new ListOfAction();

		listOfAction.setAction(getAction(consolidatedWorkOrderVoForSiebel));

		return listOfAction;

	}

	/**
	 * @author kiran List<Action>
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	private static List<Action> getAction(ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		List<Action> actions = new ArrayList<Action>();

		Action action = new Action();

		if (consolidatedWorkOrderVoForSiebel.getACTCATFIVEEndReading() != null)
			action.setACTCATFIVEEndReading(consolidatedWorkOrderVoForSiebel.getACTCATFIVEEndReading());

		if (consolidatedWorkOrderVoForSiebel.getACTCATFIVEStartReading() != null)
			action.setACTCATFIVEStartReading(consolidatedWorkOrderVoForSiebel.getACTCATFIVEStartReading());

		if (consolidatedWorkOrderVoForSiebel.getACTCxIP() != null)
			action.setACTCxIP(consolidatedWorkOrderVoForSiebel.getACTCxIP());

		if (consolidatedWorkOrderVoForSiebel.getACTCxMACId() != null)
			action.setACTCxMACId(consolidatedWorkOrderVoForSiebel.getACTCxMACId());

		if (consolidatedWorkOrderVoForSiebel.getACTCxName() != null)
			action.setACTCxName(consolidatedWorkOrderVoForSiebel.getACTCxName());

		if (consolidatedWorkOrderVoForSiebel.getACTDescription() != null)
			action.setACTDescription(consolidatedWorkOrderVoForSiebel.getACTDescription());

		if (consolidatedWorkOrderVoForSiebel.getACTFiberCableEndReading() != null)
			action.setACTFiberCableEndReading(consolidatedWorkOrderVoForSiebel.getACTFiberCableEndReading());

		if (consolidatedWorkOrderVoForSiebel.getACTFiberCableStartReading() != null)
			action.setACTFiberCableStartReading(consolidatedWorkOrderVoForSiebel.getACTFiberCableStartReading());

		if (consolidatedWorkOrderVoForSiebel.getACTFxIP() != null)
			action.setACTFxIP(consolidatedWorkOrderVoForSiebel.getACTFxIP());

		if (consolidatedWorkOrderVoForSiebel.getACTFxMACId() != null)
			action.setACTFxMACId(consolidatedWorkOrderVoForSiebel.getACTFxMACId());

		if (consolidatedWorkOrderVoForSiebel.getACTFxName() != null)
			action.setACTFxName(consolidatedWorkOrderVoForSiebel.getACTFxName());

		if (consolidatedWorkOrderVoForSiebel.getACTFxPortNumber() != null)
			action.setACTFxPortNumber(consolidatedWorkOrderVoForSiebel.getACTFxPortNumber());

		if (consolidatedWorkOrderVoForSiebel.getACTCxPortNumber() != null) {
			log.info("commonUtiltools getACTCxPortNumber" + consolidatedWorkOrderVoForSiebel.getACTCxPortNumber());
			action.setACTCxPortNumber(consolidatedWorkOrderVoForSiebel.getACTFxPortNumber());
		}

		if (consolidatedWorkOrderVoForSiebel.getACTGxId() != null)
			action.setACTGxId(consolidatedWorkOrderVoForSiebel.getACTGxId());

		if (consolidatedWorkOrderVoForSiebel.getActivityType() != null) {

			String activityName = getActivityTypeByActivityId(
					Long.valueOf(consolidatedWorkOrderVoForSiebel.getActivityType()));

			action.setActivityType(activityName);
		}

		if (consolidatedWorkOrderVoForSiebel.getACTRouterMacId() != null)
			action.setACTRouterMacId(consolidatedWorkOrderVoForSiebel.getACTRouterMacId());

		if (consolidatedWorkOrderVoForSiebel.getCompletionDate() != null)
			action.setCompletionDate(consolidatedWorkOrderVoForSiebel.getCompletionDate());

		if (consolidatedWorkOrderVoForSiebel.getLastUpdatedBy() != null)
			action.setLastUpdatedBy(consolidatedWorkOrderVoForSiebel.getLastUpdatedBy());

		if (consolidatedWorkOrderVoForSiebel.getStatus() != null) {

			String statusName = getStatusByStatusId(consolidatedWorkOrderVoForSiebel.getStatus());
			action.setStatus(statusName);
		}

		if (consolidatedWorkOrderVoForSiebel.getVendorName() != null)
			action.setVendorName(consolidatedWorkOrderVoForSiebel.getVendorName());

		actions.add(action);

		return actions;
	}

	private static String getStatusByStatusId(String statusId) {

		if ((Integer.parseInt(statusId) == FWMPConstant.TicketStatus.ACTIVATION_COMPLETED_VERIFIED) || (Integer
				.parseInt(statusId) == FWMPConstant.TicketStatus.CUSTOMER_ACCOUNT_ACTIVATION_DONE.intValue())) {

			return SiebelWorkOrderStatus.COMPLETED;
		}

		else
			return SiebelWorkOrderStatus.COMPLETED;

	}

	private static String getActivityTypeByActivityId(Long activityId) {

		if (AuthUtils.isNEUser() && (activityId == Activity.BASIC_INFO_UPDATE))
			return Copper_Flow.PHYSICAL_FEASIBILTIY;

		else if(activityId==Activity.BASIC_INFO_UPDATE)
			return Sales_Flow.CUSTOMER_BASIC_INFO_UPDATED;
		
		else if(activityId==Activity.TARIFF_UPDATE)
			return Sales_Flow.TARIFF_PLAN_UPDATED;
		
		else if(activityId==Activity.POA_DOCUMENT_UPDATE)
			return Sales_Flow.POA_DOCUMENT_UPDATED;
		
		else if(activityId==Activity.POI_DOCUMENT_UPDATE)
			return Sales_Flow.POI_DOCUMENT_UPDATED;
		
		else if(activityId==Activity.PAYMENT_UPDATE)
			return Sales_Flow.PAYMENT_UPDATED;
		
		else if(activityId==Activity.PHYSICAL_FEASIBILITY)
			return Copper_Flow.PHYSICAL_FEASIBILTIY;
		
		else if (activityId == Activity.COPPER_LAYING)
			return Copper_Flow.COPPER_LAYING;

		else if (activityId == Activity.CUSTOMER_ACCOUNT_ACTIVATION)
			return Copper_Flow.CUSTOMER_ACCOUNT_ACTIVATION;

		else if (activityId == Activity.CX_RACK_FIXING)
			return Fiber_Flow.CX_RACK_FIXING;

		else if (activityId == Activity.FIBER_LAYING)
			return Fiber_Flow.FIBER_LAYING;

		else if (activityId == Activity.MATERIAL_CONSUMPTION)
			return Fiber_Flow.MATERIAL_CONSUMPTION;

		else if (activityId == Activity.PORT_ACTIVATION)
			return Fiber_Flow.PORT_ACTIVATION;

		else if (activityId == Activity.SPLICING)
			return Fiber_Flow.SPLICING;


		return null;
	}

	private boolean isMobileNumberValid(String mobileNumber) {

		return Pattern.matches(MOBILE_VALIDATION_REG_EXPRESSION, mobileNumber);

	}

	private boolean isEmailValid(String emailId) {

		return Pattern.matches(EMAIL_VALIDATION_REG_EXPRESSION, emailId);

	}

	private boolean isMACValid(String macId) {

		return Pattern.matches(MAC_PATTERN, macId);

	}

	private boolean isIpValid(String ip) {

		return Pattern.matches(IPADDRESS_PATTERN, ip);

	}

	private boolean isZipCodeValid(String pinCode) {

		return Pattern.matches(PIN_CODE_VALIDATION_REG_EXPRESSION, pinCode);

	}

	private static String getInSiebelDateFormatter(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String formattedDate = dateFormat.format(date).toString();
		return formattedDate;
	}

}