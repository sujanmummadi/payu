package com.cupola.fwmp.util;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMError;
import org.w3c.dom.DOMErrorHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSParser;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XMLHelper
{

	private static final Logger LOGGER = Logger.getLogger(XMLHelper.class
			.getName());

	/**
	 * Get the first child elememt with certain tag name
	 * 
	 * @param parent
	 *            Element
	 * @param name
	 *            String
	 * @return Element child XML element
	 */
	public static Element getChildElement(Element parent, String name)
	{

		NodeList nodes = parent.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
			if (nodes.item(i) instanceof Element
					&& ((Element) nodes.item(i)).getTagName().equals(name))
				return (Element) nodes.item(i);
		return null;

	}

	public static Element getNextChildElement(Element parent, String name)
	{

		NodeList nodes = parent.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
			if (nodes.item(i) instanceof Element
					&& ((Element) nodes.item(i)).getTagName().equals(name))
				return (Element) nodes.item(i);
		return null;

	}

	public static Element getNextToNextChildElement(Element parent, String name)
	{

		NodeList nodes = parent.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
			if (nodes.item(i) instanceof Element
					&& ((Element) nodes.item(i)).getTagName().equals(name))
				return (Element) nodes.item(i);
		return null;

	}

	public static Element getNextToNextToNextChildElement(Element parent,
			String name)
	{

		NodeList nodes = parent.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
			if (nodes.item(i) instanceof Element
					&& ((Element) nodes.item(i)).getTagName().equals(name))
				return (Element) nodes.item(i);
		return null;

	}

	public static Element getNextToNextToNextToNextChildElement(Element parent,
			String name)
	{

		NodeList nodes = parent.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++)
			if (nodes.item(i) instanceof Element
					&& ((Element) nodes.item(i)).getTagName().equals(name))
				return (Element) nodes.item(i);
		return null;

	}

	public static Element getChildElementNotNull(Element parent, String tagName)
	{
		Element ce = XMLHelper.getChildElement(parent, tagName);
		if (ce == null)
		{
			throw new IllegalArgumentException("No such child element: "
					+ tagName);
		}
		return ce;
	}

	public static String getChildElementValue(Element parent, String tagName)
	{
		Element ce = XMLHelper.getChildElement(parent, tagName);
		if (ce != null)
		{
			return getElementValue(ce);
		}
		return null;
	}

	/**
	 * Get the first string value for the XML element
	 * 
	 * @param element
	 *            Element
	 * @return String XML element value
	 */
	public static String getElementValue(Element element)
	{
		NodeList list = element.getChildNodes();
		for (int i = 0; i < list.getLength(); i++)
		{
			if (list.item(i).getNodeType() == Node.TEXT_NODE
					|| list.item(i).getNodeType() == Node.CDATA_SECTION_NODE)
			{
				String v = list.item(i).getNodeValue();
				if (v != null && v.trim().length() > 0)
					return v.trim();
			}
		}
		return ""; // for the sake of UI, set it to "" rather than null
	}

	public static String errorCode(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		return XMLHelper.getChildElementValue(responseinfoElement, "errorNo");

	}
	
	public static String message(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		return XMLHelper.getChildElementValue(responseinfoElement, "message");

	}
	public static String transactionNo(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		return XMLHelper.getChildElementValue(responseinfoElement, "transactionNo");

	}

	public static String getMessage(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		return XMLHelper.getChildElementValue(responseinfoElement, "message");

	}

	public static String getType(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		return XMLHelper.getChildElementValue(typeElement, "type");

	}

	public static String getCity(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		return XMLHelper.getChildElementValue(typeElement, "city");

	}

	public static String getBranch(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		return XMLHelper.getChildElementValue(typeElement, "branch");

	}

	public static String getArea(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		return XMLHelper.getChildElementValue(typeElement, "area");

	}

	public static String getClusterName(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element clusterDetails = XMLHelper
				.getNextToNextToNextChildElement(typeElement, "cluster");

		return XMLHelper.getChildElementValue(clusterDetails, "name");

	}

	public static String getFxDetails(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element fxDetails = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "fx");

		return XMLHelper.getChildElementValue(typeElement, "fx");

	}

	public static String getFxMacAddress(Element pElement)
	{
		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element fxDetails = XMLHelper
				.getNextToNextToNextChildElement(typeElement, "fx");

		return XMLHelper.getChildElementValue(fxDetails, "macId");

	}

	public static String getFxName(Element pElement)
	{
		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element fxDetails = XMLHelper
				.getNextToNextToNextChildElement(typeElement, "fx");

		return XMLHelper.getChildElementValue(fxDetails, "name");

	}

	public static String getFxPorts(Element pElement)
	{
		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element fxDetails = XMLHelper
				.getNextToNextToNextChildElement(typeElement, "fx");

		return XMLHelper.getChildElementValue(fxDetails, "ports");

	}

	public static String getCxDetails(Element pElement)
	{

		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		return XMLHelper.getChildElementValue(typeElement, "cx");

	}

	public static String getCxMacAddress(Element pElement)
	{
		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element fxDetails = XMLHelper
				.getNextToNextToNextChildElement(typeElement, "cx");

		return XMLHelper.getChildElementValue(fxDetails, "macId");

	}

	public static String getCxName(Element pElement)
	{
		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element fxDetails = XMLHelper
				.getNextToNextToNextChildElement(typeElement, "cx");

		return XMLHelper.getChildElementValue(fxDetails, "name");

	}

	public static String getCxPorts(Element pElement)
	{
		Element feasibilityCheckResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element feasibilitycheckresultElement = XMLHelper
				.getNextChildElement(feasibilityCheckResponseElement, "feasibilitycheckresult");

		Element responseinfoElement = XMLHelper
				.getNextToNextChildElement(feasibilitycheckresultElement, "responseinfo");

		Element typeElement = XMLHelper
				.getNextToNextToNextChildElement(responseinfoElement, "result");

		Element fxDetails = XMLHelper
				.getNextToNextToNextChildElement(typeElement, "cx");

		return XMLHelper.getChildElementValue(fxDetails, "ports");

	}

	public static String getErrorCodeFromMqResponse(Element pElement)
	{

		Element mqResponseElement = XMLHelper
				.getChildElement(pElement, "feasibilitycheckresponse");

		Element mqStatusElement = XMLHelper
				.getNextChildElement(mqResponseElement, "feasibilitycheckresult");

		// Element responseinfoElement = XMLHelper.getNextToNextChildElement(
		// mqStatusElement, "responseinfo");
		//
		// Element typeElement = XMLHelper.getNextToNextToNextChildElement(
		// responseinfoElement, "result");

		return XMLHelper.getChildElementValue(mqStatusElement, "ERRORNO");

	}

	/**
	 * Add leaf (value) xml element containing value to a multivalued element
	 * 
	 * @param policyData
	 * @param pathName
	 * @param value
	 */
	public static void addXMLValue(Document policyData, String eleName,
			String value, String attrName, String attrValue)
	{
		StringTokenizer st = new StringTokenizer(eleName, ".");
		Element ele = policyData.getDocumentElement();
		String eName = null;
		st.nextToken();
		while (st.hasMoreElements())
		{
			eName = st.nextToken();
			ele = getChildElement(ele, eName);
		}
		ele = (Element) ele.getParentNode();
		Element newEle = policyData.createElement(eName);
		newEle.appendChild(policyData.createTextNode(value));
		if (attrName != null && !attrName.equals(""))
			newEle.setAttribute(attrName, attrValue);
		ele.appendChild(newEle);
	}

	/**
	 * create or replace element value with new value if a node exists it will
	 * be updated to not alter the containing document structure
	 */
	public static void setElementValue(Element domElem, String value)
	{
		// assume only one text_node child exists
		NodeList list = domElem.getChildNodes();
		Node textNode = null;
		for (int i = 0; i < list.getLength(); i++)
		{
			if (list.item(i).getNodeType() == Node.TEXT_NODE)
			{
				textNode = list.item(i);
				break;
			}
		}
		if (textNode == null)
		{
			domElem.appendChild(domElem.getOwnerDocument()
					.createTextNode(value));
		} else
		{
			textNode.setNodeValue(value);
		}
	}

	public static Document newDocument()
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			return dbf.newDocumentBuilder().newDocument();
		} catch (ParserConfigurationException e)
		{
			throw new RuntimeException("Failed to create DOM object.", e);
		}
	}

	public static Document parseXMLfile_OLD(String fileName)
			throws IOException, ParserConfigurationException, SAXException
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setValidating(true); // set true to ensure defaults are expaned?
		dbf.setNamespaceAware(true);
		dbf.setFeature("http://apache.org/xml/features/validation/schema", true);
		dbf.setAttribute("http://apache.org/xml/properties/dom/document-class-name", "org.apache.xerces.dom.PSVIDocumentImpl");

		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setErrorHandler(new MyErrorHandler());
		db.setEntityResolver(new MyEntityResolver());
		return db.parse(new InputSource(fileName));
	}

	private static DOMImplementationLS getDOMImplementationLS()
	{
		// we already know we need the xerces implementation,
		// there is no need for the factory lookup
		// System.setProperty(DOMImplementationRegistry.PROPERTY,
		// "org.apache.xerces.dom.DOMXSImplementationSourceImpl");
		// DOMImplementationRegistry registry = null;
		// try {
		// registry = DOMImplementationRegistry.newInstance();
		// } catch (Exception e) {
		// throw new
		// RuntimeException("Failed to obtain DOMImplementationRegistry", e);
		// }
		// DOMImplementationLS impl =
		// (DOMImplementationLS) registry.getDOMImplementation("psvi");
		// DOMImplementationLS impl =
		// (DOMImplementationLS)registry.getDOMImplementation("LS");

		DOMImplementationLS impl = (DOMImplementationLS) new org.apache.xerces.dom.DOMXSImplementationSourceImpl()
				.getDOMImplementation("psvi");

		return impl;
	}

	/**
	 * Parse the file using DOM3 LS. Assumes Xerces parser and the returned
	 * document element to contain PSVI (ElementPSVI) for schema retrieval.
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static Document parseAndValidate(String fileName, ErrorList errorMap)
	{
		DOMImplementationLS domImpl = getDOMImplementationLS();
		LSParser builder = domImpl
				.createLSParser(DOMImplementationLS.MODE_SYNCHRONOUS, null);

		DOMConfiguration config = builder.getDomConfig();

		config.setParameter("error-handler", new MyDOMErrorHandler(errorMap));

		config.setParameter("validate", Boolean.TRUE);
		config.setParameter("schema-type", "http://www.w3.org/2001/XMLSchema");
		config.setParameter("validate-if-schema", Boolean.TRUE);

		// need this to get schema info / ElementPSVI
		config.setParameter("psvi", Boolean.TRUE);
		Document document = null;
		try
		{
			document = builder.parseURI(fileName);
		} catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("Error in parsing file " + fileName + " Error : "
					+ e.getMessage());
		}
		// XSElementDeclaration xsElem =
		// ((ElementPSVI)document.getDocumentElement()).getElementDeclaration();
		return document;
	}

	public static void serializeXML(Document doc, Writer w)
			throws java.io.IOException
	{
		DOMImplementationLS domImpl = getDOMImplementationLS();
		LSSerializer dom3Writer = domImpl.createLSSerializer();
		LSOutput out = domImpl.createLSOutput();
		out.setCharacterStream(w);

		DOMConfiguration config = dom3Writer.getDomConfig();
		// https://jaxp.dev.java.net/issues/show_bug.cgi?id=6
		// works standalone with xerces 2.9 but not with the xerces in
		// jboss-4.0.3SP1
		// this try/catch kludge will fallback to the legacy serializer
		boolean useDom3Writer = true;
		try
		{
			config.setParameter("format-pretty-print", Boolean.TRUE);

		} catch (RuntimeException e)
		{
			LOGGER.warn("Using legacy XMLSerializer due to known format-pretty-print issue: "
					+ e.getMessage());
			useDom3Writer = false;
		}

		if (useDom3Writer)
		{
			dom3Writer.write(doc, out);
		} else
		{
			// deprecated xerces code
			XMLSerializer serialize = new XMLSerializer();
			serialize.setOutputCharStream(w);
			OutputFormat format = new OutputFormat();
			format.setIndenting(true);
			serialize.setOutputFormat(format);
			serialize.serialize(doc);
		}
	}

	public static class ErrorList
	{

		public static class ErrorEntry
		{
			String code;
			String message;
		}

		public List<ErrorEntry> errors = new java.util.ArrayList<ErrorEntry>();

		public void add(String code, String message)
		{
			ErrorEntry e = new ErrorEntry();
			e.code = code;
			e.message = message;
			errors.add(e);
		}

		public ErrorEntry find(String code, Element element)
		{
			ErrorEntry matchByCodeOnly = null;
			for (ErrorEntry e : errors)
			{
				if (code.equals(e.code))
				{
					if (matchByCodeOnly == null)
					{
						matchByCodeOnly = e;
					}
					String elementValue = getElementValue(element);
					if (!StringUtils.isEmpty(elementValue)
							&& e.message.indexOf(getElementValue(element)) > 0)
					{
						return e;
					}
				}
			}
			return matchByCodeOnly;
		}

		public void remove(ErrorEntry e)
		{
			errors.remove(e);
		}

	}

	private static class MyDOMErrorHandler implements DOMErrorHandler
	{
		private ErrorList errors = null;

		private MyDOMErrorHandler(ErrorList errorMap)
		{
			this.errors = errorMap;
		}

		public boolean handleError(DOMError error)
		{
			if (error.getSeverity() == DOMError.SEVERITY_FATAL_ERROR)
			{
				return false;
			}
			// only check for errors in the document itself,
			// ignore schema issues that are also reported as errors..
			// http://www.w3.org/TR/xmlschema-1/#outcomes
			if (error.getType().startsWith("cvc"))
			{
				errors.add(error.getType(), error.getMessage());
			} else
			{
				/*
				 * String msg = "Ignoring error: " + error.getMessage() +
				 * " location: " + error.getLocation().getUri();
				 * LOGGER.error(msg);
				 */
			}
			return true;
		}
	}

	private static class MyErrorHandler implements ErrorHandler
	{
		private Map<String, String> errors = new HashMap<String, String>();

		public void error(SAXParseException exception) throws SAXException
		{
			String msg = exception.getMessage();
			// get code from message
			int pos = msg.indexOf(":");
			if (pos > 0)
			{
				errors.put(msg.substring(0, pos), exception.getMessage());
			} else
			{
				throw exception;
			}
		}

		public void fatalError(SAXParseException exception) throws SAXException
		{
			throw exception;
		}

		public void warning(SAXParseException exception) throws SAXException
		{
			// ignore
			exception.printStackTrace();
		}

	}

	private static class MyEntityResolver implements EntityResolver
	{
		public InputSource resolveEntity(String publicId, String systemId)
				throws SAXException, IOException
		{
			// TODO Auto-generated method stub
			return null;
		}
	}

}
