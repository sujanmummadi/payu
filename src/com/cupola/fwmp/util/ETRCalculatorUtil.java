/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.util;

/**
 * @author kiran
 */

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Value;

public class ETRCalculatorUtil
{

	@Value("${act.business.start.hour}")
	private int BUSINESS_START_HOUR;

	@Value("${act.business.end.hour}")
	private int BUSINESS_END_HOUR;

	@Value("${act.working.mins.per.day}")
	private int WORKING_MINS_PER_DAY;

	private int MINUTES_PER_HOUR = 60;

	/**
	 * calculate ETR in working hours
	 * 
	 * @param issueReportedAt
	 * @param etrInHOursOrDays
	 * @param type
	 * @return
	 */
	public Date calculateETRInWorkingHours(Date issueReportedAt,
			long etrInHOursOrDays, String type)
	{
		if (type.equalsIgnoreCase("days"))
		{

			etrInHOursOrDays = etrInHOursOrDays * WORKING_MINS_PER_DAY; // Converting
																		// Days
																		// to
																		// Mins
		} else if (type.equalsIgnoreCase("hours"))
		{

			etrInHOursOrDays = etrInHOursOrDays * MINUTES_PER_HOUR; // Converting
																	// Hours to
																	// Mins
		}
		Calendar n_givenDateTime = Calendar.getInstance();
		n_givenDateTime.setTime(issueReportedAt);
		addBusinessTime(n_givenDateTime, etrInHOursOrDays);
		return n_givenDateTime.getTime();

	}

	/**
	 * add Review Time (in minutes) to Given Date
	 * 
	 * @param givenDateTime
	 * @param addTimeMins
	 */

	public void addBusinessTime(Calendar givenDateTime, long addTimeMins)
	{
		// To get Present Working Day & Hour excluding Holidays
		adjustToBusinessHours(givenDateTime);

		while (addTimeMins > 0)
		{
			// Finding Business End Time On That Day
			Calendar businessEndTime = Calendar.getInstance();
			businessEndTime.setTime(givenDateTime.getTime());
			businessEndTime.set(Calendar.HOUR_OF_DAY, BUSINESS_END_HOUR);
			businessEndTime.clear(Calendar.MINUTE);

			// Difference of Time in Minutes AND Conversion Milli-Seconds to
			// Minutes
			int diffMins = (int) ((businessEndTime.getTimeInMillis() - givenDateTime
					.getTimeInMillis()) / (1000 * 60));

			if (addTimeMins < diffMins) // Adding Total Minutes to the Same Day
			{

				givenDateTime.add(Calendar.MINUTE, (int) addTimeMins);
				break;
			} else
			// Adding available Minutes to the Same Day
			{

				givenDateTime.add(Calendar.MINUTE, diffMins);
				adjustToBusinessHours(givenDateTime);
				addTimeMins = addTimeMins - diffMins;
			}
		}
	}

	/**
	 * skip Non-Business Hours (Before 10 AM and After 8 PM)
	 * 
	 * @param givenDateTime
	 */

	public void adjustToBusinessHours(Calendar givenDateTime)
	{

		int hourOfDay = givenDateTime.get(Calendar.HOUR_OF_DAY);

		if (hourOfDay < BUSINESS_START_HOUR) // Skip to 10 AM on that Day
		{

			givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
			givenDateTime.clear(Calendar.MINUTE);
		}

		if (hourOfDay >= BUSINESS_END_HOUR) // Skip to Next Day 10 AM
		{

			givenDateTime.add(Calendar.DATE, 1);
			givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
			givenDateTime.clear(Calendar.MINUTE);
		}

		skipHolidays(givenDateTime); // skip Holidays
	}

	/**
	 * skip Holidays ( Weekends and General )
	 * 
	 * @param givenDateTime
	 */

	public void skipHolidays(Calendar givenDateTime)
	{
		skipGeneralHolidays(givenDateTime); // General Holidays before Weekends
		/*
		 * if(givenDateTime.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY) {
		 * 
		 * givenDateTime.add(Calendar.DATE,2); // Saturday and Sunday // in case
		 * of Business Hour givenDateTime.set(Calendar.HOUR_OF_DAY,
		 * BUSINESS_START_HOUR); givenDateTime.clear(Calendar.MINUTE); }
		 */

		/*if (givenDateTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
		{

			givenDateTime.add(Calendar.DATE, 1); // Sunday
													// in case of Business Hour
			givenDateTime.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
			givenDateTime.clear(Calendar.MINUTE);
		}*/

		//skipGeneralHolidays(givenDateTime); // General Holidays after Weekends
	}

	/**
	 * skip General Holidays
	 * 
	 * @param givenDateTime
	 */

	public void skipGeneralHolidays(Calendar givenDateTime)
	{
		// Considering only given Date without Time (Temp)
		Calendar givenDate = Calendar.getInstance();
		givenDate.setTime(givenDateTime.getTime());
		givenDate.set(Calendar.HOUR_OF_DAY, 0);
		givenDate.set(Calendar.MINUTE, 0);
		givenDate.set(Calendar.SECOND, 0);
		givenDate.set(Calendar.MILLISECOND, 0);

		// List of General Holidays
		GregorianCalendar[] generalHolidays = getGeneralHolidays(givenDate);
		if (generalHolidays != null)
		{
			for (int i = 0; i < generalHolidays.length; i++) // Check for all
																// Holidays
			{

				if (givenDate.equals(generalHolidays[i]))
				{

					givenDateTime.add(Calendar.DATE, 1); // Add one date to
															// Actual Date &
															// Time
															// in case of
															// Business Hour
					givenDateTime
							.set(Calendar.HOUR_OF_DAY, BUSINESS_START_HOUR);
					givenDateTime.clear(Calendar.MINUTE);

					givenDate.add(Calendar.DATE, 1); // Add one date to Temp
														// Date
				}
			}
		}

	}

	/**
	 * General Holidays list
	 * 
	 * @param givenDate
	 * @return
	 */

	public GregorianCalendar[] getGeneralHolidays(Calendar givenDate)
	{

		GregorianCalendar[] genHolidays /*
										 * = {
										 * 
										 * new
										 * GregorianCalendar(givenDate.get(Calendar
										 * .YEAR), Calendar.JANUARY, 14), new
										 * GregorianCalendar
										 * (givenDate.get(Calendar.YEAR),
										 * Calendar.JANUARY, 26), new
										 * GregorianCalendar
										 * (givenDate.get(Calendar.YEAR),
										 * Calendar.MAY, 1), new
										 * GregorianCalendar
										 * (givenDate.get(Calendar.YEAR),
										 * Calendar.AUGUST, 15), new
										 * GregorianCalendar
										 * (givenDate.get(Calendar.YEAR),
										 * Calendar.OCTOBER, 2), new
										 * GregorianCalendar
										 * (givenDate.get(Calendar.YEAR),
										 * Calendar.NOVEMBER, 1), new
										 * GregorianCalendar
										 * (givenDate.get(Calendar.YEAR),
										 * Calendar.DECEMBER, 25)
										 * 
										 * }
										 */= null;

		return genHolidays;
	}

}
