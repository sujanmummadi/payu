package com.cupola.fwmp.util;

import java.util.Comparator;

import com.cupola.fwmp.vo.TicketDetailVo;

public class TicketPriorityComparator implements Comparator<TicketDetailVo>
{

	@Override
	public int compare(TicketDetailVo o1, TicketDetailVo o2)
	{
		if(o1 == null || o2 == null)
			return 1;
		
		 return o2.getPriority()- o1.getPriority() ;
	}

}
