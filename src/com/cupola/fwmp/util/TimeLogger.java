 
package com.cupola.fwmp.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class TimeLogger
{
	private static final Logger LOGGER = Logger.getLogger (TimeLogger.class.getName ());
	private static int MAX_SIZE = 1000;
	private static Map<String, Long> timeMap = new HashMap<String, Long> ();
	private static boolean loggingEnabled = true;

	public static void start (String id)
	{
		if (loggingEnabled) {
			t1 (id);
		}
	}

	public static void end (String id)
	{
		if (loggingEnabled) {
			t2 (id, "");
		}

	}
	
	public static void end (String id, String additionalMsg)
	{
		if (loggingEnabled) {
			t2 (id, additionalMsg);
		}
		
	}

	private static  void t1 (String someName)
	{
		
		if (timeMap.size () > MAX_SIZE) {
			LOGGER.info ("clearing cache, size exceeded, MAX_SIZE=" + MAX_SIZE);
			timeMap.clear ();

		}

		timeMap.put (createTaskId (someName), Long.valueOf (System.currentTimeMillis ()));
	}

	private static  void t2 (String someName,  String additionalMsg)
	{
		Long t1 = timeMap.get (createTaskId (someName));
		if (t1 != null) {
			LOGGER.info (" time taken to finish task '" + someName + "' is "
				+ (System.currentTimeMillis () - t1) + "ms"   );
		} else {
			LOGGER.info (" task=" + someName + " start was not called"  );
		}
	}

	private static String createTaskId (String someName)
	{
		return Thread.currentThread().getName() + ":"+ someName;
	}
}
