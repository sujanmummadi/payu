package com.cupola.fwmp.util;

import java.util.Map;

public class EtrUpdateReasonCode {

	private Map<String, String> etrReasonCode;

	public Map<String, String> getEtrReasonCode() {
		return etrReasonCode;
	}

	public void setEtrReasonCode(Map<String, String> etrReasonCode) {
		this.etrReasonCode = etrReasonCode;
	}

	@Override
	public String toString() {
		return "EtrUpdateReasonCode [etrReasonCode=" + etrReasonCode + "]";
	}

}
