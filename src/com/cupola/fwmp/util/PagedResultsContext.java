
package com.cupola.fwmp.util;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class PagedResultsContext
	implements Serializable
{
	private int startOffset;
	private int pageSize;
	/*
	 * This list contains database record offset for each page, UI returns this
	 * object as it is,first call from UI returns null. Nth page offset is set
	 * at index N-1*
	 */
	private List<Integer> startOffsetByPageNumberList;
	private String sortByField;
	private String sortByOrder;

	public PagedResultsContext ()
	{
	}

	public PagedResultsContext (int startOffset, int pageSize)
	{
		this (startOffset, pageSize, null);
	}

	public PagedResultsContext (int startOffset, int pageSize, String sortByField)
	{
		this.setStartOffset (startOffset);
		this.setPageSize (pageSize);
		this.setSortByField (sortByField);
	}

	public int getPageSize ()
	{
		return pageSize;
	}

	public void setPageSize (int pageSize)
	{
		this.pageSize = pageSize;
	}

	public String getSortByField ()
	{
		return sortByField;
	}

	public void setSortByField (String sortByField)
	{
		this.sortByField = sortByField;
	}

	public int getStartOffset ()
	{
		return startOffset;
	}

	public void setStartOffset (int startOffset)
	{
		this.startOffset = startOffset;
	}

	public String getSortByOrder ()
	{
		return sortByOrder;
	}

	public void setSortByOrder (String sortByOrder)
	{
		this.sortByOrder = sortByOrder;
	}

	public List<Integer> getStartOffsetByPageNumberList ()
	{
		return startOffsetByPageNumberList;
	}

	public void setStartOffsetByPageNumberList (List<Integer> startOffsetByPageNumberList)
	{
		this.startOffsetByPageNumberList = startOffsetByPageNumberList;
	}

	public String toString ()
	{
		return " startOffset=" + startOffset + " pageSize=" + pageSize + " sortByField="
			+ sortByField + " sortByOrder=" + sortByOrder + " startOffsetByPageNumberList="
			+ startOffsetByPageNumberList + " super=" + super.toString () + " threa-name="
			+ Thread.currentThread ().getName ();
	}
}
