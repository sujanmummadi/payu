 
package com.cupola.fwmp.util;

import java.util.List;

public class PagedResultSetImpl extends PagedResultsContext
	implements PagedResultSet
{
	private int totalRecords;
	private List results;

	public PagedResultSetImpl (int startOffset, int pageSize)
	{
		super ();
		super.setPageSize (pageSize);
		super.setStartOffset (startOffset);
	}

	public PagedResultSetImpl (int startOffset, int pageSize,
		List<Integer> startOffsetByPageNumberList)
	{
		super ();
		super.setPageSize (pageSize);
		super.setStartOffset (startOffset);
		super.setStartOffsetByPageNumberList (startOffsetByPageNumberList);
	}

	public PagedResultSetImpl (PagedResultsContext pagedCtx)
	{
		super ();
		super.setPageSize (pagedCtx.getPageSize ());
		super.setSortByField (pagedCtx.getSortByField ());
		super.setStartOffset (pagedCtx.getStartOffset ());
		super.setStartOffsetByPageNumberList (pagedCtx.getStartOffsetByPageNumberList ());
	}

	public List getResults ()
	{
		return results;
	}

	public void setResults (List results)
	{
		this.results = results;
	}

	public int getTotalRecords ()
	{
		return totalRecords;
	}

	public void setTotalRecords (int totalRecords)
	{
		this.totalRecords = totalRecords;
	}

}
