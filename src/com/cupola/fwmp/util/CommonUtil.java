/**
 * 
 */
package com.cupola.fwmp.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.util.CellReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import com.cupola.fwmp.FWMPConstant.AddressType;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.vo.NodeValueVo;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author nalina
 * 
 */
public class CommonUtil
{
	private static final Logger logger = LogManager
			.getLogger(CommonUtil.class.getName());
	static Properties prop = null;
	private static final String PATTERN = "[{!@$%^.|\\;\"''&*()_+:}]";

	public static Long macToLong(String macStr)
	{

		byte[] addr = new byte[6];
		long address = 0;

		String[] hex = macStr.split("(\\:|\\-)");

		try
		{

			for (int i = 0; i < 6; i++)
			{
				addr[i] = (byte) Integer.parseInt(hex[i], 16);
			}

			address = unsignedByteToLong(addr[5]);
			address |= (unsignedByteToLong(addr[4]) << 8);
			address |= (unsignedByteToLong(addr[3]) << 16);
			address |= (unsignedByteToLong(addr[2]) << 24);
			address |= (unsignedByteToLong(addr[1]) << 32);
			address |= (unsignedByteToLong(addr[0]) << 40);

		} catch (NumberFormatException e)
		{
			logger.error("Error while formating number "+e.getMessage());
		}
		return address;

	}

	private static long unsignedByteToLong(byte b)
	{

		return (long) b & 0xFF;
	}

	public static void readProperties()
	{

		if (prop != null)
		{

			// log.info("Loading values ");
			// // uploads();
			// log.info("values Loaded");

		} else
		{

			logger.info("Initializing parameters from properties...!");

			prop = new Properties();

			FileInputStream file = null;
			String path = null;

			logger.info("Properties file Locations "
					+ System.getenv("FWMP_BASE") + "fwmp.properties");

			try
			{

				path = System.getenv("FWMP_BASE") + "fwmp.properties";

			} catch (Exception e1)
			{

				logger.error("Error in gettting file path :" + e1.getMessage());
				e1.printStackTrace();
			}

			try
			{

				file = new FileInputStream(path);

				prop.load(file);

			} catch (FileNotFoundException e)
			{

				logger.error("Fatal Error Please make sure properties is available at "
						+ System.getenv("FWMP_BASE") + "fwmp.properties");
				e.getStackTrace();

			} catch (IOException e)
			{
				logger.error("Error while loading property files "+e.getMessage());

				e.printStackTrace();

			} finally
			{

				try
				{

					file.close();

				} catch (IOException e)
				{

					logger.error("Error in side finnaly block of properties file loader "
							+ " " + e.getMessage());

					e.printStackTrace();

				} // End of catch in finally block

			} // End of finally block

		} // End of file existence check

	}// End of method

	public static String getPropertyByName(String key)
	{

		readProperties();

		return prop.getProperty(key);

	}

	public static List<TypeAheadVo> xformToTypeAheadFormat(
			Map<Long, String> values)
	{
		Set<TypeAheadVo> returnValue = new TreeSet<TypeAheadVo>();
		if (values == null)
			return new ArrayList<TypeAheadVo>();

		for (Map.Entry<Long, String> entry : values.entrySet())
		{
			returnValue.add(new TypeAheadVo(entry.getKey(), entry.getValue()));
		}
		 ArrayList<TypeAheadVo> sortedValue=new ArrayList<TypeAheadVo>(returnValue);
	     Collections.sort(sortedValue);
	     return sortedValue;

		//return new ArrayList<TypeAheadVo>(returnValue);
	}

	public static List<NodeValueVo> xformToNodeValueFormat(
			Map<String, String> values)
	{
		Set<NodeValueVo> returnValue = new TreeSet<NodeValueVo>();
		if (values == null)
			return new ArrayList<NodeValueVo>();

		for (Map.Entry<String, String> entry : values.entrySet())
		{
			returnValue.add(new NodeValueVo(entry.getKey(), entry.getValue()));
		}

		return new ArrayList<NodeValueVo>(returnValue);
	}

	/*
	 * public static Map<String, String> populateAdditionalAttributes(){
	 * 
	 * Map<String, String> additionalAttributes = new HashMap<String, String>();
	 * 
	 * 
	 * additionalAttributes.put(Job.AdditionalJobDetails.CUSTOMER_NAME, "");
	 * 
	 * return additionalAttributes; }
	 */

	public static PagedResultSet createPagedResultSet(List resultList)
	{
		PagedResultsContext pc = ApplicationUtils.retrievePagingContext();

		Long offset = null;
		Long pageSize = null;
		PagedResultSetImpl resultSet = new PagedResultSetImpl(0, 0);

		if (pc != null)
		{
			offset = Long.valueOf(pc.getStartOffset());
			pageSize = Long.valueOf(pc.getPageSize());
			resultSet = new PagedResultSetImpl(pc);
		}

		if (resultList == null || resultList.size() == 0)
		{
			return resultSet;
		}

		if (pc == null)
		{
			resultSet.setPageSize(resultList.size());
			resultSet.setResults(resultList);
			return resultSet;
		}

		resultSet.setTotalRecords(resultList.size());

		int startIdx = offset.intValue();
		int endIdx = offset.intValue() + pageSize.intValue();

		if (startIdx >= resultList.size())
		{
			startIdx = resultList.size();
		}
		if (endIdx >= resultList.size())
		{
			endIdx = resultList.size();
		}
		resultSet.setStartOffset(offset.intValue() + resultList.size());
		resultSet.setResults(resultList);

		return resultSet;
	}

	public static void printHeaders(HttpServletRequest httpRequest)
	{
		Enumeration<String> headerNames = httpRequest.getHeaderNames();

		if (headerNames != null)
		{
			while (headerNames.hasMoreElements())
			{
				String name = headerNames.nextElement();
			}
		}
	}

	public static String[] getLatLongPositionsFromAddress(String address)
			throws Exception
	{

		CloseableHttpClient httpclient = HttpClients.createDefault();

		URIBuilder uriBuilder = new URIBuilder("https://maps.googleapis.com/maps/api/geocode/xml")
				.addParameter("address", address)
				// .addParameter("latlng", "12.942453,%2077.606089") // to get
				// address read ormatted_address of response
				.addParameter("sensor", "true")
				.addParameter("key", "AIzaSyCzmnimtfkta4QrPgiQP2cTJTy42Howf74");

		HttpGet postRequest = new HttpGet(uriBuilder.build());

		CloseableHttpResponse response = httpclient.execute(postRequest);

		if (response.getStatusLine().getStatusCode() != 200)
		{
			response.close();httpclient.close();

			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatusLine().getStatusCode());
		}

		HttpEntity entity = response.getEntity();

		DocumentBuilder builder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();

		Document document = builder.parse(entity.getContent());

		XPathFactory xPathfactory = XPathFactory.newInstance();

		XPath xpath = xPathfactory.newXPath();

		XPathExpression expr = xpath.compile("/GeocodeResponse/status");

		String status = (String) expr.evaluate(document, XPathConstants.STRING);

		if (status.equals("OK"))
		{

			expr = xpath.compile("//geometry/location/lat");

			String latitude = (String) expr
					.evaluate(document, XPathConstants.STRING);

			expr = xpath.compile("//geometry/location/lng");

			String longitude = (String) expr
					.evaluate(document, XPathConstants.STRING);
			response.close();httpclient.close();

			return new String[] { latitude, longitude };

		} else
		{
			logger.error("Error from the API - response status: " + status);
			response.close();httpclient.close();
			return new String[] { "0", "0" };

		}

	}

	public static String replace(String str)
	{
		if (str != null && !str.isEmpty())
			return str.replaceAll(PATTERN, " ").replaceAll("\\r?\\n", " ");
		else
			return str;
	}

	public static String getTodayMidnight(int minusDays)
	{
//		String hrMinSEc = " 00:00:00";

//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		Date utilDate = new Date();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, -minusDays);
		utilDate = cal.getTime();
		
		String date = sdf.format(utilDate);

//		date = date + hrMinSEc;
		return date;
	}
	public static String getPriviousDateByMinutes( String lastFetchedDate, int miutesBack )
	{

		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date utilDate = null;
			
			Date privious = sdf.parse( lastFetchedDate );
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(privious);
			cal.add( Calendar.MINUTE, -miutesBack);
			utilDate = cal.getTime();
			
			String date = sdf.format(utilDate);
			
			return date;
		} 
		catch (Exception e)
		{
			logger.error("Error in getPriviousDateBy2Minute " + e.getMessage(),e);
			e.printStackTrace();
			
			return lastFetchedDate;
		}
	}
	
	public static String getPriviousDateBy2Minute( String lastFetchedDate )
	{

		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date utilDate = null;
			
			Date privious = sdf.parse( lastFetchedDate );
			logger.info(" ********** PREVIOUS  DATE : ***********"+privious);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(privious);
			cal.add( Calendar.MINUTE, -2);
			utilDate = cal.getTime();
			
			String date = sdf.format(utilDate);

			logger.info("-------- THIS THE DATE TO USE FIRE MQ QUERY---------------"+date);

			return date;
		} 
		catch (Exception e)
		{
			logger.error("Error in getPriviousDateBy2Minute " + e.getMessage(),e);
			e.printStackTrace();
			
			return lastFetchedDate;
		}
	}
	
	public static String getPriviousDateByHours( String lastFetchedDate )
	{

		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date utilDate = null;
			String date = null;
			Date privious = null;
			
			Calendar cal = Calendar.getInstance();
			if( lastFetchedDate != null )
			{
				privious = sdf.parse( lastFetchedDate );
				cal.setTime(privious);
			}
			logger.info(" ********** PREVIOUS  DATE BY 60 MINUTE BEFORE : ***********"+privious);
			
			cal.add( Calendar.MINUTE, -60 );
			utilDate = cal.getTime();
			
			date = sdf.format(utilDate);

			logger.info("THIS THE DATE TO USE FIRE MQ QUERY BY 60 MINUTE "+date);

			return date;
		} 
		catch (Exception e)
		{
			logger.error("Error in getPriviousDateByHours " + e.getMessage(),e);
			e.printStackTrace();
			
			return lastFetchedDate;
		}
	}
	
	public static Timestamp getSqlTimeStamp(String dateTime)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = null;
		try
		{
			date = sdf.parse(dateTime);
		} catch (ParseException e)
		{
			logger.error("Error in converting timestamp in sql time stamp " + e.getMessage());
			e.printStackTrace();
		}
		Timestamp timestamp = new Timestamp(date.getTime());
		return timestamp;
	}
	
	public static String checkCellType(Cell cell) {

		DataFormatter formatter = new DataFormatter(Locale.getDefault());
		String cellValue = null;
		CellReference ref = new CellReference(cell);

		switch (cell.getCellType()) {

		case Cell.CELL_TYPE_STRING:
			ref.formatAsString();
			cellValue = formatter.formatCellValue(cell);
			break;

		case Cell.CELL_TYPE_NUMERIC:
			ref.formatAsString();
			cellValue = formatter.formatCellValue(cell);
			break;

		}

		return cellValue;
	}

	public static String getTicketGART(int status, Date ...ticketCreationDate )
	{
		if( ticketCreationDate == null )
			return "";
		
		long timeDeffInMills = 0l;
		if( TicketStatus.COMPLETED_DEF.contains(status) 
				&& ticketCreationDate.length == 2 )
			timeDeffInMills = ticketCreationDate[1].getTime() - ticketCreationDate[0].getTime();
		else
			timeDeffInMills = System.currentTimeMillis() - ticketCreationDate[0].getTime();
		return getGARTAsString( timeDeffInMills,"YES" );
	}

	public static String getGARTAsString(long timeValueInSecond, String ...isMiliValue)
	{
		String GART = "";
		if( timeValueInSecond <= 0 )
			return GART;
		
		if( isMiliValue != null && isMiliValue.length == 1 )
			timeValueInSecond = timeValueInSecond / 1000;
		
		long aveHours = timeValueInSecond / 3600;
		long aveMinut = ( timeValueInSecond / 60 ) % 60;
		long aveScond = ( timeValueInSecond  % 60 );
		
		int hoursLength = String.valueOf(aveHours).length();
		int minutLength = String.valueOf(aveMinut).length();
		int secondLength = String.valueOf(aveScond).length();
		
		if( hoursLength >= 2)
			GART = GART + aveHours;
		else
			GART = GART + "0"+aveHours;
		
		if( minutLength >= 2 )
			GART = GART + ":"+aveMinut;
		else
			GART = GART + ":0"+aveMinut;
		
		if( secondLength >= 2)
			GART = GART + ":"+aveScond;
		else
			GART = GART + ":0"+aveScond;
		
		return GART;
	}
	
	/*public static String jsonToStringConvertor(Object obj){
		try{
			
		}catch(Exception e)
				{
			
				}
		return null;
	}*/
	
	public static boolean isValidJson(String text)
	{
		try {
			new JSONObject(text);
		} catch (JSONException ex) {
			// edited, to include @Arthur's comment
			// e.g. in case JSONArray is valid as well...
			try {
				new JSONArray(text);
			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}
	
	public String jsonToStringConvertor(Object obj){
		
		 ProspectCoreVO prospect= (ProspectCoreVO)obj;
		
         List<CustomerAddressVO> customeraddressvos=prospect.getCustomer().getCustomerAddressVOs();
		 
 
			 
		 for (Iterator<CustomerAddressVO> iterator = customeraddressvos.iterator(); iterator.hasNext();) {
			 
			try {
				
				CustomerAddressVO customerAddressVO = (CustomerAddressVO) iterator.next();

				ObjectMapper mapper = new ObjectMapper();
				String addressJsonString = mapper.writeValueAsString(customerAddressVO);

				if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.CURRENT_ADDRESS))
					prospect.getCustomer().setCurrentAddress(addressJsonString);
				logger.info("addressJsonString"+addressJsonString);
			     
				if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.PERMANENT_ADDRESS))
					prospect.getCustomer().setPermanentAddress(addressJsonString);

				if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.COMMUNICATION_ADDRESS))
					prospect.getCustomer().setCommunicationAdderss(addressJsonString);

			}   catch (Exception e) {
				
				logger.error("Error while converting address into json format " + e.getMessage());
				e.printStackTrace();
				continue;
			}
		}
		return null;
	}

	/**@author aditya
	 * com.fasterxml.jackson.databind.ObjectMapper
	 * @return
	 */
	public static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();

		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
	    mapper.disable(MapperFeature.AUTO_DETECT_GETTERS);
	    mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
	    
	    return mapper;
	   
	}
	
	
}
