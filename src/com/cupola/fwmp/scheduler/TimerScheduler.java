package com.cupola.fwmp.scheduler;

import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cupola.fwmp.service.priorityWeightage.PriorityWeightageCoreService;
import com.cupola.fwmp.service.priorityWeightage.PriorityWeightageCoreServiceImpl;



public class TimerScheduler {


    private static final Log logger = LogFactory.getLog(TimerScheduler.class);
    
    private Timer timer;
    private long interval;
    private String date;
    private Object target;
    private String methodName;
    private int starttimeInHours;
    
    
    public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	 

    public static final String INHIBIT_SCHEDULER_PROPERTY = "com.cupola.fwmp.inhibitTimerScheduler";

    /**
     * After properties set method. Needs to be wired in
     * Spring.
     */
    public void init() throws NoSuchMethodException {
    	
    	
        if (Boolean.getBoolean(INHIBIT_SCHEDULER_PROPERTY)) {
        	
            logger.warn("Disabling TimerScheduler due to system property");
            
            return;
        }

        final Method method = target.getClass().getMethod(methodName,new Class[0]);
        
        //DO enhance this by putting a configurable absolute timestamp to start
        
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.HOUR_OF_DAY, starttimeInHours); //24 hr clock
        
        calendar.set(Calendar.MINUTE, 0);
        
        logger.info("init timer" + starttimeInHours);
        
        calendar.set(Calendar.SECOND, 0);
        
        calendar.set(Calendar.MILLISECOND, 0);

        // if the starttime < 0, use the current time to start 
        
        long starttime = 0;
        
        if (starttimeInHours < 0) {
        	
            starttime = System.currentTimeMillis() + interval;
            
        } else {

            // if the current time is > (past) starttime, start one day later instead of catching up
            
        	if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
        		
                calendar.roll(Calendar.DAY_OF_YEAR, 1);
            }
        	
            starttime = calendar.getTimeInMillis();
        }

        logger.info("TimerScheduler will start: " + methodName + " on " + new Date(starttime));

        timer = new Timer();
        
        timer.scheduleAtFixedRate(new TimerTask() {
        	
            public void run() {
            	
                try {
                    if (logger.isInfoEnabled()) {
                    	
                        logger.info("start timer for method " + methodName);
                    }
                    
                    method.invoke(target, new Object[0]);
                    
                } catch (Exception exc) {
                	
                    logger.error("failure invoking method " + methodName
                        + " on " + target, exc);
                }
            }
        }, new Date(starttime), interval);
    }

    /**
     * Destroy method. Needs to be wired in Spring.
     */
    public void destroy() {
    	
        if (!Boolean.getBoolean(INHIBIT_SCHEDULER_PROPERTY)) {
        	
            timer.cancel();
        }
    }

    /**
     * Set the interval in which uploading to CRS should occur. This interval
     * is specified in minutes.
     * 
     * @param interval The upload interval, in minutes
     */
    public void setInterval(long interval) {
    	
        this.interval = interval * 1000 * 60;
    }

    /**
     * Set the method name of the method to invoke for the
     * timer.
     * 
     * @param methodName The method name
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * Set the target on which the method must be invoked.
     * 
     * @param target The target object
     */
    public void setTarget(Object target) {
        this.target = target;
    }

    public void setStarttimeInHours(int starttimeInHours) {
        this.starttimeInHours = starttimeInHours;
    }
    
    public void initTimer() throws NoSuchMethodException {
        /*if (Boolean.getBoolean(INHIBIT_SCHEDULER_PROPERTY)) {
            logger.warn("Disabling TimerScheduler due to system property");
            return;
        }*/

        final Method method = target.getClass().getMethod(methodName,
            new Class[0]);
        //DO enhance this by putting a configurable absolute timestamp to start
        GregorianCalendar calendar = new GregorianCalendar();
        long starttime = 0;
        interval=getInterval(interval);
        
        Date scheduleDate=getScheduleDateFromDelStr(date);
        if(scheduleDate!=null){
            calendar.setTime(scheduleDate);
            if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
                calendar.roll(Calendar.DAY_OF_YEAR, 1);
            }
            starttime = calendar.getTimeInMillis();
         }
         else{
        calendar.set(Calendar.HOUR_OF_DAY, starttimeInHours); //24 hr clock
        calendar.set(Calendar.MINUTE, 0);
        logger.info("init timer" + starttimeInHours);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        starttime = System.currentTimeMillis() + interval;
         }
        logger.info("TimerScheduler will start: " + methodName + " on "
            + new Date(starttime));
        logger.info("Interval: " +interval);

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                try {
                    if (logger.isInfoEnabled()) {
                        logger.info("start timer for method " + methodName);
                    }
                    logger.info("time");
                    method.invoke(target, new Object[0]);
                } catch (Exception exc) {
                    logger.error("failure invoking method " + methodName
                        + " on " + target, exc);
                }
            }
        }, new Date(starttime), interval);
    }
    
    public long getInterval(long interval) {
        return interval * 1000 * 60;
     }
    
    private Date getScheduleDateFromDelStr(String param){
		if(null == param) return null;
		String[]  value = param.split(",");

		Calendar calendar = Calendar.getInstance();
		int dayOfWeek = new Integer(stripOffSpaces(value[0])).intValue();
		if( -1 != dayOfWeek)
			calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
	    calendar.set(Calendar.AM_PM, (stripOffSpaces(value[1]).equals("AM"))?Calendar.AM:Calendar.PM);
	    calendar.set(Calendar.HOUR_OF_DAY, new Integer(stripOffSpaces(value[2])).intValue());
	    calendar.set(Calendar.MINUTE, new Integer(stripOffSpaces(value[3])).intValue());
	    calendar.set(Calendar.SECOND, new Integer(stripOffSpaces(value[4])).intValue());
	    //p(calendar.getTime()+"");
	    return calendar.getTime();
	}
    
    private String stripOffSpaces(String p){
    	if(null == p) return p;
    	if(p.trim().length() == 0) return null; 
    	
    	return p.trim();
    }


}
