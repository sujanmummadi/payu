package com.cupola.fwmp.filters;

public class TariffFilter
{

	private int startOffset;
	private int pageSize;
	private long cityId;
	private String regxName;
	
	public String getRegxName()
	{
		return regxName;
	}

	public void setRegxName(String regxName)
	{
		this.regxName = regxName;
	}

	public int getStartOffset()
	{
		return startOffset;
	}

	public void setStartOffset(int startOffset)
	{
		this.startOffset = startOffset;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public long getCityId()
	{
		return cityId;
	}

	public void setCityId(long cityId)
	{
		this.cityId = cityId;
	}

	@Override
	public String toString()
	{
		return "TariffFilter [startOffset=" + startOffset + ", pageSize="
				+ pageSize + ", cityId=" + cityId + "]";
	}

}
