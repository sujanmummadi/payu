package com.cupola.fwmp.filters;

public class UserFilter
{
	private String firstName;
	
	private long reportToUserId;
	private int startOffset;
	private int pageSize;
	
	private long userGroupId;
	
	private long areaId;
	private long branchId;
	private long cityId;
	private long subAreaId;
	private String loginId;
	private String employeeId;
	
	/**
	 * @return the employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}


	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}


	public String getLoginId()
	{
		return loginId;
	}


	public void setLoginId(String loginId)
	{
		this.loginId = loginId;
	}


	public long getReportToUserId()
	{
		return reportToUserId;
	}


	public void setReportToUserId(long reportToUserId)
	{
		this.reportToUserId = reportToUserId;
	}


	public int getStartOffset()
	{
		return startOffset;
	}


	public void setStartOffset(int startOffset)
	{
		this.startOffset = startOffset;
	}


	public int getPageSize()
	{
		return pageSize;
	}


	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}


	public long getUserGroupId()
	{
		return userGroupId;
	}


	public void setUserGroupId(long userGroupId)
	{
		this.userGroupId = userGroupId;
	}


	public long getAreaId()
	{
		return areaId;
	}


	public void setAreaId(long areaId)
	{
		this.areaId = areaId;
	}


	public long getBranchId()
	{
		return branchId;
	}


	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}


	public long getCityId()
	{
		return cityId;
	}


	public void setCityId(long cityId)
	{
		this.cityId = cityId;
	}


	public long getSubAreaId()
	{
		return subAreaId;
	}


	public void setSubAreaId(long subAreaId)
	{
		this.subAreaId = subAreaId;
	}


	@Override
	public String toString()
	{
		return "UserFilter [reportToUserId=" + reportToUserId
				+ ", startOffset=" + startOffset + ", pageSize=" + pageSize
				+ ", userGroupId=" + userGroupId + ", areaId=" + areaId
				+ ", branchId=" + branchId + ", cityId=" + cityId
				+ ", subAreaId=" + subAreaId + "]";
	}


	public String getFirstName()
	{
		return firstName;
	}


	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
}
