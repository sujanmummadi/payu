package com.cupola.fwmp.filters;

import java.util.Date;

public class VendorFilter {
	
    private Long areaId;
	private Long cityId;
	private Long branchId;
	private Date fromDate;
	private Date toDate;
	private Long userId;
	private int startOffset;
	private int pageSize;
	
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getAreaId() {
		return areaId;
	}
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
	public int getStartOffset()
	{
		return startOffset;
	}
	public void setStartOffset(int startOffset)
	{
		this.startOffset = startOffset;
	}
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	@Override
	public String toString()
	{
		return "VendorFilter [areaId=" + areaId + ", cityId=" + cityId
				+ ", branchId=" + branchId + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + ", userId=" + userId
				+ ", startOffset=" + startOffset + ", pageSize=" + pageSize
				+ "]";
	}
	
	
	
	
	
}
