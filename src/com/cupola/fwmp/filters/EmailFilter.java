package com.cupola.fwmp.filters;

import java.util.Date;

public class EmailFilter {
	private long ticketId;
	private long cityId;
	private int eventId;
	private int emailType;
	private int status;
	private int offset;
	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public int getEmailType() {
		return emailType;
	}
	public void setEmailType(int emailType) {
		this.emailType = emailType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	@Override
	public String toString() {
		return "EmailFilter [ticketId=" + ticketId + ", cityId=" + cityId
				+ ", eventId=" + eventId + ", emailType=" + emailType
				+ ", status=" + status + ", offset=" + offset + "]";
	}
	

}
