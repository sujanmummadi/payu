package com.cupola.fwmp.filters;

import java.util.List;

public class TabFilter
{
	private int startOffset;
	private int pageSize;
	private List assignedUserIds;
	private List currentUserIds;
	private String macAddress;

	public int getStartOffset()
	{
		return startOffset;
	}

	public void setStartOffset(int startOffset)
	{
		this.startOffset = startOffset;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public List getAssignedUserIds()
	{
		return assignedUserIds;
	}

	public void setAssignedUserIds(List assignedUserIds)
	{
		this.assignedUserIds = assignedUserIds;
	}

	public List getCurrentUserIds()
	{
		return currentUserIds;
	}

	public void setCurrentUserIds(List currentUserIds)
	{
		this.currentUserIds = currentUserIds;
	}

	public String getMacAddress()
	{
		return macAddress;
	}

	public void setMacAddress(String macAddress)
	{
		this.macAddress = macAddress;
	}

	@Override
	public String toString()
	{
		return "TabFilter [startOffset=" + startOffset + ", pageSize="
				+ pageSize + ", assignedUserIds=" + assignedUserIds
				+ ", currentUserIds=" + currentUserIds + ", macAddress="
				+ macAddress + "]";
	}

}
