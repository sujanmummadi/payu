package com.cupola.fwmp.filters;

import java.util.Date;
import java.util.List;


public class TicketFilter implements ViewFilter{

	private Long customerId;
	private Long customerContactNumber;
	private Long ticketId;
	private List<Long> ticketIds;
	private String workorderNumber;
	private boolean includeCompleteStatus;
	private String regxOnCustomerName;
	
	private Long loggedInUserId; //logged-in
	private Long loggedInUserName; //logged-in
	
	private Long assignedUserId; // assignedto
	private Long assignedUserName;// assignedto
	private Long assignedByUserId; // assignedBy
	
	private Boolean includeDescendents;
	
	private List<Long> assignedUserIds; 
	private Long areaId;
	private List<Long> areaIds;
	private Long branchId;
	private Long cityId;
	private Long prospectType;
	private Date toDate;
	private Date fromDate;
	
	private Integer ticketStatus;
	private long initialWorkStage;
	private List<Integer> ticketStatusList;
	
	private Integer ticketType;
	
	private Integer workStageType; //1- fiber 2- copper
	
	private Boolean groupByEngineers;

	private int startOffset;
	private int pageSize;
	private Integer pageContextId;
	
	private Long ticketSubCategoryId;
	
	
	private Long priorityFilterValue;
	
	private String mqId;
	
	/* ******************* FR TICKET FIELD WITH SETTER GETTER ************************/
	private String ticketeCategory;
	private String subCategory;
	private Integer symptomCode;
	private Integer deviceId;
	private String natureCode;
	private Long flowId;
	private Long status;
	private String deviceName;
	
	private String requestKey;
	
	public String getRequestKey()
	{
		return requestKey;
	}

	public void setRequestKey(String requestKey)
	{
		this.requestKey = requestKey;
	}

	public Long getStatus()
	{
		return status;
	}

	public void setStatus(Long status)
	{
		this.status = status;
	}

	public Long getFlowId()
	{
		return flowId;
	}

	public void setFlowId(Long flowId)
	{
		this.flowId = flowId;
	}

	public String getTicketeCategory()
	{
		return ticketeCategory;
	}

	public void setTicketeCategory(String ticketeCateogry)
	{
		this.ticketeCategory = ticketeCateogry;
	}

	public String getSubCategory()
	{
		return subCategory;
	}

	public void setSubCategory(String subCategory)
	{
		this.subCategory = subCategory;
	}

	public Integer getSymptomCode()
	{
		return symptomCode;
	}

	public void setSymptomCode(Integer symptomCode)
	{
		this.symptomCode = symptomCode;
	}

	public Integer getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(Integer deviceId)
	{
		this.deviceId = deviceId;
	}

	public String getNatureCode() {
		return natureCode;
	}

	public void setNatureCode(String natureCode) {
		this.natureCode = natureCode;
	}
	
	/* ************************ END OF FR TICKET FIELD ***********************/

	public Long getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(Long customerId)
	{
		this.customerId = customerId;
	}

	public Long getCustomerContactNumber()
	{
		return customerContactNumber;
	}

	public void setCustomerContactNumber(Long customerContactNumber)
	{
		this.customerContactNumber = customerContactNumber;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public String getWorkorderNumber()
	{
		return workorderNumber;
	}

	public void setWorkorderNumber(String workorderNumber)
	{
		this.workorderNumber = workorderNumber;
	}

	public String getRegxOnCustomerName()
	{
		return regxOnCustomerName;
	}

	public void setRegxOnCustomerName(String regxOnCustomerName)
	{
		this.regxOnCustomerName = regxOnCustomerName;
	}

	public Long getLoggedInUserId()
	{
		return loggedInUserId;
	}

	public void setLoggedInUserId(Long loggedInUserId)
	{
		this.loggedInUserId = loggedInUserId;
	}

	public Long getLoggedInUserName()
	{
		return loggedInUserName;
	}

	public void setLoggedInUserName(Long loggedInUserName)
	{
		this.loggedInUserName = loggedInUserName;
	}

	public Long getAssignedUserId()
	{
		return assignedUserId;
	}

	public void setAssignedUserId(Long assignedUserId)
	{
		this.assignedUserId = assignedUserId;
	}

	public Long getAssignedUserName()
	{
		return assignedUserName;
	}

	public void setAssignedUserName(Long assignedUserName)
	{
		this.assignedUserName = assignedUserName;
	}

	public Boolean getIncludeDescendents()
	{
		return includeDescendents;
	}

	public void setIncludeDescendents(Boolean includeDescendents)
	{
		this.includeDescendents = includeDescendents;
	}

	public Long getAreaId()
	{
		return areaId;
	}

	public void setAreaId(Long areaId)
	{
		this.areaId = areaId;
	}

	public Long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(Long branchId)
	{
		this.branchId = branchId;
	}

	public Long getCityId()
	{
		return cityId;
	}

	public void setCityId(Long cityId)
	{
		this.cityId = cityId;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}

	public Date getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate)
	{
		this.fromDate = fromDate;
	}

	public Integer getTicketStatus()
	{
		return ticketStatus;
	}

	public void setTicketStatus(Integer status)
	{
		this.ticketStatus = status;
	}

	public Integer getTicketType()
	{
		return ticketType;
	}

	public void setTicketType(Integer ticketType)
	{
		this.ticketType = ticketType;
	}

	public Boolean getGroupByEngineers()
	{
		return groupByEngineers;
	}

	public void setGroupByEngineers(Boolean groupByEngineers)
	{
		this.groupByEngineers = groupByEngineers;
	}

	public int getStartOffset()
	{
		return startOffset;
	}

	public void setStartOffset(int startOffset)
	{
		this.startOffset = startOffset;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public Long getAssignedByUserId()
	{
		return assignedByUserId;
	}

	public void setAssignedByUserId(Long assignedByUserId)
	{
		this.assignedByUserId = assignedByUserId;
	}

	public Integer getWorkStageType()
	{
		return workStageType;
	}

	public void setWorkStageType(Integer workStageType)
	{
		this.workStageType = workStageType;
	}

	public long getInitialWorkStage()
	{
		return initialWorkStage;
	}

	public void setInitialWorkStage(long initialWorkStage)
	{
		this.initialWorkStage = initialWorkStage;
	}

	public boolean isIncludeCompleteStatus()
	{
		return includeCompleteStatus;
	}

	public void setIncludeCompleteStatus(boolean includeCompleteStatus)
	{
		this.includeCompleteStatus = includeCompleteStatus;
	}

	public List<Long> getTicketIds()
	{
		return ticketIds;
	}

	public void setTicketIds(List<Long> ticketIds)
	{
		this.ticketIds = ticketIds;
	}

	public List<Integer> getTicketStatusList()
	{
		return ticketStatusList;
	}

	public void setTicketStatusList(List<Integer> ticketStatusList)
	{
		this.ticketStatusList = ticketStatusList;
	}

	public Integer getPageContextId()
	{
		return pageContextId;
	}

	public void setPageContextId(Integer pageContextId)
	{
		this.pageContextId = pageContextId;
	}

	public Long getTicketSubCategoryId()
	{
		return ticketSubCategoryId;
	}

	public void setTicketSubCategoryId(Long ticketSubCategoryId)
	{
		this.ticketSubCategoryId = ticketSubCategoryId;
	}

	public Long getPriorityFilterValue()
	{
		return priorityFilterValue;
	}

	public void setPriorityFilterValue(Long priorityFilterValue)
	{
		this.priorityFilterValue = priorityFilterValue;
	}

	public Long getProspectType()
	{
		return prospectType;
	}

	public void setProspectType(Long prospectType)
	{
		this.prospectType = prospectType;
	}

	public String getMqId()
	{
		return mqId;
	}

	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}

	public List<Long> getAssignedUserIds()
	{
		return assignedUserIds;
	}

	public void setAssignedUserIds(List<Long> assignedUserIds)
	{
		this.assignedUserIds = assignedUserIds;
	}

	public  List<Long> getAreaIds()
	{
		return areaIds;
	}

	public  void setAreaIds(List<Long> areaIds)
	{
		this.areaIds = areaIds;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TicketFilter [customerId=" + customerId + ", customerContactNumber=" + customerContactNumber
				+ ", ticketId=" + ticketId + ", ticketIds=" + ticketIds + ", workorderNumber=" + workorderNumber
				+ ", includeCompleteStatus=" + includeCompleteStatus + ", regxOnCustomerName=" + regxOnCustomerName
				+ ", loggedInUserId=" + loggedInUserId + ", loggedInUserName=" + loggedInUserName + ", assignedUserId="
				+ assignedUserId + ", assignedUserName=" + assignedUserName + ", assignedByUserId=" + assignedByUserId
				+ ", includeDescendents=" + includeDescendents + ", assignedUserIds=" + assignedUserIds + ", areaId="
				+ areaId + ", areaIds=" + areaIds + ", branchId=" + branchId + ", cityId=" + cityId + ", prospectType="
				+ prospectType + ", toDate=" + toDate + ", fromDate=" + fromDate + ", ticketStatus=" + ticketStatus
				+ ", initialWorkStage=" + initialWorkStage + ", ticketStatusList=" + ticketStatusList + ", ticketType="
				+ ticketType + ", workStageType=" + workStageType + ", groupByEngineers=" + groupByEngineers
				+ ", startOffset=" + startOffset + ", pageSize=" + pageSize + ", pageContextId=" + pageContextId
				+ ", ticketSubCategoryId=" + ticketSubCategoryId + ", priorityFilterValue=" + priorityFilterValue
				+ ", mqId=" + mqId + ", ticketeCategory=" + ticketeCategory + ", subCategory=" + subCategory
				+ ", symptomCode=" + symptomCode + ", deviceId=" + deviceId + ", natureCode=" + natureCode + ", flowId="
				+ flowId + ", status=" + status + ", deviceName=" + deviceName + ", requestKey=" + requestKey + "]";
	}
}
