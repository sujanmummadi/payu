/**
 * 
 */
package com.cupola.fwmp.response;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author aditya
 * 
 */
@JsonInclude(Include.NON_NULL)
public class APIResponse {

	private int statusCode;
	private String statusMessage;
	private String transactionId;
	
	private Set<Long> queueData;
	private Object data;
	
	public String getTransactionId()
	{
		return transactionId;
	}

	public Set<Long> getQueueData()
	{
		return queueData;
	}

	public APIResponse setQueueData(Set<Long> queueData)
	{
		this.queueData = queueData;
		return this;
	}

	public APIResponse setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
		return this;
	}

	

	public APIResponse() {
		super();
	}

	public APIResponse(int statusCode, String statusMessage) {
		super();
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
	}

	public APIResponse(int statusCode, String statusMessage, Object data) {
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.data = data;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Object getData() {
		return data;
	}

	public APIResponse setData(Object data) {
		this.data = data;
		return this;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "APIResponse [statusCode=" + statusCode + ", statusMessage=" + statusMessage + ", transactionId="
				+ transactionId + ", data=" + data + "]";
	}

}
