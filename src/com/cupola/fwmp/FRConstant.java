package com.cupola.fwmp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

public interface FRConstant
{
	public class FRStatus
	{
		public static final int FLOW_APPROVAL_PENDING = 1001;
		public static final int USER_ON_FIELD = 663;
		
		public static final long SR_ELEMENT = 505;
		public static final long CX_DOWN = 501;
		public static final long CX_UP = 502;
		public static final long FREQUENCE_DISCONNECTION = 503;
		public static final long SLOW_SPEED = 504;
		public static final long PRIORITY = 581;
		public static final long REOPEN = 582;
		
		public static final long ETR_ELAPSED = 580;
		public static final long ETR_ELAPSED_DONE = 579;
		
		public static final long COMPLETED = 101;
		public static final long TOTAL = 500;

		public static final long SPLICER_PENDING = 549;
		public static final long SPLICER_PENDING_DONE = 548;
		
		public static final long SPLICING_PENDING = 550;
		public static final long SPLICING_PENDING_DONE = 547;
		
		public static final long ELECTRICIAN_PENDING = 551;
		public static final long ELECTRICIAN_PENDING_DONE = 546;
		
		public static final long ODTR_PENDING = 552;
		public static final long ODTR_PENDING_DONE = 545;
		
		public static final long LOCKED = 582;
		
		public static final long FIBER_LAYER_PENDING = 590;
		public static final long FIBER_LAYER_PENDING_DONE = 591;
		public static final long COPPER_LAYER_PENDING = 570l;
		public static final long COPPER_LAYER_PENDING_DONE = 571l;
		
		public static final long CCNR_APPROVAL_PENDING = 601;
		public static final long CCNR_APPROVAL_DONE = 602;
		public static final long SHIFTING_PERMISSION_PENDING =604l;
		public static final long SHIFTING_PERMISSION_DONE_AGREEMENT_PENDING=605l;
		public static final long CCNR_APPROVAL_REJECTED = 603;
		
		public static final long LEAGAL_TEAM_NOTIFIED = 614; //statuslist
		public static final long SHIFTING_AGREEMENT_PENDING = 625; //statuslist
		
		public static final long FINANCE_TEAM_APPROVAL_PENDING = 626;
		public static final long FINANCE_TEAM_APPROVAL_DONE = 627;
		
		public static final long CHURN_TEAM_ACTION_PENDING  = 636; //statuslist
		public static final long CHURN_TEAM_ACTION_DONE  = 637; 
		public static final long CUSTOMER_CHURN  = 638; 
		
		public static final long NOC_TEAM_ACTION_PENDING = 650;
		public static final long NOC_TEAM_ACTION_DONE = 651;
		
		public static final Integer FR_UNASSIGNEDT_TICKET = 900;
		public static final Integer FR_WORKORDER_GENERATED = 901;
		public static final Integer FR_WORKORDER_REOPEN = 902;
		public static final Integer FR_PUSH_BACK_TICKET = 903;
		public static final Integer FLOW_CHANGED_DOWN_TO_UP = 904;
		
		public static final Integer MODEFIED = 905;
		public static final Integer NOT_MODEFIED = 906;
		
		public static final Integer SUCCESSFUL_CLOSED_IN_MQ = 103;
		
		public static Map<Long, String> definition = new HashMap<Long, String>();
		
		public static List<Integer> NOT_IN_PROGRESS_DEF = Arrays.asList(103, 101,102,254,104,900,901,902) ;
		public static List<Integer> NEW_TICKET_DEF = Arrays.asList(102,901,902) ;

		static
		{
			definition.put(CX_DOWN, "CX_DOWN");
			definition.put(CX_UP, "CX_UP");
			definition.put(FREQUENCE_DISCONNECTION, "FREQUENCE_DISCONNECTION");
			definition.put(SLOW_SPEED, "SLOW_SPEED");
			definition.put(PRIORITY, "PRIORITY");
			definition.put(ETR_ELAPSED, "ETR_ELAPSED");
			definition.put(COMPLETED, "COMPLETED");
			definition.put(TOTAL, "TOTAL");
			definition.put(SPLICER_PENDING, "SPLICER_PENDING");
			definition.put(SPLICING_PENDING, "SPLICING_PENDING");
			definition.put(ELECTRICIAN_PENDING, "ELECTRICIAN_PENDING");
			definition.put(ODTR_PENDING, "ODTR_PENDING");
			definition.put(LOCKED, "LOCKED");
		}


		public static List<Long> CX_UP_STATUS_LIST = Arrays.asList(new Long[] {

		CX_UP, FREQUENCE_DISCONNECTION, SLOW_SPEED, PRIORITY, ETR_ELAPSED,
				COMPLETED, TOTAL,  LOCKED });
		public static List<Long> CX_DOWN_STATUS_LIST = Arrays
				.asList(new Long[] {

				CX_DOWN, SPLICER_PENDING, PRIORITY, ETR_ELAPSED, COMPLETED,
						TOTAL,   LOCKED, SPLICING_PENDING,
						ELECTRICIAN_PENDING, ODTR_PENDING });
	}
	
	public class Assets
	{
		public static final long SPICING_MACHINE = 1;
		public static final long OTDR = 2;
		public static final long LASER_LIGHT = 3;
		
	}
	
	public class FrFlowStatus
	{
		public static long CX_UP = 2;
		public static long CX_DOWN = 1;
		public static long SENIOR_ELEMENT = 5;
		
		public static List<Long> FLOW_UP_GROUP = Arrays.asList(2l,0l);
	}
	
	public class MQRequestType
	{
		public static long STATUS_OR_COMMENT_MODIFY = 1;
		public static long ETR_MODIFY = 2;
		public static long TICKET_CLOSE = 3;
	}
	
	public interface FlowActionType
	{
		Long SUBMIT_BOR = 16l;
		Long REQUEST_FOR_DISCOUNT = 18l;
		Long RAISE_FOR_ELECTRICAL_WORK =  17l;
		Long RAISE_FOR_FIBER_LAYING =  30l;
		Long TRIGGER_CCNR_TEAM =  26l;
		Long RAISE_FOR_SPLICER =  28l;
		
		Long RAISE_FOR_CHUM =  27l;
		Long REBOOT_BY_NOC =  35l;
		Long RAISE_FOR_COPPER_LAYING =  36l;
		List<Long> BLOCKED_FLOW = Arrays.asList(17l,30l,28l,1l,36l);
		
		List<Long> NOTIFICATION_ACTION_TYPE = Arrays.asList(16l,42l);
		
		Long TRIGGER_BILLING_TEAM_TL = 19l;
		Long TRIGGER_MAIL_FOR_NOC = 42l;
		
	}
	public interface FlowAttribute
	{
		
		String CX_IP = "CX_IP";
		String CX_MAC = "CX_MAC";
		String CX_PORT = "CX_PORT";
		
		//discount data
		String DISCOUNT_AMOUNT = "DISCOUNTED_AMOUNT";
		String DISCOUNT_VALID_FROM = "DISCOUNT_VALID_FROM";
		String DISCOUNT_VALID_TILL = "DISCOUNT_VALID_TILL";
		
		String SYMTEM_ASSIGNED = "System";
	}
	public interface SkillType
	{	
		int SPLICER	= 2;
		int FIBER_LAYER	= 3;
		int ELECTRICIAN = 	5;
		int COPPER_LAYER = 	4;
	}
	
	public class ActionTypeAttributeDefinitions
	{
		public static	Map<Long, List<String>> DEFINITION = new HashMap<Long, List<String>>();
		
		public static	Map<Long, Long> ACTION_STATUS_DEFINITION = new HashMap<Long, Long>();
		public static	Map<Long, Integer> ACTION_SKILL_DEFINITION = new HashMap<Long, Integer>();
		public static	Map<Integer, String>SKILL_DEFINITION = new HashMap<Integer, String>();
		
		static
		{
			
			DEFINITION.put(FlowActionType.REQUEST_FOR_DISCOUNT, Arrays.asList(new String[]{
					FlowAttribute.DISCOUNT_AMOUNT,
					FlowAttribute.DISCOUNT_VALID_FROM,
					FlowAttribute.DISCOUNT_VALID_TILL
			}));
		
			ACTION_STATUS_DEFINITION.put(FlowActionType.TRIGGER_CCNR_TEAM, FRStatus.CCNR_APPROVAL_PENDING);
			ACTION_STATUS_DEFINITION.put(FlowActionType.REQUEST_FOR_DISCOUNT, FRStatus.FINANCE_TEAM_APPROVAL_PENDING);
			ACTION_STATUS_DEFINITION.put(FlowActionType.REBOOT_BY_NOC, FRStatus.NOC_TEAM_ACTION_PENDING);
			ACTION_STATUS_DEFINITION.put(FlowActionType.RAISE_FOR_CHUM, FRStatus.CHURN_TEAM_ACTION_PENDING);
			
			ACTION_STATUS_DEFINITION.put(FlowActionType.RAISE_FOR_ELECTRICAL_WORK, FRStatus.ELECTRICIAN_PENDING);
			ACTION_STATUS_DEFINITION.put(FlowActionType.RAISE_FOR_SPLICER, FRStatus.SPLICER_PENDING);
			ACTION_STATUS_DEFINITION.put(FlowActionType.RAISE_FOR_FIBER_LAYING, FRStatus.FIBER_LAYER_PENDING);
			ACTION_STATUS_DEFINITION.put(FlowActionType.RAISE_FOR_COPPER_LAYING, FRStatus.COPPER_LAYER_PENDING);
			
			ACTION_SKILL_DEFINITION.put(FlowActionType.RAISE_FOR_ELECTRICAL_WORK, SkillType.ELECTRICIAN);
			ACTION_SKILL_DEFINITION.put(FlowActionType.RAISE_FOR_SPLICER, SkillType.SPLICER);
			ACTION_SKILL_DEFINITION.put(FlowActionType.RAISE_FOR_FIBER_LAYING,SkillType.FIBER_LAYER);
			ACTION_SKILL_DEFINITION.put(FlowActionType.RAISE_FOR_COPPER_LAYING, SkillType.COPPER_LAYER);
			
			SKILL_DEFINITION.put(SkillType.ELECTRICIAN, "Electrician");
			SKILL_DEFINITION.put(SkillType.SPLICER, "Splicing");
			SKILL_DEFINITION.put(SkillType.FIBER_LAYER, "FiberLaying");
			SKILL_DEFINITION.put(SkillType.COPPER_LAYER, "CopperLaying");
		 
		 
		}
		
		public static final List<Long> FR_ACTION_PENDING_STATUS_LIST  = Arrays.asList(FRStatus.CCNR_APPROVAL_PENDING
				,FRStatus.FINANCE_TEAM_APPROVAL_PENDING,FRStatus.NOC_TEAM_ACTION_PENDING,FRStatus.CHURN_TEAM_ACTION_PENDING
				,FRStatus.ELECTRICIAN_PENDING,FRStatus.SPLICER_PENDING,FRStatus.FIBER_LAYER_PENDING,FRStatus.COPPER_LAYER_PENDING
				,FRStatus.ODTR_PENDING,FRStatus.ETR_ELAPSED,FRStatus.SPLICING_PENDING,FRStatus.SHIFTING_PERMISSION_PENDING);
		
		
		public static	Map<Long, Long> FR_ACTION_DONE_STATUS_LIST = new HashMap<Long, Long>();
		public static	Map<Long, Long> FR_ACTION_PENDING_STATUS_MAP = new HashMap<Long, Long>();
		
		static
		{
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.CCNR_APPROVAL_DONE, FRStatus.CCNR_APPROVAL_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.FINANCE_TEAM_APPROVAL_DONE, FRStatus.FINANCE_TEAM_APPROVAL_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.NOC_TEAM_ACTION_DONE, FRStatus.NOC_TEAM_ACTION_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.CHURN_TEAM_ACTION_DONE, FRStatus.CHURN_TEAM_ACTION_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.ELECTRICIAN_PENDING_DONE, FRStatus.ELECTRICIAN_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.SPLICER_PENDING_DONE, FRStatus.SPLICER_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.FIBER_LAYER_PENDING_DONE, FRStatus.FIBER_LAYER_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.COPPER_LAYER_PENDING_DONE, FRStatus.COPPER_LAYER_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.ODTR_PENDING_DONE, FRStatus.ODTR_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.ETR_ELAPSED_DONE, FRStatus.ETR_ELAPSED);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.SPLICING_PENDING_DONE, FRStatus.SPLICING_PENDING);
			
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.SHIFTING_PERMISSION_DONE_AGREEMENT_PENDING, FRStatus.SHIFTING_PERMISSION_PENDING);
			FR_ACTION_DONE_STATUS_LIST.put(FRStatus.CCNR_APPROVAL_REJECTED, FRStatus.CCNR_APPROVAL_PENDING);


			FR_ACTION_PENDING_STATUS_MAP.put(FRStatus.ELECTRICIAN_PENDING, FRStatus.ELECTRICIAN_PENDING_DONE);
			FR_ACTION_PENDING_STATUS_MAP.put(FRStatus.SPLICER_PENDING, FRStatus.SPLICER_PENDING_DONE);
			FR_ACTION_PENDING_STATUS_MAP.put(FRStatus.FIBER_LAYER_PENDING, FRStatus.FIBER_LAYER_PENDING_DONE);
			FR_ACTION_PENDING_STATUS_MAP.put(FRStatus.COPPER_LAYER_PENDING, FRStatus.COPPER_LAYER_PENDING_DONE);
			FR_ACTION_PENDING_STATUS_MAP.put(FRStatus.ODTR_PENDING, FRStatus.ODTR_PENDING_DONE);
			FR_ACTION_PENDING_STATUS_MAP.put(FRStatus.SPLICING_PENDING, FRStatus.SPLICING_PENDING_DONE);
		}
		
		public static List<Long> VENDOR_TASK_DONE_STATUS = Arrays.asList(new Long[ ]{FRStatus.ELECTRICIAN_PENDING_DONE,
				FRStatus.SPLICER_PENDING_DONE,FRStatus.FIBER_LAYER_PENDING_DONE,FRStatus.COPPER_LAYER_PENDING_DONE,
				FRStatus.ODTR_PENDING_DONE,FRStatus.SPLICING_PENDING_DONE});
		
		public static final List<Long> FR_SUPPORTS_STATUS_LIST  = Arrays.asList(FRStatus.CCNR_APPROVAL_PENDING
				,FRStatus.FINANCE_TEAM_APPROVAL_PENDING,FRStatus.NOC_TEAM_ACTION_PENDING,
				FRStatus.CHURN_TEAM_ACTION_PENDING,FRStatus.ELECTRICIAN_PENDING,FRStatus.SPLICER_PENDING,
				FRStatus.FIBER_LAYER_PENDING,FRStatus.COPPER_LAYER_PENDING,FRStatus.SPLICING_PENDING);
		
		public static final List<Long> FR_VENDOR_PENDING_STATUS = Arrays.asList(FRStatus.SPLICING_PENDING,
				FRStatus.ODTR_PENDING,FRStatus.COPPER_LAYER_PENDING,FRStatus.FIBER_LAYER_PENDING,FRStatus.SPLICER_PENDING,
				FRStatus.ELECTRICIAN_PENDING);
	}
	
	public interface FrReportConstant
	{
		public static final String CX_UP_KEY = "CX_UP";
		public static final String CX_DOWN_KEY = "CX_DOWN";
		public static final String UNASSIGNED_KEY = "Unassigned";
		public static final String ALLOCATION_REPORT = "Allocation";
		public static final String PRODUCTIVITY_REPORT = "Productivity";
		
		public static final String ALLOCATION_TYPE_AM = "AM";
		public static final String ALLOCATION_TYPE_TL = "TL";
		
		public static final String ALLOCATION_TYPE_AM_UN = "AM_UN";
		public static final String ALLOCATION_TYPE_TL_UN = "TL_UN";
		
		
		public static final String DAY_START_INFO = "Day_Start_Info";
		public static final String ETR_REPORT = "Etr";
		
		
		public static final List<Integer> PENDING_DEF = Arrays.asList(601,626,650,636, 551,549,590,552,550);
		
	}
	
	public interface FrTicketEtrStatus
	{
		public static final int ETR_APPROVAL_PENDING = 253;
		public static final int ETR_APPROVED = 251;
		public static final int ETR_REJECTED = 252;
		public static final int ETR_UPDATE_AUTO_MODE = 300;
		public static final int ETR_UPDATE_MANUAL_MODE = 301;
		public static final int ETR_NEW = 222;
		public static final int ETR_MODIFIED_LIMIT = 2;
	}
	
	public interface UserType{
		public static final int VENDOR = 1;
		public static final List<Long> VENDORS = Arrays.asList( FlowActionType.RAISE_FOR_SPLICER,
				FlowActionType.RAISE_FOR_ELECTRICAL_WORK,FlowActionType.RAISE_FOR_FIBER_LAYING );
		
	}
	
	public class DefectSubDefectCodeMapper
	{
		public static final int CX_ISSUES	= 1;
		public static final int CX_POWER_ISSUES	= 2;
		public static final int SHIFTING =	3 ;
		public static final int FIBER_ISSUE = 4;
		public static final int ROUTER_ISSUES = 5;
		public static final int	CUSTOMER_OR_PC_ISSUES = 6;
		public static final int	PLANNED_OUTAGE	= 7;
		public static final int	SFP_OR_PATCH_CORD_ISSUES = 8;
		public static final int	CAT_5_CABLE_ISSUES = 9;
		public static final int BACKEND_ISSUES	= 10;
		public static final int DEVICE = 11;
		public static final int POWER = 12;
		public static final int PLANNED = 13;
		public static final int EXTERNAL_FACTORS = 14 ;
		
		public static final int DEFECT_CODE = 99;
		
		public static Map<Integer ,String> DEFECT_SUBDEFECT_MAP = new ConcurrentHashMap<Integer, String>();
		
		/*static{
			DEFECT_SUBDEFECT_MAP.put(CX_ISSUES, "CX Issues");
			DEFECT_SUBDEFECT_MAP.put(CX_POWER_ISSUES, "CX Power Issues");
			DEFECT_SUBDEFECT_MAP.put(SHIFTING, "Shifting");
			DEFECT_SUBDEFECT_MAP.put(FIBER_ISSUE, "Fiber Issue");
			DEFECT_SUBDEFECT_MAP.put(ROUTER_ISSUES, "Router Issues");
			DEFECT_SUBDEFECT_MAP.put(CUSTOMER_OR_PC_ISSUES, "Customer or PC Issues");
			DEFECT_SUBDEFECT_MAP.put(PLANNED_OUTAGE, "Planned Outage");
			DEFECT_SUBDEFECT_MAP.put(SFP_OR_PATCH_CORD_ISSUES, "SFP / Patch Cord Issues");
			DEFECT_SUBDEFECT_MAP.put(CAT_5_CABLE_ISSUES, "CAT 5 Cable Issues");
			DEFECT_SUBDEFECT_MAP.put(BACKEND_ISSUES, "Backend Issues");
			DEFECT_SUBDEFECT_MAP.put(DEVICE, "Device");
			DEFECT_SUBDEFECT_MAP.put(POWER, "Power");
			DEFECT_SUBDEFECT_MAP.put(PLANNED, "Planned");
			DEFECT_SUBDEFECT_MAP.put(EXTERNAL_FACTORS, "External Factors");
			DEFECT_SUBDEFECT_MAP.put(DEFECT_CODE, "DEFECT_CODE");
			
		}*/
	}
	
	public class FrEscalation
	{
		static ResourceBundle rb = ResourceBundle
				.getBundle("com.cupola.fwmp.properties.priority");

		public static final String CEO_TYPE = "ceoescalation";
		public static final String NODAL_TYPE = "nodalescalation";
		public static final String QRT_TYPE = "qrtescalation";
		public static final String OTHERS_TYPE = "others";
		public static final String VICINITY_TYPE = "vicinityescalation";

//		public static final long OTHERS = -1; //TL or else
		public static final long CEO = 1;
		public static final long NODAL = 2;
		public static final long QRT = 3;
		public static final long VICINITY = 4;
		public static final long SELF = 5;
		public static Map<Long, String> def = new HashMap<Long, String>();
		static
		{
			def.put(SELF, OTHERS_TYPE);
			def.put(CEO, CEO_TYPE);
			def.put(NODAL, NODAL_TYPE);
			def.put(QRT, QRT_TYPE);
			def.put(VICINITY, VICINITY_TYPE);
//			def.put(OTHERS, OTHERS_TYPE);
		}
		public static Map<Long, Integer> values = new HashMap<Long, Integer>();
		static
		{
			values.put(CEO, Integer.parseInt(rb.getString(CEO_TYPE)));
			values.put(NODAL, Integer.parseInt(rb.getString(NODAL_TYPE)));
			values.put(QRT, Integer.parseInt(rb.getString(QRT_TYPE)));
			values.put(VICINITY, Integer.parseInt(rb.getString(VICINITY_TYPE)));
//			values.put(OTHERS, Integer.parseInt(rb.getString(OTHERS_TYPE)));
			values.put(SELF, Integer.parseInt(rb.getString(OTHERS_TYPE)));
		}

	}
	
	public class DeviceActionType
	{
		public static final long PING_DEVICE_API = 11;
		public static final long PORT_STATUS_API = 22;
		public static final long FIBER_POWER_API = 33 ;
		public static final long FX_PORT_AVAILABILITY = 44;
		public static final long CX_PORT_AVAILABILITY = 55;
		public static final long SPLICING_PORT_WISE_CHECK = 66;
		public static final long SPLICING_FX_CHECK = 77;
	}

	public interface UpdateType
	{
		public static final String NE = "NE";
		public static final String TL = "TL";
	}
	public interface FlowForDefectSubDefect
	{
		String CX_DOWN = "1";
		String CX_UP = "2";
		String FREQUENT_DISCONNECTION_FLOW = "3";
		String SLOW_SPEED_FLOW = "4";
		String SENIOR_ELEMENT_DOWN = "5";
		String OTHERS = "0";
		String UNKNOWN = "6";
	}
	


	String DEFFAULT_UNLOCK_SIZE = "5";
}
