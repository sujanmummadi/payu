package com.cupola.fwmp.handler;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.user.UserService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.vo.LoginPojo;

public class AuthSuccessHandler extends
		SavedRequestAwareAuthenticationSuccessHandler
{
	Logger LOGGER = LogManager.getLogger(AuthSuccessHandler.class);
	@Autowired
	UserService userService;
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	private final ObjectMapper mapper = new ObjectMapper();
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException,
			ServletException {
		HttpSession session = request.getSession();
		if(ApplicationUtils.retrieveHttpRequestInfo().isRequestedFromMobile())
		{
				Enumeration<String> parameterNames = request
						.getParameterNames();

				while (parameterNames.hasMoreElements())
				{

					String paramName = parameterNames.nextElement();

					String[] paramValues = request
							.getParameterValues(paramName);
					for (int i = 0; i < paramValues.length; i++)
					{
						String paramValue = paramValues[i];
						request.getSession()
								.setAttribute(paramName, paramValue);
					}

				}
				LoginPojo loginPojo = new LoginPojo();
				loginPojo.setDeviceId((String)request.getSession(). getAttribute("deviceId"));
				loginPojo.setUsername((String)request.getSession(). getAttribute("username"));
				loginPojo.setPassword((String)request.getSession(). getAttribute("password"));
				APIResponse result = userService.loginForApp(loginPojo);
				  PrintWriter writer = response.getWriter();
			        mapper.writeValue(writer, result);
			        writer.flush();
			        writer.close();
			        return;
		}
		String targetUrl = determineTargetUrl(request, response); 
        redirectStrategy.sendRedirect(request, response, targetUrl);
	}
	
	@Override
	protected String determineTargetUrl(HttpServletRequest request,
			HttpServletResponse response)
	{
		// Get the role of logged in user
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
	
	
		
		org.springframework.security.web.savedrequest.DefaultSavedRequest savedRequest = (org.springframework.security.web.savedrequest.DefaultSavedRequest) request
				.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");
		HttpSession session = request.getSession();
		session.setAttribute("SPRING_SECURITY_SAVED_REQUEST", null);
		String targetUrl = "";

		if (savedRequest != null)
		{
			targetUrl = savedRequest.getRequestURI();
			LOGGER.debug("Testing auto redirect targetUrl inner "+targetUrl);
		}
		

		else
		{
			String role = auth.getAuthorities().toString();
			if (role.contains("ROLE_ADMIN"))
			{
				targetUrl = "/Index.html";
			} else
			{
				targetUrl = "/home.html";
			}
		}
	//	SecurityContextHolderAwareRequestWrapper sc = new SecurityContextHolderAwareRequestWrapper(request, "");
		/*if(sc.isUserInRole("ROLE_ADMIN"))
		{
			targetUrl = "/Index.html#/HelpLine/";
<<<<<<< AuthSuccessHandler.java
		}*/

		//comment this later
		targetUrl = "/Index.html";
		return targetUrl;
	}
	
}
