
package com.cupola.fwmp.handler.custom.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.cupola.fwmp.auth.AppAuthenticationProvider;

/**
 * @author aditya
 * @created 12:58:30 PM Apr 13, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */
public class CustomerUserAuth
{

	Logger log=Logger.getLogger(CustomerUserAuth.class);
	@Autowired
	AppAuthenticationProvider appAuthenticationProvider;

	public void login(HttpServletRequest request, String userName,
			String password)
	{

		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(userName, password);

		// Authenticate the user
		Authentication authentication = appAuthenticationProvider
				.authenticate(authRequest);
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);

		// Create a new session and add the security context.
		HttpSession session = request.getSession(true);
		session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
	}

	public void doAutoLogin(HttpServletRequest request, String username,
			String password)
	{

		try
		{
			// Must be called from request filtered by Spring Security,
			// otherwise SecurityContextHolder is not updated
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
			token.setDetails(new WebAuthenticationDetails(request));
			Authentication authentication = this.appAuthenticationProvider
					.authenticate(token);
			log.info("Logging in with [{}]"
					+ authentication.getPrincipal());
			SecurityContextHolder.getContext()
					.setAuthentication(authentication);
		} catch (Exception e)
		{
			SecurityContextHolder.getContext().setAuthentication(null);
			log.error("Failure in autoLogin" + e);
		}
	}
}