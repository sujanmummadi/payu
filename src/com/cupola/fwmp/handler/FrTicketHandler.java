package com.cupola.fwmp.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;

public class FrTicketHandler
{
	@Autowired
	private DefinitionCoreService definitionService;
	
	@Autowired
	private FrTicketService frTicketService;
	
	@Autowired
	private DBUtil dbUtil;
	
	@Autowired
	private GlobalActivities globalActivities;
	
	@Autowired
	private UserHelper userHelper;
	
	private static Logger LOGGER = Logger.getLogger(FrTicketHandler.class);
	
	public APIResponse getFrTicketDetail(TicketFilter filter)
	{
		if( filter == null )
			return getFrTicketDetails(filter);
			
		this.setCurrentUserStatus( filter );
		
		if ( AuthUtils.isFRNEUser() || AuthUtils.isFRTLUser() 
				|| AuthUtils.isFRCXDownNEUser() || AuthUtils.isFRCxUpNEUser() || AuthUtils.isFRSRNEUser() )
			return this.getFrTlorNeTicketDetails(filter);
		
		else if (AuthUtils.isCCNRUser() || AuthUtils.isLegalTeamUser() ||
				AuthUtils.isFinanceTeamUser() || AuthUtils.isChurnTeamUser() || AuthUtils.isNOCTeamUser())
		{
			filter.setCityId(AuthUtils.getCurrentUserCity().getId());
//			LOGGER.info("******************** CCNR ******************** : "+AuthUtils.getCurrentUserLoginId());
			return getFrTicketDetails(filter);
		}
		else if ( AuthUtils.isFrManager() )
		{
			return this.getManagerTicketDetail(filter);
		}
		else 
			return getFrTicketDetails(filter);
	}
	
	private APIResponse getFrTlorNeTicketDetails(TicketFilter filter)
	{
//		LOGGER.info("******************** TL OR NE ******************** : "+AuthUtils.getCurrentUserLoginId());
		
		if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0)
			return getAllTickets(filter,null);
		
		if( filter.getTicketId() != null && filter.getTicketId() > 0 )
			return getAllTickets(filter,null);
		
		if( filter.getDeviceName() != null && !filter.getDeviceName().isEmpty() )
		{
			long userId = AuthUtils.getCurrentUserId();
			if( filter.getDeviceName().equalsIgnoreCase("All"))
				filter.setDeviceName(null);
			
			Set<Long> ticketIds = getAllTicketsForUsers(Arrays.asList(userId));
			if( ticketIds == null || ticketIds.isEmpty()  )
				return ResponseUtil.createSuccessResponse()	.setData(new ArrayList<>());
			
			filter.setTicketIds( new ArrayList<Long>(ticketIds));
			return getAllTickets(filter,null);
		}
		if( filter.getAssignedUserId() != null  
				&&  filter.getAssignedUserId().longValue() > 0 )
		{
			Set<Long> ticketIds = getAllTicketsForUsers(Arrays.asList(filter.getAssignedUserId()));
			if(ticketIds == null || ticketIds.isEmpty() )
				return ResponseUtil.createSuccessResponse()	.setData(new ArrayList<>());
			filter.setTicketIds( new ArrayList<Long>(ticketIds));
			
			return getAllTickets(filter,null);
		}
		if(filter.getAssignedUserIds() != null
				&& filter.getAssignedUserIds().size() > 0)
		{
				Set<Long> ticketIds = getAllTicketsForUsers(filter.getAssignedUserIds());
				if(ticketIds == null || ticketIds.isEmpty() )
					return ResponseUtil.createSuccessResponse()	.setData(new ArrayList<>());
				
				filter.setTicketIds( new ArrayList<Long>(ticketIds));
				return getAllTickets(filter,null);
		}
		else
		{
			Set<Long> ticketListForWorkingQueue = null;
			Long userId = AuthUtils.getCurrentUserId();
			
			if (filter.getAssignedUserId() != null
					&& filter.getAssignedUserId() > 0)
				userId = filter.getAssignedUserId();
			
			String key = dbUtil.getKeyForQueueByUserId(userId);
			ticketListForWorkingQueue = globalActivities.getAllFrTicketFromMainQueueByKey(key);
			
			if (ticketListForWorkingQueue != null
					&& !ticketListForWorkingQueue.isEmpty())
			{
				LOGGER.info(ticketListForWorkingQueue.size()
						+ " Tickets found for user " + key + " Username : "
						+ AuthUtils.getCurrentUserLoginId());
				
				List<Long> tIds = new ArrayList<>(ticketListForWorkingQueue);
				filter.setTicketIds(tIds);
				
				return getAllTickets(filter, ticketListForWorkingQueue);
			} 
			else
			{
				LOGGER.info("  Tickets not found for user " + key + " Username : "
						+ AuthUtils.getCurrentUserLoginId());
				
				return ResponseUtil.createSuccessResponse()
						.setData(new ArrayList<>());
			}
		}
	}
	
	private APIResponse getManagerTicketDetail(TicketFilter filter)
	{
//		LOGGER.info("******************** MANAGER ******************** : "+AuthUtils.getCurrentUserLoginId());
		
		LOGGER.info(" FR Executives  : " + AuthUtils.getCurrentUserLoginId());

		if (filter.getTicketIds() != null
				&& filter.getTicketIds().size() > 0)
			;
		else
		{
			if (AuthUtils.getIfExecutiveRole() == UserRole.FR_BM)
			{
				filter.setBranchId(AuthUtils.getCurrentUserBranch().getId());
			}
			else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_AM)
			{
				if (AuthUtils.getCurrentUsersAllArea() != null)
					filter.setAreaIds(new ArrayList<>(AuthUtils.getCurrentUsersAllArea().keySet()));
			}
			else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_CM)
			{
				LOGGER.info(" FR Executives  role is : " + AuthUtils.getCurrentUserRole());
				filter.setCityId(AuthUtils.getCurrentUserCity().getId());
			}
		}
		if( filter.getAssignedUserIds() != null
				&& filter.getAssignedUserIds().size() > 0
				&& !(filter.getStatus() != null 
				&& filter.getStatus().equals(900l)))
		{
				Set<Long> ticketIds = getAllTicketsForUsers(filter.getAssignedUserIds());
				if(ticketIds == null || ticketIds.isEmpty() )
					return ResponseUtil.createSuccessResponse()	.setData(new ArrayList<>());
				filter.setTicketIds( new ArrayList<Long>(ticketIds));
		}
		return getAllTickets(filter,null);
	
	}
	
	private Set<Long> getAllTicketsForUsers(List <Long> userIds)
	{
		Set<Long> allTicketIds = new HashSet<Long>();
		Set<Long>  ticketIds = null;
		for(long user : userIds )
		{
			String key = dbUtil.getKeyForQueueByUserId(user);
			ticketIds = globalActivities.getAllFrTicketFromMainQueueByKey(key);
			if(ticketIds != null )
			{
				allTicketIds.addAll(ticketIds);
			}
		}
		return allTicketIds;
	}
	
	private APIResponse getFrTicketDetails(TicketFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(frTicketService.getFrTicketDetails(filter));
	}
	
	private void setCurrentUserStatus(TicketFilter filter)
	{
		if (filter.getTicketStatus() == null || filter.getTicketStatus() <= 0)
		{
			filter.setTicketStatus(-1);
			if(filter.getTicketStatusList() != null && filter.getTicketStatusList().size() > 0)
			{
				
			}
			else if(filter.getTicketIds() == null || filter.getTicketIds().size() == 0)
			{
				if (filter.getPageContextId() != null
						&& filter.getPageContextId() > 0 && filter.getPageContextId().intValue()  != DefinitionCoreService.MQ_FAILURE_RELATED_STATUS)
					filter.setTicketStatusList(new ArrayList<Integer>(definitionService
							.getStatusFilterForCurrentUser(true, filter
									.getPageContextId())));
				else
					filter.setTicketStatusList(new ArrayList<Integer>(definitionService
							.getStatusFilterForCurrentUser(true, null)));
			}
		}
		else
		{
			if( filter.getTicketStatus() != null 
					&& filter.getTicketStatus().equals(FRStatus.FR_UNASSIGNEDT_TICKET)
					&& AuthUtils.isFRTLUser())
			{
				filter.setAssignedUserId(AuthUtils.getCurrentUserId());
				filter.setTicketStatus(null);
			}
			else if( filter.getTicketStatus() != null 
					&& filter.getTicketStatus().equals(FRStatus.FR_WORKORDER_REOPEN) )
			{
				filter.setStatus(902l);
				filter.setTicketStatus(null);
			}
			else if( filter.getTicketStatus() != null 
					&& filter.getTicketStatus().equals(FRStatus.FR_UNASSIGNEDT_TICKET)
					&& AuthUtils.isFrManager())
			{
				filter.setStatus(900l);
				filter.setTicketStatus(null);
				List<Long> ams = new ArrayList<Long>();
				
				if( filter.getAssignedUserIds() != null &&  filter.getAssignedUserIds().size() > 0 )
				{
					filter.setSymptomCode(900);
					LOGGER.info("**************** Selectrd Assigned Ids *************"+filter.getAssignedUserIds());
				}
				else if( AuthUtils.isFrAreaManager() )
				{
					ams = userHelper.getAllTlsByUsers(AuthUtils.getCurrentUserId());
					if( ams != null )
						ams.add(AuthUtils.getCurrentUserId());
					
					filter.setAssignedUserIds(ams);
				}
				else if(  AuthUtils.isFrBranchManager() )
				{
					ams = userHelper.getNestedManagers(AuthUtils.getCurrentUserId());
					if( ams != null )
						ams.add(AuthUtils.getCurrentUserId());
					
					filter.setAssignedUserIds(ams);
				}
				else
				{
					ams = userHelper.getNestedManagers(AuthUtils.getCurrentUserId());
					if( ams != null )
						ams.add(AuthUtils.getCurrentUserId());
					
					filter.setAssignedUserIds(ams);
				}
				
//				filter.setTicketStatusList(new ArrayList<Integer>(definitionService.getUnassignedStatusForSearch(1)));
				
			}
		}
	}
	
	private APIResponse getAllTickets(TicketFilter filter,Set<Long>  ticketListForWorkingQueue)
	{
		APIResponse r =  getFrTicketDetails(filter);
		if(ApplicationUtils.isAppRequest())
			r.setQueueData(ticketListForWorkingQueue);
		return r.setTransactionId(GenericUtil.createOrUpdateRequestKey(filter.getRequestKey()));
	}
}
