package com.cupola.fwmp.handler;

import com.cupola.fwmp.vo.GCMMessage;


public interface GCMListener
{
	public void processMsg(GCMMessage  gCMMessage);
}
