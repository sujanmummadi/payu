package com.cupola.fwmp.handler;

import org.apache.log4j.Logger;

import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.vo.MailMessage;


public class MailListenerImpl implements MailListener
{
	final static Logger logger = Logger.getLogger(MailListenerImpl.class);
	
	public void processMsg(MailMessage  mailMessage){
		
		logger.info("inside processMsg of MailListenerImpl");
	}
}
