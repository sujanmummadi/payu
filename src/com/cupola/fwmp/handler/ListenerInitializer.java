package com.cupola.fwmp.handler;

import org.apache.log4j.Logger;

public class ListenerInitializer
{
	final static Logger logger = Logger.getLogger(ListenerInitializer.class);
	private MessageHandler messageHandler;
	private MailListener mailListener;

	@SuppressWarnings("unused")
	private void init()
	{
		logger.info("ListenerInitializerinit method called..");
		messageHandler.registerMailListener(mailListener);
		logger.info("ListenerInitializerinit init method executed.");
	}

	public ListenerInitializer()
	{

	}

	public ListenerInitializer(MessageHandler messageHandler,MailListener mailListener)
	{
		super();
		this.messageHandler = messageHandler;
		this.mailListener = mailListener;
	}

	public MessageHandler getMessageHandler()
	{
		return messageHandler;
	}

	public void setMessageHandler(MessageHandler messageHandler)
	{
		this.messageHandler = messageHandler;
	}

	public MailListener getMailListener()
	{
		return mailListener;
	}

	public void setMailListener(MailListener mailListener)
	{
		this.mailListener = mailListener;
	}


}
