package com.cupola.fwmp.handler;

import com.cupola.fwmp.vo.NotificationMessage;


public interface NotificationListener
{
	public void processMsg(NotificationMessage  notificationMessage);
}
