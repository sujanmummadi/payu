package com.cupola.fwmp.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.vo.MailMessage;

public class MailConsumer implements Runnable
{

	final static Logger logger = Logger.getLogger(MailConsumer.class);
	
	private List<MailListener>  listeners;
	
	private BlockingQueue<MailMessage> mailMessageSharedQueue;
	
	private MailService mailService ; 
	
	
	public void setMailService(MailService mailService)
	{
		this.mailService = mailService;
	}

	public MailConsumer(){
		
	}

	   

	public MailConsumer(BlockingQueue<MailMessage> sharedQueue , List<MailListener>  listeners , MailService mailService)
	{
		this.mailMessageSharedQueue = sharedQueue;
		this.listeners = listeners;
		this.mailService = mailService;
	}

	@Override
	public void run()
	{
		MailMessage mailMessage = null;
		try
		{
			while ((mailMessage = mailMessageSharedQueue.take()) != null)
			{
				
				try
				{
					// send mail
					logger.info("Mail Send Successful ");
					
					mailService.sendSimpleMail(mailMessage.getEmailId(), mailMessage.getSubject(), mailMessage.getMessage());
					
					for(MailListener listener : this.listeners)
					{
						listener.processMsg(mailMessage);
					}
				} catch (Exception e)
				{
					logger.error("Error While sending mail to customer :"+e.getMessage());
					// TODO Auto-generated catch block
					e.printStackTrace();
					continue;
				}
			}
		} catch (InterruptedException ex)
		{
			logger.error("Exception in thread of MailConsumer"+ex.getMessage());
			ex.printStackTrace();
			
		}

	}
}
