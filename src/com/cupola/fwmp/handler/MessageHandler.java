package com.cupola.fwmp.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MailMessage;
import com.cupola.fwmp.vo.NotificationMessage;


public class MessageHandler
{
	final static Logger logger = Logger.getLogger(MessageHandler.class);
	
	final static int NOTIFIATION_MESSAGE_QUEUE_SIZE = 10;
	
	final static int MAIL_MESSAGE_QUEUE_SIZE = 10;
	
	final static int GCM_MESSAGE_QUEUE_SIZE = 10;
	
	

	private static BlockingQueue<NotificationMessage> notificationMessageSharedQueue = new LinkedBlockingQueue<NotificationMessage>(NOTIFIATION_MESSAGE_QUEUE_SIZE);
	
	private static BlockingQueue<MailMessage> mailMessageSharedQueue = new LinkedBlockingQueue<MailMessage>(MAIL_MESSAGE_QUEUE_SIZE);
	
	private static BlockingQueue<GCMMessage> gCMMessageSharedQueue = new LinkedBlockingQueue<GCMMessage>(GCM_MESSAGE_QUEUE_SIZE);
	
	
	private static List<NotificationListener> notificationListeners = new ArrayList<NotificationListener>();
	
	private static List<MailListener> mailListeners  = new ArrayList<MailListener>();
		
	private static List<GCMListener> gCMListeners  = new ArrayList<GCMListener>();

	private MailService mailService ; 
	
	public void setMailService(MailService mailService)
	{
		this.mailService = mailService;
	}

	public void init()
	{

		logger.info("MessageHandler init method called..");
		
		new Thread(new NotificationConsumer(notificationMessageSharedQueue, notificationListeners)).start();

		new Thread(new MailConsumer(mailMessageSharedQueue, mailListeners , mailService) ).start();
		
		new Thread(new GCMConsumer(gCMMessageSharedQueue, gCMListeners )).start();
		
		logger.info("BMessageHandler init method executed.");
	}

	
	public void addInNotificationMessageQueue(NotificationMessage notificationMessage)
	{
		try
		{
			notificationMessageSharedQueue.put(notificationMessage);
			logger.debug("adding in the notificationMessageSharedQueue success size: "+ notificationMessageSharedQueue.size() + " notificationMessageSharedQueue: " + notificationMessageSharedQueue);
		} catch (InterruptedException e)
		{
			logger.error("Error While thread interruption on addInNotificationMessageQueue :"+e.getMessage());
			e.printStackTrace();
		}
	
	}

	
	public void addInMailMessageQueue(MailMessage mailMessage)
	{
		try
		{
			mailMessageSharedQueue.put(mailMessage);
			logger.info("adding in the mailMessageSharedQueue success size: "+ mailMessageSharedQueue.size() + " mailMessageSharedQueue: " + mailMessageSharedQueue);
		} catch (InterruptedException e)
		{
			logger.error("Error While thread Interruption addInMailMessageQueue :"+e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public void addInGCMMessageQueue(GCMMessage gCMMessage)
	{ 
		
		logger.debug("GCMMessage ::: "+gCMMessage);
		try
		{
			gCMMessageSharedQueue.put(gCMMessage);
			
			logger.debug("adding in the gCMMessageSharedQueue success size: "+ gCMMessageSharedQueue.size() + " gCMMessageSharedQueue: " +gCMMessageSharedQueue);
		} catch (InterruptedException e)
		{
			logger.error("Error While addInGCMMessageQueue interruption occured :"+e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	
	public void registerNotificationListener(NotificationListener listener)
	{
		notificationListeners.add(listener);
	}
	
	public void registerMailListener(MailListener listener)
	{
		mailListeners.add(listener);
		
	}
	
	public void registerListener(GCMListener listener)
	{
		gCMListeners.add(listener);
	}
}
