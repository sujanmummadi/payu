package com.cupola.fwmp.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.cupola.fwmp.vo.GCMMessage;

public class GCMConsumer implements Runnable
{

	final static Logger logger = Logger.getLogger(GCMConsumer.class);

	private String gcmApiKey;
    private String gcmUrl;

	public void setGcmApiKey(String gcmApiKey)
	{
		this.gcmApiKey = gcmApiKey;
	}

	public void setGcmUrl(String gcmUrl)
	{

		this.gcmUrl = gcmUrl;
	}

	private List<GCMListener> listeners;

	private BlockingQueue<GCMMessage> gcmMessageSharedQueue;

	public GCMConsumer()
	{
	}

	public GCMConsumer(BlockingQueue<GCMMessage> sharedQueue,
			List<GCMListener> listeners)
	{
		this.gcmMessageSharedQueue = sharedQueue;
		this.listeners = listeners;
	}

	@Override
	public void run()
	{
		GCMMessage gCMMessage;
		OutputStream outputStream = null;
		InputStream inputStream = null;

		try
		{
			
			while ((gCMMessage = gcmMessageSharedQueue.take()) != null)
			{
				String response = null;
				

				try
				{
					// Prepare JSON containing the GCM message content. What to
					// send and
					// where to send.
					JSONObject jGcmData = new JSONObject();
					JSONObject jData = new JSONObject();
					try
					{
						jData.put("message", gCMMessage.getMessage());

						// Where to send GCM message.
						jGcmData.put("to", gCMMessage.getRegistrationId());

						// What to send in GCM message.
						jGcmData.put("data", jData);

					} catch (JSONException e)
					{
						logger.error("Error While creating json object "+e.getMessage());
						e.printStackTrace();
						continue;
					}

					// Create connection to send GCM Message request.
					// URL url = new
					// URL(CommonUtil.getPropertyByName("GCM_URL"));

					URL url = new URL(gcmUrl);
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					// conn.setRequestProperty("Authorization", "key=" +
					// CommonUtil.getPropertyByName("API_KEY"));
					conn.setRequestProperty("Authorization", "key=" + gcmApiKey);
					conn.setRequestProperty("Content-Type", "application/json");
					conn.setRequestMethod("POST");
					conn.setDoOutput(true);

					// Send GCM message content.
					outputStream = conn.getOutputStream();
					outputStream.write(jGcmData.toString().getBytes());

					// Read GCM response.
					inputStream = conn.getInputStream();
					response = IOUtils.toString(inputStream);
					logger.debug("GCM Sever Response : " + response);
					logger.debug("Check your device/emulator for notification or loggercat for "
							+ "confirmation of the receipt of the GCM message.");
				} catch (IOException e)
				{
					logger.error("Unable to send GCM message.");
					logger.error("Please ensure that API_KEY has been replaced by the server "
							+ "API key, and that the device's registration token is correct (if specified).");
					e.printStackTrace();
					continue;
				}

				for (GCMListener listener : this.listeners)
				{
					logger.debug("Get from the queue   listener : " + listener
							+ " , gCMMessage :" + gCMMessage);
					listener.processMsg(gCMMessage);
				}
			}
		} catch (InterruptedException ex)
		{
			logger.error("Exception in thread of GCMConsumer" + ex.getMessage());
			ex.printStackTrace();

		}finally{
			try {
				inputStream.close();
				outputStream.close();
			} catch (IOException e) {
				logger.error("Error While streams closing :"+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
