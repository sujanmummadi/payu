package com.cupola.fwmp.handler;

import java.util.List;

import org.apache.log4j.Logger;

import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MailMessage;
import com.cupola.fwmp.vo.Message;
import com.cupola.fwmp.vo.NotificationMessage;
import com.cupola.fwmp.vo.UserAndRegIdDetails;

public class MessageSender
{
	final static Logger logger = Logger.getLogger(MessageSender.class);
	
	TabletDAO tabletDAO;
	MessageHandler messageHandler;
	
	public void setTabletDAO(TabletDAO tabletDAO)
	{
		this.tabletDAO = tabletDAO;
	}

	public void setMessageHandler(MessageHandler messageHandler)
	{
		this.messageHandler = messageHandler;
	}

	public void sendMessage(Message message)
	{
		List<UserAndRegIdDetails>  userAndRegIdDetail =  null;
		
		String msg = message.getMessage();
		
		if( message.getUserId() >= 0 )
			userAndRegIdDetail = tabletDAO.getUserAndRegIdDetails(message.getUserId());

		if(userAndRegIdDetail != null)
		for (UserAndRegIdDetails userAndRegIdDetails : userAndRegIdDetail)
		{
			String emailId = userAndRegIdDetails.getEmailId();
			Long mobileNumber = userAndRegIdDetails.getMobileNo();
			String regId = userAndRegIdDetails.getRegistrationId();
			
			
			if(emailId != null && !emailId.isEmpty()){
				MailMessage mailMessage = new MailMessage();
				mailMessage.setMessage(msg);
				mailMessage.setSubject("hiiiiiiiiii");
				mailMessage.setEmailId(emailId);
				
				messageHandler.addInMailMessageQueue(mailMessage);
			}
			
			if(mobileNumber != null && mobileNumber >= 0){
				NotificationMessage notificationMessage = new NotificationMessage();
				notificationMessage.setMessage(msg);
				
				notificationMessage.setMobileNo(mobileNumber);
				
				messageHandler.addInNotificationMessageQueue(notificationMessage);
			}
			
			if(regId != null && !regId.isEmpty()){
				GCMMessage  gCMMessage = new GCMMessage();
				gCMMessage.setMessage(msg);
				
				gCMMessage.setRegistrationId(regId);
				
				messageHandler.addInGCMMessageQueue(gCMMessage);
			}
		}
		
		logger.info("Done Done Done Done Done Done ");
	}
	
}
