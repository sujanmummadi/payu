package com.cupola.fwmp.handler;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MailMessage;
import com.cupola.fwmp.vo.NotificationMessage;

public class NotificationConsumer implements Runnable
{
	final static Logger logger = Logger.getLogger(NotificationConsumer.class);

	private List<NotificationListener>  listeners;
	
	private BlockingQueue<NotificationMessage> notificationMessageSharedQueue;
	
	public NotificationConsumer(){ 	}
	
	public NotificationConsumer(BlockingQueue<NotificationMessage> sharedQueue , List<NotificationListener>  listeners)
	{
		this.notificationMessageSharedQueue = sharedQueue;
		this.listeners = listeners;
	}

	@Override
	public void run()
	{
		NotificationMessage notificationMessage = null;
		try
		{
			while ((notificationMessage = notificationMessageSharedQueue.take()) != null)
			{
				
				try
				{
					logger.info("Notification Send Successful ");
					for(NotificationListener listener : this.listeners)
					{
						logger.info("Get form the queue   listener : "+listener+" , notificationMessage :"+notificationMessage );
						listener.processMsg(notificationMessage);
					}
				} catch (Exception e)
				{
					// TODO Auto-generated catch block
					logger.error("Error While processing msg from customer :"+e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (InterruptedException ex)
		{
			logger.error("Exception in thread of NotificationConsumer"+ex.getMessage());
			ex.printStackTrace();
			
		}

	}
}
