package com.cupola.fwmp.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.ResponseUtil;

public class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	private String defaultFailureUrl;
	private final ObjectMapper mapper = new ObjectMapper();
	
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {
        
        if(ApplicationUtils.retrieveHttpRequestInfo().isRequestedFromMobile())
		{

			APIResponse res = ResponseUtil.createLoginFailureResponse(exception
					.getMessage());

			PrintWriter writer = response.getWriter();
			mapper.writeValue(writer, res);
			writer.flush();
			writer.close();
			return;
		}
        else
        {
        	if (this.defaultFailureUrl == null)
    		{
    			this.logger
    					.debug("No failure URL set, sending 401 Unauthorized error");

    			response.sendError(401, "Authentication Failed: "
    					+ exception.getMessage());
    		} else
    		{
    			saveException(request, exception);

    			
    				this.logger.debug("Redirecting to " + this.defaultFailureUrl);
    				this.redirectStrategy
    						.sendRedirect(request, response, this.defaultFailureUrl);
    			
    		}
        	
        	
        }
    }

	public String getDefaultFailureUrl()
	{
		return defaultFailureUrl;
	}

	public void setDefaultFailureUrl(String defaultFailureUrl)
	{
		this.defaultFailureUrl = defaultFailureUrl;
	}
}