package com.cupola.fwmp.handler;

import com.cupola.fwmp.vo.MailMessage;


public interface MailListener
{
	public void processMsg(MailMessage  mailMessage);
}
