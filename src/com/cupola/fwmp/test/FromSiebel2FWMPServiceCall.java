/**
* @Author aditya  28-Sep-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.test;

import java.io.IOException;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateProspectVo;

/**
 * @Author aditya 28-Sep-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FromSiebel2FWMPServiceCall {

	public static String uri = "http://106.51.41.113:9091/rest/external/crm/create/prospect";

	public static void callFwmpService(CreateUpdateProspectVo createUpdateProspectVo) {

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;
		String resetMainQueueUrlWithIp = "http://172.23.25.113:9091/rest/external/crm/create/prospect";
		APIResponse apiResponse = null;

		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("admin", "admin123"));

		try {

			ObjectMapper mapper = new ObjectMapper();

			httpclient = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
			System.out.println("httpclient " + httpclient.toString());
			HttpPost httpPost = new HttpPost(resetMainQueueUrlWithIp);

			String mainQueueString = mapper.writeValueAsString(createUpdateProspectVo);

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils.toString(httpResponse.getEntity());

			System.out.println("Merging main queue response " + stringResponse);

			apiResponse = ResponseUtil.createSuccessResponse().setData(stringResponse);
			System.out.println("Merging main queue apiResponse " + apiResponse);

			httpclient.close();
		} catch (Exception e) {
			System.err.println("Error while merging main queue " + e.getMessage());
			e.printStackTrace();

		} finally {
			try {
				if (httpResponse != null)
					httpResponse.close();

				if (httpclient != null)
					httpclient.close();

			} catch (IOException e) {
				System.err.println(
						"Error in finally block, while closing httpresponse merging main queue. " + e.getMessage());
				e.printStackTrace();
			}
		}

		System.out.println("########### " + apiResponse);
	}

	public static void main(String[] args) {

		CreateUpdateProspectVo createUpdateProspectVo = null;

		callFwmpService(createUpdateProspectVo);

		System.out.println("Done......");
	}
}
