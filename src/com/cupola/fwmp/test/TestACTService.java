/**
 * 
 */
package com.cupola.fwmp.test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.FileRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.cupola.fwmp.util.XMLHelper;
import com.cupola.fwmp.vo.gis.GISFesiabilityInput;

/**
 * @author aditya
 * 
 */
public class TestACTService {
	
	static private Logger log = LogManager.getLogger(TestACTService.class.getName());
	
	static String CUSTOMER_ID_PLACE_HOLDER = "_CUSTOMER_ID_";
	static String PHONE_NO_PLACE_HOLDER = "_PHONE_NO_";
	static String LATITUDE_PLACE_HOLDER = "_LATITUDE_";
	static String LONGITUDE_PLACE_HOLDER = "_LONGITUDE_";
	static String TRANSACTION_NO_PLACE_HOLDER = "_TRANSACTION_NO_";
	static String CITY_PLACE_HOLDER = "_CITY_";
	

	public static void main(String[] args) {
		log.debug("Starting ");
//		 testMethod();
//		stringTest();
		
		long date = 1466415302399L;
		
		log.debug("Util date " + new java.util.Date(date));
		
		log.debug("SQL date " + new java.sql.Date(date));
		
		log.debug("SQL Time "+new java.sql.Time(date));
		
		log.debug("SQL TimeStamp "+new java.sql.Timestamp(date));
		
		System.exit(0);
		
		Document doc = loadDocument(
				"/home/aditya/Projects/ACT_FWMP/Dev/workspace/FWMP/src/test.xml",
				new XMLHelper.ErrorList());
		
		Element elme = XMLHelper.getChildElement(doc.getDocumentElement(),
				"feasibilitycheck");
		
		Element elme1 = XMLHelper.getNextChildElement(elme,
				"feasibilitycheckxml");
		
		Element elme2 = XMLHelper.getNextToNextChildElement(elme1,
				"requestinfo");
		String cust2 = XMLHelper.getChildElementValue(elme2, "phoneNo");
		Element cust = XMLHelper.getNextToNextToNextChildElement(elme2, "phoneNo");
		
		XMLHelper.setElementValue(cust, "abc");
		
		String cust1 = XMLHelper.getChildElementValue(elme2, "phoneNo");
		
		log.debug("Done" + cust +" "+cust1+" "+cust2);
	}

	private static Document loadDocument(String documentId,
			XMLHelper.ErrorList errorMap) {
		try {
			return XMLHelper.parseAndValidate(documentId, errorMap);
		} catch (Exception e) {

			throw new RuntimeException("Failed to access file store.", e);
		}
	}
	
public static void actGISFesibilityCheck(GISFesiabilityInput gisInput) {
		
		String actGISServiceString = "<ACTGIS><feasibilitycheck><feasibilitycheckxml><requestinfo><customerId>_CUSTOMER_ID_</customerId><phoneNo>_PHONE_NO_</phoneNo><latitude>_LATITUDE_</latitude><longitude>_LONGITUDE_</longitude><transactionNo>_TRANSACTION_NO_</transactionNo><city>_CITY_</city></requestinfo></feasibilitycheckxml></feasibilitycheck></ACTGIS>";
		
		 actGISServiceString = actGISServiceString.replace(CUSTOMER_ID_PLACE_HOLDER, gisInput.getCustomerId()).
		 replace(PHONE_NO_PLACE_HOLDER, gisInput.getPhoneNo()).
		 replace(LATITUDE_PLACE_HOLDER, gisInput.getLatitude()).
		 replace(LONGITUDE_PLACE_HOLDER, gisInput.getLongitude()).
		 replace(TRANSACTION_NO_PLACE_HOLDER, gisInput.getTransactionNo()).
		 replace(CITY_PLACE_HOLDER, gisInput.getCity());
		
		log.debug("In method");
		
		int responseCode = 0;
		
		try {
			
			CloseableHttpClient httpClient = HttpClients.createDefault();
			String strURL = "http://202.83.23.113:8081/ActGISV2/FieldAPI/GISFeasiblityFieldApiTest";
			
			log.debug("URL " + strURL);
			
			HttpPost post = new HttpPost(strURL);
			// Request content will be retrieved directly
			// from the input stream
			StringEntity entity = new StringEntity(actGISServiceString, "UTF-8");
			entity.setContentType("application/xml");

			post.setEntity(entity);
			CloseableHttpResponse response = httpClient.execute(post);
			log.debug("Response code "+EntityUtils.toString(response.getEntity()));
			
			
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		
		
	}

	public static void testMethod() {
		
		String test = "<ACTGIS><feasibilitycheck><feasibilitycheckxml><requestinfo><customerId>_CUSTOMER_ID_</customerId><phoneNo>_PHONE_NO_</phoneNo><latitude>_LATITUDE_</latitude><longitude>_LONGITUDE_</longitude><transactionNo>_TRANSACTION_NO_</transactionNo><city>_CITY_</city></requestinfo></feasibilitycheckxml></feasibilitycheck></ACTGIS>";
		
		test.replace(CUSTOMER_ID_PLACE_HOLDER, "11318769");
		test.replace(PHONE_NO_PLACE_HOLDER, "9036977748");
		test.replace(LATITUDE_PLACE_HOLDER, "12.3456");
		test.replace(LONGITUDE_PLACE_HOLDER, "77.2345");
		test.replace(TRANSACTION_NO_PLACE_HOLDER, "126");
		test.replace(CITY_PLACE_HOLDER, "BANGALORE");
		
		log.debug("In method");
		
		String strURL = "http://202.83.23.113:8081/ActGISV2/FieldAPI/GISFeasiblityFieldApiTest";
	
		log.debug("URL " + strURL);
		
		File input = new File("/home/aditya/Projects/ACT_FWMP/Dev/workspace/FWMP/src/test.xml");
		
		PostMethod post = new PostMethod(strURL);
		// Request content will be retrieved directly
		// from the input stream
		RequestEntity entity = new FileRequestEntity(input, "application/xml");

		post.setRequestEntity(entity);
		log.debug("Entity " + entity.getContentType());
		log.debug("Post " + post.getPath());
		// Get HTTP client
		HttpClient httpclient = new HttpClient();
		// Execute request
		try {
			log.debug("Try");
			int result = httpclient.executeMethod(post);
			// Display status code
			log.debug("Response status code: " + result);
			// Display response
			log.debug("Response body: ");
			log.debug(post.getResponseBodyAsString());
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Release current connection to the connection pool once you are
			// done
			post.releaseConnection();
		}
	}

}
