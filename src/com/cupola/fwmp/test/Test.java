package com.cupola.fwmp.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.web.client.RestTemplate;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.FTPUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.integ.app.ProspectVO;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.ListOfPrioritySource;
import com.cupola.fwmp.vo.tools.PriorityVO;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.cupola.fwmp.dao.tool.ACTExternalDAOImpl;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.service.tool.ACTExternalToolServicesImpl;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.ws.tools.ACTExternalToolController;

import com.cupola.fwmp.dao.tool.ACTExternalDAOImpl;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.service.tool.ACTExternalToolServicesImpl;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.ws.tools.ACTExternalToolController;

public class Test
{
	private static String resetUserUrl = "http://localhost:8080/rest/integ/global/service/reloadAll";

	public static Map<String, List<String>> getAllTickets()
	{
		Map<String, List<String>> map = new HashMap<>();
		// iterate resultset
		// check for fxName
		if (map.containsKey(""))
		{
			List<String> list = map.get("");
			list.add("");

			map.put("", list);

		} else
		{
			List<String> list = new ArrayList<>();
			list.add("");
			map.put("", list);

		}

		return map;
	}

	public static Date getFirstDateOfPriviousMonth()
	{
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		aCalendar.set(Calendar.DATE, -30);
		aCalendar.set(Calendar.HOUR_OF_DAY, 0);
		aCalendar.set(Calendar.MINUTE, 0);
		aCalendar.set(Calendar.SECOND, 0);
		aCalendar.set(Calendar.MILLISECOND, 0);
		Date firstDateOfPreviousMonth = aCalendar.getTime();
		return firstDateOfPreviousMonth;
	}

	public static Date getLastDateOfPriviousMonth()
	{
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		aCalendar.set(Calendar.HOUR_OF_DAY, 23);
		aCalendar.set(Calendar.MINUTE, 59);
		aCalendar.set(Calendar.SECOND, 59);
		aCalendar.set(Calendar.MILLISECOND, 0);
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		return lastDateOfPreviousMonth;
	}

	public static void main(String[] args)
			throws JsonGenerationException, JsonMappingException, IOException
	{
		Object []obj = {"2018-01-04 13:07:25"};
		Date d = FrTicketUtil.convertObjectToDate(obj[0]);
		System.out.println(GenericUtil.convertToCustomerDateFormat(d));
		System.exit(0);
		String test = "SELECT TICKET_NO AS WO_NO, PROSPECT_NBR AS PROSPECT_NO, CUSTOMER_NBR AS MQ_ID, CUSTOMER_NAME AS CUST_NAME, ADDRESS AS CUST_ADDRESS, CATEGORY_NAME AS CAT_NAME, VOC AS VOC, RESPONSE_STATUS AS RESP_STATUS, SYMPTOM_NAME AS SYMP_NAME, TO_CHAR(SRV_REQ_DATE, 'YYYY-MM-DD HH24:MI') AS REQ_DATE, COMMENT_CODE AS COMMENT_CODE, COMMENTS AS COMMENTS , CX_IP AS CX_IP, FX_NAME AS FX_NAME,PHONE AS PHONE, AREA AS AREA, CITY AS CITY, BRANCH AS BRANCH FROM CRM_ROI.FWMP_BLR  where CITY = ? and CATEGORY_NAME in ('BROADBAND-COMPLAINTS') AND RESPONSE_STATUS in ('ACTIVE','NEW','REOPEN') AND ( MODIFIED_DATE > TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS') OR ( RESPONSE_STATUS in ('REOPEN') AND REOPEN_DATE > TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'))) ORDER BY MODIFIED_DATE DESC";
		
		System.out.println(test);
		
		test = test.replace("where CITY = ?", "where CITY = ? and BRANCH = ? ");
		
		System.out.println(test);
		
		if (test.contains("where CITY = ? and BRANCH = ?"))
			test = test.replace("where CITY = ? and BRANCH = ?",
					"where CITY = ? and BRANCH = ? ");

		System.out.println(test);
		
		System.exit(0);
		
		ACTExternalToolController actExternalToolController = new ACTExternalToolController();
		ACTExternalToolServicesImpl actExternalToolServicesImpl= new ACTExternalToolServicesImpl();
		ACTExternalDAOImpl actExternalDAOImpl = new ACTExternalDAOImpl();
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

		try
		{
			String basefile = createFolder("/home/aditya/Projects/ACT_FWMP/LiveReport", "Hyd", "CAF");

			String file = "CAF_Report.csv";

			String remoteArchPath = FTPUtil.getRemoteBasePath() + "output/";

			System.out.println("basee file " + basefile + " remote file "
					+ remoteArchPath);

			// FTPUtil.uploadToFTP(basefile + File.separator
			// + file, remoteArchPath);

			System.exit(0);
			
//			List unmodifiableList = Collections.unmodifiableList(list);

			Date oldDate = (Date) formatter.parse("30/09/2016 13:37:16");

			System.out.println(oldDate);

			Date newDate = (Date) formatter.parse("05/10/2016 13:38:00");

			System.out.println(newDate);

			Date getNextMaxPossibleEtrDate = getNextMaxPossibleEtrDate("Fiber", oldDate
					.getTime());

			System.out.println(getNextMaxPossibleEtrDate);

		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**@author aditya
	 * void
	 */
	private static void java8ListIteration() 
	{
		List<TestClass> intList = new ArrayList<>();
		intList.add(new TestClass("Adi1", "Address", 28));
		intList.add(new TestClass("Adi2", "Address", 28));
		intList.add(new TestClass("Adi3", "Address", 28));
		intList.add(new TestClass("Adi4", "Address", 28));
		intList.add(new TestClass("Adi5", "Address", 28));
		intList.add(new TestClass("Adi6", "Address", 28));
//
//		intList.forEach(i -> 
//		{
//			if (i.getName().equalsIgnoreCase("adi1")) 
//			{
//				i.setName("Aditya");
//			}
//			System.out.println(i);
//		});
//

//		intList.stream().forEach(i -> 
//		{
//			if (i.getName().equalsIgnoreCase("adi1")) 
//			{
//				i.setName("Aditya");
//			}
//			System.out.println(i);
//		});
//		

//		intList.parallelStream().forEach(i -> 
//		{
//			if (i.getName().equalsIgnoreCase("adi1")) 
//			{
//				i.setName("Aditya");
//			}
//			System.out.println(i);
//		});
		
		
	}

	public static Date getNextMaxPossibleEtrDate(String connectionType,
			Long commitedEtrInMillisec)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(commitedEtrInMillisec);

		if (connectionType.equalsIgnoreCase("Fiber"))
		{
			calendar.add(Calendar.HOUR, +72);

		} else if (connectionType.equalsIgnoreCase("Copper"))
		{
			calendar.add(Calendar.HOUR, +48);
		}

		return calendar.getTime();
	}

	public static boolean dateCompare(Date nextPossibleETR, Date selectedEtr)
	{
		boolean result = false;

		if (nextPossibleETR.compareTo(selectedEtr) > 0)
		{
			System.out.println("Old date is samller than new date");

			return false;

		} else if (nextPossibleETR.compareTo(selectedEtr) < 0)
		{
			System.out.println("Old date is fratter than new date");

			return true;

		} else if (nextPossibleETR.compareTo(selectedEtr) == 0)
		{
			System.out.println("Old date is equal to new date");
			return true;
		}
		return result;
	}

	public static String createFolder(String documentPath, String parentFolder,
			String folderName)
	{
		File filePath = new File(documentPath + File.separator + parentFolder);

		if (!filePath.exists())
		{
			filePath.mkdir();

			System.out.println("Creating folder for prospect " + parentFolder
					+ " at " + documentPath + "/" + parentFolder);

			String folder = documentPath + File.separator + parentFolder;
			return createChildFolder(folder, folderName);

		} else
		{

			System.out.println("Folder Exist with folder Name " + parentFolder
					+ " at " + documentPath + "/" + parentFolder);

			String folder = documentPath + File.separator + parentFolder;
			return createChildFolder(folder, folderName);
		}

	}

	public static String createChildFolder(String documentPath,
			String folderName)
	{
		File filePath = new File(documentPath + File.separator + folderName);

		if (!filePath.exists())
		{
			filePath.mkdir();

			System.out.println("Creating folder for prospect " + folderName
					+ " at " + documentPath + "/" + folderName);

			return documentPath + File.separator + folderName;

		} else
		{

			System.out.println("Folder Exist with folder Name " + folderName
					+ " at " + documentPath + folderName);

			return documentPath + File.separator + folderName;

		}

	}

	public static void uploadOnFtp()
	{

	}

	private static APIResponse addToCacheServiceCall(String serverIp)
	{
		APIResponse apiResponse = null;
		System.out.println("Reset Location cache ############### " + resetUserUrl);

		String resetUserUrlWithIp = resetUserUrl.replace("localhost", serverIp);

		System.out.println("Reset Location cache ############### " + resetUserUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{	          
			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetUserUrlWithIp);

//			String mainQueueString = "";
//
//			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");
//
//			stringEntity.setContentType("application/json");
//
//			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			apiResponse = ResponseUtil.createSuccessResponse()
					.setData(stringResponse);

			System.out.println("Reset Location cache response " + stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			System.err
					.println("Error while Location city cache " + e.getMessage());

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				httpclient.close();

			} catch (IOException e)
			{
				System.err
						.println("Error in finally block, while closing httpresponse Reset Location cache. "
								+ e.getMessage());
				e.printStackTrace();
			}
		}

		return apiResponse;

	}

	private static APIResponse addToCacheServiceCall_RestTemplate(String serverIp)
	{
		RestTemplate restTemplate = null;
		APIResponse apiResponse = null;
		System.out.println("Reset Location cache ############### " + resetUserUrl);

		String resetUserUrlWithIp = resetUserUrl.replace("localhost", serverIp);

		System.out.println("Reset Location cache ############### " + resetUserUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{	          
			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetUserUrlWithIp);

//			String mainQueueString = "";
//
//			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");
//
//			stringEntity.setContentType("application/json");
//
//			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			apiResponse = ResponseUtil.createSuccessResponse()
					.setData(stringResponse);

			System.out.println("Reset Location cache response " + stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			System.err
					.println("Error while Location city cache " + e.getMessage());

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				httpclient.close();

			} catch (IOException e)
			{
				System.err
						.println("Error in finally block, while closing httpresponse Reset Location cache. "
								+ e.getMessage());
				e.printStackTrace();
			}
		}

		return apiResponse;

	}

	public static void downloadApkTest()
	{
		String mergeQueueUrl = "http://localhost:8080/rest/integ/global/service/downlaodapk";
		System.out.println();
		System.out.println("Merge main queue Job ############### "
				+ mergeQueueUrl);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			httpclient = HttpClients.createDefault();

			HttpGet httpGet = new HttpGet(mergeQueueUrl);

			String mainQueueString = "";

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/octet-stream");

			httpResponse = httpclient.execute(httpGet);
			InputStream inputStream = httpResponse.getEntity().getContent();

			OutputStream outputStream = new FileOutputStream(new File("/home/aditya/Projects/ACT_FWMP/Releases/Training_apk/Apktest.apk"));

			int read = 0;

			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1)
			{
				outputStream.write(bytes, 0, read);
			}

			// String stringResponse = EntityUtils
			// .toString(httpResponse.getEntity());
			outputStream.close();

			// System.out.println("Merging main queue response " +
			// stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			System.err.println("Error while merging main queue "
					+ e.getMessage());
			e.printStackTrace();

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				httpclient.close();

			} catch (IOException e)
			{
				System.err
						.println("Error in finally block, while closing httpresponse merging main queue. "
								+ e.getMessage());
				e.printStackTrace();
			}
		}

		System.out.println("Merge main queue final ");

	}
}
