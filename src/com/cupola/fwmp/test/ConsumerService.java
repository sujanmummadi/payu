
package com.cupola.fwmp.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.integ.app.ProspectVO;

/**
 * @author aditya
 * @created 11:48:26 AM Apr 12, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */
public class ConsumerService
{
	public static void main(String[] args)
	{
		System.out.println("Calling Customer app service ");
		callCustomerAppService();
		System.out.println("Called Customer app service ");

	}

	public static void callCustomerAppService()
	{
		String custAppSerUrl = "http://localhost:8080/rest/integ/app/prospect/createProspect";
		System.out.println();
		System.out.println("Calling customer App service ############### "
				+ custAppSerUrl);

		ProspectVO prospectVO = getProspectVO();

		System.out.println("Calling customer App service VO  " + prospectVO);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			ObjectMapper mapper = new ObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(custAppSerUrl);

			String mainQueueString = mapper.writeValueAsString(prospectVO);

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			APIResponse apiResponse = mapper
					.readValue(stringResponse, APIResponse.class);

			System.out
					.println("customer App service response " + stringResponse);

			System.out.println("customer App service response in apiResponse  "
					+ apiResponse);

			httpclient.close();

		} catch (Exception e)
		{
			System.err.println("Error while calling customer App service "
					+ e.getMessage());

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				httpclient.close();

			} catch (IOException e)
			{
				System.err
						.println("Error in finally block, while closing httpresponse customer App service. "
								+ e.getMessage());
				e.printStackTrace();
			}
		}

	}

	private static ProspectVO getProspectVO()
	{
		ProspectVO prospectVO = new ProspectVO();

		prospectVO.setAction("CREATE");
		prospectVO.setTitle("Mr");
		prospectVO.setFirstName("Aditya");
		prospectVO.setLastName("Singh");
		prospectVO.setPrefferedCallDate(new Date());
		prospectVO.setProspectNumber("2409627");
		prospectVO.setMobileNumber("9035747331");
		prospectVO.setEmailId("aditya@cupolatechnology.com");
		prospectVO.setCurrentAddress("Test");
		prospectVO.setCommunicationAdderss("Test");
		prospectVO.setPermanentAddress("Test");
		prospectVO.setConnectionType("Domestic");
		prospectVO.setPincode(560076 + "");
		prospectVO.setFxName("AMPT5-PARKW-PANJ1-14-15-216.10");
		prospectVO.setCityCode("HYD");
		prospectVO.setBranchCode("AMRPET");
		prospectVO.setAreaCode("Begumpet");
		prospectVO.setProspectType("HOT");
		prospectVO.setLongitude("12.909321");
		prospectVO.setLattitude(" 77.649129");
		return prospectVO;
	}
}
