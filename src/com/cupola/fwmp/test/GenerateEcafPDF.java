/**
* @Author kiran  Dec 8, 2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.test;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.cupola.fwmp.vo.EcafVo;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

/**
 * @Author kiran Dec 8, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class GenerateEcafPDF {

	public static final String SRC = "/home/kiran/backup/PDFEDITOR/EcafTemplate_in.pdf";
	public static final String DEST = "/home/kiran/backup/PDFEDITOR/EcafTemplate_out.pdf";
	public static final String IMG = "/home/kiran/backup/PDFEDITOR/person.png";
	public static final String TICK = "/home/kiran/backup/PDFEDITOR/tick.png";

	int PIC_MAX_WIDTH = 100;
	int PIC_MAX_HEIGHT = 150;
	int TICK_MAX_WIDTH = 8;
	int TICK_MAX_HEIGHT = 8;
	int IMAGE_X = 464;
	int IMAGE_Y = 628;
	int CAF_NO_X = 84;
	int CAF_NO_Y = 675;
	String CAF_NO = "123456";
	int CREATION_DATE_X = 210;
	int CREATION_DATE_Y = 675;
	String CREATION_DATE = "08-17-2017";
	int PLACE_X = 330;
	int PLACE_Y = 675;
	String PLACE = "BANGLORE";
	int CUSTOMER_NAME_X = 128;
	int CUSTOMER_NAME_Y = 614;
	String CUSTOMER_NAME = "MALLA SRINIVAS KIRAN";
	int PRIMARY_TX_X = 130;
	int PRIMARY_TX_Y = 570;
	String PRIMARY_TX = "ekyc_5189427971512563472373";
	int PRIMARY_TX_DATE_X = 399;
	int PRIMARY_TX_DATE_Y = 570;
	String PRIMARY_TX_DATE = "06/12/2017";
	int PRIMARY_TX_TIME_X = 489;
	int PRIMARY_TX_TIME_Y = 570;
	String PRIMARY_TX_TIME = "18:02:56";
	int PROFESSION_X = 130;
	int PROFESSION_Y = 551;
	String PROFESSION = "BUSINESSMAN";
	int CARE_OF_X = 140;
	int CARE_OF_Y = 530;
	String CARE_OF = "S/O Rajulu";
	int GENDER_X = 370;
	int GENDER_Y = 530;
	String GENDER = "M";
	int AADHAR_ADDRESS_X = 119;
	int AADHAR_ADDRESS_Y = 509;
	String AADHAR_ADDRESS = "86-18-31, pedda veedhi, Rajahmundry urban, V L Puram, East Godavari, Andhra Pradesh, 533101,";
	int VTCNAME_X = 69;
	int VTCNAME_Y = 468;
	String VTCNAME = "Rajahmundry";
	int AADHAR_STATE_X = 238;
	int AADHAR_STATE_Y = 468;
	String AADHAR_STATE = "Andhra Pradesh";
	int AADHAR_PINCODE_X = 420;
	int AADHAR_PINCODE_Y = 468;
	String AADHAR_PINCODE = "560017";
	int AADHAR_MOBILE_NUMBER_X = 326;
	int AADHAR_MOBILE_NUMBER_Y = 448;
	String AADHAR_MOBILE_NUMBER = "9738434213";
	int ADDRESS_X = 119;
	int ADDRESS_Y = 425;
	String ADDRESS = "HSR layout";
	int CITY_X = 69;
	int CITY_Y = 386;
	String CITY = "Banglore";
	int STATE_X = 238;
	int STATE_Y = 386;
	String STATE = "Karnataka";
	int PINCODE_X = 420;
	int PINCODE_Y = 386;
	String PINCODE = "560017";
	int MOBILE_NUMBER_X = 326;
	int MOBILE_NUMBER_Y = 365;
	String MOBILE_NUMBER = "9738434213";
	int ALTERNATE_MOBILE_NUMBER_X = 474;
	int ALTERNATE_MOBILE_NUMBER_Y = 365;
	String ALTERNATE_MOBILE_NUMBER = "9090909090";
	int DRIVING_X = 231;
	int DRIVING_Y = 321;
	int PASSPORT_X = 312;
	int PASSPORT_Y = 321;
	int PANCARD_X = 393;
	int PANCARD_Y = 321;
	int RATION_CARD_X = 520;
	int RATION_CARD_Y = 321;
	int EMAIL_ADDRESS_X = 412;
	int EMAIL_ADDRESS_Y = 264;
	String EMAIL_ADDRESS = "mallasrinivaskiran@gmail.com";
	int STATIC_IP_YES_X = 205;
	int STATIC_IP_YES_Y = 177;
	int STATIC_IP_NO_X = 365;
	int STATIC_IP_NO_Y = 177;
	int CX_PERMISSION_YES_X = 205;
	int CX_PERMISSION_YES_Y = 154;
	int CX_PERMISSION_NO_X = 365;
	int CX_PERMISSION_NO_Y = 154;
	int TARIFF_PLAN_X = 148;
	int TARIFF_PLAN_Y = 757;
	String TARIFF_PLAN = "ACT Blaze 6 M + 1 M_FT";
	int TARIFF_PLAN_DURATION_1_X = 382;
	int TARIFF_PLAN_DURATION_1_Y = 756;
	int TARIFF_PLAN_DURATION_3_X = 436;
	int TARIFF_PLAN_DURATION_3_Y = 756;
	int TARIFF_PLAN_DURATION_6_X = 491;
	int TARIFF_PLAN_DURATION_6_Y = 756;
	int TARIFF_PLAN_DURATION_12_X = 549;
	int TARIFF_PLAN_DURATION_12_Y = 756;
	int ROUTER_YES_X = 437;
	int ROUTER_YES_Y = 733;
	int ROUTER_NO_X = 488;
	int ROUTER_NO_Y = 733;
	int INSTALLATION_CHARGES_X = 62;
	int INSTALLATION_CHARGES_Y = 714;
	String INSTALLATION_CHARGES = "434.78";
	int TOTAL_CHARGES_X = 437;
	int TOTAL_CHARGES_Y = 714;
	String TOTAL_CHARGES = "9426.0";
	int ONLINE_X = 227;
	int ONLINE_Y = 675;
	int CASH_X = 314;
	int CASH_Y = 675;
	int CHEQUE_X = 410;
	int CHEQUE_Y = 675;
	int DRAFT_X = 526;
	int DRAFT_Y = 675;
	int SIGN_MAX_WIDTH = 60;
	int SIGN_MAX_HEIGHT = 25;
	int SIGN_X = 53;
	int SIGN_Y = 415;
	int SIGN_DATE_X = 227;
	int SIGN_DATE_Y = 416;
	String SIGN_DATE = "06.12.2017";
	int SIGN_PLACE_X = 397;
	int SIGN_PLACE_Y = 416;
	String SIGN_PLACE = "BANGLORE";
	int SECONDRY_TX_X = 130;
	int SECONDRY_TX_Y = 380;
	String SECONDRY_TX = "ekyc_5189427971512563472373";
	int SECONDRY_TX_DATE_X = 399;
	int SECONDRY_TX_DATE_Y = 380;
	String SECONDRY_TX_DATE = "06/12/2017";
	int SECONDRY_TX_TIME_X = 489;
	int SECONDRY_TX_TIME_Y = 380;
	String SECONDRY_TX_TIME = "18:02:56";
	int HOME_X = 175;
	int HOME_Y = 275;
	int SOHO_X = 289;
	int SOHO_Y = 275;
	int CORPORATE_X = 417;
	int CORPORATE_Y = 275;
	int CYBER_CAFE_X = 549;
	int CYBER_CAFE_Y = 275;
	int INBOUND_X = 177;
	int INBOUND_Y = 252;
	int CALL_X = 293;
	int CALL_Y = 252;
	int REFERRAL_X = 419;
	int REFERRAL_Y = 252;
	int CAT5_X = 177;
	int CAT5_Y = 230;
	int FIBER_X = 294;
	int FIBER_Y = 230;
	int SE_NAME_X = 52;
	int SE_NAME_Y = 201;
	String SE_NAME = "Venkateshappa - SE";
	int CFE_NAME_X = 52;
	int CFE_NAME_Y = 160;
	String CFE_NAME = "Sridhar - CFE";
	int D_1_X = 108;
	int D_1_Y = 590;
	String D_1 = "1";
	int D_2_X = 121;
	int D_2_Y = 590;
	String D_2 = "1";
	int M_1_X = 134;
	int M_1_Y = 590;
	String M_1 = "1";
	int M_2_X = 147;
	int M_2_Y = 590;
	String M_2 = "1";
	int Y_1_X = 160;
	int Y_1_Y = 590;
	String Y_1 = "1";
	int Y_2_X = 173;
	int Y_2_Y = 590;
	String Y_2 = "1";
	int Y_3_X = 186;
	int Y_3_Y = 590;
	String Y_3 = "1";
	int Y_4_X = 199;
	int Y_4_Y = 590;
	String Y_4 = "1";
	int DECLARATION_SIGN_X = 52;
	int DECLARATION_SIGN_Y = 788;
	int DECLARATION_SIGN_DATE_X = 227;
	int DECLARATION_SIGN_DATE_Y = 788;
	String DECLARATION_SIGN_DATE = "06.12.2017";
	int DECLARATION_SIGN_PLACE_X = 397;
	int DECLARATION_SIGN_PLACE_Y = 788;
	String DECLARATION_SIGN_PLACE = "BANGLORE";
	
	
	

	public static void main(String[] args) throws IOException, DocumentException {
		File file = new File(DEST);
		file.getParentFile().mkdirs();

		EcafVo ecafVo = new EcafVo();

		new GenerateEcafPDF().manipulatePdf(SRC, DEST, ecafVo);

	}

	public void manipulatePdf(String src, String dest, EcafVo ecafVo) {
		
		try {
			
			PdfReader reader = new PdfReader(src);
			
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
			
			PdfContentByte contentOfFirstPage = stamper.getOverContent(1);
			renderingDataInFirstPage(stamper, contentOfFirstPage, ecafVo);
			
			PdfContentByte contentOfSecondPage = stamper.getOverContent(2);
			renderingDataInSecondPage(stamper, contentOfSecondPage, ecafVo);
			
			PdfContentByte contentOfSexthPage = stamper.getOverContent(6);
			renderingDataInSixthPage(stamper, contentOfSexthPage , ecafVo);

			stamper.close();
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void renderingDataInSixthPage(PdfStamper stamper, PdfContentByte contentOfSexthPage, EcafVo ecafVo) {
		
		try {
			addCustomerSignature(stamper, contentOfSexthPage ,DECLARATION_SIGN_X,DECLARATION_SIGN_Y);
			addText(contentOfSexthPage, DECLARATION_SIGN_DATE, DECLARATION_SIGN_DATE_X, DECLARATION_SIGN_DATE_Y);
			addText(contentOfSexthPage, DECLARATION_SIGN_PLACE, DECLARATION_SIGN_PLACE_X, DECLARATION_SIGN_PLACE_Y);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**@author kiran
	 * void
	 * @param stamper
	 * @param contentOfFirstPage
	 * @param ecafVo
	 */
	private void renderingDataInSecondPage(PdfStamper stamper, PdfContentByte contentOfSecondPage, EcafVo ecafVo) {
		
		try {
			
			addText(contentOfSecondPage, TARIFF_PLAN, TARIFF_PLAN_X, TARIFF_PLAN_Y);
			addTick(contentOfSecondPage, stamper, TARIFF_PLAN_DURATION_1_X, TARIFF_PLAN_DURATION_1_Y);
			addTick(contentOfSecondPage, stamper, TARIFF_PLAN_DURATION_3_X, TARIFF_PLAN_DURATION_3_Y);
			addTick(contentOfSecondPage, stamper, TARIFF_PLAN_DURATION_6_X, TARIFF_PLAN_DURATION_6_Y);
			addTick(contentOfSecondPage, stamper, TARIFF_PLAN_DURATION_12_X, TARIFF_PLAN_DURATION_12_Y);
			addTick(contentOfSecondPage, stamper, ROUTER_YES_X, ROUTER_YES_Y);
			addTick(contentOfSecondPage, stamper, ROUTER_NO_X, ROUTER_NO_Y);
			addText(contentOfSecondPage, INSTALLATION_CHARGES, INSTALLATION_CHARGES_X, INSTALLATION_CHARGES_Y);
			addText(contentOfSecondPage, TOTAL_CHARGES, TOTAL_CHARGES_X, TOTAL_CHARGES_Y);
			addTick(contentOfSecondPage, stamper, ONLINE_X, ONLINE_Y);
			addTick(contentOfSecondPage, stamper, CASH_X, CASH_Y);
			addTick(contentOfSecondPage, stamper, CHEQUE_X, CHEQUE_Y);
			addTick(contentOfSecondPage, stamper, DRAFT_X, DRAFT_Y);
			addCustomerSignature(stamper, contentOfSecondPage,SIGN_X, SIGN_Y);
			addText(contentOfSecondPage, SIGN_DATE, SIGN_DATE_X, SIGN_DATE_Y);
			addText(contentOfSecondPage, SIGN_PLACE, SIGN_PLACE_X, SIGN_PLACE_Y);
			addText(contentOfSecondPage, SECONDRY_TX, SECONDRY_TX_X, SECONDRY_TX_Y);
			addText(contentOfSecondPage, SECONDRY_TX_DATE, SECONDRY_TX_DATE_X, SECONDRY_TX_DATE_Y);
			addText(contentOfSecondPage, SECONDRY_TX_TIME, SECONDRY_TX_TIME_X, SECONDRY_TX_TIME_Y);
			addTick(contentOfSecondPage, stamper, HOME_X, HOME_Y);
			addTick(contentOfSecondPage, stamper, SOHO_X, SOHO_Y);
			addTick(contentOfSecondPage, stamper, CORPORATE_X, CORPORATE_Y);
			addTick(contentOfSecondPage, stamper, CYBER_CAFE_X, CYBER_CAFE_Y);
			addTick(contentOfSecondPage, stamper, INBOUND_X, INBOUND_Y);
			addTick(contentOfSecondPage, stamper, CALL_X, CALL_Y);
			addTick(contentOfSecondPage, stamper, REFERRAL_X, REFERRAL_Y);
			addTick(contentOfSecondPage, stamper, CAT5_X, CAT5_Y);
			addTick(contentOfSecondPage, stamper, FIBER_X, FIBER_Y);
			addText(contentOfSecondPage, SE_NAME, SE_NAME_X, SE_NAME_Y);
			addText(contentOfSecondPage, CFE_NAME, CFE_NAME_X, CFE_NAME_Y);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**@author kiran
	 * void
	 * @param stamper
	 * @param contentOfSecondPage
	 */
	private void addCustomerSignature(PdfStamper stamper, PdfContentByte contentOfSecondPage, int x ,int y) throws IOException, DocumentException {
		Image image = Image.getInstance(IMG);
		BufferedImage bimg = ImageIO.read(new File(IMG));
		int width = bimg.getWidth();
		int height = bimg.getHeight();

		System.out.println(width + "<<>>" + height);
		if (width > SIGN_MAX_WIDTH) {
			width = SIGN_MAX_WIDTH;
		}
		if (height > SIGN_MAX_HEIGHT) {
			height = SIGN_MAX_HEIGHT;
		}

		if (height == SIGN_MAX_HEIGHT || width == SIGN_MAX_WIDTH) {

			bimg = scaleImage(width, height, IMG);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bimg, "png", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			image = Image.getInstance(imageInByte);
		}

		PdfImage stream = new PdfImage(image, "", null);
		stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
		PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
		image.setDirectReference(ref.getIndirectReference());
		// image.setAbsolutePosition(80, 600);
		contentOfSecondPage.addImage(image, width, 0, 0, height, x,y);	
	}

	/**
	 * @author kiran void
	 * @param stamper
	 * @param contentOfFirstPage
	 * @param ecafVo
	 */
	private void renderingDataInFirstPage(PdfStamper stamper, PdfContentByte contentOfFirstPage, EcafVo ecafVo) {

		try {
			
			addCustomerPhoto(stamper, contentOfFirstPage);
			addText(contentOfFirstPage, CAF_NO, CAF_NO_X, CAF_NO_Y);
			addText(contentOfFirstPage, CREATION_DATE, CREATION_DATE_X, CREATION_DATE_Y);
			addText(contentOfFirstPage, PLACE, PLACE_X, PLACE_Y);
			addText(contentOfFirstPage, CUSTOMER_NAME, CUSTOMER_NAME_X, CUSTOMER_NAME_Y);
			addText(contentOfFirstPage, PRIMARY_TX, PRIMARY_TX_X, PRIMARY_TX_Y);
			addText(contentOfFirstPage, PRIMARY_TX_DATE, PRIMARY_TX_DATE_X, PRIMARY_TX_DATE_Y);
			addText(contentOfFirstPage, PRIMARY_TX_TIME, PRIMARY_TX_TIME_X, PRIMARY_TX_TIME_Y);
			addText(contentOfFirstPage, PROFESSION, PROFESSION_X, PROFESSION_Y);
			addText(contentOfFirstPage, CARE_OF, CARE_OF_X, CARE_OF_Y);
			addText(contentOfFirstPage, GENDER, GENDER_X, GENDER_Y);
			addText(contentOfFirstPage, AADHAR_ADDRESS, AADHAR_ADDRESS_X, AADHAR_ADDRESS_Y);
			addText(contentOfFirstPage, VTCNAME, VTCNAME_X, VTCNAME_Y);
			addText(contentOfFirstPage, AADHAR_STATE, AADHAR_STATE_X, AADHAR_STATE_Y);
			addText(contentOfFirstPage, AADHAR_PINCODE, AADHAR_PINCODE_X, AADHAR_PINCODE_Y);
			addText(contentOfFirstPage, AADHAR_MOBILE_NUMBER, AADHAR_MOBILE_NUMBER_X, AADHAR_MOBILE_NUMBER_Y);
			addText(contentOfFirstPage, ADDRESS, ADDRESS_X, ADDRESS_Y);
			addText(contentOfFirstPage, CITY, CITY_X, CITY_Y);
			addText(contentOfFirstPage, STATE, STATE_X, STATE_Y);
			addText(contentOfFirstPage, PINCODE, PINCODE_X, PINCODE_Y);
			addText(contentOfFirstPage, MOBILE_NUMBER, MOBILE_NUMBER_X, MOBILE_NUMBER_Y);
			addText(contentOfFirstPage, ALTERNATE_MOBILE_NUMBER, ALTERNATE_MOBILE_NUMBER_X, ALTERNATE_MOBILE_NUMBER_Y);
			addTick(contentOfFirstPage, stamper, DRIVING_X, DRIVING_Y);
			addTick(contentOfFirstPage, stamper, PASSPORT_X, PASSPORT_Y);
			addTick(contentOfFirstPage, stamper, PANCARD_X, PANCARD_Y);
			addTick(contentOfFirstPage, stamper, RATION_CARD_X, RATION_CARD_Y);
			addText(contentOfFirstPage, EMAIL_ADDRESS, EMAIL_ADDRESS_X, EMAIL_ADDRESS_Y);
			addTick(contentOfFirstPage, stamper, STATIC_IP_YES_X, STATIC_IP_YES_Y);
			addTick(contentOfFirstPage, stamper, STATIC_IP_NO_X, STATIC_IP_NO_Y);
			addTick(contentOfFirstPage, stamper, CX_PERMISSION_YES_X, CX_PERMISSION_YES_Y);
			addTick(contentOfFirstPage, stamper, CX_PERMISSION_NO_X, CX_PERMISSION_NO_Y);
			addText(contentOfFirstPage, D_1, D_1_X, D_1_Y);
			addText(contentOfFirstPage, D_2, D_2_X, D_2_Y);
			addText(contentOfFirstPage, M_1, M_1_X, M_1_Y);
			addText(contentOfFirstPage, M_2, M_2_X, M_2_Y);
			addText(contentOfFirstPage, Y_1, Y_1_X, Y_1_Y);
			addText(contentOfFirstPage, Y_2, Y_2_X, Y_2_Y);
			addText(contentOfFirstPage, Y_3, Y_3_X, Y_3_Y);
			addText(contentOfFirstPage, Y_4, Y_4_X, Y_4_Y);
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @author kiran void
	 * @param over
	 * @param stamper
	 * @param x
	 * @param y
	 */
	private void addTick(PdfContentByte over, PdfStamper stamper, int x, int y) throws IOException, DocumentException {
		Image image = Image.getInstance(TICK);
		BufferedImage bimg = ImageIO.read(new File(TICK));
		int width = bimg.getWidth();
		int height = bimg.getHeight();

		System.out.println(width + "<<>>" + height);
		if (width > TICK_MAX_WIDTH) {
			width = TICK_MAX_WIDTH;
		}
		if (height > TICK_MAX_HEIGHT) {
			height = TICK_MAX_HEIGHT;
		}

		if (height == TICK_MAX_HEIGHT || width == TICK_MAX_WIDTH) {

			bimg = scaleImage(width, height, TICK);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bimg, "png", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			image = Image.getInstance(imageInByte);
		}

		PdfImage stream = new PdfImage(image, "", null);
		stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
		PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
		image.setDirectReference(ref.getIndirectReference());
		over.addImage(image, width, 0, 0, height, x, y);
	}

	/**
	 * @author kiran void
	 * @param reader
	 * @param stamper
	 * @param over
	 */
	private void addCustomerPhoto(PdfStamper stamper, PdfContentByte over) throws IOException, DocumentException {
		Image image = Image.getInstance(IMG);
		BufferedImage bimg = ImageIO.read(new File(IMG));
		int width = bimg.getWidth();
		int height = bimg.getHeight();

		System.out.println(width + "<<>>" + height);
		if (width > PIC_MAX_WIDTH) {
			width = PIC_MAX_WIDTH;
		}
		if (height > PIC_MAX_HEIGHT) {
			height = PIC_MAX_HEIGHT;
		}

		if (height == PIC_MAX_HEIGHT || width == PIC_MAX_WIDTH) {

			bimg = scaleImage(width, height, IMG);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bimg, "png", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			image = Image.getInstance(imageInByte);
		}

		PdfImage stream = new PdfImage(image, "", null);
		stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
		PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
		image.setDirectReference(ref.getIndirectReference());
		// image.setAbsolutePosition(80, 600);
		over.addImage(image, width, 0, 0, height, IMAGE_X, IMAGE_Y);
	}

	public BufferedImage scaleImage(int WIDTH, int HEIGHT, String filename) {
		BufferedImage bi = null;
		try {
			ImageIcon ii = new ImageIcon(filename);// path to image
			bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = (Graphics2D) bi.createGraphics();
			g2d.addRenderingHints(
					new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
			g2d.drawImage(ii.getImage(), 0, 0, WIDTH, HEIGHT, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return bi;
	}

	public static void addText(PdfContentByte cb, String text, int x, int y) {

		try {

			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, 8);
			cb.showText(text);
			cb.endText();
			cb.restoreState();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
