 /**
 * @Author aditya  27-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.test;

 /**
 * @Author aditya  27-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class TestClass 
{

	private String name;
	private String address;
	private int age;
	/**
	 * @param name
	 * @param address
	 * @param age
	 */
	public TestClass(String name, String address, int age) {
		super();
		this.name = name;
		this.address = address;
		this.age = age;
	}
	/**
	 * 
	 */
	public TestClass() {
		super();
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TestClass [name=" + name + ", address=" + address + ", age=" + age + "]";
	}
	
}
