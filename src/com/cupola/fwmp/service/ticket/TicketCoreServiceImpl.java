package com.cupola.fwmp.service.ticket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FlowAttribute;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.FWMPConstant.UserPreferenceType;
import com.cupola.fwmp.controller.integ.CRMUpdateRequestHandler;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticket.TicketDetailDAO;
import com.cupola.fwmp.dao.ticket.app.AppTicketSummaryCache;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAOImpl;
import com.cupola.fwmp.dao.ticketReassignmentLog.TicketReassignmentLogDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.FrTicketEtr;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.closeticket.MQSCloseTicketService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.fr.FrTicketLockHelper;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.integ.crm.CrmServiceCall;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.ticket.counts.TicketCountCache;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreService;
import com.cupola.fwmp.service.user.UserPreference;
import com.cupola.fwmp.service.user.UserPreferenceService;
import com.cupola.fwmp.service.userNotification.UserNotificationCoreService;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.DBStatus;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.CafVo;
import com.cupola.fwmp.vo.ETRUpdatebyNE;
import com.cupola.fwmp.vo.GxCxPermission;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.NotificationOptions;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.RemarksVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.TicketEtrVo;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;
import com.cupola.fwmp.vo.TicketVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.fr.AppResponseVo;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;
import com.cupola.fwmp.vo.tools.CRMFrLogger;
import com.cupola.fwmp.vo.tools.CRMProspectLogger;
import com.cupola.fwmp.vo.tools.CRMWorkOrderLogger;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckResponse;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckVO;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.CreateUpdateFaultRepairResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.FWMPSRUpsert_Output;
import com.cupola.fwmp.vo.tools.siebel.from.vo.prospect.ProspectResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.FaultRepairActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.CreateUpdateFaultRepairRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.FWMPSRUpsert_Input;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ListOfFwmpsrthinio;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ListOfServiceRequest_Lightweight_ACTPriotization;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ServiceRequest_Lightweight;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ServiceRequest_Lightweight_ACTPriotization;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TicketCoreServiceImpl implements TicketCoreService
{

	private Logger log = Logger
			.getLogger(TicketCoreServiceImpl.class.getName());

	private static Map<Long, String> etrUpdateCache = new HashMap<Long, String>();

	@Autowired
	TicketDAO ticketDao;
	@Autowired
	UserPreferenceService userPreferenceService;
	@Autowired
	TicketDetailDAO ticketDetailDao;

	@Autowired
	UserDao userDao;

	@Autowired
	WorkOrderDAO workOrderDao;

	@Autowired
	TicketActivityLogDAOImpl ticketActivityLog;

	@Autowired
	TabletDAO tabletDAOImpl;

	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	AreaDAO areaDao;
	@Autowired
	BranchDAO branchDAO;

	@Autowired
	UserNotificationCoreService userNotification;

	@Autowired
	TicketDetailDAO ticketDetailDAO;

	@Autowired
	TicketActivityLogCoreService ticketActivityLogCoreService;

	@Autowired
	private MQSCloseTicketService closeTicketCoreService;

	@Autowired
	private FrTicketLockHelper frHelper;
	
	@Autowired
	private FrTicketDao frDao;
	
	@Autowired
	TicketReassignmentLogDAO ticketReassignmentLogDAO;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;
	
	@Autowired
	FrTicketService frTicketService;
	
	@Autowired
	SalesCoreServcie salesCoreServcie;
	
	@Autowired
	CrmServiceCall crmServiceCall;
	
	@Autowired
	DefinitionCoreService definitionCoreService;
	

	public void init()
	{

		log.info("TicketCoreServiceImpl init method called..");
		etrUpdateCache = ticketDao.getAllETRPEndingEntries();
		log.info("TicketCoreServiceImpl init method executed.");
	}

	public void setTicketDao(TicketDAO ticketDao)
	{
		this.ticketDao = ticketDao;
	}

	@Override
	public APIResponse addTicket(Ticket Ticket)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getTicketById(Long id)
	{
		return null;

	}

	@Override
	public APIResponse getAllTicket()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse deleteTicket(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateTicket(Ticket Ticket)
	{

		return null;
	}

	@Override
	public APIResponse getTickets(TicketFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(ticketDao.getTickets(filter));
	}

	@Override
	public APIResponse getTicketWithDetails(TicketFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(ticketDetailDao.getTicketWithDetails(filter));
	}

	@Override
	public APIResponse updateTicketEtrByNEorTL(ETRUpdatebyNE etrUpdatebyNE)
	{

		log.info(etrUpdatebyNE + "ETRUpdatebyNE");

		if (etrUpdatebyNE == null)
		{
			log.info("found ETRUpdatebyNE null");
			return ResponseUtil.nullArgument()
					.setData("found ETRUpdatebyNE null:");
		}
		if (etrUpdatebyNE.getTicketId() == null
				|| etrUpdatebyNE.getTicketId().isEmpty())
		{
			log.info("found ticketId null");
			return ResponseUtil.nullArgument().setData("found ticketId null:");
		}

		if (etrUpdatebyNE.getNewETR() == null
				|| etrUpdatebyNE.getNewETR().isEmpty())
		{
			log.info("found new TicketEtr null:");
			return ResponseUtil.nullArgument()
					.setData("found new TicketEtr null:");
		}

		Integer count = 0;
		Long userId = AuthUtils.getCurrentUserId();
		int role = AuthUtils.getCurrentUserRole();
		TicketEtrVo etr = new TicketEtrVo();

		etr.setNewETR(GenericUtil
				.converDateFromString(etrUpdatebyNE.getNewETR()));

		etr.setReasonCode(etrUpdatebyNE.getReasonCode());
		etr.setTicketId(Long.valueOf(etrUpdatebyNE.getTicketId().trim()));
		etr.setRemarks(etrUpdatebyNE.getRemarks());
		etr.setEtrMode(FWMPConstant.TicketStatus.ETR_UPDATE_MANUAL_MODE);
		etr.setUserId(userId);
		etr.setId(StrictMicroSecondTimeBasedGuid.newGuid());

		log.info("role id:" + role);

		if (role == UserRole.NE_NI)
		{
			log.info("found role NE::");
			count = etrUpdateByNE(etr);

			if (count >= FWMPConstant.COUNT_ETR)
			{
				log.info("Permission denied update Ticket CurrentETR more than "
						+ FWMPConstant.COUNT_ETR + "  Times");
				// return ResponseUtil.createSuccessResponse().setData("ETR
				// update Exceding the limit");
				return ResponseUtil.createETRLimitREachedResponse();
			}

		}

		else
		{
			log.info("found role TL::");
			count = etrUpdateByTL(etr);
		}

		if (count > 0)
		{
			TicketLog ticketLog = new TicketLogImpl(Long.valueOf(etrUpdatebyNE
					.getTicketId()), TicketUpdateConstant.NEW_PROSPECT_CREATED, AuthUtils
							.getCurrentUserId());
			ticketLog.setActivityValue(etrUpdatebyNE.getNewETR());
			TicketUpdateGateway.logTicket(ticketLog);

			// return ResponseUtil.createSuccessResponse().setData("ETR updated
			// sucessfully");
			
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				workOrderCoreService.updateWorkOrderActivityInCRM(
						etrUpdatebyNE.getTicketId() == null ? null : Long.valueOf(etrUpdatebyNE.getTicketId()), null, null, null,
						null, null, TicketUpdateConstant.ETR_UPDATED,null);
			}
			

			return ResponseUtil.createETRSuccessResponse();

		}

		return ResponseUtil.createUpdateFailedResponse()
				.setData("May be ticket not found as id:"
						+ etrUpdatebyNE.getTicketId());

	}

	// update TicketEtr By NE
	public int etrUpdateByNE(TicketEtrVo etr)
	{
		int count = 0;

		count = ticketDao.getCountEtr(etr.getTicketId());
		log.info("Ticket etr updated " + count + " times");

		if (count >= FWMPConstant.COUNT_ETR)
		{
			return count;
		}

		etr.setEtrMode(FWMPConstant.TicketStatus.ETR_UPDATE_MANUAL_MODE);
		int etrNECount = ticketDao.updateEtr(etr);

		if (etrNECount > 0)
		{
			if (count <= 0)
			{
				count = 1;
				etr.setCount(count);
				count = ticketDao.addTicketEtr(etr);
			}

			else
			{
				count = count + 1;
				etr.setCount(count);
				count = ticketDao.updateTicketEtr(etr);
			}
		}
		return etrNECount;
	}

	// update TicketEtr By TL
	public int etrUpdateByTL(TicketEtrVo etr)
	{

		int etrTLCount = 0;

		etrTLCount = ticketDao.updateEtr(etr);
		return etrTLCount;
	}

	@Override
	public String isETRApprovalPending(Long ticketId)
	{
		return etrUpdateCache.get(ticketId);
	}

	@Override
	public List<Long> getAllETRApprovalTickets()
	{
		return new ArrayList<Long>(etrUpdateCache.keySet());
	}

	@Override
	public APIResponse getPreSaleTicketWithDetails(TicketFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(ticketDetailDao.getPreSaleTicketWithDetails(filter));
	}

	/*@Override
	public APIResponse assignTickets(AssignVO assignVO)
	{

		Long userId = AuthUtils.getCurrentUserId();
		List<Long> areaIds = new ArrayList<Long>();
		UserVo userVo = userDao.getUserById(assignVO.getAssignedToId());
		if (AuthUtils.isManager())
		{

			Set<TypeAheadVo> roles = userVo.getUserRoles();

			if (roles == null)
			{
				log.info("No Roles found for user "
						+ assignVO.getAssignedToId());
				return ResponseUtil.createAssignFailureResponse();
			}

			for (Iterator iterator = roles.iterator(); iterator.hasNext();)
			{
				TypeAheadVo typeAheadVo = (TypeAheadVo) iterator.next();

				if (UserRole.ROLE_AM.equals(typeAheadVo.getName())
						&& userVo.getArea() != null)
				{
					
					Set<TypeAheadVo>  allAreas = userVo.getArea();
					for(TypeAheadVo areaVO : allAreas)
					{
						areaIds.add(areaVO.getId());
					 
					}
					
				} else if (UserRole.ROLE_BM.equals(typeAheadVo.getName())
						&& userVo.getBranch() != null)
				{
					areaIds.addAll(areaDao
							.getAreasByBranchId(userVo.getBranch().getId() + "")
							.keySet());

				} else if (UserRole.ROLE_CM.equals(typeAheadVo.getName())
						&& userVo.getCity() != null)
				{
					Map<Long, String> branches = branchDAO
							.getBranchesByCityId(userVo.getCity().getId() + "");

					areaIds.addAll(areaDao
							.getAreasByBranchIds(new ArrayList<Long>(branches
									.keySet()))
							.keySet());
				}
			}

		}
		Long areaId = null;

		if (areaIds != null && areaIds.size() > 0)
		{
			areaId = areaIds.get(0);

		} else
		{
			String recipientUserKey = dbUtil
					.getKeyForQueueByUserId(assignVO.getAssignedToId());
			if (!AuthUtils.isFRUser())
			{
				globalActivities
						.addTicket2MainQueue(recipientUserKey, new LinkedHashSet<Long>(assignVO
								.getTicketId()));
			} else
			{
				globalActivities
						.addFrTicket2MainQueue(recipientUserKey, new LinkedHashSet<Long>(assignVO
								.getTicketId()));
			}

			if(userVo.getArea() != null && userVo.getArea().size() > 0)
			areaId = userVo.getArea() != null ? userVo.getArea().iterator().next().getId() : null;

			List<BigInteger> currentAssignTo = ticketDao
					.getCurrentAssignedForTickets(assignVO.getTicketId());

			if (currentAssignTo != null)
			{
				for (BigInteger ticketNos : currentAssignTo)
				{
					if (ticketNos == null)
						continue;
					Long longTic = ticketNos.longValue();

					String sourceKey = dbUtil.getKeyForQueueByUserId(longTic);

					if (!AuthUtils.isFRUser())
					{
						globalActivities
								.removeBulkTicketsFromMainQueueByKey(sourceKey, new LinkedHashSet<Long>(assignVO
										.getTicketId()));
					} else
					{
						globalActivities
								.removeBulkFrTicketsFromMainQueueByKey(sourceKey, new LinkedHashSet<Long>(assignVO
										.getTicketId()));
					}

					long sourceTlId = userDao.getReportToUser(longTic);

					if (sourceTlId > 0)
					{
						String sourceTlKey = dbUtil
								.getKeyForQueueByUserId(sourceTlId);

						if (!AuthUtils.isFRUser())
						{
							globalActivities
									.removeBulkTicketsFromMainQueueByKey(sourceTlKey, new LinkedHashSet<Long>(assignVO
											.getTicketId()));
						} else
						{
							globalActivities
									.removeBulkFrTicketsFromMainQueueByKey(sourceTlKey, new LinkedHashSet<Long>(assignVO
											.getTicketId()));
						}

					}

				}
			}

			long recipientTlId = userDao
					.getReportToUser(assignVO.getAssignedToId());

			if (recipientTlId > 0)
			{
				String recipientTlKey = dbUtil
						.getKeyForQueueByUserId(recipientTlId);

				if (!AuthUtils.isFRUser())
				{
					globalActivities
							.addTicket2MainQueue(recipientTlKey, new LinkedHashSet<Long>(assignVO
									.getTicketId()));
				} else
				{
					globalActivities
							.addFrTicket2MainQueue(recipientTlKey, new LinkedHashSet<Long>(assignVO
									.getTicketId()));
				}

			}
		}

		assignVO.setAssignedById(userId);
		ticketDao.assignTickets(assignVO, areaId);
		if (assignVO.getWorkOrderNo() != null
				&& assignVO.getWorkOrderNo().size() > 0)
			workOrderDao.updateAssignedTo(assignVO);
		List<TicketLog> ticketLogs = new ArrayList<TicketLog>();

		for (Long id : assignVO.getTicketId())
		{

			TicketLog ticketLog = new TicketLogImpl(id, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED, AuthUtils
					.getCurrentUserId());
			ticketLogs.add(ticketLog);
		}
		TicketUpdateGateway.logTickets(ticketLogs);

		return ResponseUtil.createUpdateSuccessResponse();
	}*/
	
	@Override
	public APIResponse assignTickets(AssignVO assignVO)
	{

		Long userId = AuthUtils.getCurrentUserId();
		{

//			List<BigInteger> currentAssignTo = ticketDao
//					.getCurrentAssignedForTickets(assignVO.getTicketId());

			
			List<TicketReassignmentLogVo> result =	ticketDao.getCurrentAssignedForTicketsWithIds(assignVO.getTicketId());
			
			if (result != null && !result.isEmpty())
			{
				TicketReassignmentLogVo ticketReassignmentLogVo = null;
				for (TicketReassignmentLogVo currentUser : result)
				{
					if (currentUser == null)
						continue;

					if(currentUser.getAssignedFrom() == null)
						continue;

					Long currentUserID = currentUser.getAssignedFrom();
					
					String sourceKey = dbUtil.getKeyForQueueByUserId(currentUserID);

					if (!AuthUtils.isFRUser())
					{
//						System.out.println("Tckt to remove are " + assignVO
//										.getTicketId());
						
						globalActivities
								.removeBulkTicketsFromMainQueueByKey(sourceKey, new LinkedHashSet<Long>(assignVO
										.getTicketId()));
						
						ticketReassignmentLogVo = updateTicketReassignmentLog(currentUser.getTicketId(), currentUserID, assignVO.getAssignedToId(), "SD");
						
					} else
					{
						globalActivities
								.removeBulkFrTicketsFromMainQueueByKey(sourceKey, new LinkedHashSet<Long>(assignVO
										.getTicketId()));
						
						ticketReassignmentLogVo = updateTicketReassignmentLog(currentUser.getTicketId(), currentUserID, assignVO.getAssignedToId(), "FR");
					}
					
					log.info("ticketReassignmentLogVo " + ticketReassignmentLogVo);

					long sourceTlId = userDao.getReportToUser(currentUserID);

					if (sourceTlId > 0)
					{
						String sourceTlKey = dbUtil
								.getKeyForQueueByUserId(sourceTlId);

						if (!AuthUtils.isFRUser())
						{
							globalActivities
									.removeBulkTicketsFromMainQueueByKey(sourceTlKey, new LinkedHashSet<Long>(assignVO
											.getTicketId()));
						} else
						{
							globalActivities
									.removeBulkFrTicketsFromMainQueueByKey(sourceTlKey, new LinkedHashSet<Long>(assignVO
											.getTicketId()));
						}

					}

				}
			}
			
			
			
			String recipientUserKey = dbUtil
					.getKeyForQueueByUserId(assignVO.getAssignedToId());
			if (!AuthUtils.isFRUser())
			{
				globalActivities
						.addTicket2MainQueue(recipientUserKey, new LinkedHashSet<Long>(assignVO
								.getTicketId()));
			} else
			{
				globalActivities
						.addFrTicket2MainQueue(recipientUserKey, new LinkedHashSet<Long>(assignVO
								.getTicketId()));
			}

			long recipientTlId = userDao
					.getReportToUser(assignVO.getAssignedToId());

			if (recipientTlId > 0)
			{
				String recipientTlKey = dbUtil
						.getKeyForQueueByUserId(recipientTlId);

				if (!AuthUtils.isFRUser())
				{
					globalActivities
							.addTicket2MainQueue(recipientTlKey, new LinkedHashSet<Long>(assignVO
									.getTicketId()));
				} else
				{
					globalActivities
							.addFrTicket2MainQueue(recipientTlKey, new LinkedHashSet<Long>(assignVO
									.getTicketId()));
				}

			}
		}

		assignVO.setAssignedById(userId);
		ticketDao.assignTickets(assignVO, null);
		if (assignVO.getWorkOrderNo() != null
				&& assignVO.getWorkOrderNo().size() > 0)
			workOrderDao.updateAssignedTo(assignVO);
		List<TicketLog> ticketLogs = new ArrayList<TicketLog>();

		for (Long id : assignVO.getTicketId())
		{

			TicketLog ticketLog = new TicketLogImpl(id, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED, AuthUtils
					.getCurrentUserId());
			Map<String,String> additionalAttributeMap = new HashMap<String,String>();
			additionalAttributeMap.put(TicketUpdateConstant.ASSIGNED_TO_KEY,assignVO.getAssignedToId()+"");
			ticketLog.setAdditionalAttributeMap(additionalAttributeMap);
			ticketLogs.add(ticketLog);
			
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				if (AuthUtils.isNEUser()) {
					workOrderCoreService.updateWorkOrderActivityInCRM(id, null, null, null, null, null,
							TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);
				} else if (AuthUtils.isSalesUser()){
					
					salesCoreServcie.updateSalesActivityInCRM(id,  null  , null);
				}
			}
			
			
		}
		TicketUpdateGateway.logTickets(ticketLogs);

		return ResponseUtil.createUpdateSuccessResponse();
	}
	
	

	@Override
	public APIResponse searchTickets(TicketFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(ticketDetailDao.searchTicketWithDetails(filter));
	}

	@Override
	public APIResponse updateTicketCaf(CafVo cafVo)
	{
		Long userId = AuthUtils.getCurrentUserId();
		if (userId == null)
		{
			log.info("user not found");
			return ResponseUtil.createLoginFailureResponse("user not found");
		}

		Integer num = ticketDao.updateTicketCaf(cafVo, userId);
		if (num <= 0)
		{
			log.info("caf not updated as ticketId:" + cafVo.getTicketId());
			ResponseUtil.createUpdateFailedResponse()
					.setData("caf not updated as ticketId:"
							+ cafVo.getTicketId());
		}

		return ResponseUtil.createSuccessResponse()
				.setData("updated successfuly");

	}

	@Override
	public APIResponse getCountSummary()
	{

		return ResponseUtil.createSuccessResponse().setData(TicketCountCache
				.getCountSummary(AuthUtils.getCurrentUserId()));
	}

	@Override
	public APIResponse getTicketCount()
	{
		UserPreference uf = userPreferenceService
				.getUserPreferencesByPreferenceName(AuthUtils
						.getCurrentUserId(), UserPreferenceType.NOTIFICATION_BAR);

		ObjectMapper mapper = new ObjectMapper();

		try
		{
			if (uf == null || uf.getPrefStringValue() == null)
			{

				log.info("Notification preference not configured ..... for user "
						+ AuthUtils.getCurrentUserLoginId());
				log.info("Creating prefeneces for user :: "
						+ AuthUtils.getCurrentUserLoginId());
				NotificationOptions options = (NotificationOptions) userPreferenceService
						.getNotificationOption().getData();
				userPreferenceService
						.createOrUpdateNotificationPreference(options);
				uf = userPreferenceService
						.getUserPreferencesByPreferenceName(AuthUtils
								.getCurrentUserId(), UserPreferenceType.NOTIFICATION_BAR);
			}

			Map<Integer, String> statusList = new HashMap<Integer, String>();
			Map<Long, String> initialWorkStages = new HashMap<Long, String>();
			Map<Long, String> tTypes = new HashMap<Long, String>();

			Map<Long, String> activities = new HashMap<Long, String>();

			if (uf != null && uf.getPrefStringValue() != null)
			{
				NotificationOptions options = mapper.readValue(uf
						.getPrefStringValue(), NotificationOptions.class);

				for (TypeAheadVo tvo : options.getStatus())
				{
					statusList.put(tvo.getId().intValue(), tvo.getName());

				}
				for (TypeAheadVo tvo : options.getTypes())
				{
					tTypes.put(tvo.getId(), tvo.getName());
				}
				for (TypeAheadVo tvo : options.getStages())
				{
					initialWorkStages.put(tvo.getId(), tvo.getName());

				}
				for (TypeAheadVo tvo : options.getActivities())
				{
					activities.put(tvo.getId(), tvo.getName());

				}

				return ResponseUtil.createSuccessResponse().setData(ticketDao
						.getCounts(statusList, initialWorkStages, tTypes, activities));
			} else
			{

			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return ResponseUtil.createFailureResponse();

	}

	/*
	 * @Override public APIResponse assignTickets(AssignVO assignVO) { // TODO
	 * Auto-generated method stub return null; }
	 */

	@Override
	public APIResponse updateRemarks(RemarksVO remark)
	{
		int result = ticketDao.updateRemarks(remark);
		TicketVo ticket = new TicketVo(remark
				.getTicketId(), TicketUpdateConstant.REMARK_ADDED);
		ticket.setUpdatedBy(AuthUtils.getCurrentUserId());
		ticket.setCurrentActivityValue(remark.getRemark());

		TicketLog ticketLog = new TicketLogImpl(remark
				.getTicketId(), TicketUpdateConstant.REMARK_ADDED, AuthUtils
						.getCurrentUserId());
		ticketLog.setActivityValue(remark.getRemark());
		TicketUpdateGateway.logTicket(ticketLog);
		return ResponseUtil.createSaveSuccessResponse().setData(result);
	}

	@Override
	public APIResponse reassignTicket(ReassignTicketVo reassignTicketVo)
	{
		if (reassignTicketVo == null)
			return ResponseUtil.createNullParameterResponse();

		if (reassignTicketVo.getTicketIds() == null)
			return ResponseUtil
					.createNullParameterResponse("Ticket IDs could not be null");

		if (reassignTicketVo.getRecipientUserId() == null)
			return ResponseUtil
					.createNullParameterResponse("RecipientUserId IDs could not be null");

		log.info("Reassiging tickets request " + reassignTicketVo);

//		List<BigInteger> currentAssignTo = ticketDao
//				.getCurrentAssignedForTickets(reassignTicketVo.getTicketIds());
		
		List<TicketReassignmentLogVo> ticketReassignmentLogVos =	ticketDao.getCurrentAssignedForTicketsWithIds(reassignTicketVo.getTicketIds());

		LinkedHashSet<Long> ticketNumberList = new LinkedHashSet<>(reassignTicketVo
				.getTicketIds());

		long recipientUserId = Long
				.valueOf(reassignTicketVo.getRecipientUserId());

		String recipientUserKey = dbUtil
				.getKeyForQueueByUserId(recipientUserId);

		UserVo userVo = userDao.getUserById(Long
				.valueOf(reassignTicketVo.getRecipientUserId()));

		Set<TypeAheadVo> roles = userVo.getUserRoles();

		log.info("Roles are " + roles);

		int ticketStatus = 0;

		List<Long> areaIds = new ArrayList<Long>();

		if (AuthUtils.isSalesUser())
		{
			ticketStatus = TicketStatus.FEASIBILITY_PENDING_BY_NE;
			log.info("Inside sales roles " + ticketStatus);

		} else if (AuthUtils.isNIUser())
		{
			ticketStatus = TicketStatus.PERMISSION_PENDING_BY_SALES;
			log.info("Inside deployment roles " + ticketStatus);
		}

		Long areaId = null;

		if (areaIds.size() > 0)
		{
			areaId = areaIds.get(0);

		} else
		{
			globalActivities
					.addTicket2MainQueue(recipientUserKey, ticketNumberList);

			long recipientTlId = userDao.getReportToUser(recipientUserId);

			if (recipientTlId > 0)
			{
				String recipientTlKey = dbUtil
						.getKeyForQueueByUserId(recipientTlId);

				log.info("Adding ticket " + ticketNumberList + " for user "
						+ recipientUserKey);

				globalActivities
						.addTicket2MainQueue(recipientTlKey, ticketNumberList);

			}

		}

		log.info("Current assign to are " + ticketReassignmentLogVos);

		reassignTicketVo.setStatus(ticketStatus);

		int result = ticketDao.reassignTicket(reassignTicketVo, areaId);

		log.info("Total updated rows are " + result);

//		 for (BigInteger ticketNos : currentAssignTo)
//		{
//			Long longTic = ticketNos.longValue();
//
//			String sourceKey = dbUtil.getKeyForQueueByUserId(longTic);
//
//			log.info("Remove ticket " + sourceKey + " for user "
//					+ recipientUserKey);
//
//			globalActivities
//					.removeBulkTicketsFromMainQueueByKey(sourceKey, ticketNumberList);
//
//			long sourceTlId = userDao.getReportToUser(longTic);
//
//			if (sourceTlId > 0)
//			{
//				String sourceTlKey = dbUtil.getKeyForQueueByUserId(sourceTlId);
//
//				globalActivities
//						.removeBulkTicketsFromMainQueueByKey(sourceTlKey, ticketNumberList);
//
//			}
//		}

		List<TicketLog> ticketLogs = new ArrayList<TicketLog>();

		for (Long id : ticketNumberList)
		{
			TicketLog ticketLog = new TicketLogImpl(id, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED, AuthUtils
					.getCurrentUserId());
			Map<String,String> additionalAttributeMap = new HashMap<String,String>();
			additionalAttributeMap.put(TicketUpdateConstant.ASSIGNED_TO_KEY,recipientUserId+"");
			ticketLog.setAdditionalAttributeMap(additionalAttributeMap);
			ticketLogs.add(ticketLog);

			ticketDao.updateReassignment(id, recipientUserId, ticketStatus);

			if (ticketStatus == TicketStatus.FEASIBILITY_PENDING_BY_NE)
				ticketDao
						.updateTicketSubCategory(id, TicketSubCategory.FISIBLE, TicketSubCategory.FEASIBILITY);

			else if (ticketStatus == TicketStatus.PERMISSION_PENDING_BY_SALES)
				ticketDao
						.updateTicketSubCategory(id, TicketSubCategory.PERMISSION, TicketSubCategory.PERMISSION_ID);

			APIResponse response = updateTicketActivityLogVo(Activity.BASIC_INFO_UPDATE
					+ "", id + "", FWMPConstant.TicketStatus.COMPLETED + "");
			
			if( AuthUtils.isSalesUser()) {
				String currentCRM = dbUtil.getCurrentCrmName(null, null);

				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
					salesCoreServcie.updateSalesActivityInCRM(id,  null  , null);
				}
			}
			
			log.info("Comming out of Basic update dao and total activity log entry are "
					+ response);
		}

		TicketUpdateGateway.logTickets(ticketLogs);

		return ResponseUtil.createSuccessResponse()
				.setData(ResponseUtil.createSuccessResponse().getStatusMessage());
	}

	@Override
	public APIResponse updateGxCXPermission(GxCxPermission gxCxPermission)
	{

		int response = 0;
		if (Long.valueOf(gxCxPermission.getTicketId()) > 0 && ((gxCxPermission
				.getGxPermission() != null
				&& !gxCxPermission.getGxPermission().isEmpty())
				|| (gxCxPermission.getCxPermission() != null
						&& !gxCxPermission.getCxPermission().isEmpty())))
		{
			response = ticketDao.updateGxCXPermission(gxCxPermission);
		}

		if (response > 0)
		{
			return ResponseUtil.createUpdateSuccessResponse().setData(response);
		}
		return ResponseUtil.createUpdateFailedResponse().setData(response);

	}

	@Override
	public APIResponse getCountSummaryGropedByNE(long initialWorkStagetype)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(AppTicketSummaryCache
						.getCountSummaryGropedByNE(initialWorkStagetype));
	}

	@Override
	public APIResponse autoClosureStatusUpdateToUser(Long ticketNo,
			MessageVO vo)
	{
		log.info("send notification to userId as ticketNo:" + ticketNo);

		List<Long> listOfuserId = null;
		try
		{
			listOfuserId = userNotification.getUserIdByTicketNumber(ticketNo);

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		if (listOfuserId == null || listOfuserId.isEmpty())
		{
			log.info("not found userId  as TicketNo:" + ticketNo);
			return ResponseUtil.createSuccessResponse()
					.setData("not found  userId TicketNo:" + ticketNo);
		}

		try
		{
			userNotification.sendGCmNotificationToUser(listOfuserId, vo);
		} catch (Exception e)
		{
			e.printStackTrace();
			return ResponseUtil.createFailureResponse();
		}

		return ResponseUtil.createSuccessResponse();
	}

	@Override
	public APIResponse calculateDashBoardStatus(Long ticketType)
	{

		return ResponseUtil.createSuccessResponse()
				.setData(ticketDao.calculateDashBoardStatus(ticketType));
	}

	@Override
	public int updatePriority(long ticketNo, Integer escalatedValue,
			boolean modificationType)
	{
		return ticketDao
				.updatePriority(ticketNo, escalatedValue, modificationType);
	}

	public APIResponse updateTicketDefectCode(long ticketId, long defectCode,
			long subDefectCode, long status)
	{
		int rows = ticketDao
				.updateDefectCode(ticketId, defectCode, subDefectCode, status, null);
		if (rows != 0)
		{
			Long currentAssingedTo = AuthUtils.getCurrentUserId();
			String keyOfQueue = null;
			keyOfQueue = dbUtil.getKeyForQueueByUserId(currentAssingedTo);
			// globalActivities.removeFrTicketFromMainQueueByKeyAndTicketNumber(keyOfQueue,
			// ticketId);
			Long reportTo = userDao.getReportToUser(currentAssingedTo);
			if (reportTo != null)
			{
				keyOfQueue = dbUtil.getKeyForQueueByUserId(reportTo);
				// globalActivities.removeFrTicketFromMainQueueByKeyAndTicketNumber(keyOfQueue,
				// ticketId);
			}
			closeTicketCoreService.closeTicketInMQS(ticketId);
			return ResponseUtil.createSuccessResponse();
		} else
			return ResponseUtil.NoRecordFound();

	}

	public APIResponse updateTicketDefectCode(FRDefectSubDefectsVO resolutionCode)
	{
		if( resolutionCode == null )
		{
			log.info("Can't update defect code and sub defect code with status ...");
			return ResponseUtil.nullArgument();
		}
		
		try
		{
			FrTicket ticket = ticketDao.getFrTicketById(resolutionCode.getTicketId());
			if( ticket == null )
				return ResponseUtil.NoRecordFound()
						.setData(new AppResponseVo(0+""
								,"Invalid Ticket Number :"+resolutionCode.getTicketId()));
			
			/*City city = ticket.getCustomer() != null
					? ticket.getCustomer().getCity() : null;*/
			
			TypeAheadVo city = frDao.getCityNameForTicket(resolutionCode.getTicketId());
					
			String cityName = "";
			if( city != null )
				cityName = city.getName()!= null 
						&& city.getName().equalsIgnoreCase(CityName.HYDERABAD_CITY) 
						? city.getName() : "REST_OF_INDIA";
						
			log.info("********* Updating Defect sub defect code in local ***********");
						
			
			
			String currentCRM = dbUtil.getCurrentCrmName(null, null);
			
			APIResponse	mqResponse = null;
			
			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {
				
				TicketModifyInMQVo modityVo = new TicketModifyInMQVo();
				modityVo.setDefectCode(resolutionCode.getDefectCode());
				modityVo.setSubDefectCode(resolutionCode.getSubDefectCode());
				modityVo.setWorkOrderNumber(ticket.getWorkOrderNumber());
				modityVo.setCityName(cityName);
				modityVo.setTicketId(ticket.getId());
				
			   mqResponse = closeTicketCoreService.modifyFrTicket(modityVo);
				
				log.info("FR MQ COLOSURE RESPONSE : "+ mqResponse);
				
				TicketLog logDetail = new TicketLogImpl(ticket
						.getId(), TicketUpdateConstant.MQ_CLOSURE_REQUEST
								+ " Response Code " + mqResponse
										.getStatusCode(), resolutionCode.getModifiedBy());

				logDetail.setAddedOn(new Date());
				TicketUpdateGateway.logTicket(logDetail);
				
				if( mqResponse != null  && ( mqResponse.getStatusCode() == 0 
						|| mqResponse.getStatusCode() == 99070 ) )
					resolutionCode.setStatus(Long.valueOf(TicketStatus.CLOSED));
				else 
				{
//					resolutionCode.setStatus(Long.valueOf(TicketStatus.FR_MQ_CLOSURE_FAILED));
					return ResponseUtil.createSuccessResponse()
							.setData(new AppResponseVo(0+"","Mq closure failed. Please retry..."));
				}
				
				closeTicketInFWMP(ticket , resolutionCode , mqResponse);
				
			} else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				Long prioritySouceLong = new Long(ticket.getTicketPriority().getEscalationType());
				String prioritySouce = definitionCoreService.getEscalationTypeByKey(prioritySouceLong);
				ServiceRequest_Lightweight_ACTPriotization serviceRequest_Lightweight_ACTPriotization = new ServiceRequest_Lightweight_ACTPriotization();
				serviceRequest_Lightweight_ACTPriotization.setPrioritySource(prioritySouce);
				ListOfServiceRequest_Lightweight_ACTPriotization actPriotization = new ListOfServiceRequest_Lightweight_ACTPriotization();
				actPriotization.setServiceRequest_Lightweight_ACTPriotization(serviceRequest_Lightweight_ACTPriotization);
				ServiceRequest_Lightweight lightweight = new ServiceRequest_Lightweight();
				lightweight.setCustomerNumber(ticket.getCustomer().getMqId());
				lightweight.setSubResolutionCode(resolutionCode.getSubDefectCode());
				lightweight.setResolutionCode(resolutionCode.getDefectCode());
				if(ticket.getTicketDescriptionVoc() != null)
					lightweight.setTicketDescription(ticket.getTicketDescriptionVoc());
// commented by manjuprasad for Ticketdescription issue				
//				lightweight.setTicketDescription("update by tab");
				lightweight.setSRNumber(ticket.getWorkOrderNumber());
				Set<FrDetail> frDetails = ticket.getFrWorkOrders();
				if ( frDetails != null && frDetails.size() > 0 )		
				{
					FrDetail frDetail = frDetails.iterator().next();
					lightweight.setCategory(frDetail.getCategory());
					lightweight.setNature(frDetail.getSubCategory());
				}
				
				lightweight.setOwner(ticket.getUserByCurrentAssignedTo().getLoginId());
				lightweight.setPriority(ticket.getTicketPriority().getValue()+"");
				lightweight.setTicketSource(ticket.getSource());
				lightweight.setStatus(FWMPConstant.FrStatusForSiebel.COMPLETED);
				lightweight.setListOfServiceRequest_Lightweight_ACTPriotization(actPriotization);
				
				ListOfFwmpsrthinio fwmpsrthinio = new ListOfFwmpsrthinio();
				fwmpsrthinio.setServiceRequest_Lightweight(lightweight);
				
				FWMPSRUpsert_Input fwmpsrUpsert_Input = new FWMPSRUpsert_Input();
				fwmpsrUpsert_Input.setListOfFwmpsrthinio(fwmpsrthinio);
				
				CreateUpdateFaultRepairRequestInSiebel createUpdateFaultRepairRequestInSiebel = new CreateUpdateFaultRepairRequestInSiebel();
				createUpdateFaultRepairRequestInSiebel.setFWMPSRUpsert_Input(fwmpsrUpsert_Input);
				
				
				FaultRepairActivityUpdateInCRMVo activityUpdateInCRMVo = new FaultRepairActivityUpdateInCRMVo();
				activityUpdateInCRMVo.setCreateUpdateFaultRepairRequestInSiebel(createUpdateFaultRepairRequestInSiebel);
				activityUpdateInCRMVo.setFaultRepairActivityUpdateInCRMVoId(StrictMicroSecondTimeBasedGuid.newGuid());
				activityUpdateInCRMVo.setTransactionId(activityUpdateInCRMVo.getFaultRepairActivityUpdateInCRMVoId() + "");
				activityUpdateInCRMVo.getCreateUpdateFaultRepairRequestInSiebel().getFWMPSRUpsert_Input().setTransaction_spcID(activityUpdateInCRMVo.getFaultRepairActivityUpdateInCRMVoId() + "");
				
				APIResponse apiResponse = crmServiceCall.callSiebelServiceForCreateUpdateFaultRepair(activityUpdateInCRMVo);
				
				log.info("crm closer response :: "+apiResponse);
				
				CreateUpdateFaultRepairResponseFromSiebel response = null;
				
				if (apiResponse != null && apiResponse.getData() != null) {
					
					//ObjectMapper mapper = CommonUtil.getObjectMapper();

					//String stringResponse = apiResponse.getData().toString();
					
					//mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

					
					log.info("crm closer stringResponse :: "+apiResponse.getData());

					response =  ( CreateUpdateFaultRepairResponseFromSiebel ) apiResponse.getData();
					
					log.info("crm closer response@@@@@@@@ :: "+response);
					
					Integer status;
					try {
						status = Integer.valueOf(response.getFWMPSRUpsert_Output().getError_spcCode());
						if(status == 0)
						{
							frDao.updateFrTicketStatus(ticket.getId(), 
									Arrays.asList(Long.valueOf(TicketStatus.COMPLETED)));
							
							resolutionCode.setStatus(Long.valueOf(TicketStatus.CLOSED));
							closeTicketInFWMP(ticket , resolutionCode , mqResponse);
							
							TicketLog logDetail = new TicketLogImpl(ticket
									.getId(), TicketUpdateConstant.CRM_CLOSURE_REQUEST
											+ " Error_spcCode " + status, resolutionCode.getModifiedBy());

							logDetail.setAddedOn(new Date());
							TicketUpdateGateway.logTicket(logDetail);

							
						}
						else {
							
							TicketLog logDetail = new TicketLogImpl(ticket
									.getId(), TicketUpdateConstant.CRM_CLOSURE_REQUEST
											+ " Error_spcMessage " + response.getFWMPSRUpsert_Output().getError_spcMessage(), resolutionCode.getModifiedBy());

							logDetail.setAddedOn(new Date());
							TicketUpdateGateway.logTicket(logDetail);
							
							return ResponseUtil.createSuccessResponse()
									.setData(new AppResponseVo(0+"","CRM closure failed. Please retry..."));
						}
						
					} catch (Exception e) {
						
						TicketLog logDetail = new TicketLogImpl(ticket
								.getId(), TicketUpdateConstant.CRM_CLOSURE_REQUEST
										+ " Error_spcMessage " + response.getFWMPSRUpsert_Output().getError_spcMessage(), resolutionCode.getModifiedBy());

						logDetail.setAddedOn(new Date());
						TicketUpdateGateway.logTicket(logDetail);
						
						return ResponseUtil.createSuccessResponse()
								.setData(new AppResponseVo(0+"","CRM closure failed. Please retry..."));
					}
					
				}
				
			} 
						
			return ResponseUtil.createSuccessResponse()
					.setData(new AppResponseVo(resolutionCode.getStatus()+"","successfully closed"));
			
		} 
		catch (Exception e)
		{
			return ResponseUtil.createTicketClosureFailedInMQ();
		}
	}

	private void closeTicketInFWMP(FrTicket ticket, FRDefectSubDefectsVO resolutionCode, APIResponse mqResponse) {
		
		try {
			if ( ticket != null )
			{
				ticket.setDefectCode (resolutionCode.getDefectCode() );
				ticket.setSubDefectCode(resolutionCode.getSubDefectCode());
				ticket.setStatus(Integer.valueOf(resolutionCode.getStatus()+""));
				ticket.setRemarks(resolutionCode.getRemarks());
				
				long modifiedBy = resolutionCode.getModifiedBy() != null ? resolutionCode.getModifiedBy() : 0 ;
				UserVo engineer = userDao.getUserById( modifiedBy );
				String closedBy = "--";
				if( engineer != null )
				{
					if( modifiedBy == 1 )
						closedBy = FlowAttribute.SYMTEM_ASSIGNED;
					else
						closedBy = FrTicketUtil.getFullName( engineer.getFirstName(),null , engineer.getLastName() );
				}
				
				ticket.setClosedBy( closedBy );
				ticket.setModifiedOn(new Date());
				ticket.setModifiedBy(resolutionCode.getModifiedBy());
				if(mqResponse != null)
					ticket.setMqClosureCode( mqResponse.getData() != null ? mqResponse.getData().toString(): null );
				ticket.setTicketClosedTime(new Date());
				
				long userId = ticket.getUserByCurrentAssignedTo() != null 
						? ticket.getUserByCurrentAssignedTo().getId() : AuthUtils.getCurrentUserId();
				
				/*
				 * if needed send notification to customer vis message 
				 * then write write code for that here
				 * 
				 * */
						
				Set<FrTicketEtr> etrDb = ticket.getFrEtr();		
				if ( etrDb != null && etrDb.size() > 0 )		
				{
					FrTicketEtr dbVo = etrDb.iterator().next();
					dbVo.setCurrentProgress("100");
					frDao.updateFrTicketEtr(dbVo);
				}
				frDao.updateReopneTicket(ticket);
				log.info("FR MQ COLOSURE RESPONSE STATUS : "+ resolutionCode.getStatus());
				frHelper.manageUserLock(userId);
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public APIResponse getTicketStatusById(Long ticketId)
	{
		if (ticketId == null)
			return null;
		return ResponseUtil.createSuccessResponse()
				.setData(ticketDao.getTicketStatusById(ticketId));
	}

	@Override
	public TicketReassignmentLogVo updateTicketReassignmentLog( Long ticketId, Long assignedFrom, Long assignedTo,
			  String ticketCategory)
	{
		log.info("Updating TicketReassignmentLog for ticketId " + ticketId + " assignedFrom " + assignedFrom
				+ " assignedTo " + assignedTo + " ticketCategory " + ticketCategory);
		
		TicketReassignmentLogVo ticketReassignmentLogVo = new TicketReassignmentLogVo();
		
		try {
			ticketReassignmentLogVo.setAddedOn(new Date());
			ticketReassignmentLogVo.setAssignedBy(AuthUtils.getCurrentUserId());
			ticketReassignmentLogVo.setAssignedFrom(assignedFrom);
			ticketReassignmentLogVo.setAssignedTo(assignedTo);
			ticketReassignmentLogVo.setTicketId(ticketId);
			ticketReassignmentLogVo.setTicketCategory(ticketCategory);

			ticketReassignmentLogDAO.add2TicketReassignmentLog(ticketReassignmentLogVo);
			
			log.info("Updated TicketReassignmentLog for ticketId " + ticketId + " assignedFrom " + assignedFrom
					+ " assignedTo " + assignedTo + " ticketCategory " + ticketCategory);
		} catch (Exception e) {
			log.error("Error while reassigning ticket " + e.getMessage());
			e.printStackTrace();
		}
		
		return ticketReassignmentLogVo;
	}

	@Override
	public void prospectLoggerFromSiebel(CRMProspectLogger crmProspectLogger) {
		
		ticketDao.prospectLoggerFromSiebel(crmProspectLogger);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.ticket.TicketCoreService#isEligibleForWOCreation(com.cupola.fwmp.vo.tools.siebel.vo.WOEligibilityCheckVO)
	 */
	@Override
	public WOEligibilityCheckResponse isEligibleForWOCreation(WOEligibilityCheckVO woEligibilityCheckVO, Long cityId) 
	{
		log.info("isEligibleForWOCreation called in core service " + woEligibilityCheckVO);
		
		WOEligibilityCheckResponse woEligibilityCheckResponse = ticketDao.isEligibleForWOCreation(woEligibilityCheckVO, cityId);
		
		log.info("isEligibleForWOCreation reponse in core service " + woEligibilityCheckResponse);
		
		return woEligibilityCheckResponse;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.ticket.TicketCoreService#logCreateUpdateWorkOrderSiebelData(com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateWorkOrderVo)
	 */
	@Override
	public void workOrderLoggerFromSiebel(CRMWorkOrderLogger crmWorkOrderLogger) {

		ticketDao.workOrderLoggerFromSiebel(crmWorkOrderLogger);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.ticket.TicketCoreService#logCreateUpdateFrSiebelData(com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo)
	 */
	@Override
	public void frLoggerFromSiebel(CRMFrLogger crmFrLogger) {

		ticketDao.frLoggerFromSiebel(crmFrLogger);
	}
	
	private APIResponse updateTicketActivityLogVo(String activityId,
			String ticketId, String activityStatus)
	{

		TicketAcitivityDetailsVO ticketAcitivityDetailsVO = new TicketAcitivityDetailsVO();

		ticketAcitivityDetailsVO.setActivityCompletedTime(new Date());
		ticketAcitivityDetailsVO.setActivityId(activityId + "");
		ticketAcitivityDetailsVO.setActivityStatus(activityStatus);
		ticketAcitivityDetailsVO.setTicketId(ticketId + "");

		return ticketActivityLogCoreService
				.addTicketActivityLog(ticketAcitivityDetailsVO);

	}
	
	
	@Override
	public APIResponse getCFEicketWithDetails(TicketFilter filter) {
		return ResponseUtil.createSuccessResponse()
				.setData(ticketDetailDao.getCFETicketWithDetails(filter));
	}
}
