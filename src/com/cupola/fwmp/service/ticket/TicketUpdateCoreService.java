package com.cupola.fwmp.service.ticket;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.StatusVO;

public interface TicketUpdateCoreService {

	APIResponse updateTicketStatus(StatusVO status);

	APIResponse updateFrTicketStatus(StatusVO status);
	
	
}
