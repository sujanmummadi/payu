package com.cupola.fwmp.service.ticket;

import java.util.List;

import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.CafVo;
import com.cupola.fwmp.vo.ETRUpdatebyNE;
import com.cupola.fwmp.vo.GxCxPermission;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.RemarksVO;
import com.cupola.fwmp.vo.TicketApprovalVo;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.tools.CRMFrLogger;
import com.cupola.fwmp.vo.tools.CRMProspectLogger;
import com.cupola.fwmp.vo.tools.CRMWorkOrderLogger;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckResponse;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckVO;

public interface TicketCoreService
{

	public APIResponse addTicket(Ticket Ticket);

	public APIResponse getTicketById(Long id);

	public APIResponse getAllTicket();

	public APIResponse deleteTicket(Long id);

	public APIResponse updateTicket(Ticket Ticket);

	public APIResponse getTickets(TicketFilter filter);

	public APIResponse getTicketWithDetails(TicketFilter filter);

	public APIResponse getPreSaleTicketWithDetails(TicketFilter filter);

	APIResponse assignTickets(AssignVO assignVO);

	public APIResponse searchTickets(TicketFilter filter);

	public APIResponse updateTicketCaf(CafVo cafVo);

	public APIResponse updateTicketEtrByNEorTL(ETRUpdatebyNE etrUpdatebyNE);

	public APIResponse getTicketCount();

	public APIResponse getCountSummary();

	APIResponse updateRemarks(RemarksVO remark);

	APIResponse reassignTicket(ReassignTicketVo reassignTicketVo);

	APIResponse updateGxCXPermission(GxCxPermission gxCxPermission);

	String isETRApprovalPending(Long ticketId);

	List<Long> getAllETRApprovalTickets();
	
	public APIResponse getCountSummaryGropedByNE(long initialWorkStagetype);

	public APIResponse autoClosureStatusUpdateToUser(Long ticketNo, MessageVO vo);

	APIResponse calculateDashBoardStatus(Long ticketType);

	int updatePriority(long ticketNo, Integer escalatedValue,
			boolean modificationType);

	public APIResponse updateTicketDefectCode(long ticketId, long defectCode,long subDefectCode,long status);

	public APIResponse updateTicketDefectCode(FRDefectSubDefectsVO vo);

	public APIResponse getTicketStatusById(Long ticketId);

	public APIResponse getCFEicketWithDetails(TicketFilter filter);
	
	/**@author aditya
	 * TicketReassignmentLogVo
	 * @param ticketId
	 * @param assignedFrom
	 * @param assignedTo
	 * @param ticketCategory
	 * @return
	 */
	TicketReassignmentLogVo updateTicketReassignmentLog(Long ticketId, Long assignedFrom, Long assignedTo,
			String ticketCategory);
	
	void prospectLoggerFromSiebel(CRMProspectLogger crmProspectLogger);

	/**@author aditya
	 * WOEligibilityCheckResponse
	 * @param woEligibilityCheckVO
	 * @param cityId
	 * @return
	 */
	public WOEligibilityCheckResponse isEligibleForWOCreation(WOEligibilityCheckVO woEligibilityCheckVO, Long cityId);

	void workOrderLoggerFromSiebel(CRMWorkOrderLogger crmWorkOrderLogger);
	
	void frLoggerFromSiebel(CRMFrLogger crmFrLogger);
	
}
