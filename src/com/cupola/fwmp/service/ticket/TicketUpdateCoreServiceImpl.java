package com.cupola.fwmp.service.ticket;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.ticket.TicketDetailDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.StatusVO;

public class TicketUpdateCoreServiceImpl implements TicketUpdateCoreService
{
	
	private static Logger logger = Logger.getLogger(TicketUpdateCoreServiceImpl.class
			.getName());
	@Autowired
	TicketDetailDAO ticketDetailDAO;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;
	
	@Autowired
	private FrTicketDao frDao;
	
	@Autowired
	DBUtil dbUtil;
	
	@Autowired
	SalesCoreServcie salesCoreServcie;
	
	@Override
	public APIResponse updateTicketStatus(StatusVO status)
	{
		
		status.setAddedBy(AuthUtils.getCurrentUserId());
		status.setAddedByUserName(AuthUtils.getCurrentUserLoginId());
		String statusMsg = ticketDetailDAO.updateTicketStatus(status);
		
		logger.info("ticket update status:::::: "+statusMsg);
		
		TicketLog  ticketLog = null;
		List<TicketLog> ticketLogs = new ArrayList<TicketLog>();
		
		if(status.getTicketList() != null && status.getTicketList() .size() > 0)
		{
			for (Long id : status.getTicketList())
			{
				ticketLog = new TicketLogImpl(id, TicketStatus.statusCodeValue
						.get(status.getStatus()), AuthUtils.getCurrentUserId());
				
				ticketLog.setStatusId(status.getStatus());
				ticketLog.setReasonCode(status.getReasonCode());
				ticketLog.setReasonValue(status.getReasonValue());
				ticketLog.setRemarks(status.getRemarks());
				ticketLogs.add(ticketLog);
			}
			TicketUpdateGateway.logTickets(ticketLogs);
			return ResponseUtil.createUpdateSuccessResponse();
		}
		
		ticketLog = new TicketLogImpl(status.getTicketId(), TicketStatus.statusCodeValue.get(status.getStatus()), AuthUtils.getCurrentUserId());
		ticketLog.setStatusId(status.getStatus());
		ticketLog.setReasonCode(status.getReasonCode());
		ticketLog.setReasonValue(status.getReasonValue());
		ticketLog.setRemarks(status.getRemarks());
		
		logger.info("ticket update ticketLog :::::: ticketLog.getTicketId()"
		+ticketLog.getTicketId() + " ticketLog.getActivityId() " + ticketLog.getActivityId() 
		+ "ticketLog.getRemarks() " + ticketLog.getRemarks());
		
		TicketUpdateGateway.logTicket(ticketLog);
		
		if(AuthUtils.isCFEUser()) {
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				if( status.getStatus() == TicketStatus.POI_DOCUMENTS_APPROVED || status.getStatus() == TicketStatus.POI_DOCUMENTS_REJECTED ||status.getStatus() == TicketStatus.POA_DOCUMENTS_APPROVED ||
						status.getStatus() == TicketStatus.POA_DOCUMENTS_REJECTED || status.getStatus() == TicketStatus.PAYMENT_APPROVED || status.getStatus() == TicketStatus.PAYMENT_REJECTED )
				salesCoreServcie.updateSalesActivityInCRM(Long.valueOf(status.getTicketId()),  null  , null);
				else if(status.getStatus() == TicketStatus.PAYMENT_REFUND_INITIATED)
					workOrderCoreService.updateWorkOrderActivityInCRM(Long.valueOf(status.getTicketId()), null, null, null, null, null, null, null);
			}
		}
			
		
		return ResponseUtil.createUpdateSuccessResponse();
	}

	@Override
	public APIResponse updateFrTicketStatus(StatusVO status)
	{
		if( status == null )
			return ResponseUtil.nullArgument();
		
		status.setAddedBy(AuthUtils.getCurrentUserId());
		status.setAddedByUserName(AuthUtils.getCurrentUserLoginId());
		String statusMsg = frDao.updateFrTicket(status);
		
		logger.info("ticket update status:::::: "+statusMsg);
		
		TicketLog  ticketLog = null;
		List<TicketLog> ticketLogs = new ArrayList<TicketLog>();
		
		if(status.getTicketList() != null && status.getTicketList() .size() > 0)
		{
			for (Long id : status.getTicketList())
			{
				ticketLog = new TicketLogImpl(id, TicketStatus.statusCodeValue
						.get(status.getStatus()), AuthUtils.getCurrentUserId());
				
				ticketLog.setStatusId(status.getStatus());
				ticketLog.setReasonCode(status.getReasonCode());
				ticketLog.setReasonValue(status.getReasonValue());
				ticketLog.setRemarks(status.getRemarks());
				ticketLog.setFrTicket(true);
				ticketLogs.add(ticketLog);
			}
			TicketUpdateGateway.logTickets(ticketLogs);
			return ResponseUtil.createUpdateSuccessResponse();
		}
		
		ticketLog = new TicketLogImpl(status.getTicketId(), TicketStatus.statusCodeValue.get(status.getStatus()), AuthUtils.getCurrentUserId());
		ticketLog.setStatusId(status.getStatus());
		ticketLog.setReasonCode(status.getReasonCode());
		ticketLog.setReasonValue(status.getReasonValue());
		ticketLog.setRemarks(status.getRemarks());
		ticketLog.setFrTicket(true);
		
		logger.info("ticket update ticketLog :::::: ticketLog.getTicketId()"
		+ticketLog.getTicketId() + " ticketLog.getActivityId() " + ticketLog.getActivityId() 
		+ "ticketLog.getRemarks() " + ticketLog.getRemarks());
		
		TicketUpdateGateway.logTicket(ticketLog);
		
		return ResponseUtil.createUpdateSuccessResponse();
	}

}
