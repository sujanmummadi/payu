package com.cupola.fwmp.service.ticket.counts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.cupola.fwmp.FWMPConstant.ActivityStatusDefinition;
import com.cupola.fwmp.FWMPConstant.CountStatusCode;
import com.cupola.fwmp.FWMPConstant.FilterType;
import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.FWMPConstant.UserPreferenceType;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.service.user.UserPreference;
import com.cupola.fwmp.service.user.UserPreferenceService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.vo.NotificationOptions;
import com.cupola.fwmp.vo.TicketCountTlneVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.counts.SummaryCountVO;
import com.cupola.fwmp.vo.counts.TickectCountsVo;

public class TicketCountCache
{

	private static TicketDAO ticketdao;
	private static TicketCoreService ticketCoreService;
	private static UserPreferenceService userPreferenceService;
	private static WorkOrderDAO workOrderDAO;
	
	private static Integer baseValue;
	
	static final Logger log  = Logger
			.getLogger(TicketCountCache.class);
	
	private static Map<Long, TickectCountsVo > userTicketCache = new HashMap<Long, TickectCountsVo >(); //UI
	private static Map<Long, List<SummaryCountVO>  > userTicketCacheForApp = new HashMap<Long, List<SummaryCountVO>  >(); //UI
	
	
//	private static long cacheAgeInMillis =  2 * 60 * 1000;
	private static long cacheAgeInMillis = 0;
	private static long cacheResetTimestamp =0;

	private static  void resetCacheIfOld()
	{
		if ((System.currentTimeMillis() - cacheResetTimestamp) > cacheAgeInMillis)
		{
			resetAllCache();
		}
	}
	
	public static  List<SummaryCountVO> getCountSummary(Long userId)
	{
		resetCacheIfOld();
		List<SummaryCountVO> vo = userTicketCacheForApp.get(userId);
		if(vo == null || vo.size() == 0)
			updateUserTicketAppCache(userId);
		
		return userTicketCacheForApp.get(userId);
	}
	
	private static void updateUserTicketAppCache(Long userId)
	{

		List<SummaryCountVO> summaryList = getCounts(null,null,userId);
		/*populateDummySummaryCounts(summaryList);*/
		userTicketCacheForApp.put(userId, summaryList);
	}

	public static  TickectCountsVo getTicketCount(Long userId)
	{
		resetCacheIfOld();
		TickectCountsVo vo = userTicketCache.get(userId);
		if(vo == null)
			updateUserTicketCache(userId);
		
		return userTicketCache.get(userId);
	}
	

	private static  void resetAllCache()
	{
		userTicketCache.clear();
		
		userTicketCacheForApp.clear();
		
		cacheResetTimestamp = System.currentTimeMillis();
		log .info("TicketCountCache cache reset done");
	}

	
	public static  void resetUserTicketCache(Long userId)
	{
		userTicketCache.remove(userId);
		updateUserTicketCache( userId);
		
		userTicketCacheForApp.remove(userId);
		updateUserTicketAppCache(userId);
	}
	
	
	private static  void updateUserTicketCache(Long userId)
	{

		UserPreference uf = userPreferenceService
				.getUserPreferencesByPreferenceName(AuthUtils.getCurrentUserId(), UserPreferenceType.NOTIFICATION_BAR);
		
		if(uf == null)
		{
			return  ;
		}
		
		ObjectMapper mapper = new ObjectMapper();

		try
		{
			Map<Integer, String> statusList = new HashMap<Integer, String>();
			Map<Long, String> initialWorkStages = new HashMap<Long, String>();
			Map<Long, String> tTypes = new HashMap <Long, String>();
			Map<Long, String> activities = new HashMap <Long, String>();
			
			if(uf != null && uf.getPrefStringValue() != null)
			{
				NotificationOptions options = mapper.readValue(uf
						.getPrefStringValue(), NotificationOptions.class);

				for (TypeAheadVo tvo : options.getStatus())
				{
					statusList.put(tvo.getId().intValue(), tvo.getName());

				}
				for (TypeAheadVo tvo : options.getTypes())
				{
					tTypes.put(tvo.getId(), tvo.getName());
				}
				for (TypeAheadVo tvo : options.getStages())
				{
					initialWorkStages.put(tvo.getId(), tvo.getName());

				}
				for (TypeAheadVo tvo : options.getActivities())
				{
				activities.put(tvo.getId(), tvo.getName());
				}
				userTicketCache.put(userId, ticketdao.getCounts(statusList, initialWorkStages, tTypes,activities));
			}
			else
			{
				log.info("Notification preference not configured ..... for user "+ userId);
			}

		}  catch ( Exception e)
		{
			log.error("Error While executing update user ticket cache :"+e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	private static int getRandomNumber()
	{
		Random random = new Random();
		int max = 50;
		int min= 0;
		 return random.nextInt(max - min + 1) + min;
	}
//	random.nextInt(max - min + 1) + min
	
	private static Map<String,List<Long>> listOfIds = new HashMap<String,List<Long>>();
	
	private static void clearListOfIdsForCategory()
	{
		listOfIds.clear();
		listOfIds.put("FRCAT", new ArrayList<Long>());
		listOfIds.put("NICAT", new ArrayList<Long>());
		listOfIds.put("RACAT", new ArrayList<Long>());
		listOfIds.put("SHCAT", new ArrayList<Long>());
		listOfIds.put("FICAT", new ArrayList<Long>());
		listOfIds.put("COCAT", new ArrayList<Long>());
		listOfIds.put("FECAT", new ArrayList<Long>());
		listOfIds.put("PRCAT", new ArrayList<Long>());
		listOfIds.put("TOCAT", new ArrayList<Long>());
	}
	
	private static void addTicketIdsToCategory(Long ticketId, String category)
	{
		if(ticketId != null && ticketId > 0)
		{
			List<Long> ids = listOfIds.get(category);
			ids.add(ticketId);
			listOfIds.put(category, ids);
		}
	}
	
	public static List<SummaryCountVO> getCounts(Long workOrderTypeId , Long workStage , Long userId)
	{
		List<TicketCountTlneVo> list = workOrderDAO.getAllWorkOrder(workOrderTypeId, workStage, userId);
		if(list == null)
			list = new ArrayList<TicketCountTlneVo>();
//			return null;
		
		int totalTickets = list.size();
		int totalCoperTickets = 0;
		int totalFiberTickets = 0;
		int totalShiftingTickets = 0;
		int totalReactivationTickets = 0;
		int totalNIDeploymentTickets = 0;
		int totalFeasibilityTicket = 0;
		int totalPriorityTicket = 0;
		
		int totalFrTickets = 0;
		
		totalCoperTickets = (workStage != null && workStage.equals(WorkStageType.COPPER)) ? list.size() : 0; 
		totalFiberTickets = (workStage != null && workStage.equals(WorkStageType.FIBER)) ? list.size() : 0; 
		
		totalShiftingTickets = (workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.SHIFTING)) ? list.size(): 0;
		totalReactivationTickets = (workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.RE_ACTIVATION)) ? list.size(): 0;
		totalNIDeploymentTickets = (workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.NIDEPLOYMENT)) ? list.size(): 0;
		totalFrTickets = (workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.FR)) ? list.size(): 0;
		
		int coperCompleted = 0;
		int fiberCompleted = 0;
		int shiftingCompleted = 0;
		int reActivationCompleted = 0;
		int newIstallationCompleted =0;
		int feasibilityCompleted = 0;
		int priorityCompleted = 0;
		int frCompleted = 0;
		
		clearListOfIdsForCategory();
		
		for(TicketCountTlneVo tlneOrder : list)
		{
			addTicketIdsToCategory(tlneOrder.getTicketId(), "TOCAT");
			if(tlneOrder.getPriorityValue() != null 
					&& tlneOrder.getPriorityValue() >= baseValue )
			{
				totalPriorityTicket++;
				addTicketIdsToCategory(tlneOrder.getTicketId(), "PRCAT");
			}
				
			if( tlneOrder.getTicketStatus() != null 
					&&  TicketStatus.FEASIBILITY_STATUS_LIST.contains(tlneOrder.getTicketStatus()))
			{
				totalFeasibilityTicket++;
				addTicketIdsToCategory(tlneOrder.getTicketId(), "FECAT");
			}
			
			else{
				if(workStage == null && tlneOrder.getCurrentWorkStage()!=null 
						&& tlneOrder.getCurrentWorkStage() == WorkStageType.COPPER )
				{
					totalCoperTickets++;
					addTicketIdsToCategory(tlneOrder.getTicketId(), "COCAT");
				}
				else if(workStage == null && tlneOrder.getCurrentWorkStage()!=null 
						&& tlneOrder.getCurrentWorkStage() == WorkStageType.FIBER)
				{
					totalFiberTickets++;
					addTicketIdsToCategory(tlneOrder.getTicketId(), "FICAT");
				}
				
				if((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.SHIFTING )
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.SHIFTING_STR)))
				{
					totalShiftingTickets++;
					addTicketIdsToCategory(tlneOrder.getTicketId(), "SHCAT");
				}
				else if((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.RE_ACTIVATION)
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.RE_ACTIVATION_STRING)))
				{
					totalReactivationTickets++;
					addTicketIdsToCategory(tlneOrder.getTicketId(), "RACAT");
				}
				else if((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.NIDEPLOYMENT)
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.NEW_INSTALLATION)))
				{
					totalNIDeploymentTickets++;
					addTicketIdsToCategory(tlneOrder.getTicketId(), "NICAT");
				}
				else if((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.FR)
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.FAULT_REPAIR)))	
				{
					totalFrTickets++;
					addTicketIdsToCategory(tlneOrder.getTicketId(), "FRCAT");
				}
			}
			
			if(tlneOrder.getTicketStatus() != null 
					&&  TicketStatus.FEASIBILITY_STATUS_LIST.contains(tlneOrder.getTicketStatus())
					&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus()))
				feasibilityCompleted++;
			
			else{
				if(workStage == null && tlneOrder.getCurrentWorkStage()!=null 
					&& tlneOrder.getCurrentWorkStage() == WorkStageType.COPPER
					&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus()))
					coperCompleted++;
				else if(workStage == null && tlneOrder.getCurrentWorkStage()!=null 
					&& tlneOrder.getCurrentWorkStage() == WorkStageType.FIBER
					&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus()))
					fiberCompleted++;
				
				else if(workStage != null && workStage.equals(WorkStageType.COPPER)
					&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus()))
					coperCompleted++;
			
				else if(workStage != null && workStage.equals(WorkStageType.FIBER)
					&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus()))
					fiberCompleted++;
				
				if(((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.SHIFTING)
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.SHIFTING_STR)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus()))
				{
					shiftingCompleted++;
				}else if(((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.RE_ACTIVATION)
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.RE_ACTIVATION_STRING)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus()))
				{
					reActivationCompleted++;
				}else if(((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.NIDEPLOYMENT)
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.NEW_INSTALLATION)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus())){
					newIstallationCompleted++;
				}else if(((workOrderTypeId == null && tlneOrder.getWorkOrderTypeId()!= null 
						&& tlneOrder.getWorkOrderTypeId() == TicketCategory.FR)
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.FAULT_REPAIR)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus())){
					frCompleted++;
				}else if(((workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.SHIFTING))
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.SHIFTING_STR)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus())){
					shiftingCompleted++;
				}else if(((workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.RE_ACTIVATION))
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.RE_ACTIVATION_STRING)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus())){
					reActivationCompleted++;
				}else if(((workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.NIDEPLOYMENT))
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.NEW_INSTALLATION)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus())){
					newIstallationCompleted++;
				}else if(((workOrderTypeId != null && workOrderTypeId.equals(TicketCategory.FR))
						|| (tlneOrder.getTicketCategory() != null 
						&& tlneOrder.getTicketCategory().equalsIgnoreCase(TicketCategory.FAULT_REPAIR)))
						&& ActivityStatusDefinition.COMPLETED_DEF.contains(tlneOrder.getTicketStatus())){
					frCompleted++;
				}
			}
		}
		
		List<SummaryCountVO> summaryList = new ArrayList<SummaryCountVO>();
		
		summaryList
				.add(new SummaryCountVO(CountStatusCode.REACTIVATION_STRING, TicketCategory.RE_ACTIVATION, FilterType.WORK_ORDER_TYPE, totalReactivationTickets, totalReactivationTickets
						- reActivationCompleted, reActivationCompleted,CountStatusCode.REACTIVATION,listOfIds.get("RACAT")));

		summaryList
				.add(new SummaryCountVO(CountStatusCode.SHIFTING_STRING, TicketCategory.SHIFTING, FilterType.WORK_ORDER_TYPE, totalShiftingTickets, totalShiftingTickets
						- shiftingCompleted, shiftingCompleted,CountStatusCode.SHIFTING,listOfIds.get("SHCAT")));

		summaryList
				.add(new SummaryCountVO(CountStatusCode.NEW_INSTALLATION_STRING, TicketCategory.NIDEPLOYMENT, FilterType.WORK_ORDER_TYPE, totalNIDeploymentTickets, totalNIDeploymentTickets
						- newIstallationCompleted, newIstallationCompleted, CountStatusCode.NEW_INSTALLATION,listOfIds.get("NICAT")));

		summaryList
				.add(new SummaryCountVO(CountStatusCode.FEASIBILITY_STRING, TicketSubCategory.FEASIBILITY, FilterType.STATUS, totalFeasibilityTicket, totalFeasibilityTicket
						- feasibilityCompleted, feasibilityCompleted, CountStatusCode.FEASIBILITY,listOfIds.get("FECAT")));

		summaryList
				.add(new SummaryCountVO(CountStatusCode.PRIORTY_STRING, TicketSubCategory.PRIORITY, FilterType.PRIORITY, totalPriorityTicket, totalPriorityTicket
						- priorityCompleted, priorityCompleted, CountStatusCode.PRIORTY,listOfIds.get("PRCAT")));

		summaryList
				.add(new SummaryCountVO(CountStatusCode.FIBER_STRING, WorkStageType.FIBER, FilterType.INITIAL_WORK_STAGE, totalFiberTickets, totalFiberTickets
						- fiberCompleted, fiberCompleted,CountStatusCode.FIBER,listOfIds.get("FICAT")));

		summaryList
				.add(new SummaryCountVO(CountStatusCode.COPPER_STRING, WorkStageType.COPPER, FilterType.INITIAL_WORK_STAGE, totalCoperTickets, totalCoperTickets
						- coperCompleted, coperCompleted,CountStatusCode.COPPER,listOfIds.get("COCAT")));

		
		summaryList
				.add(new SummaryCountVO(CountStatusCode.TOTAL_STRING, null, 0, totalTickets, totalTickets
						- (fiberCompleted + coperCompleted + feasibilityCompleted), (fiberCompleted
						+ coperCompleted + feasibilityCompleted),CountStatusCode.TOTAL,listOfIds.get("TOCAT")));

		
		
		return summaryList;
	}
	
	public static void populateDummySummaryCounts(List<SummaryCountVO> summaryList)
	{/*
		
		Map<Integer, String> statusList = new HashMap<Integer, String>();
		Map<Long, String> initialWorkStages = new HashMap<Long, String>();
		Map<Long, String> tTypes = new HashMap <Long, String>();

		initialWorkStages.put(TicketCategory.RE_ACTIVATION, CountStatusCode.REACTIVATION_STRING );
		initialWorkStages.put(TicketCategory.SHIFTING,CountStatusCode.SHIFTING_STRING);
		initialWorkStages.put(TicketCategory.NI_SALES,CountStatusCode.NEW_INSTALLATION_STRING );
		
		tTypes.put(WorkStageType.COPPER,CountStatusCode.COPPER_STRING );
		tTypes.put(WorkStageType.FIBER,CountStatusCode.FIBER_STRING );
		
//		TickectCountsVo vo = ticketdao.getCounts(statusList, initialWorkStages, tTypes);
		
//		vo.getTypeCounts()
		
		
		
		
		{
			int total =	getRandomNumber();
			
			int all = total;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.COPPER_STRING, CountStatusCode.COPPER, total, total - 2, 2));
			
			total =	getRandomNumber();
			all += total;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.REACTIVATION_STRING, CountStatusCode.REACTIVATION, total, total - 2, 2));
			
			total =	getRandomNumber();
			all += total;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.SHIFTING_STRING, CountStatusCode.SHIFTING, total, total - 2, 2));
			
			total =	getRandomNumber();
			all += total;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.NEW_INSTALLATION_STRING, CountStatusCode.NEW_INSTALLATION, total, total - 2, 2));
			
			total =	getRandomNumber();
			all += total;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.FEASIBILITY_STRING, CountStatusCode.FEASIBILITY, total, total - 2, 2));
			
			total =	getRandomNumber();
			all += total;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.PRIORTY_STRING, CountStatusCode.PRIORTY, total, total - 2, 2));
			
			total =	getRandomNumber();
			all += total;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.FIBER_STRING, CountStatusCode.FIBER, total, total - 2, 2));
			
			total = all;
			summaryList.add(new SummaryCountVO
			(CountStatusCode.TOTAL_STRING, CountStatusCode.TOTAL, total, total - 2, 2));
			
		}
	*/}
	
	public static TicketDAO getTicketdao()
	{
		return ticketdao;
	}

	public static void setTicketdao(TicketDAO ticketdao)
	{
		TicketCountCache.ticketdao = ticketdao;
	}

	public static UserPreferenceService getUserPreferenceService()
	{
		return userPreferenceService;
	}

	public static void setUserPreferenceService(
			UserPreferenceService userPreferenceService)
	{
		TicketCountCache.userPreferenceService = userPreferenceService;
	}

	public static TicketCoreService getTicketCoreService()
	{
		return ticketCoreService;
	}

	public static void setTicketCoreService(TicketCoreService ticketCoreService)
	{
		TicketCountCache.ticketCoreService = ticketCoreService;
	}

	public static WorkOrderDAO getWorkOrderDAO() {
		return workOrderDAO;
	}

	public static void setWorkOrderDAO(WorkOrderDAO workOrderDAO) {
		TicketCountCache.workOrderDAO = workOrderDAO;
	}

	public static void setBaseValue(Integer baseValue) {
		TicketCountCache.baseValue = baseValue;
	}

}
