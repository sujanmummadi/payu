package com.cupola.fwmp.service.ticket.counts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.FlowActionType;
import com.cupola.fwmp.FRConstant.FrFlowStatus;
import com.cupola.fwmp.FWMPConstant.ActivityStatusDefinition;
import com.cupola.fwmp.FWMPConstant.ClassificationCategories;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.ticket.app.AppTicketCountUtil;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.service.fr.vendor.VendorCache;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.ticket.FrTicketCardViewComp;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.counts.dashboard.FRDashBoardCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRSummaryCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketCountSummaryVO;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketSummaryCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRUserSummaryVO;
import com.cupola.fwmp.vo.counts.dashboard.FrTicketTlneDetailVo;

public class FrTicketCountUtils
{
	private static final Logger LOGGER = Logger
			.getLogger(FrTicketCountCache.class);

	private static FrTicketDao frTicketDao;
	private static UserDao userDao;
	private static Integer baseValue;

	private static UserHelper userHelper;

	public static FRTicketSummaryCountVO getMiddleDetailByTl(long currentUserId)
	{
		List<FRTicketCountSummaryVO> countSummaryList = frTicketDao
				.findFrTicketCounts(currentUserId);
		if (countSummaryList == null)
			return middleDetail(new ArrayList<FRTicketCountSummaryVO>());

		return middleDetail(countSummaryList);
	}

	private static FRTicketSummaryCountVO middleDetail(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		Map<Long, List<FRTicketCountSummaryVO>> neTicketMap = getFrTlneDetailsMap(countSummaryList);
		List<FRUserSummaryVO> cxUpUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> cxDownUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> srElementDetails = new ArrayList<>();

		FRTicketSummaryCountVO vo = new FRTicketSummaryCountVO();

		Set<Long> keys = neTicketMap.keySet();

		for (Long key : keys)
		{
			if (AuthUtils.isFrCxDownNEUser(key))
				cxDownUserDetails.add(getMiddleUser(neTicketMap.get(key)));
			else if (AuthUtils.isFrCxUpNEUser(key))
				cxUpUserDetails.add(getMiddleUser(neTicketMap.get(key)));

			else if (AuthUtils.isSRNEUser(key))
				srElementDetails.add(getMiddleUser(neTicketMap.get(key)));

		}
		vo.setCxDownCountSummary(cxDownUserDetails);
		vo.setCxUpCountSummary(cxUpUserDetails);
		vo.setSrElementUserDetails(srElementDetails);

		getTlneWithNoTicket(keys, vo, "middle");
		return vo;
	}

	private static FRUserSummaryVO getMiddleUser(
			List<FRTicketCountSummaryVO> neDetailsVo)
	{
		FRUserSummaryVO neData = new FRUserSummaryVO();
		int totalCount = 0;
		Map<String, FRSummaryCountVO> mapObject = new HashMap<String, FRSummaryCountVO>();

		List<FRSummaryCountVO> summary = getAppAndTlnesDetail(neDetailsVo);
		for (FRSummaryCountVO newVo : summary)
		{
			mapObject.put(newVo.getStatusName(), newVo);
			if (newVo.getStatusName().equalsIgnoreCase("TOTAL SA"))
				totalCount = newVo.getTotalCounts();
		}

		neData.setUserId(neDetailsVo.get(0).getUserId());
		neData.setUserName(neDetailsVo.get(0).getUserName());
		neData.setOnline(1);
		neData.setTotalCount(totalCount);
		neData.setCountSummary(mapObject);
		return neData;
	}

	public static FRTicketSummaryCountVO getFrTiketTlneDetailCounts(
			Long reportTo)
	{/*
		 * List<FrTicketTlneDetailVo> countSummaryList =
		 * frTicketDao.findFrTicketTlneCounts(reportTo); if( countSummaryList ==
		 * null || countSummaryList.isEmpty()) { if( !AuthUtils.isFRTLUser() )
		 * return getNeDummyData(); }
		 * 
		 * Map<Long, List<FrTicketTlneDetailVo>> neTicketMap =
		 * getTlneDetailsCountsMap(countSummaryList); return
		 * getTlneDetailCounts(neTicketMap);
		 */
		return getMiddleDetailByTl(reportTo);
	}

	private static FRTicketSummaryCountVO getNeDummyData()
	{
		FRTicketSummaryCountVO summaryObject = new FRTicketSummaryCountVO();

		List<FRUserSummaryVO> cxUpUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> cxDownUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> srElementUserDetails = new ArrayList<>();
		summaryObject.setCxDownCountSummary(cxDownUserDetails);
		summaryObject.setCxUpCountSummary(cxUpUserDetails);
		summaryObject.setSrElementUserDetails(srElementUserDetails);

		Map<String, FRSummaryCountVO> mapObject = new HashMap<String, FRSummaryCountVO>();

		FRUserSummaryVO neData = new FRUserSummaryVO();
		FRSummaryCountVO sumaryVendorAssigned = new FRSummaryCountVO();

		List<Long> vendors = VendorCache
				.getAssociatedVendors(AuthUtils.getCurrentUserId());
		if (vendors != null)
			sumaryVendorAssigned.setPendingCounts(vendors.size());

		mapObject.put("VENDOR_ASSIGNED", sumaryVendorAssigned);
		mapObject.put("WIP", new FRSummaryCountVO());
		mapObject.put("TOTAL", new FRSummaryCountVO());
		mapObject.put("NEW_TICKET", new FRSummaryCountVO());
		mapObject.put("SUPPORT_REQUEST", new FRSummaryCountVO());
		mapObject.put("COMPLETED", new FRSummaryCountVO());

		cxDownUserDetails.add(neData);
		cxUpUserDetails.add(neData);
		srElementUserDetails.add(neData);
		neData.setUserId(AuthUtils.getCurrentUserId());
		neData.setUserName(AuthUtils.getCurrentUserDisplayName());
		neData.setOnline(1);
		neData.setTotalCount(0);
		neData.setCountSummary(mapObject);

		return summaryObject;
	}

	private static FRTicketSummaryCountVO getTlneDetailCounts(
			Map<Long, List<FrTicketTlneDetailVo>> neTicketMap)
	{
		FRTicketSummaryCountVO summaryObject = new FRTicketSummaryCountVO();
		List<FRUserSummaryVO> cxUpUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> cxDownUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> srElementUserDetails = new ArrayList<>();

		Set<Long> userIdAsKey = neTicketMap.keySet();
		for (Long userId : userIdAsKey)
		{
			if (AuthUtils.isFrCxDownNEUser(userId))
				cxDownUserDetails
						.add(getNeDetailsCount(neTicketMap.get(userId)));
			else if (AuthUtils.isFrCxUpNEUser(userId))
				cxUpUserDetails.add(getNeDetailsCount(neTicketMap.get(userId)));
			else
				srElementUserDetails
						.add(getNeDetailsCount(neTicketMap.get(userId)));
		}
		summaryObject.setCxDownCountSummary(cxDownUserDetails);
		summaryObject.setCxUpCountSummary(cxUpUserDetails);
		summaryObject.setSrElementUserDetails(srElementUserDetails);
		return summaryObject;
	}

	private static FRUserSummaryVO getNeDetailsCount(
			List<FrTicketTlneDetailVo> neDetailsVo)
	{
		FRUserSummaryVO neData = new FRUserSummaryVO();
		int totalCount = 0;
		Map<String, FRSummaryCountVO> mapObject = new HashMap<String, FRSummaryCountVO>();

		Map<Long, Long> duplicate = new HashMap<Long, Long>(); // ticketId &
																// userId;

		for (FrTicketTlneDetailVo detailCount : neDetailsVo)
		{
			if (duplicate.containsKey(detailCount.getTicketId()))
			{
				duplicate.put(detailCount.getTicketId(), detailCount
						.getUserId());
				continue;
			} else
			{

				duplicate.put(detailCount.getTicketId(), detailCount
						.getUserId());
			}

			if (detailCount.getTicketId() != null
					&& detailCount.getTicketId() > 0)
			{
				totalCount++;
				createCountSummaryMap(mapObject, detailCount
						.getTicketId(), "TOTAL");
				if (ActivityStatusDefinition.INPROGRESS_DEF
						.contains(detailCount.getTicketStatus())
						|| !FRStatus.NOT_IN_PROGRESS_DEF
								.contains(detailCount.getTicketStatus()))
					createCountSummaryMap(mapObject, detailCount
							.getTicketId(), "WIP");
				else
				{
					if (mapObject.get("WIP") != null)
						;
					else
						mapObject.put("WIP", new FRSummaryCountVO());
				}
				if (detailCount.getTicketStatus() != null
						&& detailCount.getTicketStatus()
								.equals(FRStatus.FR_WORKORDER_GENERATED))
					createCountSummaryMap(mapObject, detailCount
							.getTicketId(), "NEW_TICKET");
				else
				{
					if (mapObject.get("NEW_TICKET") != null)
						;
					else
						mapObject.put("NEW_TICKET", new FRSummaryCountVO());
				}

				if (detailCount.getTicketStatus() != null
						&& TicketStatus.COMPLETED_DEF
								.contains(detailCount.getTicketStatus()))
					createCountSummaryMap(mapObject, detailCount
							.getTicketId(), "COMPLETED");
				else
				{
					if (mapObject.get("COMPLETED") != null)
						;
					else
						mapObject.put("COMPLETED", new FRSummaryCountVO());
				}
			} else
			{
				mapObject.put("TOTAL", new FRSummaryCountVO());
				mapObject.put("WIP", new FRSummaryCountVO());
				mapObject.put("NEW_TICKET", new FRSummaryCountVO());
				mapObject.put("COMPLETED", new FRSummaryCountVO());
			}

			if (detailCount.getVendorTicketId() != null
					&& detailCount.getVendorTicketId() > 0)
			{
				long vStatus = Long
						.valueOf(detailCount.getVendorTicketStatus() + "");
				if (detailCount.getRequestedBy() != null
						&& ActionTypeAttributeDefinitions.FR_SUPPORTS_STATUS_LIST
								.contains(vStatus))
				{
					createCountSummaryMap(mapObject, detailCount
							.getVendorTicketId(), "SUPPORT_REQUEST");

					long userType = detailCount.getUserType();
					if (userType > 0
							&& userType == FlowActionType.TRIGGER_CCNR_TEAM)
						createCountSummaryMap(mapObject, detailCount
								.getVendorTicketId(), "CCNR_REQUEST");
					else if (userType > 0
							&& FlowActionType.BLOCKED_FLOW.contains(userType))
						createCountSummaryMap(mapObject, detailCount
								.getVendorTicketId(), "VENDOR_REQUEST");
				} else
				{
					if (mapObject.get("SUPPORT_REQUEST") != null)
						;
					else if (mapObject.get("CCNR_REQUEST") != null)
						;
					else if (mapObject.get("VENDOR_REQUEST") != null)
						;
					else
					{
						mapObject
								.put("SUPPORT_REQUEST", new FRSummaryCountVO());
						mapObject.put("CCNR_REQUEST", new FRSummaryCountVO());
						mapObject.put("VENDOR_REQUEST", new FRSummaryCountVO());
					}
				}
			} else
			{
				if (mapObject.get("SUPPORT_REQUEST") != null)
					;
				else if (mapObject.get("CCNR_REQUEST") != null)
					;
				else if (mapObject.get("VENDOR_REQUEST") != null)
					;
				else
				{
					mapObject.put("SUPPORT_REQUEST", new FRSummaryCountVO());
					mapObject.put("CCNR_REQUEST", new FRSummaryCountVO());
					mapObject.put("VENDOR_REQUEST", new FRSummaryCountVO());
				}
			}

		}
		// get data for suport request and vendor assigned from cache
		FRSummaryCountVO sumaryVendorAssigned = new FRSummaryCountVO();
		List<Long> vendors = VendorCache
				.getAssociatedVendors(neDetailsVo.get(0).getUserId());
		if (vendors != null)
			sumaryVendorAssigned.setPendingCounts(vendors.size());

		mapObject.put("VENDOR_ASSIGNED", sumaryVendorAssigned);
		neData.setUserId(neDetailsVo.get(0).getUserId());
		neData.setUserName(neDetailsVo.get(0).getUserName());
		neData.setOnline(1);
		neData.setTotalCount(totalCount);
		neData.setCountSummary(mapObject);
		return neData;
	}

	// Head count api
	public static FRDashBoardCountVO calculateFrTicketCountForUser(Long userId)
	{
		List<FRTicketCountSummaryVO> countSummaryList = frTicketDao
				.findFrTicketCounts(userId);
		clearListOfIdsForCategory();
		if (countSummaryList == null)
		{
			List<FRSummaryCountVO> summary = new ArrayList<FRSummaryCountVO>();
			summary.addAll(listOfPendingObject.values());
			FRDashBoardCountVO frTlneDetails = new FRDashBoardCountVO();
			frTlneDetails.setCountSummary(summary);
			return frTlneDetails;
		}

		// clearListOfIdsForCategory();
		return getFrTlneTicketCounts(countSummaryList);

	}

	public static FRDashBoardCountVO getDashBoradCountBasedOnGroup(Long userId)
	{
		if (!ApplicationUtils.isAppRequest())
		{
			TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
			if (filter != null)
			{
				if (filter.getAssignedByUserId() != null
						&& filter.getAssignedByUserId() > 0)
				{
					userId = filter.getAssignedByUserId();
				}
			}
		}

		List<FRTicketCountSummaryVO> countSummaryList = frTicketDao
				.findFrTicketCounts(userId);

		clearHeadCountMap();
		TypeAheadVo typeVo = AuthUtils.getCurrentUserGroup();
		if (typeVo != null && typeVo.getName().equals("TL_SR_FR"))
		{
			if (countSummaryList == null)
				return getDefaultDashBordSummary(srElementHeadCount.values());
			else
				return getSrElementTicketCount(countSummaryList);
		}

		else if (typeVo != null && typeVo.getName().equals("TL_FR_CXUP"))
		{
			if (countSummaryList == null)
				return getDefaultDashBordSummary(cxUpHeadCount.values());
			else
				return getCxUpTicketCount(countSummaryList);
		}

		else if (typeVo != null && typeVo.getName().equals("TL_FR_CXDOWN"))
		{
			if (countSummaryList == null)
				return getDefaultDashBordSummary(cxDownHeadCount.values());
			else
				return getCxDownTicketCount(countSummaryList);

		} else if (typeVo != null && typeVo.getName().equals("TL_FR"))
		{
			clearListOfIdsForCategory();
			if (countSummaryList == null)
				return getDefaultDashBordSummary(listOfPendingObject.values());
			else
				return getFrTlneTicketCounts(countSummaryList);
		} else if (AuthUtils.isFRCXDownNEUser() || AuthUtils.isFRCxUpNEUser()
				|| AuthUtils.isFRSRNEUser())
		{
			clearAppAndTlneWiseMap();
			if (countSummaryList == null)
				return getDefaultDashBordSummary(appAndTlneWiseDetail.values());
			else
				return getAppHomeCount(countSummaryList);
		}

		else
		{

			clearListOfIdsForCategory();
			if (countSummaryList == null)
				return getDefaultDashBordSummary(listOfPendingObject.values());
			else
				return getFrTlneTicketCounts(countSummaryList);
		}

	}

	private static List<FRSummaryCountVO> getAppAndTlnesDetail(
			List<FRTicketCountSummaryVO> countSummaryList)
	{

		String GART = "";
		List<FRSummaryCountVO> summary = new ArrayList<FRSummaryCountVO>();
		List<Long> statusList = null;

		Map<Long, String> openGART = new HashMap<Long, String>();
		Map<Long, String> closedGART = new HashMap<Long, String>();
		clearAppAndTlneWiseMap();

		for (FRTicketCountSummaryVO frCount : countSummaryList)
		{
			statusList = frCount.getStatusList();

			if (frCount.getTicketId() != null
					&& frCount.getTicketId().longValue() > 0
					&& frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue()
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("total", frCount.getTicketId(), 4);

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()	))
				
				updateForTotalCountOnGroup("completed", frCount
						.getTicketId(), 4);

			else if (frCount.getTicketStatus() != null && (frCount
					.getTicketStatus().equals(FRStatus.FR_WORKORDER_GENERATED) || frCount
					.getTicketStatus().equals(TicketStatus.OPEN))
					&& frCount.getCurrentflowId() != null 
					&&  frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue() )
				updateForTotalCountOnGroup("new_ticket", frCount
						.getTicketId(), 4);

			else if (frCount.getTicketStatus() != null
					&& (ActivityStatusDefinition.INPROGRESS_DEF
							.contains(frCount.getTicketStatus())
					|| !FRStatus.NOT_IN_PROGRESS_DEF
							.contains(frCount.getTicketStatus()))
					&&  frCount.getCurrentflowId() != null
							&&  frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue()   )
				
				updateForTotalCountOnGroup("wip", frCount.getTicketId(), 4);

			if (frCount.getCurrentflowId() != null 
					&&  frCount.getCurrentflowId().equals(FrFlowStatus.CX_UP )
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("cxup", frCount.getTicketId(), 4);

			else if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId() == 3
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("frequent_disconnection", frCount
						.getTicketId(), 4);

			else if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId() == 4
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("slow_speed", frCount
						.getTicketId(), 4);
			
			if (frCount.getCurrentflowId() != null 
					&&  frCount.getCurrentflowId().equals( ClassificationCategories.FLOW_ID_OTHERS)   
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("other", frCount.getTicketId(), 4);

			if (frCount.getReopenTicket() != null
					&& frCount.getReopenTicket().intValue() > 0
					&& !TicketStatus.COMPLETED_DEF.contains(frCount.getTicketStatus()) )
				
				updateForTotalCountOnGroup("reopen", frCount.getTicketId(), 4);

			if (frCount.getTicketStatus() != null && !TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
			{
				if (frCount.getTicketStatus().equals((int)FRStatus.CCNR_APPROVAL_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 4);

				else if (frCount
						.getTicketStatus().equals((int)FRStatus.SHIFTING_PERMISSION_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 4);

				if (frCount.getTicketStatus().equals((int)FRStatus.ELECTRICIAN_PENDING))
					updateForTotalCountOnGroup("electrician_pending", frCount
							.getTicketId(), 4);

				if (frCount.getTicketStatus().equals((int)FRStatus.SPLICER_PENDING))
					updateForTotalCountOnGroup("splicer_pending", frCount
							.getTicketId(), 4);

				if (frCount.getTicketStatus() .equals((int)FRStatus.SPLICING_PENDING))
					updateForTotalCountOnGroup("splicing_pending", frCount
							.getTicketId(), 4);

				if (frCount.getTicketStatus().equals((int)FRStatus.FIBER_LAYER_PENDING))
					updateForTotalCountOnGroup("fiber_pending", frCount
							.getTicketId(), 4);

				if (frCount.getTicketStatus().equals((int)FRStatus.COPPER_LAYER_PENDING))
					updateForTotalCountOnGroup("copper_pending", frCount
							.getTicketId(), 4);

			}
			/*
			 * this is the code for calculating Open/Close GART so set ticket
			 * creation date for Open GART and set both ticket creation and
			 * close Date for Closed GART and Total GART
			 * 
			 */

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
			{

				GART = CommonUtil
						.getTicketGART(frCount.getTicketStatus(), frCount
								.getTicketCreationDate(), frCount
										.getTicketClosedDate());
				closedGART.put(frCount.getTicketId(), GART);
			} else
			{
				GART = CommonUtil.getTicketGART(frCount
						.getTicketStatus(), frCount.getTicketCreationDate());
				openGART.put(frCount.getTicketId(), GART);
			}

		}

		calculateGART(openGART, "open_gart", "APP");
		calculateGART(closedGART, "close_gart", "APP");

		summary.addAll(new ArrayList<FRSummaryCountVO>(appAndTlneWiseDetail
				.values()));

		return summary;

	}

	private static FRDashBoardCountVO getAppHomeCount(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		FRDashBoardCountVO frTlneDetails = new FRDashBoardCountVO();
		if( countSummaryList == null )
			return frTlneDetails;
		
		Collections.sort(countSummaryList, new FrTicketCardViewComp());
		List<FRSummaryCountVO> countSummary = new ArrayList<>(getAppAndTlnesDetail(countSummaryList));
		frTlneDetails.setCountSummary(countSummary);
		return frTlneDetails;
	}

	private static FRDashBoardCountVO getCxDownTicketCount(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		FRDashBoardCountVO frTlneDetails = new FRDashBoardCountVO();
		List<FRSummaryCountVO> countSummary = new ArrayList<>(getTlCxDownHeadCount(countSummaryList));
		frTlneDetails.setCountSummary(countSummary);
		return frTlneDetails;
	}

	private static List<FRSummaryCountVO> getTlCxDownHeadCount(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		String GART = "";
		List<FRSummaryCountVO> summary = new ArrayList<FRSummaryCountVO>();
		List<Long> statusList = null;

		Map<Long, String> openGART = new HashMap<Long, String>();
		Map<Long, String> closedGART = new HashMap<Long, String>();
		clearHeadCountMap();

		for (FRTicketCountSummaryVO frCount : countSummaryList)
		{
			statusList = frCount.getStatusList();

			if (frCount.getTicketId() != null
					&& frCount.getTicketId().longValue() > 0
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("2_total", frCount.getTicketId(), 3);

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
				updateForTotalCountOnGroup("completed", frCount
						.getTicketId(), 3);

			else if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() == 1 && !isTicketClosed(frCount))
			{
				updateForTotalCountOnGroup("1_cxdown", frCount
						.getTicketId(), 3);

			}
			else if (frCount.getCurrentflowId() != null 
					&& frCount.getCurrentflowId().longValue() == FrFlowStatus.CX_UP 
					&& frCount.getTicketStatus() != null 
					&& frCount.getTicketStatus().equals(FRStatus.FLOW_CHANGED_DOWN_TO_UP ) && !isTicketClosed(frCount))
			{
				updateForTotalCountOnGroup("2_cxup", frCount.getTicketId(), 3);

			}
			
			if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() == ClassificationCategories.FLOW_ID_OTHERS.longValue()
					&& !isTicketClosed(frCount) )
			{
				updateForTotalCountOnGroup("other", frCount.getTicketId(), 3);

			}
			
			if (frCount.getTicketStatus() != null
					&& (ActivityStatusDefinition.INPROGRESS_DEF
							.contains(frCount.getTicketStatus())
					|| !FRStatus.NOT_IN_PROGRESS_DEF
							.contains(frCount.getTicketStatus()))
					&& frCount.getCurrentflowId() != null
							&&  frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue()  )
				
				updateForTotalCountOnGroup("wip", frCount.getTicketId(), 3);

			if (frCount.getPriorityValue() != null
					&& frCount.getPriorityValue() >= baseValue && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("priority", frCount
						.getTicketId(), 3);

			if (frCount.isLocked() && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("locked", frCount.getTicketId(), 3);
			else
			{
				if (frCount.getTicketStatus() != null
						&& !TicketStatus.COMPLETED_DEF
								.contains(frCount.getTicketStatus()))
					updateForTotalCountOnGroup("unlocked", frCount
							.getTicketId(), 3);
			}

			if (frCount.getUserId() != null && frCount.getUserId() > 0 && !isTicketClosed(frCount))
				if (userHelper.isFRTLUser(frCount.getUserId()))
					updateForTotalCountOnGroup("self", frCount
							.getTicketId(), 3);

			if (frCount.getTicketStatus() != null && !TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
			{
				if (frCount.getTicketStatus().equals((int)FRStatus.CCNR_APPROVAL_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 3);

				else if (frCount
						.getTicketStatus().equals((int)FRStatus.SHIFTING_PERMISSION_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 3);

				if (frCount.getTicketStatus() .equals((int)FRStatus.ODTR_PENDING))
					updateForTotalCountOnGroup("odtr_pending", frCount
							.getTicketId(), 3);

				if (frCount.getTicketStatus() .equals((int)FRStatus.ELECTRICIAN_PENDING))
					updateForTotalCountOnGroup("electrician_pending", frCount
							.getTicketId(), 3);

				if (frCount.getTicketStatus() .equals( (int)FRStatus.ETR_ELAPSED))
					updateForTotalCountOnGroup("etr_elapsed", frCount
							.getTicketId(), 3);

				if (frCount.getTicketStatus() .equals((int)FRStatus.SPLICER_PENDING))
					updateForTotalCountOnGroup("3_splicer_pending", frCount
							.getTicketId(), 3);

				if (frCount.getTicketStatus().equals((int)FRStatus.SPLICING_PENDING))
					updateForTotalCountOnGroup("4_splicing_pending", frCount
							.getTicketId(), 3);
				
				if (frCount.getTicketStatus().equals((int)FRStatus.FIBER_LAYER_PENDING))
					updateForTotalCountOnGroup("fiber_pending", frCount
							.getTicketId(), 3);

			}
			/*
			 * this is the code for calculating Open/Close GART so set ticket
			 * creation date for Open GART and set both ticket creation and
			 * close Date for Closed GART and Total GART
			 * 
			 */

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
			{
				GART = CommonUtil
						.getTicketGART(frCount.getTicketStatus(), frCount
								.getTicketCreationDate(), frCount
										.getTicketClosedDate());
				closedGART.put(frCount.getTicketId(), GART);
			} else
			{
				GART = CommonUtil.getTicketGART(frCount
						.getTicketStatus(), frCount.getTicketCreationDate());
				openGART.put(frCount.getTicketId(), GART);
			}

		}

		calculateGART(openGART, "5_open_gart", "DN");
		calculateGART(closedGART, "6_closed_gart", "DN");

		summary.addAll(cxDownHeadCount.values());
		return summary;
	}

	private static FRDashBoardCountVO getCxUpTicketCount(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		FRDashBoardCountVO frTlneDetails = new FRDashBoardCountVO();
		List<FRSummaryCountVO> countSummary = new ArrayList<>(getTlCxUpHeadCount(countSummaryList));
		frTlneDetails.setCountSummary(countSummary);
		return frTlneDetails;
	}

	private static List<FRSummaryCountVO> getTlCxUpHeadCount(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		String GART = "";
		List<FRSummaryCountVO> summary = new ArrayList<FRSummaryCountVO>();
		List<Long> statusList = null;

		Map<Long, String> openGART = new HashMap<Long, String>();
		Map<Long, String> closedGART = new HashMap<Long, String>();
		clearHeadCountMap();

		for (FRTicketCountSummaryVO frCount : countSummaryList)
		{
			statusList = frCount.getStatusList();

			if (frCount.getTicketId() != null
					&& frCount.getTicketId().longValue() > 0 
					&& frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue()
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("total", frCount.getTicketId(), 2);

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus())
					&& frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue() )
				updateForTotalCountOnGroup("completed", frCount
						.getTicketId(), 2);

			else if (frCount.getCurrentflowId() != null 
					&& frCount.getCurrentflowId() .equals(FrFlowStatus.CX_UP) && !isTicketClosed(frCount))
			{
				updateForTotalCountOnGroup("1_cxup", frCount.getTicketId(), 2);

			}

			else if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId() == 3 && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("2_frequence_disconnection", frCount
						.getTicketId(), 2);

			else if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId() == 4 && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("3_slow_speed", frCount
						.getTicketId(), 2);
			
			if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() == ClassificationCategories.FLOW_ID_OTHERS.longValue()
					&& !isTicketClosed(frCount) )
				updateForTotalCountOnGroup("other", frCount
						.getTicketId(), 2);
			
			if (frCount.getTicketStatus() != null
					&& (ActivityStatusDefinition.INPROGRESS_DEF
							.contains(frCount.getTicketStatus())
					|| !FRStatus.NOT_IN_PROGRESS_DEF
							.contains(frCount.getTicketStatus()))
					&& frCount.getCurrentflowId() != null
							&&  frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue()  )
				
				updateForTotalCountOnGroup("wip", frCount
						.getTicketId(), 2);
			
			if (frCount.getPriorityValue() != null
					&& frCount.getPriorityValue() >= baseValue && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("priority", frCount
						.getTicketId(), 2);

			if (frCount.isLocked() && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("locked", frCount.getTicketId(), 2);
			else
			{	
				if( !isTicketClosed(frCount))
					updateForTotalCountOnGroup("unlocked", frCount
						.getTicketId(), 2);
			}
			if (frCount.getUserId() != null && frCount.getUserId() > 0 && !isTicketClosed(frCount))
				if (userHelper.isFRTLUser(frCount.getUserId()))
					updateForTotalCountOnGroup("self", frCount
							.getTicketId(), 2);

			if (frCount.getTicketStatus() != null)
			{
				if (frCount.getTicketStatus().equals((int)FRStatus.CCNR_APPROVAL_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 2);

				else if (frCount
						.getTicketStatus().equals((int)FRStatus.SHIFTING_PERMISSION_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 2);

				if (frCount.getTicketStatus() .equals((int)FRStatus.ODTR_PENDING))
					updateForTotalCountOnGroup("odtr_pending", frCount
							.getTicketId(), 2);

				if (frCount.getTicketStatus() .equals((int)FRStatus.ELECTRICIAN_PENDING))
					updateForTotalCountOnGroup("electrician_pending", frCount
							.getTicketId(), 2);

				if (frCount.getTicketStatus().equals((int)FRStatus.ETR_ELAPSED))
					updateForTotalCountOnGroup("etr_elapsed", frCount
							.getTicketId(), 2);

				if (frCount.getTicketStatus() .equals((int)FRStatus.SPLICER_PENDING))
					updateForTotalCountOnGroup("splicer_pending", frCount
							.getTicketId(), 2);

				if (frCount.getTicketStatus() .equals((int)FRStatus.SPLICING_PENDING))
					updateForTotalCountOnGroup("splicing_pending", frCount
							.getTicketId(), 2);
				
			
/*
				if (frCount.getTicketStatus() == FRStatus.COPPER_LAYER_PENDING)
					updateForTotalCountOnGroup("copper_pending", frCount
							.getTicketId(), 2);*/

			}
			/*
			 * this is the code for calculating Open/Close GART so set ticket
			 * creation date for Open GART and set both ticket creation and
			 * close Date for Closed GART and Total GART
			 * 
			 */

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
			{
				GART = CommonUtil
						.getTicketGART(frCount.getTicketStatus(), frCount
								.getTicketCreationDate(), frCount
										.getTicketClosedDate());
				closedGART.put(frCount.getTicketId(), GART);
			} else
			{
				GART = CommonUtil.getTicketGART(frCount
						.getTicketStatus(), frCount.getTicketCreationDate());
				openGART.put(frCount.getTicketId(), GART);
			}

		}

		calculateGART(openGART, "5_open_gart", "UP");
		calculateGART(closedGART, "6_closed_gart", "UP");

		summary.addAll(cxUpHeadCount.values());
		return summary;
	}

	private static FRDashBoardCountVO getSrElementTicketCount(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		FRDashBoardCountVO frTlneDetails = new FRDashBoardCountVO();
		List<FRSummaryCountVO> countSummary = new ArrayList<>(getTlSrlHeadCount(countSummaryList));
		frTlneDetails.setCountSummary(countSummary);
		return frTlneDetails;
	}

	private static List<FRSummaryCountVO> getTlSrlHeadCount(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		String GART = "";
		List<FRSummaryCountVO> summary = new ArrayList<FRSummaryCountVO>();
		List<Long> statusList = null;

		Map<Long, String> openGART = new HashMap<Long, String>();
		Map<Long, String> closedGART = new HashMap<Long, String>();
		clearHeadCountMap();

		for (FRTicketCountSummaryVO frCount : countSummaryList)
		{
			statusList = frCount.getStatusList();

			if (frCount.getTicketId() != null
					&& frCount.getTicketId().longValue() > 0)
				updateForTotalCountOnGroup("2_total", frCount.getTicketId(), 1);

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
				updateForTotalCountOnGroup("3_completed", frCount
						.getTicketId(), 1);

			if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() == 5 && !isTicketClosed(frCount))
			{
				updateForTotalCountOnGroup("1_sr_element", frCount
						.getTicketId(), 1);
			}
			
			if (frCount.getTicketStatus() != null
					&& (ActivityStatusDefinition.INPROGRESS_DEF
							.contains(frCount.getTicketStatus())
					|| !FRStatus.NOT_IN_PROGRESS_DEF
							.contains(frCount.getTicketStatus()))
					&& frCount.getCurrentflowId() != null
							&&  frCount.getCurrentflowId() != 0  )
				
				updateForTotalCountOnGroup("wip", frCount
						.getTicketId(), 1);
			
			if (frCount.getPriorityValue() != null
					&& frCount.getPriorityValue() >= baseValue && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("priority", frCount
						.getTicketId(), 1);

			if (frCount.isLocked() && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("locked", frCount.getTicketId(), 1);
			else
			{
				if( !isTicketClosed(frCount))
					updateForTotalCountOnGroup("unlocked", frCount.getTicketId(), 1);
			}
						

			if (frCount.getReopenTicket() != null
					&& frCount.getReopenTicket().intValue() > 0 && !isTicketClosed(frCount))
				updateForTotalCountOnGroup("reopen", frCount.getTicketId(), 1);

			if (frCount.getUserId() != null && frCount.getUserId() > 0 && !isTicketClosed(frCount))
				if (userHelper.isFRTLUser(frCount.getUserId()))
					updateForTotalCountOnGroup("self", frCount
							.getTicketId(), 1);

			if (frCount.getTicketStatus() != null)
			{

				if (frCount.getTicketStatus().equals((int)FRStatus.CCNR_APPROVAL_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 1);

				else if (frCount
						.getTicketStatus() .equals((int)FRStatus.SHIFTING_PERMISSION_PENDING))
					updateForTotalCountOnGroup("ccnr_pending", frCount
							.getTicketId(), 1);

				if (frCount.getTicketStatus() .equals((int)FRStatus.ODTR_PENDING))
					updateForTotalCountOnGroup("odtr_pending", frCount
							.getTicketId(), 1);

				if (frCount.getTicketStatus().equals((int)FRStatus.ELECTRICIAN_PENDING))
					updateForTotalCountOnGroup("8_electrician_pending", frCount
							.getTicketId(), 1);

				if (frCount.getTicketStatus() .equals((int)FRStatus.ETR_ELAPSED))
					updateForTotalCountOnGroup("etr_elapsed", frCount
							.getTicketId(), 1);

				if (frCount.getTicketStatus() .equals((int)FRStatus.SPLICER_PENDING))
					updateForTotalCountOnGroup("4_splicer_pending", frCount
							.getTicketId(), 1);

				if (frCount.getTicketStatus() .equals((int)FRStatus.SPLICING_PENDING))
					updateForTotalCountOnGroup("9_splicing_pending", frCount
							.getTicketId(), 1);

			}
			/*
			 * this is the code for calculating Open/Close GART so set ticket
			 * creation date for Open GART and set both ticket creation and
			 * close Date for Closed GART and Total GART
			 * 
			 */

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
			{
				GART = CommonUtil
						.getTicketGART(frCount.getTicketStatus(), frCount
								.getTicketCreationDate(), frCount
										.getTicketClosedDate());
				closedGART.put(frCount.getTicketId(), GART);
			} else
			{
				GART = CommonUtil.getTicketGART(frCount
						.getTicketStatus(), frCount.getTicketCreationDate());
				openGART.put(frCount.getTicketId(), GART);
			}

		}

		calculateGART(openGART, "5_open_gart", "SR");
		calculateGART(closedGART, "6_closed_gart", "SR");

		summary.addAll(srElementHeadCount.values());
		return summary;
	}

	private static FRDashBoardCountVO getDefaultDashBordSummary(
			Collection<FRSummaryCountVO> values)
	{
		List<FRSummaryCountVO> summary = new ArrayList<FRSummaryCountVO>();
		summary.addAll(values);
		FRDashBoardCountVO frTlneDetails = new FRDashBoardCountVO();
		frTlneDetails.setCountSummary(summary);
		return frTlneDetails;
	}

	public static FRTicketSummaryCountVO getFrUserSummaryCount(
			long currentUserId)
	{
		if (!ApplicationUtils.isAppRequest())
		{
			TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
			if (filter != null)
			{
				if (filter.getAssignedByUserId() != null
						&& filter.getAssignedByUserId() > 0)
					currentUserId = filter.getAssignedByUserId();
			}

		}

		List<FRTicketCountSummaryVO> countSummaryList = frTicketDao
				.findFrTicketCounts(currentUserId);
		
		if( userHelper.isHYDUser( currentUserId ))
		{
			if (countSummaryList == null)
				return getFrUserCountsHYD(new ArrayList<FRTicketCountSummaryVO>());

			return getFrUserCountsHYD(countSummaryList);
		}
		else
		{
			if (countSummaryList == null)
				return getFrUserCounts(new ArrayList<FRTicketCountSummaryVO>());

			return getFrUserCounts(countSummaryList);
		}
	}
	
	private static FRTicketSummaryCountVO getFrUserCountsHYD( List<FRTicketCountSummaryVO> countSummaryList )
	{
		Map<Long, List<FRTicketCountSummaryVO>> neTicketMap = getFrTlneDetailsMap(countSummaryList);
		List<FRUserSummaryVO> cxUpUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> cxDownUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> priorityDetails = new ArrayList<>();
		List<FRUserSummaryVO> srElementDetails = new ArrayList<>();

		FRTicketSummaryCountVO vo = new FRTicketSummaryCountVO(cxUpUserDetails, cxDownUserDetails, priorityDetails);

		Set<Long> keys = neTicketMap.keySet();

		for (Long key : keys)
		{
			if (AuthUtils.isFrCxDownNEUser(key))
				cxDownUserDetails.add(getCxDownCount(neTicketMap.get(key),"HYD"));
			else if (AuthUtils.isFrCxUpNEUser(key))
				cxUpUserDetails.add(getCxUpCount(neTicketMap.get(key),"HYD"));

			else if (AuthUtils.isSRNEUser(key))
				srElementDetails
						.add(getSeniorElementCount(neTicketMap.get(key),"HYD"));

			if (neTicketMap.get(key).get(0).getUserId() > 0)
				priorityDetails.add(getPriorityCount(neTicketMap.get(key),"HYD"));
		}

		vo.setSrElementUserDetails(srElementDetails);

		getTlneWithNoTicket(keys, vo, "HYD");
		setHeaderValueForRightDetail( vo ,"HYD");
		
		return vo;
	}
	
	private static void setHeaderValueForRightDetail( FRTicketSummaryCountVO vo, String cityName )
	{
		if( vo == null )
			return ;
		
		Set<String> toBeShowInUiPriority = AppTicketCountUtil.getDashBoardSymptomCode("Priority",cityName);
		Set<String> toBeShowInUiCxUp = AppTicketCountUtil.getDashBoardSymptomCode("CxUp",cityName);
		Set<String> toBeShowInUiCxDown = AppTicketCountUtil.getDashBoardSymptomCode("CxDown",cityName);
		Set<String> toBeShowInUiSr = AppTicketCountUtil.getDashBoardSymptomCode("SrElement",cityName);
		
		if( toBeShowInUiCxUp != null )
			vo.setCxUpHeader(toBeShowInUiCxUp);
		if( toBeShowInUiPriority != null )
			vo.setPriorityHeader(toBeShowInUiPriority);
		
		if( toBeShowInUiCxDown != null )
			vo.setCxDownHeader(toBeShowInUiCxDown);
		if( toBeShowInUiSr != null )
			vo.setCxSrElementHeader(toBeShowInUiSr);
		
	}
	private static FRTicketSummaryCountVO getFrUserCounts(
			List<FRTicketCountSummaryVO> countSummaryList)
	{
		Map<Long, List<FRTicketCountSummaryVO>> neTicketMap = getFrTlneDetailsMap(countSummaryList);
		List<FRUserSummaryVO> cxUpUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> cxDownUserDetails = new ArrayList<>();
		List<FRUserSummaryVO> priorityDetails = new ArrayList<>();
		List<FRUserSummaryVO> srElementDetails = new ArrayList<>();

		FRTicketSummaryCountVO vo = new FRTicketSummaryCountVO(cxUpUserDetails, cxDownUserDetails, priorityDetails);

		Set<Long> keys = neTicketMap.keySet();

		for (Long key : keys)
		{
			if (AuthUtils.isFrCxDownNEUser(key))
				cxDownUserDetails.add(getCxDownCount(neTicketMap.get(key),null));
			else if (AuthUtils.isFrCxUpNEUser(key))
				cxUpUserDetails.add(getCxUpCount(neTicketMap.get(key),null));

			else if (AuthUtils.isSRNEUser(key))
				srElementDetails
						.add(getSeniorElementCount(neTicketMap.get(key),null));

			if (neTicketMap.get(key).get(0).getUserId() > 0)
				priorityDetails.add(getPriorityCount(neTicketMap.get(key),null));
		}

		vo.setSrElementUserDetails(srElementDetails);

		getTlneWithNoTicket(keys, vo, "");
		setHeaderValueForRightDetail( vo ,null);
		
		return vo;
	}

	private static void createCountSummaryMap(Map<String, FRSummaryCountVO> map,
			Long tickId, String mapKey)
	{
		FRSummaryCountVO summaryMapObject = null;
		List<Long> tikIds = null;
		int count = 0;
		if (map.containsKey(mapKey))
		{
			summaryMapObject = map.get(mapKey);
			tikIds = summaryMapObject.getTicketIds();
			tikIds.add(tickId);
			count = summaryMapObject.getPendingCounts();
			count++;
			summaryMapObject.setPendingCounts(count);
			summaryMapObject.setTicketIds(tikIds);
			map.put(mapKey, summaryMapObject);
		} else
		{
			summaryMapObject = new FRSummaryCountVO();
			tikIds = new ArrayList<Long>();
			tikIds.add(tickId);
			count++;
			summaryMapObject.setPendingCounts(count);
			summaryMapObject.setTicketIds(tikIds);
			map.put(mapKey, summaryMapObject);
		}
	}

	private static Map<String, FRSummaryCountVO> setDefaultCountValue(
			Map<String, FRSummaryCountVO> map, Set<String> toBeShowInUi)
	{
		if (toBeShowInUi != null)
		{
			Set<String> fromMap = map.keySet();
			toBeShowInUi.removeAll(fromMap);
			for (String value : toBeShowInUi)
				map.put(value, new FRSummaryCountVO(0, new ArrayList<Long>(), value));
		}
		Map<String, FRSummaryCountVO> newMap = new HashMap<String, FRSummaryCountVO>(map);
		return newMap;
	}

	private static FRUserSummaryVO getSeniorElementCount(
			List<FRTicketCountSummaryVO> list,String cityName)
	{

		Set<String> toBeShowInUi = AppTicketCountUtil
				.getDashBoardSymptomCode("SrElement",cityName);
		Map<String, FRSummaryCountVO> srElement = new HashMap<String, FRSummaryCountVO>();
		FRUserSummaryVO value = new FRUserSummaryVO();
		int totalCount = 0;
		String mapKey = null;

		for (FRTicketCountSummaryVO frCount : list)
		{
			mapKey = frCount.getCurrentSymptomName();
			if (mapKey == null)
				continue;

			totalCount++;
			if (toBeShowInUi != null && toBeShowInUi.contains(mapKey))
			{
				if (srElement.containsKey(mapKey))
					createCountSummaryMap(srElement, frCount
							.getTicketId(), mapKey);
				else
					createCountSummaryMap(srElement, frCount
							.getTicketId(), mapKey);
			}
		}
		setDefaultCountValue(srElement, toBeShowInUi);

		value.setOnline(1);
		value.setTotalCount(totalCount);
		value.setCountSummary(srElement);
		value.setUserName(list.get(0).getUserName());
		value.setUserId(list.get(0).getUserId());
		return value;

	}

	private static FRUserSummaryVO getCxUpCount(
			List<FRTicketCountSummaryVO> list,String cityName)
	{
		Set<String> toBeShowInUi = AppTicketCountUtil
				.getDashBoardSymptomCode("CxUp",cityName);
		Map<String, FRSummaryCountVO> cxUpMap = new HashMap<String, FRSummaryCountVO>();
		FRUserSummaryVO value = new FRUserSummaryVO();
		int totalCount = 0;
		String mapKey = null;

		for (FRTicketCountSummaryVO frCount : list)
		{
			mapKey = frCount.getCurrentSymptomName();
			if (mapKey == null)
				continue;

			totalCount++;
			if (toBeShowInUi != null && toBeShowInUi.contains(mapKey))
			{
				if (cxUpMap.containsKey(mapKey))
					createCountSummaryMap(cxUpMap, frCount
							.getTicketId(), mapKey);
				else
					createCountSummaryMap(cxUpMap, frCount
							.getTicketId(), mapKey);
			}
		}
		setDefaultCountValue(cxUpMap, toBeShowInUi);

		value.setOnline(1);
		value.setTotalCount(totalCount);
		value.setCountSummary(cxUpMap);
		value.setUserName(list.get(0).getUserName());
		value.setUserId(list.get(0).getUserId());
		return value;
	}

	private static FRUserSummaryVO getCxDownCount(
			List<FRTicketCountSummaryVO> list,String cityName)
	{
		Set<String> toBeShowInUi = AppTicketCountUtil
				.getDashBoardSymptomCode("CxDown",cityName);
		Map<String, FRSummaryCountVO> cxDownMap = new HashMap<String, FRSummaryCountVO>();
		FRUserSummaryVO value = new FRUserSummaryVO();
		int totalCount = 0;
		String mapKey = null;

		for (FRTicketCountSummaryVO frCount : list)
		{
			mapKey = frCount.getCurrentSymptomName();
			if (mapKey == null)
				continue;

			totalCount++;
			if (toBeShowInUi != null && toBeShowInUi.contains(mapKey))
			{
				if (cxDownMap.containsKey(mapKey))
					createCountSummaryMap(cxDownMap, frCount
							.getTicketId(), mapKey);
				else
					createCountSummaryMap(cxDownMap, frCount
							.getTicketId(), mapKey);
			}
		}
		setDefaultCountValue(cxDownMap, toBeShowInUi);

		value.setOnline(1);
		value.setTotalCount(totalCount);
		value.setCountSummary(cxDownMap);
		value.setUserName(list.get(0).getUserName());
		value.setUserId(list.get(0).getUserId());
		return value;
	}

	private static FRUserSummaryVO getPriorityCount(
			List<FRTicketCountSummaryVO> list,String cityName)
	{
		Set<String> toBeShowInUi = AppTicketCountUtil
				.getDashBoardSymptomCode("Priority",cityName);
		FRUserSummaryVO value = new FRUserSummaryVO();
		int totalCount = 0;

		Map<String, FRSummaryCountVO> priorityMap = new HashMap<String, FRSummaryCountVO>();
		String priorityLevel = null;
		for (FRTicketCountSummaryVO frCount : list)
		{
			priorityLevel = AppTicketCountUtil
					.getPriorityLevel(frCount.getPriorityValue());
			if (priorityLevel == null)
				continue;

			totalCount++;
			setDefaultCountValue(priorityMap, toBeShowInUi);

			if (priorityMap.containsKey(priorityLevel))
				createCountSummaryMap(priorityMap, frCount
						.getTicketId(), priorityLevel);
			else
				createCountSummaryMap(priorityMap, frCount
						.getTicketId(), priorityLevel);

		}
		value.setOnline(1);
		value.setTotalCount(totalCount);
		value.setCountSummary(priorityMap);
		value.setUserName(list.get(0).getUserName());
		value.setUserId(list.get(0).getUserId());
		return value;
	}

	public static FRDashBoardCountVO getFrTlneTicketCounts(
			List<FRTicketCountSummaryVO> summary)
	{
		FRDashBoardCountVO frTlneDetails = new FRDashBoardCountVO();
		List<FRSummaryCountVO> countSummary = new ArrayList<>(getTlHeadCount(summary));
		frTlneDetails.setCountSummary(countSummary);
		return frTlneDetails;
	}

	private static void updateMapForTotalCount(String countName, Long ticketId)
	{
		int countValue = 0;
		FRSummaryCountVO sumary = null;
		List<Long> ticketIds = null;

		if (countName != null && listOfPendingObject.containsKey(countName))
		{
			sumary = listOfPendingObject.get(countName);
			countValue = sumary.getTotalCounts();
			countValue++;

			ticketIds = sumary.getTicketIds();
			ticketIds.add(ticketId);
			sumary.setTotalCounts(countValue);
			sumary.setTicketIds(ticketIds);
			listOfPendingObject.put(countName, sumary);
		}
	}

	private static void updateForTotalCountOnGroup(String countName,
			Long ticketId, int type)
	{
		int countValue = 0;
		FRSummaryCountVO sumary = null;
		List<Long> ticketIds = new ArrayList<Long>();

		if (countName != null)
		{
			if (type == 1 && srElementHeadCount.containsKey(countName))
			{
				sumary = srElementHeadCount.get(countName);
				countValue = sumary.getTotalCounts();
				countValue++;

				ticketIds = sumary.getTicketIds();
				ticketIds.add(ticketId);
				sumary.setTotalCounts(countValue);
				sumary.setTicketIds(ticketIds);
				srElementHeadCount.put(countName, sumary);
			}
			else if ( type == 2 && cxUpHeadCount.containsKey(countName) )
			{
				sumary = cxUpHeadCount.get(countName);
				countValue = sumary.getTotalCounts();
				countValue++;

				ticketIds = sumary.getTicketIds();
				ticketIds.add(ticketId);
				sumary.setTotalCounts(countValue);
				sumary.setTicketIds(ticketIds);
				cxUpHeadCount.put(countName, sumary);
			}
			else if ( type == 3 && cxDownHeadCount.containsKey(countName) )
			{
				sumary = cxDownHeadCount.get(countName);
				countValue = sumary.getTotalCounts();
				countValue++;

				ticketIds = sumary.getTicketIds();
				ticketIds.add(ticketId);
				sumary.setTotalCounts(countValue);
				sumary.setTicketIds(ticketIds);
				cxDownHeadCount.put(countName, sumary);
			}
			else if ( type == 4 && appAndTlneWiseDetail.containsKey(countName) )
			{
				sumary = appAndTlneWiseDetail.get(countName);
				countValue = sumary.getTotalCounts();
				countValue++;

				ticketIds = sumary.getTicketIds();
				ticketIds.add(ticketId);
				sumary.setTotalCounts(countValue);
				sumary.setTicketIds(ticketIds);
				appAndTlneWiseDetail.put(countName, sumary);
			}
		}
	}

	private static Map<String, FRSummaryCountVO> getCommonHeadDefaultCount()
	{
		Map<String, FRSummaryCountVO> common = new TreeMap<String, FRSummaryCountVO>();

		common.put("locked", new FRSummaryCountVO("LOCKED", 582l));
		common.put("unlocked", new FRSummaryCountVO("UNLOCKED", 583l));

		common.put("priority", new FRSummaryCountVO("PRIORITY", 581l));
		common.put("etr_elapsed", new FRSummaryCountVO("ETR_ELAPSED", 580l));

		common.put("5_open_gart", new FRSummaryCountVO("OPEN_GART", 701l));
		common.put("6_closed_gart", new FRSummaryCountVO("CLOSED_GART", 702l));

		common.put("odtr_pending", new FRSummaryCountVO("ODTR_PENDING", 552l));
		common.put("ccnr_pending", new FRSummaryCountVO("CCNR_PENDING", 553l));

		common.put("reopen", new FRSummaryCountVO("REOPEN", 403l));
		common.put("self", new FRSummaryCountVO("UNASSIGNED", 405l));
		
		common.put("wip", new FRSummaryCountVO("WIP", 405l));

		return common;
	}

	private static Map<String, FRSummaryCountVO> listOfPendingObject = new TreeMap<String, FRSummaryCountVO>();
	private static Map<String, List<FRTicketCountSummaryVO>> cxUpMap = new HashMap<String, List<FRTicketCountSummaryVO>>();
	private static Map<String, List<FRTicketCountSummaryVO>> cxDownMap = new HashMap<String, List<FRTicketCountSummaryVO>>();

	private static Map<String, FRSummaryCountVO> cxUpHeadCount = new TreeMap<String, FRSummaryCountVO>();
	private static Map<String, FRSummaryCountVO> cxDownHeadCount = new TreeMap<String, FRSummaryCountVO>();
	private static Map<String, FRSummaryCountVO> srElementHeadCount = new TreeMap<String, FRSummaryCountVO>();

	private static Map<String, FRSummaryCountVO> appAndTlneWiseDetail = new TreeMap<String, FRSummaryCountVO>();

	private static Map<String, FRSummaryCountVO> setDetaultValueForAppAndTlWiseMap()
	{

		Map<String, FRSummaryCountVO> map = new TreeMap<String, FRSummaryCountVO>();

		map.put("FIBER PENDING", new FRSummaryCountVO("FIBER PENDING", 590l));
		map.put("COPPER PENDING", new FRSummaryCountVO("COPPER PENDING", 570l));
		map.put("SPLICER PENDING", new FRSummaryCountVO("SPLICER PENDING", 549l));
		map.put("SPLICING PENDING", new FRSummaryCountVO("SPLICING PENDING", 550));

		map.put("ELECTRICIAN PENDING", new FRSummaryCountVO("ELECTRICIAN PENDING", 551l));
		map.put("CCNR PENDING", new FRSummaryCountVO("CCNR PENDING", 553l));

		map.put("FREQUENT DISCONNECTION", new FRSummaryCountVO("FREQUENT DISCONNECTION", 503l));
		map.put("SLOW SPEED", new FRSummaryCountVO("SLOW SPEED", 504l));
		map.put("CX UP", new FRSummaryCountVO("CX UP", 502l));

		map.put("NEW TICKET", new FRSummaryCountVO("NEW TICKET", 901));
		map.put("WIP", new FRSummaryCountVO("WIP", 100));
		map.put("COMPLETED", new FRSummaryCountVO("COMPLETED", 101));
		map.put("TOTAL SA", new FRSummaryCountVO("TOTAL SA", 500));

		map.put("OPEN GART", new FRSummaryCountVO("OPEN GART", 701));
		map.put("CLOSE GART", new FRSummaryCountVO("CLOSE GART", 702));
		map.put("REOPEN", new FRSummaryCountVO("REOPEN", 403l));
		map.put("OTHER", new FRSummaryCountVO("OTHER", 505l));

		return map;

	}

	private static void clearAppAndTlneWiseMap()
	{
		appAndTlneWiseDetail.clear();

		appAndTlneWiseDetail
				.put("fiber_pending", new FRSummaryCountVO("FIBER PENDING", 590l));
		appAndTlneWiseDetail
				.put("copper_pending", new FRSummaryCountVO("COPPER PENDING", 570l));
		appAndTlneWiseDetail
				.put("splicer_pending", new FRSummaryCountVO("SPLICER PENDING", 549l));
		appAndTlneWiseDetail
				.put("splicing_pending", new FRSummaryCountVO("SPLICING PENDING", 550));

		appAndTlneWiseDetail
				.put("electrician_pending", new FRSummaryCountVO("ELECTRICIAN PENDING", 551l));
		appAndTlneWiseDetail
				.put("ccnr_pending", new FRSummaryCountVO("CCNR PENDING", 553l));

		appAndTlneWiseDetail
				.put("frequent_disconnection", new FRSummaryCountVO("FREQUENT DISCONNECTION", 503l));
		appAndTlneWiseDetail
				.put("slow_speed", new FRSummaryCountVO("SLOW SPEED", 504l));
		appAndTlneWiseDetail.put("cxup", new FRSummaryCountVO("CX UP", 502l));
		appAndTlneWiseDetail.put("other", new FRSummaryCountVO("OTHER", 505l));

		appAndTlneWiseDetail
				.put("new_ticket", new FRSummaryCountVO("NEW TICKET", 901));
		appAndTlneWiseDetail.put("wip", new FRSummaryCountVO("WIP", 100));
		appAndTlneWiseDetail
				.put("completed", new FRSummaryCountVO("COMPLETED", 101));
		appAndTlneWiseDetail.put("total", new FRSummaryCountVO("TOTAL SA", 500));

		appAndTlneWiseDetail
				.put("open_gart", new FRSummaryCountVO("OPEN GART", 701));
		appAndTlneWiseDetail
				.put("close_gart", new FRSummaryCountVO("CLOSE GART", 702));
		appAndTlneWiseDetail
				.put("reopen", new FRSummaryCountVO("REOPEN", 403l));
	}

	private static void clearHeadCountMap()
	{
		TypeAheadVo typeVo = AuthUtils.getCurrentUserGroup();

		if (typeVo != null && typeVo.getName().equals("TL_SR_FR"))
		{
			srElementHeadCount.clear();
			srElementHeadCount
					.put("1_sr_element", new FRSummaryCountVO("SR_ELEMENT", 505l));

			srElementHeadCount
					.put("4_splicer_pending", new FRSummaryCountVO("SPLICER_PENDING", 549l));
			srElementHeadCount
					.put("9_splicing_pending", new FRSummaryCountVO("SPLICING_PENDING", 550l));
			srElementHeadCount
					.put("8_electrician_pending", new FRSummaryCountVO("ELECTRICIAN_PENDING", 551l));

			srElementHeadCount
					.put("3_completed", new FRSummaryCountVO("COMPLETED", 101l));
			srElementHeadCount
					.put("2_total", new FRSummaryCountVO("TOTAL", 500l));

			srElementHeadCount.putAll(getCommonHeadDefaultCount());
		}

		else if (typeVo != null && typeVo.getName().equals("TL_FR_CXUP"))
		{
			cxUpHeadCount.clear();

			cxUpHeadCount
					.put("1_cxup", new FRSummaryCountVO("CX_UP", 502l));
			cxUpHeadCount
					.put("2_frequence_disconnection", new FRSummaryCountVO("FREQUENT_DISCONNECTION", 503l));
			cxUpHeadCount
					.put("3_slow_speed", new FRSummaryCountVO("SLOW_SPEED", 504l));
			cxUpHeadCount
					.put("other", new FRSummaryCountVO("OTHER", 505l));

			cxUpHeadCount
					.put("splicer_pending", new FRSummaryCountVO("SPLICER_PENDING", 549l));
			cxUpHeadCount
					.put("splicing_pending", new FRSummaryCountVO("SPLICING_PENDING", 550l));

			cxUpHeadCount
					.put("fiber_laying", new FRSummaryCountVO("FIBER_LAYER_PENDING", 590l));
			cxUpHeadCount
					.put("copper_laying", new FRSummaryCountVO("COPPER_LAYER_PENDING", 570l));

			cxUpHeadCount
					.put("completed", new FRSummaryCountVO("COMPLETED", 101l));
			cxUpHeadCount.put("total", new FRSummaryCountVO("TOTAL SA", 500l));

			cxUpHeadCount.putAll(getCommonHeadDefaultCount());

		}

		else if (typeVo != null && typeVo.getName().equals("TL_FR_CXDOWN"))
		{
			cxDownHeadCount.clear();
			cxDownHeadCount
					.put("1_cxdown", new FRSummaryCountVO("CX_DOWN", 501l));
			cxDownHeadCount.put("2_cxup", new FRSummaryCountVO("DOWN 2 UP", 502l));
			cxDownHeadCount.put("other", new FRSummaryCountVO("OTHER", 505l));

			cxDownHeadCount
					.put("3_splicer_pending", new FRSummaryCountVO("SPLICER_PENDING", 549l));
			cxDownHeadCount
					.put("4_splicing_pending", new FRSummaryCountVO("SPLICING_PENDING", 550l));

			cxDownHeadCount
					.put("electrician_pending", new FRSummaryCountVO("ELECTRICIAN_PENDING", 551l));
			
			cxDownHeadCount
					.put("fiber_pending", new FRSummaryCountVO("FIBER PENDING", 590l));

			cxDownHeadCount
					.put("completed", new FRSummaryCountVO("COMPLETED", 101l));
			cxDownHeadCount.put("2_total", new FRSummaryCountVO("TOTAL", 500l));

			cxDownHeadCount.putAll(getCommonHeadDefaultCount());
		}

		else
		{
			cxUpHeadCount.clear();
			cxDownHeadCount.clear();
		}
	}

	private static void clearListOfIdsForCategory()
	{
		LOGGER.debug("Clearing Category map...");
		listOfPendingObject.clear();
		cxUpMap.clear();
		cxDownMap.clear();

		listOfPendingObject.put("1_cxup", new FRSummaryCountVO("CX_UP", 502l));
		listOfPendingObject
				.put("4_cxdown", new FRSummaryCountVO("CX_DOWN", 501l));
		listOfPendingObject.put("down2up", new FRSummaryCountVO("DOWN 2 UP", 506));
		listOfPendingObject.put("other", new FRSummaryCountVO("OTHER", 505l));

		listOfPendingObject
				.put("splicer_pending", new FRSummaryCountVO("SPLICER_PENDING", 549l));
		listOfPendingObject
				.put("splicing_pending", new FRSummaryCountVO("SPLICING_PENDING", 550l));

		listOfPendingObject
				.put("electrician_pending", new FRSummaryCountVO("ELECTRICIAN_PENDING", 551l));

		listOfPendingObject
				.put("completed", new FRSummaryCountVO("COMPLETED", 101l));
		listOfPendingObject.put("7_total", new FRSummaryCountVO("TOTAL SA", 500l));

		listOfPendingObject
				.put("2_frequence_disconnection", new FRSummaryCountVO("FREQUENT_DISCONNECTION", 503l));
		listOfPendingObject
				.put("3_slow_speed", new FRSummaryCountVO("SLOW_SPEED", 504l));

		listOfPendingObject.putAll(getCommonHeadDefaultCount());

		if (AuthUtils.isFrManager())
			listOfPendingObject
					.put("0_sr_element", new FRSummaryCountVO("SR_ELEMENT", 505l));

		cxUpMap.put("cxupUser", new ArrayList<FRTicketCountSummaryVO>());
		cxDownMap.put("cxDownUser", new ArrayList<FRTicketCountSummaryVO>());
	}

	private static List<FRSummaryCountVO> getTlHeadCount(
			List<FRTicketCountSummaryVO> summaryVo)
	{
		List<FRSummaryCountVO> summary = new ArrayList<FRSummaryCountVO>();
		if( summaryVo == null )
			return summary;
		
		LOGGER.info("TOTAL HEAD COUNT TICKET FOUND : "+summaryVo.size());
		String GART = "";
	
		List<Long> statusList = null;

		Map<Long, String> openGART = new HashMap<Long, String>();
		Map<Long, String> closedGART = new HashMap<Long, String>();
		clearListOfIdsForCategory();

		for (FRTicketCountSummaryVO frCount : summaryVo)
		{
			statusList = frCount.getStatusList();

			if (frCount.getTicketId() != null
					&& frCount.getTicketId().longValue() > 0
					&& frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() !=ClassificationCategories.FLOW_ID_OTHERS.longValue()
					&& !isTicketClosed(frCount) )
				updateMapForTotalCount("7_total", frCount.getTicketId());

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus())
					&& frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() != ClassificationCategories.FLOW_ID_OTHERS.longValue() )
				updateMapForTotalCount("completed", frCount.getTicketId());

			if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() == 1
					&& !isTicketClosed(frCount))
			{
				updateMapForTotalCount("4_cxdown", frCount.getTicketId());
				/*
				 * List<FRTicketCountSummaryVO> cxDownUser =
				 * cxDownMap.get("cxDownUser"); cxDownUser.add(frCount);
				 * cxDownMap.put("cxDownUser", cxDownUser);
				 */
			} 
			else if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() == FrFlowStatus.CX_UP
					&& !isTicketClosed(frCount) )
			{
				updateMapForTotalCount("1_cxup", frCount.getTicketId());
				/*
				 * List<FRTicketCountSummaryVO> cxupUser =
				 * cxUpMap.get("cxupUser"); cxupUser.add(frCount);
				 * cxUpMap.put("cxupUser", cxupUser);
				 */
			}
			
			if( frCount.getCurrentflowId() != null 
					&& frCount.getCurrentflowId().longValue() == FrFlowStatus.CX_UP 
					&& frCount.getTicketStatus() != null 
					&& frCount.getTicketStatus().equals(FRStatus.FLOW_CHANGED_DOWN_TO_UP )
					&& !isTicketClosed(frCount))
			{
				updateMapForTotalCount("down2up", frCount.getTicketId());
			}

			if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId().longValue() == ClassificationCategories.FLOW_ID_OTHERS.longValue()
					&& !isTicketClosed(frCount))
			{
				updateMapForTotalCount("other", frCount.getTicketId());
			}

			if (frCount.getTicketStatus() != null
					&& (ActivityStatusDefinition.INPROGRESS_DEF
							.contains(frCount.getTicketStatus())
					|| !FRStatus.NOT_IN_PROGRESS_DEF
							.contains(frCount.getTicketStatus()))
					&& !isTicketClosed(frCount)
							&& frCount.getCurrentflowId() != null
							&& frCount.getCurrentflowId() != 0  )
				
				updateMapForTotalCount("wip", frCount.getTicketId());
			
			if (frCount.getPriorityValue() != null
					&& frCount.getPriorityValue() >= baseValue && !isTicketClosed(frCount))
				updateMapForTotalCount("priority", frCount.getTicketId());

			if (frCount.isLocked() && !isTicketClosed(frCount))
				updateMapForTotalCount("locked", frCount.getTicketId());
			else
			{
				if (frCount.getTicketStatus() != null
						&& !TicketStatus.COMPLETED_DEF
								.contains(frCount.getTicketStatus()))
					updateMapForTotalCount("unlocked", frCount.getTicketId());
			}

			if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId() == 3
					&& !isTicketClosed(frCount))
				updateMapForTotalCount("2_frequence_disconnection", frCount
						.getTicketId());

			if (frCount.getCurrentflowId() != null
					&& frCount.getCurrentflowId() == 4
					&& !isTicketClosed(frCount) )
				updateMapForTotalCount("3_slow_speed", frCount.getTicketId());

			if (AuthUtils.isFrManager())
			{
				if (frCount.getCurrentflowId() != null
						&& frCount.getCurrentflowId() == 5 && !isTicketClosed(frCount))
					updateMapForTotalCount("0_sr_element", frCount
							.getTicketId());
			}

			if (frCount.getTicketStatus() != null && frCount.getTicketStatus()
					.equals(FRStatus.FR_UNASSIGNEDT_TICKET) )
				updateMapForTotalCount("self", frCount.getTicketId());

			if (frCount.getReopenTicket() != null
					&& frCount.getReopenTicket() > 0
					&& !isTicketClosed(frCount) )
				updateMapForTotalCount("reopen", frCount.getTicketId());

			if (frCount.getUserId() != null && frCount.getUserId() > 0)
			{
				if (userHelper.isFRTLUser(frCount.getUserId()))
					updateMapForTotalCount("self", frCount.getTicketId());
			}

			if (frCount.getTicketStatus() != null)
			{
				if (frCount.getTicketStatus().equals((int)FRStatus.CCNR_APPROVAL_PENDING))
					updateMapForTotalCount("ccnr_pending", frCount
							.getTicketId());

				else if (frCount
						.getTicketStatus() .equals((int)FRStatus.SHIFTING_PERMISSION_PENDING))
					updateMapForTotalCount("ccnr_pending", frCount
							.getTicketId());

				if (frCount.getTicketStatus().equals((int)FRStatus.ODTR_PENDING))
					updateMapForTotalCount("odtr_pending", frCount
							.getTicketId());

				if (frCount.getTicketStatus().equals( (int)FRStatus.ELECTRICIAN_PENDING))
					updateMapForTotalCount("electrician_pending", frCount
							.getTicketId());

				if (frCount.getTicketStatus() .equals((int)FRStatus.ETR_ELAPSED))
					updateMapForTotalCount("etr_elapsed", frCount
							.getTicketId());

				if (frCount.getTicketStatus().equals((int)FRStatus.SPLICER_PENDING))
					updateMapForTotalCount("splicer_pending", frCount
							.getTicketId());

				if (frCount.getTicketStatus() .equals((int)FRStatus.SPLICING_PENDING))
					updateMapForTotalCount("splicing_pending", frCount
							.getTicketId());

			}
			/*
			 * this is the code for calculating Open/Close GART so set ticket
			 * creation date for Open GART and set both ticket creation and
			 * close Date for Closed GART and Total GART
			 * 
			 */

			if (frCount.getTicketStatus() != null && TicketStatus.COMPLETED_DEF
					.contains(frCount.getTicketStatus()))
			{

				GART = CommonUtil
						.getTicketGART(frCount.getTicketStatus(), frCount
								.getTicketCreationDate(), frCount
										.getTicketClosedDate());
				closedGART.put(frCount.getTicketId(), GART);
			} else
			{
				GART = CommonUtil.getTicketGART(frCount
						.getTicketStatus(), frCount.getTicketCreationDate());
				openGART.put(frCount.getTicketId(), GART);
			}

		}
		/*
		 * totalGART.putAll(closedGART); totalGART.putAll(openGART);
		 */

		calculateGART(openGART, "5_open_gart", "");
		calculateGART(closedGART, "6_closed_gart", "");
		// calculateGART(totalGART,"7_total_gart");

		summary.addAll(listOfPendingObject.values());
		return summary;
	}

	private static void setGARTValue(Map<String, FRSummaryCountVO> headMap,
			long avgGart, String typeGART, Map<Long, String> GARTMap)
	{
		FRSummaryCountVO sumary = null;
		List<Long> ticketIds = null;

		sumary = headMap.get(typeGART);

		ticketIds = sumary.getTicketIds();
		ticketIds.addAll(GARTMap.keySet());

		sumary.setTotalCounts(Integer.valueOf(avgGart + ""));
		sumary.setTicketIds(ticketIds);
		headMap.put(typeGART, sumary);
	}

	private static void calculateGART(Map<Long, String> GARTMap,
			String typeGART, String groupType)
	{
		String gart[] = null;
		if (!GARTMap.isEmpty())
		{
			long totalHours = 0;
			long totalMinut = 0;
			long avgMinut = 0;

			// FRSummaryCountVO sumary = null;
			// List<Long> ticketIds = null;

			for (Map.Entry<Long, String> entry : GARTMap.entrySet())
			{
				gart = entry.getValue().split(":");
				if (gart != null && gart.length > 1)
				{
					totalHours = Long.valueOf(gart[0]);
					totalMinut = Long.valueOf(gart[1]);

					avgMinut = avgMinut + totalMinut + (totalHours * 60);
				}
			}

			avgMinut = avgMinut / GARTMap.size();

			if ("UP".equals(groupType))

				setGARTValue(cxUpHeadCount, avgMinut, typeGART, GARTMap);

			else if ("DN".equals(groupType))
			{

				setGARTValue(cxDownHeadCount, avgMinut, typeGART, GARTMap);

			} else if ("SR".equals(groupType))

				setGARTValue(srElementHeadCount, avgMinut, typeGART, GARTMap);

			else if ("APP".equals(groupType))

				setGARTValue(appAndTlneWiseDetail, avgMinut, typeGART, GARTMap);
			else
				setGARTValue(listOfPendingObject, avgMinut, typeGART, GARTMap);

			/*
			 * sumary = listOfPendingObject.get(typeGART);
			 * 
			 * ticketIds = sumary.getTicketIds();
			 * ticketIds.addAll(GARTMap.keySet());
			 * 
			 * sumary.setTotalCounts(avgMinut); sumary.setTicketIds(ticketIds);
			 * listOfPendingObject.put(typeGART, sumary);
			 */

		}
	}

	public List<TypeAheadVo> getAllFrManagerTl()
	{
		List<TypeAheadVo> list = new ArrayList<TypeAheadVo>();
		Map<Long, String> users = new HashMap<Long, String>();
		TypeAheadVo headVo = null;

		if (AuthUtils.isAreaManager())
			users.putAll(userDao
					.getReportingUsers(AuthUtils.getCurrentUserId()));
		else if (AuthUtils.isBranchManager())
		{
			Map<Long, String> amUsers = userDao
					.getReportingUsers(AuthUtils.getCurrentUserId());
			for (Map.Entry<Long, String> entry : amUsers.entrySet())
			{
				users.putAll(userDao.getReportingUsers(entry.getKey()));
			}
		} else if (AuthUtils.isCityManager())
		{
			Map<Long, String> bmUsers = userDao
					.getReportingUsers(AuthUtils.getCurrentUserId());
			Map<Long, String> amUsers = null;
			for (Map.Entry<Long, String> bmEntry : bmUsers.entrySet())
			{
				amUsers = userDao.getReportingUsers(bmEntry.getKey());
				for (Map.Entry<Long, String> amId : amUsers.entrySet())
				{
					users.putAll(userDao.getReportingUsers(amId.getKey()));
				}
			}
		}

		if (!users.isEmpty())
		{
			for (Map.Entry<Long, String> tlIds : users.entrySet())
			{
				headVo = new TypeAheadVo(tlIds.getKey(), tlIds.getValue());
				list.add(headVo);
			}
		}

		return list;
	}

	private static Map<Long, String> getNeUserMap()
	{
		Map<Long, String> users = new HashMap<Long, String>();
		TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
		if (AuthUtils.isFRTLUser())
			return userDao.getReportingUsers(AuthUtils.getCurrentUserId());
		else if (AuthUtils.isAreaManager())
		{
			if (filter != null)
			{
				if (filter.getAssignedByUserId() != null
						&& filter.getAssignedByUserId() > 0)
				{
					Map<Long, String> nestedUsers = userDao
							.getNestedReportingUsers(filter
									.getAssignedByUserId(), null, null, null);
					users.putAll(nestedUsers);
				} else if (filter.getAreaId() != null && filter.getAreaId() > 0)
				{

					Area area = LocationCache.getAreaDAO()
							.getAreaById(filter.getAreaId());
					if (area != null)
					{
						List<BigInteger> reportTos = userDao
								.getReportToByBranchId(area.getBranch()
										.getId(), area.getId());
						if (reportTos != null && reportTos.size() > 0)
						{
							for (BigInteger reportTo : reportTos)
							{
								users.putAll(userDao
										.getNestedReportingUsers(reportTo
												.longValue(), null, null, null));
							}
						}

					}
				}

			} else
			{
				Map<Long, String> nestedUsers = userDao
						.getNestedReportingUsers(AuthUtils
								.getCurrentUserId(), null, null, null);
				users.putAll(nestedUsers);
			}

		}

		else if (AuthUtils.isBranchManager())
		{
			if (filter != null)
			{
				if (filter.getAssignedByUserId() != null
						&& filter.getAssignedByUserId() > 0)
				{
					Map<Long, String> nestedUsers = userDao
							.getNestedReportingUsers(filter
									.getAssignedByUserId(), null, null, null);
					users.putAll(nestedUsers);
				} else if (filter.getAreaId() != null && filter.getAreaId() > 0)
				{
					Area area = LocationCache.getAreaDAO()
							.getAreaById(filter.getAreaId());
					if (area != null)
					{

						List<BigInteger> reportTos = userDao
								.getReportToByBranchId(area.getBranch()
										.getId(), area.getId());
						if (reportTos != null && reportTos.size() > 0)
						{
							for (BigInteger reportTo : reportTos)
							{
								users.putAll(userDao
										.getNestedReportingUsers(reportTo
												.longValue(), null, null, null));
							}
						}
					}

				}

			} else
			{
				Map<Long, String> nestedUsers = userDao
						.getNestedReportingUsers(AuthUtils
								.getCurrentUserId(), null, null, null);
				users.putAll(nestedUsers);
			}
		} else if (AuthUtils.isCityManager())
		{
			if (filter != null)
			{
				if (filter.getBranchId() != null && filter.getBranchId() > 0)
				{
					List<BigInteger> reportTos = userDao
							.getReportToByBranchId(filter.getBranchId(), null);
					if (reportTos != null && reportTos.size() > 0)
					{
						for (BigInteger reportTo : reportTos)
						{
							users.putAll(userDao
									.getNestedReportingUsers(reportTo
											.longValue(), null, null, null));
						}
					}
				} else if (filter.getAssignedByUserId() != null
						&& filter.getAssignedByUserId() > 0)
				{
					Map<Long, String> nestedUsers = userDao
							.getNestedReportingUsers(filter
									.getAssignedByUserId(), null, null, null);
					users.putAll(nestedUsers);
				} else
					users.putAll(userDao.getNestedReportingUsers(AuthUtils
							.getCurrentUserId(), null, null, null));

			}
		}

		if (!users.isEmpty())
		{
			String name = "";
			for (Map.Entry<Long, String> entry : users.entrySet())
			{
				name = entry.getValue() != null ? entry.getValue().split(" ")[0]
						: "";
				if (!name.isEmpty())
					users.put(entry.getKey(), name);
			}
		}

		return users;

	}

	private static void getTlneWithNoTicket(Set<Long> keys,
			FRTicketSummaryCountVO dashBoard, String middle)
	{
		Map<Long, String> users = null;
		List<FRUserSummaryVO> cxUpDetails = dashBoard.getCxUpCountSummary();
		List<FRUserSummaryVO> cxDownDetails = dashBoard.getCxDownCountSummary();
		List<FRUserSummaryVO> priorityDetail = dashBoard
				.getPriorityCountSummary();
		List<FRUserSummaryVO> SrElementDetail = dashBoard
				.getSrElementUserDetails();

		if ("middle".equals(middle))
		{
			users = getNeUserMap();

			if (users != null && !users.isEmpty())
			{
				users.keySet().removeAll(keys);
				FRUserSummaryVO neDetails = null;
				Set<Long> tlneWithNoTick = users.keySet();

				for (Long frNeId : tlneWithNoTick)
				{
					neDetails = new FRUserSummaryVO();
					if (AuthUtils.isFrCxDownNEUser(frNeId))
					{
						neDetails.setOnline(1);
						neDetails.setTotalCount(0);
						neDetails.setUserId(frNeId);
						neDetails.setUserName(users.get(frNeId));
						neDetails
								.setCountSummary(setDetaultValueForAppAndTlWiseMap());
						cxDownDetails.add(neDetails);
					} else if (AuthUtils.isFrCxUpNEUser(frNeId))
					{
						neDetails.setOnline(1);
						neDetails.setTotalCount(0);
						neDetails.setUserId(frNeId);
						neDetails.setUserName(users.get(frNeId));
						neDetails
								.setCountSummary(setDetaultValueForAppAndTlWiseMap());
						cxUpDetails.add(neDetails);
					} else if (AuthUtils.isSRNEUser(frNeId))
					{
						neDetails.setOnline(1);
						neDetails.setTotalCount(0);
						neDetails.setUserId(frNeId);
						neDetails.setUserName(users.get(frNeId));
						neDetails
								.setCountSummary(setDetaultValueForAppAndTlWiseMap());
						SrElementDetail.add(neDetails);
					}
				}
				dashBoard.setCxDownCountSummary(cxDownDetails);
				dashBoard.setCxUpCountSummary(cxUpDetails);
				dashBoard.setSrElementUserDetails(SrElementDetail);
				return;
			}
			return;
		}
		Set<String> toBeShowInUiPriority = AppTicketCountUtil
				.getDashBoardSymptomCode("Priority",middle);
		Set<String> toBeShowInUiCxUp = AppTicketCountUtil
				.getDashBoardSymptomCode("CxUp",middle);
		Set<String> toBeShowInUiCxDown = AppTicketCountUtil
				.getDashBoardSymptomCode("CxDown",middle);
		Set<String> toBeShowInUiSr = AppTicketCountUtil
				.getDashBoardSymptomCode("SrElement",middle);

		users = getNeUserMap();

		Map<String, FRSummaryCountVO> defaultMap = new HashMap<String, FRSummaryCountVO>();

		if (users != null && !users.isEmpty())
		{
			users.keySet().removeAll(keys);
			FRUserSummaryVO neDetails = null;
			FRUserSummaryVO priority = null;
			Set<Long> tlneWithNoTick = users.keySet();
			for (Long frNeId : tlneWithNoTick)
			{
				neDetails = new FRUserSummaryVO();
				priority = new FRUserSummaryVO();
				if (AuthUtils.isFrCxDownNEUser(frNeId))
				{
					neDetails.setOnline(1);
					defaultMap.clear();
					neDetails
							.setCountSummary(setDefaultCountValue(defaultMap, toBeShowInUiCxDown));
					neDetails.setTotalCount(0);
					neDetails.setUserId(frNeId);
					neDetails.setUserName(users.get(frNeId));
					cxDownDetails.add(neDetails);
				} else if (AuthUtils.isFrCxUpNEUser(frNeId))
				{
					neDetails.setOnline(1);
					defaultMap.clear();
					neDetails
							.setCountSummary(setDefaultCountValue(defaultMap, toBeShowInUiCxUp));
					neDetails.setTotalCount(0);
					neDetails.setUserId(frNeId);
					neDetails.setUserName(users.get(frNeId));
					cxUpDetails.add(neDetails);
				} else if (AuthUtils.isSRNEUser(frNeId))
				{

					neDetails.setOnline(1);
					defaultMap.clear();
					neDetails
							.setCountSummary(setDefaultCountValue(defaultMap, toBeShowInUiSr));
					neDetails.setTotalCount(0);
					neDetails.setUserId(frNeId);
					neDetails.setUserName(users.get(frNeId));
					SrElementDetail.add(neDetails);
				}
				if (AuthUtils.isFrCxDownNEUser(frNeId)
						|| AuthUtils.isFrCxUpNEUser(frNeId)
						|| AuthUtils.isSRNEUser(frNeId))
				{
					neDetails.setOnline(1);
					defaultMap.clear();
					priority.setCountSummary(setDefaultCountValue(defaultMap, toBeShowInUiPriority));
					priority.setTotalCount(0);
					priority.setUserId(frNeId);
					priority.setUserName(users.get(frNeId));
					priorityDetail.add(priority);
				}
			}
			dashBoard.setCxDownCountSummary(cxDownDetails);
			dashBoard.setCxUpCountSummary(cxUpDetails);
			dashBoard.setPriorityCountSummary(priorityDetail);
			dashBoard.setSrElementUserDetails(SrElementDetail);
		}
	}

	private static Map<Long, List<FrTicketTlneDetailVo>> getTlneDetailsCountsMap(
			List<FrTicketTlneDetailVo> list)
	{
		Map<Long, List<FrTicketTlneDetailVo>> neTicketMap = new HashMap<Long, List<FrTicketTlneDetailVo>>();
		List<FrTicketTlneDetailVo> neticketList = null;

		Long userId = null;
		for (FrTicketTlneDetailVo summary : list)
		{
			userId = summary.getUserId();
			if (neTicketMap.containsKey(userId))
			{
				List<FrTicketTlneDetailVo> oldRibbon = neTicketMap.get(userId);
				oldRibbon.add(summary);
				neTicketMap.put(userId, oldRibbon);
			} else
			{
				neticketList = new ArrayList<FrTicketTlneDetailVo>();
				neticketList.add(summary);
				neTicketMap.put(userId, neticketList);
			}
		}
		return neTicketMap;
	}

	private static Map<Long, List<FRTicketCountSummaryVO>> getFrTlneDetailsMap(
			List<FRTicketCountSummaryVO> list)
	{
		Map<Long, List<FRTicketCountSummaryVO>> neTicketMap = new HashMap<Long, List<FRTicketCountSummaryVO>>();
		
		if( list == null || list.isEmpty() )
			return neTicketMap;
		
		List<FRTicketCountSummaryVO> neticketList = null;

		Long userId = null;
		for (FRTicketCountSummaryVO summary : list)
		{
			userId = summary.getUserId();
			if (neTicketMap.containsKey(userId))
			{
				List<FRTicketCountSummaryVO> oldRibbon = neTicketMap
						.get(userId);
				oldRibbon.add(summary);
				neTicketMap.put(userId, oldRibbon);
			} else
			{
				neticketList = new ArrayList<FRTicketCountSummaryVO>();
				neticketList.add(summary);
				neTicketMap.put(userId, neticketList);
			}
		}
		return neTicketMap;
	}

	private static boolean isTicketClosed( FRTicketCountSummaryVO first )
	{
		if( first != null && first.getTicketStatus() != null 
				&& TicketStatus.COMPLETED_DEF.contains(first.getTicketStatus()))
			return true;
		else
			return false;
	}
	
	public static FrTicketDao getFrTicketDao()
	{
		return frTicketDao;
	}

	public static void setFrTicketDao(FrTicketDao frTicketDao)
	{
		FrTicketCountUtils.frTicketDao = frTicketDao;
	}

	public static UserDao getUserDao()
	{
		return userDao;
	}

	public static void setUserDao(UserDao userDao)
	{
		FrTicketCountUtils.userDao = userDao;
	}

	public static Integer getBaseValue()
	{
		return baseValue;
	}

	public static void setBaseValue(Integer baseValue)
	{
		FrTicketCountUtils.baseValue = baseValue;
	}

	public static UserHelper getUserHelper()
	{
		return userHelper;
	}

	public static void setUserHelper(UserHelper userHelper)
	{
		FrTicketCountUtils.userHelper = userHelper;
	}

}