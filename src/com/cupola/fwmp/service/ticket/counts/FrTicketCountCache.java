package com.cupola.fwmp.service.ticket.counts;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cupola.fwmp.vo.counts.dashboard.FRDashBoardCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketSummaryCountVO;

public class FrTicketCountCache
{
	private static final Logger LOGGER  = Logger.getLogger(FrTicketCountCache.class);
	
	private static Map<Long, FRDashBoardCountVO> userFaultRepairTicketCache = 
			new HashMap<Long, FRDashBoardCountVO>(); 
	
	private static long cacheAgeInMillis =  2 * 60 * 1000;
	private static long cacheResetTimestamp = 0;

	private static  void resetCacheIfOld()
	{
		if ((System.currentTimeMillis() - cacheResetTimestamp) > cacheAgeInMillis)
		{
			resetAllCache();
			LOGGER .info("Fr Ticket count cache old so refreshing...");
		}
	}
	
	private static  void resetAllCache()
	{
		userFaultRepairTicketCache.clear();
		cacheResetTimestamp = System.currentTimeMillis();
		LOGGER .info("refreshing Fr Ticket count cache done...");
	}
	
	public static void refreshCache()
	{
		LOGGER .info("Fr Ticket cache is refreshing...");
		resetAllCache();
	}
	
	public static  FRDashBoardCountVO getCountSummary(Long userId)
	{
		/*resetCacheIfOld();
		FRDashBoardCountVO frVo = userFaultRepairTicketCache.get(userId);
		if(frVo == null)*/
		return	updateFrTicketCountCahe(userId);
		
		//return userFaultRepairTicketCache.get(userId);
	}
	
	private static FRDashBoardCountVO updateFrTicketCountCahe(Long userId)
	{
//		FRDashBoardCountVO dashBoardVo = FrTicketCountUtils.
//				calculateFrTicketCountForUser(userId);
		return FrTicketCountUtils.getDashBoradCountBasedOnGroup( userId ) ;
	//	userFaultRepairTicketCache.put(userId, dashBoardVo);
	}

	public static FRTicketSummaryCountVO getFrUserSummaryCount(long currentUserId)
	{
		return FrTicketCountUtils.getFrUserSummaryCount(currentUserId);
	}
	
	public static FRTicketSummaryCountVO getFrTiketTlneDetailCounts(Long reportTo)
	{
		return FrTicketCountUtils.getFrTiketTlneDetailCounts(reportTo);
	}

}
