package com.cupola.fwmp.service.ticket;

import java.util.Comparator;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketCountSummaryVO;

public class FrTicketCardViewComp implements Comparator<FRTicketCountSummaryVO>
{

	@Override
	public int compare( FRTicketCountSummaryVO first, FRTicketCountSummaryVO second )
	{
		if( !first.isLocked() && !second.isLocked() )
		{
			if( isTicketClosed(first) && isTicketClosed(second) )
			{
				if( isFirstClosedRecently(first, second))
					return -1;
				else
					return 1 ;
			}
			else if( !isTicketClosed(first) && isTicketClosed(second) )
				return -1;
			else if( isTicketClosed(first) && !isTicketClosed(second) )
				return 1;
			else if( !isTicketClosed(first) && !isTicketClosed(second) )
			{
				if( isFirstHigherPriority( first, second ))
					return -1;
				else if( isFirstHigherPriority(second ,first))
					return 1;
				else if ( isBothEqualPriority(first, second) && isFirstHigherGART(first, second))
					return -1;
				else if ( isBothEqualPriority(first, second) && isFirstHigherGART( second, first ))
					return 1;
			}
		
		}
		else if( !first.isLocked() && second.isLocked() )
			return -1;
		else if( first.isLocked() && !second.isLocked() )
			return 1;
		else if( first.isLocked() && second.isLocked() )
		{
			if( isFirstHigherPriority( first, second ))
				return -1;
			else if( isFirstHigherPriority(second ,first))
				return 1;
			else if ( isBothEqualPriority(first, second) && isFirstHigherGART(first, second))
				return -1;
			else if ( isBothEqualPriority(first, second) && isFirstHigherGART( second, first ))
				return 1;
		}
		return 0;
	}

	public boolean isTicketClosed( FRTicketCountSummaryVO first )
	{
		if( first != null && first.getTicketStatus() != null 
				&& TicketStatus.COMPLETED_DEF.contains(first.getTicketStatus()))
			return true;
		else
			return false;
	}
	
	public boolean isFirstClosedRecently( FRTicketCountSummaryVO first, FRTicketCountSummaryVO second )
	{
		if( first != null && second != null )
		{
			long firstClosed = first.getTicketClosedDate() != null ?
					first.getTicketClosedDate().getTime() : 0;
					
			long secondClosed = second.getTicketClosedDate() != null ?
					second.getTicketClosedDate().getTime() : 0;
					
			if( firstClosed >= secondClosed )
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	public boolean isFirstHigherPriority( FRTicketCountSummaryVO first, FRTicketCountSummaryVO second )
	{
		if( first != null && second != null )
		{
			int firstPriority = first.getPriorityValue() != null ?
					first.getPriorityValue().intValue() : 0;
					
			int secondPriority = second.getPriorityValue() != null ?
					second.getPriorityValue().intValue() : 0;
					
			if( firstPriority > secondPriority )
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	public boolean isBothEqualPriority( FRTicketCountSummaryVO first, FRTicketCountSummaryVO second )
	{
		if( first != null && second != null )
		{
			int firstPriority = first.getPriorityValue() != null ?
					first.getPriorityValue().intValue() : 0;
					
			int secondPriority = second.getPriorityValue() != null ?
					second.getPriorityValue().intValue() : 0;
					
			if( firstPriority == secondPriority )
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	public boolean isFirstHigherGART( FRTicketCountSummaryVO first, FRTicketCountSummaryVO second )
	{
		if( first != null && second != null )
		{
			long firstClosed = first.getTicketCreationDate() != null ?
					first.getTicketCreationDate().getTime() : 0;
					
			long secondClosed = second.getTicketCreationDate() != null ?
					second.getTicketCreationDate().getTime() : 0;
					
			if( firstClosed < secondClosed )
				return true;
			else
				return false;
		}
		else
			return false;
	}
}
