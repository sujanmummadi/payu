package com.cupola.fwmp.service.location;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.LocationType;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.subArea.SubAreaDAO;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.vo.LocationTypeVo;

public class LocationCache
{

	static final Logger logger = Logger.getLogger(LocationCache.class);

	private static SubAreaDAO subAreaDAO;
	private static AreaDAO areaDAO;
	private static BranchDAO branchDAO;
	private static CityDAO cityDAO;

	private static Map<Long, String> cityCache = new HashMap<Long, String>();
	private static Map<Long, String> cityCodeCache = new HashMap<Long, String>();
	private static Map<Long, String> branchCache = new HashMap<Long, String>();
	private static Map<Long, String> areaCache = new HashMap<Long, String>();
	private static Map<Long, String> subAreaCache = new HashMap<Long, String>();

	private static Map<Long, Map<Long, String>> cityBranchCache = new HashMap<Long, Map<Long, String>>();
	private static Map<Long, Map<Long, String>> branchAreaCache = new HashMap<Long, Map<Long, String>>();
	private static Map<Long, Map<Long, String>> areaSubAreaCache = new HashMap<Long, Map<Long, String>>();
	private static Map<Long, Map<Long, String>> cityAreaCache = new HashMap<Long, Map<Long, String>>();

	private static long cacheAgeInMillis;
	private static long cacheResetTimestamp = 0;

	private static void resetCacheIfOld()
	{
		/*
		 * if ((System.currentTimeMillis() - cacheResetTimestamp) >
		 * cacheAgeInMillis) { reFreshCache(); }
		 */
	}

	public static Map<Long, String> getAllCity()
	{
		resetCacheIfOld();
		return cityCache;
	}

	public static Map<Long, String> getAllArea()
	{
		resetCacheIfOld();
		return areaCache;
	}

	public static Map<Long, String> getAllBranches()
	{
		resetCacheIfOld();
		return branchCache;
	}

	public static Map<Long, String> getBranchesByCityId(String cityId)
	{
		resetCacheIfOld();
		
		if (cityId != null && !cityId.isEmpty())
			return cityBranchCache.get(Long.valueOf(cityId));
		
		return null;
	}
	

	public static String getAreaNameById(Long id)
	{
		resetCacheIfOld();
		return areaCache.get(id);
	}

	public static Map<Long, String> getAllAreaWithId()
	{
		resetCacheIfOld();
		return areaCache;
	}

	public static String getBrancheNameById(Long id)
	{
		resetCacheIfOld();
		return branchCache.get(id);
	}

	public static String getCityNameById(Long id)
	{
		resetCacheIfOld();
		return cityCache.get(id);
	}

	public static String getCityCodeById(Long id)
	{
		resetCacheIfOld();
		return cityCodeCache.get(id);
	}

	public static Map<Long, String> getAreasByBranchId(String branchId)
	{

		resetCacheIfOld();
		return branchAreaCache.get(Long.valueOf(branchId));
	}

	public static Map<Long, String> getAreasByCityId(String cityId)
	{
		resetCacheIfOld();
		Map<Long, String> branchList = cityBranchCache
				.get(Long.valueOf(cityId));
		Map<Long, String> areas = new HashMap<Long, String>();
		for (Map.Entry<Long, String> branchVO : branchList.entrySet())
		{
			areas.putAll(branchAreaCache.get(branchVO.getKey()));
		}

		return areas;
	}

	public static Map<Long, String> getSubAreasByAreaId(Long areaId)
	{
		resetCacheIfOld();
		return areaSubAreaCache.get(areaId);
	}

	public static void resetCache()
	{
		cityBranchCache.clear();
		branchAreaCache.clear();
		areaSubAreaCache.clear();
		cityCache.clear();
		cityCodeCache.clear();
		branchCache.clear();
		areaCache.clear();
		subAreaCache.clear();
		cacheResetTimestamp = System.currentTimeMillis();
		logger.info("LocationCache cache reset done");
	}

	public static void reFreshCache()
	{
		resetCache();
		Map<Long, String> cities = cityDAO.getAllCity();
		for (Map.Entry<Long, String> cityVO : cities.entrySet())
		{
			branchDAO.getRealtyBranchByBranchNameAndCityName(cityVO.getValue(),FWMPConstant.REALTY);
			
			cityCache.put(cityVO.getKey(), cityVO.getValue());

			cityCodeCache.put(cityVO.getKey(), cityDAO
					.getCityCodeById(cityVO.getKey()));

			Map<Long, String> branchList = branchDAO
					.getBranchesByCityId(cityVO.getKey() + "");
			Map<Long, String> localBranch = new HashMap<Long, String>();
			for (Map.Entry<Long, String> branchVO : branchList.entrySet())
			{
				localBranch.put(branchVO.getKey(), branchVO.getValue());

				Map<Long, String> areas = areaDAO
						.getAreasByBranchId(branchVO.getKey() + "");
				areaCache.putAll(areas);
				branchAreaCache.put(branchVO.getKey(), areas);

				for (Map.Entry<Long, String> area : areas.entrySet())
				{
					Map<Long, String> subAreas = subAreaDAO
							.getSubAreasByAreaId(area.getKey());
					subAreaCache.putAll(subAreas);
					areaSubAreaCache.put(area.getKey(), subAreas);
				}
			}
			branchCache.putAll(localBranch);
			cityBranchCache.put(cityVO.getKey(), localBranch);

		}

	}
	
	public static void updateLocationCache(LocationTypeVo locationType)
	{
		if(locationType != null)
		{
			if(locationType.getUpdateCacheCode() == LocationType.city)
			{
				City city = locationType.getCity();
				if(city != null)
				{
					cityCache.put(city.getId(), city.getCityName());

					cityCodeCache.put(city.getId(), city.getCityCode());
				}
			}
			if(locationType.getUpdateCacheCode() == LocationType.branch)
			{
				Branch branch = locationType.getBranch();
				if(branch != null)
				{
					if (!branchCache.containsKey(branch.getId()))
						branchCache.put(branch.getId(), branch.getBranchName());
					
					if(cityBranchCache.containsKey(branch.getCity().getId()))
					{
						Map<Long, String> localBranch =cityBranchCache.get(branch.getCity().getId());
						localBranch.put(branch.getId(),branch.getBranchName());
						//cityBranchCache.put(branch.getCity().getId(), localBranch);
					}
					
					else{
					      Map<Long, String> localBranch = new HashMap<Long, String>();
					      localBranch.put(branch.getId(),branch.getBranchName());
					      cityBranchCache.put(branch.getCity().getId(), localBranch);
					     }
					
				
				}
			}
			if(locationType.getUpdateCacheCode() == LocationType.area)
			{
				Area area = locationType.getArea();
				if(area != null)
				{
					if (!areaCache.containsKey(area.getId()))
						areaCache.put(area.getId(), area.getAreaName());

					if (branchAreaCache.containsKey(area.getBranch().getId()))
					{
						
						Map<Long, String> localareas = branchAreaCache.get(area.getBranch().getId());
						localareas.put(area.getId(), area.getAreaName());
						branchAreaCache.put(area.getBranch().getId(), localareas);

					}
					
					else{
					      Map<Long, String> localareas = new HashMap<Long, String>();
					      localareas.put(area.getId(), area.getAreaName());
					      branchAreaCache.put(area.getBranch().getId(), localareas);
					     }
					
				}
			}
		}
		
	}
	
	
	public static SubAreaDAO getSubAreaDAO()
	{
		return subAreaDAO;
	}

	public static void setSubAreaDAO(SubAreaDAO subAreaDAO)
	{
		LocationCache.subAreaDAO = subAreaDAO;
	}

	public static AreaDAO getAreaDAO()
	{
		return areaDAO;
	}

	public static void setAreaDAO(AreaDAO areaDAO)
	{
		LocationCache.areaDAO = areaDAO;
	}

	public static BranchDAO getBranchDAO()
	{
		return branchDAO;
	}

	public static void setBranchDAO(BranchDAO branchDAO)
	{
		LocationCache.branchDAO = branchDAO;
	}

	public static CityDAO getCityDAO()
	{
		return cityDAO;
	}

	public static void setCityDAO(CityDAO cityDAO)
	{
		LocationCache.cityDAO = cityDAO;
	}

	public static long getCacheAgeInMillis()
	{
		return cacheAgeInMillis;
	}

	public static void setCacheAgeInMillis(long cacheAgeInMillis)
	{
		LocationCache.cacheAgeInMillis = cacheAgeInMillis;
	}

	public static String getSubAreasBySubAreaId(long subAreaId) {
		if(subAreaId > 0){
			if(subAreaCache.containsKey(subAreaId))
				return subAreaCache.get(subAreaId);
			else
				return null;
		} else
			return null;
	}

}
