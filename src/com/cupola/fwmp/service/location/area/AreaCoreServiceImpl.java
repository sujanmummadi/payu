package com.cupola.fwmp.service.location.area;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.LocationType;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.location.city.CityCoreService;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.AreaVO;
import com.cupola.fwmp.vo.BranchVO;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public class AreaCoreServiceImpl implements AreaCoreService
{

	private Logger log = Logger.getLogger(AreaCoreServiceImpl.class.getName());

	AreaDAO areaDao;

	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	CityDAO cityDAO;

	@Autowired
	CityCoreService cityCoreService;

	public void setAreaDao(AreaDAO areaDao)
	{
		this.areaDao = areaDao;
	}

	@Override
	public APIResponse addAreaes(List<AreaVO> areaVOs)
	{

		if (areaVOs == null)

			return ResponseUtil.createNullParameterResponse();

		for (AreaVO areaVO : areaVOs)
		{
			Area area = new Area();
			log.info("area name   " + areaVO.getAreaName());
			BeanUtils.copyProperties(areaVO, area);
			BranchVO branchVo = branchDAO.getBranchById(areaVO.getBranchId());

			Branch branch = new Branch();
			if (branch != null)
			{
				BeanUtils.copyProperties(branchVo, branch);
			}
			/*
			 * if(areaVO.getBranchName().equalsIgnoreCase("default")){ log.info(
			 * "branch   "+branch.getBranchName()); }
			 */
			area.setBranch(branch);
			areaDao.addArea(area);
		}
		cityCoreService.resetLocationCache();
		return ResponseUtil.createSaveSuccessResponse();
	}

	@Override
	public APIResponse addArea(AreaVO areaVO)
	{

		if (areaVO == null)

			return ResponseUtil.createNullParameterResponse();

		Area area = new Area();
		log.info("area name   " + areaVO.getAreaName());
		BeanUtils.copyProperties(areaVO, area);
		BranchVO branchVo = branchDAO.getBranchById(areaVO.getBranchId());

		Branch branch = new Branch();
		if (branch != null)
		{
			BeanUtils.copyProperties(branchVo, branch);
		}

		CityVO cityVo = cityDAO.getCityById(branch.getId());

		if (cityVo != null)
		{
			City city = new City();
			BeanUtils.copyProperties(cityVo, city);

			branch.setCity(city);

		}

		area.setBranch(branch);
		AreaVO vo = areaDao.addArea(area);

		cityCoreService.updateLocationCache(null,null,area,LocationType.area);
		return ResponseUtil.createSaveSuccessResponse()
				.setData(vo.getRemarks());
	}

	@Override
	public APIResponse getAreaById(Area id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getAllArea()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse deleteArea(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse UpdateArea(Area area)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TypeAheadVo> getAreasByBranchId(String branchId)
	{

		return CommonUtil.xformToTypeAheadFormat(LocationCache
				.getAreasByBranchId(branchId));

	}

	@Override
	public List<TypeAheadVo> getAreasByCityId(String cityId)
	{

		return CommonUtil
				.xformToTypeAheadFormat(LocationCache.getAreasByCityId(cityId));

	}

	@Override
	public List<Long> getAreaIds()
	{
		return areaDao.getAllAreaIds();
	}

	@Override
	public List<TypeAheadVo> getAllAreas()
	{

		return CommonUtil.xformToTypeAheadFormat(LocationCache.getAllArea());

	}
}