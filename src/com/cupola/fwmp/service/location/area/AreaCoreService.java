package com.cupola.fwmp.service.location.area;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.AreaVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public interface AreaCoreService
{

	public APIResponse addArea(AreaVO areaVOs);

	public APIResponse getAreaById(Area id);

	public APIResponse getAllArea();

	public APIResponse deleteArea(Long id);

	public APIResponse UpdateArea(Area area);

	public List<TypeAheadVo> getAreasByBranchId(String branchId);

	public List<Long> getAreaIds();

	List<TypeAheadVo> getAreasByCityId(String cityId);

	public List<TypeAheadVo> getAllAreas();

	/**
	 * @author aditya 11:53:10 AM Apr 10, 2017 APIResponse
	 * @param areaVOs
	 * @return
	 */
	public APIResponse addAreaes(List<AreaVO> areaVOs);

}
