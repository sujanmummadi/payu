package com.cupola.fwmp.service.location.branch;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.BranchVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public interface BranchCoreService
{

	public APIResponse getBranchById(Long id);

	public APIResponse getAllBranch();

	public APIResponse deleteBranch(Long id);

	public APIResponse UpdateBranch(Branch branch);

	List<TypeAheadVo> getBranchesByCityId(String cityId);

	public List<TypeAheadVo> getAllBranches();

	public APIResponse manageFlowByBranchId(Long branchId, Long value);

	public APIResponse getBranchDetailBy(Long branchIdOrCityId);
	
	
	/**
	 * @author aditya 11:44:14 AM Apr 10, 2017 APIResponse
	 * @param branchVOs
	 * @return
	 */
	APIResponse addBranches(List<BranchVO> branchVOs);

	/**
	 * @author aditya 11:44:36 AM Apr 10, 2017 APIResponse
	 * @param branchVOs
	 * @return
	 */
	APIResponse addBranch(BranchVO branchVOs);
}
