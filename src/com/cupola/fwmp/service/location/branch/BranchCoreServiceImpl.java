package com.cupola.fwmp.service.location.branch;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.LocationType;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.location.city.CityCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.BranchVO;
import com.cupola.fwmp.vo.CityDeatilVo;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public class BranchCoreServiceImpl implements BranchCoreService
{

	private Logger log = Logger
			.getLogger(BranchCoreServiceImpl.class.getName());

	BranchDAO branchDao;

	@Autowired
	CityDAO cityDAO;
	
	@Autowired
	CityCoreService cityCoreService;

	public void setBranchDao(BranchDAO branchDao)
	{
		this.branchDao = branchDao;
	}

	@Override
	public APIResponse addBranch(BranchVO branchVO)
	{

		if (branchVO == null)
		{
			log.info("Branch should not be null");
			return ResponseUtil.createNullParameterResponse();
		}

		Branch branch = new Branch();

		BeanUtils.copyProperties(branchVO, branch);

		CityVO cityVo = cityDAO.getCityById(branchVO.getCityId());

		if (cityVo != null)
		{
			City city = new City();
			BeanUtils.copyProperties(cityVo, city);

			branch.setCity(city);

		}
		BranchVO vo = branchDao.addBranch(branch);

		cityCoreService.updateLocationCache(null, branch, null, LocationType.branch);
		return ResponseUtil.createSaveSuccessResponse()
				.setData(vo.getRemarks());
	}

	@Override
	public APIResponse addBranches(List<BranchVO> branchVOs)
	{

		if (branchVOs == null)
		{
			log.info("Branch should not be null");
			return ResponseUtil.createNullParameterResponse();
		}

		for (BranchVO branchVO : branchVOs)
		{
			Branch branch = new Branch();

			BeanUtils.copyProperties(branchVO, branch);

			CityVO cityVo = cityDAO.getCityById(branchVO.getCityId());

			if (cityVo != null)
			{

				log.debug("cityvo******* " + cityVo);

				City city = new City();

				BeanUtils.copyProperties(cityVo, city);

				branch.setCity(city);
			}

			branchDao.addBranch(branch);

		}

		cityCoreService.resetLocationCache();
		return ResponseUtil.createSaveSuccessResponse();
	}

	@Override
	public List<TypeAheadVo> getBranchesByCityId(String cityId)
	{

		return CommonUtil.xformToTypeAheadFormat(LocationCache
				.getBranchesByCityId(cityId));

	}

	@Override
	public List<TypeAheadVo> getAllBranches()
	{

		return CommonUtil
				.xformToTypeAheadFormat(LocationCache.getAllBranches());

	}

	@Override
	public APIResponse getBranchById(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getAllBranch()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse deleteBranch(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse UpdateBranch(Branch branch)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse manageFlowByBranchId(Long branchId, Long value)
	{
		if (branchId == null || branchId <= 0 || value == null)
			return ResponseUtil.nullArgument();

		String returnValue = "";
		String flowSwitch = "";

		TypeAheadVo cityVO = AuthUtils.getCurrentUserCity();

		if (value.longValue() == 0)
		{
			flowSwitch = "0";
			returnValue = "1";

			if (cityVO != null && branchId.equals(cityVO.getId()))
			{
				cityDAO.updateCityFlowSwitch(branchId, flowSwitch);
				branchDao.updateBranchFlowSwitchById(branchId, flowSwitch);

				return ResponseUtil.createSuccessResponse()
						.setData(returnValue);
			} else
			{
				branchDao.updateBranchFlowSwitchById(branchId, flowSwitch);
				return ResponseUtil.createSuccessResponse()
						.setData(returnValue);
			}
		} else if (value.longValue() == 1)
		{
			flowSwitch = "1";
			returnValue = "0";

			if (cityVO != null && branchId.equals(cityVO.getId()))
			{
				branchDao.updateBranchFlowSwitchById(branchId, flowSwitch);
				cityDAO.updateCityFlowSwitch(branchId, flowSwitch);
				return ResponseUtil.createSuccessResponse()
						.setData(returnValue);
			} else
			{
				branchDao.updateBranchFlowSwitchById(branchId, flowSwitch);

				CityVO vo = null;
				if(cityVO != null)
				 vo = cityDAO.getCityById(cityVO.getId());
				if (vo != null && vo.getFlowSwitch().equals("0"))
					cityDAO.updateCityFlowSwitch(cityVO.getId(), flowSwitch);

				return ResponseUtil.createSuccessResponse()
						.setData(returnValue);
			}

		} else
			return ResponseUtil.createUpdateFailedResponse()
					.setData("Not a valid Value : it should be 0 or 1");

	}

	@Override
	public APIResponse getBranchDetailBy(Long branchIdOrCityId)
	{
		if (branchIdOrCityId == null || branchIdOrCityId <= 0)
			return ResponseUtil.nullArgument();

		try
		{
			boolean isCityFound = true;

			TypeAheadVo cityVO = AuthUtils.getCurrentUserCity();
			CityDeatilVo cityDetail = new CityDeatilVo();

			if (cityVO != null && cityVO.getId() > 0)
			{
				CityVO vo = cityDAO.getCityById(cityVO.getId());
				if (vo != null)
				{
					cityDetail.setCityId(vo.getId());
					cityDetail.setCityFlowSwitch(vo.getFlowSwitch());
					cityDetail.setBranchDetail(branchDao
							.getAllBranchDetail(branchIdOrCityId));
				} else
				{
					log.info("City not found for branch id : "
							+ branchIdOrCityId);
					isCityFound = false;
				}
			} else
			{
				log.error("City is not valid for User with branch or City ID : "
						+ branchIdOrCityId);
				isCityFound = false;
			}
			if (isCityFound)
				return ResponseUtil.createSuccessResponse().setData(cityDetail);
		} catch (Exception e)
		{
			log.error("Error occure while fetching city branch detail" + e);
		}

		return ResponseUtil.createSuccessResponse()
				.setData("City is not valid for User branch or city id :"
						+ branchIdOrCityId);
	}

}