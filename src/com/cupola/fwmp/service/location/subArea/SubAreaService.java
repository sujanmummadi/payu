package com.cupola.fwmp.service.location.subArea;

import java.util.List;

import com.cupola.fwmp.persistance.entities.SubArea;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TypeAheadVo;

public interface SubAreaService{
	

	public APIResponse addSubArea(SubArea subArea);

	public APIResponse getSubAreaById(Long id);

	public APIResponse getAllSubArea();

	public APIResponse deleteSubArea(Long id);

	public APIResponse updateSubArea(SubArea subArea);

	public List<TypeAheadVo> getSubAreasByAreaId(Long areaId);

}
