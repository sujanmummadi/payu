package com.cupola.fwmp.service.location.subArea;

import java.util.List;

import com.cupola.fwmp.dao.subArea.SubAreaDAO;
import com.cupola.fwmp.persistance.entities.SubArea;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.vo.TypeAheadVo;

public class SubAreaServiceImpl implements SubAreaService {

	SubAreaDAO subAreaDAO;
	
	public void setSubAreaDAO(SubAreaDAO subAreaDAO) {
		this.subAreaDAO = subAreaDAO;
	}

	@Override
	public APIResponse addSubArea(SubArea subArea) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getSubAreaById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getAllSubArea() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse deleteSubArea(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateSubArea(SubArea subArea) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TypeAheadVo> getSubAreasByAreaId(Long areaId) {
		
		return CommonUtil.xformToTypeAheadFormat(LocationCache.getSubAreasByAreaId(areaId));
				
	
	}

}
