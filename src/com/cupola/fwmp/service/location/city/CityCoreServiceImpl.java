package com.cupola.fwmp.service.location.city;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.BeanUtils;

import com.cupola.fwmp.FWMPConstant.LocationType;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.vo.LocationTypeVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;

public class CityCoreServiceImpl implements CityCoreService
{

	private Logger log = Logger.getLogger(CityCoreServiceImpl.class.getName());

	private String resetUserUrl;
	private String servers;

	public String getResetUserUrl()
	{
		return resetUserUrl;
	}

	public void setResetUserUrl(String resetUserUrl)
	{
		this.resetUserUrl = resetUserUrl;
	}

	public String getServers()
	{
		return servers;
	}

	public void setServers(String servers)
	{
		this.servers = servers;
	}

	CityDAO cityDAO;

	public CityDAO getCityDAO()
	{
		return cityDAO;
	}

	public void setCityDAO(CityDAO cityDAO)
	{
		this.cityDAO = cityDAO;
	}

	@Override
	public APIResponse addCities(List<CityVO> cityVOs)
	{

		if (cityVOs == null)
		{
			log.info("city should not be null");
			return ResponseUtil.createNullParameterResponse();
		}

		for (CityVO cityVO : cityVOs)
		{
			City city = new City();

			BeanUtils.copyProperties(cityVO, city);

			CityVO cityVO1 = cityDAO.addCity(city);

		}
		resetLocationCache();
		return ResponseUtil.createSaveSuccessResponse();

	}

	@Override
	public APIResponse addCity(CityVO cityVO)
	{

		if (cityVO == null)
		{
			log.info("city should not be null");
			return ResponseUtil.createNullParameterResponse();
		}

		City city = new City();

		BeanUtils.copyProperties(cityVO, city);

		CityVO cityVO1 = cityDAO.addCity(city);
		updateLocationCache(city,null,null,LocationType.city);
		return ResponseUtil.createSaveSuccessResponse()
				.setData(cityVO1.getRemarks());

	}

	@Override
	public APIResponse getCityById(Long id)
	{
		CityVO getCityBYId = cityDAO.getCityById(id);

		return null;
	}

	@Override
	public List<TypeAheadVo> getAllCity()
	{

		return CommonUtil.xformToTypeAheadFormat(LocationCache.getAllCity());

	}

	@Override
	public APIResponse deleteCity(Long id)
	{
		City deletedCity = cityDAO.deleteCity(id);
		return null;
	}

	@Override
	public APIResponse updateCity(City city)
	{
		City updaetdCity = cityDAO.updateCity(city);
		return null;
	}

	@Override
	public List<TypeAheadVo> getAllCitiesCorrespondingToState()
	{
		if (!AuthUtils.isAdmin())
		{
			return Collections.singletonList(AuthUtils.getCurrentUserCity());
		}

		String stateName = cityDAO
				.getStateNameByCityId(AuthUtils.getCurrentUserCity().getId());
		if (stateName == null)
			return null;
		return CommonUtil.xformToTypeAheadFormat(cityDAO
				.getAllCitiesCorrespondingToState(stateName));
	}

	@Override
	public List<Long> getAllCityIds()
	{
		return cityDAO.getAllCityIds();
	}

	@Override
	public void resetLocationCache()
	{
		String serv = servers;

		String[] serverArr = serv.split(",");

		for (int i = 0; i < serverArr.length; i++)
		{
			String serverIp = serverArr[i];
			try
			{
				log.info("server  reseting cache " + serverIp);

				APIResponse apiResponse = addToCacheServiceCall(serverIp);

				log.info("server " + serverIp + " apiResponse " + apiResponse);
			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("server got error while reseting cache " + serverIp
						+ " . " + e.getMessage());
				continue;
			}
		}
	}

	private APIResponse addToCacheServiceCall(String serverIp)
	{
		APIResponse apiResponse = null;
		log.info("Reset Location cache ############### " + resetUserUrl);

		String resetUserUrlWithIp = resetUserUrl.replace("localhost", serverIp);

		log.info("Reset Location cache ############### " + resetUserUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetUserUrlWithIp);

			String mainQueueString = "";

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			apiResponse = ResponseUtil.createSuccessResponse()
					.setData(stringResponse);

			log.info("Reset Location cache response " + stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			log.error("Error while Location city cache " + e.getMessage());
			e.printStackTrace();

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				
				if(httpclient != null)
				httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse Reset Location cache. "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

		return apiResponse;

	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.location.city.CityCoreService#updateLocationCache(com.cupola.fwmp.persistance.entities.Area)
	 */
	@Override
	public void updateLocationCache(City city,Branch branch ,Area area,int cacheUpdateCode)
	{
		LocationTypeVo locationTypeVo = new LocationTypeVo();
		if(city != null)
			locationTypeVo.setCity(city);
		if(branch != null)
			locationTypeVo.setBranch(branch);
		if(area != null)
			locationTypeVo.setArea(area);
		if(cacheUpdateCode > 0)
			locationTypeVo.setUpdateCacheCode(cacheUpdateCode);

		String serv = servers;

		String[] serverArr = serv.split(",");

		for (int i = 0; i < serverArr.length; i++)
		{
			String serverIp = serverArr[i];
			try
			{
				log.info("server  reseting cache " + serverIp);

				APIResponse apiResponse = addToLocationCacheServiceCall(locationTypeVo,serverIp);

				log.info("server " + serverIp + " apiResponse " + apiResponse);
			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("server got error while reseting cache " + serverIp
						+ " . " + e.getMessage());
				continue;
			}
		}
	
	}

	/**@author kiran
	 * APIResponse
	 * @param area
	 * @param serverIp
	 * @return
	 */
	private APIResponse addToLocationCacheServiceCall(LocationTypeVo locationTypeVo,
			String serverIp)
	{
		APIResponse apiResponse = null;
		log.info("Reset Location cache ############### " + resetUserUrl);

		String resetUserUrlWithIp = resetUserUrl.replace("localhost", serverIp);

		log.info("Reset Location cache ############### " + resetUserUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			ObjectMapper mapper = new ObjectMapper();
			
			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetUserUrlWithIp);

			String mainQueueString =  mapper.writeValueAsString(locationTypeVo);

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			apiResponse = ResponseUtil.createSuccessResponse()
					.setData(stringResponse);

			log.info("Reset Location cache response " + stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			log.error("Error while Location city cache " + e.getMessage());
			e.printStackTrace();

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();

				if(httpclient != null)
				httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse Reset Location cache. "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

		return apiResponse;

	}
}
