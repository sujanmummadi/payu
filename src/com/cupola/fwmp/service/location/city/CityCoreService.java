package com.cupola.fwmp.service.location.city;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public interface CityCoreService
{

	public APIResponse addCity(CityVO cityVOs);

	public APIResponse getCityById(Long id);

	public List<TypeAheadVo> getAllCity();

	public APIResponse deleteCity(Long id);

	public APIResponse updateCity(City city);

	public List<TypeAheadVo> getAllCitiesCorrespondingToState();

	public List<Long> getAllCityIds();

	/**
	 * @author aditya 11:22:29 AM Apr 10, 2017 APIResponse
	 * @param cityVOs
	 * @return
	 */
	APIResponse addCities(List<CityVO> cityVOs);

	/**
	 * @author aditya 10:25:46 AM Apr 14, 2017 void
	 */
	void resetLocationCache();
	
	/**
	 * @author kiran
	 */
	void updateLocationCache(City city,Branch branch ,Area area,int cacheUpdateCode);

}
