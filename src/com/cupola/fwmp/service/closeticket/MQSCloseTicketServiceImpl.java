package com.cupola.fwmp.service.closeticket;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.CutomerAccountActivation;
import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.fr.FrDeploymentSettingDao;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.integ.closeticket.MQSCloseTicketDAO;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectResponse;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectStatus;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticket.TicketDetailDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.WorkOrder;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;
import com.thoughtworks.xstream.XStream;

public class MQSCloseTicketServiceImpl implements MQSCloseTicketService
{

	static final Logger logger = Logger
			.getLogger(MQSCloseTicketServiceImpl.class);

	@Autowired
	MQSCloseTicketDAO closeTicketDAO;

	@Autowired
	WorkOrderDAO workOrderDAO;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	DefinitionCoreService coreService;

	@Autowired
	private FrTicketDao frDao;

	@Autowired
	private TicketDetailDAO ticketDetail;

	@Autowired
	TicketActivityLogDAO activityLogDAO;

	@Autowired
	EtrCalculator etrCalculatorImpl;
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;
	
	@Autowired
	private FrDeploymentSettingDao frSettingDao;

	private String hydResolutionCode;

	private String roiResolutionCode;
	
	private boolean useDefaultResCode;

	public void setHydResolutionCode(String hydResolutionCode)
	{
		this.hydResolutionCode = hydResolutionCode;
	}

	public void setRoiResolutionCode(String roiResolutionCode)
	{
		this.roiResolutionCode = roiResolutionCode;
	}

	@Override
	public APIResponse closeTicket(TicketClosureDataVo closeTicketVo)
	{
		if (closeTicketVo == null)
			return ResponseUtil.createNullParameterResponse();

		try {
			logger.info("Getting Ticket detils for WO " + closeTicketVo.getWorkOrderNo());

			// WorkOrder workOrder = workOrderDAO
			// .getWorkOrderByWoNo(closeTicketVo.getWorkOrderNo());

			logger.info("work order id =========== " + closeTicketVo.getWorkorderId());

			if (closeTicketVo.getWorkOrderNo() != null && !closeTicketVo.getWorkOrderNo().isEmpty()) {

				// Ticket ticket = workOrder.getTicket();

				logger.info("ticket id =========== " + closeTicketVo.getTicketId() + " Customer id "
						+ closeTicketVo.getCustomerId() + " MQ id " + closeTicketVo.getMqID());

				closeTicketVo.setTicketId(closeTicketVo.getTicketId());

				String cityName = closeTicketVo.getCityName();

				logger.info("CIty is======================" + cityName + " for mqId " + closeTicketVo.getMqID()
						+ " and mobile num " + closeTicketVo.getMobileNo());
				if (cityName != null && !cityName.isEmpty()) {
					String remarks = null;

					if (cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY)) {

						closeTicketVo.setTicketResolutionCode(hydResolutionCode);
						remarks = coreService
								.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_WO_CLOSURE_VALUE_HYD);
						closeTicketVo.setTicketResolutionNotes(
								remarks != null ? remarks : "New Installation WorkOrder Closed by FWMP");
					}

					else {
						closeTicketVo.setTicketResolutionCode(roiResolutionCode);
						remarks = coreService
								.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_WO_CLOSURE_VALUE_HYD);
						closeTicketVo.setTicketResolutionNotes(
								remarks != null ? remarks : "New Installation WorkOrder Closed by FWMP");
					}

				}

				logger.info("closeTicketVo@@@@@@@@@@@@@@" + closeTicketVo);

				if (activityLogDAO.isActivityClosed(closeTicketVo.getTicketId(), Activity.MATERIAL_CONSUMPTION)) {

					logger.info("Material activity is closed for ticket id " + closeTicketVo.getTicketId());

					String currentCRM = dbUtil.getCurrentCrmName(null, null);
					
					if(currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)){
						commonActivityToClose(0, closeTicketVo);
						workOrderCoreService.updateWorkOrderActivityInCRM(closeTicketVo.getTicketId(), Long.valueOf(closeTicketVo.getCustomerId()), closeTicketVo.getProspectNo(), closeTicketVo.getWorkOrderNo(), null, null, FWMPConstant.SiebelTicketStatus.CUSTOMER_ACCOUNT_ACTIVATION_DONE,null);
						return ResponseUtil.createTicketClosureSuccessInMQ()
								.setData(ResponseUtil
										.createTicketClosureSuccessInMQ()
										.getStatusMessage());
					}else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {

						String mqResponse = closeTicketDAO.closeTicket(closeTicketVo);

						if (mqResponse != null) {
							XStream xstream = new XStream();

							xstream.alias("RESPONSEINFO", MqProspectResponse.class);
							xstream.aliasField("STATUS", MqProspectResponse.class, "status");
							xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
							xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
							xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

							MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream.fromXML(mqResponse);

							int errorCode = mqProspectResponse.getStatus().getErrorNo();
							String remarks = mqProspectResponse.getStatus().getMessage();

							if (errorCode == 0 || errorCode == 99070) {
								closeTicketVo.setRemarks(remarks);
								commonActivityToClose(errorCode, closeTicketVo);
							} else {
								logger.info("MQ failed " + errorCode);
								return new APIResponse(errorCode, remarks);
							}
						} else {
							return ResponseUtil.noResponseFromMqServer();
						}
					}

				}

				else {
					logger.info("Material is not completed for ticket " + closeTicketVo.getTicketId() + ", and wo is "
							+ closeTicketVo.getWorkOrderNo());
					return ResponseUtil.noResponseFromMqServer().setData("Material Activity is not completed.");
				}

			}
			else {
				return ResponseUtil.createNoWoNumber().setData("NO workorder For Closed Ticket");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in work order closure for " + closeTicketVo, e);
		}
		return null;

	}
	
	private APIResponse commonActivityToClose(int errorCode, TicketClosureDataVo closeTicketVo) {

		logger.info("Error number " + errorCode);
		
		TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();

		ticketActivityLogVo.setModifiedOn(new Date());
		ticketActivityLogVo.setAddedOn(new Date());
		ticketActivityLogVo.setTicketId(closeTicketVo.getTicketId());
		ticketActivityLogVo
				.setActivityId(CutomerAccountActivation.CUSTOMER_ACCOUNT_ACTIVATION);

		ticketActivityLogVo
				.setStatus(TicketStatus.COMPLETED);

		ticketActivityLogVo
				.setId(StrictMicroSecondTimeBasedGuid
						.newGuid());

		activityLogDAO
				.addTicketActivityLog(ticketActivityLogVo);

		activityLogDAO
				.updateTicketStatus(ticketActivityLogVo);

		try
		{

			etrCalculatorImpl
					.calculateActivityPercentage(closeTicketVo.getTicketId(), null);

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		Long result = ticketDAO
				.ticketClosureStatusUpdate(closeTicketVo);

		if (result > 0)
		{

			if ( closeTicketVo
					.getCurrentAssignedTo() != null)
			{
				// dbUtil.removeOneTicketFromQueue(ticket
				// .getUserByCurrentAssignedTo().getId(),
				// ticket
				// .getId());

				if (!activityLogDAO
						.isActivityRejected(closeTicketVo
								.getTicketId(), Activity.CUSTOMER_ACCOUNT_ACTIVATION))
				{
					TicketActivityLogVo ticketLog = new TicketActivityLogVo();
					ticketLog
							.setActivityId(Activity.CUSTOMER_ACCOUNT_ACTIVATION);
					ticketLog.setStatus(101);
					ticketLog.setTicketId(closeTicketVo.getTicketId());

					activityLogDAO
							.updateTicketActivityLogStatus(ticketLog);

				}

			}
			return new APIResponse(errorCode, closeTicketVo.getRemarks());
		}
		return new APIResponse(errorCode, closeTicketVo.getRemarks());
	
	}

	private String getWorkOrderByTicket(FrTicket ticket)
	{
		if (ticket == null)
			return null;

		String ticketCategory = ticket.getTicketCategory();

		if (FWMPConstant.TicketCategory.FAULT_REPAIR_BOTH.contains( ticketCategory ))
		{
			Set<FrDetail> frWorkOrder = ticket.getFrWorkOrders();
			if (frWorkOrder != null && frWorkOrder.size() > 0)
				return frWorkOrder.iterator().next().getWorkOrderNumber();
		}
		return null;
	}

	@Override
	public APIResponse closeTicketInMQS(Long ticketId)
	{
		if (ticketId == null)
			return ResponseUtil.createNullParameterResponse();

		try
		{
			FrTicket ticket = ticketDAO.getFrTicketById(ticketId);
			String workOrder = getWorkOrderByTicket(ticket);

			if (workOrder == null)
				return ResponseUtil.createFailureResponse()
						.setData("WorkOrder is : " + workOrder
								+ " of the ticket : " + ticket.getId());

			TicketClosureDataVo closeTicketVo = new TicketClosureDataVo();
			closeTicketVo.setTicketId(ticket.getId());
			closeTicketVo.setWorkOrderNo(workOrder);

			City city = ticket.getCustomer() != null
					? ticket.getCustomer().getCity() : null;
			if (city != null)
			{
				if (city.getCityName()
						.equalsIgnoreCase(CityName.HYDERABAD_CITY))
				{
					closeTicketVo.setTicketResolutionCode(hydResolutionCode);
					closeTicketVo.setTicketResolutionNotes("Resolved");
				}
				else
				{
					closeTicketVo.setTicketResolutionCode(roiResolutionCode);
					closeTicketVo.setTicketResolutionNotes("Resolved");
				}

			}
			logger.info("Closing ticket vo ..." + closeTicketVo);

			String mqResponse = closeTicketDAO.closeFrTicket(closeTicketVo);

			if (mqResponse != null)
			{
				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream
						.fromXML(mqResponse);

				int errorCode = mqProspectResponse.getStatus()
						.getErrorNo();
				
				String remarks = mqProspectResponse.getStatus()
						.getMessage();
				
				if (errorCode == 0 || errorCode==99070)
				{
					/*
					 *  do some thing after closing ticket in MQ
					 *  either send the message or update the status 
					 *  
					 *  */
					frDao.updateFrTicketStatus(ticketId, 
							Arrays.asList(Long.valueOf(TicketStatus.COMPLETED)));
					
					return new APIResponse(errorCode, remarks);
				}
				else
				{
					logger.info("Closing ticket in MQ is Failed ..." );
					return new APIResponse(errorCode, remarks);
				}
			} 
			else
				return ResponseUtil.noResponseFromMqServer();

		} 
		catch (Exception e)
		{
			logger.error("Error occure while closing ticket"
					+ " in MQ server : "+e.getMessage());
		}
		return null;
	}
	
	
	@Override
	public APIResponse closeFrTicket( FRDefectSubDefectsVO resolutionCode )
	{

		if (resolutionCode == null || resolutionCode.getTicketId() == null )
			return null;

		try
		{
			FrTicket ticket = ticketDAO.getFrTicketById(resolutionCode.getTicketId());
			String workOrder = getWorkOrderByTicket(ticket);

			if (workOrder == null)
				return ResponseUtil.createFailureResponse()
						.setData("WorkOrder is : " + workOrder
								+ " of the ticket : " + ticket.getId());

			TicketClosureDataVo closeTicketVo = new TicketClosureDataVo();
			closeTicketVo.setTicketId(ticket.getId());
			closeTicketVo.setWorkOrderNo(workOrder);

			City city = ticket.getCustomer() != null
					? ticket.getCustomer().getCity() : null;
			if (city != null)
			{
				if (city.getCityName()
						.equalsIgnoreCase(CityName.HYDERABAD_CITY))
				{
//					closeTicketVo.setTicketResolutionCode(hydResolutionCode);
					String resoCode = getResolutinCode( resolutionCode.getDefectCode()
							,resolutionCode.getSubDefectCode(), city.getCityName() );
					
					closeTicketVo.setTicketResolutionCode(resoCode);
					
					closeTicketVo.setTicketResolutionNotes("Resolved");
					closeTicketVo.setCityName(CityName.HYDERABAD_CITY);
				}
				else
				{
//					closeTicketVo.setTicketResolutionCode(roiResolutionCode); 
					
					String resoCode = getResolutinCode( resolutionCode.getDefectCode()
							,resolutionCode.getSubDefectCode(), city.getCityName() );
					
					closeTicketVo.setTicketResolutionCode(resoCode);
					closeTicketVo.setTicketResolutionNotes("Resolved");
				}

			}
			logger.info("Closing ticket vo ..." + closeTicketVo +" For city : "+ city );

			String mqResponse = closeTicketDAO.closeFrTicket(closeTicketVo);

			if (mqResponse != null)
			{
				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream
						.fromXML(mqResponse);

				int errorCode = mqProspectResponse.getStatus()
						.getErrorNo();
				
				String remarks = mqProspectResponse.getStatus()
						.getMessage();
				
				if (errorCode == 0 || errorCode==99070)
				{
					/*
					 *  do some thing after closing ticket in MQ
					 *  either send the message or update the status 
					 *  
					 *  */
					frDao.updateFrTicketStatus(resolutionCode.getTicketId(), 
							Arrays.asList(Long.valueOf(TicketStatus.COMPLETED)));
					
					return new APIResponse(errorCode, remarks);
				}
				else
				{
					logger.info("Closing ticket in MQ is Failed ..." );
					return new APIResponse(errorCode, remarks);
				}
			} 
			else
				return ResponseUtil.noResponseFromMqServer();

		} 
		catch (Exception e)
		{
			logger.error("Error occure while closing ticket"
					+ " in MQ server : "+e.getMessage());
			return null;
		}
	}
	
	@Override
	public APIResponse modifyFrTicket( TicketModifyInMQVo vo )
	{
		logger.info("************* modifyFrTicket *********** MQ API called *************");
		if ( vo == null ||  vo.getDefectCode() == null || vo.getDefectCode().isEmpty()
				|| vo.getSubDefectCode() == null || vo.getSubDefectCode().isEmpty() )
			return null;

		try
		{
			String workOrder = vo.getWorkOrderNumber();

			if (workOrder == null || workOrder.isEmpty())
				return ResponseUtil.createFailureResponse()
						.setData("WorkOrder is : " + workOrder
								+ " of the ticket : " + vo.getTicketId());

			TicketClosureDataVo closeTicketVo = new TicketClosureDataVo();
			closeTicketVo.setTicketId(vo.getTicketId());
			closeTicketVo.setWorkOrderNo(workOrder);

			if ( vo.getCityName() != null )
			{
				if (vo.getCityName()
						.equalsIgnoreCase(CityName.HYDERABAD_CITY))
				{
					String resoCode = getResolutinCodeDb( vo.getDefectCode()
							,vo.getSubDefectCode(), vo.getCityName() );
					
					closeTicketVo.setTicketResolutionCode(resoCode);
					closeTicketVo.setTicketResolutionNotes("Resolved");
					closeTicketVo.setCityName(CityName.HYDERABAD_CITY);
				}
				else
				{
					String resoCode = getResolutinCodeDb( vo.getDefectCode()
							,vo.getSubDefectCode(), vo.getCityName() );
					
					closeTicketVo.setTicketResolutionCode(resoCode);
					closeTicketVo.setTicketResolutionNotes("Resolved");
				}

			}
			logger.info("Closing ticket vo ..." + closeTicketVo +" For city : "+ vo.getCityName() );

			String mqResponse = closeTicketDAO.closeFrTicket(closeTicketVo);
			if (mqResponse != null)
			{
				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream
						.fromXML(mqResponse);

				int errorCode = mqProspectResponse.getStatus()
						.getErrorNo();
				
				String remarks = mqProspectResponse.getStatus()
						.getMessage();
				
				if (errorCode == 0 || errorCode==99070)
				{
					/*
					 *  do some thing after closing ticket in MQ
					 *  either send the message or update the status 
					 *  
					 *  */
					frDao.updateFrTicketStatus(vo.getTicketId(), 
							Arrays.asList(Long.valueOf(TicketStatus.COMPLETED)));
					
					String resoCode = getResolutinCodeDb( vo.getDefectCode()
							,vo.getSubDefectCode(), vo.getCityName() );
					
					return new APIResponse(errorCode, remarks).setData( resoCode );
				}
				else
				{
					logger.info("Closing ticket in MQ is Failed ..." );
					return new APIResponse(errorCode, remarks);
				}
			} 
			else
				return ResponseUtil.noResponseFromMqServer();

		} 
		catch (Exception e)
		{
			logger.error("Error occure while closing ticket"
					+ " in MQ server : "+e.getMessage());
			return null;
		}
	}
	
	private String getResolutinCode( String defectCode,String subDefecCode, String city )
	{
		if ( defectCode == null || defectCode.isEmpty() 
				|| subDefecCode == null || subDefecCode.isEmpty())
			return definitionCoreService
					.getActForceUrl(DefinitionCoreService.MQ_DEFAULT_CLOSURE_CODE);
		
		if( isDefaultResCodeEnabled() )
			return definitionCoreService
					.getActForceUrl(DefinitionCoreService.MQ_DEFAULT_CLOSURE_CODE);
		
		if( city != null && city.equalsIgnoreCase(CityName.HYDERABAD_CITY))
			return definitionCoreService.getFrMqClosurCodeHyd(defectCode, subDefecCode);
		else
			return definitionCoreService.getFrMqClosurCode(defectCode, subDefecCode);
	}
	
	private String getResolutinCodeDb( String defectCode, String subDefecCode, String city )
	{
		if ( defectCode == null || defectCode.isEmpty() 
				|| subDefecCode == null || subDefecCode.isEmpty() 
				|| city == null || city.isEmpty())
			
			return definitionCoreService
					.getActForceUrl(DefinitionCoreService.MQ_DEFAULT_CLOSURE_CODE);
		
		if( isDefaultResCodeEnabled() )
			return definitionCoreService
					.getActForceUrl(DefinitionCoreService.MQ_DEFAULT_CLOSURE_CODE);
		
		return frSettingDao.getMQClosureCode( defectCode, subDefecCode, city);
	}

	private boolean isDefaultResCodeEnabled()
	{
		String enableValue = definitionCoreService
				.getActForceUrl(DefinitionCoreService.MQ_RESO_CODE_DEFAULT_ENABLED);
		
		if( enableValue == null || enableValue.isEmpty() )
			useDefaultResCode = false;
		else
			useDefaultResCode = Boolean.valueOf(enableValue);

		logger.info("default Mq closure code enabled : "+useDefaultResCode);
		return useDefaultResCode;
	}
	
}