package com.cupola.fwmp.service.closeticket;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.MQRequestType;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrIntegrationCoreService;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;

public class MQClosureHelper
{
	private static final Logger logger = Logger.getLogger(MQClosureHelper.class);
	
	@Autowired
	private MQSCloseTicketService closeTicketCoreService;
	
	@Autowired
	private TicketCoreService ticketDetailsService;
	
	@Autowired
	private FrTicketService frService;
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	@Autowired
	private FrIntegrationCoreService mqIntegration;
	
	private boolean autoClosuerEnable;
	
	private BlockingQueue<FRDefectSubDefectsVO> ticketIds = null;
	private Thread MQClosure = null;
	
	private BlockingQueue<TicketModifyInMQVo> mqModifyTicketIds = null;
	private Thread MQModify = null;
	
	public void init()
	{
		logger.info("enabling MQ Closure processing");
		ticketIds = new LinkedBlockingQueue<FRDefectSubDefectsVO>(100000);
		mqModifyTicketIds = new LinkedBlockingQueue<TicketModifyInMQVo>(100000);
		
		if (MQClosure == null)
		{
			MQClosure = new Thread(closuer, "MQClosure");
			MQClosure.start();
		}
		
		if( MQModify == null )
		{
			MQModify = new Thread(modifier, "MQModifier");
			MQModify.start();
		}
		logger.info("MQ Closure processing enabled");
	}
	
	Runnable modifier = new Runnable()
	{
		
		@Override
		public void run()
		{
			try
			{
				ExecutorService executor = Executors.newFixedThreadPool(25);
				while (true)
				{
					try
					{
						final TicketModifyInMQVo workOrder  = mqModifyTicketIds.take();
						Runnable independentModifier = new Runnable()
						{
							@Override
							public void run() 
							{
								try
								{
									if( workOrder != null )
									{
										logger.info("Thread : "+Thread.currentThread().getName() 
												+" Modifying work order");
										modifyTicket( workOrder );
									}
								}
								catch (Exception e)
								{
									logger.error("Error occured While modifying work order : "
												,e );
								}
							}
						};
						
						executor.execute(independentModifier);
						
					} 
					catch (Exception e)
					{
						logger.error("Error While modifying ticket no : "
								+ e.getMessage());
						continue;
					}
				}
			}
			catch (Exception e)
			{
				logger.error("error in MQModifier main loop- msg="
						+ e.getMessage());
			}	
		}
		
		private void modifyTicket( TicketModifyInMQVo workOrder )
		{
			if( workOrder == null || workOrder.getMqModifyRequestType() <= 0 )
				return;
			
			else if( workOrder.getMqModifyRequestType() == MQRequestType.ETR_MODIFY)
				mqIntegration.updateEtrInMQ(workOrder);
			else if( workOrder.getMqModifyRequestType() == MQRequestType.STATUS_OR_COMMENT_MODIFY )
				mqIntegration.updateCommentOrStatusInMQ(workOrder);
		}
	};
	
	Runnable closuer = new Runnable()
	{
		@Override
		public void run()
		{
			try
			{
				ExecutorService executor = Executors.newFixedThreadPool(25);
				while (true)
				{
					try
					{
						final FRDefectSubDefectsVO workOrder  = ticketIds.take();
						Runnable independClosure = new Runnable()
						{
							@Override
							public void run()
							{ 
								try
								{
									if( workOrder != null )
									{
										logger.info("Thread : "+Thread.currentThread().getName() 
												+" closing work order");
										closeTicket( workOrder );
									}
								}
								catch (Exception e) 
								{
									logger.error("Error occured While closing work order : "
											,e );
								}
							}
						};
						
						executor.execute( independClosure );
					} 
					catch (Exception e)
					{
						logger.error("Error While closing current proccesing workOrder : "
								+ e.getMessage());
						continue;
					}
				}
			} 
			catch (Exception e)
			{
				logger.error("error in MQClosuer main loop- msg="
						+ e.getMessage());
			}
			logger.info("MQClosuer done closing...");
		}
		
		private void closeTicket(FRDefectSubDefectsVO resolutionCode)
		{
			ticketDetailsService.updateTicketDefectCode( resolutionCode );
		}
	};
	
	public void handleTicket( FRDefectSubDefectsVO resolutionCode )
	{
		if (resolutionCode == null)
			return;
		try
		{
			put(resolutionCode);
			if( isAuthClosureEnabled() )
				autoCloseTicket( resolutionCode );
		}
		catch (Exception e)
		{
			logger.error("error adding in Closure Q " + e.getMessage());
		}
	}
	
	private void put( FRDefectSubDefectsVO resolutionCode )
	{
		if( resolutionCode == null )
			return;
		
		try
		{
			
				ticketIds.put(resolutionCode);
			
		}
		catch (Exception e)
		{
			logger.error("error adding in Closure Q " + e.getMessage());
		}
	}

	private void autoCloseTicket( FRDefectSubDefectsVO resolutionCode )
	{
		if( resolutionCode == null || resolutionCode.getTicketId() == null 
				|| resolutionCode.getTicketId() <= 0 )
			return;
			
		
		FRDefectSubDefectsVO closeVo = null;
		try
		{
			List<Long> allAutoCloseIds = frService.getAllVicinityFrTicket( resolutionCode.getTicketId() );
			
			for( Long autoIds : allAutoCloseIds )
			{
				closeVo = new FRDefectSubDefectsVO();
				closeVo.setTicketId(autoIds);
				closeVo.setDefectCode(resolutionCode.getDefectCode());
				closeVo.setSubDefectCode(resolutionCode.getSubDefectCode());
				closeVo.setRemarks("Auto closed ticket by System");
				closeVo.setModifiedBy(1l);
				put(closeVo);
			}
		} 
		catch (Exception e)
		{
				logger.error("error adding in Closure Q in auto closure mode" + e);
		}
	}
	
	private boolean isAuthClosureEnabled()
	{
		String enabledValue = definitionCoreService
				.getPropertyValue(DefinitionCoreService.MQ_AUTO_CLOSURE_ENABLED);
		
		if( enabledValue == null || enabledValue.isEmpty() )
			autoClosuerEnable = false;
		else
			autoClosuerEnable = Boolean.valueOf(enabledValue);
		
		logger.info("Auto close enabled : "+autoClosuerEnable);
		return autoClosuerEnable;
	}
	
	public void putInModifyQ( TicketModifyInMQVo workOrder  )
	{
		if( workOrder == null )
			return;
		
		try
		{
			
			mqModifyTicketIds.put(workOrder);
			
		}
		catch (Exception e)
		{
			logger.error("error adding in Modify Q " + e.getMessage());
		}
	}
	
	public void doAutoClose( FRDefectSubDefectsVO resolutionCode )
	{
		if( resolutionCode == null || resolutionCode.getTicketId() == null 
				|| resolutionCode.getTicketId() <= 0 )
			
			return;
		
		try
		{
			if( isAuthClosureEnabled() )
//				MQAutoClosureTickets.put(resolutionCode);
				autoCloseTicket( resolutionCode );
			
		}catch(Exception e){
			logger.error("error occured while checking auto close eligibilty from App ", e );
		}
	}
	
}
