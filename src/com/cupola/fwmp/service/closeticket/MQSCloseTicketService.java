package com.cupola.fwmp.service.closeticket;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;

public interface MQSCloseTicketService
{

	APIResponse closeTicket(TicketClosureDataVo closeTicketVo);

	APIResponse closeTicketInMQS(Long ticketId);

	APIResponse closeFrTicket( FRDefectSubDefectsVO resolutionCode );

	APIResponse modifyFrTicket(TicketModifyInMQVo resolutionCode);
}
