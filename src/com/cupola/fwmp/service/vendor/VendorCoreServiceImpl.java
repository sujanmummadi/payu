package com.cupola.fwmp.service.vendor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.vendor.VendorDAO;
import com.cupola.fwmp.dao.vendor.VendorDAOImpl;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.filters.VendorFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseMessage;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.SkillNameList;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.util.UserRoles;
import com.cupola.fwmp.vo.InputVendorVO;
import com.cupola.fwmp.vo.SkillCountVO;
import com.cupola.fwmp.vo.SkillCountVOForWebUI;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.VendorDetailSummaryVO;
import com.cupola.fwmp.vo.VendorSummaryVO;
import com.cupola.fwmp.vo.VendorTableVo;
import com.cupola.fwmp.vo.VendorVO;
import com.cupola.fwmp.vo.VendorVoForWeb;
import com.cupola.fwmp.vo.VendorsRecordVO;

public class VendorCoreServiceImpl implements VendorCoreService
{

	private VendorDAO vendorDao;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private UserDao userDao;

	public void setVendorDao(VendorDAO vendorDao)
	{
		this.vendorDao = vendorDao;
	}

	Long vendorId = 0l;

	static final Logger log = Logger.getLogger(VendorCoreServiceImpl.class);

	public APIResponse addVendor(VendorVO vendor)
	{

		// log.debug("before setting " + vendor);

		vendor = commonValues(vendor);

		log.debug("After setting " + vendor);

		try
		{

			Long result = vendorDao.addVendor(vendor);

			// vendorDao.addVendorSkillAndCount(vendor);

			// vendorDao.addIntoVendorSkillMapping(vendor);

			if (result > 0)
			{
				vendorDao.addToCache(vendor);
				return ResponseUtil.recordAddedSucessfully().setData(result);

			}else
			{

				return ResponseUtil.createDuplicateResponse().setData(result);
			}

		} catch (Exception e)
		{
			log.error("Error while adding vendor "+e.getMessage());

			e.printStackTrace();
			return ResponseUtil.createDBExceptionResponse();
		}

	}

	private VendorVO commonValues(VendorVO vendor)
	{

		// log.debug("Vendor id in commonValues:" + vendor.getId());

		if (vendor.getId() == 0)
		{

			vendor.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			vendor.setAddedBy(AuthUtils.getCurrentUserId());
			vendor.setAddedOn(new Date());
			vendor.setDate(new Date());

			if (vendor.getEnable() != null
					&& vendor.getEnable().equalsIgnoreCase("enable"))
			{
				vendor.setStatus(1);
			} else
			{
				vendor.setStatus(0);
			}
		}

		vendor.setModifiedOn(new Date());
		vendor.setModifiedBy(AuthUtils.getCurrentUserId());

		// log.debug("commonValues() vendor modified on:" +
		// vendor.getModifiedOn());

		return vendor;
	}

	public APIResponse updateVendor(VendorVO vendor)
	{
		// log.debug("Vendor id in VendorServiceImpl:" + vendor.getId());
		// log.debug("vendor city name::" + vendor.getCityName());

		if (vendor != null)
		{
			try
			{

				vendor = commonValues(vendor);

				if (vendor.getEnable() != null)
				{

					if (vendor.getEnable().equalsIgnoreCase("enable"))
					{

						vendor.setStatus(1);

					} else
					{

						vendor.setStatus(0);
					}

				}
				String ven = vendorDao.updateVendor(vendor);

				// log.debug("vendor2 value :" + ven);

				if (ven != null && ven.equalsIgnoreCase("success"))
				{

					return ResponseUtil.createUpdateSuccessResponse()
							.setData(ven);

				} else
				{
					return ResponseUtil.noRecordUpdated().setData(ven);
				}

			} catch (Exception e)
			{
				log.error("Error while updating vendor "+e.getMessage());
				e.printStackTrace();
				return ResponseUtil.createDBExceptionResponse();
			}
		} else
		{
			log.info("vendor is emmpty****");
			return ResponseUtil
					.createNullParameterResponse("Empty vendor record is passing ...");
		}

	}

	@Override
	public APIResponse deleteVendor(Long id)
	{

		String message = null;

		if (id != null)
		{

			message = vendorDao.deleteVendor(id);
		}

		if (message != null && !message.isEmpty()
				&& message.equalsIgnoreCase("success"))
		{

			return ResponseUtil.createDeleteSuccessResponse().setData(message);
		}

		else
		{
			return ResponseUtil.createDeleteFailedResponse();
		}
	}

	@Override
	public APIResponse getCountAllVendor()
	{

		int x = vendorDao.getCountAllVendor();

		if (x != 0)
		{

			return ResponseUtil.recordFoundSucessFully().setData(x);
		} else
		{
			return ResponseUtil.createRecordNotFoundResponse();
		}

	}

	@Override
	public APIResponse getCountAllVendorBySkill(Long id)
	{

		int x = vendorDao.getCountAllVendorBySkill(id);

		if (x != 0)
		{

			return ResponseUtil.recordFoundSucessFully().setData(x);
		} else
		{
			return null;
		}

	}

	@Override
	public APIResponse addVendorSkillAndCount(VendorVO vendorvo)
	{
		VendorVO vendorVO = new VendorVO();

		vendorVO.setVendorId(vendorvo.getId());

		vendorVO.setAddedBy(vendorvo.getId());
		vendorVO.setModifiedBy(vendorvo.getId());

		vendorVO.setAddedOn(new Date());

		vendorVO.setModifiedOn(new Date());

		vendorDao.addVendorSkillAndCount(vendorvo);

		return null;

	}

	@Override
	public APIResponse updateCountSkill(InputVendorVO inputVendorvo)
	{

		VendorVO vend = new VendorVO();
		String message = null;

		if (!(AuthUtils.getCurrentUserRole() == AuthUtils.UserRole.NE_NI)
				|| (AuthUtils.getCurrentUserRole() == AuthUtils.UserRole.NE)
				|| (AuthUtils.getCurrentUserRole() == AuthUtils.UserRole.NE_FR))
		{

			if (inputVendorvo.getUserId() != null
					&& inputVendorvo.getVendorId() != null
					&& inputVendorvo.getVendorSkillIdAndCountMap() != null)
			{

				vend.setUserId(Long.valueOf(inputVendorvo.getUserId()));

				vend.setVendorId(Long.valueOf(inputVendorvo.getVendorId()));

				vend.setVendorSkillIdAndCountMap(inputVendorvo
						.getVendorSkillIdAndCountMap());

				vend.setModifiedBy(Long.valueOf(AuthUtils.getCurrentUserId()));

				vend.setAddedBy(Long.valueOf(AuthUtils.getCurrentUserId()));

				vend.setDate(new Date());

				vend.setAddedOn(new Date());

				vend.setModifiedOn(new Date());

				log.debug("vendorvo in server" + vend);

				message = vendorDao.updateCountSkill(vend);
			}

			if (message != null && !message.isEmpty())
			{

				return ResponseUtil.createUpdateSuccessResponse()
						.setData(message);

			} else
			{

				return ResponseUtil.createUpdateFailedResponse()
						.setData(message);
			}
		} else
		{

			return ResponseUtil.noRecordUpdated()
					.setData(ResponseMessage.FailureMessage);
		}

	}

	@Override
	public APIResponse addIntoVendorSkillMapping(VendorVO vendorvo)
	{

		try
		{
			vendorDao.addIntoVendorSkillMapping(vendorvo);

			return ResponseUtil.createUpdateSuccessResponse();

		} catch (Exception e)
		{
			log.error("Error while adding vendor skill mapping "+e.getMessage());

			e.printStackTrace();

			return ResponseUtil.createUpdateFailedResponse();
		}

	}

	@Override
	public APIResponse vendorAssignmentForTL(InputVendorVO inputVendorvo)
	{
		List<VendorSummaryVO> vendorAssignForTL = new ArrayList<VendorSummaryVO>() ;
		VendorVO vendorvo = new VendorVO();

		if (inputVendorvo != null)
		{

			vendorvo.setUserId(Long.valueOf(inputVendorvo.getUserId()));

			// vendorvo.setRoleid(Integer.valueOf(inputVendorvo.getRoleId()));
			//
			// vendorvo.setAreaId(Long.valueOf(inputVendorvo.getAreaId()));
			//

			// vendorAssignForTL = vendorDao.vendorAssignmentForTL(vendorvo);

		} else
		{
			return ResponseUtil
					.createNullParameterResponse("Provide input data");
		}

		if (!vendorAssignForTL.isEmpty())
		{

			return ResponseUtil.recordFoundSucessFully()
					.setData(vendorAssignForTL);

		} else
		{
			return ResponseUtil.NoRecordFound();
		}
	}

	@Override
	public APIResponse addUserVendorMapping(VendorVO vendorvo)
	{

		Integer integer = null;

		if (vendorvo.getId() > 0 && vendorvo.getUserId() != null)
		{

			vendorvo.setVendorId((Long.valueOf(vendorvo.getId())));

			vendorvo.setUserId((Long.valueOf(vendorvo.getUserId())));

			vendorvo.setAddedBy(vendorvo.getUserId());

			vendorvo.setAddedOn(new Date());

			vendorvo.setModifiedBy(vendorvo.getUserId());

			vendorvo.setModifiedOn(new Date());

			if (vendorvo.getEnable() != null
					&& vendorvo.getEnable().equalsIgnoreCase("enable"))
			{
				vendorvo.setStatus(1);
			} else
			{
				vendorvo.setStatus(0);
			}

			integer = vendorDao.addUserVendorMapping(vendorvo);

		}

		if (integer != null)
		{

			return ResponseUtil.createSaveSuccessResponse().setData(integer);

		} else
		{
			return ResponseUtil.createSaveFailedResponse().setData(null);
		}

	}

	@Override
	public APIResponse vendorAssignmentByTLForNE(InputVendorVO inputVendorvo)
	{

		VendorVO vend = new VendorVO();

		String message = null;

		if (AuthUtils.getCurrentUserRole() == AuthUtils.UserRole.TL_NI)
		{

			if (inputVendorvo.getNeUserId() != null
					&& inputVendorvo.getVendorId() != null
					&& inputVendorvo.getVendorSkillIdAndCountMap() != null)
			{

				log.debug("current userid is::" + AuthUtils.getCurrentUserId());

				vend.setNeUserId(Long.valueOf(inputVendorvo.getNeUserId()));

				vend.setVendorId(Long.valueOf(inputVendorvo.getVendorId()));

				vend.setVendorSkillIdAndCountMap(inputVendorvo
						.getVendorSkillIdAndCountMap());

				vend.setModifiedBy(Long.valueOf(AuthUtils.getCurrentUserId()));

				vend.setAddedBy(Long.valueOf(AuthUtils.getCurrentUserId()));

				vend.setDate(new Date());

				vend.setAddedOn(new Date());

				vend.setModifiedOn(new Date());

				log.debug("vendorvo in server" + vend);

				message = vendorDao.vendorAssignmentByTLForNE(vend);

			}

			if (message != null && !message.isEmpty())
			{

				return ResponseUtil.createUpdateSuccessResponse()
						.setData(message);

			} else
			{

				return ResponseUtil.createUpdateFailedResponse()
						.setData(message);
			}
		} else if (AuthUtils.getCurrentUserRole() == AuthUtils.UserRole.NE_NI)
		{

			return ResponseUtil.createUserAssociatedWithTabFalseResponse()
					.setData(ResponseMessage.FailureMessage);
		} else
		{
			return ResponseUtil.createUserAssociatedWithTabFalseResponse();
		}

	}

	@Override
	public APIResponse getVendorDetails(Long userid)
	{
		List<VendorSummaryVO> vendorSummaryVO = null;

		if (userid != null)
		{

			vendorSummaryVO = vendorDao.getVendorDetails(userid);
		}

		log.debug("vendorSummaryVO::getVendorDetails :::" + vendorSummaryVO);

		if (vendorSummaryVO != null && !vendorSummaryVO.isEmpty())
		{

			Map<String, Map<String, Integer>> vendorSkillMap = new LinkedHashMap<String, Map<String, Integer>>();

			for (VendorSummaryVO vsvo : vendorSummaryVO)
			{

				String key = vsvo.getVendorId() + ",@#" + vsvo.getVendorName();

				String skillKey = vsvo.getSkillId() + ",@#"
						+ vsvo.getSkillName();

				if (vendorSkillMap.containsKey(key))
				{

					Map<String, Integer> tempSkillCount = new HashMap<>();

					tempSkillCount.put(skillKey, vsvo.getCount());

					Map<String, Integer> skillCount = vendorSkillMap.get(key);

					skillCount.putAll(tempSkillCount);

					tempSkillCount.put(skillKey, vsvo.getCount());

					vendorSkillMap.put(key, skillCount);

				} else
				{

					Map<String, Integer> tempSkillCount = new HashMap<>();

					tempSkillCount.put(skillKey, vsvo.getCount());

					vendorSkillMap.put(key, tempSkillCount);

				}

			}

			return ResponseUtil.recordFoundSucessFully()
					.setData(vendorSkillMap);
		}

		else
		{
			return ResponseUtil.NoRecordFound();
		}

	}

	@Override
	public APIResponse getVendorDetailsForAndroid(Long userid)
	{
		List<VendorVoForWeb> vendorSummaryVOList = new ArrayList<VendorVoForWeb>();

		Map<String, Map<String, Integer>> vendorSkillMap = (Map<String, Map<String, Integer>>) getVendorDetails(userid)
				.getData();

		log.debug("vendorSkillMap  ######### " + vendorSkillMap);

		if (vendorSkillMap != null && !vendorSkillMap.isEmpty())
		{

			for (Map.Entry entry : vendorSkillMap.entrySet())
			{

				String vendorIdName = (String) entry.getKey();

				Map<String, Integer> vendorSkillCount = (Map<String, Integer>) entry
						.getValue();

				String vendors[] = vendorIdName.split(",@#");

				List<SkillCountVO> skillCountVoList = new LinkedList<SkillCountVO>();

				for (Map.Entry vendorSkill : vendorSkillCount.entrySet())
				{

					String skillNameId = (String) vendorSkill.getKey();

					int count = (int) vendorSkill.getValue();

					String[] skillNameIdArr = skillNameId.split(",@#");

					SkillCountVO skillCountVO = new SkillCountVO();

					skillCountVO.setCount(count);

					skillCountVO.setSkillId(Long.valueOf(skillNameIdArr[0]));

					skillCountVO.setSkillName(skillNameIdArr[1]);

					log.debug("vendorSkill @@@@@ " + vendorSkill
							+ " skillNameId " + skillNameIdArr[0] + " count "
							+ skillNameIdArr[1]);

					log.debug("before adding in skill count list "
							+ skillCountVO);

					skillCountVoList.add(skillCountVO);
				}

				log.debug("skillCountVoList " + skillCountVoList);

				VendorVoForWeb vendorSummaryVO = new VendorVoForWeb();

				vendorSummaryVO.setVendorId(Long.valueOf(vendors[0]));
				vendorSummaryVO.setVendorName(vendors[1]);

				vendorSummaryVO.setSkillCountList(skillCountVoList);

				log.debug("before adding in final skill count list "
						+ vendorSummaryVO.getSkillCountList());

				vendorSummaryVOList.add(vendorSummaryVO);
			}

			return ResponseUtil.createSuccessResponse()
					.setData(vendorSummaryVOList);
		} else
		{
			return ResponseUtil.NoRecordFound();
		}
	}

	@Override
	public APIResponse getVendorDetailsForUI(Long userid)
	{

		List<VendorTableVo> vendortableVOList = new ArrayList<VendorTableVo>();

		Map<String, Map<String, Integer>> vendorSkillMap = (Map<String, Map<String, Integer>>) getVendorDetails(userid)
				.getData();

		log.debug("vendorSkillMap  ######### " + vendorSkillMap);

		if (vendorSkillMap != null && !vendorSkillMap.isEmpty())
		{

			for (Map.Entry entry : vendorSkillMap.entrySet())
			{

				VendorTableVo vendorTableVo = new VendorTableVo();

				String vendorIdName = (String) entry.getKey();

				log.debug("outer forloop vendoridname" + vendorIdName);

				Map<String, Integer> vendorSkillCount = (Map<String, Integer>) entry
						.getValue();

				String vendors[] = vendorIdName.split(",@#");

				for (Map.Entry vendorSkill : vendorSkillCount.entrySet())
				{

					String skillNameId = (String) vendorSkill.getKey();

					int count = (int) vendorSkill.getValue();

					String[] skillNameIdArr = skillNameId.split(",@#");

					String skillName = skillNameIdArr[1];

					if (skillName.equalsIgnoreCase(SkillNameList.CXRACKFIXING))
					{

						vendorTableVo.setCxRackFixerCount(count);

					} else if (skillName
							.equalsIgnoreCase(SkillNameList.FIBERLAYING))
					{

						vendorTableVo.setfLCount(count);
					} else if (skillName
							.equalsIgnoreCase(SkillNameList.SPLICING))
					{

						vendorTableVo.setSplicerCount(count);
					} else if (skillName
							.equalsIgnoreCase(SkillNameList.COPPERLAYING))
					{

						vendorTableVo.setcLCount(count);
					}

					log.debug("vendorSkill @@@@@ " + vendorSkill
							+ " skillNameId " + skillNameIdArr[0] + " count "
							+ skillNameIdArr[1]);

				}

				VendorVoForWeb vendorSummaryVO = new VendorVoForWeb();

				vendorTableVo.setVendorId(Long.valueOf(vendors[0]));

				vendorTableVo.setVendorName(vendors[1]);

				log.debug("before adding in final skill count list "
						+ vendorSummaryVO.getSkillCountList());

				vendortableVOList.add(vendorTableVo);
			}

			return ResponseUtil.createSuccessResponse()
					.setData(vendortableVOList);
		} else
		{
			return ResponseUtil.NoRecordFound();
		}
	}

	@Override
	public APIResponse getAllVendors()
	{

		List<VendorsRecordVO> vendovo = vendorDao.getAllVendors();

		if (!vendovo.isEmpty())
		{

			return ResponseUtil.recordFoundSucessFully().setData(vendovo);

		} else
		{

			return ResponseUtil.NoRecordFound();
		}

	}

	@Override
	public APIResponse getVendorsOnFilter(VendorFilter vendorFilter)
	{

		List<VendorsRecordVO> vendovo = vendorDao
				.getVendorsOnFilter(vendorFilter);

		log.debug("vendovo" + vendovo);

		if (vendovo != null)
		{

			return ResponseUtil.recordFoundSucessFully().setData(vendovo);

		} else
		{

			return ResponseUtil.NoRecordFound();
		}
	}

	@Override
	public APIResponse getVendorsOnUserAndLocationFilter(
			VendorFilter vendorFilter)
	{

		vendorFilter.setUserId(AuthUtils.getCurrentUserId());

		log.info("##########" + AuthUtils.getCurrentUserId());

		vendorFilter.setAreaId(AuthUtils.getCurrentUserArea().getId());

		log.info("#########" + AuthUtils.getCurrentUserArea().getId());

		List<VendorsRecordVO> vendovo = vendorDao
				.getVendorsOnUserAndLocationFilter(vendorFilter);

		log.debug("vendovo" + vendovo);

		if (vendovo != null && !vendovo.isEmpty())
		{

			return ResponseUtil.recordFoundSucessFully().setData(vendovo);

		} else
		{

			return ResponseUtil.NoRecordFound();
		}

	}

	/*
	 * private VendorVO commonValueForAssignBYTL(InputVendorVO inputVendorvo){
	 * 
	 * VendorVO vendor=new VendorVO();
	 * 
	 * if (vendor.getId()>0) {
	 * vendor.setModifiedBy(Long.valueOf(inputVendorvo.getUserId()));
	 * 
	 * vendor.setAddedBy(Long.valueOf(inputVendorvo.getUserId()));
	 * 
	 * vendor.setDate(new Date()); }
	 * 
	 * //vendor.setModifiedOn(new Date());
	 * 
	 * return vendor; }
	 */

	/*
	 * @Override public APIResponse getAllVendorsByCityId(VendorFilter
	 * vendorFilter) {
	 * 
	 * List<VendorsRecordVO>
	 * vendovo=vendorDao.getAllVendorsByCityId(vendorFilter);
	 * 
	 * if(!vendovo.isEmpty()){
	 * 
	 * return ResponseUtil.recordFoundSucessFully().setData(vendovo);
	 * 
	 * }else{
	 * 
	 * return ResponseUtil.NoRecordFound(); } }
	 * 
	 * @Override public APIResponse getAllVendorsByBranchId(VendorFilter
	 * vendorFilter) {
	 * 
	 * List<VendorsRecordVO>
	 * vendovo=vendorDao.getAllVendorsByBranchId(vendorFilter);
	 * 
	 * if(!vendovo.isEmpty()){
	 * 
	 * return ResponseUtil.recordFoundSucessFully().setData(vendovo);
	 * 
	 * }else{
	 * 
	 * return ResponseUtil.NoRecordFound(); } }
	 * 
	 * 
	 * 
	 * 
	 * @Override public APIResponse getAllVendorsByDate(VendorFilter
	 * vendorFilter) {
	 * 
	 * List<VendorsRecordVO>
	 * vendovo=vendorDao.getAllVendorsByDate(vendorFilter);
	 * 
	 * if(!vendovo.isEmpty()){
	 * 
	 * return ResponseUtil.recordFoundSucessFully().setData(vendovo);
	 * 
	 * }else{
	 * 
	 * return ResponseUtil.NoRecordFound(); } }
	 */

	@Override
	public APIResponse vendorList()
	{

		List<TypeAheadVo> vendorIdAndNameList = new ArrayList<TypeAheadVo>();

		List<VendorsRecordVO> vendovo = vendorDao.getAllVendors();

		log.info("vendor list records :::::::" + vendovo);

		if (vendovo != null && !vendovo.isEmpty())
		{

			for (VendorsRecordVO vendorsRecordVO : vendovo)
			{

				TypeAheadVo typeAheadVo = new TypeAheadVo();

				log.debug("vendor id ::::" + vendorsRecordVO.getId());

				typeAheadVo.setId(vendorsRecordVO.getId());

				typeAheadVo.setName(vendorsRecordVO.getName());

				vendorIdAndNameList.add(typeAheadVo);

			}
			return ResponseUtil.recordFoundSucessFully()
					.setData(vendorIdAndNameList);
		} else
		{
			return ResponseUtil.NoRecordFound();
		}

	}

	@Override
	public APIResponse skillList()
	{

		//List<TypeAheadVo> skillIdAndNameList = new ArrayList<TypeAheadVo>();

		// List<VendorSummaryVO> skillList = vendorDao.skillList();

		Map<Long, String> skillMap = VendorDAOImpl.skillIdAndNameCache;

		List<TypeAheadVo> skilllist = CommonUtil
				.xformToTypeAheadFormat(skillMap);

		if (skillMap != null && !skillMap.isEmpty())
		{

			log.debug("skilll cache value:::::::" + skillMap);

			return ResponseUtil.recordFoundSucessFully().setData(skilllist);

		} else
		{

			return ResponseUtil.NoRecordFound();
		}

	}

	@Override
	public APIResponse getVendorSkillDetailsByTl()
	{
		List<Long> reportToId = new ArrayList<Long>();
		TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
		if(filter != null )
		{
			if(filter.getAssignedByUserId()  !=  null && filter.getAssignedByUserId() > 0)
			{
				if(!userHelper.isFRTLUser(filter.getAssignedByUserId()) )
				{
					Map<Long, String> reportedUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(), null, null, null);
					for( Map.Entry<Long ,String> entry : reportedUsers.entrySet() )
					{
						if(userHelper.isFRTLUser(entry.getKey()))
						{
							reportToId.add(entry.getKey());
						}
					}
				}else {
					reportToId.add(filter.getAssignedByUserId() );
				}
			}
		}
		
		if(AuthUtils.isFRTLUser())
		{
			Long userId = AuthUtils.getCurrentUserId();
			 reportToId.add(userId);
		}
		if(reportToId.isEmpty())
			reportToId.add(-1l);
		List<VendorDetailSummaryVO> list = vendorDao.getVendorSkillDetailByTl(reportToId);
		return ResponseUtil.createSuccessResponse().setData(list);
	}
}
