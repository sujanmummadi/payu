package com.cupola.fwmp.service.vendor;


import java.util.Date;
import java.util.List;

import com.cupola.fwmp.filters.VendorFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.InputVendorVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.VendorSummaryVO;
import com.cupola.fwmp.vo.VendorVO;
import com.cupola.fwmp.vo.VendorsRecordVO;

public interface VendorCoreService {


    public APIResponse addVendor(VendorVO vendor);
    
    public APIResponse getAllVendors();
	
	public APIResponse updateVendor(VendorVO vendor);
	
	APIResponse deleteVendor(Long id);
	
	APIResponse getVendorsOnFilter(VendorFilter vendorFilter);
	
	APIResponse getVendorsOnUserAndLocationFilter(VendorFilter vendorFilter);
	
	/*APIResponse getAllVendorsByBranchId(VendorFilter vendorFilter);
	
	APIResponse getAllVendorsByDate(VendorFilter vendorFilter);*/
	
	public  APIResponse getCountAllVendor();
	
	public  APIResponse getCountAllVendorBySkill(Long id);
	
	APIResponse addVendorSkillAndCount(VendorVO vendorvo);
	
	public  APIResponse updateCountSkill(InputVendorVO inputVendorvo); 
	
	APIResponse addIntoVendorSkillMapping(VendorVO vendorvo);
	
	APIResponse vendorAssignmentForTL(InputVendorVO inputVendorvo);
	
	APIResponse addUserVendorMapping(VendorVO vendorvo);
	
	APIResponse getVendorDetails(Long userid);
	
	APIResponse vendorAssignmentByTLForNE(InputVendorVO inputVendorvo);
	
	APIResponse getVendorDetailsForAndroid(Long userid);
	
	
	 APIResponse getVendorDetailsForUI(Long userid);
	 
	 APIResponse vendorList();
	 
	 public APIResponse skillList();

	public APIResponse getVendorSkillDetailsByTl();
	 
}
