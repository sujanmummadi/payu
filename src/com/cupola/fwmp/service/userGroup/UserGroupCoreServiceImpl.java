package com.cupola.fwmp.service.userGroup;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.cupola.fwmp.dao.userGroup.UserGroupDAO;
import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.persistance.entities.UserGroup;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserGroupVo;

public class UserGroupCoreServiceImpl implements UserGroupCoreService{
	
	final static Logger log = Logger.getLogger(UserGroupCoreServiceImpl.class);
	private Map<Long, String> groupReportToGroupMap = new ConcurrentHashMap<>();

	private UserGroupDAO userGroupDAO;
	
	public void setUserGroupDAO(UserGroupDAO userGroupDAO)
	{
		this.userGroupDAO = userGroupDAO;
	}

	@Override
	public APIResponse addNewUserGroup(UserGroupVo userGroupVo) {
		
		UserGroup group = userGroupDAO.addUserGroup(userGroupVo);
		
		if(group.getStatus() == StatusCodes.SAVE_FAILED)
			return ResponseUtil.createSaveFailedResponse();
		
		else if(group.getStatus() == 1)
			return ResponseUtil.createSaveSuccessResponse();
		
		else
		 return ResponseUtil.createDBExceptionResponse();
	}

	@Override
	public APIResponse updateUserGroup(UserGroupVo userGroupVo) {
		
		UserGroup group = userGroupDAO.updateUserGroup(userGroupVo);
		
		if(group.getStatus() == StatusCodes.UPDATE_FAILED)
			return ResponseUtil.createUpdateFailedResponse();
		
		else if(group.getStatus() == 1)
			return ResponseUtil.createUpdateSuccessResponse();
		
		else
		 return ResponseUtil.createDBExceptionResponse();
	}
	
	@Override
	public APIResponse deleteUserGroup(UserGroupVo userGroupVo) {
		
		UserGroup group = userGroupDAO.deleteUserGroup(userGroupVo);
		
		 if(group.getStatus() == 0)
			return ResponseUtil.createDeleteSuccessResponse();
		
		else
		 return ResponseUtil.createDBExceptionResponse();
	}
	
	@Override
	public List<TypeAheadVo> getAllGroupNames() {
		return CommonUtil.xformToTypeAheadFormat(userGroupDAO.getAllGroupNames());
	}

	@Override
	public List<TypeAheadVo> getAllRoleNames(){
		
		return CommonUtil.xformToTypeAheadFormat(userGroupDAO.getAllRoleNames());
	}

	@Override
	public APIResponse getAllUserGroups()
	{
		List<UserGroupVo> groupVos = userGroupDAO.getAllUserGroups();
		if(groupVos.isEmpty())
			return ResponseUtil.createRecordNotFoundResponse();
		else
			return ResponseUtil.createSuccessResponse().setData(groupVos);
		
	}

	@Override
	public APIResponse getUserGroups(UserFilter filter)
	{
		return ResponseUtil.createSuccessResponse().setData(userGroupDAO.getUserGroups(filter));
	}
	
	@Override
	public	APIResponse getRolesByUserGroupId(Long groupId)
	{
		return ResponseUtil.createSuccessResponse().setData(userGroupDAO.getRolesByUserGroupId(groupId));
	}

	@Override
	public APIResponse getEditableGroupByCurrentUser()
	{
		return ResponseUtil.createSuccessResponse().setData(userGroupDAO.getEditableGroupByCurrentUser());
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.userGroup.UserGroupCoreService#getImmediateManagerList()
	 */
	@Override
	public APIResponse getImmediateManagerList() {
		
		if(groupReportToGroupMap == null)
			groupReportToGroupMap = new ConcurrentHashMap<>();

		if (groupReportToGroupMap != null || groupReportToGroupMap.isEmpty()) {
			List<UserGroupVo> groupVos = userGroupDAO.getImmediateManagerList();
			for (UserGroupVo userGroupVo : groupVos) {
				groupReportToGroupMap.put(userGroupVo.getId(), userGroupVo.getImmediateManagerGroup());
			}
		}

		return ResponseUtil.createSuccessResponse().setData(groupReportToGroupMap);
	}
	
}
