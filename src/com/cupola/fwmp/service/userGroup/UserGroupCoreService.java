package com.cupola.fwmp.service.userGroup;

import java.util.List;

import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserGroupVo;

public interface UserGroupCoreService{

	APIResponse addNewUserGroup(UserGroupVo userGroupVo);
	
	APIResponse updateUserGroup(UserGroupVo userGroupVo);
	
	APIResponse deleteUserGroup(UserGroupVo userGroupVo);
	
	APIResponse getAllUserGroups();
	
	APIResponse getUserGroups(UserFilter filter);
	
	List<TypeAheadVo> getAllGroupNames();
	
	List<TypeAheadVo> getAllRoleNames();

	APIResponse getRolesByUserGroupId(Long groupId);

	APIResponse getEditableGroupByCurrentUser();

	/**@author aditya
	 * APIResponse
	 * @return
	 */
	APIResponse getImmediateManagerList();

}
