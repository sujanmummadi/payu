package com.cupola.fwmp.service.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.FWMPConstant.MQStatus;
import com.cupola.fwmp.FWMPConstant.MessagePlaceHolder;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.mongo.documents.DocumentDao;
import com.cupola.fwmp.dao.mongo.logger.LoggerDao;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.service.closeticket.MQSCloseTicketService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.notification.EmailNotiFicationHelper;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.service.userNotification.UserNotificationCoreService;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.AuditLogVo;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.UserNotificationVo;

public class TicketStatusUpdateHandler
{

	@Autowired
	TicketUpdateSubscriptionService subscriptionService;

	@Autowired
	TicketDAO ticketDao;

	@Autowired
	UserDao userDao;

	@Autowired
	LoggerDao loggerDao;

	@Autowired
	WorkOrderDAO workOrderDAO;

	@Autowired
	DefinitionCoreService coreService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	UserNotificationCoreService userNotificationCoreService;

	@Autowired
	MQSCloseTicketService closeTicketService;
	@Autowired
	WorkOrderCoreService workOrderCoreService;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	FrTicketDao frTicketDao;

	@Autowired
	DocumentDao documentDao;

	@Autowired
	MailService mailService;
	@Autowired
	EmailNotiFicationHelper emailNotiFicationHelper;
	@Autowired
	TicketActivityLogDAO ticketActivityLogDAO;
	static final Logger logger = Logger
			.getLogger(TicketStatusUpdateHandler.class);
	BlockingQueue<TicketLog> ticketQueue;
	Thread updateProcessorThread = null;

	static String FINANCE_TEAM_KEY = "finance.dept.emailIds.";

	void createSubscritions()
	{
		logger.info("createSubscritions called----");
		subscriptionService
				.subscribe(updateHandler, TicketUpdateConstant.UPDATE_CATEGORY_ALL);
		logger.info("createEventSubscritions done----");
	}

	public void init()
	{
		logger.info("enabling updateProcessorThread processing-");
		
		createSubscritions();

		ticketQueue = new LinkedBlockingQueue(100000);

		if (updateProcessorThread == null)
		{
			updateProcessorThread = new Thread(run, "TicketStatusUpdateHandler");
			updateProcessorThread.start();
		}
		logger.info("enabling updateProcessorThread enabled");
	}

	Runnable run = new Runnable()
	{

		public void run()
		{
			try
			{
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_DEVICE_CACHE_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);				
				while (true)
				{
					final TicketLog ticketLog = ticketQueue.take();

					try
					{
						Runnable worker = new Runnable()
						{
							@Override
							public void run()
							{
								try
								{
									
									logger.debug(ticketLog.getId()
											+ " Current Processing ticketLog "
											+ ticketLog
													.getCurrentUpdateCategory()
											+ " ticketLog getRemarks"
											+ ticketLog.getRemarks());

//									updateActivityRemarks( ticketLog );

									logger.debug("########################################### Test #######################"
											+ ticketLog.getTicketId()
											+ " ticketLog.getActivityId() "
											+ ticketLog.getActivityId()
											+ " ticketLog.getRemarks() "
											+ ticketLog.getRemarks()
											+ " ticketLog.getStatusId() "
											+ ticketLog.getStatusId());


									if( ticketLog.isFrTicket() )
										doOnFaultRepair(ticketLog);
									else
										doOnSalesAndDeployment(ticketLog);
									
									
								} 
								catch (Exception e)
								{
									logger.error("Error While executing currtent proccesing ticketlog :"
									+ e.getMessage());
									e.printStackTrace();
								}
							}
						};
						executor.execute(worker);
					} catch (Exception e)
					{
						logger.error("Error While executing currtent proccesing ticketlog :"
								+ e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
						continue;
					}

				}

			} catch (Exception e)
			{
				logger.error("error in Processor main loop- msg="
						+ e.getMessage());
				e.printStackTrace();
			}

			logger.debug("Processor thread interrupted- getting out of processing loop");

		}
		
		private void doOnSalesAndDeployment( TicketLog ticketLog )
		{
			if( ticketLog == null )
				return ;
			
			logger.info("************* Audit log for Sale & Deployment **************");
			try 
			{
				AuditLogVo auditLogVo = customerDAO.getCustomerDetailByTicketId( ticketLog.getTicketId() );
				
				updateActivityRemarks( ticketLog );
				
				if (TicketStatus.POI_DOCUMENTS_APPROVED == ticketLog
						.getStatusId()
						|| TicketStatus.POI_DOCUMENTS_REJECTED == ticketLog
								.getStatusId() && TicketStatus.POA_DOCUMENTS_APPROVED == ticketLog
										.getStatusId()
										|| TicketStatus.POA_DOCUMENTS_REJECTED == ticketLog
												.getStatusId())
				{


					documentDao
							.updateCustomerDocumentsStatus(ticketLog
									.getId(), ticketLog
											.getStatusId(), ticketLog
													.getAddedBy());
				}
				
				removeFromSalesQueue( auditLogVo );
				handleCustomerNotification(ticketLog, auditLogVo );
				handleUserNotification(ticketLog , auditLogVo );
			} 
			catch (Exception e) 
			{
				logger.error("Error occured in ****** Audit log for Sale & Deployment ******* ");
				e.printStackTrace();
			}
		}
		
		private void doOnFaultRepair( TicketLog ticketLog )
		{
			if( ticketLog == null )
				return ;

			try
			{
				Long statusId = ticketLog.getStatusId();
				if (statusId != null
					&& ActionTypeAttributeDefinitions.FR_ACTION_PENDING_STATUS_LIST
							.contains(statusId))
				{
					logger.info("Inserting status in Fr Detail..." + statusId);
					frTicketDao.updateFrTicketStatus(ticketLog.getTicketId(), Arrays
						.asList(statusId));
				} else if (ActionTypeAttributeDefinitions.FR_ACTION_DONE_STATUS_LIST
					.containsKey(statusId))
				{
					logger.info("Removing status from Fr Detail..." + statusId);
					Long removeStatus = ActionTypeAttributeDefinitions.FR_ACTION_DONE_STATUS_LIST
						.get(statusId);
					frTicketDao.deleteCompletedStatus(ticketLog
						.getTicketId(), Arrays.asList(removeStatus));
				}

			}
			catch (Exception e) 
			{
				logger.error("Error occured while updating audit log for FR...");
				e.printStackTrace();
			}
		}

		private void removeFromSalesQueue( AuditLogVo tcket )
		{

			if( tcket == null )
				return;

			if ( TicketStatus.POI_DOCUMENTS_APPROVED == tcket
								.getDocumentStatus() && TicketStatus.POA_DOCUMENTS_APPROVED == tcket
										.getDocumentStatus()
						&& TicketStatus.PAYMENT_APPROVED == tcket
								.getPaymentStatus())
				{
					logger.debug("Removing ticket from sales queue.."
							+ tcket.getTicketId());
					

				if (((tcket.getTicketStatus() == TicketStatus.PAYMENT_APPROVED
						|| (tcket.getTicketStatus() == TicketStatus.POI_DOCUMENTS_APPROVED
								&& tcket.getTicketStatus() == TicketStatus.POA_DOCUMENTS_APPROVED))
						&& (TicketStatus.SALES_STATUS_CODE_DEF.contains(tcket.getTicketStatus()))))
					{
						ticketDao.updateTicketStatus(tcket
							.getTicketId(), TicketStatus.WAITING_FOR_WO_GENERATION);

					}

					}
				
		}

		private void updateActivityRemarks( TicketLog ticketLog )
		{
			if (ticketLog == null)
				return;

			int status = Integer.valueOf(ticketLog.getStatusId() + "");

			logger.info("Updating Ticket Activity remarks for Ticket  " + ticketLog.getTicketId()
					+ " ticketLog.getActivityId() " + ticketLog.getActivityId() + " ticketLog.getReasonValue() "
					+ ticketLog.getReasonValue() + " ticketLog.getStatusId() " + status);

			if (definitionCoreService.getStatusFilterForRole(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER)
					.contains(status))
			{
				logger.info("Updating Ticket activity log for payment activity for ticket " + ticketLog.getTicketId());

				int updated = updateTicketClosedActivityRemarks(Activity.PAYMENT_UPDATE, ticketLog.getTicketId(),
						ticketLog.getReasonValue());

				if (updated > 0)
					logger.info("Ticket activity log for payment activity for ticket " + ticketLog.getTicketId()
							+ "is updated successfully...");
				else
					logger.info("Ticket activity log doesn't exist for payment activity for ticket "
							+ ticketLog.getTicketId());

			} 
			else if (definitionCoreService.getStatusFilterForRole(DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER)
					.contains(status))
			{

				logger.info("Ticket activity log for document activity for ticket " + ticketLog.getTicketId());

				int updated = updateTicketClosedActivityRemarks(Activity.DOCUMENT_UPDATE, ticketLog.getTicketId(),
						ticketLog.getReasonValue());

				if (updated > 0)
					logger.info("Ticket activity log for document activity for ticket " + ticketLog.getTicketId()
							+ "is updated successfully...");
				else
					logger.info("Ticket activity log doesn't exist for document activity for ticket "
							+ ticketLog.getTicketId());

			}

		}

		private void handleMqAutoClosure(TicketLog ticketLog)
		{

			if (TicketStatus.ACTIVATION_COMPLETED_VERIFIED == ticketLog
					.getStatusId())
			{
				// call mq auto closure
				String workOrderNo = workOrderDAO
						.getWoNumberForTicket(ticketLog.getTicketId());
				TicketClosureDataVo closeTicketVo = new TicketClosureDataVo();
				closeTicketVo.setWorkOrderNo(String.valueOf(workOrderNo));
				closeTicketService.closeTicket(closeTicketVo);
			}
		}

		/**
		 * @param ticketLog
		 * @throws Exception
		 */
		private void handleUserNotification( TicketLog ticketLog , AuditLogVo auditLogVo)
				throws Exception
		{

			int status = -1;
			if (auditLogVo != null)
			{
				if (ticketLog.getStatusId() == TicketStatus.PROSPECT_CREATED)
				{

					status = TicketStatus.PROSPECT_CREATED;
					sendUserNotification( status, auditLogVo );
				} 
				else
					status = auditLogVo.getTicketStatus();

			}

			if (ticketLog.getTicketId() > 0)
			{

				if (status == TicketStatus.PAYMENT_REFUND_INITIATED)
				{
					String emailIds = DefinitionCoreServiceImpl.emailCorporateDef
							.get(FINANCE_TEAM_KEY + auditLogVo.getCityCode().toLowerCase());
					
					Map<String, String> mailData = new HashMap<String, String>();
					mailData.put("prospect", auditLogVo.getProspectNo());

					if (emailIds != null)
					{

						StringBuilder text = new StringBuilder();
						EMailVO eMail = new EMailVO();
						eMail.setSubject("Refund initiated for Customer with Prospect "
								+ auditLogVo.getProspectNo());

						text.append("Team,").append("\n\n");
						text.append("Refund initiated for Customer with following details. ")
								.append("\n\n");

						text.append("Prospect ID : " + auditLogVo.getProspectNo())
								.append("\n");
						text.append("Customer Name : "
								+ auditLogVo.getCustomerName())
								.append("\n");

						text.append("Customer Mobile No. : "
								+ auditLogVo.getCustMobileNumber())
								.append("\n");
						text.append("Package booked : "
								+ auditLogVo.getPlanCode()
								+ ", " + auditLogVo.getPackageName())
								.append("\n");
						text.append("Amount Paid : "
								+ auditLogVo.getPaidAmount())
								.append("\n");
						text.append("Date of booking : " + GenericUtil
								.convertToUiDateFormat(auditLogVo.getAddedOn()))
								.append("\n");
						text.append("Reason for Refund : "
								+ auditLogVo.getPaymentRemark()).append("\n");
						text.append("").append("\n\n");

						text.append("Regards,").append("\n");
						text.append("ACT FIBERNET").append("\n\n");
						eMail.setMessage(text.toString());
						eMail.setTo(emailIds);
						emailNotiFicationHelper.addNewMailToQueue(eMail);
					}

				}

				Set<Integer> ticketStatusSet = coreService
						.getTicketStatusDef(FWMPConstant.TicketNotificationStatus.USER_NOTIFICATION_STATUS);

				logger.debug("When to send User notification, status are "
						+ ticketStatusSet + " given status is " + status);

				if (ticketStatusSet.contains(status))
				{
					UserNotificationVo vo = new UserNotificationVo();
					vo.setReasonCode(ticketLog.getReasonCode());
					vo.setReasonValue(ticketLog.getReasonValue());
					vo.setRemarks(ticketLog.getRemarks());
					vo.setStatus(status + "");
					vo.setTicketId(ticketLog.getTicketId());
					userNotificationCoreService.sendUserNotification(vo);

				}

			}
		}

		/**
		 * @param ticketLog
		 * @throws Exception
		 */
		private void handleCustomerNotification( TicketLog ticketLog, AuditLogVo tcket )
		{
			try
			{

				if (tcket != null)
				{
					int status = -1;
					if (MQStatus.SUCCESS.equals(tcket.getMqStatus())
							&& ticketLog.getStatusId() == TicketStatus.PROSPECT_CREATED)
						
						status = TicketStatus.PROSPECT_CREATED;
					else
						status = tcket.getTicketStatus();

					sendNotification( tcket, status, ticketLog);
				}

				long activityId = ticketLog.getActivityId();

				if (activityId > 0)
				{

					Set<Long> activityStatusSet = coreService
							.getActivityStatusDef(FWMPConstant.ActivityNotificationStatus.ACTIVITY_NOTIFICATION_STATUS);

					if (activityStatusSet.contains(activityId))
					{

						String statusMessageCode = coreService
								.getNotificationMessageFromProperties(String
										.valueOf(activityId));

						String messageToSend = coreService
								.getMessagesForCustomerNotificationFromProperties(statusMessageCode);

						String todayDate = GenericUtil
								.convertToCustomerDateFormat(new Date());

						if (messageToSend != null
								&& messageToSend.contains("_DATE_"))
							messageToSend.replaceAll("_DATE_", todayDate);

						if ( tcket != null && messageToSend != null)
						{

							MessageNotificationVo notificationVo = new MessageNotificationVo();
							notificationVo.setMobileNumber(tcket
									.getCustMobileNumber());
							notificationVo.setEmail(tcket.getEmailId());
							notificationVo.setMessage(messageToSend);
							notificationVo.setSubject("ACT Notification");

							notificationService
									.sendNotification(notificationVo);
						}

						if (activityId == 2)
						{

							logger.info("ACTIVITY IS CUSTOMER ACC ACTIVATION. SO SENDING VERIFICATION MESSAGE");
							// get status code for account activation
							// verification
							statusMessageCode = coreService
									.getNotificationMessageFromProperties(String
											.valueOf(TicketStatus.CUSTOMER_ACCOUNT__ACTIVATE_SUCCESSFULLY));

							// get pre defined message for status code for
							// account activation verification
							messageToSend = coreService
									.getMessagesForCustomerNotificationFromProperties(statusMessageCode);

							// send sms to customer
							if (messageToSend != null)
							{
								MessageNotificationVo notificationVo = new MessageNotificationVo();
								notificationVo.setMobileNumber(tcket
										.getCustMobileNumber());
								notificationVo.setMessage(messageToSend);
								logger.info("SMS Service :: Message to send contains =========== "
										+ notificationVo);

								// REPLACE WITH SMS SERVICE LATER
								notificationService
										.sendNotification(notificationVo);
							}
						}
					}
				}
			} catch (Exception e)
			{

				logger.error("Exception in handleCustomerNotification. Reason : "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

		private void sendUserNotification( int status, AuditLogVo auditLogVO )
		{

			if ( auditLogVO != null && auditLogVO.getAssignedMobileNumber() != null )
			{

				String messageToSend = coreService
						.getMessagesForCustomerNotificationFromProperties(DefinitionCoreService.PROSPECT_CREATION_USER_MESSAGE_KEY);

				String areaName = "NA";
				if (auditLogVO.getAreaName() != null)
					areaName = auditLogVO.getAreaName();

				if (messageToSend != null && !messageToSend.isEmpty())
				{
					messageToSend = messageToSend
							.replaceAll(MessagePlaceHolder.DATE, GenericUtil
									.convertToUiDateFormat(auditLogVO
											.getPreferedDate()))
							.replaceAll(MessagePlaceHolder.PROSPECT_NUMBER, auditLogVO
									.getProspectNo())
							.replaceAll(MessagePlaceHolder.CUSTOMER_NAME, auditLogVO
									.getCustomerName())
							.replaceAll(MessagePlaceHolder.MOBILE, auditLogVO
									.getCustMobileNumber())
							.replaceAll(MessagePlaceHolder.ADDRESS, auditLogVO
									.getCommunicationAdderss())
							.replaceAll(MessagePlaceHolder.AREA, areaName);

					MessageNotificationVo notificationVo = new MessageNotificationVo();
					notificationVo.setMobileNumber(auditLogVO.getAssignedMobileNumber());
					notificationVo.setEmail(null);
					notificationVo.setMessage(messageToSend);
					notificationVo.setSubject("ACT Notification");
					notificationService.sendNotification(notificationVo);
				}

			}
		}

		/**
		 * @param customer
		 * @param status
		 * @param string
		 */
		private void sendNotification( AuditLogVo auditLogVo, int status,
				 TicketLog ticketLog)
		{
			Set<Integer> ticketStatusSet = coreService
					.getTicketStatusDef(FWMPConstant.TicketNotificationStatus.CUSTOMER_NOTIFICATION_STATUS);

			logger.debug(status
					+ " When to send customer notification, status are "
					+ ticketStatusSet + " given status is " + status);
			String prospectNumber = auditLogVo.getProspectNo();

			String messageToSend = null;
			String helpLineNo = definitionCoreService
					.getNIHelpLineNumber(LocationCache
							.getCityCodeById(auditLogVo.getCityId()));

			if (ticketStatusSet.contains(status)
					|| ticketLog.getStatusId() == TicketStatus.WO_GENERATED)
			{
				if (ticketLog.getStatusId() == TicketStatus.WO_GENERATED)
				{
					sendWONotification( auditLogVo , ticketLog, helpLineNo);

				}

				else
				{
					String statusMessageCode = coreService
							.getNotificationMessageFromProperties(String
									.valueOf(status));

					logger.debug(" statusMessageCode " + statusMessageCode);

					messageToSend = coreService
							.getMessagesForCustomerNotificationFromProperties(statusMessageCode);

					String todayDate = GenericUtil
							.convertToCustomerDateFormat(new Date());

					logger.debug(" messageToSend " + messageToSend + todayDate);

					if (messageToSend != null)
					{

						String portalUrl = definitionCoreService
								.getPortalPageUrl(LocationCache
										.getCityCodeById(auditLogVo.getCityId()));

						messageToSend = messageToSend
								.replaceAll(MessagePlaceHolder.DATE, todayDate)
								.replaceAll(MessagePlaceHolder.PROSPECT_NUMBER, prospectNumber)
								.replaceAll(MessagePlaceHolder.DOC_REJECT_CODE, auditLogVo
										.getDocumentRemark())
								.replaceAll(MessagePlaceHolder.PAYMENT_REJECT_CODE, auditLogVo
										.getPaymentRemark())

								.replaceAll(MessagePlaceHolder.HELP_LINE_NUMBER, helpLineNo)
								.replaceAll(MessagePlaceHolder.PORTAL_URL, portalUrl);

					}

					if ( auditLogVo != null && messageToSend != null)
					{
						logger.debug("Sending customer notification to customer Name "
								+ auditLogVo.getCustomerName() + " Mobile number "
								+ auditLogVo.getCustMobileNumber()
								+ " and email id is " + auditLogVo.getEmailId()
								+ " Mq id " + auditLogVo.getMqId()
								+ " Message to send is " + messageToSend);

						MessageNotificationVo notificationVo = new MessageNotificationVo();
						notificationVo
								.setMobileNumber(auditLogVo.getCustMobileNumber());
						notificationVo.setEmail(null);
						notificationVo.setMessage(messageToSend);
						notificationVo.setSubject("ACT Notification");

						notificationService.sendNotification(notificationVo);
					}

				}
			}
		}

		/**
		 * @param customer
		 * @param ticket
		 * @param ticketLog
		 * @param helpLineNo
		 */
		private void sendWONotification(AuditLogVo auditLogVo,
				TicketLog ticketLog, String helpLineNo)
		{

			logger.debug(" sendWONotification statusMessageCode ");
			String messageToSend;
			messageToSend = coreService
					.getMessagesForCustomerNotificationFromProperties(DefinitionCoreService.WO_CREATION_MESSAGE_KEY);
			messageToSend = messageToSend
					.replaceAll(MessagePlaceHolder.WORK_ORDER_NO, ticketLog
							.getWoNumber() + "")
					.replaceAll(MessagePlaceHolder.MQID, auditLogVo.getMqId())
					.replaceAll(MessagePlaceHolder.HELP_LINE_NUMBER, helpLineNo)
					.replaceAll("_DATE_", GenericUtil
							.convertToCustomerDateFormat(auditLogVo
									.getCommunicationETR()));

			MessageNotificationVo notificationVo = new MessageNotificationVo();
			notificationVo.setMobileNumber(auditLogVo.getCustMobileNumber());
			notificationVo.setEmail(auditLogVo.getEmailId());
			notificationVo.setMessage(messageToSend);
			notificationVo.setSubject("ACT Notification");
			notificationService.sendNotification(notificationVo);

			if (auditLogVo.getCommunicationETR() != null)
			{
				messageToSend = coreService
						.getMessagesForCustomerNotificationFromProperties(DefinitionCoreService.ETR_MESSAGE_KEY);
				messageToSend = messageToSend
						.replaceAll(MessagePlaceHolder.WORK_ORDER_NO, ticketLog
								.getWoNumber() + "")
						.replaceAll(MessagePlaceHolder.MQID, auditLogVo.getMqId())
						.replaceAll(MessagePlaceHolder.HELP_LINE_NUMBER, helpLineNo)
						.replaceAll("_DATE_", GenericUtil
								.convertToCustomerDateFormat(auditLogVo
										.getCommunicationETR()));
				MessageNotificationVo notificationVo1 = new MessageNotificationVo();
				notificationVo1.setMobileNumber(auditLogVo.getCustMobileNumber());

				notificationVo1.setEmail(auditLogVo.getEmailId());
				notificationVo1.setMessage(messageToSend);
				notificationVo1.setSubject("ACT Notification");
				notificationService.sendNotification(notificationVo1);
			}
		}

	};

	TicketUpdateHandler updateHandler = new TicketUpdateHandler()
	{

		public void handleTicket(TicketLog ticketLog)
		{
			if (ticketLog == null)
			{
				return;
			}
			try
			{
				ticketQueue.add(ticketLog);

			} catch (Exception e)
			{
				e.printStackTrace();
				logger.error("error" + e.getMessage());
			}

		}

	};

	public TicketUpdateSubscriptionService getSubscriptionService()
	{
		return subscriptionService;
	}

	public void setSubscriptionService(
			TicketUpdateSubscriptionService subscriptionService)
	{
		this.subscriptionService = subscriptionService;
	}

	private int updateTicketClosedActivityRemarks(long activityId, long ticketId,
			String remarks)
	{

		TicketActivityLogVo ticketAcitivityDetailsVO = new TicketActivityLogVo();

		ticketAcitivityDetailsVO.setModifiedOn(new Date());
		ticketAcitivityDetailsVO.setActivityId(activityId);
		ticketAcitivityDetailsVO.setTicketId(ticketId);
		ticketAcitivityDetailsVO.setRemarks(remarks);

		return ticketActivityLogDAO
				.updateTicketActivityLogRemarksForClosedActivity( ticketAcitivityDetailsVO );
		
		
	}

}
