
package com.cupola.fwmp.service.domain.gateway;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.cupola.fwmp.service.domain.logger.TicketLog;

public class TicketQueue
{
	private static final Logger LOGGER = Logger.getLogger (TicketQueue.class);
	private LinkedList tickets;
	private String queueName;
	private boolean isNotificationOnEnqueueRequired;
	private int maxQueueSize;
	public long queuedCount;
	public long dequeuedCount;

	public TicketQueue (int maxQueueSize, String queueName, boolean isNotificationOnEnqueueRequired)
	{
		this.tickets = new LinkedList ();
		this.maxQueueSize = maxQueueSize;
		this.queueName = queueName;
		this.isNotificationOnEnqueueRequired = isNotificationOnEnqueueRequired;
	}

	public  void enqueue (TicketLog ticketLog)
	{
		if (tickets.size () >= maxQueueSize) {
			TicketLog ticketDropped = (TicketLog) tickets.removeFirst ();
			LOGGER.error (" queue '" + queueName + "' reached max queue size '" + maxQueueSize
				+ "', unprocessed ticketLog '" + ticketDropped.getId()
				+ "' is being dropped. Total (queued, dequeued, unprocessed-message)=("
				+ this.queuedCount + "," + this.dequeuedCount + ","
				+ (this.queuedCount - this.dequeuedCount) + ")");
		}
		tickets.add (ticketLog);
		queuedCount++;

		if (isNotificationOnEnqueueRequired) {
			this.notifyAll ();
		}
	}

	public  List<TicketLog> dequeue ()
	{
		if (tickets.size () > 0) {
			List<TicketLog> dequeueTickets = tickets;
			dequeuedCount += dequeueTickets.size ();
			this.tickets = new LinkedList ();
			return dequeueTickets;
		} else {
			return null;
		}

	}
}
