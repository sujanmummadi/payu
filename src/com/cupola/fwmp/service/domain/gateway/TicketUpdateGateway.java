package com.cupola.fwmp.service.domain.gateway;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.TicketUpdateSubscriptionService;
import com.cupola.fwmp.service.domain.logger.TicketLog;

public class TicketUpdateGateway
{
	private static Logger LOGGER = Logger
			.getLogger(TicketUpdateGateway.class.getName());
	private static TicketProcessor ticketProcessor = null;
	private static TicketUpdateSubscriptionService subscriptionService;

	public static void receiveNewTicket(TicketLog ticketLog)
	{
		setRequiredTicketAttribute(ticketLog);

		ticketProcessor.receiveTicket(ticketLog);

	}

	public static void logTickets(List<TicketLog> tickets)
	{
		/*
		 * Comparator<TicketLog> comp = new Comparator <TicketLog> () {
		 * 
		 * public int compare (TicketLog o1, TicketLog o2) { return (int)
		 * (o1.getId() - o2.getId ()); }
		 * 
		 * };
		 * 
		 * Collections.sort (tickets, comp);
		 */
		for (Iterator iter = tickets.iterator(); iter.hasNext();)
		{
			TicketLog ticketLog = (TicketLog) iter.next();
			logTicket(ticketLog);
		}

	}

	public static void logTicket(TicketLog ticketLog)
	{
		/**
		 * This API could be called even before ticketLog module initialized.
		 */
		if (ticketProcessor == null)
		{
			startNewProcessingThread();
		}
		receiveNewTicket(ticketLog);
	}

	private static void setRequiredTicketAttribute(TicketLog ticketLog)
	{
	}

	public static void startNewProcessingThread()
	{
		if (ticketProcessor == null)
		{
			ticketProcessor = new TicketProcessor();
			ticketProcessor.startNewProcessingThread();
		}
	}
	public static void terminateProcessingThread()
	{
		if (ticketProcessor == null)
		{
			ticketProcessor = new TicketProcessor();
			ticketProcessor.startNewProcessingThread();
		}
	}

//	public static void terminateProcessingThread()
//	{
//		if (ticketProcessor != null)
//		{
//			ticketProcessor.terminateProcessingThread();
//			ticketProcessor = null;
//		}
//	}

	private static class TicketProcessor implements Runnable
	{
		private BlockingQueue<TicketLog> queue;

		public TicketProcessor()
		{
			queue = new LinkedBlockingQueue<TicketLog>(100000);
		}

		public void startNewProcessingThread()
		{
			Thread thread = new Thread(this);
			/* runnable is very much initilized by now, start thread */
			thread.start();
		}

//		public void terminateProcessingThread()
//		{
//			synchronized (queue)
//			{
//				queue.notifyAll();
//			}
//		}
		public void terminateProcessingThread()
		{
			synchronized (queue)
			{
				queue.notifyAll();
			}
		}

		public void run()
		{		
			LOGGER.info(" ticketLog processor thread starting");
 			try
			{
				String threadPoolSizeString = null;

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);
				
				while (true)
				{
					final TicketLog tickets = queue.take();
					try {

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									if (tickets != null) {
										notifyInternalTicketSubscribers(tickets);
										// logNewTicketsToDb(tickets);
									}

								} catch (Exception e) {
									e.printStackTrace();
									LOGGER.error("Error in tickets update gateway thread "
											+ " " + tickets + ""
											+ e.getMessage());
								}

							}
						};

						executor.execute(worker);

					} catch (Exception e)
					{
						e.printStackTrace();
						LOGGER.error("Error in prospect thread " + " " + tickets
								+ "" + e.getMessage());
						continue;
					}
				}
			} catch (Exception e)
			{
				e.printStackTrace();
				LOGGER.info(" ticketLog processor thread " + e.getMessage());
			}

			LOGGER.info(" ticketLog processor thread terminated");
		}

		private void notifyInternalTicketSubscribers(TicketLog ticketLog)
		{
			try
			{
				if (subscriptionService != null)
				{

					subscriptionService.processNewUpdate(ticketLog);

				} else
				{
					LOGGER.error("notifyTicketSubscribers encountered error, LocalServiceFactory cannot create TicketSubscriptionService Object");
				}
			} catch (Exception e)
			{
				LOGGER.error("notifyTicketSubscribers encountered error", e);
			}
		}

		private void receiveTicket(TicketLog ticketLog)
		{
			queue.add(ticketLog);
		}

	}

	public static TicketUpdateSubscriptionService getSubscriptionService()
	{
		return subscriptionService;
	}

	public static void setSubscriptionService(
			TicketUpdateSubscriptionService subscriptionService)
	{
		TicketUpdateGateway.subscriptionService = subscriptionService;
	}
}
