package com.cupola.fwmp.service.domain;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;

public class TicketUpdateSubscriptionServiceImpl implements TicketUpdateSubscriptionService 
{
	private static Logger LOGGER = Logger
			.getLogger(TicketUpdateSubscriptionServiceImpl.class.getName());
	private static List<TicketUpdateSubscriber> subscriptions = new LinkedList<TicketUpdateSubscriber>();

	@Override
	public void subscribe(TicketUpdateHandler handler, String updateCategory)
	{
		
		TicketUpdateSubscriber sub = new TicketUpdateSubscriber();
		sub.handler = handler;
		sub.updateCategory = updateCategory;
		
		LOGGER.info(" subscribe registering subscription: handler=" + handler);

		if (handler == null)
		{
			throw new RuntimeException(" event handler not set");
		}

		subscriptions.add(sub);
	}

	public void processNewUpdate(TicketLog ticketLog)
	{
		LOGGER.debug(" processNewticket processing subscription");

		 
			
			for (Iterator iter = subscriptions.iterator(); iter.hasNext();)
			{
				TicketUpdateSubscriber sub = (TicketUpdateSubscriber) iter.next();
				
				if (sub.updateCategory != null
						&& (sub.updateCategory.equals(ticketLog
								.getCurrentUpdateCategory()) || sub.updateCategory
								.equals(TicketUpdateConstant.UPDATE_CATEGORY_ALL)))
				{	
					TicketLog log = new TicketLogImpl();
					BeanUtils.copyProperties(ticketLog, log);
					sub.handler.handleTicket(log);
				}
		}

	}
	

	private class TicketUpdateSubscriber
	{
		TicketUpdateHandler handler;
		String updateCategory;

		String getDebugLog ()
		{
			return ("handler=" + handler + ", updateCategory=" + updateCategory );
		}
	}
}
