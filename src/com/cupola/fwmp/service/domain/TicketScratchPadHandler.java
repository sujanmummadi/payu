package com.cupola.fwmp.service.domain;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.fr.ScratchPadDAO;
import com.cupola.fwmp.vo.fr.ScratchVo;

public class TicketScratchPadHandler
{
	@Autowired
	ScratchPadDAO scratchPadDAO;

	private static final Logger logger = Logger
			.getLogger(TicketScratchPadHandler.class);

	private BlockingQueue<ScratchVo> scratchPad = null;

	private Thread scratchPadThread = null;

	public void init()
	{
		logger.info("ScratchPad processing");
		scratchPad = new LinkedBlockingQueue<ScratchVo>(100000);

		if (scratchPadThread == null)
		{
			scratchPadThread = new Thread(scratchpad, "ScratchPad");
			scratchPadThread.start();
		}
		logger.info("MQ Closure processing enabled");
	}

	Runnable scratchpad = new Runnable()
	{
		@Override
		public void run()
		{
			try
			{
				ExecutorService executor = Executors.newFixedThreadPool(25);
				while (true)
				{
					try
					{
						final ScratchVo scratchVo  = scratchPad.take();
						Runnable worker = new Runnable()
						{
							@Override
							public void run()
							{
								try
								{
									// Start processing from here
									if (scratchVo != null)
									{
										logScratchPad(scratchVo);
									}
								}
								catch (Exception e)
								{
									logger.error("error in scratchPad thread- msg="
											+ e.getMessage());
								}
							}
						};
							executor.execute(worker);
							// Thread.sleep(10);
						} catch (Exception e)
						{
							logger.error("error in scratchPad main thread- msg="
									+ e.getMessage());
						}
				}

				
			} catch (Exception e)
			{
				logger.error("error in scratchPad main loop- msg="
						+ e.getMessage());
			}
			logger.info("scratchPad done closing...");
		}

		private void logScratchPad(ScratchVo scratchVo)
		{
			scratchPadDAO.spAPI(scratchVo);

		}

	};

	public void handleScratchPad(ScratchVo scratchVo)
	{
		if (scratchVo == null)
			return;
		try
		{
			
				scratchPad.put(scratchVo);
			

		} catch (Exception e)
		{
			logger.error("error adding in scratchPad Q " + e.getMessage());
		}
	}

}
