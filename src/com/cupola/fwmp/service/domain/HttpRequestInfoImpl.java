 
package com.cupola.fwmp.service.domain;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class HttpRequestInfoImpl
	implements HttpRequestInfo
{
	private String remoteHost;
	private String remoteAddr;
	private String contextPath;
	private String pathInfo;
	private String requestURI;
	private String requestURL;
	private String requestedSessionId;
	private boolean isRequestedSessionIdValid;
	private boolean isRequestSecure;
	private boolean isRequestedFromMobile;
	private String baseUrl;
	private HttpSession httpSession;
	private Map<String,String> parameters;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#getContextPath()
	 */
	public String getContextPath ()
	{
		return contextPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#setContextPath(java.lang.String)
	 */
	public void setContextPath (String contextPath)
	{
		this.contextPath = contextPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#isRequestedSessionIdValid()
	 */
	public boolean isRequestedSessionIdValid ()
	{
		return isRequestedSessionIdValid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#setRequestedSessionIdValid(boolean)
	 */
	public void setRequestedSessionIdValid (boolean isRequestedSessionIdValid)
	{
		this.isRequestedSessionIdValid = isRequestedSessionIdValid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#isRequestSecure()
	 */
	public boolean isRequestSecure ()
	{
		return isRequestSecure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#setRequestSecure(boolean)
	 */
	public void setRequestSecure (boolean isRequestSecure)
	{
		this.isRequestSecure = isRequestSecure;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#getPathInfo()
	 */
	public String getPathInfo ()
	{
		return pathInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#setPathInfo(java.lang.String)
	 */
	public void setPathInfo (String pathInfo)
	{
		this.pathInfo = pathInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#getRequestedSessionId()
	 */
	public String getRequestedSessionId ()
	{
		return requestedSessionId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#setRequestedSessionId(java.lang.String)
	 */
	public void setRequestedSessionId (String requestedSessionId)
	{
		this.requestedSessionId = requestedSessionId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#getRequestURI()
	 */
	public String getRequestURI ()
	{
		return requestURI;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#setRequestURI(java.lang.String)
	 */
	public void setRequestURI (String requestURI)
	{
		this.requestURI = requestURI;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#getRequestURL()
	 */
	public String getRequestURL ()
	{
		return requestURL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.apprion.service.HttpParameter#setRequestURL(java.lang.String)
	 */
	public void setRequestURL (String requestURL)
	{
		this.requestURL = requestURL;
	}

	public String getRemoteHost ()
	{
		return remoteHost;
	}

	public void setRemoteHost (String remoteHost)
	{
		this.remoteHost = remoteHost;
	}

	public String getRemoteAddr ()
	{
		return remoteAddr;
	}

	public void setRemoteAddr (String remoteAddr)
	{
		this.remoteAddr = remoteAddr;
	}

	public String getBaseUrl ()
	{
		return baseUrl;
	}

	public void setBaseUrl (String baseUrl)
	{
		this.baseUrl = baseUrl;
	}

	public HttpSession getHttpSession ()
	{
		return httpSession;
	}

	public void setHttpSession (HttpSession httpSession)
	{
		this.httpSession = httpSession;
	}

	public boolean isRequestedFromMobile()
	{
		return isRequestedFromMobile;
	}

	public void setRequestedFromMobile(boolean isRequestedFromMobile)
	{
		this.isRequestedFromMobile = isRequestedFromMobile;
	}

	public Map<String, String> getParameters()
	{
		return parameters;
	}

	public void addParameters(String key, String value)
	{
		if(this.parameters == null)
			this.parameters = new HashMap<String, String>();
		
		this.parameters.put(key, value);
	}
	
	
	
}
