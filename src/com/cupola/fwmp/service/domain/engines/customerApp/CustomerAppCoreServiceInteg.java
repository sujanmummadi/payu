package com.cupola.fwmp.service.domain.engines.customerApp;


import java.util.List;

import com.cupola.fwmp.dao.integ.customerAppNotification.CustomerActivationVO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.MessageVO;



public interface CustomerAppCoreServiceInteg {
	
	String customerActivation(CustomerActivationVO customerActivation); 

}
