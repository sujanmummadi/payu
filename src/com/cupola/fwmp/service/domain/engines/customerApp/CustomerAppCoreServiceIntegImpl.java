package com.cupola.fwmp.service.domain.engines.customerApp;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CustomerAppNotification;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.integ.aat.AATDAO;
import com.cupola.fwmp.dao.integ.customerAppNotification.CustomerActivationVO;
import com.cupola.fwmp.dao.integ.customerAppNotification.CustomerAppDAO;
import com.cupola.fwmp.dao.ticket.TicketDetailDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.handler.GCMConsumer;
import com.cupola.fwmp.handler.MessageHandler;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreService;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TabletInfoVo;
import com.cupola.fwmp.vo.TicketActivityLogVo;

public class CustomerAppCoreServiceIntegImpl
		implements CustomerAppCoreServiceInteg
{

	@Autowired
	GCMConsumer gcmConsumer;

	@Autowired
	TicketActivityLogCoreService ticketActivityLoggingService;

	@Autowired
	TicketActivityLogDAO ticketActivityLogDAOImpl;
	@Autowired
	TicketDetailDAO ticketDetailDAO;

	private CustomerAppDAO customerAppDAO;

	@Autowired
	AATDAO aatDAOImpl;

	public CustomerAppDAO getCustomerAppDAO()
	{
		return customerAppDAO;
	}

	public void setCustomerAppDAO(CustomerAppDAO customerAppDAO)
	{
		this.customerAppDAO = customerAppDAO;
	}

	GCMMessage gcmMessage = new GCMMessage();

	MessageHandler messageHandler = new MessageHandler();

	MessageVO messageVO = new MessageVO();

	String customerResponse;

	String correlationId;
	boolean state;

	final static Logger log = Logger
			.getLogger(CustomerAppCoreServiceIntegImpl.class);

	TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();

	public String customerActivation(CustomerActivationVO customerActivation)
	{

		if (customerActivation.getMqId() > 0)
		{

			List<CustomerActivationVO> custActRespList = customerAppDAO
					.customerActivation(customerActivation);

			

			if (custActRespList != null && !custActRespList.isEmpty())
			{
				log.info("custActRespList:: size " + custActRespList.size() + "    "
						+ "custActRespList value ::" + custActRespList);
				
				for (int i = 0; i < custActRespList.size(); i++)
				{

					CustomerActivationVO customerActResponse = new CustomerActivationVO();

					customerActResponse = custActRespList.get(i);

					String registrationId = customerActResponse
							.getRegistrationId();

					gcmMessage.setRegistrationId(registrationId);

					messageVO.setMessage(String
							.valueOf(customerActResponse.getTicketId()));

					messageVO
							.setMessageType(FWMPConstant.MessageType.CUSTOMER_ACTIVATION_SUCCESS);

					ObjectMapper mapper = new ObjectMapper();
					String jsonInString = null;

					try
					{
						jsonInString = mapper.writeValueAsString(messageVO);
					} catch (JsonGenerationException e)
					{
						log.error("Error While generating json :"+e.getMessage());

						e.printStackTrace();
					} catch (JsonMappingException e)
					{
						log.error("Error While mapping json :"+e.getMessage());

						e.printStackTrace();
					} catch (IOException e)
					{
						log.error("Error While executing customerActivation :"+e.getMessage());

						e.printStackTrace();
					}

					gcmMessage.setMessage(jsonInString);

					// Gcm are Calling ...

					messageHandler.addInGCMMessageQueue(gcmMessage);

					// Ticket Activity Log are updating...

					log.debug("ticketAcitivityDetailsVO::::");

					ticketActivityLogVo
							.setTicketId(customerActResponse.getTicketId());

					ticketActivityLogVo.setActivityId(Long
							.valueOf(FWMPConstant.CustomerAppNotification.CUSTOMER_ACTIVITYID));

					correlationId = String
							.valueOf(customerActResponse.getTicketId());

				}

				state = Boolean
						.valueOf(FWMPConstant.CustomerAppNotification.CUSTOMER_SUB_ACTIVITYID_TRUE_VALUE);

				// copper.notifyCopper(correlationId, state);

				customerResponse = FWMPConstant.CustomerAppNotification.CUSTOMER_APP_NOTIFICATION_SUCCESS;

			} else
			{
				customerResponse = FWMPConstant.CustomerAppNotification.CUSTOMER_APP_NOTIFICATION_FAILURE;

			}

		} else
		{

			return null;
		}

		List<TabletInfoVo> tabletInfoVo = aatDAOImpl
				.getAATDetailsforCustomer(customerActivation.getMqId() + "");

		if (!tabletInfoVo.isEmpty()
				&& tabletInfoVo.get(0).getTicketId() != null)
		{
			ticketActivityLogVo.setModifiedOn(new Date());
			ticketActivityLogVo.setAddedOn(new Date());
			ticketActivityLogVo.setTicketId(tabletInfoVo.get(0).getTicketId());
			ticketActivityLogVo.setActivityId(Long
					.valueOf(CustomerAppNotification.CUSTOMER_ACTIVITYID));

			ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);

			ticketActivityLogVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			ticketActivityLogVo.setSubActivityId(Long
					.valueOf((FWMPConstant.CustomerAppNotification.CUSTOMER_SUB_ACTIVITYID)));
			ticketActivityLogVo
					.setValue(FWMPConstant.CustomerAppNotification.CUSTOMER_SUB_ACTIVITYID_TRUE_VALUE);
			ticketActivityLogDAOImpl.addTicketActivityLog(ticketActivityLogVo);
			ticketActivityLogDAOImpl.updateTicketStatus(ticketActivityLogVo);

			int activityUpdate = ticketActivityLogDAOImpl
					.updateWorkOrderActivity(ticketActivityLogVo);

			if (activityUpdate > 0)
			{
				if (tabletInfoVo.get(0).getUserId() != null)
				{
					TicketLog logDetail = new TicketLogImpl(tabletInfoVo.get(0)
							.getTicketId(), TicketUpdateConstant.ACTIVITY_UPDATED, tabletInfoVo
									.get(0).getUserId());
					logDetail
							.setActivityId(ticketActivityLogVo.getActivityId());
					logDetail.setActivityValue(ticketActivityLogVo.getStatus()
							+ "");

					TicketUpdateGateway.logTicket(logDetail);

				}
				StatusVO status = new StatusVO();
				status.setTicketId(tabletInfoVo.get(0).getTicketId());
				status.setStatus(TicketStatus.CUSTOMER_ACCOUNT__ACTIVATE_SUCCESSFULLY);
				status.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
				ticketDetailDAO.updateTicketStatus(status);

			}

		} else
		{
			ticketActivityLogVo.setModifiedOn(new Date());
			ticketActivityLogVo.setAddedOn(new Date());
			ticketActivityLogVo.setTicketId(customerActivation.getTicketId());
			ticketActivityLogVo.setActivityId(Long
					.valueOf(CustomerAppNotification.CUSTOMER_ACTIVITYID));

			ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);

			ticketActivityLogVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			ticketActivityLogVo.setSubActivityId(Long
					.valueOf((FWMPConstant.CustomerAppNotification.CUSTOMER_SUB_ACTIVITYID)));
			ticketActivityLogVo
					.setValue(FWMPConstant.CustomerAppNotification.CUSTOMER_SUB_ACTIVITYID_TRUE_VALUE);
			ticketActivityLogDAOImpl.addTicketActivityLog(ticketActivityLogVo);
			ticketActivityLogDAOImpl.updateTicketStatus(ticketActivityLogVo);

			int activityUpdate = ticketActivityLogDAOImpl
					.updateWorkOrderActivity(ticketActivityLogVo);

			if (activityUpdate > 0)
			{
				StatusVO status = new StatusVO();
				status.setTicketId(customerActivation.getTicketId());
				status.setStatus(TicketStatus.CUSTOMER_ACCOUNT__ACTIVATE_SUCCESSFULLY);
				status.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
				ticketDetailDAO.updateTicketStatus(status);
			}
		}

		return customerResponse;
	}
}
