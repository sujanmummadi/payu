/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.doc.service;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.DocumentSubType;
import com.cupola.fwmp.FWMPConstant.DocumentType;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.mongo.documents.CustomerDocument;
import com.cupola.fwmp.dao.mongo.documents.DocumentDao;
import com.cupola.fwmp.dao.mongo.documents.DocumentFactory;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.DocumentVo;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.sales.ImageInput;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @author aditya
 * 
 */
public class DocumentationCoreEngineImpl implements DocumentationCoreEngine
{

	private Logger log = LogManager
			.getLogger(DocumentationCoreEngineImpl.class);

	private String documentPath;
	static int FONT_SIZE = 12;

	public void setDocumentPath(String documentPath)
	{
		this.documentPath = documentPath;
	}

	@Autowired
	DocumentDao documentDao;
	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	TicketActivityLogCoreService ticketActivityLogCoreService;
	
	@Autowired
	SalesCoreServcie salesCoreServcie;
	
	@Autowired
	DBUtil dbUtil;

	@Override
	public String createFolder(String prospectId)
	{
		File filePath = new File(documentPath + File.separator + prospectId);

		if (!filePath.exists())
		{

			filePath.mkdir();

			log.info("Creating folder for prospect " + prospectId + " at "
					+ documentPath + prospectId);

			return documentPath + File.separator + prospectId;

		} else
		{

			log.info("Folder Exist with folder Name " + prospectId + " at "
					+ documentPath + prospectId);

			return documentPath + File.separator + prospectId;

		}

	}

	@Override
	public boolean uploadImage(ImageInput imageInput, String filePath)
	{

		BufferedImage image = null;
		byte[] imageByte;

		try
		{
			log.info("##################Image input file " + imageInput.getAadhaarTransactionType() + " , " + imageInput.getTicketId());

			List<CustomerDocument> documents = new ArrayList<CustomerDocument>();
			BASE64Decoder decoder = new BASE64Decoder();

			if (imageInput.getImageNameBase64ValueMap() != null
					&& !imageInput.getImageNameBase64ValueMap().isEmpty())
			{
				for (Map.Entry<String, String> entry : imageInput
						.getImageNameBase64ValueMap().entrySet())
				{
					imageByte = decoder.decodeBuffer(entry.getValue());
					ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
					image = ImageIO.read(bis);
					bis.close();

					// write the image to a file
					String docName = imageInput.getPropspectNo() + "_"
							+ entry.getKey() + ".png";
					File outputfile = new File(filePath + File.separator
							+ docName);
					log.info("Uploaded doc " + docName);
					String in = entry.getKey();
					if (in.indexOf("_") < 0)
					{
						continue;
					} else
					{

						String type = in.substring(0, in.indexOf("_"));
						String sub = in.substring(in.indexOf("_") + 1, in
								.lastIndexOf("_"));

						log.debug("Test###################### " + type
								+ " sub type " + sub);

						Integer subType = DocumentSubType.DOCUMENT_SUBTYPE_STRING_VALUE
								.get(sub);
						if (subType == null)
						{
							log.info("Unknown subtype doc " + sub);
							continue;

						}

						log.debug("Test###################### " + type
								+ " sub type " + sub + " subType " + subType);

						CustomerDocument doc = null;

						long userId = AuthUtils.getCurrentUserId();

						if (DocumentType.ID_PROOF_STRING.equals(type))
						{
							doc = DocumentFactory
									.createIDProof(Long.valueOf(imageInput
											.getTicketId()), null, subType, userId);

						} else if (DocumentType.ADDRESS_PROOF_STRING
								.equals(type))
						{
							doc = DocumentFactory
									.createAddressProof(Long.valueOf(imageInput
											.getTicketId()), null, subType, userId);

						} else if (DocumentType.PERMISSION_STRING.equals(type))
						{
							doc = DocumentFactory
									.createPermissionDoc(Long.valueOf(imageInput
											.getTicketId()), null, subType, userId);
						} else if (DocumentType.AADHAR_STRING.equals(type))
						{
							doc = DocumentFactory
									.createAadharDoc(Long.valueOf(imageInput
											.getTicketId()), null, subType, userId);
						} else
						{
							log.info("Unknown type doc " + type);
							continue;
						}

						doc.setDisplayName(entry.getKey());
						doc.setDocumentName(docName);
						doc.setActivityId(imageInput.getActivityId());
						documents.add(doc);
					}

					ImageIO.write(image, "png", outputfile);
				}
			}

			documentDao.storeCustomeDocument(documents);

			if (!imageInput.isPermissionDoc())
			{
				ticketDAO.updateDocpath(Long
						.valueOf(imageInput.getTicketId()), filePath,  imageInput.getActivityId() , imageInput.getUinType() ,imageInput.getUin());

				if (imageInput != null && imageInput.getActivityId() > 0
						&& imageInput.getActivityId() == Activity.POA_DOCUMENT_UPDATE) 
				{
					APIResponse response = getTicketActivityLogVo(Activity.POA_DOCUMENT_UPDATE + "",
							imageInput.getTicketId(), TicketStatus.POA_DOCUMENTS_UPLOADED + "");
					log.info("POA saved for Ticket " + imageInput.getTicketId() + " and response is " + response);

				} else if (imageInput != null && imageInput.getActivityId() > 0
						&& imageInput.getActivityId() == Activity.POI_DOCUMENT_UPDATE) 
				{
					APIResponse response = getTicketActivityLogVo(Activity.POI_DOCUMENT_UPDATE + "",
							imageInput.getTicketId(), TicketStatus.POI_DOCUMENTS_UPLOADED + "");
					log.info("POI saved for Ticket " + imageInput.getTicketId() + " and response is " + response);
				} else if (imageInput != null && imageInput.getActivityId() > 0
						&& imageInput.getActivityId() == Activity.BASIC_INFO_UPDATE) {
					APIResponse response = getTicketActivityLogVo(Activity.BASIC_INFO_UPDATE + "",
							imageInput.getTicketId(), TicketStatus.CUSTOMER_BASIC_INFO_UPDATED + "");
					log.info("BasicInfo saved for Ticket " + imageInput.getTicketId() + " and response is " + response);
				}
				
/*				TicketLog logDetail = new TicketLogImpl(Long.valueOf(imageInput
						.getTicketId()), TicketUpdateConstant.ACTIVITY_UPDATED, AuthUtils
								.getCurrentUserId());
				logDetail.setActivityName("DOCUMENTS_UPLOADED");
				logDetail.setStatusId(TicketStatus.DOCUMENTS_UPLOADED);
				TicketUpdateGateway.logTicket(logDetail);*/

			}

			return true;

		} catch (IOException e)
		{

			log.error("Image upload failed for prospect "
					+ imageInput.getPropspectNo() + " at path :: " + filePath);

			e.printStackTrace();

			return false;
		}

	}

	
	
	public List<DocumentVo> getAllDocumentByProspectNumber(String prospectId)
	{
		List<DocumentVo> listDoc = null;
		File docPath = new File(documentPath + File.separator + prospectId);
		log.info("getAllDocumentByProspectNumber " + docPath);
		if (docPath.exists())
		{
			long createdDate = docPath.lastModified();
			listDoc = new ArrayList<DocumentVo>();
			String[] files = docPath.list();
			List<String> docs = Arrays.asList(files);
			for (String doc : docs)
			{
				listDoc.add(new DocumentVo(createDocDisplayName(doc), doc, GenericUtil
						.convertToUiDateFormat(new Date(createdDate))));
			}

		}
		return listDoc;
	}

	private String createDocDisplayName(String input)
	{
		if (input.contains("_") && input.contains("."))
			return input.substring(input.indexOf("_") + 1, input.indexOf("."));
		else
			return input;

	}

	public File getDocumentByFileName(String prospectId, String filename)
	{

		File docPath = new File(documentPath + File.separator + prospectId
				+ File.separator + filename);
		if (docPath.isFile())
			return docPath;

		return null;
	}

	/**
	 * @param imageInput
	 * @return
	 */
	public APIResponse uploadDoucuments(ImageInput imageInput)
	{

		if (imageInput.getPropspectNo() == null)
		{
			String prospectNumber = ticketDAO.getProspectNumberForTicket(Long
					.parseLong(imageInput.getTicketId()));
			if (prospectNumber == null)

			{
				APIResponse response = ResponseUtil
						.createNullParameterResponse();
				response.setStatusMessage("Prospect number not found");
				return response;
			}
			imageInput.setPropspectNo(prospectNumber);

		}

		if (imageInput.getTicketId() == null
				|| "null".equalsIgnoreCase(imageInput.getTicketId())
				|| imageInput.getTicketId().isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("Ticket id could not be empty or null");

		if (imageInput.getImageNameBase64ValueMap() == null
				|| imageInput.getImageNameBase64ValueMap().isEmpty())
			return ResponseUtil.createNullParameterResponse()
					.setData("Image could not be empty or null");

		String filePath = createFolder(imageInput.getPropspectNo());

		if (uploadImage(imageInput, filePath))
		{
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				salesCoreServcie.updateSalesActivityInCRM(Long.valueOf(imageInput.getTicketId()),  null  , null);
			}
			
			return ResponseUtil.createSuccessResponse()
					.setData("Document Updated");

		} else
		{
			return ResponseUtil.createSuccessResponse()
					.setData("Document Update Failed");
		}
	}

	@Override
	public APIResponse uploadKycImageData(ImageInput imageInput)
	{
		try
		{
			String kycString = imageInput.getKycData();
			KycDetails kycDetail = parseKycDetail(kycString);
			String photoData = kycDetail.getPhoto();
			photoData = photoData.replace("\\", "");
			byte[] imageByte = photoData.getBytes();
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);

			BufferedImage image = decodeToImage(photoData);

			image = process(image, kycDetail, imageInput.getTicketId(),imageInput.getTransactionId());

			bis.close();

			/*
			 * File outputfile = new File("/home/gyaneshvar/skypenew/Screenshot"
			 * + System.currentTimeMillis() + ".png"); if(outputfile.exists());
			 * else outputfile.createNewFile(); ImageIO.write(image, "png",
			 * outputfile);
			 */
			String docType = "";
			
			if(imageInput.getActivityId()==FWMPConstant.Activity.POA_DOCUMENT_UPDATE)
				docType = FWMPConstant.Activity.POA_STATUS;
			
			if(imageInput.getActivityId()==FWMPConstant.Activity.POI_DOCUMENT_UPDATE)
				docType = FWMPConstant.Activity.POI_STATUS;
			
			if(imageInput.getActivityId()==FWMPConstant.Activity.PAYMENT_UPDATE)
				docType = FWMPConstant.Activity.PAYMENT_STATUS;
			
			Map<String, String> images = new HashMap<String, String>();
			images.put(DocumentType.AADHAR_STRING + "_"
					+ DocumentType.AADHAR_STRING + "_"
					+ docType, encodeToString(image));
			imageInput.setImageNameBase64ValueMap(images);
			
			return uploadDoucuments(imageInput);

		} catch (Exception e)
		{
			log.error("Error While executing  uploadKycImageData :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private BufferedImage process(BufferedImage old, KycDetails kycDetail,
			String ticketId, String transactionId)
	{
		try
		{
			int w = old.getWidth();
			int h = old.getHeight();

			w = 600;
			h = 175;

			BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

			Graphics2D g2d = img.createGraphics();
			g2d.setPaint(Color.WHITE);
			g2d.fillRect(0, 0, w, h);

			g2d.drawImage(old, 10, 20, 100, 125, null);

			g2d.setPaint(Color.BLACK);
			int startLabelX = 150;
			int startValueX = 220;
			int startY = 10;
			int verticleGap = 10;
			startY += FONT_SIZE + verticleGap;
			addLabel(g2d, "TID : ", startLabelX, startY);

			addValue(g2d, transactionId, startValueX, startY);

			startY += FONT_SIZE + verticleGap;
			addLabel(g2d, "Name : ", startLabelX, startY);
			addValue(g2d, kycDetail.getName(), startValueX, startY);

			startY += FONT_SIZE + verticleGap;
			addLabel(g2d, "DOB : ", startLabelX, startY);
			addValue(g2d, kycDetail.getDob(), startValueX, startY);

			startY += FONT_SIZE + verticleGap;
			addLabel(g2d, "Mobile: ", startLabelX, startY);
			addValue(g2d, kycDetail.getPhone() == null ? "--"
					: kycDetail.getPhone(), startValueX, startY);

			addLabel(g2d, "E-Mail : ", startLabelX + 180, startY);
			addValue(g2d, kycDetail.getEmail() == null ? "--"
					: kycDetail.getEmail(), startValueX + 180, startY);

			startY += FONT_SIZE + verticleGap;
			addLabel(g2d, "Address : ", startLabelX, startY);
			drawTextUgly(g2d, kycDetail
					.getCompleteAddress(), startValueX, startY);

			g2d.dispose();

			/*ticketDAO.updateAadharPermanentAddress(Long
					.parseLong(ticketId), kycDetail.getCompleteAddress());*/
		
			ticketDAO.updateAadharPermanentAddressAndCustomerFlag(Long.parseLong(ticketId),
					kycDetail.getCompleteAddress(), kycDetail != null ? true : false);
	
			return img;
			
		} catch (Exception e)
		{
			log.error("Error While processing bufferImage :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return old;
	}

	/**
	 * @param img
	 * @param g2d
	 */
	public static void addLabel(Graphics2D g2d, String label, int x, int y)
	{
		g2d.setFont(new Font("Serif", Font.BOLD, FONT_SIZE));
		g2d.drawString(label, x, y);
	}

	public static void addValue(Graphics2D g2d, String value, int x, int y)
	{
		g2d.setFont(new Font("Serif", Font.LAYOUT_LEFT_TO_RIGHT, FONT_SIZE));
		String s = value;// kycDetail.getCompleteAddress();
		g2d.drawString(s, x, y);
	}

	private static void drawTextUgly(Graphics2D g2, String text, int startX,
			int startY)
	{
		g2.setFont(new Font("Serif", Font.LAYOUT_LEFT_TO_RIGHT, FONT_SIZE));
		FontMetrics textMetrics = g2.getFontMetrics();
		int lineHeight = textMetrics.getHeight();
		String textToDraw = text;
		String[] arr = textToDraw.split(" ");
		int nIndex = 0;
		while (nIndex < arr.length)
		{
			String line = arr[nIndex++];
			while ((nIndex < arr.length) && (textMetrics
					.stringWidth(line + " " + arr[nIndex]) < 300))
			{
				line = line + " " + arr[nIndex];
				nIndex++;
			}
			g2.drawString(line, startX, startY);
			startY = startY + lineHeight;
		}
	}

	/**
	 * @param obj
	 * @return
	 * @throws JSONException
	 */
	@Override
	public KycDetails parseKycDetail(String kycString)
	{
		try
		{
			String content = kycString;/*
										 * new Scanner(new File(
										 * "/home/gyaneshvar/Projects/IONOSPHERE/workspacenew_Nov/workspacenew_Nov/FWMP_HEAD/test.json"
										 * )) .useDelimiter("\\Z").next();
										 */
			JSONObject obj = new JSONObject(content.replace("\n", "")
					.replace("\r", ""));
			JSONObject kycDetails = null;
			if (content.contains("KycDetails")) {
				kycDetails = obj.getJSONObject("KycDetails");
			}else {
				kycDetails = obj;
			}
			
			return getKycData(kycDetails);
		} catch (Exception e)
		{
			log.error("Error While executing parseKycDetail :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public KycDetails getKycData(JSONObject kycDetails)
	{
		ObjectMapper mapper = new ObjectMapper();

		KycDetails details = null;
		log.info(kycDetails.toString());
		try
		{
			details = mapper.readValue(kycDetails.toString(), KycDetails.class);
		} catch (Exception e)
		{
			log.error("Error While executinggetKycData :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return details;

	}

	public BufferedImage decodeToImage(String imageString)
	{

		BufferedImage image = null;
		byte[] imageByte;
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			imageByte = decoder.decodeBuffer(imageString);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return image;
	}

	public String encodeToString(BufferedImage image)
	{
		String base64String = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try
		{
			ImageIO.write(image, "png", bos);
			byte[] imageBytes = bos.toByteArray();
			BASE64Encoder encoder = new BASE64Encoder();
			base64String = encoder.encode(imageBytes);
			bos.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return base64String;
	}

	private APIResponse getTicketActivityLogVo(String activityId,
			String ticketId, String activityStatus)
	{

		TicketAcitivityDetailsVO ticketAcitivityDetailsVO = new TicketAcitivityDetailsVO();

		ticketAcitivityDetailsVO.setActivityCompletedTime(new Date());
		ticketAcitivityDetailsVO.setActivityId(activityId + "");
		ticketAcitivityDetailsVO.setActivityStatus(activityStatus);
		ticketAcitivityDetailsVO.setTicketId(ticketId + "");

		return ticketActivityLogCoreService
				.addTicketActivityLog(ticketAcitivityDetailsVO);

	}
	
	private static Map<Long, ImageInput > bannerCache = new HashMap<Long, ImageInput >();
	
	@Override
	public Long readCurrentBannerVersion()
	{
		try
		{
			File filePath = new File(documentPath);

			File bannerDir = new File(filePath.getParent() + File.separator
					+ "Banners" + File.separator
					+ AuthUtils.getCurrentUserCity().getName());

			File version = new File(bannerDir.getAbsolutePath()
					+ File.separator + ".version");
			if (version.exists())
			{
				String ver = FileUtils
						.readLines(new File(bannerDir.getAbsolutePath()
								+ File.separator + ".version")).get(0);

				return Long.valueOf(ver.trim());
			}
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0l;
	}
	
	public	ImageInput getAllBanners()
	{
		if(bannerCache.containsKey(AuthUtils.getCurrentUserCity().getId()))
			return bannerCache.get(AuthUtils.getCurrentUserCity().getId());
		
		File filePath = new File(documentPath);
		File bannerDir = null;
		try
		{
			ImageInput data = null;
			Map<String, String> imageNameBase64ValueMap = null;
			if (filePath.exists())
			{
				bannerDir = new File(filePath.getParent() + File.separator
						+ "Banners" + File.separator
						+ AuthUtils.getCurrentUserCity().getName());

				if (bannerDir.exists())
					;
				else
					{
					bannerDir.mkdirs();
					FileUtils.writeStringToFile(new File(bannerDir.getAbsolutePath()+File.separator+".version"), StrictMicroSecondTimeBasedGuid.newGuid()+"");
					}

				for (File file : bannerDir.listFiles())
				{
					
					if ("png".equalsIgnoreCase(FilenameUtils.getExtension(file
							.getName()))
							|| "jpeg".equalsIgnoreCase(FilenameUtils
									.getExtension(file.getName()))
							|| "jpg".equalsIgnoreCase(FilenameUtils
									.getExtension(file.getName()))
							|| "gif".equalsIgnoreCase(FilenameUtils
									.getExtension(file.getName()))

					)
					{
						if (data == null)
						{
							data = new ImageInput();
							imageNameBase64ValueMap = new HashMap<String, String>();
							data.setImageNameBase64ValueMap(imageNameBase64ValueMap);
						}
						
						imageNameBase64ValueMap
								.put(file.getName(), encodeToString(ImageIO
										.read(file)));

						/*
						 * File outputfile = new
						 * File("/home/gyaneshvar/skypenew/new_banner/temp/"
						 * +file.getName());
						 * 
						 * if("jpg".equalsIgnoreCase(
						 * FilenameUtils.getExtension(file .getName())))
						 * ImageIO.write(decodeToImage(encodeToString(ImageIO
						 * .read(file))), "jpg", outputfile); else
						 * ImageIO.write(decodeToImage(encodeToString(ImageIO
						 * .read(file))), "png", outputfile);
						 */

					}
				}

				bannerCache.put(AuthUtils.getCurrentUserCity().getId(), data);

			}
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 	bannerCache.get(AuthUtils.getCurrentUserCity().getId());

	}
	
	@Override
	public void renameProspectFileAndDirectory(String oldProspectNumber,String newProspectNumber)
	{
		File dir = new File(documentPath + File.separator + oldProspectNumber);
		if(dir.exists())
		{
			dir.renameTo(new File(dir.getAbsolutePath().replace(oldProspectNumber, newProspectNumber)));
			
			File newdir = new File(documentPath + File.separator + newProspectNumber);
			for(File file : newdir.listFiles())
			{
				  file.renameTo(new File(file.getAbsolutePath().replace(oldProspectNumber, newProspectNumber)));
			}
			
			
			
		}
	}
}
