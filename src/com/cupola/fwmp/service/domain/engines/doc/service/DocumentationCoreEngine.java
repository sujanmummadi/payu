/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.doc.service;

import java.io.File;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.vo.sales.ImageInput;

/**
 * @author aditya
 *
 */
public interface DocumentationCoreEngine {
	
	String createFolder(String prospectId);
	
	boolean uploadImage(ImageInput imageInput, String filePath);

	Object getAllDocumentByProspectNumber(String prospectId);

	File getDocumentByFileName(String prospectId, String filename);

	APIResponse uploadDoucuments(ImageInput imageInput);

	APIResponse uploadKycImageData(ImageInput imageInput);

	ImageInput getAllBanners();

	Long readCurrentBannerVersion();

	void renameProspectFileAndDirectory(String oldProspectNumber,
			String newProspectNumber);

	/**@author aditya
	 * KycDetails
	 * @param kycString
	 * @return
	 */
	KycDetails parseKycDetail(String kycString);

}
