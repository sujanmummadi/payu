/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.assignment;

/**
 * @author aditya
 * 
 */
public interface AutomaticTicketAssignment
{

	void assignPropspectToSE(String prospectNo);

	void assignWorkToNE(String workorderNo);

}
