/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "resultinfo")
@XmlType(propOrder = { "type", "city", "branch", "area", "cluster", "fx"})
@JsonPropertyOrder({ "type", "city", "branch", "area", "cluster", "fx" })
@SuppressWarnings("unused")
public class PossibleCxFxResultRS {
	private static final long serialVersionUID = 1L;

	private String type;
	private String city;
	private String branch;
	private String area;

	// private String cluster;
	private String fxdevicename;
	// private String fx;
	private String fxavailableportnos;

	private String cxdevicename;
	// private String cx;
	private String cxavailableportnos;
	private ArrayList<PossibleFxResultRs> fx = new ArrayList<PossibleFxResultRs>();
	private ArrayList<PossibleCxFxClusterResultRs> cluster = new ArrayList<PossibleCxFxClusterResultRs>();
	
	public @XmlElement String gettype() {
		return type;
	}

	public @XmlElement String getcity() {
		return city;
	}

	public @XmlElement String getbranch() {
		return branch;
	}

	public @XmlElement String getarea() {
		return area;
	}

	/*
	 * public @XmlElement String getclustername() { return clustername; }
	 */

	/*
	 * public @XmlElement String getfx() { return fx; }
	 */
	/*
	 * public @XmlElement String getcx() { return cx; }
	 */
	public void settype(String value) {
		type = value;
	}

	public void setbranch(String value) {
		branch = value;
	}

	public void setarea(String value) {
		area = value;
	}

	public void setcity(String value) {
		city = value;
	}


	@XmlElement
	public ArrayList<PossibleFxResultRs> getfx() {
		return fx;
	}

	public void setfx(ArrayList<PossibleFxResultRs> value) {
		this.fx = value;
	}

	/*
	 * public void setfx(String value) { fx = value; }
	 */

	


	@XmlElement
	public ArrayList<PossibleCxFxClusterResultRs> getcluster() {
		return cluster;
	}

	public void setcluster(ArrayList<PossibleCxFxClusterResultRs> value) {
		this.cluster = value;
	}

	/*
	 * public void setcx(String value) { cx = value; }
	 */
	/*
	 * public void setclustername(String value) { clustername = value; }
	 */

	public PossibleCxFxResultRS() {
	}

	public PossibleCxFxResultRS(String type, String city, String branch, String area) {

		this.type = type;
		this.city = city;
		this.branch = branch;
		this.area = area;
	}

	public PossibleCxFxResultRS(String type, String city, String branch, String area,
			ArrayList<PossibleCxFxClusterResultRs> clustername, ArrayList<PossibleFxResultRs> fx) {

		this.type = type;
		this.city = city;
		this.branch = branch;
		this.area = area;
		this.cluster = clustername;
		this.fx = fx;

		

	}

	@Override
	public String toString()
	{
		return "PossibleCxFxResultRS [type=" + type + ", city=" + city
				+ ", branch=" + branch + ", area=" + area + ", fxdevicename="
				+ fxdevicename + ", fxavailableportnos=" + fxavailableportnos
				+ ", cxdevicename=" + cxdevicename + ", cxavailableportnos="
				+ cxavailableportnos + ", fx=" + fx + ", cluster=" + cluster
				+ "]";
	}

	
	
}
