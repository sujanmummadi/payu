/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "ACTGIS")
@XmlType(propOrder = { "feasibilitycheckresponse" })
@JsonPropertyOrder({ "feasibilitycheckresponse" })
@SuppressWarnings("unused")
public class PossibleCxFxGISResponse {
	private static final long serialVersionUID = 1L;

	private ArrayList<PossibleCxFxCheckResponseDT> feasibilitycheckresponse = new ArrayList<PossibleCxFxCheckResponseDT>();

	@XmlElement
	public ArrayList<PossibleCxFxCheckResponseDT> getfeasibilitycheckresponse() {
		return feasibilitycheckresponse;
	}

	public void setfeasibilitycheckresponse(ArrayList<PossibleCxFxCheckResponseDT> value) {
		this.feasibilitycheckresponse = value;
	}

	public PossibleCxFxGISResponse() {
	}

	@Override
	public String toString()
	{
		return "PossibleCxFxGISResponse [feasibilitycheckresponse="
				+ feasibilitycheckresponse + "]";
	}

}
