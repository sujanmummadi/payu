/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "feasibilityCheckresult")
@XmlType(propOrder = { "responseinfo" })
@JsonPropertyOrder({ "responseinfo" })
@SuppressWarnings("unused")
public class PossibleCxFxCheckResultRS {
	private static final long serialVersionUID = 1L;

	private ArrayList<PossibleCxFxResponseInfoRS> responseinfo = new ArrayList<PossibleCxFxResponseInfoRS>();

	@XmlElement
	public ArrayList<PossibleCxFxResponseInfoRS> getresponseinfo() {
		return responseinfo;
	}

	public void setresponseinfo(ArrayList<PossibleCxFxResponseInfoRS> value) {
		this.responseinfo = value;
	}

	public PossibleCxFxCheckResultRS() {
	}

	public PossibleCxFxCheckResultRS(ArrayList<PossibleCxFxResponseInfoRS> requestinfoxml) {
		this.responseinfo = requestinfoxml;

	}

	@Override
	public String toString()
	{
		return "PossibleCxFxCheckResultRS [responseinfo=" + responseinfo + "]";
	}

	
}
