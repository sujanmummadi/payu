/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.GisFailed;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.sales.SalesDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleCxFxCheckResponseDT;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleCxFxCheckResultRS;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleCxFxClusterResultRs;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleCxFxGISResponse;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleCxFxResponseInfoRS;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleCxFxResultRS;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleCxResultRs;
import com.cupola.fwmp.service.domain.engines.gis.multiple.PossibleFxResultRs;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreService;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.util.XMLHelper;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.gis.GISDeviceDetailsVO;
import com.cupola.fwmp.vo.gis.GISFesiabilityInput;
import com.cupola.fwmp.vo.gis.GISResponseVO;
import com.cupola.fwmp.vo.gis.GisCxVO;
import com.cupola.fwmp.vo.gis.GisFxVO;
import com.cupola.fwmp.vo.gis.GisLog;
import com.cupola.fwmp.vo.gis.GisMultipleFeasibilityVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

/**
 * @author aditya
 * 
 */
public class GISServiceIntegrationImpl implements GISServiceIntegration
{

	private static Logger log = Logger
			.getLogger(GISServiceIntegrationImpl.class.getName());

	@Autowired
	AreaDAO areaDao;
	
	@Autowired
	GisDAO gisDAO;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	TicketActivityLogCoreService ticketActivityLogCoreService;

	@Autowired
	WorkOrderCoreService workOrderCoreService;

	@Autowired
	SalesCoreServcie salesCoreServcie;

	@Autowired
	UserDao userDao;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	SalesDAO salesDAO;

	@Autowired
	CustomerDAO customerDAO;

	private static String CUSTOMER_ID_PLACE_HOLDER = "_CUSTOMER_ID_";
	private static String PHONE_NO_PLACE_HOLDER = "_PHONE_NO_";
	private static String LATITUDE_PLACE_HOLDER = "_LATITUDE_";
	private static String LONGITUDE_PLACE_HOLDER = "_LONGITUDE_";
	private static String TRANSACTION_NO_PLACE_HOLDER = "_TRANSACTION_NO_";
	private static String CITY_PLACE_HOLDER = "_CITY_";

	private String actGISFeasibilityUrl;

	private String actGISMultipleFeasibilityUrl;

	public void setActGISFeasibilityUrl(String actGISFeasibilityUrl)
	{
		this.actGISFeasibilityUrl = actGISFeasibilityUrl;
	}

	public void setActGISMultipleFeasibilityUrl(
			String actGISMultipleFeasibilityUrl)
	{
		this.actGISMultipleFeasibilityUrl = actGISMultipleFeasibilityUrl;
	}

	@Override
	public GISResponseVO actGISFesibility(GISFesiabilityInput gisInput)
	{

		return actIntegrationCall(gisInput);
	}

	/**
	 * @param gisInput
	 * @return
	 */
	private GISResponseVO actIntegrationCall(GISFesiabilityInput gisInput)
	{
		String actGISServiceString = "<ACTGIS><feasibilitycheck><feasibilitycheckxml><requestinfo><customerId>_CUSTOMER_ID_</customerId><phoneNo>_PHONE_NO_</phoneNo><latitude>_LATITUDE_</latitude><longitude>_LONGITUDE_</longitude><transactionNo>_TRANSACTION_NO_</transactionNo><city>_CITY_</city></requestinfo></feasibilitycheckxml></feasibilitycheck></ACTGIS>";

		GISResponseVO gisResponseVO = null;

		actGISServiceString = actGISServiceString
				.replace(CUSTOMER_ID_PLACE_HOLDER, gisInput.getCustomerId())
				.replace(PHONE_NO_PLACE_HOLDER, gisInput.getPhoneNo())
				.replace(LATITUDE_PLACE_HOLDER, gisInput.getLatitude())
				.replace(LONGITUDE_PLACE_HOLDER, gisInput.getLongitude())
				.replace(TRANSACTION_NO_PLACE_HOLDER, String
						.valueOf(StrictMicroSecondTimeBasedGuid.newGuid()));

		if (AuthUtils.getCurrentUserCity() != null)
			actGISServiceString = actGISServiceString
					.replace(CITY_PLACE_HOLDER, AuthUtils.getCurrentUserCity()
							.getName().toUpperCase());
		else
		{

			actGISServiceString = actGISServiceString
					.replace(CITY_PLACE_HOLDER, gisInput.getCity()
							.toUpperCase());
		}

		log.info("ACT GIS service with input as " + actGISServiceString);

		GisLog input = getGisInput(gisInput);
		input.setRequestString(actGISServiceString);

		CloseableHttpResponse httpResponse = null;
		CloseableHttpClient httpclient = null;
		try
		{
			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(actGISFeasibilityUrl);
			input.setUrl(actGISFeasibilityUrl);
			StringEntity stringEntity = new StringEntity(actGISServiceString, "UTF-8");
			stringEntity.setContentType("application/xml");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String xmlResponse = EntityUtils.toString(httpResponse.getEntity());

			input.setResponseTime(new Date().toString());
			input.setResponseString(xmlResponse);

			mongoTemplate.insert(input);

			log.info("XML Response " + xmlResponse);

			log.debug("Error Code " + xmlResponse);

			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();

			InputSource inputStream = new InputSource(new StringReader(xmlResponse));

			Document document = builder.parse(inputStream);

			int errorCode = Integer.valueOf(XMLHelper
					.errorCode(document.getDocumentElement()));

			String message = XMLHelper.message(document.getDocumentElement());

			String transactionNo = XMLHelper
					.transactionNo(document.getDocumentElement());

			log.debug("Error code  " + errorCode);
			httpclient.close();
			if (errorCode == 0)
			{

				String type = XMLHelper.getType(document.getDocumentElement());
				
				type = StringUtils.capitalize(type.toLowerCase());

				if (!type.equalsIgnoreCase("NON_FEASIBLE"))
				{
					String city = XMLHelper
							.getCity(document.getDocumentElement());
					String branch = XMLHelper
							.getBranch(document.getDocumentElement());
					String area = XMLHelper
							.getArea(document.getDocumentElement());
					String clusterName = XMLHelper
							.getClusterName(document.getDocumentElement());

					GISDeviceDetailsVO fxDetails = new GISDeviceDetailsVO();

					fxDetails.setMacAddress(XMLHelper
							.getFxMacAddress(document.getDocumentElement()));
					fxDetails.setName(XMLHelper
							.getFxName(document.getDocumentElement()));
					List<String> fxList = new ArrayList<String>();

					fxList.add(XMLHelper
							.getFxPorts(document.getDocumentElement()));

					fxDetails.setAvailablePorts(fxList);

					GISDeviceDetailsVO cxDetails = new GISDeviceDetailsVO();

					cxDetails.setMacAddress(XMLHelper
							.getCxMacAddress(document.getDocumentElement()));
					cxDetails.setName(XMLHelper
							.getCxName(document.getDocumentElement()));
					List<String> cxList = new ArrayList<String>();

					cxList.add(XMLHelper
							.getCxPorts(document.getDocumentElement()));

					cxDetails.setAvailablePorts(cxList);

					// String fxDetail = XMLHelper.getFxDetails(document
					// .getDocumentElement());
					// String cxDetail = XMLHelper.getCxDetails(document
					// .getDocumentElement());

					log.info("Error Code " + errorCode + " type " + type
							+ " city " + city + " Branch " + branch + " area "
							+ area + " cluster Name " + clusterName
							+ " fx Details " + fxDetails + " cxDetails "
							+ cxDetails);

					// GISDeviceDetailsVO fxDetails = deviceParcer(fxDetail);
					//
					// GISDeviceDetailsVO cxDetails = deviceParcer(cxDetail);

					gisResponseVO = createGISResponse(type, city, branch, area, clusterName, fxDetails, cxDetails, message, transactionNo, String
							.valueOf(errorCode));

				} else
				{
					String city = XMLHelper
							.getCity(document.getDocumentElement());
					String branch = XMLHelper
							.getBranch(document.getDocumentElement());
					String area = XMLHelper
							.getArea(document.getDocumentElement());
					String clusterName = XMLHelper
							.getClusterName(document.getDocumentElement());

					gisResponseVO = new GISResponseVO();
					gisResponseVO.setArea(area);
					gisResponseVO.setBranch(branch);
					gisResponseVO.setCity(city);
					gisResponseVO.setClusterName(clusterName);
					gisResponseVO.setType(type);
					gisResponseVO.setMessage(message);
					gisResponseVO.setTransactionNo(transactionNo);
					gisResponseVO.setErrorNo(errorCode + "");
					return gisResponseVO;
				}
			} else
			{
				log.info("Non Feasible Ticket");
			}

		} catch (IOException e)
		{
			log.error("Error while communicating GIS service "
					+ actGISFeasibilityUrl + " " + e.getMessage());
			gisResponseVO = duringGisException();
			e.printStackTrace();
			return gisResponseVO;

		} catch (ParserConfigurationException e)
		{
			log.error("Error while Parsing GIS service " + actGISFeasibilityUrl
					+ " " + e.getMessage());
			gisResponseVO = duringGisException();
			e.printStackTrace();
			return gisResponseVO;

		} catch (IllegalStateException e)
		{
			log.error("Error while Parsing GIS service with IllegalStateExcetion "
					+ actGISFeasibilityUrl + " " + e.getMessage());
			gisResponseVO = duringGisException();
			e.printStackTrace();
			return gisResponseVO;

		} catch (SAXException e)
		{
			log.error("Error while reading GIS service document from XML "
					+ actGISFeasibilityUrl + " " + e.getMessage());
			gisResponseVO = duringGisException();
			e.printStackTrace();
			return gisResponseVO;

		} catch (Exception e)
		{
			log.error("Error while reading GIS service " + actGISFeasibilityUrl
					+ " " + e.getMessage());
			gisResponseVO = duringGisException();
			e.printStackTrace();
			return gisResponseVO;

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				if (httpclient != null)
					httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse for GIS."
						+ e.getMessage());
				e.printStackTrace();
				gisResponseVO = duringGisException();
				return gisResponseVO;
			}
		}

		log.info("Returring gis reponse from single response " + gisResponseVO);

		return gisResponseVO;
	}

	private static GISResponseVO createGISResponse(String type, String city,
			String branch, String area, String clusterName,
			GISDeviceDetailsVO fxDetails, GISDeviceDetailsVO cxDetails,
			String message, String transactionNo, String errorNo)
	{
		GISResponseVO gisResponseVO = new GISResponseVO();
		gisResponseVO.setArea(area);
		gisResponseVO.setBranch(branch);
		gisResponseVO.setCity(city);
		gisResponseVO.setClusterName(clusterName);
		gisResponseVO.setType(type);
		gisResponseVO.setMessage(message);
		gisResponseVO.setTransactionNo(transactionNo);
		gisResponseVO.setErrorNo(errorNo);

		if (fxDetails != null)
		{
			gisResponseVO.setFxIpAddress(fxDetails.getIpAddress());
			gisResponseVO.setFxMacAddress(fxDetails.getIpAddress());
			gisResponseVO.setFxName(fxDetails.getName());
			gisResponseVO.setFxPorts(fxDetails.getAvailablePorts());
		}

		if (cxDetails != null)
		{
			gisResponseVO.setCxIpAddress(cxDetails.getIpAddress());
			gisResponseVO.setCxMacAddress(cxDetails.getIpAddress());
			gisResponseVO.setCxName(cxDetails.getName());
			gisResponseVO.setCxPorts(cxDetails.getAvailablePorts());
		}

		return gisResponseVO;

	}

	@Override
	public APIResponse addGISInfoOfTicket2GisTable(GISPojo gisPojo)
	{
		Long gisId = gisDAO.insertGISInfo(gisPojo);

		log.info("gisPojo details during addGISInfoOfTicket2GisTable "
				+ gisPojo);

		if (gisId != null && gisId > 0)
		{

			if (gisPojo.isWorkOrderGenerated())
			{
				// to check payment and document is approved in case of
				// feasibility ticket

				if (!ticketDAO.isPaymentApproved(gisPojo.getTicketNo()))
				{
					return ResponseUtil.createPaymentNotApproved()
							.setData(ResponseUtil.createPaymentNotApproved()
									.getStatusMessage());
				}
				/*if (!ticketDAO.isDocumentApproved(gisPojo.getTicketNo()))
				{
					return ResponseUtil.createDocumentNotApproved()
							.setData(ResponseUtil.createDocumentNotApproved()
									.getStatusMessage());
				}*/if (!ticketDAO.isPOADocumentApproved(gisPojo.getTicketNo()))
				{
					return ResponseUtil.createPOADocumentNotApproved()
							.setData(ResponseUtil.createPOADocumentNotApproved()
									.getStatusMessage());
				}if (!ticketDAO.isPOIDocumentApproved(gisPojo.getTicketNo()))
				{
					return ResponseUtil.createPOIDocumentNotApproved()
							.setData(ResponseUtil.createPOIDocumentNotApproved()
									.getStatusMessage());
				}

			}

			int updateNeCommentCount = customerDAO.updateNeComment(gisPojo
					.getTicketNo(), gisPojo.getNeComment());

			log.info("Ne comment updated with values gisPojo.getTicketNo() "
					+ gisPojo.getTicketNo() + " gisPojo.getNeComment() "
					+ gisPojo.getNeComment() + " and result "
					+ updateNeCommentCount);

			if (!gisPojo.isFeasibilityTicket()
					|| gisPojo.isFeasibilityRejectedDuringDeployment())
			{
				APIResponse response = updateTicketActivityLogVo(Activity.PHYSICAL_FEASIBILITY
						+ "", gisPojo.getTicketNo() + "", TicketStatus.COMPLETED
								+ "", gisPojo.getSubActivityDetail());

				/*
				 * log.info(
				 * "Ticket logger updated during PHYSICAL_FEASIBILITY Update " +
				 * response); TicketLog logDetail = new TicketLogImpl(gisPojo
				 * .getTicketNo(), TicketUpdateConstant.PHYSICAL_FEASIBILITY,
				 * AuthUtils .getCurrentUserId());
				 * logDetail.setActivityName("PHYSICAL_FEASIBILITY");
				 * logDetail.setStatusId(TicketStatus.PHYSICAL_FEASIBILITY);
				 * TicketUpdateGateway.logTicket(logDetail);
				 */
			}

			if (!gisPojo.isFeasibilityRejectedDuringDeployment())
			{
				log.info("Feasibility Rejected During Deployment in if"
						+ gisPojo.isFeasibilityRejectedDuringDeployment()
						+ " ticket id " + gisPojo.getTicketNo());

				if (WorkStageName.FIBER
						.equalsIgnoreCase(gisPojo.getConnectionType()))
				{
					ticketDAO.updateTicketSubCategory(gisPojo
							.getTicketNo(), TicketSubCategory.FIBER, TicketSubCategory.FIBER_ID);

				} else if (WorkStageName.COPPER
						.equalsIgnoreCase(gisPojo.getConnectionType()))
				{
					ticketDAO.updateTicketSubCategory(gisPojo
							.getTicketNo(), TicketSubCategory.COPPER, TicketSubCategory.COPPER_ID);
				}
			} else
			{
				log.info("Feasibility Rejected During Deployment in else "
						+ gisPojo.isFeasibilityRejectedDuringDeployment()
						+ " ticket id " + gisPojo.getTicketNo());
			}

			if (gisPojo.isFiberToCopper())
			{
				log.info("Converting work stage from fiber to copper for ticket id "
						+ gisPojo.getTicketNo());

				return workOrderCoreService.updateFiberToCopper(gisPojo
						.getTicketNo(), TicketSubCategory.COPPER_ID);

			} else if (gisPojo.isFeasibilityTicket())
			{
				// passing ticket to sales, this is called during feasibility
				// work, with work order or without workorder

				log.info("Updating feasibility for ticket "
						+ gisPojo.getTicketNo());

				UpdateProspectVO updateProspectVO = new UpdateProspectVO();

				if (gisPojo.isPermissionFeasibilityStatus())
					updateProspectVO.setPermissionFeasibilityStatus("true");
				else
					updateProspectVO.setPermissionFeasibilityStatus("false");

				if (gisPojo.isFeasibilityRejectedDuringDeployment())
					updateProspectVO
							.setFeasibilityRejectedDuringDeployment(true);
				else
					updateProspectVO
							.setFeasibilityRejectedDuringDeployment(false);

				updateProspectVO.setTicketId(gisPojo.getTicketNo().toString());

				updateProspectVO.setConnectionType(gisPojo.getConnectionType());

				updateProspectVO.setFxName(gisPojo.getFxName());
				updateProspectVO.setFxMacAddress(gisPojo.getFxMacAddress());
				updateProspectVO.setFxIpAddress(gisPojo.getFxIpAddress());
				if (gisPojo.getFxPorts() != null)
					updateProspectVO.setFxPorts(new ArrayList<String>(Arrays.asList(gisPojo.getFxPorts().split(","))));
				
				updateProspectVO.setCxName(gisPojo.getCxName());
				updateProspectVO.setCxMacAddress(gisPojo.getCxMacAddress());
				updateProspectVO.setCxIpAddress(gisPojo.getCxIpAddress());
				
				if (gisPojo.getCxPorts() != null)
					updateProspectVO.setCxPorts(new ArrayList<String>(Arrays.asList(gisPojo.getCxPorts().split(","))));
				
				log.info("Called Update feasibility for ticket "
						+ gisPojo.getTicketNo() + " and details are "
						+ updateProspectVO);

				int result = salesDAO.updatePermission(updateProspectVO);

				if (result == 1)
				{
					if (!gisPojo.isFeasibilityRejectedDuringDeployment())
					{
						workOrderCoreService.updateFiberToCopper(gisPojo
								.getTicketNo(), TicketSubCategory.FIBER_ID);
					}
					
					UserVo userVo=null;
					
					if(gisPojo!=null && gisPojo.getBranchName()!=null && gisPojo.getBranchName().equalsIgnoreCase(FWMPConstant.REALTY))
					{
						//For realty flow
						//get area , branch and city ids from ticket id
						Map<String,Long> areaInfo=areaDao.getAreaInfoIdFromTicketId(gisPojo.getTicketNo());
						
						if(areaInfo!=null)
						{
							//get default user associated with area
							userVo=userDao.getDefaultSalesUserForArea(areaInfo.get("area"), areaInfo.get("branch"), areaInfo.get("city"),
									UserRole.ROLE_EX);
						}					
						}
					else
					{
					userVo = userDao.getUserById(ticketDAO
							.ticketCurrentUser(Long
									.valueOf(gisPojo.getTicketNo()))
							.getAssignedBy());
					}
					List<TypeAheadVo> typeAheadVos = new ArrayList<>();

					if (userVo != null)
					{
						ReassignTicketVo reassignTicketVo = new ReassignTicketVo();

						reassignTicketVo.setDonnerUserId(AuthUtils
								.getCurrentUserLoginId());
						reassignTicketVo.setReason("Reassigned Ticket");
						reassignTicketVo.setRecipientUserId(String
								.valueOf(userVo.getId()));
						reassignTicketVo.setRemark("");
						reassignTicketVo.setWorkOrderGenerated(gisPojo
								.isWorkOrderGenerated());

						List<Long> ticketIds = new ArrayList<Long>();
						ticketIds.add(Long.valueOf(gisPojo.getTicketNo()));

						reassignTicketVo.setTicketIds(ticketIds);
						salesCoreServcie.reassign(reassignTicketVo);

						TypeAheadVo typeAheadVo = new TypeAheadVo(userVo
								.getId(), GenericUtil.completeUserName(userVo));

						typeAheadVos.add(typeAheadVo);

						return ResponseUtil.createSuccessResponse()
								.setData(typeAheadVos);

					} else
						return ResponseUtil.createSuccessResponse()
								.setData(typeAheadVos);
				} else
					return ResponseUtil.createSuccessResponse()
							.setData("Feasibility reassignemnt to sales Failed");
			} else
				return ResponseUtil.createSuccessResponse()
						.setData("Deployment Feasibility updated");
		} else
			return ResponseUtil.createFailureResponse()
					.setData("Feasibility update Failed");
	}

	@Override
	public APIResponse getGisInfo4TicketNo(long ticketId)
	{
		return ResponseUtil.createSaveSuccessResponse()
				.setData(gisDAO.getGisInfoByTicketId(ticketId));
	}

	@Override
	public GisMultipleFeasibilityVO multipleGISFesibility(
			GISFesiabilityInput gisInput)
	{
		String actGISServiceString = "<ACTGIS><feasibilitycheck><feasibilitycheckxml><requestinfo><customerId>_CUSTOMER_ID_</customerId><phoneNo>_PHONE_NO_</phoneNo><latitude>_LATITUDE_</latitude><longitude>_LONGITUDE_</longitude><transactionNo>_TRANSACTION_NO_</transactionNo><city>_CITY_</city></requestinfo></feasibilitycheckxml></feasibilitycheck></ACTGIS>";

		if (gisInput.getPhoneNo() != null )
			actGISServiceString = actGISServiceString
				.replace(CUSTOMER_ID_PLACE_HOLDER, gisInput.getCustomerId())
				.replace(PHONE_NO_PLACE_HOLDER, gisInput.getPhoneNo())
				.replace(LATITUDE_PLACE_HOLDER, gisInput.getLatitude())
				.replace(LONGITUDE_PLACE_HOLDER, gisInput.getLongitude())
				.replace(TRANSACTION_NO_PLACE_HOLDER, String
						.valueOf(StrictMicroSecondTimeBasedGuid.newGuid()));
		else
			actGISServiceString = actGISServiceString
			.replace(CUSTOMER_ID_PLACE_HOLDER, gisInput.getCustomerId())
			.replace(LATITUDE_PLACE_HOLDER, gisInput.getLatitude())
			.replace(LONGITUDE_PLACE_HOLDER, gisInput.getLongitude())
			.replace(TRANSACTION_NO_PLACE_HOLDER, String
					.valueOf(StrictMicroSecondTimeBasedGuid.newGuid()));

		GisMultipleFeasibilityVO feasibilityVO = null;

		if (AuthUtils.getCurrentUserCity() != null)
			actGISServiceString = actGISServiceString
					.replace(CITY_PLACE_HOLDER, AuthUtils.getCurrentUserCity()
							.getName().toUpperCase());
		else
		{

			actGISServiceString = actGISServiceString
					.replace(CITY_PLACE_HOLDER, gisInput.getCity()
							.toUpperCase());
		}

		log.info("ACT GIS service with input as for multiple "
				+ actGISServiceString);

		CloseableHttpResponse httpResponse = null;

		GisLog input = getGisInput(gisInput);
		input.setRequestString(actGISServiceString);
		CloseableHttpClient httpclient = null;
		try
		{
			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(actGISMultipleFeasibilityUrl);
			input.setUrl(actGISMultipleFeasibilityUrl);
			StringEntity stringEntity = new StringEntity(actGISServiceString, "UTF-8");
			stringEntity.setContentType("application/xml");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);
			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			input.setResponseTime(new Date().toString());
			input.setResponseString(stringResponse);

			mongoTemplate.insert(input);

			log.info("XML Response " + stringResponse);

			ObjectMapper mapper = new ObjectMapper();

			if (stringResponse != null)
			{
				PossibleCxFxGISResponse gisMultipleFeasibilityVO = mapper
						.readValue(stringResponse, PossibleCxFxGISResponse.class);

				log.info("#################PossibleCxFxGISResponse  "
						+ gisMultipleFeasibilityVO);

				feasibilityVO = convert2AndroidFormat(gisMultipleFeasibilityVO);
			}
			httpclient.close();
		} catch (IOException e)
		{
			log.error("Error while communicating multiple GIS service "
					+ actGISMultipleFeasibilityUrl + " " + e.getMessage());
			feasibilityVO = duringMultipleGisException();
			e.printStackTrace();
			return feasibilityVO;

		} catch (IllegalStateException e)
		{
			log.error("Error while Parsing multiple GIS service with IllegalStateExcetion "
					+ actGISMultipleFeasibilityUrl + " " + e.getMessage());
			feasibilityVO = duringMultipleGisException();
			e.printStackTrace();
			return feasibilityVO;

		} catch (Exception e)
		{
			log.error("Error while Parsing multiple GIS service with IllegalStateExcetion "
					+ actGISMultipleFeasibilityUrl + " " + e.getMessage());
			feasibilityVO = duringMultipleGisException();
			e.printStackTrace();
			return feasibilityVO;

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				
				if (httpclient != null)
					httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse for multiple GIS."
						+ e.getMessage());
				feasibilityVO = duringMultipleGisException();
				e.printStackTrace();
				return feasibilityVO;
			}
		}

		return feasibilityVO;
	}

	private GisMultipleFeasibilityVO convert2AndroidFormat(
			PossibleCxFxGISResponse gisMultipleFeasibilityVO)
	{
		GisMultipleFeasibilityVO feasibilityVO = new GisMultipleFeasibilityVO();

		Map<String, GisCxVO> cxNameCxDetailsMap = new LinkedHashMap<String, GisCxVO>();
		Map<String, GisFxVO> fxNameFxDetailsMap = new LinkedHashMap<String, GisFxVO>();
		Map<String, List<String>> fxNameListCxMap = new LinkedHashMap<String, List<String>>();

		if (gisMultipleFeasibilityVO != null)
		{

			List<PossibleCxFxCheckResponseDT> feasibilityCheckResponse = gisMultipleFeasibilityVO
					.getfeasibilitycheckresponse();

			if (feasibilityCheckResponse != null)
			{
				for (Iterator<PossibleCxFxCheckResponseDT> feasibilityCheckResponseIterator = feasibilityCheckResponse
						.iterator(); feasibilityCheckResponseIterator
								.hasNext();)
				{
					PossibleCxFxCheckResponseDT possibleCxFxCheckResponseDT = (PossibleCxFxCheckResponseDT) feasibilityCheckResponseIterator
							.next();
					if (possibleCxFxCheckResponseDT != null)
					{
						List<PossibleCxFxCheckResultRS> feasibilityCheckResult = possibleCxFxCheckResponseDT
								.getfeasibilitycheckresult();
						if (feasibilityCheckResult != null)
						{
							for (Iterator<PossibleCxFxCheckResultRS> feasibilityCheckResultIterator = feasibilityCheckResult
									.iterator(); feasibilityCheckResultIterator
											.hasNext();)
							{
								PossibleCxFxCheckResultRS possibleCxFxCheckResultRS = (PossibleCxFxCheckResultRS) feasibilityCheckResultIterator
										.next();

								if (possibleCxFxCheckResultRS != null)
								{
									List<PossibleCxFxResponseInfoRS> responseinfo = possibleCxFxCheckResultRS
											.getresponseinfo();
									if (responseinfo != null)
									{
										for (Iterator<PossibleCxFxResponseInfoRS> responseinfoIterator = responseinfo
												.iterator(); responseinfoIterator
														.hasNext();)
										{
											PossibleCxFxResponseInfoRS possibleCxFxResponseInfoRS = (PossibleCxFxResponseInfoRS) responseinfoIterator
													.next();
											if (possibleCxFxResponseInfoRS != null)
											{
												String transactionNo = possibleCxFxResponseInfoRS
														.gettransactionNo();

												String errorNo = possibleCxFxResponseInfoRS
														.geterrorNo();

												List<PossibleCxFxResultRS> result = possibleCxFxResponseInfoRS
														.getresult();
												feasibilityVO
														.setErrorNo(errorNo);
												feasibilityVO
														.setMessage(possibleCxFxResponseInfoRS
																.getmessage());
												feasibilityVO
														.setTransactionNo(transactionNo);

												log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@transactionNo "
														+ transactionNo
														+ " errorNo " + errorNo
														+ " result " + result);

												if (errorNo != null && Integer
														.valueOf(errorNo) == 0)
												{
													if (result != null)
													{

														for (Iterator<PossibleCxFxResultRS> resultIterator = result
																.iterator(); resultIterator
																		.hasNext();)
														{
															PossibleCxFxResultRS possibleCxFxResultRS = (PossibleCxFxResultRS) resultIterator
																	.next();
															if (possibleCxFxResultRS != null)
															{
																String type = possibleCxFxResultRS
																		.gettype();
																type = StringUtils.capitalize(type.toLowerCase());
																
																String city = possibleCxFxResultRS
																		.getcity();
																String branch = possibleCxFxResultRS
																		.getbranch();
																String area = possibleCxFxResultRS
																		.getarea();

																List<PossibleFxResultRs> fx = possibleCxFxResultRS
																		.getfx();

																log.debug("^^^^^^^^^^^^^^^^^ type "
																		+ type
																		+ " city "
																		+ city
																		+ " branch "
																		+ branch
																		+ " area "
																		+ area
																		+ " fx "
																		+ fx);

																feasibilityVO
																		.setType(type);
																feasibilityVO
																		.setBranch(branch);
																feasibilityVO
																		.setArea(area);
																feasibilityVO
																		.setCity(city);

																if (fx != null)
																{

																	for (Iterator<PossibleFxResultRs> fxIterator = fx
																			.iterator(); fxIterator
																					.hasNext();)
																	{
																		PossibleFxResultRs possibleFxResultRs = (PossibleFxResultRs) fxIterator
																				.next();

																		if (possibleFxResultRs != null)
																		{
																			List<PossibleCxResultRs> cx = possibleFxResultRs
																					.getcx();

																			String fxMacId = possibleFxResultRs
																					.getmacId();
																			String fxName = possibleFxResultRs
																					.getname();
																			String fxPorts = possibleFxResultRs
																					.getports();
																			GisFxVO gisFxVO = new GisFxVO(fxName, null, fxMacId, fxPorts);
																			fxNameFxDetailsMap
																					.put(fxName, gisFxVO);
																			List<String> cxList = new ArrayList<String>();

																			log.debug("%%%%%%%%%%%%%% fxMacId "
																					+ fxMacId
																					+ " fxName "
																					+ fxName
																					+ " fxPorts "
																					+ fxPorts
																					+ " cx "
																					+ cx);
																			if (cx != null)
																			{

																				for (Iterator<PossibleCxResultRs> cxIterator = cx
																						.iterator(); cxIterator
																								.hasNext();)
																				{
																					PossibleCxResultRs possibleCxResultRs = (PossibleCxResultRs) cxIterator
																							.next();
																					if (possibleCxResultRs != null)
																					{
																						String cxMacId = possibleCxResultRs
																								.getmacId();
																						String cxName = possibleCxResultRs
																								.getname();
																						String cxPorts = possibleCxResultRs
																								.getports();
																						log.debug("!!!!!!!!!!!!!!!cxMacId "
																								+ cxMacId
																								+ " cxName "
																								+ cxName
																								+ " cxPorts "
																								+ cxPorts);

																						GisCxVO gisCxVO = new GisCxVO(cxName, null, cxMacId, cxPorts);

																						cxNameCxDetailsMap
																								.put(cxName, gisCxVO);

																						cxList.add(cxName);

																					}
																				}
																			}

																			fxNameListCxMap
																					.put(fxName, cxList);
																		}

																	}
																} else
																{
																	log.info("Fx details not available for multiple GIS");
																}
																List<PossibleCxFxClusterResultRs> cluster = possibleCxFxResultRS
																		.getcluster();

																for (Iterator<PossibleCxFxClusterResultRs> clusterIterator = cluster
																		.iterator(); clusterIterator
																				.hasNext();)
																{
																	PossibleCxFxClusterResultRs possibleCxFxClusterResultRs = (PossibleCxFxClusterResultRs) clusterIterator
																			.next();
																	feasibilityVO
																			.setClusterName(possibleCxFxClusterResultRs
																					.getname());

																}
															}
														}

													}
												} else
												{
													log.info("Error while getting Multiple gis response");
												}
											}
										}

									}
								}
							}

						}
					}
				}
			}

		}

		feasibilityVO.setCxNameCxDetailsMap(cxNameCxDetailsMap);
		feasibilityVO.setFxNameFxDetailsMap(fxNameFxDetailsMap);
		feasibilityVO.setFxNameListCxMap(fxNameListCxMap);

		log.info("Multiple FeasibilityVO " + feasibilityVO);

		return feasibilityVO;
	}

	private APIResponse updateTicketActivityLogVo(String activityId,
			String ticketId, String activityStatus,
			Map<String, String> subActivityDetail)
	{

		TicketAcitivityDetailsVO ticketAcitivityDetailsVO = new TicketAcitivityDetailsVO();

		ticketAcitivityDetailsVO.setActivityCompletedTime(new Date());
		ticketAcitivityDetailsVO.setActivityId(activityId + "");
		ticketAcitivityDetailsVO.setActivityStatus(activityStatus);
		ticketAcitivityDetailsVO.setTicketId(ticketId + "");
		ticketAcitivityDetailsVO.setSubActivityDetail(subActivityDetail);
		return ticketActivityLogCoreService
				.addTicketActivityLog(ticketAcitivityDetailsVO);

	}

	private GisMultipleFeasibilityVO duringMultipleGisException()
	{
		GisMultipleFeasibilityVO feasibilityVO = new GisMultipleFeasibilityVO();

		Map<String, GisCxVO> cxNameCxDetailsMap = new LinkedHashMap<String, GisCxVO>();
		Map<String, GisFxVO> fxNameFxDetailsMap = new LinkedHashMap<String, GisFxVO>();
		Map<String, List<String>> fxNameListCxMap = new LinkedHashMap<String, List<String>>();

		feasibilityVO.setType(GisFailed.GIS_RESPONSE_FAILED_NON_FEASIBLE);
		feasibilityVO.setErrorNo(GisFailed.GIS_RESPONSE_FAILED_ERROE);
		feasibilityVO.setMessage(GisFailed.GIS_RESPONSE_FAILED_MESSAGE);
		feasibilityVO
				.setTransactionNo(GisFailed.GIS_RESPONSE_FAILED_TRANSACTION_NO);

		feasibilityVO.setCxNameCxDetailsMap(cxNameCxDetailsMap);
		feasibilityVO.setFxNameFxDetailsMap(fxNameFxDetailsMap);
		feasibilityVO.setFxNameListCxMap(fxNameListCxMap);

		return feasibilityVO;
	}

	private static GISResponseVO duringGisException()
	{
		GISResponseVO gisResponseVO = new GISResponseVO();
		gisResponseVO.setArea("");
		gisResponseVO.setBranch("");
		gisResponseVO.setCity("");
		gisResponseVO.setClusterName("");

		gisResponseVO.setType(GisFailed.GIS_RESPONSE_FAILED_NON_FEASIBLE);
		gisResponseVO.setErrorNo(GisFailed.GIS_RESPONSE_FAILED_ERROE);
		gisResponseVO.setMessage(GisFailed.GIS_RESPONSE_FAILED_MESSAGE);
		gisResponseVO
				.setTransactionNo(GisFailed.GIS_RESPONSE_FAILED_TRANSACTION_NO);

		GISDeviceDetailsVO fxDetails = new GISDeviceDetailsVO();
		GISDeviceDetailsVO cxDetails = new GISDeviceDetailsVO();

		if (fxDetails != null)
		{
			gisResponseVO.setFxIpAddress(fxDetails.getIpAddress());
			gisResponseVO.setFxMacAddress(fxDetails.getIpAddress());
			gisResponseVO.setFxName(fxDetails.getName());
			gisResponseVO.setFxPorts(fxDetails.getAvailablePorts());
		}

		if (cxDetails != null)
		{
			gisResponseVO.setCxIpAddress(cxDetails.getIpAddress());
			gisResponseVO.setCxMacAddress(cxDetails.getIpAddress());
			gisResponseVO.setCxName(cxDetails.getName());
			gisResponseVO.setCxPorts(cxDetails.getAvailablePorts());
		}

		return gisResponseVO;

	}

	private GisLog getGisInput(GISFesiabilityInput feasibilityInput)
	{
		GisLog gisInput = new GisLog();

		BeanUtils.copyProperties(feasibilityInput, gisInput);
		gisInput.setRequestTime(new Date().toString());
		gisInput.setAddedBy(AuthUtils.getCurrentUserId());
		gisInput.setGisLogId(StrictMicroSecondTimeBasedGuid.newGuid());
		return gisInput;

	}

}
