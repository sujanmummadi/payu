/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "feasibilitycheckresponse")
@XmlType(propOrder = { "feasibilitycheckresult" })
@JsonPropertyOrder({ "feasibilitycheckresult" })
public class PossibleCxFxCheckResponseDT {
	private ArrayList<PossibleCxFxCheckResultRS> feasibilitycheckresult = new ArrayList<PossibleCxFxCheckResultRS>();

	@XmlElement
	public ArrayList<PossibleCxFxCheckResultRS> getfeasibilitycheckresult() {
		return feasibilitycheckresult;
	}

	public void setfeasibilitycheckresult(ArrayList<PossibleCxFxCheckResultRS> value) {
		this.feasibilitycheckresult = value;
	}

	public PossibleCxFxCheckResponseDT() {}

	public PossibleCxFxCheckResponseDT(ArrayList<PossibleCxFxCheckResultRS> feasibiltyCheckXMLs) {
		this.feasibilitycheckresult = feasibiltyCheckXMLs;
		
	}

	@Override
	public String toString()
	{
		return "PossibleCxFxCheckResponseDT [feasibilitycheckresult="
				+ feasibilitycheckresult + "]";
	}

}
