/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "requestinfo")
@XmlType(propOrder = { "transactionNo", "errorNo", "message", "result" })
@JsonPropertyOrder({ "transactionNo", "errorNo", "message", "result" })
@SuppressWarnings("unused")

public class PossibleCxFxResponseInfoRS {

	private static final long serialVersionUID = 1L;

	private String transactionNo;
	private String errorNo;
	private String message;
	private ArrayList<PossibleCxFxResultRS> result = new ArrayList<PossibleCxFxResultRS>();

	public @XmlElement String gettransactionNo() {
		return transactionNo;
	}

	public @XmlElement String geterrorNo() {
		return errorNo;
	}

	public @XmlElement String getmessage() {
		return message;
	}

	public void settransactionNo(String value) {
		transactionNo = value;
	}

	public void seterrorNo(String value) {
		errorNo = value;
	}

	public void setmessage(String value) {
		message = value;
	}

	@XmlElement
	public ArrayList<PossibleCxFxResultRS> getresult() {
		return result;
	}

	public void setresult(ArrayList<PossibleCxFxResultRS> value) {
		this.result = value;
	}

	public PossibleCxFxResponseInfoRS() {
	}

	public PossibleCxFxResponseInfoRS(String transactionNo, String errorNo, String message, ArrayList<PossibleCxFxResultRS> resultrs) {
		this.transactionNo = transactionNo;
		this.errorNo = errorNo;
		this.message = message;

		this.result = resultrs;
	}

	@Override
	public String toString()
	{
		return "PossibleCxFxResponseInfoRS [transactionNo=" + transactionNo
				+ ", errorNo=" + errorNo + ", message=" + message + ", result="
				+ result + "]";
	}

}
