/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "cxinfo")
@XmlType(propOrder = { "macId", "name", "ports" })
@JsonPropertyOrder({ "macId", "name", "ports" })
@SuppressWarnings("unused")
public class PossibleCxResultRs {
	private static final long serialVersionUID = 1L;

	private String macId;
	private String name;
	private String ports;

	public @XmlElement String getmacId() {
		return macId;
	}

	public @XmlElement String getname() {
		return name;
	}

	public @XmlElement String getports() {
		return ports;
	}
	
	public void setmacId(String value) {
		macId = value;
	}

	public void setname(String value) {
		name = value;
	}
	
	public void setports(String value) {
		ports = value;
	}


	public PossibleCxResultRs() {
	}

	public PossibleCxResultRs(String macId, String name, String ports) {

		this.macId = macId;
		this.name = name;
		this.ports = ports;

	}

	@Override
	public String toString()
	{
		return "PossibleCxResultRs [macId=" + macId + ", name=" + name
				+ ", ports=" + ports + "]";
	}

}
