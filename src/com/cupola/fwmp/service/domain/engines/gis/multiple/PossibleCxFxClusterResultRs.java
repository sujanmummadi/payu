/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "clusterinfo")
@XmlType(propOrder = { "name", "standalone" })
@JsonPropertyOrder({ "name", "standalone" })
@SuppressWarnings("unused")
public class PossibleCxFxClusterResultRs {
	private static final long serialVersionUID = 1L;

	private String name;
	private String standalone;

	public @XmlElement String getname() {
		return name;
	}

	public @XmlElement String getstandalone() {
		return standalone;
	}

	public void setname(String value) {
		name = value;
	}

	public void setstandalone(String value) {
		standalone = value;
	}

	public PossibleCxFxClusterResultRs() {
	}

	public PossibleCxFxClusterResultRs(String name, String standalone) {

		this.name = name;
		this.standalone = standalone;

	}

	@Override
	public String toString()
	{
		return "PossibleCxFxClusterResultRs [name=" + name + ", standalone="
				+ standalone + "]";
	}

}
