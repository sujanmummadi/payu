/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis.multiple;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author dilip.duraiswamy
 *
 */
@XmlRootElement(name = "fxinfo")
@XmlType(propOrder = { "macId", "name", "ports", "cx" })
@JsonPropertyOrder({ "macId", "name", "ports", "cx" })
@SuppressWarnings("unused")
public class PossibleFxResultRs {
	private static final long serialVersionUID = 1L;

	private String macId;
	private String name;
	private String ports;

	public @XmlElement String getmacId() {
		return macId;
	}

	public @XmlElement String getname() {
		return name;
	}

	public @XmlElement String getports() {
		return ports;
	}

	public void setmacId(String value) {
		macId = value;
	}

	public void setname(String value) {
		name = value;
	}

	public void setports(String value) {
		ports = value;
	}

	private ArrayList<PossibleCxResultRs> cx = new ArrayList<PossibleCxResultRs>();

	@XmlElement
	public ArrayList<PossibleCxResultRs> getcx() {
		return cx;
	}

	public void setcx(ArrayList<PossibleCxResultRs> value) {
		this.cx = value;
	}

	public PossibleFxResultRs() {
	}

	public PossibleFxResultRs(String macId, String name, String ports, ArrayList<PossibleCxResultRs> cx) {

		this.macId = macId;
		this.name = name;
		this.ports = ports;
		this.cx = cx;
	}

	@Override
	public String toString()
	{
		return "PossibleFxResultRs [macId=" + macId + ", name=" + name
				+ ", ports=" + ports + ", cx=" + cx + "]";
	}


}
