/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.gis;

import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.gis.GISFesiabilityInput;
import com.cupola.fwmp.vo.gis.GISResponseVO;
import com.cupola.fwmp.vo.gis.GisMultipleFeasibilityVO;

/**
 * @author aditya
 *
 */
public interface GISServiceIntegration
{
	GISResponseVO actGISFesibility(GISFesiabilityInput gisInput);
	
	APIResponse addGISInfoOfTicket2GisTable(GISPojo gisPojo);
	
	APIResponse getGisInfo4TicketNo(long ticketId);
	
	GisMultipleFeasibilityVO multipleGISFesibility(GISFesiabilityInput gisInput);
}
