package com.cupola.fwmp.service.domain.engines.etr;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;

public class EtrCalculatorCoreServiceImpl implements EtrCalculatorCoreService {
	private EtrCalculator etrCalculatorDAOImpl;

	public void setEtrCalculatorDAOImpl(EtrCalculator etrCalculatorDAOImpl) {
		this.etrCalculatorDAOImpl = etrCalculatorDAOImpl;
	}

	@Override
	public APIResponse calculateActivityPercentage(
			EtrCalculatorPojo etrCalculatorPojo) {
		// TODO Auto-generated method stub
		if (etrCalculatorPojo.getTicketId() > 0)
			return ResponseUtil.createSuccessResponse().setData(
					etrCalculatorDAOImpl
							.calculateActivityPercentage(etrCalculatorPojo.getTicketId(),null));
		else
			return ResponseUtil.createNullParameterResponse();

	}

}
