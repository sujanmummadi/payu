/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.etr;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.fr.FrDeploymentSettingDaoImpl;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.util.ETRCalculatorUtil;
import com.cupola.fwmp.vo.DeploymentSettingVo;
import com.cupola.fwmp.vo.WorkStageVo;

/**
 * @author aditya
 * 
 */
@Transactional
public class EtrCalculatorImpl implements EtrCalculator
{
	private Logger log = Logger.getLogger(EtrCalculatorImpl.class.getName());

	@Autowired
	HibernateTemplate hibernetTemplate;

	@Autowired
	TicketActivityLogDAO ticketActivityLoggingDAO;

	@Autowired
	TicketDAO ticketDAO;
	
	@Autowired
	ETRCalculatorUtil etrCalculatorUtil;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernetTemplate = hibernetTemplate;

	}

	@Autowired
	JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{

		this.jdbcTemplate = jdbcTemplate;

	}

	public final String GET_WORKORDER_DETAILS = "SELECT currentWorkStage FROM WorkOrder where ticketId=:ticketId";
	public final String GET_ACTIVITYPROGRESSWEIGHTAGEMAPPING = "SELECT * FROM ActivityProgressWeightageMapping";
	public final String GET_DEPLOYMENTSETTING = "SELECT * FROM DeploymentSetting";
	public final String GET_WORKODER = "SELECT ticketId, initialWorkStage FROM WorkOrder";
	public final String GET_WORKSTAGE = "SELECT * FROM WorkStage";
	public final String GET_PROGRESSWEIGHTAGE = "SELECT sum(progressWeightage) FROM ActivityProgressWeightageMapping where activityId in (:activities) and workStageId = :wsId";
	public final String GET_NEW_PROGRESSWEIGHTAGE = "SELECT sum(progressWeightage) from (SELECT a.progressWeightage  from ActivityProgressWeightageMapping a inner join TicketActivityLog t  inner join WorkOrder w  on a.activityId=t.activityId  and a.workStageId=w.currentWorkStage and w.ticketId=t.ticketId and   t.ticketId=:ticketId group by a.activityId) as T";
	public final String GET_WORKSTAGETYPE_GIS = "SELECT workstageType FROM Gis where TicketNo =:TicketNo";
	private final static String UPDATE_CURRENT_PROGRESS = "UPDATE WorkOrder SET currentProgress= ? WHERE ticketId= ?";

	public static Map<Long, String> workStageIDWorkStageNameDetails = new HashMap<Long, String>();
	public static Map<String, String> etrDetails = new HashMap<String, String>();


	public void init()
	{
		log.info("EtrCalculatorImpl init method called..");
		getWorkStage();
		getDeploymentSettings();
		log.info("EtrCalculatorImpl init method executed.");
	}

	private void getDeploymentSettings()
	{
		List<DeploymentSettingVo> deploymentSettingslist = jdbcTemplate
				.query(GET_DEPLOYMENTSETTING, new DeploymentSettingMapper());
		if (deploymentSettingslist.isEmpty())
		{

			log.info("DeploymentSetting details are not available");
			return;

		} else
		{

			for (DeploymentSettingVo deploymentSettings : deploymentSettingslist)
			{
				addDloymentSettingsToCache(deploymentSettings);
			}

		}

	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}

	private void addDloymentSettingsToCache(
			DeploymentSettingVo deploymentSettings)
	{
		etrDetails.put(deploymentSettings.getName(), deploymentSettings
				.getValue());
	}

	
	private void getWorkStage()
	{
		List<WorkStageVo> workStageList = jdbcTemplate
				.query(GET_WORKSTAGE, new WorkStageMapper());
		if (workStageList.isEmpty())
		{

			log.info("WorkStage details are not available");
			return;

		} else
		{

			for (WorkStageVo workStage : workStageList)
			{
				addToCache(workStage);
			}

		}
	}

	private void addToCache(WorkStageVo workStages)
	{
		workStageIDWorkStageNameDetails.put(workStages.getId(), workStages
				.getWorkStageName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.domain.engines.etr.EtrCalculator#
	 * calculateEtrForTickert
	 * (com.cupola.fwmp.service.domain.engines.etr.EtrCalculatorPojo)
	 */

	@Override
	public Date getETRforWorkOrderType(String woType)
	{

		String value = etrDetails.get(woType);

		if (value == null)
			return new Date();

		int valueOf = Integer.valueOf(value.replace("days", "").trim());
		Date communicationEtr = etrCalculatorUtil
				.calculateETRInWorkingHours(new Date(),valueOf , "days");

		log.info("Returing woType from days " + value + " to date " + communicationEtr);

		return communicationEtr;

	}

	@Override
	public Date getETRforWorkStageType(Long initialWorkStage)
	{
		String value =  WorkStageType.COPPER == initialWorkStage  ? etrDetails.get(WorkStageName.COPPER ) : 
																	etrDetails.get(WorkStageName.FIBER );
		if (value == null)
			return new Date();

		int valueOf = Integer.valueOf(value.replace("days", "").trim());
		Date communicationEtr = etrCalculatorUtil
				.calculateETRInWorkingHours(new Date(),valueOf , "days");

		log.info("Returing woType from days " + value + " to date " + communicationEtr);

		return communicationEtr;

	}

	
	@Override
	public int calculateActivityPercentage(Long ticketId, Long activityId)
	{
		Session session = null;
		try
		{
			session = hibernetTemplate.getSessionFactory().openSession();

			if (ticketId > 0)
			{
				Query q = null;

				q = session.createSQLQuery(GET_NEW_PROGRESSWEIGHTAGE)
						.setLong("ticketId", ticketId);

				log.debug("Get etr " + q.getQueryString());
				List<BigDecimal> sum = q.list();

				if (sum != null && !sum.isEmpty() && sum.get(0) != null)
				{
					return updateProgressPersentage(sum.get(0).intValue(), ticketId);

				} else
				{
					return updateProgressPersentage(0, ticketId);
				}
			} else
			{
				log.info("ticket id is not get so progress is : 0");
			}

		} catch (Exception e)
		{
			log.error("calculateActivityPercentage for  " + ticketId + " "+e.getMessage());
			return 0;
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		return 0;
	}

	private int updateProgressPersentage(int intValue, Long ticketId)
	{

		Object[] params = { intValue, ticketId };

		int[] types = { Types.BIGINT, Types.BIGINT };

		int rows = jdbcTemplate.update(UPDATE_CURRENT_PROGRESS, params, types);

		return intValue;
	}

	@Override
	public Date calculateEtrForTicketCreation(long ticketId)
	{
		Session session = null;
		try
		{
			session = hibernetTemplate.getSessionFactory().openSession();
			if (ticketId > 0)
			{
				log.info(">>>>>>>>>etrCalculatorPojo.getTicketId(): "
						+ ticketId);
				Query query = session.createQuery(GET_WORKSTAGETYPE_GIS)
						.setParameter("TicketNo", ticketId);

				List workstageType = query.list();

				if (workstageType != null && !workstageType.isEmpty()
						&& workstageType.get(0) != null)
				{
					log.info("WorkStage type from GIS while calculating etr "
							+ workstageType.get(0) + " for ticket id "
							+ ticketId);

					int workTypeTemp = (int) workstageType.get(0);
					long workType = workTypeTemp;

					log.info("WorkStage type from GIS while calculating etr in english "
							+ workStageIDWorkStageNameDetails.get(workType)
							+ " for ticket id " + ticketId);

					if (workStageIDWorkStageNameDetails.get(workType)
							.equalsIgnoreCase(WorkStageName.FIBER))
					{
						log.info("Estimated ETR for Fiber "
								+ etrDetails.get(WorkStageName.FIBER)
								+ " for ticketId " + ticketId);
						return getETRforWorkOrderType(WorkStageName.FIBER);
					}
					if (workStageIDWorkStageNameDetails.get(workType)
							.equalsIgnoreCase(WorkStageName.COPPER))
					{
						log.info("Estimated ETR for copper "
								+ etrDetails.get(WorkStageName.COPPER)
								+ " for ticketId " + ticketId);

						return getETRforWorkOrderType(WorkStageName.COPPER);
					}
				} else
				{
					return getETRforWorkOrderType(WorkStageName.FIBER);
				}

			}
			return getETRforWorkOrderType(WorkStageName.FIBER);
		} catch (HibernateException e)
		{
			log.error("Error While executing query :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		return new Date();
	}


	@Override
	public Date calculateEtrForFRTicket(Long flowID,String cityCode,String branchCode)
	{
		log.info("flowID :: " + flowID + " cityCode :: "+cityCode+" branchCode :: " +branchCode);
		if (flowID != null && flowID.longValue() >= -1 && cityCode != null && branchCode != null )
		{
			try
			{
				String defaultEtr =  FrDeploymentSettingDaoImpl.frDefaultETR.get(cityCode+"_"+branchCode+"_"+flowID);
				String bufferTime = FrDeploymentSettingDaoImpl.bufferTime.get(cityCode+"_"+branchCode+"_"+flowID);
				
				if(defaultEtr != null && bufferTime != null)
					return etrCalculatorUtil.calculateETRInWorkingHours(new Date(),Long.valueOf(defaultEtr)+Long.valueOf(bufferTime) , "MINUTES");
				
				else
				{
					log.info(" cityCode or  branchCode or flowID not found :"
							+ cityCode + "_" + branchCode + "_" + flowID);
					log.info("claculate Etr With city code");
				}
			} 
			catch (Exception e)
			{
				log.error("Error while calculating Etr for flow id : "+flowID);
				e.printStackTrace();
			}
		}
		if(cityCode != null )
		{
			String cityDefaultEtr =  DefinitionCoreServiceImpl.frCityWiseDefaultETR.get(cityCode);
			log.info("default ETR calculation for city " + cityCode);
			if(cityDefaultEtr!=null) {
				Date defDate = etrCalculatorUtil.calculateETRInWorkingHours(new Date(),Integer.valueOf(cityDefaultEtr) , "hours");
				log.info("default ETR calculation for city " + cityCode + " is " + defDate);
				return defDate;
			}
			
			
			
		}
		return null;
	}

}

class WorkStageMapper implements RowMapper<WorkStageVo>
{

	@Override
	public WorkStageVo mapRow(ResultSet resultSet, int rowNumber)
			throws SQLException
	{

		WorkStageVo workStage = new WorkStageVo();

		workStage.setId(resultSet.getLong("id"));
		workStage.setWorkStageName(resultSet.getString("workStageName"));

		return workStage;
	}
}

class DeploymentSettingMapper implements RowMapper<DeploymentSettingVo>
{

	@Override
	public DeploymentSettingVo mapRow(ResultSet resultSet, int rowNumber)
			throws SQLException
	{

		DeploymentSettingVo ds = new DeploymentSettingVo();

		ds.setName(resultSet.getString("name"));
		ds.setValue(resultSet.getString("value"));
		return ds;
	}
}
