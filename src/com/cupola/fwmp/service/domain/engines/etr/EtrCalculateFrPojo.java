package com.cupola.fwmp.service.domain.engines.etr;

import com.cupola.fwmp.vo.fr.DecisionMethod;

public class EtrCalculateFrPojo {
	private int flowId;
	private DecisionMethod decisionPointId;
	private int selectedOption;
	public DecisionMethod getDecisionPointId() {
		return decisionPointId;
	}
	public void setDecisionPointId(DecisionMethod decisionPointId) {
		this.decisionPointId = decisionPointId;
	}
	public int getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
	}
	
	public int getFlowId() {
		return flowId;
	}
	public void setFlowId(int flowId) {
		this.flowId = flowId;
	}
	@Override
	public String toString() {
		return "EtrCalculateFrPojo [flowId=" + flowId + ", decisionPointId="
				+ decisionPointId + ", selectedOption=" + selectedOption + "]";
	}
	
	

}
