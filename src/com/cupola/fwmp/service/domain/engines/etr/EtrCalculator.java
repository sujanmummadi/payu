/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.etr;

import java.util.Date;

/**
 * @author aditya
 * 
 */
public interface EtrCalculator
{

	int calculateActivityPercentage(Long ticketId, Long activityId);
	Date getETRforWorkOrderType(String woType);
	Date calculateEtrForTicketCreation(long ticketId);
	Date calculateEtrForFRTicket(Long flowID,String cityCode,String branchCode);
	Date getETRforWorkStageType(Long initialWorkStage);

}
