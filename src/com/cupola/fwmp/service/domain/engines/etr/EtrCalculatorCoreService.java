package com.cupola.fwmp.service.domain.engines.etr;

import com.cupola.fwmp.response.APIResponse;

public interface EtrCalculatorCoreService {
	public APIResponse calculateActivityPercentage(EtrCalculatorPojo etrCalculatorPojo);

}
