/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.customernotification.CustomerNotificationDAO;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.CustomerNotification;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.vo.MessageNotificationVo;

/**
 * created by kiran on 12/20/2016
 * 
 * @author kiran
 * 
 */

public class CustomerNotifierJob
{
	private static Logger log = LogManager.getLogger(CustomerNotifierJob.class
			.getName());

	@Autowired
	CustomerNotificationDAO customerNotificationDAO;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DefinitionCoreService definitionCoreService;

	public static String STATUS_MESSAGE_CODE = "customer.notifier";

	@Scheduled(cron = "${fwmp.customer.notifier.job.cron.trigger}")
	public void executeInternal()
	{

		customerNotifier();
	}

	/**
	 * get all ticketIds from {@link CustomerNotification}
	 */
	private void customerNotifier()
	{
		List<CustomerNotification> customerNotifications = customerNotificationDAO
				.getAllTicketsToSendCustomerNotification();
		if (customerNotifications != null)
		{
			List<BigInteger> ticketIds = new ArrayList<BigInteger>();
			for (CustomerNotification customerNotification : customerNotifications)
			{

				ticketIds.add(BigInteger.valueOf(customerNotification
						.getTicketId()));
			}

			if (ticketIds != null)
			{
				getCustomerDetailsByTicketIds(ticketIds);
			}
		}

	}

	/**
	 * send customer notification with corresponding mobile number
	 * 
	 * @param ticketIds
	 */
	private void getCustomerDetailsByTicketIds(List<BigInteger> ticketIds)
	{
		String messageToSend = definitionCoreService
				.getMessagesForCustomerNotificationFromProperties(STATUS_MESSAGE_CODE);
		List<Customer> customers = customerDAO
				.getCustomersByTicketIdList(ticketIds);
		if (customers != null && !customers.isEmpty())
		{
			for (Customer customer : customers)
			{
				if (customer != null)
				{
					if (customer.getTickets() != null)
					{
						Set<Ticket> tickets = customer.getTickets();
						for (Ticket ticket : tickets)
						{
							if (messageToSend.contains("_DATE_"))
								messageToSend = messageToSend
										.replaceAll("_DATE_", ticket
												.getCommunicationETR() + "");
							MessageNotificationVo notificationVo = new MessageNotificationVo();
							notificationVo.setMobileNumber(customer
									.getMobileNumber());
							notificationVo.setEmail(customer.getEmailId());
							notificationVo.setMessage(messageToSend);
							notificationVo.setSubject("ACT Notification");
							notificationService
									.sendNotification(notificationVo);
							updateCustomerNotificationAsSent(ticket.getId());
						}
					}
				}
			}
		}
	}

	/**
	 * update CustomerNotification status as sent(101)
	 * 
	 * @param id
	 */
	private void updateCustomerNotificationAsSent(long id)
	{
		int result = customerNotificationDAO
				.updateCustomerNotificationStatus(id);
		if (result > 0)
			log.info("CustomerNotification sent status updated");

	}

}
