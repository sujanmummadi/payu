/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.classification.jobs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.ticketgc.TicketsForGCCoreService;

/**
 * @author aditya
 * 
 */
@Lazy(false)
public class TicketGcJob
{
	private static Logger log = LogManager.getLogger(TicketGcJob.class
			.getName());

	@Autowired
	TicketsForGCCoreService ticketsForGCCoreService;

	@Scheduled(cron = "${fwmp.ticket.gc.cron.trigger.time}")
	public void executeCommands()
	{
		log.info("Execute commands called from  TicketGcJob");

		removeDeploymentTicketFromQueue();

		removeSalesTicketFromQueue();
		
		removeFrTicketFromQueue();

		log.info("Executed commands from  TicketGcJob");
	}

	public void removeDeploymentTicketFromQueue()
	{
		log.info("Remove Deployment Ticket From Queue Called");

		APIResponse response = ticketsForGCCoreService
				.removeDeploymentTicketFromQueue();

		log.info("Removed Deployment Ticket From Queue " + response);
	}

	public void removeSalesTicketFromQueue()
	{
		log.info("Remove Sales Ticket From Queue Called");

		APIResponse response = ticketsForGCCoreService
				.removeSalesTicketFromQueue();

		log.info("Removed Sales Ticket From Queue " + response);
	}
	
	public void removeFrTicketFromQueue()
	{
		log.info("Remove FR Ticket From Queue Called");

		APIResponse response = ticketsForGCCoreService
				.removeFrTicketFromQueue();

		log.info("Removed FR Ticket From Queue " + response);
	}


}
