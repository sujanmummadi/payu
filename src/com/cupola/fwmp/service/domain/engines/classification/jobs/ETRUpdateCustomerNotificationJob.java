package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.util.Date;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.fr.FrDeploymentSettingDaoImpl;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.fr.etr.FrTicketEtrDao;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrIntegrationCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.util.ETRCalculatorUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.fr.ETRExpireVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;

/**
 * 
 * @author kiran
 *
 */
@Lazy(false)
public class ETRUpdateCustomerNotificationJob
{
	private static Logger log = LogManager
			.getLogger(ETRUpdateCustomerNotificationJob.class.getName());
	@Autowired
	FrTicketDao frTicketDao;
	
	@Autowired
	FrTicketEtrDao frTicketEtrDao;
	
	@Autowired
	ETRCalculatorUtil etrCalculatorUtil;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DefinitionCoreService definitionCoreService;
	
	@Autowired
	BranchDAO branchDAO;

	@Autowired
	private FrIntegrationCoreService frInteMq;
	
	public static String STATUS_MESSAGE_CODE = "etr.updated";
	
	@Value("${fwmp.etr.expire.remarks}")
	private String remarks;



	@Scheduled(cron = "${fwmp.customer.notification.cron.trigger}")
	public void executeInternal()
	{
		log.info("ETRUpdate for Customer Notification called from Cron trigger");
		log.debug(">>>>>>>thread name " + Thread.currentThread().getName());
		etrExpiryUpdate();
	}

	/**
	 * get all  not completed Tickets  before going to expire 1 hour
	 */
	private void etrExpiryUpdate()
	{
		List<ETRExpireVo> etrExpireVos =  frTicketDao.getAllETRExpiredTickets();
		if(etrExpireVos != null)
		updateCalculatedEtr(etrExpireVos);
		
	}

	/**
	 * re-calculate etr currtent ,communication etr and etr expirey count
	 *  update FrTicketETR 
	 * @param etrExpireVos
	 */
	private void updateCalculatedEtr(List<ETRExpireVo> etrExpireVos)
	{
		if(etrExpireVos == null  || etrExpireVos.isEmpty())
			return ;
		
		for (ETRExpireVo etrExpireVo : etrExpireVos)
		{
			String cityCode = 	LocationCache.getCityCodeById(etrExpireVo.getCityId());
			String branchCode = branchDAO.getBranchCodeById(etrExpireVo.getBranchId());
			String defaultEtr = FrDeploymentSettingDaoImpl.frDefaultETR.get(cityCode+"_"+branchCode+"_"+etrExpireVo.getCurrentFlowId());
			Date calculatedETR = null;
			if(defaultEtr != null)
			{

				calculatedETR = etrCalculatorUtil.calculateETRInWorkingHours(new Date(), Integer.valueOf(defaultEtr), "MINUTES");
				Integer result = frTicketEtrDao.updateETRByJob(etrExpireVo.getTicketId(), calculatedETR, etrExpireVo.getEtrElapsedCount()+1);
				if(result > 0)
				{
					log.info("ETR Updated Successfully for ticketId :: "+etrExpireVo.getTicketId() );
					TicketModifyInMQVo vo = new TicketModifyInMQVo();
					vo.setTicketId(etrExpireVo.getTicketId());
					vo.setEtrValue(GenericUtil.convertToUiDateFormat(calculatedETR));
					vo.setRemarks(remarks);
					frInteMq.updateEtrInMQ(vo);
//					sendCustomerNotification(etrExpireVo , calculatedETR);
				}
			}
			else
				log.info("default etr key not found :"+cityCode+"_"+branchCode+"_"+etrExpireVo.getCurrentFlowId());
				
			
			
		}
		
		
	}

	/**
	 * send customer notification after updating etr
	 * @param etrExpireVos
	 * @param calculatedETR
	 */
	private void sendCustomerNotification(ETRExpireVo etrExpireVo,
			Date calculatedETR)
	{
		String messageToSend = definitionCoreService
				.getMessagesForCustomerNotificationFromProperties(STATUS_MESSAGE_CODE);
		String currentEtrDate = GenericUtil.convertToUiDateFormat( calculatedETR);
		if (messageToSend.contains("_MQID_"))
			messageToSend = messageToSend.replaceAll("_MQID_", etrExpireVo.getMqId()+"");
		
		if (messageToSend.contains("_DATE_"))
			messageToSend = messageToSend.replaceAll("_DATE_", currentEtrDate);
		
		String helpLineNo = definitionCoreService
				.getNIHelpLineNumber(LocationCache.getCityCodeById(etrExpireVo.getCityId()));
		
		if (messageToSend.contains("_HELP_LINE_NUMBER_"))
			messageToSend = messageToSend.replaceAll("_HELP_LINE_NUMBER_", helpLineNo);
		
		MessageNotificationVo notificationVo = new MessageNotificationVo();
		notificationVo.setMobileNumber(etrExpireVo.getMobileNumber());
		notificationVo.setMessage(messageToSend);
		notificationVo.setSubject("ACT Notification");
		notificationService.sendNotification(notificationVo);
		
	}
	

}
