package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.dao.integ.mq.hyd.HydMqDAOImpl;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.integ.mq.roi.RoiMqDAOImpl;
import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.vo.integ.CRMJobInput;

public class CrmFrTicketPullerJob {
	private final static Logger LOGGER = LogManager.getLogger(CrmFrTicketPullerJob.class);

	BlockingQueue<CRMJobInput> roiCityBasedQueue;
	Thread updateProcessorThread = null;

	BlockingQueue<String> roiHYDBranchBasedQueue;
	Thread updateProcessorThreadHYD = null;

	public void init() {
		LOGGER.info("CrmFrTicketPullerJob enabling updateProcessorThread processing-");
		roiCityBasedQueue = new LinkedBlockingQueue<CRMJobInput>(100000);

		if (updateProcessorThread == null) {
			updateProcessorThread = new Thread(roiCityThread, "RoiCityThreadHelper");
			updateProcessorThread.start();
		}

		roiHYDBranchBasedQueue = new LinkedBlockingQueue<String>(100000);

		if (updateProcessorThreadHYD == null) {
			updateProcessorThreadHYD = new Thread(hydCityThread, "HydCityThreadHelper");
			updateProcessorThreadHYD.start();
		}
		
//		runROI();
//		runHYD();

		LOGGER.info("CrmFrTicketPullerJob updateProcessorThread enabled");
	}

	Runnable roiCityThread = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_DEVICE_CACHE_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final CRMJobInput prospect = roiCityBasedQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (prospect != null && !prospect.getCity().isEmpty()) {
										LOGGER.info("Getting record for city " + prospect);

										createRoiComplaints(prospect.getCity(), prospect.getBranch());

									}

								} catch (Exception e) {
									e.printStackTrace();
									LOGGER.error("Error in FR WO thread " + " " + prospect + "" + e.getMessage());
								}

							}
						};
						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						LOGGER.error("Error in FR WO main thread " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				LOGGER.error("error in Processor main ROI FR WO loop- msg=" + e.getMessage());
				e.printStackTrace();
			}

			LOGGER.info("Processor thread interrupted- getting out of processing loop");
		}

	};

	Runnable hydCityThread = new Runnable() {
		@Override
		public void run() {
			try {
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_DEVICE_CACHE_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true) {
					try {
						final String prospect = roiHYDBranchBasedQueue.take();

						Runnable worker = new Runnable() {
							@Override
							public void run() {
								try {
									// Start processing from here
									if (prospect != null && !prospect.isEmpty()) {
										LOGGER.info("Getting record for branch " + prospect);

										createHydComplaints(prospect);

									}

								} catch (Exception e) {
									e.printStackTrace();
									LOGGER.error("Error in FR WO thread " + " " + prospect + "" + e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						LOGGER.error("Error in HYD FR WO main thread " + e.getMessage());
						continue;
					}

				}

			} catch (Exception e) {
				LOGGER.error("error in Processor main HYD FR WO loop- msg=" + e.getMessage());
				e.printStackTrace();
			}

			LOGGER.info("Processor thread interrupted- getting out of processing loop");
		}

	};

	@Autowired
	private CrmJobHelper helper;

	@Autowired
	private RoiMqDAOImpl roiMqDAOImpl;

	@Autowired
	private HydMqDAOImpl hydMqDAOImpl;

	@Autowired
	private DefinitionCoreService definitionCoreService;

	@Scheduled(cron = "${fwmp.fr.crm.cron.trigger.hyd}")
	public void runHYD() {
		LOGGER.info("*************** RUN HYD Started ************");
		Map<Long, String> cities = LocationCache.getAllCity();
		Long hydId = 0L;
		for (Map.Entry<Long, String> entry : cities.entrySet()) {
			
			if (entry.getValue().toUpperCase().equalsIgnoreCase(CityName.HYDERABAD_CITY))
			{
				LOGGER.info("*************** Getting MQ data for city ************" + entry.getValue().toUpperCase());

				hydId = entry.getKey();
			}
		}

		Map<Long, String> branches = LocationCache.getBranchesByCityId(hydId + "");

		for (Map.Entry<Long, String> entry : branches.entrySet()) {
			LOGGER.info("*************** Getting MQ data for Branch ************" + entry.getValue().toUpperCase());
			roiHYDBranchBasedQueue.add(entry.getValue().toUpperCase());
		}

		LOGGER.info("*************** RUN HYD ************");
	}

	@Scheduled(cron = "${fwmp.fr.crm.cron.trigger.roi}")
	public void runROI() {
		LOGGER.info("*************** RUN ROI started ************");

		Map<Long, String> cities = LocationCache.getAllCity();

		for (Map.Entry<Long, String> cityEntry : cities.entrySet()) {
			LOGGER.info("*************** Getting MQ data for city start ************" + cityEntry.getValue().toUpperCase());

			if (!cityEntry.getValue().toUpperCase().equalsIgnoreCase(CityName.HYDERABAD_CITY)) {

				Long cityId = cityEntry.getKey();

				Map<Long, String> branches = LocationCache.getBranchesByCityId(cityId + "");

				CRMJobInput crmJobInput = new CRMJobInput();

				/* #######################only branch based################ */
				
//				Long cityId = cityEntry.getKey();
//				Map<Long, String> branches = LocationCache.getBranchesByCityId(cityId + "");
//				for (Map.Entry<Long, String> branchEntry : branches.entrySet()) {
//
//					crmJobInput.setCity(cityEntry.getValue().toUpperCase());
//					crmJobInput.setBranch(branchEntry.getValue().toUpperCase());
//
//					LOGGER.info("*************** Getting MQ data for ROI Branch ************"
//							+ branchEntry.getValue().toUpperCase() + " of city " + cityEntry.getValue().toUpperCase());
//
//					roiCityBasedQueue.add(crmJobInput);
//				}

				/* #######################only city based################ */

				 crmJobInput.setCity(cityEntry.getValue().toUpperCase());
				 crmJobInput.setBranch(null);
				
				 LOGGER.info("*************** Getting MQ data for ROI city " +
				 cityEntry.getValue().toUpperCase());
				
				 roiCityBasedQueue.add(crmJobInput);

				/*
				 * ####################### PAN INDIA city based where as CHENNAI and BLR branch
				 * based ################
				 */

				// if (cityEntry.getValue().equalsIgnoreCase(CityName.BANGALORE_CITY)
				// || cityEntry.getValue().equalsIgnoreCase(CityName.CHENNAI_CITY)) {
				//
				// for (Map.Entry<Long, String> branchEntry : branches.entrySet()) {
				//
				// crmJobInput.setCity(cityEntry.getValue().toUpperCase());
				// crmJobInput.setBranch(branchEntry.getValue().toUpperCase());
				//
				// LOGGER.info("*************** Getting MQ data for ROI Branch ************"
				// + branchEntry.getValue().toUpperCase() + " of city "
				// + cityEntry.getValue().toUpperCase());
				//
				// roiCityBasedQueue.add(crmJobInput);
				// }
				//
				// } else {
				//
				// crmJobInput.setCity(cityEntry.getValue().toUpperCase());
				// crmJobInput.setBranch(null);
				//
				// LOGGER.info("*************** Getting MQ data for ROI city " +
				// cityEntry.getValue().toUpperCase());
				//
				// roiCityBasedQueue.add(crmJobInput);
				// }
			}
		}

		LOGGER.info("*************** RUN ROI ************");
	}

	private void createRoiComplaints(String cityName, String branchName) {
		LOGGER.info("*************** RUN ROI called for ************ " + cityName + " and branch " + branchName);

		List<MQWorkorderVO> roiMQWorkorderVOList = null;
		/*Please uncomment below line if data pulling is anything other other than branch level and comment single argument method*/
		
		 roiMQWorkorderVOList = roiMqDAOImpl.getAllOpenFrTicketForROI(cityName,
		 branchName);

		/*Please uncomment below line if data pulling is branch level and comment double argument method*/
				
//		roiMQWorkorderVOList = roiMqDAOImpl.getAllOpenFrTicketForROI(branchName);

		LOGGER.info("*************** GOT Data from MQ for city ************ " + cityName + " and branch " + branchName);

		if (roiMQWorkorderVOList != null && !roiMQWorkorderVOList.isEmpty())
			helper.createFrTicketDetail(roiMQWorkorderVOList, cityName);
	}

	private void createHydComplaints(String branchName) {
		LOGGER.info("*************** RUN HYD called for ************ " + branchName);
		List<MQWorkorderVO> hydMQWorkorderVOList = hydMqDAOImpl.getAllOpenFrTicketForHYD(branchName);
		LOGGER.info("***************GOT HYD data for ************ " + branchName);
		if (hydMQWorkorderVOList != null && !hydMQWorkorderVOList.isEmpty())
			helper.createFrTicketDetail(hydMQWorkorderVOList, branchName);

	}

	private void createRoiSkipWO() {
		/*
		 * List<MQWorkorderVO> hydMQWorkorderVOList =
		 * roiMqDAOImpl.getAllOpenFrTicketForROI(); if( hydMQWorkorderVOList != null &&
		 * !hydMQWorkorderVOList.isEmpty() )
		 * helper.createFrTicketDetail(hydMQWorkorderVOList, "");
		 */
	}

	private void createHydSkipWO(String branchName) {
		List<MQWorkorderVO> hydMQWorkorderVOList = hydMqDAOImpl.getAllFrOpenWorkOrderPrivious15Minute(branchName);
		if (hydMQWorkorderVOList != null && !hydMQWorkorderVOList.isEmpty())
			helper.createFrTicketDetail(hydMQWorkorderVOList, "");

	}

	@Scheduled(cron = "${fwmp.fr.crm.cron.trigger.every.hour.hyd}")
	public void runHYDEvery15Minute() {

		Map<Long, String> cities = LocationCache.getAllCity();
		Long hydId = 0L;
		for (Map.Entry<Long, String> entry : cities.entrySet()) {

			if (entry.getValue().toUpperCase().equalsIgnoreCase(CityName.HYDERABAD_CITY)) {
				LOGGER.info("*************** Getting MQ data for city ************" + entry.getValue().toUpperCase());

				hydId = entry.getKey();
			}
		}

		Map<Long, String> branches = LocationCache.getBranchesByCityId(hydId + "");

		for (Map.Entry<Long, String> entry : branches.entrySet()) {
			LOGGER.info("*************** Getting MQ data for Branch ************" + entry.getValue().toUpperCase());
			createHydSkipWO(entry.getValue().toUpperCase());
		}

		LOGGER.info("*************** RUN HYD EVERY HOUR ************");
	}

	@Scheduled(cron = "${fwmp.fr.crm.cron.trigger.every.hour.roi}")
	public void runROIEvery15Minute() {
		
		createRoiSkipWO();
		LOGGER.info("*************** RUN ROI EVERY HOUR ************");
	}

	int defaultWorkOrder = 10120;

	private List<MQWorkorderVO> creatNDummyFRWO(int noOfDummyWO) {
		List<MQWorkorderVO> dummy = new ArrayList<MQWorkorderVO>();

		if (noOfDummyWO == 0)
			return dummy;

		MQWorkorderVO wo = null;
		String newWo = "";
		FrDeviceVo device = null;
		for (int i = 0; i < noOfDummyWO; i++) {
			defaultWorkOrder++;
			newWo = defaultWorkOrder + "" + (i * noOfDummyWO + Math.round(Math.random()));
			LOGGER.info("Dummy WO Generated : " + newWo);
			wo = new MQWorkorderVO();

			wo.setWorkOrderNumber(newWo);
			wo.setSymptom("CX Not pinging");
			wo.setTicketCategory("SD- Complaint");
			wo.setCityId("1483009728469023");
			wo.setAreaId("1483009728977335");
			wo.setBranchId("1483009728831807");

			wo.setProspectNumber("123456");
			wo.setMqId("12345" + (Math.round(Math.random())));
			wo.setCustomerName("ABC TEST ACT");
			wo.setCustomerAddress("Madivala Address");
			wo.setCustomerMobile("9999999999");
			wo.setResponseStatus("ACTIVE");

			wo.setVoc("VOC BY JAMAL");

			device = new FrDeviceVo();
			device.setFxName("KKPL5-MOHDI-CHDRB-8-9-244.7");
			device.setFxIp("NA");
			device.setFxMax("NA");
			device.setFxPort(3);

			wo.setDeviceData(device);
			dummy.add(wo);
		}
		return dummy;
	}
}
