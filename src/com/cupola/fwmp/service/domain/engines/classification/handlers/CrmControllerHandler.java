package com.cupola.fwmp.service.domain.engines.classification.handlers;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;

/**
 * @author aditya
 * 
 */
// @Component("classificationControllerHandler")
public interface CrmControllerHandler
{
	public void createCustomerEntry();

	public void createTableEntry();

	public boolean createWorkOrderEntry(String prospectNo, String mqId,
			String workOrderNumber, String ticketCategory);

	public boolean createFrDetailsEntry(MQWorkorderVO roiMQWorkorderVO,String deviceName);

	Map<Long, Boolean> createWorkOrderEntryInBulk(
			List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap);

	public Map<Long, Boolean> createFrWorkOrderInBulk(
			List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap);

}
