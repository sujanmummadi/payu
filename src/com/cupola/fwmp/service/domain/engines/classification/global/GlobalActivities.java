/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.classification.global;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.domain.engines.classification.global.constant.DeploymentConstants;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.UserVo;

/**
 * @author aditya
 * 
 */
public class GlobalActivities
{

	@Autowired
	UserDao userDao;

	private static Logger log = LogManager
			.getLogger(GlobalActivities.class.getName());

	private Map<String, String> deploymentSetting = new ConcurrentHashMap<>();

	private Map<String, LinkedHashSet<Long>> mainQueue = new ConcurrentHashMap<>();

	private Map<String, LinkedHashSet<Long>> workingQueue = new ConcurrentHashMap<>();

	private Map<String, LinkedHashSet<Long>> frMainQueue = new ConcurrentHashMap<>();

	public Map<String, LinkedHashSet<Long>> getFrMainQueue()
	{
		return frMainQueue;
	}

	public void setFrMainQueue(Map<String, LinkedHashSet<Long>> frMainQueue)
	{
		this.frMainQueue = frMainQueue;
	}

	public Map<String, LinkedHashSet<Long>> getWorkingQueue()
	{
		return workingQueue;
	}

	public void setWorkingQueue(Map<String, LinkedHashSet<Long>> neWorkingQueue)
	{
		this.workingQueue = neWorkingQueue;
	}

	public Map<String, String> getDeploymentSetting()
	{
		return deploymentSetting;
	}

	public void setDeploymentSetting(Map<String, String> deploymentSetting)
	{
		this.deploymentSetting = deploymentSetting;
	}

	public Map<String, LinkedHashSet<Long>> getMainQueue()
	{
		return mainQueue;
	}

	public void setMainQueue(Map<String, LinkedHashSet<Long>> neMainQueue)
	{
		this.mainQueue = neMainQueue;
	}

	public void addToDeploymentSetting(String key, String value)
	{

		deploymentSetting.put(key, value);
	}

	public String getDeploymentSettingByKey(String key)
	{
		if (deploymentSetting.containsKey(key))
			return deploymentSetting.get(key);
		else
			return null;
	}

	public String mergeMainQueue(Map<String, LinkedHashSet<Long>> mainQueue)
	{
		if (mainQueue != null && !mainQueue.isEmpty())
		{
			log.info("########### Merge main queue ########### " + mainQueue.size());
			
			for (Map.Entry<String, LinkedHashSet<Long>> entry : mainQueue.entrySet())
			{
				String key = entry.getKey();
				LinkedHashSet<Long> ticketNumberList = entry.getValue();

				addTicket2MainQueue(key, ticketNumberList);
			}

			return ResponseUtil.createSuccessResponse().getStatusMessage();
		}
		{
			return ResponseUtil.createNullParameterResponse().getStatusMessage();
		}
	}

	public void addTicket2MainQueue(String key,
			LinkedHashSet<Long> ticketNumberList)
	{
		if (key == null)
		{
			log.error(" Null Key " + key + "tickets trying to add "
					+ ticketNumberList);
			return;
		}
		if (mainQueue.containsKey(key))
		{
			mainQueue.get(key).addAll(ticketNumberList);

			// LinkedHashSet<Long> tempAreaWorkOrderList =
			// mainQueue.get(key).add;
			//
			// log.debug("Key " + key
			// + " already present in Main Queue map values are "
			// + tempAreaWorkOrderList);
			//
			// tempAreaWorkOrderList.addAll(ticketNumberList);
			//
			// log.debug("Ater adding" + ticketNumberList
			// + " in Main Queue map values for " + key + " are "
			// + tempAreaWorkOrderList);

			// Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new
			// ConcurrentHashMap<>();
			//
			// tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
			//
			// mainQueue.putAll(tempAreaWorkOrderMapping);

//			log.debug("Consolidated value for  " + key
//					+ " in Main Queue map are " + mainQueue.get(key));
		} else
		{

			mainQueue.put(key, new LinkedHashSet<Long>(ticketNumberList));

				log.debug("Consolidated value for " + key
						+ " in main queue are " + mainQueue.get(key));

		}

	}

	public Set<Long> getAllTicketFromMainQueueByKey(String key)
	{
		if (mainQueue.containsKey(key))
		{

			log.debug("For Key " + key
					+ " already present in NE Main Queue   ");

			return mainQueue.get(key);

		} else
		{

			log.info("Value not available in NE Main Queue map for " + key);

			return null;

		}
	}

	public boolean removeTicketFromMainQueueByKeyAndTicketNumber(String key,
			Long ticketNumber)
	{

		if (mainQueue.containsKey(key))
		{
			return mainQueue.get(key).remove(ticketNumber);

			// LinkedHashSet<Long> tempAreaWorkOrderList = mainQueue.get(key);
			//
			// log.debug("Key " + key + " already present in MainQueue values
			// are "
			// + tempAreaWorkOrderList);
			//
			// boolean result = tempAreaWorkOrderList.remove(ticketNumber);
			//
			// log.debug("After removing " + ticketNumber
			// + " from MainQueue map values for " + key + " are "
			// + tempAreaWorkOrderList);
			//
			// Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new
			// ConcurrentHashMap<>();
			//
			// tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
			//
			// mainQueue.putAll(tempAreaWorkOrderMapping);
			//
			// log.debug("Consolidated value for key " + key
			// + " are in MainQueue map" + mainQueue.get(key));
			//
			// return result;
		} else
		{
			return false;
		}

	}

	public boolean removeBulkTicketsFromMainQueueByKey(String key,
			LinkedHashSet<Long> ticketNumberList)
	{
		if (mainQueue.containsKey(key))
		{
			return mainQueue.get(key).removeAll(ticketNumberList);

			//
			// LinkedHashSet<Long> tempAreaWorkOrderList = mainQueue.get(key);
			//
			// log.debug("Key " + key + " already present in MainQueue values
			// are "
			// + tempAreaWorkOrderList);
			//
			// boolean result =
			// tempAreaWorkOrderList.removeAll(ticketNumberList);
			//
			// log.debug("After removing " + ticketNumberList
			// + " from MainQueue map values for " + key + " are "
			// + tempAreaWorkOrderList);
			//
			// Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new
			// ConcurrentHashMap<>();
			//
			// tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
			//
			// mainQueue.putAll(tempAreaWorkOrderMapping);
			//
			// log.debug("Consolidated value for key " + key
			// + " are in MainQueue map" + mainQueue.get(key));
			//
			// return result;
		} else
		{
			return false;
		}

	}

	public boolean addTicket2WorkingQueue(String key,
			LinkedHashSet<Long> ticketNumberList, boolean flagOfManualInter)
	{

		if (workingQueue.containsKey(key))
		{

			if (flagOfManualInter)
			{
				return workingQueue.get(key).addAll(ticketNumberList);
				//
				// LinkedHashSet<Long> tempAreaWorkOrderList = workingQueue
				// .get(key);
				//
				// log.debug("Key " + key
				// + " already present in WorkingQueue map values are "
				// + tempAreaWorkOrderList);
				//
				// tempAreaWorkOrderList.addAll(ticketNumberList);
				//
				// log.debug("Ater adding" + ticketNumberList
				// + " in WorkingQueue map values for " + key + " are "
				// + tempAreaWorkOrderList);
				//
				// Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping =
				// new ConcurrentHashMap<>();
				//
				// tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
				//
				// workingQueue.putAll(tempAreaWorkOrderMapping);
				//
				// log.debug("Consolidated value for key " + key
				// + " in WorkingQueue map are " + workingQueue.get(key));
				//
				// removeTicketFromMainQueueAfterPuttingInWorkingQueue4Offline(key,
				// ticketNumberList);
				//
				// // removeDataFromMainQueueAfterPuttingInWorkingQueue(key,
				// // ticketNumberList);
				//
				// return true;

			} else
			{
				String idString = key.split("_")[3];

				int workingQueueDepth = calculateWorkingQueueDepth(Long
						.valueOf(idString));

				if (workingQueue.size() < workingQueueDepth)
				{

					int depth = workingQueueDepth - workingQueue.size();

					ticketNumberList = getDataFromMainQueueAndPutInWorkingQueue(key, depth);

					LinkedHashSet<Long> tempAreaWorkOrderList = workingQueue
							.get(key);

					log.debug("Key " + key
							+ " already present in WorkingQueue map values are "
							+ tempAreaWorkOrderList);

					tempAreaWorkOrderList.addAll(ticketNumberList);
//
//					log.debug("Ater adding" + ticketNumberList
//							+ " in WorkingQueue map values for " + key + " are "
//							+ tempAreaWorkOrderList);
//
//					Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();
//
//					tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
//
//					workingQueue.putAll(tempAreaWorkOrderMapping);
//
//					log.debug("Consolidated value for key " + key
//							+ " in WorkingQueue map are "
//							+ workingQueue.get(key));
//
					return true;

				} else
				{

					log.debug("Working queue is having maximum defined number of ticket in his queue for key "
							+ key);

					return false;

				}

			}
		} else
		{

			log.debug("Making first time entry for key in WorkingQueue map  "
					+ key);

			workingQueue.put(key, ticketNumberList);

			log.debug("Consolidated value for key " + key
					+ " in WorkingQueue map are " + workingQueue.get(key));

			return true;

		}

	}

	public Set<Long> getAllTicketFromWorkingQueueByKey(String key)
	{

		if (workingQueue.containsKey(key))
		{

			log.debug("For Key " + key
					+ " already present in Working Queue map values are "
					+ workingQueue.get(key));

			return workingQueue.get(key);

		} else
		{

			log.debug("Value not available in Working Queue map for " + key);

			return null;

		}
	}

	public boolean removeTicketFromWorkingQueueByKeyAndTicketNumber(String key,
			Long ticketNumberList)
	{

		if (workingQueue.containsKey(key))
		{

			LinkedHashSet<Long> tempAreaWorkOrderList = workingQueue.get(key);

			log.debug("Key " + key
					+ " already present in WorkingQueue map values are "
					+ tempAreaWorkOrderList);

			boolean result = tempAreaWorkOrderList.remove(ticketNumberList);

//			log.debug("After removing " + ticketNumberList
//					+ " from WorkingQueue map values for key " + key + " are "
//					+ tempAreaWorkOrderList);
//
//			Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();
//
//			tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
//
//			workingQueue.putAll(tempAreaWorkOrderMapping);
//
//			log.debug("Consolidated value for key " + key
//					+ " in WorkingQueue map are " + workingQueue.get(key));

			return result;

		} else
		{

			return false;

		}

	}

	//
	// public int calculateWorkingQueueDepth()
	// {
	//
	// if (AuthUtils.getCurrentUserRole() == UserRole.NE_NI)
	// {
	//
	// return Integer
	// .valueOf(getDeploymentSetting()
	// .get(ClassificationEngineConstants.NI_NE_JOB_WORKING_QUEUE_DEPTH));
	//
	// } else if (AuthUtils.getCurrentUserRole() == UserRole.SALES_EX)
	// {
	//
	// return Integer
	// .valueOf(getDeploymentSetting()
	// .get(ClassificationEngineConstants.SALES_EX_JOB_WORKING_QUEUE_DEPTH));
	//
	// } else if (AuthUtils.getCurrentUserRole() == UserRole.NE_FR)
	// {
	//
	// return Integer
	// .valueOf(getDeploymentSetting()
	// .get(ClassificationEngineConstants.FR_NE_JOB_WORKING_QUEUE_DEPTH));
	//
	// } else
	// {
	//
	// return 0;
	// }
	//
	// }

	public int calculateWorkingQueueDepth(Long id)
	{

		UserVo user = userDao.getUserById(id);

		if (user == null)
			return 0;
		if (user.getUserRoles().iterator().next().getName()
				.equalsIgnoreCase(UserRole.ROLE_NI))
		{

			return Integer.valueOf(getDeploymentSetting()
					.get(DeploymentConstants.NI_NE_JOB_WORKING_QUEUE_DEPTH));

		} else if (userDao.getUserById(id).getUserRoles().iterator().next()
				.getName().equalsIgnoreCase(UserRole.ROLE_SALES))
		{

			return Integer.valueOf(getDeploymentSetting()
					.get(DeploymentConstants.SALES_EX_JOB_WORKING_QUEUE_DEPTH));

		} else if (userDao.getUserById(id).getUserRoles().iterator().next()
				.getName().equalsIgnoreCase(UserRole.ROLE_FR))
		{

			return Integer.valueOf(getDeploymentSetting()
					.get(DeploymentConstants.FR_NE_JOB_WORKING_QUEUE_DEPTH));

		} else
		{

			return 0;
		}

	}

	private LinkedHashSet<Long> getDataFromMainQueueAndPutInWorkingQueue(
			String key, int depth)
	{

		if (mainQueue.containsKey(key))
		{

			LinkedHashSet<Long> tempAreaWorkOrderList = mainQueue.get(key);

			log.debug("Key " + key
					+ " already present in mainQueue map values are "
					+ tempAreaWorkOrderList);

			List<Long> list = new ArrayList<Long>(tempAreaWorkOrderList);

			LinkedHashSet<Long> ticketNumberList = new LinkedHashSet<>(list
					.subList(0, depth));

			log.debug("Before removing " + ticketNumberList + " main queue is "
					+ tempAreaWorkOrderList);

			tempAreaWorkOrderList.removeAll(ticketNumberList);
//
//			log.debug("After removing " + ticketNumberList
//					+ " from WorkingQueue map values for key " + key + " are "
//					+ tempAreaWorkOrderList);
//
//			Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();
//
//			tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
//
//			workingQueue.putAll(tempAreaWorkOrderMapping);
//
//			log.debug("Consolidated value for key " + key
//					+ " in WorkingQueue map are " + workingQueue.get(key));

			log.debug("After removing " + ticketNumberList
					+ " from WorkingQueue map values for key " + key + " are "
					+ tempAreaWorkOrderList);

			Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();

			tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);

			workingQueue.putAll(tempAreaWorkOrderMapping);

			log.debug("Consolidated value for key " + key
					+ " in WorkingQueue map are " + workingQueue.get(key));
//
//			log.debug("After removing " + ticketNumberList
//					+ " from WorkingQueue map values for key " + key + " are "
//					+ tempAreaWorkOrderList);
//
//			Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();
//
//			tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
//
//			workingQueue.putAll(tempAreaWorkOrderMapping);
//
//			log.debug("Consolidated value for key " + key
//					+ " in WorkingQueue map are " + workingQueue.get(key));

			return ticketNumberList;

		} else
		{

			return null;

		}

	}

	private boolean removeTicketFromMainQueueAfterPuttingInWorkingQueue4Offline(
			String key, LinkedHashSet<Long> ticketNumberList)
	{

		if (mainQueue.containsKey(key))
		{

			LinkedHashSet<Long> tempAreaWorkOrderList = mainQueue.get(key);

			log.debug("Key " + key + " already present in MainQueue values are "
					+ tempAreaWorkOrderList);

			log.debug("Before removing " + ticketNumberList + " main queue is "
					+ tempAreaWorkOrderList);
			boolean result = tempAreaWorkOrderList.removeAll(ticketNumberList);

			log.debug("After removing " + ticketNumberList
					+ " from MainQueue map values for " + key + " are "
					+ tempAreaWorkOrderList);

			Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();

			tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);

			mainQueue.putAll(tempAreaWorkOrderMapping);

			log.debug("Consolidated value for key " + key
					+ " are in MainQueue map" + mainQueue.get(key));

			return result;

		} else
		{

			return false;

		}

	}

	/*
	 * ********************** Main Queue utility for Fr type Ticket
	 ***********************/

	public void addFrTicket2MainQueue(String key,
			LinkedHashSet<Long> ticketNumberList)
	{

		if (key == null)
		{
			log.info(" Null Key " + key + "No.of tickets trying to add :"
					+ ticketNumberList.size());
			return;
		}
		
		if( ticketNumberList == null || ticketNumberList.isEmpty() )
		{
			log.info("Tickets trying to add " + ticketNumberList +" with key "+key);
			return;
		}

		if (frMainQueue.containsKey(key))
		{
			frMainQueue.get(key).addAll(ticketNumberList);
			
			/*LinkedHashSet<Long> tempAreaWorkOrderList = frMainQueue.get(key);

			log.debug("Key :" + key
					+ " already present in Fr Main Queue with values are :"
					+ tempAreaWorkOrderList);

			tempAreaWorkOrderList.addAll(ticketNumberList);

			log.debug("Ater adding" + ticketNumberList
					+ " in Fr Main Queue map values for " + key + " are "
					+ tempAreaWorkOrderList);

			frMainQueue.put(key, tempAreaWorkOrderList);

			log.debug("Consolidated value for  " + key
					+ " in Main Queue map are " + frMainQueue.get(key));*/

		}
		else
		{
			log.debug("Fr Main Queue initializing first time " + key);
			
			LinkedHashSet<Long> Qdata = new LinkedHashSet<Long>(ticketNumberList);
			frMainQueue.put(key, Qdata);
			
			log.debug("Consolidated value for " + key + " in main queue are "
					+ frMainQueue.get(key));
		}
	}

	public Set<Long> getAllFrTicketFromMainQueueByKey(String key)
	{
		if (key != null && frMainQueue.containsKey(key))
		{
			log.debug("For Key " + key
					+ " already present in NE Main Queue map values size :: "
					+ frMainQueue.get(key).size());

			return frMainQueue.get(key);
		} else
		{
			log.info("Values are not available in Main Queue for Key :" + key);
			return null;
		}
	}

	public boolean removeFrTicketFromMainQueueByKeyAndTicketNumber(String key,
			Long ticketNumber)
	{

		if( ticketNumber == null || ticketNumber.longValue() <= 0 )
		{
			log.info("Can't be removed Tickets from Fr Q for key :"+key+" and tickets : "+ticketNumber);
			return false;
		}
		if ( key != null && frMainQueue.containsKey(key) )
		{
			/*LinkedHashSet<Long> tempAreaWorkOrderList = frMainQueue.get(key);

			log.debug("Key " + key
					+ " already present in Main Queue with values "
					+ tempAreaWorkOrderList);

			boolean result = tempAreaWorkOrderList.remove(ticketNumber);

			log.debug("After removing " + ticketNumber
					+ " from MainQueue map values for " + key + " are "
					+ tempAreaWorkOrderList);*/

			/*Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();
			tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
			frMainQueue.putAll(tempAreaWorkOrderMapping);*/
			
			
			/*frMainQueue.put(key, tempAreaWorkOrderList);

			log.debug("Consolidated value for key " + key
					+ " are in MainQueue map" + frMainQueue.get(key));

			return result;*/
			
			return frMainQueue.get(key).remove(ticketNumber);

		} else
		{
			log.info("Values are not present in Main Queue for Key :" + key);
			return false;
		}
	}

	public boolean removeBulkFrTicketsFromMainQueueByKey(String key,
			LinkedHashSet<Long> ticketNumberList)
	{
		if( ticketNumberList == null || ticketNumberList.isEmpty() )
		{
			log.info("Can't be removed Tickets from Fr Q for key :"+key+" and tickets : "+ticketNumberList);
			return false;
		}
		
		if (key != null && frMainQueue.containsKey(key))
		{
			/*LinkedHashSet<Long> tempAreaWorkOrderList = frMainQueue.get(key);

			log.debug("Key " + key + " already present in MainQueue values are "
					+ tempAreaWorkOrderList);

			boolean result = tempAreaWorkOrderList.removeAll(ticketNumberList);

			log.debug("After removing " + ticketNumberList
					+ " from MainQueue map values for " + key + " are "
					+ tempAreaWorkOrderList);*/

			/*Map<String, LinkedHashSet<Long>> tempAreaWorkOrderMapping = new ConcurrentHashMap<>();
			tempAreaWorkOrderMapping.put(key, tempAreaWorkOrderList);
			frMainQueue.putAll(tempAreaWorkOrderMapping);*/
			
			/*frMainQueue.put(key, tempAreaWorkOrderList);

			log.debug("Consolidated value for key " + key
					+ " are in MainQueue map" + frMainQueue.get(key));

			return result;*/
			
			return frMainQueue.get(key).removeAll(ticketNumberList);
		} else
		{
			log.info("Fr Ticket can't be removed from Q for Key : "+key);
			return false;
		}
	}

	public synchronized boolean clearMainQueue()
	{
		mainQueue.clear();

		if (mainQueue.isEmpty())
		{
			return true;

		} else
		{
			return false;
		}

	}

	public synchronized boolean clearFrMainQueue() 
	{
		frMainQueue.clear();
		if( frMainQueue.isEmpty() )
			return true;
		return false;
	}

	public String mergeFrMainQueue(Map<String, LinkedHashSet<Long>> mainFRQueue)
	{
		
		
		if (mainFRQueue != null && !mainFRQueue.isEmpty())
		{
			
			log.info("************* Merge fr main queue *********** " + mainFRQueue.size());
			
			LinkedHashSet<Long> ticketNumberList = null;
			for (Map.Entry<String, LinkedHashSet<Long>> entry : mainFRQueue.entrySet())
			{
				String key = entry.getKey();
				ticketNumberList = entry.getValue();

				addFrTicket2MainQueue(key, ticketNumberList);
			}
			
			return ResponseUtil.createSuccessResponse().getStatusMessage();
		}
		else
		{
			return ResponseUtil.createNullParameterResponse().getStatusMessage();
		}
	}
}
