package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.WorkOrder;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.fr.FrTicketVo;

@Lazy(false)
public class CRMTicketJobManager
{
	@Autowired
	private TicketDAO ticketDAO;

	@Autowired
	private WorkOrderDAO workOrderDAO;

	@Autowired
	private UserDao userDao;

	@Autowired
	private DBUtil dbUtil;

	@Autowired
	private GlobalActivities globalActivities;

	@Autowired
	private CustomerCoreService customerCoreService;

	private static Logger LOGGER = Logger.getLogger(CRMTicketJobManager.class);

	@Scheduled(cron = "${fwmp.auto.assigned.ticket.cron.triger}")
	public void executeJob()
	{
		LOGGER.debug("started auto assignment of NI ticket for the users...");
		this.doWork();
		LOGGER.debug("completed auto assignment of NI ticket for the user...");
	}

	private void doWork()
	{
		List<WorkOrder> workOrders = workOrderDAO.getAllUnassignedWorkOrder();

		

		if (workOrders != null && workOrders.size() > 0)
		{
			LOGGER.info("Total unassigned work order are " + workOrders.size());
			String queueKey = null;
			Long userTobeAssignedReportTo = null;
			Ticket ticket = null;
			Long areaId = null;

			List<Long> ticketIds = new ArrayList<Long>();

			for (Iterator<WorkOrder> iterator = workOrders.iterator(); iterator
					.hasNext();)
			{
				WorkOrder workOrder = (WorkOrder) iterator.next();
				ticketIds.add(workOrder.getTicket().getId());

			}

			LOGGER.info("Total unassigned work order for tickets " + ticketIds);

			Map<Long, String> ticketIdAndFxNameMap = customerCoreService
					.getFxNameFromTicketId(ticketIds);

			LOGGER.info("Fx for unassigned work order for tickets " + ticketIds
					+ " are " + ticketIdAndFxNameMap);

			Map<Long, String> ticketIdAndConnectionTypeMap = customerCoreService
					.getConnectionTypeFromTicketId(ticketIds);

			LOGGER.info("Total ticketIdAndConnectionTypeMap for ROI size is "
					 + ticketIdAndFxNameMap.size());
//					+ ticketIdAndConnectionTypeMap);

			Map<Long, Long> ticketIdAndSaelsExecutiveMap = customerCoreService
					.getTicketIdAndSaelsExecutiveMap(ticketIds);

			LOGGER.info("Total ticketIdAndSaelsExecutiveMap for HYD size is "
					 + ticketIdAndFxNameMap.size());
//					+ ticketIdAndSaelsExecutiveMap);
			for (WorkOrder dbvo : workOrders)
			{
				ticket = dbvo.getTicket();

				// if (ticket.getUserByAssignedTo() == null
				// || ticket.getStatus() == TicketStatus.UNASSIGNED_TICKET)
				// {
				Long ticketId = ticket.getId();
				Long assignedBy = ticketIdAndSaelsExecutiveMap.get(ticketId);

				String fxName = ticketIdAndFxNameMap.get(ticketId);
				String connectionType = ticketIdAndConnectionTypeMap
						.get(ticketId);

				UserVo userVo = null;

				if (TicketSubCategory.COPPER.equalsIgnoreCase(connectionType))
				{

					userVo = userDao
							.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

				} else if (TicketSubCategory.FIBER
						.equalsIgnoreCase(connectionType))
				{
					userVo = userDao
							.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

				}
				UserVo user = userDao
						.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);

				LOGGER.debug("############ user " + user);

				if (user != null && user.getId() > 0)
				{
					AssignVO assignVO = new AssignVO();
					assignVO.setAssignedToId(user.getId());
					assignVO.setAssignedById(FWMPConstant.SYSTEM_ENGINE);
					assignVO.setTicketId(Arrays.asList(ticket.getId()));

					if (ticket.getArea() != null)
						areaId = ticket.getArea().getId();

					ticketDAO.updateCurrentAssignedTo(ticketId, user
							.getId(), assignedBy, TicketStatus.WO_GENERATED);

					LOGGER.info("Assigned to for ticketId for ROI " + ticketId
							+ " and fxName " + fxName + " userVo is "
							+ user.getLoginId());

					queueKey = dbUtil.getKeyForQueueByUserId(user.getId());

					globalActivities
							.addTicket2MainQueue(queueKey, new LinkedHashSet<Long>(Arrays
									.asList(ticket.getId())));

					userTobeAssignedReportTo = userDao
							.getReportToUser(user.getId());

					if (userTobeAssignedReportTo != null)
					{
						queueKey = dbUtil
								.getKeyForQueueByUserId(userTobeAssignedReportTo);
						globalActivities
								.addTicket2MainQueue(queueKey, new LinkedHashSet<Long>(Arrays
										.asList(ticket.getId())));
					}
				} else
				{
					userVo = userDao
							.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);

					if (userVo != null)
					{

						AssignVO assignVO = new AssignVO();
						assignVO.setAssignedToId(user.getId());
						assignVO.setAssignedById(FWMPConstant.SYSTEM_ENGINE);
						assignVO.setTicketId(Arrays.asList(ticket.getId()));

						if (ticket.getArea() != null)
							areaId = ticket.getArea().getId();

						ticketDAO.updateCurrentAssignedTo(ticketId, user
								.getId(), assignedBy, TicketStatus.WO_GENERATED);

						LOGGER.info("Assigned to for ticketId for ROI "
								+ ticketId + " and fxName " + fxName
								+ " userVo is " + user.getLoginId());

						queueKey = dbUtil.getKeyForQueueByUserId(user.getId());

						globalActivities
								.addTicket2MainQueue(queueKey, new LinkedHashSet<Long>(Arrays
										.asList(ticket.getId())));

						userTobeAssignedReportTo = userDao
								.getReportToUser(user.getId());

						if (userTobeAssignedReportTo != null)
						{
							queueKey = dbUtil
									.getKeyForQueueByUserId(userTobeAssignedReportTo);
							globalActivities
									.addTicket2MainQueue(queueKey, new LinkedHashSet<Long>(Arrays
											.asList(ticket.getId())));
						}
					}
				}

				// }
			}
		} else
		{
			LOGGER.debug("Ticket not found...");
		}
	}

}
