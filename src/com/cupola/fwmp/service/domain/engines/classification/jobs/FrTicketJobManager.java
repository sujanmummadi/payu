package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.fr.FrTicketVo;

@Lazy(false)
public class FrTicketJobManager
{
	@Autowired
	private TicketDAO ticketDAO;
	
	@Autowired
	private FrTicketDao frTicketDao ;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private DBUtil dbUtil;
	
	@Autowired
	private GlobalActivities globalActivities;
	
	private static Logger LOGGER = Logger.getLogger(FrTicketJobManager.class);
	
	
	@Scheduled(cron = "${fwmp.auto.assigned.ticket.cron.triger}")
	public void executeJob(){
		LOGGER.debug("started auto assignment of fr ticket for the users...");
		this.doWork();
		LOGGER.debug("completed auto assignment of fr ticket for the user...");
	}
	
	private void doWork()
	{
		List<FrDetail> frTicketVo = frTicketDao.getAllUnassignedFrTicket();
		if( frTicketVo != null && frTicketVo.size() > 0)
		{
			String queueKey = null;
			Long userTobeAssignedReportTo = null;
			Long flowId = null;
			FrTicket ticket = null;
			Long areaId = null;
			
			for(FrDetail dbvo : frTicketVo)
			{
				ticket = dbvo.getTicket();
				
				if( ticket.getUserByAssignedTo() == null 
						|| ticket.getStatus() == FRStatus.FR_UNASSIGNEDT_TICKET)
				{
					flowId = dbvo.getCurrentFlowId();
					List<String> roles = AuthUtils.getUserRolesByFlowId(flowId);
					
					UserVo user = userDao.getUsersByDeviceName(ticket.getFxName(), roles);
					if( user != null)
					{
						AssignVO assignVO = new AssignVO();
						assignVO.setAssignedToId(user.getId());
						assignVO.setAssignedById(FWMPConstant.SYSTEM_ENGINE);
						assignVO.setTicketId(Arrays.asList(ticket.getId()));
						
						if(ticket.getArea() != null )
							areaId = ticket.getArea().getId();
						
						frTicketDao.assignFrTickets(assignVO, areaId);
						
						queueKey = dbUtil.getKeyForQueueByUserId(user.getId());
						globalActivities.addFrTicket2MainQueue(queueKey,
								new LinkedHashSet<Long>(Arrays.asList(ticket.getId())) );
						
						userTobeAssignedReportTo = userDao.getReportToUser(user.getId());
						if( userTobeAssignedReportTo > 0 )
						{
							queueKey = dbUtil.getKeyForQueueByUserId(userTobeAssignedReportTo);
							globalActivities.addFrTicket2MainQueue(queueKey,
									new LinkedHashSet<Long>(Arrays.asList(ticket.getId())) );
						}
					}else{
						LOGGER.debug("User not found for device ..."+ticket.getFxName());
					}
					
				}
			}
		}else{
			LOGGER.info("FR Ticket not found...");
		}
	}
	
}
