package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.math.BigDecimal;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.cupola.fwmp.constants.NamingConstants;
import com.cupola.fwmp.dao.priorityWeightage.TicketPriorityDao;
import com.cupola.fwmp.vo.TicketPriorityVo;

/**
 * 
 * @author G Ashraf
 * 
 * */

public class PriortyUpdateHelper
{
	
	private static Logger LOGGER = Logger.getLogger(PriortyUpdateHelper.class.getName());
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("com.cupola.fwmp.properties.priority");
	private static TicketPriorityDao ticketPriorityDao;
	
	private static double globalPriority = 0.0;
	private static long lastUpdatedTime = System.currentTimeMillis();
	private static boolean isGlobalPriorityUpdater = true;
	

	public static void updateGlobalPriority()
	{
		isGlobalPriorityUpdater = true;
		long currentTime = System.currentTimeMillis();
		double priority = calculatePriorityBasedOnTime(currentTime ,lastUpdatedTime);
		globalPriority = globalPriority + priority;
		lastUpdatedTime = currentTime;
		isGlobalPriorityUpdater = false;
	}
	
	public static Double getGlobalPriority()
	{
		return globalPriority;
	}
	
	//changed from private to public by manjuprasad for priority issue
	public static Double calculatePriorityBasedOnTime(long currentTime , long lastUpdatedTime)
	{
		double timeDiffe = currentTime - lastUpdatedTime;
		double noOfHours = timeDiffe / (1000 * 60 * 60);
		String priorityValueBasedOnPerHours = null;
		double calculatedPriority = 0.0;
		BigDecimal noOfPointsPerHours = null;
		try{
			if( isGlobalPriorityUpdater )
			{	
				priorityValueBasedOnPerHours = bundle.getString(NamingConstants
						.Number_Of_Points_PerHour_For_GlobalPriority);
				
				noOfPointsPerHours = new BigDecimal(priorityValueBasedOnPerHours);
				calculatedPriority = noOfHours * (noOfPointsPerHours.doubleValue());
				return calculatedPriority;
			}
			else
			{	
				priorityValueBasedOnPerHours =  bundle.getString(NamingConstants
						.Base_GlobalPriority);
				
				noOfPointsPerHours = new BigDecimal(priorityValueBasedOnPerHours);
				calculatedPriority = 24 * (noOfPointsPerHours.doubleValue());
				return calculatedPriority;
			}
			
		}catch(Exception e){
			LOGGER.error("Can't calculate priority based on time difference...");
			return 0.0;
		}
	}
	
	public static void updateBasePriorityForTickets(List<TicketPriorityVo> ticketPriorityVo)
	{
		if( ticketPriorityVo == null || ticketPriorityVo.isEmpty() )
			return;
		
		long currentTime = System.currentTimeMillis();
		long ticketCreattionTime = 0l;
		Long priorityId = null;
		Integer caculatedPriority = null;
		
		resetGlobalPriorityAndLastUpdatedTime();
		
		for(TicketPriorityVo priority : ticketPriorityVo)
		{
			ticketCreattionTime = priority.getTicketCreationDate() != null ? 
					priority.getTicketCreationDate().getTime() : 0l;
			
			priorityId = priority.getPriorityId();
			
			caculatedPriority = calculatePriorityBasedOnTime(currentTime,
					ticketCreattionTime).intValue();
			
			updatePriorityForTickets(caculatedPriority,null,null,priorityId);
		}
	}
	
	public static void updatePriorityForTickets( Integer value , Long escalationType, 
			Integer escalatedValue, Long priorityId )
	{
		if(priorityId == null)
		{
//			LOGGER.info("Can't be update priority for ticket with priority id : "+priorityId);
			return;
		}
		
		if(escalationType == null || escalatedValue == null )
			ticketPriorityDao.updateBasePriorityForTicket(value, priorityId);
		else
			ticketPriorityDao.updateEscalatedPriority(escalationType, escalatedValue, priorityId);
		
	}
	
	/* ******** provide either ticket id or priority id for particular ticket ************/
	
	public Integer getBasePriorityByTicketIdOrPriorityId(Long forTicketPriority)
	{
		return ticketPriorityDao.getBasePriorityByTicketIdOrPriorityId(forTicketPriority);
	}
	
	public Integer getEscalatedValueByTicketIdOrPriorityId(Long forTicketPriority)
	{
		return ticketPriorityDao.getEscalatedValueByTicketIdOrPriorityId(forTicketPriority);
	}
	
	private static void resetGlobalPriorityAndLastUpdatedTime()
	{
		globalPriority = 0.0; 
		isGlobalPriorityUpdater = false;
	}
	
	public static void setTicketPriorityDao(TicketPriorityDao ticketPriorityDao)
	{
		PriortyUpdateHelper.ticketPriorityDao = ticketPriorityDao;
	}
	
}
