/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.classification.servcie;

import java.util.LinkedHashSet;
import java.util.Set;

import com.cupola.fwmp.response.APIResponse;

/**
 * @author aditya
 */

public interface ClassificationEngineCoreService {

	/*APIResponse getAllWorkOrderOfAreaByAreaName(String areaId);

	APIResponse addWorkOrder2AreaTicketList(String areaId,
			LinkedHashSet<Long> workOrderList);

	APIResponse removeWorkOrderFromAreaTicketList(String areaId,
			Long workOrderNumber);
	*/

	APIResponse addTicket2MainQueue(String key,
			LinkedHashSet<Long> ticketNumberList);

	APIResponse getAllTicketFromMainQueueByKey(String key);

	APIResponse removeTicketFromMainQueueByKeyAndTicketNumber(String key,
			Long ticketNumber);

	APIResponse addTicket2WorkingQueue(String key,
			LinkedHashSet<Long> ticketNumberList,boolean flagOfManualInter);

	APIResponse getAllTicketFromWorkingQueueByKey(String key);

	APIResponse removeTicketFromWorkingQueueByKeyAndTicketNumber(String key,
			Long ticketNumberList);

	public APIResponse addFrTicket2MainQueue(String key,
			LinkedHashSet<Long> ticketNumberList);

	public APIResponse getAllFrTicketFromMainQueueByKey(String key);

	public APIResponse removeFrTicketFromMainQueueByKeyAndTicketNumber(String key,
			Long ticketNumber);

	public APIResponse removeBulkFrTicketFromMainQueueByKey(String key ,
			LinkedHashSet<Long> ticketNumberList);
	
}
