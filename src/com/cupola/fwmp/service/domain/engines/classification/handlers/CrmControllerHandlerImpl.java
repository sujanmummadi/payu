package com.cupola.fwmp.service.domain.engines.classification.handlers;

import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * @author aditya
 * 
 */
public class CrmControllerHandlerImpl implements CrmControllerHandler
{

	static private Logger log = LogManager
			.getLogger(CrmControllerHandlerImpl.class.getName());

	@Autowired
	WorkOrderCoreService workOrderCoreService;

	@Override
	public void createCustomerEntry()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void createTableEntry()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean createWorkOrderEntry(String prospectNo, String mqId,
			String workOrderNumber, String ticketCategory)
	{
		log.info("Work order to be created for prospect no " + prospectNo
				+ " and mqId " + mqId + " workOrderNumber " + workOrderNumber
				+ " ticketCategory " + ticketCategory);

		APIResponse response = workOrderCoreService
				.createWorkOrderByProspect(prospectNo, mqId, workOrderNumber, ticketCategory);

		if (response.getStatusCode() == ResponseUtil.createSuccessResponse()
				.getStatusCode())
		{
			log.info("Work order created sucessfully for Prospect no "
					+ prospectNo);
			return true;

		} else
		{
			log.info("Work order creation failed for Prospect no "
					+ prospectNo);

			return false;
			// if failed put in some queue and manage the queue
		}

	}

	@Override
	public boolean createFrDetailsEntry(MQWorkorderVO roiMQWorkorderVO,
			String deviceName)
	{
		log.info("Fr details created for prospect no. :"
				+ roiMQWorkorderVO.getProspectNumber() + " and mqId :"
				+ roiMQWorkorderVO.getMqId());

		APIResponse response = workOrderCoreService
				.createFrDetailsByProspect(roiMQWorkorderVO, deviceName);

		if (response.getStatusCode() == ResponseUtil.createSuccessResponse()
				.getStatusCode())
		{
			log.info("Fr detail created sucessfully for prospect no. : "
					+ roiMQWorkorderVO.getProspectNumber());
			return true;
		} else
		{
			log.info("Fr detail creation failed for prospect no. : "
					+ roiMQWorkorderVO.getProspectNumber());
			return false;
		}
	}

	@Override
	public Map<Long, Boolean> createWorkOrderEntryInBulk(
			List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap)
	{
		return workOrderCoreService
				.createWorkOrderInBulk(roiMQWorkorderVOList, completeProspectMap);
	}

	@Override
	public Map<Long, Boolean> createFrWorkOrderInBulk(
			List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap)
	{
		/*return workOrderCoreService
				.createFrWorkOrderInBulk(roiMQWorkorderVOList, completeProspectMap);*/
		
		return workOrderCoreService
				.createFrTicketWorkOrderInBulk(roiMQWorkorderVOList, completeProspectMap);
	}

}
