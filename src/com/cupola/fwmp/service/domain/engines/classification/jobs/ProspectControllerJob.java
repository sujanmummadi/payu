/**
 
 */
package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.integ.prospect.ProspectDAO;
import com.cupola.fwmp.dao.integ.prospect.vo.ProspectVoForClassification;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ProspectControllerCoreService;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.util.DBUtil;

/**
 * @author aditya
 */
@Lazy(false)
public class ProspectControllerJob
{
	private static Logger log = LogManager
			.getLogger(ProspectControllerJob.class.getName());

	@Autowired
	private ProspectControllerCoreService propspectControllerCoreService;

	@Autowired
	private GlobalActivities globalActivities;

	@Autowired
	private ProspectDAO prospectDAO;

	@Autowired
	private UserDao userDao;

	@Autowired
	private DBUtil dbUtil;

	private String copperUrl;

	@Autowired
	private CustomerCoreService customerCoreService;
	
	public void setCopperUrl(String copperUrl)
	{
		this.copperUrl = copperUrl;
	}
	
	public void init()
	{
		executeInternal();
	}
	

	@Scheduled(cron = "${fwmp.prospect.cron.trigger}")
	public void executeInternal()
	{
		try
		{
			log.info("ProspectControllerJob called from Cron trigger ");

			List<ProspectVoForClassification> propspectVoForClassificationList = prospectDAO
					.getAllOpenPropspects();

			if (propspectVoForClassificationList != null)
			{
				log.debug("Call prioritization Engine for "
						+ propspectVoForClassificationList);
				LinkedHashSet<Long> setValue = new LinkedHashSet<>();
				
				for (ProspectVoForClassification propspectVoForClassification : propspectVoForClassificationList)
				{
					try
					{
						log.debug("propspectVoForClassification "
								+ propspectVoForClassification);

						setValue.add(Long.valueOf(propspectVoForClassification
								.getTicketId()));

						Long assignedTo = propspectVoForClassification
								.getAssignedTo();

						if (propspectVoForClassification.getAssignedTo() <= 0)
						{
							if (propspectVoForClassification.getStatus() != FWMPConstant.TicketStatus.SALES_UNASSIGNED_TICKET)
							{
//								StatusVO status = new StatusVO();
//								status.setStatus(FWMPConstant.TicketStatus.SALES_UNASSIGNED_TICKET);
//								status.setTicketId(propspectVoForClassification
//										.getTicketId());
//								status.setPropspectNo(null);
//
//								customerCoreService.updateTicket(status);
//								 callLogger(propspectVoForClassification.getTicketId(),
//										 TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
									
							}
							setValue.clear();
							continue;

						} else
						{
							addTicket2MAinQueue(propspectVoForClassification, setValue, assignedTo);
						}

					} catch (Exception e)
					{
						log.error("Error while classifying tickets for ticket Id "
								+ propspectVoForClassification.getTicketId()
								+ ". Message is " + e.getMessage());
						e.printStackTrace();
					}
					setValue.clear();
				}
			} else
			{
				log.info("PropspectVoForClassificationList does not found "
						+ propspectVoForClassificationList);

			}

			/*log.debug("Main queue for prospect is "
					+ globalActivities.getMainQueue());*/

		} catch (Exception e)
		{
			log.error("Error while classifying prospect tickets "
					+ e.getMessage());
			e.printStackTrace();
		}

	}

	private void callLogger(long ticketId, String ticketCategory)
	{
		TicketUpdateGateway
				.logTicket(new TicketLogImpl(ticketId, ticketCategory, FWMPConstant.SYSTEM_ENGINE));

	}

	private void addTicket2MAinQueue(
			ProspectVoForClassification propspectVoForClassification,
			LinkedHashSet<Long> setValue, Long assignedTo)
	{
		log.debug("Ticket " + propspectVoForClassification.getTicketId()
				+ " is  assigned to" + assignedTo + " with values " + setValue
				+ " propspectVoForClassification "
				 );

		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

		propspectControllerCoreService.addTicket2MainQueue(neKey, setValue);

		long reportToId = userDao.getReportToUser(assignedTo);

		if (reportToId == 0)
		{
			log.info("No Report to found for user " + assignedTo);
			return;
		}

		String tlKey = dbUtil.getKeyForQueueByUserId(reportToId);

		propspectControllerCoreService.addTicket2MainQueue(tlKey, setValue);
//		 callLogger(propspectVoForClassification.getTicketId(),
//		 TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
	}

}
