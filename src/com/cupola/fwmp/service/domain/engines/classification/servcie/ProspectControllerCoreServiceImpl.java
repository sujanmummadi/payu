/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.classification.servcie;

import java.util.LinkedHashSet;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * @author aditya
 *
 */
public class ProspectControllerCoreServiceImpl implements
ProspectControllerCoreService {


	@Autowired
	GlobalActivities globalActivities;

	@Override
	public APIResponse getAllTicketFromMainQueueByKey(String key)
	{

		return ResponseUtil
				.createSuccessResponse()
				.setData(globalActivities.getAllTicketFromMainQueueByKey(key));
	}

	@Override
	public APIResponse addTicket2MainQueue(String key,
			LinkedHashSet<Long> ticketNumberList)
	{

		globalActivities.addTicket2MainQueue(key, ticketNumberList);

		return ResponseUtil.createSuccessResponse();
	}

	@Override
	public APIResponse removeTicketFromMainQueueByKeyAndTicketNumber(
			String key, Long ticketNumberList)
	{

//		if (globalActivities
//				.removeTicketFromMainQueueByKeyAndTicketNumber(key, ticketNumberList))
//		{
//
//			return ResponseUtil.createSuccessResponse();
//
//		} else
//		{
//
//			return ResponseUtil.createRecordNotFoundResponse();
//
//		}
		
		return null;
	}

	@Override
	public APIResponse addTicket2WorkingQueue(String key,
			LinkedHashSet<Long> ticketNumberList, boolean flagOfManualInter)
	{

		globalActivities
				.addTicket2WorkingQueue(key, ticketNumberList, flagOfManualInter);

		return ResponseUtil.createSuccessResponse();
	}

	@Override
	public APIResponse getAllTicketFromWorkingQueueByKey(String key)
	{

		return ResponseUtil
				.createSuccessResponse()
				.setData(globalActivities.getAllTicketFromWorkingQueueByKey(key));
	}

	@Override
	public APIResponse removeTicketFromWorkingQueueByKeyAndTicketNumber(
			String key, Long ticketNumberList)
	{

		if (globalActivities
				.removeTicketFromWorkingQueueByKeyAndTicketNumber(key, ticketNumberList))
		{

			return ResponseUtil.createSuccessResponse();

		} else
		{

			return ResponseUtil.createRecordNotFoundResponse();

		}
	}

}
