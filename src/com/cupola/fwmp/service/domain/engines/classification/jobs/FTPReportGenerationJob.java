package com.cupola.fwmp.service.domain.engines.classification.jobs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.service.reports.caf.CafReportService;
import com.cupola.fwmp.service.reports.closedcomplaints.ComplaintsReportService;
import com.cupola.fwmp.service.reports.feasibility.FeasibilityReportService;
import com.cupola.fwmp.service.reports.lightweight.LightWeightReportService;
import com.cupola.fwmp.service.reports.material.MaterialReportService;
import com.cupola.fwmp.service.reports.reassignment.ReAssignmentReportService;
import com.cupola.fwmp.service.reports.reopen.ReOpenReportService;
import com.cupola.fwmp.service.reports.sales.SalesReportService;
import com.cupola.fwmp.service.reports.workorder.WorkOrderReportService;

/**
 * created by kiran on 03/23/2017
 * 
 * @author kiran
 *
 */
public class FTPReportGenerationJob
{

	private static Logger log = LogManager
			.getLogger(FTPReportGenerationJob.class.getName());

	@Autowired
	CafReportService cafReport;

	@Autowired
	FeasibilityReportService feasibilityReport;

	@Autowired
	MaterialReportService materialReport;

	@Autowired
	SalesReportService salesReport;

	@Autowired
	WorkOrderReportService workOrderReport;

	@Scheduled(cron = "${fwmp.auto.report.generator.cron.triger}")
	public void executeJobReport()
	{
		log.info("Auto report generation started");

		log.info("feasibilityReport Report started");
		feasibilityReport();
		log.info("feasibilityReport Report generated");

		log.info("Sales dcr Report started");
		salesReport();
		log.info("Sales dcr Report generated");

		log.info("CAF Report started");
		cafReport();

		log.info("Material Report started");
		materialReport();
		log.info("Material Report generated");

		log.info("Work order Report started");
		workOrderReport();
		log.info("Work order Report generated");
		
		log.info("Auto report generation completed");
	}

	private void workOrderReport()
	{
		workOrderReport.getAutoWorkOrderReports();
	}

	private void salesReport()
	{
		salesReport.getAutosalesDcrReports();

	}

	private void materialReport()
	{
		materialReport.getAutoMaterialReports();

	}

	private void feasibilityReport()
	{
		feasibilityReport.getAutoFeasibilityReports();

	}

	private void cafReport()
	{
		cafReport.getAutoCafReports();
	}
}
