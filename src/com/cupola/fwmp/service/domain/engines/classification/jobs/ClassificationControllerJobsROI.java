package com.cupola.fwmp.service.domain.engines.classification.jobs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author aditya
 *
 */
public class ClassificationControllerJobsROI extends QuartzJobBean {

	private static Logger log = LogManager
			.getLogger(ClassificationControllerJobsROI.class.getName());

	// private ClassificationControllerHandler classificationControllerHandler;
	//
	//
	// public void setClassificationControllerHandler(
	// ClassificationControllerHandler classificationControllerHandler) {
	// this.classificationControllerHandler = classificationControllerHandler;
	// }

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {

		// classificationControllerHandler.printAnotherMessage();

		log.info("ClassificationControllerJobsROI called from Cron trigger");

	}

}
