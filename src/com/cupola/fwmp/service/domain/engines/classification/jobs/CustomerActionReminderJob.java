package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.service.closeticket.MQSCloseTicketService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.TicketCustomerDetailsVO;

@Lazy(false)
public class CustomerActionReminderJob {

	private static Logger log = LogManager
			.getLogger(CustomerActionReminderJob.class.getName());
	@Autowired
	TicketDAO ticketDAO;
	@Autowired
	NotificationService notificationService;
	@Autowired
	MQSCloseTicketService closeTicketService;
	@Autowired
	DefinitionCoreService coreService;

	public static Map<Long, MessageData> ticketIdAndCountCache = 
			new ConcurrentHashMap<Long, MessageData>();

	@Scheduled(cron = "${fwmp.customer.notification.controller.cron.trigger}")
	protected void executeInternal() {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());

		int currentHour = calendar.get(Calendar.HOUR_OF_DAY);

		int currentDay = calendar.get(Calendar.DAY_OF_WEEK);

		if (currentDay != 1) {

			if (currentHour >= TicketStatus.START_TIME_OF_WORKING_DAY_IN_HOURS
					&& currentHour <= TicketStatus.END_TIME_OF_WORKING_DAY_IN_HOURS) {

				List<TicketCustomerDetailsVO> ticketCustomerDetails = ticketDAO
						.getTicketsForCustomerAutoNotification();

				log.debug("ticketCustomerDetail @@@@@@@@@@@@@@@@@ "
						+ ticketCustomerDetails);

				if (ticketCustomerDetails == null
						|| ticketCustomerDetails.isEmpty()) {
					log.debug("NO tickets found");
				}

				else {

					String statusMessageCode = coreService
							.getNotificationMessageFromProperties(String
									.valueOf(TicketStatus.PORT_ACTIVATION_SUCCESSFULL));

					String messageToSend = coreService
							.getMessagesForCustomerNotificationFromProperties(statusMessageCode);

					for (TicketCustomerDetailsVO ticketCustomerDetail : ticketCustomerDetails) {
						
						if(messageToSend == null)
							{
							log.info("No Message found for "+statusMessageCode);
							continue;
							}
						
						if (ticketIdAndCountCache
								.containsKey(ticketCustomerDetail.getTicketId())) {

							log.debug("ticketIdAndCountCache contains key ========= "
									+ ticketIdAndCountCache);

							MessageData data = ticketIdAndCountCache
									.get(ticketCustomerDetail.getTicketId());

							if (data.count < TicketStatus.NO_OF_TIMES_THE_MESSAGE_SHOULD_BE_SENT) {
								long diff = data.lastSend
										- ticketCustomerDetail.getModifiedOn()
												.getTime();
								long diffHours = diff / (60 * 60 * 1000);
								// long diffMinutes = diff / (60 * 1000) % 60;

								if (diffHours > TicketStatus.DIFF_BETWEEN_TICKET_TIME_AND_LAST_SEND_IN_HOURS) {
									log.debug("COUNT IS LESS THAN 2 "
											+ data.count
											+ " and diffHours is ========= "
											+ diffHours);
									// UPDATE IN CACHE
									data.count++;
									data.lastSend = new Date().getTime();
									ticketIdAndCountCache.put(
											ticketCustomerDetail.getTicketId(),
											data);

									// send notification to customer
									MessageNotificationVo notificationVo = new MessageNotificationVo();
									notificationVo.setMessage(messageToSend); // read
																				// from
																				// property
																				// file

									DateFormat df = new SimpleDateFormat(
											"MM/dd/yyyy HH:mm:ss");

									Date today = Calendar.getInstance()
											.getTime();
									String todayDate = df.format(today);

									if (messageToSend.contains("_DATE_"))
										messageToSend = messageToSend
												.replaceAll("_DATE_", todayDate);

									notificationVo
											.setMobileNumber(ticketCustomerDetail
													.getMobileNumber());
									notificationVo
											.setEmail(ticketCustomerDetail
													.getCustomerEmail());
									notificationVo
											.setSubject("ACT Notification"); // read
																				// from
																				// property
																				// file
									notificationService
											.sendNotification(notificationVo);
								}
							} else {
								log.debug("COUNT IS GREATER THAN 2 "
										+ data.count);
								// count > 2. SO close the ticket
								TicketClosureDataVo closeTicketVo = new TicketClosureDataVo();
								closeTicketVo
										.setWorkOrderNo(ticketCustomerDetail
												.getWorkOrderNumber());
								closeTicketService.closeTicket(closeTicketVo);

								// remove from cache
								ticketIdAndCountCache
										.remove(ticketCustomerDetail
												.getTicketId());
							}
						}

						else {

							// sending notification to customer first time
							MessageNotificationVo notificationVo = new MessageNotificationVo();
							notificationVo.setMessage(messageToSend); // read
																		// from
																		// property
																		// file

							DateFormat df = new SimpleDateFormat(
									"MM/dd/yyyy HH:mm:ss");

							Date today = Calendar.getInstance().getTime();
							String todayDate = df.format(today);

							if (messageToSend.contains("_DATE_"))
								messageToSend = messageToSend.replaceAll(
										"_DATE_", todayDate);

							notificationVo.setMobileNumber(ticketCustomerDetail
									.getMobileNumber());
							notificationVo.setEmail(ticketCustomerDetail
									.getCustomerEmail());
							notificationVo.setSubject("ACT Notification"); // read
																			// from
																			// property
																			// file
							notificationService
									.sendNotification(notificationVo);

							// ADD IN CACHE
							MessageData data = new MessageData();
							data.count = 1;
							data.lastSend = new Date().getTime();
							log.debug("message data ===== " + data);
							ticketIdAndCountCache.put(
									ticketCustomerDetail.getTicketId(), data);
						}
					}
				}

			}

			else {

				log.info("Current time is greater or lesser than daily working hours");
			}

		} else {

			log.info("Current day is Sunday");
		}
	}

	class MessageData {
		int count;
		Long lastSend;

		@Override
		public String toString() {
			return "MessageData [count=" + count + ", lastSend=" + lastSend
					+ "]";
		}
	}

	/*public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 20);

		int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
		int currentDay = calendar.get(Calendar.DAY_OF_WEEK);

		log.debug(currentHour + " " + currentDay
				+ calendar.get(Calendar.MINUTE));
	}*/
}
