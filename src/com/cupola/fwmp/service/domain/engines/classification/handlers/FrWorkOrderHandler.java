package com.cupola.fwmp.service.domain.engines.classification.handlers;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;

public class FrWorkOrderHandler
{

	private static final Logger logger = Logger.getLogger(FrWorkOrderHandler.class);
	
	
	@Autowired
	private FrTicketService frService;
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	private BlockingQueue<List<FrTicketEtrDataVo>> ticketIds = null;
	private Thread EtrCalculator = null;
	
	public void init()
	{
		logger.info("enabling Etr calculator....");
		ticketIds = new LinkedBlockingQueue<List<FrTicketEtrDataVo>>(100000);
		
		if ( EtrCalculator == null)
		{
			EtrCalculator = new Thread( calculator, "MQClosure");
			EtrCalculator.start();
		}
		
		logger.info("Etr calculator enabled ...");
	}
	
	Runnable calculator = new Runnable()
	{
		@Override
		public void run()
		{
			try
			{
				ExecutorService executor = Executors.newFixedThreadPool(25);
				while (true)
				{
					try
					{
						final List<FrTicketEtrDataVo> workOrder  = ticketIds.take();
						Runnable independClosure = new Runnable()
						{
							@Override
							public void run()
							{ 
								try
								{
									if( workOrder != null )
									{
										logger.info("Thread : "+Thread.currentThread().getName() 
												+" calculating etr of  work order");
//										Thread.sleep(5000);
										calculateEtr( workOrder );
									}
								}
								catch (Exception e) 
								{
									logger.error("Error occured While calculating etr of work order : "+e.getMessage()
											,e );
									e.printStackTrace();
								}
							}
						};
						
						executor.execute( independClosure );
					} 
					catch (Exception e)
					{
						logger.error("*******Error occured While calculating etr of workOrder******* : "
								+ e.getMessage(),e);
						continue;
					}
				}
			} 
			catch (Exception e)
			{
				logger.error("error in EtrCalculator main loop- msg="
						+ e.getMessage());
			}
			logger.info("EtrCalculator done creation...");
		}
		
		private void calculateEtr( List<FrTicketEtrDataVo> resolutionCode)
		{
			frService.createFrTicketEtrInBulk( resolutionCode );
		}
	};
	
	public void calculateFrTicketEtr( List<FrTicketEtrDataVo> etrDataVaues )
	{
		if( etrDataVaues == null || etrDataVaues.isEmpty() )
			return;
		
		try
		{
			
			ticketIds.put(etrDataVaues);
			
		}
		catch (Exception e)
		{
			logger.error("error adding in Etr  Q " + e.getMessage(),e);
		}
	}

}
