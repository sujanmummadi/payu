package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FWMPConstant.MessageType;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.TicketCustomerDetailsVO;
@Lazy(false)
public class CustomerActionReminderOnETRUpdateJob {

	private static Logger log = LogManager
			.getLogger(CustomerActionReminderOnETRUpdateJob.class.getName());
	@Autowired
	TicketDAO ticketDAO;
	@Autowired
	NotificationService notificationService;
	@Autowired
	DefinitionCoreService coreService;

	public static Map<Long, MessageData> ticketIdAndCountCache = 
			new ConcurrentHashMap<Long, MessageData>();

	@Scheduled(cron = "${fwmp.customer.notification.controller.on.etr.update.cron.trigger}")
	public void executeInternal() {

		//performAction();
	}

	/**
	 * 
	 */
	private void performAction()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());

		List<TicketCustomerDetailsVO> customerTicketDetails = ticketDAO
				.getTicketsForCustomerAutoNotificationOnETRUpdate();

		log.debug("ticketCustomerDetail @@@@@@@@@@@@@@@@@ "
				+ customerTicketDetails);

		if (customerTicketDetails == null || customerTicketDetails.isEmpty()) {
			log.info("NO tickets found");
		}

		else {

			String statusMessageCode = coreService
					.getNotificationMessageFromProperties(String
							.valueOf(TicketStatus.ETR_UPDATE_AUTO_MODE));

			String messageToSend = coreService
					.getMessagesForCustomerNotificationFromProperties(statusMessageCode);

			for (TicketCustomerDetailsVO ticketCustomerDetail : customerTicketDetails) {

				long diffBetweenCurrentAndCommitted = ((ticketCustomerDetail
						.getCommitedEtr().getTime() - new Date().getTime()));
				long diffHours = diffBetweenCurrentAndCommitted
						/ (60 * 60 * 1000);

				int diffBetweenCurrentTimeAndCommittedETRInHours = MessageType.DIFF_BETWEEN_CURRENT_TIME_AND_COMMITTED_ETR;

				if (diffHours > 0
						&& diffHours <= diffBetweenCurrentTimeAndCommittedETRInHours) {

					if (ticketIdAndCountCache.containsKey(ticketCustomerDetail
							.getTicketId())) {

						MessageData data = ticketIdAndCountCache
								.get(ticketCustomerDetail.getTicketId());

						log.debug("ticketIdAndCountCache =========== " + data);
						if (data.count < MessageType.NO_OF_TIMES_THE_MESSAGE_SHOULD_BE_SENT) {

							long diff = new Date().getTime() - data.lastSend;
							long diffInHours = diff / (60 * 60 * 1000);
							// long diffMinutes = diff / (60 * 1000) % 60;

							if (diffInHours >= MessageType.TIME_INTERVAL_BETWEEN_CURRENT_AND_LAST_SEND_IN_HOUR) {
								log.debug("COUNT IS LESS THAN 2 " + data.count
										+ " and diffHours is ========= "
										+ diffInHours);

								// UPDATE IN CACHE
								data.count++;
								data.lastSend = new Date().getTime();
								ticketIdAndCountCache.put(
										ticketCustomerDetail.getTicketId(),
										data);

								// send updated ETR to customer

								MessageNotificationVo notificationVo = new MessageNotificationVo();
								if (messageToSend.contains("_DATE_")) {

									Date currentEtr = ticketCustomerDetail
											.getCurrentEtr();
									String etrUpdateDate = GenericUtil.convertToUiDateFormat( currentEtr);

									messageToSend = messageToSend.replaceAll(
											"_DATE_", etrUpdateDate);
								}
								if (messageToSend.contains("_MQID_"))
									messageToSend = messageToSend.replaceAll("_MQID_", ticketCustomerDetail.getMqId());
							
								
								notificationVo.setMessage(messageToSend);
								notificationVo
										.setMobileNumber(ticketCustomerDetail
												.getMobileNumber());
								notificationVo.setEmail(ticketCustomerDetail
										.getCustomerEmail());
								notificationVo.setSubject("ACT Notification"); // read
																				// from
																				// property
																				// file
								// notificationService.sendNotification(notificationVo);

							}
						}

						else {
							log.info("COUNT IS GREATER THAN 2 " + data.count);
							// count > 2. SO Don't send updated ETR again.

							// remove from cache
							ticketIdAndCountCache.remove(ticketCustomerDetail
									.getTicketId());
						}

					} else {

						// send updated ETR to customer first time

						log.debug("SENDING ETR FIRST TIME=======================");
						MessageNotificationVo notificationVo = new MessageNotificationVo();
						if (messageToSend.contains("_DATE_")) {

							DateFormat df = new SimpleDateFormat(
									"MM/dd/yyyy HH:mm:ss");
							Date currentEtr = ticketCustomerDetail
									.getCurrentEtr();
							String etrUpdateDate = df.format(currentEtr);

							messageToSend = messageToSend.replaceAll("_DATE_",
									etrUpdateDate);
						}
						notificationVo.setMessage(messageToSend);
						notificationVo.setMobileNumber(ticketCustomerDetail
								.getMobileNumber());
						notificationVo.setEmail(ticketCustomerDetail
								.getCustomerEmail());
						notificationVo.setSubject("ACT Notification"); // read
																		// from
																		// property
																		// file

						log.debug("notificationVo contains data >>>>>>>>>>>>>>>>>>>> "
								+ notificationVo);
						// notificationService.sendNotification(notificationVo);

						// ADD IN CACHE
						MessageData data = new MessageData();
						data.count = 1;
						data.lastSend = new Date().getTime();
						log.debug("message data ===== " + data);
						ticketIdAndCountCache.put(
								ticketCustomerDetail.getTicketId(), data);
					}
				}

				else {

					log.info("Difference between current time and committed ETR is less than zero OR greater than "
							+ diffBetweenCurrentTimeAndCommittedETRInHours
							+ " .Difference is ==== " + diffHours);
				}
			}
		}
	}

	class MessageData {

		int count;
		Long lastSend;

		@Override
		public String toString() {
			return "MessageData [count=" + count + ", lastSend=" + lastSend
					+ "]";
		}
	}

//	public static void main(String[] args) {
//		Calendar calendar = Calendar.getInstance();
//		calendar.set(Calendar.DAY_OF_MONTH, 20);
//
//		int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
//		int currentDay = calendar.get(Calendar.DAY_OF_WEEK);
//
//		log.debug(currentHour + " " + currentDay
//				+ calendar.get(Calendar.MINUTE));
//	}
}
