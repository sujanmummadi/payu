package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 
 * @author G Ashraf
 * 
 * */
@Lazy(false)
public class PriorityUpdaterJobByHours
{

	private Logger LOGGER = Logger.getLogger(PriorityUpdaterJobByHours.class);
	
	ResourceBundle rb = ResourceBundle
			.getBundle("com.cupola.fwmp.properties.priority");
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Scheduled(cron = "${fwmp.priority.hourly.cron.trigger}")
	public void executeInternal()
	{
		LOGGER.info("************************************************");
		LOGGER.info("************* PRIOIRTY PER HOURS ***************");
		LOGGER.info("************************************************");
		PriortyUpdateHelper.updateGlobalPriority();
		
	}

}
