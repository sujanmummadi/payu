/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.customernotification.CustomerNotificationDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.persistance.entities.CustomerNotification;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.WorkOrder;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculatorImpl;
import com.cupola.fwmp.util.ETRCalculatorUtil;
import com.cupola.fwmp.vo.CustomerNotificationVO;

/**
 * 
 * @author kiran
 * 
 */
public class DynamicETRCalculatorJob
{
	private static Logger log = LogManager
			.getLogger(DynamicETRCalculatorJob.class.getName());

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	ETRCalculatorUtil etrCalculatorUtil;

	@Autowired
	CustomerNotificationDAO customerNotificationDAO;
	
	@Autowired
	EtrCalculator etrCalculator;

	@Scheduled(cron = "${fwmp.dynamic.etr.calculation.cron.trigger}")
	public void executeInternal()
	{

		dynamicETRCalculator();
	}

	/**
	 * get All tickets with not completed status
	 */
	private void dynamicETRCalculator()
	{
		List<Ticket> tickets = ticketDAO.getAllNotCompletedDeployedTickets();
		if (tickets != null)
		{
			for (Ticket ticket : tickets)
			{
				Set<WorkOrder> workOrderDetails = ticket.getWorkOrders();
				for (WorkOrder workOrder : workOrderDetails)
				{
					if (workOrder.getWorkStageByCurrentWorkStage() != null)
					{
						if (workOrder.getWorkStageByCurrentWorkStage().getId() == WorkStageType.COPPER)
						{
							calculateAndUpdateCommunicationETR(ticket, workOrder , WorkStageType.COPPER);
						}
						if (workOrder.getWorkStageByCurrentWorkStage().getId() == WorkStageType.FIBER)
						{
							calculateAndUpdateCommunicationETR(ticket, workOrder , WorkStageType.FIBER);
						}
					} else
					{
						log.info("CurrentWorkStage in WorkOrder not available with ticketId : "
								+ ticket.getId());
					}

				}
			}

		}
	}

	/**
	 * calculate communication etr on the basics of fiber/copper and update
	 * communicationETR
	 * 
	 * @param ticket
	 * @param workOrder
	 * @param type 
	 */
	private void calculateAndUpdateCommunicationETR(Ticket ticket,
			WorkOrder workOrder, long type)
	{
		if (workOrder.getCurrentProgress() != null || workOrder.getCurrentProgress() != 0)
		{
			if (workOrder.getCurrentProgress() >= 70)
			{
				Date communicationEtr = etrCalculatorUtil
						.calculateETRInWorkingHours(new Date(), 1, "days");
				log.info("updating communication Etr : " + communicationEtr);
				int result = ticketDAO
						.updateCommunicationETR(ticket.getId(), communicationEtr);
				if (result > 0)
				{
					log.info("ETR updated Succesfully");
					saveInCustomerNotification(ticket.getId());
				}

			}
			else {
				if(type == WorkStageType.COPPER)
				{
					calculateRemainingETRAndUpdateCommunicationETR(workOrder.getCurrentProgress() , EtrCalculatorImpl.etrDetails.get(WorkStageName.COPPER) , ticket);
				}
				if (type == WorkStageType.FIBER)
				{
					calculateRemainingETRAndUpdateCommunicationETR(workOrder.getCurrentProgress() , EtrCalculatorImpl.etrDetails.get(WorkStageName.FIBER), ticket);
				}
			}
		}

		else if (workOrder.getCurrentProgress() == null
					|| workOrder.getCurrentProgress() == 0)
			{
				if(type == WorkStageType.COPPER)
				{
					calculateRemainingETRAndUpdateCommunicationETR(workOrder.getCurrentProgress() , EtrCalculatorImpl.etrDetails.get(WorkStageName.COPPER) , ticket);
				}
				if (type == WorkStageType.FIBER)
				{
					calculateRemainingETRAndUpdateCommunicationETR(workOrder.getCurrentProgress() , EtrCalculatorImpl.etrDetails.get(WorkStageName.FIBER), ticket);
				}

			}

		

	}

	private void calculateRemainingETRAndUpdateCommunicationETR(
			Long currentProgress, String defaltETr, Ticket ticket)
	{
		int valueOf = Integer.valueOf(defaltETr.replace("days", "").trim());
		if(currentProgress == null || currentProgress == 0 )
		{
			
			Date communicationEtr = etrCalculatorUtil
					.calculateETRInWorkingHours(ticket.getCommunicationETR(),valueOf , "days");
			int result = ticketDAO
					.updateCommunicationETR(ticket.getId(), communicationEtr);
			if (result > 0)
			{
				log.info("ETR updated Succesfully");
			}
		}
		else {
			int defaltEtr = valueOf * 10;
			long remainingProgress = 100/currentProgress;
			int newDefaltETR = (int) (defaltEtr/remainingProgress);
			Date communicationEtr = etrCalculatorUtil
					.calculateETRInWorkingHours(ticket.getCommunicationETR(),newDefaltETR , "hours");
			int result = ticketDAO
					.updateCommunicationETR(ticket.getId(), communicationEtr);
			if (result > 0)
			{
				log.info("ETR updated Succesfully");
			}
		}
		
	}

	/**
	 * Insert a record in {@link CustomerNotification} to send an notification
	 * to customer
	 * 
	 * @param id
	 */
	private void saveInCustomerNotification(long id)
	{
		CustomerNotificationVO customerNotificationVO = new CustomerNotificationVO();
		customerNotificationVO.setTicketId(id);
		customerNotificationVO.setStatus(TicketStatus.OPEN);
		CustomerNotification customerNotification = customerNotificationDAO
				.add(customerNotificationVO);
		if (customerNotification != null)
			log.info("CustomerNotification inserted Succesfully");

	}

}
