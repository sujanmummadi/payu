/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.classification.controllers;

/**
 * @author aditya
 *
 */
public interface ClassificationController {
	
	void getAllOpenWorkOrderTicket();
	void getAllOpenProspectTicket();
	void preparFrQueueForUsers();
}
