/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.classification.servcie;

import com.cupola.fwmp.dao.integ.prospect.pojo.MQProspectRequest;
import com.cupola.fwmp.response.APIResponse;

/**
 * @author aditya
 *
 */
public interface CrmControllerCoreServiceHYD {

	APIResponse createProspectInCRM(MQProspectRequest requestInfo);
	
}
