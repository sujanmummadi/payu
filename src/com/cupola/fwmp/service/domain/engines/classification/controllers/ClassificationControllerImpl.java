package com.cupola.fwmp.service.domain.engines.classification.controllers;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.integ.prospect.ProspectDAO;
import com.cupola.fwmp.dao.integ.prospect.vo.ProspectVoForClassification;
import com.cupola.fwmp.dao.mongo.ticket.MongoTicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.customer.ProspectCreatorHelper;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ClassificationEngineCoreServiceImpl;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ProspectControllerCoreService;
import com.cupola.fwmp.service.fr.FrTicketLockHelper;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.vo.ProspectCoreVO;

public class ClassificationControllerImpl implements ClassificationController
{
	private static Logger log = LogManager
			.getLogger(ClassificationControllerImpl.class.getName());

	@Autowired
	ClassificationEngineCoreServiceImpl classificationEngineCoreServiceImpl;

	@Autowired
	ProspectControllerCoreService propspectControllerCoreService;

	@Autowired
	UserDao userDao;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	WorkOrderDAO workOrderDAO;
	@Autowired
	GlobalActivities globalActivities;
	@Autowired
	ProspectDAO prospectDAO;

	@Autowired
	CustomerCoreService customerCoreService;

	@Autowired
	private FrTicketLockHelper frHelper;

	@Autowired
	ProspectCreatorHelper prospectCreatorHelper;

	@Autowired
	MongoTicketDAO mongoTicketDAO;

	public void init()
	{

		log.info("ClassificationControllerImpl init method called..");

		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				getAllOpenWorkOrderTicket();
			}
		}).start();

		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				getAllOpenProspectTicket();
			}
		}).start();

		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				preparFrQueueForUsers();
				// log.info("*************************************************************************");
			}
		}).start();

		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				getAllPendingProspects();
			}
		}).start();

		log.info("ClassificationControllerImpl init method executed..");
	}

	Map<String, LinkedHashSet<Long>> userTicketMap = new ConcurrentHashMap<String, LinkedHashSet<Long>>();

	private void addTicketToUserQueue(Long ticketId, String key)
	{
		if (key == null || key.isEmpty() || ticketId == null)
			return;
		else
		{
			if (userTicketMap.containsKey(key))
			{
				LinkedHashSet<Long> inMap = userTicketMap.get(key);
				inMap.add(ticketId);
				userTicketMap.put(key, inMap);
			} else
			{
				LinkedHashSet<Long> listOfTickets = new LinkedHashSet<Long>();
				listOfTickets.add(ticketId);
				userTicketMap.put(key, listOfTickets);
			}

		}
	}

	/*
	 * private void makeDefaultUnlockTicketForUser(Long ticketId, Long
	 * assignedTo, boolean isBlocked, boolean isLocked) { FrTicketStateVo state
	 * = null; state = new FrTicketStateVo(); state.setBlocked(isBlocked);
	 * state.setLocked(isLocked); state.setTicketId(ticketId);
	 * state.setUserId(assignedTo); frUtil.createMap(state); }
	 */

	@Override
	@Scheduled(cron = "${fwmp.refresh.fr.queue.cron.trigger}")
	public void preparFrQueueForUsers()
	{
		try {
			log.info("Preparing Fr User Queue server startup...");

			List<MQWorkorderVO> hydMQWorkorderVOList = workOrderDAO
					.getAllOpenFrTicket();
			
			LinkedHashSet<Long> setValue = new LinkedHashSet<>();
			Set<Long> usrIds = new TreeSet<Long>();

			for (MQWorkorderVO hydMQWorkorderVO : hydMQWorkorderVOList)
			{
				try {
					Long ticketId = Long.valueOf(hydMQWorkorderVO.getWorkOrderNumber());
					setValue.add(ticketId);
					
					long assignedTo = hydMQWorkorderVO.getAssignedTo();
					if( assignedTo <= 0 )
					{
						setValue.clear();
						continue;
					}

					String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);
					classificationEngineCoreServiceImpl
						.addFrTicket2MainQueue(neKey, setValue);
					
					usrIds.add(assignedTo);
					
					/*makeDefaultUnlockTicketForUser(ticketId,assignedTo,hydMQWorkorderVO.isBlocked()
							,hydMQWorkorderVO.isLocked());*/
					
					if( !frHelper.isFRNEUser(assignedTo) )
					{
						setValue.clear();
						continue;
					}

					long reportToId = userDao.getReportToUser(assignedTo);
					if (reportToId == 0)
					{
						log.debug("No Report to found for user " + assignedTo);
						setValue.clear();
						continue;
					}

					String tlKey = dbUtil.getKeyForQueueByUserId(reportToId);
					classificationEngineCoreServiceImpl
						.addFrTicket2MainQueue(tlKey, setValue);
					
					setValue.clear();
				} catch (Exception e) {
					log.error("Error occured while preparing FR Q",e);
					e.printStackTrace();
					continue;
				}

			}
			frHelper.manageBulkUserLock(usrIds);
		} catch (Exception e) {
			log.error("Error occured while fetching TT OF TL AND NE",e);
			e.printStackTrace();
		}
	}

	@Override
	@Scheduled(cron = "${fwmp.refresh.deployment.queue.cron.trigger}")
	public void getAllOpenWorkOrderTicket()
	{
		try
		{
			log.info("WorkOrder Controller Job called On server startup");

			List<MQWorkorderVO> hydMQWorkorderVOList = workOrderDAO
					.getAllOpenTicket();

			log.debug("Call prioritization Engine on WorkOrder Controller Job called On server startup"
					+ hydMQWorkorderVOList);

			LinkedHashSet<Long> setValue = new LinkedHashSet<>();

			for (MQWorkorderVO hydMQWorkorderVO : hydMQWorkorderVOList)
			{
				log.debug("WorkOrder Controller Job called On server startup for "
						+ hydMQWorkorderVO.getProspectNumber());

				setValue.add(Long
						.valueOf(hydMQWorkorderVO.getWorkOrderNumber()));

				long assignedTo = hydMQWorkorderVO.getAssignedTo();

				// if (assignedTo <= 0)
				// {
				// StatusVO status = new StatusVO();
				// status.setStatus(FWMPConstant.TicketStatus.UNASSIGNED_TICKET);
				// status.setPropspectNo(hydMQWorkorderVO.getProspectNumber());
				// customerCoreService.updateTicket(status);
				// setValue.clear();
				// continue;
				// }

				String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

				classificationEngineCoreServiceImpl
						.addTicket2MainQueue(neKey, setValue);

				long reportToId = userDao.getReportToUser(assignedTo);

				if (reportToId == 0)
				{
					log.debug("No Report to found for user " + assignedTo);
					setValue.clear();
					continue;
				}

				String tlKey = dbUtil.getKeyForQueueByUserId(reportToId);

				propspectControllerCoreService
						.addTicket2MainQueue(tlKey, setValue);

				setValue.clear();
			}

			// log.debug("Main queue for WorkOrder Controller Job On server
			// startup is "
			// + globalActivities.getMainQueue());

		} catch (Exception e)
		{
			log.error("Error in  WorkOrder Controller Job On server startup "
					+ e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	@Scheduled(cron = "${fwmp.refresh.sales.queue.cron.trigger}")
	public void getAllOpenProspectTicket()
	{
		try
		{
			log.info("ProspectController called at server startup");

			List<ProspectVoForClassification> propspectVoForClassificationList = prospectDAO
					.getAllOpenPropspects();

			log.info("Call prioritization Engine at server startup "
					+ propspectVoForClassificationList.size());

			// Thread based logic have to be performed here based on city;

			LinkedHashSet<Long> setValue = new LinkedHashSet<>();

			if (propspectVoForClassificationList != null
					&& propspectVoForClassificationList.size() > 0)
			{
				for (ProspectVoForClassification propspectVoForClassification : propspectVoForClassificationList)
				{

					try
					{
						log.debug("Updating details for ticketId "
								+ propspectVoForClassification.getTicketId());

						setValue.add(Long.valueOf(propspectVoForClassification
								.getTicketId()));

						Long assignedTo = propspectVoForClassification
								.getAssignedTo();

						// if (propspectVoForClassification.getAssignedTo() <=
						// 0)
						// {
						// if (propspectVoForClassification
						// .getStatus() !=
						// FWMPConstant.TicketStatus.UNASSIGNED_TICKET)
						// {
						// StatusVO status = new StatusVO();
						// status.setStatus(FWMPConstant.TicketStatus.UNASSIGNED_TICKET);
						// status.setTicketId(propspectVoForClassification
						// .getTicketId());
						// status.setPropspectNo(null);
						//
						// customerCoreService.updateTicket(status);
						//
						// TicketLog logs = new TicketLogImpl(Long
						// .valueOf(propspectVoForClassification
						// .getTicketId()),
						// TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED,
						// AuthUtils
						// .getCurrentUserId());
						//
						// TicketUpdateGateway.logTicket(logs);
						// }
						// setValue.clear();
						//
						// continue;
						//
						// }

						String seKey = dbUtil
								.getKeyForQueueByUserId(assignedTo);

						propspectControllerCoreService
								.addTicket2MainQueue(seKey, setValue);

						long reportToId = userDao.getReportToUser(assignedTo);

						if (reportToId == 0)
						{
							log.debug("No Report to found for user "
									+ assignedTo);
							setValue.clear();
							continue;
						}

						String tlKey = dbUtil
								.getKeyForQueueByUserId(reportToId);

						propspectControllerCoreService
								.addTicket2MainQueue(tlKey, setValue);

					} catch (Exception e)
					{
						log.error("Error while classifying tickets for ticket Id at server startup"
								+ propspectVoForClassification.getTicketId()
								+ ". Message is " + e.getMessage());
						e.printStackTrace();
					}

					setValue.clear();
				}
			} else
			{
			}
			// log.debug("Main queue for prospect at server startup is "
			// + globalActivities.getMainQueue());
		} catch (Exception e)
		{
			log.error("Error while classifying tickets during start up"
					+ e.getMessage());
			e.printStackTrace();
		}

	}

	private void getAllPendingProspects()
	{
		try
		{
			List<ProspectCoreVO> allPendingProspects = mongoTicketDAO
					.getAllPendingProspects();

			log.info("Total pending prospect are "
					+ allPendingProspects.size());

			if (allPendingProspects != null && !allPendingProspects.isEmpty())
			{
				for (Iterator<ProspectCoreVO> iterator = allPendingProspects
						.iterator(); iterator.hasNext();)
				{
					ProspectCoreVO prospectCoreVO = (ProspectCoreVO) iterator
							.next();

					log.info("Adding prospect in virtual queue "
							+ prospectCoreVO.getCustomer().getMobileNumber());

					prospectCreatorHelper.addNewProspect(prospectCoreVO, null);

				}

			}
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error while peparing pending prospect queue at server startup "
					+ e.getMessage());
		}
	}

}
