package com.cupola.fwmp.service.domain.engines.classification.jobs;

  
 
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.TicketPriority;
import com.cupola.fwmp.persistance.entities.WorkOrder;
import com.cupola.fwmp.vo.TicketPriorityVo;

/**
 * 
 * @author G Ashraf
 * 
 * */
@Lazy(false)
public class PriortyUpdaterJob
{
	private static Logger LOGGER = LogManager.getLogger(PriortyUpdaterJob.class.getName());
	
	
	private TicketDAO ticketDAO;

	public TicketDAO getTicketDAO()
	{
		return ticketDAO;
	}

	public void setTicketDAO(TicketDAO ticketDAO)
	{
		this.ticketDAO = ticketDAO;
	}
	
	@Scheduled(cron = "${fwmp.priority.cron.trigger}")
	public void executeInternal()
	{
		LOGGER.info("****************************************************");
		LOGGER.info("************* PRIOIRTY PER MID NIGHT ***************");
		LOGGER.info("****************************************************");
		List<Ticket> listOfTicket = ticketDAO.getAllTicket();
		
		if( listOfTicket != null )
		{
			List<TicketPriorityVo> listTicketPriorityVo = new ArrayList<TicketPriorityVo>();
			TicketPriorityVo  ticketVo = null;
			
			for(Ticket ticket : listOfTicket)
			{
				ticketVo = new TicketPriorityVo();
				ticketVo.setTicketCreationDate(ticket.getTicketCreationDate());
				ticketVo.setTicketId(ticket.getId());
				if(ticket.getUserByAssignedTo() != null)
					ticketVo.setAssignedTo(ticket.getUserByAssignedTo().getId());
				
				TicketPriority priority = ticket.getTicketPriority();
				Set<WorkOrder> workOrder = ticket.getWorkOrders();
//				Set<FrDetail> frWorkOrder = ticket.getFrWorkOrders();
				
				if( priority != null )
				{
					if( workOrder != null && workOrder.size() > 0)
						ticketVo.setTicketCreationDate(workOrder.iterator().next().getAddedOn());
//					else if (frWorkOrder != null && frWorkOrder.size() > 0)
//						ticketVo.setTicketCreationDate(frWorkOrder.iterator().next().getAddedOn());
					
					ticketVo.setPriorityId(priority.getId());
					ticketVo.setPriorityValue(priority.getValue());
					ticketVo.setEscalationType(priority.getEscalationType());
					ticketVo.setEscalatedValue(priority.getEscalatedValue());
					ticketVo.setPriorityModifiedBy(priority.getModifiedBy());
					ticketVo.setPriorityModifiedOn(priority.getModifiedOn());
					ticketVo.setUpdatedCount(priority.getUpdatedCount());
				}
				
				listTicketPriorityVo.add(ticketVo);
			}
			PriortyUpdateHelper.updateBasePriorityForTickets(listTicketPriorityVo);
			
		}
		this.calculateFrTickePriority();
	}
	
	private void calculateFrTickePriority()
	{
		LOGGER.info("****************************************************");
		LOGGER.info("************** FR PRORITY CALCULATOR ***************");
		LOGGER.info("****************************************************");
		
		List<FrTicket> listOfTicket = ticketDAO.getAllOpenFrTicket();
		
		if( listOfTicket != null )
		{
			List<TicketPriorityVo> listTicketPriorityVo = new ArrayList<TicketPriorityVo>();
			TicketPriorityVo  ticketVo = null;
			
			for(FrTicket ticket : listOfTicket)
			{
				ticketVo = new TicketPriorityVo();
				ticketVo.setTicketCreationDate(ticket.getTicketCreationDate());
				ticketVo.setTicketId(ticket.getId());
				
				if(ticket.getUserByAssignedTo() != null)
					ticketVo.setAssignedTo(ticket.getUserByAssignedTo().getId());
				
				TicketPriority priority = ticket.getTicketPriority();
//				Set<FrDetail> frWorkOrder = ticket.getFrWorkOrders();
				
				if( priority != null )
				{
					ticketVo.setPriorityId(priority.getId());
					ticketVo.setPriorityValue(priority.getValue());
					ticketVo.setEscalationType(priority.getEscalationType());
					ticketVo.setEscalatedValue(priority.getEscalatedValue());
					ticketVo.setPriorityModifiedBy(priority.getModifiedBy());
					ticketVo.setPriorityModifiedOn(priority.getModifiedOn());
					ticketVo.setUpdatedCount(priority.getUpdatedCount());
				}
				
				listTicketPriorityVo.add(ticketVo);
			}
			PriortyUpdateHelper.updateBasePriorityForTickets(listTicketPriorityVo);
			
		}
	}

}
