package com.cupola.fwmp.service.domain.engines.classification.jobs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.domain.engines.classification.handlers.CrmControllerHandler;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ClassificationEngineCoreServiceImpl;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.fr.FrTicketLockHelper;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.UserVo;

public class CrmJobHelper
{
	private final static Logger LOGGER = LogManager
			.getLogger(CrmJobHelper.class);

	// private List<MQWorkorderVO> frTicketDetails = null;
	private String cityName;

	@Autowired
	private ClassificationEngineCoreServiceImpl classificationEngineCoreServiceImpl;

	@Autowired
	private UserDao userDao;

	@Autowired
	private DBUtil dbUtil;

	@Autowired
	private CrmControllerHandler crmControllerHandler;

	@Autowired
	private CustomerCoreService customerCoreService;

	@Autowired
	private TicketDAO ticketDAO;

	@Autowired
	private FrTicketService frService;

	@Autowired
	private FrTicketLockHelper frHelper;
	
	@Autowired
	private FrTicketService frTicketService;

	private CrmJob crm = new CrmJob();

	public void createFrTicketDetail(List<MQWorkorderVO> frTicketDetails,
			String cityName)
	{
		LOGGER.info(" Started Fr Work Order Insertion in FWMP for city ...." + cityName) ;
		this.cityName = cityName;
//		crm.addInProcessor( frTicketDetails );
		createDetail(frTicketDetails,cityName);
	}

	public APIResponse createDetail( List<MQWorkorderVO> frTicketDetails, String cityName )
	{
		/*if (frTicketDetails == null)
			return;*/
		final Map<String, Long> completeMap = new ConcurrentHashMap<String, Long>();

		try
		{
			LOGGER.info(" Started Fr Work Order Insertion in FWMP .... " + cityName);

			final List<MQWorkorderVO> mqDataNotInFWMP = getFrWorkOrderNotInDb(frTicketDetails);
			
			LOGGER.info(" GOT Fr Work Order which is not in FWMP .... " + cityName);
			
			final Map<String, Long> newlyCreatedWorkOrderInDb = customerCoreService
					.addFrCustomerViaCRM(mqDataNotInFWMP);

			completeMap.putAll(newlyCreatedWorkOrderInDb);

			Set<Long> autoAssignedUserId = new HashSet<Long>();
			
			LinkedHashSet<Long> setValue = new LinkedHashSet<>();
			
			LOGGER.info(" Total FR Wo to create is .... " + mqDataNotInFWMP + " for city " + cityName + " with completeMap " + completeMap);
			
			final Map<Long, Boolean> workOrderGenerated = crmControllerHandler
					.createFrWorkOrderInBulk(mqDataNotInFWMP, completeMap);
			
			LOGGER.info("Done Fr Work Order Insertion in FWMP ...." + cityName);
			LOGGER.info("Started Prapering NE Q & auto assigning...." + cityName);

			for (MQWorkorderVO workOrder : mqDataNotInFWMP) 
			{
				try {
					long assignedTo = 0;
					long ticketId = 0;
					
					if(completeMap.containsKey(workOrder.getWorkOrderNumber()))
					{
						if(completeMap.get(workOrder.getWorkOrderNumber()) != null)
							ticketId = completeMap.get(workOrder.getWorkOrderNumber());
						else
							ticketId = dbUtil.getFrTicketIdByWorkOrder(workOrder.getWorkOrderNumber());
					}
							

					// String fxName = ticketIdAndFxNameMap.get(ticketId);
					setValue.add(ticketId);

					String fxName = workOrder.getFxName();
					LOGGER.info("Found FxName : "+fxName+" for workorder no. : "+workOrder.getWorkOrderNumber() + " cityName " + cityName);

					if(!workOrderGenerated.containsKey(ticketId))
						LOGGER.info("terminated because of workOrderGenerated map does not contain ticket :: "+ticketId);
						
					if (workOrderGenerated != null && workOrderGenerated.containsKey(ticketId) && workOrderGenerated.get(ticketId))
					{
						if (workOrder.getAssignedTo() == null
								|| workOrder.getAssignedTo() <= 0)
						{
							Long flowId = FrTicketUtil
									.findFlowIdforMatchingSymptomName(workOrder
											.getSymptom());

							UserVo userVo = userDao.getUsersByDeviceName(fxName,
									AuthUtils.getUserRolesByFlowId(flowId));

							if (userVo != null) 
							{
								assignedTo = userVo.getId();
								ticketDAO.updateFrCurrentAssignedToViaCrm(ticketId,
										assignedTo, FWMPConstant.SYSTEM_ENGINE,
										FRStatus.FR_WORKORDER_GENERATED);

								LOGGER.info("For FxName : " + fxName + " workorder no. : "
										+ workOrder.getWorkOrderNumber() + " assigned to " + userVo.getLoginId()
										+ " cityName " + cityName + " flowid " + flowId);

								/*LOGGER.info("In "+ cityName+" Found User for Ticket Id "
										+ ticketId + " assignedTo " + assignedTo);*/
								this.addFrTicket2MainQueue(workOrder, setValue,
										assignedTo);
								
								autoAssignedUserId.add(assignedTo);
								callLogger(ticketId,TicketUpdateConstant.UPDATE_CATEGORY_TICKET_ASSIGNED);
								
								String currentCRM = dbUtil.getCurrentCrmName(null, null);

								if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
									
									frTicketService.updateFaltRepairActivityInCRM(ticketId, null, null, null, null);
								}
							} 
							else 
							{
								StatusVO status = new StatusVO();
								status.setStatus(FRStatus.FR_UNASSIGNEDT_TICKET);
								status.setTicketId(ticketId);
								status.setWorkOrderNumber(workOrder.getWorkOrderNumber());

								frService.updateFrTicketStatus(status);

								/*LOGGER.info("In "+ cityName+" Found User for Ticket Id "
										+ ticketId + " assignedTo " + assignedTo);*/

								LOGGER.info(
										"For FxName : " + fxName + " workorder no. : " + workOrder.getWorkOrderNumber()
												+ " auto assignment failed " + cityName + " flowid " + flowId);

								setValue.clear();
								callLogger(ticketId,TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
							}
						}
						else 
						{
							assignedTo = workOrder.getAssignedTo();
							this.addFrTicket2MainQueue(workOrder, setValue, assignedTo);
							LOGGER.info("For FxName assigned is present " + fxName + " workorder no. : "
									+ workOrder.getWorkOrderNumber() + " assigned to " + assignedTo);
							autoAssignedUserId.add(assignedTo);
							callLogger(ticketId,TicketUpdateConstant.AUTO_ASSIGNMENT_BY_CRM);
						}
					} 
					else
					{
						LOGGER.info("" + cityName + " Work order could not be assigned for FR "
								+ workOrder.getWorkOrderNumber() + " cityName " + cityName + "workOrderGenerated "
								+ workOrderGenerated);

						ticketId = ticketDAO.getFrTicketIdByWorkOrder(workOrder.getWorkOrderNumber());
						
						LOGGER.info("" + cityName + " Work order could not be assigned for FR so retrying "
								+ workOrder.getWorkOrderNumber() + " cityName " + cityName);
						
						if (ticketId > 0) {

							if (workOrder.getAssignedTo() == null || workOrder.getAssignedTo() <= 0) {
								Long flowId = FrTicketUtil.findFlowIdforMatchingSymptomName(workOrder.getSymptom());

								UserVo userVo = userDao.getUsersByDeviceName(fxName,
										AuthUtils.getUserRolesByFlowId(flowId));

								if (userVo != null) {
									assignedTo = userVo.getId();
									ticketDAO.updateFrCurrentAssignedToViaCrm(ticketId, assignedTo,
											FWMPConstant.SYSTEM_ENGINE, FRStatus.FR_WORKORDER_GENERATED);

									LOGGER.info("For FxName retrying: " + fxName + " workorder no. : "
											+ workOrder.getWorkOrderNumber() + " assigned to " + userVo.getLoginId()
											+ " cityName " + cityName + " flowid " + flowId);

									/*
									 * LOGGER.info("In "+ cityName+" Found User for Ticket Id " + ticketId +
									 * " assignedTo " + assignedTo);
									 */
									this.addFrTicket2MainQueue(workOrder, setValue, assignedTo);

									autoAssignedUserId.add(assignedTo);
									callLogger(ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_ASSIGNED);
								} else {
									StatusVO status = new StatusVO();
									status.setStatus(FRStatus.FR_UNASSIGNEDT_TICKET);
									status.setTicketId(ticketId);
									status.setWorkOrderNumber(workOrder.getWorkOrderNumber());

									frService.updateFrTicketStatus(status);

									/*
									 * LOGGER.info("In "+ cityName+" Found User for Ticket Id " + ticketId +
									 * " assignedTo " + assignedTo);
									 */

									LOGGER.info("For FxName retrying: " + fxName + " workorder no. : "
											+ workOrder.getWorkOrderNumber() + " auto assignment failed " + cityName
											+ " flowid " + flowId);

									setValue.clear();
									callLogger(ticketId, TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
								}
								
							} else {
								assignedTo = workOrder.getAssignedTo();
								this.addFrTicket2MainQueue(workOrder, setValue, assignedTo);
								LOGGER.info("For FxName assigned is present retrying" + fxName + " workorder no. : "
										+ workOrder.getWorkOrderNumber() + " assigned to " + assignedTo);
							}

						}

						setValue.clear();
					}
				} catch (Exception e) {
					LOGGER.error("Exception for workorder " + workOrder.getWorkOrderNumber() + " cityName " + cityName + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
			
			LOGGER.info("Done Prapering NE Q & auto assigning.... " + cityName);
			LOGGER.info("Started Locking/unlocking management..." + cityName);
			if( !autoAssignedUserId.isEmpty() )
			{
				frHelper.manageBulkUserLock(autoAssignedUserId);
				autoAssignedUserId.clear();
			}
			LOGGER.info("Done Locking/unlocking management... " + cityName);
		}
		catch (Exception e) {
			LOGGER.error("Error occured while creating Fr Ticket ..." + cityName ,e);
			e.printStackTrace();
			return ResponseUtil.createDBExceptionResponse();
		}
		
		
		return ResponseUtil.createSuccessResponse().setData(completeMap);
	}

	private void manageReopenWorkOrder(MQWorkorderVO workOrder)
	{
		LOGGER.info("WorkOrder no. :" + workOrder.getWorkOrderNumber()
				+ " Re open for " + cityName);
		frService.reopenFrWorkOrder(workOrder);
	}
	
	private List<MQWorkorderVO> getFrWorkOrderNotInDb( List<MQWorkorderVO> frTicketDetails ) 
	{
		try
		{
			List<String> workOrderList = new ArrayList<>();
			Map<String, MQWorkorderVO> workOrderDetailMap = new ConcurrentHashMap<String, MQWorkorderVO>();

			for (MQWorkorderVO workOrder : frTicketDetails)
			{
				matchSymptomForSlowSpeed( workOrder );
				workOrderList.add(workOrder.getWorkOrderNumber());
				workOrderDetailMap.put(workOrder.getWorkOrderNumber(), workOrder);
				
				if( workOrder.getResponseStatus().equalsIgnoreCase("REOPEN") )
					manageReopenWorkOrder( workOrder );
			}

			Map<String, Long> woInDb = frService.getFrTicketByWorkOrder(workOrderList);
			
			Set<String> woListsFromDb = woInDb.keySet();

			workOrderDetailMap.keySet().removeAll(woListsFromDb);

			List<MQWorkorderVO> mqDataNotInFWMP = new ArrayList<MQWorkorderVO>(
					workOrderDetailMap.values());

//			completeMap.putAll(woInDb);

			return mqDataNotInFWMP;
			
		} 
		catch (Exception e) 
		{
			LOGGER.error("Error occured while getting wo to be created in FWMP..." + cityName + e.getMessage());
			e.printStackTrace();
			return new ArrayList<MQWorkorderVO>();
		}
		
	}

	private void addFrTicket2MainQueue(MQWorkorderVO hydMQWorkorderVO,
			LinkedHashSet<Long> setValue, long assignedTo)
	{
		long reportTo = 0l;
		reportTo = userDao.getReportToUser(assignedTo);
		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

		LOGGER.info("Adding ticket in NE queue for "+ cityName+" with TL key " + neKey + " and values are " + setValue);
		
		classificationEngineCoreServiceImpl.addFrTicket2MainQueue(neKey,
				setValue);
		if (reportTo == 0l)
		{
			LOGGER.info("No Report to found for user " + assignedTo);
			return;
		}
		String tlKey = dbUtil.getKeyForQueueByUserId(reportTo);
		LOGGER.info("Adding ne ticket in Tl queue for "+ cityName+" with TL key " + tlKey + " and values are " + setValue);
		classificationEngineCoreServiceImpl.addFrTicket2MainQueue(tlKey,
				setValue);

		setValue.clear();
	}

	private void callLogger(long ticketId, String ticketCategory)
	{
		TicketUpdateGateway
				.logTicket(new TicketLogImpl(ticketId, ticketCategory, FWMPConstant.SYSTEM_ENGINE));
	}

	private class CrmJob
	{
		private BlockingQueue<List<MQWorkorderVO>> dataFromMQ = null;
		private Thread crmJob = null;

		private CrmJob()
		{
			startCrmJob();
		}

		public void startCrmJob()
		{
			LOGGER.info("enabling Crm  processing");
			dataFromMQ = new LinkedBlockingQueue<List<MQWorkorderVO>>(100000);

			if (crmJob == null)
			{
				crmJob = new Thread(jobRunnar, "Crm Fr Processor");
				crmJob.start();
			}
			LOGGER.info("Crm processing enabled");
		}

		Runnable jobRunnar = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					List<MQWorkorderVO> workOrderList = null;
					while ((workOrderList = dataFromMQ.take()) != null)
					{
						try
						{
							
								createDetail(workOrderList,"");
							
							workOrderList = null;
						} catch (Exception e)
						{
							LOGGER.error("Error While Crm proccesing workOrder : "
									+ e.getMessage());
							continue;
						}
					}

				} catch (Exception e)
				{
					LOGGER.error("Error in Crm Processor main loop- msg="
							+ e.getMessage());
				}
			}
		};

		public void addInProcessor(List<MQWorkorderVO> addInQ)
		{
			if (addInQ == null)
				return;
			try
			{

				dataFromMQ.put(addInQ);

			} catch (Exception e)
			{
				LOGGER.error("error adding in Crm Fr Processor "
						+ e.getMessage());
			}
		}
	}

	private void matchSymptomForSlowSpeed(MQWorkorderVO workOrder)
	{
		if (workOrder == null)
			return;

		try
		{
			String symptom = workOrder.getSymptom();
			Long flowId = FrTicketUtil
					.findFlowIdforMatchingSymptomName(symptom);

			if (flowId == null && symptom != null
					&& symptom.contains("Fiber Cut")
					&& symptom.indexOf("Slow Issue") > 0)
			{
				symptom = "Fiber Cut-Slow Issue";
				workOrder.setSymptom(symptom);
			}

		} catch (Exception e)
		{
			LOGGER.error("error occured while matching slow speed symptom ", e);
		}
	}
}
