/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.pfm;

import java.util.LinkedHashSet;

import com.cupola.fwmp.response.APIResponse;

/**
 * @author aditya
 * 
 */
public interface ManageQueueCoreServcie
{
	APIResponse addTicket2MainQueue(String key,
			LinkedHashSet<Long> ticketNumberList);

	APIResponse getAllTicketFromMainQueueByKey(String key);

	APIResponse removeTicketFromMainQueueByKeyAndTicketNumber(String key,
			Long ticketNumber);

	APIResponse addTicket2WorkingQueue(String key,
			LinkedHashSet<Long> ticketNumberList,boolean flagOfManualInter);

	APIResponse getAllTicketFromWorkingQueueByKey(String key);

	APIResponse removeTicketFromWorkingQueueByKeyAndTicketNumber(String key,
			Long ticketNumberList);

}
