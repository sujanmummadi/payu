/**
 * 
 */
package com.cupola.fwmp.service.domain.engines.pfm;

import java.util.LinkedHashSet;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * @author aditya
 * 
 */
public class ManageQueueCoreServcieImpl implements
		ManageQueueCoreServcie
{
	@Autowired
	GlobalActivities globalActivities;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.domain.engines.pfm.ManageQueueForNECoreServcie
	 * #addTicket2NeMainQueue(java.lang.String, java.util.LinkedHashSet)
	 */
	@Override
	public APIResponse addTicket2MainQueue(String key,
			LinkedHashSet<Long> ticketNumberList)
	{

		globalActivities.addTicket2MainQueue(key, ticketNumberList);

		return ResponseUtil.createSuccessResponse();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.domain.engines.pfm.ManageQueueForNECoreServcie
	 * #getAllTicketFromNeMainQueueByKey(java.lang.String)
	 */
	@Override
	public APIResponse getAllTicketFromMainQueueByKey(String key)
	{
		return ResponseUtil.recordFoundSucessFully()
				.setData(globalActivities.getAllTicketFromMainQueueByKey(key));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.domain.engines.pfm.ManageQueueForNECoreServcie
	 * #removeTicketFromNeMainQueueByKeyAndTicketNumber(java.lang.String,
	 * java.lang.Long)
	 */
	@Override
	public APIResponse removeTicketFromMainQueueByKeyAndTicketNumber(
			String key, Long ticketNumber)
	{
//		globalActivities
//				.removeTicketFromMainQueueByKeyAndTicketNumber(key, ticketNumber);

		return ResponseUtil.createDeleteSuccessResponse();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.domain.engines.pfm.ManageQueueForNECoreServcie
	 * #addTicket2NeWorkingQueue(java.lang.String, java.util.LinkedHashSet)
	 */
	@Override
	public APIResponse addTicket2WorkingQueue(String key,
			LinkedHashSet<Long> ticketNumberList, boolean flagOfManualInter)
	{
		if (globalActivities
				.addTicket2WorkingQueue(key, ticketNumberList, flagOfManualInter))
		{
			return ResponseUtil.createSuccessResponse();
			
		} else
		{
			return ResponseUtil.createDeleteFailedResponse();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.domain.engines.pfm.ManageQueueForNECoreServcie
	 * #getAllTicketFromNeWorkingQueueByKey(java.lang.String)
	 */
	@Override
	public APIResponse getAllTicketFromWorkingQueueByKey(String key)
	{
		return ResponseUtil
				.recordFoundSucessFully()
				.setData(globalActivities.getAllTicketFromWorkingQueueByKey(key));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.domain.engines.pfm.ManageQueueForNECoreServcie
	 * #removeTicketFromNeWorkingQueueByKeyAndTicketNumber(java.lang.String,
	 * java.lang.Long)
	 */
	@Override
	public APIResponse removeTicketFromWorkingQueueByKeyAndTicketNumber(
			String key, Long ticketNumberList)
	{
		globalActivities
				.removeTicketFromWorkingQueueByKeyAndTicketNumber(key, ticketNumberList);

		return ResponseUtil.createDeleteSuccessResponse();
	}

}
