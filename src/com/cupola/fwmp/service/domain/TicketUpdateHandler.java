
package com.cupola.fwmp.service.domain;

import com.cupola.fwmp.service.domain.logger.TicketLog;

public interface TicketUpdateHandler
{
	public void handleTicket (TicketLog ticketLog);

}
