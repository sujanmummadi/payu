 
package com.cupola.fwmp.service.domain;

import java.util.Map;

import javax.servlet.http.HttpSession;

public interface HttpRequestInfo
{

	public String getContextPath ();

	public void setContextPath (String contextPath);

	public boolean isRequestedSessionIdValid ();

	public void setRequestedSessionIdValid (boolean isRequestedSessionIdValid);

	public boolean isRequestSecure ();

	public void setRequestSecure (boolean isRequestSecure);

	public String getPathInfo ();

	public void setPathInfo (String pathInfo);

	public String getRequestedSessionId ();

	public void setRequestedSessionId (String requestedSessionId);

	public String getRequestURI ();

	public void setRequestURI (String requestURI);

	public String getRequestURL ();

	public void setRequestURL (String requestURL);

	public void setRemoteHost (String remoteHost);

	public String getRemoteHost ();

	public void setRemoteAddr (String remoteAddr);

	public String getRemoteAddr ();
	
	public String getBaseUrl ();
	
	public HttpSession getHttpSession ();

	public void setHttpSession (HttpSession httpSession);
	
	public void setRequestedFromMobile(boolean isRequestedFromMobile);
	public boolean isRequestedFromMobile ();
	public void addParameters(String key, String value);
	public Map<String, String> getParameters();
}