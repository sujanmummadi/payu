package com.cupola.fwmp.service.domain;

public interface TicketAssignmentChangeHandler
{

	public TicketUpdateSubscriptionService getSubscriptionService();

	public void setSubscriptionService(
			TicketUpdateSubscriptionService subscriptionService);

}