package com.cupola.fwmp.service.domain;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.logger.TicketLog;

public class TicketAssignmentChangeHandlerImpl implements
		TicketAssignmentChangeHandler
{

	TicketUpdateSubscriptionService subscriptionService;
	static final Logger logger = Logger
			.getLogger(TicketAssignmentChangeHandlerImpl.class);
	BlockingQueue<TicketLog> ticketQueue;
	Thread updateProcessorThread = null;

	void createSubscritions()
	{
		logger.info("createSubscritions called----");
		subscriptionService.subscribe(updateHandler,TicketUpdateConstant.UPDATE_CATEGORY_TICKET_ASSIGNED);
		logger.info("createEventSubscritions done----");
	}

	public void init()
	{
		logger.info("TicketAssignmentChangeHandlerImpl init method called..");
		createSubscritions();
		logger.info("enabling updateProcessorThread processing-");
		ticketQueue = new LinkedBlockingQueue(100000);

		if (updateProcessorThread == null)
		{
			updateProcessorThread = new Thread(run, "TicketAssignmentChangeHandler");
			updateProcessorThread.start();
		}

		logger.info("TicketAssignmentChangeHandlerImpl init method executed.");
	
	}

	
	
	Runnable run = new Runnable()
	{

		public void run()
		{
			try
			{
				TicketLog ticketLog = null;

				while ((ticketLog = ticketQueue.take()) != null)
				{

					try
					{

							// Start processing from here
							logger.debug("Current Processing ticketLog " + ticketLog);
							
							// check ticketLog and start processing

							ticketLog = null;
						Thread.sleep(500);
					} catch (Exception e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
						continue;
					}
				}

			} catch (Exception e)
			{
				logger.error("error in Processor main loop- msg="
						+ e.getMessage());
			}

			logger.info("Processor thread interrupted- getting out of processing loop");

		}

	};

	TicketUpdateHandler updateHandler = new TicketUpdateHandler()
	{

		public void handleTicket(TicketLog ticketLog)
		{
			if (ticketLog == null)
			{
				return;
			}
			try
			{

				 
					ticketQueue.add(ticketLog);

			} catch (Exception e)
			{
				e.printStackTrace();
				logger.error("error" + e.getMessage());
			}

		}

	};

	public TicketUpdateSubscriptionService getSubscriptionService()
	{
		return subscriptionService;
	}

	public void setSubscriptionService(
			TicketUpdateSubscriptionService subscriptionService)
	{
		this.subscriptionService = subscriptionService;
	}

}
