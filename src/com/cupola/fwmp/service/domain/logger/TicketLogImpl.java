package com.cupola.fwmp.service.domain.logger;

import java.util.Date;
import java.util.Map;

import org.springframework.data.annotation.Id;


public class TicketLogImpl implements TicketLog 
{
	@Id
	private long id;
	
	private long ticketId;
	private String woNumber;
	private long activityId;
	private String activityValue;
	private long subActivityId;
	private String activityName;
	private String subActivityValue;
	private long statusId;
	private String statusName;
	private String currentUpdateCategory; 
	private String message;
	private int reasonCode;
	private String reasonValue;
	private String remarks;
	
	private String category;
	private long addedBy;
	
	private Date addedOn;
	private boolean isFrTicket;
	
	public boolean isFrTicket() {
		return isFrTicket;
	}
	public void setFrTicket(boolean isFrTicket) {
		this.isFrTicket = isFrTicket;
	}
	private Map<String, String> additionalAttributeMap;
	public TicketLogImpl()
	{
	}
	public TicketLogImpl(TicketLog value)
	{
		setTicketId(value.getTicketId());
		setCategory(value.getCategory());
		setCurrentUpdateCategory( value.getCategory());
		setAddedOn(value.getAddedOn());
		setAddedBy(value.getAddedBy());
		setActivityId(value.getActivityId());
		
	}
	public TicketLogImpl(long ticketId, String currentUpdateCategory)
	{
		super();
		setTicketId(ticketId);
		setCurrentUpdateCategory(currentUpdateCategory);
	}
	public TicketLogImpl(long ticketId, String category, long addedBy)
	{
		super();
		/*this.ticketId = ticketId;
		this.category = category;
		this.currentUpdateCategory= category;
		this.addedOn = new Date();
		this.addedBy = addedBy;*/
		
		setTicketId(ticketId);
		setCategory(category);
		setCurrentUpdateCategory( category);
		setAddedOn(new Date());
		setAddedBy(addedBy);
	}
	public  long getId()
	{
		return id;
	}
	public  void setId(long id)
	{
		this.id = id;
	}
	public  long getTicketId()
	{
		return ticketId;
	}
	public  void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}
	public  String getWoNumber()
	{
		return woNumber;
	}
	public  void setWoNumber(String woNumber)
	{
		this.woNumber = woNumber;
	}
	public  long getActivityId()
	{
		return activityId;
	}
	public  void setActivityId(long activityId)
	{
		this.activityId = activityId;
	}
	public  String getActivityValue()
	{
		return activityValue;
	}
	public  void setActivityValue(String activityValue)
	{
		this.activityValue = activityValue;
	}
	public  long getSubActivityId()
	{
		return subActivityId;
	}
	public  void setSubActivityId(long subActivityId)
	{
		this.subActivityId = subActivityId;
	}
	public  String getActivityName()
	{
		return activityName;
	}
	public  void setActivityName(String activityName)
	{
		this.activityName = activityName;
	}
	public  String getSubActivityValue()
	{
		return subActivityValue;
	}
	public  void setSubActivityValue(String subActivityValue)
	{
		this.subActivityValue = subActivityValue;
	}
	public  long getStatusId()
	{
		return statusId;
	}
	public  void setStatusId(long statusId)
	{
		this.statusId = statusId;
	}
	public  String getStatusName()
	{
		return statusName;
	}
	public  void setStatusName(String statusName)
	{
		this.statusName = statusName;
	}
	public  String getCurrentUpdateCategory()
	{
		return currentUpdateCategory;
	}
	public  void setCurrentUpdateCategory(String currentUpdateCategory)
	{
		this.currentUpdateCategory = currentUpdateCategory;
	}
	public  String getMessage()
	{
		return message;
	}
	public  void setMessage(String message)
	{
		this.message = message;
	}
	public  int getReasonCode()
	{
		return reasonCode;
	}
	public  void setReasonCode(int reasonCode)
	{
		this.reasonCode = reasonCode;
	}
	public  String getReasonValue()
	{
		return reasonValue;
	}
	public  void setReasonValue(String reasonValue)
	{
		this.reasonValue = reasonValue;
	}
	public  String getRemarks()
	{
		return remarks;
	}
	public  void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}
	public  String getCategory()
	{
		return category;
	}
	public  void setCategory(String category)
	{
		this.category = category;
	}
	public  long getAddedBy()
	{
		return addedBy;
	}
	public  void setAddedBy(long addedBy)
	{
		this.addedBy = addedBy;
	}
	public  Date getAddedOn()
	{
		return addedOn;
	}
	public  void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}
	public  Map<String, String> getAdditionalAttributeMap()
	{
		return additionalAttributeMap;
	}
	public  void setAdditionalAttributeMap(
			Map<String, String> additionalAttributeMap)
	{
		this.additionalAttributeMap = additionalAttributeMap;
	}
	 
}
