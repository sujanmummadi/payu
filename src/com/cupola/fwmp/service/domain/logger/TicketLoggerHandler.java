package com.cupola.fwmp.service.domain.logger;

import org.apache.log4j.Logger;

import com.cupola.fwmp.dao.mongo.logger.LoggerDao;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.vo.ActivityVo;

public class TicketLoggerHandler
{
	private static Logger LOGGER = Logger.getLogger(TicketLoggerHandler.class.getName());
	
	private LoggerDao loggerDao;
	
	public  void logTicketUpdate(TicketLog ticketLog)
	{
		LOGGER.debug("Logging ticketLog for ticketLog " + ticketLog.getTicketId());

		if(ticketLog.getActivityId() > 0 )
		{
			ActivityVo vo =  WorkOrderDefinitionCache.getActivityById(ticketLog.getActivityId());
			if(vo != null)
			{
				ticketLog.setActivityName(vo.getActivityName());
			}
		}
		loggerDao.insertLog(LogFactory.createNewLog(ticketLog));

	}

	public static Logger getLOGGER()
	{
		return LOGGER;
	}

	public static void setLOGGER(Logger lOGGER)
	{
		LOGGER = lOGGER;
	}

	public LoggerDao getLoggerDao()
	{
		return loggerDao;
	}

	public void setLoggerDao(LoggerDao loggerDao)
	{
		this.loggerDao = loggerDao;
	}

}
