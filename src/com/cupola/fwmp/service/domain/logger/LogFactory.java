package com.cupola.fwmp.service.domain.logger;

import org.springframework.beans.BeanUtils;

import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;

public class LogFactory
{
	public static TicketLog createNewLog (TicketLog ticketLog)
	{
		TicketLog log = new TicketLogImpl(ticketLog.getTicketId(), ticketLog.getCurrentUpdateCategory(),ticketLog.getAddedBy());
		BeanUtils.copyProperties(ticketLog, log);
		log.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		return log;
	}
}
