package com.cupola.fwmp.service.domain.logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.cupola.fwmp.service.domain.TicketUpdateHandler;
import com.cupola.fwmp.service.domain.TicketUpdateSubscriptionService;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;

public class TicketLoggerServiceImpl implements TicketLoggerService
{
	TicketUpdateSubscriptionService subscriptionService;
	static final Logger logger = Logger
			.getLogger(TicketLoggerServiceImpl.class);
	BlockingQueue<TicketLog> ticketQueue;
	Thread updateProcessorThread = null;
	TicketLoggerHandler loggerHandler;

	public void init()
	{
		logger.info("createSubscritions called----");
		subscriptionService
				.subscribe(updateHandler, TicketUpdateConstant.UPDATE_CATEGORY_ALL);
		logger.info("createEventSubscritions done----");
		logger.info("enabling updateProcessorThread processing-");
		ticketQueue = new LinkedBlockingQueue(100000);

		if (updateProcessorThread == null)
		{
			updateProcessorThread = new Thread(run, "TicketLoggerService");
			updateProcessorThread.start();
		}
		logger.info("createSubscritions done----");
	}

	Runnable run = new Runnable()
	{

		public void run()
		{
			try
			{
				TicketLog ticketLog = null;

				while (true)
				{
					ticketLog = ticketQueue.take();

					try
					{
						if (ticketLog != null)
						{
							loggerHandler.logTicketUpdate(ticketLog);
							// Start processing from here

							if (TicketUpdateConstant.NEW_PROSPECT_CREATED
									.equalsIgnoreCase(ticketLog
											.getCurrentUpdateCategory()))
							{

								// create prospect for CRM

							}

							logger.debug("Current Processing ticketLog "
									+ ticketLog);

							ticketLog = null;
							// Thread.sleep(100);
						}
					} catch (Exception e)
					{
						logger.error("Error While executing  Current Processing ticketLog :"
								+ e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
						continue;
					}
				}

			} catch (Exception e)
			{
				e.printStackTrace();
				logger.error("error in Processor main loop- msg="
						+ e.getMessage());
			}

			logger.info("Processor thread interrupted- getting out of processing loop");

		}

	};

	TicketUpdateHandler updateHandler = new TicketUpdateHandler()
	{

		public void handleTicket(TicketLog ticketLog)
		{
			if (ticketLog == null)
			{
				return;
			}
			try
			{

				ticketQueue.add(ticketLog);

			} catch (Exception e)
			{
				e.printStackTrace();
				logger.error("error" + e.getMessage());
			}

		}

	};

	public TicketUpdateSubscriptionService getSubscriptionService()
	{
		return subscriptionService;
	}

	public void setSubscriptionService(
			TicketUpdateSubscriptionService subscriptionService)
	{
		this.subscriptionService = subscriptionService;
	}

	public TicketLoggerHandler getLoggerHandler()
	{
		return loggerHandler;
	}

	public void setLoggerHandler(TicketLoggerHandler loggerHandler)
	{
		this.loggerHandler = loggerHandler;
	}
}
