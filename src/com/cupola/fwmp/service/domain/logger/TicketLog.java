package com.cupola.fwmp.service.domain.logger;

import java.util.Date;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TicketLog")
public interface TicketLog
{

	public long getId();

	public void setId(long id);

	public long getTicketId();

	public void setTicketId(long ticketId);

	public String getWoNumber();

	public void setWoNumber(String woNumber);

	public long getActivityId();

	public void setActivityId(long activityId);

	public String getActivityValue();

	public void setActivityValue(String activityValue);

	public String getMessage();

	public void setMessage(String message);

	public String getCategory();

	public void setCategory(String category);

	public long getAddedBy();

	public void setAddedBy(long addedBy);

	public Date getAddedOn();

	public void setAddedOn(Date addedOn);

	public Map<String, String> getAdditionalAttributeMap();

	public void setAdditionalAttributeMap(
			Map<String, String> additionalAttributeMap);

	public long getSubActivityId();

	public void setSubActivityId(long subActivityId);

	public String getSubActivityValue();

	public void setSubActivityValue(String subActivityValue);

	public String getCurrentUpdateCategory();

	public void setCurrentUpdateCategory(String currentUpdateCategory);

	public long getStatusId();

	public void setStatusId(long statusId);

	public String getActivityName();

	public void setActivityName(String activityName);

	public abstract void setReasonValue(String reasonValue);

	public abstract String getReasonValue();

	public abstract void setReasonCode(int reasonCode);

	public abstract int getReasonCode();

	public abstract void setRemarks(String remarks);

	public abstract String getRemarks();
	
	public boolean isFrTicket() ;
	public void setFrTicket(boolean isFrTicket);
}
