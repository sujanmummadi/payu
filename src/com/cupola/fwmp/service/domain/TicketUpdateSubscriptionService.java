package com.cupola.fwmp.service.domain;

import com.cupola.fwmp.service.domain.logger.TicketLog;

public interface TicketUpdateSubscriptionService
{


	public void processNewUpdate(TicketLog ticketLog);

	void subscribe(TicketUpdateHandler handler, String updateCategory);

}