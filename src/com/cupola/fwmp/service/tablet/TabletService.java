package com.cupola.fwmp.service.tablet;

import java.util.List;

import com.cupola.fwmp.filters.TabFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TabDetailsVo;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserAndRegIdDetails;

public interface TabletService {
	
	public APIResponse getTabletById(Long id);

	public APIResponse getAllTablet();

	public APIResponse deleteTablet(Long id);

	public APIResponse updateTablet(TabletVO tabletVo);

	public APIResponse updateRegistrationId(TabDetailsVo tabDetailsVo);

	APIResponse addTablet(TabletVO tabletVo);
	
	public APIResponse tabPagination(TabFilter tf);
	
	public List<UserAndRegIdDetails> getUserAndRegIdDetails(Long currentUser);

	public List<TypeAheadVo> getAllTabs();

	APIResponse getTabletByMacAddress(String macAddress);

}
