package com.cupola.fwmp.service.tablet;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TabFilter;
import com.cupola.fwmp.persistance.entities.Tablet;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.vo.TabDetailsVo;
import com.cupola.fwmp.vo.TabletFilterVO;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserAndRegIdDetails;


public class TabletServiceImpl implements TabletService
{
	final static Logger log = Logger.getLogger(TabletServiceImpl.class);

	TabletDAO tabletDAO;

	@Autowired
	UserDao userDao;
	
	public void setTabletDAO(TabletDAO tabletDAO)
	{
		this.tabletDAO = tabletDAO;
	}
	
	@Override
	public APIResponse addTablet(TabletVO tabletVo)
		{
		
		log.info("tablet vo ====== "+tabletVo);
		if(tabletVo == null)
			return ResponseUtil.createNullParameterResponse();
		
	
			
			 Tablet tab = tabletDAO.addTablet(tabletVo );
			 if(tab == null)
				 return ResponseUtil.createDuplicateResponse();
			 
			 if(tab.getStatus()==1)
				 return ResponseUtil.createSaveSuccessResponse();
			 
			 else if(tab.getStatus()==StatusCodes.FAILURE)
				 return ResponseUtil.createDBExceptionResponse();
			 
			 else
				 return ResponseUtil.createSaveFailedResponse();
				 
		}

	@Override
	public APIResponse getTabletById(Long id)
	{
		return tabletDAO.getTabletById(id);
	}


	@Override
	public APIResponse getAllTablet()
	{
		return tabletDAO.getAllTablet();
	}

	@Override
	public List<TypeAheadVo> getAllTabs() {
		
		return CommonUtil.xformToTypeAheadFormat(tabletDAO.getAllTabs());
				
	}
	
	@Override
	public APIResponse deleteTablet(Long id)
	{
		return tabletDAO.deleteTablet(id);
	}

	@Override
	public APIResponse updateTablet(TabletVO tabletVo)
	{
		return tabletDAO.updateTablet(tabletVo);
	}

	@Override
	public APIResponse updateRegistrationId(TabDetailsVo tabDetailsVo) {
		
		return tabletDAO.updateRegistrationId(tabDetailsVo);
	}

	@Override
	public List<UserAndRegIdDetails> getUserAndRegIdDetails(Long currentUser)
	{
		return tabletDAO.getUserAndRegIdDetails(currentUser);
	}

	@Override
	public APIResponse tabPagination(TabFilter tf)
	{
		List<TabletFilterVO> tablist = tabletDAO.tabPagination(tf);

		if (tablist != null)
		{

			log.info("tablist*********" + tablist);

			return ResponseUtil.recordFoundSucessFully().setData(tablist);

		} else
		{

			return ResponseUtil.NoRecordFound();
		}

	}
	
	@Override
	public APIResponse getTabletByMacAddress(String macAddress)
	{
		boolean result;
		
		try
		{
			log.info("Getting macAdderss existance " + macAddress);
			
			result = tabletDAO.getTabletByMacAddress(macAddress);
			
			log.info("Getting macAdderss existance for " + macAddress + " and result is " + result);
			
			if(result)
			{
				return ResponseUtil.createSuccessResponse().setData(result);
				
			} else
			{
				return ResponseUtil.createRecordNotFoundResponse().setData(result);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			return ResponseUtil.createRecordNotFoundResponse().setData("false");
		}
	}

}