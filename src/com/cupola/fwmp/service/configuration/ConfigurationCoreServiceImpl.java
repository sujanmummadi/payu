package com.cupola.fwmp.service.configuration;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.DeviceType;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.dao.skill.SkillDAO;
import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.userGroup.UserGroupDAO;
import com.cupola.fwmp.dao.vendor.VendorDAO;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.DefaultUserAreaMapping;
import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.persistance.entities.Tablet;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.excels.ToolsExcelsDefinition;
import com.cupola.fwmp.service.tool.ReadExcels;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.tools.DeviceToolVo;

public class ConfigurationCoreServiceImpl implements ConfigurationCoreService
{
	static private Logger log = LogManager
			.getLogger(ConfigurationCoreServiceImpl.class.getName());

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	DeviceDAO deviceDAO;

	@Autowired
	UserGroupDAO userGroupDAO;

	@Autowired
	TabletDAO tabletDAO;

	@Autowired
	UserDao userDao;

	@Autowired
	VendorDAO vendorDAO;

	@Autowired
	SkillDAO skillDAO;
	
	@Autowired
	ToolsExcelsDefinition toolsExcelsDefinition;
	
	@Autowired
	ReadExcels readExcels;


	@Override
	public APIResponse addDevices(List<DeviceVO> deviceVOs) {
		
		if (deviceVOs == null || deviceVOs.size() == 0)

			return ResponseUtil.createNullParameterResponse();

		log.info("deviceVOs ##################### " + deviceVOs);
		for (DeviceVO deviceVO : deviceVOs) {

			Branch branch = null;

			log.debug(deviceVO);

			Device device = new Device();

			BeanUtils.copyProperties(deviceVO, device);

			device.setId(StrictMicroSecondTimeBasedGuid.newGuid());

			if (deviceVO.getAreaName() != null) {
				Area area = areaDAO.getAreaByAreaName(deviceVO.getAreaName());

				device.setArea(area);

			}

			if (deviceVO.getBranchName() != null) {
				branch = branchDAO.getBranchByBranchCode(deviceVO
						.getBranchName());

				if (branch != null) {
					log.debug("branch  " + branch.getBranchName()
							+ deviceVO.getBranchName());
					device.setBranch(branch);
				} else {
					branch = branchDAO.getBranchByBranchName(deviceVO
							.getBranchName());

					if (branch != null) {
						device.setBranch(branch);
					}
				}

				if (device.getArea() == null) {
					Map<Long, String> areaMap = areaDAO
							.getAreasByBranchName(deviceVO.getBranchName());

					if (areaMap != null && areaMap.keySet().size() > 0) {
						Long areaId = areaMap.keySet().iterator().next();
						device.setArea(areaDAO.getAreaById(areaId));
					}

				}

			}

			if (deviceVO.getCityName() != null) {

				Long cityId = cityDAO.getCityIdByName(deviceVO.getCityName());
				
				CityVO cityvo = cityDAO.getCityById(cityId);

				City city = new City();

				BeanUtils.copyProperties(cityvo, city);

				if (city != null) {
					log.debug("city  " + city.getCityName()
							+ deviceVO.getCityName());

					device.setCity(city);
				}

			}

			device.setAddedOn(new Date());

			device.setModifiedOn(new Date());

			deviceDAO.addDevice(device);
		}
		return ResponseUtil.createSaveSuccessResponse();
	}

	public APIResponse addTablets(List<TabletVO> tabletVOs)
	{

		if (tabletVOs == null)
			return ResponseUtil.createNullParameterResponse();

		for (TabletVO tabletVo : tabletVOs)
		{
			Tablet tablet = new Tablet();

			User userByAssignedTo = null;

			BeanUtils.copyProperties(tabletVo, tablet);

			if (tabletVo.getCurrentUserName() != null)
			{
				userByAssignedTo = userDao
						.getUserByUserName(tabletVo.getCurrentUserName());
			}
			if (tabletVo.getUserByAssignedTo() == null)
			{

				if (userByAssignedTo != null)
				{

					tablet.setUserByAssignedTo(userByAssignedTo);

					tablet.setUserByCurrentUser(userByAssignedTo);
				}
			}

			else if (tabletVo.getUserByCurrentUser() == null)
			{

				tablet.setUserByCurrentUser(userByAssignedTo);
			}

			// tabletDAO.addTablet(tablet);
		}

		return ResponseUtil.createSaveSuccessResponse();

	}

	public APIResponse addUsers(List<UserVo> userVos)
	{

		if (userVos == null || userVos.size() == 0)
			return ResponseUtil.createNullParameterResponse();

		for (UserVo userVo : userVos)
		{
			log.debug("user vo ========= " + userVo);
			if (userVo.getDevices() != null)
			{
				Set<TypeAheadVo> deviceVos = userVo.getDevices();

				Set<TypeAheadVo> devices = new HashSet<TypeAheadVo>();

				for (TypeAheadVo typeAheadVo : deviceVos)
				{
					long deviceId = deviceDAO
							.getDeviceIdByDeviceName(typeAheadVo.getName());
					if (deviceId > 0)
					{
						typeAheadVo.setId(deviceId);
						devices.add(typeAheadVo);
					}
				}
				userVo.setDevices(devices);

			}

			if (userVo.getUserGroups() != null)
			{
				Set<TypeAheadVo> groupVos = userVo.getUserGroups();
				Set<TypeAheadVo> groups = new HashSet<TypeAheadVo>();

				for (TypeAheadVo typeAheadVo : groupVos)
				{
					long groupId = userGroupDAO
							.getUserGroupIdByGroupName(typeAheadVo.getName());
					if (groupId > 0)
					{
						typeAheadVo.setId(groupId);
						groups.add(typeAheadVo);
					}
				}
				userVo.setUserGroups(groups);
			}

			if (userVo.getCity() != null)
			{

				TypeAheadVo aheadVo = userVo.getCity();
				City city = cityDAO.getCityByCityCode(aheadVo.getName());

				if (city != null)
				{
					aheadVo.setId(city.getId());
					userVo.setCity(aheadVo);
				}

				else
					userVo.setCity(null);
			}

			if (userVo.getBranch() != null)
			{

				TypeAheadVo aheadVo = userVo.getBranch();
				Branch branch = branchDAO
						.getBranchByBranchCode(aheadVo.getName());

				if (branch != null)
				{
					aheadVo.setId(branch.getId());
					userVo.setBranch(aheadVo);
				}

				else
				{
					Branch branch1 = branchDAO
							.getBranchByBranchName(aheadVo.getName());

					if (branch1 != null)
					{

						aheadVo.setId(branch1.getId());
						userVo.setBranch(aheadVo);
					}

					else
						userVo.setBranch(null);
				}

			}

			if (userVo.getTablet() != null)
			{

				TypeAheadVo aheadVo = userVo.getTablet();
				long tabId = tabletDAO
						.getTabletIdByTabletMac(aheadVo.getName());
				if (tabId > 0)
				{
					aheadVo.setId(tabletDAO
							.getTabletIdByTabletMac(aheadVo.getName()));
					userVo.setTablet(aheadVo);
				} else
					userVo.setTablet(null);
			}

			if (userVo.getReportTo() != null)
			{

				TypeAheadVo aheadVo = userVo.getReportTo();
				UserVo vo = null;
				if(aheadVo.getId() != null && !(aheadVo.getId() <= 0))
				 {
					vo = userDao.getUserByEmpId(aheadVo.getId());
				}
				else
				vo = userDao.getUserByLoginId(aheadVo.getName());
				
				
				if (vo != null)
				{
					log.info("report to user id ========= " + vo.getId());
					aheadVo.setId(vo.getId());
					userVo.setReportTo(aheadVo);
				} else
				{
					userVo.setReportTo(null);
				}
			}

			if (userVo.getEmployer() != null)
			{
				TypeAheadVo aheadVo = userVo.getEmployer();
				long vendorId = vendorDAO
						.getVendorIdByVendorName(aheadVo.getName());
				if (vendorId > 0)
				{
					aheadVo.setId(vendorId);
					userVo.setEmployer(aheadVo);
				} else
					userVo.setEmployer(null);
			}

			if (userVo.getSkills() != null)
			{

				Set<TypeAheadVo> skillVos = userVo.getSkills();
				Set<TypeAheadVo> skills = new HashSet<TypeAheadVo>();

				for (TypeAheadVo typeAheadVo : skillVos)
				{
					long skillId = skillDAO
							.getSkillIdBySkillName(typeAheadVo.getName());

					if (skillId > 0)
					{
						typeAheadVo.setId(skillId);
						skills.add(typeAheadVo);
					}
				}
				userVo.setSkills(skills);
			}
			log.debug("user vo ========= " + userVo);
			try
			{
				userDao.addUser(userVo);
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ResponseUtil.createSaveSuccessResponse();
	}

	@Override
	public List<String> getAllDeviceNames()
	{

		return deviceDAO.getAllDeviceNames();
	}

	@Override
	public void addBulkDevice(Set<DeviceToolVo> deviceToolVos)
	{
		log.info("Total device from Gis are " + deviceToolVos.size());

		List<Device> devicesFromDB = deviceDAO.getAllDevice();

		Map<String, DeviceToolVo> fxAndGisDeviceMap = new ConcurrentHashMap<>();
		Map<String, Device> fxAndDeviceMapFromDBMap = new ConcurrentHashMap<>();

		Set<String> devicesFromGisList = new LinkedHashSet<>();
		Set<String> devicesFromDBList = new LinkedHashSet<>();

		List<DeviceToolVo> deviceToAdd = new ArrayList<>();

		for (DeviceToolVo deviceToolVo : deviceToolVos)
		{
			String deviceFromGis = deviceToolVo.getDeviceName().toLowerCase();
			fxAndGisDeviceMap.put(deviceFromGis, deviceToolVo);
			devicesFromGisList.add(deviceFromGis);
		}

		for (Device device : devicesFromDB)
		{
			String deviceFromDB = device.getDeviceName().toLowerCase();
			fxAndDeviceMapFromDBMap.put(deviceFromDB, device);
			devicesFromDBList.add(deviceFromDB);
		}

//		Set<String> tempDevicesFromGisList = new LinkedHashSet<>();
//		tempDevicesFromGisList.addAll(devicesFromGisList);

		log.info("Device from DB " + devicesFromDBList.size());
//		log.info("Before Removing device from DB, Device from GIS are "
//				+ tempDevicesFromGisList.size());

		log.info("Before Removing device from DB, Device from GIS are "
				+ devicesFromGisList.size());
		
		devicesFromGisList.removeAll(devicesFromDBList);

		log.info("After Removing device from DB, Device from GIS are "
				+ devicesFromGisList.size());

		for (String deviceFromGis : devicesFromGisList)
		{
			if (fxAndGisDeviceMap!=null && fxAndGisDeviceMap.containsKey(deviceFromGis))
			{
				deviceToAdd.add(fxAndGisDeviceMap.get(deviceFromGis));
			}
		}

		log.info("############## Device To Add are " + deviceToAdd.size());

		deviceDAO.addBulkDevice(getDeviceToAdd(deviceToAdd));
		
		String basePath = toolsExcelsDefinition
				.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVCIE_BASE_PATH);

		String fileName = toolsExcelsDefinition
				.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_NAME);

		String archivePath = toolsExcelsDefinition
				.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVCIE_ARCHIVE_PATH);

		readExcels.moveFileToArchive(fileName, basePath, archivePath);

	}

	private List<Device> getDeviceToAdd(List<DeviceToolVo> deviceToolVos)
	{
		List<Device> devicesToAdd = new ArrayList<>();

		if (deviceToolVos == null || deviceToolVos.isEmpty())
		{
			return new ArrayList<>();

		} else
		{
			for (Iterator<DeviceToolVo> iterator = deviceToolVos
					.iterator(); iterator.hasNext();)
			{
				DeviceToolVo deviceToolVo = (DeviceToolVo) iterator.next();

				Device device = getDevcie(deviceToolVo);

				devicesToAdd.add(device);

			}
		}

		return devicesToAdd;

	}

	private Device getDevcie(DeviceToolVo deviceToolVo)
	{
		Device device = new Device();
		device.setId(StrictMicroSecondTimeBasedGuid.newGuid());

//		System.out.println("deviceToolVo.getCity() " + deviceToolVo.getCity());

		device.setCity(cityDAO.getCityByCityName(deviceToolVo.getCity()));

//		System.out
//				.println("deviceToolVo.getBranch()" + deviceToolVo.getBranch());
		device.setBranch(branchDAO
				.getBranchByBranchCode(deviceToolVo.getBranch()));

		if (deviceToolVo.getArea() != null && !deviceToolVo.getArea().isEmpty())
			device.setArea(areaDAO.getAreaByAreaName(deviceToolVo.getArea()));

		device.setDeviceName(deviceToolVo.getDeviceName().toUpperCase());
		device.setIpAddress(deviceToolVo.getIpAddress());
		device.setMacAddress(deviceToolVo.getMacAddress());

//		System.out.println("deviceToolVo.getDeviceType() "
//				+ deviceToolVo.getDeviceType());

		if (deviceToolVo.getDeviceType().equalsIgnoreCase("FX"))
		{
			device.setDeviceType(DeviceType.FX);

		} else if (deviceToolVo.getDeviceType().equalsIgnoreCase("CX"))
		{
			device.setDeviceType(DeviceType.CX);
		}

		device.setAvailablePort(deviceToolVo.getAvailablePort() != null
				? Integer.parseInt(deviceToolVo.getAvailablePort()) : 0);

		device.setTotalPort(deviceToolVo.getTotalPort() != null
				? Integer.parseInt(deviceToolVo.getTotalPort()) : 0);

		device.setUsedPort(deviceToolVo.getUsedPort() != null
				? Integer.parseInt(deviceToolVo.getUsedPort()) : 0);

		device.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
		device.setAddedOn(new Date());
		device.setModifiedBy(FWMPConstant.SYSTEM_ENGINE);
		device.setModifiedOn(new Date());
		device.setStatus(deviceToolVo.getStatus());

		return device;
	}
	
	
	@Override
	public APIResponse addDefaultSalesUserForAreas(List<UserVo> mappings)
	{
		List<DefaultUserAreaMapping> mappingsData = new ArrayList<DefaultUserAreaMapping>();
		for (Iterator iterator = mappings.iterator(); iterator.hasNext();)
		{

			UserVo userVo = (UserVo) iterator.next();
			if (userVo.getArea() != null && userVo.getArea().size() > 0)
			{
				UserVo vo = userDao.getUserByLoginId(userVo.getLoginId());
				if (vo != null && vo.getArea() != null)
				{
					for (Iterator iterator2 = userVo.getArea().iterator(); iterator2
							.hasNext();)
					{
						TypeAheadVo defaultUserAreaMapping = (TypeAheadVo) iterator2
								.next();
						mappingsData
								.add(new DefaultUserAreaMapping(vo.getId(), defaultUserAreaMapping
										.getId()));
					}

				}
			}
		}
		if(mappingsData.size() > 0)
			userDao.addDefaultSalesUserForAreas(mappingsData);
		return ResponseUtil.createSaveSuccessResponse();
	}

}
