package com.cupola.fwmp.service.configuration;

import java.util.List;
import java.util.Set;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.tools.DeviceToolVo;

public interface ConfigurationCoreService
{

	APIResponse addDevices(List<DeviceVO> deviceVO);
	
	APIResponse addUsers(List<UserVo> userVOs);
	
	APIResponse addTablets(List<TabletVO> tabletVOs);
	
	List<String> getAllDeviceNames();

	void addBulkDevice(Set<DeviceToolVo> deviceToolVos);

	APIResponse addDefaultSalesUserForAreas(List<UserVo> mappings);
}
