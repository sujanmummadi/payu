package com.cupola.fwmp.service.rollback;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.dao.rollback.RollBackDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.RollBackVO;

public class RollBackServiceImpl implements RollBackService {

	private Logger log = Logger.getLogger(RollBackServiceImpl.class
			.getName());
	
	
	@Autowired
	RollBackDAO rollBackDAO;

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	EtrCalculator etrCalculator;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;
	
	@Autowired
	DBUtil dbUtil;
	
	@Override
	public APIResponse rollBackActivity(RollBackVO rollBackVO) {
		
		if(rollBackVO.getTicketId() == null || rollBackVO.getActivityId() == null)
			return ResponseUtil.createSuccessResponse().setData(ResponseUtil.createRecordNotFoundResponse());
	

		log.info("CAlling roll back " + rollBackVO);
		
		APIResponse response = rollBackDAO.confirmRollBackActivity(rollBackVO.getTicketId(),
				rollBackVO.getActivityId());
	
		log.info("Roll back response " + response);
		
		etrCalculator.calculateActivityPercentage(Long.valueOf(rollBackVO.getTicketId()),null);
		
		if (rollBackVO.getTicketId() != null
				&& rollBackVO.getActivityId() != null)
		{
			TicketLog ticketLog = new TicketLogImpl(Long.valueOf(rollBackVO
					.getTicketId()), TicketUpdateConstant.ACTIVITY_ROLLBACK, AuthUtils
					.getCurrentUserId());
			ticketLog.setActivityId(Long.valueOf(rollBackVO.getActivityId()));

			TicketUpdateGateway.logTicket(ticketLog);
		}
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			workOrderCoreService.updateWorkOrderActivityInCRM(
					rollBackVO.getTicketId() == null ? null : Long.valueOf(rollBackVO.getTicketId()), null, null, null,
					null, null, TicketUpdateConstant.ACTIVITY_ROLLBACK,rollBackVO.getActivityId());
		}
		return response;
	}
	}

		