package com.cupola.fwmp.service.rollback;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.RollBackVO;

public interface RollBackService {
	
	public APIResponse rollBackActivity(RollBackVO confirmRollBackVO);

}
