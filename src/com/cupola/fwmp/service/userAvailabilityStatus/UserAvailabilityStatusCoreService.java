package com.cupola.fwmp.service.userAvailabilityStatus;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.UserAvailabilityStatusVo;

public interface UserAvailabilityStatusCoreService {


	APIResponse addAndupdateUserAvailabilityStatus(
			UserAvailabilityStatusVo status);

}
