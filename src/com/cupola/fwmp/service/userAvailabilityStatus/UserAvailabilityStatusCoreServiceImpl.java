package com.cupola.fwmp.service.userAvailabilityStatus;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.MessageType;
import com.cupola.fwmp.FWMPConstant.UserAvailabilityStatus;
import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.userAvailabilityStatus.UserAvailabilityStatusDAO;
import com.cupola.fwmp.handler.MessageHandler;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.UserAndRegIdDetails;
import com.cupola.fwmp.vo.UserAvailabilityStatusVo;
import com.cupola.fwmp.vo.UserAvailabilityVo;

public class UserAvailabilityStatusCoreServiceImpl implements
		UserAvailabilityStatusCoreService
{

	private Logger log = Logger
			.getLogger(UserAvailabilityStatusCoreServiceImpl.class.getName());

	@Autowired
	UserAvailabilityStatusDAO userAvailabilityDAO;

	@Autowired
	TabletDAO tabletDAOImpl;

	@Autowired
	UserDao userDao;

	@Override
	public APIResponse addAndupdateUserAvailabilityStatus(
			UserAvailabilityStatusVo userStatus)
	{
		String userLoginStatusMessage = null;

		Long userId = AuthUtils.getCurrentUserId();

		int status = userStatus.getStatus();

		try
		{
			@SuppressWarnings("unused")
			int row = userAvailabilityDAO
					.addOrUpdateuserAvailabilityStatus(userId, status);
		} catch (Exception e)
		{
			log.error("Error while adding addAndupdateUserAvailabilityStatus "+e.getMessage());
			e.printStackTrace();
		}
		/*
		if (userStatus.getStatus() == UserAvailabilityStatus.USER_AVAILABILITY_STATUS)
			userLoginStatusMessage = UserAvailabilityStatus.USER_ONLINE_STATUS;

		if (userStatus.getStatus() == UserAvailabilityStatus.USER_UNAVAILABILITY_STATUS)
			userLoginStatusMessage = UserAvailabilityStatus.USER_OFFLINE_STATUS;

		List<UserAndRegIdDetails> userAndRegIdDetails = tabletDAOImpl
				.getUserAndRegIdDetails(reportTo);
		if (userAndRegIdDetails.isEmpty())
		{

			log.debug("registrationId not found as user:" + reportTo);

			return ResponseUtil.createSuccessResponse()
					.setData(userLoginStatusMessage);
		}

		UserAvailabilityVo userAvailability = new UserAvailabilityVo();
		userAvailability.setUserId(userId);
		userAvailability.setUserName(userName);
		userAvailability.setStatus(status);
		MessageVO messageVO = new MessageVO();
		messageVO.setMessageType(MessageType.USER_AVAILABILITY_STATUS);
		messageVO.setMessage(convertObjectToString(userAvailability));

		GCMMessage message = new GCMMessage();
		message.setMessage(convertObjectToString(messageVO));

		for (UserAndRegIdDetails registration : userAndRegIdDetails)

			if (registration.getRegistrationId() != null
					&& !registration.getRegistrationId().isEmpty())
			{
				
				log.debug(registration.getRegistrationId()+"*********************");

				message.setRegistrationId(registration.getRegistrationId()
						.trim());

				log.debug(message+"**********************");
				MessageHandler messageHandler = new MessageHandler();
				try
				{
					messageHandler.addInGCMMessageQueue(message);
					log.debug("Gcm Message" + message);
				} catch (Exception e)
				{
					log.error("Gcm Exception:" + e);

				}

			} else
			{
				log.debug("registration Id not found as userId" + userId);
			}*/
		return ResponseUtil.createSuccessResponse()
				.setData(userLoginStatusMessage);
	}

	public String convertObjectToString(Object message)
	{
		if (message instanceof MessageVO)
			message = (MessageVO) message;
		if (message instanceof UserAvailabilityVo)
			message = (UserAvailabilityVo) message;

		ObjectMapper mapper = new ObjectMapper();

		String dataString = "";
		try
		{
			dataString = mapper.writeValueAsString(message);

		} catch (JsonGenerationException e1)
		{
			log.error("Error while generating json "+e1.getMessage());
			e1.printStackTrace();
		} catch (JsonMappingException e1)
		{
			log.error("Error while mapping json "+e1.getMessage());
			e1.printStackTrace();
		} catch (IOException e1)
		{
			log.error("Error while converting object to string "+e1.getMessage());
			e1.printStackTrace();
		}
		return dataString;
	}
}