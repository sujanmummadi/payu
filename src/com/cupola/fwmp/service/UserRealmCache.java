package com.cupola.fwmp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.userGroup.UserGroupDAO;
import com.cupola.fwmp.vo.UserGroupVo;

public class UserRealmCache
{
	static final Logger logger = Logger
			.getLogger(UserRealmCache.class);

	private static UserDao userDao;
	private static UserGroupDAO userGroupDAO;

	private static Map<Long, UserGroupVo> userGroupCache = new HashMap<Long, UserGroupVo>();
	
	private static Map<Long, List<Long>> userGroupUserCache = new HashMap<Long, List<Long>>();
	
	private static long cacheAgeInMillis ;
	private static long cacheResetTimestamp = 0;

	private static  void resetCacheIfOld()
	{
		if ((System.currentTimeMillis() - cacheResetTimestamp) > cacheAgeInMillis)
		{
			reFreshCache();
		}
	}

	public static  void resetCache()
	{
		userGroupUserCache.clear();
		userGroupCache.clear();
		cacheResetTimestamp = System.currentTimeMillis();
		logger.info("UserRealmCache cache reset done");
	}

	public static  void reFreshCache()
	{
		resetCache();
		List<UserGroupVo> groups = userGroupDAO.getAllUserGroups();
		
		if(groups == null)
			return;
		
		for(UserGroupVo group : groups)
		{
			userGroupCache.put(group.getId(), group);
		}
	}

	
	
	
	
	

	public static long getCacheAgeInMillis()
	{
		return cacheAgeInMillis;
	}

	public static void setCacheAgeInMillis(long cacheAgeInMillis)
	{
		UserRealmCache.cacheAgeInMillis = cacheAgeInMillis;
	}
}
