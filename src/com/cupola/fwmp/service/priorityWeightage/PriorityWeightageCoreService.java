package com.cupola.fwmp.service.priorityWeightage;

import com.cupola.fwmp.persistance.entities.PriorityWeightage;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.PriorityWeightageVo;

public interface PriorityWeightageCoreService {

	public APIResponse addPriorityWeightage(PriorityWeightage priorityWeightage);

	public APIResponse getPriorityWeightageById(Long id);
	public APIResponse getPriorityWeightageByName(String key);
	public APIResponse getAllPriorityWeightage();

	public APIResponse deletePriorityWeightage(Long id);

	public APIResponse updatePriorityWeightage(PriorityWeightage priorityWeightage);
}
