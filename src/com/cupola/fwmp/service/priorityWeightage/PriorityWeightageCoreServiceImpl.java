package com.cupola.fwmp.service.priorityWeightage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.priorityWeightage.PriorityWeightageDAO;
import com.cupola.fwmp.persistance.entities.PriorityWeightage;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.PriorityWeightageVo;

public class PriorityWeightageCoreServiceImpl implements
		PriorityWeightageCoreService
{
	private static Logger log = LogManager.getLogger(PriorityWeightageCoreServiceImpl.class
			.getName());
	@Autowired
	PriorityWeightageDAO priorityWeightageDAO;

	public static Map<String, PriorityWeightageVo> weightageNameBasedCache = new ConcurrentHashMap<String, PriorityWeightageVo>();
	public static Map<Long, PriorityWeightageVo> weightageIdBasedCache = new ConcurrentHashMap<Long, PriorityWeightageVo>();

	public void init()
	{

		log.info("From init");

		List<PriorityWeightage> list = priorityWeightageDAO
				.getAllPriorityWeightage();

		List<PriorityWeightageVo> listVo = new ArrayList<>();

		for (PriorityWeightage priorityWeightage : list)
		{

			PriorityWeightageVo priorityWeightageVo = new PriorityWeightageVo();

			BeanUtils.copyProperties(priorityWeightage, priorityWeightageVo);

			listVo.add(priorityWeightageVo);

			weightageNameBasedCache
					.put(priorityWeightage.getName(), priorityWeightageVo);
			weightageIdBasedCache
					.put(priorityWeightage.getId(), priorityWeightageVo);

		}

	}

	@Override
	public APIResponse addPriorityWeightage(PriorityWeightage priorityWeightage)
	{
		// TODO Auto-generated method stub

		addInCache(priorityWeightage);
		return null;
	}

	@Override
	public APIResponse getPriorityWeightageById(Long id)
	{
		if (weightageIdBasedCache!=null && weightageIdBasedCache.containsKey(id))
		{
			return ResponseUtil.recordFoundSucessFully()
					.setData(weightageIdBasedCache.get(id));

		} else
		{
			init();
			if (weightageIdBasedCache!=null && weightageIdBasedCache.containsKey(id))
			{
				return ResponseUtil.recordFoundSucessFully()
						.setData(weightageIdBasedCache.get(id));

			} else
			{
				return ResponseUtil.createRecordNotFoundResponse();
			}
		}
	}

	@Override
	public APIResponse getPriorityWeightageByName(String key)
	{
		if (weightageNameBasedCache!=null && weightageNameBasedCache.containsKey(key))
		{
			return ResponseUtil.recordFoundSucessFully()
					.setData(weightageNameBasedCache.get(key));

		} else
		{
			init();

			if (weightageNameBasedCache!=null && weightageNameBasedCache.containsKey(key))
			{
				return ResponseUtil.recordFoundSucessFully()
						.setData(weightageNameBasedCache.get(key));

			} else
			{
				return ResponseUtil.createRecordNotFoundResponse();
			}
		}
	}

	@Override
	public APIResponse getAllPriorityWeightage()
	{

		if (weightageNameBasedCache.isEmpty())
		{

			log.info("from if of getAllPriorityWeightage");
			init();

			return ResponseUtil.createSaveSuccessResponse()
					.setData(weightageNameBasedCache);

		} else
		{

			log.info("from cache");

			return ResponseUtil.createSaveSuccessResponse()
					.setData(weightageNameBasedCache);
		}
	}

	@Override
	public APIResponse deletePriorityWeightage(Long id)
	{
		// TODO Auto-generated method stub

		removeFromCache(id);
		return null;
	}

	@Override
	public APIResponse updatePriorityWeightage(
			PriorityWeightage priorityWeightage)
	{
		// TODO Auto-generated method stub
		return null;
	}

	private void addInCache(PriorityWeightage priorityWeightage)
	{

		PriorityWeightageVo priorityWeightageVo = new PriorityWeightageVo();

		BeanUtils.copyProperties(priorityWeightage, priorityWeightageVo);

		if (weightageNameBasedCache!=null && weightageNameBasedCache.containsKey(priorityWeightage.getName()))
		{
			weightageNameBasedCache
					.put(priorityWeightage.getName(), priorityWeightageVo);
			weightageIdBasedCache
					.put(priorityWeightage.getId(), priorityWeightageVo);

		} else
		{

			if(priorityWeightage.getName() != null)
				weightageNameBasedCache
					.put(priorityWeightage.getName(), priorityWeightageVo);
			
			if(priorityWeightage.getId() != null)
				weightageIdBasedCache
					.put(priorityWeightage.getId(), priorityWeightageVo);
		}

	}

	private void removeFromCache(Long key)
	{

		if (weightageIdBasedCache!=null && weightageIdBasedCache.containsKey(key))
		{
			weightageNameBasedCache.remove(weightageNameBasedCache.get(key));
			weightageNameBasedCache.remove(key);

		}

	}

}
