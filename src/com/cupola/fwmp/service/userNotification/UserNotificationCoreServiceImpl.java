package com.cupola.fwmp.service.userNotification;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.MessageType;
import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.userNotification.UserNotificationDAO;
import com.cupola.fwmp.handler.MessageHandler;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.UserAndRegIdDetails;
import com.cupola.fwmp.vo.UserNotificationToGCMVo;
import com.cupola.fwmp.vo.UserNotificationVo;

public class UserNotificationCoreServiceImpl
		implements UserNotificationCoreService
{

	private Logger log = Logger
			.getLogger(UserNotificationCoreServiceImpl.class.getName());

	@Autowired
	TicketDAO ticketDAOImpl;

	@Autowired
	TabletDAO tabletDAOImpl;

	@Autowired
	UserNotificationDAO userNotificationDAO;

	@Autowired
	DefinitionCoreServiceImpl definitionCoreServiceImpl;

	@Override
	public void sendUserNotification(UserNotificationVo userVo)
	{

		String reasonMessage = null;
		String messageType = null;
		Ticket ticket = null;

		log.info("UserNotificationVo" + userVo);

		if (userVo == null)
		{
			log.info("found  userVo null");
			return;
		}
		if (userVo.getTicketId() == null)
		{
			log.info("found ticketId null");
			return;
		}
		if (userVo.getStatus() == null)
		{
			log.info("found status null");
			return;
		}

		try
		{
			reasonMessage = definitionCoreServiceImpl
					.getNotificationMessageFromProperties(userVo.getStatus());

			log.info("reasonMessage::" + reasonMessage);

			messageType = definitionCoreServiceImpl
					.getMessagesForCustomerNotificationFromProperties(reasonMessage)
					+ " " + new Date();
			log.info("messageType::" + messageType);

		} catch (Exception e)
		{
			log.error("Exception in sending user notification "
					+ e.getMessage());

			e.printStackTrace();
		}

		if (reasonMessage == null || messageType == null)
		{
			log.info("Not found reason meassge in NotificationMessageFromProperties");

			return;

		}

		try
		{
			ticket = ticketDAOImpl.getTicketById(userVo.getTicketId());
			if (ticket == null)
			{
				log.info("found ticket null");
				return;
			}

			if (ticket.getUserByCurrentAssignedTo() == null)
			{
				log.info("not found userId as ticketId:"
						+ userVo.getTicketId());
				return;
			}
			Long userId = ticket.getUserByCurrentAssignedTo().getId();

			List<UserAndRegIdDetails> userAndRegIdDetails = tabletDAOImpl
					.getUserAndRegIdDetails(userId);

			if (userAndRegIdDetails.isEmpty())
			{

				log.info("registrationId not found as user:" + userId);
				return;
			}

			log.info("ReasonMessage:" + reasonMessage);

			MessageVO messageVO = new MessageVO();

			UserNotificationToGCMVo userNotificationToGCMVo = new UserNotificationToGCMVo();

			if (definitionCoreServiceImpl.isDocumetStatus(userVo.getStatus()))
			{
				messageVO.setMessageType(MessageType.DOCUMENTS__STATUS);
			} else if (definitionCoreServiceImpl
					.isPaymentStatus(userVo.getStatus()))
			{
				messageVO.setMessageType(MessageType.PAYMENT__STATUS);
			}

			userNotificationToGCMVo.setMessage(messageType);
			userNotificationToGCMVo.setTicketId(userVo.getTicketId());
			userNotificationToGCMVo
					.setStatusId(Integer.valueOf(userVo.getStatus()));
			userNotificationToGCMVo.setRemarks(userVo.getRemarks());
			userNotificationToGCMVo.setReasonCode(userVo.getReasonCode());
			userNotificationToGCMVo.setReasonValue(userVo.getReasonValue());

			messageVO
					.setMessage(convertObjectToString(userNotificationToGCMVo));

			GCMMessage message = new GCMMessage();

			MessageHandler messageHandler = new MessageHandler();
			message.setMessage(convertObjectToString(messageVO));

			for (UserAndRegIdDetails registration : userAndRegIdDetails)
			{

				if (registration.getRegistrationId() != null
						&& !registration.getRegistrationId().isEmpty())
				{

					message.setRegistrationId(registration.getRegistrationId()
							.trim());

					try
					{
						messageHandler.addInGCMMessageQueue(message);
						log.debug("Gcm Message" + message);
					} catch (Exception e)
					{
						log.error("Gcm Exception:" + e);
					}

				} else
				{
					log.info("registration Id not found as userId" + userId);
				}
			}

		} catch (Exception e)
		{
			log.error("Ticket not foiund for the ticketID "
					+ userVo.getTicketId());
			e.printStackTrace();
		}

	}

	@Override
	public void sendGCmNotificationToUser(List<Long> userId, MessageVO vo)
	{
		if (userId == null || userId.isEmpty())
			return;

		log.info("User to whom send the notifications are " + userId
				+ " message is " + vo);

		List<String> listOfRegistrationId = userNotificationDAO
				.getRegistrationIdByUserId(userId);

		if (listOfRegistrationId == null || listOfRegistrationId.isEmpty())
		{
			log.info("registration Id not found as user " + userId);
			return;
		}

		log.info("Total tab registed with user id " + userId + " are "
				+ listOfRegistrationId.size());

		GCMMessage message = new GCMMessage();
		MessageHandler messageHandler = new MessageHandler();

		message.setMessage(convertObjectToString(vo));

		for (String registration : listOfRegistrationId)
		{

			if (registration != null && !registration.isEmpty())
			{

				message.setRegistrationId(registration.trim());
				try
				{
					messageHandler.addInGCMMessageQueue(message);
					log.debug("Gcm Message" + message);
					
				} catch (Exception e)
				{
					log.error("Gcm Exception:" + e);
				}

			} else
			{
				log.info("registration Id not found as userId" + userId);
			}
		}
	}

	@Override
	public List<Long> getUserIdByTicketNumber(Long ticketNo)
	{
		return userNotificationDAO.getUserByTicketNumber(ticketNo);
	}

	public String convertObjectToString(Object message)
	{
		if (message instanceof MessageVO)
			message = (MessageVO) message;
		if (message instanceof UserNotificationToGCMVo)
			message = (UserNotificationToGCMVo) message;

		ObjectMapper mapper = new ObjectMapper();

		String dataString = "";
		try
		{
			dataString = mapper.writeValueAsString(message);

		} catch (JsonGenerationException e1)
		{
			e1.printStackTrace();
		} catch (JsonMappingException e1)
		{
			e1.printStackTrace();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
		return dataString;
	}
}
