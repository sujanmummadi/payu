package com.cupola.fwmp.service.userNotification;

import java.util.List;

import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.UserNotificationVo;

public interface UserNotificationCoreService
{

	void sendUserNotification(UserNotificationVo userVo);

	void sendGCmNotificationToUser(List<Long> userId, MessageVO vo);

	List<Long> getUserIdByTicketNumber(Long ticketNo);

}


