package com.cupola.fwmp.service.releases;

import com.cupola.fwmp.response.APIResponse;

public interface ReleaseCoreService
{

	APIResponse getReleases(String version);

	public APIResponse updateRelease(Long cityId, String newVersion, Long branchId);

	APIResponse resetVersionCache();

}
