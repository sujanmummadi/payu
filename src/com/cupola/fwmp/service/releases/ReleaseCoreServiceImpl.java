package com.cupola.fwmp.service.releases;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.releases.ReleaseDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;

public class ReleaseCoreServiceImpl implements ReleaseCoreService
{

	@Autowired
	ReleaseDAO releaseDAO;

	@Override
	public APIResponse getReleases(String version)
	{
		return  ResponseUtil.createSuccessResponse().setData(releaseDAO.getReleases(version));
	}

	@Override
	public APIResponse updateRelease(Long cityId, String newVersion, Long branchId)
	{
		return releaseDAO.updateRelease(cityId, newVersion,branchId);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.releases.ReleaseCoreService#resetVersionCache()
	 */
	@Override
	public APIResponse resetVersionCache()
	{
		return  ResponseUtil.createSuccessResponse().setData(releaseDAO.resetVersionCache());
	}

}
