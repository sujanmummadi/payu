package com.cupola.fwmp.service.device;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public interface DeviceService {
	
	public APIResponse addDevice(Device device);

	public APIResponse getDeviceById(Long id);

	public APIResponse getAllDevice();

	public APIResponse deleteDevice(Long id);

	public APIResponse updateDevice(Device device);

	public APIResponse addDevices(List<DeviceVO> devices);

	public APIResponse getDevices(Long cityId, Long branchId);

	public APIResponse getDevicesForUser(Long userId);

	public APIResponse mapDevicesForUser(List<TypeAheadVo> deviceIds, Long userId);

	APIResponse getDevicesByKey(Long cityId, Long branchId, String key);
	
	
	/**
	 * @throws Exception
	 * @Author : Sharan.Shastri
	 */
	public Workbook userDeviceBulkUploadFileFormat(Workbook workbook) throws Exception;

	/**
	 * @Author : Sharan.Shastri
	 */
	public Workbook mapDeviceForUserInBulk(Workbook workbook);

}
