package com.cupola.fwmp.service.device;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public class DeviceServiceImpl implements DeviceService{

	private static final Logger LOGGER = LogManager
			.getLogger(DeviceServiceImpl.class.getName());
	@Autowired
	DeviceDAO deviceDAO;
	
	@Override
	public APIResponse addDevice(Device device) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getDeviceById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getAllDevice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse deleteDevice(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateDevice(Device device) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse addDevices(List<DeviceVO> devices)
	{
		LOGGER.info("Adding Device "+devices);
		if(devices == null || devices.isEmpty())
			return ResponseUtil.createNullParameterResponse();
	
		for (Iterator iterator = devices.iterator(); iterator.hasNext();)
		{
			DeviceVO deviceVO = (DeviceVO) iterator.next();
			if(deviceVO.getBranchId() <= 0 || deviceVO.getCityId() <= 0 || deviceVO.getDeviceName() == null )
			{
				return ResponseUtil.createNullParameterResponse();
			}
		}
		deviceDAO.addDevices( devices);
		return ResponseUtil.createSaveSuccessResponse();
	}
	
	public APIResponse getDevices(Long cityId, Long branchId )
	{
		Map<Long, String>  devices = deviceDAO.getDevices(  cityId,   branchId,null);
		if(devices !=null && !devices.isEmpty())
			return ResponseUtil.createSuccessResponse().setData(devices.values());
		else
			return ResponseUtil.createSuccessResponse().setData(null);
	}
	@Override
	public APIResponse getDevicesByKey(Long cityId, Long branchId, String key)
	{
		Map<Long, String>  devices = deviceDAO.getDevices(  cityId,   branchId,key);
		
		return ResponseUtil.createSuccessResponse().setData(CommonUtil.xformToTypeAheadFormat(devices));
	}
	@Override
	public APIResponse getDevicesForUser(Long userId)
	{
		
		Map<Long, String>  devices = deviceDAO.getDevicesForUser(userId);
		return ResponseUtil.createSuccessResponse().setData(CommonUtil.xformToTypeAheadFormat(devices));
	}

	@Override
	public APIResponse mapDevicesForUser(List<TypeAheadVo> deviceIds, Long userId)
	{
		LOGGER.info("Mapping Device with user "+userId +" >> "+deviceIds);
		if(userId == null || deviceIds == null || deviceIds.isEmpty())
			return ResponseUtil.createNullParameterResponse();
	
	 
		deviceDAO.mapDevicesForUser(deviceIds, userId);
		return ResponseUtil.createSaveSuccessResponse();
	}
	
	/**
	 * @Author : Sharan.Shastri
	 * 
	 *         workbook sheet management for user-device-bulk-upload-file-format
	 */
	@Override
	public Workbook userDeviceBulkUploadFileFormat(Workbook workbook) throws Exception {

		try {

			Sheet sheetnew = workbook.createSheet("User_Device_BulkUpload_File_Format");
			Row row = sheetnew.createRow(0);
			row.createCell(0);
			row.getCell(0).setCellValue("LoginId");
			row.createCell(1);
			row.getCell(1).setCellValue("DeviceName");

			return workbook;

		} catch (Exception e) {
			return workbook;
		}

	}

	/**
	 * @Author : Sharan.Shastri
	 * 
	 *         parse workbook row by row to DAO and returns the updated workbook
	 */
	@Override
	public Workbook mapDeviceForUserInBulk(Workbook workBook) {

		DataFormatter formatter = new DataFormatter();

		Iterator<Row> rowIterator = workBook.getSheetAt(0).rowIterator();
		int count = 0;
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (count == 0) {
				/** creating new column in workbook */
				row.createCell(2);
				row.getCell(2).setCellValue("Upload Status");

			} else {

				String loginId = formatter.formatCellValue(row.getCell(0));
				String deviceName = formatter.formatCellValue(row.getCell(1));

				if ((loginId.equals("") || loginId.equals(null))) {
					row.createCell(2);
					row.getCell(2).setCellValue("loginId should not be NULL");
				} else if (((deviceName.equals("") || deviceName.equals(null)))) {
					row.createCell(2);
					row.getCell(2).setCellValue("deviceName should not be NULL");
				} else {
					row.createCell(2);
					row.getCell(2).setCellValue(deviceDAO.mapDeviceForUserInBulk(loginId, deviceName));
				}
			}
			count++;
		}

		return workBook;
	}
}
