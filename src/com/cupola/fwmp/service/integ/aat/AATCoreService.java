package com.cupola.fwmp.service.integ.aat;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.CustomerClosures;
import com.cupola.fwmp.vo.aat.AATEligibilityInput;

public interface AATCoreService
{

	public APIResponse getAATDetailsforCustomer(CustomerClosures aatResponse);

	APIResponse isAllFWMPActivityDone(AATEligibilityInput aatEligibilityInput);
	
	// APIResponse getDemo();
}
