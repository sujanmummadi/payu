package com.cupola.fwmp.service.integ.aat;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.FWMPConstant.MessagePlaceHolder;
import com.cupola.fwmp.FWMPConstant.MessageType;
import com.cupola.fwmp.FWMPConstant.PortActivation;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.integ.aat.AATDAO;
import com.cupola.fwmp.dao.integ.aat.AATResponseVo;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.ticket.TicketDetailDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAOImpl;
import com.cupola.fwmp.handler.MessageHandler;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CustomerClosures;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.GCMMessage;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TabletInfoVo;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.aat.AATEligibilityInput;
import com.cupola.fwmp.vo.aat.AATEligibilityOutput;

public class AATCoreServiceImpl implements AATCoreService
{

	private final static String secretKey = "AIzaSyC_wo57AKZnbcWXy8hFMhdguyy7ASdKEUOFM123";
	private static Logger log = LogManager
			.getLogger(AATCoreServiceImpl.class.getName());

	@Autowired
	AATDAO aatDAOImpl;

	@Autowired
	GisDAO gisDAO;

	@Autowired
	TicketActivityLogDAOImpl ticketActivityLogDAOImpl;

	@Autowired
	TicketDetailDAO ticketDetailDAO;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	DefinitionCoreService coreService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	EtrCalculator etrCalculatorImpl;

	@Autowired
	DefinitionCoreService definitionCoreService;
	
	@Autowired 
	WorkOrderCoreService workOrderCoreService;
	
	@Autowired
	DBUtil dbUtil;

	@Override
	public APIResponse getAATDetailsforCustomer(
			CustomerClosures customerClosures)
	{

		TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();
		GISPojo gis = new GISPojo();

		if (customerClosures == null)
		{
			log.info("found CustomerClosures null");
			return ResponseUtil.createFailureResponse();
		}
		if (customerClosures.getSecretKey() == null
				|| customerClosures.getSecretKey().isEmpty())
		{
			log.info("invalid User");
			return ResponseUtil.createBadCredentialsExceptionResponse();
		}

		if (!customerClosures.getSecretKey()
				.equals(AATCoreServiceImpl.secretKey))
		{
			log.info("invalid User");
			return ResponseUtil.createBadCredentialsExceptionResponse();
		}

		if (customerClosures.getMqId() == null
				|| customerClosures.getMqId().isEmpty())
		{
			log.info("found mqId null or empty");
			return ResponseUtil.createFailureResponse()
					.setData("found mqId null");
		}

		List<TabletInfoVo> tabletInfoVo = aatDAOImpl
				.getAATDetailsforCustomer(customerClosures.getMqId());

		if (tabletInfoVo == null || tabletInfoVo.isEmpty())
		{
			log.info("No customer or user  found as mqId:"
					+ customerClosures.getMqId());
			return ResponseUtil.createFailureResponse();
		}

		if (tabletInfoVo.get(0).getTicketId() == null
				|| tabletInfoVo.get(0).getTicketId() == 0)
		{

			log.info("ticket Id not found as mqid:"
					+ customerClosures.getMqId());
			return ResponseUtil.createSuccessResponse();

		}
		log.info("found userLogin in :" + tabletInfoVo.size() + ":device");

		try
		{

			if (tabletInfoVo.get(0).getTicketId() != null)
			{
				ticketActivityLogVo.setModifiedOn(new Date());
				ticketActivityLogVo.setAddedOn(new Date());
				ticketActivityLogVo
						.setTicketId(tabletInfoVo.get(0).getTicketId());
				ticketActivityLogVo
						.setActivityId(PortActivation.PORT_ACTIVATION);

				ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);

				int activityUpdate = ticketActivityLogDAOImpl
						.updateWorkOrderActivity(ticketActivityLogVo);

				if (activityUpdate <= 0)
				{
					log.info("Ticket not found in WorkOrder as mqId::"
							+ customerClosures.getMqId());
					return ResponseUtil.createSuccessResponse();
				}

				CustomerVO customer = customerDAO
						.getCustomerByTicketId(tabletInfoVo.get(0)
								.getTicketId());

				MessageNotificationVo notificationVo = new MessageNotificationVo();
				notificationVo.setEmail(customer.getEmailId());
				notificationVo.setMobileNumber(customer.getMobileNumber());

				String statusMessageCode = coreService
						.getNotificationMessageFromProperties("111");

				String messageToSend = coreService
						.getMessagesForCustomerNotificationFromProperties(statusMessageCode);

				if (messageToSend != null)
				{
					String helpLineNo = definitionCoreService
							.getNIHelpLineNumber(LocationCache
									.getCityCodeById(customer.getCityId()));
					String portalUrl = definitionCoreService
							.getPortalPageUrl(LocationCache
									.getCityCodeById(customer.getCityId()));

					messageToSend = messageToSend
							.replaceAll(MessagePlaceHolder.HELP_LINE_NUMBER, helpLineNo)
							.replaceAll(MessagePlaceHolder.PORTAL_URL, portalUrl);

					log.debug("aditya ############# test sasassq "
							+ messageToSend);

				}
				notificationVo.setTicketId(tabletInfoVo.get(0).getTicketId()+"");

				notificationVo.setMessage(messageToSend);

				notificationService.sendNotification(notificationVo);

				log.info("Invoking  notificationService MessageNotificationVo :"
						+ notificationVo);

				// ticketActivityLogVo.setStatus(customerClosures.getActivationStatus());

				for (Entry<Long, String> entry : PortActivation.activityPort
						.entrySet())
				{
					ticketActivityLogVo
							.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					ticketActivityLogVo.setSubActivityId(entry.getKey());
					ticketActivityLogDAOImpl
							.addTicketActivityLog(ticketActivityLogVo);

					ticketActivityLogDAOImpl
							.updateTicketStatus(ticketActivityLogVo);
				}

				if (activityUpdate > 0)
				{
					if (tabletInfoVo.get(0).getUserId() != null)
					{
						TicketLog logDetail = new TicketLogImpl(tabletInfoVo
								.get(0)
								.getTicketId(), TicketUpdateConstant.ACTIVITY_UPDATED, tabletInfoVo
										.get(0).getUserId());
						logDetail.setActivityId(ticketActivityLogVo
								.getActivityId());
						logDetail.setActivityValue(ticketActivityLogVo
								.getStatus() + "");

						TicketUpdateGateway.logTicket(logDetail);

					}
					StatusVO status = new StatusVO();
					status.setTicketId(tabletInfoVo.get(0).getTicketId());
					status.setStatus(TicketStatus.PORT_ACTIVATION_SUCCESSFULL);
					status.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
					
					String currentCRM = dbUtil.getCurrentCrmName(null, null);

					if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
						
						workOrderCoreService.updateWorkOrderActivityInCRM(status.getTicketId(), customer.getId(), status.getPropspectNo(), status.getWorkOrderNumber(), null, null, FWMPConstant.SiebelTicketStatus.PORT_ACTIVATION_SUCCESSFULL,null);
					}
					
					
					ticketDetailDAO.updateTicketStatus(status);

				}

			}

		} catch (Exception e)
		{
			log.error("Error while updating activity log during aat update"
					+ e.getMessage());
			e.printStackTrace();
		}

		Long ticketId = tabletInfoVo.get(0).getTicketId();

		gis.setCxIpAddress(customerClosures.getCxIpAddress());
		gis.setCxName(customerClosures.getCxName());
		gis.setCxMacAddress(customerClosures.getCxMacAddress());
		gis.setFxIpAddress(customerClosures.getFxIpAddress());
		gis.setFxMacAddress(customerClosures.getFxMacAddress());
		gis.setFxName(customerClosures.getFxName());
		gis.setCxPorts(customerClosures.getCxPorts());
		gis.setTicketNo(ticketId);
		gis.setFxPorts(customerClosures.getFxPorts());
		gis.setFeasibilityType(FeasibilityType.DEPLOYMENT_AAT);

		try
		{
			long gisId = gisDAO.insertGISInfo(gis);

			log.info("Gis response for ticket id " + ticketId + " is " + gisId);

		} catch (Exception e)
		{
			log.error("Error while updating gis for ticket Id " + ticketId
					+ " . Error message " + e.getMessage());
			e.printStackTrace();
		}
		AATResponseVo aatResponseVo = new AATResponseVo();
		aatResponseVo.setDeviceIp(customerClosures.getCxIpAddress());
		// aatResponseVo.setDeviceType(customerClosures.getc);
		aatResponseVo
				.setPortNumber(String.valueOf(customerClosures.getCxPorts()));
		aatResponseVo
				.setTicketId(String.valueOf(tabletInfoVo.get(0).getTicketId()));

		MessageVO messageVO = new MessageVO();
		messageVO.setMessageType(MessageType.AAT_ACTIVATION);
		messageVO.setMessage(convertObjectToString(aatResponseVo));
		GCMMessage message = new GCMMessage();
		message.setMessage(convertObjectToString(messageVO));

		for (TabletInfoVo tablet : tabletInfoVo)
		{

			if (tablet.getRegistrationId() != null
					&& !tablet.getRegistrationId().isEmpty())
			{

				message.setRegistrationId(tablet.getRegistrationId().trim());

				MessageHandler messageHandler = new MessageHandler();

				messageHandler.addInGCMMessageQueue(message);

				log.debug("message send  to GCMServer as userId:+"
						+ tablet.getUserId() + " with registrationId: "
						+ tablet.getRegistrationId() + "and" + "message:"
						+ messageVO.getMessage());
			} else
			{
				log.info("registration Id not found as ticketId"
						+ tablet.getTicketId());
			}
		}

		try
		{
			if (tabletInfoVo.get(0).getTicketId() != null)
			{
				etrCalculatorImpl.calculateActivityPercentage(tabletInfoVo
						.get(0).getTicketId(), null);
			} else
			{
				log.debug("ticket id not found");
			}

		} catch (Exception e)
		{
			log.error("Error While calculatingActivityPercentage  :"
					+ e.getMessage());
			e.printStackTrace();
		}
		/*
		 * GCMNotificationDAOImpl gcmnotification = new
		 * GCMNotificationDAOImpl(); String str =
		 * gcmnotification.sendGcmNotificationToApp(gcmMessage); // call add
		 * method to messageHandler add in gcm Queue log.debug(
		 * "send notification to gcm:" + gcmnotification);
		 */

		return ResponseUtil.createSuccessResponse();
	}

	public String convertObjectToString(Object message)
	{
		if (message instanceof MessageVO)
			message = (MessageVO) message;
		if (message instanceof AATResponseVo)
			message = (AATResponseVo) message;

		ObjectMapper mapper = new ObjectMapper();

		String dataString = "";
		try
		{
			dataString = mapper.writeValueAsString(message);

		} catch (JsonGenerationException e1)
		{
			log.error("exception while AATCoreService convertObjectToString ::"
					+ e1.getMessage());
		} catch (JsonMappingException e1)
		{
			log.error("exception while AATCoreService convertObjectToString ::"
					+ e1.getMessage());
		} catch (IOException e1)
		{
			log.error("exception while AATCoreService convertObjectToString ::"
					+ e1.getMessage());
		}
		return dataString;
	}

	@Override
	public APIResponse isAllFWMPActivityDone(
			AATEligibilityInput aatEligibilityInput)
	{
		log.info("Checking eligibility of mqId " + aatEligibilityInput.getMqId()
				+ " for AAT, city " + aatEligibilityInput.getCityCode());

		AATEligibilityOutput aatEligibilityOutput = aatDAOImpl
				.isAllFWMPActivityDone(aatEligibilityInput);

		log.info("Checked eligibility of mqId " + aatEligibilityInput.getMqId()
				+ " for AAT, city " + aatEligibilityInput.getCityCode()
				+ " and output is " + aatEligibilityOutput);

		return ResponseUtil.createSuccessResponse()
				.setData(aatEligibilityOutput);
	}

	/*
	 * @Override public APIResponse getDemo() { List<CustomerClosuresVo> cust =
	 * aatDAOImpl.getDemo(); APIResponse api = null; if (!cust.isEmpty()) {
	 * CustomerClosuresVo c = cust.get(0); log.info(c +
	 * "CustomerClosuresVo***********"); CustomerClosures customerClosures = new
	 * CustomerClosures(); customerClosures.setUserName(c.getUserName());
	 * customerClosures.setCity(c.getCity());
	 * customerClosures.setCustomerId(c.getCustomerId());
	 * customerClosures.setDeviceIp(c.getDeviceIp());
	 * customerClosures.setMqClosureStatus(c.getMqClosureStatus());
	 * customerClosures.setMqId(c.getMqId());
	 * customerClosures.setSecretKey(secretKey); api =
	 * getAATDetailsforCustomer(customerClosures);
	 * 
	 * log.info(api + "apiresponse************************");
	 * 
	 * } return api;
	 * 
	 * }
	 */
}
