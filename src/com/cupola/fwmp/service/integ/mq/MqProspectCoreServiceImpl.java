/**
 * 
 */
package com.cupola.fwmp.service.integ.mq;

import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.integ.prospect.pojo.AddressInfo;
import com.cupola.fwmp.dao.integ.prospect.pojo.ContactInfo;
import com.cupola.fwmp.dao.integ.prospect.pojo.FlexAttributeInfo;
import com.cupola.fwmp.dao.integ.prospect.pojo.MQProspectRequest;
import com.cupola.fwmp.dao.integ.prospect.pojo.ProspectInfo;
import com.cupola.fwmp.dao.integ.prospect.pojo.TrailPlanInfo;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.ProspectCoreVO;

/**
 * @author aditya
 * 
 */
public class MqProspectCoreServiceImpl implements MqProspectCoreService
{
	private static Logger log = LogManager
			.getLogger(MqProspectCoreServiceImpl.class);
	@Autowired
	BranchDAO branchDAO;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Override
	public MQProspectRequest convertTicket2MqProspect(ProspectCoreVO prospect)
	{
		log.info("Converting propspect in mq format " + prospect);

		MQProspectRequest mqProspectRequest = new MQProspectRequest();

		ProspectInfo prospectInfo = new ProspectInfo();

		prospectInfo.setProspectNo("");
		if (prospect.getCustomer().getTitle() == null)
			prospect.getCustomer().setTitle("Mr.");

		prospectInfo.setTitle(prospect.getCustomer().getTitle());

		prospectInfo.setFirstName(prospect.getCustomer().getFirstName());

		if (prospect.getCustomer().getMiddleName() == null)
			prospect.getCustomer().setMiddleName("");
		prospectInfo.setMiddleName(prospect.getCustomer().getMiddleName());
		prospectInfo.setLastName(prospect.getCustomer().getLastName());

		String cityName = cityDAO
				.getCityNameById(prospect.getCustomer().getCityId());
		cityName = cityName != null ? cityName.toUpperCase() : cityName;
		log.info("Getting city id for mobile  "
				+ prospect.getCustomer().getMobileNumber() + " " + cityName);
		String branchCode = branchDAO
				.getBranchCodeById(prospect.getCustomer().getBranchId());

		branchCode = branchCode != null ? branchCode.toUpperCase() : branchCode;
		log.info("Getting branchCode id for mobile  "
				+ prospect.getCustomer().getMobileNumber() + " " + branchCode);
		
		if(branchCode != null && FWMPConstant.REALTY.equalsIgnoreCase(branchCode))
		{
			log.info("Got branchCode for mobile  "
					+ prospect.getCustomer().getMobileNumber() + " " + branchCode);
			
			if (cityName != null && cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY)) {
				prospectInfo.setCustomerType(definitionCoreService
						.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_CUSTOMER_TYPE_VALUE_HYD));
				log.info("branchCode for mobile  " + prospect.getCustomer().getMobileNumber() + " is " + branchCode
						+ ". This belongs to" + CityName.HYDERABAD_CITY + " and customer Type is " + prospectInfo.getCustomerType());

			} else {
				prospectInfo.setCustomerType(definitionCoreService
						.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_CUSTOMER_TYPE_VALUE_ROI));
				log.info("branchCode for mobile  " + prospect.getCustomer().getMobileNumber() + " is " + branchCode
						+ ". This belongs to ROI");

			}
			
		} else
		{
			log.info("branchCode for mobile  " + prospect.getCustomer().getMobileNumber() + " is " + branchCode
					+ ". This belongs to Non realty branch");
			prospectInfo.setCustomerType("N");
		}
	
		log.info("Getting prospectInfo.getCustomerType for mobile  "
				+ prospect.getCustomer().getMobileNumber() + " " + prospectInfo.getCustomerType());

		
		String areaCode = areaDAO
				.getAreaNameById(prospect.getCustomer().getAreaId());

		areaCode = areaCode != null ? areaCode.toUpperCase() : areaCode;

		log.info("Getting areaCode id for mobile  "
				+ prospect.getCustomer().getMobileNumber() + " " + areaCode);

		if (cityName != null && cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY) && branchCode != null
				&& branchCode.equalsIgnoreCase("DEFAULT"))
		{
			prospectInfo.setOpentity("HYD_IEEE");

		} else
		{
			prospectInfo.setOpentity(branchCode != null ? branchCode.toUpperCase() : ""); // set branch
																// code as per
																// mq
		}
		prospectInfo.setCompanyName("");

		if (cityName != null && cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY))
		{
			if ((prospect.getCustomer().getNotes() == null
					|| (prospect.getCustomer().getNotes() != null
							&& prospect.getCustomer().getNotes().isEmpty())))
				prospectInfo.setNotes(CityName.HYDERABAD_CITY);
			else
				prospectInfo.setNotes((prospect.getCustomer().getNotes()));
		} else
			prospectInfo.setNotes("");

		if (prospect.getCustomer().getPrefferedCallDate() != null)
			prospectInfo.setPreferredCallDate(GenericUtil
					.convertToMqDateFormat(prospect.getCustomer()
							.getPrefferedCallDate()));
		else
			prospectInfo.setPreferredCallDate(GenericUtil
					.convertToUiDateFormat(GenericUtil.addDays(new Date(), 2)));

		ContactInfo contactInfo = new ContactInfo();
		contactInfo.setAlteMail(prospect.getCustomer().getAlternativeEmailId());
		contactInfo.setContactName("");
		contactInfo.setEmail(prospect.getCustomer().getEmailId());

		if (prospect.getCustomer().getAlternativeEmailId() == null)
			prospect.getCustomer().setAlternativeEmailId("");

		else
			prospect.getCustomer().setAlternativeEmailId(prospect.getCustomer()
					.getAlternativeEmailId());

		contactInfo.setAlteMail(prospect.getCustomer().getEmailId());

		contactInfo.setFaxNbr("");
		contactInfo.setHomePhone(prospect.getCustomer().getAlternativeMobileNo());
		contactInfo.setMobilePhone(prospect.getCustomer().getMobileNumber());
		contactInfo.setWorkPhone("");
		TrailPlanInfo trailPlanInfo = new TrailPlanInfo();

		AddressInfo addressInfo = new AddressInfo();

		if (prospect.getCustomer().getCurrentAddress() != null)
			addressInfo.setAddress1(CommonUtil
					.replace(prospect.getCustomer().getCurrentAddress()));
		else
			addressInfo.setAddress1(prospect.getCustomer().getCurrentAddress());

		addressInfo.setAddress2("");
		addressInfo.setAddressTypeCode("PRI");

		addressInfo.setArea(areaCode); // area
										// as
										// per
										// MQ
		addressInfo.setCity(cityName != null ? cityName.toUpperCase() : "");

		addressInfo.setState(cityDAO
				.getStateNameByCityId(prospect.getCustomer().getCityId())
				.toUpperCase());
		if(prospect.getCustomer().getPincode() != null)
			addressInfo.setZipCode(prospect.getCustomer().getPincode().toString());
		else
			addressInfo.setZipCode("");

		addressInfo.setCountry(FWMPConstant.COUNTRY_INDIA);

		FlexAttributeInfo flexAttributeInfo = new FlexAttributeInfo();

		flexAttributeInfo.setAttribute1(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_ONE_VALUES));
		
		flexAttributeInfo.setAttribute2(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));
	
		flexAttributeInfo.setAttribute3(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_THREE_VALUES));
	
		flexAttributeInfo.setAttribute4(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_FOUR_VALUES));
		
		if (prospect.getCustomer().getEnquiryModeId() != null)
			flexAttributeInfo.setAttribute1(definitionCoreService
					.getEnqueryById(prospect.getCustomer().getEnquiryModeId()));
		else
			flexAttributeInfo.setAttribute1(definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_ONE_VALUES));

		if (cityName != null && cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY))
		{
			/*if (prospect.getCustomer().getSourceModeId() != null)
			{
				flexAttributeInfo.setAttribute2(definitionCoreService
						.getSourceById(prospect.getCustomer()
								.getSourceModeId(), prospect.getCustomer()
										.getCityId()));
				System.out
						.println("ROI Additional attributes 2 check prospect.getCustomer().getSourceModeId() "
								+ definitionCoreService.getSourceById(prospect
										.getCustomer()
										.getSourceModeId(), prospect
												.getCustomer().getCityId()));
			} else*/
				flexAttributeInfo.setAttribute2(definitionCoreService
						.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));
		} else
			flexAttributeInfo.setAttribute2(definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_TWO_VALUES));

		// if (cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY))
		// {
		// log.info("HYD Additional attributes 3 check");
		// flexAttributeInfo
		// .setAttribute3("Outdoors (Hoardings/Buses/Pole boards etc)");
		// } else
		// {
		log.info("ROI Additional attributes 3 check");
		if (prospect.getCustomer().getSourceModeId() != null)
		{
			flexAttributeInfo
					.setAttribute3(definitionCoreService.getSourceById(prospect
							.getCustomer().getSourceModeId(), prospect
									.getCustomer().getCityId()));
			
			log.info("ROI Additional attributes 3 check prospect.getCustomer().getSourceModeId() "
					+ definitionCoreService.getSourceById(prospect.getCustomer()
							.getSourceModeId(), prospect.getCustomer()
									.getCityId()));

		} else
		{
			flexAttributeInfo.setAttribute3(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_THREE_VALUES));
		}

		// }

		if (cityName != null && cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY))
			flexAttributeInfo.setAttribute4(definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_FOUR_VALUES));
		else
			flexAttributeInfo.setAttribute4(definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_ATTRIBUTE_FOUR_VALUES));

		flexAttributeInfo.setAttribute5("");
		flexAttributeInfo.setAttribute6("");
		flexAttributeInfo.setAttribute7("");
		flexAttributeInfo.setAttribute8("");
		flexAttributeInfo.setAttribute9("");
		flexAttributeInfo.setAttribute10("");
		flexAttributeInfo.setAttribute11("");
		flexAttributeInfo.setAttribute12("");
		flexAttributeInfo.setAttribute13("");
		flexAttributeInfo.setAttribute14("");
		flexAttributeInfo.setAttribute15("");
		flexAttributeInfo.setAttribute16("");

		mqProspectRequest.setProspectInfo(prospectInfo);
		mqProspectRequest.setContactInfo(contactInfo);
		mqProspectRequest.setTrailPlanInfo(trailPlanInfo);
		mqProspectRequest.setAddressInfo(addressInfo);
		mqProspectRequest.setFlexAttributeInfo(flexAttributeInfo);

		log.info("############ mqProspectRequest " + mqProspectRequest);

		return mqProspectRequest;
	}

}
