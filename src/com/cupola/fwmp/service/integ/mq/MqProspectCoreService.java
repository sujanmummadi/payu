/**
 * 
 */
package com.cupola.fwmp.service.integ.mq;

import com.cupola.fwmp.dao.integ.prospect.pojo.MQProspectRequest;
import com.cupola.fwmp.vo.ProspectCoreVO;

/**
 * @author aditya
 * 
 */
public interface MqProspectCoreService
{

	public MQProspectRequest convertTicket2MqProspect(ProspectCoreVO prospect);

}
