/**
* @Author aditya  11-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.service.integ.crm;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.tools.siebel.to.vo.FaultRepairActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.last3ServiceImpacting.Last3ServiceImpactingRequest;

/**
 * @Author aditya 11-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

public interface CrmServiceCall {

	APIResponse callSiebelServiceForCreateUpdateProspect(SalesActivityUpdateInCRMVo prospect);

	APIResponse callSiebelServiceForCreateUpdateWorkOrder(WorkOrderActivityUpdateInCRMVo workOrder);

	APIResponse callSiebelServiceForCreateUpdateFaultRepair(FaultRepairActivityUpdateInCRMVo ticket);
	
	APIResponse callSiebelServiceForLast3ServiceImpactingTickets(Last3ServiceImpactingRequest serviceRequest);
	
}
