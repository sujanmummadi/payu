/**
* @Author aditya  11-Oct-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.service.integ.crm;

import java.io.IOException;
import java.util.Date;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.CreateUpdateFaultRepairResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.from.vo.faultRepair.last3ServiceImpacting.Last3ServiceImpactingResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.from.vo.prospect.ProspectResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.from.vo.workorder.CreateUpdateWorkOrderResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.log.SiebelFaultRepairLogger;
import com.cupola.fwmp.vo.tools.siebel.log.SiebelLast3ServiceLogger;
import com.cupola.fwmp.vo.tools.siebel.log.SiebelProspectLogger;
import com.cupola.fwmp.vo.tools.siebel.log.SiebelWorkOrderLogger;
import com.cupola.fwmp.vo.tools.siebel.to.vo.FaultRepairActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.CreateUpdateFaultRepairRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.last3ServiceImpacting.Last3ServiceImpactingRequest;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.CreateUpdateWorkOrderRequestInSiebel;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @Author aditya 11-Oct-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class CrmServiceCallImpl implements CrmServiceCall {

	static final Logger logger = Logger.getLogger(CrmServiceCallImpl.class);

	private String siebelURIForCreateUpdateProspect;
	private String siebelURIForCreateUpdateWorkOrder;
	private String siebelURIForCreateUpdateFaultRepair;
	private String siebelURIForLast3Service;

	@Autowired
	MongoTemplate mongoTemplate;

	/**
	 * @return the siebelURIForCreateUpdateProspect
	 */
	public String getSiebelURIForCreateUpdateProspect() {
		return siebelURIForCreateUpdateProspect;
	}

	/**
	 * @param siebelURIForCreateUpdateProspect
	 *            the siebelURIForCreateUpdateProspect to set
	 */
	public void setSiebelURIForCreateUpdateProspect(String siebelURIForCreateUpdateProspect) {
		this.siebelURIForCreateUpdateProspect = siebelURIForCreateUpdateProspect;
	}

	/**
	 * @return the siebelURIForCreateUpdateWorkOrder
	 */
	public String getSiebelURIForCreateUpdateWorkOrder() {
		return siebelURIForCreateUpdateWorkOrder;
	}

	/**
	 * @param siebelURIForCreateUpdateWorkOrder
	 *            the siebelURIForCreateUpdateWorkOrder to set
	 */
	public void setSiebelURIForCreateUpdateWorkOrder(String siebelURIForCreateUpdateWorkOrder) {
		this.siebelURIForCreateUpdateWorkOrder = siebelURIForCreateUpdateWorkOrder;
	}

	/**
	 * @return the siebelURIForCreateUpdateFaultRepair
	 */
	public String getSiebelURIForCreateUpdateFaultRepair() {
		return siebelURIForCreateUpdateFaultRepair;
	}

	/**
	 * @param siebelURIForCreateUpdateFaultRepair
	 *            the siebelURIForCreateUpdateFaultRepair to set
	 */
	public void setSiebelURIForCreateUpdateFaultRepair(String siebelURIForCreateUpdateFaultRepair) {
		this.siebelURIForCreateUpdateFaultRepair = siebelURIForCreateUpdateFaultRepair;
	}

	/**
	 * @return the siebelURIForLast3Service
	 */
	public String getSiebelURIForLast3Service() {
		return siebelURIForLast3Service;
	}

	/**
	 * @param siebelURIForLast3Service
	 *            the siebelURIForLast3Service to set
	 */
	public void setSiebelURIForLast3Service(String siebelURIForLast3Service) {
		this.siebelURIForLast3Service = siebelURIForLast3Service;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.integ.crm.CrmServiceCall#
	 * callSiebelServiceForCreateUpdateProspect(com.cupola.fwmp.vo.tools.siebel.to.
	 * vo.SalesActivityUpdateInCRMVo)
	 */
	@Override
	public APIResponse callSiebelServiceForCreateUpdateProspect(SalesActivityUpdateInCRMVo prospect) {

		SiebelProspectLogger siebelLogger = new SiebelProspectLogger();

		siebelLogger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		siebelLogger.setRequestTime(new Date().toString());

		APIResponse apiResponse = null;

		CreateUpdateProspectRequestInSiebel createUpdateProspectInSiebel = prospect.getCreateUpdateProspectInSiebel();

		logger.info("calling Siebel Service For Create Update Prospect ");

		String resetMainQueueUrlWithIp = siebelURIForCreateUpdateProspect;

		logger.info(
				"Siebel Service For Create Update Prospect modified URI ############### " + resetMainQueueUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try {

			ObjectMapper mapper = CommonUtil.getObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetMainQueueUrlWithIp);
//			createUpdateProspectInSiebel.getCreateUpdateProspect_Input().getListOfActcreateprospectthinio()
//					.getActListMgmtProspectiveContactBc().setProspectAssignedTo("T528816");
			String mainQueueString = mapper.writeValueAsString(createUpdateProspectInSiebel);

			mainQueueString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(createUpdateProspectInSiebel);

			siebelLogger.setUri(resetMainQueueUrlWithIp);
			siebelLogger.setRequest(mainQueueString);

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			logger.info(mainQueueString + " Siebel Service For Create Update Prospect Request  "
					+ createUpdateProspectInSiebel);

			httpResponse = httpclient.execute(httpPost);

			// logger.info(mainQueueString + " Siebel Service For Create Update Prospect
			// Request " + createUpdateProspectInSiebel);

			String stringResponse = EntityUtils.toString(httpResponse.getEntity());

			logger.info(" Siebel Service For Create Update Prospect response " + stringResponse);

			ProspectResponseFromSiebel fromSiebel = mapper.readValue(stringResponse, ProspectResponseFromSiebel.class);

			logger.info("ProspectResponseFromSiebel " + fromSiebel);

			siebelLogger.setFromSiebel(fromSiebel);
			siebelLogger.setResponse(stringResponse);
			siebelLogger.setResponseTime(new Date().toString());

			apiResponse = ResponseUtil.createSuccessResponse().setData(stringResponse);

		} catch (Exception e) {

			logger.error("Error while calling Siebel Service For Create Update Prospect for Ticket "
					+ prospect.getTicketId() + " and Transaction id " + prospect.getTransactionId() + " prospect "
					+ createUpdateProspectInSiebel + " . Error message ## " + e.getMessage(), e);

			e.printStackTrace();

		} finally {
			try {
				if (httpResponse != null)
					httpResponse.close();

				if (httpclient != null)
					httpclient.close();

			} catch (IOException e) {
				logger.error(
						"Error in finally block, while closing httpresponse for Siebel Service For Create Update Prospect . "
								+ e.getMessage());
				e.printStackTrace();
			}

			mongoTemplate.insert(siebelLogger);
		}

		logger.info("Siebel Service For Create Update Prospect final " + createUpdateProspectInSiebel);

		return apiResponse;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.integ.crm.CrmServiceCall#
	 * callSiebelServiceForCreateUpdateWorkOrder(com.cupola.fwmp.vo.tools.siebel.to.
	 * vo.WorkOrderActivityUpdateInCRMVo)
	 */
	@Override
	public APIResponse callSiebelServiceForCreateUpdateWorkOrder(WorkOrderActivityUpdateInCRMVo workOrder) {
		
		SiebelWorkOrderLogger siebelLogger = new SiebelWorkOrderLogger();
		
		siebelLogger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		siebelLogger.setRequestTime(new Date().toString());

		APIResponse apiResponse = null;

		CreateUpdateWorkOrderRequestInSiebel createUpdateWorkOrderRequestInSiebel = workOrder
				.getCreateUpdateWorkOrderRequestInSiebel();

		logger.info("calling Siebel Service For Create Update WorkOrder ");

		String resetMainQueueUrlWithIp = siebelURIForCreateUpdateWorkOrder;

		logger.info(
				"Siebel Service For Create Update WorkOrder modified URI ############### " + resetMainQueueUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try {

			ObjectMapper mapper = CommonUtil.getObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetMainQueueUrlWithIp);

			String mainQueueString = mapper.writeValueAsString(createUpdateWorkOrderRequestInSiebel);

			mainQueueString = mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(createUpdateWorkOrderRequestInSiebel);
			
			siebelLogger.setUri(resetMainQueueUrlWithIp);
			siebelLogger.setRequest(mainQueueString);

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			logger.info(mainQueueString + " Siebel Service For Create Update WorkOrder Request  "
					+ createUpdateWorkOrderRequestInSiebel);

			httpResponse = httpclient.execute(httpPost);

			// logger.info(mainQueueString + " Siebel Service For Create Update Prospect
			// Request " + createUpdateProspectInSiebel);

			String stringResponse = EntityUtils.toString(httpResponse.getEntity());

			logger.info(" Siebel Service For Create Update WorkOrder response " + stringResponse);

			CreateUpdateWorkOrderResponseFromSiebel fromSiebel = mapper.readValue(stringResponse,
					CreateUpdateWorkOrderResponseFromSiebel.class);

			logger.info("WorkOrderResponseFromSiebel " + fromSiebel);

			siebelLogger.setFromSiebel(fromSiebel);
			siebelLogger.setResponse(stringResponse);
			siebelLogger.setResponseTime(new Date().toString());

			apiResponse = ResponseUtil.createSuccessResponse().setData(stringResponse);

			httpclient.close();

		} catch (Exception e) {

			logger.error("Error while calling Siebel Service For Create Update WorkOrder for Ticket "
					+ workOrder.getTicketId() + " and Transaction id " + workOrder.getTransactionId() + " prospect "
					+ createUpdateWorkOrderRequestInSiebel + " . Error message ## " + e.getMessage(), e);

			e.printStackTrace();

		} finally {
			try {
				if (httpResponse != null)
					httpResponse.close();

				if (httpclient != null)
					httpclient.close();

			} catch (IOException e) {
				logger.error(
						"Error in finally block, while closing httpresponse for Siebel Service For Create Update WorkOrder . "
								+ e.getMessage());
				e.printStackTrace();
			}
			mongoTemplate.insert(siebelLogger);
		}

		logger.info("Siebel Service For Create Update WorkOrder final " + createUpdateWorkOrderRequestInSiebel);

		return apiResponse;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.integ.crm.CrmServiceCall#
	 * callSiebelServiceForCreateUpdateFaultRepair(com.cupola.fwmp.vo.tools.siebel.
	 * to.vo.FaultRepairActivityUpdateInCRMVo)
	 */
	@Override
	public APIResponse callSiebelServiceForCreateUpdateFaultRepair(FaultRepairActivityUpdateInCRMVo ticket) {

		SiebelFaultRepairLogger siebelLogger = new SiebelFaultRepairLogger();
		
		siebelLogger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		siebelLogger.setRequestTime(new Date().toString());

		
		APIResponse apiResponse = null;

		CreateUpdateFaultRepairRequestInSiebel createUpdateWorkOrderRequestInSiebel = ticket
				.getCreateUpdateFaultRepairRequestInSiebel();

		logger.info("calling Siebel Service For Create Update Fault Repair ");

		String resetMainQueueUrlWithIp = siebelURIForCreateUpdateFaultRepair;

		logger.info("Siebel Service For Create Update Fault Repair modified URI ############### "
				+ resetMainQueueUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try {

			ObjectMapper mapper = CommonUtil.getObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetMainQueueUrlWithIp);

			String mainQueueString = mapper.writeValueAsString(createUpdateWorkOrderRequestInSiebel);

			mainQueueString = mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(createUpdateWorkOrderRequestInSiebel);

			siebelLogger.setUri(resetMainQueueUrlWithIp);
			siebelLogger.setRequest(mainQueueString);
			
			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			logger.info(mainQueueString + " Siebel Service For Create Update Fault Repair Request  "
					+ createUpdateWorkOrderRequestInSiebel);

			httpResponse = httpclient.execute(httpPost);

			// logger.info(mainQueueString + " Siebel Service For Create Update Prospect
			// Request " + createUpdateProspectInSiebel);

			String stringResponse = EntityUtils.toString(httpResponse.getEntity());

			logger.info(" Siebel Service For Create Update Fault Repair response " + stringResponse);

			CreateUpdateFaultRepairResponseFromSiebel fromSiebel = mapper.readValue(stringResponse,
					CreateUpdateFaultRepairResponseFromSiebel.class);

			logger.info("FaultRepairResponseFromSiebel " + fromSiebel);
			
			siebelLogger.setFromSiebel(fromSiebel);
			siebelLogger.setResponse(stringResponse);
			siebelLogger.setResponseTime(new Date().toString());

			
			apiResponse = ResponseUtil.createSuccessResponse().setData(fromSiebel);

			httpclient.close();

		} catch (Exception e) {

			logger.error("Error while calling Siebel Service For Create Update Fault Repair for Ticket "
					+ ticket.getTicketId() + " and Transaction id " + ticket.getTransactionId() + " prospect "
					+ createUpdateWorkOrderRequestInSiebel + " . Error message ## " + e.getMessage(), e);

			e.printStackTrace();

		} finally {
			try {
				if (httpResponse != null)
					httpResponse.close();

				if (httpclient != null)
					httpclient.close();

			} catch (IOException e) {
				logger.error(
						"Error in finally block, while closing httpresponse for Siebel Service For Create Update Fault Repair . "
								+ e.getMessage());
				e.printStackTrace();
			}
			mongoTemplate.insert(siebelLogger);
		}

		logger.info("Siebel Service For Create Update Fault Repair final " + createUpdateWorkOrderRequestInSiebel);

		return apiResponse;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.integ.crm.CrmServiceCall#
	 * callSiebelServiceForLast3ServiceImpactingTickets(com.cupola.fwmp.vo.tools.
	 * siebel.to.vo.Last3ServiceImpactingRequest)
	 */
	@Override
	public APIResponse callSiebelServiceForLast3ServiceImpactingTickets(Last3ServiceImpactingRequest serviceRequest) {

		SiebelLast3ServiceLogger siebelLogger = new SiebelLast3ServiceLogger();
		
		siebelLogger.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		siebelLogger.setRequestTime(new Date().toString());

		APIResponse apiResponse = null;

		logger.info("calling Siebel Service For Last three services impacting ");

		String last3ServiceImpactUrlWithIp = siebelURIForLast3Service;

		logger.info("Siebel Service For Last three service impacting modified URI ############### "
				+ last3ServiceImpactUrlWithIp);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try {

			ObjectMapper mapper = CommonUtil.getObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(last3ServiceImpactUrlWithIp);

			String requestString = mapper.writeValueAsString(serviceRequest);

			requestString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceRequest);

			siebelLogger.setUri(last3ServiceImpactUrlWithIp);
			siebelLogger.setRequest(requestString);

			
			StringEntity stringEntity = new StringEntity(requestString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			logger.info(requestString + " Siebel Service For Last three services impact Request  " + serviceRequest);

			httpResponse = httpclient.execute(httpPost);

			// logger.info(mainQueueString + " Siebel Service For Create Update Prospect
			// Request " + createUpdateProspectInSiebel);

			String stringResponse = EntityUtils.toString(httpResponse.getEntity());

			logger.info(" Siebel Service For Last three services impacting response" + stringResponse);

			Last3ServiceImpactingResponseFromSiebel fromSiebel = mapper.readValue(stringResponse,
					Last3ServiceImpactingResponseFromSiebel.class);

			logger.info("Last three service impact ResponseFromSiebel " + fromSiebel);

			apiResponse = ResponseUtil.createSuccessResponse().setData(fromSiebel);

			siebelLogger.setFromSiebel(fromSiebel);
			siebelLogger.setResponse(stringResponse);
			siebelLogger.setResponseTime(new Date().toString());

			httpclient.close();

		} catch (Exception e) {

			logger.error("Error while calling Siebel Service For Last three services impacting customer id :"
					+ serviceRequest.getQueryLast3SR_Input().getCustomerNo() + " . Error message ## " + e.getMessage(),
					e);
			e.printStackTrace();
			apiResponse.setData(ResponseUtil.createServerErrorResponse());
		} finally {
			try {
				if (httpResponse != null)
					httpResponse.close();

				if (httpclient != null)
					httpclient.close();

			} catch (IOException e) {
				logger.error(
						"Error in finally block, while closing httpresponse for Siebel Service For Last three service impacting . "
								+ e.getMessage());
				e.printStackTrace();
				apiResponse.setData(ResponseUtil.createServerErrorResponse());
			}
			
			mongoTemplate.insert(siebelLogger);
		}

		logger.info("Siebel Service For Last three services impacting request : " + serviceRequest);

		return apiResponse;

	}

}
