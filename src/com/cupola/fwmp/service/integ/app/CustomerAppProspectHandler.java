package com.cupola.fwmp.service.integ.app;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.customer.ProspectCoreService;
import com.cupola.fwmp.vo.integ.app.ProspectVO;

public class CustomerAppProspectHandler
{
	static final Logger logger = Logger
			.getLogger(CustomerAppProspectHandler.class);

	@Autowired
	ProspectCoreService prospectCoreService;

	@Autowired
	AppPropspectCoreService appPropspectCoreService;

	BlockingQueue<ProspectVO> prospectUpdateQueue;

	Thread prospectThread = null;

	BlockingQueue<Long> prospectQueue;

	Thread updateProspectThread = null;

	public void init()
	{
		logger.info("CustomerAppProspectHandler init method called..");
	
		prospectQueue = new LinkedBlockingQueue<Long>(100000);
		prospectUpdateQueue = new LinkedBlockingQueue<ProspectVO>(100000);

		if (prospectThread == null)
		{
			prospectThread = new Thread(run, "AppProspectHandler Service");
			prospectThread.start();
		}

		if (updateProspectThread == null)
		{
			updateProspectThread = new Thread(prospectUpdateQueueRun, "AppProspectHandlerUpdateService");
			updateProspectThread.start();
		}
		
		logger.info("CustomerAppProspectHandler init method executed.");
	}

	Runnable run = new Runnable()
	{
		public void run()
		{
			try
			{
				Long ticket = null;

				while (true)
				{
					ticket = prospectQueue.take();

					try
					{
						if (ticket != null)
						{
							APIResponse apiResponse = prospectCoreService
									.appProspectWrapper(ticket);

							logger.info("Converted prospect " + ticket
									+ " from mongo format to mySQL format "
									+ apiResponse);
						}

					} catch (Exception e)
					{
						logger.error("Error While Converted prospect :"
								+ e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
						continue;
					}
				}

			} catch (Exception e)
			{
				logger.error("Error in updateProspectThread Processor main loop- msg="
						+ e.getMessage());
				e.printStackTrace();
			}

			logger.info("Update Prospect thread interrupted- getting out of processing loop");

		}

	};

	public void add2CreateProspectByAppQueue(Long prospectId)
	{

		prospectQueue.add(prospectId);
	}

	public void add2UpdateProspectByAppQueue(ProspectVO prospectVO)
	{

		prospectUpdateQueue.add(prospectVO);
	}

	Runnable prospectUpdateQueueRun = new Runnable()
	{
		public void run()
		{
			try
			{
				ProspectVO ticket = null;

				while (true)
				{
					ticket = prospectUpdateQueue.take();

					try
					{
						if (ticket != null)
						{
							APIResponse apiResponse = appPropspectCoreService
									.updateAppProspect(ticket);

							logger.info("Update App Prospect " + ticket
									+ " and Response is  " + apiResponse);
						}

					} catch (Exception e)
					{
						logger.error("Error While Updating prospect from Customer App for mobilie "
								+ ticket.getMobileNumber()
								+ " having prospect no  "
								+ ticket.getProspectNumber() + ": "
								+ e.getMessage());
						// TODO Auto-generated catch block
						e.printStackTrace();
						continue;
					}
				}

			} catch (Exception e)
			{
				logger.error("Error in AppProspectHandlerUpdateService Processor main loop- msg="
						+ e.getMessage());
				e.printStackTrace();
			}

			logger.info("Update AppProspectHandlerUpdateService thread interrupted- getting out of processing loop");

		}

	};

}
