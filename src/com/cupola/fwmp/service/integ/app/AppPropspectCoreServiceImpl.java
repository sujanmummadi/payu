package com.cupola.fwmp.service.integ.app;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.ApiAction;
import com.cupola.fwmp.dao.integ.app.AppPropspectDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.customer.AppToFWMPConverter;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.integ.app.ProspectVO;

public class AppPropspectCoreServiceImpl implements AppPropspectCoreService
{

	private static Logger log = LogManager
			.getLogger(AppPropspectCoreServiceImpl.class.getName());

	@Autowired
	AppPropspectDAO appPropspectDAO;

	@Override
	public APIResponse createProspect(ProspectVO prospectVO)
	{
		log.info(" Prospect VO from App side is PropspectFormAppCoreServiceImpl "
				+ prospectVO);

		if (prospectVO.getAction().equalsIgnoreCase(ApiAction.CREATE))
		{
			log.info(" Prospect VO from App side is PropspectFormAppCoreServiceImpl CREATE Action ");
			appPropspectDAO.addPropspectInDB(prospectVO);

		} else if (prospectVO.getAction().equalsIgnoreCase(ApiAction.UPDATE))
		{
			log.info(" Prospect VO from App side is PropspectFormAppCoreServiceImpl UPDATE Action "
					+ prospectVO);

			appPropspectDAO.updatePropspectInDB(prospectVO);

		} else
		{
			return ResponseUtil.createInvalidActionType();
		}
		return ResponseUtil.createSuccessResponse().setData("Success");
	}

	@Override
	public APIResponse updateAppProspect(ProspectVO prospectVO)
	{
		int result = 0;
		
		if (prospectVO.getAction().equalsIgnoreCase(ApiAction.UPDATE))
		{
			log.info(" Prospect VO from App side is PropspectFormAppCoreServiceImpl UPDATE Action "
					+ prospectVO);

			// appPropspectDAO.updatePropspectInDB(prospectVO);

			if (prospectVO.getUpdateType()
					.equalsIgnoreCase(ApiAction.UPDATE_TYPE_TARIFF))
			{
				result = appPropspectDAO.updateTariff(prospectVO);
				log.info("Tariff plan update status is " +  result + " for prospect " + prospectVO.getProspectNumber());
			}

			if (prospectVO.getUpdateType()
					.equalsIgnoreCase(ApiAction.UPDATE_TYPE_DOCUMENT))
			{
				result = appPropspectDAO.uploadDoucuments(prospectVO);
				log.info("Document update status is " +  result + " for prospect " + prospectVO.getProspectNumber());
			}

			if (prospectVO.getUpdateType()
					.equalsIgnoreCase(ApiAction.UPDATE_TYPE_PAYMENT))
			{
				result = appPropspectDAO.updatePayment(prospectVO);
				log.info("Pyment update status is " +  result + " for prospect " + prospectVO.getProspectNumber());
			}

			return ResponseUtil.createSuccessResponse();

		} else
		{
			return ResponseUtil.createInvalidActionType();
		}
	}

	@Override
	public APIResponse getPropspectById(Long id)
	{
		log.info("Prospect VO from App side is PropspectFormAppCoreServiceImpl "
				+ id);

		return ResponseUtil.createSuccessResponse()
				.setData(appPropspectDAO.getPropspectById(id));
	}

}
