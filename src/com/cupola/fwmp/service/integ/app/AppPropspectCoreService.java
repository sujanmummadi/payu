package com.cupola.fwmp.service.integ.app;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.integ.app.ProspectVO;

public interface AppPropspectCoreService
{
	
	APIResponse createProspect(ProspectVO prospectVO);
	APIResponse getPropspectById(Long id);
	/**@author aditya 6:54:31 PM Apr 10, 2017
	 * APIResponse
	 * @param prospectVO
	 * @return
	 */
	APIResponse updateAppProspect(ProspectVO prospectVO);
}
