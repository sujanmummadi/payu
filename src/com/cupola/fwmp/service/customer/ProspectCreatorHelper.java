package com.cupola.fwmp.service.customer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.FWMPConstant.TicketSource;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.TicketVo;

public class ProspectCreatorHelper
{
	static final Logger logger = Logger.getLogger(ProspectCreatorHelper.class);
	BlockingQueue<ProspectCoreVO> prospectQueue;
	Thread updateProcessorThread = null;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	ProspectCoreService prospectCoreService;
	
	@Autowired
	DefinitionCoreService definitionCoreService;

	public void init()
	{
		logger.info("ProspectCreatorHelper enabling updateProcessorThread processing-");
		prospectQueue = new LinkedBlockingQueue<ProspectCoreVO>(100000);

		if (updateProcessorThread == null)
		{
			updateProcessorThread = new Thread(run, "ProspectCreatorHelper");
			updateProcessorThread.start();
		}
		logger.info("ProspectCreatorHelper updateProcessorThread enabled");
	}

	public void addNewProspect(ProspectCoreVO prospect, String source)
	{
		try
		{
			logger.info("Adding new prospect request for customer "
					+ prospect.getCustomer().getMobileNumber());

			if (prospect.getStatus() != null
					&& prospect.getStatus().equalsIgnoreCase("open"))
			{
				logger.info("Ticket from Server start up and adding this ticket at the time of server start "
						+ prospect.getCustomer().getMobileNumber());

			} else
			{
				logger.info("Marking pospect as open "
						+ prospect.getCustomer().getMobileNumber());

				prospect.setStatus("open");
				mongoTemplate.insert(prospect);
			}

			prospectQueue.add(prospect);

			logger.info("Depth of queue afte adding "
					+ prospect.getCustomer().getMobileNumber() + " is "
					+ prospectQueue.size());

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("error" + e.getMessage());
		}
	}

	Runnable run = new Runnable()
	{
		@Override
		public void run()
		{
			try
			{
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_DEVICE_CACHE_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true)
				{
					try
					{
						final ProspectCoreVO prospect = prospectQueue.take();

						Runnable worker = new Runnable()
						{
							@Override
							public void run()
							{
								try
								{
									// Start processing from here
									if (prospect != null
											&& prospect.getCustomer() != null)
									{
										logger.info(prospect.getCustomer()
												.getMobileNumber()
												+ " Current Processing Prospect :: "
												+ prospect.getCustomer()
														.getFirstName()
												+ " and current depth after processing is "
												+ prospectQueue.size());

										// Create Prospect
										createNewProspect(prospect);

									}

								} catch (Exception e)
								{
									e.printStackTrace();
									logger.error("Error in prospect thread "
											+ " " + prospect + ""
											+ e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e)
					{
						e.printStackTrace();

						logger.error("Error in prospect main thread "
								+ e.getMessage());
						continue;
					}

				}

			} catch (Exception e)
			{
				logger.error("error in Processor main loop- msg="
						+ e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor thread interrupted- getting out of processing loop");
		}

		/**
		 * @param prospect
		 * @throws Exception
		 */
		private void createNewProspect(ProspectCoreVO prospect) throws Exception
		{
			logger.info("Creating prospect for customer "
					+ prospect.getCustomer().getMobileNumber());
			String source = null;

//Made chnages by Manjuprasad for Seibel issue 			
/*			if (prospect.getCreationSource() != null && prospect.getCreationSource()
					.equalsIgnoreCase(CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL))
				source = prospect.getCreationSource();*/
			if (prospect.getCreationSource() != null && !prospect.getCreationSource()
					.equalsIgnoreCase(TicketSource.DOOR_KNOCK))
				source = CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL;
		
			APIResponse response = prospectCoreService.addNewProspect(prospect, source);
			
			TicketVo vo = (TicketVo) response.getData();

			Query query = new Query();

			query.addCriteria(Criteria.where("customer.mobileNumber")
					.is(prospect.getCustomer().getMobileNumber())
					.andOperator(Criteria.where("customer.firstName")
							.is(prospect.getCustomer().getFirstName()), Criteria
									.where("customer.emailId")
									.is(prospect.getCustomer().getEmailId())));

			// Update update = new Update();
			// update.set("status", "closed prospectNo " + vo.getProspectNo());
			//
			// mongoTemplate.findAndModify(query, update, ProspectCoreVO.class)
			// ;

			mongoTemplate.findAndRemove(query, ProspectCoreVO.class);

			logger.info("Prospect creation result for customer "
					+ prospect.getCustomer().getMobileNumber() + " :: "
					+ response.getStatusMessage());

		}

	};
}
