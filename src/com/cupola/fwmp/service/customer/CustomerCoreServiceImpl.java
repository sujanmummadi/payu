package com.cupola.fwmp.service.customer;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.ClassificationCategories;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticket.TicketDetailDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CustomerAadhaarMappingVo;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TicketVo;
import com.cupola.fwmp.vo.UserVo;

public class CustomerCoreServiceImpl implements CustomerCoreService
{
	
	private static Logger log = LogManager
			.getLogger(CustomerCoreServiceImpl.class.getName());


	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	TicketDetailDAO ticketDetailDAO;

	@Autowired
	GisDAO gisDAO;

	@Autowired
	UserDao userDao;

	@Autowired
	AreaDAO areaDao;
	
	@Autowired
	private DefinitionCoreService definitionCoreService;

	@Override
	public Map<String, Long> addFrCustomerViaCRM(
			List<MQWorkorderVO> mqWorkorderVOList)
	{
		Map<String, Long> prospectTicketIdMap = new ConcurrentHashMap<String, Long>();
		
		Map<String, CustomerVO> mqIdCustomerMap =customerDAO.addFrCustomerInBulk(mqWorkorderVOList);
		
		Map<String, Long> prospectNoTicketIdMap =  ticketDAO.addNewFrTicketInBulk(mqWorkorderVOList, mqIdCustomerMap);
		
		prospectTicketIdMap.putAll(prospectNoTicketIdMap);
		
		return prospectTicketIdMap;
	}
	
	@Override
	public Map<String, Long> addCustomerViaCRM(
			List<MQWorkorderVO> mqWorkorderVOList, String location)
	{
		log.info("Adding customer for " + location);
		
		Map<String, Long> prospectTicketIdMap = new ConcurrentHashMap<String, Long>();
		
		Map<String, CustomerVO> mqIdCustomerMap =customerDAO.addCustomerInBulk(mqWorkorderVOList);
	
		log.info("Adding Ticket for " + location);
		
		Map<String, Long> prospectNoTicketIdMap =  ticketDAO.addNewTicketInBulk(mqWorkorderVOList, mqIdCustomerMap);
		
		log.info("Adding GIS details for " + location);
		
		Map<Long, Long> ticketGisMap = gisDAO.insertGISInfoInBulk(mqWorkorderVOList, prospectNoTicketIdMap);
	
		log.info("Added GIS details for " + location);
		
		prospectTicketIdMap.putAll(prospectNoTicketIdMap);
//
//		for (MQWorkorderVO mqWorkorderVO : mqWorkorderVOList)
//		{
//			try
//			{
//				CustomerVO customerVo = xformToCustomer(mqWorkorderVO);
//				
//				customerVo = customerDAO.addCustomer(customerVo);
//				
//				TicketVo ticketVo = xformToTicket(mqWorkorderVO, customerVo);
//
//				long ticketId = ticketDAO.addNewTicket(ticketVo);
//
//				GISPojo gisPojo = xformToGISPojo(mqWorkorderVO, ticketId);
//
//				long gisId = gisDAO.insertGISInfo(gisPojo);
//
//				prospectTicketIdMap
//						.put(mqWorkorderVO.getProspectNumber(), ticketId);
//			} catch (Exception e)
//			{
//				continue;
//			}
//
//		}
		
		log.info("Returning prospectTicketIdMap for location " + location);
		
		return prospectTicketIdMap;
	}


	private TicketVo xformToTicket(MQWorkorderVO mqWorkorderVO,
			CustomerVO customerVo)
	{
		TicketVo ticketVo = new TicketVo();
		ticketVo.setCustomerId(customerVo.getId());

		try
		{
			if (mqWorkorderVO.getAreaId() != null)
				ticketVo.setAreaId(Long.valueOf(mqWorkorderVO.getAreaId()));
			else
				ticketVo.setAreaId(areaDao
						.getDefaultAreaByCity(customerVo.getCityId()));
		} catch (Exception e)
		{
		}

		if (mqWorkorderVO.getAssignedTo() != null)
			ticketVo.setAssignTo(mqWorkorderVO.getAssignedTo());

		if (mqWorkorderVO.getFxName() != null)
			ticketVo.setFxName(mqWorkorderVO.getFxName());

		ticketVo.setSymptomCode(mqWorkorderVO.getSymptom());
		ticketVo.setTicketCategory(mqWorkorderVO.getTicketCategory());
		if (definitionCoreService
				.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
				.contains(mqWorkorderVO.getTicketCategory())
				|| definitionCoreService
						.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_SYMPTOM)
						.contains(mqWorkorderVO
								.getSymptom()))
		{
			ticketVo.setTicketCategory(TicketCategory.NEW_INSTALLATION);
		}
		ticketVo.setProspectNo(mqWorkorderVO.getProspectNumber());
		return ticketVo;
	}

	private CustomerVO xformToCustomer(MQWorkorderVO mqWorkorderVO)
	{
		CustomerVO customerVo = new CustomerVO();
		customerVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		customerVo.setAddedOn(new Date());

		if (mqWorkorderVO.getAreaId() != null)
			customerVo.setAreaId(Long.valueOf(mqWorkorderVO.getAreaId()));

		if (mqWorkorderVO.getBranchId() != null)
			customerVo.setBranchId(Long.valueOf(mqWorkorderVO.getBranchId()));

		if (mqWorkorderVO.getCityId() != null)
			customerVo.setCityId(Long.valueOf(mqWorkorderVO.getCityId()));

		customerVo.setCommunicationAdderss(mqWorkorderVO.getCustomerAddress());
		customerVo.setCurrentAddress(mqWorkorderVO.getCustomerAddress());

		customerVo.setFirstName(mqWorkorderVO.getCustomerName());
		customerVo.setMobileNumber(mqWorkorderVO.getCustomerMobile());
		customerVo.setMqId(mqWorkorderVO.getMqId());
		return customerVo;
	}

	@Override
	public boolean getCustomerByMqId(String mqId)
	{
		long id = customerDAO.getCustomerByMqId(mqId);

		if (id <= 0)
			return true;
		else
			return false;
	}

	@Override
	public APIResponse getAllCustomer()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse deleteCustomer(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateCustomer(CustomerVO customerVo)
	{
		CustomerVO customer = customerDAO.updateCustomerDetails(customerVo);
		if(customer != null)
			return ResponseUtil.createUpdateSuccessResponse();
		else
			return ResponseUtil.createUpdateFailedResponse();
	}

	@Override
	public APIResponse updateTicket(StatusVO status)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(ticketDetailDAO.updateTicketStatus(status));
	}

	@Override
	public Map<String, Long> getTicketIdsFromProspects(
			List<String> prospectList, String location)
	{
		 
		log.info("Getting Ticket Ids From Prospects for " + location);
		
		if (prospectList.isEmpty())
			return  new ConcurrentHashMap<String, Long>();

//		for (Ticket ticketList : ticketDAO
//				.getTicketNosFromProspects(prospectList))
//		{
//			propspectTicketMap
//					.put(ticketList.getProspectNo(), ticketList.getId());
//		}

		return ticketDAO
				.getTicketNosFromProspects(prospectList, location);
	}

	@Override
	public Map<Long, String> getFxNameFromTicketId(
			List<Long> ticketList)
	{
		Map<Long, String> ticketIdAndFxNameMap = new ConcurrentHashMap<Long, String>();

		if (ticketList.isEmpty())
			return ticketIdAndFxNameMap;

		for (Gis gis : gisDAO.getGisInfo4Tickets(ticketList))
		{
			if (gis.getTicketNo() != null && gis.getFxName() != null)
				ticketIdAndFxNameMap.put(gis.getTicketNo(), gis.getFxName());
		}

		return ticketIdAndFxNameMap;
	}

	@Override
	public UserVo getUserByRoleAndFXName(String fxName,
			String role)
	{
		return userDao.getUsersByDeviceName(fxName, role);
	}

	@Override
	public GISPojo xformToGISPojo(MQWorkorderVO mqWorkorderVO,
			long ticketId)
	{
		GISPojo gisPojo = new GISPojo();

		gisPojo.setId(StrictMicroSecondTimeBasedGuid.newGuid());

		gisPojo.setAddedBy(FWMPConstant.SYSTEM_ENGINE);

		gisPojo.setAddedOn(new Date());

		if (mqWorkorderVO.getAreaId() != null)
			gisPojo.setArea(mqWorkorderVO.getAreaId());

		if (mqWorkorderVO.getBranchId() != null)
			gisPojo.setBranch(mqWorkorderVO.getBranchId());

		if (mqWorkorderVO.getCityId() != null)
			gisPojo.setCity(mqWorkorderVO.getCityId());

		gisPojo.setCxIpAddress(mqWorkorderVO.getCxIp());

		gisPojo.setFxName(mqWorkorderVO.getFxName());

		gisPojo.setStatus(1);

		gisPojo.setTicketNo(ticketId);

		gisPojo.setFeasibilityType(FeasibilityType.GIS_CLASSIFICATION);

		return gisPojo;
	}

	@Override
	public Map<Long, String> getConnectionTypeFromTicketId(
			List<Long> ticketList)
	{
		Map<Long, String> ticketIdAndFxNameMap = new ConcurrentHashMap<Long, String>();

		if (ticketList.isEmpty())
			return ticketIdAndFxNameMap;

		for (Gis gis : gisDAO.getGisInfo4Tickets(ticketList))
		{
			if (gis.getTicketNo() != null && gis.getConnectionType() != null)
				ticketIdAndFxNameMap
						.put(gis.getTicketNo(), gis.getConnectionType());
		}

		return ticketIdAndFxNameMap;
	}

	@Override
	public Map<Long, Long> getTicketIdAndSaelsExecutiveMap(
			List<Long> ticketList)
	{
		Map<Long, Long> ticketIdAndFxNameMap = new ConcurrentHashMap<Long, Long>();

		if (ticketList.isEmpty())
			return ticketIdAndFxNameMap;

		for (Gis gis : gisDAO.getGisInfo4Tickets(ticketList))
		{
			if (gis.getTicketNo() != null && gis.getAddedBy() != null)
				ticketIdAndFxNameMap.put(gis.getTicketNo(), gis.getAddedBy());
		}

		return ticketIdAndFxNameMap;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.customer.CustomerCoreService#populateCustomerAadhaarMapping(com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping)
	 */
	@Override
	public APIResponse populateCustomerAadhaarMapping(CustomerAadhaarMapping aadhaarMapping) {

		if(aadhaarMapping == null)
			ResponseUtil.createNullParameterResponse();
		
		try {
			CustomerAadhaarMappingVo aadhaarMappingVo = customerDAO.populateCustomerAadhaarMapping(aadhaarMapping);
			return ResponseUtil.createSuccessResponse().setData(aadhaarMappingVo);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseUtil.createFailureResponse().setData("Data population failed for " + aadhaarMapping + " ." + e.getMessage());
		}
	}

}
