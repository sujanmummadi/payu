package com.cupola.fwmp.service.customer;

import com.cupola.fwmp.vo.integ.app.ProspectVO;
import com.cupola.fwmp.vo.sales.PaymentUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

public interface AppToFWMPConverter
{
	UpdateProspectVO updateBasicInfo(ProspectVO prospectVO);

	TariffPlanUpdateVO updateTariff(ProspectVO prospectVO);
	
	PaymentUpdateVO updatePayment(ProspectVO prospectVO);

	/**@author aditya 5:20:00 PM Apr 10, 2017
	 * PaymentUpdateVO
	 * @param prospectVO
	 * @return
	 */
	PaymentUpdateVO uploadDoucuments(ProspectVO prospectVO);

}
