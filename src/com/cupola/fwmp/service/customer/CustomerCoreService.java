package com.cupola.fwmp.service.customer;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.UserVo;

public interface CustomerCoreService
{

	public Map<String, Long> addCustomerViaCRM(List<MQWorkorderVO> mqWorkorderVO, String location);

	public boolean getCustomerByMqId(String mqId);

	public APIResponse getAllCustomer();

	public APIResponse deleteCustomer(Long id);

	public APIResponse updateCustomer(CustomerVO customerVo);

	public APIResponse updateTicket(StatusVO status);

	Map<String, Long> getTicketIdsFromProspects(List<String> prospectList, String location);

	Map<Long, String> getFxNameFromTicketId(List<Long> ticketList);

	UserVo getUserByRoleAndFXName(String fxName, String role);

	GISPojo xformToGISPojo(MQWorkorderVO mqWorkorderVO, long ticketId);

	Map<Long, String> getConnectionTypeFromTicketId(List<Long> ticketList);

	Map<Long, Long> getTicketIdAndSaelsExecutiveMap(List<Long> ticketList);

	public Map<String, Long> addFrCustomerViaCRM(List<MQWorkorderVO> mqWorkorderVOList);
	
	APIResponse populateCustomerAadhaarMapping(CustomerAadhaarMapping aadhaarMapping);
}
