package com.cupola.fwmp.service.customer;

import com.cupola.fwmp.dao.integ.prospect.pojo.MQProspectRequest;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.ProspectUpdateVO;
import com.cupola.fwmp.vo.TicketVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateProspectVo;

public interface ProspectCoreService
{

	public APIResponse createNewProspect(ProspectCoreVO prospect, String source);
	
	public APIResponse updateNewProspect(ProspectUpdateVO prospect);
	
	public APIResponse appProspectWrapper(Long prospectId);
	
	public APIResponse addNewProspect(ProspectCoreVO prospect, String source);

	public APIResponse updateNewProspect(Long ticketId, Long prospectNumber);

	APIResponse retryProspectCreation(ProspectCoreVO prospect);

	TicketVo createPropsectInMQ(MQProspectRequest mqProspect,
			ProspectCoreVO prospect);

	/**@author aditya
	 * TicketVo
	 * @param prospectCoreVO
	 * @return
	 */
	TicketVo createPropsectInSiebel(ProspectCoreVO prospectCoreVO);

}