package com.cupola.fwmp.service.customer;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.tempuri.ServiceLocator;
import org.tempuri.ServiceSoap;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.AadhaarTransactionType;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.AddressType;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.CustomerAppFeasibilityType;
import com.cupola.fwmp.FWMPConstant.DeDupFlag;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.FWMPConstant.MQStatus;
import com.cupola.fwmp.FWMPConstant.TicketSource;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.adhaar.AdhaarDAO;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.dao.integ.app.AppPropspectDAO;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.integ.prospect.pojo.MQProspectRequest;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectResponse;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectStatus;
import com.cupola.fwmp.dao.integ.prospect.pojo.XmlUtil;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAOImpl;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.persistance.entities.Adhaar;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.domain.engines.gis.GISServiceIntegration;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.service.integ.crm.CrmServiceCall;
import com.cupola.fwmp.service.integ.mq.MqProspectCoreService;
import com.cupola.fwmp.service.notification.EmailNotiFicationHelper;
import com.cupola.fwmp.service.sales.SalesCoreServcie;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.CrmUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.util.tools.CommonToolUtil;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.ProspectUpdateVO;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.TicketVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.gis.GISFesiabilityInput;
import com.cupola.fwmp.vo.gis.GISResponseVO;
import com.cupola.fwmp.vo.integ.app.MqLog;
import com.cupola.fwmp.vo.integ.app.ProspectVO;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateProspectVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.prospect.CreateProspectAPI_Output;
import com.cupola.fwmp.vo.tools.siebel.from.vo.prospect.ProspectResponseFromSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.xstream.XStream;

public class ProspectCoreServiceImpl implements ProspectCoreService {
	private static Logger LOGGER = Logger.getLogger(ProspectCoreServiceImpl.class);

	@Autowired
	AdhaarDAO adhaarDAO;

	@Autowired
	CustomerDAO customerDao;

	@Autowired
	TicketDAO tickectDao;

	@Autowired
	MqProspectCoreService mqProspectCoreService;

	@Autowired
	TicketActivityLogDAOImpl ticketActivityLogDAOImpl;

	@Autowired
	AppPropspectDAO appPropspectDAO;

	@Autowired
	GisDAO gisDAO;

	@Autowired
	DeviceDAO deviceDAO;

	@Autowired
	EtrCalculator etrCalculator;
	@Autowired
	UserDao userDao;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	GISServiceIntegration gisServiceIntegration;

	@Autowired
	ProspectCreatorHelper prospectCreatorHelper;

	@Autowired
	EmailNotiFicationHelper emailNotiFicationHelper;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	AppToFWMPConverter appToFWMPConverter;

	@Autowired
	SalesCoreServcie salesCoreServcie;

	@Autowired
	CrmUtil crmUtil;

	@Autowired
	CrmServiceCall crmServiceCall;
	
	//for static issue
	@Autowired
	CommonToolUtil commonToolUtil;

	private String failedMqSupportEmailId;
	private String fwmpServerIp;
	private String hydMqUrl;
	private String roiMqUrl;

	private static Map<String, Long> customerMobileNumbers = new HashMap<String, Long>();
	private static long MAX_ALLOWED_TIME_DIFF = 3 * 60 * 1000;

	public void setHydMqUrl(String hydMqUrl) {
		this.hydMqUrl = hydMqUrl;
	}

	public void setRoiMqUrl(String roiMqUrl) {
		this.roiMqUrl = roiMqUrl;
	}

	public APIResponse createNewProspect(ProspectCoreVO prospect, String source) {

		LOGGER.info("In createNewProspect method of ProspectCoreServiceImpl,prospect details are: " + prospect
				+ " ######### Source" + source);

		TicketVo ticketVo = null;

		if (prospect.getTicket() != null && prospect.getTicket().getAssignTo() > 0) {
			if (prospect.getCustomer().getCityId() == null || prospect.getCustomer().getCityId() <= 0)
				prospect.getCustomer().setCityId(AuthUtils.getCurrentUserCity().getId());

			long currentTime = System.currentTimeMillis();

			if (prospect.getCustomer() != null
					&& customerMobileNumbers.containsKey(prospect.getCustomer().getMobileNumber())) {
				Long timeStamp = customerMobileNumbers.get(prospect.getCustomer().getMobileNumber());

				Log.info("timeStamp" + timeStamp);

				if ((currentTime - timeStamp) < MAX_ALLOWED_TIME_DIFF) {
					LOGGER.info("Ignoring duplicate prospect creation with mobile "
							+ prospect.getCustomer().getMobileNumber());

					return ResponseUtil.createSaveSuccessResponse().setData(ticketVo);

				} else {
					customerMobileNumbers.put(prospect.getCustomer().getMobileNumber(), currentTime);
				}

			} else if (prospect.getCustomer() != null) {
				customerMobileNumbers.put(prospect.getCustomer().getMobileNumber(), currentTime);
			}
			customerMobileNumbers.clear();

			LOGGER.info(" Inside quick add of prospect ######### Source " + source);

			APIResponse response = addNewProspect(prospect, source);

			if (response != null && response.getData() != null) {
				ticketVo = (TicketVo) response.getData();
				LOGGER.info("Ticket details for mobile number " + prospect.getCustomer().getMobileNumber() + " is "
						+ ticketVo);
			}
		} else {

//set seibel source to validate to FWMP manjuprasad			
//			if (source != null && !source.isEmpty())
//				prospect.setCreationSource(source);
			
			if (source != null && !source.isEmpty())
				prospect.setCreationSource(prospect.getTicket().getSource());

			LOGGER.info(" Inside queue add of prospect ######### Source " + source);
			customerMobileNumbers.clear();
			prospectCreatorHelper.addNewProspect(prospect, source);

		}

		customerMobileNumbers.clear();

		return ticketVo != null ? ResponseUtil.createSaveSuccessResponse().setData(ticketVo)
				: ResponseUtil.createSaveSuccessResponse();

	}

	public APIResponse updateNewProspect(Long ticketId, Long prospectNumber) {
		try {
			tickectDao.updateProspectNumberInTicket(ticketId, prospectNumber + "");
			return ResponseUtil.createMqServerTicketCtreationSuccess(prospectNumber + "");

		} catch (Exception e) {
			LOGGER.error("Duplicate prospect number for ticket id " + ticketId);
			return ResponseUtil.createDuplicateResponse();
		}
	}

	/**
	 * @param prospect
	 * @return
	 */
	public APIResponse addNewProspect(ProspectCoreVO prospect, String source) {

		LOGGER.info("prospect details in addNewProspect method in ProspectCoreServiceImpl::" + prospect
				+ "######### Source " + source);

		if (prospect.getTicket() != null && prospect.getTicket().getAssignTo() > 0) {
			return addNewProspectByGis(prospect, null, source);

		} else {

			return addNewProspectWithoutGis(prospect, null, source);
		}
	}

	@Override
	public APIResponse retryProspectCreation(ProspectCoreVO prospect) {
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);
		TicketVo ticketVo = null;

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {

			ticketVo = createPropsectInMQ(mqProspectCoreService.convertTicket2MqProspect(prospect), prospect);
		} else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			List<CustomerAddressVO> customeraddressvos = prospect.getCustomer().getCustomerAddressVOs();

			LOGGER.info(" $$$$$$$$$$$$%%%%%%%%%%%%%% customeraddressvos  " + customeraddressvos);

			if (customeraddressvos != null) {

				for (Iterator<CustomerAddressVO> iterator = customeraddressvos.iterator(); iterator.hasNext();) {

					try {

						CustomerAddressVO customerAddressVO = (CustomerAddressVO) iterator.next();
						
						if(customerAddressVO == null)
							continue;

						ObjectMapper mapper = new ObjectMapper();
						String addressJsonString = mapper.writeValueAsString(customerAddressVO);
						
						if (customerAddressVO.getCustomerAddressType() != null
								&& (customerAddressVO.getArea() != null || customerAddressVO.getAreaId() > 0)
								&& (customerAddressVO.getOperationalEntity() != null
										|| customerAddressVO.getOperationalEntityId() > 0)
								&& (customerAddressVO.getSubArea() != null || customerAddressVO.getSubAreaId() > 0))

						{
							if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.CURRENT_ADDRESS))
								prospect.getCustomer().setCurrentAddress(addressJsonString);
							LOGGER.info("addressJsonString" + addressJsonString);

							if (customerAddressVO.getCustomerAddressType().equalsIgnoreCase(AddressType.PERMANENT_ADDRESS))
								prospect.getCustomer().setPermanentAddress(addressJsonString);

							if (customerAddressVO.getCustomerAddressType()
									.equalsIgnoreCase(AddressType.COMMUNICATION_ADDRESS))
								prospect.getCustomer().setCommunicationAdderss(addressJsonString);
						} else {
							LOGGER.info("Invalid Address Type !!");
							return ResponseUtil.createInvalidAddress();
						}
					} catch (Exception e) {
						
						LOGGER.error("Error while converting address into json format " + e.getMessage());
						e.printStackTrace();
						continue;
					}
				}
			} else {
				LOGGER.info("Invalid Address Type !!");
				return ResponseUtil.createInvalidAddress();
			}

			ticketVo = createPropsectInSiebel(prospect);

			

		}
			
			if (!(MQStatus.FAILED + "").equals(ticketVo.getMqStatus())) {
				customerDao.updateCustomer(prospect.getCustomer(), prospect.getTicket().getId());
				APIResponse response = updateNewProspect(prospect.getTicket().getId(), Long.valueOf(ticketVo.getProspectNo()));
				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {

					salesCoreServcie.updateSalesActivityInCRM(ticketVo.getId(), ticketVo.getCustomerId(),
							ticketVo.getProspectNo());

				}
				return response;
			} else {
				tickectDao.updateProspectRetryCountForFailed(prospect.getTicket().getId());
				return ResponseUtil.createMqServerTicketCtreationFailed();
			}
			
			
			
				
	}
			
		
		

	/**
	 * @param prospect
	 * @return
	 */
	public APIResponse addNewProspectWithoutGis(ProspectCoreVO prospect, CreateUpdateProspectVo createUpdateProspectVo,
			String source) {
		Long customerId = StrictMicroSecondTimeBasedGuid.newGuid();
		UserVo assignedUser = null;

		TicketVo ticketVo = null;

		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		LOGGER.info(
				"Prospect service reponse ----------- without GIS method" + currentCRM + "######### Source" + source);

		if (source == null) {

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {
				LOGGER.info("Prospect service reponse ----------- CRM_MQ without GIS method " + currentCRM);
				ticketVo = createPropsectInMQ(mqProspectCoreService.convertTicket2MqProspect(prospect), prospect);

			} else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				LOGGER.info("Prospect service reponse ----------- CRM_SIEBEL without GIS method" + currentCRM);

				ticketVo = createPropsectInSiebel(prospect);
				LOGGER.info("Prospect service reponse ----------- CRM_SIEBEL without GIS method" + ticketVo);

			} else {

			}
		} else if (source.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			ticketVo = getTicketVoFormReqBySiebel(prospect);

		} else {
			LOGGER.error("Invalid source type ");
		}
		LOGGER.info("Prospect service reponse -----------" + ticketVo + " mobile nubmber "
				+ prospect.getCustomer().getMobileNumber());

		prospect.getCustomer().setId(customerId);
		
		if (prospect.getCustomer().getKycDetails() != null) {

			if (prospect.getCustomer().isAadhaarForPOI()) {

				try {

					
					Adhaar adhar = adhaarDAO.saveAdhar(prospect.getCustomer().getKycDetails());

					if (adhar != null)
						prospect.getCustomer().setAdhaarId(adhar.getId());
					
				} catch (Exception e) {
					LOGGER.info("Not able to save aadhar info with adhar information ---> "
							+ prospect.getCustomer().getKycDetails().getName() + " " 
							+ prospect.getCustomer().getKycDetails().getPhone() + " " + e.getMessage());
					e.printStackTrace();
					return ResponseUtil.createSaveFailedResponse();
				}
			}
		}
		
		CustomerVO vo = customerDao.addCustomer(prospect.getCustomer());

		if (vo == null)
			return ResponseUtil.createSaveFailedResponse();

		Long guId = vo.getId();

		LOGGER.info("New Prospect created for customer " + guId + " Mobile number "
				+ prospect.getCustomer().getMobileNumber());

		/**
		 * #################################################### Problem area started
		 * ###########################################################
		 **/

		prospect.getCustomer().setId(guId);
		if (prospect.getCustomer().getAreaId() != null)
			ticketVo.setAreaId(prospect.getCustomer().getAreaId());
		ticketVo.setCustomerId(prospect.getCustomer().getId());
		ticketVo.setSubAreaId(prospect.getTicket().getSubAreaId());
		

		prospect.setTicket(ticketVo);
		
		if(prospect.getCustomer().getAddedBy() != null && prospect.getCustomer().getAddedBy() > 0)
			prospect.getTicket().setAssignedBy(prospect.getCustomer().getAddedBy());
		
		prospect.getTicket()
				.setPreferedDate(prospect.getCustomer().getPrefferedCallDate());

		if (prospect.getCustomer().getAddedBy() != null && prospect.getCustomer().getAddedBy() > 0)
			prospect.getTicket().setAssignedBy(prospect.getCustomer().getAddedBy());

		if (prospect.getCustomer().getPrefferedCallDate() != null)
			prospect.getTicket().setPreferedDate(prospect.getCustomer().getPrefferedCallDate());

		
		if (source == null) {

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {

				if (prospect.getCustomer().getAreaId() != null) {
					assignedUser = userDao.getDefaultSalesUserForArea(prospect.getCustomer().getAreaId(),
							prospect.getCustomer().getBranchId(), prospect.getCustomer().getCityId(), UserRole.ROLE_EX);
				}

			} else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				if (prospect.getCustomer().getAreaId() != null) {
					assignedUser = userDao.getDefaultSalesUserForSubArea(prospect.getTicket().getSubAreaId(),prospect.getCustomer().getAreaId(),
							prospect.getCustomer().getBranchId(), prospect.getCustomer().getCityId(), UserRole.ROLE_EX);
				}

			}
		} else if (source.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			if (prospect.getCustomer().getAreaId() != null) {
				assignedUser = userDao.getDefaultSalesUserForSubArea(prospect.getTicket().getSubAreaId(),prospect.getCustomer().getAreaId(),
						prospect.getCustomer().getBranchId(), prospect.getCustomer().getCityId(), UserRole.ROLE_EX);
			}

		} else {
			LOGGER.error("Invalid source type ");
		}
	
		LOGGER.info("AssignedUser found for Mobile number " + prospect.getCustomer().getMobileNumber());

		if (assignedUser != null)
			prospect.getTicket().setAssignTo(assignedUser.getId());
		else
			ticketVo.setStatus(TicketStatus.SALES_UNASSIGNED_TICKET + "");

		if (prospect.getCreationSource() == null)
			prospect.setCreationSource(TicketSource.FWMP);

		Date etr = etrCalculator.getETRforWorkOrderType(prospect.getTicket().getTicketSubCategory());

		LOGGER.info("Etr calculate for Mobile number " + prospect.getCustomer().getMobileNumber());

		prospect.getTicket().setCommitedEtr(GenericUtil.convertToUiDateFormat(etr));

		Long ticketId = tickectDao.createNewInstallationProspect(prospect);

		if (prospect.getCustomer().getKycDetails() != null) {
			CustomerAadhaarMapping aadhaarMapping = getCustomerAadhaarMappingForProspect(prospect);
			aadhaarMapping.setTicketId(ticketId);

			customerDao.populateCustomerAadhaarMapping(aadhaarMapping);
		}
		
		prospect.getTicket().setId(ticketId);
		prospect.getTicket().setCurrentUpdateCategory(TicketUpdateConstant.NEW_PROSPECT_CREATED);
		if (prospect.getCustomer().getAddedBy() != null)
			prospect.getTicket().setUpdatedBy(prospect.getCustomer().getAddedBy());

		LOGGER.info("Ticket Added for Mobile number " + prospect.getCustomer().getMobileNumber());

		Long addedBy = 1L;

		addedBy = prospect.getCustomer() != null && prospect.getCustomer().getAddedBy() != null
				&& prospect.getCustomer().getAddedBy().equals("null") ? prospect.getCustomer().getAddedBy()
						: FWMPConstant.SYSTEM_ENGINE;

		System.out.println(prospect + "aklfjadskjfdij aditya @@@@@@@@@@@@@@@@ " + prospect.getCustomer().getAddedBy());

		TicketLog ticketLog = new TicketLogImpl(ticketId, TicketUpdateConstant.NEW_PROSPECT_CREATED, addedBy);

		ticketLog.setStatusId(TicketStatus.PROSPECT_CREATED);

		LOGGER.info("ticketLog for Mobile number " + prospect.getCustomer().getMobileNumber());

		TicketUpdateGateway.logTicket(ticketLog);

		LOGGER.info("Logger done for Mobile number " + prospect.getCustomer().getMobileNumber());

		/**
		 * #################################################### Problem area End
		 * ###########################################################
		 **/

		LOGGER.info("New Prospect created for customer " + guId + " with ticket " + ticketId);

		if (ticketVo != null && prospect.getTicket() != null && prospect.getTicket().getAssignTo() > 0) {
			LOGGER.info("Adding new ticket to " + ticketId + " to " + prospect.getTicket().getAssignTo());

			dbUtil.addOneTicketToQueue(prospect.getTicket().getAssignTo(), ticketId);
			

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				salesCoreServcie.updateSalesActivityInCRM(ticketId,  null  , 
						prospect.getTicket().getProspectNo());
			}
			
		}

		return ResponseUtil.createSaveSuccessResponse().setData(ticketVo);
	}

	/**
	 * @param prospect
	 * @return
	 */
	public APIResponse addNewProspectByGis(ProspectCoreVO prospect, CreateUpdateProspectVo createUpdateProspectVo,
			String source) {
		GISFesiabilityInput gisInput = new GISFesiabilityInput();
		Long customerId = StrictMicroSecondTimeBasedGuid.newGuid();
		UserVo assignedUser = null;
		String[] latLngArray = null;
		GISResponseVO gisResponse = null;

		if (prospect.getTicket() != null && prospect.getTicket().getAssignTo() > 0) {
			if (prospect.getCustomer().getPrefferedCallTimeStamp() != null)
				prospect.getCustomer()
						.setPrefferedCallDate(new Date(prospect.getCustomer().getPrefferedCallTimeStamp()));
			assignedUser = userDao.getUserById(prospect.getTicket().getAssignTo());
			prospect.getTicket().setSource(TicketSource.DOOR_KNOCK);
			prospect.setCreationSource(TicketSource.DOOR_KNOCK);
			// prospect.getTicket().setSubAttribute(TicketSubAttribute.DOOR_KNOCK);
			prospect.getCustomer().setAddedBy(prospect.getTicket().getAssignTo());

		} else {
			try {
				List<CustomerAddressVO> customerAddress = prospect.getCustomer().getCustomerAddressVOs();

				LOGGER.info("Siebel customerAddress :::" + customerAddress);

				LOGGER.info("address :::" + prospect.getCustomer().getCurrentAddress());

				latLngArray = CommonUtil.getLatLongPositionsFromAddress(prospect.getCustomer().getCurrentAddress());

				// latLngArray = CommonUtil.getLatLongPositionsFromAddress(currentAddress);

			} catch (Exception e) {
				LOGGER.error("Address parsing error " + e.getMessage());
				e.printStackTrace();
			}

			gisInput.setCustomerId(customerId + "");
			gisInput.setPhoneNo(prospect.getCustomer().getMobileNumber());

			if (prospect.getCustomer() != null && prospect.getCustomer().getCityId() != null) {
				gisInput.setCity(cityDAO.getCityNameById(prospect.getCustomer().getCityId()));
			}

			if (latLngArray != null) {
				gisInput.setLatitude(latLngArray[0]);
				gisInput.setLongitude(latLngArray[1]);
				gisResponse = gisServiceIntegration.actGISFesibility(gisInput);

				LOGGER.info("gis response-----------" + gisResponse);

				if (gisResponse != null) {

					// assign to the se who is creating prospect
					if (prospect.getTicket() != null && prospect.getTicket().getAssignTo() > 0) {
						assignedUser = userDao.getUserById(prospect.getTicket().getAssignTo());
					} else {
						if (gisResponse.getFxMacAddress() == null && gisResponse.getFxName() != null) {
							assignedUser = userDao.getUsersByDeviceName(gisResponse.getFxName(), UserRole.ROLE_EX);
							LOGGER.info(
									"Got FX name " + gisResponse.getFxName() + " and getting user :: " + assignedUser);
						}

						if (gisResponse.getFxMacAddress() != null) {
							gisResponse.setFxMacAddress(gisResponse.getFxMacAddress().toUpperCase());
							assignedUser = userDao.getUsersByDeviceName(gisResponse.getFxName(), UserRole.ROLE_EX);
							LOGGER.info(gisResponse.getFxMacAddress() + " AssignedUser Prospect assigned to user:  "
									+ assignedUser);
						}

					}

					if (gisResponse.getBranch() != null) {
						Long branchId = branchDAO.getIdByCode(gisResponse.getBranch());

						if (branchId != null)
							prospect.getCustomer().setBranchId(branchId);
						else {
							Long branId = branchDAO.getIdByCode("DEFAULT");
							if (branId != null)
								prospect.getCustomer().setBranchId(branId);
						}
					} else {
						Long branId = branchDAO.getIdByCode("DEFAULT");
						if (branId != null)
							prospect.getCustomer().setBranchId(branId);
					}
					if (gisResponse.getArea() != null) {
						long areaId = areaDAO.getIdByCode(gisResponse.getArea());
						if (areaId > 0)
							prospect.getCustomer().setAreaId(areaId);
						else {
							long areId = areaDAO.getIdByCode("DEFAULT");
							prospect.getCustomer().setAreaId(areId);
						}
					} else {

						long areId = areaDAO.getIdByCode("DEFAULT");
						prospect.getCustomer().setAreaId(areId);
					}

				} else {
					LOGGER.info("Got no response from GIS gisResponse : " + gisResponse);
				}
			} else {
				long areaId = areaDAO.getIdByCode("DEFAULT");

				if (areaId > 0)
					prospect.getCustomer().setAreaId(areaId);

				Long branId = branchDAO.getIdByCode("DEFAULT");
				if (branId != null)
					prospect.getCustomer().setBranchId(branId);
			}

		}

		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		LOGGER.info("Prospect service reponse ----------- with GIS method" + currentCRM + "######### Source" + source);

		TicketVo ticketVo = null;

		if (source == null) {

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {

				LOGGER.info("Prospect service reponse ----------- CRM_MQ with GIS method " + currentCRM);
				ticketVo = createPropsectInMQ(mqProspectCoreService.convertTicket2MqProspect(prospect), prospect);

			} else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {

				LOGGER.info("Prospect service reponse ----------- CRM_SIEBEL with GIS method" + currentCRM);

				ticketVo = createPropsectInSiebel(prospect);

				LOGGER.info("Prospect service reponse ----------- CRM_SIEBEL with GIS method" + ticketVo);

			} else {

			}
		} else if (source.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			ticketVo = getTicketVoFormReqBySiebel(prospect);

		} else {
			LOGGER.error("Invalid source type ");
		}
		// createPropsectInMQ(mqProspectCoreService.convertTicket2MqProspect(prospect),
		// prospect);

		LOGGER.info("Prospect service reponse addNewProspectByGis -----------" + ticketVo.getProspectNo()
			+ " mobile number "	+ prospect.getCustomer().getMobileNumber());

		// }

		if (gisResponse == null || gisResponse.getFxName() == null) {
			ticketVo.setTicketSubCategory(TicketSubCategory.GIS_FEASIBILITY_FAILURE);
			ticketVo.setTicketSubCategoryId(TicketSubCategory.FEASIBILITY);
			ticketVo.setStatus(TicketStatus.GIS_DATA_NOT_AVAILABLE + "");
		}

		prospect.getCustomer().setId(customerId);

		if (prospect.getCustomer().getKycDetails() != null) {

			if (prospect.getCustomer().isAadhaarForPOI()) {

				try {
					Adhaar adhar = adhaarDAO.saveAdhar(prospect.getCustomer().getKycDetails());

					if (adhar != null)
						prospect.getCustomer().setAdhaarId(adhar.getId());
					
					customerDao.populateCustomerAadhaarMapping(getCustomerAadhaarMappingForProspect(prospect));

				} catch (Exception e) {
					LOGGER.info("Not able to save aadhar info with adhar information ---> "
							+ prospect.getCustomer().getKycDetails().getName() + " "
							+ prospect.getCustomer().getKycDetails().getPhone() + " " + e.getMessage());
					e.printStackTrace();
					return ResponseUtil.createSaveFailedResponse();
				}
			}
		}
		CustomerVO vo = customerDao.addCustomer(prospect.getCustomer());

		if (vo == null)
			return ResponseUtil.createSaveFailedResponse();

		Long guId = vo.getId();

		LOGGER.info("New Prospect created for customer " + guId);

		prospect.getCustomer().setId(guId);
		if (prospect.getCustomer().getAreaId() != null)
			ticketVo.setAreaId(prospect.getCustomer().getAreaId());
		ticketVo.setCustomerId(prospect.getCustomer().getId());

		prospect.setTicket(ticketVo);
		prospect.getTicket().setAssignedBy(prospect.getCustomer().getAddedBy());
		prospect.getTicket().setPreferedDate(prospect.getCustomer().getPrefferedCallDate());

		if (assignedUser != null)
			prospect.getTicket().setAssignTo(assignedUser.getId());
		else
			ticketVo.setStatus(TicketStatus.SALES_UNASSIGNED_TICKET + "");

		if (prospect.getCreationSource() == null)
			prospect.setCreationSource(TicketSource.FWMP);

		if (gisResponse != null) {
			if ("COPPER".equalsIgnoreCase(gisResponse.getType())) {
				prospect.getTicket().setTicketSubCategory(WorkStageName.COPPER);
			} else if (gisResponse.getFxName() != null && gisResponse.getCxMacAddress() != null)
				prospect.getTicket().setTicketSubCategory(WorkStageName.COPPER);
			else if (gisResponse.getFxName() != null)
				prospect.getTicket().setTicketSubCategory(WorkStageName.FIBER);
		}
		Date etr = etrCalculator.getETRforWorkOrderType(prospect.getTicket().getTicketSubCategory());

		prospect.getTicket().setCommitedEtr(GenericUtil.convertToUiDateFormat(etr));

		Long ticketId = tickectDao.createNewInstallationProspect(prospect);
		GISPojo gis = new GISPojo();

		if (gisResponse != null) {
			BeanUtils.copyProperties(gisResponse, gis);

			if (gisResponse.getType().equalsIgnoreCase(WorkStageName.COPPER)) {
				gis.setWorkstageType((int) WorkStageType.COPPER);
				gis.setConnectionType(WorkStageName.COPPER);

			} else if (gisResponse.getType().equalsIgnoreCase(WorkStageName.FIBER)) {
				gis.setWorkstageType((int) WorkStageType.FIBER);
				gis.setConnectionType(WorkStageName.FIBER);

			} else {
				gis.setConnectionType(gisResponse.getType());
			}
		}

		gis.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		gis.setTicketNo(ticketId);
		gis.setFeasibilityType(FeasibilityType.GIS_AUTO);

		if (gisResponse != null && gisResponse.getCity() != null)
			gis.setCity(cityDAO.getCityIdByName(gisResponse.getCity()) + "");
		if (gisResponse != null && gisResponse.getBranch() != null)
			gis.setBranch(branchDAO.getIdByCode(gisResponse.getBranch()) + "");

		if (gisResponse != null && gisResponse.getArea() != null)
			gis.setArea(areaDAO.getIdByCode(gisResponse.getArea()) + "");

		gisDAO.insertGISInfo(gis);

		prospect.getTicket().setId(ticketId);
		prospect.getTicket().setCurrentUpdateCategory(TicketUpdateConstant.NEW_PROSPECT_CREATED);
		prospect.getTicket().setUpdatedBy(prospect.getCustomer().getAddedBy());

		TicketLog ticketLog = new TicketLogImpl(ticketId, TicketUpdateConstant.NEW_PROSPECT_CREATED,
				prospect.getCustomer().getAddedBy());
		ticketLog.setStatusId(TicketStatus.PROSPECT_CREATED);
		if (gisResponse != null)
			ticketLog.setActivityValue(gisResponse.getType());
		TicketUpdateGateway.logTicket(ticketLog);

		LOGGER.info("New Prospect created for customer " + guId + " with ticket " + ticketId);

		if (ticketVo != null && prospect.getTicket() != null && prospect.getTicket().getAssignTo() > 0) {
			LOGGER.info("Adding new ticket to " + ticketId + " to " + prospect.getTicket().getAssignTo());

			dbUtil.addOneTicketToQueue(prospect.getTicket().getAssignTo(), ticketId);
		}

		return ResponseUtil.createSaveSuccessResponse().setData(ticketVo);
	}

	/**
	 * @author aditya TicketVo
	 * @param prospect
	 * @return
	 */
	private TicketVo getTicketVoFormReqBySiebel(ProspectCoreVO prospect) {

		TicketVo ticketVo = new TicketVo();

		LOGGER.info("dedupflage value in prospect  ::" + prospect);

		String dedupflage = prospect.getDeDupFlag();
		LOGGER.info("dedupflage is ::" + dedupflage);

		if (prospect.getDeDupFlag() != null && !prospect.getDeDupFlag().isEmpty()
				&& !prospect.getDeDupFlag().equals("null") && Long.valueOf(prospect.getDeDupFlag()) >= 0) {

			if (DeDupFlag.cleanProspect == Long.valueOf(prospect.getDeDupFlag())) {

				ticketVo.setDeDupFlag(DeDupFlag.deDupFlagSuccess);
				ticketVo.setDeDupMessage(DeDupFlag.cleanProspectString);
				ticketVo.setMqStatus(DeDupFlag.deDupFlagSuccess + "");

			} else if (DeDupFlag.duplicatedProspect == Long.valueOf(prospect.getDeDupFlag())) {

				ticketVo.setDeDupFlag(DeDupFlag.deDupFlagFailure);
				ticketVo.setMqStatus(DeDupFlag.deDupFlagFailure + "");
				ticketVo.setDeDupMessage(DeDupFlag.duplicatedProspectString);
			} else {
				ticketVo.setDeDupFlag(DeDupFlag.deDupFlagFailure);
				ticketVo.setMqStatus(DeDupFlag.deDupFlagFailure + "");
				ticketVo.setDeDupMessage(DeDupFlag.duplicatedProspectString);
			}
		} else {
			ticketVo.setDeDupFlag(DeDupFlag.deDupFlagFailure);
			ticketVo.setMqStatus(DeDupFlag.deDupFlagFailure + "");
			ticketVo.setDeDupMessage(DeDupFlag.duplicatedProspectString);
		}

		if (prospect.getTicket().getProspectNo() != null) {

			ticketVo.setProspectNo(prospect.getTicket().getProspectNo());

		} else {
			ticketVo.setProspectNo(StrictMicroSecondTimeBasedGuid.newGuid() + "");
		}

		return ticketVo;
	}

	@Override
	public TicketVo createPropsectInMQ(MQProspectRequest mqProspect, ProspectCoreVO prospect) {
		LOGGER.info("MQProspectRequest " + mqProspect + " " + "ProspectCoreVO" + prospect + " "
				+ "In prospectcoreserviceIMPL");

		MqLog mqLog = getMqInput();

		TicketVo ticketVo = new TicketVo();

		ticketVo.setProspectNo(StrictMicroSecondTimeBasedGuid.newGuid() + "");
		ticketVo.setMqStatus(MQStatus.FAILED + "");
		// ticketVo.setSubAttribute("Door Knock");
		XmlUtil xmlUtil = new XmlUtil();

		String xml = xmlUtil.convertToXml(mqProspect, mqProspect.getClass());

		String mqUrl;

		String serviceArea = null;

		if (mqProspect.getAddressInfo().getCity().equalsIgnoreCase(CityName.HYDERABAD_CITY)) {
			mqUrl = hydMqUrl;

			serviceArea = CityName.HYDERABAD_CITY;

		} else {
			mqUrl = roiMqUrl;
			serviceArea = "ROI";
		}

		String transactionNo = StrictMicroSecondTimeBasedGuid.newGuid().toString();

		LOGGER.info("Mq Request URL " + mqUrl + "   Mq Request input " + xml);

		mqLog.setRequestString(xml);
		mqLog.setMqUrl(mqUrl + " transactionNo " + transactionNo);
		mqLog.setProspect(prospect);

		ServiceLocator locator = new ServiceLocator(mqUrl);
		String response = null;
		try {
			ServiceSoap ws = locator.getServiceSoap();

			((Stub) ws).setHeader(crmUtil.createMqAuth());

			response = ws.createProspect(xml, transactionNo);

			if (response != null) {
				LOGGER.info("Mq response ########### " + response + " for mobile "
						+ prospect.getCustomer().getMobileNumber());

				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");
				xstream.aliasField("PROSPECTNO", MqProspectResponse.class, "prospectNo");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream.fromXML(response);

				int errorCode = mqProspectResponse.getStatus().getErrorNo();
				ticketVo.setMqErrorCode(errorCode + "");
				mqLog.setResponseTime(new Date().toString());

				if (mqProspectResponse.getStatus().getErrorNo() == 0) {

					if (mqProspectResponse.getProspectNo() != null)
						ticketVo.setProspectNo(mqProspectResponse.getProspectNo());
					ticketVo.setMqStatus(MQStatus.SUCCESS + "");
					mqLog.setMqErrorCode(errorCode + "");

				} else {
					ticketVo.setStatus(TicketStatus.MQ_INSERTION_FAILED + "");
					ticketVo.setMqStatus(MQStatus.FAILED + "");
					// ticketVo.setMqStatus(TicketStatus.MQ_INSERTION_FAILED +
					// "");
					handleProspectCreationFailed(mqUrl, xml, mqProspectResponse.toString(), ticketVo.getProspectNo(),
							transactionNo, errorCode, serviceArea);
					mqLog.setMqErrorCode(errorCode + "");
					mqLog.setMqErrorMessage(definitionCoreService.getMqErrorMessageById(Long.valueOf(errorCode + "")));

				}

				LOGGER.info("Response form MQ server is " + mqProspectResponse);

				mqLog.setResponseString(response);
				mqLog.setMqProspectResponse(mqProspectResponse);
			} else {
				LOGGER.info("Response form MQ server is " + response);

				handleProspectCreationFailed(mqUrl, xml, null, ticketVo.getProspectNo(), transactionNo, 900000,
						serviceArea);

				mqLog.setResponseString("null");
				ticketVo.setMqStatus(MQStatus.FAILED + "");
			}
		} catch (ServiceException e) {

			LOGGER.error("URL " + mqUrl + "exception for MQ Server " + e.getMessage());
			ticketVo.setMqStatus(MQStatus.FAILED + "");
			handleProspectCreationFailed(mqUrl, xml, null, ticketVo.getProspectNo(), transactionNo, 900000,
					serviceArea);

			e.printStackTrace();

		} catch (SOAPException e) {

			LOGGER.error("Header exception " + mqUrl + " for MQ Server " + e.getMessage());
			ticketVo.setMqStatus(MQStatus.FAILED + "");
			handleProspectCreationFailed(mqUrl, xml, null, ticketVo.getProspectNo(), transactionNo, 900000,
					serviceArea);

			e.printStackTrace();

		} catch (RemoteException e) {
			ticketVo.setMqStatus(MQStatus.FAILED + "");
			LOGGER.error("Resposne " + mqUrl + " exception for MQ " + e.getMessage());
			handleProspectCreationFailed(mqUrl, xml, null, ticketVo.getProspectNo(), transactionNo, 900000,
					serviceArea);
			e.printStackTrace();

		} catch (Exception e) {
			ticketVo.setMqStatus(MQStatus.FAILED + "");
			LOGGER.error("Resposne " + mqUrl + " exception for MQ " + e.getMessage());
			handleProspectCreationFailed(mqUrl, xml, null, ticketVo.getProspectNo(), transactionNo, 90890000,
					serviceArea);

			e.printStackTrace();
		}

		mqLog.setProspectNo(ticketVo.getProspectNo());

		mongoTemplate.insert(mqLog);

		return ticketVo;
	}

	private void handleProspectCreationFailed(String url, String request, String response, String mqId,
			String transactionNo, int errorCode, String serviceArea) {

		StringBuilder text = new StringBuilder();
		EMailVO eMail = new EMailVO();
		eMail.setSubject("[PROSPECT FAIL]Prospect Creation Failed in MQ : " + mqId + " Error Code " + errorCode);

		text.append("Mq URL : " + url).append("\n\n");
		text.append("Mq TransactionNo : " + transactionNo).append("\n\n");
		text.append("Mq Service Area : " + serviceArea).append("\n\n");
		text.append("FWMP Server IP : " + fwmpServerIp).append("\n\n");
		text.append("Generated ProspectNo : " + mqId).append("\n\n");
		text.append("ErrorCode : " + errorCode).append("\n\n");
		text.append("Error Message : " + definitionCoreService.getMqErrorMessageById(Long.valueOf(errorCode + "")))
				.append("\n\n");

		text.append("Mq Input XML : " + request).append("\n\n");
		text.append("Mq Response  : " + response).append("\n\n");
		eMail.setMessage(text.toString());
		eMail.setTo(failedMqSupportEmailId);
		emailNotiFicationHelper.addNewMailToQueue(eMail);
	}

	public static void main(String[] args) {
		int i = (int) ((float) Math.random() % 2 * 1 + Math.random() % 0.2);
		System.out.println(i);
	}

	@Override
	public TicketVo createPropsectInSiebel(ProspectCoreVO prospectCoreVO) {

		TicketVo ticketVo = new TicketVo();

		try {

			LOGGER.info("prospect request for siebel " + prospectCoreVO);

			ticketVo.setProspectNo(StrictMicroSecondTimeBasedGuid.newGuid() + "");
			ticketVo.setMqStatus(MQStatus.FAILED + "");

			SalesActivityUpdateInCRMVo prospect = convert2SiebelFormat(prospectCoreVO);

			APIResponse apiResponse = crmServiceCall.callSiebelServiceForCreateUpdateProspect(prospect);

			ProspectResponseFromSiebel fromSiebel = null;

			if (apiResponse != null && apiResponse.getData() != null) {

				ObjectMapper mapper = CommonUtil.getObjectMapper();

				String stringResponse = apiResponse.getData().toString();

				fromSiebel = mapper.readValue(stringResponse, ProspectResponseFromSiebel.class);

				if (fromSiebel != null) {

					CreateProspectAPI_Output createProspectAPI_Output = fromSiebel.getCreateProspectAPI_Output();

					LOGGER.info("Response from Siebel is " + createProspectAPI_Output);

					if (createProspectAPI_Output != null) {

						if (createProspectAPI_Output.getProspectNumber() != null
								&& !createProspectAPI_Output.getProspectNumber().isEmpty())
							

						if (createProspectAPI_Output.getDeDupFlag() != null
								&& !createProspectAPI_Output.getDeDupFlag().isEmpty()
								&& DeDupFlag.cleanProspect == Integer
										.valueOf(createProspectAPI_Output.getDeDupFlag())) {
							ticketVo.setProspectNo(createProspectAPI_Output.getProspectNumber());

							ticketVo.setDeDupFlag(DeDupFlag.deDupFlagSuccess);
							ticketVo.setDeDupMessage(DeDupFlag.cleanProspectString);
							ticketVo.setMqStatus(DeDupFlag.deDupFlagSuccess + "");

						} else if (createProspectAPI_Output.getDeDupFlag() != null
								&& !createProspectAPI_Output.getDeDupFlag().isEmpty()
								&& DeDupFlag.duplicatedProspect == Integer
										.valueOf(createProspectAPI_Output.getDeDupFlag())) {

							ticketVo.setProspectNo(createProspectAPI_Output.getProspectNumber());
							ticketVo.setDeDupFlag(DeDupFlag.deDupFlagFailure);
							ticketVo.setMqStatus(DeDupFlag.deDupFlagFailure + "");
							ticketVo.setDeDupMessage(DeDupFlag.duplicatedProspectString);

						} else {
							// as discussed with Navneeth and swapnil
							//ticketVo.setDeDupFlag(DeDupFlag.deDupFlagFailure);
							//ticketVo.setMqStatus(DeDupFlag.deDupFlagFailure + "");
							//ticketVo.setDeDupMessage(DeDupFlag.duplicatedProspectString);
							LOGGER.info("Siebel failed to create prospect :: "+createProspectAPI_Output.getError_spcMessage());
						}
					} else {
						LOGGER.info("Response from Siebel is not null where as createProspectAPI_Output is null.");
					}
				} else {
					LOGGER.info("Response from Siebel is null ");
				}
			} else {
				LOGGER.info("Response of service call is null ");
			}

			return ticketVo;
		} catch (IOException e) {
			e.printStackTrace();
			return ticketVo;
		}
	}

	/**
	 * @author aditya SalesActivityUpdateInCRMVo
	 * @param createUpdateProspectVo
	 * @return
	 */
	private SalesActivityUpdateInCRMVo convert2SiebelFormat(ProspectCoreVO prospectCoreVO) {

		SalesActivityUpdateInCRMVo prospect = new SalesActivityUpdateInCRMVo();

		CreateUpdateProspectRequestInSiebel createUpdateProspectRequestInSiebel = commonToolUtil
				.convertCreateUpdateProspectVO2ProspectRequestInSiebel(prospectCoreVO);

		prospect.setCreateUpdateProspectInSiebel(createUpdateProspectRequestInSiebel);
		prospect.setTransactionId(
				createUpdateProspectRequestInSiebel.getCreateUpdateProspect_Input().getTransation_spcId());

		return prospect;
	}

	@Override
	public APIResponse updateNewProspect(ProspectUpdateVO prospect) {
		// TODO Auto-generated method stub

		return ResponseUtil.createSuccessResponse();

	}

	@Override
	public APIResponse appProspectWrapper(Long prospectId) {

		LOGGER.info("######### appProspectWrapper method called for " + prospectId);

		CustomerVO customer;
		ProspectCoreVO prospectCoreVO;
		GISPojo gisPojo;
		Long ticketId = null;
		ProspectVO prospect = appPropspectDAO.getPropspectById(prospectId);

		LOGGER.info("Response form the appProspectDAO for " + prospectId + " is " + prospect);

		if (prospect != null) {
			customer = getCustomerDetaisFromProspect(prospect);
			prospectCoreVO = getTicketDetaisFromProspect(prospect);
			gisPojo = getGISDetaisFromProspect(prospect);

			LOGGER.info("customer " + customer + " prospectCoreVO " + prospectCoreVO + " gisPojo " + gisPojo);

			customer.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
			customer.setId(StrictMicroSecondTimeBasedGuid.newGuid());

			Long custId = customerDao.addCustomer(customer).getId();

			LOGGER.info("Customer details saved " + custId);

			prospectCoreVO.getTicket().setCustomerId(custId);
			prospectCoreVO.getTicket().setStatus(TicketStatus.PROSPECT_CREATED + "");
			prospectCoreVO.getTicket().setId(StrictMicroSecondTimeBasedGuid.newGuid());

			if (prospectCoreVO.getCreationSource() == null) {
				prospectCoreVO.getTicket().setSource(TicketSource.CUSTOMER_APP);
				prospectCoreVO.setCreationSource(TicketSource.CUSTOMER_APP);
				prospectCoreVO.getTicket().setMqStatus((MQStatus.SUCCESS + ""));
			}

			if (prospect.getConnectionType().equalsIgnoreCase(CustomerAppFeasibilityType.NON_FEASIBLE)
					|| prospect.getConnectionType().equalsIgnoreCase(CustomerAppFeasibilityType.OTHERS)
					|| prospect.getConnectionType().equalsIgnoreCase(CustomerAppFeasibilityType.FIBER_FX_CHOKE)) {

			} else {

				UserVo userVo = userDao.getUsersByDeviceName(prospect.getFxName(), AuthUtils.SALES_EX_DEF);

				if (userVo != null && userVo.getId() > 0)
					prospectCoreVO.getTicket().setAssignTo(userVo.getId());

			}

			ticketId = tickectDao.createNewInstallationProspect(prospectCoreVO);

			LOGGER.info("Ticket details saved " + ticketId);

			gisPojo.setTicketNo(ticketId);

			// if (prospect.getf.equalsIgnoreCase("Copper"))
			// {
			// gis.setWorkstageType(2);
			//
			// } else if (gisResponse.getType().equalsIgnoreCase("Fiber"))
			// {
			// gis.setWorkstageType(1);
			//
			// }

			Long gisId = gisDAO.insertGISInfo(gisPojo);

			if (prospect.getFxName() != null) {
				UserVo userVo = userDao.getUsersByDeviceName(prospect.getFxName(), AuthUtils.SALES_EX_DEF);

				if (userVo != null) {
					LOGGER.info("Adding " + ticketId + " to " + userVo.getId()
							+ ". This ticket had come form Customer App");
					dbUtil.addOneTicketToQueue(userVo.getId(), ticketId);
				} else
					LOGGER.info("No SE is assosiated with FX " + prospect.getFxName());
			} else
				LOGGER.info("FX is null for prospect " + prospectId);

			LOGGER.info("Gis info saved " + gisId);

		}

		tickectDao.updateTicketStatus(ticketId, TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);

		TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();

		ticketActivityLogVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		ticketActivityLogVo.setSubActivityId(Activity.BASIC_INFO_UPDATE);

		ticketActivityLogVo.setModifiedOn(new Date());
		ticketActivityLogVo.setAddedOn(new Date());
		ticketActivityLogVo.setTicketId(ticketId);
		ticketActivityLogVo.setActivityId(Activity.BASIC_INFO_UPDATE);

		ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);

		ticketActivityLogDAOImpl.addTicketActivityLog(ticketActivityLogVo);

		if (prospect.getConnectionType()
				.equalsIgnoreCase(CustomerAppFeasibilityType.NON_FEASIBLE)
				|| prospect.getConnectionType()
						.equalsIgnoreCase(CustomerAppFeasibilityType.OTHERS)
				|| prospect.getConnectionType()
						.equalsIgnoreCase(CustomerAppFeasibilityType.FIBER_FX_CHOKE))
		{
			Long branchId = branchDAO
					.getBranchIdByName(prospect.getBranchCode()) == null
							? branchDAO.getIdByCode(prospect.getBranchCode())
							: null;

			Long seId = appPropspectDAO.getDefaultExecutiveMappingByBranchForApp(branchId);

			if (seId != null && seId > 0) {

			} else {

			}

			tickectDao.updateTicketStatusAndAssignedBy(ticketId, TicketStatus.FEASIBILITY_PENDING_BY_NE, seId);
			Query query = new Query();

			query.addCriteria(Criteria.where("prospectNumber").is(prospect.getProspectNumber()));

			mongoTemplate.findAndRemove(query, ProspectVO.class);

		} else {
			tickectDao.updateTicketStatus(ticketId, TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);

		}

		return ResponseUtil.createSaveSuccessResponse()
				.setData(ResponseUtil.createSaveSuccessResponse().getStatusMessage());

	}

	private CustomerVO getCustomerDetaisFromProspect(ProspectVO prospect) {
		CustomerVO customerVo = new CustomerVO();
		customerVo.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
		customerVo.setAddedOn(prospect.getAddedOn());
		customerVo.setAlternativeEmailId(prospect.getAlternativeEmailId());
		customerVo.setAlternativeMobileNo(prospect.getAlternativeMobileNo());
		customerVo.setPincode(prospect.getPincode() != null ? Integer.valueOf(prospect.getPincode()) : null);
		customerVo.setAreaId(areaDAO.getAreaIdByName(prospect.getBranchCode()+"_"+prospect.getAreaCode()));

		Long branchId = branchDAO.getIdByCode(prospect.getBranchCode());

		if (branchId == null)
			branchId = branchDAO.getBranchIdByName(prospect.getBranchCode());

		customerVo.setBranchId(branchId);

		Long cityId = cityDAO.getCityIdByName(prospect.getCityCode()) == null
				? cityDAO.getIdByCode(prospect.getCityCode())
				: null;

		customerVo.setCityId(cityId);

		customerVo.setCommunicationAdderss(prospect.getCommunicationAdderss());
		customerVo.setCurrentAddress(prospect.getCurrentAddress());
		customerVo.setPermanentAddress(prospect.getPermanentAddress());

		customerVo.setTitle(prospect.getTitle());
		customerVo.setFirstName(prospect.getFirstName());
		customerVo.setMiddleName(prospect.getMiddleName());
		customerVo.setLastName(prospect.getLastName());
		/*
		 * customerVo.setCustomerName(prospect.getFirstName() + " " +
		 * prospect.getLastName());
		 */
		customerVo.setCustomerProfession(prospect.getCustomerProfession());
		customerVo.setCustomerType(prospect.getCustomerType());
		customerVo.setEmailId(prospect.getEmailId());
		customerVo.setFirstName(prospect.getFirstName());
		customerVo.setId(prospect.getId());
		customerVo.setLastName(prospect.getLastName());
		customerVo.setMobileNumber(prospect.getMobileNumber());
		customerVo.setOfficeNumber(prospect.getOfficeNumber());
		customerVo.setPrefferedCallDate(prospect.getPrefferedCallDate());
		// customerVo.setSubAreaId(Long.parseLong(prospect.getSubAreaId()));
		// customerVo.setTariffId(Long.parseLong(prospect.getPackageCode()));
		customerVo.setLat(prospect.getLattitude());
		customerVo.setLon(prospect.getLongitude());
		customerVo.setEnquiryModeId(456L);
		customerVo.setSourceModeId(104L);
		customerVo.setStatus((FWMPConstant.ProspectType.HOT_ID));
		return customerVo;
	}

	private ProspectCoreVO getTicketDetaisFromProspect(ProspectVO prospect) {
		ProspectCoreVO prospectCoreVO = new ProspectCoreVO();

		TicketVo ticketVo = new TicketVo();

		UserVo assignedUser = null;

		assignedUser = userDao.getUsersByDeviceName(prospect.getFxName(), AuthUtils.SALES_EX_DEF);

		if (assignedUser != null)
			ticketVo.setAssignTo(assignedUser.getId());

		ticketVo.setPreferedDate(prospect.getPrefferedCallDate());

		ticketVo.setProspectNo(prospect.getProspectNumber());

		ticketVo.setAreaId(areaDAO.getAreaIdByName(prospect.getBranchCode()+"_"+prospect.getAreaCode()));
		ticketVo.setStatus(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED + "");
		prospectCoreVO.setTicket(ticketVo);

		return prospectCoreVO;
	}

	private GISPojo getGISDetaisFromProspect(ProspectVO prospect) {
		GISPojo gisPojo = new GISPojo();

		gisPojo.setAddedOn(prospect.getAddedOn());
		gisPojo.setArea(prospect.getAreaCode());
		gisPojo.setBranch(prospect.getBranchCode());
		gisPojo.setCity(prospect.getCityCode());
		gisPojo.setClusterName(prospect.getClusterName());
		gisPojo.setCxIpAddress(prospect.getCxIpAddress());
		gisPojo.setCxMacAddress(prospect.getCxMacAddress());
		gisPojo.setCxName(prospect.getCxName());
		gisPojo.setCxPorts(prospect.getCxPorts());
		gisPojo.setFxIpAddress(prospect.getFxIpAddress());
		gisPojo.setFxMacAddress(prospect.getFxMacAddress());
		gisPojo.setFxName(prospect.getFxName());
		gisPojo.setFxPorts(prospect.getFxPorts());
		gisPojo.setConnectionType(prospect.getConnectionType());

		if (prospect.getConnectionType().equalsIgnoreCase(CustomerAppFeasibilityType.FIBER)) {
			gisPojo.setWorkstageType(Integer.valueOf(WorkStageType.FIBER + ""));

		} else if (prospect.getConnectionType().equalsIgnoreCase(CustomerAppFeasibilityType.COPPER)) {
			gisPojo.setWorkstageType(Integer.valueOf(WorkStageType.COPPER + ""));

		} else if (prospect.getConnectionType()
				.equalsIgnoreCase(CustomerAppFeasibilityType.NON_FEASIBLE)
				|| prospect.getConnectionType()
						.equalsIgnoreCase(CustomerAppFeasibilityType.OTHERS)
				|| prospect.getConnectionType()
						.equalsIgnoreCase(CustomerAppFeasibilityType.FIBER_FX_CHOKE))
		{

		}

		gisPojo.setFeasibilityType(FeasibilityType.GIS_CUSTOMER_APP);

		gisPojo.setLattitude(prospect.getLattitude());
		gisPojo.setLongitude(prospect.getLongitude());

		return gisPojo;
	}

	private MqLog getMqInput() {
		MqLog mqLogs = new MqLog();

		mqLogs.setRequestTime(new Date().toString());
		mqLogs.setAddedBy(AuthUtils.getCurrentUserId());
		mqLogs.setMqLogId(StrictMicroSecondTimeBasedGuid.newGuid());
		return mqLogs;

	}

	public String getFailedMqSupportEmailId() {
		return failedMqSupportEmailId;
	}

	public void setFailedMqSupportEmailId(String failedMqSupportEmailId) {
		this.failedMqSupportEmailId = failedMqSupportEmailId;
	}

	public String getFwmpServerIp() {
		return fwmpServerIp;
	}

	public void setFwmpServerIp(String fwmpServerIp) {
		this.fwmpServerIp = fwmpServerIp;
	}
	
	/**@author noor
	 * CustomerAadhaarMapping
	 * @param prospect
	 * @return
	 */
	private CustomerAadhaarMapping getCustomerAadhaarMappingForProspect(ProspectCoreVO prospect) {
		
		LOGGER.info("ProspectCoreVo for CustomerAadhaarMapping : "+prospect);
		
		CustomerAadhaarMapping aadhaarMapping = new CustomerAadhaarMapping();
		
		aadhaarMapping.setTicketId(prospect.getTicket().getId());
		
		aadhaarMapping.setCustomerId(prospect.getCustomer().getId());
		aadhaarMapping.setActivityId(Activity.POI_DOCUMENT_UPDATE);
		aadhaarMapping.setAddedBy(AuthUtils.getCurrentUserId());
		
		aadhaarMapping.setAddedOn(new Date());
		
		if( prospect.getCustomer().getAadhaarTransactionType().equalsIgnoreCase(AadhaarTransactionType._PRIMARY))
			aadhaarMapping.setLabel(Long.valueOf(AadhaarTransactionType.PRIMARY));
		else if( prospect.getCustomer().getAadhaarTransactionType().equalsIgnoreCase(AadhaarTransactionType._SECONDARY))
			aadhaarMapping.setLabel(Long.valueOf(AadhaarTransactionType.SECONDARY));
	
		aadhaarMapping.setAdhaarTrnId(prospect.getCustomer().getFinahubTransactionId());
		
		return aadhaarMapping;
	}

}
