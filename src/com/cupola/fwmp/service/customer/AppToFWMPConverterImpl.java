/**
 * 
 */
package com.cupola.fwmp.service.customer;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.sales.SalesDAO;
import com.cupola.fwmp.vo.integ.app.ProspectVO;
import com.cupola.fwmp.vo.sales.PaymentUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

/**
 * @author aditya
 *
 */
public class AppToFWMPConverterImpl implements AppToFWMPConverter
{

	@Override
	public UpdateProspectVO updateBasicInfo(ProspectVO prospectVO)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TariffPlanUpdateVO updateTariff(ProspectVO prospectVO)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentUpdateVO updatePayment(ProspectVO prospectVO)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentUpdateVO uploadDoucuments(ProspectVO prospectVO)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
