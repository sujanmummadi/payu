package com.cupola.fwmp.service.deploymentSetting;

import com.cupola.fwmp.persistance.entities.DeploymentSetting;
import com.cupola.fwmp.response.APIResponse;

public interface DeploymentSettingCoreService {
	
	public APIResponse addDeploymentSetting(DeploymentSetting deploymentSetting);

	public APIResponse getDeploymentSettingById(Long id);

	public APIResponse getAllDeploymentSetting();
	
	public APIResponse getAllDeploymentSettingFromGlobalCache();
	
	public APIResponse getAllDeploymentSettingBasedOnKey(String key);

	public APIResponse deleteDeploymentSetting(Long id);

	public APIResponse updateDeploymentSetting(DeploymentSetting deploymentSetting);
	
	

}
