package com.cupola.fwmp.service.deploymentSetting;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.deploymentSetting.DeploymentSettingDAOImpl;
import com.cupola.fwmp.persistance.entities.DeploymentSetting;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.DeploymentSettingVo;

public class DeploymentSettingCoreServiceImpl implements
		DeploymentSettingCoreService
{

	private static Logger log = LogManager
			.getLogger(DeploymentSettingCoreServiceImpl.class.getName());

	@Autowired
	DeploymentSettingDAOImpl deploymentSettingDAOImpl;

	@Autowired
	GlobalActivities globalActivities;

	@Override
	public APIResponse addDeploymentSetting(DeploymentSetting deploymentSetting)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getDeploymentSettingById(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public void init()
	{
		log.info("DeploymentSettingCoreServiceImpl init method called..");
		getAllDeploymentSetting();
		log.info("DeploymentSettingCoreServiceImpl init method executed.");

	}

	@Override
	public APIResponse getAllDeploymentSetting()
	{

		List<DeploymentSettingVo> deploymentSetting = deploymentSettingDAOImpl
				.getAllDeploymentSetting();

		for (DeploymentSettingVo deploymentVO : deploymentSetting)
		{

			globalActivities
					.addToDeploymentSetting(deploymentVO.getName(), deploymentVO
							.getValue());

		}

		return ResponseUtil.createSuccessResponse().setData(deploymentSetting);

	}

	@Override
	public APIResponse deleteDeploymentSetting(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateDeploymentSetting(
			DeploymentSetting deploymentSetting)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getAllDeploymentSettingFromGlobalCache()
	{

		if (globalActivities.getDeploymentSetting().isEmpty())
		{

			log.debug("From DB all setting");

			return getAllDeploymentSetting();

		} else
		{

			log.debug("From Cache all setting");

			return ResponseUtil.createSuccessResponse()
					.setData(globalActivities.getDeploymentSetting());

		}

	}

	@Override
	public APIResponse getAllDeploymentSettingBasedOnKey(String key)
	{

		if (globalActivities.getDeploymentSetting().isEmpty())
		{

			log.debug("From DB based on key");

			getAllDeploymentSetting();

			if (globalActivities.getDeploymentSetting().containsKey(key))
			{

				return ResponseUtil.createSuccessResponse()
						.setData(globalActivities.getDeploymentSetting()
								.get(key));
			} else
			{

				return ResponseUtil.createRecordNotFoundResponse();

			}

		} else
		{

			log.debug("From Cache based on key");

			if (globalActivities.getDeploymentSetting().containsKey(key))
			{

				return ResponseUtil.createSuccessResponse()
						.setData(globalActivities.getDeploymentSetting()
								.get(key));
			} else
			{

				return ResponseUtil.createRecordNotFoundResponse();

			} // End of contains else condition

		}// End of is Empty Condition

	}// End of method

}
