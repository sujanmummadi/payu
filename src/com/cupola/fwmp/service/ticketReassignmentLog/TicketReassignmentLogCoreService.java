 /**
 * @Author aditya  25-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.ticketReassignmentLog;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;

/**
 * @Author aditya  25-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface TicketReassignmentLogCoreService {
	
	APIResponse add2TicketReassignmentLog(TicketReassignmentLogVo ticketReassignmentLogVo);
}
