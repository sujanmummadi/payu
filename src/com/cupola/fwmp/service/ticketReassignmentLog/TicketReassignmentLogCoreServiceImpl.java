/**
* @Author aditya  25-Aug-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.service.ticketReassignmentLog;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.ticketReassignmentLog.TicketReassignmentLogDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;

/**
 * @Author aditya 25-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class TicketReassignmentLogCoreServiceImpl implements TicketReassignmentLogCoreService {

	private Logger log = LogManager.getLogger(TicketReassignmentLogCoreServiceImpl.class.getName());

	@Autowired
	TicketReassignmentLogDAO ticketReassignmentLogDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.ticketReassignmentLog.
	 * TicketReassignmentLogCoreService#add2TicketReassignmentLog(com.cupola.fwmp.vo
	 * .TicketReassignmentLogVo)
	 */
	@Override
	public APIResponse add2TicketReassignmentLog(TicketReassignmentLogVo ticketReassignmentLogVo) {

		if (ticketReassignmentLogVo == null)
			return ResponseUtil.createNullParameterResponse();

		if (ticketReassignmentLogVo != null && ticketReassignmentLogVo.getTicketId() <= 0)
			return ResponseUtil.createNullParameterResponse().setData("TicketId could not be null");

		if (ticketReassignmentLogVo != null && ticketReassignmentLogVo.getAssignedBy() <= 0)
			return ResponseUtil.createNullParameterResponse().setData("Assigned by could not be null");

		if (ticketReassignmentLogVo != null && ticketReassignmentLogVo.getAssignedFrom() <= 0)
			return ResponseUtil.createNullParameterResponse().setData("Assigned from could not be null");

		if (ticketReassignmentLogVo != null && ticketReassignmentLogVo.getAssignedTo() <= 0)
			return ResponseUtil.createNullParameterResponse().setData("Assigned to could not be null");

		log.info("Adding ticketReassignmentLog for Ticket " + ticketReassignmentLogVo.getTicketId());

		TicketReassignmentLogVo ticketReassignmentLogDAOVo = ticketReassignmentLogDAO
				.add2TicketReassignmentLog(ticketReassignmentLogVo);
	
		log.info("Added ticketReassignmentLog for Ticket " + ticketReassignmentLogVo.getTicketId());

		return ResponseUtil.createSuccessResponse().setData(ticketReassignmentLogDAOVo);
	}

}
