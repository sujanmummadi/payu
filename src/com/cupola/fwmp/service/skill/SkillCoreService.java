package com.cupola.fwmp.service.skill;

import com.cupola.fwmp.persistance.entities.Skill;
import com.cupola.fwmp.response.APIResponse;

public interface SkillCoreService {
	
	public APIResponse addSkill(Skill skill);

	public APIResponse getSkillById(Long id);

	public APIResponse getAllSkill();

	public APIResponse deleteSkill(Long id);

	public APIResponse updateSkill(Skill skill);

}
