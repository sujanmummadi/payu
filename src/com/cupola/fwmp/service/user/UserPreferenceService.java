package com.cupola.fwmp.service.user;

import java.util.List;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.NotificationOptions;

public interface UserPreferenceService {

	public UserPreference createUserPreference (Long userGuid, UserPreferenceVO userPref);
	
	public List<UserPreference> getUserPreferencesByUserId (Long userGuid);

	public List<UserPreference> getUserPreferencesByUserLoginId (String userLoginId);
	
	public UserPreference getUserPreference (Long userI, String prefName);
	
	public UserPreference updateUserPreference (Long userI, UserPreferenceVO userPref);
	
	
	public void deleteUserPrefereceByGuid(Long Id);

	UserPreference getUserPreferencesByPreferenceName(Long userId,
			String preferenceName);

	APIResponse createOrUpdateNotificationPreference(NotificationOptions options);

	APIResponse getNotificationOption();
}
