package com.cupola.fwmp.service.user;

public interface UserPreference {
	
	public Long getUserId ();

	public String getUserLoginId ();
	
	public String getPrefName ();

	public String getPrefStringValue ();
	
	public Long getUserGroupId();

}
