package com.cupola.fwmp.service.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.vo.TypeAheadVo;

public class UserHelper
{
	@Autowired
	private UserDao userDao;
	
	
	public boolean isFRTLUser(Long userId)
	{
		if( userId == null)
			userId = 0l;
		
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if(roles == null || roles.isEmpty() )
			return false;
		return (roles.contains("ROLE_FR") && roles.contains("ROLE_TL"));
	}
	
	public boolean isFRAreaManager(Long userId)
	{
		if( userId == null)
			userId = 0l;
		
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if(roles == null || roles.isEmpty() )
			return false;
		return (roles.contains("ROLE_FR") && roles.contains("ROLE_AM"));
	}
	
	public boolean isFRBranchManager(Long userId)
	{
		if( userId == null)
			userId = 0l;
		
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if(roles == null || roles.isEmpty() )
			return false;
		return (roles.contains("ROLE_FR") && roles.contains("ROLE_BM"));
	}
	
	public boolean isFRCityManager(Long userId)
	{
		if( userId == null)
			userId = 0l;
		
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if(roles == null || roles.isEmpty() )
			return false;
		return (roles.contains("ROLE_FR") && roles.contains("ROLE_CM"));
	}
	
	public boolean isFRManager(Long userId)
	{
		if( userId == null)
			userId = 0l;
		
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if(roles == null || roles.isEmpty() )
			return false;
		return (roles.contains("ROLE_FR") && (roles.contains("ROLE_CM") 
				|| roles.contains("ROLE_BM") || roles.contains("ROLE_AM")) );
	}
	
	public boolean isFRNEUser(Long userId)
	{
		if( userId == null)
			userId = 0l;
		
		List<String> roles = userDao.getAllRolesByUserId(userId);
		if(roles == null || roles.isEmpty() )
			return false;
		return ( roles.contains("ROLE_FR") && roles.contains("ROLE_NE") ) ;
	}
	
	public TypeAheadVo getGroupNameByUser(Long userId)
	{
		return userDao.getUserGroupName(userId);
	}
	
	public List<Long> getAllTlsByUsers( long managerId )
	{
		List<Long> user = new ArrayList<>();
		if( managerId <= 0 )
			return user;
		
		Map<Long,String> users = userDao.getNestedReportingUsersWithRoles(managerId,
				Arrays.asList("ROLE_TL"), true );
		
		if( users != null && !users.isEmpty())
			user.addAll(users.keySet());
		
		return user;
	}
	

	public List<Long> getAllAmsByUsers( long managerId )
	{
		List<Long> user = new ArrayList<>();
		if( managerId <= 0 )
			return user;
		
		Map<Long,String> users = userDao.getNestedReportingUsersWithRoles(managerId, 
				Arrays.asList("ROLE_AM"), true );
		
		if( users != null && !users.isEmpty())
			user.addAll(users.keySet());
		
		return user;
	}
	
	public List<Long> getAllBmsByUsers( long managerId )
	{
		List<Long> user = new ArrayList<>();
		if( managerId <= 0 )
			return user;
		
		Map<Long,String> users = userDao.getNestedReportingUsersWithRoles(managerId, 
				Arrays.asList("ROLE_BM"), true );
		
		if( users != null && !users.isEmpty())
			user.addAll(users.keySet());
		
		return user;
	}
	
	public List<Long> getNestedManagers( long managerId )
	{
		List<Long> user = new ArrayList<>();
		if( managerId <= 0 )
			return user;
		
		Map<Long, String> users = userDao.getNestedReportingUsers(managerId,
				null, null, null);
		
		if( users != null && !users.isEmpty())
		{
			for( Map.Entry<Long, String> entry : users.entrySet())
			{
				if( !isFRNEUser(entry.getKey()))
					user.add(entry.getKey());
			}
		}
		return user;
	}
	
	public boolean isHYDUser( Long userId )
	{
		if( userId == null || userId.longValue() <= 0 )
			return false;
		
		TypeAheadVo cityName = userDao.getCityNameByUserId( userId );
		if( cityName != null && cityName.getName() != null 
				&& cityName.getName().equalsIgnoreCase(CityName.HYDERABAD_CITY))
			return true;
		else
			return false;
		  
	}
	
	public boolean isROIUser( Long userId )
	{
		if( userId == null || userId.longValue() <= 0 )
			return false;
		
		TypeAheadVo cityName = userDao.getCityNameByUserId( userId );
		if( !(cityName != null && cityName.getName() != null 
				&& cityName.getName().equalsIgnoreCase(CityName.HYDERABAD_CITY)))
			return true;
		else
			return false;
		  
	}
}
