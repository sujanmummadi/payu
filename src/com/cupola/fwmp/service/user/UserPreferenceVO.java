package com.cupola.fwmp.service.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "UserPreference")
public class UserPreferenceVO implements UserPreference{
	
	@Id
	private Long id;
	private Long userId;
	private String userLoginId;
	private String prefName;
	private Long userGroupId;	
	private String prefStringValue;
	
	public UserPreferenceVO(){}
	
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserLoginId() {
		return userLoginId;
	}

	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}

	public String getPrefName() {
		return prefName;
	}
	
	public void setPrefName(String prefName) {
		this.prefName = prefName;
	}
	
	public String getPrefStringValue() {
		return prefStringValue;
	}
	
	public void setPrefStringValue(String prefStringValue) {
		this.prefStringValue = prefStringValue;
	}

	public Long getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}
	
}
