/**
 * 
 */
package com.cupola.fwmp.service.user;

/**
 * @author aditya
 *
 */
public class ResetUserVo
{
	private Long userId;
	private String newPassword;
	private String loginId;

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getNewPassword()
	{
		return newPassword;
	}

	public void setNewPassword(String newPassword)
	{
		this.newPassword = newPassword;
	}

	public String getLoginId()
	{
		return loginId;
	}

	public void setLoginId(String loginId)
	{
		this.loginId = loginId;
	}

	@Override
	public String toString()
	{
		return "ResetUserVo [userId=" + userId + ", newPassword=" + newPassword
				+ ", loginId=" + loginId + "]";
	}

}
