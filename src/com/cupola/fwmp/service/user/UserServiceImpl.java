package com.cupola.fwmp.service.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.BooleanValue;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.sales.SalesDAO;
import com.cupola.fwmp.dao.subArea.SubAreaDAO;
import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.userGroup.UserGroupDAO;
import com.cupola.fwmp.dao.vendor.VendorDAO;
import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.persistance.entities.DefaultUserAreaMapping;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.util.StatusMessages;
import com.cupola.fwmp.vo.LoginPojo;
import com.cupola.fwmp.vo.LoginResponseVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserBulkUploadVO;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.gis.GISNotFeasibleVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

public class UserServiceImpl implements UserService
{

	final static Logger log = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	private DBUtil dbUtil;
	@Autowired
	GlobalActivities globalActivities;
	@Autowired
	GisDAO gisDAO;

	@Autowired
	VendorDAO vendorDAO;

	@Autowired
	SalesDAO salesDAO;

	private UserDao userDao;

	private TabletDAO tabletDao;

	private AreaDAO areDao;

	private BranchDAO branchDao;

	private CityDAO cityDao;

	private SubAreaDAO subAreaDao;

	private DeviceDAO deviceDao;

	@Autowired
	UserGroupDAO userGroupDAO;

	private String resetUserUrl;
	private String servers;

	public void setTabletDao(TabletDAO tabletDao)
	{
		this.tabletDao = tabletDao;
	}

	public void setUserDao(UserDao userDao)
	{
		this.userDao = userDao;
	}

	public void setAreDao(AreaDAO areDao)
	{
		this.areDao = areDao;
	}

	public void setBranchDao(BranchDAO branchDao)
	{
		this.branchDao = branchDao;
	}

	public void setCityDao(CityDAO cityDao)
	{
		this.cityDao = cityDao;
	}

	public void setSubAreaDao(SubAreaDAO subAreaDao)
	{
		this.subAreaDao = subAreaDao;
	}

	public void setDeviceDao(DeviceDAO deviceDao)
	{
		this.deviceDao = deviceDao;
	}

	public APIResponse loginForApp(LoginPojo loginPojo)
	{

		log.debug("credentials::" + loginPojo);

		LoginResponseVo responseVo = userDao.loginForApp(loginPojo);

		if (responseVo.getStatus() == 200)
		{

			String result = setCurrentUser(loginPojo, responseVo.getUserId());

			if (result.equalsIgnoreCase("success"))
				log.info("User is set to tab : " + loginPojo.getDeviceId());
			else
				log.info("Failed to set user to tab : "
						+ loginPojo.getDeviceId());

			return ResponseUtil.createSuccessResponse().setData(responseVo);
		} else if (responseVo.getStatus() == StatusCodes.BAD_CREDENTIALS)
			return ResponseUtil.createBadCredentialsExceptionResponse();

		else if (responseVo.getStatus() == StatusCodes.NO_RECORD_FOUND)
			return ResponseUtil.createRecordNotFoundResponse();

		else
			return ResponseUtil.createDBExceptionResponse();

	}

	public User findByUserName(String username)
	{

		log.info("find by user name::" + username);

		return userDao.findByUserName(username);
	}

	public APIResponse resetPassword(String oldpassword, String newpassword)
	{
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		
		String name = auth.getName();
		
		log.info("resetPassword@@@@@@@@@s for loginId " + name + " "
				+ oldpassword + " " + newpassword);

		String statusMessage = userDao
				.resetPassword(name, oldpassword, newpassword);

		if (statusMessage.equals(StatusMessages.DUPLICATE_RECORD))
			return ResponseUtil.createDuplicateResponse();

		if (statusMessage.equals(StatusMessages.UPDATED_SUCCESSFULLY))
			return ResponseUtil.createUpdateSuccessResponse();

		else if (statusMessage.equals(StatusMessages.BAD_CREDENTIALS))
			return ResponseUtil.createBadCredentialsExceptionResponse();

		else if (statusMessage.equals(StatusMessages.NO_RECORD_FOUND))
			return ResponseUtil.createRecordNotFoundResponse();

		else
			return ResponseUtil.createDBExceptionResponse();
	}

	@Override
	public APIResponse createNewPassword(Long userId, String newPassword)
	{
		return ResponseUtil.createSaveSuccessResponse()
				.setData(userDao.createNewPassword(userId, newPassword));
	}

	public APIResponse getUserName()
	{
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String name = auth.getName(); // get logged in username

		log.info("user name::::::::::" + name);
		return ResponseUtil.createSuccessResponse().setData(name);
	}

	public APIResponse addUser(UserVo usr)
	{

		log.info("User#######" + usr);

		User user = userDao.addUser(usr);

		if (user.getStatus() != null
				&& user.getStatus() == StatusCodes.DUPLICATE_RECORD)
			return ResponseUtil.createDuplicateResponse();

		else if (user.getStatus() == 1)
		{
			log.info("$$$$$$$$$$$ " + resetUserUrl + " servers "
					+ servers);

			String serv = servers;

			String[] serverArr = serv.split(",");

			for (int i = 0; i < serverArr.length; i++)
			{
				String serverIp = serverArr[i];

				try
				{
					log.debug("server " + serverIp);
					addToCacheServiceCall(usr, serverIp);
				} catch (Exception e)
				{
					e.printStackTrace();
					log.error("server got error while reseting user cache "
							+ serverIp + " . " + e.getMessage());
				}
			}

			return ResponseUtil.createSaveSuccessResponse();

		} else
			return ResponseUtil.createDBExceptionResponse();
	}

	public APIResponse updateUser(UserVo userVo)
	{

		log.info("User#######" + userVo);

		User user = userDao.updateUser(userVo);

		if (user.getStatus() == StatusCodes.NO_RECORD_FOUND)
			return ResponseUtil.createRecordNotFoundResponse();

		else if (user.getStatus() == 1 || user.getStatus() == 0)
			return ResponseUtil.createUpdateSuccessResponse();

		else
			return ResponseUtil.createDBExceptionResponse();
	}

	public APIResponse deleteUser(UserVo userVo)
	{

		log.info("User#######" + userVo);

		if (userVo.getId() == 1)
		{
			return ResponseUtil.createBadCredentialsExceptionResponse();
		}

		
		UserVo user = userDao.deleteUser(userVo);

		if (user.getStatus() == StatusCodes.DELETE_FAILED)
			return ResponseUtil.createDeleteFailedResponse();

		else if (user.getStatus() == 0)
			return ResponseUtil.createDeleteSuccessResponse();

		else
			return ResponseUtil.createDBExceptionResponse();
	}

	public APIResponse getAllUsers()
	{

		log.debug("get all users");

		List<UserVo> userVos = userDao.getAllUsers();

		if (userVos.isEmpty())
			return ResponseUtil.createRecordNotFoundResponse();

		else
			return ResponseUtil.createSuccessResponse().setData(userVos);
	}

	@Override
	public User getUserByUserName(String name)
	{
		return userDao.getUserByUserName(name);
	}

	/*
	 * @Override public APIResponse addUserRole(String userLoginId, List<String>
	 * userRoles) {
	 * 
	 * User user = new User(); user.setLoginId(userLoginId); return
	 * userDao.addUserGroup(user, userRoles);
	 * 
	 * }
	 */

	@Override
	public List<String> getAllElementMacs()
	{
		return userDao.getAllElementMacs();
	}

	/*
	 * @Override public APIResponse addUserGroup(String userLoginId,
	 * List<String> userGroups) {
	 * 
	 * log.info("user userGroups:" + userGroups + " for user:" + userLoginId);
	 * 
	 * User user = new User(); user.setLoginId(userLoginId);
	 * 
	 * return userDao.addUserGroup(user, userGroups);
	 * 
	 * }
	 */

	/*
	 * @Override public APIResponse addElement(List<Element> elements) {
	 * 
	 * return userDao.addElement(elements); }
	 */

	@Override
	public List<TypeAheadVo> userSearchSuggestion(String userQuery)
	{

		return CommonUtil.xformToTypeAheadFormat(userDao
				.userSearchSuggestion(userQuery));

	}

	@Override
	public List<TypeAheadVo> tabSearchSuggestion(String userQuery)
	{

		return tabletDao.tabSearchSuggestion(userQuery);

	}

	@Override
	public List<TypeAheadVo> elementSearchSuggestion(String elementQuery)
	{

		return CommonUtil.xformToTypeAheadFormat(userDao
				.elementSearchSuggestion(elementQuery));

	}

	/*
	 * @Override public APIResponse getUserListByPageIndex(int index, int
	 * itemPerPage) {
	 * 
	 * return userDao.getUserListByPageIndex(index, itemPerPage); }
	 */

	@Override
	public List<TypeAheadVo> getVendorsByBranchName(TypeAheadVo aheadVo)
	{

		return CommonUtil.xformToTypeAheadFormat(vendorDAO
				.getVendorsByBranchId(aheadVo));

	}

	/*
	 * @Override public List<String> getVendorTypesByVendorName(String
	 * vendorName) {
	 * 
	 * return userDao.getVendorTypesByVendorName(vendorName);
	 * 
	 * }
	 */

	@Override
	public List<TypeAheadVo> getSkillsByVendorName(TypeAheadVo aheadVo)
	{

		return CommonUtil
				.xformToTypeAheadFormat(userDao.getSkillsByVendorName(aheadVo));

	}

	@Override
	public List<TypeAheadVo> getSubAreasByAreaName(String areaName)
	{

		return CommonUtil.xformToTypeAheadFormat(subAreaDao
				.getSubAreasByAreaName(areaName));

	}

	@Override
	public String setCurrentUser(LoginPojo loginPojo, Long userId)
	{

		// User user = userDao.findByUserName(loginPojo.getUsername());

		return tabletDao.setCurrentUser(loginPojo, userId);
	}

	@Override
	public APIResponse checkCurrentUser(LoginPojo loginPojo)
	{
		String response = tabletDao.checkCurrentUser(loginPojo);

		if (response.contains("true"))
		{

			log.info("User:" + loginPojo.getUsername()
					+ " is currently associated with tab");

			return ResponseUtil.createUserAssociatedWithTabTrueResponse();
		}

		else
		{

			return ResponseUtil.createUserAssociatedWithTabFalseResponse();
		}

	}

	@Override
	public List<String> getUserRoles()
	{

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		List<String> roles = new ArrayList<String>();
		for (GrantedAuthority auth : authentication.getAuthorities())
		{
			roles.add(auth.getAuthority());
		}
		return roles;
	}

	@Override
	public List<TypeAheadVo> getAllUserNames(Long userType)
	{
		return CommonUtil
				.xformToTypeAheadFormat(userDao.getAllUserNames(userType));
	}

	@Override
	public APIResponse getUsers(UserFilter filter)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(userDao.getUsers(filter));
	}

	@Override
	public List<TypeAheadVo> getAreasByBranchName(String branchName)
	{
		return CommonUtil.xformToTypeAheadFormat(areDao
				.getAreasByBranchName(branchName));
	}

	@Override
	public List<TypeAheadVo> citySearchSuggestion(String cityQuery)
	{
		return CommonUtil.xformToTypeAheadFormat(cityDao
				.citySearchSuggestion(cityQuery));

	}

	@Override
	public List<TypeAheadVo> getBranchesByCityName(String cityName)
	{
		return CommonUtil.xformToTypeAheadFormat(branchDao
				.getBranchesByCityName(cityName));
	}
	
	@Override
	public List<TypeAheadVo> getAssociatedUsers(Long areaId,
			Boolean includeReportTo)
	{
		long currentUserId = AuthUtils.getCurrentUserId();
		Map<Long, String> users = userDao
				.getNestedReportingUsers(currentUserId, null, areaId, null);
		if (includeReportTo)
		{
			long reportToUserId = userDao.getReportToUser(currentUserId);
			UserVo vo = userDao.getUserById(reportToUserId);
			if (vo != null && reportToUserId != FWMPConstant.SYSTEM_ENGINE)
			{
				users.put(vo.getId(), GenericUtil.buildUserSelectOption(vo));
			}
		}
		
		return CommonUtil.xformToTypeAheadFormat(users);
		
	}
	
	/*public List<TypeAheadVo> getAssociatedUsers (Long areaId,
			Boolean includeReportTo)
	{
		long currentUserId = AuthUtils.getCurrentUserId();

		String[] niRoles = { "ROLE_NI", "ROLE_NE_COPPER", "ROLE_NE_FIBER" };
		List<String> roles = null;

		if (AuthUtils.isNIManager())
			roles = Arrays.asList(niRoles);
		else if (AuthUtils.isFrManager())
			roles = Arrays.asList("ROLE_FR");
		else if (AuthUtils.isSalesManager())
			roles = Arrays.asList("ROLE_SALES");
		else if (AuthUtils.isCFEUser())
			roles = Arrays.asList("ROLE_SALES", UserRole.ROLE_EX);

		else if (AuthUtils.isFRTLUser() || AuthUtils.isNITLUser()
				|| AuthUtils.isSalesTlUser())
		{

			Map<Long, String> users = userDao
					.getAssociatedUsers(currentUserId, null);

			if (AuthUtils.isFRTLUser())
			{
				Map<Long, String> frUsers = new HashMap<Long, String>();
				for (Entry<Long, String> entry : users.entrySet())
				{
					if (AuthUtils.isFrCxDownNEUser(entry.getKey())
							|| AuthUtils.isFrCxUpNEUser(entry.getKey()))
						frUsers.put(entry.getKey(), entry.getValue());
				}
				users = frUsers;
			}
			if (includeReportTo)
			{
				long reportToUserId = userDao.getReportToUser(currentUserId);
				UserVo vo = userDao.getUserById(reportToUserId);
				if (vo != null)
				{
					users.put(vo.getId(), GenericUtil
							.buildUserSelectOption(vo));
				}
			}
			return CommonUtil.xformToTypeAheadFormat(users);
		} else if (AuthUtils.isSalesUser() || AuthUtils.isNEUser())
		{
			long reportToUserId = userDao.getReportToUser(currentUserId);
			UserVo vo = userDao.getUserById(reportToUserId);
			if (vo != null)
			{
				TypeAheadVo ta = new TypeAheadVo();
				ta.setId(vo.getId());
				ta.setName(GenericUtil.buildUserSelectOption(vo));
				return Collections.singletonList(ta);
			}

		}

		// Need to revisit this in case department wise assignment require
		else if (AuthUtils.isNEUser())
		{
			long reportToUserId = 0l;
			List<UserVo> users = userDao
					.getUsersAssosiatedToDevice(currentUserId, "ROLE_NE", "ROLE_SALES");

			if (reportToUserId > 0)
			{
				UserVo user = userDao.getUserById(reportToUserId);
				if (user != null)
				{
					if (users == null)
						users = new ArrayList<UserVo>();

					users.add(user);
				}
			}
			List<TypeAheadVo> list = new ArrayList<TypeAheadVo>();
			for (UserVo vo : users)
			{
				list.add(new TypeAheadVo(vo.getId(), GenericUtil
						.buildUserSelectOption(vo)));
			}

			return list;
		}

		if (roles == null)
			return CommonUtil.xformToTypeAheadFormat(userDao
					.getAssociatedUsers(AuthUtils.getCurrentUserId(), null));
		else
		{
			List<Long> areaIds = new ArrayList<Long>();

			if (areaId != null && areaId > 0)
			{

				areaIds.add(areaId);

			} else
			{
						if (!AuthUtils.isCFEUser()
						&& AuthUtils.getCurrentUserArea() != null
						&& AuthUtils.getCurrentUserArea().getId() != null)
					areaIds.add(AuthUtils.getCurrentUserArea().getId());
				else if (AuthUtils.getCurrentUserBranch() != null
						&& AuthUtils.getCurrentUserBranch().getId() != null)
					areaIds.addAll(areDao.getAreasByBranchId(AuthUtils
							.getCurrentUserBranch().getId() + "").keySet());

				else if (AuthUtils.getCurrentUserCity() != null
						&& AuthUtils.getCurrentUserCity().getId() != null)
				{
					Map<Long, String> branches = branchDao
							.getBranchesByCityId(AuthUtils.getCurrentUserCity()
									.getId() + "");
					areaIds.addAll(areDao
							.getAreasByBranchIds(new ArrayList<Long>(branches
									.keySet()))
							.keySet());
				}
				areaIds = AuthUtils.getAreaIdsForCurrentUser();
			}
			List<Long> branches = null;
			if (AuthUtils.isCityManager())
			{
				branches = new ArrayList<Long>(LocationCache
						.getBranchesByCityId(AuthUtils.getCurrentUserCity()
								.getId() + "")
						.keySet());
			}
			return CommonUtil.xformToTypeAheadFormat(userDao
					.getUserForAreas(areaIds, branches, roles, null));
		}
	}

	*/
	
	@Override
	public List<TypeAheadVo> getAssociatedUsersForContext(Long areaId,
			Boolean includeReportTo, Integer pageContext)
	{
		if (FWMPConstant.CAF_ALLOCATION.equals(pageContext))
		{
			return getAssociatedUsersForCaf();
		} else
			return getAssociatedUsers(areaId, includeReportTo);

		/*
		 * List<Long> areaIds = new ArrayList<Long>();
		 * 
		 * if (areaId != null && areaId > 0) {
		 * 
		 * areaIds.add(areaId);
		 * 
		 * } else { areaIds = AuthUtils.getAreaIdsForCurrentUser();
		 * 
		 * }
		 * 
		 * return CommonUtil.xformToTypeAheadFormat(userDao
		 * .getUserForAreasWithRoles(areaIds, roles, true));
		 */
	}
	
	private List<TypeAheadVo> getAssociatedUsersForCaf()
	{
		Map<Long, String> associatedWithContext = new HashMap<Long, String>();
		
		Map<Long, String> reportingTls = userDao
				.getReportingUsers(AuthUtils.getCurrentUserId());
			if(reportingTls != null)
			{
				for(Long tlId : reportingTls.keySet())
				{
					associatedWithContext.putAll(userDao.getReportingUsers(tlId));
				}
			}
			
			Map<Long, String> associatedUsers = new HashMap<Long, String>();
			
			
			for(Long userId : associatedWithContext.keySet())
			{
			associatedUsers.put(userId, GenericUtil
					.buildUserSelectOption(userDao.getUserById(userId)));
			}
			
			return CommonUtil.xformToTypeAheadFormat(associatedUsers); 
	 
		 
	}
	
	@Override
	public List<TypeAheadVo> getAssociatedUsersByGroup(Long areaId,
			Long branchId, long userGroupId)
	{
		long currentUserId = AuthUtils.getCurrentUserId();

		if (AuthUtils.isAdmin())
			currentUserId = FWMPConstant.SYSTEM_ENGINE;

		Map<Long, String> users = userDao
				.getNestedReportingUsers(currentUserId, branchId, areaId, userGroupId);

		return CommonUtil.xformToTypeAheadFormat(users);
	}
	
	/*@Override
	public List<TypeAheadVo> getAssociatedUsersByGroup(Long areaId,
			Long branchId, long userGroupId)
	{
		long currentUserId = AuthUtils.getCurrentUserId();
		List<String> roles = userGroupDAO.getRolesByUserGroupId(userGroupId);

		if (AuthUtils.isFRTLUser() || AuthUtils.isNITLUser()
				|| AuthUtils.isSalesTlUser())
		{

			Map<Long, String> users = userDao
					.getAssociatedUsers(currentUserId, userGroupId);

			if (AuthUtils.isFRTLUser())
			{
				Map<Long, String> frUsers = new HashMap<Long, String>();
				for (Entry<Long, String> entry : users.entrySet())
				{
					if (AuthUtils.isFrCxDownNEUser(entry.getKey())
							|| AuthUtils.isFrCxUpNEUser(entry.getKey()))
						frUsers.put(entry.getKey(), entry.getValue());
				}
				users = frUsers;
			}

			long reportToUserId = userDao.getReportToUser(currentUserId);
			UserVo vo = userDao.getUserById(reportToUserId);

			if (vo != null && isUserContainsAllRole(vo, roles))
			{
				users.put(vo.getId(), GenericUtil.buildUserSelectOption(vo));
			}

			return CommonUtil.xformToTypeAheadFormat(users);
		}

		List<Long> areaIds = new ArrayList<Long>();

		if (areaId != null && areaId > 0)
		{

			areaIds.add(areaId);

		} else
		{
			if (!AuthUtils.isCFEUser() && AuthUtils.getCurrentUserArea() != null
					&& AuthUtils.getCurrentUserArea().getId() != null)
				areaIds.add(AuthUtils.getCurrentUserArea().getId());
			else if (AuthUtils.getCurrentUserBranch() != null
					&& AuthUtils.getCurrentUserBranch().getId() != null)
				areaIds.addAll(areDao.getAreasByBranchId(AuthUtils
						.getCurrentUserBranch().getId() + "").keySet());

			else if (AuthUtils.getCurrentUserCity() != null
					&& AuthUtils.getCurrentUserCity().getId() != null)
			{
				Map<Long, String> branches = branchDao
						.getBranchesByCityId(AuthUtils.getCurrentUserCity()
								.getId() + "");
				areaIds.addAll(areDao
						.getAreasByBranchIds(new ArrayList<Long>(branches
								.keySet()))
						.keySet());
			}
		}
		return CommonUtil.xformToTypeAheadFormat(userDao
				.getUserForAreas(areaIds, Collections
						.singletonList(branchId), roles, userGroupId));
	}*/

	@Override
	public List<TypeAheadVo> getDevicesByBranchId(String branchId)
	{
		return CommonUtil.xformToTypeAheadFormat(deviceDao
				.getDevicesByBranchId(branchId));
	}

	@Override
	public APIResponse getUserByDeviceMac(String mac, String role)
	{

		UserVo userVo = userDao.getUsersByDeviceName(mac, role);

		return ResponseUtil.createSuccessResponse().setData(userVo);

	}

	@Override
	public APIResponse getUsersOfAssosiatedDevice(
			GISNotFeasibleVO gisNotFeasibleVO)
	{
		Long userId = AuthUtils.getCurrentUserId();

		Set<TypeAheadVo> userNameIdList = new LinkedHashSet<>();
		log.info("gisNotFeasibleVO.getTicketId() " + gisNotFeasibleVO);

		if (AuthUtils.getCurrentUserRole() == UserRole.NE_NI)
		{
			GISPojo gisPojo = gisDAO
					.getGisInfoByTicketId(gisNotFeasibleVO.getTicketId());

			log.info("Gis response for ticket " + gisNotFeasibleVO.getTicketId()
					+ " in Deployment role are " + gisPojo);

			if (gisPojo != null)
			{
				UserVo userVo = userDao.getUsersByDeviceName(gisPojo
						.getFxName(), AuthUtils.SALES_EX_DEF);

				log.info("Sales user details for ticket "
						+ gisNotFeasibleVO.getTicketId() + "  are " + userVo);

				if (userVo == null)
				{

					List<String> allFx = deviceDao
							.getAllDeviceForExecutive(AuthUtils
									.getCurrentUserId());

					log.info("All fx for " + AuthUtils.getCurrentUserId()
							+ " are " + allFx);

					if (allFx != null && !allFx.isEmpty())
					{
						for (Iterator<String> iterator = allFx
								.iterator(); iterator.hasNext();)
						{
							String fxName = (String) iterator.next();

							userVo = userDao
									.getUsersByDeviceName(fxName, AuthUtils.SALES_EX_DEF);

							if (userVo != null)
							{
								TypeAheadVo aheadVo = new TypeAheadVo();
								aheadVo.setId(userVo.getId());
								aheadVo.setName(GenericUtil
										.completeUserName(userVo) + " ("
										+ userVo.getLoginId() + ")");

								userNameIdList.add(aheadVo);
							}
						}
					}
				}

				if (userVo != null)
				{
					TypeAheadVo aheadVo = new TypeAheadVo();
					aheadVo.setId(userVo.getId());
					aheadVo.setName(GenericUtil.completeUserName(userVo) + " ("
							+ userVo.getLoginId() + ")");
					userNameIdList.add(aheadVo);

				} else
				{
					log.info("Fx name not found for ticket id "
							+ gisNotFeasibleVO.getTicketId()
							+ " during deployment");

					log.info("Getting Sales Executive for "
							+ AuthUtils.getCurrentUserId());

					List<UserVo> userVos = userDao
							.getAssociatedUsersByUserId(userId, AuthUtils.SALES_EX_DEF);

					log.info("Sales user details for ticket in Fx name not found"
							+ gisNotFeasibleVO.getTicketId() + "  are "
							+ userVos.size());

					if (userVos != null)
					{
						for (Iterator<UserVo> iterator = userVos
								.iterator(); iterator.hasNext();)
						{
							UserVo vo = (UserVo) iterator.next();
							TypeAheadVo aheadVo = new TypeAheadVo();
							aheadVo.setId(vo.getId());
							aheadVo.setName(GenericUtil.completeUserName(vo)
									+ " (" + vo.getLoginId() + ")");
							userNameIdList.add(aheadVo);
						}
					}
				}
			}

			log.info("Returing User Vo for usr id " + userId
					+ " in deployment role are userNameIdList "
					+ userNameIdList);

			List<TypeAheadVo> userNameIds = new ArrayList<>(userNameIdList);

			return ResponseUtil.createSuccessResponse().setData(userNameIds);

		} else if (AuthUtils.getCurrentUserRole() == UserRole.SALES_EX)
		{
			GISPojo gisPojo = gisDAO
					.getGisInfoByTicketId(gisNotFeasibleVO.getTicketId());

			log.info("Gis response for ticket " + gisNotFeasibleVO.getTicketId()
					+ " in Sales role are " + gisPojo);

			if (gisPojo != null)
			{
				UserVo userVo = null;

				userVo = userDao.getUsersByDeviceName(gisPojo
						.getFxName(), AuthUtils.NE_NI_FIBER);

				log.info("Fiber ne for FX " + gisPojo.getFxName() + " userVo "
						+ userVo);

				if (userVo == null)
				{
					log.info("Fiber ne is not found for FX "
							+ gisPojo.getFxName()
							+ ". Trying to get the copper DEFAULT NE");

					userVo = userDao.getUsersByDeviceName(gisPojo
							.getFxName(), AuthUtils.NE_NI_DEF);

					if (userVo == null)
					{

						List<String> allFx = deviceDao
								.getAllDeviceForExecutive(AuthUtils
										.getCurrentUserId());

						log.info("All fx for " + AuthUtils.getCurrentUserId()
								+ " are " + allFx);

						if (allFx != null && !allFx.isEmpty())
						{
							for (Iterator<String> iterator = allFx
									.iterator(); iterator.hasNext();)
							{
								String fxName = (String) iterator.next();

								userVo = userDao
										.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);

								if (userVo != null)
								{
									TypeAheadVo aheadVo = new TypeAheadVo();
									aheadVo.setId(userVo.getId());
									aheadVo.setName(GenericUtil
											.completeUserName(userVo) + " ("
											+ userVo.getLoginId() + ")");

									userNameIdList.add(aheadVo);

								}

							}
						}
					}
				}

				log.info("Deployment user details for ticket "
						+ gisNotFeasibleVO.getTicketId() + "  are " + userVo);

				if (userVo != null)
				{
					TypeAheadVo aheadVo = new TypeAheadVo();
					aheadVo.setId(userVo.getId());
					aheadVo.setName(GenericUtil.completeUserName(userVo) + " ("
							+ userVo.getLoginId() + ")");
					userNameIdList.add(aheadVo);

				} else
				{
					log.info("Fx name not found for ticket id "
							+ gisNotFeasibleVO.getTicketId() + " during sales");

					log.info("Getting deployment executive for "
							+ AuthUtils.getCurrentUserId());

					List<UserVo> userVos = null;

					log.info("Getting all Fiber ne for FX "
							+ gisPojo.getFxName() + " and SE " + userId);

					userVos = userDao
							.getAssociatedUsersByUserId(userId, Collections
									.singletonList(UserRole.ROLE_NI_FIBER));

					if (userVos == null || userVos.size() <= 0)
					{
						log.info("Getting all Copper ne for FX "
								+ gisPojo.getFxName() + " and SE " + userId);

						userVos = userDao
								.getAssociatedUsersByUserId(userId, Collections
										.singletonList(UserRole.ROLE_NI_COPPER));
					}
					if (userVos == null || userVos.size() <= 0)
					{
						log.info("Getting all ne for FX " + gisPojo.getFxName()
								+ " and SE " + userId);

						userVos = userDao
								.getAssociatedUsersByUserId(userId, Collections
										.singletonList(UserRole.ROLE_NI));
					}

					log.info("Deployemnt user details for ticket in Fx name not found, so all the ne with Sales "
							+ userId + "  are " + userVos.size());

					if (userVos != null)
					{
						for (Iterator<UserVo> iterator = userVos
								.iterator(); iterator.hasNext();)
						{
							UserVo vo = (UserVo) iterator.next();
							TypeAheadVo aheadVo = new TypeAheadVo();
							aheadVo.setId(vo.getId());
							aheadVo.setName(GenericUtil.completeUserName(vo)
									+ " (" + vo.getLoginId() + ")");
							userNameIdList.add(aheadVo);
						}

					}
				}
			}

			UpdateProspectVO updateProspectVO = new UpdateProspectVO();

			if (gisNotFeasibleVO.getProspectType() != null)
			{
				updateProspectVO
						.setProspectType(gisNotFeasibleVO.getProspectType());

				updateProspectVO
						.setTicketId(gisNotFeasibleVO.getTicketId().toString());

				updateProspectVO.setLattitude(gisNotFeasibleVO.getLattitude());
				updateProspectVO.setLongitude(gisNotFeasibleVO.getLongitude());
				updateProspectVO
						.setPermissionFeasibilityStatus(BooleanValue.TRUE);

				// salesDAO.updatePropspectType(updateProspectVO);

			}

			log.info("Returing User Vo for usr id " + userId
					+ " in sales role are userNameIdList " + userNameIdList);

			List<TypeAheadVo> userNameIds = new ArrayList<>(userNameIdList);

			return ResponseUtil.createSuccessResponse().setData(userNameIds);

		} else
		{
			return ResponseUtil.createBadCredentialsExceptionResponse();
		}
	}

	@Override
	public APIResponse reAssignedDeviceToUser(long userId, String fxName)
	{
		String result = userDao
				.reAssignedDeviceToUser(userId, fxName.toUpperCase()).trim();

		if (result.equalsIgnoreCase("Saved successfully"))
		{
			return ResponseUtil.createSaveSuccessResponse();
		} else if (result.equalsIgnoreCase("Save failed"))
		{
			return ResponseUtil.createSaveFailedResponse();

		} else if (result.equalsIgnoreCase("Database Exception"))
		{
			return ResponseUtil.createDBExceptionResponse();

		} else if (result.equalsIgnoreCase("Failure"))
		{
			return ResponseUtil.createFailureResponse();

		} else
			return ResponseUtil.createDeviceAssociatedWithOtherUserResponse();

	//	return null;

	}

	public APIResponse resetCachedUsers()
	{
		log.info("reset Cached Users Success:::");
		// userDao.resetCachedUsers();
		userDao.modifiedResetUser();
		return ResponseUtil.createSuccessResponse();
	}
	
	@Override
	public APIResponse addDefaultSalesUserForAreas(List<DefaultUserAreaMapping> mappings)
	{
		userDao.addDefaultSalesUserForAreas(mappings);
		return ResponseUtil.createSaveSuccessResponse();
	}
	
	
	public APIResponse mapAllAreaForUser(List<TypeAheadVo> areaIds,
			Long userId)
	{
		log.debug("Mapping Area with user "+userId +" >> "+areaIds);
		if(userId == null || areaIds == null || areaIds.isEmpty())
			return ResponseUtil.createNullParameterResponse();
	
	 
		userDao.mapAllAreaForUser(areaIds, userId);
		return ResponseUtil.createSaveSuccessResponse();
	}

	public APIResponse mapDefaultAreaForUser(List<TypeAheadVo> areaIds,
			Long userId)
	{
		log.debug("Mapping Area with user "+userId +" >> "+areaIds);
		/*if(userId == null || areaIds == null || areaIds.isEmpty())
			return ResponseUtil.createNullParameterResponse();*/
		if(userId == null)
			return ResponseUtil.createNullParameterResponse();
	 
		userDao.mapDefaultAreaForUser(areaIds, userId);
		return ResponseUtil.createSaveSuccessResponse();
	}
	public APIResponse getAllAreasForUser(Long userId)
	{
		return ResponseUtil.createSuccessResponse().setData(userDao.getAllAreasForUser(userId));
	}
	public APIResponse getDefaultAreasForUser(Long userId)
	{

		Map<Long, String>  devices = userDao.getDefaultAreasForUser(userId);
		return ResponseUtil.createSuccessResponse().setData(CommonUtil.xformToTypeAheadFormat(devices));
	}
	
	@Override
	public APIResponse restUserById(ResetUserVo resetUserVo)
	{
		userDao.resetCachedUsersByUserId(resetUserVo.getUserId());
		
		return ResponseUtil.createSuccessResponse().setData("Success");
	}

	@Override
	public APIResponse restPassword(ResetUserVo resetUserVo)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(userDao.restUserById(resetUserVo));
	}

	@Override
	public APIResponse addToCache(UserVo userVo)
	{
		int result = userDao.addToCache(userVo);
		if (result == 1)
			return ResponseUtil.createSuccessResponse()
					.setData("User cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User cache reset failed!");
	}

	private APIResponse addToCacheServiceCall(UserVo userVo, String serverIp)
	{
		APIResponse apiResponse = null;

		log.info("Merge User cache original URI ############### " + resetUserUrl
				+ "userVo  " + userVo);

		String resetUserUrlWithIp = resetUserUrl.replace("localhost", serverIp);

		Set<TypeAheadVo> groupAheadVos = userVo.getUserGroups();

		Set<TypeAheadVo> roleAheadVos = new HashSet<>();
		if (groupAheadVos != null && !groupAheadVos.isEmpty())
		{
			for (Iterator<TypeAheadVo> iterator2 = groupAheadVos
					.iterator(); iterator2.hasNext();)
			{
				TypeAheadVo typeAheadVo = (TypeAheadVo) iterator2.next();

				roleAheadVos.addAll(userGroupDAO
						.getuseGroupAndRolesTAByGroupId(typeAheadVo.getId()));

			}

			userVo.setUserRoles(roleAheadVos);
		}

		log.info("Merge User cache ############### " + resetUserUrlWithIp
				+ " userVo " + userVo);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			ObjectMapper mapper = new ObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetUserUrlWithIp);

			String mainQueueString = mapper.writeValueAsString(userVo);

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());
			apiResponse = ResponseUtil.createSuccessResponse()
					.setData(stringResponse);
			log.debug("Merge User cache response " + stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			log.error("Error while Merge User cache " + e.getMessage());

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				if(httpclient != null)
				httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse merging User cache. "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

		return apiResponse;

	}

	public String getServers()
	{
		return servers;
	}

	public void setServers(String servers)
	{
		this.servers = servers;
	}

	public String getResetUserUrl()
	{
		return resetUserUrl;
	}

	public void setResetUserUrl(String resetUserUrl)
	{
		this.resetUserUrl = resetUserUrl;
	}
	
	
	private APIResponse updateCacheServiceCall(UserVo userVo, String serverIp)
	{
		APIResponse apiResponse = null;

		log.info("Merge User cache original URI ############### " + resetUserUrl
				+ "userVo  " + userVo);

		String resetUserUrlWithIp = resetUserUrl.replace("localhost", serverIp);

		Set<TypeAheadVo> groupAheadVos = userVo.getUserGroups();

		Set<TypeAheadVo> roleAheadVos = new HashSet<>();
		if (groupAheadVos != null && !groupAheadVos.isEmpty())
		{
			for (Iterator<TypeAheadVo> iterator2 = groupAheadVos
					.iterator(); iterator2.hasNext();)
			{
				TypeAheadVo typeAheadVo = (TypeAheadVo) iterator2.next();

				roleAheadVos.addAll(userGroupDAO
						.getuseGroupAndRolesTAByGroupId(typeAheadVo.getId()));

			}

			userVo.setUserRoles(roleAheadVos);
		}

		log.info("Merge User cache ############### " + resetUserUrlWithIp
				+ " userVo " + userVo);

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			ObjectMapper mapper = new ObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetUserUrlWithIp);

			String mainQueueString = mapper.writeValueAsString(userVo);

			StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());
			apiResponse = ResponseUtil.createSuccessResponse()
					.setData(stringResponse);
			log.debug("Merge User cache response " + stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			log.error("Error while Merge User cache " + e.getMessage());

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				if(httpclient != null)
				httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse merging User cache. "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

		return apiResponse;

	}
	
	@Override
	public Workbook getAreaMasterMapping(Workbook workbook) throws Exception {
		List<String> result = new ArrayList<String>();
		
		UserVo login_user = new UserVo();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login_id = authentication.getName();
		login_user = userDao.getUserByLoginId(login_id);
		
		try {
			result=userDao.getAreaMasterMappingDetails(workbook,login_user);
			
			if (result != null) {
				int i=0;
			Sheet sheet = workbook.createSheet("Area_Master_Data");
			Row row = sheet.createRow(i);
			row.createCell(0);
			row.getCell(0).setCellValue("AreaName");
			
			for (String obj : result)
			{
				i++;
				row = sheet.createRow(i);
				row.createCell(0);
				row.getCell(0).setCellValue(obj);
			}
			}
			
			Sheet sheetnew = workbook.createSheet("UploadFormat");
			Row rownew = sheetnew.createRow(0);
			rownew.createCell(0);
			rownew.getCell(0).setCellValue("LoginId");
			rownew.createCell(1);
			rownew.getCell(1).setCellValue("AreaName");
			
			return workbook;
			
		}catch(Exception e) {
			return workbook;
		}
		
		
	}

	/*
	 * @author manjuprasad.nidlady
	 * 
	 * @param workbook
	 * @return workbook
	 * 
	 */
		public Workbook uploadBulkDefaultAreaMapping(Workbook workbook) throws Exception {
			APIResponse response = new APIResponse();
			Iterator<Row> rowIterator = workbook.getSheetAt(0).rowIterator();
			int i = 0;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (i == 0) {
					row.createCell(2);
					row.getCell(2).setCellValue("Bulk Upload Status");

				}else {
					UserVo userId = new UserVo();
					Long idarea;
					DataFormatter formatter = new DataFormatter();
					List<TypeAheadVo> areaIds = new ArrayList<TypeAheadVo>();
					TypeAheadVo areaid= new TypeAheadVo();
					
					userId = userDao.getUserByLoginId(formatter.formatCellValue(row.getCell(0)));
					areaid.setName(formatter.formatCellValue(row.getCell(1)));
					idarea = areDao.getAreaIdByName(formatter.formatCellValue(row.getCell(1)));
					if(userId != null && areaid.getName()!=null && idarea==null) {
						idarea = areDao.getAreaIdForBulk(formatter.formatCellValue(row.getCell(1)), userId.getId());
						areaid.setId(idarea);
						areaIds.add(areaid);
					}else {
						areaid.setId(idarea);
						areaIds.add(areaid);
					}
					
				
					
					try {
						if(userId == null) {
							response.setData("User Not Found");
						} else if (areaIds == null) {
							response.setData("Area Not Found");
						}else {
							response = mapDefaultAreaForUserBulk(areaIds,userId.getId()) ;
						}
					
						
					} catch (Exception e) {
						e.printStackTrace();
						
					}
					finally {
					row.createCell(2);
					row.getCell(2).setCellValue(response.getData().toString());}

				}
				i++;				
		}
			return workbook;
	}

		@Override
		public APIResponse mapDefaultAreaForUserList(List<TypeAheadVo> areaIds, Long userId) {
			
			return  mapDefaultAreaForUser(areaIds,userId); 
		}
	
	/**
	 * @author manjuprasad.nidlady
	 * 
	 * @param Row,UserBulkUploadVO
	 * @return response string
	 */
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public String uploadDataToDatabase(Row row, UserBulkUploadVO userbulk) throws Exception {
		
		String msg = "";
		try{
			UserVo user = new UserVo();
			UserVo reportToUser = new UserVo();
			Long idarea = 0l;
			Long idcity = 0l;
			Long idbranch = 0l;
			Long idtablet = 0l;
			Long idemp = 0l;
			Long idsubarea = 0l;
			Long idskills = 0l;
			Long idusergroup = 0l;
			Long iduserrole = 0l;
			
			
			TypeAheadVo areavo = new TypeAheadVo();
			TypeAheadVo subAreasvo =new TypeAheadVo();
			TypeAheadVo skillsvo = new TypeAheadVo();
			TypeAheadVo userGroupsvo = new TypeAheadVo();
			TypeAheadVo userRolesvo = new TypeAheadVo();
			TypeAheadVo tablet = new TypeAheadVo();
			TypeAheadVo branch = new TypeAheadVo();
			TypeAheadVo employer = new TypeAheadVo();
			TypeAheadVo city = new TypeAheadVo();
			TypeAheadVo reportTo = new TypeAheadVo();
			Set<TypeAheadVo> area = new HashSet<TypeAheadVo>();
			Set<TypeAheadVo> subAreas = new HashSet<TypeAheadVo>();
			Set<TypeAheadVo> skills = new HashSet<TypeAheadVo>();
			Set<TypeAheadVo> userGroups = new HashSet<TypeAheadVo>();
			Set<TypeAheadVo> userRoles = new HashSet<TypeAheadVo>();
		
			
			user.setId(userbulk.getId());
			user.setLoginId(userbulk.getLoginId());
			user.setEmpId(Long.parseLong(userbulk.getEmpId()));
			user.setFirstName(userbulk.getFirstName());
			user.setLastName(userbulk.getLastName());
			user.setEmailId(userbulk.getEmailId());
			user.setMobileNo(userbulk.getMobileNo());
			user.setPassword(userbulk.getPassword());
			
			
			userbulk.getSubAreas();
			userbulk.getSkills();
			
		 try {
			 if(userbulk.getReportTo()!=null) {
				 reportToUser = userDao.getUserByLoginId(userbulk.getReportTo());
				 if(reportToUser!=null) {
					 reportTo.setId(reportToUser.getId());
					 reportTo.setName(reportToUser.getFirstName());
					 user.setReportTo(reportTo); 
				 }else {
					 return "Please enter a valid report to id";
				 }
				
			 }
			 
		 }catch(Exception e) {
			 return "Please enter a valid report to id";
		 }
			
			
			
			try {
				if (userbulk.getArea().get(0)!=null) {
					idarea = areDao.getAreaIdByName(userbulk.getArea().get(0));
					if(idarea!=null) {
						areavo.setId(idarea);
						areavo.setName(userbulk.getArea().get(0));
						area.add(areavo);
						user.setArea(area);
					}

				}
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			try {
				if (userbulk.getUserGroups().get(0)!=null) {
					idusergroup= userGroupDAO.getUserGroupIdByGroupName(userbulk.getUserGroups().get(0));
					if(idusergroup!=0) {
						userGroupsvo.setId(idusergroup);
						userGroupsvo.setName(userbulk.getUserGroups().get(0));
						userGroups.add(userGroupsvo);
						user.setUserGroups(userGroups);
					}else {
						return "Please enter valid UserGroup";
					}

				}
			}catch(Exception e) {
				return "Please enter valid UserGroup";
			}


			try {
				if(userbulk.getCity()!=null) {
					idcity = cityDao.getCityIdByName(userbulk.getCity());
					if(idcity!=null) {
						city.setId(idcity);
						city.setName(userbulk.getCity());
						user.setCity(city);
					}else {
						return "Please enter Valid City";
					}

				}
			}catch(Exception e) {
				return "Please enter Valid City";
			}
			
			
//since the map has all upper case values
			
			try {
				if (userbulk.getBranch()!=null) {
					idbranch = branchDao.getBranchIdByName(userbulk.getBranch().toUpperCase());
					if(idbranch!=null) {
						branch.setId(idbranch);
						branch.setName(userbulk.getBranch());
						user.setBranch(branch);
					}

				}
			}catch(Exception e) {
				return "Please enter Valid Branch";
			}
			
			try {
				if(userbulk.getEmployer()!=null) {
					idemp = vendorDAO.getVendorIdByVendorName(userbulk.getEmployer());
					if(idemp!=0) {
						employer.setId(idemp);
						employer.setName(userbulk.getEmployer());
						user.setEmployer(employer);	
					}

				}
			}catch(Exception e) {
				return "Please enter Valid Employer";
			}


			try {
				if(userbulk.getTablet()!=null) {
					idtablet = tabletDao.getTabletIdByTabletMac(userbulk.getTablet());
					if(idtablet!=0) {
						tablet.setId(idtablet);
						tablet.setName(userbulk.getTablet());
					}
					}

			}catch(Exception e) {
				log.info(e.toString());
			}
			
			msg = addUser(user).getStatusMessage();
			return msg; 
			
		}  catch (Exception e) {
			msg = "Insertion_failed_please verify the data";
			return msg;
		
		} 		
		
	}

	@Override
	public APIResponse mapDefaultAreaForUserBulk(List<TypeAheadVo> areaIds,Long userId)
	{
		//modified by manjuprasad
		
		log.debug("Mapping Area with user "+userId +" >> "+areaIds);
		if(userId == null || areaIds == null || areaIds.isEmpty())
			return ResponseUtil.createNullParameterResponse();
	 
		String  result  = userDao.mapDefaultAreaForUserBulk(areaIds, userId);
		
		return ResponseUtil.defaultAreaMappingResponse(result);
		
	}
}
