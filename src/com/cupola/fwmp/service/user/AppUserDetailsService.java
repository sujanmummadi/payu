package com.cupola.fwmp.service.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.cupola.fwmp.auth.AppUser;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.Role;
import com.cupola.fwmp.persistance.entities.UserGroup;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;

/*@Service
 @Transactional*/
public class AppUserDetailsService implements UserDetailsService
{

	private UserDao userDao;

	/*
	 * @Autowired public MyUserDetailsService(UserDao userDao) { this.userDao =
	 * userDao; }
	 */
 /*	@Override
	public UserDetails loadUserByUsername(final String username)
			throws UsernameNotFoundException
	{
		AppUser userDetail = null;
		
		com.cupola.fwmp.persistance.entities.User user = userDao
				.findByUserName(username);
		if(user == null)
		{
			throw new UsernameNotFoundException("User "+username+" not found");
		}
		
		return buildUserAuthority(userDetail , user);

	} */

 	@Override
	public UserDetails loadUserByUsername(final String username)
			throws UsernameNotFoundException
	{
		AppUser userDetail = null;
		
		 UserVo user = userDao
				.findByUserNameNew(username);
		if(user == null)
		{
			throw new UsernameNotFoundException("User "+username+" not found");
		}
		
		return buildUserAuthority(userDetail , user);

	}
	
	private  AppUser buildUserAuthority(
			AppUser userDetail, UserVo user)
	{
		Set<TypeAheadVo> userGroups = user.getUserGroups();

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		Set<String> roleList = new HashSet<String>();
		Map<Long, String> allUserGroup = new HashMap<Long, String>();

		// Build user's authorities
		for (TypeAheadVo userGroup : userGroups)
		{
			allUserGroup.put(userGroup.getId(), userGroup.getName());

		}
		for (TypeAheadVo role : user.getUserRoles())
		{
			roleList.add(role.getName());
			setAuths.add(new SimpleGrantedAuthority(role.getName()));
		}

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(setAuths);
		// if (user.getStatus() == 1)
		userDetail = new AppUser(user.getLoginId(), user.getPassword(), true, true, true, true, authorities);
		// else
		// userDetail = new AppUser(user.getLoginId(), user.getPassword(),
		// false, true, true, true, authorities);

		Map<Long, String> cities = new HashMap<Long, String>();
		Map<Long, String> branches = new HashMap<Long, String>();
		Map<Long, String> areas = new HashMap<Long, String>();
		if (user.getCity() != null)
		{

			cities.put(user.getCity().getId(), user.getCity().getName());

		}
		if (user.getBranch() != null)
		{

			branches.put(user.getBranch().getId(), user.getBranch().getName());

		}
		if (user.getArea() != null && !areas.isEmpty())
		{
			for (TypeAheadVo area : user.getArea())
			{
				areas.put(area.getId(), area.getName());
			}
		}

		userDetail.setAreas(areas);
		userDetail.setBranches(branches);
		userDetail.setCities(cities);
		userDetail.setUserGroup(allUserGroup);
		userDetail.setRoles(new ArrayList<String>(roleList));
		userDetail.setUserDisplayName(user.getFirstName());
		userDetail.setId(user.getId());
		return userDetail;
	} 
/* private  AppUser buildUserAuthority(
			AppUser userDetail, com.cupola.fwmp.persistance.entities.User user)
	{
		Set<UserGroup> userGroups = user.getUserGroups();
		
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		
		Set<String> roleList = new HashSet<String>();
		  Map<Long,String>  allUserGroup = new HashMap<Long, String>();
			
		// Build user's authorities
		for (UserGroup userGroup : userGroups) {
			allUserGroup.put(userGroup.getId(), userGroup.getUserGroupName());
			Set<Role> roles = userGroup.getRoles();
			
			for (Role role : roles) {
				roleList.add(role.getRoleCode());
				setAuths.add(new SimpleGrantedAuthority(role.getRoleCode()));
			}
		}
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(setAuths);
		if (user.getStatus() == 1)
			userDetail = new AppUser(user.getLoginId(), user.getPassword(), true, true, true, true, authorities);
		else
			userDetail = new AppUser(user.getLoginId(), user.getPassword(), false, true, true, true, authorities);
		
		  Map<Long,String> cities  = new HashMap<Long, String>();
		  Map<Long,String> branches  = new HashMap<Long, String>();
		  Map<Long,String> areas  = new HashMap<Long, String>();
			  if(user.getCities() != null)
		  {
			  for(Object obj :  user.getCities())
				{
				  City city  = (City)obj;
				  cities.put(city.getId(), city.getCityName());
				}
		  }
		  if(user.getBranches() != null)
		  {
			  for(Object obj :  user.getBranches())
				{
				  Branch branch  = (Branch)obj;
				  branches.put(branch.getId(), branch.getBranchName());
				}
		  }
		  if(user.getAreas() != null)
		  {
			  for(Object obj :  user.getAreas())
				{
				  Area area  = (Area)obj;
				  areas.put(area.getId(), area.getAreaName());
				}
		  }
		 
		  userDetail.setAreas(areas);
		  userDetail.setBranches(branches);
		  userDetail.setCities(cities);
		  userDetail.setUserGroup(allUserGroup);
		userDetail.setRoles(new ArrayList<String>(roleList));
		userDetail.setUserDisplayName(user.getFirstName());
		userDetail.setId(user.getId());
		return userDetail;
	} */

	public UserDao getUserDao()
	{
		return userDao;
	}

	public void setUserDao(UserDao userDao)
	{
		this.userDao = userDao;
	}

}