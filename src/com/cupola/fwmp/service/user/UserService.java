package com.cupola.fwmp.service.user;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.persistance.entities.DefaultUserAreaMapping;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.LoginPojo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserBulkUploadVO;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.gis.GISNotFeasibleVO;

public interface UserService
{

	APIResponse addUser(UserVo user);

	User findByUserName(String username);

	APIResponse loginForApp(LoginPojo loginPojo);

	APIResponse resetPassword(String oldPassword, String newPassword);

	APIResponse getAllUsers();

	APIResponse getUsers(UserFilter filter);

	User getUserByUserName(String name);

	// APIResponse addUserRole(String userLoginId, List<String> userRoles);

	// APIResponse getAllRoles();

	List<String> getAllElementMacs();

	// APIResponse addUserGroup(String userLoginId, List<String> userGroups);

	APIResponse updateUser(UserVo user);

	APIResponse deleteUser(UserVo user);

	APIResponse getUserName();

	// APIResponse addElement(List<Element> elements);

	List<TypeAheadVo> userSearchSuggestion(String userQuery);

	List<TypeAheadVo> tabSearchSuggestion(String userQuery);

	List<TypeAheadVo> elementSearchSuggestion(String elementQuery);

	// APIResponse getUserListByPageIndex(int pageIndex, int itemPrePage);

	List<TypeAheadVo> getAreasByBranchName(String branchName);

	List<TypeAheadVo> citySearchSuggestion(String cityQuery);

	List<TypeAheadVo> getBranchesByCityName(String cityName);

	List<TypeAheadVo> getVendorsByBranchName(TypeAheadVo aheadVo);

	// List<String> getVendorTypesByVendorName(String vendorName);

	List<TypeAheadVo> getSkillsByVendorName(TypeAheadVo typeAheadVo);

	List<TypeAheadVo> getSubAreasByAreaName(String areaName);

	String setCurrentUser(LoginPojo loginPojo, Long userId);

	APIResponse checkCurrentUser(LoginPojo loginPojo);

	List<String> getUserRoles();



	List<TypeAheadVo> getDevicesByBranchId(String branchId);

	APIResponse getUserByDeviceMac(String mac, String role);

	APIResponse getUsersOfAssosiatedDevice(GISNotFeasibleVO gisNotFeasibleVO);

	APIResponse reAssignedDeviceToUser(long userId, String macAddress);

	APIResponse createNewPassword(Long userId, String newPassword);

	List<TypeAheadVo> getAllUserNames(Long userType);

	List<TypeAheadVo> getAssociatedUsers(Long areaId, Boolean includeReportTo);
	
	public APIResponse resetCachedUsers();

	List<TypeAheadVo> getAssociatedUsersByGroup(Long areaId, Long branchId,long userGroupId);

	List<TypeAheadVo> getAssociatedUsersForContext(Long areaId,
			Boolean includeReportTo, Integer pageContext);

	APIResponse addDefaultSalesUserForAreas(
			List<DefaultUserAreaMapping> mappings);

	public APIResponse mapAllAreaForUser(List<TypeAheadVo> areaIds, Long userId);

	public APIResponse mapDefaultAreaForUser(List<TypeAheadVo> areaIds, Long userId);

	public APIResponse getAllAreasForUser(Long userId);

	public APIResponse getDefaultAreasForUser(Long userId);

	APIResponse restUserById(ResetUserVo resetUserVo);

	APIResponse restPassword(ResetUserVo resetUserVo);

	/**@author aditya 3:26:48 PM Apr 6, 2017
	 * APIResponse
	 * @param userVo
	 * @return
	 */
	APIResponse addToCache(UserVo userVo);
	
	/*
	 * @author manjuprasad.nidlady
	 */
	
	public Workbook uploadBulkDefaultAreaMapping(Workbook create) throws Exception;
	public Workbook getAreaMasterMapping(Workbook create) throws Exception;
	public APIResponse mapDefaultAreaForUserList(List<TypeAheadVo> areaIds, Long userId);
	public String uploadDataToDatabase(Row row,UserBulkUploadVO userbulk) throws Exception;
	public APIResponse mapDefaultAreaForUserBulk(List<TypeAheadVo> areaIds,Long userId);
	
}
