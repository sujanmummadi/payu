package com.cupola.fwmp.service.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.UserPreferenceType;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.NotificationOptions;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserPreferenceServiceImpl implements UserPreferenceService{

	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	DefinitionCoreService  definitionService;
	
	private static Map<String,UserPreference> preferenceCache = new HashMap<String, UserPreference>();
	private static Logger logger = Logger.getLogger(UserPreferenceService.class
			.getName());

	public UserPreference createUserPreference(Long userGuid, UserPreferenceVO userPref) {
		
		if(userPref != null)
		{
			
			userPref.setUserId(userGuid);
			userPref.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			mongoOperations.save(userPref);
			
			UserPreference vo = new UserPreferenceImpl();
			BeanUtils.copyProperties(userPref,vo);
			preferenceCache.put(userGuid+userPref.getPrefName(), vo);
			return userPref;
		}

		return null;
	}

	
	public List<UserPreference> getUserPreferencesByUserId(Long userId) {
		
		List<UserPreferenceVO> list = mongoOperations.find(new Query().addCriteria(Criteria.where("userId")
				.is(userId)), UserPreferenceVO.class);
		return getUserPreferencesFromDbvo(list);
	}

	
	@Override
	public APIResponse getNotificationOption()
	{
		NotificationOptions options = new NotificationOptions();

		/*for (ActivityVo vo : activityCoreService.getAllActivity())
		{
			options.getActivities().add(new TypeAheadVo(vo.getId(), vo.getActivityName()));
		}*/
		/*for (TypeAheadVo vo : workOrderTypeService.getAllWorkOrderTypes())
		{
			options.getTypes().add(new TypeAheadVo(vo.getId(), vo.getName()));
		}*/
		
		if (AuthUtils.isNIUser())
		{
			options.getStages()
					.add(new TypeAheadVo(WorkStageType.FIBER, WorkStageName.FIBER));
			options.getStages()
					.add(new TypeAheadVo(WorkStageType.COPPER, WorkStageName.COPPER));
		}
		
		Map<Long , String> activities = new HashMap<Long , String>();
		if(AuthUtils.isNIUser())
		{	
			activities = WorkOrderDefinitionCache.getActivitiesForNI();
		
			//MaterialConsumption PhysicalFeasibility Removed
			activities.remove(8l);
			activities.remove(9l);
		}
		else if(AuthUtils.isSalesUser())
			{
			
			List<TypeAheadVo> statusList = new ArrayList<TypeAheadVo>();
			TypeAheadVo vo = new TypeAheadVo(-1l, "All");
//			statusList.add(vo);
			Set<Integer> codeDefinition = definitionService.getStatusFilterForCurrentUser(true,null);
			for (Integer id : codeDefinition)
			{
				vo = new TypeAheadVo(Long.valueOf(id), TicketStatus.statusCodeValue
						.get(id));
				statusList.add(vo);
			}
			options.getStatus().addAll(statusList);
			
			activities = WorkOrderDefinitionCache.getActivitiesForSales();
			}
		
		options.getActivities().addAll(CommonUtil.xformToTypeAheadFormat(activities));
		
		
 		UserPreference uf = getUserPreferencesByPreferenceName(AuthUtils.getCurrentUserId(), UserPreferenceType.NOTIFICATION_BAR);
 		ObjectMapper mapper = new ObjectMapper();
 		if(uf != null && uf.getPrefStringValue() != null)
		{
			try
			{
				NotificationOptions savedOptions = mapper.readValue(uf
						.getPrefStringValue(), NotificationOptions.class);
				
				options.getStages().removeAll(savedOptions.getStages());
				options.getStages().addAll(savedOptions.getStages());
				
				options.getTypes().removeAll(savedOptions.getTypes());
				options.getTypes().addAll(savedOptions.getTypes());
				
				options.getStatus().removeAll(savedOptions.getStatus());
				options.getStatus().addAll(savedOptions.getStatus());
				
				
				options.getActivities().removeAll(savedOptions.getActivities());
				options.getActivities().addAll(savedOptions.getActivities());
				
			} catch (JsonParseException e)
			{
				logger.error("Error While parsing json :"+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e)
			{
				logger.error("Error While mapping json :"+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				logger.error("Error While getting notification option  :"+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		return ResponseUtil.createSuccessResponse().setData(options);
	}
	/**
	 * @param options
	 * @return
	 */
	@Override
	public  APIResponse createOrUpdateNotificationPreference(
			NotificationOptions options)
	{
		long userLoginId = AuthUtils.getCurrentUserId();
 		UserPreferenceVO userPrefVO = new UserPreferenceVO();
 		ObjectMapper mapper = new ObjectMapper();
 		UserPreference pref = null;
 		userPrefVO.setUserId(userLoginId);
 		userPrefVO.setPrefName(UserPreferenceType.NOTIFICATION_BAR);
 		try
		{
 			String stringValue =  mapper.writeValueAsString(options);
 			stringValue = stringValue.replaceAll("\"status\":0", "\"status\":1");
			userPrefVO.setPrefStringValue(stringValue);
			
			
		} catch (JsonProcessingException e)
		{
			logger.error("Error While processing json data :"+e.getMessage());
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		UserPreference uf = getUserPreferencesByPreferenceName(AuthUtils.getCurrentUserId(), UserPreferenceType.NOTIFICATION_BAR);
	
 		if(uf != null && uf.getPrefStringValue() != null)
 		{
 			logger.info("Updating User preference for user "+userLoginId);
 			pref = updateUserPreference(userLoginId,userPrefVO);
 			return ResponseUtil.createSuccessResponse().setData(pref.getPrefStringValue());
 		}
 		else
 		{
 			logger.info("Creating User preference for user "+userLoginId);
 			pref = createUserPreference(userLoginId,userPrefVO);
 			return ResponseUtil.createSuccessResponse().setData(pref.getPrefStringValue());
 		}
	}
	
	@Override
	public  UserPreference  getUserPreferencesByPreferenceName(Long userId,String preferenceName) {
		
		UserPreference pref = null;
		if(preferenceCache.containsKey(userId+preferenceName))
		{
			return	preferenceCache.get(userId+preferenceName);
		}
		UserPreferenceVO vo = mongoOperations.findOne(new Query().addCriteria(Criteria.where("userId")
				.is(userId)).addCriteria(Criteria.where("prefName")
				.is(preferenceName)), UserPreferenceVO.class);
		pref = new  UserPreferenceImpl();
		if(vo != null)
			{
			BeanUtils.copyProperties(vo,pref);
			preferenceCache.put(userId+preferenceName, pref);
			}
		return pref;
	}
	
	public List<UserPreference> getUserPreferencesByUserLoginId(String userLoginId) {
		
		List<UserPreferenceVO> list = mongoOperations.find(new Query().addCriteria(Criteria.where("userId")
				.is(userLoginId)), UserPreferenceVO.class);
		return getUserPreferencesFromDbvo(list);
	}

	
	public UserPreference getUserPreference(Long userId, String prefName) {
		
		return null;
	}

	
	public UserPreference updateUserPreference(Long userId,UserPreferenceVO userPref) {
		
		Update update = new Update();
		update.set("prefStringValue", userPref.getPrefStringValue());
		UserPreferenceVO vo =  mongoOperations.findAndModify (new Query().addCriteria(Criteria.where("userId")
				.is(userId)),update, UserPreferenceVO.class);
		UserPreference pref = new  UserPreferenceImpl();
		if(vo != null)
		{
			BeanUtils.copyProperties(vo,pref);
			preferenceCache.put(userId+userPref.getPrefName(), pref);
		}
		
		return pref;
	}

	public void deleteUserPrefereceByGuid(Long userId) {
		
		mongoOperations.findAllAndRemove(new Query().addCriteria(Criteria.where("userId")
				.is(userId)), UserPreferenceVO.class);
	}
	
	private List<UserPreference> getUserPreferencesFromDbvo(List<UserPreferenceVO> list)
	{
		if(list == null)
			return null;
		
		List<UserPreference> userPrefer = new ArrayList<UserPreference>();
		UserPreference ufimpl = null;
		UserPreferenceVO userPrefVo = null;
		
		for(Iterator<UserPreferenceVO> itr = list.iterator();itr.hasNext();)
		{
			userPrefVo = itr.next();
			ufimpl = new UserPreferenceImpl();
			BeanUtils.copyProperties(userPrefVo,ufimpl);
			userPrefer.add(ufimpl);
		}
		return userPrefer;
	}
}
