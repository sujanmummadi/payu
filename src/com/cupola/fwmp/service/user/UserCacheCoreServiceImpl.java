package com.cupola.fwmp.service.user;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.user.UserCache;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * @author aditya
 * @created 5:40:09 PM Apr 6, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */
public class UserCacheCoreServiceImpl implements UserCacheCoreService
{
	@Autowired
	UserCache userCache;

	@Override
	public APIResponse userAreaCache()
	{
		int result = userCache.userAreaCache(null);

		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User Area Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User Area Cache reset failed!");

	}

	@Override
	public APIResponse userBranchCache()
	{
		int result = userCache.userBranchCache(null);

		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User Branch Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User Branch Cache reset failed!");
	}

	@Override
	public APIResponse userCityCache()
	{
		int result = userCache.userCityCache(null);

		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User City Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User City Cache reset failed!");

	}

	@Override
	public APIResponse userDeviceCache()
	{
		int result = userCache.userDeviceCache(null);
		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User Device Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User Device Cache reset failed!");

	}

	@Override
	public APIResponse tabletCache()
	{
		int result = userCache.tabletCache();
		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("Tablet Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("Tablet Cache reset failed!");

	}

	@Override
	public APIResponse userTabletCache()
	{
		int result = userCache.userTabletCache(null);
		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User Tablet Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User Tablet Cache reset failed!");

	}

	@Override
	public APIResponse userSkillCache()
	{
		int result = userCache.userSkillCache(null);
		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User Skill Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User Skill Cache reset failed!");

	}

	@Override
	public APIResponse userEmployeerCache()
	{
		int result = userCache.userEmployeerCache(null);
		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User Employeer Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User Employeer Cache reset failed!");

	}

	@Override
	public APIResponse deviceCache()
	{
		int result = userCache.deviceCache();
		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("Device Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("Device Cache reset failed!");

	}

	@Override
	public APIResponse userUserGroupCache()
	{
		int result = userCache.userUserGroupCache(null);
		if (result > 0)
			return ResponseUtil.createSuccessResponse()
					.setData("User User Group Cache reset done!");
		else
			return ResponseUtil.createSuccessResponse()
					.setData("User User Group Cache reset failed!");

	}

}
