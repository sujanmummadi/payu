package com.cupola.fwmp.service.user;

public class UserPreferenceImpl implements UserPreference
{
	private Long userId;
	private String userLoginId;
	private String prefName;
	private Long userGroupId;	
	private String prefStringValue;
	
	public UserPreferenceImpl(){}
	
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getUserLoginId() {
		return userLoginId;
	}
	
	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}
	public String getPrefName() {
		return prefName;
	}
	
	public void setPrefName(String prefName) {
		this.prefName = prefName;
	}
	
	public Long getUserGroupId() {
		return userGroupId;
	}
	
	public void setUserGroupId(Long userGroupId) {
		this.userGroupId = userGroupId;
	}
	
	public String getPrefStringValue() {
		return prefStringValue;
	}
	
	public void setPrefStringValue(String prefStringValue) {
		this.prefStringValue = prefStringValue;
	}

}
