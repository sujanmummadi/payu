package com.cupola.fwmp.service.user;

import com.cupola.fwmp.response.APIResponse;

/**
 * @author aditya
 * @created 5:39:47 PM Apr 6, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */
public interface UserCacheCoreService
{

	APIResponse userAreaCache();

	APIResponse userBranchCache();

	APIResponse userCityCache();

	APIResponse userDeviceCache();

	APIResponse tabletCache();

	APIResponse userTabletCache();

	APIResponse userSkillCache();

	APIResponse userEmployeerCache();

	APIResponse deviceCache();

	APIResponse userUserGroupCache();

}
