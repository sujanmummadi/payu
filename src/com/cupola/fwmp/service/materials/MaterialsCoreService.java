package com.cupola.fwmp.service.materials;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Materials;
import com.cupola.fwmp.persistance.entities.TicketMaterialMapping;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.MaterialRequestVO;
import com.cupola.fwmp.vo.MultipleMaterialConsumptionVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;

public interface MaterialsCoreService
{

	public APIResponse addMaterials(Materials materials);

	public APIResponse getMaterialsById(Long id);

	public APIResponse getAllMaterials();

	public APIResponse deleteMaterials(Long id);

	public APIResponse updateMaterials(Materials materials);

	public APIResponse updateMultipleMaterialConsumption(
			MultipleMaterialConsumptionVO multipleMaterialConsumptionList);

	public APIResponse UpdateMaterialConsumption(
			TicketAcitivityDetailsVO ticketAcitivityDetailsVO);

	public APIResponse raiseMaterialRequest(
			List<MaterialRequestVO> materialRequestList);
	


}