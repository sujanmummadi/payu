package com.cupola.fwmp.service.materials;

/**
 * 
 * @author Mahesh Chouhan
 * 
 * */

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.cupola.fwmp.dao.materials.MaterialsDAO;
import com.cupola.fwmp.vo.MaterialsVO;

public class MaterialCache {
	
	static final Logger logger = Logger.getLogger(MaterialCache.class);

	private static Map<Long, String> materialCache = new HashMap<Long, String>();

	private static MaterialsDAO materialsDAO;

	public static void setMaterialsDAO(MaterialsDAO materialsDAO) {
	
		MaterialCache.materialsDAO = materialsDAO;
	
	}

	public void init() {

//		loadData(); uncomment later
	
	}

	/**
	 * 
	 */
	public void loadData()
	{
		logger.debug("Inside init method of MaterialCache.Class");
		
		List<MaterialsVO> materialsList = materialsDAO.getAllMaterials();
		
		if (materialsList != null) {
		
			logger.debug("Materials in db " + materialsList);
			
			Iterator it = materialsList.iterator();
			
			while (it.hasNext()) {
			
				MaterialsVO material = new MaterialsVO();
				
				material = (MaterialsVO) it.next();
				
				materialCache.put(material.getId(), material.getName());
			
			}
		
		}
	}

	public static void resetCache() {
	
		logger.debug("Clearing material cache");
		
		materialCache.clear();
		
		logger.debug("Material cache cleared. material cache -->" + materialCache);
		
			new MaterialCache().init();
		
		
		logger.debug("reset of MaterialCache done");
	
	}

	public static  Map<Long, String> getAllMaterials() {
	
		logger.debug("Inside getAllMaterials method of MaterialCache.Class");
		
		return materialCache;
	}
}