package com.cupola.fwmp.service.materials;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.dao.materials.MaterialsDAO;
import com.cupola.fwmp.dao.materials.MaterialsDAOImpl;
import com.cupola.fwmp.persistance.entities.Materials;
import com.cupola.fwmp.persistance.entities.TicketMaterialMapping;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreServiceImpl;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.MaterialConstants;
import com.cupola.fwmp.util.MaterialName;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.UomName;
import com.cupola.fwmp.vo.MaterialRequestVO;
import com.cupola.fwmp.vo.MultipleMaterialConsumptionVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.TicketMaterialMappingVO;
import com.cupola.fwmp.vo.TypeAheadVo;

public class MaterialsCoreServiceImpl implements MaterialsCoreService
{

	final static Logger log = Logger.getLogger(MaterialsCoreServiceImpl.class);

	private MaterialsDAO materialsDAO;

	@Autowired
	TicketActivityLogCoreServiceImpl ticketActivityLogCoreServiceImpl;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;
	
	@Autowired
	DBUtil dbUtil;
	
	

	public MaterialsDAO getMaterialsDAO()
	{
		return materialsDAO;
	}

	public void setMaterialsDAO(MaterialsDAO materialsDAO)
	{
		this.materialsDAO = materialsDAO;
	}

	@Override
	public APIResponse addMaterials(Materials materials)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getMaterialsById(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateMultipleMaterialConsumption(
			MultipleMaterialConsumptionVO multipleMaterialConsumptionList)
	{

		log.info("Inside UpdateMultipleMaterialConsumption() of MaterialsCoreServiceImpl");

		boolean allMtlInfoAddCrct = true; // assume all material info added into
											// db
		try
		{

			if (multipleMaterialConsumptionList == null)
			{

				log.error("Input(MultipleMaterialConsumptionVO) is null");

				return ResponseUtil.createNullParameterResponse();

			}

			List<TicketMaterialMappingVO> ticketMaterialMappingVoList = multipleMaterialConsumptionList
					.getTicketMaterialMappingList();

			Long ticket = multipleMaterialConsumptionList.getTicketId();

			if (ticketMaterialMappingVoList == null)
			{

				log.error("Input(MultipleMaterialConsumptionVO.TicketMaterialMappingList) is null");

				return ResponseUtil.createNullParameterResponse();

			}

			for (int i = 0; i < ticketMaterialMappingVoList.size(); i++)
			{

				ticketMaterialMappingVoList.get(i).setTicketId(ticket);

				if (materialsDAO
						.updateMultipleMaterialConsumtion(ticketMaterialMappingVoList
								.get(i))
						.getStatusCode() != ResponseUtil
								.createSaveSuccessResponse().getStatusCode())
				{
					allMtlInfoAddCrct = false;

					log.debug(ticketMaterialMappingVoList.get(i)
							+ " failed to add");

				}

			}

		} catch (Exception e)
		{

			log.error("Adding material consumption info into db failed \n"
					+ e.getMessage());

		}

		if (allMtlInfoAddCrct)
		{

			return ResponseUtil.createSaveSuccessResponse();

		} else
		{

			return ResponseUtil.createSaveFailedResponse();

		}

	}

	@Override
	public APIResponse getAllMaterials()
	{

		List<TypeAheadVo> typeAheadData = null;

		log.info("Inside getAllMaterials method of MaterialCoreServiceImpl");

		try
		{

			typeAheadData = CommonUtil
					.xformToTypeAheadFormat(MaterialCache.getAllMaterials());

		} catch (Exception e)
		{

			log.error("Error while getting all materials list from cache. error msg is "
					+ e.getMessage());

			return ResponseUtil.createFailureResponse();

	}

		log.info("Getting list of materials from cache is successfull");

		return ResponseUtil.createSuccessResponse().setData(typeAheadData);

	}

	@Override
	public APIResponse deleteMaterials(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateMaterials(Materials materials)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse UpdateMaterialConsumption(
			TicketAcitivityDetailsVO ticketAcitivityDetailsVO)
	{

		log.info("ticketAcitivityDetailsVO details in core services:::"
				+ ticketAcitivityDetailsVO);

		String startRang;
		String endRang;
		String drumNumber = null;
		String message = "";
		APIResponse response=null;
		Map<String, String> subActivityMap = ticketAcitivityDetailsVO
				.getSubActivityDetail();

		log.info("getSubActivityDetails in services:::::" + subActivityMap);

		String ticketId = (ticketAcitivityDetailsVO.getTicketId());

		if (subActivityMap != null && !subActivityMap.isEmpty()
				&& ticketId != null)
		{

			for (Map.Entry<String, String> activityDetails : subActivityMap
					.entrySet())
			{

				TicketMaterialMappingVO ticketMaterialMappingvo = new TicketMaterialMappingVO();

				ticketMaterialMappingvo.setAddedOn(ticketAcitivityDetailsVO
						.getActivityCompletedTime());

				ticketMaterialMappingvo.setModifiedOn(ticketAcitivityDetailsVO
						.getActivityCompletedTime());

				ticketMaterialMappingvo.setActivityId(ticketAcitivityDetailsVO
						.getActivityId());

				ticketMaterialMappingvo
						.setActivityStatus(ticketAcitivityDetailsVO
								.getActivityStatus());

				String ticketid = ticketAcitivityDetailsVO.getTicketId();


				ticketMaterialMappingvo.setTicketId(Long
						.valueOf(ticketAcitivityDetailsVO.getTicketId()));


				if (activityDetails.getKey() != null
						&& !activityDetails.getKey().isEmpty()
						&& !activityDetails.getKey().equals("null"))
				{

					int materialId = (Integer.valueOf(activityDetails.getKey()));

					log.debug("MAterial id for Ticket no " + ticketid + " is "
							+ materialId);

					if (materialId == MaterialConstants.COPPER_CABLE
							|| materialId == MaterialConstants.FIBER_CABLE)
					{

						ticketMaterialMappingvo.setMaterialId(materialId);

						if (activityDetails.getValue() != null
								&& !activityDetails.getValue().isEmpty()
								&& !activityDetails.getValue().equals("null"))
						{

							String copperFiberStartAndEnd = activityDetails
									.getValue();

							String[] copperFiberStartEnd = copperFiberStartAndEnd
									.split(",");

							if (copperFiberStartEnd.length == 3)
							{

								startRang = copperFiberStartAndEnd.split(",")[0]
										.split("=")[1];

								endRang = copperFiberStartAndEnd.split(",")[1]
										.split("=")[1];

								drumNumber = copperFiberStartAndEnd.split(",")[2]
										.split("=")[1];

								ticketMaterialMappingvo.setDrumNo(drumNumber);

							} else
							{

								startRang = copperFiberStartAndEnd.split(",")[0]
										.split("=")[1];

								endRang = copperFiberStartAndEnd.split(",")[1]
										.split("=")[1];

								ticketMaterialMappingvo.setDrumNo(null);
							}

							ticketMaterialMappingvo.setStartPoint(startRang);

							ticketMaterialMappingvo.setEndPoint(endRang);
						}

						ticketMaterialMappingvo.setUoMid(String
								.valueOf(UomName.METERS));

						ticketMaterialMappingvo.setStatus("1");

						// ticketMaterialMappingvo.setTotalConsumption(5);

					} else if (materialId == MaterialConstants.ROUTER
							|| materialId == MaterialConstants.CX_MAC_ID)
					{

						if (activityDetails.getValue() != null
								&& !activityDetails.getValue().isEmpty()
								&& !activityDetails.getValue().equals("null"))
						{

							String macid = activityDetails.getValue();

							ticketMaterialMappingvo.setMacid(activityDetails
									.getValue());
						}

						ticketMaterialMappingvo.setMaterialId(materialId);

						ticketMaterialMappingvo.setUoMid(String
								.valueOf(UomName.NoS));

						// ticketMaterialMappingvo.setTotalConsumption(5);

						ticketMaterialMappingvo.setStatus("1");

					} else if (materialId == MaterialConstants.Battery_SERIAL_N0
							|| materialId == MaterialConstants.CX_RACK)
					{

						if (activityDetails.getValue() != null
								&& !activityDetails.getValue().isEmpty()
								&& !activityDetails.getValue().equals("null"))
						{

							String serialNo = activityDetails.getValue();

							ticketMaterialMappingvo.setSerialNo(activityDetails
									.getValue());
						}

						ticketMaterialMappingvo.setMaterialId(materialId);

						ticketMaterialMappingvo.setUoMid(String
								.valueOf(UomName.NoS));

						// ticketMaterialMappingvo.setTotalConsumption(5);

						ticketMaterialMappingvo.setStatus("1");

					} else
					{

						ticketMaterialMappingvo.setMaterialId(materialId);

						if (activityDetails.getValue() != null
								&& !activityDetails.getValue().isEmpty()
								&& !activityDetails.getValue().equals("null"))
						{

							ticketMaterialMappingvo
									.setTotalConsumption(activityDetails
											.getValue());

						}

						ticketMaterialMappingvo.setUoMid(String
								.valueOf(UomName.NoS));

						ticketMaterialMappingvo.setStatus("1");

					}
					message = materialsDAO
							.UpdateMaterialConsumption(ticketMaterialMappingvo);
				}

			}
             if(AuthUtils.isFRUser()){
            	 
            	 return ResponseUtil.createSuccessResponse().setData(message); 
            	 
             }else{
            	
            	 log.info(" ticketActivityLogCoreServiceImpl services are calling just wait..."
     					+ ticketAcitivityDetailsVO);

     			response = ticketActivityLogCoreServiceImpl
     					.addTicketActivityLog(ticketAcitivityDetailsVO);
     			
     			String currentCRM = dbUtil.getCurrentCrmName(null, null);

    			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
    				
    				workOrderCoreService.updateWorkOrderActivityInCRM(
         					ticketAcitivityDetailsVO.getTicketId() == null ? null : Long.valueOf(ticketAcitivityDetailsVO.getTicketId()), null, null, null,
        					null, null, TicketUpdateConstant.MATERIAL_CONSUMPTION,null);
    			}
     			
             }
			
			if (response.getData() != null)
				return ResponseUtil.createSuccessResponse().setData(message);
			else
				return ResponseUtil
						.createNullParameterResponse("Please provide all required data !");
		}

		else
		{
			return ResponseUtil
					.createNullParameterResponse("Please provide all required data !");
		}
	}

	@Override
	public APIResponse raiseMaterialRequest(
			List<MaterialRequestVO> materialRequestList)
	{

		log.info("Inside raiseMaterialRequest() method of MaterialCoreServiceImpl");

		APIResponse apiResponse = materialsDAO
				.raiseMaterialRequest(materialRequestList);

		return apiResponse;
	}

	
}
