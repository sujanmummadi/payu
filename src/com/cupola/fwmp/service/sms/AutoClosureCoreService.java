package com.cupola.fwmp.service.sms;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.MessageNotificationVo;

public interface AutoClosureCoreService
{
	APIResponse sendMessageToCustomer(MessageNotificationVo notificationVo);
	
//	APIResponse autoClosureTicket(String message,String mobileNo);

	APIResponse autoClosureTicket(String message, String mobileNo,
			String prospectNo);
}
