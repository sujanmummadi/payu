package com.cupola.fwmp.service.sms;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.CutomerAccountActivation;
import com.cupola.fwmp.FWMPConstant.MessageType;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.closeticket.MQSCloseTicketService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.logs.SmsGatewayLog;
import com.cupola.fwmp.vo.logs.SmsReceivedLog;

public class AutoClosureCoreServiceImpl implements AutoClosureCoreService
{

	static final Logger logger = Logger
			.getLogger(AutoClosureCoreServiceImpl.class);

	@Autowired
	MQSCloseTicketService closeTicketCoreService;

	@Autowired
	TicketDAO ticketDao;

	@Autowired
	TicketCoreService ticketCoreService;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	TicketActivityLogDAO activityLogDAO;

	@Autowired
	DefinitionCoreService definitionCoreService;
	
	private int timeout;

	public void setTimeout(int timeout)
	{
		this.timeout = timeout;
	}

	@Autowired
	MongoTemplate mongoTemplate;

	private String smsGatewayAddress;

	@Override
	public APIResponse sendMessageToCustomer(
			MessageNotificationVo notificationVo)
	{

		SmsGatewayLog smsGatewayLog = getSmsGatewayLog();
		BeanUtils.copyProperties(notificationVo, smsGatewayLog);

		logger.debug("Sending sms to customer ===== " + notificationVo);

		smsGatewayAddress = definitionCoreService
				.getIntegrationFilePropertiesByKey(DefinitionCoreService.SMS_GATEWAY_ADDRESS);

		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;
		try
		{
			RequestConfig config = RequestConfig.custom()
					.setConnectTimeout(timeout * 1000)
					.setConnectionRequestTimeout(timeout * 1000)
					.setSocketTimeout(timeout * 1000).build();

			httpClient = HttpClientBuilder.create()
					.setDefaultRequestConfig(config).build();

			String customerPhone = notificationVo != null ? notificationVo.getMobileNumber() : null;

			if (customerPhone != null)
			{
				if (customerPhone.length() == 12)

					customerPhone = customerPhone.substring(2);

				else if (customerPhone.length() == 13)

					customerPhone = customerPhone.substring(3);

				notificationVo.setMobileNumber(customerPhone);
			}
			

			logger.debug("######################### smsGatewayAddress "
					+ smsGatewayAddress);

			if (smsGatewayAddress.contains("{mobile}"))
			{
				logger.debug("######################## " + customerPhone);
				smsGatewayAddress = smsGatewayAddress
						.replace("{mobile}", customerPhone);
			}

			if (smsGatewayAddress != null && smsGatewayAddress.contains("{sms}"))
			{						
				logger.info(customerPhone + " ###############SMS "
						+ notificationVo.getMessage());

				if (notificationVo != null
						&& notificationVo.getMessage() != null)
				{
					logger.info(customerPhone + " ###############SMS "
							+ notificationVo.getMessage());
					
					smsGatewayAddress = smsGatewayAddress
							.replace("{sms}", URLEncoder.encode(notificationVo
									.getMessage(), "UTF-8"));
					
					logger.debug("customerPhone " + customerPhone
							+ " notificationVo.getMessage() "
							+ notificationVo.getMessage());
				}
					
			}

			// smsGatewayAddress = smsGatewayAddress.replaceAll("\\s+", "+");

			

			logger.debug(customerPhone + " SMS Gateway Address Is === "
					+ smsGatewayAddress);

			// smsGatewayAddress = URLEncoder.encode(smsGatewayAddress,
			// "UTF-8");
			//
			// logger.info("SMS Gateway Address Is === " + smsGatewayAddress);

			URIBuilder uriBuilder = new URIBuilder(smsGatewayAddress);

			smsGatewayLog.setUrl(smsGatewayAddress);
			smsGatewayLog.setRequestTime(new Date() + "");

			HttpGet getRequest = new HttpGet(uriBuilder.build());
			smsGatewayLog.setRequest(getRequest + "");

			response = httpClient.execute(getRequest);

			smsGatewayLog.setResponseTime(new Date() + "");

			smsGatewayLog.setResponse(response + "");

			logger.debug("Customer smsGatewayAddress gateway response "
					+ response);
			
			if (notificationVo != null && notificationVo.getTicketId() != null
					&& !notificationVo.getTicketId().isEmpty())
			{
				TicketLog logDetail = new TicketLogImpl(Long
						.valueOf(notificationVo
								.getTicketId()), TicketUpdateConstant.PORT_ACTIVATION_MESSAGE_TRIGGERED_TO_CUSTOMER, FWMPConstant.SYSTEM_ENGINE);

				logDetail.setAddedOn(new Date());
				TicketUpdateGateway.logTicket(logDetail);
			}

			mongoTemplate.insert(smsGatewayLog);

			if (response.getStatusLine().getStatusCode() == StatusCodes.SUCCESS)
			{
				String output = EntityUtils.toString(response.getEntity());

				logger.debug("Output of Sending SMS::" + output);
				httpClient.close();

				return ResponseUtil.createSuccessResponse();

			} else
			{
				httpClient.close();
				
				response.close();

				return ResponseUtil.createFailureResponse();
			}

		} catch (Exception e)
		{

			logger.error("Failed To Connect To SMS Server . Reason :: "
					+ e.getMessage());
			e.printStackTrace();
			try
			{
				httpClient.close();
			} catch (IOException e1)
			{
				logger.error("Error While closing httpcling :"
						+ e.getMessage());
				e1.printStackTrace();
			}
			return ResponseUtil.createFailureResponse();
		} finally
		{
			try
			{
				if (httpClient != null)
					httpClient.close();

				if (response != null)
					response.close();

			} catch (IOException e)
			{
				logger.error("Error in finally block, while closing httpresponse for SMS Server."
						+ e.getMessage());
				e.printStackTrace();
			}
		}

	}

	@Override
	public APIResponse autoClosureTicket(String message, String mobileNo,
			String prospectNo)
	{
		SmsReceivedLog smsReceivedLog = new SmsReceivedLog();
		smsReceivedLog.setSmsGatewayLogId(StrictMicroSecondTimeBasedGuid.newGuid());
		smsReceivedLog.setRequestTime(new Date().toString());
		smsReceivedLog.setMessage(message);
		smsReceivedLog.setMobileNumber(mobileNo);
		smsReceivedLog.setProspectNo(prospectNo);

		String customerPhone = mobileNo;

		logger.info("customerPhone::"+customerPhone.length()+" "+mobileNo.length()+""+mobileNo);
		
		if (customerPhone.length() == 12)

			customerPhone = customerPhone.substring(2);

		else if (customerPhone.length() == 13)

			customerPhone = customerPhone.substring(3);
		TicketClosureDataVo ticket = null;

		if (prospectNo != null)
		{
			logger.info("Ticket is from Web " + mobileNo + " prospectNo "
					+ prospectNo);

			ticket = ticketDao.getTicketByCustomerMobileNoWithProspect(customerPhone, prospectNo);

			smsReceivedLog.setRequestSource("Web");

		} else
		{
			logger.info("Ticket is from SMS " + customerPhone + " prospectNo "
					+ prospectNo);

			ticket = ticketDao
				.getTicketByCustomerMobileNo(customerPhone);

			logger.info("ticket details for customer account activation in AutoClousureCoreimpl::"+ticket);
			smsReceivedLog.setRequestSource("Customer SMS");
		}
		if (ticket != null)
		{
			smsReceivedLog.setTicketId(ticket.getTicketId() + "");
			logger.info("ticket id for customer for closing ticket "
					+ ticket.getTicketId() + " prospect number "
					+ ticket.getProspectNo() + " and message is "
					+ message.toLowerCase());

			TicketLog logDetails = new TicketLogImpl(ticket
					.getTicketId(), TicketUpdateConstant.SMS_RECEIVED_FROM_CUSTOMER
							+ " [" + message + "]", FWMPConstant.SYSTEM_ENGINE);

			logDetails.setAddedOn(new Date());
			TicketUpdateGateway.logTicket(logDetails);

			message = message.toLowerCase();

			if (message.toLowerCase().contains("yes"))
			{
				TicketClosureDataVo closeTicketVo = new TicketClosureDataVo();

				BeanUtils.copyProperties(ticket, closeTicketVo);

				@SuppressWarnings("unchecked")
				String workOrderNumber = ticket.getWorkOrderNo();

				if (workOrderNumber != null && !workOrderNumber.isEmpty())
				{
					smsReceivedLog.setWorkOrderId(workOrderNumber);
					closeTicketVo.setWorkOrderNo(workOrderNumber);

					logger.info("close ticket vo ========== " + closeTicketVo);
					smsReceivedLog.setMqRequestTime(new Date().toString());
					
					String currentCRM = dbUtil.getCurrentCrmName(null, null);

					if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
						
							MessageVO vo = new MessageVO();
							vo.setMessageType(MessageType.AUTO_CLOSURE_CUSTOMER_RESPONSE_YES);
							String msg = MessageType.AUTO_CLOSURE_CUSTOMER_RESPONSE_YES_MESSAGE;

							if (msg.contains("_id_"))
								msg = msg.replaceAll("_id_", ticket.getTicketId()
										+ "");

							vo.setMessage(ticket.getTicketId() + "");

							logger.info("Message VO sending to User contains info ========= "
									+ vo);

							ticketCoreService.autoClosureStatusUpdateToUser(ticket
									.getTicketId(), vo);
							mongoTemplate.insert(smsReceivedLog);
							
							return closeTicketCoreService
							.closeTicket(closeTicketVo);
							
					}else if( currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)){
						
						APIResponse mqResponse = closeTicketCoreService
								.closeTicket(closeTicketVo);

						smsReceivedLog.setMqResponseTime(new Date().toString());
						smsReceivedLog.setMqResponse(mqResponse.toString());
						logger.info("Mq Response after closing ticket of customer ::  "
								+ customerPhone + " is ====== " + mqResponse);

						TicketLog logDetail = new TicketLogImpl(ticket
								.getTicketId(), TicketUpdateConstant.MQ_CLOSURE_REQUEST
										+ " Response Code " + mqResponse
												.getStatusCode(), FWMPConstant.SYSTEM_ENGINE);

						logDetail.setAddedOn(new Date());
						TicketUpdateGateway.logTicket(logDetail);
						
						if (mqResponse != null && mqResponse.getStatusCode() == 0)
						{

							MessageVO vo = new MessageVO();
							vo.setMessageType(MessageType.AUTO_CLOSURE_CUSTOMER_RESPONSE_YES);
							String msg = MessageType.AUTO_CLOSURE_CUSTOMER_RESPONSE_YES_MESSAGE;

							if (msg.contains("_id_"))
								msg = msg.replaceAll("_id_", ticket.getTicketId()
										+ "");

							vo.setMessage(ticket.getTicketId() + "");

							logger.info("Message VO sending to User contains info ========= "
									+ vo);

							ticketCoreService.autoClosureStatusUpdateToUser(ticket
									.getTicketId(), vo);
							mongoTemplate.insert(smsReceivedLog);
							
							
							
							return ResponseUtil.createTicketClosureSuccessInMQ()
									.setData(ResponseUtil
											.createTicketClosureSuccessInMQ()
											.getStatusMessage());
						} else
						{
							logger.info("Ticket not closed properly in mq, MQ error is "
									+ mqResponse);

							if (mqResponse != null)
							{
								String mqErrorMessage = definitionCoreService
										.getMqErrorMessageById(Long
												.valueOf(mqResponse
														.getStatusCode()));
								mongoTemplate.insert(smsReceivedLog);
								return ResponseUtil.createTicketClosureFailedInMQ()
										.setData(mqErrorMessage);
							}
						}
					}
				}

				else
				{
					logger.info("No Work order found for customer "
							+ customerPhone);
					mongoTemplate.insert(smsReceivedLog);
					return ResponseUtil.NoRecordFound();
				}

			} else if (message.contains("no"))
			{
				MessageVO vo = new MessageVO();
				vo.setMessageType(MessageType.AUTO_CLOSURE_CUSTOMER_RESPONSE_NO);
				String msg = MessageType.AUTO_CLOSURE_CUSTOMER_RESPONSE_NO_MESSAGE;

				if (msg.contains("_id_"))
					msg = msg.replaceAll("_id_", ticket.getTicketId() + "");

				vo.setMessage(ticket.getTicketId() + "");

				logger.info("Message VO sending to User contains info ========= "
						+ vo);
				ticketCoreService.autoClosureStatusUpdateToUser(ticket
						.getTicketId(), vo);

				TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();

				ticketActivityLogVo.setModifiedOn(new Date());
				ticketActivityLogVo.setAddedOn(new Date());
				ticketActivityLogVo.setTicketId(ticket.getTicketId());
				ticketActivityLogVo
						.setActivityId(CutomerAccountActivation.CUSTOMER_ACCOUNT_ACTIVATION);

				ticketActivityLogVo.setStatus(TicketStatus.ACTIVATION_PENDING);

				ticketActivityLogVo
						.setId(StrictMicroSecondTimeBasedGuid.newGuid());

				activityLogDAO.addTicketActivityLog(ticketActivityLogVo);

				activityLogDAO.updateTicketStatus(ticketActivityLogVo);
				mongoTemplate.insert(smsReceivedLog);
				return ResponseUtil.createPortalPageNotReachable();
			}

			else
			{
				logger.info("Message doesn't contain yes or no. Message is "
						+ message);
				mongoTemplate.insert(smsReceivedLog);
				return ResponseUtil.createInvalidReponse();
			}

		} else
		{

			logger.info("NO ticket is associated with customer no: "
					+ customerPhone);
			smsReceivedLog.setTicketId("Customer Not found");
			mongoTemplate.insert(smsReceivedLog);
			return ResponseUtil.NoRecordFound();
		}
		return null;
	}

	private SmsGatewayLog getSmsGatewayLog()
	{
		SmsGatewayLog smsGatewayLog = new SmsGatewayLog();

		smsGatewayLog.setRequestTime(new Date().toString());

		smsGatewayLog
				.setSmsGatewayLogId(StrictMicroSecondTimeBasedGuid.newGuid());

		return smsGatewayLog;

	}
}
