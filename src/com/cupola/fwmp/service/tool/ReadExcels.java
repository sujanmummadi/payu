/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.DeviceType.DeviceTypeStatusString;
import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.service.configuration.ConfigurationCoreService;
import com.cupola.fwmp.service.definition.excels.ToolsExcelsDefinition;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.vo.tools.DeviceToolVo;

/**
 * @author aditya
 */

public class ReadExcels
{
	private Logger log = Logger.getLogger(ReadExcels.class.getName());

	@Autowired
	ToolsExcelsDefinition toolsExcelsDefinition;

	@Autowired
	ConfigurationCoreService configurationCoreService;

	public void readElemetsDetails()
	{
		Set<DeviceToolVo> deviceToolVos = new LinkedHashSet<>();

		String basePath = toolsExcelsDefinition
				.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVCIE_BASE_PATH);

		String fileName = toolsExcelsDefinition
				.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_NAME);

		String archivePath = toolsExcelsDefinition
				.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVCIE_ARCHIVE_PATH);

		String file = basePath + File.separator + fileName;

		log.info("File to read is " + file);

		File devcieFile = new File(file);
		FileInputStream fis = null;

		try
		{
			fis = new FileInputStream(devcieFile);
			Iterator<Row> rowIterator = null;
			int totalRow = 0;

			if (devcieFile.getName().endsWith(".xlsx"))
			{

				XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
				XSSFSheet mySheet = myWorkBook.getSheetAt(0);
				totalRow = mySheet.getPhysicalNumberOfRows();
				rowIterator = mySheet.iterator();

			} else if (devcieFile.getName().endsWith(".xls"))
			{

				HSSFWorkbook myWorkBook = new HSSFWorkbook(fis);
				HSSFSheet mySheet = myWorkBook.getSheetAt(0);
				totalRow = mySheet.getPhysicalNumberOfRows();
				rowIterator = mySheet.iterator();
			}

			if(rowIterator != null)
			while (rowIterator.hasNext())
			{
				DeviceToolVo deviceToolVo = new DeviceToolVo();

				Row row = rowIterator.next();

				if (row.getRowNum() == Integer.valueOf(toolsExcelsDefinition
						.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_HEARDER_COUNT)))
				{
					continue;
				}

				Iterator<Cell> cellIterator = row.cellIterator();

				int columnIndex = 1;

				String smsId = "";

				if (row.getPhysicalNumberOfCells() != Integer
						.valueOf(toolsExcelsDefinition
								.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_COLUMN_COUNT)))
				{

					log.error("Skipping row no " + row.getRowNum()
							+ " as it is having extra number "
							+ "of column than Expected. Count of column is  "
							+ row.getPhysicalNumberOfCells());

					continue;

				} else
				{
					while (cellIterator.hasNext())
					{
						Cell cell = cellIterator.next();
						if (columnIndex == Integer.valueOf(toolsExcelsDefinition
								.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_NAME)))
						{
							smsId = CommonUtil.checkCellType(cell);

							deviceToolVo.setDeviceName(smsId);

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_IP_ADDRESS)))
						{
							deviceToolVo.setIpAddress(CommonUtil
									.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_MAC_ADDRESS)))
						{
							deviceToolVo.setMacAddress(CommonUtil
									.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_DEVCIE_TYPE)))
						{
							deviceToolVo.setDeviceType(CommonUtil
									.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_CITY_NAME)))
						{
							deviceToolVo
									.setCity(CommonUtil.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_BRANCH_CODE)))
						{
							deviceToolVo
									.setBranch(CommonUtil.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_AREA_NAME)))
						{
							deviceToolVo
									.setArea(CommonUtil.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_AVAIALBLE_PORT)))
						{
							deviceToolVo.setAvailablePort(CommonUtil
									.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_TOTAL_PORT)))
						{
							deviceToolVo.setTotalPort(CommonUtil
									.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_USED_PORT)))
						{
							deviceToolVo.setUsedPort(CommonUtil
									.checkCellType(cell));

						} else if (columnIndex == Integer
								.valueOf(toolsExcelsDefinition
										.getDevcieToolsIndexByKey(ToolsExcelsDefinition.FWMP_TOOL_DEVICE_FILE_FX_STATUS)))
						{
							String status = CommonUtil.checkCellType(cell);
							if (status
									.equalsIgnoreCase(DeviceTypeStatusString.ACTIVE))
							{
								deviceToolVo.setStatus(Status.ACTIVE);
							}
						}
						columnIndex += 1;
					} // end of while for cell

					deviceToolVos.add(deviceToolVo);
					log.info("Remaing line to read in " + file + " is "
							+ --totalRow);

				} // End of else cell

			} // end of else

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();

			log.error("Error in finding Devcie file " + e.getMessage());

		} catch (IOException e)
		{

			e.printStackTrace();

			log.error("Error in Reading Device file " + e.getMessage());

		} finally
		{
			if (fis != null)
			{
				try
				{
					fis.close();

				} catch (IOException e)
				{
					e.printStackTrace();

				}
			}
		}

//		moveFileToArchive(fileName, basePath, archivePath);

		log.info("Calling add bulk devcie with total available device sixe "
				+ deviceToolVos.size());

		configurationCoreService.addBulkDevice(deviceToolVos);

	}

	public void moveFileToArchive(String fileName, String sourceFilePath,
			String destinationFilePath)
	{
		File source = new File(sourceFilePath + File.separator + fileName);
		File destination = new File(destinationFilePath + File.separator
				+ fileName);

		log.info("Source " + source + " Destiantion " + destination);

		try
		{

			File tempFile = destination;

			if (tempFile.exists())
			{

				Date renamingTime = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy,HH:mm:ss");

				String formatedDateRenaming = formatter.format(renamingTime);

				File renameFile = new File(destination + "Back_Up_"
						+ formatedDateRenaming + "_" + fileName);

				try
				{
					tempFile.renameTo(renameFile);

					log.info("File " + destination + " renamed to "
							+ renameFile);

				} catch (Exception e)
				{

					e.printStackTrace();
					log.error("Error in renaming file " + destinationFilePath
							+ fileName);
				}

			}

			FileUtils.moveFile(source, destination);

		} catch (IOException e)
		{

			e.printStackTrace();
			log.error("Error in moving file " + fileName + " from " + source
					+ " to " + destinationFilePath + " " + e.getMessage());

		} // End of catch

	}

	/**@author aditya
	 * Set<String>
	 * @param filePath
	 * @return
	 */
	public Set<String> readManualFrClosureFile(String filePath) {

		log.info("File to read is " + filePath);
		
		Set<String> set = new HashSet<>();
		File devcieFile = new File(filePath);
		FileInputStream fis = null;

		try
		{
			fis = new FileInputStream(devcieFile);
			Iterator<Row> rowIterator = null;
			int totalRow = 0;

			if (devcieFile.getName().endsWith(".xlsx"))
			{

				XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
				XSSFSheet mySheet = myWorkBook.getSheetAt(0);
				totalRow = mySheet.getPhysicalNumberOfRows();
				rowIterator = mySheet.iterator();

			} else if (devcieFile.getName().endsWith(".xls"))
			{

				HSSFWorkbook myWorkBook = new HSSFWorkbook(fis);
				HSSFSheet mySheet = myWorkBook.getSheetAt(0);
				totalRow = mySheet.getPhysicalNumberOfRows();
				rowIterator = mySheet.iterator();
			}
			
			int targetCellNumber = 0;
			String workOrderNumber = "";

			if(rowIterator != null)
			while (rowIterator.hasNext())
			{

				Row row = rowIterator.next();

				if (row.getRowNum() == 0)
				{
					for(int i = 0; i < row.getLastCellNum(); i++)
		            {
		                if(row.getCell(i).getStringCellValue().trim().equalsIgnoreCase("COMPLAINT_NO"))
		                	targetCellNumber = i;
		            }
					
					continue;
				}

				log.info("Target cell value is " + targetCellNumber );
				
								
				Cell c = row.getCell(targetCellNumber);
				
				workOrderNumber = CommonUtil
						.checkCellType(c);
				
// 				Iterator<Cell> cellIterator = row.cellIterator();
//
//				int columnIndex = 1;
//
//				String workOrderNumber = "";
//
// 					while (cellIterator.hasNext())
//					{
//						Cell cell = cellIterator.next();
//						if (columnIndex == targetCellNumber)
//						{
//							 
//							workOrderNumber = CommonUtil
//									.checkCellType(cell);
//
//						}						
//						columnIndex += 1;
//					}
 					set.add(workOrderNumber);
					log.info("Remaing line to read in " + filePath + " after wo " + workOrderNumber + " is "
							+ --totalRow);

				} // End of else cell


		} catch (FileNotFoundException e)
		{
			e.printStackTrace();

			log.error("Error in finding fr closure file " + e.getMessage());

		} catch (Exception e)
		{

			e.printStackTrace();

			log.error("Error in Reading fr closure file " + e.getMessage());

		} finally
		{
			if (fis != null)
			{
				try
				{
					fis.close();

				} catch (IOException e)
				{
					e.printStackTrace();

				}
			}
		}

		log.info("Total workorder to close are " + set.size());
		
		return set;

	}

}
