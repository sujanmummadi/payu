/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.util.LinkedHashSet;
import java.util.Map;

import com.cupola.fwmp.response.APIResponse;

/**
 * @author aditya
 *
 */
public interface GlobalCoreService
{
	APIResponse mergeMainQueue(Map<String, LinkedHashSet<Long>> mainQueue);
	
	void mergeMainQueueJob();

	APIResponse resetQueue();

	void cleanQueue();

	APIResponse resetFrQueue();

	APIResponse mergeFrMainQueue(Map<String, LinkedHashSet<Long>> mainQueue);

	void mergeFrMainQueueJob();

	/**@author aditya
	 * void
	 * @param setValue
	 * @param assignedTo
	 */
	void addTicket2MainQueue(LinkedHashSet<Long> setValue, long assignedTo);

	/**@author aditya
	 * void
	 * @param setValue
	 * @param assignedTo
	 */
	void addFrTicket2MainQueue(LinkedHashSet<Long> setValue, long assignedTo);
	
	
	
	/*boolean isTicketAvailable(Long userId, Long ticketId);*/
}
