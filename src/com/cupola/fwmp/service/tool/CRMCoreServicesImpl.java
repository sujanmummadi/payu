/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.math.BigInteger;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.tool.CRMDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.jobs.CrmJobHelper;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.tools.TransactionHistoryLogger;

/**
 * @author aditya
 *
 */
public class CRMCoreServicesImpl implements CRMCoreServices {
	@Autowired
	CRMDao crmDao;

	@Autowired
	CrmJobHelper crmJobHelper;
	
	@Autowired
	CRMCoreServiceHYD crmCoreServiceHYD;

	@Autowired
	CRMCoreServiceROI crmCoreServiceROI;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	WorkOrderDAO workOrderDAO;

	@Autowired
	FrTicketDao frTicketDao;
	
	@Autowired
	TicketDAO ticketDAO;
	

	private static Logger log = LogManager.getLogger(CRMCoreServicesImpl.class.getName());

	@Override
	public APIResponse getCrmResponseByCompleteQuery(String query, String cityName) {
		try {
			log.info(" Get all the open ticket On demand for " + cityName + " using " + query);

			List<MQWorkorderVO> crmOpenTickets = crmDao.getAllOpenTicketFormCRM(query, cityName);

			return populateWorkOrder(query, cityName, crmOpenTickets);

		} catch (Exception e) {
			log.error("Exception in getting all the open ticket On demand for " + cityName + " using " + query + " "
					+ e.getMessage());

			e.printStackTrace();

			return ResponseUtil.createDBExceptionResponse();
		}
	}

	@Override
	public APIResponse getCrmResponseByWhereClauseQuery(String query, String cityName) {
		StringBuilder baseQuery = new StringBuilder(
				definitionCoreService.getClassificationQuery(DefinitionCoreService.CRM_BASE_QUERY));

		log.info(" Get all the open ticket On demand for " + cityName + " using where clause query " + query);

		try {
			if (CityName.HYDERABAD_CITY.equalsIgnoreCase(cityName)) {
				baseQuery.append(
						" " + definitionCoreService.getClassificationQuery(DefinitionCoreService.CRM_HYD_VIEW_NAME));

			} else {
				baseQuery.append(
						" " + definitionCoreService.getClassificationQuery(DefinitionCoreService.CRM_ROI_VIEW_NAME));
			}

			baseQuery.append(" " + query);

			log.info(" Get all the open ticket On demand for " + cityName + " using " + baseQuery.toString());

			List<MQWorkorderVO> crmOpenTickets = crmDao.getAllOpenTicketFormCRM(baseQuery.toString(), cityName);

			return populateWorkOrder(baseQuery.toString(), cityName, crmOpenTickets);

		} catch (Exception e) {
			log.error("Exception in getting all the open ticket On demand for " + cityName + " using " + query + " "
					+ e.getMessage());

			e.printStackTrace();

			return ResponseUtil.createDBExceptionResponse();
		}
	}

	private APIResponse populateWorkOrder(String query, String cityName, List<MQWorkorderVO> crmOpenTickets) {
		APIResponse apiResponse = null;

		if (CityName.HYDERABAD_CITY.equalsIgnoreCase(cityName)) {
			if (crmOpenTickets != null && !crmOpenTickets.isEmpty())
				apiResponse = crmCoreServiceHYD.controleHYDCrm(crmOpenTickets);
			else
				log.info("No record found On demand for " + cityName + " using " + query);
		} else {
			if (crmOpenTickets != null && !crmOpenTickets.isEmpty())
				apiResponse = crmCoreServiceROI.controleROICrm(crmOpenTickets);
			else {
				log.info("No record found On demand for " + cityName + " using " + query);
				return ResponseUtil.createRecordNotFoundResponse()
						.setData("No record found On demand for " + cityName + " using " + query);
			}
		}
		return apiResponse;
	}

	@Override
	public APIResponse populateWorkOrder(String cityName, List<MQWorkorderVO> crmOpenTickets) {

		APIResponse apiResponse = null;

		if (CityName.HYDERABAD_CITY.equalsIgnoreCase(cityName)) {

			if (crmOpenTickets != null && !crmOpenTickets.isEmpty())
				apiResponse = crmCoreServiceHYD.controleHYDCrm(crmOpenTickets);

		} else {

			if (crmOpenTickets != null && !crmOpenTickets.isEmpty())
				apiResponse = crmCoreServiceROI.controleROICrm(crmOpenTickets);
		}

		return apiResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.CRMCoreServices#getTransactionById(java.lang.
	 * String)
	 */
	@Override
	public APIResponse getTransactionById(String transactionId) {

		if (crmDao.getTransactionById(transactionId)) {
			log.info("Duplicate Transaction ID : "+transactionId);
			return ResponseUtil.createDuplicateTransaction();

		} else {

			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.CRMCoreServices#getTransactionById(com.cupola.
	 * fwmp.vo.tools.TransactionHistoryLogger)
	 */
	@Override
	public APIResponse getTransactionById(TransactionHistoryLogger transactionHistoryLogger) {

		
		try
	      {
			new BigInteger(transactionHistoryLogger.getTransactionId());
	      }
	      catch (NumberFormatException ex)
	      {
	         return ResponseUtil.createInvalidTransactionID();
	      }
		
		if (crmDao.getTransactionById(transactionHistoryLogger)) {

			return ResponseUtil.createDuplicateTransaction();

		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.CRMCoreServices#getProspectByProspectNumber(java
	 * .lang.String)
	 */
	@Override
	public APIResponse getProspectByProspectNumber(String prospectNumber) {
		if (ticketDAO.isProspectPresentInDB(prospectNumber))
			return ResponseUtil.createpDuplicateProspect();
		else
			return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.CRMCoreServices#getWorkOrderByWorkOrderNumber(
	 * java.lang.String)
	 */
	@Override
	public APIResponse getWorkOrderByWorkOrderNumber(String workOrderNumber) {
		if (workOrderDAO.isWOPresentInDB(workOrderNumber))
			return ResponseUtil.createDuplicateWorkOrder();
		else
			return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.CRMCoreServices#getFrWOByFrWONumber(java.lang.
	 * String)
	 */
	@Override
	public APIResponse getFrWOByFrWONumber(String frWONumber) {
		if (frTicketDao.isFrWOPresentInDB(frWONumber))
			
			return ResponseUtil.createDuplicateSR();
		else
		return null;
	}
	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.tool.CRMCoreServices#getCrmResponseByWhereClauseQueryForFr(java.lang.String, java.lang.String)
	 */
	@Override
	public APIResponse getCrmResponseByWhereClauseQueryForFr(String query, String cityName) {

		StringBuilder baseQuery = new StringBuilder(definitionCoreService
				.getClassificationQuery(DefinitionCoreService.CRM_BASE_QUERY));

		log.info(" Get all the open ticket On demand for " + cityName
				+ " using where clause query " + query);

		try
		{
			if (CityName.HYDERABAD_CITY.equalsIgnoreCase(cityName))
			{
				baseQuery.append(" " + definitionCoreService
						.getClassificationQuery(DefinitionCoreService.CRM_HYD_VIEW_NAME));

			} else
			{
				baseQuery.append(" " + definitionCoreService
						.getClassificationQuery(DefinitionCoreService.CRM_ROI_VIEW_NAME));
			}

			baseQuery.append(" " + query);

			log.info(" Get all the open ticket On demand for " + cityName
					+ " using " + baseQuery.toString());

			List<MQWorkorderVO> crmOpenTickets = crmDao
					.getAllOpenTicketFormCRM(baseQuery.toString(), cityName);
			
			log.info(" Got all the open ticket On demand for " + cityName
					+ " using " + baseQuery.toString());
			
			return populateWorkOrder(baseQuery.toString(), cityName, crmOpenTickets);

		} catch (Exception e)
		{
			log.error("Exception in getting all the open ticket On demand for fr "
					+ cityName + " using " + query + " " + e.getMessage());

			e.printStackTrace();

			return ResponseUtil.createDBExceptionResponse();
		}
	
	}

}
