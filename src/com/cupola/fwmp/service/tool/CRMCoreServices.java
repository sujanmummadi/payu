/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.util.List;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.tools.TransactionHistoryLogger;

/**
 * @author aditya
 *
 */
public interface CRMCoreServices {

	APIResponse getCrmResponseByCompleteQuery(String query, String cityName);

	APIResponse getCrmResponseByWhereClauseQuery(String query, String cityName);

	/**
	 * @author kiran APIResponse
	 * @param cityName
	 * @param crmOpenTickets
	 * @return
	 */
	APIResponse populateWorkOrder(String cityName, List<MQWorkorderVO> crmOpenTickets);

	/**
	 * @author aditya APIResponse
	 * @param transactionId
	 * @return
	 */
	APIResponse getTransactionById(String transactionId);

	/**
	 * @author aditya APIResponse
	 * @param transactionHistoryLogger
	 * @return
	 */
	APIResponse getTransactionById(TransactionHistoryLogger transactionHistoryLogger);

	/**
	 * @author aditya APIResponse
	 * @param prospectNumber
	 * @return
	 */
	APIResponse getProspectByProspectNumber(String prospectNumber);

	/**
	 * @author aditya APIResponse
	 * @param workOrderNumber
	 * @return
	 */
	APIResponse getWorkOrderByWorkOrderNumber(String workOrderNumber);

	/**
	 * @author aditya APIResponse
	 * @param frWONumber
	 * @return
	 */
	APIResponse getFrWOByFrWONumber(String frWONumber);

	/**@author aditya
	 * APIResponse
	 * @param query
	 * @param cityName
	 * @return
	 */
	APIResponse getCrmResponseByWhereClauseQueryForFr(String query, String cityName);
}
