/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.ClassificationCategories;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.handlers.CrmControllerHandler;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ClassificationEngineCoreServiceImpl;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.UserVo;

/**
 * @author aditya
 *
 */
public class CRMCoreServiceROI
{

	private static Logger log = LogManager
			.getLogger(CRMCoreServiceROI.class.getName());

	@Autowired
	private ClassificationEngineCoreServiceImpl classificationEngineCoreServiceImpl;

	@Autowired
	private GlobalActivities globalActivities;

	@Autowired
	private UserDao userDao;

	@Autowired
	private DBUtil dbUtil;

	@Autowired
	private CrmControllerHandler crmControllerHandler;

	@Autowired
	private CustomerCoreService customerCoreService;

	@Autowired
	TicketDAO ticketDAO;

	private boolean isFREnable = false; // take off when fr is fully
										// functionally integrated

	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;

	public APIResponse controleROICrm(List<MQWorkorderVO> roiMQWorkorderVOList)
	{
		try
		{

			if (roiMQWorkorderVOList != null && !roiMQWorkorderVOList.isEmpty())
			{
				log.info("Call prioritization Engine form ROI for "
						+ roiMQWorkorderVOList.size());
				// + roiMQWorkorderVOList);

				// Thread based logic have to be performed here based on city;

				List<String> prospectList = new ArrayList<>();
				Map<String, MQWorkorderVO> prospectDetailMap = new ConcurrentHashMap<String, MQWorkorderVO>();

				for (MQWorkorderVO roiMQWorkorderVO : roiMQWorkorderVOList)
				{
					try 
					{
						if(roiMQWorkorderVO.getProspectNumber() != null) 
						{
							prospectList.add(roiMQWorkorderVO.getProspectNumber());
							prospectDetailMap.put(roiMQWorkorderVO.getProspectNumber(), roiMQWorkorderVO);
						}
						
					} catch (Exception e) 
					{
						e.printStackTrace();
						log.error("ROI manual No prospect found for WO " + roiMQWorkorderVO.getWorkOrderNumber());
						continue;
					}
				}

				log.info("Size of total data from ROI MQ is "
						+ prospectDetailMap.size());
				// + prospectDetailMap);

				Map<String, Long> propspectTicketMapFromDb = customerCoreService
						.getTicketIdsFromProspects(prospectList,"ROI");

				log.info("Size of total data from FWMP DB for ROI is "
						+ propspectTicketMapFromDb.size());
				// + propspectTicketMapFromDb);

				Set<String> prospectListsFromDb = propspectTicketMapFromDb
						.keySet();

				prospectDetailMap.keySet().removeAll(prospectListsFromDb);

				log.info("Size of total data Not available in FWMP DB for ROI is "
						+ prospectDetailMap.size());
				// + prospectDetailMap);

				List<MQWorkorderVO> mqDataNotInFWMP = new ArrayList<MQWorkorderVO>(prospectDetailMap
						.values());

				log.info("Creating data in fwmp for ROI "
						+ mqDataNotInFWMP.size());

				Map<String, Long> latestPropspectTicketMapFromDb = customerCoreService
						.addCustomerViaCRM(mqDataNotInFWMP,"ROI");

				Map<String, Long> completeProspectMap = new ConcurrentHashMap<String, Long>();
				completeProspectMap.putAll(latestPropspectTicketMapFromDb);
				completeProspectMap.putAll(propspectTicketMapFromDb);

				log.info("Complete propspect map for ROI "
						+ completeProspectMap.size());
				// + completeProspectMap);

				List<Long> ticketIds = new ArrayList<Long>(completeProspectMap
						.values());

				Map<Long, String> ticketIdAndFxNameMap = customerCoreService
						.getFxNameFromTicketId(ticketIds);

				log.info("Total ticketIdAndFxNameMap for ROI size is "
						+ ticketIdAndFxNameMap.size());
				// + ticketIdAndFxNameMap);

				Map<Long, String> ticketIdAndConnectionTypeMap = customerCoreService
						.getConnectionTypeFromTicketId(ticketIds);

				log.info("Total ticketIdAndConnectionTypeMap for ROI size is "
						+ ticketIdAndFxNameMap.size());
				// + ticketIdAndConnectionTypeMap);

				Map<Long, Long> ticketIdAndSaelsExecutiveMap = customerCoreService
						.getTicketIdAndSaelsExecutiveMap(ticketIds);

				log.info("Total ticketIdAndSaelsExecutiveMap for ROI size is "
						+ ticketIdAndFxNameMap.size());
				// + ticketIdAndSaelsExecutiveMap);

				LinkedHashSet<Long> setValue = new LinkedHashSet<>();

				Map<Long, Boolean> workOrderCreationStatus = crmControllerHandler
						.createWorkOrderEntryInBulk(roiMQWorkorderVOList, completeProspectMap);

				log.info("Total workOrderCreationStatus for ROI size is "
						+ workOrderCreationStatus.size());

				for (MQWorkorderVO roiMQWorkorderVO : roiMQWorkorderVOList)
				{
					try
					{
						long ticketId = completeProspectMap
								.get(roiMQWorkorderVO.getProspectNumber());

						setValue.add(ticketId);

						Long assignedTo = null;
						Long assignedBy = ticketIdAndSaelsExecutiveMap
								.get(ticketId);

						// if (ClassificationCategories.NEW_INSTALLATION_CATEG
						// .equalsIgnoreCase(roiMQWorkorderVO
						// .getTicketCategory())
						// || ClassificationCategories.NEW_INSTALLATION_SYMPTOM
						// .equalsIgnoreCase(roiMQWorkorderVO
						// .getSymptom()))

						log.info("ROI definitionCoreService.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)"
								+ definitionCoreService
										.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
								+ "roiMQWorkorderVO.getTicketCategory().toLowerCase()"
								+ roiMQWorkorderVO.getTicketCategory());

						if (definitionCoreService
								.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
								.contains(roiMQWorkorderVO.getTicketCategory().toUpperCase())
								|| definitionCoreService
										.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_SYMPTOM)
										.contains(roiMQWorkorderVO
												.getSymptom().toUpperCase()))
						{
							//
							// if (crmControllerHandler
							// .createWorkOrderEntry(roiMQWorkorderVO
							// .getProspectNumber(), roiMQWorkorderVO
							// .getMqId(), roiMQWorkorderVO
							// .getWorkOrderNumber(),
							// TicketCategory.NEW_INSTALLATION))
							if (workOrderCreationStatus.get(ticketId))
							{

								if (roiMQWorkorderVO.getAssignedTo() == null
										|| roiMQWorkorderVO
												.getAssignedTo() <= 0)
								{

									String fxName = ticketIdAndFxNameMap
											.get(ticketId);

									String connectionType = ticketIdAndConnectionTypeMap
											.get(ticketId);
									UserVo userVo = null;

									if (TicketSubCategory.COPPER
											.equalsIgnoreCase(connectionType))
									{

										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

									} else if (TicketSubCategory.FIBER
											.equalsIgnoreCase(connectionType))
									{
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

									}

									log.info("Assigned to for ticketId for ROI "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();

										log.info("Assiging ROI NEW_INSTALLATION ticket "
												+ ticketId + "to "
												+ assignedTo);

										addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

										String currentCRM = dbUtil.getCurrentCrmName(null, null);

										if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
											
											workOrderCoreService.updateWorkOrderActivityInCRM(
													ticketId, null, null, null,
													null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);
										}
										
										
										log.info("In ROI Found User for Ticket Id "
												+ ticketId + " assignedTo "
												+ assignedTo);

									} else
									{

//										userVo = userDao
//												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF_STRICT);
										
										if (userVo != null)
										{
											assignedTo = userVo.getId();

											log.info("Assiging ROI NEW_INSTALLATION ticket "
													+ ticketId + "to "
													+ assignedTo);

											addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
											
											String currentCRM = dbUtil.getCurrentCrmName(null, null);

											if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
												
												workOrderCoreService.updateWorkOrderActivityInCRM(
														ticketId, null, null, null,
														null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);

											}
											
											log.info("In ROI Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);

										} else
										{
											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

											StatusVO status = new StatusVO();
											status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
											status.setTicketId(ticketId);
											status.setPropspectNo(roiMQWorkorderVO
													.getProspectNumber());

											log.info("In ROI Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);

											customerCoreService
													.updateTicket(status);
											// callLogger(ticketId,
											// TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
											setValue.clear();
											continue;

										}
									}

								} else
								{

									assignedTo = roiMQWorkorderVO
											.getAssignedTo();

									log.info("Assiging ROI NEW_INSTALLATION ticket "
											+ ticketId + "to " + assignedTo);

									ticketDAO
											.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
									
									workOrderCoreService.updateWorkOrderActivityInCRM(
											ticketId, null, null, null,
											null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);

									addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
								}

								// callCopper(roiMQWorkorderVO, assignedTo);
								// callLogger(ticketId);
							} else
							{
								log.info("ROI Work order could not be generated for New Installation "
										+ roiMQWorkorderVO
												.getWorkOrderNumber());
							}
						}
						// else if (ClassificationCategories.RE_ACTIVATION_CATEG
						// .equalsIgnoreCase(roiMQWorkorderVO
						// .getTicketCategory())
						// || ClassificationCategories.RE_ACTIVATION_SYMPTOM
						// .equalsIgnoreCase(roiMQWorkorderVO
						// .getSymptom()))
						else if (definitionCoreService
								.getClassificationCategory(DefinitionCoreService.RE_ACTIVATION_CATEG)
								.contains(roiMQWorkorderVO.getTicketCategory().toUpperCase())
								|| definitionCoreService
										.getClassificationCategory(DefinitionCoreService.RE_ACTIVATION_SYMPTOM)
										.contains(roiMQWorkorderVO
												.getSymptom().toUpperCase()))
						{

							// if (crmControllerHandler
							// .createWorkOrderEntry(roiMQWorkorderVO
							// .getProspectNumber(), roiMQWorkorderVO
							// .getMqId(), roiMQWorkorderVO
							// .getWorkOrderNumber(),
							// TicketCategory.RE_ACTIVATION_STRING))
							if (workOrderCreationStatus.get(ticketId))

							{
								if (roiMQWorkorderVO.getAssignedTo() == null
										|| roiMQWorkorderVO
												.getAssignedTo() <= 0)
								{

									String fxName = ticketIdAndFxNameMap
											.get(ticketId);

									String connectionType = ticketIdAndConnectionTypeMap
											.get(ticketId);
									UserVo userVo = null;

									if (TicketSubCategory.COPPER
											.equalsIgnoreCase(connectionType))
									{

										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

									} else if (TicketSubCategory.FIBER
											.equalsIgnoreCase(connectionType))
									{
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

									}

									log.info("Assigned to for ticketId for ROI "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();
										log.info("Assiging ROI RE_ACTIVATION_STRING ticket "
												+ ticketId + "to "
												+ assignedTo);
										addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
										
										workOrderCoreService.updateWorkOrderActivityInCRM(
												ticketId, null, null, null,
												null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);
									} else
									{

//										userVo = userDao
//												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF_STRICT);
										
										if (userVo != null)
										{

											assignedTo = userVo.getId();
											log.info("Assiging ROI RE_ACTIVATION_STRING ticket "
													+ ticketId + "to "
													+ assignedTo);
											addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

											workOrderCoreService.updateWorkOrderActivityInCRM(
													ticketId, null, null, null,
													null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);
											
										} else
										{
											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

											StatusVO status = new StatusVO();
											status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
											status.setTicketId(ticketId);
											status.setPropspectNo(roiMQWorkorderVO
													.getProspectNumber());

											customerCoreService
													.updateTicket(status);

											log.info("In ROI Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);
											// callLogger(ticketId,
											// TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
											setValue.clear();
											continue;
										}
									}

								} else
								{

									assignedTo = roiMQWorkorderVO
											.getAssignedTo();

									addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

									log.info("Assiging ROI RE_ACTIVATION_STRING ticket "
											+ ticketId + "to " + assignedTo);
									ticketDAO
											.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
									workOrderCoreService.updateWorkOrderActivityInCRM(
											ticketId, null, null, null,
											null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);
								}

								// callCopper(roiMQWorkorderVO, assignedTo);
								// callLogger(ticketId);
							} else
							{
								log.info("ROI Work order could not be generated for Reactivation "
										+ roiMQWorkorderVO
												.getWorkOrderNumber());
							}
						}
						// else if (ClassificationCategories.SHIFTING_CATEG
						// .equalsIgnoreCase(roiMQWorkorderVO
						// .getTicketCategory())
						// ||
						// ClassificationCategories.SHIFTING_CONNECTION_SYMPTOM
						// .equalsIgnoreCase(roiMQWorkorderVO
						// .getSymptom()))
						else if (definitionCoreService
								.getClassificationCategory(DefinitionCoreService.SHIFTING_CATEG)
								.contains(roiMQWorkorderVO.getTicketCategory().toUpperCase())
								|| definitionCoreService
										.getClassificationCategory(DefinitionCoreService.SHIFTING_CONNECTION_SYMPTOM)
										.contains(roiMQWorkorderVO
												.getSymptom().toUpperCase()))
						{

							// if (crmControllerHandler
							// .createWorkOrderEntry(roiMQWorkorderVO
							// .getProspectNumber(), roiMQWorkorderVO
							// .getMqId(), roiMQWorkorderVO
							// .getWorkOrderNumber(),
							// TicketCategory.SHIFTING_STR))
							if (workOrderCreationStatus.get(ticketId))
							{

								if (roiMQWorkorderVO.getAssignedTo() == null
										|| roiMQWorkorderVO
												.getAssignedTo() <= 0)
								{

									String fxName = ticketIdAndFxNameMap
											.get(ticketId);

									String connectionType = ticketIdAndConnectionTypeMap
											.get(ticketId);
									UserVo userVo = null;

									if (TicketSubCategory.COPPER
											.equalsIgnoreCase(connectionType))
									{

										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

									} else if (TicketSubCategory.FIBER
											.equalsIgnoreCase(connectionType))
									{
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

									}

									log.info("Assigned to for ticketId for ROI "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();
										log.info("Assiging ROI SHIFTING_CATEG ticket "
												+ ticketId + "to "
												+ assignedTo);
										addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
										workOrderCoreService.updateWorkOrderActivityInCRM(
												ticketId, null, null, null,
												null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);
									} else
									{

//										userVo = userDao
//												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF_STRICT);
										
										if (userVo != null)
										{

											assignedTo = userVo.getId();
											log.info("Assiging ROI SHIFTING_CATEG ticket "
													+ ticketId + "to "
													+ assignedTo);
											addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
											workOrderCoreService.updateWorkOrderActivityInCRM(
													ticketId, null, null, null,
													null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);

										} else
										{

											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
											StatusVO status = new StatusVO();
											status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
											status.setTicketId(ticketId);
											status.setPropspectNo(roiMQWorkorderVO
													.getProspectNumber());

											customerCoreService
													.updateTicket(status);

											log.info("In ROI Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);
											// callLogger(ticketId,
											// TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
											setValue.clear();
											continue;
										}
									}

								} else
								{

									assignedTo = roiMQWorkorderVO
											.getAssignedTo();
									log.info("Assiging ROI SHIFTING_CATEG ticket "
											+ ticketId + "to " + assignedTo);

									addTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
									ticketDAO
											.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
									workOrderCoreService.updateWorkOrderActivityInCRM(
											ticketId, null, null, null,
											null, null, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,null);
								}

								// callCopper(roiMQWorkorderVO, assignedTo);
								// callLogger(ticketId);
							} else
							{
								log.info("ROI Work order could not be generated for Shifting "
										+ roiMQWorkorderVO
												.getWorkOrderNumber());
							}
						}

						else if (ClassificationCategories.FR_CATEG
								.equalsIgnoreCase(roiMQWorkorderVO
										.getTicketCategory().toUpperCase())
								|| FrTicketUtil
										.isTicketHavingFrSymptom(roiMQWorkorderVO
												.getSymptom().toUpperCase())
										&& isFREnable)
						{

							String fxName = ticketIdAndFxNameMap.get(ticketId);
							if (crmControllerHandler
									.createFrDetailsEntry(roiMQWorkorderVO, fxName))
							{

								if (roiMQWorkorderVO.getAssignedTo() == null
										|| roiMQWorkorderVO
												.getAssignedTo() <= 0)
								{

									Long flowId = FrTicketUtil
											.findFlowIdforMatchingSymptomName(roiMQWorkorderVO
													.getSymptom());

									UserVo userVo = userDao
											.getUsersByDeviceName(fxName, AuthUtils
													.getUserRolesByFlowId(flowId));

									log.info("Assigned to for ticketId for ROI "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();
										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

										log.info("In ROI Found User for Ticket Id "
												+ ticketId + " assignedTo "
												+ assignedTo);
										this.addFrTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo);
									} else
									{
										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
										StatusVO status = new StatusVO();
										status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
										status.setTicketId(ticketId);
										status.setPropspectNo(roiMQWorkorderVO
												.getProspectNumber());

										customerCoreService
												.updateTicket(status);

										log.info("In ROI Found User for Ticket Id "
												+ ticketId + " assignedTo "
												+ assignedTo);
										setValue.clear();
										setValue.clear();
										continue;
									}

								} else
								{
									assignedTo = roiMQWorkorderVO
											.getAssignedTo();
									ticketDAO
											.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

									log.info("In ROI Found User for Ticket Id "
											+ ticketId + " assignedTo "
											+ assignedTo);
									this.addFrTicket2MainQueue(roiMQWorkorderVO, setValue, assignedTo);
								}
							} else
							{
								log.info("ROI Work order could not be generated for FR "
										+ roiMQWorkorderVO
												.getWorkOrderNumber());
							}

						} else
						{
							log.info("Can't find ROI Ticket category : "
									+ roiMQWorkorderVO.getTicketCategory()
									+ " having symptom : "
									+ roiMQWorkorderVO.getSymptom());
						}

						// callCopper(roiMQWorkorderVO, assignedTo);
						// callLogger(ticketId);
						setValue.clear();
					} catch (Exception e)
					{
						log.error("Error while iterating mq ROI result for "
								+ roiMQWorkorderVO.getWorkOrderNumber());
						continue;
					}
				}
				

			} else
			{
				log.info("No record found from CRM ROI");
				return ResponseUtil.createRecordNotFoundResponse();
			}

			log.debug("Global Activities got all the values for area ROI ");
			
			return ResponseUtil.createSuccessResponse();

		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error in CRM Job of ROI " + e.getMessage());
			return ResponseUtil.createDBExceptionResponse();
		}

	}

	private void addFrTicket2MainQueue(MQWorkorderVO roiMQWorkorderVO,

			LinkedHashSet<Long> setValue, long assignedTo)
	{
		long reportTo = 0l;
		reportTo = userDao.getReportToUser(assignedTo);
		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

		classificationEngineCoreServiceImpl
				.addFrTicket2MainQueue(neKey, setValue);
		if (reportTo == 0l)
		{
			log.info("No Report to found for user " + assignedTo);
			return;
		}
		String tlKey = dbUtil.getKeyForQueueByUserId(reportTo);
		log.info("Adding ne ticket in Tl queue for ROI with TL key " + tlKey);
		classificationEngineCoreServiceImpl
				.addFrTicket2MainQueue(tlKey, setValue);

		setValue.clear();
	}

	private void addTicket2MainQueue(MQWorkorderVO roiMQWorkorderVO,

			LinkedHashSet<Long> setValue, long assignedTo, long ticketId,
			String ticketCategory)
	{

		long reportTo = 0l;

		reportTo = userDao.getReportToUser(assignedTo);

		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

		classificationEngineCoreServiceImpl
				.addTicket2MainQueue(neKey, setValue);

		if (reportTo == 0)
		{
			log.info("No Report to found for user " + assignedTo);
			return;
		}

		String tlKey = dbUtil.getKeyForQueueByUserId(reportTo);

		log.info("Adding ne queue for ROI" + neKey + " TL key " + tlKey);

		classificationEngineCoreServiceImpl
				.addTicket2MainQueue(tlKey, setValue);

		// callCopper(roiMQWorkorderVO, _wfRequestList, assignedTo);
		callLogger(ticketId, ticketCategory);
		setValue.clear();
	}

	private void callLogger(long ticketId, String ticketCategory)
	{
		TicketUpdateGateway
				.logTicket(new TicketLogImpl(ticketId, ticketCategory, FWMPConstant.SYSTEM_ENGINE));

	}

}
