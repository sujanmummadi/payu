/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.domain.engines.classification.controllers.ClassificationController;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ClassificationEngineCoreService;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.MainQueueVo;

/**
 * @author aditya
 *
 */
@Lazy(false)
public class GlobalCoreServiceImpl implements GlobalCoreService
{
	private static Logger log = LogManager
			.getLogger(GlobalCoreServiceImpl.class);

	private static boolean HTTP_CALL_QUEUE_MERGE = false;

	@Autowired
	private GlobalActivities globalActivities;
	@Autowired
	ClassificationController classificationController;

	@Autowired
	DBUtil dbUtil;
	@Autowired
	UserDao userDao;
	@Autowired
	ClassificationEngineCoreService classificationEngineCoreServiceImpl;

	private String mergeQueueUrl;
	private String servers;
	private String mergeFRQueueUrl;
	private String frServers;

	public String getMergeFRQueueUrl()
	{
		return mergeFRQueueUrl;
	}

	public void setMergeFRQueueUrl(String mergeFRQueueUrl)
	{
		this.mergeFRQueueUrl = mergeFRQueueUrl;
	}

	public String getFrServers()
	{
		return frServers;
	}

	public void setFrServers(String frServers)
	{
		this.frServers = frServers;
	}

	public String getMergeQueueUrl()
	{
		return mergeQueueUrl;
	}

	public void setMergeQueueUrl(String mergeQueueUrl)
	{
		this.mergeQueueUrl = mergeQueueUrl;
	}

	public String getServers()
	{
		return servers;
	}

	public void setServers(String servers)
	{
		this.servers = servers;
	}

	@Override
	public APIResponse mergeMainQueue(
			Map<String, LinkedHashSet<Long>> mainQueue)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(globalActivities.mergeMainQueue(mainQueue));
	}

	private APIResponse addToCacheServiceCall(String serverIp)
	{
		APIResponse apiResponse = null;
		log.debug("Merge main queue  cache Original URI ############### "
				+ mergeQueueUrl);

		String resetMainQueueUrlWithIp = mergeQueueUrl
				.replace("localhost", serverIp);

		log.info("Merge main queue  cache modified URI ############### "
				+ resetMainQueueUrlWithIp);

		MainQueueVo mainQueueVo = new MainQueueVo();
		Map<String, LinkedHashSet<Long>> mainQueue = globalActivities
				.getMainQueue();

		mainQueueVo.setMainQueue(mainQueue);

		log.debug("Merge main queue " + mainQueue.size());

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			if (!HTTP_CALL_QUEUE_MERGE)
			{
				HTTP_CALL_QUEUE_MERGE = true;
				ObjectMapper mapper = new ObjectMapper();

				httpclient = HttpClients.createDefault();

				HttpPost httpPost = new HttpPost(resetMainQueueUrlWithIp);

				String mainQueueString = mapper.writeValueAsString(mainQueueVo);

				StringEntity stringEntity = new StringEntity(mainQueueString, "UTF-8");

				stringEntity.setContentType("application/json");

				httpPost.setEntity(stringEntity);

				httpResponse = httpclient.execute(httpPost);

				String stringResponse = EntityUtils
						.toString(httpResponse.getEntity());

				log.debug("Merging main queue response " + stringResponse);

				apiResponse = ResponseUtil.createSuccessResponse()
						.setData(stringResponse);

				httpclient.close();
			}
		} catch (Exception e)
		{
			log.error("Error while merging main queue " + e.getMessage(), e);
			e.printStackTrace();
		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();

				if (httpclient != null)
					httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse merging main queue. "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

		HTTP_CALL_QUEUE_MERGE = false;

		log.info("Merge main queue final "
				+ globalActivities.getMainQueue().size());

		return apiResponse;

	}

	@Override
	@Scheduled(cron = "${fwmp.merge.queue.cron.trigger}")
	public void mergeMainQueueJob()
	{
		String serv = servers;

		String[] serverArr = serv.split(",");

		for (int i = 0; i < serverArr.length; i++)
		{
			String serverIp = serverArr[i];
			try
			{
				log.info("server while merging queue " + serverIp);
				addToCacheServiceCall(serverIp);

			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("server got error while merging queue " + serverIp
						+ " . " + e.getMessage());
				continue;
			}
		}

	}

	@Override
	public APIResponse resetQueue()
	{
		if (globalActivities.clearMainQueue())
		{
			classificationController.getAllOpenWorkOrderTicket();
			classificationController.getAllOpenProspectTicket();

			if (globalActivities.getMainQueue().isEmpty())
			{
				return ResponseUtil.createAssignFailureResponse()
						.setData("Queue Cleaning failed.. Please retry...!");

			} else
			{
				return ResponseUtil.createSuccessResponse()
						.setData("Queue Reset Done.");
			}

		} else
		{
			return ResponseUtil.createAssignFailureResponse()
					.setData("Queue Cleaaning failed.. Please retry...!");
		}
	}

	@Override
	@Scheduled(cron = "${fwmp.clean.queue.cron.trigger}")
	public void cleanQueue()
	{
		if (globalActivities.clearMainQueue())
		{
			log.info("Queue Reset Done.");

		} else
		{
			log.info("Queue Cleaaning failed.. Please retry...!");
		}
	}

	@Scheduled(cron = "${fwmp.user.reset.job.cron.trigger}")
	public void resetCachedUsersByJob()
	{
		log.info("Reset Cached Users Success by cron:::");
		// userDao.resetCachedUsers();
		userDao.modifiedResetUser();
	}

	/*
	 * @Override public boolean isTicketAvailable(Long ticketId, Long userId) {
	 * Map<String, LinkedHashSet<Long>> mainQueue = globalActivities
	 * .getMainQueue();
	 * 
	 * String key = dbUtil.getKeyForQueueByUserId(userId);
	 * 
	 * if (mainQueue.containsKey(key)) { LinkedHashSet<Long> ticketList =
	 * mainQueue.get(key);
	 * 
	 * if (ticketList.contains(ticketId)) return true; else return false; }
	 * return false; }
	 */

	@Override
	public APIResponse resetFrQueue()
	{
		if (globalActivities.clearFrMainQueue())
		{
			classificationController.preparFrQueueForUsers();

			if (globalActivities.getFrMainQueue().isEmpty())
			{
				return ResponseUtil.createAssignFailureResponse()
						.setData("Queue Cleaning failed.. Please retry...!");

			} else
			{
				return ResponseUtil.createSuccessResponse()
						.setData("Queue Reset Done.");
			}

		} else
		{
			return ResponseUtil.createAssignFailureResponse()
					.setData("Queue Cleaaning failed.. Please retry...!");
		}
	}

	@Override
	public APIResponse mergeFrMainQueue(
			Map<String, LinkedHashSet<Long>> mainFRQueue)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(globalActivities.mergeFrMainQueue(mainFRQueue));
	}

	@Override
	@Scheduled(cron = "${fwmp.merge.fr.queue.cron.trigger}")
	public void mergeFrMainQueueJob()
	{
		log.info("FR Queue merge started ");

		String serv = frServers;

		String[] serverArr = serv.split(",");

		for (int i = 0; i < serverArr.length; i++)
		{
			String serverIp = serverArr[i];
			try
			{
				log.info("server while merging FR  queue " + serverIp);
				addToCacheFRServiceCall(serverIp);

			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("server got error while merging FR queue " + serverIp
						+ " . " + e.getMessage());
				continue;
			}
		}

	}

	/**@author kiran
	 * void
	 * @param serverIp
	 */
	private APIResponse addToCacheFRServiceCall(String serverIp)
	{

		APIResponse apiResponse = null;
		log.debug("Merge FR main queue  cache Original URI ############### "
				+ mergeFRQueueUrl);

		String resetFRMainQueueUrlWithIp = mergeFRQueueUrl
				.replace("localhost", serverIp);

		log.info("Merge FR main queue  cache modified URI ############### "
				+ resetFRMainQueueUrlWithIp);

		MainQueueVo mainFRQueueVo = new MainQueueVo();
		Map<String, LinkedHashSet<Long>> frMainQueue = globalActivities
				.getFrMainQueue();

		mainFRQueueVo.setMainFrQueue(frMainQueue);

		log.debug("Merge FR  main queue " + frMainQueue.size());

		CloseableHttpResponse httpResponse = null;

		CloseableHttpClient httpclient = null;

		try
		{
			ObjectMapper mapper = new ObjectMapper();

			httpclient = HttpClients.createDefault();

			HttpPost httpPost = new HttpPost(resetFRMainQueueUrlWithIp);

			String mainFRQueueString = mapper.writeValueAsString(mainFRQueueVo);

			StringEntity stringEntity = new StringEntity(mainFRQueueString, "UTF-8");

			stringEntity.setContentType("application/json");

			httpPost.setEntity(stringEntity);

			httpResponse = httpclient.execute(httpPost);

			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			log.debug("Merging FR main queue response " + stringResponse);

			apiResponse = ResponseUtil.createSuccessResponse()
					.setData(stringResponse);

			httpclient.close();

		} catch (Exception e)
		{
			log.error("Error while merging FR main queue " + e.getMessage());

		} finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				if (httpclient != null)
					httpclient.close();

			} catch (IOException e)
			{
				log.error("Error in finally block, while closing httpresponse merging FR main queue. "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

		log.info("Merge FR main queue final "
				+ globalActivities.getFrMainQueue().size());

		return apiResponse;

	}
	
	@Override
	public void addTicket2MainQueue( LinkedHashSet<Long> setValue, long assignedTo )
	{

		long reportTo = 0l;

		reportTo = userDao.getReportToUser(assignedTo);

		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);
	
		classificationEngineCoreServiceImpl
				.addTicket2MainQueue(neKey, setValue);

		if (reportTo == 0)
		{
			log.info("No Report to found for user " + assignedTo);
			return;
		}

		String tlKey = dbUtil.getKeyForQueueByUserId(reportTo);

		log.info("Adding tickets + " + setValue + " for HYD" + neKey
				+ " TL key " + tlKey);

		classificationEngineCoreServiceImpl
				.addTicket2MainQueue(tlKey, setValue);

		setValue.clear();


	}
	
	@Override
	public void addFrTicket2MainQueue(LinkedHashSet<Long> setValue, long assignedTo) {

		long reportTo = 0l;
		reportTo = userDao.getReportToUser(assignedTo);
		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

		classificationEngineCoreServiceImpl.addFrTicket2MainQueue(neKey, setValue);

		log.info("Adding ticket FR " + setValue + "in NE queue for HYD with TL key " + neKey);

		if (reportTo == 0l) {
			log.info("No Report to found for user " + assignedTo);
			return;
		}

		String tlKey = dbUtil.getKeyForQueueByUserId(reportTo);

		log.info("Adding ne ticket FR" + setValue + "in Tl queue for HYD with TL key " + tlKey);

		classificationEngineCoreServiceImpl.addFrTicket2MainQueue(tlKey, setValue);

		setValue.clear();
	}

}
