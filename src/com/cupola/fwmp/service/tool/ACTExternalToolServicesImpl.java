/**
* @Author aditya  28-Dec-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.service.tool;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.tool.ACTExternalDAO;
import com.cupola.fwmp.dao.user.UserDaoImpl;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.tools.dashboard.PerformaceStasInput;
import com.cupola.fwmp.vo.tools.dashboard.deployment.DeplPerformaceStasOutput;
import com.cupola.fwmp.vo.tools.dashboard.deployment.DeplPerformaceStasOutputVo;
import com.cupola.fwmp.vo.tools.dashboard.fr.FrPerformaceStasOutput;
import com.cupola.fwmp.vo.tools.dashboard.fr.FrPerformaceStasOutputVo;
import com.cupola.fwmp.vo.tools.dashboard.sales.SalesPerformaceStasOutput;
import com.cupola.fwmp.vo.tools.dashboard.sales.SalesPerformaceStasOutputVo;

/**
 * @Author aditya 28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ACTExternalToolServicesImpl implements ACTExternalToolServices {

	private static Logger log = LogManager.getLogger(ACTExternalToolServicesImpl.class.getName());

	@Autowired
	ACTExternalDAO actExternalDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.ACTExternalToolServices#getPerformaceStas(com.
	 * cupola.fwmp.vo.tools.PerformaceStasInput)
	 */
	@Override
	public APIResponse getFrPerformaceStas(PerformaceStasInput performaceStasInput) {

		FrPerformaceStasOutputVo outputVo = new FrPerformaceStasOutputVo();

		Map<String, FrPerformaceStasOutput> userAndReportingSubordinateMap = new ConcurrentHashMap<>();
		Map<String, Set<String>> userAndReportingSubordinateIdMap = new ConcurrentHashMap<>();

		List<FrPerformaceStasOutput> performaceStasOutputFromDB = actExternalDAO
				.getAllFrPerformaceStats(performaceStasInput);

		log.info("Got performance details for fr " + performaceStasInput + " and details are "
				+ performaceStasOutputFromDB.size());

		log.info("Preparing performance details for fr " + performaceStasInput + " with details "
				+ performaceStasOutputFromDB.size());

		if (performaceStasInput.getCurrentRole() == null || performaceStasInput.getCurrentRole().isEmpty()) {

			for (Iterator<FrPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				FrPerformaceStasOutput performaceStasOutput = (FrPerformaceStasOutput) iterator.next();

				if (AuthUtils.getIfExecutiveRole() == UserRole.FR_CM) {

					// map of all the bm reporting to cm 4
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getCmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getCmId())
								.addAll(Arrays.asList(performaceStasOutput.getBmId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getBmId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getCmId(), strings);
					}

					// all cm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getCmId())) {
						FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getCmId());
						FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
					} else {

						FrPerformaceStasOutput output = new FrPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.FR_CM + "");
						if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
					}

					// all bm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
						FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getBmId());
						FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					} else {

						FrPerformaceStasOutput output = new FrPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.FR_BM + "");
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					}

				}

				else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_BM) {

					// map of all the Am reporting to Bm 3
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getBmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getBmId())
								.addAll(Arrays.asList(performaceStasOutput.getAmId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getAmId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getBmId(), strings);
					}

					// all bm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
						FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getBmId());
						FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					} else {

						FrPerformaceStasOutput output = new FrPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.FR_BM + "");
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					}

					// all Am map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
						FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getAmId());
						FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					} else {

						FrPerformaceStasOutput output = new FrPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.FR_AM + "");
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					}

				}

				else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_AM) {

					// map of all the tl reporting to Am 2
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getAmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getAmId())
								.addAll(Arrays.asList(performaceStasOutput.getTlId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getTlId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getAmId(), strings);
					}

					// all Am map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
						FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getAmId());
						FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					} else {

						FrPerformaceStasOutput output = new FrPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.FR_AM + "");
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					}

					// all Tl map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
						FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getTlId());
						FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					} else {

						FrPerformaceStasOutput output = new FrPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.FR_TL + "");
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					}
				}

				else if (AuthUtils.isFRTLUser()) {

					// map of all the executive reporting to TL 1
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getTlId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getTlId())
								.addAll(Arrays.asList(performaceStasOutput.getLoginid()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getLoginid());
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getTlId(), strings);
					}

					// all Tl map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
						FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getTlId());
						FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					} else {

						FrPerformaceStasOutput output = new FrPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.FR_TL + "");
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					}
					// all executive map
					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_NE + "");
					if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
				} else {

					// all executive map
					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_NE + "");
					if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_CM) {

			for (Iterator<FrPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				FrPerformaceStasOutput performaceStasOutput = (FrPerformaceStasOutput) iterator.next();

				// map of all the bm reporting to cm 4
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getCmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getCmId())
							.addAll(Arrays.asList(performaceStasOutput.getBmId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getBmId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getCmId(), strings);
				}

				// all cm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getCmId())) {
					FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getCmId());
					FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
				} else {

					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_CM + "");
					if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
				}

				// all bm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
					FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getBmId());
					FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				} else {

					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_BM + "");
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_BM) {

			for (Iterator<FrPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {
				FrPerformaceStasOutput performaceStasOutput = (FrPerformaceStasOutput) iterator.next();

				// map of all the Am reporting to Bm 3
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getBmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getBmId())
							.addAll(Arrays.asList(performaceStasOutput.getAmId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getAmId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getBmId(), strings);
				}

				// all bm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
					FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getBmId());
					FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				} else {

					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_BM + "");
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				}

				// all Am map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
					FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getAmId());
					FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				} else {

					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_AM + "");
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_AM) {

			for (Iterator<FrPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {
				FrPerformaceStasOutput performaceStasOutput = (FrPerformaceStasOutput) iterator.next();

				// map of all the tl reporting to Am 2
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getAmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getAmId())
							.addAll(Arrays.asList(performaceStasOutput.getTlId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getTlId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getAmId(), strings);
				}

				// all Am map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
					FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getAmId());
					FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				} else {

					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_AM + "");
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				}

				// all Tl map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
					FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getTlId());
					FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				} else {

					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_TL + "");
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_TL) {

			for (Iterator<FrPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				FrPerformaceStasOutput performaceStasOutput = (FrPerformaceStasOutput) iterator.next();

				// map of all the executive reporting to TL 1
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getTlId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getTlId())
							.addAll(Arrays.asList(performaceStasOutput.getLoginid()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getLoginid());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getTlId(), strings);
				}

				// all Tl map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
					FrPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getTlId());
					FrPerformaceStasOutput output = getModifiedFrOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				} else {

					FrPerformaceStasOutput output = new FrPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.FR_TL + "");
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				}
				// all executive map
				FrPerformaceStasOutput output = new FrPerformaceStasOutput();
				BeanUtils.copyProperties(performaceStasOutput, output);
				output.setCssClass(UserRole.FR_NE + "");
				if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
					UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
					if( userVo != null) {
						performaceStasOutput.setFirstName(userVo.getFirstName());
						performaceStasOutput.setLastName(userVo.getLastName());
					}
				}
				userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_NE) {

			for (Iterator<FrPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				FrPerformaceStasOutput performaceStasOutput = (FrPerformaceStasOutput) iterator.next();
				// all executive map
				FrPerformaceStasOutput output = new FrPerformaceStasOutput();
				BeanUtils.copyProperties(performaceStasOutput, output);
				output.setCssClass(UserRole.FR_NE + "");
				if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
					UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
					if( userVo != null) {
						performaceStasOutput.setFirstName(userVo.getFirstName());
						performaceStasOutput.setLastName(userVo.getLastName());
					}
				}
				userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
			}

		}
		outputVo.setUserAndReportingSubordinateMap(userAndReportingSubordinateMap);
		outputVo.setUserAndReportingSubordinateIdMap(userAndReportingSubordinateIdMap);

		log.info("Prepared performance details for fr " + performaceStasInput + " with details "
				+ performaceStasOutputFromDB.size() + " and " + "details are " + userAndReportingSubordinateMap.size()
				+ " where as subordinate maps are " + userAndReportingSubordinateIdMap.size());
		return ResponseUtil.createSuccessResponse().setData(outputVo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.ACTExternalToolServices#getSdPerformaceStas(com.
	 * cupola.fwmp.vo.tools.PerformaceStasInput)
	 */
	@Override
	public APIResponse getSdPerformaceStas(PerformaceStasInput performaceStasInput) {

		DeplPerformaceStasOutputVo outputVo = new DeplPerformaceStasOutputVo();

		Map<String, DeplPerformaceStasOutput> userAndReportingSubordinateMap = new ConcurrentHashMap<>();
		Map<String, Set<String>> userAndReportingSubordinateIdMap = new ConcurrentHashMap<>();

		List<DeplPerformaceStasOutput> performaceStasOutputFromDB = actExternalDAO
				.getAllSdPerformaceStats(performaceStasInput);

		log.info("Got performance details for deployment " + performaceStasInput + " and details are "
				+ performaceStasOutputFromDB.size());

		log.info("Preparing performance details for deployment " + performaceStasInput + " with details "
				+ performaceStasOutputFromDB.size());

		if (performaceStasInput.getCurrentRole() == null || performaceStasInput.getCurrentRole().isEmpty()) {

			for (Iterator<DeplPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				DeplPerformaceStasOutput performaceStasOutput = (DeplPerformaceStasOutput) iterator.next();

				if (AuthUtils.getIfExecutiveRole() == UserRole.NI_CM) {

					// map of all the bm reporting to cm 4
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getCmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getCmId())
								.addAll(Arrays.asList(performaceStasOutput.getBmId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getBmId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getCmId(), strings);
					}

					// all cm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getCmId())) {
						DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getCmId());
						DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
					} else {

						DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.NI_CM + "");
						if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
					}

					// all bm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
						DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getBmId());
						DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					} else {

						DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.NI_BM + "");
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					}

				}

				else if (AuthUtils.getIfExecutiveRole() == UserRole.NI_BM) {

					// map of all the Am reporting to Bm 3
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getBmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getBmId())
								.addAll(Arrays.asList(performaceStasOutput.getAmId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getAmId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getBmId(), strings);
					}

					// all bm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
						DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getBmId());
						DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					} else {

						DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.NI_BM + "");
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					}

					// all Am map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
						DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getAmId());
						DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					} else {

						DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.NI_AM + "");
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					}

				}

				else if (AuthUtils.getIfExecutiveRole() == UserRole.NI_AM) {

					// map of all the tl reporting to Am 2
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getAmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getAmId())
								.addAll(Arrays.asList(performaceStasOutput.getTlId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getTlId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getAmId(), strings);
					}

					// all Am map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
						DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getAmId());
						DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					} else {

						DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.NI_AM + "");
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					}

					// all Tl map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
						DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getTlId());
						DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					} else {

						DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.NI_TL + "");
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					}
				}

				else if (AuthUtils.isNITLUser()) {

					// map of all the executive reporting to TL 1
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getTlId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getTlId())
								.addAll(Arrays.asList(performaceStasOutput.getLoginid()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getLoginid());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getTlId(), strings);
					}

					// all Tl map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
						DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getTlId());
						DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					} else {

						DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.NI_TL + "");
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								performaceStasOutput.setFirstName(userVo.getFirstName());
								performaceStasOutput.setLastName(userVo.getLastName());
							}
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					}
					// all executive map
					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_NE + "");
					if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
				} else {

					// all executive map
					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_NE + "");
					if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_CM) {

			for (Iterator<DeplPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				DeplPerformaceStasOutput performaceStasOutput = (DeplPerformaceStasOutput) iterator.next();

				// map of all the bm reporting to cm 4
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getCmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getCmId())
							.addAll(Arrays.asList(performaceStasOutput.getBmId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getBmId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getCmId(), strings);
				}

				// all cm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getCmId())) {
					DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getCmId());
					DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
				} else {

					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_CM + "");
					if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
				}

				// all bm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
					DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getBmId());
					DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				} else {

					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_BM + "");
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_BM) {

			for (Iterator<DeplPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {
				DeplPerformaceStasOutput performaceStasOutput = (DeplPerformaceStasOutput) iterator.next();

				// map of all the Am reporting to Bm 3
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getBmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getBmId())
							.addAll(Arrays.asList(performaceStasOutput.getAmId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getAmId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getBmId(), strings);
				}

				// all bm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
					DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getBmId());
					DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				} else {

					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_BM + "");
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				}

				// all Am map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
					DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getAmId());
					DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				} else {

					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_AM + "");
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_AM) {

			for (Iterator<DeplPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {
				DeplPerformaceStasOutput performaceStasOutput = (DeplPerformaceStasOutput) iterator.next();

				// map of all the tl reporting to Am 2
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getAmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getAmId())
							.addAll(Arrays.asList(performaceStasOutput.getTlId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getTlId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getAmId(), strings);
				}

				// all Am map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
					DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getAmId());
					DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				} else {

					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_AM + "");
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				}

				// all Tl map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
					DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getTlId());
					DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				} else {

					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_TL + "");
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_TL) {

			for (Iterator<DeplPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				DeplPerformaceStasOutput performaceStasOutput = (DeplPerformaceStasOutput) iterator.next();

				// map of all the executive reporting to TL 1
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getTlId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getTlId())
							.addAll(Arrays.asList(performaceStasOutput.getLoginid()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getLoginid());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getTlId(), strings);
				}

				// all Tl map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
					DeplPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getTlId());
					DeplPerformaceStasOutput output = getModifiedSdOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				} else {

					DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.NI_TL + "");
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							performaceStasOutput.setFirstName(userVo.getFirstName());
							performaceStasOutput.setLastName(userVo.getLastName());
						}
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				}
				// all executive map
				DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
				BeanUtils.copyProperties(performaceStasOutput, output);
				output.setCssClass(UserRole.NI_NE + "");
				if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
					UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
					if( userVo != null) {
						performaceStasOutput.setFirstName(userVo.getFirstName());
						performaceStasOutput.setLastName(userVo.getLastName());
					}
				}
				userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_NE) {

			for (Iterator<DeplPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				DeplPerformaceStasOutput performaceStasOutput = (DeplPerformaceStasOutput) iterator.next();
				// all executive map
				DeplPerformaceStasOutput output = new DeplPerformaceStasOutput();
				BeanUtils.copyProperties(performaceStasOutput, output);
				output.setCssClass(UserRole.NI_NE + "");
				if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
					UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
					if( userVo != null) {
						performaceStasOutput.setFirstName(userVo.getFirstName());
						performaceStasOutput.setLastName(userVo.getLastName());
					}
				}
				
				userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
			}

		}
		outputVo.setUserAndReportingSubordinateMap(userAndReportingSubordinateMap);
		outputVo.setUserAndReportingSubordinateIdMap(userAndReportingSubordinateIdMap);

		log.info("Prepared performance details for deployment " + performaceStasInput + " with details "
				+ performaceStasOutputFromDB.size() + " and " + "details are " + userAndReportingSubordinateMap.size()
				+ " where as subordinate maps are " + userAndReportingSubordinateIdMap.size());
		return ResponseUtil.createSuccessResponse().setData(outputVo);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.tool.ACTExternalToolServices#getSalesPerformaceStas(
	 * com.cupola.fwmp.vo.tools.PerformaceStasInput)
	 */
	@Override
	public APIResponse getSalesPerformaceStas(PerformaceStasInput performaceStasInput) {

		SalesPerformaceStasOutputVo outputVo = new SalesPerformaceStasOutputVo();

		Map<String, SalesPerformaceStasOutput> userAndReportingSubordinateMap = new ConcurrentHashMap<>();
		Map<String, Set<String>> userAndReportingSubordinateIdMap = new ConcurrentHashMap<>();

		List<SalesPerformaceStasOutput> performaceStasOutputFromDB = actExternalDAO
				.getAllSalesPerformaceStats(performaceStasInput);

		log.info("Got performance details for sales " + performaceStasInput + " and details are "
				+ performaceStasOutputFromDB.size());

		log.info("Preparing performance details for sales " + performaceStasInput + " with details "
				+ performaceStasOutputFromDB.size());

		if (performaceStasInput.getCurrentRole() == null || performaceStasInput.getCurrentRole().isEmpty()) {

			for (Iterator<SalesPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				SalesPerformaceStasOutput performaceStasOutput = (SalesPerformaceStasOutput) iterator.next();

				if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_CM) {

					// map of all the bm reporting to cm 4
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getCmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getCmId())
								.addAll(Arrays.asList(performaceStasOutput.getBmId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getBmId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getCmId(), strings);
					}

					// all cm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getCmId())) {
						SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getCmId());
						SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
					} else {

						SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.SALES_CM + "");
						if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
					}

					// all bm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
						SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getBmId());
						SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					} else {

						SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.SALES_BM + "");
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					}

				}

				else if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_BM) {

					// map of all the Am reporting to Bm 3
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getBmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getBmId())
								.addAll(Arrays.asList(performaceStasOutput.getAmId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getAmId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getBmId(), strings);
					}

					// all bm map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
						SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getBmId());
						SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					} else {

						SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.SALES_BM + "");
						if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
					}

					// all Am map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
						SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getAmId());
						SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					} else {

						SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.SALES_AM + "");
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					}

				}

				else if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_AM) {

					// map of all the tl reporting to Am 2
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getAmId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getAmId())
								.addAll(Arrays.asList(performaceStasOutput.getTlId()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getTlId());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getAmId(), strings);
					}

					// all Am map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
						SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getAmId());
						SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					} else {

						SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.SALES_AM + "");
						if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
					}

					// all Tl map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
						SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getTlId());
						SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					} else {

						SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.SALES_TL + "");
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					}
				}

				else if (AuthUtils.isSalesTlUser()) {

					// map of all the executive reporting to TL 1
					if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getTlId()))
						userAndReportingSubordinateIdMap.get(performaceStasOutput.getTlId())
								.addAll(Arrays.asList(performaceStasOutput.getLoginid()));
					else {
						Set<String> strings = new LinkedHashSet<>();
						strings.add(performaceStasOutput.getLoginid());
						userAndReportingSubordinateIdMap.put(performaceStasOutput.getTlId(), strings);
					}

					// all Tl map
					if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
						SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
								.get(performaceStasOutput.getTlId());
						SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}
						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					} else {

						SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
						BeanUtils.copyProperties(performaceStasOutput, output);
						output.setCssClass(UserRole.SALES_TL + "");
						if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
							//Performance Dashboard showing FirstName and LastName Fix Start 
							UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
							if( userVo != null) {
								output.setFirstName(userVo.getFirstName());
								output.setLastName(userVo.getLastName());
							}
							//Performance Dashboard showing FirstName and LastName Fix End
						}

						userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
					}
					// all executive map
					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_EX + "");
					if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
				} else {

					// all executive map
					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_EX + "");
					if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_CM) {

			for (Iterator<SalesPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				SalesPerformaceStasOutput performaceStasOutput = (SalesPerformaceStasOutput) iterator.next();

				// map of all the bm reporting to cm 4
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getCmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getCmId())
							.addAll(Arrays.asList(performaceStasOutput.getBmId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getBmId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getCmId(), strings);
				}

				// all cm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getCmId())) {
					SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getCmId());
					SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
				} else {

					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_CM + "");
					if( performaceStasOutput.getCmId() != null && !performaceStasOutput.getCmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getCmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getCmId(), output);
				}

				// all bm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
					SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getBmId());
					SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				} else {

					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_BM + "");
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_BM) {

			for (Iterator<SalesPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {
				SalesPerformaceStasOutput performaceStasOutput = (SalesPerformaceStasOutput) iterator.next();

				// map of all the Am reporting to Bm 3
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getBmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getBmId())
							.addAll(Arrays.asList(performaceStasOutput.getAmId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getAmId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getBmId(), strings);
				}

				// all bm map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getBmId())) {
					SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getBmId());
					SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				} else {

					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_BM + "");
					if( performaceStasOutput.getBmId() != null && !performaceStasOutput.getBmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getBmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getBmId(), output);
				}

				// all Am map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
					SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getAmId());
					SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				} else {

					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_AM + "");
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_AM) {

			for (Iterator<SalesPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {
				SalesPerformaceStasOutput performaceStasOutput = (SalesPerformaceStasOutput) iterator.next();

				// map of all the tl reporting to Am 2
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getAmId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getAmId())
							.addAll(Arrays.asList(performaceStasOutput.getTlId()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getTlId());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getAmId(), strings);
				}

				// all Am map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getAmId())) {
					SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getAmId());
					SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				} else {

					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_AM + "");
					if( performaceStasOutput.getAmId() != null && !performaceStasOutput.getAmId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getAmId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getAmId(), output);
				}

				// all Tl map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
					SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getTlId());
					SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				} else {

					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_TL + "");
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				}

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_TL) {

			for (Iterator<SalesPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				SalesPerformaceStasOutput performaceStasOutput = (SalesPerformaceStasOutput) iterator.next();

				// map of all the executive reporting to TL 1
				if (userAndReportingSubordinateIdMap.containsKey(performaceStasOutput.getTlId()))
					userAndReportingSubordinateIdMap.get(performaceStasOutput.getTlId())
							.addAll(Arrays.asList(performaceStasOutput.getLoginid()));
				else {
					Set<String> strings = new LinkedHashSet<>();
					strings.add(performaceStasOutput.getLoginid());
					userAndReportingSubordinateIdMap.put(performaceStasOutput.getTlId(), strings);
				}

				// all Tl map
				if (userAndReportingSubordinateMap.containsKey(performaceStasOutput.getTlId())) {
					SalesPerformaceStasOutput originalOutput = userAndReportingSubordinateMap
							.get(performaceStasOutput.getTlId());
					SalesPerformaceStasOutput output = getModifiedSalesOutput(originalOutput, performaceStasOutput);
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}
					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				} else {

					SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
					BeanUtils.copyProperties(performaceStasOutput, output);
					output.setCssClass(UserRole.SALES_TL + "");
					if( performaceStasOutput.getTlId() != null && !performaceStasOutput.getTlId().isEmpty()) {
						//Performance Dashboard showing FirstName and LastName Fix Start 
						UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getTlId().toUpperCase());
						if( userVo != null) {
							output.setFirstName(userVo.getFirstName());
							output.setLastName(userVo.getLastName());
						}
						//Performance Dashboard showing FirstName and LastName Fix End
					}

					userAndReportingSubordinateMap.put(performaceStasOutput.getTlId(), output);
				}
				// all executive map
				SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
				BeanUtils.copyProperties(performaceStasOutput, output);
				output.setCssClass(UserRole.SALES_EX + "");
				if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
					//Performance Dashboard showing FirstName and LastName Fix Start 
					UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
					if( userVo != null) {
						output.setFirstName(userVo.getFirstName());
						output.setLastName(userVo.getLastName());
					}
					//Performance Dashboard showing FirstName and LastName Fix End
				}
				userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);

			}

		} else if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
				&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_EX) {

			for (Iterator<SalesPerformaceStasOutput> iterator = performaceStasOutputFromDB.iterator(); iterator
					.hasNext();) {

				SalesPerformaceStasOutput performaceStasOutput = (SalesPerformaceStasOutput) iterator.next();
				// all executive map
				SalesPerformaceStasOutput output = new SalesPerformaceStasOutput();
				BeanUtils.copyProperties(performaceStasOutput, output);
				output.setCssClass(UserRole.SALES_EX + "");
				if( performaceStasOutput.getLoginid() != null && !performaceStasOutput.getLoginid().isEmpty()) {
					//Performance Dashboard showing FirstName and LastName Fix Start 
					UserVo userVo = UserDaoImpl.cachedUsersByLoginId.get(performaceStasOutput.getLoginid().toUpperCase());
					if( userVo != null) {
						output.setFirstName(userVo.getFirstName());
						output.setLastName(userVo.getLastName());
					}
					//Performance Dashboard showing FirstName and LastName Fix End
				}
				userAndReportingSubordinateMap.put(performaceStasOutput.getLoginid(), output);
			}

		}
		outputVo.setUserAndReportingSubordinateMap(userAndReportingSubordinateMap);
		outputVo.setUserAndReportingSubordinateIdMap(userAndReportingSubordinateIdMap);

		log.info("Prepared performance details for sales " + performaceStasInput + " with details "
				+ performaceStasOutputFromDB.size() + " and " + "details are " + userAndReportingSubordinateMap.size()
				+ " where as subordinate maps are " + userAndReportingSubordinateIdMap.size());
		return ResponseUtil.createSuccessResponse().setData(outputVo);

	}

	/**
	 * @author aditya SalesPerformaceStasOutput
	 * @param originalOutput
	 * @param performaceStasOutput
	 * @return
	 */
	private SalesPerformaceStasOutput getModifiedSalesOutput(SalesPerformaceStasOutput originalOutput,
			SalesPerformaceStasOutput performaceStasOutput) {

		if (originalOutput.getLoginsMtd() != null && !originalOutput.getLoginsMtd().isEmpty()
				&& performaceStasOutput.getLoginsMtd() != null && !performaceStasOutput.getLoginsMtd().isEmpty()) {

			originalOutput.setLoginsMtd(Double.valueOf(originalOutput.getLoginsMtd())
					+ Double.valueOf(performaceStasOutput.getLoginsMtd()) + "");
		}

		if (originalOutput.getAadharMtd() != null && !originalOutput.getAadharMtd().isEmpty()
				&& performaceStasOutput.getAadharMtd() != null && !performaceStasOutput.getAadharMtd().isEmpty()) {

			originalOutput.setAadharMtd(Double.valueOf(originalOutput.getAadharMtd())
					+ Double.valueOf(performaceStasOutput.getAadharMtd()) + "");

		}
		if (originalOutput.getActivatedMtd() != null && !originalOutput.getActivatedMtd().isEmpty()
				&& performaceStasOutput.getActivatedMtd() != null
				&& !performaceStasOutput.getActivatedMtd().isEmpty()) {

			originalOutput.setActivatedMtd(Double.valueOf(originalOutput.getActivatedMtd())
					+ Double.valueOf(performaceStasOutput.getActivatedMtd()) + "");

		}

		if (originalOutput.getFtSalesMtd() != null && !originalOutput.getFtSalesMtd().isEmpty()
				&& performaceStasOutput.getFtSalesMtd() != null && !performaceStasOutput.getFtSalesMtd().isEmpty()) {

			originalOutput.setFtSalesMtd(Double.valueOf(originalOutput.getFtSalesMtd())
					+ Double.valueOf(performaceStasOutput.getFtSalesMtd()) + "");

		}
		if (originalOutput.getLoginsLmtd() != null && !originalOutput.getLoginsLmtd().isEmpty()
				&& performaceStasOutput.getLoginsLmtd() != null && !performaceStasOutput.getLoginsLmtd().isEmpty()) {

			originalOutput.setLoginsLmtd(Double.valueOf(originalOutput.getLoginsLmtd())
					+ Double.valueOf(performaceStasOutput.getLoginsLmtd()) + "");

		}

		if (originalOutput.getAadharLmtd() != null && !originalOutput.getAadharLmtd().isEmpty()
				&& performaceStasOutput.getAadharLmtd() != null && !performaceStasOutput.getAadharLmtd().isEmpty()) {

			originalOutput.setAadharLmtd(Double.valueOf(originalOutput.getAadharLmtd())
					+ Double.valueOf(performaceStasOutput.getAadharLmtd()) + "");
		}

		if (originalOutput.getActivatedLmtd() != null && !originalOutput.getActivatedLmtd().isEmpty()
				&& performaceStasOutput.getActivatedLmtd() != null
				&& !performaceStasOutput.getActivatedLmtd().isEmpty()) {

			originalOutput.setActivatedLmtd(Double.valueOf(originalOutput.getActivatedLmtd())
					+ Double.valueOf(performaceStasOutput.getActivatedLmtd()) + "");
		}

		if (originalOutput.getFtSalesLmtd() != null && !originalOutput.getFtSalesLmtd().isEmpty()
				&& performaceStasOutput.getFtSalesLmtd() != null && !performaceStasOutput.getFtSalesLmtd().isEmpty()) {

			originalOutput.setFtSalesLmtd(Double.valueOf(originalOutput.getFtSalesLmtd())
					+ Double.valueOf(performaceStasOutput.getFtSalesLmtd()) + "");
		}

		return originalOutput;
	}

	/**
	 * @author aditya PerformaceStasOutput
	 * @param originalOutput
	 * @param performaceStasOutput
	 * @return copy performance data to original data with mathematical calculation
	 */
	private FrPerformaceStasOutput getModifiedFrOutput(FrPerformaceStasOutput originalOutput,
			FrPerformaceStasOutput performaceStasOutput) {

		if (originalOutput.getClosedTktsMtd() != null && !originalOutput.getClosedTktsMtd().isEmpty()
				&& performaceStasOutput.getClosedTktsMtd() != null
				&& !performaceStasOutput.getClosedTktsMtd().isEmpty()) {

			originalOutput.setClosedTktsMtd(Double.valueOf(originalOutput.getClosedTktsMtd())
					+ Double.valueOf(performaceStasOutput.getClosedTktsMtd()) + "");
		}

		if (originalOutput.getClosedGartMtd() != null && !originalOutput.getClosedGartMtd().isEmpty()
				&& performaceStasOutput.getClosedGartMtd() != null
				&& !performaceStasOutput.getClosedGartMtd().isEmpty()) {

			originalOutput.setClosedGartMtd(Double.valueOf(originalOutput.getClosedGartMtd())
					+ Double.valueOf(performaceStasOutput.getClosedGartMtd()) + "");

		}
		if (originalOutput.getOpenTktsMtd() != null && !originalOutput.getOpenTktsMtd().isEmpty()
				&& performaceStasOutput.getOpenTktsMtd() != null && !performaceStasOutput.getOpenTktsMtd().isEmpty()) {

			originalOutput.setOpenTktsMtd(Double.valueOf(originalOutput.getOpenTktsMtd())
					+ Double.valueOf(performaceStasOutput.getOpenTktsMtd()) + "");

		}

		if (originalOutput.getOpenGartMtd() != null && !originalOutput.getOpenGartMtd().isEmpty()
				&& performaceStasOutput.getOpenGartMtd() != null && !performaceStasOutput.getOpenGartMtd().isEmpty()) {

			originalOutput.setOpenGartMtd(Double.valueOf(originalOutput.getOpenGartMtd())
					+ Double.valueOf(performaceStasOutput.getOpenGartMtd()) + "");

		}
		if (originalOutput.getClosedTktsFtd() != null && !originalOutput.getClosedTktsFtd().isEmpty()
				&& performaceStasOutput.getClosedTktsFtd() != null
				&& !performaceStasOutput.getClosedTktsFtd().isEmpty()) {

			originalOutput.setClosedTktsFtd(Double.valueOf(originalOutput.getClosedTktsFtd())
					+ Double.valueOf(performaceStasOutput.getClosedTktsFtd()) + "");

		}

		if (originalOutput.getClosedGartFtd() != null && !originalOutput.getClosedGartFtd().isEmpty()
				&& performaceStasOutput.getClosedGartFtd() != null
				&& !performaceStasOutput.getClosedGartFtd().isEmpty()) {

			originalOutput.setClosedGartFtd(Double.valueOf(originalOutput.getClosedGartFtd())
					+ Double.valueOf(performaceStasOutput.getClosedGartFtd()) + "");
		}

		if (originalOutput.getOpenTktsFtd() != null && !originalOutput.getOpenTktsFtd().isEmpty()
				&& performaceStasOutput.getOpenTktsFtd() != null && !performaceStasOutput.getOpenTktsFtd().isEmpty()) {

			originalOutput.setOpenTktsFtd(Double.valueOf(originalOutput.getOpenTktsFtd())
					+ Double.valueOf(performaceStasOutput.getOpenTktsFtd()) + "");
		}

		if (originalOutput.getOpenGartFtd() != null && !originalOutput.getOpenGartFtd().isEmpty()
				&& performaceStasOutput.getOpenGartFtd() != null && !performaceStasOutput.getOpenGartFtd().isEmpty()) {

			originalOutput.setOpenGartFtd(Double.valueOf(originalOutput.getOpenGartFtd())
					+ Double.valueOf(performaceStasOutput.getOpenGartFtd()) + "");
		}
		if (originalOutput.getCsatMtd() != null && !originalOutput.getCsatMtd().isEmpty()
				&& performaceStasOutput.getCsatMtd() != null && !performaceStasOutput.getCsatMtd().isEmpty()) {

			originalOutput.setCsatMtd(Double.valueOf(originalOutput.getCsatMtd())
					+ Double.valueOf(performaceStasOutput.getCsatMtd()) + "");

		}
		if (originalOutput.getCsatFtd() != null && !originalOutput.getCsatFtd().isEmpty()
				&& performaceStasOutput.getCsatFtd() != null && !performaceStasOutput.getCsatFtd().isEmpty()) {

			originalOutput.setCsatFtd(Double.valueOf(originalOutput.getCsatFtd())
					+ Double.valueOf(performaceStasOutput.getCsatFtd()) + "");

		}

		return originalOutput;
	}

	/**
	 * @author aditya DeplPerformaceStasOutput
	 * @param originalOutput
	 * @param performaceStasOutput
	 * @return
	 */
	private DeplPerformaceStasOutput getModifiedSdOutput(DeplPerformaceStasOutput originalOutput,
			DeplPerformaceStasOutput performaceStasOutput) {

		if (originalOutput.getFiberActivatedMtd() != null && !originalOutput.getFiberActivatedMtd().isEmpty()
				&& performaceStasOutput.getFiberActivatedMtd() != null
				&& !performaceStasOutput.getFiberActivatedMtd().isEmpty()) {

			originalOutput.setFiberActivatedMtd(Double.valueOf(originalOutput.getFiberActivatedMtd())
					+ Double.valueOf(performaceStasOutput.getFiberActivatedMtd()) + "");
		}

		if (originalOutput.getFiberClosedGartMtd() != null && !originalOutput.getFiberClosedGartMtd().isEmpty()
				&& performaceStasOutput.getFiberClosedGartMtd() != null
				&& !performaceStasOutput.getFiberClosedGartMtd().isEmpty()) {

			originalOutput.setFiberClosedGartMtd(Double.valueOf(originalOutput.getFiberClosedGartMtd())
					+ Double.valueOf(performaceStasOutput.getFiberClosedGartMtd()) + "");

		}
		if (originalOutput.getFiberOpenMtd() != null && !originalOutput.getFiberOpenMtd().isEmpty()
				&& performaceStasOutput.getFiberOpenMtd() != null
				&& !performaceStasOutput.getFiberOpenMtd().isEmpty()) {

			originalOutput.setFiberOpenMtd(Double.valueOf(originalOutput.getFiberOpenMtd())
					+ Double.valueOf(performaceStasOutput.getFiberOpenMtd()) + "");

		}

		if (originalOutput.getFiberOpenGartMtd() != null && !originalOutput.getFiberOpenGartMtd().isEmpty()
				&& performaceStasOutput.getFiberOpenGartMtd() != null
				&& !performaceStasOutput.getFiberOpenGartMtd().isEmpty()) {

			originalOutput.setFiberOpenGartMtd(Double.valueOf(originalOutput.getFiberOpenGartMtd())
					+ Double.valueOf(performaceStasOutput.getFiberOpenGartMtd()) + "");

		}
		if (originalOutput.getCopperActivatedMtd() != null && !originalOutput.getCopperActivatedMtd().isEmpty()
				&& performaceStasOutput.getCopperActivatedMtd() != null
				&& !performaceStasOutput.getCopperActivatedMtd().isEmpty()) {

			originalOutput.setCopperActivatedMtd(Double.valueOf(originalOutput.getCopperActivatedMtd())
					+ Double.valueOf(performaceStasOutput.getCopperActivatedMtd()) + "");

		}

		if (originalOutput.getCopperClosedGartMtd() != null && !originalOutput.getCopperClosedGartMtd().isEmpty()
				&& performaceStasOutput.getCopperClosedGartMtd() != null
				&& !performaceStasOutput.getCopperClosedGartMtd().isEmpty()) {

			originalOutput.setCopperClosedGartMtd(Double.valueOf(originalOutput.getCopperClosedGartMtd())
					+ Double.valueOf(performaceStasOutput.getCopperClosedGartMtd()) + "");
		}

		if (originalOutput.getCopperOpenMtd() != null && !originalOutput.getCopperOpenMtd().isEmpty()
				&& performaceStasOutput.getCopperOpenMtd() != null
				&& !performaceStasOutput.getCopperOpenMtd().isEmpty()) {

			originalOutput.setCopperOpenMtd(Double.valueOf(originalOutput.getCopperOpenMtd())
					+ Double.valueOf(performaceStasOutput.getCopperOpenMtd()) + "");
		}

		if (originalOutput.getCopperOpenGartMtd() != null && !originalOutput.getCopperOpenGartMtd().isEmpty()
				&& performaceStasOutput.getCopperOpenGartMtd() != null
				&& !performaceStasOutput.getCopperOpenGartMtd().isEmpty()) {

			originalOutput.setCopperOpenGartMtd(Double.valueOf(originalOutput.getCopperOpenGartMtd())
					+ Double.valueOf(performaceStasOutput.getCopperOpenGartMtd()) + "");
		}
		if (originalOutput.getProductivityScoreMtd() != null && !originalOutput.getProductivityScoreMtd().isEmpty()
				&& performaceStasOutput.getProductivityScoreMtd() != null
				&& !performaceStasOutput.getProductivityScoreMtd().isEmpty()) {

			originalOutput.setProductivityScoreMtd(Double.valueOf(originalOutput.getProductivityScoreMtd())
					+ Double.valueOf(performaceStasOutput.getProductivityScoreMtd()) + "");

		}
		if (originalOutput.getFiberActivatedFtd() != null && !originalOutput.getFiberActivatedFtd().isEmpty()
				&& performaceStasOutput.getFiberActivatedFtd() != null
				&& !performaceStasOutput.getFiberActivatedFtd().isEmpty()) {

			originalOutput.setFiberActivatedFtd(Double.valueOf(originalOutput.getFiberActivatedFtd())
					+ Double.valueOf(performaceStasOutput.getFiberActivatedFtd()) + "");

		}
		if (originalOutput.getFiberClosedGartFtd() != null && !originalOutput.getFiberClosedGartFtd().isEmpty()
				&& performaceStasOutput.getFiberClosedGartFtd() != null
				&& !performaceStasOutput.getFiberClosedGartFtd().isEmpty()) {

			originalOutput.setFiberClosedGartFtd(Double.valueOf(originalOutput.getFiberClosedGartFtd())
					+ Double.valueOf(performaceStasOutput.getFiberClosedGartFtd()) + "");

		}
		if (originalOutput.getFiberOpenFtd() != null && !originalOutput.getFiberOpenFtd().isEmpty()
				&& performaceStasOutput.getFiberOpenFtd() != null
				&& !performaceStasOutput.getFiberOpenFtd().isEmpty()) {

			originalOutput.setFiberOpenFtd(Double.valueOf(originalOutput.getFiberOpenFtd())
					+ Double.valueOf(performaceStasOutput.getFiberOpenFtd()) + "");

		}
		if (originalOutput.getFiberOpenGartFtd() != null && !originalOutput.getFiberOpenGartFtd().isEmpty()
				&& performaceStasOutput.getFiberOpenGartFtd() != null
				&& !performaceStasOutput.getFiberOpenGartFtd().isEmpty()) {

			originalOutput.setFiberOpenGartFtd(Double.valueOf(originalOutput.getFiberOpenGartFtd())
					+ Double.valueOf(performaceStasOutput.getFiberOpenGartFtd()) + "");

		}
		if (originalOutput.getCopperActivatedFtd() != null && !originalOutput.getCopperActivatedFtd().isEmpty()
				&& performaceStasOutput.getCopperActivatedFtd() != null
				&& !performaceStasOutput.getCopperActivatedFtd().isEmpty()) {

			originalOutput.setCopperActivatedFtd(Double.valueOf(originalOutput.getCopperActivatedFtd())
					+ Double.valueOf(performaceStasOutput.getCopperActivatedFtd()) + "");

		}
		if (originalOutput.getCopperClosedGartFtd() != null && !originalOutput.getCopperClosedGartFtd().isEmpty()
				&& performaceStasOutput.getCopperClosedGartFtd() != null
				&& !performaceStasOutput.getCopperClosedGartFtd().isEmpty()) {

			originalOutput.setCopperClosedGartFtd(Double.valueOf(originalOutput.getCopperClosedGartFtd())
					+ Double.valueOf(performaceStasOutput.getCopperClosedGartFtd()) + "");

		}
		if (originalOutput.getCopperOpenFtd() != null && !originalOutput.getCopperOpenFtd().isEmpty()
				&& performaceStasOutput.getCopperOpenFtd() != null
				&& !performaceStasOutput.getCopperOpenFtd().isEmpty()) {

			originalOutput.setCopperOpenFtd(Double.valueOf(originalOutput.getCopperOpenFtd())
					+ Double.valueOf(performaceStasOutput.getCopperOpenFtd()) + "");

		}
		if (originalOutput.getCopperOpenGartFtd() != null && !originalOutput.getCopperOpenGartFtd().isEmpty()
				&& performaceStasOutput.getCopperOpenGartFtd() != null
				&& !performaceStasOutput.getCopperOpenGartFtd().isEmpty()) {

			originalOutput.setCopperOpenGartFtd(Double.valueOf(originalOutput.getCopperOpenGartFtd())
					+ Double.valueOf(performaceStasOutput.getCopperOpenGartFtd()) + "");

		}
		if (originalOutput.getProductivityScoreFtd() != null && !originalOutput.getProductivityScoreFtd().isEmpty()
				&& performaceStasOutput.getProductivityScoreFtd() != null
				&& !performaceStasOutput.getProductivityScoreFtd().isEmpty()) {

			originalOutput.setProductivityScoreFtd(Double.valueOf(originalOutput.getProductivityScoreFtd())
					+ Double.valueOf(performaceStasOutput.getProductivityScoreFtd()) + "");

		}

		return originalOutput;
	}

}
