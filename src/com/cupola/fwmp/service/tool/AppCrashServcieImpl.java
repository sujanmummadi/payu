/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.mongo.app.AppCrashDao;
import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.service.notification.EmailNotiFicationHelper;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.tools.AppCrashVo;

/**
 * @author aditya
 *
 */
public class AppCrashServcieImpl implements AppCrashServcie
{
	@Autowired
	AppCrashDao appCrashDao;
	@Autowired
	EmailNotiFicationHelper emailNotiFicationHelper;

	private String failedMqSupportEmailId;

	public String getFailedMqSupportEmailId()
	{
		return failedMqSupportEmailId;
	}

	public void setFailedMqSupportEmailId(String failedMqSupportEmailId)
	{
		this.failedMqSupportEmailId = failedMqSupportEmailId;
	}

	@Override
	public void recordAppCrashServcie(AppCrashVo appCrashVo)
	{
		appCrashDao.recordAppCrashServcie(appCrashVo);

		handleProspectCreationFailed(appCrashVo.getLog(), appCrashVo
				.getServerIp(), appCrashVo.getUserId(), appCrashVo
						.getAppVersion(), appCrashVo.getProspectNo(), appCrashVo
								.getLoginId(), appCrashVo.getTicketId());

	}

	@Override
	public void sendAppCrashNotification(AppCrashVo appCrashVo)
	{
		// TODO Auto-generated method stub

	}

	private void handleProspectCreationFailed(String log, String serverIp,
			String userId, String appVersion, String prospectNo, String loginId,
			String ticketId)
	{

		StringBuilder text = new StringBuilder();
		EMailVO eMail = new EMailVO();

		eMail.setSubject("[!URGENT APP CRASH] FWMP App Crash Reported on server "
				+ serverIp + ", for user " + loginId + " as on "
				+ GenericUtil.convertToUiDateFormat(new Date()));

		text.append("FWMP Server IP : " + serverIp).append("\n\n");

		text.append("User id " + userId).append("\n\n");

		text.append("User loginId : " + loginId).append("\n\n");

		text.append("Ticket Id : " + ticketId).append("\n\n");

		text.append("Prospect No : " + prospectNo).append("\n\n");

		text.append("App version : " + appVersion).append("\n\n");

		text.append("Error Message : \n\n\n" + log).append("\n\n");

		eMail.setMessage(text.toString());

		eMail.setTo(failedMqSupportEmailId);

		emailNotiFicationHelper.addNewMailToQueue(eMail);
	}

}
