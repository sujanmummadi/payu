 /**
 * @Author aditya  24-Jul-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.tool;

import java.util.List;

import com.cupola.fwmp.vo.tools.ToolCityVO;

/**
 * @Author aditya  24-Jul-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface ToolCoreService {
	
	public List<ToolCityVO> getAllLocations();

	/**@author aditya
	 * int
	 * @param filePath
	 * @return
	 */
	int closeWOinBulk4Fr(String filePath);

	/**@author aditya
	 * int
	 * @param filePath
	 * @return
	 */
	int closeWOinBulk(String filePath);

}
