/**
* @Author aditya  24-Jul-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.service.tool;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.vo.tools.ToolAreaVO;
import com.cupola.fwmp.vo.tools.ToolBranchVO;
import com.cupola.fwmp.vo.tools.ToolCityVO;

/**
 * @Author aditya 24-Jul-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ToolCoreServiceImpl implements ToolCoreService {

	private static Logger log = LogManager.getLogger(ToolCoreServiceImpl.class.getName());

	@Autowired
	FrTicketDao frTicketDao;
	@Autowired
	WorkOrderDAO workOrderDAO;
	@Autowired
	ReadExcels readExcel;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.tool.ToolCoreService#getAllLocations()
	 */

	@Override
	public List<ToolCityVO> getAllLocations() {

		log.info("Get all locations service called from Tool");

		List<ToolCityVO> cityVOs = new LinkedList<>();

		Map<Long, String> allCities = LocationCache.getAllCity();

		for (Map.Entry<Long, String> cities : allCities.entrySet()) {

			ToolCityVO cityVO = new ToolCityVO(cities.getKey(), cities.getValue());

			log.debug("City ############### Test ##############" + cityVO);

			Map<Long, String> allBranches4City = LocationCache.getBranchesByCityId(cities.getKey().toString());

			List<ToolBranchVO> branchList = new LinkedList<>();

			for (Map.Entry<Long, String> branches : allBranches4City.entrySet()) {

				ToolBranchVO toolBranchVO = new ToolBranchVO(branches.getKey(), branches.getValue());

				log.debug("Branch ############### toolBranchVO ##############" + toolBranchVO);

				Map<Long, String> allArea4Branch = LocationCache.getAreasByBranchId(branches.getKey().toString());
				List<ToolAreaVO> areaList = new LinkedList<>();
				for (Map.Entry<Long, String> areas : allArea4Branch.entrySet()) {

					ToolAreaVO toolAreaVO = new ToolAreaVO(areas.getKey(), areas.getValue());

					log.debug("Area ############### toolAreaVO ##############" + toolAreaVO);

					areaList.add(toolAreaVO);
				}

				toolBranchVO.setAreaList(areaList);

				log.info("Total Area for branch " + toolBranchVO.getBranchName() + " of city " + cityVO.getCityName()
						+ " are " + areaList.size());

				branchList.add(toolBranchVO);

			}

			log.info("Total branch for city " + cityVO.getCityName() + " are " + branchList.size());

			cityVO.setBranchList(branchList);

			cityVOs.add(cityVO);
		}

		log.info("Returing Get all locations service called from Tool " + cityVOs.size());

		log.debug("Returing Get all locations service called from Tool " + cityVOs.size());
		return cityVOs;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.tool.ToolCoreService#closeWOinBulk4Fr(java.lang.String)
	 */
	@Override
	public int closeWOinBulk4Fr(String filePath) {

		log.info("Reading fr force closure file from " + filePath);
		
		Set<String> workOrders = readExcel.readManualFrClosureFile(filePath);

		if (workOrders !=null && !workOrders.isEmpty()) {
			
			log.info("Total number wo for force closre are " + workOrders.size());
			
			 return frTicketDao.closeWOinBulk4Fr(workOrders);
			
		} else
		{
			log.info("Total number wo for force closre are " + 0);

			return 90000002;
		}
	}
	@Override
	public int closeWOinBulk(String filePath) {

		log.info("Reading fr force closure file from " + filePath);
		
		Set<String> workOrders = readExcel.readManualFrClosureFile(filePath);
	
		if (workOrders !=null && !workOrders.isEmpty()) {
	
			log.info("Total number wo for force closre are " + workOrders.size());
	
			 return workOrderDAO.closeWOinBulk(workOrders);

		} else
		{
			log.info("Total number wo for force closre are " + 0);

			return 90000002;
		}
	}
}
