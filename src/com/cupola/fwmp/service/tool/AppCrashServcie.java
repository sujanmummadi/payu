/**
 * 
 */
package com.cupola.fwmp.service.tool;

import com.cupola.fwmp.vo.tools.AppCrashVo;

/**
 * @author aditya
 *
 */
public interface AppCrashServcie
{
	void recordAppCrashServcie(AppCrashVo appCrashVo);

	void sendAppCrashNotification(AppCrashVo appCrashVo);
}
