 /**
 * @Author aditya  28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.tool;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.tools.dashboard.PerformaceStasInput;

/**
 * @Author aditya  28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface ACTExternalToolServices {
	
	APIResponse getFrPerformaceStas(PerformaceStasInput performaceStasInput);

	/**@author aditya
	 * APIResponse
	 * @param performaceStasInput
	 * @return
	 */
	APIResponse getSdPerformaceStas(PerformaceStasInput performaceStasInput);

	/**@author aditya
	 * APIResponse
	 * @param performaceStasInput
	 * @return
	 */
	APIResponse getSalesPerformaceStas(PerformaceStasInput performaceStasInput);

}
