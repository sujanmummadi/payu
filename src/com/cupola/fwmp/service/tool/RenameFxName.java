/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.vo.tools.RenameFXVo;

/**
 * @author aditya
 *
 */
public class RenameFxName
{
	private Logger log = Logger.getLogger(RenameFxName.class.getName());

	@Autowired
	DeviceDAO deviceDAO;

	public void readElemetsDetails()
	{

		List<RenameFXVo> renameFXVos = new ArrayList<>();

		String file = "/home/aditya/Projects/ACT_FWMP/FromLive/mapping/"
				+ File.separator + "Dilsukhnagar.xlsx";

		log.info("File to read is " + file);

		File devcieFile = new File(file);
		FileInputStream fis = null;

		try
		{
			fis = new FileInputStream(devcieFile);
			Iterator<Row> rowIterator = null;
			int totalRow = 0;

			if (devcieFile.getName().endsWith(".xlsx"))
			{

				XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
				XSSFSheet mySheet = myWorkBook.getSheetAt(0);
				totalRow = mySheet.getPhysicalNumberOfRows();
				rowIterator = mySheet.iterator();

			} else if (devcieFile.getName().endsWith(".xls"))
			{

				HSSFWorkbook myWorkBook = new HSSFWorkbook(fis);
				HSSFSheet mySheet = myWorkBook.getSheetAt(0);
				totalRow = mySheet.getPhysicalNumberOfRows();
				rowIterator = mySheet.iterator();
			}

			if(rowIterator != null)
			while (rowIterator.hasNext())
			{
				RenameFXVo renameFXVo = new RenameFXVo();

				Row row = rowIterator.next();

				if (row.getRowNum() == Integer.valueOf(0))
				{
					continue;
				}

				Iterator<Cell> cellIterator = row.cellIterator();

				int columnIndex = 0;

				if (row.getPhysicalNumberOfCells() != Integer.valueOf(2))
				{

					log.error("Skipping row no " + row.getRowNum()
							+ " as it is having extra number "
							+ "of column than Expected. Count of column is  "
							+ row.getPhysicalNumberOfCells());

					continue;

				} else
				{
					while (cellIterator.hasNext())
					{
						Cell cell = cellIterator.next();
						if (columnIndex == Integer.valueOf(0))
						{

							renameFXVo.setOldFxName(CommonUtil
									.checkCellType(cell));

						} else if (columnIndex == Integer.valueOf(1))
						{
							renameFXVo.setNewFxName(CommonUtil
									.checkCellType(cell));
						}
						columnIndex += 1;
					} // end of while for cell

					renameFXVos.add(renameFXVo);
					log.info("Remaing line to read in " + file + " is "
							+ --totalRow);

				} // End of else cell

			} // end of else

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();

			log.error("Error in finding Devcie file " + e.getMessage());

		} catch (IOException e)
		{

			e.printStackTrace();

			log.error("Error in Reading Device file " + e.getMessage());

		} finally
		{
			if (fis != null)
			{
				try
				{
					fis.close();

				} catch (IOException e)
				{
					e.printStackTrace();

				}
			}
		}

		// moveFileToArchive(fileName, basePath, archivePath);

		log.info("Calling rename bulk devcie with total available device sixe "
				+ renameFXVos);
		
		deviceDAO.renameDevice(renameFXVos);

	}

}
