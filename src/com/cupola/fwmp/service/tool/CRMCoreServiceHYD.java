/**
 * 
 */
package com.cupola.fwmp.service.tool;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.ClassificationCategories;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.handlers.CrmControllerHandler;
import com.cupola.fwmp.service.domain.engines.classification.servcie.ClassificationEngineCoreServiceImpl;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.UserVo;

/**
 * @author aditya
 *
 */
public class CRMCoreServiceHYD
{

	private static Logger log = LogManager
			.getLogger(CRMCoreServiceHYD.class.getName());

	@Autowired
	private ClassificationEngineCoreServiceImpl classificationEngineCoreServiceImpl;

	@Autowired
	private GlobalActivities globalActivities;

	@Autowired
	private UserDao userDao;

	@Autowired
	private DBUtil dbUtil;

	@Autowired
	private CrmControllerHandler crmControllerHandler;

	@Autowired
	private CustomerCoreService customerCoreService;

	@Autowired
	TicketDAO ticketDAO;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;

	private boolean isFREnable = false; // take off when fr is fully
										// functionally integrated

	public Boolean getFwmpTestBuild()
	{
		return fwmpTestBuild;
	}

	private Boolean fwmpTestBuild;

	@Autowired
	private DefinitionCoreService definitionCoreService;

	public APIResponse controleHYDCrm(List<MQWorkorderVO> hydMQWorkorderVOList)
	{
		try
		{
			log.info("CRM Controller Job HYD called from Cron trigger");

			if (hydMQWorkorderVOList != null && !hydMQWorkorderVOList.isEmpty())
			{
				log.info("Call prioritization Engine form HYD for "
						+ hydMQWorkorderVOList.size());
				// + hydMQWorkorderVOList);

				// Thread based logic have to be performed here based on city;

				List<String> prospectList = new ArrayList<>();
				Map<String, MQWorkorderVO> prospectDetailMap = new ConcurrentHashMap<String, MQWorkorderVO>();

				for (MQWorkorderVO hydMQWorkorderVO : hydMQWorkorderVOList)
				{
					try 
					{
						if (hydMQWorkorderVO.getProspectNumber() != null) 
						{
							prospectList.add(hydMQWorkorderVO.getProspectNumber());
							prospectDetailMap.put(hydMQWorkorderVO.getProspectNumber(), hydMQWorkorderVO);
						}
					} catch (Exception e) {
						e.printStackTrace();
						log.error("HYD manual, No prospect found for WO " + hydMQWorkorderVO.getWorkOrderNumber());
						continue;
					}
				}

				log.info("Size of total data from HYD MQ is "
						+ prospectDetailMap.size());
				// + prospectDetailMap);

				Map<String, Long> propspectTicketMapFromDb = customerCoreService
						.getTicketIdsFromProspects(prospectList,"HYD");

				log.info("Size of total data from FWMP DB for HYD is "
						+ propspectTicketMapFromDb.size());
				// + " propspectTicketMapFromDb for HYD "
				// + propspectTicketMapFromDb);

				Set<String> prospectListsFromDb = propspectTicketMapFromDb
						.keySet();
                if(prospectDetailMap!=null)
				prospectDetailMap.keySet().removeAll(prospectListsFromDb);

				log.info("Size of total data Not available in FWMP DB for HYD is "
						+ prospectDetailMap.size());
				// + prospectDetailMap);
              
                	
				List<MQWorkorderVO> mqDataNotInFWMP = new ArrayList<MQWorkorderVO>(prospectDetailMap
						.values());

				log.info("Creating data in fwmp for HYD for "
						+ mqDataNotInFWMP.size());
				// + mqDataNotInFWMP);

				Map<String, Long> latestPropspectTicketMapFromDb = customerCoreService
						.addCustomerViaCRM(mqDataNotInFWMP,"HYD");

				Map<String, Long> completeProspectMap = new ConcurrentHashMap<String, Long>();
				completeProspectMap.putAll(latestPropspectTicketMapFromDb);
				completeProspectMap.putAll(propspectTicketMapFromDb);

				log.info("Complete propspect map for HYD "
						+ completeProspectMap.size());
				// + " completeProspectMap "
				// + completeProspectMap);

				List<Long> ticketIds = new ArrayList<Long>(completeProspectMap
						.values());

				Map<Long, String> ticketIdAndFxNameMap = customerCoreService
						.getFxNameFromTicketId(ticketIds);

				log.info("Total ticketIdAndFxNameMap for HYD size is "
						+ ticketIdAndFxNameMap.size());
				// + " ticketIdAndFxNameMap "
				// + ticketIdAndFxNameMap);

				Map<Long, String> ticketIdAndConnectionTypeMap = customerCoreService
						.getConnectionTypeFromTicketId(ticketIds);

				log.info("Total ticketIdAndConnectionTypeMap for HYD size is "
						+ ticketIdAndFxNameMap.size());
				// + ticketIdAndConnectionTypeMap);

				Map<Long, Long> ticketIdAndSaelsExecutiveMap = customerCoreService
						.getTicketIdAndSaelsExecutiveMap(ticketIds);

				log.info("Total ticketIdAndSaelsExecutiveMap for HYD size is "
						+ ticketIdAndFxNameMap.size());
				// + ticketIdAndSaelsExecutiveMap);

				LinkedHashSet<Long> setValue = new LinkedHashSet<>();

				Map<Long, Boolean> workOrderCreationStatus = crmControllerHandler
						.createWorkOrderEntryInBulk(hydMQWorkorderVOList, completeProspectMap);

				log.info("Total workOrderCreationStatus for HYD size is "
						+ workOrderCreationStatus.size());

				for (MQWorkorderVO hydMQWorkorderVO : hydMQWorkorderVOList)
				{
					try
					{
						long ticketId = completeProspectMap
								.get(hydMQWorkorderVO.getProspectNumber());

						setValue.add(ticketId);

						Long assignedTo = null;
						Long assignedBy = ticketIdAndSaelsExecutiveMap
								.get(ticketId);

						log.info(" In HYD definitionCoreService.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)"
								+ definitionCoreService
										.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
								+ "hydMQWorkorderVO.getTicketCategory().toLowerCase()"
								+ hydMQWorkorderVO.getTicketCategory());

						// if (ClassificationCategories.NEW_INSTALLATION_CATEG
						// .equalsIgnoreCase(hydMQWorkorderVO
						// .getTicketCategory())
						// || ClassificationCategories.NEW_INSTALLATION_SYMPTOM
						// .equalsIgnoreCase(hydMQWorkorderVO
						// .getSymptom()))

						log.info("HYD definitionCoreService.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)"
								+ definitionCoreService
										.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
								+ "hydMQWorkorderVO.getTicketCategory().toLowerCase()"
								+ hydMQWorkorderVO.getTicketCategory());

						if (definitionCoreService
								.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
								.contains(hydMQWorkorderVO.getTicketCategory())
								|| definitionCoreService
										.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_SYMPTOM)
										.contains(hydMQWorkorderVO
												.getSymptom()))
						{

							// if (crmControllerHandler
							// .createWorkOrderEntry(hydMQWorkorderVO
							// .getProspectNumber(), hydMQWorkorderVO
							// .getMqId(), hydMQWorkorderVO
							// .getWorkOrderNumber(),
							// TicketCategory.NEW_INSTALLATION))
							// {
							if (workOrderCreationStatus.get(ticketId))
							{
								if (hydMQWorkorderVO.getAssignedTo() == null
										|| hydMQWorkorderVO
												.getAssignedTo() <= 0)
								{

									String fxName = ticketIdAndFxNameMap
											.get(ticketId);

									log.info("In HYD FxName for " + ticketId
											+ " is  " + fxName);

									String connectionType = ticketIdAndConnectionTypeMap
											.get(ticketId);

									UserVo userVo = null;

									if (TicketSubCategory.COPPER
											.equalsIgnoreCase(connectionType))
									{

										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

									} else if (TicketSubCategory.FIBER
											.equalsIgnoreCase(connectionType))
									{
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

									}

									log.info("Assigned to for ticketId for HYD "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();

										addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);
										
										String currentCRM = dbUtil.getCurrentCrmName(null, null);

										if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
											
											workOrderCoreService.updateWorkOrderActivityInCRM(ticketId, null, null, null , null, null, null,null);
										}


										log.info("In HYD Found User for Ticket Id "
												+ ticketId + " assignedTo "
												+ assignedTo);

									} else
									{
//										userVo = userDao
//												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF_STRICT);
										
										if (userVo != null)
										{
											assignedTo = userVo.getId();

											addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

											log.info("In HYD Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);

										} else
										{

											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

											StatusVO status = new StatusVO();
											status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
											status.setTicketId(ticketId);
											status.setPropspectNo(hydMQWorkorderVO
													.getProspectNumber());

											customerCoreService
													.updateTicket(status);

											log.info("In HYD Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);
											// callLogger(ticketId,
											// TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
											setValue.clear();
											continue;
										}
									}

								} else
								{

									assignedTo = hydMQWorkorderVO
											.getAssignedTo();

									ticketDAO
											.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

									log.info("Assiging HYD NEW_INSTALLATION ticket "
											+ ticketId + "to " + assignedTo);
									addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
								}

								// callCopper(hydMQWorkorderVO, assignedTo);
								// callLogger(ticketId);
							} else
							{
								log.info("HYD Work order could not be generated for New Installation "
										+ hydMQWorkorderVO
												.getWorkOrderNumber());
							}
						}
						// else if (ClassificationCategories.RE_ACTIVATION_CATEG
						// .equalsIgnoreCase(hydMQWorkorderVO
						// .getTicketCategory())
						// || ClassificationCategories.RE_ACTIVATION_SYMPTOM
						// .equalsIgnoreCase(hydMQWorkorderVO
						// .getSymptom()))
						else if (definitionCoreService
								.getClassificationCategory(DefinitionCoreService.RE_ACTIVATION_CATEG)
								.contains(hydMQWorkorderVO.getTicketCategory())
								|| definitionCoreService
										.getClassificationCategory(DefinitionCoreService.RE_ACTIVATION_SYMPTOM)
										.contains(hydMQWorkorderVO
												.getSymptom()))
						{
							// if (crmControllerHandler
							// .createWorkOrderEntry(hydMQWorkorderVO
							// .getProspectNumber(), hydMQWorkorderVO
							// .getMqId(), hydMQWorkorderVO
							// .getWorkOrderNumber(),
							// TicketCategory.RE_ACTIVATION_STRING))
							// {
							if (workOrderCreationStatus.get(ticketId))
							{
								if (hydMQWorkorderVO.getAssignedTo() == null
										|| hydMQWorkorderVO
												.getAssignedTo() <= 0)
								{

									String fxName = ticketIdAndFxNameMap
											.get(ticketId);

									String connectionType = ticketIdAndConnectionTypeMap
											.get(ticketId);

									UserVo userVo = null;

									if (TicketSubCategory.COPPER
											.equalsIgnoreCase(connectionType))
									{

										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

									} else if (TicketSubCategory.FIBER
											.equalsIgnoreCase(connectionType))
									{
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

									}

									log.info("Assigned to for ticketId for HYD "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();
										addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
										log.info("Assiging HYD RE_ACTIVATION_STRING ticket "
												+ ticketId + "to "
												+ assignedTo);
										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

									} else
									{
//										userVo = userDao
//												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF_STRICT);
										
										if (userVo != null)
										{
											assignedTo = userVo.getId();
											addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
											log.info("Assiging HYD RE_ACTIVATION_STRING ticket "
													+ ticketId + "to "
													+ assignedTo);
											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

										} else
										{
											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

											StatusVO status = new StatusVO();
											status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
											status.setTicketId(ticketId);
											status.setPropspectNo(hydMQWorkorderVO
													.getProspectNumber());

											customerCoreService
													.updateTicket(status);

											log.info("In HYD Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);
											// callLogger(ticketId,
											// TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
											setValue.clear();
											continue;
										}
									}

								} else
								{

									assignedTo = hydMQWorkorderVO
											.getAssignedTo();
									addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
									log.info("Assiging HYD RE_ACTIVATION_STRING ticket "
											+ ticketId + "to " + assignedTo);
									ticketDAO
											.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

								}

								// callCopper(hydMQWorkorderVO, assignedTo);
								// callLogger(ticketId);
							} else
							{
								log.info("HYD Work order could not be generated for Reactivation  "
										+ hydMQWorkorderVO
												.getWorkOrderNumber());
							}

						}
						// else if (ClassificationCategories.SHIFTING_CATEG
						// .equalsIgnoreCase(hydMQWorkorderVO
						// .getTicketCategory())
						// ||
						// ClassificationCategories.SHIFTING_CONNECTION_SYMPTOM
						// .equalsIgnoreCase(hydMQWorkorderVO
						// .getSymptom()))
						else if (definitionCoreService
								.getClassificationCategory(DefinitionCoreService.SHIFTING_CATEG)
								.contains(hydMQWorkorderVO.getTicketCategory())
								|| definitionCoreService
										.getClassificationCategory(DefinitionCoreService.SHIFTING_CONNECTION_SYMPTOM)
										.contains(hydMQWorkorderVO
												.getSymptom()))
						{

							// if (crmControllerHandler
							// .createWorkOrderEntry(hydMQWorkorderVO
							// .getProspectNumber(), hydMQWorkorderVO
							// .getMqId(), hydMQWorkorderVO
							// .getWorkOrderNumber(),
							// TicketCategory.SHIFTING_STR))
							// {
							if (workOrderCreationStatus.get(ticketId))
							{

								if (hydMQWorkorderVO.getAssignedTo() == null
										|| hydMQWorkorderVO
												.getAssignedTo() <= 0)
								{

									String fxName = ticketIdAndFxNameMap
											.get(ticketId);

									String connectionType = ticketIdAndConnectionTypeMap
											.get(ticketId);

									UserVo userVo = null;

									if (TicketSubCategory.COPPER
											.equalsIgnoreCase(connectionType))
									{

										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

									} else if (TicketSubCategory.FIBER
											.equalsIgnoreCase(connectionType))
									{
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

									}

									log.info("Assigned to for ticketId for HYD "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();
										log.info("Assiging HYD SHIFTING_STR ticket "
												+ ticketId + "to "
												+ assignedTo);
										addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

									} else
									{

//										userVo = userDao
//												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
										userVo = userDao
												.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF_STRICT);
										
										if (userVo != null)
										{
											assignedTo = userVo.getId();
											log.info("Assiging HYD SHIFTING_STR ticket "
													+ ticketId + "to "
													+ assignedTo);
											addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);

											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

										} else
										{
											ticketDAO
													.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

											StatusVO status = new StatusVO();
											status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
											status.setTicketId(ticketId);
											status.setPropspectNo(hydMQWorkorderVO
													.getProspectNumber());

											customerCoreService
													.updateTicket(status);

											log.info("In HYD Found User for Ticket Id "
													+ ticketId + " assignedTo "
													+ assignedTo);
											// callLogger(ticketId,
											// TicketUpdateConstant.AUTO_ASSIGNMENT_FAILED);
											setValue.clear();
											continue;
										}
									}

								} else
								{

									assignedTo = hydMQWorkorderVO
											.getAssignedTo();
									log.info("Assiging HYD SHIFTING_STR ticket "
											+ ticketId + "to " + assignedTo);
									addTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo, ticketId, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_CREATED);
									ticketDAO
											.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

								}

								// callCopper(hydMQWorkorderVO, assignedTo);
								// callLogger(ticketId);
							} else
							{
								log.info("HYD Work order could not be generated for Shifting "
										+ hydMQWorkorderVO
												.getWorkOrderNumber());
							}
						} else if (ClassificationCategories.FR_CATEG
								.equalsIgnoreCase(hydMQWorkorderVO
										.getTicketCategory())
								|| FrTicketUtil
										.isTicketHavingFrSymptom(hydMQWorkorderVO
												.getSymptom())
										&& isFREnable)
						{
							String fxName = ticketIdAndFxNameMap.get(ticketId);
							if (crmControllerHandler
									.createFrDetailsEntry(hydMQWorkorderVO, fxName))
							{
								if (hydMQWorkorderVO.getAssignedTo() == null
										|| hydMQWorkorderVO
												.getAssignedTo() <= 0)
								{
									Long flowId = FrTicketUtil
											.findFlowIdforMatchingSymptomName(hydMQWorkorderVO
													.getSymptom());

									UserVo userVo = userDao
											.getUsersByDeviceName(fxName, AuthUtils
													.getUserRolesByFlowId(flowId));

									log.info("Assigned to for ticketId for HYD "
											+ ticketId + " and fxName "
											+ fxName);
									// + " userVo is " + userVo);

									if (userVo != null)
									{
										assignedTo = userVo.getId();
										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

										log.info("In HYD Found User for Ticket Id "
												+ ticketId + " assignedTo "
												+ assignedTo);
										this.addFrTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo);
									} else
									{
										ticketDAO
												.updateCurrentAssignedTo(ticketId, assignedTo, assignedBy, TicketStatus.WO_GENERATED);

										StatusVO status = new StatusVO();
										status.setStatus(FWMPConstant.TicketStatus.WO_UNASSIGNED_TICKET);
										status.setTicketId(ticketId);
										status.setPropspectNo(hydMQWorkorderVO
												.getProspectNumber());

										customerCoreService
												.updateTicket(status);

										log.info("In HYD Found User for Ticket Id "
												+ ticketId + " assignedTo "
												+ assignedTo);
										setValue.clear();
										setValue.clear();
									}
								} else
								{
									assignedTo = hydMQWorkorderVO
											.getAssignedTo();
									this.addFrTicket2MainQueue(hydMQWorkorderVO, setValue, assignedTo);
								}
							} else
							{
								log.info("HYD Work order could not be generated for FR "
										+ hydMQWorkorderVO
												.getWorkOrderNumber());
							}
						} else
						{
							log.info("Can't find HYD Ticket category : "
									+ hydMQWorkorderVO.getTicketCategory()
									+ " having symptom : "
									+ hydMQWorkorderVO.getSymptom());
						}
						setValue.clear();
					} catch (Exception e)
					{
						log.error("Error while iterating mq hyd result for "
								+ hydMQWorkorderVO.getWorkOrderNumber());
						continue;
					}
				}
				
			} else
			{
				log.info("No record found from CRM HYD");
				
				return ResponseUtil.createRecordNotFoundResponse();
			}

			log.debug("Global Activities got all the values for area HYD "
					+ globalActivities.getMainQueue());
			
			return ResponseUtil.createSuccessResponse();

		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error in CRM Job of HYD " + e.getMessage());
			
			return ResponseUtil.createDBExceptionResponse();
		}
	}

	private void addFrTicket2MainQueue(MQWorkorderVO hydMQWorkorderVO,
			LinkedHashSet<Long> setValue, long assignedTo)
	{
		long reportTo = 0l;
		reportTo = userDao.getReportToUser(assignedTo);
		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

		classificationEngineCoreServiceImpl
				.addFrTicket2MainQueue(neKey, setValue);
		if (reportTo == 0l)
		{
			log.info("No Report to found for user " + assignedTo);
			return;
		}
		String tlKey = dbUtil.getKeyForQueueByUserId(reportTo);
		log.info("Adding ne ticket in Tl queue for HYD with TL key " + tlKey);
		classificationEngineCoreServiceImpl
				.addFrTicket2MainQueue(tlKey, setValue);

		setValue.clear();
	}

	private void addTicket2MainQueue(MQWorkorderVO hydMQWorkorderVO,
			LinkedHashSet<Long> setValue, long assignedTo, long ticketId,
			String ticketCategory)
	{

		long reportTo = 0l;

		reportTo = userDao.getReportToUser(assignedTo);

		String neKey = dbUtil.getKeyForQueueByUserId(assignedTo);

		classificationEngineCoreServiceImpl
				.addTicket2MainQueue(neKey, setValue);

		if (reportTo == 0)
		{
			log.info("No Report to found for user " + assignedTo);
			return;
		}

		String tlKey = dbUtil.getKeyForQueueByUserId(reportTo);

		log.info("In HYD Adding tickets + " + setValue + " for HYD" + neKey
				+ " TL key " + tlKey);

		classificationEngineCoreServiceImpl
				.addTicket2MainQueue(tlKey, setValue);

		// callCopper(hydMQWorkorderVO, _wfRequestList, assignedTo);
		callLogger(ticketId, ticketCategory);
		setValue.clear();

		log.debug("In HYD Global Activities got all the values for area HYD after adding queue "
				+ globalActivities.getMainQueue());

	}

	private void callLogger(long ticketId, String ticketCategory)
	{
		TicketUpdateGateway
				.logTicket(new TicketLogImpl(ticketId, ticketCategory, FWMPConstant.SYSTEM_ENGINE));

	}

}
