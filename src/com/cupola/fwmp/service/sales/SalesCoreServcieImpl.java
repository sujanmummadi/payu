/**
 * 
 */
package com.cupola.fwmp.service.sales;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.BooleanValue;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.ProspectType;
import com.cupola.fwmp.FWMPConstant.SalesCount;
import com.cupola.fwmp.FWMPConstant.TicketSource;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.UserCaf;
import com.cupola.fwmp.controller.integ.CRMUpdateRequestHandler;
import com.cupola.fwmp.dao.router.RouterDAO;
import com.cupola.fwmp.dao.sales.SalesDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.userCaf.UserCafDAO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.user.UserService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CafDetailsVo;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.counts.SalesTypeCountVO;
import com.cupola.fwmp.vo.counts.TLSEDetails;
import com.cupola.fwmp.vo.sales.PaymentUpdateVO;
import com.cupola.fwmp.vo.sales.RouterVo;
import com.cupola.fwmp.vo.sales.SalesCountVO;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.TicketStatusDetails;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;
import com.cupola.fwmp.vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo;

/**
 * @author aditya
 * 
 */
public class SalesCoreServcieImpl implements SalesCoreServcie
{
	private static Logger log = LogManager
			.getLogger(SalesCoreServcieImpl.class.getName());

	@Autowired
	SalesDAO salesDAO;
	@Autowired
	RouterDAO routerDAO;
	@Autowired
	UserCafDAO userCafDAO;

	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	UserDao userDao;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	UserService userService;

	@Autowired
	TicketDAO ticketDAO;
	
	@Autowired
	CRMUpdateRequestHandler crmUpdateRequestHandler;

	private int baseValue;

	public void setBaseValue(int baseValue)
	{
		this.baseValue = baseValue;
	}

	@Override
	public APIResponse getCount() {return null;}
	
	@Override
	public APIResponse getSalesCounts()
	{
		List<TicketStatusDetails> statusList = new ArrayList<TicketStatusDetails>();

		try
		{
			List<SalesTypeCountVO> customerList = salesDAO
					.customerTypeCount(AuthUtils.getCurrentUserId());

			if (customerList != null && !customerList.isEmpty())
			{
				log.debug("customerList size " + customerList.size()
						+ " customerList " + customerList);

//				log.debug("DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER "
//						+ definitionCoreService
//								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER));

				log.debug("DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER "
						+ definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER));

				if (customerList != null && !customerList.isEmpty())
				{
					for (SalesTypeCountVO customer : customerList)
					{
						int paymentStatus = customer.getPaymentStatus();
						int documentStatus = customer.getDocumentStatus();
						int ticketStatus = customer.getTicketStatus();
						int customerStatus = customer.getProspectStatus();
						int poaDocumentStatus = customer.getPoaDocumentStatus();
						int poiDocumentStatus = customer.getPoiDocumentStatus();
						
						log.debug("paymentStatsu" + paymentStatus
								+ " poaDocumentStatus " + poaDocumentStatus
								+ "poiDocumentStatus " + poiDocumentStatus
								+ "ticketStatus " + ticketStatus
								+ " customerStatus " + customerStatus);
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_NEW_TICKET)
								.contains(ticketStatus))
						{
							log.debug("FILTER_SALES_NEW_TICKET "
									+ ticketStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.NEW_ID));
						}
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_HOT_TICKET)
								.contains(customerStatus))
						{
							log.debug("customerStatus FILTER_SALES_HOT_TICKET"
									+ customerStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), ProspectType.HOT_ID));
						}
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_MEDIUM_TICKET)
								.contains(customerStatus))
						{
							log.debug("customerStatus FILTER_SALES_MEDIUM_TICKET"
									+ customerStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), ProspectType.MEDIUM_ID));
						}
						if (definitionCoreService.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_COLD_TICKET)
								.contains(customerStatus)
								|| definitionCoreService
										.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_COLD_TICKET)
										.contains(ticketStatus)
								|| definitionCoreService
										.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_COLD_TICKET)
										.contains(paymentStatus)
								|| definitionCoreService
										.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_COLD_TICKET)
										.contains(poiDocumentStatus)
								|| definitionCoreService
										.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_COLD_TICKET)
										.contains(poaDocumentStatus)
								|| definitionCoreService
										.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_COLD_TICKET)
										.contains(documentStatus))
						{
							log.debug("customerStatus FILTER_SALES_HOT_TICKET"
									+ customerStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), ProspectType.COLD_ID));
						}
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_FEASIBILITY_PENDING_TICKET)
								.contains(ticketStatus))
						{
							log.debug("FILTER_SALES_FEASIBILITY_PENDING_TICKET "
									+ ticketStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.FEASIBILITY_PENDING_ID));
						}

						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_POI_DOCUMENT_STATUS_FILTER)
								.contains(poiDocumentStatus))
						{
							log.debug("FILTER_SALES_POI_DOCUMENT_PENDING_WITH_CFE_TICKET "
									+ poiDocumentStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.POI_DOCUMENT_PENDING_WITH_CFE_ID));
						}
						
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_POA_DOCUMENT_STATUS_FILTER)
								.contains(poaDocumentStatus))
						{
							log.debug("FILTER_SALES_POA_DOCUMENT_PENDING_WITH_CFE_TICKET "
									+ poaDocumentStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.POA_DOCUMENT_PENDING_WITH_CFE_ID));
						}
//						if (definitionCoreService
//								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_DOCUMENT_PENDING_WITH_CFE_TICKET)
//								.contains(documentStatus))
//						{
//							log.debug("FILTER_SALES_DOCUMENT_PENDING_WITH_CFE_TICKET "
//									+ documentStatus);
//
//							statusList.add(new TicketStatusDetails(customer
//									.getTicketId(), SalesCount.DOCUMENT_PENDING_WITH_CFE_ID));
//						}
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_PAYMENT_PENDING_WITH_CFE_TICKET)
								.contains(paymentStatus))
						{
							log.debug("FILTER_SALES_PAYMENT_PENDING_WITH_CFE_TICKET "
									+ paymentStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.PAYMENT_PENDING_WITH_CFE_ID));
						}
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_PERMISSION_TICKET)
								.contains(ticketStatus))
						{
							log.debug("FILTER_SALES_PERMISSION_TICKET "
									+ ticketStatus);

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.PERMISSION_ID));
						}
						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_FEASIBILITY_APPROVED_TICKET)
								.contains(ticketStatus))
						{
							log.debug("FILTER_SALES_FEASIBILITY_APPROVED_TICKET "
									+ ticketStatus);
							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.FEASIBILITY_APPROVED_ID));
						}

						if (definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_CAF_ACCEPTED_BY_CFE_TICKET)
								.contains(poiDocumentStatus)
								&& definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_CAF_ACCEPTED_BY_CFE_TICKET)
								.contains(poaDocumentStatus)
								&& definitionCoreService
										.getStatusFilterForRole(DefinitionCoreService.FILTER_SALES_CAF_ACCEPTED_BY_CFE_TICKET)
										.contains(paymentStatus))
						{
							log.debug("TicketStatus.PAYMENT_APPROVED "
									+ ticketStatus
									+ " TicketStatus.POA_POI-DOCUMENTS_APPROVED ");

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.CAF_ACCEPTED_BY_CFE_ID));
						}
						if (customer.getTicketSource() != null && customer
								.getTicketSource()
								.equalsIgnoreCase(TicketSource.DOOR_KNOCK))
						{
							log.debug("TicketStatus.PAYMENT_APPROVED "
									+ ticketStatus
									+ " TicketStatus.DOCUMENTS_APPROVED ");

							statusList.add(new TicketStatusDetails(customer
									.getTicketId(), SalesCount.DOOR_KNOCK_ID));
						}

						statusList.add(new TicketStatusDetails(customer
								.getTicketId(), SalesCount.TOTAL_ID));
					}
			}

			}
		} catch (Exception e)
		{
			log.error("Exception in getting dash board for user "
					+ AuthUtils.getCurrentUserId());
			e.printStackTrace();
		}

		List<SalesCountVO> salesCountVOList = salesCountSupport(statusList);

		log.debug("salesCountVOList from getSalesCounts " + salesCountVOList);

		return ResponseUtil.createSuccessResponse().setData(salesCountVOList);
	}

	@Override
	public APIResponse updateBasicInfo(UpdateProspectVO updateProspectVO,boolean isValuesFromCRM,Long assignToFromCRM,String deDupFlag)
	{
		try
		{
			log.debug("Update basic info called for " + updateProspectVO);

			if (updateProspectVO == null)
				return ResponseUtil.createNullParameterResponse();

			if (updateProspectVO.getTicketId() == null
					|| "null".equalsIgnoreCase(updateProspectVO.getTicketId())
					|| updateProspectVO.getTicketId().isEmpty())

				return ResponseUtil.createNullParameterResponse()
						.setData("Ticket id could not be empty or null");

			long result = salesDAO.updateBasicInfo(updateProspectVO ,  isValuesFromCRM ,assignToFromCRM,deDupFlag);

			log.debug("Update basic info called and result is " + result);

			if (updateProspectVO.isNonFeasible())
			{
				log.debug("################# baseValue " + baseValue);

				ticketDAO.updatePriority(Long.valueOf(updateProspectVO
						.getTicketId()), baseValue, updateProspectVO
								.isNonFeasible());
			}

			String ammountPayable = null;

			// if (updateProspectVO.getGxPermission() != null
			// && updateProspectVO.getGxPermission().equalsIgnoreCase("true"))
			// ammountPayable = globalActivities
			// .getDeploymentSettingByKey(DeploymentConstants.PAYABLE_AMMOUNT_GX);
			//
			// else if (updateProspectVO.getCxPermission() != null
			// && updateProspectVO.getCxPermission().equalsIgnoreCase("true"))
			// ammountPayable = globalActivities
			// .getDeploymentSettingByKey(DeploymentConstants.PAYABLE_AMMOUNT_CX);
			//
			// else
			// ammountPayable = null;

			log.debug("Amount Payable is " + ammountPayable);

			if (result > 0)
			{
				log.info("Calling sms gateway for ticket "
						+ updateProspectVO.getTicketId()
						+ " updateProspectVO.isNonFeasible() "
						+ updateProspectVO.isNonFeasible());

				if (updateProspectVO.getProspectType() != null)
				{
					if (updateProspectVO.getProspectType()
							.equalsIgnoreCase(FWMPConstant.ProspectType.HOT))
					{
						salesDAO.triggerMessageAfterBasicInfoUpdate(Long
								.valueOf(updateProspectVO
										.getTicketId()), updateProspectVO
												.isNonFeasible());

					}

				}
				
				if (updateProspectVO.getSource() != null && updateProspectVO.getSource()
						.equalsIgnoreCase(CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL)) {

				} else
					log.info("updateSalesActivityInCRM method() called from salescoreservices"+updateProspectVO.getMobileNumber()); 
				
				String currentCRM = dbUtil.getCurrentCrmName(null, null);

				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
					
					updateSalesActivityInCRM(Long.valueOf(updateProspectVO.getTicketId()),
							updateProspectVO.getCustomerId() == null ? null
									: Long.valueOf(updateProspectVO.getCustomerId()),
							updateProspectVO.getProspectNo() == null ? null : updateProspectVO.getProspectNo());
				}
				

				return ResponseUtil.createSuccessResponse()
						.setData(ammountPayable);
			} else
				return ResponseUtil.createSuccessResponse()
						.setData("Basic Info Updated Failed");
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error(" " + e.getMessage());
			return ResponseUtil.createSuccessResponse()
					.setData("Basic Info Updated Failed");
		}

	}

	@Override
	public APIResponse updateTariff(TariffPlanUpdateVO planUpdateVO)
	{
		try
		{
			if (planUpdateVO == null)
				return ResponseUtil.createNullParameterResponse();

			if (planUpdateVO.getTariffPlanId() <= 0)
				return ResponseUtil.createNullParameterResponse()
						.setData("Tariff Plan could not be empty or null");

			if (planUpdateVO.getTicketId() <= 0)
				return ResponseUtil.createNullParameterResponse()
						.setData("Ticket id could not be empty or null");

			if (planUpdateVO.getExternalRouterRequired() != null
					&& planUpdateVO.getExternalRouterRequired()
							.equalsIgnoreCase(BooleanValue.TRUE))
				planUpdateVO.setExternalRouterRequired("1");
			else
				planUpdateVO.setExternalRouterRequired("0");

			int result = salesDAO.updateTariff(planUpdateVO);

			log.debug("Update Tariff response for propspect id "
					+ planUpdateVO.getProspectNo() + " is " + result);

			if (result == 1)
			{
				
				String currentCRM = dbUtil.getCurrentCrmName(null, null);

				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
					
					updateSalesActivityInCRM(Long.valueOf(planUpdateVO.getTicketId()),  null  , 
							planUpdateVO.getProspectNo() == null ? null : planUpdateVO.getProspectNo());
				}
				
				
				return ResponseUtil.createSuccessResponse()
						.setData("Tariff Plan Updated");

			}
				
			else
				return ResponseUtil.createSuccessResponse()
						.setData("Tariff Plan Update failed");
		} catch (Exception e)
		{
			log.error(" " + e.getMessage());
			e.printStackTrace();
			return ResponseUtil.createSuccessResponse()
					.setData("Tariff Plan Update failed");
		}

	}

	@Override
	public APIResponse updatePayment(PaymentUpdateVO paymentUpdateVO)
	{
		try
		{
			if (paymentUpdateVO == null)
				return ResponseUtil.createNullParameterResponse();

			if (paymentUpdateVO.getTicketId() <= 0)
				return ResponseUtil.createNullParameterResponse()
						.setData("Ticket id could not be empty or null");

			int result = salesDAO.updatePayment(paymentUpdateVO);

			if (result == 1)
			{

				long currentUser = AuthUtils.getCurrentUserId();

				CafDetailsVo cafDetailsVo = new CafDetailsVo(paymentUpdateVO
						.getCafNo(), UserCaf.USED, currentUser);

				log.debug("Updating caf status for ticket "
						+ paymentUpdateVO.getTicketId() + " caf no is "
						+ paymentUpdateVO.getCafNo() + " currentUser "
						+ currentUser);

				userCafDAO.updateUserCafByUserId(cafDetailsVo);
				
				String currentCRM = dbUtil.getCurrentCrmName(null, null);

				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
					
					updateSalesActivityInCRM(Long.valueOf(paymentUpdateVO.getTicketId()),  null  , null);
				}

				return ResponseUtil.createSuccessResponse()
						.setData("Payment Updated");
			} else
				return ResponseUtil.createSuccessResponse()
						.setData("Payment Update Failed");
		} catch (Exception e)
		{
			log.error(" " + e.getMessage());
			e.printStackTrace();
			return ResponseUtil.createSuccessResponse()
					.setData("Payment Update Failed");
		}

	}

	@Override
	public APIResponse getRouter(Long cityId)
	{
		List<RouterVo> routerVoList = routerDAO.getRouter(cityId);

		log.info("Router details Test ########## " + routerVoList);

		return ResponseUtil.createSuccessResponse().setData(routerVoList);
	}

	@Override
	public APIResponse seSummary()
	{
		Map<Long, TLSEDetails> tlSeDetailsMap = new HashMap<Long, TLSEDetails>();

		try
		{
			List<UserVo> userVoList = userDao
					.getReportee(AuthUtils.getCurrentUserId());
			//List<UserVo> userVoList = userDao.getReportingUsers(AuthUtils.getCurrentUserId()).entrySet().stream().map(entity -> userDao.getUserById(entity.getKey())).collect(Collectors.toList());
			log.debug("Repotee are " + userVoList);

			if (AuthUtils.isManager())
			{
				TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
				if (filter.getAreaId() != null && filter.getAreaId() > 0)
					userVoList = userDao
							.getUserDetailForAreasWithRoles(Collections
									.singletonList(filter
											.getAreaId()), AuthUtils.SALES_TL_DEF, true);
				else
				{
					userVoList = userDao
							.getUserDetailForAreasWithRoles(AuthUtils
									.getAreaIdsForCurrentUser(), AuthUtils.SALES_TL_DEF, true);
				}

			}

			TLSEDetails tlseDetail = null;

			for (Iterator<UserVo> iterator = userVoList.iterator(); iterator
					.hasNext();)
			{
				UserVo userVo = (UserVo) iterator.next();

				tlseDetail = new TLSEDetails(GenericUtil
						.completeUserName(userVo), userVo
								.getId(), 0, 0, 0, 0, 0);

				tlSeDetailsMap.put(userVo.getId(), tlseDetail);
			}

			List<SalesTypeCountVO> customerList = salesDAO
					.customerTypeCount(AuthUtils.getCurrentUserId());

			if (customerList != null)
			{
				log.debug("customerList size " + customerList.size()
						+ " customerList " + customerList);

				for (SalesTypeCountVO customer : customerList)
				{

					if (AuthUtils.isManager())
					{
						customer.setAssignedTo(userDao
								.getReportToUser(customer.getAssignedTo()));
					}

					if (tlSeDetailsMap.containsKey(customer.getAssignedTo()))
					{
						tlseDetail = tlSeDetailsMap
								.get(customer.getAssignedTo());

						log.debug("TLSEDetails " + tlseDetail + " for "
								+ customer.getAssignedTo());
					} else
					{
						tlseDetail = new TLSEDetails();

						tlseDetail.setSeUserId(customer.getAssignedTo());
						tlseDetail.setSeName(GenericUtil
								.completeUserName(userDao.getUserById(customer
										.getAssignedTo())));
						log.debug("First entry TLSEDetails " + tlseDetail
								+ " for " + customer.getAssignedTo());
					}
					Long ticketId = customer.getTicketId();
					if (customer.getProspectStatus() == ProspectType.HOT_ID)
					{
						tlseDetail.addHotCustomerCount();
						tlseDetail.addHotCustomerTicketIds(ticketId);

					} else if (customer
							.getProspectStatus() == ProspectType.COLD_ID)
					{
						tlseDetail.addColdCustomerCount();
						tlseDetail.addColdCustomerTicketIds(ticketId);

					} else if (customer
							.getProspectStatus() == ProspectType.MEDIUM_ID)
					{

						tlseDetail.addMediumCustomerCount();
						tlseDetail.addMediumCustomerTicketIds(ticketId);

					} else if (customer.getTicketStatus() == TicketStatus.OPEN)
					{
						tlseDetail.addPendingProspectCount();
						tlseDetail.addPendingProspectTicketIds(ticketId);
					}

					tlSeDetailsMap.put(customer.getAssignedTo(), tlseDetail);
				}
			}
		} catch (Exception e)
		{
			log.error(" " + e.getMessage());
			e.printStackTrace();
		}

		List<TLSEDetails> tlseDetails = new ArrayList<TLSEDetails>();

		if (!tlSeDetailsMap.isEmpty())
		{
			for (Map.Entry<Long, TLSEDetails> entry : tlSeDetailsMap.entrySet())
			{
				tlseDetails.add((TLSEDetails) entry.getValue());
			}
		}
		log.debug(" TLSEDetails " + tlseDetails);

		return ResponseUtil.createSuccessResponse().setData(tlseDetails);
	}

	private List<SalesCountVO> salesCount(int document, int payment,
			int permission, int pending, int feasibility, int hotCustomerCount,
			int coldCustomerCount, int mediumCustomerCount, int total)
	{

		List<SalesCountVO> salesCountVOList = new ArrayList<>();

		salesCountVOList
				.add(new SalesCountVO(SalesCount.DOCUMENT_PENDING_WITH_CFE, SalesCount.DOCUMENT_PENDING_WITH_CFE_ID, document));

		salesCountVOList
				.add(new SalesCountVO(SalesCount.PAYMENT_PENDING_WITH_CFE, SalesCount.PAYMENT_PENDING_WITH_CFE_ID, payment));

		salesCountVOList
				.add(new SalesCountVO(SalesCount.PERMISSION, SalesCount.PERMISSION_ID, permission));

		salesCountVOList
				.add(new SalesCountVO(SalesCount.NEW, SalesCount.NEW_ID, pending));

		salesCountVOList
				.add(new SalesCountVO(SalesCount.FEASIBILITY_PENDING, SalesCount.FEASIBILITY_PENDING_ID, feasibility));

		salesCountVOList
				.add(new SalesCountVO(SalesCount.TOTAL, SalesCount.TOTAL_ID, total));

		salesCountVOList
				.add(new SalesCountVO(ProspectType.HOT, ProspectType.HOT_ID, hotCustomerCount));

		salesCountVOList
				.add(new SalesCountVO(ProspectType.COLD, ProspectType.COLD_ID, coldCustomerCount));

		salesCountVOList
				.add(new SalesCountVO(ProspectType.MEDIUM, ProspectType.MEDIUM_ID, mediumCustomerCount));

		return salesCountVOList;
	}

	@Override
	public APIResponse updatePermission(UpdateProspectVO updateProspectVO)
	{
		log.debug("Update Permission called for " + updateProspectVO);

		try
		{
			if (updateProspectVO == null)
				return ResponseUtil.createNullParameterResponse();

			if (updateProspectVO.getTicketId() == null
					|| "null".equalsIgnoreCase(updateProspectVO.getTicketId())
					|| updateProspectVO.getTicketId().isEmpty())

				return ResponseUtil.createNullParameterResponse()
						.setData("Ticket id could not be empty or null");

			if (updateProspectVO.getPermissionFeasibilityStatus() == null
					|| "null".equalsIgnoreCase(updateProspectVO
							.getPermissionFeasibilityStatus())
					|| updateProspectVO.getPermissionFeasibilityStatus()
							.isEmpty())

				return ResponseUtil.createNullParameterResponse()
						.setData("Permission Status could not be empty or null");

			int result = salesDAO.updatePermission(updateProspectVO);

			if (result == 1)
			{
				UserVo userVo = userDao.getUserById(ticketDAO
						.ticketCurrentUser(Long
								.valueOf(updateProspectVO.getTicketId()))
						.getAssignedBy());

				List<TypeAheadVo> typeAheadVos = new ArrayList<>();

				if (userVo != null)
				{
					ReassignTicketVo reassignTicketVo = new ReassignTicketVo();

					reassignTicketVo
							.setDonnerUserId(AuthUtils.getCurrentUserLoginId());
					reassignTicketVo.setReason("Reassigned Ticket");
					reassignTicketVo
							.setRecipientUserId(String.valueOf(userVo.getId()));
					reassignTicketVo.setRemark(updateProspectVO.getRemarks());

					List<Long> ticketIds = new ArrayList<Long>();
					ticketIds.add(Long.valueOf(updateProspectVO.getTicketId()));

					reassignTicketVo.setTicketIds(ticketIds);
					reassign(reassignTicketVo);

					TypeAheadVo typeAheadVo = new TypeAheadVo(userVo
							.getId(), GenericUtil.completeUserName(userVo));

					typeAheadVos.add(typeAheadVo);

					ticketDAO.updatePriority(Long.valueOf(updateProspectVO
							.getTicketId()), baseValue, !updateProspectVO
									.isNonFeasible());

				}
				return ResponseUtil.createSuccessResponse()
						.setData(typeAheadVos);
			} else
				return ResponseUtil.createSuccessResponse()
						.setData("Update Permission Failed");
		} catch (NumberFormatException e)
		{
			log.error(" " + e.getMessage());
			e.printStackTrace();
			return ResponseUtil.createSuccessResponse()
					.setData("Update Permission Failed");
		}
	}

	@Override
	public void reassign(ReassignTicketVo reassignTicketVo)
	{
		try
		{
			log.info("Reassiging tickets request " + reassignTicketVo);

			long recipientUserId = Long
					.valueOf(reassignTicketVo.getRecipientUserId());

			// String recipientUserKey = dbUtil
			// .getKeyForQueueByUserId(recipientUserId);
			//
			// LinkedHashSet<Long> ticketNumberList = new
			// LinkedHashSet<Long>(reassignTicketVo
			// .getTicketIds());

			int result = ticketDAO.reassignTicket(reassignTicketVo, null);

			log.info("Total updated rows are " + result);

			if (result > 0)
			{
				if (AuthUtils.isNIUser()
						&& !reassignTicketVo.isWorkOrderGenerated())
				{
					dbUtil.removeOneTicketFromQueue(AuthUtils
							.getCurrentUserId(), reassignTicketVo.getTicketIds()
									.get(0));
				}

				dbUtil.addOneTicketToQueue(recipientUserId, reassignTicketVo
						.getTicketIds().get(0));
			}
			// log.info("Adding ticket " + ticketNumberList + " for user " +
			// recipientUserKey);
			//
			// globalActivities
			// .addTicket2MainQueue(recipientUserKey, ticketNumberList);
			//
			//
			// String sourceKey = dbUtil.getKeyForQueueByUserId(AuthUtils
			// .getCurrentUserId());
			//
			// log.info("Remove ticket " + sourceKey + " for user " +
			// recipientUserKey);
			//
			// globalActivities
			// .removeBulkTicketsFromMainQueueByKey(sourceKey,
			// ticketNumberList);
			//
			// long recipientTlId = userDao.getReportToUser(recipientUserId);
			//
			// if (recipientTlId > 0)
			// {
			// String recipientTlKey = dbUtil
			// .getKeyForQueueByUserId(recipientTlId);
			//
			// globalActivities
			// .addTicket2MainQueue(recipientTlKey, ticketNumberList);
			//
			// }
			//
			// long sourceTlId =
			// userDao.getReportToUser(AuthUtils.getCurrentUserId());
			//
			// if (sourceTlId > 0)
			// {
			// String sourceTlKey = dbUtil.getKeyForQueueByUserId(sourceTlId);
			//
			// globalActivities
			// .removeBulkTicketsFromMainQueueByKey(sourceTlKey,
			// ticketNumberList);
			//
			// }

			List<TicketLog> ticketLogs = new ArrayList<TicketLog>();

			for (Long id : reassignTicketVo.getTicketIds())
			{
				TicketLog ticketLog = new TicketLogImpl(id, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED, AuthUtils
						.getCurrentUserId());
				Map<String, String> additionalAttributeMap = new HashMap<String, String>();
				additionalAttributeMap
						.put(TicketUpdateConstant.ASSIGNED_TO_KEY, recipientUserId
								+ "");
				ticketLog.setAdditionalAttributeMap(additionalAttributeMap);
				ticketLogs.add(ticketLog);
			}
			TicketUpdateGateway.logTickets(ticketLogs);
		} catch (NumberFormatException e)
		{
			log.error("Error While formating number :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public APIResponse updatePropspectType(UpdateProspectVO updateProspectVO)
	{
		try
		{
			if (updateProspectVO == null)
				return ResponseUtil.createNullParameterResponse();

			if (updateProspectVO != null
					&& updateProspectVO.getTicketId() == null
					|| "null".equalsIgnoreCase(updateProspectVO.getTicketId())
					|| updateProspectVO.getTicketId().isEmpty())
				return ResponseUtil.createNullParameterResponse()
						.setData("Ticket number could not be null or empty");

			int result = salesDAO.updatePropspectType(updateProspectVO);

			if (result == 1)
			{
				String currentCRM = dbUtil.getCurrentCrmName(null, null);

				if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
					
					updateSalesActivityInCRM(Long.valueOf(updateProspectVO.getTicketId()),
							updateProspectVO.getCustomerId() == null ? null
									: Long.valueOf(updateProspectVO.getCustomerId()),
							updateProspectVO.getProspectNo() == null ? null : updateProspectVO.getProspectNo());
				}
				
				return ResponseUtil.createSuccessResponse()
						.setData("Propspect Type Updated");

			} else if (result == ProspectType.COLD_ID)
			{
				return ResponseUtil.createSuccessResponse()
						.setData("Ticket already closed by BM");
			} else
			{
				return ResponseUtil.createSuccessResponse()
						.setData("Update Propspect Type Failed");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error(" " + e.getMessage());
			return ResponseUtil.createSuccessResponse()
					.setData("Update Propspect Type Failed");
		}
	}

	private List<SalesCountVO> salesCountSupport(
			List<TicketStatusDetails> statusList)
	{

		log.debug("Total Tickets are " + statusList);

		Map<Integer, List<Long>> statusTicketIds = populateSalesCountMap();

		if (statusList != null && !statusList.isEmpty())
		{
			for (Iterator<TicketStatusDetails> iterator = statusList
					.iterator(); iterator.hasNext();)
			{
				TicketStatusDetails ticketStatusDetails = (TicketStatusDetails) iterator
						.next();

				log.debug(" Ticket status = " + ticketStatusDetails.getStatus()
						+ " ticket No = " + ticketStatusDetails.getTicketId());

				if (statusTicketIds
						.containsKey(ticketStatusDetails.getStatus()))
				{
					log.debug("statusTicketIds contains "
							+ ticketStatusDetails.getStatus());

					List<Long> ticketIds = statusTicketIds
							.get(ticketStatusDetails.getStatus());
					if(ticketIds != null)
					ticketIds.add(ticketStatusDetails.getTicketId());

					if (ticketIds != null && !ticketIds.isEmpty())
						statusTicketIds.put(ticketStatusDetails
								.getStatus(), ticketIds);
					else
						statusTicketIds.put(ticketStatusDetails
								.getStatus(), new ArrayList<Long>());

				} else
				{
					log.debug("statusTicketIds contains "
							+ ticketStatusDetails.getStatus());

					List<Long> ticketIds = new ArrayList<>();
					ticketIds.add(ticketStatusDetails.getTicketId());
					statusTicketIds
							.put(ticketStatusDetails.getStatus(), ticketIds);

				}
			}
		}

		log.debug("statusTicketIds " + statusTicketIds);

		List<SalesCountVO> salesCountVOList = new ArrayList<>();

		if (!statusTicketIds.isEmpty())
		{
			Set<Long> totalValue = new LinkedHashSet<>();

			for (Map.Entry<Integer, List<Long>> entry : statusTicketIds
					.entrySet())
			{
				int status = entry.getKey();
				List<Long> value = entry.getValue();

				log.debug("###################  " + status + " value " + value);

				if (SalesCount.NEW_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.NEW, SalesCount.NEW_ID, value
									.size(), value));

				}
				if (ProspectType.HOT_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(ProspectType.HOT, ProspectType.HOT_ID, value
									.size(), value));

				}
				if (ProspectType.MEDIUM_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(ProspectType.MEDIUM, ProspectType.MEDIUM_ID, value
									.size(), value));

				}
				if (ProspectType.COLD_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(ProspectType.REJECTED, ProspectType.COLD_ID, value
									.size(), value));

				}
				if (SalesCount.FEASIBILITY_PENDING_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.FEASIBILITY_PENDING, SalesCount.FEASIBILITY_PENDING_ID, value
									.size(), value));

				}
				if (SalesCount.FEASIBILITY_APPROVED_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.FEASIBILITY_APPROVED, SalesCount.FEASIBILITY_APPROVED_ID, value
									.size(), value));

				}
				if (SalesCount.DOCUMENT_PENDING_WITH_CFE_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.DOCUMENT_PENDING_WITH_CFE, SalesCount.DOCUMENT_PENDING_WITH_CFE_ID, value
									.size(), value));

				}
				if (SalesCount.POA_DOCUMENT_PENDING_WITH_CFE_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.POA_DOCUMENT_PENDING_WITH_CFE , SalesCount.POA_DOCUMENT_PENDING_WITH_CFE_ID, value
									.size(), value));

				}
				
				if (SalesCount.POI_DOCUMENT_PENDING_WITH_CFE_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.POI_DOCUMENT_PENDING_WITH_CFE, SalesCount.POI_DOCUMENT_PENDING_WITH_CFE_ID, value
									.size(), value));

				}
				if (SalesCount.PAYMENT_PENDING_WITH_CFE_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.PAYMENT_PENDING_WITH_CFE, SalesCount.PAYMENT_PENDING_WITH_CFE_ID, value
									.size(), value));

				}
				if (SalesCount.PERMISSION_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.PERMISSION, SalesCount.PERMISSION_ID, value
									.size(), value));

				}

				if (SalesCount.CAF_ACCEPTED_BY_CFE_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.CAF_ACCEPTED_BY_CFE, SalesCount.CAF_ACCEPTED_BY_CFE_ID, value
									.size(), value));

				}
				if (SalesCount.DOOR_KNOCK_ID == status)
				{
					salesCountVOList
							.add(new SalesCountVO(SalesCount.DOOR_KNOCK, SalesCount.DOOR_KNOCK_ID, value
									.size(), value));

				}
				if (SalesCount.TOTAL_ID == status)
				{
					totalValue.addAll(value);
				
				}

/*				totalValue.addAll(value);*/
			}

			salesCountVOList
					.add(new SalesCountVO(SalesCount.TOTAL, SalesCount.TOTAL_ID, totalValue
							.size(), new ArrayList<Long>(totalValue)));
		}

		return salesCountVOList;

	}

	public Map<Integer, List<Long>> populateSalesCountMap()
	{
		Map<Integer, List<Long>> statusTicketIds = new LinkedHashMap<>();
		statusTicketIds.put(SalesCount.DOOR_KNOCK_ID, new ArrayList<Long>());
		statusTicketIds.put(SalesCount.NEW_ID, new ArrayList<Long>());
		statusTicketIds
				.put(SalesCount.FEASIBILITY_PENDING_ID, new ArrayList<Long>());
		statusTicketIds
				.put(SalesCount.FEASIBILITY_APPROVED_ID, new ArrayList<Long>());
		statusTicketIds.put(SalesCount.PERMISSION_ID, new ArrayList<Long>());
		statusTicketIds.put(SalesCount.TOTAL_ID, new ArrayList<Long>());
		statusTicketIds.put(ProspectType.MEDIUM_ID, new ArrayList<Long>());
		statusTicketIds.put(ProspectType.HOT_ID, new ArrayList<Long>());
		statusTicketIds.put(ProspectType.COLD_ID, new ArrayList<Long>());
		statusTicketIds
				.put(SalesCount.DOCUMENT_PENDING_WITH_CFE_ID, new ArrayList<Long>());
		statusTicketIds
				.put(SalesCount.PAYMENT_PENDING_WITH_CFE_ID, new ArrayList<Long>());
		statusTicketIds
		.put(SalesCount.POA_DOCUMENT_PENDING_WITH_CFE_ID, new ArrayList<Long>());
		statusTicketIds
		.put(SalesCount.POI_DOCUMENT_PENDING_WITH_CFE_ID, new ArrayList<Long>());

		statusTicketIds
				.put(SalesCount.CAF_ACCEPTED_BY_CFE_ID, new ArrayList<Long>());
		return statusTicketIds;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.sales.SalesCoreServcie#callSiebel(java.lang.Long, java.lang.Long, java.lang.String)
	 */
	@Override
	public void updateSalesActivityInCRM(Long ticketId, Long customerId, String prospectNumber) {
		
		SalesActivityUpdateInCRMVo salesActivityUpdateInCRMVo = new SalesActivityUpdateInCRMVo();
		
		salesActivityUpdateInCRMVo.setTicketId(ticketId);
		
		if(customerId != null )
		salesActivityUpdateInCRMVo.setCustomerId(customerId);
		
		if (prospectNumber != null )
			salesActivityUpdateInCRMVo.setProspectNumber(prospectNumber);
		
		log.info("salesActivityUpdateInCRM value in sales coreservicesimpl "+salesActivityUpdateInCRMVo);
		
		crmUpdateRequestHandler.salesActivityUpdateInCRM(salesActivityUpdateInCRMVo);
	}
	
}
