/**
 * 
 */
package com.cupola.fwmp.service.sales;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.sales.PaymentUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

/**
 * @author aditya
 * 
 */
public interface SalesCoreServcie
{
	APIResponse getCount();

	APIResponse updateBasicInfo(UpdateProspectVO updateProspectVO,boolean isValuesFromCRM, Long assignToFromCRM,String deDupFlag);

	APIResponse updateTariff(TariffPlanUpdateVO planUpdateVO);

	APIResponse updatePayment(PaymentUpdateVO paymentUpdateVO);

	APIResponse getRouter(Long cityId);

	APIResponse seSummary();

	APIResponse updatePermission(UpdateProspectVO updateProspectVO);

	void reassign(ReassignTicketVo reassignTicketVo);

	APIResponse updatePropspectType(UpdateProspectVO updateProspectVO);

	APIResponse getSalesCounts();
	
	/**
	 * @author kiran
	 * void
	 * @param ticketId
	 * @param customerId
	 * @param prospectNumber
	 */
	void updateSalesActivityInCRM(Long ticketId , Long customerId , String prospectNumber);
}
