package com.cupola.fwmp.service.ekyc;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class KycDetails
{
	/* "uid":"999922176052",
     "postoffice":"Paingarapilly",
     "phone":null,
     "houseno":"Kanjiramkolath",
     "street":"Thuruthikkara",
     "state":"Kerala",
     "vtcname":"Mulamthuruthy",
     "photo":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHsAXwMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAGBwMEBQIBCAD/xAA6EAABAgQCBgcGBQUBAAAAAAABAgMABAUREiEGEzFBUbEHIiNhcYGRFDI0cpKhJDNCU8EVUmKi0eH/xAAZAQACAwEAAAAAAAAAAAAAAAABAgMEBQD/xAAiEQACAgICAgIDAAAAAAAAAAAAAQIRAzESIQRBIjITI1H/2gAMAwEAAhEDEQA/ADdhlrUN9mj3B+kcIk1LX7SPpEesDsG/kHKJALRTJSPUtftI+kR4WWv2kfSIlMeWgnEeoa/aR9IiN5Ms02px1LSEpFypQAAge010xltGpfVthL9QcF22b5JH9yu7u3/eE3WK/VayvWVGcdWL3CL4Up8EjKHUWwDuGkWjxnBJifkzMK2JBGfdfZfujRaXKPjEyGljilIMfN2BVrpAHjvixIVGbkngqVfdYeSci2soN/5g8AWfRhZa3No+kRyphv8AaR9IgG0L09M881Tq6EtzS+qzMCwS4dyVcFfY928/MJTQaKymWv2kfSIoVBlsSy+yRu/SOMaihFGoj8Mvy5wQGwx+Q18g5RJHDH5DfyDlEkAJ5aMvSSsNUKjzFQfz1aeon+5RyA9Y1bQrOmepWcp9MSoW6z7qQfJN/wDb0gpWwi8qM09PTj07PL1kw8rEondFFRxKzMSuHEYiKSVAAZxKgE6uq3nnlHM0ApxtYN1LTdV+MachRZ6eAS0wsg/qIyjiqUt2RmktPpsUi0LyV0guDq2isCAkId2bjDp0FraqzRU+0LxTUsdW6T+rgrzH3vCVcNwUqzgg6Pq7/SK4hD6rS8yAy4dyTfqn15x0l0d6HWqKVRH4Zzy5xeOabiKVR+FX5c4RCmux8O18g5RJaOWPyG/kHKO7QAnhNhHztpzPrqGks7MLN+1KE9yU9UD7H1h4aYVluh0R6ZUrCs9VHG54R87zLgfdWviSq3jDwRx0kFdrbTBpono2l/DMPICt+cCchKuBZXgcUpKC5ZKRkkbSb7oPNF9IHHUtsuNpSkjqkJsTu4kGI80nx6LGFK+9hlKyiGEhLYCQOAgc6QaSudkRNMNnXMi+Q94RYr1Ym6cBgCUgjapJUb9wjDGk04X3papInlKaVhcSFpTq7kAXSnxiDGpfZE02l8WL11fWvsEehN12OxQjV0npfsUymYQCWZi5zFs4zGkdRJvex9IuqSkrRTlFxdD60UnFz+jlPmHTdxTIC+9QyJ9QYt1H4Vzy5wN9GNURPUD2WwDkmrARxScwefpBLUfhV+XOI/YrNeX/ACG/kHKJNgziOX+Ha+QcokUoAXzNoABKdLFcFTrKaewvsJK6Vf5OHb6ZD1hfupU2jHbI3F40qopc5XJ8II1jky6sAm1ySTh/iM5busbA4DZEyVBY2aRSJeoSMssoGLVJIUBxEWRR5SReaSyhIVcAYRsEY+g1TW7Qm0IN1s3aVnw2fa0bcuVKUHkrSpwHPEcvCM6fJNxNKHFxUjZqspJvtpE4BbK14qSmjckl3Xpwub7kR6HH6gFpmg221hth/VzilIzrrBcYSrGGzbEDfKBbSHpMwuk5LaKW1kAoOgJ7oAGD2BO+0E3SJPmYXLSyjncrI7oFm1HASckcYuYF+so+Q/2DB6IEETdRezwqQgH1Nv5hjVHOVX5c4CuinV/0her98vHH5Wt9j94NKh8Kvy5iGeyBmxLn8O38g5RDMuOhNmQMXeYkl/h2/kHKFZ0laSTMhpA1LUuorZLbd3kNKGS91++3HiIFHJGd0p0Myupqyyyl957VrS2DdeRIUTxFuEL9ZBRbYY2q/WajV3WzUJpUypGTdk4UJ4m28njGO6sKJu0ANw3iJVoDNjQuoLkaqUX7J1PXHhsPOGIuSbmAFtN4rm+RIv6QttF5Rbs8HSnqpEMRh2Ylhdk+Rin5H3tF/wAa1AtS1JxrGukkpR/ldRPqYjrc3KUtpS04W0gZpAtFeaqdQCDgASeMAmkj8w6u8w4VK4bhEcIubpkuWbirMyozqqjPOTj5yOSU9wiqXVOe8bAx000X3UNp3mOnChtvVDM4o0FS6MyVvsKdAtKGqFNuNzTa1Sr+EFbYvqyL523/APkN2ZdS9T9a2rEhaUqSriDaFp0d6IIn1f1CpyyVSR/KbWfzDvJHDnDOnwEyigBYAAADxEI6sBrSi8bDe33ByivM0mVdCgqWZUlWZu2Dc98W5NNpZr5ByiyAPLeIyJ5JN22TJICJ/QChzyiTLFlZz7FWHlFZfRfRlyim5fXJmBmha3CRfgRwg5dTgmGeCiQfS/8AETAZx35cnpjdJinaoyqW8qXdl1IWg2IteLYQSPdUPEQxKnTkTzYUgAPJFgTsI4GBx2XLKy26jAobUkQ8cnLey5jkmgddZWtuyQIANIGFreeUrYg2ENKYYCQSkD1gS0hp2vHZoOM7AnfE+KXGQMseUQCYsyQrYSCBH7DibNrAk53i5XJQ09UvLbXsBWs8LnZFBggXKhdMX4u1ZnNU6G90YzxfopllKzl1FNu4i4/n0gpqJ/DL8ucKbo9rSKfVTLOnCiYyBJyB3Q051wKk1kHhzhHugMIZUfh2vkHKJCbRkSGkcupltK5d5PVGYwnd43jUZmZacQVSzgVbaNhHiDnGK20+0WHjlHZIbOuNneld/wDU/wDY6EBtU0nqEnUphqnSHtTMsSl9xdwhBNssWwHZ6xbYq9b9hXPKlZaYbFyGmHQCAMjt4b8/LdDRxyq2I32FOwx+cDbqcL7aHE8FAGBUaRVpCC6/o2+WuLL6XCB4JuYiZ0/p67h2QqLdtp1QUB94b8Un6OTaCNyk05zayodwWf8AsZ09S5NhBMtLgLItiJz9Y4Y0wobpsZzVq/tcbUDHU1XaW6ttlufl7rVmdYMhmf4tC8JD82KfTemIbSqafulTZsCNhHCAh0BISW1dU7e6HLp7SmKrT1plJhpahZRCFhWQ3wmXmlJCsVrJyuN9o0/ElcKfoins8xFBCkqVcZ3B2Q3dFNIGq5Qg2Vj21hCUuo3nPJXnaFMZULAJWGxhJ62+NvQFT8vpF2DRcxMKS4QbYE5G58wB5xLlipK1tCK9DMklWZb+UcotF9TXaNKKHEZpUNojDlJl4NosvcNw4R7MTL2FXX3cBGc4mu2qMSfrs1Pe1pC7NPzOuUnv4eH/AARsyTjr2jCVBtTimJtTjim1AFIIHqCeQgReJaKNX1ezQrzwjOLtNqE1LtTCWXigONlK7AZjbFlwVdGU32btOeqk1NGd0fRMrfb+I1hbINzcWGXfeNSpzsy+DPVOkvyExLouH2rpS8SQEpUbcTfbfI8Yq9H7yxKzyQRbWjakHdGnXX3H6jRmHlBTKnypTeEWUQMrjfEcl8tBTK1QXVJ+hiaqcuC02rECBZaRxzGYjAq9JCpRl2UaLylo1iZjAQkdwH2PfeGMubeWkoWoKQoEFJSLEekD+i80/LzdUk2XCmXae7Nu1wm+214WMmlaCBDaJxDZcS41KPoSUKU02VKVfvuQNwvaMCvS7SFMPSmL3Brmydigbk57bw5dI3luUScx4T2R/SIVa1F3EXLKJSq9xwSbRPik27BLQP1SdROuIOrU1hAFrwwejtlhyhvOyzeF0rwuLIzVYi2fDOFnMjrHxtB1Tp+ak9H5NqVdLSFouoJAzOW+G8mPwSX9Ox9s/9k=",
     "pincode":"682314",
     "landmark":null,
     "email":null,
     "dob":"1983­05­09",
     "name":"Ajith K George",
     "gender":"M",
     "locality":null,
     "careof":"S\/O: George K K",
     "district":"Ernakulam"*/
	
	private String uid;
	private String name;
	private String careof ;
	private String gender ;
	private String email ;
	private String postoffice ;
	private String dob;
	private String phone ;
	private String houseno ;
	private String locality ;
	private String street ;
	private String vtcname ;
	private String landmark ;
	private String district ;
	private String pincode ;
	private String state ;
	private String completeAddress;
	private String photo  ;
	@JsonIgnore
	@JsonProperty("UidForDisplay")
    private String UidForDisplay;
    @JsonIgnore
    @JsonProperty("UidReferenceKey")
    private String UidReferenceKey;
	
	public String getUid()
	{
		return uid;
	}
	public void setUid(String uid)
	{
		this.uid = uid;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getCareof()
	{
		return careof;
	}
	public void setCareof(String careof)
	{
		this.careof = careof;
	}
	public String getGender()
	{
		return gender;
	}
	public void setGender(String gender)
	{
		this.gender = gender;
	}
	public String getPostoffice()
	{
		return postoffice;
	}
	public void setPostoffice(String postoffice)
	{
		this.postoffice = postoffice;
	}
	public String getPhone()
	{
		return phone;
	}
	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	public String getHouseno()
	{
		return houseno;
	}
	public void setHouseno(String houseno)
	{
		this.houseno = houseno;
	}
	public String getStreet()
	{
		return street;
	}
	public void setStreet(String street)
	{
		this.street = street;
	}
	public String getVtcname()
	{
		return vtcname;
	}
	public void setVtcname(String vtcName)
	{
		this.vtcname = vtcName;
	}
	public String getDistrict()
	{
		return district;
	}
	public void setDistrict(String district)
	{
		this.district = district;
	}
	public String getPincode()
	{
		return pincode;
	}
	public void setPincode(String pinCode)
	{
		this.pincode = pinCode;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getCompleteAddress()
	{
		if (completeAddress != null)
			return this.completeAddress;

		StringBuilder address = new StringBuilder("");

		if (this.houseno != null)
			address.append(this.houseno).append(", ");
		else
			address.append("");
		if (this.street != null)
			address.append(this.street).append(", ");
		else
			address.append("");
		if (this.vtcname != null)
			address.append(this.vtcname).append(", ");
		else
			address.append("");
		if (this.locality != null)
			address.append(this.locality).append(", ");
		else
			address.append("");
		if (this.landmark != null)
			address.append(this.landmark).append(", ");
		else
			address.append("");
		if (this.postoffice != null)
			address.append(this.postoffice).append(", ");
		else
			address.append("");
		if (this.district != null)
			address.append(this.district).append(", ");
		else
			address.append("");

		if (this.state != null)
			address.append(this.state).append(", ");
		else
			address.append("");
		if (this.pincode != null)
			address.append(this.pincode).append(", ");
		else
			address.append("");
		this.completeAddress = address.toString();
		return this.completeAddress;
	}
	public void setCompleteAddress(String completeAddress)
	{
		this.completeAddress = completeAddress;
	}
	@Override
	public String toString()
	{
		return "KycDetails [uid=" + uid + ", name=" + name + ", careof="
				+ careof + ", gender=" + gender + ", postoffice=" + postoffice
				+ ", phone=" + phone + ", houseno=" + houseno + ", street="
				+ street + ", vtcName=" + vtcname + ", district=" + district
				+ ", pinCode=" + pincode + ", state=" + state
				+ ", completeAddress=" + getCompleteAddress() + "]";
	}
	public String getPhoto()
	{
		return photo;
	}
	public void setPhoto(String photo)
	{
		this.photo = photo;
	}
	public String getLandmark()
	{
		return landmark;
	}
	public void setLandmark(String landmark)
	{
		this.landmark = landmark;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getDob()
	{
		return dob;
	}
	public void setDob(String dob)
	{
		this.dob = dob;
	}
	public String getLocality()
	{
		return locality;
	}
	public void setLocality(String locality)
	{
		this.locality = locality;
	}
	
	@JsonProperty("UidForDisplay")
	public String getUidForDisplay() {
		return UidForDisplay;
	}
	@JsonProperty("UidForDisplay")
	public void setUidForDisplay(String uidForDisplay) {
		UidForDisplay = uidForDisplay;
	}
	@JsonProperty("UidReferenceKey")
	public String getUidReferenceKey() {
		return UidReferenceKey;
	}
	@JsonProperty("UidReferenceKey")
	public void setUidReferenceKey(String uidReferenceKey) {
		UidReferenceKey = uidReferenceKey;
	}
	
	
}
