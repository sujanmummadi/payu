/**
 * 
 */
package com.cupola.fwmp.service.router;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.router.RouterDAO;
import com.cupola.fwmp.persistance.entities.Router;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * @author aditya
 * 
 */
public class RouterCoreServiceImpl implements RouterCoreService
{
	@Autowired
	RouterDAO routerDAO;

	@Override
	public APIResponse getRouter(Long cityId)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(routerDAO.getRouter(cityId));
	}

	@Override
	public APIResponse bulkRouterUpload(List<Router> routers)
	{
		return ResponseUtil.createSuccessResponse()
				.setData(routerDAO.bulkRouterUpload(routers));
	}
}
