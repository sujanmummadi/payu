package com.cupola.fwmp.service.router;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Router;
import com.cupola.fwmp.response.APIResponse;

public interface RouterCoreService
{
	APIResponse getRouter(Long cityId);
	APIResponse bulkRouterUpload(List<Router> routers);
}
