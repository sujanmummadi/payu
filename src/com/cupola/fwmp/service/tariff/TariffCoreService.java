package com.cupola.fwmp.service.tariff;

import java.util.List;

import com.cupola.fwmp.filters.TariffFilter;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;

public interface TariffCoreService {

	public APIResponse getTariffById(Long id);

	public APIResponse getAllTariff();

	public APIResponse deleteTariff(TariffPlan tariff);

	public APIResponse getTariffByCity(TariffFilter filter);

	public APIResponse UpdateCustomerTariff(TariffPlanUpdateVO tariffVo);
	
	public APIResponse addTariffPlan(TariffPlan tariffVo);
	public APIResponse getTariffByCity();

	APIResponse bulkTariffPlanUpload(List<TariffPlan> tariffPlans);

	APIResponse getTariffByPackageCode(String planCode);

	boolean getTariffByPlanCode(String planCode);

	/**@author aditya 5:11:28 PM Apr 7, 2017
	 * APIResponse
	 * @param filter
	 * @return
	 */
	APIResponse getPlanCategoeryByCity(TariffFilter filter);
	
	/**
	 * @author kiran
	 * APIResponse
	 * @param tariff
	 * @return
	 */
	public APIResponse updateTariff(TariffPlan tariff);
}
