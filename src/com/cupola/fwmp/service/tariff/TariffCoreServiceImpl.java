package com.cupola.fwmp.service.tariff;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cupola.fwmp.dao.tariff.TariffDAO;
import com.cupola.fwmp.filters.TariffFilter;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.sales.TariffDetailsVO;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanVo;

public class TariffCoreServiceImpl implements TariffCoreService
{

	private Logger log = Logger
			.getLogger(TariffCoreServiceImpl.class.getName());

	TariffDAO tariffDAO;

	public void setTariffDAO(TariffDAO tariffDAO)
	{
		this.tariffDAO = tariffDAO;
	}

	@Override
	public APIResponse getAllTariff()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse deleteTariff(TariffPlan tariff)
	{
		TariffPlan result = tariffDAO.deleteTariff(tariff);
		if(result != null)
			return ResponseUtil.createDeleteSuccessResponse();
		else
			return ResponseUtil.createDeleteFailedResponse();
	}

	@Override
	public APIResponse getTariffById(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getTariffByCity(TariffFilter filter)
	{
		Long cityId = filter.getCityId();

		List<TariffPlanVo> tariffList = tariffDAO.getTariffByCity(filter);
		log.info(tariffList);

		TariffDetailsVO tariffDetailsVO = new TariffDetailsVO();

		if (tariffList.isEmpty())
		{
			return ResponseUtil.createRecordNotFoundResponse();

		} else
		{
			Map<String, List<String>> mapOfTariffPackageList = new HashMap<>();

			for (TariffPlanVo tariff : tariffList)
			{

				if (mapOfTariffPackageList
						.containsKey(tariff.getPlanCategoery()))
				{

					List<String> tariffPackageList = mapOfTariffPackageList
							.get(tariff.getPlanCategoery());

					log.debug("tariffPackageList before adding "
							+ tariff.getPackageName());

					tariffPackageList.add(tariff.getPackageName());

					mapOfTariffPackageList
							.put(tariff.getPlanCategoery(), tariffPackageList);

					log.debug("tariffPackageList after adding "
							+ tariff.getPackageName()
							+ " mapOfTariffPackageList "
							+ mapOfTariffPackageList);

				} else
				{

					List<String> tariffPackageList = new ArrayList<>();

					tariffPackageList.add(tariff.getPackageName());

					mapOfTariffPackageList
							.put(tariff.getPlanCategoery(), tariffPackageList);

				}

			}

			tariffDetailsVO.setMapOfTariffPackageList(mapOfTariffPackageList);

			tariffDetailsVO.setTariffPlanVoList(tariffList);

		}

		log.info("Total tariff details for " + cityId + " is "
				+ tariffList.size());

		return ResponseUtil.createSuccessResponse().setData(tariffList);
	}

	@Override
	public APIResponse getPlanCategoeryByCity(TariffFilter filter)
	{
		List<TariffPlanVo> tariffPlan = tariffDAO
				.getPlanCategoeryByCity(filter);

		List<TypeAheadVo> planCategoery = new ArrayList<>();

		if (!tariffPlan.isEmpty())
		{
			long id = 1;
			for (Iterator<TariffPlanVo> iterator = tariffPlan
					.iterator(); iterator.hasNext();)
			{
				TariffPlanVo string = (TariffPlanVo) iterator.next();

				planCategoery.add(new TypeAheadVo(id ++, string.getPlanCategoery()));

			}
		}

		return ResponseUtil.createSuccessResponse().setData(planCategoery);
	}

	@Override
	public APIResponse getTariffByCity()
	{
		Long cityId = null;

		cityId = AuthUtils.getCurrentUserCity().getId();

		List<TariffPlanVo> tariffList = tariffDAO.getTariffByCity(cityId);

		if (tariffList != null && tariffList.isEmpty())
		{
			return ResponseUtil.createRecordNotFoundResponse();

		}

		log.info("Total tariff details for " + cityId + " is "
				+ tariffList != null ? tariffList.size() : null);

		return ResponseUtil.createSuccessResponse().setData(tariffList);
	}

	@Override
	public APIResponse getTariffByPackageCode(String planCode)
	{
		List<TariffPlanVo> tariffList = tariffDAO
				.getTariffByPlanCode(planCode);

		if (tariffList != null && tariffList.isEmpty())
		{
			return ResponseUtil.createRecordNotFoundResponse();

		}

		return ResponseUtil.createSuccessResponse().setData(tariffList);
	}

	@Override
	public boolean getTariffByPlanCode(String planCode)
	{
		List<TariffPlanVo> tariffList = tariffDAO
				.getTariffByPlanCode(planCode);

		if (tariffList != null && tariffList.isEmpty())
		{
			return false;
		}

		return true;
	}

	@Override
	public APIResponse UpdateCustomerTariff(TariffPlanUpdateVO tariffVo)
	{

		int row = tariffDAO.UpdateCustomerTariff(tariffVo);

		if (row < 1)
		{
			return ResponseUtil.createUpdateFailedResponse().setData(tariffVo);
		}

		return ResponseUtil.createUpdateSuccessResponse().setData(tariffVo);
	}

	@Override
	public APIResponse addTariffPlan(TariffPlan tariffPlanVo)
	{

		int row = tariffDAO.addTariff(tariffPlanVo);
		if (row < 1)
		{
			return ResponseUtil.createUpdateFailedResponse()
					.setData(tariffPlanVo);
		}

		return ResponseUtil.createSaveSuccessResponse().setData(tariffPlanVo);

	}

	@Override
	public APIResponse bulkTariffPlanUpload(List<TariffPlan> tariffPlans)
	{
		int resultCount = tariffDAO.bulkTariffPlanUpload(tariffPlans);

		return ResponseUtil.createSaveSuccessResponse()
				.setData("Total Tariff plans inseted are " + resultCount);
	}

	@Override
	public APIResponse updateTariff(TariffPlan tariff) {
		TariffPlan  tarif = tariffDAO.updateTariff(tariff);
		if(tarif != null)
			return ResponseUtil.createSaveSuccessResponse();
		return ResponseUtil.createSaveFailedResponse();
	}
}
