 /**
 * @Author snoor  Nov 27, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.fr.configuration;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.fr.FrDeploymentSettingDao;
import com.cupola.fwmp.persistance.entities.FlowType;
import com.cupola.fwmp.persistance.entities.FrDeploymentSetting;
import com.cupola.fwmp.persistance.entities.FrSymptomNames;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.BranchFlowConfigVO;
import com.cupola.fwmp.vo.FrSymptomCodesVo;

/**
 * @Author snoor  Nov 27, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FRConfigurationServiceImp implements FRConfigurationService {
	
	
	@Autowired
	private FrDeploymentSettingDao frDeploymentSettingDao ;

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#getMQLevelData(java.lang.String)
	 */
	@Override
	public List<FrDeploymentSetting> getMQLevelData(String level) {

		return frDeploymentSettingDao.getMQLevelData(level);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#addCRMData(com.cupola.fwmp.persistance.entities.FrDeploymentSetting)
	 */
	@Override
	public APIResponse addCRMData(FrDeploymentSetting deploymentSetting) {
		return frDeploymentSettingDao.createFrDeploymentEntry(deploymentSetting);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#updateCRMData(com.cupola.fwmp.persistance.entities.FrDeploymentSetting)
	 */
	@Override
	public APIResponse updateCRMData(FrDeploymentSetting deploymentSetting) {
		return frDeploymentSettingDao.updateFrDeploymentSetting(deploymentSetting);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#deleteCRMData(com.cupola.fwmp.persistance.entities.FrDeploymentSetting)
	 */
	@Override
	public APIResponse deleteCRMData(FrDeploymentSetting deploymentSetting) {
		return frDeploymentSettingDao.deleteFrDeploymentSetting(deploymentSetting);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#getbranchFlowData(java.lang.String)
	 */
	@Override
	public List<BranchFlowConfigVO> getbranchFlowData(String cityId, String branchId) {
		return frDeploymentSettingDao.getBranchFlowConfigurations(cityId,branchId);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#addbranchFlowData(com.cupola.fwmp.vo.BranchFlowConfigVO)
	 */
	@Override
	public APIResponse addbranchFlowData(BranchFlowConfigVO branchFlowConfigVO) {
		return frDeploymentSettingDao.addBranchFlowConfiguration(branchFlowConfigVO);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#updatebranchFlowData(com.cupola.fwmp.vo.BranchFlowConfigVO)
	 */
	@Override
	public APIResponse updatebranchFlowData(BranchFlowConfigVO branchFlowConfigVO) {
		return frDeploymentSettingDao.upDateBranchFlowConfiguration(branchFlowConfigVO);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#deletebranchFlowData(com.cupola.fwmp.vo.BranchFlowConfigVO)
	 */
	@Override
	public APIResponse deletebranchFlowData(long branchFlowId) {
		return frDeploymentSettingDao.deleteBranchFlowConfiguration(branchFlowId);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#getAllbranchFlowData()
	 */
	@Override
	public List<BranchFlowConfigVO> getAllbranchFlowData() {
		return frDeploymentSettingDao.getAllBranchFlowConfigurations();
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.configuration.FRConfigurationService#getAllFlowTypeData()
	 */
	@Override
	public List<FlowType> getAllFlowTypeData() {
		return frDeploymentSettingDao.getAllFlowTypes();
	}

	@Override
	public List<FrSymptomCodesVo> getAllSymptomTypes() {
		return frDeploymentSettingDao.getAllSymptomTypes();
	}

	@Override
	public List<FrSymptomNames> getSymptomForFlowId(long flowId) {
		return frDeploymentSettingDao.getSymptomNamesFromFlowId(flowId);
	}

	@Override
	public Map<Integer, List<String>> getSymptomNamesForFlowId() {
		return frDeploymentSettingDao.getAllSymptomNamesForFlowIds();
	}

	@Override
	public List<FrSymptomNames> getAllSymptomNames() {
		return frDeploymentSettingDao.getAllSymptomNames();
	}

	@Override
	public APIResponse updateSymtoms(Integer flowId, List<FrSymptomNames> symptomNames) {
		return frDeploymentSettingDao.updateSymptomNames(flowId, symptomNames);
	}

	@Override
	public APIResponse addSymptomName(String symptomName) {
		return frDeploymentSettingDao.addSymptom(symptomName);
	}

}
