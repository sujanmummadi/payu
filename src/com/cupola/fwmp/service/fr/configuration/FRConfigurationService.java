 /**
 * @Author snoor  Nov 27, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.fr.configuration;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.FlowType;
import com.cupola.fwmp.persistance.entities.FrDeploymentSetting;
import com.cupola.fwmp.persistance.entities.FrSymptomNames;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.BranchFlowConfigVO;
import com.cupola.fwmp.vo.FrSymptomCodesVo;

/**
 * @Author snoor  Nov 27, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface FRConfigurationService {
	
	List<FrDeploymentSetting> getMQLevelData( String level );
	
	APIResponse addCRMData( FrDeploymentSetting deploymentSetting );
	
	APIResponse updateCRMData( FrDeploymentSetting deploymentSetting );

	APIResponse deleteCRMData(FrDeploymentSetting deploymentSetting);

	List<BranchFlowConfigVO> getbranchFlowData(String cityId, String branchId);

	APIResponse addbranchFlowData(BranchFlowConfigVO branchFlowConfigVO);

	APIResponse updatebranchFlowData(BranchFlowConfigVO branchFlowConfigVO);

	APIResponse deletebranchFlowData(long branchFlowId);

	List<BranchFlowConfigVO> getAllbranchFlowData();

	List<FlowType> getAllFlowTypeData();

	List<FrSymptomCodesVo> getAllSymptomTypes();

	List<FrSymptomNames> getSymptomForFlowId(long flowid);

	Map<Integer, List<String>> getSymptomNamesForFlowId();

	List<FrSymptomNames> getAllSymptomNames();

	APIResponse updateSymtoms(Integer flowId, List<FrSymptomNames> symptomNames);

	APIResponse addSymptomName(String symptomName);
}
