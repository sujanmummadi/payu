package com.cupola.fwmp.service.fr;

import java.util.ArrayList;
import java.util.List;

public class FrTicketLockInfoVo
{
	private List<Long> ticketId;
	private List<Long> blockedTicketIds;
	private Long currentAssignedTo;
	private boolean locked;
	private Long currentUnlocked;
	private Long totalUnlockedDb;
	
	public FrTicketLockInfoVo(){
		
		this.blockedTicketIds = new ArrayList<Long>();
		this.ticketId = new ArrayList<Long>();
	}

	public FrTicketLockInfoVo(List<Long> ticketId, Long currentAssignedTo,
			boolean locked,Long currentUnlocked) {
		super();
		this.ticketId = ticketId;
		this.currentAssignedTo = currentAssignedTo;
		this.locked = locked;
		this.currentUnlocked = currentUnlocked;
	}

	public Long getCurrentUnlocked() {
		return currentUnlocked;
	}

	public void setCurrentUnlocked(Long currentUnlocked) {
		this.currentUnlocked = currentUnlocked;
	}

	public List<Long> getTicketId() {
		return ticketId;
	}

	public void setTicketId(List<Long> ticketId) {
		this.ticketId = ticketId;
	}

	public Long getCurrentAssignedTo() {
		return currentAssignedTo;
	}

	public void setCurrentAssignedTo(Long currentAssignedTo) {
		this.currentAssignedTo = currentAssignedTo;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Override
	public String toString() {
		return "FrTicketLockInfoVo [ticketId=" + ticketId
				+ ", blockedTicketIds=" + blockedTicketIds
				+ ", currentAssignedTo=" + currentAssignedTo + ", locked="
				+ locked + ", currentUnlocked=" + currentUnlocked
				+ ", totalUnlockedDb=" + totalUnlockedDb + "]";
	}

	public List<Long> getBlockedTicketIds() {
		return blockedTicketIds;
	}

	public void setBlockedTicketIds(List<Long> blockedTicketIds) {
		this.blockedTicketIds = blockedTicketIds;
	}

	public Long getTotalUnlockedDb() {
		return totalUnlockedDb;
	}

	public void setTotalUnlockedDb(Long totalUnlockedDb) {
		this.totalUnlockedDb = totalUnlockedDb;
	}
	
	
	
}
