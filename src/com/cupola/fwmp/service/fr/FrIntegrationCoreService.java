package com.cupola.fwmp.service.fr;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.fr.FrIntegrationVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;

public interface FrIntegrationCoreService
{

	public	APIResponse getDeviceFreeportStatus(FrIntegrationVo inteVo );

	public APIResponse getDeviceInfo(FrIntegrationVo inteVo);

	public APIResponse getDeviceReachabilityStatus(FrIntegrationVo inteVo);

	public APIResponse getDevicePortStatus(FrIntegrationVo inteVo);

	public APIResponse getFiberPowerStatus(FrIntegrationVo inteVo);

	public APIResponse getSplicingPortWiseStatus(FrIntegrationVo inteVo);

	public APIResponse getSplicingFxWiseCheck(FrIntegrationVo inteVo);

	public APIResponse updateEtrInMQ(TicketModifyInMQVo vo);

	public APIResponse updateCommentOrStatusInMQ(TicketModifyInMQVo vo);

}
