package com.cupola.fwmp.service.fr;

import java.io.IOException;
import java.util.Date;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.FRConstant.DeviceActionType;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.integ.closeticket.MQSCloseTicketDAO;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectResponse;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectStatus;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.fr.FrIntegrationVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;
import com.cupola.fwmp.vo.fr.UtilityLogger;
import com.thoughtworks.xstream.XStream;

public class FrIntegrationCoreServiceImp implements FrIntegrationCoreService
{
	private static final Logger LOGGER = Logger.getLogger(FrIntegrationCoreServiceImp.class);
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	@Autowired
	private TicketDAO tickeDao;
	
	@Autowired
	private FrTicketService frService;
	
	@Autowired
	private FrTicketDao frDao;
	
	@Autowired
	private MQSCloseTicketDAO mqClosure;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public APIResponse getDeviceInfo( FrIntegrationVo inteVo )
	{
		if( inteVo == null || inteVo.getActionType() <= 0 )
			return ResponseUtil.nullArgument();
		
		/*if( inteVo != null)
			return ResponseUtil.createSuccessResponse().setData(getFROmOTHERACTResponse(inteVo));*/
		
		try 
		{
			long actionType = inteVo.getActionType();
			if ( actionType  == DeviceActionType.PING_DEVICE_API)
				return getDeviceReachabilityStatus(inteVo);
			else if( actionType  == DeviceActionType.PORT_STATUS_API)
				return getDevicePortStatus( inteVo );
			else if( actionType  == DeviceActionType.FIBER_POWER_API)
				return getFiberPowerStatus( inteVo );
			else if( actionType  == DeviceActionType.CX_PORT_AVAILABILITY)
				return getDeviceFreeportStatus( inteVo );
			else if( actionType  == DeviceActionType.FX_PORT_AVAILABILITY )
				return getDeviceFreeportStatus( inteVo );
			else if( actionType  == DeviceActionType.SPLICING_PORT_WISE_CHECK )
				return getSplicingPortWiseStatus( inteVo  );
			else if( actionType  == DeviceActionType.SPLICING_FX_CHECK )
				return getSplicingFxWiseCheck( inteVo );
			else
				return  ResponseUtil.createSuccessResponse().setData(" Provided data is Invalid : ");
		}
		catch (Exception e)
		{
			LOGGER.error("Error in getDeviceInfo() : ",e);
		}
		return ResponseUtil.createRecordNotFoundResponse();
	}
	
	@Override
	public APIResponse getSplicingFxWiseCheck( FrIntegrationVo inteVo )
	{

		if( inteVo == null || inteVo.getActionType() <= 0 )
			return ResponseUtil.nullArgument();
		
		long actionType = inteVo.getActionType();
		String url = "";
		if( actionType  == DeviceActionType.SPLICING_FX_CHECK)
		{
			url = definitionCoreService.getActForceUrl(DefinitionCoreService.SPLICING_FX_CHECK);
			url = url.replace("{0}", inteVo.getDeviceIp());
			url = url.replace("{1}", getCityName());
		}
		return ResponseUtil.createSuccessResponse().setData(getACTResponse(  url ));
	
	}
	
	@Override
	public APIResponse getSplicingPortWiseStatus( FrIntegrationVo inteVo  )
	{
		if( inteVo == null || inteVo.getActionType() <= 0 )
			return ResponseUtil.nullArgument();
		
		long actionType = inteVo.getActionType();
		String url = "";
		if( actionType  == DeviceActionType.SPLICING_PORT_WISE_CHECK )
		{
			url = definitionCoreService.getActForceUrl(DefinitionCoreService.SPLICING_PORT_WISE_CHECK);
			url = url.replace("{0}", inteVo.getDeviceIp());
			url = url.replace("{2}", getCityName());
			url = url.replace("{1}", inteVo.getPort());
		}
		return ResponseUtil.createSuccessResponse().setData(getACTResponse(  url ));
		
	}
	
	@Override
	public APIResponse getFiberPowerStatus( FrIntegrationVo inteVo )
	{
		if( inteVo == null || inteVo.getActionType() <= 0 )
			return ResponseUtil.nullArgument();
		
		long actionType = inteVo.getActionType();
		String url = "";
		if( actionType  == DeviceActionType.FIBER_POWER_API )
		{
			url = definitionCoreService.getActForceUrl(DefinitionCoreService.FIBER_POWER_API);
			url = url.replace("{0}", inteVo.getDeviceIp());
			url = url.replace("{2}", getCityName());
			url = url.replace("{1}", inteVo.getPort());
		}
		return ResponseUtil.createSuccessResponse().setData(getACTResponse(  url ));
	}
	
	@Override
	public APIResponse getDevicePortStatus( FrIntegrationVo inteVo )
	{
		if( inteVo == null || inteVo.getActionType() <= 0 )
			return ResponseUtil.nullArgument();
		
		long actionType = inteVo.getActionType();
		String url = "";
		if( actionType  == DeviceActionType.PORT_STATUS_API)
		{
			url = definitionCoreService.getActForceUrl(DefinitionCoreService.PORT_STATUS_API);
			url = url.replace("{0}", inteVo.getDeviceIp());
			url = url.replace("{2}", getCityName());
			url = url.replace("{1}", inteVo.getPort());
		}
		return ResponseUtil.createSuccessResponse().setData(getACTResponse(  url ));
	}
	
	@Override
	public APIResponse getDeviceReachabilityStatus( FrIntegrationVo inteVo )
	{
		if( inteVo == null || inteVo.getActionType() <= 0 )
			return ResponseUtil.nullArgument();
		
		long actionType = inteVo.getActionType();
		String url = "";
		if( actionType  == DeviceActionType.PING_DEVICE_API)
		{
			url = definitionCoreService.getActForceUrl(DefinitionCoreService.PING_DEVICE_API);
			url = url.replace("{0}", inteVo.getDeviceIp());
			url = url.replace("{1}", getCityName());
		}
		return ResponseUtil.createSuccessResponse().setData(getACTResponse(  url ));
	}
	
	@Override
	public APIResponse getDeviceFreeportStatus( FrIntegrationVo inteVo )
	{
		if( inteVo == null || inteVo.getActionType() <= 0 )
			return ResponseUtil.nullArgument();
		
		long actionType = inteVo.getActionType();
		String url = "";
		if( actionType  == DeviceActionType.CX_PORT_AVAILABILITY)
			url = definitionCoreService.getActForceUrl(DefinitionCoreService.CX_PORT_AVAILABILITY);
		else if (  actionType  == DeviceActionType.FX_PORT_AVAILABILITY )
			url = definitionCoreService.getActForceUrl(DefinitionCoreService.FX_PORT_AVAILABILITY);
		else
			return  ResponseUtil.createSuccessResponse().setData(" Provided data is Invalid : ");
		
		url = url.replace("{0}", inteVo.getDeviceIp());
		url = url.replace("{1}", getCityName());
		
		return ResponseUtil.createSuccessResponse().setData(getACTResponse(  url ));
	}
	
	private String getACTResponse( String url )
	{
		LOGGER.info(" ACT FORCE API URL : " + url);
		if( url == null || url.isEmpty() )
			return "Invalid url : "+url;
		
		UtilityLogger utilityLogger = getUtilityLogger(url);
		utilityLogger.setUtilityServerResponseTime(new Date().toString());
		
		String stringResponse = null;
		CloseableHttpResponse httpResponse = null;
		CloseableHttpClient httpclient = null;

		try
		{
			httpclient = HttpClients.createDefault();
			HttpGet httpPost = new HttpGet(url);
			httpResponse = httpclient.execute(httpPost);
			
			stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			httpclient.close();

		} 
		catch (Exception e)
		{
			LOGGER.error("Error while calling ACT FORCE  API  " ,e);

		}
		finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();

			} 
			catch (IOException e)
			{
				LOGGER.error("Error while closing http client connection ",e);
			}
			utilityLogger.setUtilityServerResponse(stringResponse);
			mongoTemplate.save(utilityLogger);
		}
		return stringResponse;
		}
	
	private UtilityLogger getUtilityLogger(String utilityServerURI)
	{
		UtilityLogger utilityLogger = new UtilityLogger();
		
		utilityLogger.setUtilityLoggerId(StrictMicroSecondTimeBasedGuid.newGuid());
		utilityLogger.setUtilityServerRequestTime(new Date().toString());
		utilityLogger.setUtilityServerURI(utilityServerURI);
		
		return utilityLogger;
		
	}
	
	private String getFROmOTHERACTResponse( FrIntegrationVo voInt )
	{
		
		LOGGER.info(" ACT FORCE API URL : " + getURl(voInt));
		if( getURl(voInt) == null || getURl(voInt).isEmpty() )
			return "Invalid url : "+ getURl(voInt);
		
		CloseableHttpResponse httpResponse = null;
		CloseableHttpClient httpclient = null;

		try
		{
			httpclient = HttpClients.createDefault();
			HttpGet httpPost = new HttpGet( getURl(voInt));
			httpResponse = httpclient.execute(httpPost);
			
			String stringResponse = EntityUtils
					.toString(httpResponse.getEntity());

			httpclient.close();
			return stringResponse;

		} 
		catch (Exception e)
		{
			LOGGER.error("Error while calling ACT FORCE  API  " ,e);

		}
		finally
		{
			try
			{
				if (httpResponse != null)
					httpResponse.close();
				httpclient.close();

			} 
			catch (IOException e)
			{
				LOGGER.error("Error while closing http client connection ",e);
			}
		}
		return null;
	}
	
	private String getCityName()
	{
		TypeAheadVo cityVo = AuthUtils.getCurrentUserCity();
		if( cityVo == null)
			return null;
		
		return cityVo.getName().toUpperCase();
	}
	
	private String getURl(FrIntegrationVo voInt)
	{
		String url = "http://202.83.23.113:9091/ToolTester/tool/device/"+getCityName()+"/"
				+voInt.getDeviceIp()+"/"+voInt.getActionType()+"/"+voInt.getPort();
		return url;
	}
	
	private APIResponse updateFrTicketInMQ( TicketModifyInMQVo vo )
	{
		if( vo == null || vo.getRequestParam() == null 
				|| vo.getRequestParam().isEmpty())
			
			return ResponseUtil.nullArgument();
		
		try
		{
			
			LOGGER.info("Modifying Ticket no : "+vo.getWorkOrderNumber()+" for city : "+vo.getCityName());
			if( vo.getCityName() != null )
			{
				if (vo.getCityName()
						.equalsIgnoreCase(CityName.HYDERABAD_CITY))
				{
					 vo.setCityName(CityName.HYDERABAD_CITY);
					 vo.setWorkOrderNumber(vo.getWorkOrderNumber());
					 vo.setRequestParam(vo.getRequestParam());
				}
				else
				{
					 vo.setWorkOrderNumber(vo.getWorkOrderNumber());
					 vo.setRequestParam(vo.getRequestParam());
				}
			}
			String mqResponse = mqClosure.modifyTicket(vo);
			if (mqResponse != null)
			{
				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream
						.fromXML(mqResponse);

				int errorCode = mqProspectResponse.getStatus()
						.getErrorNo();
				
				String remarks = mqProspectResponse.getStatus()
						.getMessage();
				
				if (errorCode == 0 )
				{
					/*
					 *  do some thing after closing ticket in MQ
					 *  either send the message or update the status 
					 *  
					 *  */
				}	
				return new APIResponse(errorCode, remarks);
			}
			else
				return ResponseUtil.noResponseFromMqServer();
		} 
		catch (Exception e)
		{
			LOGGER.error("Error while calling mq modifyTicket api ",e);
			return null;
		}
	}
	
	@Override
	public APIResponse updateEtrInMQ( TicketModifyInMQVo vo )
	{
		if( vo == null || vo.getTicketId() <= 0 
				|| vo.getEtrValue() == null || vo.getEtrValue().isEmpty())
			
			return ResponseUtil.nullArgument();
		
		LOGGER.info("Connecting to Mq to update Etr value...");
		try
		{
			FrTicket ticket = tickeDao.getFrTicketById( vo.getTicketId() );
			if( ticket == null )
				return ResponseUtil.noRecordUpdated().setData("Ticket number invalid");
			
			String wo = ticket.getWorkOrderNumber();
			
			/*City city = ticket.getCustomer() != null
					? ticket.getCustomer().getCity() : null; */
			
			TypeAheadVo city = frDao.getCityNameForTicket(vo.getTicketId());
					
			if( city != null )
				vo.setCityName(city.getName());
			
			vo.setWorkOrderNumber(wo);
			vo.setRequestParam(etrModifyRequestString(vo));
			
			return updateFrTicketInMQ( vo );
		} 
		catch (Exception e)
		{
			LOGGER.error("Error while calling mq modifyTicket api ",e);
			return null;
		}
	}
	
	@Override
	public APIResponse updateCommentOrStatusInMQ( TicketModifyInMQVo vo )
	{
		if( vo == null || vo.getTicketId() <= 0 
				|| vo.getRemarks() == null || vo.getRemarks().isEmpty())
			
			return ResponseUtil.nullArgument();
		
		LOGGER.info("Connecting to Mq to update Comments ...");
		try
		{
			FrTicket ticket = tickeDao.getFrTicketById( vo.getTicketId() );
			if( ticket == null )
				return ResponseUtil.noRecordUpdated().setData("Ticket number invalid");
			
			String wo = ticket.getWorkOrderNumber();
			
			/*City city = ticket.getCustomer() != null
					? ticket.getCustomer().getCity() : null;*/
			
			TypeAheadVo city = frDao.getCityNameForTicket(vo.getTicketId());
					
			if( city != null )
				vo.setCityName(city.getName());
					
			vo.setWorkOrderNumber(wo);
			vo.setRequestParam(commentOrStatusRequestString(vo));
			
			return updateFrTicketInMQ( vo );
		} 
		catch (Exception e)
		{
			LOGGER.error("Error while calling status or comment update mq modifyTicket api ",e);
			return null;
		}
	}
	
	private String etrModifyRequestString(TicketModifyInMQVo vo)
	{
		String etrRequest = "<REQUESTINFO> <MODIFYTICKET><TICKETNO>"+vo.getWorkOrderNumber()+"</TICKETNO>"
			    +"<TICKETCATEGORY></TICKETCATEGORY><TICKETSHORTDESCRIPTION></TICKETSHORTDESCRIPTION>"
			    +"<TICKETDESCRIPTION></TICKETDESCRIPTION><TICKETNATURE></TICKETNATURE><TICKETSTATUS></TICKETSTATUS>"
			    +"<TICKETPRIORITY></TICKETPRIORITY><PREFERREDDATE></PREFERREDDATE><SCHEDULEDDATE></SCHEDULEDDATE>"
			    + "<SERVICETEAM></SERVICETEAM><ASSIGNEDTO></ASSIGNEDTO><NEXTCALLON></NEXTCALLON><CALLTYPE></CALLTYPE>"
			    +"<COMMENT>BCMT0013</COMMENT><COMMENTNOTES>"+vo.getRemarks()+"</COMMENTNOTES><SMSTOEMP></SMSTOEMP>"
			    +"<SMSTOCUST></SMSTOCUST><AREA></AREA><TYPE></TYPE><TICKETRESCODE></TICKETRESCODE><TICKETRESNOTES></TICKETRESNOTES>"
			    +"</MODIFYTICKET><FLEX-ATTRIBUTE-INFO><ATTRIBUTE1></ATTRIBUTE1><ATTRIBUTE2></ATTRIBUTE2><ATTRIBUTE3></ATTRIBUTE3>"
			    +"<ATTRIBUTE4></ATTRIBUTE4><ATTRIBUTE5>"+vo.getEtrValue()+"</ATTRIBUTE5><ATTRIBUTE6></ATTRIBUTE6>"
			    +"<ATTRIBUTE7></ATTRIBUTE7><ATTRIBUTE8></ATTRIBUTE8><ATTRIBUTE9></ATTRIBUTE9><ATTRIBUTE10></ATTRIBUTE10>"
			    +"</FLEX-ATTRIBUTE-INFO><PROBLEM-INFO><PROBLEM></PROBLEM></PROBLEM-INFO></REQUESTINFO>";
		return etrRequest;
	}
	
	private String commentOrStatusRequestString(TicketModifyInMQVo vo)
	{
		String commentOrStatusRequest ="<REQUESTINFO><MODIFYTICKET><TICKETNO>"+vo.getWorkOrderNumber()+"</TICKETNO>"
				+"<TICKETCATEGORY></TICKETCATEGORY><TICKETSHORTDESCRIPTION></TICKETSHORTDESCRIPTION>"
				+"<TICKETDESCRIPTION></TICKETDESCRIPTION><TICKETNATURE></TICKETNATURE><TICKETSTATUS> </TICKETSTATUS>"
				+"<TICKETPRIORITY></TICKETPRIORITY><PREFERREDDATE></PREFERREDDATE> <SCHEDULEDDATE></SCHEDULEDDATE>"
				+"<SERVICETEAM></SERVICETEAM><ASSIGNEDTO></ASSIGNEDTO><NEXTCALLON></NEXTCALLON><CALLTYPE></CALLTYPE>"
				+"<COMMENT>DEFAULT</COMMENT> <COMMENTNOTES>"+vo.getRemarks()+"</COMMENTNOTES> <SMSTOEMP></SMSTOEMP>"
				+"<SMSTOCUST></SMSTOCUST><AREA></AREA><TYPE></TYPE><TICKETRESCODE></TICKETRESCODE>"
				+"<TICKETRESNOTES></TICKETRESNOTES></MODIFYTICKET><FLEX-ATTRIBUTE-INFO><ATTRIBUTE1></ATTRIBUTE1>"
				+"<ATTRIBUTE2></ATTRIBUTE2><ATTRIBUTE3></ATTRIBUTE3><ATTRIBUTE4></ATTRIBUTE4><ATTRIBUTE5></ATTRIBUTE5>"
				+"<ATTRIBUTE6></ATTRIBUTE6><ATTRIBUTE7></ATTRIBUTE7><ATTRIBUTE8></ATTRIBUTE8><ATTRIBUTE9></ATTRIBUTE9>"
				+"<ATTRIBUTE10></ATTRIBUTE10></FLEX-ATTRIBUTE-INFO><PROBLEM-INFO><PROBLEM></PROBLEM></PROBLEM-INFO>"
				+"</REQUESTINFO>";
		
		return commentOrStatusRequest;
	}
	
}
