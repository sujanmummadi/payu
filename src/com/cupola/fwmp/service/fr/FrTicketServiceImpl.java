package com.cupola.fwmp.service.fr;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.FlowActionType;
import com.cupola.fwmp.FRConstant.FlowForDefectSubDefect;
import com.cupola.fwmp.FRConstant.FrEscalation;
import com.cupola.fwmp.FRConstant.FrTicketEtrStatus;
import com.cupola.fwmp.FRConstant.MQRequestType;
import com.cupola.fwmp.FRConstant.UserType;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.ControlBlockStatus;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.controller.integ.CRMUpdateRequestHandler;
import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.dao.fr.FrDeploymentSettingDao;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.fr.FrTicketDetailsDao;
import com.cupola.fwmp.dao.fr.ScratchPadDAO;
import com.cupola.fwmp.dao.fr.etr.FrTicketEtrDao;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.mongo.fr.FrActionDao;
import com.cupola.fwmp.dao.priorityWeightage.TicketPriorityDao;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.FrDeploymentSetting;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.FrTicketEtr;
import com.cupola.fwmp.persistance.entities.TicketPriority;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.closeticket.MQClosureHelper;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.fr.vendor.FRVendorCoreService;
import com.cupola.fwmp.service.notification.FrEmailNotificationHelper;
import com.cupola.fwmp.service.ticket.TicketCoreServiceImpl;
import com.cupola.fwmp.service.ticket.TicketUpdateCoreService;
import com.cupola.fwmp.service.ticket.counts.FrTicketCountCache;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.counts.dashboard.CountRequestVO;
import com.cupola.fwmp.vo.counts.dashboard.FRDashBoardCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketSummaryCountVO;
import com.cupola.fwmp.vo.fr.AppResponseVo;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FlowActionData;
import com.cupola.fwmp.vo.fr.FlowWiseDefectAndSubDefectCodeData;
import com.cupola.fwmp.vo.fr.FrEtrUpdateVo;
import com.cupola.fwmp.vo.fr.FrFlowRequestVo;
import com.cupola.fwmp.vo.fr.FrTicketDetailsVO;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;
import com.cupola.fwmp.vo.fr.FrTicketEtrVo;
import com.cupola.fwmp.vo.fr.FrTicketPushBackVo;
import com.cupola.fwmp.vo.fr.FrTicketVo;
import com.cupola.fwmp.vo.fr.ShiftingRequestVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.FaultRepairActivityUpdateInCRMVo;

@Transactional
public class FrTicketServiceImpl implements FrTicketService
{

	@Autowired
	private FrTicketDao frTicketDao;
	
	@Autowired
	private FrTicketDetailsDao  frTicketDetailsDao;

	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private TicketDAO ticketDao;
	
	@Autowired
	private DBUtil dbUtil;
	
	@Autowired
	private GlobalActivities globalActivities;
	
	@Autowired
	FrActionDao frActionDao;
	
	@Autowired
	TicketUpdateCoreService ticketUpdateCoreService;
	
	@Autowired
	DeviceDAO deviceDAOSercive;
	
	@Autowired
	private FrTicketEtrDao etrDao;
	
	@Autowired
	private EtrCalculator etrCalculator;
	
	@Autowired
	private FRVendorCoreService vendorCoreService;
	
	@Autowired
	private FrTicketLockHelper frHelper;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private TicketPriorityDao priorityDao;
	
	@Autowired
	private FrEmailNotificationHelper emailHelper;
	
	@Autowired
	private FrDeploymentSettingDao frSettingDao;
	
	@Autowired
	TicketCoreServiceImpl ticketCoreServiceImpl;
	
	@Autowired
	ScratchPadDAO scratchPad;
	
	@Autowired
	CRMUpdateRequestHandler crmUpdateRequestHandler;

	private static Logger LOGGER = Logger.getLogger(FrTicketServiceImpl.class);
	
	@Override
	public Long createFrDetails(FrTicketVo voObject)
	{
		Long result = 0l;
		if( voObject == null )
			return result;
		
		try{
			String natureCode = definitionCoreService.
					findFrNatureCodeBySymptomName(voObject.getSubCategory());
			Long flowId = FrTicketUtil.
					findFlowIdforMatchingSymptomName(voObject.getSubCategory());
			voObject.setNatureCode(natureCode);
			voObject.setFlowId(flowId);
			
			result = frTicketDao.createFrTicket(voObject);
			return result;
			
		}catch(Exception exception){
			LOGGER.error("Error occured in service layer while create Fr detail",exception);
			return result;
		}
	}
	
	@Override
	public  Map<Long, Boolean> createFrDetailInBulk( List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap )
	{
		if( roiMQWorkorderVOList == null || completeProspectMap == null)
			return new ConcurrentHashMap<>();
		
		return frTicketDao.createFrDetailInBulk(roiMQWorkorderVOList,
				completeProspectMap );
	}
	
	@Override
	public Integer updateFrTicketSymptom(Long flowId , String symptomName, Long ticketId)
	{
		return frTicketDao.updateFrTicketSymptom(flowId, symptomName, ticketId);
	}
	
	@Override
	public void deleteFrTicket(Long ticketId)
	{
		
		
	}

	@Override
	public void saveOrUpdateFrTicket(FrTicketVo dbvo)
	{
		frTicketDao.saveOrUpdateFrTicket(dbvo);
	}

	@Override
	public List<FrTicketVo> getAllFrTicket()
	{
		List<FrDetail> frTicketVo = frTicketDao.getAllFrTicket();
		return xformFromToFrTicketVo(frTicketVo);
	}

	@Override
	public List<FrTicketVo> getFrTicketByUser(Long userId)
	{
		
		return null;
	}
	
	@Override
	public FrTicketDetailsVO getFrTicketDetailById(Long ticketId)
	{
		if( ticketId == null || ticketId.longValue() <= 0 )
			return new FrTicketDetailsVO();
		
		TicketFilter filter = new TicketFilter();
		filter.setTicketId(ticketId);
		List<FrTicketDetailsVO> list =  frTicketDetailsDao.
				getFrTicketWithDetails(filter);
		return ( list != null && list.size() > 0 ) ? list.get(0) : new FrTicketDetailsVO();
	}

	@Override
	public List<FrTicketDetailsVO> getFrTicketDetails(TicketFilter filter)
	{
		return frTicketDetailsDao.getFrTicketWithDetails(filter);
	}

	@Override
	public APIResponse searchFrTicketDetailsBy(TicketFilter filter)
	{
		List<FrTicketDetailsVO> dbvo = frTicketDetailsDao.searchFrTicketWithDetails(filter);
		if( dbvo != null && dbvo.size() > 0 )
			return ResponseUtil.recordFoundSucessFully().setData(dbvo);
		else
			return ResponseUtil.createRecordNotFoundResponse().setData("Record Not Fond");
	}

	@Override
	public FRDashBoardCountVO getFrSummaryCounts(CountRequestVO countVo)
	{
		/*if( countVo != null && countVo.isModified())
		{
			LOGGER.info("Fr Ticket dashBoard status modifed...");
			FrTicketCountCache.refreshCache();
		}*/
		return FrTicketCountCache.getCountSummary(
				AuthUtils.getCurrentUserId());
	}
	
	@Override
	public FRTicketSummaryCountVO getFrUserSummaryCount()
	{
		return  FrTicketCountCache.getFrUserSummaryCount(
				AuthUtils.getCurrentUserId());
	}
	
	@Override
	public FRTicketSummaryCountVO getTlneDetailCounts() 
	{
		return  FrTicketCountCache.getFrTiketTlneDetailCounts(
				AuthUtils.getCurrentUserId());
	}

	@Override
	public void updateFlowId(Long ticketId, String symptomName, Long flowId)
	{
		try{
			
			boolean isAutoAssign = false;
			
			FrTicket ticket = ticketDao.getFrTicketById(ticketId);
			if( ticket != null)
			{
				String fxName =  ticket.getFxName();
				Long currentAssignedUser = ticket.getUserByCurrentAssignedTo().getId();
				
				List<String> roles = AuthUtils.getUserRolesByFlowId(flowId);
				UserVo userVo = userDao.getUsersByDeviceName(fxName, roles);
				
				if( userVo != null && isAutoAssign )
				{
					Long userTobeAssigned = userVo.getId();
					Integer updated = frTicketDao.updateFlowId(ticketId, symptomName,
							flowId,userTobeAssigned ,null);
					
					if( updated > 0)
					{
						frHelper.swapFrTicketInUserQueu(currentAssignedUser, userTobeAssigned, ticketId);
						frHelper.manageUserLock(currentAssignedUser);
					
					}
				}
				else{
					long reportToUser = userDao.getReportToUser(currentAssignedUser);
					
					if( reportToUser > 0 )
					{
						Integer updated = frTicketDao.updateFlowId(ticketId, symptomName,
								flowId,reportToUser,FRStatus.FLOW_CHANGED_DOWN_TO_UP);
						
						if( updated > 0 )
						{
							LOGGER.info("Ticket : "+ticketId+" Assigned to Tl : "+reportToUser
									+ "because of Ne user not map with fx device : "+fxName);
							
							/*StatusVO statusVo = new StatusVO();
							statusVo.setStatus(FRStatus.FLOW_CHANGED_DOWN_TO_UP);
							statusVo.setTicketId(ticketId);
							ticketUpdateCoreService.updateFrTicketStatus( statusVo );*/
							
							String useQueueKey = dbUtil.getKeyForQueueByUserId(currentAssignedUser);
							globalActivities.removeFrTicketFromMainQueueByKeyAndTicketNumber(useQueueKey,ticketId);
							
							useQueueKey = dbUtil.getKeyForQueueByUserId(reportToUser);
							globalActivities.addFrTicket2MainQueue(useQueueKey, 
									new LinkedHashSet<Long>(Arrays.asList(ticketId)));
							
							frHelper.manageUserLock(currentAssignedUser);
						}
						
					} else {
						LOGGER.info("Can't update flow id for Ticket : "+ticketId
								+ "because user not map with fx device : "+fxName);
					}
					
				}
			}
		}catch(Exception exception){
			LOGGER.error("can't update flow id for ticket : "+ticketId,exception);
		}
		
	}
	
	private List<FrTicketVo> xformFromToFrTicketVo(List<FrDetail> list)
	{
		List<FrTicketVo> voObject = new ArrayList<FrTicketVo>();
		if( list == null || list.isEmpty() )
			return voObject;
			
		FrTicketVo frVo = null;
		for(FrDetail value : list)
		{
			frVo = new FrTicketVo();
			frVo.setId(value.getId());
			frVo.setUserByAssignedTo(value.getTicket() != null ?
					(value.getTicket().getUserByAssignedTo() != null ?
							value.getTicket().getUserByAssignedTo().getId():0l) : 0l);
			
			frVo.setUserByAssignedBy(value.getTicket() != null ?
					(value.getTicket().getUserByAssignedBy() != null ?
							value.getTicket().getUserByAssignedBy().getId() :0l) : 0l);
			
			frVo.setTicketId(value.getTicket().getId());
			frVo.setCategory(value.getCategory());
			frVo.setSubCategory(value.getSubCategory());
			
			frVo.setSymptomCode(value.getSymptomCode());
			frVo.setUpdatedSymptomCode(value.getUpdatedSymptomCode());
			frVo.setCurrentProgress(value.getCurrentProgress());
			frVo.setNatureCode(value.getNatureCode());
			frVo.setAddedBy(value.getAddedBy());
			frVo.setAddedOn(FrTicketUtil.convertDbDateToDbStringDate(value.getAddedOn()));
			
			frVo.setModifiedBy(value.getModifiedBy());
			frVo.setModifiedOn(FrTicketUtil.convertDbDateToDbStringDate(value.getModifiedOn()));
			frVo.setStatus(value.getTicket() != null ? value.getTicket().getStatus() : 0 );
			frVo.setVoc(value.getVoc());
			
			frVo.setSubCode(value.getSubCode());
			frVo.setDeviceId(value.getDevice() !=null ? value.getDevice().getId() : 0l);
			frVo.setFlowId(value.getFlowId());
			frVo.setWorkOrderNumber(value.getWorkOrderNumber());
			frVo.setLocked(value.getLocked());
			
			frVo.setFxName(value.getTicket() != null ? value.getTicket().getFxName() : null);
			frVo.setCurrentFlowId(value.getCurrentFlowId());
			frVo.setCurrentSymptomName(value.getCurrentSymptomName());
			
			voObject.add(frVo);
		}
		return voObject;
	}
	

	@Override
	public APIResponse updateTicketLock(Long userId,Long ticketId, boolean lockStatus)
	{
		if(frTicketDao.updateTicketLock(lockStatus, ticketId) > 0)
		{
			return ResponseUtil.createUpdateSuccessResponse();
		}
		else
			return ResponseUtil.createUpdateFailedResponse();
	}
	
	private APIResponse doShiting( ShiftingRequestVo requestVo , Long status, Long actionType)
	{
		if( requestVo == null )
			return ResponseUtil.nullArgument();
		
		try 
		{
			FrTicket ticket = ticketDao.getFrTicketById(Long.valueOf(requestVo.getTicketId()));
			if( ticket != null )
			{
				FlowActionData actionData = new FlowActionData(Long.valueOf(requestVo.getTicketId()),actionType);
				Map<String,String> map = new HashMap<>();
				map.put("oldCustAccNo", requestVo.getOldCustomerAcc());
				map.put("newCustAccNo", requestVo.getNewCustomerAcc());
				map.put("cxMac",requestVo.getCxMac());
				map.put("cxIp",requestVo.getCxIp());
			
				actionData.setAdditionalAttributes(map);
				submitFrActionData(actionData);
			
				ticket.setOldCustAccNo(requestVo.getOldCustomerAcc());
				ticket.setNewCustAccNo( requestVo.getNewCustomerAcc());
				ticket.setCxMac(requestVo.getCxMac());
				ticket.setCxIp(requestVo.getCxIp());
			
				ticket.setStatus(Integer.valueOf(status+""));
			
				frTicketDao.updateReopneTicket(ticket);
				frTicketDao.updateBlockStatus(Long.valueOf(requestVo.getTicketId()), false);
			
				return ResponseUtil.createSuccessResponse()
						.setData(new AppResponseVo(status+"",
								TicketStatus.statusDisplayValue.get(Integer.valueOf(status+""))));
			}
		} 
		catch (Exception e) 
		{
			LOGGER.error("Error occured while updating shifting info...",e);
			e.printStackTrace();
		}
		
		return ResponseUtil.NoRecordFound();
	}
	
	@Override
	public APIResponse getShiftingPermission( ShiftingRequestVo requestVo )
	{
		if( requestVo == null || requestVo.getTicketId() == null )
			return ResponseUtil.nullArgument();
		
		try{
			
			FrTicket ticket = ticketDao.getFrTicketById(Long.valueOf(requestVo.getTicketId()+""));
			if( ticket == null )
				return ResponseUtil.nullArgument()
						.setData(new AppResponseVo(0+"","Invalid ticket number"));
			
			long status = FRStatus.SHIFTING_PERMISSION_PENDING;
			
			ticket.setStatus(Integer.valueOf(status+""));
			ticket.setModifiedOn(new Date());
			ticket.setModifiedBy(AuthUtils.getCurrentUserId());
			
			long userId = ticket.getUserByCurrentAssignedTo() != null 
					? ticket.getUserByCurrentAssignedTo().getId() 
							: AuthUtils.getCurrentUserId();
			
			frTicketDao.updateReopneTicket(ticket);
			frHelper.blockedTicketForUser(userId, ticket.getId());
			
			TicketLogImpl	ticketLog = new TicketLogImpl(Long.valueOf(requestVo.getTicketId()+""),
					TicketStatus.statusCodeValue.get(Integer.valueOf(status+"")), AuthUtils.getCurrentUserId());
			
			ticketLog.setStatusId(Integer.valueOf(status+""));
			ticketLog.setFrTicket(true);
			TicketUpdateGateway.logTicket(ticketLog);
			
			return ResponseUtil.createFailureResponse()
					.setData(new AppResponseVo(status+"","Shifting permission pending in TL"));
			
			
		}catch(Exception e){
			LOGGER.error("Error while shifting permission from TL");
			return ResponseUtil.createFailureResponse()
					.setData(new AppResponseVo(0+"","Error while shifting permission"));
		}
		
	}
	
	@Override
	public APIResponse tlRaiseSubTicketToBillingTeam( ShiftingRequestVo requestVo )
	{
		if( requestVo == null || requestVo.getTicketId() == null )
			return ResponseUtil.nullArgument();
		
		long status = FRStatus.SHIFTING_PERMISSION_DONE_AGREEMENT_PENDING;
		return doShiting( requestVo ,status, FlowActionType.TRIGGER_BILLING_TEAM_TL);
	}

	 
	@Override
	public APIResponse submitFrActionData(FlowActionData actionData)
	{
		if( actionData == null )
			return ResponseUtil.nullArgument();
		
		frActionDao.submitFrActionData(actionData);
		
		if ( actionData.getActionType() <= 0 )
			return ResponseUtil.nullArgument()
					.setData("Must set action type constant value ie ccnr, vendor or noc etc.");
		
		
		if( FlowActionType.NOTIFICATION_ACTION_TYPE.contains(actionData.getActionType()) )
			return emailHelper.sendNotificationToActionType( actionData );
		
		long userId = 0;
		long status = 0;
		if( FlowActionType.TRIGGER_CCNR_TEAM.equals(actionData.getActionType()) )
		{
			UserVo userVo = AuthUtils.getCurrentCityCCNRUser() ;
			userId = ( userVo != null ) ? userVo.getId() : 0 ;
			status = FRStatus.CCNR_APPROVAL_PENDING;
			TicketLog ticketLog = new TicketLogImpl(actionData.getTicketId(),
					TicketUpdateConstant.TRIGGER_CCNR_TEAM,AuthUtils.getCurrentUserId());
			TicketUpdateGateway.logTicket(ticketLog);
		}
		else if( FlowActionType.TRIGGER_BILLING_TEAM_TL.equals(actionData.getActionType()) )
		{
			UserVo userVo = AuthUtils.getCurrentCityFinanceTeam() ;
			userId = ( userVo != null ) ? userVo.getId() : 0 ;
			status = FRStatus.SHIFTING_PERMISSION_DONE_AGREEMENT_PENDING;
			TicketLog ticketLog = new TicketLogImpl(actionData.getTicketId(),
					TicketUpdateConstant.TRIGGER_BILLING_TEAM_TL,AuthUtils.getCurrentUserId());
			TicketUpdateGateway.logTicket(ticketLog);
		}
		else
		{
			LOGGER.info("********** Invalid action type : "+actionData.getActionType());
			return ResponseUtil.createSaveSuccessResponse();
		}
		
		 FrFlowRequestVo subTicket  = new FrFlowRequestVo();
		 subTicket.setActionType(actionData.getActionType());
		 subTicket.setRequestedBy(AuthUtils.getCurrentUserId());
		 subTicket.setUserId(userId);
		 subTicket.setTicketId(actionData.getTicketId());
		 subTicket.setStatus(Integer.valueOf(status+""));
		 
		 
		 APIResponse response = null;
		 if ( ! UserType.VENDORS.contains(subTicket.getActionType()) )
			 response = vendorCoreService.createSubTicketTo(subTicket);
		 else
			 response = ResponseUtil.createRecordNotFoundResponse();
		
		
		return  response;
	}

	@Override
	public APIResponse getFrVendorTicket(FRVendorFilter filter)
	{
		if( filter == null )
			return ResponseUtil.nullArgument();
		
		filter.setVendorUserId(AuthUtils.getCurrentUserId());
		return ResponseUtil.createSuccessResponse().setData(frTicketDetailsDao.getFrVendorTickets(filter));
	}
	
	/**
	 * 
	 *  this method is used to create Etr for Fr Ticket only from crm
	 *  
	 *  @param ticketId
	 *  @param symptomCode
	 *  
	 *  @return 
	 *  
	 *  */
	@Override
	public void createFrTicketEtr( FrTicketEtrDataVo etrDataVo )
	{
		if( etrDataVo == null || etrDataVo.getTicketId() <= 0 )
			return;
		
		try
		{
			FrTicketEtrVo etrVo = new FrTicketEtrVo();
			etrVo.setTicketId(etrDataVo.getTicketId());
			Long flowId = 6l;
			
			if( etrDataVo.getSymptomName() != null && !etrDataVo.getSymptomName().isEmpty() )
			{
				flowId = FrTicketUtil.
						findFlowIdforMatchingSymptomName(etrDataVo.getSymptomName());
				
				LOGGER.info("******** Symtom Name : "+etrDataVo.getSymptomName() +"Flow Id : "+flowId);
			}
			//commented by Manjuprasad For etr issue
			
			/*Date committedEtr = etrCalculator
					.calculateEtrForFRTicket(flowId,etrDataVo.getCityCode(),etrDataVo.getBranchCode());
			
			if( committedEtr != null )
				etrVo.setCommittedEtr(committedEtr);
			else 
			{
				String defaultHours = definitionCoreService.getPropertyValue(DefinitionCoreService.ETR_DEFAULT_HOURS);
				int hours = 0;
				try{
					hours = Integer.valueOf(defaultHours);
				}catch(Exception ex){
					LOGGER.error("Please provid default etr value in property file : ");
				}
				etrVo.setCommittedEtr(GenericUtil.addHours(hours));
			}*/
			
			etrDataVo.setFlowId(flowId);
			etrDataVo.setCommittedEtr(etrVo.getCommittedEtr());
			frActionDao.dumpSymptomDetail(etrDataVo);
			
			long result = etrDao.addFrTicketEtr(etrVo);
			
//Commented by Manjuprasad
/*			if( result > 0 )
			{
				// if needed 
				// then write code to send notification 
				// to customer about committed Etr or else
				// with message.
//				emailHelper.sendSMSToCustomer( etrDataVo, committedEtr);
			}*/
		} 
		catch (Exception e)
		{
			LOGGER.error("Error occure while calculating ETR for Ticket no. : "
					+etrDataVo.getTicketId());
			e.printStackTrace();
		}
	}
	
	@Override
	public void createFrTicketEtrInBulk( List<FrTicketEtrDataVo> listOfObject )
	{
		if ( listOfObject == null || listOfObject.isEmpty() )
			return;
		
		LOGGER.info("Creating Etr for Fr Tickets..."+listOfObject.size());
		try
		{
			
			Set<FrTicketEtrDataVo> etrResultSet = etrDao.addFrTicketEtrInBulk(listOfObject);
			
			LOGGER.info("After Creation of Etr for Fr Tickets..."+etrResultSet.size()); 
			/*if( etrResultSet != null && !etrResultSet.isEmpty() )
			{
				frActionDao.dumpSymptomDetailInBulk( etrResultSet );
				emailHelper.putInFrNotifyQ(etrResultSet);
			}*/
			LOGGER.info("done Etr for Fr Tickets..."+listOfObject.size());
		}
		catch(Exception e)
		{
			LOGGER.error("Error occure while calculating ETR for IN bulk : "+e.getMessage()
					,e);
			e.printStackTrace();
		}
		LOGGER.info("Creating Etr for Fr Ticket IN Bulk have been done...");
	}

	@Override
	public APIResponse updateFrTicketEtrByNeOrTl(FrEtrUpdateVo etrUpdatebyNE)
	{
		if (etrUpdatebyNE == null)
		{
			LOGGER.info("found Etr update object null");
			return ResponseUtil.nullArgument()
					.setData("found Etr update object null");
		}
		if (etrUpdatebyNE.getTicketId() == null
				|| etrUpdatebyNE.getTicketId().isEmpty())
		{
			LOGGER.info("found ticketId null");
			return ResponseUtil.nullArgument().setData("found ticketId null");
		}

		if (etrUpdatebyNE.getNewETR() == null
				|| etrUpdatebyNE.getNewETR().isEmpty())
		{
			LOGGER.info("found new Fr Ticket Etr null");
			return ResponseUtil.nullArgument()
					.setData("found new Fr Ticket Etr null");
		}
		
		Integer count = 0;
		int role = AuthUtils.getCurrentUserRole();
		
		FrTicketEtrVo etrVo = new FrTicketEtrVo();
		
		etrVo.setApprovalEtr(GenericUtil
				.converDateFromString(etrUpdatebyNE.getNewETR()));
		etrVo.setTicketId(Long.valueOf(etrUpdatebyNE.getTicketId().trim()));
		etrVo.setResonCode(etrUpdatebyNE.getReasonCode());
		
		etrVo.setRemarks(etrUpdatebyNE.getRemarks());
		etrVo.setModifiedBy(AuthUtils.getCurrentUserId());
		etrVo.setEtrUpdateMode(FrTicketEtrStatus.ETR_UPDATE_MANUAL_MODE);
		etrVo.setCurrentProgress(etrUpdatebyNE.getCurrentProgress());
		
		/*
		 *  if required etr approval then  
		 *  we should have to change etr update technique 
		 *  according to requirement
		 *  
		 *  for now we are updating new etr in currentEtr column directly
		 *  not checking the etr approval status or else
		 *  status is approved for now
		 * 
		 * */
		
		etrVo.setStatus(FrTicketEtrStatus.ETR_APPROVED);
		
		// Etr Changed by NE
		if( role == UserRole.NE_FR ){
			
			// Write code if NE allowed to change ETR
			// and how many times he can change ETR
			
			LOGGER.info("NE :"+AuthUtils.getCurrentUserDisplayName()+" Updating current Etr & progress %");
			
			count = etrDao.updateCurrentEtrByNeOrTl(etrVo);
			
		}else if( role == UserRole.TL ){
			
			// Write code if TL allowed to change ETR
			// and how many times he can change ETR
			// for now simple current ETR update statement
			
			count = etrDao.updateEtrByNeOrTl(etrVo);
		}
		
		if( count > 0 )
		{
			// if needed 
			// then write code to send notification 
			// to customer or Ne or Tl about currentEtr 
			// or committed Etr or else
			// with message.
			
			return ResponseUtil.createETRSuccessResponse()
					.setData(GenericUtil.convertToUiDateFormat(etrVo.getApprovalEtr()));
		}
		
		return ResponseUtil.createUpdateFailedResponse()
				.setData("May be ticket not found as id:"
						+ etrUpdatebyNE.getTicketId());
	}

	@Override
	public APIResponse updateFrTicketEtrApp(List<FrEtrUpdateVo> updateRequest) 
	{
		if ( updateRequest == null )
			return ResponseUtil.nullArgument();
		
		for( FrEtrUpdateVo etrVo : updateRequest)
		{
			this.updateFrTicketEtrByNeOrTl(etrVo);
		}
		return ResponseUtil.createSaveSuccessResponse();
	}
	
	@Override
	public APIResponse getDefectSubDefectCode()
	{
		TypeAheadVo userCity = AuthUtils.getCurrentUserCity();
		if( userCity != null && userCity.getName() != null )
		{
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {

				if (userCity.getName().equalsIgnoreCase(CityName.HYDERABAD_CITY))
					return ResponseUtil.createSuccessResponse()
							.setData(frSettingDao.getDefectSubDefectByCityId(userCity.getName().toUpperCase()));
				else
					return ResponseUtil.createSuccessResponse()
							.setData(frSettingDao.getDefectSubDefectByCityId("REST_OF_INDIA".toUpperCase()));

			} else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {

				return ResponseUtil.createSuccessResponse()
						.setData(frSettingDao.getDefectSubDefectByCityId(CurrentCrmNames.CRM_SIEBEL.toUpperCase()));
			}
		}
		
			return ResponseUtil.createFailureResponse();
		
		/*
		Map<String,List<String>> resultMap = new HashMap<String,List<String>>();
		String key = null;
		String defectCode = null;
		try{
			Map<String,String> defectSubDefectCodeMap = definitionCoreService.getDefectSubDefectCode();
			defectSubDefectCodeMap.remove(DefectSubDefectCodeMapper.DEFECT_CODE+"");
			
			for( Map.Entry<String, String > entry : defectSubDefectCodeMap.entrySet())
			{
				key = entry.getKey();
				defectCode = DefectSubDefectCodeMapper.DEFECT_SUBDEFECT_MAP.get(Integer.valueOf(key));
				
				if(defectCode != null )
				{
					resultMap.put(defectCode, Arrays.asList(entry.getValue().split(",")));
				}
				else{
					LOGGER.info("No defect code for Key : "+key);
				}
			}
			return ResponseUtil.createSuccessResponse().setData(resultMap);
			
		}catch (Exception exception){
			LOGGER.error("Error occure while fetching defect an sub defect code : "
					+exception.getMessage());
		}
		return ResponseUtil.createFailureResponse();
	*/}

	@Override
	public APIResponse updateCCNRCompletedStatus( StatusVO status ) 
	{
		if( status == null )
			return ResponseUtil.nullArgument();
		
		try
		{
			vendorCoreService.updateSubTicketStatusByCCNR(status.getTicketId(),status.getStatus() );
			
			/*if( status.getStatus() == FRStatus.CCNR_APPROVAL_DONE )
			{
				ticketUpdateCoreService.updateFrTicketStatus( status );
			}
			if( status.getStatus() == FRStatus.CCNR_APPROVAL_REJECTED )
			{
				ticketUpdateCoreService.updateFrTicketStatus( status );
			}
			
			ticketUpdateCoreService.updateFrTicketStatus( status );
			frTicketDao.updateBlockStatus(status.getTicketId(), false);*/
			
			return ResponseUtil.createUpdateSuccessResponse();
		}
		catch( Exception exception )
		{
			LOGGER.error("Error occure while updating ccnr complete status : "
					+exception.getMessage());
			return ResponseUtil.createUpdateFailedResponse();
		}
	}
	
	@Override
	public APIResponse reopenFrWorkOrder( MQWorkorderVO workOrder )
	{
		if( workOrder == null )
			return ResponseUtil.nullArgument();
		
		try 
		{
			FrDetail frTicket = frTicketDao.findFrTicketDetailByWorkOrder( workOrder.getWorkOrderNumber() );
			if( frTicket != null )
			{
				LOGGER.info("Work order found in db for re open" + workOrder.getWorkOrderNumber());
				FrTicket ticket = frTicket.getTicket();
			
				if( ticket.getStatus() != null 
						&& !TicketStatus.COMPLETED_DEF.contains(ticket.getStatus()))
					return  ResponseUtil.nullArgument();
			
				int reopenCount = ticket.getReopenCount() != null ? ticket.getReopenCount() : 0;
			
				ticket.setReopenCount( reopenCount + 1 );
				ticket.setStatus(FRStatus.FR_WORKORDER_REOPEN);
				ticket.setPushBackCount(0);
//				ticket.setRemarks(workOrder.getComment());
				ticket.setReopenDateTime(new Date());
				ticket.setDefectCode("");
				ticket.setSubDefectCode("");
				LOGGER.info("Work order found in db for re open and updating count from " + reopenCount + " to "
						+ reopenCount + 1 + workOrder.getWorkOrderNumber());
			
				frTicketDao.updateReopneTicket(ticket);
			
				LOGGER.info("Work order found in db for re open and updated count from " + reopenCount + " to "
						+ reopenCount + 1 + workOrder.getWorkOrderNumber());
			
			
				String natureCode = definitionCoreService.
						findFrNatureCodeBySymptomName(workOrder.getSymptom() );
				Long flowId = FrTicketUtil.
						findFlowIdforMatchingSymptomName(workOrder.getSymptom() );
			
				frTicket.setNatureCode(natureCode);
			
				frTicket.setCurrentFlowId(flowId);
				frTicket.setCurrentSymptomName(workOrder.getSymptom());
				frTicket.setStatus(FRStatus.FR_WORKORDER_REOPEN+",");
				frTicket.setLocked(true);
				frTicket.setBlocked(false);
			
				frTicket.setTicket(ticket);
			
				frTicketDao.reopenWorkOrder(frTicket);
				scratchPad.delectContextDataInScratchPad(ticket.getId(), ControlBlockStatus.CONTEXT_TYPE);
				Set<FrTicketEtr> etrDb = ticket.getFrEtr();		
				if ( etrDb != null && etrDb.size() > 0 )		
				{
					FrTicketEtr dbVo = etrDb.iterator().next();
					dbVo.setCurrentProgress("0");
					frTicketDao.updateFrTicketEtr(dbVo);
				}
			
				frHelper.manageUserLock( ticket.getUserByCurrentAssignedTo()  != null ?
						ticket.getUserByCurrentAssignedTo().getId() : 0 );
			
				return ResponseUtil.createUpdateSuccessResponse();
			} else
			{
				LOGGER.info("Work order not found in db for re open" + workOrder.getWorkOrderNumber());
				
			}
		} 
		catch (Exception e) 
		{
			LOGGER.error("Error occured while reopening work order in FWMP... for work order " + workOrder.getWorkOrderNumber(),e);
			e.printStackTrace();
		}
		return ResponseUtil.createFailureResponse();
	}
	
	@Override
	public APIResponse assignFrTickets( AssignVO assignVO )
	{	

		if( assignVO == null || assignVO.getTicketId() == null 
				|| assignVO.getTicketId().isEmpty())
			return ResponseUtil.nullArgument();
		
		try {
			LOGGER.info("***** ASSIGNING TICKET " + assignVO.getTicketId() + "TO USER *****" + assignVO.getAssignedToId());
		
		Set<Long> userLock = new HashSet<Long>();
		List<TicketReassignmentLogVo> currentAssignTo = frTicketDao
					.getCurrentAssignedForTicketsWithIds(assignVO.getTicketId());
		
		LOGGER.info("Current AssignedTO"+currentAssignTo+" user for Ticket :"+assignVO.getTicketId());

		Long currentUser = null;
		if ( currentAssignTo != null )
		{
			TicketReassignmentLogVo ticketReassignmentLogVo = null;
			for ( TicketReassignmentLogVo ticketNos : currentAssignTo )
			{
					try {
				if (ticketNos == null)
					continue;
				
				if (ticketNos.getAssignedFrom() == null)
					continue;	
				
				currentUser = ticketNos.getAssignedFrom();
				

				String sourceKey = dbUtil.getKeyForQueueByUserId(currentUser);
				globalActivities
						.removeBulkFrTicketsFromMainQueueByKey(sourceKey, 
										new LinkedHashSet<Long>(assignVO.getTicketId()));
										
				userLock.add(currentUser);	
										
				long sourceTlId = userDao.getReportToUser(currentUser);
				if ( sourceTlId > 0 )
				{
					String sourceTlKey = dbUtil
							.getKeyForQueueByUserId(sourceTlId);

					globalActivities
							.removeBulkFrTicketsFromMainQueueByKey(sourceTlKey,
											new LinkedHashSet<Long>(assignVO.getTicketId()));
					
					ticketReassignmentLogVo = ticketCoreServiceImpl.updateTicketReassignmentLog(ticketNos.getTicketId(), currentUser, assignVO.getAssignedToId(), "FR");
							LOGGER.info("ticketReassignmentLogVo for FR " + ticketReassignmentLogVo);

				}
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.info("Error while ticket Reassignment Log Vo for FR " + ticketReassignmentLogVo + " . " + e.getMessage());
						continue;
			}
			}
			
//			frHelper.manageBulkUserLock(userLock);
		}
		
		Long userId = AuthUtils.getCurrentUserId();
		assignVO.setAssignedById(userId);
		frTicketDao.assignFrTickets(assignVO, null);
		
		userLock.add(assignVO.getAssignedToId());	
		
		String recipientUserKey = dbUtil
				.getKeyForQueueByUserId(assignVO.getAssignedToId());
		
		globalActivities
			.addFrTicket2MainQueue(recipientUserKey, new LinkedHashSet<Long>(assignVO
						.getTicketId()));
		
		long recipientTlId = userDao.getReportToUser(assignVO.getAssignedToId());
		if (recipientTlId > 0)
		{
			String recipientTlKey = dbUtil
					.getKeyForQueueByUserId(recipientTlId);

			globalActivities
					.addFrTicket2MainQueue(recipientTlKey, new LinkedHashSet<Long>(assignVO
							.getTicketId()));

		}
		
//		frHelper.manageUserLock(assignVO.getAssignedToId());
		
		frHelper.manageBulkUserLock(userLock);
		
		List<TicketLog> ticketLogs = new ArrayList<TicketLog>();
			TicketLog ticketLog = null;
		for (Long id : assignVO.getTicketId())
		{

				try {
					ticketLog = new TicketLogImpl(id, TicketUpdateConstant.UPDATE_CATEGORY_TICKET_REASSIGNED,
					AuthUtils.getCurrentUserId());
			
			Map<String,String> additionalAttributeMap = new HashMap<String,String>();
			additionalAttributeMap.put(TicketUpdateConstant.ASSIGNED_TO_KEY,assignVO.getAssignedToId()+"");
			ticketLog.setAdditionalAttributeMap(additionalAttributeMap);
			ticketLog.setFrTicket(true);
			ticketLogs.add(ticketLog);
			
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				
				if (AuthUtils.isFRUser()) {
					updateFaltRepairActivityInCRM(id, null, null, null, null);
				} 
			}
			
			

			
				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error("Error while populating logger " + ticketLog);
					continue;
		}
			}
			
		TicketUpdateGateway.logTickets(ticketLogs);

		} catch (Exception e) {
			LOGGER.error("Error while reassigning ticket " + assignVO +" .. " + e.getMessage());
			e.printStackTrace();
		}

		return ResponseUtil.createUpdateSuccessResponse();
	
	}
	
	@Override
	public void updateFrTicketStatus(StatusVO status)
	{
		if( status == null  )
			return ;
		
		frTicketDao.updateFrTicket(status);
	}
	
	@Override
	public Map<String, Long> getFrTicketByWorkOrder(List<String> workOrders)
	{
		return frTicketDao.getFrTicketByWorkOrder(workOrders);
	}
	
	@Override
	public APIResponse managePermission( Long ticketId , Long status ) 
	{
		if( ticketId == null || ticketId <= 0 
				|| status == null )
		{
			LOGGER.info("Can't Provide permission for id : "+ticketId);
			return ResponseUtil.nullArgument();
		}	
		
		/*
		 * if status = 1 means permission given by TL 
		 * so unblock ticket for NE to shift.
		 *  
		 * else status = 0 means permission ask to CCNR 
		 *  
		 */
		
		try
		{
			long newStatus = 0;
			StatusVO statusVo = new StatusVO();
			if( status.longValue() == 1 )
			{
				newStatus = FRStatus.SHIFTING_PERMISSION_DONE_AGREEMENT_PENDING;
				
				statusVo.setStatus(Integer.valueOf(newStatus+""));
				statusVo.setTicketId(ticketId);
				statusVo.setRemarks("Permission given for shifting by TL : "
						+AuthUtils.getCurrentUserDisplayName());
				
				ticketUpdateCoreService.updateFrTicketStatus( statusVo );
				frTicketDao.updateBlockStatus(ticketId, false);
				TicketLog ticketLog = new TicketLogImpl(ticketId,
						TicketUpdateConstant.SHIFTING_PERMISSION_DONE_AGREEMENT_PENDING, AuthUtils.getCurrentUserId());
				TicketUpdateGateway.logTicket(ticketLog);
				
				return ResponseUtil.createSuccessResponse();
				
			}
			else if( status.longValue() == 0 )
			{
				/*newStatus = FRStatus.CCNR_APPROVAL_PENDING;
				statusVo.setStatus(Integer.valueOf(newStatus+""));lll
				statusVo.setTicketId(ticketId);
				statusVo.setRemarks("Needed CCNR Permission for shifting");
				
				ticketUpdateCoreService.updateFrTicketStatus( statusVo );
				
				updateFrTicketStatus( statusVo );*/
				
				LOGGER.info("Sub TT raised for CCNR permission");
				
				newStatus = FRStatus.CCNR_APPROVAL_PENDING;
				
				FlowActionData actionData = new FlowActionData();
				actionData.setActionType(FlowActionType.TRIGGER_CCNR_TEAM);
				actionData.setTicketId(ticketId);
				actionData.setRemarks("Needed CCNR Permission for shifting");
				
				return submitFrActionData(actionData);
			}
			else
			{
				LOGGER.info("Can't Provide permission for id : "+ticketId);
				return ResponseUtil.createFailureResponse();
			}
			
		}
		catch (Exception e) 
		{
			LOGGER.error("Error in Permission service for ticket : "
					+ticketId,e);
		}
		
		return ResponseUtil.createSaveFailedResponse();
	}
	
	@Override
	public List<TypeAheadVo> getAllTlForUser( Long userId )
	{

		if( userId == null || userId <= 0 )
			return null;
		
	
		Map<Long ,String> nestedUsers =  userDao.getReportingUsers(userId);
		List<TypeAheadVo> listUsers = new ArrayList<TypeAheadVo>();
		TypeAheadVo aheadVo = null;
		
		for( Map.Entry<Long ,String> entry : nestedUsers.entrySet() )
		{
			
				aheadVo = new TypeAheadVo();
				aheadVo.setId(entry.getKey());
				UserVo user = userDao.getUserById(entry.getKey());
				Set<TypeAheadVo> area = user.getArea();
				if(area != null)
				{
					for (TypeAheadVo typeAheadVo : area) {
						aheadVo.setName(entry.getValue()+" ["+typeAheadVo.getName()+"]");
					}
				}
				else
				{
					if(user.getBranch() != null)
					{
					TypeAheadVo branch = user.getBranch();
					aheadVo.setName(entry.getValue()+" ["+branch.getName()+"]");
				}
				}
			
			
				
				listUsers.add(aheadVo);
	}
		return listUsers;
	
	}
	
	@Override
	public List<Long> getAllVicinityFrTicket( Long ticketId )
	{

		List<Long> ids = new ArrayList<Long>();
		if( ticketId == null || ticketId <= 0 )
			return ids;
		
		try 
		{
			FrTicket ticket = ticketDao.getFrTicketById(ticketId);
			if( ticket != null )
			{
				FrDetail detail = ( ticket.getFrWorkOrders() != null && !ticket.getFrWorkOrders().isEmpty() ) ?
						ticket.getFrWorkOrders().iterator().next() : null;
					
				Long flowId = null;		
				if( detail != null )
					flowId = detail.getFlowId();
				
				if ( flowId != null && flowId.longValue() == 1)
				{
					ids.addAll( frTicketDao.getAllVicinityFrTicketByCxIp( ticket.getCxIp(), flowId ) );
					ids.remove(ticketId);
				}
				
				LOGGER.info("Total vicinity Ticket found for ticket : "+ticketId +" is : "+ids);
			}
		} 
		catch (Exception e)
		{
			LOGGER.error("Error occured while getting Vicinity ticket for Auto closure..."+ e.getMessage() );
			e.printStackTrace();
		}
		return ids;
	
	}
	
	@Override
	public APIResponse pushBackFrTicketForUser( FrTicketPushBackVo pushBackVo )
	{
		if( pushBackVo == null || pushBackVo.getTicketId() == null )
			return ResponseUtil.nullArgument();
		
		try
		{
			String message = "";
			Long ticketId = Long.valueOf( pushBackVo.getTicketId() );
			
			if( frHelper.isPushBackAllowed( ticketId ))
			{
				message = "push back is failed ";
				if( frHelper.pushBackInNEQ( pushBackVo ) > 0 )
					message = "push back is done";
					
				pushBackVo.setResMessage( message );
				return ResponseUtil.createSuccessResponse().setData(pushBackVo);
			}
			
			else 
			{
				message = "push back is failed ";
				if( frHelper.pushBackInTLQ( pushBackVo ) > 0 )
					message = "You have exceeded push back limit ticket is moving to your TL"; 
					
				return ResponseUtil.createSuccessResponse().setData(pushBackVo);
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Error in FrTicketServiceImple "
					+ "push back service"+e);
			
			pushBackVo.setResMessage("Error sever side");
			return ResponseUtil.createFailureResponse().setData(pushBackVo);
		}
	}

	@Override
	public APIResponse updateFrPriority(String workOrderNo, int priority,
			Integer escalationType, Integer escalatedValue)
	{
		if( workOrderNo == null || workOrderNo.isEmpty() || priority <= 0 )
			return ResponseUtil.nullArgument();
		
		FrTicket ticket = frTicketDao.getFrTicketByWorkOrder(workOrderNo+"");
		if( ticket != null )
		{
			TicketPriority tp = ticket.getTicketPriority();
			if (tp != null)
			{

				if (escalationType == null || escalationType <= 0)
					tp.setSliderEscalatedValue(new Integer(0));
				else
				{
					tp.setEscalatedValue(escalatedValue);
					tp.setEscalationType(escalationType);
				}
				tp.setModifiedOn(new Date());
				tp.setValue(priority);
				int uc = tp.getUpdatedCount();
				uc++;
				tp.setUpdatedCount(uc);
				tp.setModifiedBy(AuthUtils.getCurrentUserId());
				priorityDao.updateFrTicketPriority(tp);
				
				/*frHelper.manageUserLockByPriority( ticket.getUserByCurrentAssignedTo() 
						!= null ? ticket.getUserByCurrentAssignedTo().getId() :null ,ticket.getId());*/
			}
		}
		else
		{
			LOGGER.info("Work order is not valid :"+workOrderNo);
			return ResponseUtil.noRecordUpdated();
		}
		return ResponseUtil.createSuccessResponse();
	}
	
	@Override
	public APIResponse getAllFxByTlUser( Long userId )
	{
		if( userId == null || userId <= 0 )
			return ResponseUtil.nullArgument();
		
		List<TypeAheadVo> vo = new ArrayList<TypeAheadVo>(userDao.getFxDeviceByTl(userId));
		vo.add(0, new TypeAheadVo(-1l,"All"));
		return ResponseUtil.createSuccessResponse()
				.setData(vo);
	}
	
	@Override
	public APIResponse createDefectSubDefectWithMQClosureCode( String fileName, String filePath, String addedOrRefresh)
	{
		if( fileName == null || filePath == null 
				|| addedOrRefresh == null || addedOrRefresh.isEmpty())
			return ResponseUtil.nullArgument();
		
		int restul = 0;
		if( addedOrRefresh.equalsIgnoreCase("add"))
		{
			restul = frSettingDao.addeNewCityDefectSubDefectCode(fileName, filePath);
			return ResponseUtil.createSuccessResponse()
					.setData(new AppResponseVo(restul+"","defect sub defect code successfully added..."));
		}
		else if( addedOrRefresh.equalsIgnoreCase("refresh"))
		{
			restul = frSettingDao.refresh(fileName, filePath);
			return ResponseUtil.createSuccessResponse()
					.setData(new AppResponseVo(restul+"","defect sub defect code successfully refreshed..."));
		}
		else
		{
			LOGGER.info("Provide valid option key 'add' or 'refresh'");
			return ResponseUtil.createFailureResponse()
					.setData(new AppResponseVo(restul+"","updation in record failed..."));
		}
		
	}
	
	@Override
	public APIResponse reloadSymptomFlowIdMappingFile()
	{
		Integer result = FrTicketUtil.reloadFlowIdSymptomMapping();
		if( result != null && result.intValue() > 0 )
			return ResponseUtil.createUpdateSuccessResponse().setData("Refreshed successfully...");
		else
			return ResponseUtil.createUpdateFailedResponse().setData("Refresh failed...");
	}
	
	@Override
	public List<TypeAheadVo>  getAllReportingTlList(Long currentUserId)
	{
		if( currentUserId == null || currentUserId <= 0 )
			return null;
		
	
		Map<Long ,String> nestedUsers =  userDao.getNestedReportingUsers(currentUserId, null, null, null);
		
		List<TypeAheadVo> listUsers = new ArrayList<TypeAheadVo>();
		TypeAheadVo aheadVo = null;
		Set<TypeAheadVo> area = null;
		for( Map.Entry<Long ,String> entry : nestedUsers.entrySet() )
		{
			if(userHelper.isFRTLUser(entry.getKey()))
			{
				aheadVo = new TypeAheadVo();
				aheadVo.setId(entry.getKey());
				UserVo user = userDao.getUserById(entry.getKey());
				 area = user.getArea();
				if(area != null)
				{
					for (TypeAheadVo typeAheadVo : area) {
						aheadVo.setName(entry.getValue()+" ["+typeAheadVo.getName()+"]");
					}
				}
				else
				{
					TypeAheadVo branch = user.getBranch();
					aheadVo.setName(entry.getValue()+" ["+branch.getName()+"]");
				}
			
				
				listUsers.add(aheadVo);
			}
				
			
		}
		return listUsers;
		
	}
	
	@Override
	public Integer getVicinityTicketPriority( MQWorkorderVO workOrder )
	{
		Integer vicinityPrioirty = 0;
		if( workOrder == null || workOrder.getFxName() == null 
				|| workOrder.getFxName().isEmpty())
			
			return vicinityPrioirty;
		
		try
		{
			Long flowId = FrTicketUtil.findFlowIdforMatchingSymptomName(workOrder.getSymptom());
			if( flowId == null || flowId.longValue() <= 0 )
				return vicinityPrioirty;
			
			else if( FrTicketUtil.isVicinityTicket( workOrder.getFxName(), flowId) )
				vicinityPrioirty = FrEscalation.values.get(FrEscalation.VICINITY);
			else
			{
				boolean ticket = frTicketDao.getFrTicketByElementNameAndFlowId( workOrder.getFxName() ,flowId);
				if( ticket )
					vicinityPrioirty = FrEscalation.values.get(FrEscalation.VICINITY);
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Error in FrTicketServiceImple "
					+ "manage vicinity ticket priority :"+e);
		}
		
		return vicinityPrioirty;
	}
	
	@Override
	public void dumpFailedWorkOrderMongoDb( List<MQWorkorderVO> mqWorkOrder )
	{
		if( mqWorkOrder == null || mqWorkOrder.isEmpty() )
			return ;
		
		try{
			frActionDao.dumpFailedWODetailInBulk( mqWorkOrder );
		}catch (Exception e) {
			LOGGER.error("Error occured while dumpping Fr Failed work order...",e);
			e.printStackTrace();
		}
		
	}
	
	@Override
	public FlowWiseDefectAndSubDefectCodeData getFrDeploymentSetting()
	{
		
		 Map<String, List<String>> cxDownDefectSubDefectCodeMap = new HashMap<String, List<String>>();

		 Map<String, List<String>> cxUpDefectSubDefectCodeMap = new HashMap<String, List<String>>();

		 Map<String, List<String>> slowSpeedDefectSubDefectCodeMap=new HashMap<String, List<String>>();;

		 Map<String, List<String>> frequentDisconnectionDefectSubDefectCodeMap = new HashMap<String, List<String>>();

		 Map<String, List<String>> seniorElementDefectSubDefectCodeMap=new HashMap<String, List<String>>();

		 Map<String, List<String>> otherDefectSubDefectCodeMap=new HashMap<String, List<String>>();
		
		
		FlowWiseDefectAndSubDefectCodeData codeData = new FlowWiseDefectAndSubDefectCodeData();
		
		List<FrDeploymentSetting> list =	frSettingDao.getFrDeploymentSetting();
		
		LOGGER.debug("FrDeploymentSetting"+list.size());
		
		for (FrDeploymentSetting frDeploymentSetting : list) {
			
			if(frDeploymentSetting.getFlowId().equalsIgnoreCase(FlowForDefectSubDefect.CX_DOWN))
			{
				
				if(cxDownDefectSubDefectCodeMap.containsKey(frDeploymentSetting.getDefectCode())){
					
					List<String> tempList= cxDownDefectSubDefectCodeMap.get(frDeploymentSetting.getDefectCode());
					tempList.add(frDeploymentSetting.getSubDefectCode());
					
					cxDownDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
					
				}else{
					List<String> tempList= new ArrayList<String>();
					tempList.add(frDeploymentSetting.getSubDefectCode());
					cxDownDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
				}
			} else if(frDeploymentSetting.getFlowId().equalsIgnoreCase(FlowForDefectSubDefect.CX_UP))
			{
				
				if(cxUpDefectSubDefectCodeMap.containsKey(frDeploymentSetting.getDefectCode())){
					
					List<String> tempList= cxUpDefectSubDefectCodeMap.get(frDeploymentSetting.getDefectCode());
					tempList.add(frDeploymentSetting.getSubDefectCode());
					
					cxUpDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
					
				}else{
					List<String> tempList= new ArrayList<String>();
					tempList.add(frDeploymentSetting.getSubDefectCode());
					cxUpDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
				}
			} else if(frDeploymentSetting.getFlowId().equalsIgnoreCase(FlowForDefectSubDefect.SLOW_SPEED_FLOW))
			{
				
				if(slowSpeedDefectSubDefectCodeMap.containsKey(frDeploymentSetting.getDefectCode())){
					
					List<String> tempList= slowSpeedDefectSubDefectCodeMap.get(frDeploymentSetting.getDefectCode());
					tempList.add(frDeploymentSetting.getSubDefectCode());
					
					slowSpeedDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
					
				}else{
					List<String> tempList= new ArrayList<String>();
					tempList.add(frDeploymentSetting.getSubDefectCode());
					slowSpeedDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
				}
			} else if(frDeploymentSetting.getFlowId().equalsIgnoreCase(FlowForDefectSubDefect.FREQUENT_DISCONNECTION_FLOW))
			{
				
				if(frequentDisconnectionDefectSubDefectCodeMap.containsKey(frDeploymentSetting.getDefectCode())){
					
					List<String> tempList= frequentDisconnectionDefectSubDefectCodeMap.get(frDeploymentSetting.getDefectCode());
					tempList.add(frDeploymentSetting.getSubDefectCode());
					
					frequentDisconnectionDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
					
				}else{
					List<String> tempList= new ArrayList<String>();
					tempList.add(frDeploymentSetting.getSubDefectCode());
					frequentDisconnectionDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
				}
			} else if(frDeploymentSetting.getFlowId().equalsIgnoreCase(FlowForDefectSubDefect.SENIOR_ELEMENT_DOWN))
			{
				
				if(seniorElementDefectSubDefectCodeMap.containsKey(frDeploymentSetting.getDefectCode())){
					
					List<String> tempList= seniorElementDefectSubDefectCodeMap.get(frDeploymentSetting.getDefectCode());
					tempList.add(frDeploymentSetting.getSubDefectCode());
					
					seniorElementDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
					
				}else{
					List<String> tempList= new ArrayList<String>();
					tempList.add(frDeploymentSetting.getSubDefectCode());
					seniorElementDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
				}
			} else if(frDeploymentSetting.getFlowId().equalsIgnoreCase(FlowForDefectSubDefect.OTHERS))
			{
				
				if(otherDefectSubDefectCodeMap.containsKey(frDeploymentSetting.getDefectCode())){
					
					List<String> tempList= otherDefectSubDefectCodeMap.get(frDeploymentSetting.getDefectCode());
					tempList.add(frDeploymentSetting.getSubDefectCode());
					
					otherDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
				}else{
					
					List<String> tempList= new ArrayList<String>();
					tempList.add(frDeploymentSetting.getSubDefectCode());
					otherDefectSubDefectCodeMap.put(frDeploymentSetting.getDefectCode(), tempList);
					
				}
			}
			
			
		}
		
		codeData.setCxDownDefectSubDefectCodeMap(cxDownDefectSubDefectCodeMap);
		codeData.setCxUpDefectSubDefectCodeMap(cxUpDefectSubDefectCodeMap);
		codeData.setFrequentDisconnectionDefectSubDefectCodeMap(frequentDisconnectionDefectSubDefectCodeMap);
		codeData.setOtherDefectSubDefectCodeMap(otherDefectSubDefectCodeMap);
		codeData.setSeniorElementDefectSubDefectCodeMap(seniorElementDefectSubDefectCodeMap);
		codeData.setSlowSpeedDefectSubDefectCodeMap(slowSpeedDefectSubDefectCodeMap);
	 
		return codeData;
		
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.fr.FrTicketService#updateWorkOrderActivityInCRM(java.lang.Long, java.lang.Long, java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public void updateFaltRepairActivityInCRM(Long ticketId, Long customerId, String workOrderNumber, Long cityId,
			Long branchId) {
		
		FaultRepairActivityUpdateInCRMVo activityUpdateInCRMVo = new FaultRepairActivityUpdateInCRMVo();
		
		if(ticketId != null )
			activityUpdateInCRMVo.setTicketId(ticketId);
		
		if(customerId != null)
			activityUpdateInCRMVo.setCustomerId(customerId);
		
		if(workOrderNumber != null)
			activityUpdateInCRMVo.setWorkOrderNumber(workOrderNumber);
		
		if(cityId != null)
			activityUpdateInCRMVo.setCityId(cityId);
		
		if(branchId != null)
			activityUpdateInCRMVo.setBranchId(branchId);
		
		crmUpdateRequestHandler.faultRepairActivityUpdateInCRM(activityUpdateInCRMVo);

		
	}
}
