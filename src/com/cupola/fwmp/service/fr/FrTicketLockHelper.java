package com.cupola.fwmp.service.fr;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.MQRequestType;
import com.cupola.fwmp.FRConstant;
import com.cupola.fwmp.FRConstant.UpdateType;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.closeticket.MQClosureHelper;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.ticket.TicketUpdateCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.fr.FrTicketPushBackVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;

public class FrTicketLockHelper
{
	private static final Logger LOGGER = Logger.getLogger(FrTicketLockHelper.class);
	private static final String baseKeyPrefix = "afp.fr.ticket.unlock.size.";
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	@Autowired
	private FrTicketDao frDao;
	
	@Autowired
	private DBUtil dbUtil;
	
	@Autowired
	private GlobalActivities globalActivities;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private TicketUpdateCoreService ticketUpdateCoreService;
	
	@Autowired
	private TicketDAO ticketDao;
	
	@Autowired
	private MQClosureHelper mqClosureHelper;
	
	private static Map<Long,Long > maxUnlockedUserTicket = new ConcurrentHashMap<Long,Long >(); 
	static{
		maxUnlockedUserTicket.clear();
	}
	
	public void manageBulkUserLock( Set<Long> userId )
	{
		if ( userId == null || userId.isEmpty())
			return;
		
		for( Long usr : userId)
			manageUserLock(usr);
	}
	
	public void blockedTicketForUser(Long userId, Long ticketId)
	{
		if( userId == null || ticketId == null 
				|| ticketId.longValue() <= 0 || userId.longValue() <=0 )
		{
			LOGGER.info("Can't blocked Ticket : "+ticketId +" : for User : "+userId);
			return;
		}
		frDao.updateBlockStatus(ticketId,true);
		manageUserLock( userId );
	}
	
	private void doUnlockTicket(Long userId ,List<Long> ignoreIds )
	{

		if ( userId == null || userId <= 0  || !isFRNEUser( userId ) )
			return;
		
		Integer nextUnlockSize = getNextUnlockSize(userId);
		if( nextUnlockSize > 0 )
		{
			Integer unlockedCount = frDao.activateNextLockedFrTicket(userId, nextUnlockSize, ignoreIds );
			LOGGER.debug(unlockedCount+" : Higher priority ticket Unlocked for User id : "+userId);
		}
	
	}
	
	public void manageUserLock( Long userId )
	{
		this.doUnlockTicket( userId ,null );
	}
	
	public void manageUserLockIgnorTickets( Long userId , List<Long> ticketIds )
	{
		if( ticketIds != null && ticketIds.size() > 0 )
			this.doUnlockTicket( userId ,ticketIds );
	}
	
	public void manageUserLockIgnorTicket( Long userId , Long ticketIds )
	{
		if( ticketIds != null && ticketIds.longValue() > 0 )
			this.doUnlockTicket( userId ,Arrays.asList(ticketIds) );
	}
	
	private Integer getUnlockLimitByCity()
	{
		String count = null;
		try {
			TypeAheadVo userCity = AuthUtils.getCurrentUserCity();
		if(userCity != null)
			count =  definitionCoreService.getPropertyValue(baseKeyPrefix +userCity.getName().toLowerCase());
		else if(count == null)
			 count =  definitionCoreService.getPropertyValue(baseKeyPrefix +"default");
		} catch (Exception e) {
			e.printStackTrace();
			count = FRConstant.DEFFAULT_UNLOCK_SIZE;
		}
		return Integer.valueOf(count.trim());
		 
	}
	
	private int getNextUnlockSize(Long userId)
	{
		Integer unlocked = frDao.getUnlockedTicketWithUnblockedCount(userId);
		Integer cityLimit = getUnlockLimitByCity();
		
		if( unlocked == cityLimit )
			return 0;
		else if( unlocked < cityLimit)
			return cityLimit - unlocked;
		
		return 0;
	}
	
	public void swapFrTicketInUserQueu( Long fromUser , Long toUser , Long ticketId )
	{
		if( fromUser == null || toUser == null || ticketId == null )
			return ;
		else if ( fromUser <= 0 || toUser <= 0 || ticketId <= 0 )
			return ;
		

		LinkedHashSet<Long> tIds = new LinkedHashSet<Long>();
		tIds.add(ticketId);
		
		String useQueueKey = dbUtil.getKeyForQueueByUserId(fromUser);
		globalActivities.removeFrTicketFromMainQueueByKeyAndTicketNumber(useQueueKey,ticketId);
		
		long currentReportToUser = userDao
				.getReportToUser(fromUser);
		if( currentReportToUser > 0)
		{
			useQueueKey = dbUtil.getKeyForQueueByUserId(currentReportToUser);
			globalActivities.removeFrTicketFromMainQueueByKeyAndTicketNumber(
					useQueueKey,ticketId);
		}
		
		useQueueKey = dbUtil.getKeyForQueueByUserId(toUser);
		globalActivities.addFrTicket2MainQueue(useQueueKey, tIds);
		
		long userTobeAssignedReportTo = userDao.getReportToUser(toUser);
		if( userTobeAssignedReportTo > 0 )
		{
			useQueueKey = dbUtil.getKeyForQueueByUserId(userTobeAssignedReportTo);
			globalActivities.addFrTicket2MainQueue(useQueueKey,tIds);
		}
	
	}
	
	public boolean isFRNEUser( Long userId )
	{
		if(userId == null || userId <= 0 )
			return false;
		
		List<String> roles = userDao.getAllRolesByUserId(userId);
		
		if ( roles.containsAll(AuthUtils.NE_FR_CXDOWN_DEF) )
			return true;
		else if ( roles.containsAll(AuthUtils.NE_FR_CXUP_DEF) )
			return true;
		else if ( roles.containsAll(AuthUtils.NE_SR_CXDOWN_DEF) )
			return true;
		else 
			return false;
	}
	
	private Integer getPushBackLimit()
	{
		
		try
		{
			String value = definitionCoreService
					.getPropertyValue(DefinitionCoreService
							.PUSH_BACK_FRTICKET_LIMIT);
			
			if( value == null )
			{
				LOGGER.info("Configure push back limit in property file for "
						+ "now we are using default value as : 0 ");
				
				return 0;
			}
			return Integer.valueOf(value);
				
		}
		catch (Exception e)
		{
			LOGGER.info("Configure push back limit in property file for "
					+ "now we are using default value as : 0 ");
			
			return 0 ;
		}
	}
	
	public boolean isPushBackAllowed(Long ticketId)
	{
		int limitValue = getPushBackLimit();
		Integer currPushBackCount = frDao.getCurrentPushBackCount( ticketId );
		
		if(currPushBackCount == null)
			currPushBackCount =  0;
		
		if( currPushBackCount == -1 )
			throw new RuntimeException("Error in getting push back count from"
					+ " db for ticket :"+ticketId);
		
		else if ( limitValue > currPushBackCount )
			return true;
		
		else if( limitValue <= currPushBackCount )
			return false;
		else
			throw new RuntimeException("Not a valid input for ticket : "+ticketId);
	}
	
	public Integer pushBackInTLQ( FrTicketPushBackVo pushBackVo )
	{
		int updated = 0;
		if(  pushBackVo == null || pushBackVo.getTicketId() == null
				|| pushBackVo.getTicketId().isEmpty() )
		
			return updated;
		
		long reportToUser = userDao.getReportToUser(AuthUtils.getCurrentUserId());
		if( reportToUser > 0 )
		{
			pushBackVo.setUpdateType( UpdateType.TL );
			pushBackVo.setReportTo( reportToUser );
			updated = frDao.updateFrTicketPushBackCount( pushBackVo );
			
			String useQueueKey = dbUtil.getKeyForQueueByUserId( AuthUtils.getCurrentUserId() );
			globalActivities.removeFrTicketFromMainQueueByKeyAndTicketNumber(useQueueKey,Long.valueOf(pushBackVo.getTicketId()));
				
			useQueueKey = dbUtil.getKeyForQueueByUserId(reportToUser);
			globalActivities.addFrTicket2MainQueue(useQueueKey, 
						new LinkedHashSet<Long>(Arrays.asList(Long.valueOf(pushBackVo.getTicketId()))));
				
			this.manageUserLock( AuthUtils.getCurrentUserId() );
			this.updatePushBackLog( pushBackVo , TicketUpdateConstant.UPDATE_CATEGORY_TICKET_PUSHED_BACK_TO_TL );
		}
		return updated;
	}
	
	public Integer pushBackInNEQ( FrTicketPushBackVo pushBackVo )
	{
		int updated = 0;
		if( pushBackVo == null || pushBackVo.getTicketId() == null
				|| pushBackVo.getTicketId().isEmpty())
			
			return updated ;
		
		pushBackVo.setUpdateType(UpdateType.NE);
		boolean isMinUserTT = this.isUserMinUnlockTT(AuthUtils.getCurrentUserId());
		
		if( !isMinUserTT )
			pushBackVo.setLocked( true );
		
		updated = frDao.updateFrTicketPushBackCount( pushBackVo );
		if( !isMinUserTT )
			this.manageUserLockIgnorTicket(AuthUtils.getCurrentUserId(), Long.valueOf(pushBackVo.getTicketId()));
		
		this.updatePushBackLog( pushBackVo , TicketUpdateConstant.UPDATE_CATEGORY_TICKET_PUSHED_BACK );
		return updated ;
	}
	
	public void manageUserLockByPriority( Long userId, Long ticketId )
	{
		if ( ticketId == null || ticketId <= 0 
				|| userId == null || userId <= 0 || !isFRNEUser(userId))
			return;
		
		List<Long> tickIds = frDao.getNTopPriorityLockedFrTicket(userId, getUnlockLimitByCity());
		if( tickIds != null && tickIds.contains(ticketId))
			frDao.activateTicket(true, userId, ticketId);
	}
	
	
	private boolean isUserMinUnlockTT( Long userId )
	{
		if( userId == null || userId.longValue() <= 0 )
			return true;
		
		return frDao.getAllOpenFrTicketByUser( userId ).size() 
				> getUnlockLimitByCity().intValue() ? false : true ;
	}
	
	private void updatePushBackLog( FrTicketPushBackVo pushBackVo , String UPDATE_CATEGORY )
	{
		if( pushBackVo == null || pushBackVo.getTicketId() == null 
				|| pushBackVo.getTicketId().isEmpty())
			
			return;
		
		try
		{
			Long ticketId = Long.valueOf( pushBackVo.getTicketId() );
			
			TicketModifyInMQVo vo = new TicketModifyInMQVo();
			vo.setTicketId( ticketId );
			vo.setRemarks(pushBackVo.getRemarks());
			vo.setMqModifyRequestType(MQRequestType.STATUS_OR_COMMENT_MODIFY);
			
			mqClosureHelper.putInModifyQ(vo);
			TicketLog ticketLog = new TicketLogImpl(ticketId,UPDATE_CATEGORY, AuthUtils.getCurrentUserId() );
			TicketUpdateGateway.logTicket(ticketLog);
			
		}
		catch (Exception e) 
		{
			LOGGER.error("Error occured while updating push back log for ticket id : "
					+pushBackVo.getTicketId(),e);
			e.printStackTrace();
		}
		
	}
}
