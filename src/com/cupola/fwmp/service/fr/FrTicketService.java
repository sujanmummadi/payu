package com.cupola.fwmp.service.fr;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.counts.dashboard.CountRequestVO;
import com.cupola.fwmp.vo.counts.dashboard.FRDashBoardCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketSummaryCountVO;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FlowActionData;
import com.cupola.fwmp.vo.fr.FlowWiseDefectAndSubDefectCodeData;
import com.cupola.fwmp.vo.fr.FrEtrUpdateVo;
import com.cupola.fwmp.vo.fr.FrTicketDetailsVO;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;
import com.cupola.fwmp.vo.fr.FrTicketPushBackVo;
import com.cupola.fwmp.vo.fr.FrTicketVo;
import com.cupola.fwmp.vo.fr.ShiftingRequestVo;

public interface FrTicketService
{
	public void deleteFrTicket(Long ticketId);
	public void saveOrUpdateFrTicket(FrTicketVo dbvo);
	public List<FrTicketVo> getAllFrTicket();
	
	public List<FrTicketVo> getFrTicketByUser(Long userId);
	public List<FrTicketDetailsVO> getFrTicketDetails(TicketFilter filter);
	public APIResponse searchFrTicketDetailsBy(TicketFilter filter);
	public Long createFrDetails(FrTicketVo voObject);
	public Integer updateFrTicketSymptom(Long flowId, String symptomName, Long ticketId);
	
	public FRDashBoardCountVO getFrSummaryCounts(CountRequestVO countVo);
	
	public void updateFlowId(Long ticketId,String symptomName ,Long flowId);
	public APIResponse updateTicketLock(Long userId,Long ticketId, boolean lockStatus);
	public FrTicketDetailsVO getFrTicketDetailById(Long ticketId);
	public FRTicketSummaryCountVO getFrUserSummaryCount();
	public FRTicketSummaryCountVO getTlneDetailCounts();
	public APIResponse submitFrActionData(FlowActionData actionData);
	APIResponse getFrVendorTicket(FRVendorFilter filter);
	public void createFrTicketEtr( FrTicketEtrDataVo etrDataVo );
	
	public APIResponse updateFrTicketEtrByNeOrTl(FrEtrUpdateVo etrUpdatebyNE);
	public APIResponse updateFrTicketEtrApp(List<FrEtrUpdateVo> updateRequest);
	public APIResponse getDefectSubDefectCode();
	public APIResponse updateCCNRCompletedStatus(StatusVO status);
	
	public APIResponse reopenFrWorkOrder(MQWorkorderVO workOrder);
	public APIResponse assignFrTickets(AssignVO assignVO);
	public void updateFrTicketStatus(StatusVO status);
	public Map<String, Long> getFrTicketByWorkOrder(List<String> workOrders);
	public APIResponse managePermission(Long ticketId ,Long status);
	
	public List<TypeAheadVo> getAllTlForUser(Long userId);
	public List<Long> getAllVicinityFrTicket(Long ticketId);
	public APIResponse pushBackFrTicketForUser(FrTicketPushBackVo pushBackVo);
	
	public APIResponse updateFrPriority(String workOrderNo, int priority,
			Integer escalationType, Integer escalatedValue);
	public APIResponse getShiftingPermission(ShiftingRequestVo requestVo);
	public APIResponse getAllFxByTlUser(Long userId);
	
	public APIResponse tlRaiseSubTicketToBillingTeam(ShiftingRequestVo requestVo);
	public APIResponse createDefectSubDefectWithMQClosureCode(String fileName,
			String filePath, String addedOrRefresh);
	public APIResponse reloadSymptomFlowIdMappingFile();
	/**@author kiran
	 * Object
	 * @param currentUserId
	 * @return
	 */
	public List<TypeAheadVo>  getAllReportingTlList(Long currentUserId);
	
	/**@author Ashraf
	 * Object
	 * @param workOrder
	 * @return Integer
	 */
	public Integer getVicinityTicketPriority( MQWorkorderVO workOrder );

	
	/**@author Ashraf
	 * Object
	 * @param listOfObject
	 * @return 
	 */
	public void createFrTicketEtrInBulk(List<FrTicketEtrDataVo> listOfObject );
	
	/**@author Ashraf
	 * Object
	 * @param roiMQWorkorderVOList
	 * @param completeProspectMap
	 * 
	 * @return map
	 */
	
	public Map<Long, Boolean> createFrDetailInBulk(List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap);
	/**@author pawan
	 * FlowWiseDefectAndSubDefectCodeData
	 * @return
	 */
	FlowWiseDefectAndSubDefectCodeData getFrDeploymentSetting();
	
	public void updateFaltRepairActivityInCRM(Long ticketId, Long customerId, String workOrderNumber, Long cityId, Long branchId );

	/**@author Ashraf
	 * Object
	 * @param mqWorkOrder
	 * 
	 * @return 
	 */
	void dumpFailedWorkOrderMongoDb( List<MQWorkorderVO> mqWorkOrder );
	
}


