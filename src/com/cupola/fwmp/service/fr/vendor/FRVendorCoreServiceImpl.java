/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.service.fr.vendor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.FlowActionType;
import com.cupola.fwmp.FRConstant.FrReportConstant;
import com.cupola.fwmp.FRConstant.UserType;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.fr.vendor.SubTicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.persistance.entities.SubTickets;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.fr.FrTicketLockHelper;
import com.cupola.fwmp.service.ticket.TicketUpdateCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FRVendorVO;
import com.cupola.fwmp.vo.fr.FrFlowRequestVo;
import com.cupola.fwmp.vo.fr.VendorNEAssignVO;
import com.cupola.fwmp.vo.fr.VendorScheduleVO;
import com.cupola.fwmp.vo.fr.VendorTicketUpdateVO;

public class FRVendorCoreServiceImpl implements FRVendorCoreService
{
	@Autowired
	private SubTicketDAO frVendorDao;
	@Autowired
	private FrTicketDao frTicketDao;

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private FrTicketLockHelper frHelper;
	
	@Autowired
	TicketUpdateCoreService ticketUpdateCoreService;

	private static Logger LOGGER = Logger.getLogger(FRVendorCoreService.class
			.getName());

	@Override
	public APIResponse associateVendorUserToMultipleNE(
			List<VendorNEAssignVO> vendorNEAssignVO)
	{
		LOGGER.info("Associate Vendor To MultipleNE as VendorId::"
				+ vendorNEAssignVO);

		if (vendorNEAssignVO == null)
			return ResponseUtil.nullArgument();

		int insertRecords = frVendorDao
				.associateVendorUserToMultipleNE(vendorNEAssignVO);

		if (insertRecords <= -1)
			return ResponseUtil.createSaveFailedResponse();

		return ResponseUtil.createSuccessResponse();
	}

	@Override
	public APIResponse associateVendorUserToNE(long vendorUserId,
			long associateToUserId)
	{
		LOGGER.info("Associate Vendor To NE as VendorId::" + vendorUserId
				+ " and NE::" + associateToUserId);

		if (vendorUserId <= 0 || associateToUserId <= 0)
			return ResponseUtil.nullArgument();

		int insertRecord = frVendorDao.associateVendorUserToNE(vendorUserId,
				associateToUserId);

		if (insertRecord <= 0)
			return ResponseUtil.createSaveFailedResponse();

		return ResponseUtil.createSaveSuccessResponse();

	}

	@Override
	public APIResponse assignTicketToVendor(SubTickets vendorTicket,Long layoutId)
	{
		LOGGER.info("Assign Ticket to Vendor as VendorId::" + vendorTicket.getUserId()
				+ " and  TicketId::" + vendorTicket.getTicketId());

		if (vendorTicket.getUserId() <= 0 || vendorTicket.getTicketId() <= 0)
			return ResponseUtil.nullArgument();

		int assignTicket = frVendorDao.assignTicketToVendor(vendorTicket.getUserId(),
				vendorTicket.getTicketId());

		if (assignTicket <= 0)
			return ResponseUtil.createSaveFailedResponse();
		
		if(layoutId != null)
		{
			Long statusId = ActionTypeAttributeDefinitions.ACTION_STATUS_DEFINITION
					.get(layoutId);

			if (statusId != null)
			{
				StatusVO status = new StatusVO();
				status.setTicketId(vendorTicket.getTicketId());

				status.setStatus(statusId.intValue());
				ticketUpdateCoreService.updateTicketStatus(status);
			}
		}
		return ResponseUtil.createSaveSuccessResponse();

	}
	
	@Override
	public APIResponse createSubTicketTo( FrFlowRequestVo subTicket )
	{
		if( subTicket == null )
			return ResponseUtil.nullArgument();
		
		if ( subTicket.getUserId() <= 0 || subTicket.getTicketId() <= 0 )
			return ResponseUtil.nullArgument();
		
		if ( subTicket.getActionType() <= 0 )
			return ResponseUtil.nullArgument()
					.setData("Must set action type constant value ie ccnr, vendor or noc etc.");
		
		LOGGER.info("associating sub ticket to Vendor :: " + subTicket.getUserId()
				+ " and  TicketId::" + subTicket.getTicketId());
		
		if( subTicket.getRequestedBy() <= 0 )
			subTicket.setRequestedBy(AuthUtils.getCurrentUserId());
		
		try{
			
			Long statusId = null;
			
			if( subTicket.getStatus() != null && subTicket.getStatus() > 0 )
				statusId = Long.valueOf(subTicket.getStatus());
			else
				statusId = ActionTypeAttributeDefinitions.ACTION_STATUS_DEFINITION
						.get(subTicket.getActionType());
			
			
			if( statusId == null)
				return ResponseUtil.NoRecordFound().setData("invalid action type :"
						+ subTicket.getActionType());
			
			SubTickets ticket = new SubTickets();
			ticket.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			ticket.setRequestedBy(subTicket.getRequestedBy());
			ticket.setUserId(subTicket.getUserId());
			
			ticket.setUserType(Integer.valueOf(subTicket.getActionType()+""));
			if( UserType.VENDORS.contains(subTicket.getActionType()))
				ticket.setUserType(UserType.VENDOR);
			
			ticket.setTicketId(subTicket.getTicketId());
			ticket.setAddedBy(AuthUtils.getCurrentUserId());
			
			ticket.setAddedOn(new Date());
			ticket.setStatus(Integer.valueOf(statusId+""));
			ticket.setInStatus(Integer.valueOf(statusId+""));
			Integer skillId = ActionTypeAttributeDefinitions.ACTION_SKILL_DEFINITION.get(subTicket.getActionType());
			if(skillId != null)
			ticket.setSkillId(skillId);
		//	long result = frVendorDao.assignSubTicketToVendor(ticket);
			long result = frVendorDao.addSubTicketToVendor(ticket);
			
			if ( result > 0 )
			{
				StatusVO status = new StatusVO();
				status.setTicketId(subTicket.getTicketId());

				status.setStatus(statusId.intValue());
//				if(!( statusId.longValue() == FRStatus.FINANCE_TEAM_NOTIFIED))
					ticketUpdateCoreService.updateFrTicketStatus(status);
				
				if( FlowActionType.BLOCKED_FLOW.contains(subTicket.getActionType()))
					frHelper.blockedTicketForUser(subTicket.getRequestedBy(), 
							subTicket.getTicketId());
				
				LOGGER.info("Sub TT Raise successfully...");
				
				return ResponseUtil.createSaveSuccessResponse();
			}
			
		}
		catch(Exception ex)
		{
			LOGGER.error("Error occure while creating sub ticket for Team : "
					+ex.getMessage());
			ex.printStackTrace();
		}
		return ResponseUtil.createSaveFailedResponse();
	}

	@Override
	public APIResponse updateEstimatedTimeToTicketByVendor(
			SubTickets ticketData)
	{
		LOGGER.info("Update EstimatedTime To Ticket By Vendor::" + ticketData);

		if (ticketData == null)
			return ResponseUtil.nullArgument();
		if(ticketData.getUserId() == null)
			ticketData.setUserId(AuthUtils.getCurrentUserId());

		if (ticketData.getTicketId() == null || ticketData.getUserId() == null
				|| ticketData.getEstimatedStartTime() == null
				|| ticketData.getEstimatedStartTime() == null)

			return ResponseUtil.nullArgument();

		int updateRecord = frVendorDao
				.updateEstimatedTimeToTicketByVendor(ticketData);
		if (updateRecord == 0)
			return ResponseUtil.createUpdateFailedResponse();

		return ResponseUtil.createSaveSuccessResponse();

	}

	@Override
	public APIResponse updateActualTimeToTicketByVendor(SubTickets ticketData)
	{
		LOGGER.info("Update ActualTime To Ticket By Vendor::" + ticketData);

		if (ticketData == null)
			return ResponseUtil.nullArgument();

		if (ticketData.getTicketId() == null || ticketData.getUserId() == null
				|| ticketData.getActualEndTime() == null
				|| ticketData.getActualStartTime() == null)

			return ResponseUtil.nullArgument();

		int updateRecord = frVendorDao
				.updateActualTimeToTicketByVendor(ticketData);

		if (updateRecord <= 0)
			return ResponseUtil.createUpdateFailedResponse();

		return ResponseUtil.createSaveSuccessResponse();

	}

	@Override
	public APIResponse updateTicketStatus(long ticketId, int status)
	{
		LOGGER.info("Update Ticket Status as TicketId::" + ticketId);

		if (ticketId <= 0)
			return ResponseUtil.nullArgument();

		int result = frVendorDao.updateTicketStatus(ticketId, status);
 
		if ( result > 0 )
		{
			StatusVO statusVo = new StatusVO();
			statusVo.setTicketId(ticketId);

			statusVo.setStatus(status );
			ticketUpdateCoreService.updateFrTicketStatus(statusVo);
			return ResponseUtil.createSuccessResponse();
		}
		
		return ResponseUtil.createUpdateFailedResponse();

	}
	
	@Override
	public APIResponse updateSubTicketStatusByCCNR(long ticketId, int status)
	{
		LOGGER.info("Update Ticket Status as TicketId::" + ticketId);

		if (ticketId <= 0)
			return ResponseUtil.nullArgument();

		int result = frVendorDao.updateTicketStatusByCCNR(ticketId, status);
 
		if ( result > 0 )
		{
			StatusVO statusVo = new StatusVO();
			statusVo.setTicketId(ticketId);

			statusVo.setStatus(status );
			ticketUpdateCoreService.updateFrTicketStatus(statusVo);
			return ResponseUtil.createSuccessResponse();
		}
		
		return ResponseUtil.createUpdateFailedResponse();

	}

	@Override
	public APIResponse getAllSkills()
	{
		LOGGER.info("Get All Skills");
		List<TypeAheadVo> results = frVendorDao.getAllSkills();

		if (results == null || results.isEmpty())
			return ResponseUtil.createRecordNotFoundResponse();

		return ResponseUtil.createSuccessResponse().setData(results);

	}

	@Override
	public APIResponse getVendorUserSchedule(long vendorUserId)
	{
		LOGGER.info("Get VendorUser Schedule as vendorId::" + vendorUserId);

		if (vendorUserId <= 0)
			return ResponseUtil.nullArgument();

		List<VendorScheduleVO> results = frVendorDao.getVendorUserSchedule(
				vendorUserId, FrReportConstant.PENDING_DEF);

		if (results == null || results.isEmpty())
			return ResponseUtil.createRecordNotFoundResponse();

		return ResponseUtil.createSuccessResponse().setData(results);

	}

	public APIResponse getVendors(FRVendorFilter filter)
	{
		LOGGER.info("Get Vendor By Filters::" + filter);

		List<TypeAheadVo> results = frVendorDao.getVendors(filter);

		if (results == null || results.isEmpty())
			return ResponseUtil.createRecordNotFoundResponse();

		return ResponseUtil.createSuccessResponse().setData(results);

	}

	 
	public APIResponse getVendorsWithDetails(FRVendorFilter filter)
	{
		LOGGER.info("Get Vendor By Filters::" + filter);

		List<FRVendorVO> results = frVendorDao.getVendorsWithDetails(filter);

		if (results == null || results.isEmpty())
			return ResponseUtil.createRecordNotFoundResponse();
		
		return ResponseUtil.createSuccessResponse().setData(results);

	}
	
	@Override
	public APIResponse getReportingVendors(FRVendorFilter filter)
	{
		return ResponseUtil.createSuccessResponse().setData(
				frVendorDao.getReportingVendors(filter));
	}

	@Override
	public APIResponse getAllFrEngineers()
	{
		return ResponseUtil.createSuccessResponse().setData(
				userDao.getAllFrUserNames());
	}
	
	@Override
	public APIResponse getAllTlEngineers(Long userId)
	{
		List<TypeAheadVo> frNeNames = new ArrayList<TypeAheadVo>();
		/**
		 * @author kiran
		 * from ui tl id is getting means it will use user id if it comes null means it will take current userId
		 */
		Map<Long,String> tlNes = userDao.getReportingUsers(((userId == null) || (userId <=0 ) )? AuthUtils.getCurrentUserId() : userId);
		
		TypeAheadVo user = null;
		for (Map.Entry<Long, String> entry : tlNes.entrySet())
		{
			if( AuthUtils.isFRUser())
			{
				if (AuthUtils.isFrCxDownNEUser(entry.getKey())
						|| AuthUtils.isFrCxUpNEUser(entry.getKey()))
				{
					user = new TypeAheadVo();
					user.setId(entry.getKey());
					user.setName(entry.getValue());
					frNeNames.add(user);
				}
			}
			else ;
		}
			
		return ResponseUtil.createSuccessResponse()
				.setData(frNeNames);
	}

	@Override
	public APIResponse assignOrUpdateVendorTicket(VendorTicketUpdateVO vo)
	{

		return ResponseUtil.createSuccessResponse().setData(
				frVendorDao.assignOrUpdateVendorTicket(vo));
	}
	
	@Override
	public APIResponse getVendorTicketSummary()
	{
		return ResponseUtil.createSuccessResponse().setData(
				frTicketDao.getVendorTicketSummary());
	}
}
