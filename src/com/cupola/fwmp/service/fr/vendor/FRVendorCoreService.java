/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.service.fr.vendor;

import java.util.List;

import com.cupola.fwmp.persistance.entities.SubTickets;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FrFlowRequestVo;
import com.cupola.fwmp.vo.fr.VendorNEAssignVO;
import com.cupola.fwmp.vo.fr.VendorTicketUpdateVO;

public interface FRVendorCoreService
{

	APIResponse getVendorUserSchedule(long vendorUserId);

	APIResponse updateTicketStatus(long ticketId,int status);

	APIResponse updateActualTimeToTicketByVendor(SubTickets ticketData);


	APIResponse assignTicketToVendor(SubTickets vendorTicket,Long layoutId);

	APIResponse associateVendorUserToNE(long vendorUserId,
			long associateToUserId);

	APIResponse associateVendorUserToMultipleNE(List<VendorNEAssignVO> listofVendorsNEs);

	APIResponse getAllSkills();
	
	APIResponse getVendors(FRVendorFilter filter);

	APIResponse updateEstimatedTimeToTicketByVendor(SubTickets ticketData);

	APIResponse getReportingVendors(FRVendorFilter filter);

	APIResponse getAllFrEngineers();
	
	
	APIResponse assignOrUpdateVendorTicket(VendorTicketUpdateVO vo);

	APIResponse getVendorsWithDetails(FRVendorFilter filter);

	APIResponse getVendorTicketSummary();

	APIResponse getAllTlEngineers(Long userId);

	APIResponse createSubTicketTo(FrFlowRequestVo subTicket);

	APIResponse updateSubTicketStatusByCCNR(long ticketId, int status);


}
