package com.cupola.fwmp.service.fr.vendor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.cupola.fwmp.dao.fr.vendor.SubTicketDAO;
import com.cupola.fwmp.persistance.entities.VendorAllocation;

public class VendorCache 
{
	private static final Logger LOGGER = Logger.getLogger(VendorCache.class);
	
	private static Map<Long , List<Long>> vendorCache = new ConcurrentHashMap<Long , List<Long>>();
	private static Map<Long , List<Long>> tlVendorsCache = new ConcurrentHashMap<Long , List<Long>>();
	private static SubTicketDAO vendorDao;
	
	public void init(){
		LOGGER.info("Initializing vendor cache...");
		refreshCache();
		refreshTlVendorCache();
		LOGGER.info("Initialization vendor cache done..."+vendorCache);
	}
	
	public static  void resetCache()
	{
		vendorCache.clear();
		LOGGER.info("LocationCache cache reset done...");
	}

	public static  void refreshCache()
	{
		resetCache();
		List<VendorAllocation> allocatedVendor = vendorDao.getAllVendorAssociateWithNes();
		if( allocatedVendor != null && !allocatedVendor.isEmpty())
		{
			List<Long> listOfVendors = null;
			for(VendorAllocation vendor : allocatedVendor)
			{
				Long assignedTo = vendor.getCurrentAllocatedToUserId();
				if( vendorCache.containsKey(assignedTo))
				{
					listOfVendors = vendorCache.get(assignedTo);
					listOfVendors.add(vendor.getUserId());
					vendorCache.put(assignedTo,listOfVendors);
				}else{
					listOfVendors = new ArrayList<Long>();
					listOfVendors.add(vendor.getUserId());
					vendorCache.put(assignedTo,listOfVendors);
				}
			}
		}
	}
	
	public static void addToVendorCahe(){
		 refreshCache();
	}

	public static void addToVendorCahe(Long associateToUserId, long vendorUserId)
	{
		if( vendorCache.containsKey(associateToUserId) 
				&& vendorCache.get(associateToUserId).contains(vendorUserId))
			;
		else{
			List<Long> listOfVendors = new ArrayList<Long>();
			listOfVendors.add(vendorUserId);
			vendorCache.put(associateToUserId,listOfVendors);
		}
	}
	
	public static List<Long> getAssociatedVendors(Long userId)
	{
		if( userId != null)
			return vendorCache.get(userId);
		else
			return null;
	}
	
	public static List<Long> getTlAssociatedVendors( Long tlId )
	{
		if( tlId == null)
			return new ArrayList<Long>();
		
		if( tlVendorsCache.isEmpty() )
			refreshTlVendorCache();
		
		return tlVendorsCache.get(tlId);
	}
	
	private static void refreshTlVendorCache()
	{
		Map<Long,List<Long>> vendors = vendorDao.getAllAssociateVendorIdsWithTl();
		if( vendors != null && !vendors.isEmpty())
			tlVendorsCache.putAll(vendors);
	}
	
	public static SubTicketDAO getVendorDao() {
		return vendorDao;
	}

	public static void setVendorDao(SubTicketDAO vendorDao) {
		VendorCache.vendorDao = vendorDao;
	}
	
}
