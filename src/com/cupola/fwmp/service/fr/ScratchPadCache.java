/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.service.fr;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cupola.fwmp.vo.fr.Activity;
import com.cupola.fwmp.vo.fr.AndroidFlowCxDown;
import com.cupola.fwmp.vo.fr.Decision;
import com.cupola.fwmp.vo.fr.DefectCode;
import com.cupola.fwmp.vo.fr.ETRDetails;
import com.cupola.fwmp.vo.fr.SubDefectCode;

/**
 * 
 * @author kiran
 *
 */

public class ScratchPadCache {
	static final Logger logger = Logger.getLogger(ScratchPadCache.class);

	public static Map<Integer, Decision> decisionData = new HashMap<Integer, Decision>();
	public static Map<Integer, Activity> activityData = new HashMap<Integer, Activity>();
	public static Map<Integer, DefectCode> defectCodeData = new HashMap<Integer, DefectCode>();
	public static Map<Integer, SubDefectCode> subDefectCodeData = new HashMap<Integer, SubDefectCode>();
	public static Map<Integer, ETRDetails> etrData = new HashMap<Integer, ETRDetails>();
	public static Map<Integer, AndroidFlowCxDown> cxDownFlowData = new HashMap<Integer, AndroidFlowCxDown>();
	public static Map<Integer, AndroidFlowCxDown> slowSpeedFlowData = new HashMap<Integer, AndroidFlowCxDown>();
	public static Map<Integer, AndroidFlowCxDown> fdFlowData = new HashMap<Integer, AndroidFlowCxDown>();
	public static Map<Integer, AndroidFlowCxDown> cxUpFlowData = new HashMap<Integer, AndroidFlowCxDown>();
	public static Map<Integer, AndroidFlowCxDown> srElementFlowData = new HashMap<Integer, AndroidFlowCxDown>();
	public static int noOfSheets = 10;
	
	  ClassLoader classLoader = this.getClass().getClassLoader();

	

	public static void reFreshCache() throws IOException {
		resetCache();
		readFromExcel("/fr/defnition_doc/FR_Def.xlsx");
		
	}
	public static   Decision  getDecisionById( Integer id) throws IOException
	{
		//resetCacheIfOld();
		return  decisionData.get( id );
	}
	public static   Activity  getActivityById( Integer id) throws IOException
	{
		//resetCacheIfOld();
		return  activityData.get( id );
	}
	public static   ETRDetails  getETRDetailsById( Integer id) throws IOException
	{
		//resetCacheIfOld();
		return  etrData.get( id );
	}

	public static void readFromExcel(String file) throws IOException {
		InputStream input = null;
		try {

			input = ScratchPadCache.class.getResourceAsStream(file);

			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(input);
			for (int i = 0; i < noOfSheets; i++) {
				switch (i) {
				case 0: // Get first/desired sheet from the workbook
					XSSFSheet decisionSheet = workbook.getSheetAt(i);

					// Iterate through each rows one by one
					Iterator<Row> decisionSheetRowIterator = decisionSheet.iterator();
					decisionSheetRowIterator.hasNext();
					while (decisionSheetRowIterator.hasNext()) {
						Row decisionSheetRow = decisionSheetRowIterator.next();
						// For each row, iterate through all the columns
						putDecisionDataInMap(decisionSheetRow);

					}

					break;
				case 1:
					XSSFSheet activitySheet = workbook.getSheetAt(i);

					// Iterate through each rows one by one
					Iterator<Row> activitySheetRowIterator = activitySheet.iterator();
					activitySheetRowIterator.hasNext();
					while (activitySheetRowIterator.hasNext()) {
						Row activitySheetRow = activitySheetRowIterator.next();
						// For each row, iterate through all the columns
						putActivityDataInMap(activitySheetRow);

					}

					break;
				case 2:
					XSSFSheet defectCodeSheet = workbook.getSheetAt(i);

					// Iterate through each rows one by one
					Iterator<Row> defectCodeSheetRowIterator = defectCodeSheet.iterator();
					defectCodeSheetRowIterator.hasNext();
					while (defectCodeSheetRowIterator.hasNext()) {
						Row defectCodeSheetRow = defectCodeSheetRowIterator.next();
						// For each row, iterate through all the columns
						putDefectCodeDataInMap(defectCodeSheetRow);

					}

					break;
				case 3:
					XSSFSheet subDefectCodeSheet = workbook.getSheetAt(i);

					// Iterate through each rows one by one
					Iterator<Row> subDefectCodeSheetRowIterator = subDefectCodeSheet.iterator();
					subDefectCodeSheetRowIterator.hasNext();
					while (subDefectCodeSheetRowIterator.hasNext()) {
						Row subDefectCodeSheetRow = subDefectCodeSheetRowIterator.next();
						// For each row, iterate through all the columns
						putSubDefectCodeDataInMap(subDefectCodeSheetRow);

					}
					break;
				case 4:
					XSSFSheet etrSheet = workbook.getSheetAt(i);

					// Iterate through each rows one by one
					Iterator<Row> etrSheetRowIterator = etrSheet.iterator();
					etrSheetRowIterator.hasNext();
					while (etrSheetRowIterator.hasNext()) {
						Row etrSheetRow = etrSheetRowIterator.next();
						// For each row, iterate through all the columns
						putETRDataInMap(etrSheetRow);

					}

					break;
				case 5:
					XSSFSheet cxDownFlowSheet = workbook.getSheetAt(i);
					// Iterate through each rows one by one
					Iterator<Row> cxDownFlowSheetRowIterator = cxDownFlowSheet.iterator();
					cxDownFlowSheetRowIterator.hasNext();
					while (cxDownFlowSheetRowIterator.hasNext()) {
						Row cxDownFlowSheetRow = cxDownFlowSheetRowIterator.next();
						// For each row, iterate through all the columns
						putCxDownFlowInMap(cxDownFlowSheetRow);

					}
					break;
				case 6:
					XSSFSheet slowSpeedSheet = workbook.getSheetAt(i);
					// Iterate through each rows one by one
					Iterator<Row> slowSpeedSheetRowIterator = slowSpeedSheet.iterator();
					slowSpeedSheetRowIterator.hasNext();
					while (slowSpeedSheetRowIterator.hasNext()) {
						Row slowSpeedSheetRow = slowSpeedSheetRowIterator.next();
						// For each row, iterate through all the columns
						putSlowSpeedFlowInMap(slowSpeedSheetRow);

					}
					break;
					
				case 7:
					XSSFSheet fdSheet = workbook.getSheetAt(i);
					// Iterate through each rows one by one
					Iterator<Row> fdSheetRowIterator = fdSheet.iterator();
					fdSheetRowIterator.hasNext();
					while (fdSheetRowIterator.hasNext()) {
						Row fdSheetRow = fdSheetRowIterator.next();
						// For each row, iterate through all the columns
						putFDFlowInMap(fdSheetRow);

					}
					break;
				case 8:
					XSSFSheet cxUpSheet = workbook.getSheetAt(i);
					// Iterate through each rows one by one
					Iterator<Row> cxUpSheetRowIterator = cxUpSheet.iterator();
					cxUpSheetRowIterator.hasNext();
					while (cxUpSheetRowIterator.hasNext()) {
						Row cxUpSheetRow = cxUpSheetRowIterator.next();
						// For each row, iterate through all the columns
						putCxUpFlowInMap(cxUpSheetRow);

					}
					break;
				case 9:
					XSSFSheet srElementSheet = workbook.getSheetAt(i);
					// Iterate through each rows one by one
					Iterator<Row> srElementSheetRowIterator = srElementSheet.iterator();
					srElementSheetRowIterator.hasNext();
					while (srElementSheetRowIterator.hasNext()) {
						Row srElementSheetRow = srElementSheetRowIterator.next();
						// For each row, iterate through all the columns
						putSrElementFlowInMap(srElementSheetRow);

					}
					break;
				default:
					break;

				}

			}
		} catch (Exception e) {
			logger.info("Scratchpad cache error"+e.getMessage());
		} finally {
			if(input != null)
			input.close();
		}
	}
	
	private static void putSrElementFlowInMap(Row srElementSheetRow)
	{

		AndroidFlowCxDown androidFlowCxDown = new AndroidFlowCxDown();
		if(srElementSheetRow.getRowNum() != 0)
		{
			for(Cell cell : srElementSheetRow){
				cell.setCellType(Cell.CELL_TYPE_STRING);
				switch (cell.getColumnIndex()) {
				case 0: 
					androidFlowCxDown.setId(Integer.parseInt(srElementSheetRow.getCell(cell.getColumnIndex()).getStringCellValue()));
					break;
				case 1: 
					androidFlowCxDown.setType(srElementSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 2: 
					androidFlowCxDown.setTypeId(srElementSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 3: 
					androidFlowCxDown.setSelectedOption(srElementSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 4: 
					androidFlowCxDown.setJumpId(srElementSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 5: 
					androidFlowCxDown.setAndroidId(srElementSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 6: 
					androidFlowCxDown.setStepId(srElementSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				default:
					break;
				}
				srElementFlowData.put(androidFlowCxDown.getId(),androidFlowCxDown);
			}
		}
		
	
		
	}
	/**
	 * 
	 * @param cxUpSheetRow
	 */
	private static void putCxUpFlowInMap(Row cxUpSheetRow) {

		AndroidFlowCxDown androidFlowCxDown = new AndroidFlowCxDown();
		if(cxUpSheetRow.getRowNum() != 0)
		{
			for(Cell cell : cxUpSheetRow){
				cell.setCellType(Cell.CELL_TYPE_STRING);
				switch (cell.getColumnIndex()) {
				case 0: 
					androidFlowCxDown.setId(Integer.parseInt(cxUpSheetRow.getCell(cell.getColumnIndex()).getStringCellValue()));
					break;
				case 1: 
					androidFlowCxDown.setType(cxUpSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 2: 
					androidFlowCxDown.setTypeId(cxUpSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 3: 
					androidFlowCxDown.setSelectedOption(cxUpSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 4: 
					androidFlowCxDown.setJumpId(cxUpSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 5: 
					androidFlowCxDown.setAndroidId(cxUpSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 6: 
					androidFlowCxDown.setStepId(cxUpSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				default:
					break;
				}
				cxUpFlowData.put(androidFlowCxDown.getId(),androidFlowCxDown);
			}
		}
		
	
		
	}

	private static void putFDFlowInMap(Row fdSheetRow) {

		AndroidFlowCxDown androidFlowCxDown = new AndroidFlowCxDown();
		if(fdSheetRow.getRowNum() != 0)
		{
			for(Cell cell : fdSheetRow){
				cell.setCellType(Cell.CELL_TYPE_STRING);
				switch (cell.getColumnIndex()) {
				case 0: 
					androidFlowCxDown.setId(Integer.parseInt(fdSheetRow.getCell(cell.getColumnIndex()).getStringCellValue()));
					break;
				case 1: 
					androidFlowCxDown.setType(fdSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 2: 
					androidFlowCxDown.setTypeId(fdSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 3: 
					androidFlowCxDown.setSelectedOption(fdSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 4: 
					androidFlowCxDown.setJumpId(fdSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 5: 
					androidFlowCxDown.setAndroidId(fdSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 6: 
					androidFlowCxDown.setStepId(fdSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				default:
					break;
				}
				fdFlowData.put(androidFlowCxDown.getId(),androidFlowCxDown);
			}
		}
		
	
		
	}

	private static void putSlowSpeedFlowInMap(Row slowSpeedSheetRow) {

		AndroidFlowCxDown androidFlowCxDown = new AndroidFlowCxDown();
		if(slowSpeedSheetRow.getRowNum() != 0)
		{
			for(Cell cell : slowSpeedSheetRow){
				cell.setCellType(Cell.CELL_TYPE_STRING);
				switch (cell.getColumnIndex()) {
				case 0: 
					androidFlowCxDown.setId(Integer.parseInt(slowSpeedSheetRow.getCell(cell.getColumnIndex()).getStringCellValue()));
					break;
				case 1: 
					androidFlowCxDown.setType(slowSpeedSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 2: 
					androidFlowCxDown.setTypeId(slowSpeedSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 3: 
					androidFlowCxDown.setSelectedOption(slowSpeedSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 4: 
					androidFlowCxDown.setJumpId(slowSpeedSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 5: 
					androidFlowCxDown.setAndroidId(slowSpeedSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 6: 
					androidFlowCxDown.setStepId(slowSpeedSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				default:
					break;
				}
				slowSpeedFlowData.put(androidFlowCxDown.getId(),androidFlowCxDown);
			}
		}
		
	
		
	}

	private static void putCxDownFlowInMap(Row cxDownFlowSheetRow) {

		AndroidFlowCxDown androidFlowCxDown = new AndroidFlowCxDown();
		if(cxDownFlowSheetRow.getRowNum() != 0)
		{
			for(Cell cell : cxDownFlowSheetRow){
				cell.setCellType(Cell.CELL_TYPE_STRING);
				switch (cell.getColumnIndex()) {
				case 0: 
					androidFlowCxDown.setId(Integer.parseInt(cxDownFlowSheetRow.getCell(cell.getColumnIndex()).getStringCellValue()));
					break;
				case 1: 
					androidFlowCxDown.setType(cxDownFlowSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 2: 
					androidFlowCxDown.setTypeId(cxDownFlowSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 3: 
					androidFlowCxDown.setSelectedOption(cxDownFlowSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 4: 
					androidFlowCxDown.setJumpId(cxDownFlowSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 5: 
					androidFlowCxDown.setAndroidId(cxDownFlowSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 6: 
					androidFlowCxDown.setStepId(cxDownFlowSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				default:
					break;
				}
				cxDownFlowData.put(androidFlowCxDown.getId(),androidFlowCxDown);
			}
		}
		
	
		
	}

	private static void putETRDataInMap(Row etrSheetRow) {
		// TODO Auto-generated method stub
		ETRDetails etrDetatils = new ETRDetails();
		if(etrSheetRow.getRowNum() != 0)
		{
			for(Cell cell : etrSheetRow){
				switch (cell.getColumnIndex()) {
				case 0: 
					etrDetatils.setStepID((int)etrSheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				case 1: 
					etrDetatils.setStepName(etrSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 2: 
					etrDetatils.setEtr((int)etrSheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				case 3: 
					etrDetatils.setProgressPercentage((int)etrSheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				default:
					break;
				}
				etrData.put(etrDetatils.getStepID(), etrDetatils);
			}
		}
		
	}

	private static void putSubDefectCodeDataInMap(Row subDefectCodeSheetRow) {
		// TODO Auto-generated method stub
		SubDefectCode subDefectCode = new SubDefectCode();
		if(subDefectCodeSheetRow.getRowNum() != 0)
		{
			for (Cell cell : subDefectCodeSheetRow) {
				switch (cell.getColumnIndex()) {
				case 0: 
					subDefectCode.setSubDefectCodeId((int)subDefectCodeSheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				case 1: 
					subDefectCode.setSubDefectCodeName(subDefectCodeSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				default:
					break;
				}
				subDefectCodeData.put(subDefectCode.getSubDefectCodeId(),subDefectCode);
			}
		}
	}

	private static void putDefectCodeDataInMap(Row defectCodeSheetRow) {
		// TODO Auto-generated method stub
		DefectCode defectCode = new DefectCode();
		if(defectCodeSheetRow.getRowNum() != 0)
		{
			for (Cell cell : defectCodeSheetRow) {
				switch (cell.getColumnIndex()) {
				case 0: 
					defectCode.setDefectCodeId((int)defectCodeSheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				case 1: 
					defectCode.setDefectCodeName(defectCodeSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				default:
					break;
				}
				defectCodeData.put(defectCode.getDefectCodeId(),defectCode);
			}
		}
	}

	private static void putActivityDataInMap(Row activitySheetRow) {
		// TODO Auto-generated method stub
		Activity activity = new Activity();
		 if (activitySheetRow.getRowNum()!=0) {
			 
			 for (Cell cell : activitySheetRow) {
				
				//System.out.println();
				switch (cell.getColumnIndex()) {
				case 0: 
					activity.setActivityId((int)activitySheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				case 1: 
					activity.setActivityShortName(activitySheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
					
				case 2: 
					activity.setActivityName(activitySheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				

				default:
					break;
				}
				
				activityData.put(activity.getActivityId(),activity);
				
	}
		 }
	}

	private static void putDecisionDataInMap(Row decisionSheetRow) {
		// TODO Auto-generated method stub
		Decision decision = new Decision();
		 if (decisionSheetRow.getRowNum()!=0) {
			 
			 for (Cell cell : decisionSheetRow) {
				
				//System.out.println();
				switch (cell.getColumnIndex()) {
				case 0: 
					decision.setDecisionId((int)decisionSheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				case 1: 
					decision.setShortName(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
					
				case 2: 
					decision.setDecisionName(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					break;
				case 3: 
					decision.setNoOfOptions((int)decisionSheetRow.getCell(cell.getColumnIndex()).getNumericCellValue());
					break;
				case 4: 
					if(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue() != null)
					{
					decision.setOption1(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					}
					break;
				case 5: 
					if(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue() != null)
					{
					decision.setOption2(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					}
					break;
				case 6: 
					if(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue() != null)
					{
					decision.setOption3(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					}
					break;
				case 7: 
					if(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue() != null)
					{
					decision.setOption4(decisionSheetRow.getCell(cell.getColumnIndex()).getStringCellValue());
					}
					break;

				default:
					break;
				}
				
				decisionData.put(decision.getDecisionId(), decision);
				
				
			 }
		 }
	}


	public static  void resetCache()
	{
		decisionData.clear();
		activityData.clear();
		defectCodeData.clear();
		subDefectCodeData.clear();
		etrData.clear();
		cxDownFlowData.clear();
		slowSpeedFlowData.clear();
		fdFlowData.clear();
		cxUpFlowData.clear();
		srElementFlowData.clear();
		logger.debug("ScratchPadCache cache reset done");
	}
	
}
