package com.cupola.fwmp.service.definition;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TypeAheadVo;

public interface DefinitionCoreService
{

	public final Integer PAYMENT_RELATED_STATUS = 1;
	public final Integer DOCUMENT_RELATED_STATUS = 2;
	public final Integer REFUND_RELATED_STATUS = 5;
	public final Integer MQ_FAILURE_RELATED_STATUS = 6;
	public final Integer ECAF_RELATED_STATUS = 7;

	public final long PAYMENT_REASON_GROUP_CODE = 1;
	public final long PAYMENT_ACCEPT_STATUS_CODE = 1;
	public final long PAYMENT_REJECT_STATUS_CODE = 2;
	public final long PAYMENT_REFUND_STATUS_CODE = 3;

	public final long DOCUMENT_REASON_GROUP = 2;
	public final long DOCUMENT_ACCEPT_STATUS_CODE = 1;
	public final long DOCUMENT_REJECT_STATUS_CODE = 2;

	public final long ETR_REASON_GROUP = 3;
	public final long ETR_UPDATED = 1;
	public final long ETR_NOT_UPDATED = 2;

	public final long REASSIGN_REASON_GROUP = 4;
	public final long REASSIGN_ACCEPT_STATUS_CODE = 1;
	public final long REASSIGN_REJECT_STATUS_CODE = 2;

	public final long RESOLVE_REASON_GROUP = 6;
	public final String PUSH_BACK_FRTICKET_LIMIT = "afp.frticket.ne.push.back";

	public final static String FILTER_FR_STATUS = "filter.status.fr.search.option";

	public final static String SALES_STATUS_FILTER = "filter.status.sales";
	public final static String SALES_TL_STATUS_FILTER = "filter.status.sales.tl";

	public final static String SALES_MANAGER_STATUS_FILTER = "filter.status.sales.manager";
	public final static String NI_MANAGER_STATUS_FILTER = "filter.status.ni.manager";
	public final static String NI_TL_STATUS_FILTER = "filter.status.ni.tl";
	public final static String NI_NE_STATUS_FILTER = "filter.status.ne";

	public final static String CFE_STATUS_FILTER = "filter.status.cfe";
	public final static String CFE_AT_PAYMENT_STATUS_FILTER = "filter.status.cfe.payment";
	public final static String CFE_AT_REFUND_STATUS_FILTER = "filter.status.cfe.refund";
	public final static String CFE_AT_DOCUMENT_STATUS_FILTER = "filter.status.cfe.document";
	public final static String CFE_AT_POA_DOCUMENT_STATUS_FILTER = "filter.status.cfe.poa.document";
	public final static String CFE_AT_POI_DOCUMENT_STATUS_FILTER = "filter.status.cfe.poi.document";
	public final static String CC_STATUS_FILTER = "filter.status.cc";

	public final static String COMPLETED_STATUS_FILTER = "filter.status.completed";

	public final static String REACH_TEAM_STATUS_FILTER = "filter.status.reach.team";

	public final static String PROSPECT_TYPE_FILTER_SATUS = "filter.satus.prospect.type";

	public final static String FILTER_VERY_HIGH = "filter.priority.value.veryhigh";
	public final static String FILTER_HIGH = "filter.priority.value.high";
	public final static String FILTER_MEDIUM = "filter.priority.value.medium";
	public final static String FILTER_LOW = "filter.priority.value.low";

	public final static String NEW_INSTALLATION_CATEG = "mq.new.installation.categ";
	public final static String NEW_INSTALLATION_SYMPTOM = "mq.new.installation.symptom";

	public final static String SHIFTING_CATEG = "mq.shifting.categ";
	public final static String SHIFTING_CONNECTION_SYMPTOM = "mq.shifting.symptom";

	public final static String RE_ACTIVATION_CATEG = "mq.re.activation.categ";
	public final static String RE_ACTIVATION_SYMPTOM = "mq.re.activation.symptom";

	public final static String PERMISSION_STATUS_FILTER = "filter.sales.permission.status";
	public final static String FEASIBLITY_STATUS_FILTER = "filter.sales.feasibility.status";

	public final static String FILTER_SALES_NEW_TICKET = "filter.sales.new.ticket";
	public final static String FILTER_SALES_HOT_TICKET = "filter.sales.hot.ticket";
	public final static String FILTER_SALES_MEDIUM_TICKET = "filter.sales.medium.ticket";
	public final static String FILTER_SALES_COLD_TICKET = "filter.sales.cold.ticket";
	public final static String FILTER_SALES_FEASIBILITY_PENDING_TICKET = "filter.sales.feasibility.pending.ticket";
	public final static String FILTER_SALES_FEASIBILITY_APPROVED_TICKET = "filter.sales.feasibility.approved.ticket";
	public final static String FILTER_SALES_DOCUMENT_PENDING_WITH_CFE_TICKET = "filter.sales.document.pending.with.cfe.ticket";
	public final static String FILTER_SALES_POI_DOCUMENT_PENDING_WITH_CFE_TICKET = "filter.sales.poi.document.pending.with.cfe.ticket";
	public final static String FILTER_SALES_POA_DOCUMENT_PENDING_WITH_CFE_TICKET = "filter.sales.poa.document.pending.with.cfe.ticket";
	public final static String FILTER_SALES_PAYMENT_PENDING_WITH_CFE_TICKET = "filter.sales.payment.pending.with.cfe.ticket";
	public final static String FILTER_SALES_PERMISSION_TICKET = "filter.sales.permission.ticket";
	public final static String FILTER_SALES_CAF_ACCEPTED_BY_CFE_TICKET = "filter.sales.caf.accepted.by.cfe.ticket";

	public final static String FILTER_FR_TL = "filter.status.fr.manager";
	public final static String FILTER_LEGAL = "filter.status.legal";
	public final static String FILTER_CCNR = "filter.status.ccnr";
	public final static String FILTER_FINANCE = "filter.status.finance";
	public final static String FILTER_CHURN = "filter.status.churn";
	public final static String FILTER_NOC = "filter.status.noc";
	public final static String TICKET_PENDING_STATUS = "status.where.ticket.pending.for";

	public final static String DASHBOARD_FR_CX_UP_SYMPTOMS = "dashboard.fr.cx.up.symptomcodes";
	public final static String DASHBOARD_FR_CX_DOWN_SYMPTOMS = "dashboard.fr.cx.down.symptomcodes";
	public final static String DASHBOARD_FR_SR_SYMPTOMS = "dashboard.fr.srelement.symptomcodes";
	
	public final static String DASHBOARD_FR_CX_UP_SYMPTOMS_HYD = "dashboard.fr.cx.up.symptomcodes.hyderabad";
	public final static String DASHBOARD_FR_CX_DOWN_SYMPTOMS_HYD = "dashboard.fr.cx.down.symptomcodes.hyderabad";
	public final static String DASHBOARD_FR_SR_SYMPTOMS_HYD = "dashboard.fr.srelement.symptomcodes.hyderabad";
	
	public final static String SMS_GATEWAY_ADDRESS = "sms.gateway.url";
	public final static String WO_CREATION_MESSAGE_KEY = "deployment.ticket.creation";
	public final static String ETR_MESSAGE_KEY = "etr.while.generating.wo";
	public final static String PROSPECT_FEASIBLE_MESSAGE_KEY = "prospect.feasible.message";
	public final static String PROSPECT_NON_FEASIBLE_MESSAGE_KEY = "prospect.non.feasible.message";
	
	public final static String MQ_ROI_QUERY = "mq.roi.get.all.open.ticket";
	public final static String MQ_HYD_QUERY = "mq.hyd.get.all.open.ticket";
	
	public final static String MQ_FR_ROI_QUERY = "mq.roi.get.all.open.fr.ticket";
	public final static String MQ_FR_ROI_QUERY_BRANCH = "mq.roi.get.all.open.fr.ticket.branch";
	public final static String MQ_FR_HYD_QUERY = "mq.hyd.get.all.open.fr.ticket";
	public final static String FR_QUEUE_QUERY_KEY = "fr.queue.for.neandtl.query";
	
	public final static String PROSPECT_QUEUE_QUERY = "fwmp.all.open.prospect.from.fwmp";
	public final static String PROSPECT_CREATION_USER_MESSAGE_KEY = "prospect.creation.user.message";
	public final static String REMOVE_SALES_COMPLETED_TICKET = "fwmp.get.garbage.eligible.ticket.for.sales";
	public final static String REMOVE_DEPLOYMENT_COMPLETED_TICKET = "fwmp.get.garbage.eligible.ticket.for.deployment";
	
	public final static String REMOVE_FR_COMPLETED_TICKET = "fwmp.get.garbage.eligible.ticket.for.fr";
	
	public final static String PREPARE_DEPLOYMENT_QUEUE = "fwmp.all.open.ticket.from.wo";
	
	
	
	public final static String MQ_URI = "mq.api.uri";
	public final static String MQ_APP_USER_NAME = "mq.api.user.name";
	public final static String MQ_APP_PASSWORD = "mq.api.password";
	public final static String FILTER_EDITABLE_SALES = "filter.sales.editable.status";
	public final static String FILTER_EDITABLE_NI = "filter.ni.editable.status";
	public final static String MEMCACHED_URL = "memcached.url";
	public final static String MEMCACHED_PORT = "memcached.port";
	
	public final static String CRM_BASE_QUERY = "crm.base.query";
	public final static String CRM_HYD_VIEW_NAME = "crm.hyd.view.name";
	public final static String CRM_ROI_VIEW_NAME = "crm.roi.view.name";
	public final static String MQ_ATTRIBUTE_TWO_VALUES = "mq.attribute.two.values";
	public final static String MQ_ATTRIBUTE_THREE_VALUES = "mq.attribute.three.values";
	public final static String MQ_ATTRIBUTE_FOUR_VALUES = "mq.attribute.four.values";
	public final static String MQ_ATTRIBUTE_ONE_VALUES = "mq.attribute.one.values";
	public final static String MQ_ATTRIBUTE_WO_CLOSURE_VALUE_ROI = "mq.attribute.wo.closure.value.roi";
	public final static String MQ_ATTRIBUTE_WO_CLOSURE_VALUE_HYD = "mq.attribute.wo.closure.value.hyd";
	public final static String MQ_ATTRIBUTE_CUSTOMER_TYPE_VALUE_HYD = "mq.attribute.customer.type.value.hyd";
	public final static String MQ_ATTRIBUTE_CUSTOMER_TYPE_VALUE_ROI = "mq.attribute.customer.type.value.roi";
	
	public final static String FWMP_DEVICE_CACHE_THREAD_POOL_SIZE = "fwmp.device.cache.thread.pool.size";
	public final static String FWMP_CREATE_PROSPECT_BY_CC_THREAD_POOL_SIZE = "fwmp.create.prospect.by.cc.thread.pool.size";
	public final static String FWMP_TICKET_UPDATE_GATEWAY_THREAD_POOL_SIZE = "fwmp.ticket.update.gateway.thread.pool.size";
	public final static String FWMP_TICKET_UPDATE_STATUS_HANDLER_THREAD_POOL_SIZE = "fwmp.ticket.update.status.handler.thread.pool.size";
	public final static String FWMP_STARTUP_SALES_QUEUE_PREPARATION_THREAD_POOL_SIZE = "fwmp.startup.sales.queue.preparation.thread.pool.size";
	public final static String FWMP_STARTUP_DEPLOYMENT_QUEUE_PREPARATION_THREAD_POOL_SIZE = "fwmp.startup.deployment.queue.preparation.thread.pool.size";
	public final static String FWMP_AREA_CACHE_THREAD_POOL_SIZE = "fwmp.area.cache.thread.pool.size";
	public final static String FWMP_USER_CACHE_THREAD_POOL_SIZE = "fwmp.user.cache.thread.pool.size";
	
	public final static String FWMP_EXTERNAL_CREATE_PROSPECT_THREAD_POOL_SIZE = "fwmp.external.create.prospect.thread.pool.size";
	public final static String FWMP_EXTERNAL_UPDATE_PROSPECT_THREAD_POOL_SIZE = "fwmp.external.update.prospect.thread.pool.size";
	public final static String FWMP_EXTERNAL_CREATE_WORK_ORDER_THREAD_POOL_SIZE = "fwmp.external.create.work.order.thread.pool.size";
	public final static String FWMP_EXTERNAL_UPDATE_WORK_ORDER_THREAD_POOL_SIZE = "fwmp.external.update.work.order.thread.pool.size";
	public final static String FWMP_EXTERNAL_CREATE_FR_THREAD_POOL_SIZE = "fwmp.external.create.fr.thread.pool.size";
	public final static String FWMP_EXTERNAL_UPDATE_FR_THREAD_POOL_SIZE = "fwmp.external.update.fr.thread.pool.size"; 
	 
	public static final String PING_DEVICE_API = "fr.device.ping.status.url";
	public static final String PORT_STATUS_API = "fr.device.port.status.url";
	public static final String FIBER_POWER_API = "fr.device.fiber.power.status.url" ;
	
	public final String CX_PORT_AVAILABILITY = "fr.cx.port.availability.status.url";
	public final String FX_PORT_AVAILABILITY = "fr.fx.port.availability.status.url";
	public final String FILTER_FR_UNASSIGNED_STATUS = "filter.unassigned.fr.ticket.search.option";
	public final String ETR_DEFAULT_HOURS = "afp.frticket.default.etr.hours";
	
	public static final String SPLICING_PORT_WISE_CHECK = "fr.splicing.port.wise.status.url";
	public static final String SPLICING_FX_CHECK = "fr.splicing.fx.wise.status.url";
	
	public static final String HYD_MQ_API_KEY = "fr.mq.hyd.close.ticket.url";
	public static final String ROI_MQ_API_KEY = "fr.mq.roi.close.ticket.url";
	
	public static final String MQ_DEFAULT_CLOSURE_CODE = "mq.default.resolution.code";
	public final static String FILTER_EDITABLE_FRUSER = "filter.status.fr.not.editable.status";
	
	public static final  String MQ_RESO_CODE_DEFAULT_ENABLED = "mq.default.resolution.code.enable";
	public static final  String MQ_AUTO_CLOSURE_ENABLED = "afp.frticket.auto.closure.enable";
	
	public static final  String FR_TICKET_CATEGORY_ROI = "afp.roi.fr.ticket.category";
	public static final  String FR_TICKET_CATEGORY_HYD = "afp.hyd.fr.ticket.category";

	public final static String SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON = "substract.minutes.to.current.time.for.regular.cron";
	public final static String SUBSTRACT_DAYS_TO_CURRENT_TIME = "substract.days.to.current.time";
	public final static	String SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_SPECIAL_CRON = "substract.minutes.to.current.time.for.special.cron";
	
	public final static	String FROM_EMAIL_ID = "from";
	
	public final static String SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON_FR = "substract.minutes.for.fr.from.regular.cron.time";
	public final static String SUBSTRACT_DAYS_TO_CURRENT_TIME_FR = "substract.days.for.fr.from.current.time";
	public final static	String SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_SPECIAL_CRON_FR = "substract.minutes.for.fr.specialorbackup.cron";
	
	public static final String APK_NAME = "fwmp.recent.apk.file.name";
	
	String DESTINATION_KEY = "fwmp.doc.initial.path";

	String SOURCE_KEY = "fwmp.ecaf.input.file";

	String SUBJECT_KEY = "ecaf.subject";

	String BODY_KEY = "ecaf.body";
	
	String TICK_KEY = "fwmp.ecaf.tick.img";
	
	public final static	String CURRENT_APPLICATION_CRM_NAME = "current.application.crm.name";
	

	void reloadDataFromProperties();

	String getDataFromProperties(String statusKey);

	Set<Integer> getStatusFilterForRole(String roleKey);

	Set<Integer> getStatusFilterForCurrentUser(boolean includeCompleted,
			Integer pageDefinition);

	APIResponse getInquiry();

	APIResponse getMediaSource(Long city);

	APIResponse getEtrReasonCode();

	APIResponse getRoleStatusDifinition();

	Set<Integer> getTicketStatusDef(String key);

	String getNotificationMessageFromProperties(String statusCode);

	APIResponse readReassignmentReasonCode();

	APIResponse readPaymentreasonCode();

	APIResponse readDocumentReasonCode();

	APIResponse getHotProspectReasoncode();

	APIResponse getReasonCode(Long groupId, Long statusId);

	APIResponse getCustomerType();

	APIResponse readResolveStatusCode();

	String getPropertiesForCustomerScheduling(String propertyName);

	Set<Long> getActivityStatusDef(String key);

	String getMessagesForCustomerNotificationFromProperties(
			String statusMessageCode);

	Boolean getIsTestBuild();

	boolean isDocumetStatus(String status);

	boolean isPaymentStatus(String status);

	Map<Long, PriorityValue> getPriorityDefinitions();

	PriorityValue getPriorityDefinitionsFor(Long prorityKey);

	Map<String, Set<String>> populateClassificationCategoryMap();

	Set<String> getClassificationCategory(String key);

	String getMqErrors(String key);

	APIResponse readfeasibilityReasonCode();

	Set<String> getEmailsBasedOnTypeAndCity(String key);

	APIResponse readVersionDetails();

	APIResponse getAllfrNatureCodes();

	String findFrNatureCodeBySymptomName(String symptomName);

	String getEnqueryById(Long enqueryId);

	String getSourceById(Long sourceId, Long cityCode);

	APIResponse getColdProspectReasoncode();

	APIResponse getMediumProspectReasoncode();

	APIResponse getSalesCustomerDeniedReasons();

	APIResponse getDeploymentCustomerDeniedReasons();

	APIResponse getCopper2FiberReasons();

	APIResponse getFeasibilityRejectedByNeReasons();

	APIResponse getFiberTypes();

	APIResponse getAllFrSymptomNameByFlowId(Long flowId);

	APIResponse getFrClosureRemarks();

	Set<String> getClassificationProperty(String key);

	String getPropertyValue(String key);

	APIResponse getElementDenialReasonCodeBlr();

	APIResponse getElementDenialReasonCodeHyd();

	String getMqErrorMessageById(Long errorId);

	APIResponse getMqErrorMessage();

	String getIntegrationFilePropertiesByKey(String key);
	
	APIResponse getProfession();

	String getProfessionByKey(long key);

	APIResponse getTitle();

	APIResponse getProspectTypes();

	String getNIHelpLineNumber(String cityCode);
	
	String getClassificationQuery(String queryKey);

	APIResponse getSalesDropdown();
	String getPortalPageUrl(String cityCode);

	Set<Integer> getEditableStatusFilterForCurrentUser();

	APIResponse getCustomerRemarks();

	String getCustomerRemarksByKey(String key);
	Map<String, String> getDefectSubDefectCode();

	Map<String, List<String>> getDefectSubDefectCodeDefinition();

	String getFrStatusStringByStatusId(Long statusId);

	Set<Integer> getFrCurrentUserStatusFilterForSearch();

	List<TypeAheadVo> getFrBushBackReasonCode();
	
	public void refreshDefecSubDefectMQClosureCode();

	public String getFrMqClosurCode(String defectCode, String subDefect);

	public String getActForceUrl(String key);

	APIResponse getAllStates();

	String getFrMqClosurCodeHyd(String defectCode, String subDefect);

	void reloadClassificationAndIntegration();

	public Set<Integer> getUnassignedStatusForSearch(Integer filter);
	
	APIResponse getNationality();
	
	APIResponse getExistingISP();
	
	APIResponse getUinType();
	
	
	String getMergingQueueDefByKey(String key);
	/**@author aditya
	 * String
	 * @param queryKey
	 * @return
	 */
	String getThreadsPoolsInfo(String queryKey);
	
	/**@author aditya
	 * String
	 * @param key
	 * @return
	 */
	String getMailConfig(String key);
	
	interface EmailProperties
	{
		String FROM = "from";
		String PASSWORD = "password";
		String HOST = "host";
		String PORT = "port";
		String MAIL_SMTP_HOST = "mail.smtp.host";
		String MAIL_SMTP_AUTH = "mail.smtp.auth";
		String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
		String MAIL_SMTP_SSL_TRUST = "mail.smtp.ssl.trust";
	}

	/**@author aditya
	 * String
	 * @param key
	 * @return
	 */
	String getDeploymentPropertyValue(String key);

	/**@author aditya
	 * boolean
	 * @param values
	 * @return
	 */
	boolean getProfessionByValues(String values);

	/**@author aditya
	 * boolean
	 * @param values
	 * @return
	 */
	boolean getTitlesByValues(String values);

	/**@author aditya
	 * boolean
	 * @param values
	 * @return
	 */
	boolean getProspectTypeByValues(String values);

	/**@author aditya
	 * boolean
	 * @param values
	 * @return
	 */
	boolean getHowDiducome2knowAbtACTByValues(String values);

	/**@author aditya
	 * boolean
	 * @param values
	 * @return
	 */
	boolean getInqueryTypeByValues(String values);

	/**@author aditya
	 * Long
	 * @param values
	 * @return
	 */
	Long getEscalationTypeByValues(String values);

	/**@author aditya
	 * String
	 * @param key
	 * @return
	 */
	String getEscalationTypeByKey(Long key);

}
