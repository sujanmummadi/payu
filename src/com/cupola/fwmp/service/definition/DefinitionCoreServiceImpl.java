package com.cupola.fwmp.service.definition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant;
import com.cupola.fwmp.FRConstant.DefectSubDefectCodeMapper;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.PriorityFilter;
import com.cupola.fwmp.dao.router.RouterDAO;
import com.cupola.fwmp.dao.tariff.TariffDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.location.branch.BranchCoreService;
import com.cupola.fwmp.service.materials.MaterialCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonPropertiesReader;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.EtrUpdateReasonCode;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.NodeValueVo;
import com.cupola.fwmp.vo.ReasonCodeVo;
import com.cupola.fwmp.vo.SalesDropdownVo;
import com.cupola.fwmp.vo.TypeAheadVo;

public class DefinitionCoreServiceImpl implements DefinitionCoreService
{
	private static final Logger LOGGER = LogManager
			.getLogger(DefinitionCoreServiceImpl.class.getName());

	// private long cityId;
	//
	// public void setCityId(long cityId)
	// {
	// this.cityId = cityId;
	// }

	public static Map<String, String> statusDef;
	public static Map<String, String> notificationDef;
	public static Map<String, String> notificationMessageDef;
	public static Map<String, String> customerNotificationMessageDef;
	public static Map<String, String> helpLineNUmbers;
	public static Map<String, String> customerScheduleDef;
	private static Map<String, Set<Integer>> filterDefnition = new HashMap<String, Set<Integer>>();
	private static Map<String, Set<Integer>> ticketStatusDefnition = new HashMap<String, Set<Integer>>();
	private static Map<String, Set<Long>> activityStatusDefnition = new HashMap<String, Set<Long>>();
	private static Map<Long, String> roiMediaSource;
	private static Map<Long, String> hydMediaSource;
	private static Map<Long, String> readInquiry;
	private static Map<Long, String> escalationType;
	private static Map<Long, String> reassignReasoncode = new LinkedHashMap<Long, String>();;
	private static Map<Long, String> reassignRejectReasonCode;
	private static Map<Long, String> reassignAcceptReasonCode;
	private static Map<Long, String> documentReasonCode = new LinkedHashMap<Long, String>();;
	private static Map<Long, String> documentRejectReasonCode;
	private static Map<Long, String> documentAcceptReasonCode;
	private static Map<Long, String> paymentAcceptReasonCode;
	private static Map<Long, String> paymentRejectReasonCode;
	private static Map<Long, String> paymentRefundReasonCode;

	private static Map<Long, String> paymentresoncode = new LinkedHashMap<Long, String>();
	public static Map<Long, String> hotProspectReasoncode;
	public static Map<Long, String> coldProspectReasoncode;
	public static Map<Long, String> mediumProspectReasoncode;
	private static Map<Long, String> salesCustomerDeniedReasons;
	private static Map<Long, String> deploymentCustomerDeniedReasons;
	private static Map<Long, String> copper2fiber;
	private static Map<Long, String> feasibilityRejectedByNeReasons;
	public static Map<Long, String> fiberTypes;
	public static Map<String, String> etrReasonCode;
	private static Map<Long, String> customerType;
	private static Map<Long, String> resolveStatusCode;
	private Boolean isTestBuild;
	private static Map<String, Set<String>> classificationCategoryMap = new HashMap<String, Set<String>>();
	public static Map<String, String> classificationCategoryDef;
	public static Map<String, String> mqErrors;
	private static Map<Long, String> feasibilityReasonCode;
	public static Map<String, String> emailCorporateDef;
	private static Map<String, Set<String>> emailIDsDefnition = new HashMap<String, Set<String>>();
	private static Map<String, String> versionDef;
	private static Map<String, String> integrationProperties;
	private static Map<String, String> frNatureCodes;
	private static Map<String, String> frFlowNameAndFlowIdMap;
	private static Map<Long, String> frClosuerRemarks;
	public static Map<Long, String> workProgressCode;

	private static Map<Long, String> elementDenialCopperReasonCodeBlr;
	private static Map<Long, String> elementDenialFiberReasonCodeBlr;
	private static Map<Long, String> elementDenialCopperReasonCodeHyd;
	private static Map<Long, String> elementDenialFiberReasonCodeHyd;
	private static Map<Long, String> elementDenialNonFeasibleReasonCodeHyd;
	private static Map<Long, String> elementDenialNonFeasibleReasonCodeBlr;
	private static Map<Long, String> elementPermissionDenialReasonCodeBlr;
	private static Map<Long, String> elementPermissionDenialReasonCodeHyd;
	private static Map<Long, String> elementRacFixingDenialReasonsBlr;
	private static Map<Long, String> elementRacFixingDenialReasonsHyd;

	private static Map<Long, String> mqErrorMessage;

	public static Map<String, String> defectAndSubdefectCode;
	public static Map<String, String> frMqClosurWithDefectSubCode;
	public static Map<String, String> frMqClosurWithDefectSubCodeHyd;
	public static Map<String, String> defectCodeNameValue;
	public static Map<String, String> frMqClosure = new LinkedHashMap<String, String>();
	public static Map<String, String> frMqClosureHyd = new LinkedHashMap<String, String>();
 
	private static Map<Long, String> professions;
	private static Map<Long, String> titles;
	private static Map<Long, String> prospectTypes;

	private static Map<String, String> classificationQuery;
	private static Map<String, String> threadPoolInfo;
	private static Map<Long, String> paymentMode;
	
	public static Map<String, String> frDefaultETR;
	private static Map<String, String> frStatusMap;
	public static Map<String, String> ftpClientProperties;

	private static Map<String, String> customerRemarks = new LinkedHashMap<String, String>();
	static	Map<String, List<String>> defectSubDefectCodeDefinition = new HashMap<String, List<String>>();
	static	Map<String,Integer> defectCodeDefinition = new HashMap<String,Integer>();
	static	Map<String,Integer> subDefectCodeDefinition = new HashMap<String,Integer>();

	
	private static Map<Long, String> allStates;
	
	private static Map<Long, String> frPushBackReasonCode;
	private static Map<String, String> integrationKeyAndValue;
	public  static Map<String, String> bufferTime;
	public  static Map<String, String> workingHoursDef;
	
	private  static Map<String, String> mergingQueueDef;
	
	public static Map<String, String> frCityWiseDefaultETR;
	public static Map<String, String> mailConfig;
	public static Map<String, String> deploymentProperties;
	public static Map<String, String> emailConfig;
	
	public static Map<Long,String> nationality;
	
	public static Map<Long,String> existingIsp;
	
	public static Map<Long,String> uinType;
	
	@SuppressWarnings("unused")
	private void init()
	{
		LOGGER.info("DefinitionCoreServiceImpl init method called..");
		reloadDataFromProperties();
		LOGGER.info("DefinitionCoreServiceImpl init method executed.");
	}

	@Autowired
	CommonPropertiesReader commonPropertiesReader;

	@Autowired
	TariffDAO tariffDAO;

	@Autowired
	RouterDAO routerDAO;
	@Autowired
	BranchCoreService branchCoreService;

	@Override
	public void reloadDataFromProperties()
	{
		new Thread()
		{
			@Override
			public void run()
			{
				MaterialCache.resetCache();
				filterDefnition.clear();
				priorityDefinitions.clear();
				//frDefaultETR.clear();
				
			 defectSubDefectCodeDefinition.clear();
			  defectCodeDefinition.clear();
				 subDefectCodeDefinition.clear();
				
				statusDef = commonPropertiesReader.readStatusDefProperties();
				notificationDef = commonPropertiesReader
						.readCustomerNotificationStatusDefProperties();
				notificationMessageDef = commonPropertiesReader
						.readNotificationMessageDefProperties();
				customerNotificationMessageDef = commonPropertiesReader
						.readCustomerNotificationMessageDefProperties();

				helpLineNUmbers = commonPropertiesReader
						.readhelpLineNUmbersMessageDefProperties();
				customerScheduleDef = commonPropertiesReader
						.readSchedularProperties();
				hydMediaSource = commonPropertiesReader.readHydMediaSource();
				roiMediaSource = commonPropertiesReader.readRoiMediaSource();
				readInquiry = commonPropertiesReader.readInquiry();
				escalationType = commonPropertiesReader.readEscalationType();
				reassignAcceptReasonCode = commonPropertiesReader
						.readReassignAcceptReasoncode();

				reassignRejectReasonCode = commonPropertiesReader
						.readReassignRejectReasoncode();
				readReassignmentReasonCode();

				documentAcceptReasonCode = commonPropertiesReader
						.readDocumentAcceptReasonCode();

				documentRejectReasonCode = commonPropertiesReader
						.readDocumentRejectReasonCode();

				paymentAcceptReasonCode = commonPropertiesReader
						.readPaymentAcceptResoncode();

				paymentRejectReasonCode = commonPropertiesReader
						.readPaymentRejectResoncode();

				paymentRefundReasonCode = commonPropertiesReader
						.readPaymentRefundResoncode();

				hotProspectReasoncode = commonPropertiesReader
						.readHotProspectReasoncode();
				coldProspectReasoncode = commonPropertiesReader
						.readColdProspectReasoncode();
				mediumProspectReasoncode = commonPropertiesReader
						.readMediumProspectReasoncode();
				etrReasonCode = commonPropertiesReader.readMessageProperties();
				customerType = commonPropertiesReader
						.readCustomerTypeProperties();
				resolveStatusCode = commonPropertiesReader
						.readResolveStatusCode();

				classificationCategoryDef = commonPropertiesReader
						.readClassificationCategoryDefProperties();
				
				emailConfig = commonPropertiesReader
						.readMailConfig();
				
				deploymentProperties = commonPropertiesReader.readDeploymentProperties();

				mqErrors = commonPropertiesReader.readMqErrors();
				deploymentProperties = commonPropertiesReader.readDeploymentProperties();

				classificationCategoryMap = populateClassificationCategoryMap();
				feasibilityReasonCode = commonPropertiesReader
						.readfeasibilityReasonCode();
				emailIDsDefnition.clear();
				versionDef = commonPropertiesReader.readVersionDetails();
				emailCorporateDef = commonPropertiesReader
						.readEmailCorporateConfigurationProperties();

				frNatureCodes = commonPropertiesReader.readFrNatureCodes();
				frFlowNameAndFlowIdMap = commonPropertiesReader
						.readFrSymptomNames();
				frClosuerRemarks = commonPropertiesReader
						.readFrClosureRemarks();

				workProgressCode = commonPropertiesReader
						.readWorkProgressCode();
				feasibilityRejectedByNeReasons = commonPropertiesReader
						.readFeasibilityRejectedByNeReasons();

				fiberTypes = commonPropertiesReader.readFiberTypes();

				copper2fiber = commonPropertiesReader.readCopper2fiber();

				deploymentCustomerDeniedReasons = commonPropertiesReader
						.readDeploymentCustomerDeniedReasons();

				salesCustomerDeniedReasons = commonPropertiesReader
						.readSalesCustomerDeniedReasons();

				elementDenialCopperReasonCodeBlr = commonPropertiesReader
						.readElementCopperDenialReasonCodeBlr();

				elementDenialFiberReasonCodeBlr = commonPropertiesReader
						.readElementFiberDenialReasonCodeBlr();

				elementDenialCopperReasonCodeHyd = commonPropertiesReader
						.readElementCopperDenialReasonCodeHyd();

				elementDenialFiberReasonCodeHyd = commonPropertiesReader
						.readElementFiberDenialReasonCodeHyd();

				elementDenialNonFeasibleReasonCodeHyd = commonPropertiesReader
						.readElementCopperDenialNonFeasibleReasonCodeHyd();

				elementDenialNonFeasibleReasonCodeBlr = commonPropertiesReader
						.readElementCopperDenialNonFeasibleReasonCodeBlr();

				elementPermissionDenialReasonCodeBlr = commonPropertiesReader
						.readElementPermissionReasonCodeBlr();

				elementPermissionDenialReasonCodeHyd = commonPropertiesReader
						.readElementPermissionReasonCodeHyd();

				elementRacFixingDenialReasonsBlr = commonPropertiesReader
						.readCxDenialReasonCodeBlr();

				elementRacFixingDenialReasonsHyd = commonPropertiesReader
						.readCxDenialReasonCodeHyd();

				mqErrorMessage = commonPropertiesReader.readMqErrorMessage();

				integrationProperties = commonPropertiesReader
						.readIntegrationProperties();

				professions = commonPropertiesReader.readProfessions();

				titles = commonPropertiesReader.readTitle();

				
				defectAndSubdefectCode = commonPropertiesReader.readDefectAndSubDefectCode();
				defectCodeNameValue = commonPropertiesReader.readeDefectCodeNameAndValue();
				frMqClosurWithDefectSubCode = commonPropertiesReader.readFrMqClosureCodeWithDefectSubDefectCode();
				frMqClosurWithDefectSubCodeHyd = commonPropertiesReader
						.readFrMqClosureCodeHydWithDefectSubDefectCode();

				prospectTypes = commonPropertiesReader.readProspectTypes();

				classificationQuery = commonPropertiesReader
						.readClassificationQueryProperties();

				threadPoolInfo = commonPropertiesReader
						.readClassificationQueryProperties();

				paymentMode = commonPropertiesReader.readPaymentMode();
				
				frDefaultETR = commonPropertiesReader.readFRDefaultETR();
				frStatusMap = commonPropertiesReader.getFrStatusMap();
				
				frPushBackReasonCode = commonPropertiesReader.getFrPushBackCount();
				
				integrationKeyAndValue = commonPropertiesReader.getIntegrationMap();

					ftpClientProperties = commonPropertiesReader
						.readFTPProperties();

				allStates = commonPropertiesReader.allStates();
				
				bufferTime = commonPropertiesReader.readBufferTimeProperties();
				
				workingHoursDef  = commonPropertiesReader.readWorkingHoursProperties();
				mergingQueueDef = commonPropertiesReader.readMergingQueueProperties();
				frCityWiseDefaultETR = commonPropertiesReader.readFRCityWiseDefaultETR();
				mailConfig = commonPropertiesReader.readMailConfig();

				ftpClientProperties = commonPropertiesReader
						.readFTPProperties();

				allStates = commonPropertiesReader.allStates();
				
				nationality=commonPropertiesReader.getnationality();
				
				existingIsp=commonPropertiesReader.getExistingISP();
				
				uinType=commonPropertiesReader.getUinType();
				

				populateCustomerRemeaks();
				getDefectSubDefectCodeDefinition();
				
				populateDefectCodeMap();
				populateMqClosureMap();
				populateMqClosureMapHyd();
				
				populateFrTicketCategory();
			}
		}.start();
		;

	}

	@Override
	public Set<Integer> getStatusFilterForRole(String roleKey)
	{
		if (filterDefnition.get(roleKey) != null)
			return filterDefnition.get(roleKey);
		Set<Integer> list = new HashSet<Integer>();
		String values = statusDef.get(roleKey);
		if (values == null)
			return list;

		List<String> allValue = Arrays.asList(values.split(","));
		for (String s : allValue)
		{
			list.add(Integer.valueOf(s));
		}
		filterDefnition.put(roleKey, list);

		return filterDefnition.get(roleKey);

	}

	Map<Long, PriorityValue> priorityDefinitions = new HashMap<Long, PriorityValue>();

	@Override
	public Map<Long, PriorityValue> getPriorityDefinitions()
	{

		if (priorityDefinitions.size() == 0)
		{
			List<Integer> values = (getProirityValues(FILTER_VERY_HIGH));
			if (values.size() == 2)
				priorityDefinitions
						.put(PriorityFilter.VERY_HIGH, new PriorityValue(values
								.get(0), values.get(1)));

			values = getProirityValues(FILTER_HIGH);
			if (values.size() == 2)
				priorityDefinitions
						.put(PriorityFilter.HIGH, new PriorityValue(values
								.get(0), values.get(1)));
			values = getProirityValues(FILTER_MEDIUM);
			if (values.size() == 2)
				priorityDefinitions
						.put(PriorityFilter.MEDIUM, new PriorityValue(values
								.get(0), values.get(1)));

			values = getProirityValues(FILTER_LOW);
			if (values.size() == 2)
				priorityDefinitions
						.put(PriorityFilter.LOW, new PriorityValue(values
								.get(0), values.get(1)));
		}
		return priorityDefinitions;

	}

	@Override
	public PriorityValue getPriorityDefinitionsFor(Long prorityKey)
	{
		if (priorityDefinitions.size() == 0)
			getPriorityDefinitions();

		return priorityDefinitions.get(prorityKey);

	}

	private List<Integer> getProirityValues(String statusKey)
	{
		List<Integer> list = new ArrayList<Integer>();
		String values = getDataFromProperties(statusKey);
		List<String> allValue = Arrays.asList(values.split(","));
		for (String s : allValue)
		{
			list.add(Integer.valueOf(s));
		}
		return list;

	}

	@Override
	public Set<Integer> getTicketStatusDef(String key)
	{

		Set<Integer> list = new HashSet<Integer>();
		String values = notificationDef.get(key);
		if (values == null)
			return list;

		List<String> allValue = Arrays.asList(values.split(","));
		for (String s : allValue)
		{
			list.add(Integer.valueOf(s));
		}
		ticketStatusDefnition.put(key, list);

		return ticketStatusDefnition.get(key);

	}

	@Override
	public Set<Long> getActivityStatusDef(String key)
	{

		Set<Long> list = new HashSet<Long>();
		String values = notificationDef.get(key);
		if (values == null)
			return list;

		List<String> allValue = Arrays.asList(values.split(","));
		for (String s : allValue)
		{
			list.add(Long.valueOf(s));
		}
		activityStatusDefnition.put(key, list);

		return activityStatusDefnition.get(key);

	}

	@Override
	public Set<Integer> getEditableStatusFilterForCurrentUser()
	{
		if (AuthUtils.isAdmin())
		{
			Set<Integer> allFilters = new HashSet<Integer>();

			for (Set<Integer> value : filterDefnition.values())
			{
				allFilters.addAll(value);
			}
			return allFilters;

		} 
		else if (AuthUtils.isSalesUser())
		{
			return getStatusFilterForRole(DefinitionCoreService.FILTER_EDITABLE_SALES);

		} 
		else if (AuthUtils.isNIUser())
		{
			return getStatusFilterForRole(DefinitionCoreService.FILTER_EDITABLE_NI);
		} 
		else if( AuthUtils.isFRUser())
		{
			return getStatusFilterForRole(DefinitionCoreService.FILTER_EDITABLE_FRUSER);
		}
		else
			return new HashSet<Integer>();
	}

	@Override
	public Set<Integer> getStatusFilterForCurrentUser(boolean includeCompleted,
			Integer pageDefinition)
	{
		Set<Integer> codeDefinition = null;
		if (AuthUtils.isAdmin())
		{
			Set<Integer> allFilters = new HashSet<Integer>();

			for (Set<Integer> value : filterDefnition.values())
			{
				allFilters.addAll(value);
			}
			return allFilters;
		} else if (AuthUtils.isFrManager())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_FR_TL);
		}

		else if (AuthUtils.isCCNRUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_CCNR);
		} else if (AuthUtils.isChurnTeamUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_CHURN);
		} else if (AuthUtils.isFinanceTeamUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_FINANCE);
		} else if (AuthUtils.isLegalTeamUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_LEGAL);
		} else if (AuthUtils.isNOCTeamUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_NOC);
		}

		else if (AuthUtils.isSalesManager())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.SALES_MANAGER_STATUS_FILTER);
		} else if (AuthUtils.isNIManager())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.NI_MANAGER_STATUS_FILTER);
		} else if (AuthUtils.isCCUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.CC_STATUS_FILTER);
		} else if (AuthUtils.isCFEUser())
		{
			if (pageDefinition == null || pageDefinition <= 0)
				codeDefinition = getStatusFilterForRole(DefinitionCoreService.CFE_STATUS_FILTER);
			else if (DOCUMENT_RELATED_STATUS.equals(pageDefinition))
				codeDefinition = getStatusFilterForRole(DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER);
			else if (PAYMENT_RELATED_STATUS.equals(pageDefinition))
				codeDefinition = getStatusFilterForRole(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER);
			else if (REFUND_RELATED_STATUS.equals(pageDefinition))
				codeDefinition = getStatusFilterForRole(DefinitionCoreService.CFE_AT_REFUND_STATUS_FILTER);

		} else if (AuthUtils.isNITLUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.NI_TL_STATUS_FILTER);
		} else if (AuthUtils.isSalesTlUser())
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.SALES_TL_STATUS_FILTER);
		} else
		{
			codeDefinition = new HashSet<Integer>();
		}

		// if (includeCompleted)
		// codeDefinition
		// .addAll(getStatusFilterForRole(DefinitionCoreService.COMPLETED_STATUS_FILTER));
		return codeDefinition;
	}

	@Override
	public Set<Integer> getFrCurrentUserStatusFilterForSearch()
	{
		Set<Integer> codeDefinition = null;
		
		if ( AuthUtils.isFRUser() )
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_FR_STATUS);
		}
		else
		{
			codeDefinition = new HashSet<Integer>();
		}
		return codeDefinition;
	}
	
	@Override
	public Set<Integer> getUnassignedStatusForSearch( Integer filter)
	{
		Set<Integer> codeDefinition = null;
		
		if ( AuthUtils.isFRUser() )
		{
			codeDefinition = getStatusFilterForRole(DefinitionCoreService.FILTER_FR_UNASSIGNED_STATUS);
		}
		else
		{
			codeDefinition = new HashSet<Integer>();
		}
		return codeDefinition;
	}

	@Override
	public String getDataFromProperties(String statusKey)
	{
		return statusDef.get(statusKey);
	}

	@Override
	public String getNotificationMessageFromProperties(String statusCode)
	{
		return notificationMessageDef.get(statusCode);
	}

	@Override
	public String getMessagesForCustomerNotificationFromProperties(
			String statusMessageCode)
	{
		return customerNotificationMessageDef.get(statusMessageCode);
	}

	@Override
	public String getNIHelpLineNumber(String cityCode)
	{
		if (cityCode == null)
			return "NA";
		cityCode = cityCode.toLowerCase();
		if (!helpLineNUmbers.containsKey("ni." + cityCode))
			return "NA";
		return helpLineNUmbers.get("ni." + cityCode);
	}

	@Override
	public String getPortalPageUrl(String cityCode)
	{
		if (cityCode == null)
			return "NA";
		cityCode = cityCode.toLowerCase();
		if (!helpLineNUmbers.containsKey("portal." + cityCode))
			return "NA";
		return helpLineNUmbers.get("portal." + cityCode);
	}

	@Override
	public APIResponse getRoleStatusDifinition()
	{
		return ResponseUtil.createSuccessResponse().setData((statusDef));
	}

	@Override
	public APIResponse getMediaSource(Long cityId)
	{
		if (cityId == null)
		{
			/*
			 * return ResponseUtil.createSuccessResponse().setData(CommonUtil
			 * .xformToTypeAheadFormat(hydMediaSource)
			 * .addAll(CommonUtil.xformToTypeAheadFormat(roiMediaSource)));
			 */

			List<TypeAheadVo> hyd = CommonUtil
					.xformToTypeAheadFormat(hydMediaSource);
			hyd.addAll(CommonUtil.xformToTypeAheadFormat(roiMediaSource));
			return ResponseUtil.createSuccessResponse().setData(hyd);

		}

		LOGGER.info("found cityId:" + cityId);

		if (cityId != null)
		{
			String cityCode = LocationCache.getCityCodeById(cityId);

			if (cityCode != null && cityCode.equalsIgnoreCase(CityName.HYDERABAD_CITY_CODE))
			{

				return ResponseUtil.createSuccessResponse().setData(CommonUtil
						.xformToTypeAheadFormat(hydMediaSource));
			}
		}
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(roiMediaSource));

	}

	@Override
	public APIResponse getInquiry()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(readInquiry));

	}

	@Override
	public String getEnqueryById(Long enqueryId)
	{
		if (readInquiry.containsKey(enqueryId))
			return readInquiry.get(enqueryId);
		else
			return null;

	}

	@Override
	public String getSourceById(Long sourceId, Long cityId)
	{
		String cityCode = LocationCache.getCityCodeById(cityId);
		if (cityCode !=null && cityCode.equalsIgnoreCase(CityName.HYDERABAD_CITY_CODE))
		{
			if (hydMediaSource.containsKey(sourceId))
				return hydMediaSource.get(sourceId);
			else
				return null;
		} else
		{
			if (roiMediaSource.containsKey(sourceId))
				return roiMediaSource.get(sourceId);
			else
				return null;
		}
	}

	@Override
	public APIResponse getCustomerType()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(customerType));
	}

	@Override
	public APIResponse getEtrReasonCode()
	{

		EtrUpdateReasonCode etrUpdateReasonCode = new EtrUpdateReasonCode();
		etrUpdateReasonCode.setEtrReasonCode(etrReasonCode);
		return ResponseUtil.createSuccessResponse()
				.setData(etrUpdateReasonCode);
	}

	@Override
	public APIResponse readReassignmentReasonCode()
	{
		if (reassignReasoncode != null)
		{
			if (reassignReasoncode.size() == reassignAcceptReasonCode.size()
					+ reassignRejectReasonCode.size())
				return ResponseUtil.createSuccessResponse()
						.setData(reassignReasoncode);
			reassignReasoncode.clear();
			reassignReasoncode.putAll(reassignAcceptReasonCode);
			reassignReasoncode.putAll(reassignRejectReasonCode);

			return ResponseUtil.createSuccessResponse().setData(reassignReasoncode);
		}
		return ResponseUtil.createFailureResponse();
			

	}

	@Override
	public APIResponse readPaymentreasonCode()
	{
		if (paymentresoncode != null)
			if (paymentresoncode.size() == paymentAcceptReasonCode.size()
					+ paymentRejectReasonCode.size()
					+ paymentRefundReasonCode.size())
				return ResponseUtil.createSuccessResponse()
						.setData(paymentresoncode);
			paymentresoncode.clear();
			paymentresoncode.putAll(paymentAcceptReasonCode);
			paymentresoncode.putAll(paymentRejectReasonCode);
			paymentresoncode.putAll(paymentRefundReasonCode);
			return ResponseUtil.createSuccessResponse().setData(paymentresoncode);
		}


	@Override
	public APIResponse readDocumentReasonCode()
	{
		if (documentReasonCode != null)
			if (documentReasonCode.size() == documentAcceptReasonCode.size() + documentRejectReasonCode.size())
				return ResponseUtil.createSuccessResponse().setData(documentReasonCode);
			
			documentReasonCode.clear();
			documentReasonCode.putAll(documentAcceptReasonCode);
			documentReasonCode.putAll(documentRejectReasonCode);
			return ResponseUtil.createSuccessResponse().setData(documentReasonCode);
	}
		
	@Override
	public APIResponse getHotProspectReasoncode()
	{
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(hotProspectReasoncode));
	}

	@Override
	public APIResponse getColdProspectReasoncode()
	{
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(coldProspectReasoncode));
	}

	@Override
	public APIResponse getMediumProspectReasoncode()
	{
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(mediumProspectReasoncode));
	}

	@Override
	public APIResponse readResolveStatusCode()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(resolveStatusCode));
	}

	@Override
	public APIResponse getReasonCode(Long groupId, Long statusId)
	{
		LOGGER.info("found groupId::" + groupId + "and statusId::" + statusId);
		Map<Long, String> reasonCodeDif = new ConcurrentHashMap<Long, String>();

		if (groupId == DefinitionCoreService.DOCUMENT_REASON_GROUP)
		{
			reasonCodeDif.clear();

			if (statusId == null || statusId <= 0)
			{

				return ResponseUtil.createSuccessResponse().setData(CommonUtil
						.xformToTypeAheadFormat(documentReasonCode));
			}
			if (statusId == DefinitionCoreService.DOCUMENT_REJECT_STATUS_CODE)
			{

				reasonCodeDif.putAll(documentRejectReasonCode);

			}

			if (statusId == DefinitionCoreService.DOCUMENT_ACCEPT_STATUS_CODE)
			{

				reasonCodeDif.putAll(documentAcceptReasonCode);

			}
			return ResponseUtil.createSuccessResponse()
					.setData(CommonUtil.xformToTypeAheadFormat(reasonCodeDif));
		}

		if (groupId == DefinitionCoreService.PAYMENT_REASON_GROUP_CODE)
		{
			reasonCodeDif.clear();

			if (statusId == null || statusId <= 0)
			{
				return ResponseUtil.createSuccessResponse().setData(CommonUtil
						.xformToTypeAheadFormat(paymentresoncode));
			}
			if (statusId == DefinitionCoreService.PAYMENT_ACCEPT_STATUS_CODE)
			{

				reasonCodeDif.putAll(paymentAcceptReasonCode);

			}
			if (statusId == DefinitionCoreService.PAYMENT_REJECT_STATUS_CODE)
			{

				reasonCodeDif.putAll(paymentRejectReasonCode);

			}
			if (statusId == DefinitionCoreService.PAYMENT_REFUND_STATUS_CODE)
			{

				reasonCodeDif.putAll(paymentRefundReasonCode);

			}
			return ResponseUtil.createSuccessResponse()
					.setData(CommonUtil.xformToTypeAheadFormat(reasonCodeDif));
		}

		if (groupId == DefinitionCoreService.REASSIGN_REASON_GROUP)
		{
			reasonCodeDif.clear();

			if (statusId == null || statusId <= 0)
			{
				return ResponseUtil.createSuccessResponse().setData(CommonUtil
						.xformToTypeAheadFormat(reassignReasoncode));
			}
			if (statusId == DefinitionCoreService.REASSIGN_ACCEPT_STATUS_CODE)
			{
				reasonCodeDif.putAll(reassignAcceptReasonCode);

			}
			if (statusId == DefinitionCoreService.REASSIGN_REJECT_STATUS_CODE)
			{

				reasonCodeDif.putAll(reassignRejectReasonCode);

			}
			return ResponseUtil.createSuccessResponse()
					.setData(CommonUtil.xformToTypeAheadFormat(reasonCodeDif));
		}

		if (groupId == DefinitionCoreService.ETR_REASON_GROUP)
		{

			for (Entry<String, String> entry : etrReasonCode.entrySet())
			{
				reasonCodeDif
						.put(Long.valueOf(entry.getKey()), entry.getValue());
			}
			return ResponseUtil.createSuccessResponse()
					.setData(CommonUtil.xformToTypeAheadFormat(reasonCodeDif));

		}

		if (groupId == DefinitionCoreService.RESOLVE_REASON_GROUP)
		{
			return ResponseUtil.createSuccessResponse().setData(CommonUtil
					.xformToTypeAheadFormat(resolveStatusCode));

		}
		return new APIResponse();
	}

	@Override
	public String getPropertiesForCustomerScheduling(String propertyName)
	{
		return customerScheduleDef.get(propertyName);
	}

	public Boolean getIsTestBuild()
	{
		return isTestBuild;
	}

	public void setIsTestBuild(Boolean isTestBuild)
	{
		this.isTestBuild = isTestBuild;
	}

	@Override
	public boolean isDocumetStatus(String status)
	{

		String documentStatus = statusDef
				.get(DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER);

		String[] listOfDocumentStatus = documentStatus.split(",");

		for (String str : listOfDocumentStatus)
		{
			if (str.trim().equalsIgnoreCase(status.trim()))
			{
				return true;
			}

		}

		return false;

	}

	@Override
	public boolean isPaymentStatus(String status)
	{
		String paymentStatus = statusDef
				.get(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER);

		String[] listOfPaymentStatus = paymentStatus.split(",");

		for (String str : listOfPaymentStatus)
		{
			if (str.trim().equalsIgnoreCase(status.trim()))
			{
				return true;
			}

		}

		return false;

	}

	@Override
	public Set<String> getClassificationCategory(String key)
	{
		if (classificationCategoryMap.containsKey(key))
		{
			return classificationCategoryMap.get(key);
		}
		return null;
	}

	@Override
	public Map<String, Set<String>> populateClassificationCategoryMap()
	{
		Map<String, Set<String>> classificationCategoryMap = new HashMap<String, Set<String>>();

		for (Map.Entry<String, String> entry : classificationCategoryDef
				.entrySet())
		{
			Set<String> hashSet = convertString2Set(entry.getValue());
			classificationCategoryMap.put(entry.getKey(), hashSet);
		}

		return classificationCategoryMap;
	}

	@Override
	public String getPropertyValue(String key)
	{
		if (key != null)
			return classificationCategoryDef.get(key);
		return null;
	}
	
	@Override
	public String getDeploymentPropertyValue(String key)
	{
		if (key != null && deploymentProperties.containsKey(key))
			return deploymentProperties.get(key);
		return null;
	}
	
	@Override
	public Set<String> getClassificationProperty(String key)
	{
		return convertString2Set(classificationCategoryDef.get(key));
	}

	private Set<String> convertString2Set(String str)
	{
		if (str == null || str.isEmpty() || str.equalsIgnoreCase("null"))
			return new HashSet<String>();

		String[] strArr = str.split(",");

		HashSet<String> hashSet = new HashSet<String>();

		for (int i = 0; i < strArr.length; i++)
		{
			String string = strArr[i];
			hashSet.add(string);

		}

		return hashSet;

	}

	@Override
	public String getMqErrors(String key)
	{
		if (mqErrors.containsKey(key))
		{
			return mqErrors.get(key);
		}
		return null;
	}

	@Override
	public APIResponse readfeasibilityReasonCode()
	{
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat((feasibilityReasonCode)));
	}

	@Override
	public APIResponse readVersionDetails()
	{
		return ResponseUtil.createSuccessResponse().setData(versionDef);
	}

	@Override
	public Set<String> getEmailsBasedOnTypeAndCity(String key)
	{

		if (emailIDsDefnition.get(key) != null)
			return emailIDsDefnition.get(key);
		Set<String> list = new HashSet<String>();
		String values = emailCorporateDef.get(key);
		if (values == null)
			return list;

		List<String> allValue = Arrays.asList(values.split(","));
		for (String s : allValue)
		{
			list.add(s);
		}
		emailIDsDefnition.put(key, list);

		return emailIDsDefnition.get(key);
	}

	@Override
	public APIResponse getAllfrNatureCodes()
	{
		List<NodeValueVo> list = CommonUtil
				.xformToNodeValueFormat(frNatureCodes);
		list.add(0, new NodeValueVo("-1", "All"));

		return ResponseUtil.createSuccessResponse().setData(list);
	}

	@Override
	public String findFrNatureCodeBySymptomName(String symptomName)
	{
		return frNatureCodes.get(symptomName);
	}

	@Override
	public APIResponse getAllFrSymptomNameByFlowId(Long flowId)
	{
		List<TypeAheadVo> typeAhead = new ArrayList<TypeAheadVo>();
		if (flowId != null)
		{
			try
			{
				String key = flowId + "";
				String names = frFlowNameAndFlowIdMap.get(key);
				TypeAheadVo vo = null;
				for (String value : Arrays.asList(names.split(",")))
				{
					vo = new TypeAheadVo();
					vo.setName(value);
					typeAhead.add(vo);
				}
				return ResponseUtil.createSuccessResponse().setData(typeAhead);

			} catch (Exception exception)
			{
				LOGGER.error("Error While executing getAllFrSymptomNameByFlowId   :"
						+ exception.getMessage());
				exception.printStackTrace();
				return ResponseUtil.createSuccessResponse().setData(typeAhead);
			}
		}
		return ResponseUtil.createSuccessResponse().setData(typeAhead);
	}

	@Override
	public APIResponse getSalesCustomerDeniedReasons()
	{
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(salesCustomerDeniedReasons));
	}

	@Override
	public APIResponse getDeploymentCustomerDeniedReasons()
	{
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(deploymentCustomerDeniedReasons));
	}

	@Override
	public APIResponse getCopper2FiberReasons()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(copper2fiber));
	}

	@Override
	public APIResponse getFeasibilityRejectedByNeReasons()
	{
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(feasibilityRejectedByNeReasons));
	}

	@Override
	public APIResponse getFiberTypes()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(fiberTypes));
	}

	@Override
	public APIResponse getFrClosureRemarks()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(frClosuerRemarks));

	}

	@Override
	public APIResponse getElementDenialReasonCodeBlr()
	{
		ReasonCodeVo reasonCodeVo = new ReasonCodeVo();

		reasonCodeVo.setCopperReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementDenialCopperReasonCodeBlr));
		reasonCodeVo.setFiberReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementDenialFiberReasonCodeBlr));
		reasonCodeVo.setNonFeasibleReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementDenialNonFeasibleReasonCodeBlr));
		reasonCodeVo.setPermissionDenialReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementPermissionDenialReasonCodeBlr));
		reasonCodeVo.setElementRacFixingDenialReasons(CommonUtil
				.xformToTypeAheadFormat(elementRacFixingDenialReasonsBlr));
		return ResponseUtil.createSuccessResponse().setData(reasonCodeVo);
	}
	
	@Override
	public List<TypeAheadVo> getFrBushBackReasonCode()
	{
//		ReasonCodeVo reasonCodeVo = new ReasonCodeVo();

		return CommonUtil
				.xformToTypeAheadFormat(frPushBackReasonCode);
		
	}

	@Override
	public APIResponse getElementDenialReasonCodeHyd()
	{
		ReasonCodeVo reasonCodeVo = new ReasonCodeVo();

		reasonCodeVo.setCopperReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementDenialCopperReasonCodeHyd));
		reasonCodeVo.setFiberReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementDenialFiberReasonCodeHyd));
		reasonCodeVo.setNonFeasibleReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementDenialNonFeasibleReasonCodeHyd));
		reasonCodeVo.setPermissionDenialReasonCode(CommonUtil
				.xformToTypeAheadFormat(elementPermissionDenialReasonCodeHyd));
		reasonCodeVo.setElementRacFixingDenialReasons(CommonUtil
				.xformToTypeAheadFormat(elementRacFixingDenialReasonsHyd));

		return ResponseUtil.createSuccessResponse().setData(reasonCodeVo);
	}

	@Override
	public APIResponse getMqErrorMessage()
	{
		return ResponseUtil.createSuccessResponse()
				.setData((CommonUtil.xformToTypeAheadFormat(mqErrorMessage)));
	}

	@Override
	public String getMqErrorMessageById(Long errorId)
	{

		if (mqErrorMessage.containsKey(errorId))
		{
			return mqErrorMessage.get(errorId);
		} else
			return null;
	}

	@Override
	public String getIntegrationFilePropertiesByKey(String key)
	{
		return integrationProperties.get(key);
	}

	@Override
	public APIResponse getProfession()
	{
		return ResponseUtil.createSuccessResponse()
				.setData((CommonUtil.xformToTypeAheadFormat(professions)));
	}

	@Override
	public String getProfessionByKey(long key)
	{
		return professions.get(key);
	}
	@Override
	public boolean getProfessionByValues(String values)
	{
		return professions.values().contains(values);
	}
	
	@Override
	public boolean getTitlesByValues(String values)
	{
		return titles.values().contains(values);
	}
	
	@Override
	public boolean getProspectTypeByValues(String values)
	{
		return prospectTypes.values().contains(values);
	}

	@Override
	public Long getEscalationTypeByValues(String values) {
		
		if (escalationType.values().contains(values)) {
			
			for (Map.Entry<Long, String> entry : escalationType.entrySet()) {
				
				if (entry.getValue().equalsIgnoreCase(values)) {
					
					return entry.getKey();
				}
			}
		}
		return null;
	}
	@Override
	public String getEscalationTypeByKey(Long key) {
		
		if (escalationType.containsKey(key)) {
			
			return escalationType.get(key);
		}
		return null;
	}
	
	@Override
	public boolean getHowDiducome2knowAbtACTByValues(String values)
	{
		return roiMediaSource.values().contains(values);
	}
	@Override
	public boolean getInqueryTypeByValues(String values)
	{
		return readInquiry.values().contains(values);
	}

	@Override
	public APIResponse getTitle()
	{
		return ResponseUtil.createSuccessResponse()
				.setData((CommonUtil.xformToTypeAheadFormat(titles)));
	}

	@Override
	public APIResponse getProspectTypes()
	{
		return ResponseUtil.createSuccessResponse()
				.setData((CommonUtil.xformToTypeAheadFormat(prospectTypes)));
	}

	public static  Map<String, String> getHelpLineNUmbers()
	{
		return helpLineNUmbers;
	}

	public static  void setHelpLineNUmbers(
			Map<String, String> helpLineNUmbers)
	{
		DefinitionCoreServiceImpl.helpLineNUmbers = helpLineNUmbers;
	}

	@Override
	public String getClassificationQuery(String queryKey)
	{
		LOGGER.info(" Classification properties :: " +classificationQuery);
		if (classificationQuery.containsKey(queryKey))
			return classificationQuery.get(queryKey);

		return null;
	}
	
	
	@Override
	public String getThreadsPoolsInfo(String queryKey)
	{
		if (threadPoolInfo.containsKey(queryKey))
			return threadPoolInfo.get(queryKey);

		return null;
	}

	@Override
	public APIResponse getSalesDropdown()
	{
		SalesDropdownVo reasonCodeVo = new SalesDropdownVo();

		reasonCodeVo.setColdProspectReason(CommonUtil
				.xformToTypeAheadFormat(coldProspectReasoncode));

		reasonCodeVo.setCustomerType(CommonUtil
				.xformToTypeAheadFormat(customerType));

		reasonCodeVo.setDocumentStatus(CommonUtil
				.xformToTypeAheadFormat(documentReasonCode));

		reasonCodeVo.setHotProspectReason(CommonUtil
				.xformToTypeAheadFormat(hotProspectReasoncode));

		reasonCodeVo.setInquiry(CommonUtil.xformToTypeAheadFormat(readInquiry));

		reasonCodeVo.setMediumProspectReason(CommonUtil
				.xformToTypeAheadFormat(mediumProspectReasoncode));

		reasonCodeVo
				.setPaymentMode(CommonUtil.xformToTypeAheadFormat(paymentMode));

		reasonCodeVo.setPaymentStatus(CommonUtil
				.xformToTypeAheadFormat(paymentresoncode));

		reasonCodeVo.setProfession((CommonUtil
				.xformToTypeAheadFormat(professions)));

		reasonCodeVo.setProspectTypes((CommonUtil
				.xformToTypeAheadFormat(prospectTypes)));

		reasonCodeVo.setReassignmentStatus(CommonUtil
				.xformToTypeAheadFormat(elementDenialCopperReasonCodeHyd));

		reasonCodeVo.setSalesCustomerDeniedPermissionReason(CommonUtil
				.xformToTypeAheadFormat(salesCustomerDeniedReasons));

		reasonCodeVo.setSource((List<TypeAheadVo>) getMediaSource(AuthUtils.getCurrentUserCity().getId()).getData());

		reasonCodeVo.setTitles((CommonUtil.xformToTypeAheadFormat(titles)));

		reasonCodeVo.setTariffList(tariffDAO
				.getTariffByCity(AuthUtils.getCurrentUserCity().getId()));

		reasonCodeVo.setRouterVoList(routerDAO
				.getRouter(AuthUtils.getCurrentUserCity().getId()));

		reasonCodeVo.setBranchList(branchCoreService
				.getBranchesByCityId(AuthUtils.getCurrentUserCity().getId()
						+ ""));
		reasonCodeVo.setNationalityList(CommonUtil.xformToTypeAheadFormat(nationality));
		
		reasonCodeVo.setIspList(CommonUtil.xformToTypeAheadFormat(existingIsp));
		
		reasonCodeVo.setUinTypesList(CommonUtil.xformToTypeAheadFormat(uinType));
		
		return ResponseUtil.createSuccessResponse().setData(reasonCodeVo);
	}

	@Override
	public APIResponse getCustomerRemarks()
	{
		return ResponseUtil.createSuccessResponse().setData(customerRemarks);
	}

	private void populateCustomerRemeaks()
	{
		customerRemarks.putAll(convertLongKeyToString(coldProspectReasoncode));
		customerRemarks.putAll(convertLongKeyToString(hotProspectReasoncode));
		customerRemarks
				.putAll(convertLongKeyToString(mediumProspectReasoncode));
	}

	private Map<String, String> convertLongKeyToString(
			Map<Long, String> longKeys)
	{
		Map<String, String> customerRemark = new LinkedHashMap<String, String>();

		for (Map.Entry<Long, String> entry : longKeys.entrySet())
		{
			customerRemark.put(((Long) entry.getKey())
					+ "", (String) entry.getValue());
		}

		return customerRemark;
	}

	@Override
	public String getCustomerRemarksByKey(String key)
	{
		if (customerRemarks.containsKey(key))
			return customerRemarks.get(key);
		else
			return null;
	}

	@Override
	public Map<String,String> getDefectSubDefectCode()
	{
		return defectAndSubdefectCode;
	}
	
	@Override
	public String getFrStatusStringByStatusId( Long statusId )
	{
		if( statusId == null)
			return "";
		
		return frStatusMap.get(statusId+"");
	}

	
	@Override
	public Map<String, List<String>> getDefectSubDefectCodeDefinition()
	{
		if (defectSubDefectCodeDefinition != null && !defectSubDefectCodeDefinition.isEmpty())
			return defectSubDefectCodeDefinition;
		
		String key = null;
		String defectCode = null;
		try
		{
			Map<String, String> defectSubDefectCodeMap = getDefectSubDefectCode();
			defectSubDefectCodeMap
					.remove(DefectSubDefectCodeMapper.DEFECT_CODE + "");

			int defectCodeStart = 100;
			for (Map.Entry<String, String> entry : defectSubDefectCodeMap
					.entrySet())
			{
				key = entry.getKey();
				defectCodeDefinition.put(key, defectCodeStart);
				
				defectCode = DefectSubDefectCodeMapper.DEFECT_SUBDEFECT_MAP
						.get(Integer.valueOf(key));

				if (defectCode != null)
				{
					
					List<String> subdefects = 	Arrays
					.asList(entry.getValue().split(","));
					defectSubDefectCodeDefinition.put(defectCode, subdefects);
					int subDefectCodeStart = 1;
					for (int i = 0; i < subdefects.size(); i++)
					{
						subDefectCodeDefinition.put(subdefects.get(i), subDefectCodeStart+400);
						subDefectCodeStart ++;
					}
					 
				} else
				{
//					LOGGER.info("No defect code for Key : " + key);
				}
				
				defectCodeStart ++;
			}
			
		} catch (Exception e)
		{

		}
		
//		LOGGER.info("Complete defectSubDefectCodeDefinition " + defectSubDefectCodeDefinition);
		return defectSubDefectCodeDefinition;
	}
	
	private void  populateFrTicketCategory()
	{
		if( classificationCategoryDef == null || classificationCategoryDef.isEmpty() ) 
			classificationCategoryDef = commonPropertiesReader
					.readClassificationCategoryDefProperties();
			
		String roiFrTicketCategoryList = classificationCategoryDef.get(FR_TICKET_CATEGORY_ROI);
		String hydFrTicketCategoryList = classificationCategoryDef.get(FR_TICKET_CATEGORY_HYD);
		String[] categorys = null;
		if( roiFrTicketCategoryList != null )
		{
			categorys = roiFrTicketCategoryList.split(",");
			if( categorys != null && categorys.length > 0)
			{
				for( String value : categorys)
				{
					FWMPConstant.TicketCategory.FAULT_REPAIR_ROI.add(value);
					FWMPConstant.TicketCategory.FAULT_REPAIR_BOTH.add(value);
				}
			}
		}
		
		if( hydFrTicketCategoryList != null )
		{
			categorys = hydFrTicketCategoryList.split(",");
			if( categorys != null && categorys.length > 0)
			{
				for( String value : categorys)
				{
					FWMPConstant.TicketCategory.FAULT_REPAIR_HYD.add(value);
					FWMPConstant.TicketCategory.FAULT_REPAIR_BOTH.add(value);
				}
				
			}
		}
		
	}
	
	private void populateDefectCodeMap()
	{
		if( defectCodeNameValue == null || defectCodeNameValue.isEmpty())
			defectCodeNameValue = commonPropertiesReader.readeDefectCodeNameAndValue();
		
		for( Map.Entry<String, String> entry : defectCodeNameValue.entrySet() )
		{
			int key = Integer.valueOf(entry.getKey());
			FRConstant.DefectSubDefectCodeMapper.DEFECT_SUBDEFECT_MAP.put(key, entry.getValue());
		}
		
	}
	
	private void  populateMqClosureMap()
	{
		if( frMqClosurWithDefectSubCode == null || frMqClosurWithDefectSubCode.isEmpty())
			frMqClosurWithDefectSubCode = commonPropertiesReader
				.readFrMqClosureCodeWithDefectSubDefectCode();
		
		String mqClosureCode = null;
		String defectCodeSubDefectCode = null;
		for( Map.Entry<String, String> entry : frMqClosurWithDefectSubCode.entrySet() )
		{
			mqClosureCode = entry.getKey();
			defectCodeSubDefectCode = entry.getValue();
			frMqClosure.put(defectCodeSubDefectCode, mqClosureCode);
		}
	}
	
	private void  populateMqClosureMapHyd()
	{
		if( frMqClosurWithDefectSubCodeHyd == null || frMqClosurWithDefectSubCodeHyd.isEmpty())
			frMqClosurWithDefectSubCodeHyd = commonPropertiesReader
				.readFrMqClosureCodeHydWithDefectSubDefectCode();
		
		String mqClosureCode = null;
		String defectCodeSubDefectCode = null;
		for( Map.Entry<String, String> entry : frMqClosurWithDefectSubCodeHyd.entrySet() )
		{
			mqClosureCode = entry.getKey();
			defectCodeSubDefectCode = entry.getValue();
			frMqClosureHyd.put(defectCodeSubDefectCode, mqClosureCode);
		}
	}
	
	@Override
	public void refreshDefecSubDefectMQClosureCode()
	{
		defectCodeNameValue = commonPropertiesReader.readeDefectCodeNameAndValue();
		defectAndSubdefectCode =commonPropertiesReader.readDefectAndSubDefectCode();	
		frMqClosurWithDefectSubCode = commonPropertiesReader
				.readFrMqClosureCodeWithDefectSubDefectCode();
		frMqClosurWithDefectSubCodeHyd = commonPropertiesReader
				.readFrMqClosureCodeHydWithDefectSubDefectCode();
		
		populateDefectCodeMap();
		getDefectSubDefectCodeDefinition();
		populateMqClosureMap();
		populateMqClosureMapHyd();
	}
	
	@Override
	public String getFrMqClosurCode( String defectCode, String subDefect )
	{
		if( defectCode == null || subDefect == null )
			return null;
		
		String key = defectCode+","+subDefect;
		return frMqClosure.get(key);
	}
	
	@Override
	public String getFrMqClosurCodeHyd( String defectCode, String subDefect )
	{
		if( defectCode == null || subDefect == null )
			return null;
		
		String key = defectCode+","+subDefect;
		return frMqClosureHyd.get(key);
	}
	
	@Override 
	public String getActForceUrl(String key)
	{
		if( key == null || key.isEmpty() )
			return null;
		
		return  integrationKeyAndValue.get(key);
	}
	
	@Override
	public  void reloadClassificationAndIntegration()
	{
		integrationKeyAndValue = commonPropertiesReader.readIntegrationProperties();
		classificationCategoryDef = commonPropertiesReader
				.readClassificationCategoryDefProperties();
		
	}
		/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.service.definition.DefinitionCoreService#getAllStates()
	 */
	@Override
	public APIResponse getAllStates()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(CommonUtil.xformToTypeAheadFormat(allStates));
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.definition.DefinitionCoreService#getMergingQueueDefByKey(java.lang.String)
	 */
	@Override
	public String getMergingQueueDefByKey(String key)
	{
		if(mergingQueueDef.containsKey(key))
			return mergingQueueDef.get(key);
		else
			return null;
					
	}
	/**
	 * @author aditya
	 * String
	 * @param key
	 * @return
	 */
	
	@Override
	public String getMailConfig(String key)
	{
		if(mailConfig.containsKey(key))
			return mailConfig.get(key);
		else
			return null;
					
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.definition.DefinitionCoreService#getDeploymentPropertyValue(java.lang.String)
	 */
	@Override
	public APIResponse getNationality() {
		{
			return ResponseUtil.createSuccessResponse().setData(CommonUtil
					.xformToTypeAheadFormat(nationality));
		}

	}

	@Override
	public APIResponse getExistingISP() {
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(existingIsp));
	}

	@Override
	public APIResponse getUinType() {
		
		return ResponseUtil.createSuccessResponse().setData(CommonUtil
				.xformToTypeAheadFormat(uinType));
	}
	 
	
}
