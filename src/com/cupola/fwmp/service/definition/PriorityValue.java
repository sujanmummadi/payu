package com.cupola.fwmp.service.definition;

public class PriorityValue
{
	int start;
	int end;
	public PriorityValue(int start, int end)
	{
		super();
		this.start = start;
		this.end = end;
	}
	public int getStart()
	{
		return start;
	}
	public void setStart(int start)
	{
		this.start = start;
	}
	public int getEnd()
	{
		return end;
	}
	public void setEnd(int end)
	{
		this.end = end;
	}
	
}
