/**
 * 
 */
package com.cupola.fwmp.service.definition.excels;

import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.util.CommonPropertiesReader;

/**
 * @author aditya
 *
 */
public class ToolsExcelsDefinitionImpl implements ToolsExcelsDefinition
{
	@Autowired
	CommonPropertiesReader commonPropertiesReader;

	private static Map<String, String> devcieToolProperties;
	private static Logger log = LogManager.getLogger(ToolsExcelsDefinitionImpl.class.getName());
	private void init()
	{
		log.info("ToolsExcelsDefinitionImpl init method called..");
		reloadDataFromProperties();
		log.info("ToolsExcelsDefinitionImpl init method executed.");
	}

	@Override
	public void reloadDataFromProperties()
	{
		new Thread()
		{
			@Override
			public void run()
			{
				devcieToolProperties = commonPropertiesReader
						.readDevcieToolProperties();

			}
		}.start();
	}

	@Override
	public String getDevcieToolsIndexByKey(String key)
	{
		if (devcieToolProperties.containsKey(key))
			return devcieToolProperties.get(key);
		else
			return null;
	}

}
