/**
 * 
 */
package com.cupola.fwmp.service.definition.excels;

/**
 * @author aditya
 *
 */
public interface ToolsExcelsDefinition
{
	String getDevcieToolsIndexByKey(String key);

	void reloadDataFromProperties();

	String FWMP_TOOL_DEVCIE_BASE_PATH = "fwmp.tool.devcie.base.path";
	
	String FWMP_TOOL_DEVCIE_ARCHIVE_PATH = "fwmp.tool.devcie.archive.path";

	String FWMP_TOOL_DEVICE_FILE_NAME = "fwmp.tool.device.file.name";

	String FWMP_TOOL_DEVICE_FILE_COLUMN_COUNT = "fwmp.tool.device.file.column.count";

	String FWMP_TOOL_DEVICE_FILE_HEARDER_COUNT = "fwmp.tool.device.file.hearder.count";

	String FWMP_TOOL_DEVICE_FILE_FX_NAME = "fwmp.tool.device.file.fx.name";

	String FWMP_TOOL_DEVICE_FILE_FX_IP_ADDRESS = "fwmp.tool.device.file.fx.ip.address";

	String FWMP_TOOL_DEVICE_FILE_FX_MAC_ADDRESS = "fwmp.tool.device.file.fx.mac.address";

	String FWMP_TOOL_DEVICE_FILE_FX_DEVCIE_TYPE = "fwmp.tool.device.file.fx.devcie.type";

	String FWMP_TOOL_DEVICE_FILE_FX_CITY_NAME = "fwmp.tool.device.file.fx.city.name";

	String FWMP_TOOL_DEVICE_FILE_FX_BRANCH_CODE = "fwmp.tool.device.file.fx.branch.code";

	String FWMP_TOOL_DEVICE_FILE_FX_AREA_NAME = "fwmp.tool.device.file.fx.area.name";

	String FWMP_TOOL_DEVICE_FILE_FX_AVAIALBLE_PORT = "fwmp.tool.device.file.fx.avaialble.port";

	String FWMP_TOOL_DEVICE_FILE_FX_TOTAL_PORT = "fwmp.tool.device.file.fx.total.port";

	String FWMP_TOOL_DEVICE_FILE_FX_USED_PORT = "fwmp.tool.device.file.fx.used.port";

	String FWMP_TOOL_DEVICE_FILE_FX_STATUS = "fwmp.tool.device.file.fx.status";

}
