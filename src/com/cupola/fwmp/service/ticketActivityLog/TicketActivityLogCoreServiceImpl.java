package com.cupola.fwmp.service.ticketActivityLog;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.ProspectType;
import com.cupola.fwmp.FWMPConstant.SubActivity;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.integ.aat.AATResponseVo;
import com.cupola.fwmp.dao.integ.closeticket.MQSCloseTicketDAO;
import com.cupola.fwmp.dao.tablet.TabletDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticket.TicketDetailDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.handler.GCMConsumer;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.workOrder.WorkOrderCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.MessageVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsResponseVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

public class TicketActivityLogCoreServiceImpl
		implements TicketActivityLogCoreService
{
	private Logger log = Logger
			.getLogger(TicketActivityLogCoreServiceImpl.class.getName());

	private TicketActivityLogDAO ticketActivityLogDao;

	private String hydResolutionCode;

	private String roiResolutionCode;

	public void setHydResolutionCode(String hydResolutionCode)
	{
		this.hydResolutionCode = hydResolutionCode;
	}

	public void setRoiResolutionCode(String roiResolutionCode)
	{
		this.roiResolutionCode = roiResolutionCode;
	}

	@Autowired
	EtrCalculator etrCalculatorImpl;

	@Autowired
	TicketDetailDAO ticketDetailDao;

	@Autowired
	UserDao userDao;

	@Autowired
	TabletDAO tabletDAOImpl;

	@Autowired
	GCMConsumer gCMConsumer;

	@Autowired
	WorkOrderDAO workOrderDAO;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	DefinitionCoreService coreService;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	MQSCloseTicketDAO closeTicketDAO;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	CustomerDAO customerDAO;
	
	@Autowired
	WorkOrderCoreService workOrderCoreService;

	public void setTicketActivityLogDao(
			TicketActivityLogDAO ticketActivityLogDao)
	{
		this.ticketActivityLogDao = ticketActivityLogDao;
	}
	@Override
	public APIResponse addTicketActivityLog(
			TicketAcitivityDetailsVO ticketActivity )
	{
		return addTicketActivityLogCommon(ticketActivity, null);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public APIResponse addTicketActivityLogCommon(
			TicketAcitivityDetailsVO ticketActivity,UpdateProspectVO updateProspectVO)
	{

		log.info("Update WorkOrder CurrentActivity as TicketAcitivityDetailsVO:"
				+ ticketActivity);

		TicketActivityLogVo ticketLog = new TicketActivityLogVo();
		TicketAcitivityDetailsResponseVO responseVo = new TicketAcitivityDetailsResponseVO();

		int ticketActivityValue = 0;

		int updatedActivityCount = 0;

		Long userId = AuthUtils.getCurrentUserId();
		int role = AuthUtils.getCurrentUserRole();

		if (userId == null)
		{
			log.info("User not found as:" + userId);
			return ResponseUtil.createNullParameterResponse()
					.setData("User not found as:" + userId);
		}
		if (ticketActivity == null)
		{
			log.info("TicketAcitivityDetailsVO :" + ticketActivity);
			return ResponseUtil.nullArgument()
					.setData("found TicketAcitivityDetailsVO null");
		}
		if (ticketActivity.getTicketId() == null
				|| ticketActivity.getTicketId().isEmpty())
		{
			log.info("found ticketId null :");
			return ResponseUtil.createNullParameterResponse()
					.setData("found ticket id null");
		}
		if (ticketActivity.getActivityId() == null
				|| ticketActivity.getActivityId().equals(""))
		{

			log.info("found activity null");
			return ResponseUtil.createNullParameterResponse()
					.setData("found activity null");
		}

		if (ticketActivity != null)
		{
			if (ticketActivity.getTicketId() != null)
			{
				if (ticketActivity.getReasons() != null
						&& !ticketActivity.getReasons().isEmpty())
				{
					Long ticketId = Long.valueOf(ticketActivity.getTicketId());

					log.info("NE comment updating during Rac fixing denied for ticket Id "
							+ ticketId + " reasons " + ticketActivity.getReasons());

					int updateNeCommentCount = customerDAO
							.updateNeComment(ticketId, ticketActivity
									.getReasons());

					log.info("NE comment updated during Rac fixing denied"
							+ updateNeCommentCount);
				}
			}

		}

		
		Map<String, String> subActivityDetails = ticketActivity
				.getSubActivityDetail();

		if (ticketActivity.getVendorId() != null
				&& !ticketActivity.getVendorId().isEmpty())

		{

			ticketLog.setVendorId(Long.valueOf(ticketActivity.getVendorId()));
		}

		ticketLog
				.setTicketId(Long.valueOf(ticketActivity.getTicketId().trim()));
		ticketLog.setAddedBy(userId);
		ticketLog.setModifiedBy(userId);
		ticketLog.setAddedOn(ticketActivity.getActivityCompletedTime());
		ticketLog.setModifiedOn(ticketActivity.getActivityCompletedTime());

		ticketLog.setActivityId(Long
				.valueOf(ticketActivity.getActivityId().trim()));

		ticketLog.setStatus(TicketStatus.COMPLETED);
		

		try
		{
			updatedActivityCount = ticketActivityLogDao
					.updateWorkOrderActivity(ticketLog);

			log.info("ticketActivity.getActivityId() "
					+ ticketActivity.getActivityId()
					+ " ticketActivity.getTicketId() "
					+ ticketActivity.getTicketId());

			if (Long.valueOf(ticketActivity.getActivityId()
					.trim()) == FWMPConstant.Activity.MATERIAL_CONSUMPTION)
			{
				log.info("Inside Material Activity for ticket "
						+ ticketActivity.getTicketId());

				try
				{
					if (ticketActivityLogDao.isActivityClosed(Long
							.valueOf(ticketActivity.getTicketId()
									.trim()), Activity.PORT_ACTIVATION))
					{

						log.info("AAT is done for Ticket id "
								+ ticketActivity.getTicketId());

						String workOrderNo = workOrderDAO
								.getWoNumberForTicket(Long
										.valueOf(ticketActivity.getTicketId()
												.trim()));
						log.info("Workorder for Ticket id "
								+ ticketActivity.getTicketId() + " is "
								+ workOrderNo);

						TicketClosureDataVo closeTicketVo = new TicketClosureDataVo();

						closeTicketVo.setWorkOrderNo(workOrderNo);

						closeTicketVo.setTicketId(Long
								.valueOf(ticketActivity.getTicketId().trim()));

						if (cityDAO
								.getCityNameByTicketId(Long
										.valueOf(ticketActivity.getTicketId()
												.trim()))
								.equalsIgnoreCase(CityName.HYDERABAD_CITY))
						{
							closeTicketVo
									.setTicketResolutionCode(hydResolutionCode);
						} else
						{
							closeTicketVo
									.setTicketResolutionCode(roiResolutionCode);
						}
						String mqResponse = null;
//						String mqResponse = closeTicketDAO
//								.closeTicket(closeTicketVo);

						log.info("Ticket close response mqResponse "
								+ mqResponse);

						if (mqResponse != null)
						{
							InputSource is = new InputSource();
							is.setCharacterStream(new StringReader(mqResponse));

							DocumentBuilder db = DocumentBuilderFactory
									.newInstance().newDocumentBuilder();
							Document doc = db.parse(is);

							// Node status =
							// doc.getElementsByTagName("MESSAGE").item(0);

							Node errorNo = doc.getElementsByTagName("ERRORNO")
									.item(0);

							String remarks = coreService.getMqErrors(errorNo
									.getTextContent().trim());

							closeTicketVo.setRemarks(remarks);

							Long result = ticketDAO
									.ticketClosureStatusUpdate(closeTicketVo);

							Ticket ticket = ticketDAO
									.getTicketById(Long.valueOf(ticketActivity
											.getTicketId().trim()));

							if (result > 0)
							{

								if (ticket != null && ticket
										.getUserByCurrentAssignedTo() != null)
								{
									dbUtil.removeOneTicketFromQueue(ticket
											.getUserByCurrentAssignedTo()
											.getId(), ticket.getId());

								}

							}

							log.info("Ticket is closed as AAT was done before material update for ticket no "
									+ ticket != null ? ticket.getId() : null);
						}

					}
				} catch (NumberFormatException e)
				{
					log.error("Exception NumberFormatException while closing AAT from TicketActivityLog for ticket no "
							+ Long.valueOf(ticketActivity.getTicketId()
									.trim()));
					e.printStackTrace();

				} catch (DOMException e)
				{
					log.error("Exception DOMException while closing AAT from TicketActivityLog for ticket no "
							+ Long.valueOf(ticketActivity.getTicketId()
									.trim()));
					e.printStackTrace();

				} catch (ParserConfigurationException e)
				{
					log.error("Exception ParserConfigurationException while closing AAT from TicketActivityLog for ticket no "
							+ Long.valueOf(ticketActivity.getTicketId()
									.trim()));
					e.printStackTrace();

				} catch (SAXException e)
				{
					log.error("Exception SAXException while closing AAT from TicketActivityLog for ticket no "
							+ Long.valueOf(ticketActivity.getTicketId()
									.trim()));
					e.printStackTrace();

				} catch (IOException e)
				{
					log.error("Exception IOException while closing AAT from TicketActivityLog for ticket no "
							+ Long.valueOf(ticketActivity.getTicketId()
									.trim()));
					e.printStackTrace();
				}

			}

		} catch (Exception e)
		{
			updatedActivityCount = 0;
			e.printStackTrace();
		}
		if (updatedActivityCount <= 0)
		{
			return ResponseUtil.createFailureResponse()
					.setData("found no tickrtId in WorkOrder for ticketId:"
							+ ticketActivity.getTicketId());
		}

		responseVo.setTicketId(ticketActivity.getTicketId());
		String connectionType = null;
		if (updatedActivityCount > 0)
		{

			if (ticketActivity.getSubActivityDetail() == null
					|| ticketActivity.getSubActivityDetail().isEmpty())
			{

				ticketLog.setId(StrictMicroSecondTimeBasedGuid.newGuid());

				try
				{
					ticketActivityValue = ticketActivityLogDao
							.addTicketActivityLog(ticketLog);

				}

				catch (Exception e)
				{
					e.printStackTrace();
				}

			}

			else
			{
				Iterator<Entry<String, String>> iterator = subActivityDetails
						.entrySet().iterator();

				while (iterator.hasNext())
				{
					Map.Entry mapEntry = iterator.next();
					String subActivityid = (String) mapEntry.getKey();
					String value = ((String) mapEntry.getValue()).trim();
					
					if (value !=null && !value.isEmpty())
					{
						
						if((SubActivity.FEASIBILITY_CONNECTION_TYPE+"").equals(subActivityid))
							connectionType = value;
						
						ticketLog.setValue(value);
						
					} else
					{
						ticketLog.setValue(null);
					}

					

					if ( subActivityid != null && (!subActivityid.equals("") || !subActivityid.equals(" ")))
					{

						Long subActvityId = Long.valueOf(subActivityid.trim());
						ticketLog.setSubActivityId(subActvityId);
					}

					ticketLog.setId(StrictMicroSecondTimeBasedGuid.newGuid());

					try
					{
						ticketActivityValue = ticketActivityLogDao
								.addTicketActivityLog(ticketLog);
						value = null;
						
					} catch (Exception e)
					{
						e.printStackTrace();
					}

				}
			}
			ticketActivityLogDao.updateTicketStatus(ticketLog);
			try
			{
				ticketLog.setCurrentProgress((long) (etrCalculatorImpl
						.calculateActivityPercentage(ticketLog
								.getTicketId(), ticketLog.getActivityId())));
				responseVo
						.setProgressPercentage(ticketLog.getCurrentProgress());
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			TicketLog logDetail = new TicketLogImpl(ticketLog
					.getTicketId(), TicketUpdateConstant.ACTIVITY_UPDATED, AuthUtils
							.getCurrentUserId());
			logDetail.setActivityId(ticketLog.getActivityId());
			
			if(connectionType != null && 
					Long.valueOf(ticketActivity.getActivityId().trim()) == Activity.PHYSICAL_FEASIBILITY)
			{
				Map<String,String> additionalAttributeMap = new HashMap<String,String>();
				additionalAttributeMap.put(TicketUpdateConstant.NOTE_KEY,connectionType);
				logDetail.setAdditionalAttributeMap(additionalAttributeMap);
			
			}
			else if( updateProspectVO != null && updateProspectVO.getProspectType() != null 
					&& Long.valueOf(ticketActivity.getActivityId().trim()) == Activity.BASIC_INFO_UPDATE)
			{
				 
					Map<String, String> additionalAttributeMap = new HashMap<String, String>();
					additionalAttributeMap
							.put(TicketUpdateConstant.NOTE_KEY, updateProspectVO.getProspectType());
					logDetail.setAdditionalAttributeMap(additionalAttributeMap);
					
					
			} else if(Long.valueOf(ticketActivity.getActivityId().trim())
					== Activity.BASIC_INFO_UPDATE && AuthUtils.isNIUser())
			{

				Map<String, String> additionalAttributeMap = new HashMap<String, String>();
				additionalAttributeMap
						.put(TicketUpdateConstant.NOTE_KEY, "FEASIBILITY");
				logDetail.setAdditionalAttributeMap(additionalAttributeMap);
			}
			
			if (ticketLog.getStatus() != null)
				logDetail.setActivityValue(ticketLog.getStatus() + "");
			TicketUpdateGateway.logTicket(logDetail);
			
			log.info("Ticket Activitylog updated ...");

		}
		
		if (updateProspectVO == null)
		{
			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				if( AuthUtils.isNIUser())
					workOrderCoreService.updateWorkOrderActivityInCRM(
						ticketLog.getTicketId() == null ? null : Long.valueOf(ticketLog.getTicketId()), null, null,
						ticketLog.getWorkOrderNumber(), null, null, null,null);
			}
		}

		return ResponseUtil.createSuccessResponse().setData(responseVo);
	}

	public String convertObjectToString(Object message)
	{
		if (message instanceof MessageVO)
			message = (MessageVO) message;
		if (message instanceof AATResponseVo)
			message = (TicketAcitivityDetailsVO) message;

		ObjectMapper mapper = new ObjectMapper();

		String dataString = "";
		try
		{
			dataString = mapper.writeValueAsString(message);

		} catch (JsonGenerationException e1)
		{
			log.error("exception TicketActivityLogService convertObjectToString::"
					+ e1.getMessage());
		} catch (JsonMappingException e1)
		{
			log.error("exception TicketActivityLogService convertObjectToString::"
					+ e1.getMessage());
		} catch (IOException e1)
		{
			log.error("exception TicketActivityLogService convertObjectToString::"
					+ e1.getMessage());
		}
		return dataString;
	}
}