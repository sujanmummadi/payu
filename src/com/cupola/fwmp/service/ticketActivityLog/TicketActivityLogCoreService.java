package com.cupola.fwmp.service.ticketActivityLog;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

public interface TicketActivityLogCoreService
{


	APIResponse addTicketActivityLog(TicketAcitivityDetailsVO ticketActivity);

	APIResponse addTicketActivityLogCommon(
			TicketAcitivityDetailsVO ticketActivity,
			UpdateProspectVO updateProspectVO);

}
