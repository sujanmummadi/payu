package com.cupola.fwmp.service.workOrder;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.AppointmentVO;

public interface WorkOrderCoreService
{
	public APIResponse updatePriority(String workOrderNo, int priority);

	APIResponse createWorkOrderByTicketId(Long ticketid);

	APIResponse createWorkOrderByProspect(String prospectNo, String mqId,
			String workOrderNumber, String ticketCategory);

	public APIResponse updatePriority(String workOrderNo, int priority,
			Integer escalationType, Integer escalatedValue);

	APIResponse updateFiberToCopper(long ticketId, long typeId);

	public APIResponse createFrDetailsByProspect(
			MQWorkorderVO roiMQWorkorderVO, String deviceName);

	Map<Long, Boolean> createWorkOrderInBulk(
			List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap);

	public Map<Long, Boolean> createFrWorkOrderInBulk(
			List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap);

	public APIResponse updateFrPriority(String workOrderNo, int priority,
			Integer escalationType, Integer escalatedValue);

	public Map<Long, Boolean> createFrTicketWorkOrderInBulk(List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap);
	
//	APIResponse updateAppointmentDate(AppointmentVO appointmentVO);
	
	public void updateWorkOrderActivityInCRM(Long ticketId, Long customerId, String prospectNumber, String workOrderNumber, Long cityId, Long branchId ,String activityStage,String activityId);

}
