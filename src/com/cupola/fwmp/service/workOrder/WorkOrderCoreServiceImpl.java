package com.cupola.fwmp.service.workOrder;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.FrTicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.controller.integ.CRMUpdateRequestHandler;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.dao.fr.FrTicketDao;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.workOrder.WorkOrderDAO;
import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.WorkOrder;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.notification.EmailNotiFicationHelper;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.AppointmentVO;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.WorkOrderVo;
import com.cupola.fwmp.vo.fr.FrTicketVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo;

public class WorkOrderCoreServiceImpl implements WorkOrderCoreService {

	@Autowired
	WorkOrderDAO workOrderDao;
	@Autowired
	TicketDAO ticketDao;
	@Autowired
	WorkOrderDAO workOrderDAO;
	@Autowired
	UserDao userDao;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	EtrCalculator etrCalculator;

	@Autowired
	private FrTicketService frTicketService;
	@Autowired
	private DefinitionCoreService definitionCoreService;

	@Autowired
	private DeviceDAO deviceDao;

	@Autowired
	private FrTicketDao frTicketDao;

	private String failedMqSupportEmailId;
	
	@Autowired
	EmailNotiFicationHelper emailNotiFicationHelper;
	
	@Autowired
	CRMUpdateRequestHandler crmUpdateRequestHandler;
	
	@Autowired
	DBUtil dbUtil;

	static final Logger logger = Logger
			.getLogger(WorkOrderCoreServiceImpl.class);

	@Override
	public APIResponse createWorkOrderByTicketId(Long ticketid) {
		Ticket ticket = ticketDao.getTicketById(ticketid);
		if (ticket != null && ticket.getProspectNo() != null)
			return createWorkOrderByProspect(ticket.getProspectNo(), null, null, null);
		else
			return ResponseUtil.createFailureResponse();
	}

	@Override
	public APIResponse createWorkOrderByProspect(String prospectNo, String mqId, String workOrderNumber,
			String ticketCategory) {
		WorkOrder woResponse = null;
		try {
			logger.info("Creating Prospect for Ticket " + prospectNo + " with mqId " + mqId + " workOrderNumber "
					+ workOrderNumber);

			Ticket ticketData = ticketDao.getTicketByProspectNumber(prospectNo);

			if (ticketData == null)
				return ResponseUtil.createSaveFailedResponse();

			WorkOrderVo wo = new WorkOrderVo();
			wo.setStatus(TicketStatus.WO_GENERATED);
			wo.setTicketId(ticketData.getId());
			wo.setAddedBy(ticketData.getModifiedBy());

			if (ticketCategory.equalsIgnoreCase(TicketCategory.NEW_INSTALLATION)) {
				wo.setWorkOrderTypeId(TicketCategory.NIDEPLOYMENT);

			} else if (ticketCategory.equalsIgnoreCase(TicketCategory.FAULT_REPAIR)) {
				wo.setWorkOrderTypeId(TicketCategory.FR);

			} else {
				wo.setWorkOrderTypeId(0l);
			}

			wo.setAssignedBy(FWMPConstant.SYSTEM_ENGINE); // Auto Assigned
			wo.setWorkOrderNo(workOrderNumber);

			// wo.setCurrentEtr(GenericUtil.convertToUiDateFormat(etrCalculator
			// .calculateEtrForTicketCreation(ticketData.getId())));

			if (ticketData != null) {
				Gis gisData = ticketDao.getGisInfoTicket(ticketData.getId());

				logger.info("##################################### gisData " + gisData);
				UserVo assignedUser = new UserVo();
				if (gisData != null) {
					wo.setGisId(gisData.getId());

					String fxName = null;

					/*
					 * if (gisData.getCxMacAddress() != null &&
					 * gisData.getCxMacAddress().trim().length() >0) { deviceMac
					 * = gisData.getCxMacAddress(); } else if
					 * (gisData.getFxMacAddress() != null) {
					 */
					fxName = gisData.getFxName();

					// if (gisData.getWorkstageType() == null
					// || gisData.getWorkstageType() <= 0
					// || gisData.getConnectionType() == null
					// || gisData.getConnectionType().isEmpty())
					//
					// {
					// wo.setInitialWorkStageId(WorkStageType.FIBER);
					// wo.setCurrentWorkStageId(WorkStageType.FIBER);
					//
					// } else
					// {
					if ((gisData.getWorkstageType() != null
							&& gisData.getWorkstageType().longValue() == WorkStageType.COPPER)
							|| (gisData.getConnectionType() != null
									&& WorkStageName.COPPER.equalsIgnoreCase(gisData.getConnectionType()))) {
						wo.setInitialWorkStageId(WorkStageType.COPPER);
						wo.setCurrentWorkStageId(WorkStageType.COPPER);

					} else if ((gisData.getWorkstageType() != null
							&& gisData.getWorkstageType().longValue() == WorkStageType.FIBER)
							|| (gisData.getConnectionType() != null
									&& WorkStageName.FIBER.equalsIgnoreCase(gisData.getConnectionType()))) {
						wo.setInitialWorkStageId(WorkStageType.FIBER);
						wo.setCurrentWorkStageId(WorkStageType.FIBER);
					}
					// }

					/* } */
					logger.info("GisData for prospect " + prospectNo + " device " + gisData);

					if (wo.getInitialWorkStageId() != null && wo.getInitialWorkStageId() == WorkStageType.FIBER) {
						assignedUser = userDao.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

						if (assignedUser == null) {
							assignedUser = userDao.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
						}

					} else if (wo.getInitialWorkStageId() != null
							&& wo.getInitialWorkStageId() == WorkStageType.COPPER) {
						assignedUser = userDao.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

						if (assignedUser == null) {
							assignedUser = userDao.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
						}
					}

					if (assignedUser != null) {
						logger.debug("Found user for device " + fxName + " User " + assignedUser);
						wo.setAssignedTo(assignedUser.getId());

						UserVo assignedBy = new UserVo();
						assignedBy = userDao.getUsersByDeviceName(fxName, AuthUtils.SALES_EX_DEF);
						ticketDao.updateCurrentAssignedTo(ticketData.getId(), assignedUser.getId(),
								assignedBy != null ? assignedBy.getId() : null, TicketStatus.WO_GENERATED);

						// ticketDao.updateCurrentAssignedTo(ticketData
						// .getId(), assignedUser.getId());
					} else {
						logger.error("No User Assigned to device " + fxName + " with role " + UserRole.ROLE_NI);
						wo.setStatus(TicketStatus.NOT_ASSIGNED);

					}

				} else {
					wo.setStatus(TicketStatus.GIS_DATA_NOT_AVAILABLE);
					logger.info("No GIS Data available for Ticket " + ticketData.getId());
				}
				Date etr = null;
				if (wo.getInitialWorkStageId() != null)
					etr = etrCalculator.getETRforWorkStageType(wo.getInitialWorkStageId());

				if (etr == null)
					etr = new Date();
				logger.info("Got etr " + etr + " for mqId " + mqId + " and ticket Id " + ticketData.getId());

				customerDAO.updateCustomerMq(ticketData.getId(), mqId, etr);

				wo.setWorkOrderTypeId(TicketCategory.NIDEPLOYMENT);

				logger.debug("@@@@@@@@@@@@@@@@ wo " + wo);

				woResponse = workOrderDAO.addWorkOrder(wo);
			}

			if (woResponse == null)
				return ResponseUtil.createServerErrorResponse().setData("false");
			else
				return ResponseUtil.createSuccessResponse().setData("true");

		} catch (Exception e) {
			logger.error("Error in Creating Prospect for Ticket " + prospectNo + " with mqId " + mqId
					+ " workOrderNumber " + workOrderNumber + ". Message " + e.getMessage());
			// e.printStackTrace();
			return ResponseUtil.createServerErrorResponse().setData("false");
		}
	}

	@Override
	public APIResponse createFrDetailsByProspect(MQWorkorderVO roiMQWorkorderVO, String deviceName) {
		try {

			String prospectNo = roiMQWorkorderVO.getWorkOrderNumber();
			String mqId = null;

			logger.info("Creating fr detail for WorkOrder no. : " + prospectNo);

			FrTicket ticketData = frTicketDao.getFrTicketByWorkOrder(prospectNo);

			if (ticketData == null)
				return ResponseUtil.createSaveFailedResponse();

			FrTicketVo dbvoObject = new FrTicketVo();
			dbvoObject.setTicketId(ticketData.getId());
			if (ticketData.getUserByAssignedTo() != null && ticketData.getUserByAssignedTo().getId() > 0)
				dbvoObject.setUserByAssignedTo(ticketData.getUserByAssignedTo().getId());

			if (ticketData.getUserByAssignedBy() != null && ticketData.getUserByAssignedBy().getId() > 0)
				dbvoObject.setUserByAssignedBy(ticketData.getUserByAssignedBy().getId());

			dbvoObject.setModifiedBy(ticketData.getModifiedBy());
			dbvoObject.setAddedBy(ticketData.getAddedBy());
			dbvoObject.setCategory(roiMQWorkorderVO.getTicketCategory());
			dbvoObject.setSubCategory(roiMQWorkorderVO.getSymptom());
			dbvoObject.setVoc(roiMQWorkorderVO.getVoc());
			dbvoObject.setStatus(FrTicketStatus.OPENED);
			dbvoObject.setWorkOrderNumber(roiMQWorkorderVO.getWorkOrderNumber());

			long deviceId = deviceDao.getDeviceIdByDeviceName(deviceName);
			dbvoObject.setDeviceId(deviceId);

			Long result = frTicketService.createFrDetails(dbvoObject);
			if (result > 0) {
				logger.info("fr detail created successfully...");
				return ResponseUtil.createSuccessResponse().setData("true");

			} else {
				return ResponseUtil.createSaveFailedResponse().setData("false");
			}

		} catch (Exception exception) {
			logger.error("Error occure while adding fr details for prospect no. : ", exception);
			return ResponseUtil.createServerErrorResponse();
		}
	}

	@Override
	public APIResponse updateFrPriority(String workOrderNo, int priority, Integer escalationType,
			Integer escalatedValue) {

		return frTicketService.updateFrPriority(workOrderNo, priority, escalationType, escalatedValue);
	}

	@Override
	public APIResponse updatePriority(String workOrderNo, int priority) {

		logger.info(priority + "*****************priority");
		workOrderDao.updatePriority(workOrderNo, priority, null, null);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			updateWorkOrderActivityInCRM(null, null, null, workOrderNo + "", null, null, null,null);
		}


		return ResponseUtil.createUpdateSuccessResponse();
	}

	public APIResponse updatePriority(String workOrderNo, int priority, Integer escalationType, Integer escalatedValue) {

		workOrderDao.updatePriority(workOrderNo, priority, escalationType, escalatedValue);
		
		String currentCRM = dbUtil.getCurrentCrmName(null, null);

		if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
			
			updateWorkOrderActivityInCRM(null, null, null, workOrderNo + "", null, null, null,null);
		}

		return ResponseUtil.createUpdateSuccessResponse();
	}

	@Override
	public APIResponse updateFiberToCopper(long ticketId, long typeId) {
		int result = workOrderDao.updateFiberToCopper(ticketId, typeId);

		if (result >= 1) {
			return ResponseUtil.createSuccessResponse().setData("Connection type Converted from fiber to copper");
		} else {
			return ResponseUtil.createFailureResponse()
					.setData("Conversion of connection type from fiber to copper failed ");
		}

	}

	@Override
	public Map<Long, Boolean> createWorkOrderInBulk(List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap) {
		Map<Long, Boolean> workOrderCreationStatus = new ConcurrentHashMap<>();

		try {
			int totalTicketSize = roiMQWorkorderVOList.size();

			String prospectNo = null;
			WorkOrderVo wo = null;
			WorkOrder woResponse = null;
			Gis gisData = null;
			
			for (MQWorkorderVO roiMQWorkorderVO : roiMQWorkorderVOList)
			{
				try {
					prospectNo = roiMQWorkorderVO.getProspectNumber();
					long ticketId = 0l;

					if(completeProspectMap.containsKey(prospectNo))
					 ticketId = completeProspectMap.get(prospectNo);
					else
					{
						logger.info("Ticket details not found for workorder " + roiMQWorkorderVO.getWorkOrderNumber());

					
						handleWorkOrderCreationFailed(roiMQWorkorderVO);
						
						continue;
					}
					
					

					logger.info("WO Insetion preparation starterd for ticket " + ticketId + " at " + new Date());

					String mqId = roiMQWorkorderVO.getMqId();
					String workOrderNumber = roiMQWorkorderVO.getWorkOrderNumber();
					String ticketCategory = null;

					boolean result = workOrderDAO.isWOPresentInDB(workOrderNumber);

					logger.info("Wo existance is " + result + " for result " + result);

					if (result)
						continue;

					if (definitionCoreService.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
							.contains(roiMQWorkorderVO.getTicketCategory())
							|| definitionCoreService
									.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_SYMPTOM)
									.contains(roiMQWorkorderVO.getSymptom()))

						ticketCategory = TicketCategory.NEW_INSTALLATION;

					else if (definitionCoreService.getClassificationCategory(DefinitionCoreService.RE_ACTIVATION_CATEG)
							.contains(roiMQWorkorderVO.getTicketCategory())
							|| definitionCoreService
									.getClassificationCategory(DefinitionCoreService.RE_ACTIVATION_SYMPTOM)
									.contains(roiMQWorkorderVO.getSymptom()))

						ticketCategory = TicketCategory.RE_ACTIVATION_STRING;

					else if (definitionCoreService.getClassificationCategory(DefinitionCoreService.SHIFTING_CATEG)
							.contains(roiMQWorkorderVO.getTicketCategory())
							|| definitionCoreService
									.getClassificationCategory(DefinitionCoreService.SHIFTING_CONNECTION_SYMPTOM)
									.contains(roiMQWorkorderVO.getSymptom()))

						ticketCategory = TicketCategory.SHIFTING_STR;

					try
					{
						logger.info("Creating Prospect for Ticket " + prospectNo
								+ " with mqId " + mqId + " workOrderNumber "
								+ workOrderNumber);

						// Ticket ticketData =
						// ticketDao.getTicketByProspectNumber(prospectNo);
						String fxName = null;

						wo = new WorkOrderVo();
						wo.setStatus(TicketStatus.WO_GENERATED);
						wo.setTicketId(ticketId);
						wo.setAddedBy(FWMPConstant.SYSTEM_ENGINE);

						if (ticketCategory != null && ticketCategory
								.equalsIgnoreCase(TicketCategory.NEW_INSTALLATION))
						{
							wo.setWorkOrderTypeId(TicketCategory.NIDEPLOYMENT);

						} else if (ticketCategory != null && ticketCategory
								.equalsIgnoreCase(TicketCategory.FAULT_REPAIR))
						{
							wo.setWorkOrderTypeId(TicketCategory.FR);

						} else {
							wo.setWorkOrderTypeId(0l);
						}

						wo.setAssignedBy(FWMPConstant.SYSTEM_ENGINE); // Auto
																		// Assigned
						wo.setWorkOrderNo(workOrderNumber);
						gisData = null;
						gisData = ticketDao.getGisInfoTicket(ticketId);

						logger.debug("##################################### gisData "
								+ gisData);
						UserVo assignedUser = new UserVo();

						if (gisData != null) {
							
							wo.setGisId(gisData.getId());

							fxName = gisData.getFxName();

							if ((gisData.getWorkstageType() != null
									&& gisData.getWorkstageType().longValue() == WorkStageType.COPPER)
									|| (gisData.getConnectionType() != null
											&& WorkStageName.COPPER.equalsIgnoreCase(gisData.getConnectionType()))) {
								wo.setInitialWorkStageId(WorkStageType.COPPER);
								wo.setCurrentWorkStageId(WorkStageType.COPPER);

							} else if ((gisData.getWorkstageType() != null
									&& gisData.getWorkstageType().longValue() == WorkStageType.FIBER)
									|| (gisData.getConnectionType() != null
											&& WorkStageName.FIBER.equalsIgnoreCase(gisData.getConnectionType()))) {
								wo.setInitialWorkStageId(WorkStageType.FIBER);
								wo.setCurrentWorkStageId(WorkStageType.FIBER);
							}
							logger.debug("GisData for prospect " + prospectNo
									+ " device " + gisData.getFxName());
							
							if (wo.getInitialWorkStageId() != null && wo
									.getInitialWorkStageId() == WorkStageType.FIBER)
							{
								assignedUser = userDao
										.getUsersByDeviceName(fxName, AuthUtils.NE_NI_FIBER);

								if (assignedUser == null)
								{
									assignedUser = userDao
											.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
								}

							} else if (wo.getInitialWorkStageId() != null
									&& wo.getInitialWorkStageId() == WorkStageType.COPPER) {
								assignedUser = userDao.getUsersByDeviceName(fxName, AuthUtils.NE_NI_COPPER);

								if (assignedUser == null) {
									assignedUser = userDao.getUsersByDeviceName(fxName, AuthUtils.NE_NI_DEF);
								}
							}

							if (assignedUser != null)
							{
								logger.debug("Found user for device " + fxName
										+ " User " + assignedUser);
								wo.setAssignedTo(assignedUser.getId());


							} else {
								logger.error("No User Assigned to device " + fxName + " with role " + UserRole.ROLE_NI);
								wo.setStatus(TicketStatus.WO_UNASSIGNED_TICKET);

							}

						} else {
							logger.info("No GIS Data available for Ticket " + ticketId);
							wo.setStatus(TicketStatus.GIS_DATA_NOT_AVAILABLE);
						}
						
						Date etr = null;
						if (wo.getInitialWorkStageId() != null)
							etr = etrCalculator.getETRforWorkStageType(wo.getInitialWorkStageId());

						if (etr == null)
							etr = new Date();
						logger.info("Got etr " + etr + " for mqId " + mqId + " and ticket Id " + ticketId);

						customerDAO.updateCustomerMq(ticketId, mqId, etr);

						wo.setWorkOrderTypeId(TicketCategory.NIDEPLOYMENT);

						logger.debug("@@@@@@@@@@@@@@@@ wo " + wo);

						logger.info("WO Insetion prepared for ticket " + ticketId + " at " + new Date());

						woResponse = workOrderDAO.addWorkOrder(wo);
						
						if(assignedUser != null)
						{
							UserVo assignedBy = new UserVo();
							assignedBy = userDao.getUsersByDeviceName(fxName, AuthUtils.SALES_EX_DEF);

							ticketDao.updateCurrentAssignedTo(ticketId, assignedUser.getId(),
									assignedBy != null ? assignedBy.getId() : null, TicketStatus.WO_GENERATED);
						}

						if (woResponse == null)
							workOrderCreationStatus.put(ticketId, false);
						else
							workOrderCreationStatus.put(ticketId, true);

					} catch (Exception e) {
						logger.error("Error in Creating Prospect for Ticket " + prospectNo + " with mqId " + mqId
								+ " workOrderNumber " + workOrderNumber + ". Message " + e.getMessage());
						e.printStackTrace();
						workOrderCreationStatus.put(ticketId, false);
						continue;
					}

					logger.info("Remaining WO to add are " + totalTicketSize--);

				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Error in wo creation for wo " + roiMQWorkorderVO.getWorkOrderNumber() + " "
							+ e.getMessage());
					continue;
				}
			}

		} catch (Exception e) {
			logger.error("Error in wo creation " + e.getMessage());
			e.printStackTrace();
		}
		return workOrderCreationStatus;
	}

	@Override
	public Map<Long, Boolean> createFrWorkOrderInBulk(List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap) {
		Map<Long, Boolean> result = new ConcurrentHashMap<>();
		if (roiMQWorkorderVOList == null)
			return result;

		for (MQWorkorderVO workOrder : roiMQWorkorderVOList) {
			try {
				Long ticketId = completeProspectMap.get(workOrder.getWorkOrderNumber());
				APIResponse response = createFrDetailsByProspect(workOrder, workOrder.getFxName());
				if ("true".equals(response.getData()))
					result.put(ticketId, true);
				else
					result.put(ticketId, false);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}

		return result;
	}

	@Override
	public Map<Long, Boolean> createFrTicketWorkOrderInBulk(List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap) {
		Map<Long, Boolean> result = new ConcurrentHashMap<>();
		if (roiMQWorkorderVOList == null || completeProspectMap == null)
			return result;

		return frTicketService.createFrDetailInBulk(roiMQWorkorderVOList, completeProspectMap);
	}
	
	private void handleWorkOrderCreationFailed(MQWorkorderVO mqWorkorderVO)
	{

		StringBuilder text = new StringBuilder();
		EMailVO eMail = new EMailVO();
		eMail.setSubject("![WorkOrderCreation FAIL]Workorder Creation Failed  " + mqWorkorderVO.getWorkOrderNumber());

 		text.append("MQWorkorderVO  : " + mqWorkorderVO).append("\n\n");
	
 		eMail.setMessage(text.toString());
		eMail.setTo(failedMqSupportEmailId);
		
		emailNotiFicationHelper.addNewMailToQueue(eMail);
	}
	
	/**
	 * @return the failedMqSupportEmailId
	 */
	public String getFailedMqSupportEmailId() {
		return failedMqSupportEmailId;
	}

	/**
	 * @param failedMqSupportEmailId the failedMqSupportEmailId to set
	 */
	public void setFailedMqSupportEmailId(String failedMqSupportEmailId) {
		this.failedMqSupportEmailId = failedMqSupportEmailId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.service.workOrder.WorkOrderCoreService#
	 * updateWorkOrderActivityInCRM(java.lang.Long, java.lang.Long,
	 * java.lang.String, java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public void updateWorkOrderActivityInCRM(Long ticketId, Long customerId, String prospectNumber,
			String workOrderNumber, Long cityId, Long branchId , String activityStage , String activityId) {
		
		if (workOrderNumber == null && ticketId != null)
			workOrderNumber = workOrderDAO.getWoNumberForTicket(ticketId);
		
		WorkOrderActivityUpdateInCRMVo activityUpdateInCRMVo = new WorkOrderActivityUpdateInCRMVo();
		
		if(ticketId != null )
			activityUpdateInCRMVo.setTicketId(ticketId);
		
		if(customerId != null)
			activityUpdateInCRMVo.setCustomerId(customerId);
		
		if(prospectNumber != null)
			activityUpdateInCRMVo.setProspectNumber(prospectNumber);
		
		if(workOrderNumber != null)
			activityUpdateInCRMVo.setWorkOrderNumber(workOrderNumber);
		
		if(cityId != null)
			activityUpdateInCRMVo.setCityId(cityId);
		
		if(branchId != null)
			activityUpdateInCRMVo.setBranchId(branchId);
		
		if(activityStage != null)
			activityUpdateInCRMVo.setActivityStage(activityStage);
		
		crmUpdateRequestHandler.workOrderActivityUpdateInCRM(activityUpdateInCRMVo , activityId);
		
	}
}
