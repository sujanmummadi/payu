package com.cupola.fwmp.service.workOrderType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.dao.workOrderType.WorkOrderTypeDAO;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.vo.TypeAheadVo;

public class WorkOrderTypeCoreServiceImpl implements WorkOrderTypeCoreService{

	@Autowired
	WorkOrderTypeDAO workOrderTypeDAO;
	
	
	private static final Logger logger = LogManager.getLogger(WorkOrderTypeCoreServiceImpl.class.getName());
	@Override
	public List<TypeAheadVo> getAllWorkOrderTypes() 
	{
		
		if(AuthUtils.isNIUser())
		{
			TypeAheadVo vo = new TypeAheadVo();
			vo.setId(TicketCategory.NIDEPLOYMENT);
			vo.setName(WorkOrderDefinitionCache.getWorkOrderTypes().get(TicketCategory.NIDEPLOYMENT));
			return Collections.singletonList(vo);
		}
		 
		return new ArrayList<TypeAheadVo>();
	}

}
