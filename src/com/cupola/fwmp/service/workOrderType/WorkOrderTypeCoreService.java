package com.cupola.fwmp.service.workOrderType;

import java.util.List;

import com.cupola.fwmp.persistance.entities.WorkOrderType;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.TypeAheadVo;

public interface WorkOrderTypeCoreService {

	List<TypeAheadVo> getAllWorkOrderTypes();

}
