package com.cupola.fwmp.service.activity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.activity.ActivityDAO;
import com.cupola.fwmp.persistance.entities.Activity;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.ActivityVo;

public class ActivityCoreServiceImpl implements ActivityCoreService{

	@Autowired
	ActivityDAO activityDAO;
	
	@Override
	public APIResponse addActivity(Activity activity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getActivityById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActivityVo> getAllActivity() {
		return  activityDAO.getAllActivities() ;
	}

	@Override
	public APIResponse deleteActivity(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateActivity(Activity activity) {
		// TODO Auto-generated method stub
		return null;
	}

}
