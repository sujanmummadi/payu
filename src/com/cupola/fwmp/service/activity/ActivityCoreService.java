package com.cupola.fwmp.service.activity;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Activity;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.ActivityVo;

public interface ActivityCoreService {

	public APIResponse addActivity(Activity activity);

	public APIResponse getActivityById(Long id);

	public List<ActivityVo> getAllActivity();

	public APIResponse deleteActivity(Long id);

	public APIResponse updateActivity(Activity activity);

}
