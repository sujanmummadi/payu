package com.cupola.fwmp.service.notification;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import notification.response.MQResponse;
import notification.service.UserAPI;
import notification.vo.UserDataFromMq;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.MessagePlaceHolder;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.sms.AutoClosureCoreService;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.vo.MessageNotificationVo;

public class NotificationServiceImpl implements NotificationService
{
	static final Logger logger = Logger
			.getLogger(NotificationServiceImpl.class);

	@Autowired
	MailService mailService;

	@Autowired
	UserAPI userAPI;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	AutoClosureCoreService smsService;
	
	@Autowired
	NotificationServiceHelper notificationServiceHelper;

	@Autowired
	DefinitionCoreService coreService;
	

	@Autowired
	CustomerDAO customerDAO;

 

	
	@Override
	public void sendNotification(MessageNotificationVo notificationVo)
	{
		notificationServiceHelper.addNotificationRequest(notificationVo);
	}
	
	@Override
	public APIResponse sendNotificationToCustomer(MessageNotificationVo notificationVo)
	{
		logger.debug("Inside sendNotificationToCustomer " + notificationVo);
		
		try
		{
			// SENDING APP NOTIFICATION
			String customerPhone = notificationVo.getMobileNumber();
			
			if (customerPhone.length() == 12)

				customerPhone = customerPhone.substring(2);

			else if (customerPhone.length() == 13)

				customerPhone = customerPhone.substring(3);

			notificationVo.setMobileNumber(customerPhone);

			UserDataFromMq mq = new UserDataFromMq(notificationVo
					.getMobileNumber(), notificationVo.getMessage(), "1"); // 1 is for general notification
			// SENDING SMS
			APIResponse apiResponse = smsService
					.sendMessageToCustomer(notificationVo);
			
			logger.info("SMS gateway Response after sending SMS to "
					+ notificationVo.getMobileNumber() + " is ====== "
					+ apiResponse);
			
			
			// push notification
			
			logger.info("Sending push notification to "	+ mq + " notificationVo " + notificationVo);
			
			MQResponse response = userAPI.sendNotification(mq);

			logger.info("Meru Service Response after sending push notification to "
					+ notificationVo.getMobileNumber() + " is ====== "
					+ response +"  mq  "+mq);

	

			// SENDING MAIL
			/*if (notificationVo.getEmail() != null
					&& notificationVo.getSubject() != null
					&& notificationVo.getMessage() != null)
			{
				// check all the nulls
				mailService.sendSimpleMail(notificationVo
						.getEmail(), notificationVo
								.getSubject(), notificationVo.getMessage());
			}*/

			if (apiResponse.getStatusCode() == StatusCodes.SUCCESS)
			{

				return ResponseUtil.createSuccessResponse();

			} else
				return ResponseUtil.createFailureResponse();

		} catch (Exception e)
		{
			logger.error("Exception in Sending Notification. Reason ==== "
					+ e.getMessage());
			e.printStackTrace();
			return ResponseUtil.createFailureResponse();
		}
	}

	@Override
	public void sendNotificationToCustomTicketId(long ticketId,
			MessageNotificationVo notificationVo)
	{
		try
		{
			logger.info("Ticket id of sendNotificationToCustomTicketId Customer " + ticketId);

			if (ticketId > 0)
			{

				Ticket tcket = ticketDAO.getTicketById(ticketId);

				if (tcket != null)
				{

					Customer customer = tcket.getCustomer();
					if (customer != null)
					{

						notificationVo.setMobileNumber(customer
								.getMobileNumber());
						notificationVo.setEmail(customer.getEmailId());

						// SENDING APP NOTIFICATION
						UserDataFromMq mq = new UserDataFromMq(customer.getMobileNumber(), notificationVo
								.getMessage(), "1");
						MQResponse response = userAPI.sendNotification(mq);

						logger.info("sendNotificationToCustomTicketId Mq Response after sending notification to "
								+ customer.getMobileNumber()
								+ " is ====== "
								+ response);

						// SENDING SMS
						APIResponse apiResponse = smsService.sendMessageToCustomer(notificationVo);
						logger.info("sendNotificationToCustomTicketId SMS gateway Response after sending SMS to "
								+ notificationVo.getMobileNumber() + " is ====== "
								+ apiResponse);
						// SENDING MAIL
						if (customer.getEmailId() != null)
						{
							mailService
									.sendSimpleMail(customer.getEmailId(), notificationVo
											.getSubject(), notificationVo
											.getMessage());
						}
					}

					else
						logger.info("No customer found with ticket id ======= "
								+ ticketId);
				} else
				{

					logger.info("No ticket found with ticket id ======= "
							+ ticketId);
				}
			}

			else
			{

				logger.info("Ticket id is null");
			}

		} catch (Exception e)
		{

			logger.error("Exception in Sending Notification. Reason ==== "
					+ e.getMessage());
			e.printStackTrace();
		}
	}
	@Override
	public void sendCustomerWorkOrderNotificationByTicketIds(List<BigInteger> ticketIds,
			String statusMessageCode,String workOrderNo)
	{

		String messageToSend = coreService
				.getMessagesForCustomerNotificationFromProperties(statusMessageCode);
		List<Customer> customers = customerDAO
				.getCustomersByTicketIdList(ticketIds);
		
		if (customers != null && !customers.isEmpty())
		{
			for (Customer customer : customers)
			{
				if (customer != null)
				{
					String helpLineNo = 
							coreService.getNIHelpLineNumber(customer.getCity().getCityCode());
				
					if (customer.getTickets() != null)
					{
						Set<Ticket> tickets = customer.getTickets();
						for (Ticket ticket : tickets)
						{
							if(messageToSend != null)
							{
								messageToSend = messageToSend
										.replaceAll(MessagePlaceHolder.WORK_ORDER_NO, workOrderNo)
										.replaceAll(MessagePlaceHolder.MQID, customer
												.getMqId())
										.replaceAll(MessagePlaceHolder.HELP_LINE_NUMBER, helpLineNo)
										.replaceAll("_DATE_", GenericUtil
												.convertToCustomerDateFormat(ticket
														.getCommunicationETR()));

								MessageNotificationVo notificationVo = new MessageNotificationVo();
								notificationVo.setMobileNumber(customer
										.getMobileNumber());
								notificationVo.setEmail(customer.getEmailId());
								notificationVo.setMessage(messageToSend);
								notificationVo.setSubject("ACT Notification");
								sendNotification(notificationVo);

							}
							 break;
						}
					}
				}
			}
		}
	
		
	}

	@Override
	public void sendFeasibilityNotificationByTicketIds(
			List<BigInteger> ticketIds, String statusMessageCode,
			String prospectNo)
	{

		String messageToSend = coreService
				.getMessagesForCustomerNotificationFromProperties(statusMessageCode);
		List<Customer> customers = customerDAO
				.getCustomersByTicketIdList(ticketIds);
		
		if (customers != null && !customers.isEmpty())
		{
			for (Customer customer : customers)
			{
				if (customer != null)
				{
					if (customer.getTickets() != null)
					{
					
							if(messageToSend != null)
								messageToSend = 
								messageToSend.replaceAll(MessagePlaceHolder.PROSPECT_NUMBER, prospectNo);
							
							MessageNotificationVo notificationVo = new MessageNotificationVo();
							notificationVo.setMobileNumber(customer
									.getMobileNumber());
							notificationVo.setEmail(customer.getEmailId());
							notificationVo.setMessage(messageToSend);
							notificationVo.setSubject("ACT Notification");
							 sendNotification(notificationVo);
							 break;
					
					}
				}
			}
		}
	
		
	}
}
