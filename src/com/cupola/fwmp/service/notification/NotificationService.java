package com.cupola.fwmp.service.notification;

import java.math.BigInteger;
import java.util.List;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.MessageNotificationVo;

public interface NotificationService
{

	void sendNotification(MessageNotificationVo notificationVo);
	
	void sendNotificationToCustomTicketId(long ticketId, MessageNotificationVo notificationVo);

	APIResponse sendNotificationToCustomer(MessageNotificationVo notificationVo);

	void sendCustomerWorkOrderNotificationByTicketIds(List<BigInteger> ticketIds,
			String statusMessageCode, String workOrderNo);
	
	void sendFeasibilityNotificationByTicketIds(List<BigInteger> ticketIds,
			String statusMessageCode, String prospectNo);
	
	

}
