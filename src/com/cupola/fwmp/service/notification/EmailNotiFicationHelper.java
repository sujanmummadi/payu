package com.cupola.fwmp.service.notification;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.mail.MailService;

public class EmailNotiFicationHelper
{
	static final Logger logger = Logger.getLogger(EmailNotiFicationHelper.class);
	BlockingQueue<EMailVO> eMailQueue;
	Thread updateProcessorThread = null;
	
	@Autowired
	MailService mailService;
	
	public void init()
	{
		logger.info("EmailNotiFicationHelper enabling updateProcessorThread processing-");
		eMailQueue = new LinkedBlockingQueue<EMailVO>(100000);

		if (updateProcessorThread == null)
		{
			updateProcessorThread = new Thread(run, "EmailNotiFicationHelper");
			updateProcessorThread.start();
		}
		logger.info("EmailNotiFicationHelper updateProcessorThread enabled");
	}

	public void addNewMailToQueue(EMailVO eMail)
	{
		try
		{

			 
				logger.info("Adding to Queue  mail to "+  eMail.getTo());
				
				eMailQueue.add(eMail);

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("error" + e.getMessage());
		}
	}

	Runnable run = new Runnable()
	{

		public void run()
		{
			try
			{
				EMailVO eMail = null;

				while ((eMail = eMailQueue.take()) != null)
				{

						try
						{
							// Start processing from here
							if(eMail != null  )
							{
								sendEmail(eMail);
							}
						} catch (Exception e)
						{
							
							e.printStackTrace();
							logger.error("Error in eMail thread "+e.getMessage());
							continue;
						}
				}

			} catch (Exception e)
			{
				logger.error("error in Processor main loop- msg="
						+ e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor thread interrupted- getting out of processing loop");

		}

		/**
		 * @param eMail
		 * @throws Exception
		 */
		private void sendEmail(EMailVO eMail)
				throws Exception
		{
			logger.info("Creating eMail for  "+  eMail.getTo());
			mailService.sendSimpleMail(eMail.getTo(), eMail.getSubject(), eMail.getMessage());
			
		}

	};
}
