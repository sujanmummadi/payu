package com.cupola.fwmp.service.notification;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.FlowActionType;
import com.cupola.fwmp.dao.fr.FrTicketDetailsDao;
import com.cupola.fwmp.dao.materials.MaterialsDAO;
import com.cupola.fwmp.dao.materials.MaterialsDAOImpl;
import com.cupola.fwmp.dao.mongo.material.MaterialRequestDAO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.persistance.entities.FrMaterialDetail;
import com.cupola.fwmp.persistance.entities.TicketMaterialMapping;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.MessageNotificationVo;
import com.cupola.fwmp.vo.fr.FlowActionData;
import com.cupola.fwmp.vo.fr.FrEmailVo;
import com.cupola.fwmp.vo.fr.FrTicketDetailsVO;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;
import com.cupola.fwmp.vo.fr.FrTicketMaterialMappingVo;
import com.cupola.fwmp.vo.fr.MaterialRequestVo;

public class FrEmailNotificationHelper
{
	private Logger LOGGER = Logger.getLogger(FrEmailNotificationHelper.class);
	
	@Autowired 
	private FrTicketDetailsDao frTicketDetailsDao; 
	
	@Autowired
	private MaterialRequestDAO materialRequestDAO;
	
	@Autowired
	private MaterialsDAO materialsDAO;
	
	@Autowired
	private MailService mailService;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	private String borSub = "Material Request & Consumption";
	
	@Value("${fwmp.noc.team.subject}")
	private String nocSub ;
	public final static String EMAIL_BOR_KEY = "bor.dept.emailIds.";
	
	public final static String EMAIL_NOC_KEY = "noc.dept.emailIds.";
	
	private BlockingQueue<FrTicketEtrDataVo> NotifyingQ = new LinkedBlockingQueue<FrTicketEtrDataVo>(100000);
	private Thread NotifyThread = null;

	
	public APIResponse sendNotificationToActionType(FlowActionData actionData)
	{
		if( actionData == null )
			return null;
		
		if( actionData.getActionType() == FlowActionType.SUBMIT_BOR )
		{
//			return this.sendNotificationToBOR( actionData.getTicketId() );
			return this.sendMaterialNotificationToBOR( actionData.getTicketId() );
		}
		else if(actionData.getActionType() == FlowActionType.TRIGGER_MAIL_FOR_NOC)
			return this.sendNotificationToNOC( actionData.getTicketId() );
		else
			return ResponseUtil.createFailureResponse();
	}

	/**@author kiran
	 * APIResponse
	 * @param ticketId
	 * @return
	 */
	private APIResponse sendNotificationToNOC(long ticketId)
	{
		//String sub = "New Legal Team Request";

		Map cc = null;
		Map bcc = null;
		String templateName = "noc_team_report.vm";
		
		TicketFilter filter = new TicketFilter();
		
		filter.setTicketId(ticketId);
		List<FrTicketDetailsVO> ticket = frTicketDetailsDao.getFrTicketWithDetails(filter);
		if(ticket == null || ticket.isEmpty())
		{
			LOGGER.info("No ticket found for " + ticketId);
			return ResponseUtil.NoRecordFound();
		}
		FrTicketDetailsVO vo = ticket.get(0);
		String cityCode =  vo.getCity();
		String to = DefinitionCoreServiceImpl.emailCorporateDef
				.get(EMAIL_NOC_KEY + cityCode.toLowerCase());
		
		LOGGER.info("Sending mail to :"+to);
	
		LOGGER.info("Sending mail  with ticket id :"+vo.getTicketId());
		mailService
				.sendMiMeMail(nocSub, to, cc, bcc, templateName, vo);
		LOGGER.info("mail send successfully with ticket id :"+vo.getTicketId());
		return ResponseUtil.createSuccessResponse();

	}

	private APIResponse sendNotificationToBOR( long ticketId ) 
	{
		Map cc = null;
		Map bcc = null;
		FrEmailVo frEmailVo = new FrEmailVo();
		List<FrTicketMaterialMappingVo>  frTicketMaterialMappingVos = new ArrayList<FrTicketMaterialMappingVo>();
		String templateName = "bor_report.vm";
		
		TicketFilter filter = new TicketFilter();
		
		filter.setTicketId(ticketId);
		
		List<MaterialRequestVo> requestedMatearialDetails = materialRequestDAO.getMaterialRequestDetailsByTicketIdWithNotModefied(ticketId);
		List<Integer> materialIds = new ArrayList<Integer>();
		for (MaterialRequestVo materialRequestVo : requestedMatearialDetails)
		{
			if(materialRequestVo.getMaterialRequestId() != null)
			materialIds.add(materialRequestVo.getMaterialRequestId());
			
		}
	List<TicketMaterialMapping> cunsumedMaterials = 	materialsDAO.getMaterialsByTicketIDAndMaterailIds(ticketId, materialIds);
	for (TicketMaterialMapping ticketMaterialMapping : cunsumedMaterials)
	{
		FrTicketMaterialMappingVo mappingVo = new FrTicketMaterialMappingVo();
		BeanUtils.copyProperties(ticketMaterialMapping, mappingVo);
		mappingVo.setMaterialName(MaterialsDAOImpl.cachedMaterialsById.get(mappingVo.getMaterialId()));
		frTicketMaterialMappingVos.add(mappingVo);
	}
	List<FrTicketDetailsVO> ticket = frTicketDetailsDao.getFrTicketWithDetails(filter);
		if(ticket == null || ticket.isEmpty())
		{
			LOGGER.info("No ticket found for " + ticketId);
			return ResponseUtil.NoRecordFound();
		}
		FrTicketDetailsVO vo = ticket.get(0);
		BeanUtils.copyProperties(vo, frEmailVo);
		
		frEmailVo.setConsuptiomMaterials(frTicketMaterialMappingVos);
	
		
		String cityCode =  frEmailVo.getCity();
		
		String to = DefinitionCoreServiceImpl.emailCorporateDef
				.get(EMAIL_BOR_KEY + cityCode.toLowerCase());
		
		LOGGER.info("Sending mail to :"+to);
	
		LOGGER.info("Sending mail  with ticket id :"+vo.getTicketId());
		mailService
				.sendMiMeMail(borSub, to, cc, bcc, templateName, frEmailVo);
		materialRequestDAO.updateMaterialRequestDetailsByTicketIdWithtModefiedStatus(ticketId, materialIds);
		LOGGER.info("mail send successfully with ticket id :"+vo.getTicketId());
		return ResponseUtil.createSuccessResponse();
	}
	
	private  FrTicketMaterialMappingVo xformEmailVo( FrMaterialDetail details)
	{
		if( details == null )
			return null;
		
		FrTicketMaterialMappingVo mappingVo = new FrTicketMaterialMappingVo();
		
		mappingVo.setTicketId(Long.valueOf(details.getTicket()));
		mappingVo.setMaterialId(Integer.valueOf(details.getMaterialId()));
		mappingVo.setTotalConsumption(details.getMaterialConsumedQty());
		
		mappingVo.setMacid(details.getMaterialUniqNumber());
		mappingVo.setSerialNo(details.getMaterialUniqNumber());
		
		mappingVo.setUoMid( details.getMaterialUnit() );
		mappingVo.setDrumNo(details.getMaterialDrumNumber());
		mappingVo.setFiberType(details.getMaterialFiberType());
		
		String addedBy = details.getConsumptionAddedBy();
		if( addedBy != null && !addedBy.isEmpty())
			mappingVo.setAddedBy(Long.valueOf(details.getConsumptionAddedBy()));
		
		mappingVo.setAddedOn(details.getConsumptionAddedOn());
		
		addedBy = details.getConsumptionModifiedBy();
		if( addedBy != null && !addedBy.isEmpty())
			mappingVo.setModifiedBy(Long.valueOf(details.getConsumptionModifiedBy()));
		mappingVo.setModifiedOn(details.getConsumptionModifiedOn());
		
		addedBy = details.getMaterialStartEndPoint();
		if( addedBy != null && !addedBy.isEmpty())
		{
			String[] startEnd = addedBy.split(",");
			if( startEnd != null && startEnd.length == 2 )
			{
				String[] wireValue = startEnd[0].split("=");
				if( wireValue != null && wireValue.length == 2)
					mappingVo.setStartPoint(wireValue[1]);
				
				wireValue =  startEnd[1].split("=");
				if( wireValue != null && wireValue.length == 2)
					mappingVo.setEndPoint(wireValue[1]);
			}
		}
		return mappingVo;
	}
	
	private APIResponse sendMaterialNotificationToBOR( Long ticketId )
	{
		if ( ticketId == null || ticketId.longValue() == 0 )
			return ResponseUtil.nullArgument();
		
		try
		{
			Map cc = null;
			Map bcc = null;
			FrEmailVo frEmailVo = new FrEmailVo();
			List<FrTicketMaterialMappingVo>  frTicketMaterialMappingVos = new ArrayList<FrTicketMaterialMappingVo>();
			String templateName = "bor_report.vm";
			
			TicketFilter filter = new TicketFilter();
			filter.setTicketId(ticketId);
			
					
			List<FrMaterialDetail> cunsumedMaterials = materialsDAO.getMaterialsDetailByFrTicket( ticketId, 
					FRStatus.NOT_MODEFIED+"");
			
			FrTicketMaterialMappingVo mappingVo = null;
			
			for ( FrMaterialDetail ticketMaterialMapping : cunsumedMaterials)
			{
				mappingVo = xformEmailVo( ticketMaterialMapping );
				mappingVo.setMaterialName(MaterialsDAOImpl.cachedMaterialsById.get(mappingVo.getMaterialId()));
				frTicketMaterialMappingVos.add(mappingVo);
			}
			List<FrTicketDetailsVO> ticket = frTicketDetailsDao.getFrTicketWithDetails(filter);
			if(ticket == null || ticket.isEmpty())
			{
				LOGGER.info("No ticket found for " + ticketId);
				return ResponseUtil.NoRecordFound();
			}
			
			FrTicketDetailsVO vo = ticket.get(0);
			BeanUtils.copyProperties(vo, frEmailVo);
			
			frEmailVo.setConsuptiomMaterials(frTicketMaterialMappingVos);
			String cityCode =  frEmailVo.getCity();
			
			String to = DefinitionCoreServiceImpl.emailCorporateDef
					.get(EMAIL_BOR_KEY + cityCode.toLowerCase());
			
			LOGGER.info("Sending mail to :"+to);
			LOGGER.info("Sending mail  with ticket id :"+vo.getTicketId());
			
			mailService
					.sendMiMeMail(borSub, to, cc, bcc, templateName, frEmailVo);
			
			materialsDAO.updateFrMaterialStatus( ticketId );
			
			LOGGER.info("mail send successfully with ticket id :"+vo.getTicketId());
			return ResponseUtil.createSuccessResponse();
		
		}
		catch(Exception e)
		{
			LOGGER.info("Error occure while sending mail for material"
					+ " request and consumption of ticket :"+ticketId);
			e.printStackTrace();
		}
		return ResponseUtil.createSuccessResponse();
	}
	
	public void putInFrNotifyQ( FrTicketEtrDataVo etrDataVo )
	{
		if( etrDataVo == null )
			return;
		
		try
		{
			if( NotifyThread == null )
			{	
				NotifyThread = new Thread( notify, "Notifier");
				NotifyThread.start();
			}
			
			NotifyingQ.put(etrDataVo);
		}
		catch(Exception e){
			LOGGER.info("Error occured while putting in NotifyQ to customer ...",e);
			e.printStackTrace();
		}
	}
	
	public void putInFrNotifyQ( Set<FrTicketEtrDataVo> etrDataVo )
	{
		if( etrDataVo == null || etrDataVo.isEmpty() )
			return;
		
		try
		{
			if( NotifyThread == null )
			{	
				NotifyThread = new Thread( notify, "Notifier");
				NotifyThread.start();
			}
			
			for( FrTicketEtrDataVo vo : etrDataVo)
			{
				NotifyingQ.put(vo);
			}
			
		}catch(Exception e){
			LOGGER.info("Error occured while putting in NotifyQ to customer ...");
		}
	}
	
	Runnable notify = new Runnable()
	{
		@Override
		public void run()
		{
			try
			{
				ExecutorService executor = Executors.newFixedThreadPool(25);
				while (true)
				{
					try
					{
						final FrTicketEtrDataVo etrData  = NotifyingQ.take();
						Runnable independClosure = new Runnable()
						{
							@Override
							public void run()
							{ 
								try
								{
									if( etrData != null )
									{
										LOGGER.info("Thread : "+Thread.currentThread().getName() 
												+" closing work order");
										
										sendSMSToCustomer(  etrData , etrData.getCommittedEtr() );
									}
								}
								catch (Exception e) 
								{
									LOGGER.error("Error occured While Notifying Etr for  work order : "
											,e );
								}
							}
						};
						
						executor.execute( independClosure );
					} 
					catch (Exception e)
					{
						LOGGER.error("Error While Notifying Etr for workOrder : "
								+ e.getMessage());
						continue;
					}
				}
			} 
			catch (Exception e)
			{
				LOGGER.error("error in Notifying main loop- msg="
						+ e.getMessage());
			}
			LOGGER.info("Notifying done ...");
		}
	};
	
	int count = 0;
	public void sendSMSToCustomer( FrTicketEtrDataVo etrDataVo , Date committedEtr )
	{
		if( etrDataVo == null || etrDataVo.getMobileNumber() == null 
				|| etrDataVo.getMobileNumber().isEmpty()
				|| committedEtr == null )
			
			return;
		
		try
		{
			String statusMessageCode = definitionCoreService
					.getNotificationMessageFromProperties("50000");

			String messageToSend = definitionCoreService
					.getMessagesForCustomerNotificationFromProperties(statusMessageCode);
			
			MessageNotificationVo notificationVo = new MessageNotificationVo();
			
			messageToSend = MessageFormat.format( messageToSend, etrDataVo.getWorkOrderNumber()
					,committedEtr.toString());
			
			notificationVo.setMobileNumber(etrDataVo.getMobileNumber());
			notificationVo.setMessage(messageToSend);
	//		notificationVo.setSubject("Testing Message");
			
			notificationService.sendNotification( notificationVo );
			
		} 
		catch (Exception e)
		{
			LOGGER.error("Error occure while sending SMS to customer ...",e);
			e.printStackTrace();
		}
		

		/*
			CustomerVO customer = customerDAO.getCustomerByTicketId(Long.valueOf(notificationVo.getTicketId()));
			String helpLineNo = 
					coreService.getNIHelpLineNumber(
							LocationCache.getCityCodeById(customer.getCityId()));
			String portalUrl = coreService
					.getPortalPageUrl(LocationCache
							.getCityCodeById(customer.getCityId()));
			messageToSend = messageToSend
					.replaceAll(MessagePlaceHolder.HELP_LINE_NUMBER, helpLineNo)
					.replaceAll(MessagePlaceHolder.PORTAL_URL, portalUrl);
			notificationVo.setMessage(messageToSend);
		  	notificationService.sendNotification(notificationVo);
			return ResponseUtil.createSuccessResponse();
		 */
	
	
	}

}
