package com.cupola.fwmp.service.notification;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.vo.MessageNotificationVo;

public class NotificationServiceHelper
{

	static final Logger logger = Logger
			.getLogger(NotificationServiceHelper.class);
	BlockingQueue<MessageNotificationVo> notificationQueue;
	Thread updateProcessorThread = null;

	@Autowired
	NotificationService notificationService;

	@Autowired
	TicketActivityLogDAO activityLogDAO;

	public void init()
	{
		logger.info("NotificationServiceHelper enabling updateProcessorThread processing-");
		notificationQueue = new LinkedBlockingQueue<MessageNotificationVo>(100000);

		if (updateProcessorThread == null)
		{
			updateProcessorThread = new Thread(run, "NotificationServiceHelper Service");
			updateProcessorThread.start();
		}
		logger.info("NotificationServiceHelper updateProcessorThread enabled");
	}

	public void addNotificationRequest(MessageNotificationVo notificationVO)
	{
		try
		{

			 
				logger.info("Adding new notificationVO  request for customer "
						+ notificationVO.getMobileNumber());

				notificationQueue.add(notificationVO);
			 

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("error" + e.getMessage());
		}
	}

	Runnable run = new Runnable()
	{

		public void run()
		{
			try
			{
				MessageNotificationVo notificationVO = null;

				while ((notificationVO = notificationQueue.take()) != null)
				{

						try
						{
							// Start processing from here
							if (notificationVO != null)
							{
								logger.debug(" Current Processing notification  :: "
										+ notificationVO);

								// Create Prospect
								notifyToCustomer(notificationVO);
							}
						} catch (Exception e)
						{

							e.printStackTrace();
							logger.error("Error in notificationVO  thread "
									+ e.getMessage());
							continue;
						}
				}

			} catch (Exception e)
			{
				logger.error("error in Processor main loop- msg="
						+ e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor thread interrupted- getting out of processing loop");

		}

		/**
		 * @param notificationVO
		 * @throws Exception
		 */
		private void notifyToCustomer(MessageNotificationVo notificationVO)
				throws Exception
		{
			if (notificationVO != null)
			{
				try
				{
					logger.info("Sending notification to customer "
							+ notificationVO);
					APIResponse apiResponse = notificationService
							.sendNotificationToCustomer(notificationVO);

					if (apiResponse.getStatusCode() == StatusCodes.SUCCESS)
					{

						if (notificationVO.getTicketId() != null
								&& notificationVO.getActivityId() != null)
						{
							long ticketId = Long
									.valueOf(notificationVO.getTicketId());

							long activityId = Long
									.valueOf(notificationVO.getActivityId());

							if (activityId == Activity.CUSTOMER_ACCOUNT_ACTIVATION)
							{
								int result = activityLogDAO
										.deleteFromTicketActivityLog(ticketId, activityId);
								
								Log.info("Deleted CUSTOMER_ACCOUNT_ACTIVATION for ticket id "
										+ ticketId + " count = " + result);
							}

						}

					}

				} catch (Exception e)
				{
					logger.error("Error in Sending notification to customer "
							+ notificationVO);
					e.printStackTrace();
				}
			} else
			{
				return;
			}

		}

	};

}
