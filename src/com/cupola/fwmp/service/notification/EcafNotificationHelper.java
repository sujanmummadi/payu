 /**
 * @Author kiran  Dec 11, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.notification;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.vo.ProspectCoreVO;

/**
 * @Author kiran  Dec 11, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class EcafNotificationHelper {
	

	static final Logger logger = Logger.getLogger(EcafNotificationHelper.class);
	BlockingQueue<EMailVO> eMailQueue;
	Thread updateProcessorThread = null;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	DefinitionCoreService definitionCoreService;
	
	public void init()
	{
		logger.info("EmailNotiFicationHelper enabling updateProcessorThread processing-");
		eMailQueue = new LinkedBlockingQueue<EMailVO>(100000);

		if (updateProcessorThread == null)
		{
			updateProcessorThread = new Thread(run, "EcafNotificationHelper");
			updateProcessorThread.start();
		}
		logger.info("EmailNotiFicationHelper updateProcessorThread enabled");
	}

	public void addNewMailToQueue(EMailVO eMail)
	{
		try
		{

			 
				logger.info("Adding to Queue  mail to "+  eMail.getTo());
				
				eMailQueue.add(eMail);

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("error" + e.getMessage());
		}
	}

	Runnable run = new Runnable()
	{

		public void run()
		{
			try
			{
				String threadPoolSizeString = definitionCoreService
						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_DEVICE_CACHE_THREAD_POOL_SIZE);

				ExecutorService executor = Executors
						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
								? Integer.valueOf(threadPoolSizeString)
								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

				while (true)
				{
					try
					{
						final EMailVO eMail = eMailQueue.take();

						Runnable worker = new Runnable()
						{
							@Override
							public void run()
							{
								try
								{
									// Start processing from here
									if (eMail != null && eMail.getTo() != null)
									{
										logger.info(eMail.getTo()
												+ ", Current Processing email and current depth after processing is "
												+ eMailQueue.size());

										// Create Prospect
										sendEmail(eMail);

									} else
									{
										logger.info("Email is null or whom to sent is empty");
									}

								} catch (Exception e)
								{
									e.printStackTrace();
									logger.error("Error in prospect thread "
											+ " " + eMail + ""
											+ e.getMessage());
								}

							}
						};

						executor.execute(worker);
						// Thread.sleep(10);
					} catch (Exception e)
					{
						e.printStackTrace();

						logger.error("Error in eMail main thread "
								+ e.getMessage());
						continue;
					}

				}

			} catch (Exception e)
			{
				logger.error("error in Processor main loop- msg="
						+ e.getMessage());
				e.printStackTrace();
			}

			logger.info("Processor thread interrupted- getting out of processing loop");
		
		}

		/**
		 * @param eMail
		 * @throws Exception
		 */
		private void sendEmail(EMailVO eMail)
				throws Exception
		{
			logger.info("Creating eMail for  "+  eMail.getTo());
			mailService.sendSimpleMailWithAttachment(eMail.getTo(), eMail.getSubject(), eMail.getMessage(),
					eMail.getDocName(), eMail.getAbsolutePath());

		}

	};


}
