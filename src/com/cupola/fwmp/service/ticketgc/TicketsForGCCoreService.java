/**
 * 
 */
package com.cupola.fwmp.service.ticketgc;

import com.cupola.fwmp.response.APIResponse;

/**
 * @author aditya
 * 
 */
public interface TicketsForGCCoreService
{
	APIResponse getSalesTicketsForGC();

	APIResponse getDeploymentTicketsForGC();
	
	APIResponse removeDeploymentTicketFromQueue();
	
	APIResponse removeSalesTicketFromQueue();

	APIResponse removeFrTicketFromQueue();
}
