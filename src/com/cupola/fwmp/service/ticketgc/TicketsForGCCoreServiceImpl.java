/**
 * 
 */
package com.cupola.fwmp.service.ticketgc;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.ticketgc.TicketsForGCDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StatusMessages;
import com.cupola.fwmp.vo.TicketsForGC;

/**
 * @author aditya
 * 
 */
public class TicketsForGCCoreServiceImpl implements TicketsForGCCoreService
{

	private static final Logger log = LogManager
			.getLogger(TicketsForGCCoreServiceImpl.class);

	@Autowired
	TicketsForGCDAO ticketsForGCDAO;

	@Autowired
	DBUtil dbUtil;

	@Override
	public APIResponse getSalesTicketsForGC()
	{
		List<TicketsForGC> ticketsForGCs = ticketsForGCDAO
				.getSalesTicketsForGC();

		log.debug("ticketsForGCDAO.getSalesTicketsForGC() " + ticketsForGCs);
		return ResponseUtil.createSuccessResponse().setData(ticketsForGCs);
	}

	@Override
	public APIResponse getDeploymentTicketsForGC()
	{
		List<TicketsForGC> ticketsForGCs = ticketsForGCDAO
				.getDeploymentTicketsForGC();

		log.debug("ticketsForGCDAO.getDeploymentTicketsForGC() " + ticketsForGCs);
		return ResponseUtil.createSuccessResponse().setData(ticketsForGCs);
	}

	@Override
	public APIResponse removeDeploymentTicketFromQueue()
	{
		List<TicketsForGC> ticketsForGCs = ticketsForGCDAO
				.getDeploymentTicketsForGC();

		

		boolean removableFlag = false;

		if (ticketsForGCs != null && !ticketsForGCs.isEmpty())
		{
			log.info("Total deployment ticket eligible for GC are "
					+ ticketsForGCs.size()
					+ "\n Details of deployment tickets are " + ticketsForGCs.size());
			
			for (Iterator<TicketsForGC> iterator = ticketsForGCs.iterator(); iterator
					.hasNext();)
			{
				TicketsForGC ticketsForGC = (TicketsForGC) iterator.next();

				if (ticketsForGC.getTicketStatus() > 0
						&& ticketsForGC.getTicketStatus() == TicketStatus.ACTIVATION_COMPLETED_VERIFIED)
				{
					removableFlag = dbUtil
							.removeOneTicketFromQueue(ticketsForGC
									.getCurrentAssignedTo(), ticketsForGC
									.getTicketId());
				}
			}
		}

		if (removableFlag)
		{
			return ResponseUtil.createTicketRemoveFromQueueSuccessResponse()
					.setData(StatusMessages.TICKET_REMOVE_FROM_QUEUE_SUCCESS);
		} else
		{
			return ResponseUtil.createTicketRemoveFromQueueFailResponse()
					.setData(StatusMessages.TICKET_REMOVE_FROM_QUEUE_FAILED);
		}
	}

	@Override
	public APIResponse removeSalesTicketFromQueue()
	{
		List<TicketsForGC> ticketsForGCs = ticketsForGCDAO
				.getSalesTicketsForGC();

		
		boolean removableFlag = false;

		if (ticketsForGCs != null && !ticketsForGCs.isEmpty())
		{
			log.info("Total SALES ticket eligible for GC are "
					+ ticketsForGCs.size() + "\n Details of sales tickets are "
					+ ticketsForGCs.size());

			
			for (Iterator<TicketsForGC> iterator = ticketsForGCs.iterator(); iterator
					.hasNext();)
			{
				TicketsForGC ticketsForGC = (TicketsForGC) iterator.next();

				if (ticketsForGC.getPaymentStatus() > 0
						&& ticketsForGC.getPaymentStatus() == TicketStatus.PAYMENT_APPROVED
						&& ticketsForGC.getDocumentStatus() > 0
						&& ticketsForGC.getDocumentStatus() == TicketStatus.POI_DOCUMENTS_APPROVED
						&& ticketsForGC.getDocumentStatus() == TicketStatus.POA_DOCUMENTS_APPROVED)
				{
					removableFlag = dbUtil
							.removeOneTicketFromQueue(ticketsForGC
									.getCurrentAssignedTo(), ticketsForGC
									.getTicketId());
				}
			}
		}

		if (removableFlag)
		{
			return ResponseUtil.createTicketRemoveFromQueueSuccessResponse()
					.setData(StatusMessages.TICKET_REMOVE_FROM_QUEUE_SUCCESS);
		} else
		{
			return ResponseUtil.createTicketRemoveFromQueueFailResponse()
					.setData(StatusMessages.TICKET_REMOVE_FROM_QUEUE_FAILED);
		}
	}

	@Override
	public APIResponse removeFrTicketFromQueue()
	{
		List<TicketsForGC> ticketsForGCs = ticketsForGCDAO
				.getFrTicketsForGC();

		
		boolean removableFlag = false;

		if (ticketsForGCs != null && !ticketsForGCs.isEmpty())
		{
			log.info("Total FR ticket eligible for GC are "
					+ ticketsForGCs.size() );

			for (Iterator<TicketsForGC> iterator = ticketsForGCs.iterator(); iterator
					.hasNext();)
			{
				TicketsForGC ticketsForGC = (TicketsForGC) iterator.next();

				if ( ticketsForGC.getTicketId() > 0 
						&& ticketsForGC.getCurrentAssignedTo() > 0 )
				{
					removableFlag = dbUtil
							.removeOneFrTicketFromQueue(ticketsForGC
									.getCurrentAssignedTo(), ticketsForGC
									.getTicketId());
				}
			}
		}

		if ( removableFlag )
		{
			return ResponseUtil.createTicketRemoveFromQueueSuccessResponse()
					.setData(StatusMessages.TICKET_REMOVE_FROM_QUEUE_SUCCESS);
		} 
		else
		{
			return ResponseUtil.createTicketRemoveFromQueueFailResponse()
					.setData(StatusMessages.TICKET_REMOVE_FROM_QUEUE_FAILED);
		}
	}
}
