package com.cupola.fwmp.service.userCaf;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.userCaf.UserCafDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.CafDetailsVo;
import com.cupola.fwmp.vo.ReassignCAF;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserCafResponseVo;
import com.cupola.fwmp.vo.UserCafVo;
import com.cupola.fwmp.vo.UserVo;

public class UserCafCoreServiceImpl implements UserCafCoreService
{

	final static Logger log = Logger.getLogger(UserCafCoreServiceImpl.class);

	@Autowired
	UserCafDAO userCafDAO;
	@Autowired
	UserDao userDao;
	@Override
	public APIResponse saveAndUpdateUserCAF(UserCafVo userCafVo)
	{

		Long userId = AuthUtils.getCurrentUserId();

		if (userCafVo == null)
		{
			return ResponseUtil.createFailureResponse()
					.setData("found UserCaf null");
		}

		if (userCafVo.getUserId() == null)
		{
			return ResponseUtil.createFailureResponse()
					.setData("found userId null");
		}

		if (userCafDAO.updateUserCAF(userCafVo, userId) > 0)

		{
			return ResponseUtil.createUpdateSuccessResponse()
					.setData("userCaf updated successfuly");
		}
		int addedUserCaf = userCafDAO.addUserCAF(userCafVo, userId);

		if (addedUserCaf == -1)
		{
			return ResponseUtil.createDBExceptionResponse()
					.setData("Not found user in UserTables as userId :"
							+ userCafVo.getUserId());
		}

		if (addedUserCaf <= 0)
		{
			return ResponseUtil.createSaveFailedResponse();
		}

		return ResponseUtil.createSaveSuccessResponse()
				.setData("data adeed in userCaf");
	}

	@Override
	public APIResponse deleteUserCAF(UserCafVo userCafVo)
	{
		int row = userCafDAO.deleteUserCAF(userCafVo);

		if (userCafVo == null)
		{
			return ResponseUtil.createFailureResponse()
					.setData("found UserCaf null");
		}

		if (userCafVo.getUserId() == null)
		{
			return ResponseUtil.createFailureResponse()
					.setData("found userId null");
		}

		if (row <= 0)
		{
			return ResponseUtil.createDeleteFailedResponse()
					.setData("No record found as userId:"
							+ userCafVo.getUserId());
		}

		return ResponseUtil.createDeleteSuccessResponse()
				.setData("data deleted from  userCafVo as userId:"
						+ userCafVo.getUserId());
	}

	@Override
	public APIResponse getUserCAFByUserId(UserCafVo userCafVo)
	{
		if (userCafVo == null)
		{
			return ResponseUtil.createFailureResponse()
					.setData("found UserCaf null");
		}
		
		UserCafResponseVo caf = userCafDAO.getUserCAFByUserId(userCafVo
				.getUserId());

		

		if (userCafVo.getUserId() == null)
		{
			return ResponseUtil.createFailureResponse()
					.setData("found userId null");

		}

		if (caf == null)
		{
			return ResponseUtil.createRecordNotFoundResponse();

		}

		return ResponseUtil.createSuccessResponse().setData(caf);
	}

	@Override
	public APIResponse getListUserCAF()
	{
		List<UserCafResponseVo> caf = userCafDAO.getListUserCAF();

		if (caf == null || caf.isEmpty())
		{
			return ResponseUtil.createRecordNotFoundResponse();
		}
		return ResponseUtil.createSuccessResponse().setData(caf);
	}

	@Override
	public APIResponse getUserCAFNumberByUserId(UserCafVo userCafVo)
	{
		List<Integer> cafNumberDetails = userCafDAO
				.getUserCAFNumberByUserId(userCafVo);
		if (cafNumberDetails == null || cafNumberDetails.isEmpty())
		{
			return ResponseUtil.createRecordNotFoundResponse()
					.setData(cafNumberDetails);
		}
		return ResponseUtil.createSuccessResponse().setData(cafNumberDetails);
	}

	@Override
	public APIResponse addCafDetails(List<CafDetailsVo> userCafVo)
	{
		Long assignedUser = AuthUtils.getCurrentUserId();
		List<String> failedList = null;
		try
		{

			if (userCafVo == null || userCafVo.isEmpty())
			{
				log.info("found CafDetailsVo null");
				return ResponseUtil.nullArgument();
			}
			log.debug("Total caf for user " + userCafVo.get(0).getUserId() + " assigned by " + assignedUser + " are " + userCafVo.size());
			failedList = userCafDAO.addCafDetails(userCafVo, assignedUser);
			//
			// for (CafDetailsVo vo : userCafVo)
			// {
			// if (vo.getCafNumber() == null)
			// {
			// log.info("found CafNumber null" + vo);
			// return ResponseUtil.nullArgument()
			// .setData("found cafNumber null");
			// }
			//
			// if (vo.getStatus() == null)
			// {
			// log.info("found status null" + vo);
			// return ResponseUtil.nullArgument().setData("found status null");
			// }
			// if (vo.getUserId() == null)
			// {
			// log.info("found userId null" + vo);
			// return ResponseUtil.nullArgument().setData("found userId null");
			// }
			//
			//
			//
			// list.add(vo.getCafNumber());
			//
			// }
			// if (row <= 0)
			// {
			// log.info("failed to insert data in UserCaf ");
			// return ResponseUtil.createSaveFailedResponse();
			// }

			log.info(" data inserted  in UserCaf successfully as userId:");

		} catch (Exception e)
		{
			log.error("Error while adding caf details "+e.getMessage());
			e.printStackTrace();
		}
		return ResponseUtil.createSaveSuccessResponse().setData(failedList);

	}

	@Override
	public APIResponse gerCafDetailsByUserId()
	{
		Long userId = AuthUtils.getCurrentUserId();

		if (userId == null)
		{
			log.info("found userId null");
			return ResponseUtil.nullArgument();
		}

		List<String> listOfCafnum = userCafDAO.getCafDetailsByUserId(userId);

		if (listOfCafnum == null)
		{
			log.info("not found cafNumber as userId" + userId);
			return ResponseUtil.NoRecordFound();
		}
		log.debug("found cafNumber as userId" + listOfCafnum);

		return ResponseUtil.createSuccessResponse().setData(listOfCafnum);
	}

	@Override
	public APIResponse updateUserCafByUserId(List<CafDetailsVo> vo)
	{

		Integer row = 0;

		if (vo == null)
		{
			log.info("found CafDetailsVo null");
			return ResponseUtil.nullArgument();
		}
		for (CafDetailsVo cafVo : vo)
		{
			if (cafVo.getCafNumber() == null)
			{
				log.info("found CafNumber null" + cafVo);
				return ResponseUtil.nullArgument()
						.setData("found cafNumber null");
			}

			if (cafVo.getStatus() == null)
			{
				log.info("found status null" + cafVo);
				return ResponseUtil.nullArgument().setData("found status null");
			}

			if (cafVo.getUserId() == null)
			{
				log.info("found userId null" + vo);
				return ResponseUtil.nullArgument().setData("found userId null");
			}

			row = userCafDAO.updateUserCafByUserId(cafVo);
		}

		if (row <= 0)
		{
			log.info("failed to update  data in UserCaf as userId:");
			return ResponseUtil.createUpdateFailedResponse();
		}

		log.info(" data updated  in UserCaf successfully as userId:");
		return ResponseUtil.createUpdateSuccessResponse();

	}
	
	@Override
	public APIResponse searchCAF(String cafNo)
	{
		UserCafResponseVo userCafResponseVo = userCafDAO.searchCAF(cafNo);
		
		log.info("userCafResponseVo " + userCafResponseVo);
		
		if(userCafResponseVo != null )
		{
			UserVo vo = userDao.getUserById (userCafResponseVo.getUserId());
		
			if(vo != null)
			return ResponseUtil.createSuccessResponse().setData(
					 new TypeAheadVo(vo.getId(), GenericUtil.completeUserName(vo) ));
			else
				return ResponseUtil.createRecordNotFoundResponse();
		} else
		{
			return ResponseUtil.createRecordNotFoundResponse();
		}
	}

	@Override
	public APIResponse cafReassignment(ReassignCAF reassignCAF)
	{
		int result = userCafDAO.cafReassignment(reassignCAF);
		
		if(result > 0)
		{
			return ResponseUtil.createDeleteSuccessResponse().setData("CAF Reassigned Successfully");
			
		} else
		{
			return ResponseUtil.createFailureResponse().setData("CAF Reassigned Failed");
		}
	}

}
