package com.cupola.fwmp.service.userCaf;

import java.util.List;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.CafDetailsVo;
import com.cupola.fwmp.vo.ReassignCAF;
import com.cupola.fwmp.vo.UserCafVo;

public interface UserCafCoreService
{

	public APIResponse deleteUserCAF(UserCafVo userCafVo);

	APIResponse saveAndUpdateUserCAF(UserCafVo userCafVo);

	APIResponse getUserCAFByUserId(UserCafVo userCafVo);

	APIResponse getListUserCAF();

	APIResponse getUserCAFNumberByUserId(UserCafVo userCafVo);

	APIResponse addCafDetails(List<CafDetailsVo> userCafVo);

	APIResponse updateUserCafByUserId(List<CafDetailsVo> vo);

	APIResponse gerCafDetailsByUserId();

	public APIResponse cafReassignment(ReassignCAF reassignCAF);

	public APIResponse searchCAF(String cafNo);

}
