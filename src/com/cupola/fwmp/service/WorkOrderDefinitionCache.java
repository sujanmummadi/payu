package com.cupola.fwmp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.activity.ActivityDAO;
import com.cupola.fwmp.dao.subActivity.SubActivityDAO;
import com.cupola.fwmp.dao.workOrderType.WorkOrderTypeDAO;
import com.cupola.fwmp.dao.workStage.WorkStageDAO;
import com.cupola.fwmp.util.ActivitySequenceComparator;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.SubActivityVo;
import com.cupola.fwmp.vo.WorkOrderTypeVO;
import com.cupola.fwmp.vo.WorkStageVo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WorkOrderDefinitionCache
{
	static final Logger logger = Logger
			.getLogger(WorkOrderDefinitionCache.class);

	private static WorkOrderTypeDAO workOrderTypeDAO;
	private static WorkStageDAO workStageDAO;
	private static ActivityDAO activityDAO;
	private static SubActivityDAO subActivityDAO;

	private static Map<Long, WorkOrderTypeVO> workOrderTypeCache = new HashMap<Long, WorkOrderTypeVO>();
	private static Map<Long, ActivityVo> activityVoCache = new HashMap<Long, ActivityVo>();
	private static Map<Long, SubActivityVo> subActivityVoCache = new HashMap<Long, SubActivityVo>();
	
	private static Map<Long, WorkStageVo> workStagesCache = new HashMap<Long, WorkStageVo>();
	private static Map<Long, List<WorkStageVo>> woTypeWorkStageCache = new HashMap<Long, List<WorkStageVo>>();
	private static Map<Long, List<ActivityVo>> workStageActivityCache = new HashMap<Long, List<ActivityVo>>();
	private static Map<Long, List<ActivityVo>> workOrderActivityCache = new HashMap<Long, List<ActivityVo>>();
	private static Map<Long, List<SubActivityVo>> activitySubActivityCache = new HashMap<Long, List<SubActivityVo>>();
	private static Map<Long , String> NI_ACTIVITY_DEF = new HashMap<Long , String>();
	private static Map<Long , String> SALES_ACTIVITY_DEF = new HashMap<Long , String>();
	
	
	private static long cacheAgeInMillis ;
	private static long cacheResetTimestamp = 0;

	private static  void resetCacheIfOld()
	{
		/* if ((System.currentTimeMillis() - cacheResetTimestamp) > cacheAgeInMillis)
		{
			reFreshCache();
		} */
	}

	public static  void resetCache()
	{
		activityVoCache.clear();
		subActivityVoCache.clear();
		workOrderTypeCache.clear();
		woTypeWorkStageCache.clear();
		workOrderActivityCache.clear();
		workStageActivityCache.clear();
		workStagesCache.clear();
		activitySubActivityCache.clear();
		cacheResetTimestamp = System.currentTimeMillis();
		logger.info("WorkOrderDefinitionCache cache reset done");
	}
	static ObjectMapper mapper = new ObjectMapper();
	public static  void reFreshCache()
	{
		resetCache();
		workOrderTypeCache = workOrderTypeDAO.getAllWorkOrderTypes();
		List <ActivityVo>  allActivity = activityDAO.getAllActivities();
		
		for (Iterator iterator = allActivity.iterator(); iterator.hasNext();)
		{
			ActivityVo activityVo = (ActivityVo) iterator.next();
			activityVoCache.put(activityVo.getId(), activityVo);
		}
		
		if(workOrderTypeCache == null)
			return;
		
		for(long type : workOrderTypeCache.keySet())
		{
			List<WorkStageVo> stages =  workStageDAO.getWorkStagesForOrderTypeByTypeId(type) ;
			
			List<ActivityVo> activities = null; 
			List<ActivityVo> activitiesOfWorkOrder = new ArrayList<ActivityVo>();
			
			if(stages == null)
				continue;
			
			for(WorkStageVo stage: stages)
			{
				workStagesCache.put(stage.getId(), stage);
				activities =  activityDAO.getActivitiesForWorkStage(stage.getId());
				if(activities == null)
					continue;
				
				Collections.sort(activities, new  ActivitySequenceComparator());
				workStageActivityCache.put(stage.getId(), activities);
				
				activitiesOfWorkOrder.addAll(activities);
				
				
				for(ActivityVo activity : activities)
				{
					List<SubActivityVo> subActivities = subActivityDAO.getSubActivitiesForActivity(activity.getId());
					
					if(subActivities != null)
					for (Iterator iterator = subActivities.iterator(); iterator
							.hasNext();)
					{
						SubActivityVo subActivityVo = (SubActivityVo) iterator
								.next();
						subActivityVoCache.put(subActivityVo.getId(), subActivityVo);
						
					}
					activitySubActivityCache.put(activity.getId(), subActivities);
				}
				
			}
			
			Collections.sort(activitiesOfWorkOrder, new  ActivitySequenceComparator());
			workOrderActivityCache.put(type, activitiesOfWorkOrder);
			woTypeWorkStageCache.put(type, stages); // re-visit
		}
		

		List<ActivityVo> vos =	 workStageActivityCache.get(WorkStageType.FIBER);
		for (Iterator iterator = vos.iterator(); iterator.hasNext();)
		{
			ActivityVo activityVo = (ActivityVo) iterator.next();
			NI_ACTIVITY_DEF.put(activityVo.getId(), activityVo.getActivityName());
		}
		
		List<ActivityVo> salesVos =	 workStageActivityCache.get(WorkStageType.SALES);
		for (Iterator iterator = salesVos.iterator(); iterator.hasNext();)
		{
			ActivityVo activityVo = (ActivityVo) iterator.next();
			SALES_ACTIVITY_DEF.put(activityVo.getId(), activityVo.getActivityName());
		}
		
		logger.info("WorkOrderActivityCache refreshed " );
	}

	public static  WorkOrderTypeVO getWorkOrderTypeVO(Long id)
	{
		resetCacheIfOld();
		return workOrderTypeCache.get(id);
	}

	public static  ActivityVo getActivityById(Long id)
	{
		resetCacheIfOld();
		return activityVoCache.get(id);
	}

	public static  SubActivityVo getSubActivityById(Long id)
	{
		resetCacheIfOld();
		return subActivityVoCache.get(id);
	}

	public static  Map<Long, String> getWorkOrderTypes()
	{
		resetCacheIfOld();
		Map<Long, String> map = new HashMap<Long, String>();
		for (WorkOrderTypeVO vo : workOrderTypeCache.values())
		{
			map.put(vo.getId(), vo.getWorkOrderType());
		}

		return map;
	}

	public static  List<WorkStageVo> getWorkStagesForOrderTypeByTypeId(
			Long id)
	{
		resetCacheIfOld();
		return woTypeWorkStageCache.get(id);
	}
	
	public static  List<ActivityVo> getActivitiesForOrderTypeByTypeId(
			Long id)
	{
		resetCacheIfOld();
		return workOrderActivityCache.get(id);
	}

	public static  List<WorkStageVo> getWorkStages()
	{
		resetCacheIfOld();
		return new ArrayList<WorkStageVo>(workStagesCache.values());
	}
	
	public static  WorkStageVo getWorkStageById(Long id)
	{
		resetCacheIfOld();
		return workStagesCache.get(id);
	}

	public static   List<ActivityVo> getActivitiesForWorkStage(Long workStageId)
	{
		resetCacheIfOld();
		return workStageActivityCache.get(workStageId);
	}
	
	public static   Map<Long , String> getActivitiesForNI()
	{
		resetCacheIfOld();
		return NI_ACTIVITY_DEF;
	}
	public static   Map<Long , String> getActivitiesForSales()
	{
		resetCacheIfOld();
		return SALES_ACTIVITY_DEF;
	}
	
	
	public  static  List<SubActivityVo> getSubActivitiesForActivity(Long activityId) 
	{
		resetCacheIfOld();
		return activitySubActivityCache.get(activityId);
	}
	
	
	
	
	
	
	
	
	
	
	
	public static WorkOrderTypeDAO getWorkOrderTypeDAO()
	{
		return workOrderTypeDAO;
	}

	public static void setWorkOrderTypeDAO(WorkOrderTypeDAO workOrderTypeDAO)
	{
		WorkOrderDefinitionCache.workOrderTypeDAO = workOrderTypeDAO;
	}

	public static WorkStageDAO getWorkStageDAO()
	{
		return workStageDAO;
	}

	public static void setWorkStageDAO(WorkStageDAO workStageDAO)
	{
		WorkOrderDefinitionCache.workStageDAO = workStageDAO;
	}

	public static ActivityDAO getActivityDAO()
	{
		return activityDAO;
	}

	public static void setActivityDAO(ActivityDAO activityDAO)
	{
		WorkOrderDefinitionCache.activityDAO = activityDAO;
	}

	public static SubActivityDAO getSubActivityDAO()
	{
		return subActivityDAO;
	}

	public static void setSubActivityDAO(SubActivityDAO subActivityDAO)
	{
		WorkOrderDefinitionCache.subActivityDAO = subActivityDAO;
	}

	public static long getCacheAgeInMillis()
	{
		return cacheAgeInMillis;
	}

	public static void setCacheAgeInMillis(long cacheAgeInMillis)
	{
		WorkOrderDefinitionCache.cacheAgeInMillis = cacheAgeInMillis;
	}
}
