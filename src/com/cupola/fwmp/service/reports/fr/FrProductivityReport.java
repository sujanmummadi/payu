package com.cupola.fwmp.service.reports.fr;

public class FrProductivityReport  
{
	private String userName;
	private Integer mtd;
	private Integer totalFtd;
	
	private Integer before11Am;
	private Integer between11AmTo1Pm;
	private Integer between1PmTo3Pm;
	
	private Integer between3PmTo5Pm;
	private Integer between5PmTo7Pm;
	private Integer after7Pm;
	
	// for AM User field
	private Integer numberOfNeonField;
	private Integer numberOfVendors;
	private Long userId;
	private String type;
	
	public FrProductivityReport(){
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Integer getMtd() {
		return mtd;
	}
	
	public void setMtd(Integer mtd) {
		this.mtd = mtd;
	}
	
	public Integer getTotalFtd() {
		return totalFtd;
	}
	
	public void setTotalFtd(Integer totalFtd) {
		this.totalFtd = totalFtd;
	}
	
	public Integer getBefore11Am() {
		return before11Am;
	}
	
	public void setBefore11Am(Integer before11Am) {
		this.before11Am = before11Am;
	}
	
	public Integer getBetween11AmTo1Pm() {
		return between11AmTo1Pm;
	}
	
	public void setBetween11AmTo1Pm(Integer between11AmTo1Pm) {
		this.between11AmTo1Pm = between11AmTo1Pm;
	}
	
	public Integer getBetween1PmTo3Pm() {
		return between1PmTo3Pm;
	}
	
	public void setBetween1PmTo3Pm(Integer between1PmTo3Pm) {
		this.between1PmTo3Pm = between1PmTo3Pm;
	}
	
	public Integer getBetween3PmTo5Pm() {
		return between3PmTo5Pm;
	}
	
	public void setBetween3PmTo5Pm(Integer between3PmTo5Pm) {
		this.between3PmTo5Pm = between3PmTo5Pm;
	}
	
	public Integer getBetween5PmTo7Pm() {
		return between5PmTo7Pm;
	}
	public void setBetween5PmTo7Pm(Integer between5PmTo7Pm) {
		this.between5PmTo7Pm = between5PmTo7Pm;
	}
	
	public Integer getAfter7Pm() {
		return after7Pm;
	}
	
	public void setAfter7Pm(Integer after7Pm) {
		this.after7Pm = after7Pm;
	}
	
	public Integer getNumberOfNeonField() {
		return numberOfNeonField;
	}
	
	public void setNumberOfNeonField(Integer numberOfNeonField) {
		this.numberOfNeonField = numberOfNeonField;
	}
	
	public Integer getNumberOfVendors() {
		return numberOfVendors;
	}
	
	public void setNumberOfVendors(Integer numberOfVendors) {
		this.numberOfVendors = numberOfVendors;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	// constructor for Tl report...
	public FrProductivityReport(String userName, Integer mtd, Integer totalFtd,
			Integer before11Am, Integer between11AmTo1Pm,
			Integer between1PmTo3Pm, Integer between3PmTo5Pm,
			Integer between5PmTo7Pm, Integer after7Pm) {
		super();
		this.userName = userName;
		this.mtd = mtd;
		this.totalFtd = totalFtd;
		this.before11Am = before11Am;
		this.between11AmTo1Pm = between11AmTo1Pm;
		this.between1PmTo3Pm = between1PmTo3Pm;
		this.between3PmTo5Pm = between3PmTo5Pm;
		this.between5PmTo7Pm = between5PmTo7Pm;
		this.after7Pm = after7Pm;
	}

	// constructor for AM report...
	public FrProductivityReport(String userName, Integer mtd, Integer totalFtd,
			Integer before11Am, Integer between11AmTo1Pm,
			Integer between1PmTo3Pm, Integer between3PmTo5Pm,
			Integer between5PmTo7Pm, Integer after7Pm,
			Integer numberOfNeonField, Integer numberOfVendors) {
		super();
		this.userName = userName;
		this.mtd = mtd;
		this.totalFtd = totalFtd;
		this.before11Am = before11Am;
		this.between11AmTo1Pm = between11AmTo1Pm;
		this.between1PmTo3Pm = between1PmTo3Pm;
		this.between3PmTo5Pm = between3PmTo5Pm;
		this.between5PmTo7Pm = between5PmTo7Pm;
		this.after7Pm = after7Pm;
		this.numberOfNeonField = numberOfNeonField;
		this.numberOfVendors = numberOfVendors;
	}

	@Override
	public String toString() {
		return "FrProductivityReport [userName=" + userName + ", mtd=" + mtd
				+ ", totalFtd=" + totalFtd + ", before11Am=" + before11Am
				+ ", between11AmTo1Pm=" + between11AmTo1Pm
				+ ", between1PmTo3Pm=" + between1PmTo3Pm + ", between3PmTo5Pm="
				+ between3PmTo5Pm + ", between5PmTo7Pm=" + between5PmTo7Pm
				+ ", after7Pm=" + after7Pm + ", numberOfNeonField="
				+ numberOfNeonField + ", numberOfVendors=" + numberOfVendors
				+ "]";
	}

}
