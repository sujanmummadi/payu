package com.cupola.fwmp.service.reports.fr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public interface FrReportService 
{ 
	
	public List<FrProductivityReport> getFrProductivityReportByTlNew(Long tlId);
	public List<FrProductivityReport> getFrProductivityReportByAmNew(Long amId);
	
	public List<FrEtrReport> getFrEtrReportByTlNew(Long tlId);
	public List<FrEtrReport> getFrEtrReportByAMNew(Long amId);
	
	// Requirement still not clear for Allocation TL & AM 
	
	public List<FrAllocationReport> getFrAllocationReportByTl(Long tlId);
	public List<FrAllocationReport> getFrAllocationReportByAM(Long amId);
	
	
	 
	//public  List<FrClosedComplaintsReport> getFrClosedComplaintsReport();
	//public  List<FrNewLightWeight> getFrNewLightWeightReportByTl(Long tlId);
	
	public Object getDownTimeReportOnOpenTicket();
}
 