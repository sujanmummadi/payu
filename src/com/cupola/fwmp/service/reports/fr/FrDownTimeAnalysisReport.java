package com.cupola.fwmp.service.reports.fr;

public class FrDownTimeAnalysisReport
{
	private String customerName;
	private String customerId;
	
	private Integer downtimeMtd;
	private Integer numberOfTicketsRegisteredMtd;
	private String assignedDisplayName;
	
	public FrDownTimeAnalysisReport(){
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Integer getDowntimeMtd() {
		return downtimeMtd;
	}

	public void setDowntimeMtd(Integer downtimeMtd) {
		this.downtimeMtd = downtimeMtd;
	}

	public Integer getNumberOfTicketsRegisteredMtd() {
		return numberOfTicketsRegisteredMtd;
	}

	public void setNumberOfTicketsRegisteredMtd(Integer numberOfTicketsRegisteredMtd) {
		this.numberOfTicketsRegisteredMtd = numberOfTicketsRegisteredMtd;
	}

	public String getAssignedDisplayName() {
		return assignedDisplayName;
	}

	public void setAssignedDisplayName(String assignedDisplayName) {
		this.assignedDisplayName = assignedDisplayName;
	}
	
}
