 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.reports.fr;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrReAssignmentHistoryReport {
	
	private String TicketCreationDate;
	private String currentSymptomName;
	private String workOrderNumber;
	private String mqId;
	private String communicationETR;
	private String mobileNumber;
	public String getTicketCreationDate() {
		return TicketCreationDate;
	}
	public void setTicketCreationDate(String ticketCreationDate) {
		TicketCreationDate = ticketCreationDate;
	}
	public String getCurrentSymptomName() {
		return currentSymptomName;
	}
	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public String getCommunicationETR() {
		return communicationETR;
	}
	public void setCommunicationETR(String communicationETR) {
		this.communicationETR = communicationETR;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	@Override
	public String toString() {
		return "FrReAssignmentHistoryReport [TicketCreationDate="
				+ TicketCreationDate + ", currentSymptomName="
				+ currentSymptomName + ", workOrderNumber=" + workOrderNumber
				+ ", mqId=" + mqId + ", communicationETR=" + communicationETR
				+ ", mobileNumber=" + mobileNumber + "]";
	}
	
	
	
	
	

}
