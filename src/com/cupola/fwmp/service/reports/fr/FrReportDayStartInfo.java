package com.cupola.fwmp.service.reports.fr;

public class FrReportDayStartInfo implements FrReport
{
	int dayStared ;
	int inflow ;
	
	public int getDayStared() {
		return dayStared;
	}
	public void setDayStared(int dayStared) {
		this.dayStared = dayStared;
	}
	public int getInflow() {
		return inflow;
	}
	public void setInflow(int inflow) {
		this.inflow = inflow;
	}
	
}
