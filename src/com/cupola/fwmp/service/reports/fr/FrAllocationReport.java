package com.cupola.fwmp.service.reports.fr;

public class FrAllocationReport
{
	private String userName;
	private Integer pendingStartInflow;
	private Integer pendingReassigned;
	
	private Integer closedStartInflow;
	private Integer closedReassigned;
	
	private Integer totalPending;
	private Integer totalClosed;
	
	private String openGart;
	private String closedGart;
	private Integer ticketOpenMoreThan4Hours;
	
	// for AM user field
	
	private Integer numberOfNeonField;
	private Integer numberOfVendors;
	
	
//  common for All ne ie for tl level count report
//	private Integer dayStartWith;
//	private Integer inffow;
	
	public FrAllocationReport(){
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getPendingStartInflow() {
		return pendingStartInflow;
	}

	public void setPendingStartInflow(Integer pendingStartInflow) {
		this.pendingStartInflow = pendingStartInflow;
	}

	public Integer getPendingReassigned() {
		return pendingReassigned;
	}

	public void setPendingReassigned(Integer pendingReassigned) {
		this.pendingReassigned = pendingReassigned;
	}

	public Integer getClosedStartInflow() {
		return closedStartInflow;
	}

	public void setClosedStartInflow(Integer closedStartInflow) {
		this.closedStartInflow = closedStartInflow;
	}

	public Integer getClosedReassigned() {
		return closedReassigned;
	}

	public void setClosedReassigned(Integer closedReassigned) {
		this.closedReassigned = closedReassigned;
	}

	public Integer getTotalPending() {
		return totalPending;
	}

	public void setTotalPending(Integer totalPending) {
		this.totalPending = totalPending;
	}

	public Integer getTotalClosed() {
		return totalClosed;
	}

	public void setTotalClosed(Integer totalClosed) {
		this.totalClosed = totalClosed;
	}

	public String getOpenGart() {
		return openGart;
	}

	public void setOpenGart(String openGart) {
		this.openGart = openGart;
	}

	public String getClosedGart() {
		return closedGart;
	}

	public void setClosedGart(String closedGart) {
		this.closedGart = closedGart;
	}

	public Integer getTicketOpenMoreThan4Hours() {
		return ticketOpenMoreThan4Hours;
	}

	public void setTicketOpenMoreThan4Hours(Integer ticketOpenMoreThan4Hours) {
		this.ticketOpenMoreThan4Hours = ticketOpenMoreThan4Hours;
	}

	public Integer getNumberOfNeonField() {
		return numberOfNeonField;
	}

	public void setNumberOfNeonField(Integer numberOfNeonField) {
		this.numberOfNeonField = numberOfNeonField;
	}

	public Integer getNumberOfVendors() {
		return numberOfVendors;
	}

	public void setNumberOfVendors(Integer numberOfVendors) {
		this.numberOfVendors = numberOfVendors;
	}

	// constructor for Tl Allocation Report
	public FrAllocationReport(String userName, Integer pendingStartInflow,
			Integer pendingReassigned, Integer closedStartInflow,
			Integer closedReassigned, Integer totalPending,
			Integer totalClosed, String openGart, String closedGart,
			Integer ticketOpenMoreThan4Hours) {
		super();
		this.userName = userName;
		this.pendingStartInflow = pendingStartInflow;
		this.pendingReassigned = pendingReassigned;
		this.closedStartInflow = closedStartInflow;
		this.closedReassigned = closedReassigned;
		this.totalPending = totalPending;
		this.totalClosed = totalClosed;
		this.openGart = openGart;
		this.closedGart = closedGart;
		this.ticketOpenMoreThan4Hours = ticketOpenMoreThan4Hours;
	}

	// constructor for AM Allocation Report
	public FrAllocationReport(String userName, Integer pendingStartInflow,
			Integer pendingReassigned, Integer closedStartInflow,
			Integer closedReassigned, Integer totalPending,
			Integer totalClosed, String openGart, String closedGart,
			Integer ticketOpenMoreThan4Hours, Integer numberOfNeonField,
			Integer numberOfVendors) {
		super();
		this.userName = userName;
		this.pendingStartInflow = pendingStartInflow;
		this.pendingReassigned = pendingReassigned;
		this.closedStartInflow = closedStartInflow;
		this.closedReassigned = closedReassigned;
		this.totalPending = totalPending;
		this.totalClosed = totalClosed;
		this.openGart = openGart;
		this.closedGart = closedGart;
		this.ticketOpenMoreThan4Hours = ticketOpenMoreThan4Hours;
		this.numberOfNeonField = numberOfNeonField;
		this.numberOfVendors = numberOfVendors;
	}

	@Override
	public String toString() {
		return "FrAllocationReport [userName=" + userName
				+ ", pendingStartInflow=" + pendingStartInflow
				+ ", pendingReassigned=" + pendingReassigned
				+ ", closedStartInflow=" + closedStartInflow
				+ ", closedReassigned=" + closedReassigned + ", totalPending="
				+ totalPending + ", totalClosed=" + totalClosed + ", openGart="
				+ openGart + ", closedGart=" + closedGart
				+ ", ticketOpenMoreThan4Hours=" + ticketOpenMoreThan4Hours
				+ ", numberOfNeonField=" + numberOfNeonField
				+ ", numberOfVendors=" + numberOfVendors + "]";
	}
	
}
