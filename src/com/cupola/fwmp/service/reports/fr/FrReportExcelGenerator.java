package com.cupola.fwmp.service.reports.fr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cupola.fwmp.FRConstant.FrReportConstant;
import com.cupola.fwmp.util.AuthUtils;

public class FrReportExcelGenerator 
{
	
	private static Logger LOGGER = Logger.getLogger(FrReportExcelGenerator.class.getName());
	private final static String BASE_PATH = "/usr/share/fwmp/reports/";
	
	
	private static XSSFWorkbook workBook = null;
	
	
	private static XSSFSheet getReportSheet(String ...reportType)
	{
		String inputFileName = null;
		FileInputStream inputStream = null;
		try {
			if( reportType != null && reportType.length == 2)
			{
				File path = new File(BASE_PATH + File.separator +"template");
				if( !path.exists())
					path.mkdirs();
				
				inputFileName = path.getAbsolutePath()+ File.separator +reportType[0] + ".xlsx";
				inputStream = new FileInputStream(inputFileName);
				workBook = new XSSFWorkbook(inputStream);
				return workBook.getSheet(reportType[1]);
			}
			
		} 
		catch (FileNotFoundException e)
		{
			LOGGER.error("Fr report Template file are not available in Location : "
					+inputFileName);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try{
				if( inputStream != null)
					inputStream.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	public static String generateEtrReportInExcel (Map<String, List<FrReport>> reportObject)
	{

		XSSFSheet spreadsheet = getReportSheet("EtrTemplate","EtrReports");
		if( spreadsheet == null )
		{
			LOGGER.info("Error occure while generating Etr Report in Excel...");
			return null;
		}
		
		XSSFRow row;
		List<FrReport> etrReports = null;

		int rowid = 3;
		int nextMergeSize = 0;

		int startRow = 0;
		FrEtrReport etrReport = null;
		Cell cell = null;

		for (Map.Entry<String, List<FrReport>> entry : reportObject.entrySet()) {
			boolean isCreated = true;
			etrReports = entry.getValue();

			int reportSize = entry.getValue().size();
			int mergeSize = reportSize + nextMergeSize;

			startRow = rowid;
			startRow--;

			for (FrReport obj : etrReports)
			{
				int cellid = 1;
				etrReport = (FrEtrReport) obj;

				row = spreadsheet.createRow(rowid++);
				if (isCreated)
				{
					cell = row.createCell(0);
					cell.setCellValue(new XSSFRichTextString(entry.getKey()));
					spreadsheet.addMergedRegion(new CellRangeAddress(
							startRow + 1, mergeSize + 2, 0, 0));
				}
				isCreated = false;

				cell = row.createCell(cellid++);
				cell.setCellValue((String) etrReport.getUserName());

				cell = row.createCell(cellid++);
				cell.setCellValue(etrReport.getTotalOpenTicket());

				cell = row.createCell(cellid++);
				cell.setCellValue(etrReport.getEtrExpired());

				cell = row.createCell(cellid++);
				cell.setCellValue(etrReport.getOneTimes());

				cell = row.createCell(cellid++);
				cell.setCellValue(etrReport.getTwoTimes());

				cell = row.createCell(cellid++);
				cell.setCellValue(etrReport.getThreeTimes());

				cell = row.createCell(cellid++);
				cell.setCellValue(etrReport.getMoreThan3Times());

			}
			nextMergeSize = reportSize;
		}
		if ( AuthUtils.isManager() )
			return generateExcelReport("ETR_AM_");
		else
			return generateExcelReport("ETR_Tl_");
	}
	
	private static String generateExcelReport(String repprtType)
	{
		if ( workBook == null )
		{
			LOGGER.info("Error occure while writing report in Excel...");
			return null;
		}
		long tlReport = AuthUtils.getCurrentUserId();
		String outFileName = null;
		File path = new File(BASE_PATH + File.separator +"Fr");
		if( !path.exists())
			path.mkdirs();
		
		outFileName = path.getAbsolutePath()+ File.separator +repprtType+ tlReport + ".xlsx";
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(outFileName);
			LOGGER.debug("Report file output " + outFileName);
			workBook.write(fileOut);
			fileOut.close();
			return outFileName;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				if( fileOut != null)
					fileOut.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return outFileName;
	}
	
	public static void generateProductivityReportInExcel(Map<String, List<FrReport>> reportObject)
	{
		XSSFSheet spreadsheet = null;
		
		if ( AuthUtils.isManager() )
			spreadsheet = getReportSheet("ProductivityTemplate_AM","ProductivityReports-AM");
		else if( AuthUtils.isFRTLUser() )
			spreadsheet = getReportSheet("ProductivityTemplate_Tl","ProductivityReports-Tl");
		
		if( spreadsheet == null )
		{
			LOGGER.info("Error occure while generating Productivity Report in Excel...");
			return;
		}
		
		XSSFRow row;
		List<FrReport> reports = null;
		
		int rowid = 2;
		int nextMergeSize = 0;

		int startRow = 0;
		FrProductivityReport productiveReport = null;
		Cell cell = null;

		for (Map.Entry<String, List<FrReport>> entry : reportObject.entrySet()) 
		{
			boolean isCreated = true;
			reports = entry.getValue();

			int reportSize = entry.getValue().size();
			int mergeSize = reportSize + nextMergeSize;

			startRow = rowid;
			startRow--;

			for (FrReport obj : reports)
			{
				int cellid = 1;
				productiveReport = (FrProductivityReport) obj;

				row = spreadsheet.createRow(rowid++);
				if (isCreated)
				{
					cell = row.createCell(0);
					cell.setCellValue(new XSSFRichTextString(entry.getKey()));
					spreadsheet.addMergedRegion(new CellRangeAddress(
							startRow + 1, mergeSize + 1, 0, 0));
				}
				isCreated = false;

				cell = row.createCell(cellid++);
				cell.setCellValue((String) productiveReport.getUserName());
				
				if( AuthUtils.isManager() )
				{
					cell = row.createCell(cellid++);
					cell.setCellValue(productiveReport.getNumberOfNeonField());

					cell = row.createCell(cellid++);
					cell.setCellValue(productiveReport.getNumberOfVendors());
				}

				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getMtd());

				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getTotalFtd());

				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getBefore11Am());

				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getBetween11AmTo1Pm());

				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getBetween1PmTo3Pm());

				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getBetween3PmTo5Pm());
				
				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getBetween5PmTo7Pm());
				
				cell = row.createCell(cellid++);
				cell.setCellValue(productiveReport.getAfter7Pm());

			}
			nextMergeSize = reportSize;
		}
		
		if( AuthUtils.isManager() )
			generateExcelReport("Productivity_AM_");
		else
			generateExcelReport("Productivity_Tl_");
	}
	
	public static void generateAllocationReportInExcel(Map<String, List<FrReport>> reportObject)
	{
		XSSFSheet spreadsheet = null;
		if ( AuthUtils.isManager() )
			spreadsheet = getReportSheet("AllocationTemplate_AM","AllocationReports-AM");
		else if( AuthUtils.isFRTLUser() )
			spreadsheet = getReportSheet("AllocationTemplate_Tl","AllocationReports-Tl");
		
		if( spreadsheet == null )
		{
			LOGGER.info("Error occure while generating allocation Report in Excel...");
			return;
		}
		
		XSSFRow row;
		List<FrReport> reports = null;
		List<FrReport> dayStartReports = null;
		
		Cell cell = null;
		int rowid = 0;
		if( AuthUtils.isFRTLUser() )
		{
			dayStartReports = reportObject.get(FrReportConstant.DAY_START_INFO);
			FrReportDayStartInfo dayStart = null;
			
			if( dayStartReports != null && dayStartReports.size() > 0 )
			{
				dayStart = (FrReportDayStartInfo)dayStartReports.get(0);
				reportObject.remove(FrReportConstant.DAY_START_INFO);
				int dayStartWith = dayStart.getDayStared();
				int inflow = dayStart.getInflow();
				
				row = spreadsheet.getRow(0);
				cell = row.createCell(2);
				cell.setCellValue(dayStartWith);
				
				row = spreadsheet.getRow(1);
				cell = row.createCell(2);
				cell.setCellValue(inflow);
			}
			rowid = 4;
		}
		else
			rowid = 2;
		
		int nextMergeSize = 0;
		int startRow = 0;
		FrAllocationReport allocationReport = null;
		
		for (Map.Entry<String, List<FrReport>> entry : reportObject.entrySet()) 
		{
			boolean isCreated = true;
			reports = entry.getValue();

			int reportSize = entry.getValue().size();
			int mergeSize = reportSize + nextMergeSize;

			startRow = rowid;
			startRow--;

			for (FrReport obj : reports)
			{
				int cellid = 1;
				
				allocationReport = (FrAllocationReport) obj;
				
				row = spreadsheet.createRow(rowid++);
				if (isCreated)
				{
					cell = row.createCell(0);
					cell.setCellValue(new XSSFRichTextString(entry.getKey()));
					spreadsheet.addMergedRegion(new CellRangeAddress(
							startRow + 1, mergeSize + 1, 0, 0));
				}
				isCreated = false;

				cell = row.createCell(cellid++);
				cell.setCellValue((String) allocationReport.getUserName());
				
				if( AuthUtils.isManager() )
				{
					cell = row.createCell(cellid++);
					cell.setCellValue(allocationReport.getNumberOfNeonField());

					cell = row.createCell(cellid++);
					cell.setCellValue(allocationReport.getNumberOfVendors());
				}

				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getPendingStartInflow());

				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getPendingReassigned());

				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getClosedStartInflow());

				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getClosedReassigned());

				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getTotalPending());

				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getTotalClosed());
				
				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getOpenGart());
				
				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getClosedGart());
				
				cell = row.createCell(cellid++);
				cell.setCellValue(allocationReport.getTicketOpenMoreThan4Hours());

			}
			nextMergeSize = reportSize;
		}
		
		if( AuthUtils.isManager() )
			generateExcelReport("Allocation_AM_");
		else
		{
			reportObject.put(FrReportConstant.DAY_START_INFO, dayStartReports);
			generateExcelReport("Allocation_Tl_");
		}
		
	}
	
}
