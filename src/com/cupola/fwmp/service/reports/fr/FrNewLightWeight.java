 /**
 * @Author leela  Jun 8, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.reports.fr;

 /**
 * @Author leela  Jun 8, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrNewLightWeight {
	
	private String TicketCreationDate;
	private String modifiedOn;
	private String workOrderNumber;
	private String mqId;
	private String areaName;
	private String fxName;
	private String cxName;
	private String communicationAdderss;
	private String status;
	private String firstName;
	private String branchName;
	private String mobileNumber;
	private String TicketCategory;
	private String reopenCount;
	private String communicationETR;
	private String voc;
	public String getTicketCreationDate() {
		return TicketCreationDate;
	}
	public void setTicketCreationDate(String ticketCreationDate) {
		TicketCreationDate = ticketCreationDate;
	}
	public String getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getFxName() {
		return fxName;
	}
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	public String getCxName() {
		return cxName;
	}
	public void setCxName(String cxName) {
		this.cxName = cxName;
	}
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getTicketCategory() {
		return TicketCategory;
	}
	public void setTicketCategory(String ticketCategory) {
		TicketCategory = ticketCategory;
	}
	public String getReopenCount() {
		return reopenCount;
	}
	public void setReopenCount(String reopenCount) {
		this.reopenCount = reopenCount;
	}
	public String getCommunicationETR() {
		return communicationETR;
	}
	public void setCommunicationETR(String communicationETR) {
		this.communicationETR = communicationETR;
	}
	public String getVoc() {
		return voc;
	}
	public void setVoc(String voc) {
		this.voc = voc;
	}
	@Override
	public String toString() {
		return "FrNewLightWeight [TicketCreationDate=" + TicketCreationDate
				+ ", modifiedOn=" + modifiedOn + ", workOrderNumber="
				+ workOrderNumber + ", mqId=" + mqId + ", areaName=" + areaName
				+ ", fxName=" + fxName + ", cxName=" + cxName
				+ ", communicationAdderss=" + communicationAdderss
				+ ", status=" + status + ", firstName=" + firstName
				+ ", branchName=" + branchName + ", mobileNumber="
				+ mobileNumber + ", TicketCategory=" + TicketCategory
				+ ", reopenCount=" + reopenCount + ", communicationETR="
				+ communicationETR + ", voc=" + voc + "]";
	}
	 
	 
	
	

}
