package com.cupola.fwmp.service.reports.fr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FrReportConstant;
import com.cupola.fwmp.dao.reports.fr.FrReportDao;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.vo.fr.FrAllocationReportVo;
import com.cupola.fwmp.vo.fr.FrEtrReportVo;

public class FrReportServiceImpl implements FrReportService
{
	private static Logger LOGGER = Logger.getLogger(FrReportServiceImpl.class.getName());
	
	@Autowired
	private FrReportDao frReportDao;
	
	@Autowired
	private UserDao userDao;
	
  
	@Override
	public List<FrProductivityReport> getFrProductivityReportByTlNew(Long tlId)
	{
		List<FrProductivityReport> productiveReportList = frReportDao
				.getFrProductivityReportByTlNew(tlId);
		
		return productiveReportList;
	}
	 
	
	@Override
	public List<?> getDownTimeReportOnOpenTicket() {
		
		return null;
	}
 
	  
 

	@Override
	public List<FrProductivityReport> getFrProductivityReportByAmNew(Long amId) {
		List<FrProductivityReport> productiveReportList = frReportDao
				.getFrProductivityReportByAmNew(amId);
		
		return productiveReportList;
	}

	@Override
	public List<FrEtrReport> getFrEtrReportByTlNew(Long tlId) {
		List<FrEtrReport> etrReportList = frReportDao
				.getFrEtrReportByTlNew(tlId);
		
		return etrReportList;
	}

	@Override
	public List<FrEtrReport> getFrEtrReportByAMNew(Long amId) {
		List<FrEtrReport> etrReportList = frReportDao
				.getFrEtrReportByAMNew(amId);
		
		return etrReportList;
		 
	}


	@Override
	public List<FrAllocationReport> getFrAllocationReportByTl(Long tlId) {
		// TODO Auto-generated method stub
		List<FrAllocationReport> allocationReportList = frReportDao
				.getFrAllocationReportByTl(tlId);
		
		return allocationReportList;
	}


	@Override
	public List<FrAllocationReport> getFrAllocationReportByAM(Long amId) {
		// TODO Auto-generated method stub
		List<FrAllocationReport> allocationReportList = frReportDao
				.getFrAllocationReportByAM(amId);
		
		return allocationReportList;
		 
	}



	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.reports.fr.FrReportService#getFrClosedComplaintsReportByTl(java.lang.Long)
	 */
	/*@Override
	public List<FrClosedComplaintsReport> getFrClosedComplaintsReport() {
		// TODO Auto-generated method stub
		List<FrClosedComplaintsReport> closedComplaintsReportList = frReportDao.getFrClosedComplaintsReport();
				 

		return closedComplaintsReportList;
	}*/


	/* (non-Javadoc)
	 * @see com.cupola.fwmp.service.reports.fr.FrReportService#getFrNewLightWeightReportByTl(java.lang.Long)
	 */
	/*@Override
	public List<FrNewLightWeight> getFrNewLightWeightReportByTl(Long tlId) {
		// TODO Auto-generated method stub
		List<FrNewLightWeight>  netLightWeightReportList = frReportDao.getFrNewLightWeightReportByTl(tlId);
		 

		return netLightWeightReportList;
	}*/
}
