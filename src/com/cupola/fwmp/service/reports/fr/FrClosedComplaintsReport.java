 /**
 * @Author leela  Jun 7, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.reports.fr;

 /**
 * @Author leela  Jun 7, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrClosedComplaintsReport {
	   
	private String TicketCreationDate;
	private String modifiedOn;
	private String workOrderNumber;
	private String mqId;
	private String areaName;
	private String fxName;
	private String cxName;
	private String communicationAdderss;
	private String firstName;
	private String branchName;
	private String TicketCategory;
	private String resCode;
	private String reopenCount;
	private String communicationETR;
	private String voc;
	private String closedBy;
	private String TicketClosedTime;
	private String defectCode;
	private String subDefectCode;
	public String getTicketCreationDate() {
		return TicketCreationDate;
	}
	public void setTicketCreationDate(String ticketCreationDate) {
		TicketCreationDate = ticketCreationDate;
	}
	public String getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getWorkOrderNumber() {
		return workOrderNumber;
	}
	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}
	public String getMqId() {
		return mqId;
	}
	public void setMqId(String mqId) {
		this.mqId = mqId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getFxName() {
		return fxName;
	}
	public void setFxName(String fxName) {
		this.fxName = fxName;
	}
	public String getCxName() {
		return cxName;
	}
	public void setCxName(String cxName) {
		this.cxName = cxName;
	}
	public String getCommunicationAdderss() {
		return communicationAdderss;
	}
	public void setCommunicationAdderss(String communicationAdderss) {
		this.communicationAdderss = communicationAdderss;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getTicketCategory() {
		return TicketCategory;
	}
	public void setTicketCategory(String ticketCategory) {
		TicketCategory = ticketCategory;
	}
	public String getResCode() {
		return resCode;
	}
	public void setResCode(String resCode) {
		this.resCode = resCode;
	}
	public String getReopenCount() {
		return reopenCount;
	}
	public void setReopenCount(String reopenCount) {
		this.reopenCount = reopenCount;
	}
	public String getCommunicationETR() {
		return communicationETR;
	}
	public void setCommunicationETR(String communicationETR) {
		this.communicationETR = communicationETR;
	}
	public String getVoc() {
		return voc;
	}
	public void setVoc(String voc) {
		this.voc = voc;
	}
	public String getClosedBy() {
		return closedBy;
	}
	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}
	public String getTicketClosedTime() {
		return TicketClosedTime;
	}
	public void setTicketClosedTime(String ticketClosedTime) {
		TicketClosedTime = ticketClosedTime;
	}
	public String getDefectCode() {
		return defectCode;
	}
	public void setDefectCode(String defectCode) {
		this.defectCode = defectCode;
	}
	public String getSubDefectCode() {
		return subDefectCode;
	}
	public void setSubDefectCode(String subDefectCode) {
		this.subDefectCode = subDefectCode;
	}
	@Override
	public String toString() {
		return "FrClosedComplaintsReport [TicketCreationDate="
				+ TicketCreationDate + ", modifiedOn=" + modifiedOn
				+ ", workOrderNumber=" + workOrderNumber + ", mqId=" + mqId
				+ ", areaName=" + areaName + ", fxName=" + fxName + ", cxName="
				+ cxName + ", communicationAdderss=" + communicationAdderss
				+ ", firstName=" + firstName + ", branchName=" + branchName
				+ ", TicketCategory=" + TicketCategory + ", resCode=" + resCode
				+ ", reopenCount=" + reopenCount + ", communicationETR="
				+ communicationETR + ", voc=" + voc + ", closedBy=" + closedBy
				+ ", TicketClosedTime=" + TicketClosedTime + ", defectCode="
				+ defectCode + ", subDefectCode=" + subDefectCode + "]";
	}
	
	 

}
