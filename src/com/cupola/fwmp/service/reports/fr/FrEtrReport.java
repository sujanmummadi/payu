package com.cupola.fwmp.service.reports.fr;

public class FrEtrReport  implements FrReport
{
	private String userName;
	private Integer totalOpenTicket;
	private Integer etrExpired;
	private Integer oneTimes;
	private Integer twoTimes;
	private Integer threeTimes;
	private Integer moreThan3Times;
	private String type;
	private Long userId;
	
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public FrEtrReport(){}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getTotalOpenTicket() {
		return totalOpenTicket;
	}
	public void setTotalOpenTicket(Integer totalOpenTicket) {
		this.totalOpenTicket = totalOpenTicket;
	}
	public Integer getEtrExpired() {
		return etrExpired;
	}
	public void setEtrExpired(Integer etrExpired) {
		this.etrExpired = etrExpired;
	}
	public Integer getOneTimes() {
		return oneTimes;
	}
	public void setOneTimes(Integer oneTimes) {
		this.oneTimes = oneTimes;
	}
	public Integer getTwoTimes() {
		return twoTimes;
	}
	public void setTwoTimes(Integer twoTimes) {
		this.twoTimes = twoTimes;
	}
	public Integer getThreeTimes() {
		return threeTimes;
	}
	public void setThreeTimes(Integer threeTimes) {
		this.threeTimes = threeTimes;
	}
	public Integer getMoreThan3Times() {
		return moreThan3Times;
	}
	public void setMoreThan3Times(Integer moreThan3Times) {
		this.moreThan3Times = moreThan3Times;
	}

	public FrEtrReport(String userName, Integer totalOpenTicket,
			Integer etrExpired, Integer oneTimes, Integer twoTimes,
			Integer threeTimes, Integer moreThan3Times ) {
		super();
		this.userName = userName;
		this.totalOpenTicket = totalOpenTicket;
		this.etrExpired = etrExpired;
		this.oneTimes = oneTimes;
		this.twoTimes = twoTimes;
		this.threeTimes = threeTimes;
		this.moreThan3Times = moreThan3Times;
	}

	@Override
	public String toString() {
		return "FrEtrReport [userName=" + userName + ", totalOpenTicket="
				+ totalOpenTicket + ", etrExpired=" + etrExpired
				+ ", oneTimes=" + oneTimes + ", twoTimes=" + twoTimes
				+ ", threeTimes=" + threeTimes + ", moreThan3Times="
				+ moreThan3Times + ", type=" + type + ", userId=" + userId
				+ "]";
	}
	
	
	
	
	
	
}
