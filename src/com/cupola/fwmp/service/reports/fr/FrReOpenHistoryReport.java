 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.service.reports.fr;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class FrReOpenHistoryReport {
	
	private String mqId;
	private String workOrderNumber;
	private String TicketCreationDate;
	private String status;
	private String TicketCategory;
	private String currentSymptomName;
	private String empId;
	private String firstName;
	private String TicketClosedTime;
	private String reopenCount;
	private String reopenDateTime;

	
	
	public String getReopenDateTime() {
		return reopenDateTime;
	}


	public void setReopenDateTime(String reopenDateTime) {
		this.reopenDateTime = reopenDateTime;
	}


	public String getTicketClosedTime() {
		return TicketClosedTime;
	}


	public void setTicketClosedTime(String ticketClosedTime) {
		TicketClosedTime = ticketClosedTime;
	}


	public String getMqId() {
		return mqId;
	}


	public void setMqId(String mqId) {
		this.mqId = mqId;
	}


	public String getWorkOrderNumber() {
		return workOrderNumber;
	}




	public void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}




	public String getTicketCreationDate() {
		return TicketCreationDate;
	}




	public void setTicketCreationDate(String ticketCreationDate) {
		TicketCreationDate = ticketCreationDate;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	public String getTicketCategory() {
		return TicketCategory;
	}




	public void setTicketCategory(String ticketCategory) {
		TicketCategory = ticketCategory;
	}




	public String getCurrentSymptomName() {
		return currentSymptomName;
	}




	public void setCurrentSymptomName(String currentSymptomName) {
		this.currentSymptomName = currentSymptomName;
	}




	public String getEmpId() {
		return empId;
	}




	public void setEmpId(String empId) {
		this.empId = empId;
	}




	public String getFirstName() {
		return firstName;
	}




	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}




	public String getReopenCount() {
		return reopenCount;
	}




	public void setReopenCount(String reopenCount) {
		this.reopenCount = reopenCount;
	}


	@Override
	public String toString() {
		return "FrReOpenHistoryReport [mqId=" + mqId + ", workOrderNumber="
				+ workOrderNumber + ", TicketCreationDate="
				+ TicketCreationDate + ", status=" + status
				+ ", TicketCategory=" + TicketCategory
				+ ", currentSymptomName=" + currentSymptomName + ", empId="
				+ empId + ", firstName=" + firstName + ", TicketClosedTime="
				+ TicketClosedTime + ", reopenCount=" + reopenCount
				+ ", reopenDateTime=" + reopenDateTime + "]";
	}


	 
  
	
	

}
