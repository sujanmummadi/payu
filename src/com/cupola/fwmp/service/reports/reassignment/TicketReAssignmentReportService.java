package com.cupola.fwmp.service.reports.reassignment;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import com.cupola.fwmp.FWMPConstant.ReportType;
import com.cupola.fwmp.dao.reports.reassignment.ReAssignmentReportDAO;
import com.cupola.fwmp.reports.jasper.JasperManager;
import com.cupola.fwmp.reports.util.NamingConstants;
import com.cupola.fwmp.reports.util.PropLoader;
import com.cupola.fwmp.reports.vo.FrReAssignmentHistoryReport;
import com.cupola.fwmp.reports.vo.FrTicketReAssignmentReport;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.FTPUploader;
import com.cupola.fwmp.util.FTPUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.google.gson.Gson;


@Path("frreport/ticketreassign")
public class TicketReAssignmentReportService {
	
	public static Logger log = LogManager.getLogger(TicketReAssignmentReportService.class);

	 @Autowired
		ReAssignmentReportDAO reassignmentReportDAO;

		@Context
		HttpServletRequest request;

		@Context
		HttpServletResponse response;

		@Context
		SecurityContext securityContext;

		private static String JRXML_HOST_PATH = "jrxml.path";
		@Value("${auto.report.path}")
		private String BASE_PATH;
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("fr/{fromDate}/{toDate}/{branchId}/{areaId}")
	public List<FrTicketReAssignmentReport> getFrTicketReassignmentHistoryReport(
			@PathParam("fromDate") String fromDate,
			@PathParam("toDate") String toDate,
			@PathParam("branchId") String branchId,
			@PathParam("areaId") String areaId) throws IOException {
		
		log.info("Inside getFrReassignmentHistoryReport %%%%%%%%%%%%%%%%%%%%%%%%");

		log.info("@@@@@@@@@  fromDate(" +fromDate + "), toDate("+toDate+"),branchId("+branchId+"),areaId("+areaId+")");
		
		List<FrTicketReAssignmentReport> frReportList = reassignmentReportDAO.getFrTicketReassignmentHistoryReport(fromDate, toDate,branchId, areaId ,null);
			 
		log.info("List size of FrReassignmentHistoryReport : " + frReportList.size());

		Gson gson = new Gson();

		String jsonCartList = "";

		jsonCartList = gson.toJson(frReportList);

		JasperReport jasperReport = null;

		List<Object> objectList = null;
		try {

			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.TICKETREASSIGNMENTJRXML + ".jrxml";
			// log.info( "URL %%%%%%%%%%%%%%%%%%%%%%% " +URL);
			jasperReport = JasperCompileManager.compileReport(URL);

		} catch (JRException e1) {
			log.error("Error While compiling jasper report :" + e1.getMessage());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Parameters for report

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("reportData", objectList);
		parameters.put("IS_IGNORE_PAGINATION", true);
		// DataSource
		// This is simple example, no database.
		// then using empty datasource.
		// JRBeanCollectionDataSource beanCollectionDataSource = new
		// JRBeanCollectionDataSource(ticket);
		JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
				1);

		JasperPrint jasperPrint = null;
		try {
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					parameters, beanCollectionDataSource);
		} catch (JRException e) {
			log.error("Error While printing jasper :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JasperManager.exportData(jsonCartList,NamingConstants.WORKORDERREPORTJRXML,PropLoader.getPropertyForReport("workorderreport.xls.output.path"));

		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		log.info("appPath =  " + appPath);
		String fileName = " TicketReAssignmentReport.xls";

		String fullPath = appPath + fileName;
		// log.info("fullPath %%%%%%%%%%%% " + fullPath);
		// File downloadFile = new File(fullPath);

		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		response.setContentType(mimeType);

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileName);

		response.setHeader(headerKey, headerValue);

		OutputStream outStream = response.getOutputStream();

		JRXlsExporter excellReporter = new JRXlsExporter();

		excellReporter.setExporterInput(new SimpleExporterInput(jasperPrint));

		excellReporter.setExporterOutput(new SimpleOutputStreamExporterOutput(
				outStream));

		SimpleXlsReportConfiguration config = new SimpleXlsReportConfiguration();

		config.setOnePagePerSheet(false);

		config.setDetectCellType(true);

		config.setRemoveEmptySpaceBetweenColumns(true);

		config.setRemoveEmptySpaceBetweenRows(true);

		config.setWhitePageBackground(false);

		config.setIgnoreGraphics(true);

		config.setCollapseRowSpan(true);

		config.setIgnoreCellBorder(false);

		config.setFontSizeFixEnabled(true);

		config.setMaxRowsPerSheet(0);
		// config.setWrapText(true);
		config.setIgnorePageMargins(true);
		// config.sete

		String[] name = { "TicketReAssignmentreport" };

		config.setSheetNames(name);

		config.setFormatPatternsMap(new HashMap<String, String>());

		excellReporter.setConfiguration(config);

		try {
			excellReporter.exportReport();
		} catch (JRException e) {
			log.error("Error While excel report :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			outStream.close();
		}
		
		return new ArrayList<>();

	}
	
	@Scheduled(cron = "${fwmp.auto.frreport.generator.cron.triger}")
	public  void getAutoReAssignReports() {
		FTPUploader ftpUploader = null;
		try {
			ftpUploader = new FTPUploader(
					DefinitionCoreServiceImpl.ftpClientProperties.get("server"),
					DefinitionCoreServiceImpl.ftpClientProperties
							.get("username"),
					DefinitionCoreServiceImpl.ftpClientProperties
							.get("password"));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Date fromDate, toDate;

		{
			Calendar calendar = GenericUtil.getCalendarForNow();
			calendar.set(Calendar.DAY_OF_MONTH,
					calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
			GenericUtil.setTimeToBeginningOfDay(calendar);
			fromDate = calendar.getTime();
		}

		{
			Calendar calendar = GenericUtil.getCalendarForNow();
			GenericUtil.setTimeToEndofDay(calendar);
			toDate = calendar.getTime();
		}

		Map<Long, String> cities = LocationCache.getAllCity();
		for (Entry<Long, String> city : cities.entrySet()) {
			log.debug("Report generating city name :"
					+ city.getValue());
			List<FrTicketReAssignmentReport> reassignList = reassignmentReportDAO.getFrTicketReassignmentHistoryReport(GenericUtil.dateToStringFormate(fromDate),
					GenericUtil.dateToStringFormate(toDate), null, null,
					String.valueOf(city.getKey()));

			Gson gson = new Gson();

			String jsonCartList = "";

			List<Object> objectList = null;

			jsonCartList = gson.toJson(reassignList);
			objectList = JasperManager.createJsonToReportListData(jsonCartList);

			String URL = PropLoader.getPropertyForReport(JRXML_HOST_PATH)
					+ NamingConstants.TICKETREASSIGNMENTJRXML + ".jrxml";
			try {
				JasperReport jasperReport = JasperCompileManager
						.compileReport(URL);
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("reportData", objectList);
				parameters.put("IS_IGNORE_PAGINATION", true);
				JREmptyDataSource beanCollectionDataSource = new net.sf.jasperreports.engine.JREmptyDataSource(
						1);

				JasperPrint print = JasperFillManager.fillReport(jasperReport,
						parameters, beanCollectionDataSource);
				String path = GenericUtil.createFolder(BASE_PATH,
						city.getValue(), ReportType.TICKETREASSIGNMENT);
				String file = createFileName();
				JRXlsxExporter exporter = new JRXlsxExporter();
				exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT,
						print);
				exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME,
						path + file);
				exporter.exportReport();
				String remoteArchPath = FTPUtil.getRemoteBasePath()
						+ city.getValue() + "/" + ReportType.TICKETREASSIGNMENT;

				log.info("remote file path " + remoteArchPath);
				ftpUploader.uploadFile(path + file, file, remoteArchPath);

			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		ftpUploader.disconnect();
	 

	}

	public static String createFileName() {

		SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY HH:mm");
		String date = format.format(new Date());
		date = date.replaceAll("-", "_").replaceAll(":", "_")
				.replaceAll("\\s", "__");

		return "/TicketReAssignment_" + date + ".xlsx";

	}
	
	
	
	
}
