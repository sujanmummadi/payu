package com.cupola.fwmp.service.workStage;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.workStage.WorkStageDAO;
import com.cupola.fwmp.persistance.entities.WorkStage;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.vo.WorkStageVo;

public class WorkStageCoreServiceImpl implements WorkStageCoreService {

	@Autowired
	WorkStageDAO workStageDAO ;
	
	@Override
	public APIResponse addWorkStage(WorkStage workStage) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getWorkStageById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse getAllWorkStage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public APIResponse getWorkStages() {
		
		List<WorkStageVo>  list = new ArrayList<WorkStageVo>();
			list.add(WorkOrderDefinitionCache.getWorkStageById(WorkStageType.FIBER));
			list.add(WorkOrderDefinitionCache.getWorkStageById(WorkStageType.COPPER));
		return ResponseUtil.createSuccessResponse().setData(list);
	}

	@Override
	public APIResponse deleteWorkStage(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public APIResponse updateWorkStage(WorkStage workStage) {
		// TODO Auto-generated method stub
		return null;
	}

}
