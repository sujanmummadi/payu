package com.cupola.fwmp.service.workStage;

import com.cupola.fwmp.persistance.entities.WorkStage;
import com.cupola.fwmp.response.APIResponse;

public interface WorkStageCoreService {

	public APIResponse addWorkStage(WorkStage workStage);

	public APIResponse getWorkStageById(Long id);

	public APIResponse getAllWorkStage();

	public APIResponse deleteWorkStage(Long id);

	public APIResponse updateWorkStage(WorkStage workStage);

	APIResponse getWorkStages();

}
