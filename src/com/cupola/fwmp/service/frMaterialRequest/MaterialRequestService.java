package com.cupola.fwmp.service.frMaterialRequest;


import java.util.List;





import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.MaterialsVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.fr.MaterialRequestInputVo;
import com.cupola.fwmp.vo.fr.MaterialRequestVo;


public interface MaterialRequestService
{
public APIResponse addMaterialRequest(MaterialRequestInputVo materialRequestInputVo);

public List<MaterialRequestVo> getMaterialRequestDetails(Long ticketId);

public List<TicketAcitivityDetailsVO> getMaterialDetailsByTicket(Long ticketId);

public List<MaterialsVO> getAllMaterialForFr();

public APIResponse addFrMaterialRequest(MaterialRequestInputVo materialFrRequest);
public APIResponse addFrMaterialConsumption(TicketAcitivityDetailsVO materialConsumption);


}
