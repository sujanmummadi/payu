package com.cupola.fwmp.service.frMaterialRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.dao.materials.MaterialsDAO;
import com.cupola.fwmp.dao.mongo.material.MaterialRequestDAO;
import com.cupola.fwmp.persistance.entities.FrMaterialDetail;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.MaterialConstants;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.util.UomName;
import com.cupola.fwmp.vo.MaterialsVO;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.fr.MaterialRequestInputVo;
import com.cupola.fwmp.vo.fr.MaterialRequestVo;

public class MaterialRequstServiceImpl implements MaterialRequestService
{
	@Autowired
	MaterialRequestDAO materialRequestDAO;
	
	@Autowired
	private MaterialsDAO materialsDAO;

	private static Logger LOGGER = Logger
			.getLogger(MaterialRequstServiceImpl.class.getName());

	@Override
	public APIResponse addMaterialRequest(MaterialRequestInputVo materialRequestInputVo)
	{

		Map<String, String> subActivityOfMaterialRequest = materialRequestInputVo != null ? materialRequestInputVo
				.getSubActivityDetail() : null;

		LOGGER.info("subactivity of material request is::"
				+ subActivityOfMaterialRequest);

		if (materialRequestInputVo !=null && subActivityOfMaterialRequest != null
				&& !subActivityOfMaterialRequest.isEmpty())
		{
			List<MaterialRequestVo> materialRequestVoList=new ArrayList<MaterialRequestVo>();
			for (Map.Entry<String, String> subActivityId : subActivityOfMaterialRequest
					.entrySet())
			{

				MaterialRequestVo materialRequestVo = new MaterialRequestVo();
				
				
				materialRequestVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				
				Integer materialId = Integer.valueOf(subActivityId.getKey());

				boolean materialValue = Boolean.valueOf(subActivityId.getValue());

				materialRequestVo.setTicketId(Long.valueOf(materialRequestInputVo
						.getTicketId()));
				materialRequestVo.setMaterialRequestId(materialId);
				materialRequestVo.setMaterialRequestValue(materialValue);
				materialRequestVo.setMaterialRequestBy(1L);
				materialRequestVo.setMaterialRequestOn(new Date());
				materialRequestVo.setStatus(FRStatus.NOT_MODEFIED);
				
				materialRequestVoList.add(materialRequestVo);
				
			}
			
			LOGGER.info("List of object of materialRequestVO that are to be add in mongodb :::"+materialRequestVoList);
			  
			materialRequestDAO.addMaterialRequest(materialRequestVoList);
			  
			  return ResponseUtil.createSaveSuccessResponse();
		}
		else{
			return ResponseUtil.createSaveFailedResponse();
		}

		
	}

	@Override
	public List<MaterialRequestVo> getMaterialRequestDetails(Long ticketId)
	{
		
		LOGGER.info("getMaterialRequestDetails  ticketId :::"+ticketId);
		
		List<MaterialRequestVo> materialList = materialRequestDAO
				.getMaterialRequestDetails(ticketId);

		if (materialList != null)
		{

			return materialList;
		} else
		{

			return new ArrayList<>();
		}

	}
	
	@Override
	public List<TicketAcitivityDetailsVO> getMaterialDetailsByTicket( Long ticketId )
	{
		LOGGER.info("Getting Materail Request and Conumption detail for Tickets :"+ticketId);
		
		List<TicketAcitivityDetailsVO> responseVo = new ArrayList<TicketAcitivityDetailsVO>();
		
		if( ticketId == null || ticketId.longValue() <= 0 )
			return responseVo;
		
		try
		{

			List<FrMaterialDetail> materialsDetail = materialsDAO.getMaterialsDetailByFrTicket(ticketId);
			List<Integer> materialIds = new ArrayList<Integer>();
			
			TicketAcitivityDetailsVO materialRequestDetail = new TicketAcitivityDetailsVO();
			TicketAcitivityDetailsVO materialConsumptionDetail = new TicketAcitivityDetailsVO();
			
			Map<String, String> requestActivity = new HashMap<String, String>();
			Map<String, String> consumeActivity = new HashMap<String, String>();
			
			materialRequestDetail.setActivityId(2+"");
			materialRequestDetail.setTicketId(ticketId+"");
			
			materialConsumptionDetail.setActivityId(3+"");
			materialConsumptionDetail.setTicketId(ticketId+"");
			
			String consumedMaterial = null;
			String requestedMaterial = null;
			boolean isMateriaConsume = false;
			if ( materialsDetail != null && !materialsDetail.isEmpty() )
			{
				for (FrMaterialDetail details : materialsDetail )
				{
					materialIds.add(Integer.valueOf(details.getMaterialId()));
					setMaterialRequestDetail( details , requestActivity );
					setMaterialConsumptionDetail( details , consumeActivity );
					
					consumedMaterial = details.getConsumedMaterial();
					if( consumedMaterial != null )
						isMateriaConsume = true;
					
					requestedMaterial = details.getNotifiedToBOR();
				}
				
				setDefaultFrMaterialInList( materialIds, requestActivity ,  MaterialConstants.MATERIA_REQ );
				setDefaultFrMaterialInList( materialIds, consumeActivity ,  MaterialConstants.MATERIA_CON );
				
			}
			/*else
			{
				setDefaultFrMaterialInList( materialIds, requestActivity ,  MaterialConstants.MATERIA_REQ);
				setDefaultFrMaterialInList( materialIds, consumeActivity ,  MaterialConstants.MATERIA_CON );
			}*/
			
			materialRequestDetail.setSubActivityDetail(requestActivity);
			materialConsumptionDetail.setSubActivityDetail(consumeActivity);
			
			if( isMateriaConsume )
				responseVo.add(materialConsumptionDetail);
			
			if( requestedMaterial != null   )
				responseVo.add(materialRequestDetail);
		
		} 
		catch (Exception e)
		{
			LOGGER.error("Error occure while getting material req & comsumption details :"+ticketId);
			e.printStackTrace();
		}
		return responseVo;
	}
	
	private void setDefaultFrMaterialInList( List<Integer> materialIds, 
			Map<String, String> materialDetail ,String reqOrCon )
	{
		if( materialDetail == null || reqOrCon == null )
			return;
		
		List<Integer> totalMaterials = MaterialConstants.FR_MATERIAL;
		totalMaterials.removeAll(materialIds);
		
		for( Integer material : totalMaterials)
		{
			if( reqOrCon.equals( MaterialConstants.MATERIA_REQ ))
				materialDetail.put(material+"", "false");
			
			else if( reqOrCon.equals( MaterialConstants.MATERIA_CON ))
			{
				if (material == MaterialConstants.COPPER_CABLE
						|| material == MaterialConstants.FIBER_CABLE)
					
//					materialDetail.put(material+"","StartPoint=0,EndPoint=0");
					
					materialDetail.put(material+"",null);
				
				else if (material == MaterialConstants.ROUTER
						|| material == MaterialConstants.CX_MAC_ID
						||material == MaterialConstants.Battery_SERIAL_N0
						|| material == MaterialConstants.CX_RACK
						|| material == MaterialConstants.SFP)
					
//					materialDetail.put(material+"",0+"");
					materialDetail.put(material+"",null);
				
				else if( material == MaterialConstants.FIBER_TYPE)
//					materialDetail.put(material+"","Select Fiber Type");
					
					materialDetail.put(material+"",null);
				
				else if( material == MaterialConstants.DRUM_NO)
//					materialDetail.put(material+"", 0+"");
					materialDetail.put(material+"", null);
				
				else
//					materialDetail.put(material+"", "0");
					materialDetail.put(material+"", null);
			}
		}
	}
	
	private void setMaterialRequestDetail( FrMaterialDetail details ,
			Map<String, String> requestActivity )
	{
		if( details == null || requestActivity == null )
			return;
		
		else
		{
			requestActivity.put(details.getMaterialId(), details.getMaterialRequested());
		}
	}
	
	private void setMaterialConsumptionDetail( FrMaterialDetail details ,
			Map<String, String> consumeActivity)
	{
		if( details == null || consumeActivity == null )
			return;
		
		else
		{
			int materialId = details.getMaterialId() != null ? Integer.valueOf(details.getMaterialId()) : 0;
			if (materialId == MaterialConstants.COPPER_CABLE
					|| materialId == MaterialConstants.FIBER_CABLE)
			{
				String value = details.getMaterialStartEndPoint() /*!= null 
						? details.getMaterialStartEndPoint() :"StartPoint=0,EndPoint=0"*/;
				consumeActivity.put(details.getMaterialId(), value);
			}
			
			else if (materialId == MaterialConstants.ROUTER
					|| materialId == MaterialConstants.CX_MAC_ID
					||materialId == MaterialConstants.Battery_SERIAL_N0
					|| materialId == MaterialConstants.CX_RACK
					|| materialId == MaterialConstants.SFP)
			{
				String value = details.getMaterialUniqNumber() /*!= null 
						? details.getMaterialUniqNumber() :"0"*/;
						
				consumeActivity.put(details.getMaterialId(), value );
			}
			
			else if( materialId == MaterialConstants.FIBER_TYPE)
			{
				String value = details.getMaterialFiberType() /*!= null 
						? details.getMaterialFiberType() :"Select Fiber Type"*/;
						
				consumeActivity.put(details.getMaterialId(), value );
			}
			
			else if( materialId == MaterialConstants.DRUM_NO)
			{
				String value = details.getMaterialDrumNumber() /*!= null 
						? details.getMaterialDrumNumber() :"0"*/;
						
				consumeActivity.put(details.getMaterialId(), value );
			}
			
			else
			{
				String value = details.getMaterialConsumedQty()/* != null 
						? details.getMaterialConsumedQty() :"0"*/;
						
				consumeActivity.put(details.getMaterialId(), value );
			}
		}
	}

	/*
	 * @Override public APIResponse getMaterialRequestDetailsByTicketId(Long
	 * ticketId) {
	 * 
	 * return materialRequestDAO.getMaterialRequestDetailsByTicketId(ticketId) ;
	 * }
	 */
	
	@Override
	public List<MaterialsVO> getAllMaterialForFr()
	{
		try
		{
			List<MaterialsVO> materials = materialsDAO.getAllMaterials();
			if( materials == null )
				return new ArrayList<MaterialsVO>();
			else
				return materials;
		} 
		catch (Exception e)
		{
			LOGGER.error("Error occure while getting material list :");
			e.printStackTrace();
			return new ArrayList<MaterialsVO>();
		}
		
	}

	@Override
	public APIResponse addFrMaterialRequest( MaterialRequestInputVo materialFrRequest )
	{
		if( materialFrRequest == null || materialFrRequest.getTicketId() == null 
				|| materialFrRequest.getTicketId().isEmpty())
			
			return ResponseUtil.nullArgument();

		LOGGER.info("******** Requesting Material For Ticket :"+ materialFrRequest.getTicketId()+" ***********");
		
		try
		{
			Map<String, String> subActivityOfMaterialRequest = materialFrRequest
					.getSubActivityDetail();

			if (subActivityOfMaterialRequest != null && !subActivityOfMaterialRequest.isEmpty())
			{
				List<FrMaterialDetail> materialRequestVoList = new ArrayList<FrMaterialDetail>();
				FrMaterialDetail materialRequestVo = null;
				int materialId = 0;
				
				for (Map.Entry<String, String> subActivityId : subActivityOfMaterialRequest
						.entrySet())
				{
					materialId = Integer.valueOf(subActivityId.getKey());
					if( materialId == 0 )
						continue;
					
					materialRequestVo = new FrMaterialDetail();
					materialRequestVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					materialRequestVo.setTicket(Long.valueOf(materialFrRequest.getTicketId()));
					materialRequestVo.setMaterialId(subActivityId.getKey());
					materialRequestVo.setMaterialRequested(subActivityId.getValue());
					materialRequestVo.setMaterialRequestQty(materialFrRequest.getMaterialQty());
					
					
					if (materialId == MaterialConstants.COPPER_CABLE
							|| materialId == MaterialConstants.FIBER_CABLE)
						
						materialRequestVo.setMaterialUnit(String
								.valueOf(UomName.METERS));
					else
						materialRequestVo.setMaterialUnit(String
								.valueOf(UomName.NoS));
					
					
					materialRequestVo.setAddedOn(new Date());
					materialRequestVo.setAddedBy(AuthUtils.getCurrentUserId()+"");
					materialRequestVo.setModifiedOn(new Date());
					materialRequestVo.setModifiedBy(AuthUtils.getCurrentUserId()+"");
					materialRequestVo.setNotifiedToBOR(FRStatus.NOT_MODEFIED+"");
					
					materialRequestVoList.add(materialRequestVo);
				}
				
				int updateRoe = 0;
				/*if( !materialFrRequest.isEditable() )
				{
					updateRoe = materialsDAO.addFrMaterialRequest( materialRequestVoList );
					if( updateRoe > 0 )
						return ResponseUtil.createSuccessResponse();
				}
				else
				{*/
					updateRoe = materialsDAO.updateFrMaterialRequest( materialRequestVoList );
					if( updateRoe > 0 )
						return ResponseUtil.createSuccessResponse();
				/*}*/
			}
			
		} 
		catch (Exception e)
		{
			LOGGER.error("Error occure while adding Fr Material Request");
			e.printStackTrace();
		}
		return ResponseUtil.createSaveFailedResponse();
	}

	@Override
	public APIResponse addFrMaterialConsumption(
			TicketAcitivityDetailsVO materialConsumption)
	{
		if( materialConsumption == null || materialConsumption.getTicketId() == null 
				|| materialConsumption.getTicketId().isEmpty())
			
			return ResponseUtil.nullArgument();
		
		LOGGER.info("******** Updating Material comsumption for :"+ materialConsumption.getTicketId()+" ***********");
		try{
			Map<String, String> subActivityOfMaterialRequest = materialConsumption
					.getSubActivityDetail();

			if (subActivityOfMaterialRequest != null && !subActivityOfMaterialRequest.isEmpty())
			{
				List<FrMaterialDetail> materialRequestVoList = new ArrayList<FrMaterialDetail>();
				FrMaterialDetail materialRequestVo = null;
				int materialId = 0;
				List<Integer> materialIds = new ArrayList<Integer>();
				
				for (Map.Entry<String, String> subActivityId : subActivityOfMaterialRequest
						.entrySet())
				{
					materialId = Integer.valueOf(subActivityId.getKey());
					if( materialId == 0 )
						continue;
					
					materialRequestVo = new FrMaterialDetail();
					materialRequestVo.setTicket(Long.valueOf(materialConsumption.getTicketId()));
					materialRequestVo.setMaterialId(materialId+"");
					materialIds.add(materialId);
					
					if (materialId == MaterialConstants.COPPER_CABLE
							|| materialId == MaterialConstants.FIBER_CABLE)
						materialRequestVo.setMaterialStartEndPoint(subActivityId.getValue());

					else if (materialId == MaterialConstants.ROUTER
							|| materialId == MaterialConstants.CX_MAC_ID)
						materialRequestVo.setMaterialUniqNumber(subActivityId.getValue());
					
					else if (materialId == MaterialConstants.Battery_SERIAL_N0
							|| materialId == MaterialConstants.CX_RACK
							|| materialId == MaterialConstants.SFP)
						materialRequestVo.setMaterialUniqNumber(subActivityId.getValue());
					
					else if( materialId == MaterialConstants.FIBER_TYPE)
						materialRequestVo.setMaterialFiberType(subActivityId.getValue());
					
					else if( materialId == MaterialConstants.DRUM_NO)
						materialRequestVo.setMaterialDrumNumber(subActivityId.getValue());
					
					else
						materialRequestVo.setMaterialConsumedQty(subActivityId.getValue());
					
					materialRequestVo.setConsumptionAddedOn(new Date());
					materialRequestVo.setConsumptionAddedBy(AuthUtils.getCurrentUserId()+"");
					materialRequestVo.setConsumptionModifiedOn(new Date());
					materialRequestVo.setConsumptionModifiedBy(AuthUtils.getCurrentUserId()+"");
					materialRequestVo.setConsumedMaterial(MaterialConstants.MATERIA_CON);
					materialRequestVo.setNotifiedToBOR(FRStatus.NOT_MODEFIED+"");
					
					materialRequestVoList.add( materialRequestVo );
				}
				
				int updateRow = materialsDAO.addFrMaterialConsumption( materialRequestVoList, materialIds );
				if( updateRow > 0 )
					return ResponseUtil.createSuccessResponse();
			}
		}catch(Exception e){
			LOGGER.error("Error occure while adding Fr Material Consumptions...");
			e.printStackTrace();
		}
		return ResponseUtil.createSaveFailedResponse();
	}
}
