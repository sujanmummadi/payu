package com.cupola.fwmp.service.report;

import com.cupola.fwmp.response.APIResponse;

public interface ReportCoreService {

	public APIResponse getCountWorkStageType();

	public APIResponse getStatusReport();

}
