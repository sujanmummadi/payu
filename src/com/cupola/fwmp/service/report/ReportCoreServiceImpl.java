package com.cupola.fwmp.service.report;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cupola.charts.Highchart;
import com.cupola.charts.HighchartData;
import com.cupola.charts.HighchartSeries;
import com.cupola.fwmp.FWMPConstant.ReportStatusType;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.dao.report.ReportDAO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;

public class ReportCoreServiceImpl implements ReportCoreService
{

	private Logger log = Logger
			.getLogger(ReportCoreServiceImpl.class.getName());

	ReportDAO reportDAO;

	public void setReportDAO(ReportDAO reportDAO)
	{
		this.reportDAO = reportDAO;
	}

	@Override
	public APIResponse getCountWorkStageType()
	{// Count Fiber and Copper of
		// WorkStage
		Long userId = AuthUtils.getCurrentUserId();
		Map<Long, Integer> wgCount = reportDAO.getCountWorkStageType(userId);

		if (wgCount == null || wgCount.isEmpty() )
			return ResponseUtil.createSuccessResponse();

		Integer fiberCount = wgCount.get(TicketSubCategory.FIBER_ID);
		Integer copperCount = wgCount.get(TicketSubCategory.COPPER_ID);
		Integer feasibilityCount = wgCount.get(TicketSubCategory.FEASIBILITY);

		log.debug(fiberCount + "fiber" + copperCount + "copper"
				+ feasibilityCount + "feasibilityCount");

		Highchart highchart = new Highchart();
		highchart.setChartype("pie");
		highchart.setTitleText("");
		highchart.setSubtitleText("");
		List<HighchartData> dataList = new ArrayList<HighchartData>();
		HighchartData data1 = new HighchartData();

		data1.setName(TicketSubCategory.FIBER);
		if (fiberCount != null)
			data1.setY(Double.valueOf(fiberCount));
		else
			data1.setY(0.0);

		data1.setColor("#0040ff");
		dataList.add(data1);
		HighchartData data2 = new HighchartData();
		data2.setName(TicketSubCategory.COPPER);
		if (copperCount != null)
			data2.setY(Double.valueOf(copperCount));
		else
			data2.setY(0.0);

		data2.setColor("#ff8000");
		dataList.add(data2);

		HighchartData data3 = new HighchartData();
		data3.setName(TicketSubCategory.FISIBLE);
		if (feasibilityCount != null)
			data3.setY(Double.valueOf(feasibilityCount));
		else
			data3.setY(0.0);

		data3.setColor("#ff6783");
		dataList.add(data3);
		HighchartSeries series = new HighchartSeries();
		series.setData(dataList);
		List<HighchartSeries> seriesList = new ArrayList<HighchartSeries>();
		seriesList.add(series);
		highchart.setSeries(seriesList);
		highchart
				.setTooltipHeaderFormat("<span style=\"font-size: 10px\">{point.key} : <b> {point.y}</b></span> <span style=\"font-size: 15px;color:{point.color} \"><b></b> </span><br/>");
		highchart.setTooltipPointFormat("");
		highchart.setExportingButtonsContextButtonEnabled(false);
		highchart.setLegendAlign("right");
		highchart.setLegendVerticalAlign("top");
		highchart.setLegendLayout("vertical");
		highchart.setLegendX(0);
		highchart.setLegendY(80);
		highchart.setSizeHeight(160);
		highchart.setTitleStyleFontSize("14px");

		highchart.setLegendEnabled(true);
		highchart.setCreditsEnabled(false);

		return ResponseUtil.createSuccessResponse().setData(highchart);

	}

	@Override
	public APIResponse getStatusReport()
	{

		Long userId = AuthUtils.getCurrentUserId();

		if (userId == null)
		{
			log.info("not found user");
			return ResponseUtil.createLoginFailureResponse("user not found");
		}

		Map<Integer, Integer> reportCount = reportDAO.getStatusReport(userId);

		if (reportCount == null || reportCount.isEmpty() )
		{
			return ResponseUtil.createSuccessResponse();
		}
		int completedTicketCount = 0;
		int newTicketCount = 0;
		int inProgressTicketCount = 0;
		for (int i = 0; i < TicketStatus.COMPLETED_DEF.size(); i++)
		{
			if (reportCount.get(TicketStatus.COMPLETED_DEF.get(i)) != null)
				completedTicketCount = completedTicketCount
						+ reportCount.get(TicketStatus.COMPLETED_DEF.get(i));

		}

		for (int i = 0; i < TicketStatus.NEW_TICKET_DEF.size(); i++)
		{
			if (reportCount.get(TicketStatus.NEW_TICKET_DEF.get(i)) != null)
				newTicketCount = newTicketCount
						+ reportCount.get(TicketStatus.NEW_TICKET_DEF.get(i));

		}
		for (int i = 0; i < TicketStatus.IN_PROGRESS_TICKET_DEF.size(); i++)
		{
			if (reportCount.get(TicketStatus.IN_PROGRESS_TICKET_DEF.get(i)) != null)
				inProgressTicketCount = inProgressTicketCount
						+ reportCount.get(TicketStatus.IN_PROGRESS_TICKET_DEF
								.get(i));

		}

		if (completedTicketCount == 0 && newTicketCount == 0
				&& inProgressTicketCount == 0)
			return ResponseUtil.createSuccessResponse();

		Highchart highchart = new Highchart();
		highchart.setChartype("pie");
		highchart.setTitleText("");
		highchart.setSubtitleText("");
		List<HighchartData> dataList = new ArrayList<HighchartData>();
		HighchartData data1 = new HighchartData();
		data1.setName(ReportStatusType.IN_PROGRESS);
		data1.setY(Double.valueOf(inProgressTicketCount));

		data1.setColor("#0A9CD6");
		dataList.add(data1);
		HighchartData data2 = new HighchartData();
		data2.setName(ReportStatusType.COMPLETED);

		data2.setY(Double.valueOf(completedTicketCount));
		data2.setColor("#71C755");
		dataList.add(data2);
		HighchartData data3 = new HighchartData();
		data3.setName(ReportStatusType.NEW_TICKET);

		data3.setY(Double.valueOf(newTicketCount));
		data3.setColor("#1B3366");
		dataList.add(data3);

		highchart.setLegendEnabled(false);
		HighchartSeries series = new HighchartSeries();
		series.setData(dataList);
		List<HighchartSeries> seriesList = new ArrayList<HighchartSeries>();
		seriesList.add(series);
		highchart.setSeries(seriesList);
		highchart.setExportingButtonsContextButtonEnabled(false);
		highchart
				.setTooltipHeaderFormat("<span style=\"font-size: 10px\">{point.key} : <b> {point.y}</b></span> <span style=\"font-size: 15px;color:{point.color} \"><b></b> </span><br/>");
		highchart.setTooltipPointFormat("");
		highchart.setLegendAlign("right");
		highchart.setLegendVerticalAlign("top");
		highchart.setLegendLayout("vertical");
		highchart.setLegendX(0);
		highchart.setLegendY(70);
		highchart.setTitleStyleFontSize("14px");
		highchart.setSizeHeight(160);

		highchart.setLegendEnabled(true);
		highchart.setCreditsEnabled(false);

		return ResponseUtil.createSuccessResponse().setData(highchart);

	}
}
