package com.cupola.fwmp.auth;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.web.filter.DelegatingFilterProxy;

import com.cupola.fwmp.util.ApplicationUtils;

public class AppAuthFilterProxy extends DelegatingFilterProxy
{
	Logger LOGGER = LogManager.getLogger(AppAuthFilterProxy.class);
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws ServletException, IOException
	{
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		
		ApplicationUtils.saveHttpRequestInfo (servletRequest);
		super.doFilter(request, response, filterChain);
	}
}
