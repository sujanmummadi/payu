package com.cupola.fwmp.auth;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class AppUser extends User
{

	private long id ;
	private List<String> roles ;
	private String userDisplayName;
	
	private Map<Long,String> cities ;
	private Map<Long,String> branches ;
	private Map<Long,String> areas ;
	private Map<Long,String> userGroup ;
	
	public AppUser(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities)
	{
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public List<String> getRoles()
	{
		return roles;
	}

	public void setRoles(List<String> roles)
	{
		this.roles = roles;
	}

	public String getUserDisplayName()
	{
		return userDisplayName;
	}

	public void setUserDisplayName(String userDisplayName)
	{
		this.userDisplayName = userDisplayName;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return "AppUser [id=" + id + ", roles=" + roles + ", userDisplayName="
				+ userDisplayName + "]";
	}

	public Map<Long, String> getCities()
	{
		return cities;
	}

	public void setCities(Map<Long, String> cities)
	{
		this.cities = cities;
	}

	public Map<Long, String> getBranches()
	{
		return branches;
	}

	public void setBranches(Map<Long, String> branches)
	{
		this.branches = branches;
	}

	public Map<Long, String> getAreas()
	{
		return areas;
	}

	public void setAreas(Map<Long, String> areas)
	{
		this.areas = areas;
	}

	public Map<Long, String> getUserGroup()
	{
		return userGroup;
	}

	public void setUserGroup(Map<Long, String> userGroup)
	{
		this.userGroup = userGroup;
	}

}
