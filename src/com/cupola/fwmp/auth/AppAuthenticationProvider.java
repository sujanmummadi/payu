package com.cupola.fwmp.auth;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.service.domain.HttpRequestInfo;
import com.cupola.fwmp.util.ApplicationUtils;
//import org.springframework.security.authentication.encoding.PasswordEncoder;
public class AppAuthenticationProvider extends DaoAuthenticationProvider
{

	private UserDetailsService appUserDetailsService;
	final static Logger log = Logger.getLogger(AppAuthenticationProvider.class);
	private PasswordEncoder passwordEncoder;
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException
	{
		
		HttpRequestInfo request = ApplicationUtils.retrieveHttpRequestInfo();
		
		
		// TODO Auto-generated method stub
		String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        
        if( "".equals(username) || "".equals(password) )//  username == null && password == null && request.getParameters() != null)
        {
        	username = request.getParameters().get("username" );
        	password = request.getParameters().get("password" );
        }
     
          UserDetails user = appUserDetailsService.loadUserByUsername(username);
        
          if (user == null || !user.getUsername().equalsIgnoreCase(username)) {
        	  log.error("Username not found. "+username);
              throw new BadCredentialsException("Username not found.");
          }
   
   
          if (!this.passwordEncoder.matches(password, user.getPassword())
        		  )
          {
        	  log.debug("Wrong password."+username  + " password " + password);
        	  
        		  throw new BadCredentialsException("Wrong password.");
        	  
          }
          Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
   
          return new UsernamePasswordAuthenticationToken(user, password, authorities);
	}
	
	
	protected void doAfterPropertiesSet() throws Exception
	{
		Assert.notNull(this.appUserDetailsService, "A AppUserDetailsService must be set");
	}
	
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException
	{

		if (authentication.getCredentials() == null)
		{
			this.logger.debug("Authentication failed: no credentials provided");

			throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
		}

		String presentedPassword = authentication.getCredentials().toString();

		if ( this.passwordEncoder.matches(presentedPassword, userDetails.getPassword()))
			return;
		this.logger
				.debug("Authentication failed: password does not match stored value");

		throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
	}

	@Override
	public boolean supports(Class<?> arg0)
	{
		// TODO Auto-generated method stub
		return true;
	} 




	public void setPasswordEncoder(PasswordEncoder passwordEncoder)
	{
		this.passwordEncoder = passwordEncoder;
	}


	public UserDetailsService getAppUserDetailsService()
	{
		return appUserDetailsService;
	}


	public void setAppUserDetailsService(UserDetailsService appUserDetailsService)
	{
		this.appUserDetailsService = appUserDetailsService;
	}


	/*public UserDetailsService getUserDetailsService()
	{
		return userDetailsService;
	}


	public void setUserDetailsService(UserDetailsService userDetailsService)
	{
		this.userDetailsService = userDetailsService;
	}*/



}
