package com.cupola.fwmp.dao.userGroup;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.persistance.entities.Role;
import com.cupola.fwmp.persistance.entities.UserGroup;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserGroupVo;
import com.cupola.fwmp.vo.user.UserGroups;

@Transactional
public class UserGroupDAOImpl implements UserGroupDAO
{

	private static final Logger log = LogManager
			.getLogger(UserGroupDAOImpl.class.getName());

	private HibernateTemplate hibernateTemplate;

	public static Map<Long, UserGroupVo> cachedUserGroups = new ConcurrentHashMap<Long, UserGroupVo>();

	public Map<Long, String> groupNames = new TreeMap<>();

	public Map<String, Long> groupNameAndIdMap = new TreeMap<String, Long>();

	public Map<Long, String> roleNames = new TreeMap<>();
	public static Map<String, Long> roleIdRoleMap = new ConcurrentHashMap<String, Long>();
	public static Map<Long, Set<String>> useGroupAndRoles = new TreeMap<Long, Set<String>>();

	public static Map<Long, Set<TypeAheadVo>> useGroupAndRolesTA = new ConcurrentHashMap<Long, Set<TypeAheadVo>>();

	public static Map<Long, String> useGroupCache = new TreeMap<Long, String>();

	public static Map<Long, List<TypeAheadVo>> useGroupAndEditableGroupCache = new TreeMap<Long, List<TypeAheadVo>>();

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	DefinitionCoreService definitionCoreService;
	// private static final String GET_ALL_USERGROUPS = "select * from
	// UserGroup";
	private static final String GET_ALL_USERGROUPS = "select id,userGroupcode,userGroupName,userGroupDesc,userGroupReportTo from UserGroup";

	private void init()
	{

		log.info("UserGroupDAO init method called..");
		resetCachedUserGroupNames();
		initUserGroupRoleMapping();
		log.info("UserGroupDAO init method executed.");
	}

	private void initUserGroupRoleMapping()
	{
		log.debug(" inside initUserGroupRoleMapping  ");
		
		String query = "select ug.id as userGroupId,ug.userGroupName as userGroupName ,r.id as roleId,r.roleName as roleName,"
				+ "r.roleCode as roleCode,r.roleDesc as roleDesc"
				+ " from UserGroupRoleMapping ugum Join UserGroup ug on ug.id=ugum.idUserGroup "
				+ "JOin Role r on r.id=ugum.idRole ";

		// List<UserGroup> groupList = (List<UserGroup>) hibernateTemplate
		// .find("from UserGroup");

		List<UserGroups> groupList = jdbcTemplate
				.query(query, new UserGroupRolesMappingMapper());

		for (Iterator<UserGroups> iterator = groupList.iterator(); iterator
				.hasNext();)
		{
			UserGroups userGroup = (UserGroups) iterator.next();
			
			if (useGroupAndRoles.containsKey(userGroup.getUserGroupId()))
			{
				Set<String> roles = useGroupAndRoles
						.get(userGroup.getUserGroupId());

				roles.add(userGroup.getRoleCode());

				useGroupAndRoles.put(userGroup.getUserGroupId(), roles);

				Set<TypeAheadVo> aheadVos = useGroupAndRolesTA
						.get(userGroup.getUserGroupId());

				aheadVos.add(new TypeAheadVo(userGroup.getRoleId(), userGroup
						.getRoleCode()));

				useGroupAndRolesTA.put(userGroup.getUserGroupId(), aheadVos);

			} else
			{
				Set<String> roles = new LinkedHashSet<String>();
				roles.add(userGroup.getRoleCode());
				
				useGroupAndRoles.put(userGroup.getUserGroupId(), roles);
			
				useGroupCache.put(userGroup.getUserGroupId(), userGroup.getUserGroupName());
				
				Set<TypeAheadVo> aheadVos = new LinkedHashSet<TypeAheadVo>();

				aheadVos.add(new TypeAheadVo(userGroup.getRoleId(), userGroup
						.getRoleCode()));

				useGroupAndRolesTA.put(userGroup.getUserGroupId(), aheadVos);

			}

			roleIdRoleMap.put(userGroup.getRoleName(), userGroup.getRoleId());
		}

		log.info("############## useGroupAndRoles ###############3 "
				+ useGroupAndRoles);

	}

	@Override
	public Set<TypeAheadVo> getuseGroupAndRolesTAByGroupId(Long groupId)
	{
		if(groupId == null || groupId <= 0)
			return null;
		
		log.debug(" inside getuseGroupAndRolesTAByGroupId  "+groupId);
		
		if (useGroupAndRolesTA.containsKey(groupId))
			return useGroupAndRolesTA.get(groupId);
		else
			return null;
	}

	@Override
	public Long getRoleIdByRoleName(String roleName)
	{
		 if(roleName == null || roleName.isEmpty())
				return null;
		 
		log.debug(" inside getRoleIdByRoleName  "+roleName);
			
		if (roleIdRoleMap.containsKey(roleName))
		{
			log.debug(" Exitting from  getRoleIdByRoleName  "+roleName);
			return roleIdRoleMap.get(roleName);
		} else
		{
			log.debug(" Exitting from  getRoleIdByRoleName  "+roleName);
			return null;
		}
	}

	@Override
	public List<String> getRolesByUserGroupId(Long groupId)
	{
		List<String> roles = new ArrayList<>();
		 if(groupId == null || groupId <= 0)
				return roles;
		 
		 log.debug(" inside getRolesByUserGroupId  "+groupId);
		 
		if (useGroupAndRoles.containsKey(groupId))
		{
			roles.addAll(useGroupAndRoles.get(groupId));
			log.debug(" Exitting from  getRolesByUserGroupId  "+groupId);
			return roles;
		} else
			{
			log.debug(" Exitting from  getRolesByUserGroupId  "+groupId);
			return roles;
	}
  }
	@Override
	public List<TypeAheadVo> getEditableGroupByCurrentUser()
	{
		log.debug(" inside getEditableGroupByCurrentUser  ");
		
		long goupId = AuthUtils.getCurrentUserGroup().getId();
		
		if (useGroupAndEditableGroupCache.containsKey(goupId))
		{
			log.debug(" Exitting from  getEditableGroupByCurrentUser  ");
			return useGroupAndEditableGroupCache.get(goupId);
		} else
		{
			Set<Integer> groups = definitionCoreService
					.getStatusFilterForRole(goupId + "");
			
			List<TypeAheadVo> userGroupData = new ArrayList<TypeAheadVo>();
			
			for (Iterator<Integer> iterator = groups.iterator(); iterator.hasNext();)
			{
				Integer integer = (Integer) iterator.next();
				userGroupData.add(new TypeAheadVo(integer
						.longValue(), useGroupCache.get(integer.longValue())));
			}
			useGroupAndEditableGroupCache.put(goupId, userGroupData);
			
			log.debug(" Exitting from  getEditableGroupByCurrentUser  ");
			
			return useGroupAndEditableGroupCache.get(goupId);
		}
	}

	@Override
	public UserGroup addUserGroup(UserGroupVo userGroupVo)
	{

		UserGroup userGroup = new UserGroup();

		if (userGroupVo == null)
			return userGroup;
		
		log.debug(" inside addUserGroup  "+userGroupVo.getId());
		
		BeanUtils.copyProperties(userGroupVo, userGroup);

		userGroup.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		userGroup.setAddedBy(100l);
		userGroup.setAddedOn(new Date());
		userGroup.setModifiedBy(100l);
		userGroup.setModifiedOn(new Date());
		userGroup.setStatus(1);

		Set<Role> roleSet = new HashSet<Role>();

		List<TypeAheadVo> roles = userGroupVo.getRoles();

		for (TypeAheadVo roleName : roles)
		{

			log.debug("role:" + roleName);

			Role role = new Role();

			role.setId(roleName.getId());

			roleSet.add(role);

		}

		userGroup.setRoles(roleSet);

		long userGroupGuid = (Long) hibernateTemplate.save(userGroup);

		log.info("user group id:" + userGroupGuid);
		userGroup.setId(userGroupGuid);
		userGroupVo.setId(userGroupGuid);
		groupNames.put(userGroup.getId(), userGroup.getUserGroupName());
		groupNameAndIdMap.put(userGroup.getUserGroupName(), userGroupGuid);
		cachedUserGroups.put(userGroupGuid, userGroupVo);

		if (userGroupGuid > 0)
		{
			log.debug(" Exitting from  addUserGroup  "+userGroupVo);
			return userGroup;
		}

		else
		{
			UserGroup usrgrp = new UserGroup();
			usrgrp.setStatus(StatusCodes.SAVE_FAILED);
			log.debug(" Exitting from  addUserGroup  "+userGroupVo);
			return usrgrp;
		}
	}

	@Override
	public UserGroup updateUserGroup(UserGroupVo userGroupVo)
	{
		if(userGroupVo == null)
			return new UserGroup();

		log.debug(" inside updateUserGroup  "+userGroupVo.getUserGroupName());
		
		UserGroup usrGroup = hibernateTemplate
				.get(UserGroup.class, userGroupVo.getId());

		
		BeanUtils.copyProperties(userGroupVo, usrGroup);
		usrGroup.setModifiedOn(new Date());
		usrGroup.setModifiedBy(100l);
		usrGroup.setStatus(1);

		long groupGuid = 0;

		List<TypeAheadVo> roles = userGroupVo.getRoles();

		Set<Role> roleSet = new HashSet<Role>();

		for (TypeAheadVo roleName : roles)
		{

			log.debug("role:" + roleName);

			Role role = new Role();

			role.setId(roleName.getId());

			roleSet.add(role);

		}
		usrGroup.setRoles(roleSet);

		hibernateTemplate.update(usrGroup);

		cachedUserGroups.put(userGroupVo.getId(), userGroupVo);
		groupNames.put(userGroupVo.getId(), userGroupVo.getUserGroupName());
		groupNameAndIdMap
				.put(userGroupVo.getUserGroupName(), userGroupVo.getId());
		if (groupGuid > 0)

		{
			log.info(" Exitting from updateUserGroup  "+userGroupVo.getUserGroupName());
			return usrGroup;
		}

		else
		{
			UserGroup group = new UserGroup();
			group.setStatus(StatusCodes.UPDATE_FAILED);
			
			log.debug(" Exitting from updateUserGroup  "+userGroupVo.getUserGroupName());
			
			return group;
		}
	}

	@Override
	public UserGroup deleteUserGroup(UserGroupVo userGroupVo)
	{
		if( userGroupVo == null )
			return null;

		log.debug(" inside deleteUserGroup  "+userGroupVo.getId());
		
		try {
		UserGroup usrGroup = hibernateTemplate
				.get(UserGroup.class, userGroupVo.getId());

		groupNames.remove(userGroupVo.getId());

		if(cachedUserGroups!=null)
		
		cachedUserGroups.remove(userGroupVo.getId());

		log.info("group name contains:" + groupNames);

		hibernateTemplate.delete(usrGroup);

		usrGroup.setStatus(0);
		return usrGroup;
		} catch (Exception e) {
			log.error("Error occured while deleteUserGroup",e);
			e.printStackTrace();
		}
		log.debug(" exit from  deleteUserGroup  "+userGroupVo.getId());
		return null;
	}

	@Override
	public List<UserGroupVo> getAllUserGroups()
	{
		log.debug(" inside getAllUserGroups  ");

		if (cachedUserGroups.size() == 0)
			resetCachedUserGroups();
		
		if(cachedUserGroups!=null && !cachedUserGroups.isEmpty()){
		return new ArrayList<UserGroupVo>(cachedUserGroups.values());
		}else{
			return new ArrayList<>();
		}

	}

	@Override
	public Map<Long, String> getAllGroupNames()
	{
		log.debug(" inside getAllGroupNames  ");

		if (groupNames.size() == 0)
			resetCachedUserGroupNames();
		
		log.debug(" Exitting from  getAllGroupNames  ");
		
		return groupNames;
	}

	@Override
	public Map<Long, String> getAllRoleNames()
	{
		log.debug(" inside getAllRoleNames  ");

		if (roleNames.size() == 0)
			resetCachedRoles();
		
		log.debug(" Exitting from getAllRoleNames  ");
		
		return roleNames;
	}

	private void resetCachedUserGroups()
	{

		log.debug("inside getAllUserGroups");

		List<UserGroupVo> userGroupVos = new ArrayList<>();

		try
		{

			String query = "from UserGroup where status <> 0";

			List<UserGroup> userGroups = (List<UserGroup>) hibernateTemplate
					.find(query);

			if (userGroups != null && !userGroups.isEmpty())
			{

				log.info("size::::" + userGroups.size());

				for (UserGroup userGroup : userGroups)
				{

					groupNames.put(userGroup.getId(), userGroup
							.getUserGroupName());

					List<TypeAheadVo> roleNames = new ArrayList<>();

					Set<Role> roles = userGroup.getRoles();

					for (Role role : roles)
					{

						roleNames.add(new TypeAheadVo(role.getId(), role
								.getRoleCode()));

					}

					UserGroupVo groupVo = new UserGroupVo();

					BeanUtils.copyProperties(userGroup, groupVo);

					groupVo.setStatus("enabled");
					groupVo.setRoles(roleNames);

					cachedUserGroups.put(userGroup.getId(), groupVo);
					userGroupVos.add(groupVo);
				}

			}
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Failed to get users. Reason:" + e.getMessage());
		}
		log.debug("Exitting from  getAllUserGroups");
	}

	@Transactional
	private void resetCachedUserGroupNames()
	{

		log.info("Fetching All Groups to which the user has to be assigned ");

		try
		{

			log.debug("########## getCities START jdbcTemplate ##########"
					+ new Date());
			List<UserGroupVo> groupList = jdbcTemplate
					.query(GET_ALL_USERGROUPS, new UserGroupMapper());
			log.debug("########## getCities END jdbcTemplate ##########");

			if (groupList != null && !groupList.isEmpty())
			{

				for (UserGroupVo userGroup : groupList)
				{

					groupNames.put(userGroup.getId(), userGroup
							.getUserGroupName());

					groupNameAndIdMap.put(userGroup
							.getUserGroupName(), userGroup.getId());
				}

				log.info("Fetching All Groups from Database has been Successfully Done");

			} else
			{

				log.info("User has not been assigned to any Group");

			}

		} catch (Exception e)
		{

			log.error("Error in Fetching All Groups to which the user has to be assigned : Error Message is "
					+ e.getMessage() );

			e.printStackTrace();

		}
		log.debug("Exitting Fetching All Groups to which the user has to be assigned ");
	}

	private void resetCachedRoles()
	{

		log.info("Fetching All roles");

		try
		{

			List<Role> roles = (List<Role>) hibernateTemplate.find("from Role");

			if (roles != null && !roles.isEmpty())
			{

				for (Role role : roles)
				{

					roleNames.put(role.getId(), role.getRoleCode());
				}

				log.info("Fetching All Roles from Database has been Successfully Done"
						+ roleNames);

			} else
			{

				log.info("No Role Found In Database");

			}

		} catch (Exception e)
		{

			log.error("Error in Fetching All Roles to which the user has to be assigned : Error Message is "
					+ e.getMessage() );

			e.printStackTrace();

		}
		log.debug("Exitting from Fetching All roles");
	}

	@Override
	public UserGroup getUserGroupById(Long id)
	{
		return null;
	}

	@Override
	public List<UserGroupVo> getUserGroups(UserFilter filter)
	{
		log.debug(" inside getUserGroups  ");
		Criteria criteria = hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createCriteria(UserGroup.class, "UserGroup");

		if (filter != null)
		{

			if (filter.getUserGroupId() > 0)
			{

				criteria.add(Restrictions.eq("id", filter.getUserGroupId()));

			}

			/*
			 * if (filter.getAssignedUserId() != null &&
			 * filter.getAssignedUserId() > 0) {
			 * 
			 * criteria.add(Restrictions.eq("ticket.user.id",
			 * filter.getAssignedUserId())); }
			 */
			if (filter.getPageSize() > 0 || filter.getStartOffset() > 0)
			{
				int pageIndex = filter.getStartOffset();
				int pageSize = filter.getPageSize();

				if (pageIndex == 0 || pageSize == 0)
				{
					pageIndex = 1;
					pageSize = 5;
				}
				criteria.setFirstResult(pageIndex);
				criteria.setMaxResults(pageSize);
			}
		}
		log.debug(" Exitting from  getUserGroups  ");
		return xformToVoList((List<UserGroup>) criteria.list());
	}

	private List<UserGroupVo> xformToVoList(List<UserGroup> userGroups)
	{
		List<UserGroupVo> userGroupVos = new ArrayList<>();

		log.debug(" inside getAllUserGroups " );
		if (userGroups != null)
		{

			log.info("size::::" + userGroups.size());

			for (UserGroup userGroup : userGroups)
			{

				groupNames.put(userGroup.getId(), userGroup.getUserGroupName());

				List<TypeAheadVo> roleNames = new ArrayList<>();

				Set<Role> roles = userGroup.getRoles();

				for (Role role : roles)
				{

					roleNames.add(new TypeAheadVo(role.getId(), role
							.getRoleCode()));

				}

				UserGroupVo groupVo = new UserGroupVo();

				BeanUtils.copyProperties(userGroup, groupVo);

				groupVo.setStatus("enabled");
				groupVo.setRoles(roleNames);

				cachedUserGroups.put(userGroup.getId(), groupVo);
				userGroupVos.add(groupVo);
			}

			log.debug("Exitting from getAllUserGroups"+userGroups.size());
		}
		
		
		return userGroupVos;

	}

	@Override
	public long getUserGroupIdByGroupName(String groupName)
	{
		log.debug(" inside getUserGroupIdByGroupName  " +groupName);
		if (groupNameAndIdMap.size() == 0)
			resetCachedUserGroupNames();

		if (groupNameAndIdMap.containsKey(groupName))
			return groupNameAndIdMap.get(groupName);

		else
			return 0;

	}

	private class UserGroupRolesMappingMapper implements RowMapper<UserGroups>
	{
		// ug.id as userGroupId,ug.userGroupName as userGroupName ,r.id as
		// roleId,r.roleName as roleName,r.roleCode as roleCode,r.roleDesc as
		// roleDesc

		@Override
		public UserGroups mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			UserGroups userGroups = new UserGroups();
			userGroups.setRoleCode(resultSet.getString("roleCode"));
			userGroups.setRoleDesc(resultSet.getString("roleDesc"));
			userGroups.setRoleId(resultSet.getLong("roleId"));
			userGroups.setRoleName(resultSet.getString("roleName"));
			userGroups.setUserGroupId(resultSet.getLong("userGroupId"));
			userGroups.setUserGroupName(resultSet.getString("userGroupName"));
			return userGroups;
		}

	}

	private class UserGroupMapper implements RowMapper<UserGroupVo>
	{
		@Override
		public UserGroupVo mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			UserGroupVo userGroupVo = new UserGroupVo();
			userGroupVo.setId(resultSet.getLong("id"));
			userGroupVo.setUserGroupName(resultSet.getString("userGroupName"));
			userGroupVo.setUserGroupDesc(resultSet.getString("userGroupDesc"));
			userGroupVo.setImmediateManagerGroup(resultSet.getString("userGroupReportTo"));
			return userGroupVo;
		}
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.userGroup.UserGroupDAO#getImmediateManagerList()
	 */
	@Override
	public List<UserGroupVo> getImmediateManagerList() {

		log.info("Fetching All Groups to which the user has to be assigned ");
		List<UserGroupVo> groupList = null;
		try
		{

			log.debug("########## getCities START jdbcTemplate ##########"
					+ new Date());
			 groupList = jdbcTemplate
					.query(GET_ALL_USERGROUPS, new UserGroupMapper());
			log.debug("########## getCities END jdbcTemplate ##########");


		} catch (Exception e)
		{

			log.error("Error in Fetching All Groups to which the user has to be assigned : Error Message is "
					+ e.getMessage() );

			e.printStackTrace();

		}
		log.debug("Exitting Fetching All Groups to which the user has to be assigned ");
		return groupList;
	}

}
