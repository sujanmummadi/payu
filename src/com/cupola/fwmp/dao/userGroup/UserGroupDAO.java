package com.cupola.fwmp.dao.userGroup;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.persistance.entities.UserGroup;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserGroupVo;

public interface UserGroupDAO {

	UserGroup addUserGroup(UserGroupVo userGroup);
	
	UserGroup updateUserGroup(UserGroupVo userGroup);
	
	UserGroup deleteUserGroup(UserGroupVo userGroupVo);
	
	List<UserGroupVo> getAllUserGroups();
	
	Map<Long, String> getAllGroupNames();
	
	Map<Long, String> getAllRoleNames();
	
	UserGroup getUserGroupById(Long id);

	List<UserGroupVo> getUserGroups(UserFilter filter);
	
	long getUserGroupIdByGroupName(String groupName);

	List<TypeAheadVo> getEditableGroupByCurrentUser();

	List<String> getRolesByUserGroupId(Long groupId);

	Long getRoleIdByRoleName(String roleName);

	Set<TypeAheadVo> getuseGroupAndRolesTAByGroupId(Long groupId);

	/**@author aditya
	 * List<UserGroupVo>
	 * @return
	 */
	List<UserGroupVo> getImmediateManagerList();

}
