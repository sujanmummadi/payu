package com.cupola.fwmp.dao.subActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.persistance.entities.Activity;
import com.cupola.fwmp.persistance.entities.SubActivity;
import com.cupola.fwmp.vo.SubActivityVo;

public class SubActivityDAOImpl implements SubActivityDAO
{

	private static final Logger logger = LogManager
			.getLogger(SubActivityDAOImpl.class.getName());
	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	public List<SubActivityVo> getSubActivitiesForActivity(Long activityId)
	{
		List<SubActivityVo> subActivitiesVo = new ArrayList<SubActivityVo>();
        if(activityId == null || activityId <= 0)
			return subActivitiesVo;
        
		logger.debug("getSubActivitiesForActivity " +activityId);
		
		try
		{
			Activity activity = hibernateTemplate.load(Activity.class, activityId);
			if (activity == null || activity.getSubActivities() == null)
			{
				return subActivitiesVo;
			}

			Set<SubActivity> subActivities = activity.getSubActivities();

			SubActivityVo vo = null;
			for (Iterator iterator = subActivities.iterator(); iterator.hasNext();)
			{
				SubActivity subActivity = (SubActivity) iterator.next();
				vo = new SubActivityVo();
				BeanUtils.copyProperties(subActivity, vo);
				vo.setSubactivityName(subActivity.getSubActivityName());
				subActivitiesVo.add(vo);
			}

			logger.debug("Got subactivities " + subActivitiesVo.size()
					+ " for activityId " + activityId);
		} catch ( Exception e) {
			 logger.error(" error while getting SubActivitiesForActivity " ,e);
		}  
		logger.debug(" Exit From getSubActivitiesForActivity " +activityId);
		return subActivitiesVo;
	}

}
