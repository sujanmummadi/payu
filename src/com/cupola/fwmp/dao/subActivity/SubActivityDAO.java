package com.cupola.fwmp.dao.subActivity;

import java.util.List;

import com.cupola.fwmp.vo.SubActivityVo;

public interface SubActivityDAO {

	List<SubActivityVo> getSubActivitiesForActivity(Long activityId);

}
