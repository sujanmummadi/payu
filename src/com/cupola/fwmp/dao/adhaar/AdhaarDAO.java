package com.cupola.fwmp.dao.adhaar;

import com.cupola.fwmp.persistance.entities.Adhaar;
import com.cupola.fwmp.service.ekyc.KycDetails;

//For adhaar operations
public interface AdhaarDAO {

//to save adhaar info in db	
public Adhaar saveAdhar(KycDetails adharDetails);

}
