package com.cupola.fwmp.dao.adhaar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.customer.CustomerDAOImpl;
import com.cupola.fwmp.persistance.entities.Adhaar;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CustomerVO;

@Transactional
public class AdhaarDAOImpl implements AdhaarDAO {

	private static Logger LOGGER = Logger.getLogger(AdhaarDAOImpl.class);

	@Autowired
	HibernateTemplate hiberTemplate;

	@Override
	public Adhaar saveAdhar(KycDetails adharDetails) {
		// to save adhar information

		Long adhaarId = StrictMicroSecondTimeBasedGuid.newGuid();

		LOGGER.info("Inside saveAdhar() of AdhaarDAOImpl for mobile number " + adharDetails.getPhone()
				+ " and customer " + adharDetails.getName());

		Adhaar adhaar = new Adhaar();

		try {

			if (adharDetails != null) {

				adhaar.setCareOf(adharDetails.getCareof());

				adhaar.setCompleteAddress(adharDetails.getCompleteAddress());

				adhaar.setDistrict(adharDetails.getDistrict());

				adhaar.setDob(adharDetails.getDob());

				adhaar.setEmail(adharDetails.getEmail());

				adhaar.setGender(adharDetails.getGender());

				adhaar.setHouseno(adharDetails.getHouseno());

				adhaar.setLandmark(adharDetails.getLandmark());

				adhaar.setLocality(adharDetails.getLandmark());

				adhaar.setName(adharDetails.getName());

				adhaar.setPhone(adharDetails.getPhone());

				adhaar.setPhoto(adharDetails.getPhoto());

				adhaar.setPincode(adharDetails.getPincode());

				adhaar.setPostoffice(adharDetails.getPostoffice());

				adhaar.setState(adharDetails.getState());

				adhaar.setStreet(adharDetails.getStreet());

				adhaar.setUid(adharDetails.getUid());

				adhaar.setVtcname(adharDetails.getVtcname());

				Long aadharIdFromDb = (Long) hiberTemplate.getSessionFactory().getCurrentSession()
						.createQuery("select id from Adhaar where UID=:adharNo")
						.setParameter("adharNo", adharDetails.getUid()).uniqueResult();

				if (aadharIdFromDb != null && aadharIdFromDb > 0) {
				
					LOGGER.info("Aadhaar details exists so, updating aadhaar for mobile number " + adharDetails.getPhone()
					+ " and customer " + adharDetails.getName());

					adhaar.setId(aadharIdFromDb);

					hiberTemplate.update(adhaar);
					
					LOGGER.info("Aadhaar details exists so, updated aadhaar for mobile number " + adharDetails.getPhone()
					+ " and customer " + adharDetails.getName());
				} else {
					
					LOGGER.info("Aadhaar details doesn't exists so, adding aadhaar for mobile number " + adharDetails.getPhone()
					+ " and customer " + adharDetails.getName());
					adhaar.setId(adhaarId);

					hiberTemplate.saveOrUpdate(adhaar);
									
					LOGGER.info("Aadhaar details doesn't exists so, added aadhaar for mobile number " + adharDetails.getPhone()
					+ " and customer " + adharDetails.getName() + " with aadhaar id " + adhaarId);
				}

			}

		} catch (Exception e) {

			LOGGER.error("Error while adding Aadhar info : " + e.getMessage());

			e.printStackTrace();
		}

		return adhaar;
	}

}
