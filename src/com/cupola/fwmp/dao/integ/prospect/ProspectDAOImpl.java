/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cupola.fwmp.dao.integ.prospect.vo.ProspectVoForClassification;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.DBUtil;

/**
 * @author aditya
 * 
 */
public class ProspectDAOImpl implements ProspectDAO
{

	private static Logger log = LogManager
			.getLogger(ProspectDAOImpl.class.getName());
	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	DefinitionCoreService definitionCoreService;

	// private static final String GET_ALL_PROSPECT =
	// "select
	// t.id,t.assignedTo,t.ticketCategory,a.id,a.areaName,b.branchName,b.id,
	// c.id, c.cityName from Ticket t, Area a, Branch b, City c where
	// t.areaId=a.id and b.id=a.branchId and c.id=b.cityId and t.addedOn BETWEEN
	// DATE_SUB(NOW() , INTERVAL 10000 MINUTE) AND NOW()";

	private String getAllProspect;/*
									 * =
									 * "select t.id,t.assignedTo,t.ticketCategory,a.id,a.areaName,b.branchName,b.id, c.id, c.cityName from Ticket t, Area a, Branch b, City c where t.areaId=a.id and b.id=a.branchId and c.id=b.cityId and t.addedOn>=NOW() - INTERVAL 100000000 MINUTE"
									 * ;
									 */

	public void setGetAllProspect(String getAllProspect)
	{
		this.getAllProspect = getAllProspect;
	}

	public void init()
	{
		log.info("Init method of Propspect Controller Job called");

		getAllOpenPropspects();

		log.info("Init method of Propspect Controller Job Success");
	}

	@Override
	public List<ProspectVoForClassification> getAllOpenPropspects()
	{
		log.debug(" getAllOpenPropspects ");
		List<ProspectVoForClassification> propspectVoListFromDB = null;

		try
		{
			getAllProspect = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.PROSPECT_QUEUE_QUERY);

			log.debug("Get All Prospect of FWMP query " + getAllProspect);

			propspectVoListFromDB = jdbcTemplate
					.query(getAllProspect, new ProspectMapper());

			log.debug("@@@@@@@@@@@@@@@ " + propspectVoListFromDB);

		} catch (Exception e)
		{
			log.error("Error while getting ticket from FWMP db  "
					+ e.getMessage());
			e.printStackTrace();
		}

		return propspectVoListFromDB;

	}

	class ProspectMapper implements RowMapper<ProspectVoForClassification>
	{

		@Override
		public ProspectVoForClassification mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			ProspectVoForClassification propspectVoForClassification = new ProspectVoForClassification();
			propspectVoForClassification.setTicketId(resultSet.getLong("t.id"));
			propspectVoForClassification
					.setAssignedTo(resultSet.getLong("t.currentAssignedTo"));
			propspectVoForClassification
					.setTicketCategory(resultSet.getString("t.ticketCategory"));
			propspectVoForClassification.setAreaId(resultSet.getLong("a.id"));
			propspectVoForClassification.setBranchId(resultSet.getLong("b.id"));
			propspectVoForClassification.setCityId(resultSet.getLong("c.id"));
			propspectVoForClassification
					.setStatus(resultSet.getInt("t.status"));
			return propspectVoForClassification;

		}
	}

}
