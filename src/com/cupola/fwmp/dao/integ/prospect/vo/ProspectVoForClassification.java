/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.vo;

/**
 * @author aditya
 * 
 */
public class ProspectVoForClassification
{
	private long ticketId;
	private long areaId;
	private long branchId;
	private long cityId;
	private long assignedTo;
	private String ticketCategory;
	private String key;
	private int status;
	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public long getAreaId()
	{
		return areaId;
	}

	public void setAreaId(long areaId)
	{
		this.areaId = areaId;
	}

	public long getBranchId()
	{
		return branchId;
	}

	public void setBranchId(long branchId)
	{
		this.branchId = branchId;
	}

	public long getCityId()
	{
		return cityId;
	}

	public void setCityId(long cityId)
	{
		this.cityId = cityId;
	}

	public long getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(long assignedTo)
	{
		this.assignedTo = assignedTo;
	}

	public String getTicketCategory()
	{
		return ticketCategory;
	}

	public void setTicketCategory(String ticketCategory)
	{
		this.ticketCategory = ticketCategory;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	@Override
	public String toString()
	{
		return "PropspectVoForClassification [ticketId=" + ticketId
				+ ", areaId=" + areaId + ", branchId=" + branchId + ", cityId="
				+ cityId + ", assignedTo=" + assignedTo + ", ticketCategory="
				+ ticketCategory + ", key=" + key + "]";
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

}
