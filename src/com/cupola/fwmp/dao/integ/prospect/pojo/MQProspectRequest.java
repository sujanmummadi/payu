/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author aditya
 * 
 */

@XmlRootElement(name="REQUESTINFO")
@XmlType(propOrder = { "prospectInfo", "contactInfo", "trailPlanInfo",
		"addressInfo", "flexAttributeInfo" })
public class MQProspectRequest
{
	private ProspectInfo prospectInfo;

	private ContactInfo contactInfo;

	private TrailPlanInfo trailPlanInfo;

	private AddressInfo addressInfo;

	private FlexAttributeInfo flexAttributeInfo;

	@XmlElement(name = "PROSPECTINFO")
	public ProspectInfo getProspectInfo()
	{
		return prospectInfo;
	}

	public void setProspectInfo(ProspectInfo prospectInfo)
	{
		this.prospectInfo = prospectInfo;
	}

	@XmlElement(name = "CONTACTINFO")
	public ContactInfo getContactInfo()
	{
		return contactInfo;
	}

	public void setContactInfo(ContactInfo contactInfo)
	{
		this.contactInfo = contactInfo;
	}

	@XmlElement(name = "TRAILPLANINFO")
	public TrailPlanInfo getTrailPlanInfo()
	{
		return trailPlanInfo;
	}

	public void setTrailPlanInfo(TrailPlanInfo trailPlanInfo)
	{
		this.trailPlanInfo = trailPlanInfo;
	}

	@XmlElement(name = "ADDRESSINFO")
	public AddressInfo getAddressInfo()
	{
		return addressInfo;
	}

	public void setAddressInfo(AddressInfo addressInfo)
	{
		this.addressInfo = addressInfo;
	}

	@XmlElement(name = "FLEX-ATTRIBUTE-INFO")
	public FlexAttributeInfo getFlexAttributeInfo()
	{
		return flexAttributeInfo;
	}

	public void setFlexAttributeInfo(FlexAttributeInfo flexAttributeInfo)
	{
		this.flexAttributeInfo = flexAttributeInfo;
	}

	@Override
	public String toString()
	{
		return "RequestInfo [prospectInfo=" + prospectInfo + ", contactInfo="
				+ contactInfo + ", trailPlanInfo=" + trailPlanInfo
				+ ", addressInfo=" + addressInfo + ", flexAttributeInfo="
				+ flexAttributeInfo + "]";
	}

}
