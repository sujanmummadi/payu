/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.pojo;

/**
 * @author aditya
 * 
 */
public class MqProspectStatus
{
	private String transactionNo;
	private int errorNo;
	private String message;

	public String getTransactionNo()
	{
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo)
	{
		this.transactionNo = transactionNo;
	}

	public int getErrorNo()
	{
		return errorNo;
	}

	public void setErrorNo(int errorNo)
	{
		this.errorNo = errorNo;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return "Status [transactionNo=" + transactionNo + ", errorNo="
				+ errorNo + ", message=" + message + "]";
	}

}
