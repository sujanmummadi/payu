/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author aditya
 * 
 */
@XmlType(propOrder = { "prospectNo", "title", "partyType", "firstName",
		"middleName", "lastName", "customerType", "opentity", "currencyCode",
		"category", "companyName", "notes", "preferredCallDate" })
public class ProspectInfo
{
	private String prospectNo;

	private String title;

	private final String partyType = "I";/* fixed */

	private String firstName;

	private String middleName;

	private String lastName;

	private String customerType = "N";/* fixed */

	private String opentity;/* branch code */

	private final String currencyCode = "RS";/* fixed */

	private final String category = "DEFAULT"; /* fixed */

	private String companyName;

	private String notes; /* remarks */

	private String preferredCallDate;/* appointment date */

	@XmlElement(name = "PROSPECTNO")
	public String getProspectNo()
	{
		return prospectNo;
	}

	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}

	@XmlElement(name = "TITLE")
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	@XmlElement(name = "PARTYTYPE")
	public String getPartyType()
	{
		return partyType;
	}

	// public void setPartyType(String partyType)
	// {
	// this.partyType = partyType;
	// }

	@XmlElement(name = "FIRSTNAME")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	@XmlElement(name = "MIDDLENAME")
	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	@XmlElement(name = "LASTNAME")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	@XmlElement(name = "CUSTOMERTYPE")
	public String getCustomerType()
	{
		return customerType;
	}

	 public void setCustomerType(String customerType)
	 {
		 this.customerType = customerType;
	 }

	@XmlElement(name = "OPENTITY")
	public String getOpentity()
	{
		return opentity;
	}

	public void setOpentity(String opentity)
	{
		this.opentity = opentity;
	}

	@XmlElement(name = "CURRENCYCODE")
	public String getCurrencyCode()
	{
		return currencyCode;
	}

	// public void setCurrencyCode(String currencyCode)
	// {
	// this.currencyCode = currencyCode;
	// }

	@XmlElement(name = "CATEGORY")
	public String getCategory()
	{
		return category;
	}

	// public void setCategory(String category)
	// {
	// this.category = category;
	// }

	@XmlElement(name = "COMPANYNAME")
	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	@XmlElement(name = "NOTES")
	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	@XmlElement(name = "PREFERREDCALLDATE")
	public String getPreferredCallDate()
	{
		return preferredCallDate;
	}

	public void setPreferredCallDate(String preferredCallDate)
	{
		this.preferredCallDate = preferredCallDate;
	}

	@Override
	public String toString()
	{
		return "ProspectInfo [prospectNo=" + prospectNo + ", title=" + title
				+ ", partyType=" + partyType + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName
				+ ", customerType=" + customerType + ", opentity=" + opentity
				+ ", currencyCode=" + currencyCode + ", category=" + category
				+ ", companyName=" + companyName + ", notes=" + notes
				+ ", preferredCallDate=" + preferredCallDate + "]";
	}

}
