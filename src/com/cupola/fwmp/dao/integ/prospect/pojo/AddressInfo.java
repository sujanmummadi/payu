/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author aditya
 * 
 */

@XmlType(propOrder = { "addressTypeCode", "address1", "address2", "area",
		"city", "state", "zipCode", "country" })
public class AddressInfo
{
	private String addressTypeCode;

	private String address1;

	private String address2;

	private String area;/* area */

	private String city;

	private String state;

	private String zipCode;

	private String country;

	@Override
	public String toString()
	{
		return "AddressInfo [addressTypeCode=" + addressTypeCode
				+ ", address1=" + address1 + ", address2=" + address2
				+ ", area=" + area + ", city=" + city + ", state=" + state
				+ ", zipCode=" + zipCode + ", country=" + country + "]";
	}

	@XmlElement(name = "ADDRESSTYPECODE")
	public String getAddressTypeCode()
	{
		return addressTypeCode;
	}

	public void setAddressTypeCode(String addressTypeCode)
	{
		this.addressTypeCode = addressTypeCode;
	}

	@XmlElement(name = "ADDRESS1")
	public String getAddress1()
	{
		return address1;
	}

	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}

	@XmlElement(name = "ADDRESS2")
	public String getAddress2()
	{
		return address2;
	}

	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}

	@XmlElement(name = "AREA")
	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	@XmlElement(name = "CITY")
	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	@XmlElement(name = "STATE")
	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	@XmlElement(name = "ZIPCODE")
	public String getZipCode()
	{
		return zipCode;
	}

	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	@XmlElement(name = "COUNTRY")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}
}
