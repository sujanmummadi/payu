/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author aditya
 * 
 */

@XmlType(propOrder = { "contactName", "email", "alteMail", "homePhone",
		"workPhone", "mobilePhone", "faxNbr" })
public class ContactInfo
{

	private String contactName;

	private String email;

	private String alteMail;

	private String homePhone;

	private String workPhone;

	private String mobilePhone;

	private String faxNbr;

	@XmlElement(name = "CONTACTNAME")
	public String getContactName()
	{
		return contactName;
	}

	public void setContactName(String contactName)
	{
		this.contactName = contactName;
	}

	@XmlElement(name = "EMAIL")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	@XmlElement(name = "ALTEMAIL")
	public String getAlteMail()
	{
		return alteMail;
	}

	public void setAlteMail(String alteMail)
	{
		this.alteMail = alteMail;
	}

	@XmlElement(name = "HOMEPHONE")
	public String getHomePhone()
	{
		return homePhone;
	}

	public void setHomePhone(String homePhone)
	{
		this.homePhone = homePhone;
	}

	@XmlElement(name = "WORKPHONE")
	public String getWorkPhone()
	{
		return workPhone;
	}

	public void setWorkPhone(String workPhone)
	{
		this.workPhone = workPhone;
	}

	@XmlElement(name = "MOBILEPHONE")
	public String getMobilePhone()
	{
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}

	@XmlElement(name = "FAXNBR")
	public String getFaxNbr()
	{
		return faxNbr;
	}

	public void setFaxNbr(String faxNbr)
	{
		this.faxNbr = faxNbr;
	}

	@Override
	public String toString()
	{
		return "ContactInfo [contactName=" + contactName + ", email=" + email
				+ ", alteMail=" + alteMail + ", homePhone=" + homePhone
				+ ", workPhone=" + workPhone + ", mobilePhone=" + mobilePhone
				+ ", faxNbr=" + faxNbr + "]";
	}

}
