/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author aditya
 * 
 */
@XmlType(propOrder = { "hasTrailPlan" })
public class TrailPlanInfo
{
	private final String hasTrailPlan = "N"; /* fixed */

	@XmlElement(name = "HASTRAILPLAN")
	public String getHasTrailPlan()
	{
		return hasTrailPlan;
	}

	// public void setHasTrailPlan(String hasTrailPlan)
	// {
	// this.hasTrailPlan = hasTrailPlan;
	// }

	@Override
	public String toString()
	{
		return "TrailPlanInfo [hasTrailPlan=" + hasTrailPlan + "]";
	}

}
