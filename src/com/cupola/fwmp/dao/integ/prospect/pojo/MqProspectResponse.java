/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect.pojo;

/**
 * @author aditya
 *
 */
public class MqProspectResponse
{
	private MqProspectStatus status;
	private String prospectNo;
	public MqProspectStatus getStatus()
	{
		return status;
	}
	public void setStatus(MqProspectStatus status)
	{
		this.status = status;
	}
	public String getProspectNo()
	{
		return prospectNo;
	}
	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}
	@Override
	public String toString()
	{
		return "MqProspectResponse [status=" + status + ", prospectNo="
				+ prospectNo + "]";
	}

}
