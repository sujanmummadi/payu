/**
 * 
 */
package com.cupola.fwmp.dao.integ.prospect;

import java.util.List;

import com.cupola.fwmp.dao.integ.prospect.vo.ProspectVoForClassification;

/**
 * @author aditya
 *
 */
public interface ProspectDAO
{
	
	public List<ProspectVoForClassification>  getAllOpenPropspects();

}
