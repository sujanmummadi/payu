package com.cupola.fwmp.dao.integ.mq.roi;

import java.util.List;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;

public interface RoiMqDAO 
{

	/**
	 * @author aditya List<MQWorkorderVO>
	 * @param lastDataFetchedTimeManualForROI
	 * @return
	 */
	List<MQWorkorderVO> getAllOpenTicketForROISpecial();


	/**@author aditya
	 * List<MQWorkorderVO>
	 * @param cityName
	 * @param branchName
	 * @return
	 */
	List<MQWorkorderVO> getAllOpenFrTicketForROI(String cityName, String branchName);
	/**@author aditya
	 * List<MQWorkorderVO>
	 * @param cityName
	 * @param branchName
	 * @return
	 */
	List<MQWorkorderVO> getAllOpenFrTicketForROI(String branchName);

	/**@author aditya
	 * List<MQWorkorderVO>
	 * @param cityName
	 * @return
	 */
	List<MQWorkorderVO> getAllOpenTicketForROI(String cityName);
}
