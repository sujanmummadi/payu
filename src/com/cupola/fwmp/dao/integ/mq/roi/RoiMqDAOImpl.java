package com.cupola.fwmp.dao.integ.mq.roi;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.CommonUtil;

public class RoiMqDAOImpl implements RoiMqDAO {

	private static Logger log = LogManager.getLogger(RoiMqDAOImpl.class.getName());

	// @Resource(name="mqHydjdbcTemplate")
	@Autowired
	@Qualifier("mqROIjdbcTemplate")
	JdbcTemplate mqROIjdbcTemplate;

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	UserDao userDao;

	@Autowired
	DefinitionCoreService definitionCoreService;

	private String getAllOpenTicket;

	String lastDataFetchedTimeForROI = null;
	String lastDataFetchedTimeManualForROI = null;

	Set<String> previousWOroi = null;

	private String getAllOpenFrTicket;
//	  String lastTimeFrDataFetched = null;

	public void setGetAllOpenTicket(String getAllOpenTicket) {
		this.getAllOpenTicket = getAllOpenTicket;
	}

	Map<String, String> deploymentCityBasedLastFetchedTime = new HashMap<>();

	@Override
	public List<MQWorkorderVO> getAllOpenTicketForROI(String cityName) {

		List<MQWorkorderVO> roiMqOpenTickets = new ArrayList<MQWorkorderVO>();
		try {

			log.info("################ TEST ############ Get all the open ticket for ROI " + cityName);

			lastDataFetchedTimeForROI = deploymentCityBasedLastFetchedTime.containsKey(cityName)
					? cityBasedLastFetchedTime.get(cityName)
					: null;

			getAllOpenTicket = definitionCoreService.getClassificationQuery(DefinitionCoreService.MQ_ROI_QUERY);

			String timeToMinusInString = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON);

			String daysToMinusInString = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME);

			log.info("lastDataFetchedTimeForROI " + lastDataFetchedTimeForROI + " timeToMinusInString "
					+ timeToMinusInString);

			if (lastDataFetchedTimeForROI == null) {
				log.info("lastDataFetchedTimeForROI " + lastDataFetchedTimeForROI + " daysToMinusInString "
						+ daysToMinusInString);

				lastDataFetchedTimeForROI = CommonUtil
						.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
								: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);

			} else {
				lastDataFetchedTimeForROI = CommonUtil.getPriviousDateByMinutes(lastDataFetchedTimeForROI,
						timeToMinusInString != null ? Integer.valueOf(timeToMinusInString)
								: FWMPConstant.MINUS_MINUTES_FROM_CURRENT_TIME);
			}

			if (previousWOroi == null) {
				previousWOroi = new LinkedHashSet<String>();
				previousWOroi.add("-1");

			} else if (previousWOroi.isEmpty()) {
				previousWOroi.add("-1");
			}

			log.info("lastDataFetchedTimeForROI city " + cityName + " is " + lastDataFetchedTimeForROI);

			// String previousWOs = StringUtils.join(previousWOroi, ",");

			roiMqOpenTickets = mqROIjdbcTemplate.query(getAllOpenTicket,
					new Object[] { cityName, lastDataFetchedTimeForROI }, new ROIMqDAOImplMapper());

			log.info("################################################# test ROI city " + cityName + " "
					+ roiMqOpenTickets.size());

			if (previousWOroi != null)
				previousWOroi.clear();

			if (!roiMqOpenTickets.isEmpty())
				lastDataFetchedTimeForROI = roiMqOpenTickets.get(0).getReqDate() + ":00";

			deploymentCityBasedLastFetchedTime.put(cityName, lastDataFetchedTimeForROI);

			return roiMqOpenTickets;

		} catch (Exception e) {
			log.error("exception in getting tickets for MQ ROI  city " + cityName + " " + e.getMessage(), e);
			e.printStackTrace();
		}
		return roiMqOpenTickets;
	}

	@Override
	public List<MQWorkorderVO> getAllOpenTicketForROISpecial() {
		try {
			log.info(
					"################ TEST ############ Get all the open ticket for ROI Special cron ###########################");

			getAllOpenTicket = definitionCoreService.getClassificationQuery(DefinitionCoreService.MQ_ROI_QUERY);

			String timeToMinusInString = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_SPECIAL_CRON);

			String daysToMinusInString = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME);

			log.info("lastDataFetchedTimeManualForROI " + lastDataFetchedTimeManualForROI + " timeToMinusInString "
					+ timeToMinusInString);

			if (lastDataFetchedTimeManualForROI == null) {
				log.info("lastDataFetchedTimeManualForROI " + lastDataFetchedTimeManualForROI + " daysToMinusInString "
						+ daysToMinusInString);

				lastDataFetchedTimeManualForROI = CommonUtil
						.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
								: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);

			} else {
				lastDataFetchedTimeManualForROI = CommonUtil.getPriviousDateByMinutes(lastDataFetchedTimeManualForROI,
						timeToMinusInString != null ? Integer.valueOf(timeToMinusInString) : 200);
			}

			log.info("lastDataFetchedTimeManualForROI " + lastDataFetchedTimeManualForROI);

			// String previousWOs = StringUtils.join(previousWOroi, ",");

			List<MQWorkorderVO> roiMqOpenTickets = mqROIjdbcTemplate.query(getAllOpenTicket,
					new Object[] { lastDataFetchedTimeManualForROI }, new ROIMqDAOImplMapper());

			log.info(
					"################################################# test ROI Special cron $############################### "
							+ roiMqOpenTickets.size());

			if (!roiMqOpenTickets.isEmpty())
				lastDataFetchedTimeManualForROI = roiMqOpenTickets.get(0).getReqDate() + ":00";

			List<MQWorkorderVO> mqOpenTickets = new ArrayList<MQWorkorderVO>();
			MQWorkorderVO mqWorkorderVO = null;

			for (Iterator<MQWorkorderVO> iterator = roiMqOpenTickets.iterator(); iterator.hasNext();) {
				mqWorkorderVO = (MQWorkorderVO) iterator.next();

				try {
					if (mqWorkorderVO.getCityId() != null && !"".equals(mqWorkorderVO.getCityId()))
						mqOpenTickets.add(mqWorkorderVO);

					log.info("Special cron ROI WO " + mqWorkorderVO.getWorkOrderNumber() + " MQ "
							+ mqWorkorderVO.getMqId() + " PROSPECT " + mqWorkorderVO.getProspectNumber()
							+ " MQ CREATED TIME " + mqWorkorderVO.getReqDate());

					// log.info("Special cron ROI WO Complete Object is " + mqWorkorderVO);

				} catch (Exception e) {
					log.error("Special cron HYD Exception in processing data for " + mqWorkorderVO);
					e.printStackTrace();
					continue;
				}
			}

			return mqOpenTickets;
		} catch (Exception e) {
			log.error("exception in getting tickets for MQ ROI  while running special cron " + e.getMessage(), e);

			e.printStackTrace();
		}
		return null;
	}

	class ROIMqDAOImplMapper implements RowMapper<MQWorkorderVO> {
		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo) throws SQLException {
			MQWorkorderVO roiMQWorkorderVO = new MQWorkorderVO();

			try {
				Long areaId = null;
				if (resultSet.getString("area") != null)
					areaId = areaDAO.getIdByCode(resultSet.getString("area"));

				Long branchId = null;
				if (resultSet.getString("branch") != null) {
					if (resultSet.getString("branch").equalsIgnoreCase(FWMPConstant.REALTY)) {
						branchId = null;
						log.info("Realty " + "resultSet.getString(\"branch\") == FWMPConstant.REALTY");
						branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
								resultSet.getString("branch").toUpperCase().trim(),
								resultSet.getString("city").toUpperCase());
					} else {
						branchId = branchDAO.getBranchIdByName(resultSet.getString("branch").toUpperCase());
					}

				}

				Long cityId = null;
				if (resultSet.getString("city") != null)
					cityId = cityDAO.getCityIdByName(resultSet.getString("city").toUpperCase());

				if (areaId != null)
					roiMQWorkorderVO.setAreaId(areaId + "");

				if (branchId != null)
					roiMQWorkorderVO.setBranchId(branchId + "");

				if (cityId != null)
					roiMQWorkorderVO.setCityId(cityId + "");

				if (resultSet.getString("COMMENTS") != null)
					roiMQWorkorderVO.setComment(resultSet.getString("COMMENTS"));

				if (resultSet.getString("COMMENT_CODE") != null)
					roiMQWorkorderVO.setCommentCode(resultSet.getString("COMMENT_CODE"));

				if (resultSet.getString("CUST_ADDRESS") != null)
					roiMQWorkorderVO.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));

				if (resultSet.getString("PHONE") != null)
					roiMQWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));

				if (resultSet.getString("CUST_NAME") != null)
					roiMQWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));

				if (resultSet.getString("CX_IP") != null)
					roiMQWorkorderVO.setCxIp(resultSet.getString("CX_IP"));

				if (resultSet.getString("FX_NAME") != null)
					roiMQWorkorderVO.setFxName(resultSet.getString("FX_NAME"));

				if (resultSet.getString("MQ_ID") != null)
					roiMQWorkorderVO.setMqId(resultSet.getString("MQ_ID"));

				if (resultSet.getString("PROSPECT_NO") != null)
					roiMQWorkorderVO.setProspectNumber(resultSet.getString("PROSPECT_NO"));

				if (resultSet.getString("REQ_DATE") != null)
					roiMQWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));

				if (resultSet.getString("RESP_STATUS") != null)
					roiMQWorkorderVO.setResponseStatus(resultSet.getString("RESP_STATUS"));

				if (resultSet.getString("SYMP_NAME") != null)
					roiMQWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));

				if (resultSet.getString("CAT_NAME") != null)
					roiMQWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));

				if (resultSet.getString("WO_NO") != null)
					roiMQWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));

				if (resultSet.getString("VOC") != null)
					roiMQWorkorderVO.setVoc(resultSet.getString("VOC"));
				
				roiMQWorkorderVO.setModifiedDate(resultSet.getString("MODIFIED_DATE"));
				
				Long assignedTo = null;

				if (roiMQWorkorderVO.getFxName() != null)
					assignedTo = userDao.getUserIdByDevcieName(roiMQWorkorderVO.getFxName());
				roiMQWorkorderVO.setAssignedTo(assignedTo);

				String fullyQualifiedName = roiMQWorkorderVO.getCityId() + "_" + roiMQWorkorderVO.getBranchId() + "_"
						+ roiMQWorkorderVO.getAreaId() + "_" + roiMQWorkorderVO.getAssignedTo();

				roiMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);
				roiMQWorkorderVO.setTicketSource("OUTSIDE FWMP");

				log.info("Original content for ROI is city " + resultSet.getString("city") + " branch "
						+ resultSet.getString("branch") + " and area " + resultSet.getString("area") + " for WO "
						+ resultSet.getString("WO_NO") + ", MQ " + resultSet.getString("MQ_ID"));

			} catch (Exception e) {
				log.error("Error while preparing data from result set while iterating ROI " + e.getMessage());
				e.printStackTrace();
			}

			return roiMQWorkorderVO;
		}
	}

	private String timeToMinusInString = null;
	private String daysToMinusInString = null;

	Map<String, String> cityBasedLastFetchedTime = new ConcurrentHashMap<>();
	Map<String, String> branchBasedLastFetchedTime = new ConcurrentHashMap<>();
	// Map<String, Boolean> cityBasedFirstFetchedTime = new HashMap<>();

	@Override
	public List<MQWorkorderVO> getAllOpenFrTicketForROI(final String cityName, String branchName) {
		List<MQWorkorderVO> roiMqOpenTickets = new ArrayList<>();

		log.info("*********** Fetching FR Ticket from ROI *************** " + cityName + " and branch " + branchName);
		
		String lastTimeFrDataFetched = null;
		try {
			if (branchName == null || branchName.isEmpty()) {

				log.info("*********** Fetching FR Ticket from ROI *************** " + cityName);

				lastTimeFrDataFetched = cityBasedLastFetchedTime.containsKey(cityName)
						? cityBasedLastFetchedTime.get(cityName)
						: null;

				getAllOpenFrTicket = definitionCoreService
						.getClassificationQuery(DefinitionCoreService.MQ_FR_ROI_QUERY);

				timeToMinusInString = definitionCoreService.getClassificationQuery(
						DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON_FR);

				daysToMinusInString = definitionCoreService
						.getClassificationQuery(DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME_FR);

				log.info("lastTimeFrDataFetched " + lastTimeFrDataFetched + " timeToMinusInString "
						+ timeToMinusInString + " for city " + cityName);

				if (lastTimeFrDataFetched == null)
					lastTimeFrDataFetched = CommonUtil
							.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
									: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);

				else
					lastTimeFrDataFetched = CommonUtil.getPriviousDateByMinutes(lastTimeFrDataFetched,
							timeToMinusInString != null ? Integer.valueOf(timeToMinusInString)
									: FWMPConstant.MINUS_MINUTES_FROM_CURRENT_TIME);

				// if(cityBasedFirstFetchedTime.containsKey(cityName))
				// {
				// lastTimeFrDataFetched =
				// CommonUtil.getPriviousDateByMinutes(lastTimeFrDataFetched,FWMPConstant.MINUS_MINUTES_FROM_CURRENT_TIME);
				//
				// } else
				// cityBasedFirstFetchedTime.put(cityName, true);

				log.info("lastTimeFrDataFetched : " + lastTimeFrDataFetched + " for " + cityName);

				if (!"BANGALORE".equalsIgnoreCase(cityName)) {
					roiMqOpenTickets = mqROIjdbcTemplate.query(getAllOpenFrTicket,
							new Object[] { cityName, lastTimeFrDataFetched, lastTimeFrDataFetched },
							"BANGALORE".equalsIgnoreCase(cityName) ? new BLRROIMqFrMapper() : new ROIMqFrMapper());
				}
				
				
				else if("BANGALORE".equalsIgnoreCase(cityName))
				{
				List<Map<String, Object>> result = mqROIjdbcTemplate.queryForList(getAllOpenFrTicket,
						new Object[] { cityName, lastTimeFrDataFetched, lastTimeFrDataFetched });
				
				
					log.info("BLRListResult lastTimeFrDataFetched : " + lastTimeFrDataFetched + " for  city " + cityName
						+ " and complete result is " + result.size());

				int rowNo = 0;
				for (Map<String, Object> tempRow : result) {
					
					  MQWorkorderVO roiMQWorkorderVO = new MQWorkorderVO();

					try {

						log.info("BLRListResult " + rowNo + "Entry  ");
						if (rowNo == 0) {
							log.info("BLRListResult " + rowNo + " First time stamp " + lastTimeFrDataFetched);
							lastTimeFrDataFetched = tempRow.get("REQ_DATE") + ":00";
							cityBasedLastFetchedTime.put(tempRow.get("city").toString().toUpperCase(),
									lastTimeFrDataFetched);
						}
						rowNo++;

						log.info("BLRListResult  Getting area " + tempRow.get("area") + " for " + tempRow.get("WO_NO"));
						Long areaId = areaDAO.getIdByCode(tempRow.get("area").toString());
						log.info("BLRListResult Got area for " + tempRow.get("area") + " for " + tempRow.get("WO_NO"));

						Long branchId = null;
						log.info("BLRListResult " + "Getting branch " + tempRow.get("branch") + " for "
								+ tempRow.get("WO_NO"));
						if (tempRow.get("branch") != null) {

							log.info("\"BLRListResult \"+ tempRow.get(\"branch\") != null");
							if (tempRow.get("branch").toString().equalsIgnoreCase(FWMPConstant.REALTY)) {
								log.info("BLRListResult " + "tempRow.get(\"branch\") == FWMPConstant.REALTY");
								branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
										tempRow.get("branch").toString().toUpperCase().trim(),
										tempRow.get("city").toString().toUpperCase());
							} else {

								branchId = branchDAO.getBranchIdByName(tempRow.get("branch").toString().toUpperCase());
							}

						}

						log.info("BLRListResult " + branchId + " Got branch " + tempRow.get("branch") + " for "
								+ tempRow.get("WO_NO"));

						log.info("BLRListResult " + "Getting city " + tempRow.get("city") + " for "
								+ tempRow.get("WO_NO"));
						Long cityId = cityDAO.getCityIdByName(tempRow.get("city").toString().toUpperCase());
						log.info("BLRListResult " + cityId + " Got city" + tempRow.get("city") + " for "
								+ tempRow.get("WO_NO"));

						if (areaId != null)
							roiMQWorkorderVO.setAreaId(areaId + "");

						if (branchId != null)
							roiMQWorkorderVO.setBranchId(branchId + "");

						if (cityId != null)
							roiMQWorkorderVO.setCityId(cityId + "");
						log.info("BLRListResult " + "Start extract.......");
						roiMQWorkorderVO.setComment(tempRow.get("COMMENTS").toString());
						roiMQWorkorderVO.setCommentCode(tempRow.get("COMMENT_CODE").toString());
						roiMQWorkorderVO.setCustomerAddress(tempRow.get("CUST_ADDRESS").toString());
						roiMQWorkorderVO.setCustomerMobile(tempRow.get("PHONE").toString());
						roiMQWorkorderVO.setCustomerName(tempRow.get("CUST_NAME").toString());
						roiMQWorkorderVO.setCxIp(tempRow.get("CX_IP").toString());
						roiMQWorkorderVO.setFxName(tempRow.get("FX_NAME").toString());
						roiMQWorkorderVO.setMqId(tempRow.get("MQ_ID").toString());
						roiMQWorkorderVO.setProspectNumber(tempRow.get("PROSPECT_NO").toString());
						roiMQWorkorderVO.setReqDate(tempRow.get("REQ_DATE").toString());
						roiMQWorkorderVO.setResponseStatus(tempRow.get("RESP_STATUS").toString());
						roiMQWorkorderVO.setSymptom(tempRow.get("SYMP_NAME").toString());
						roiMQWorkorderVO.setTicketCategory(tempRow.get("CAT_NAME").toString());
						roiMQWorkorderVO.setWorkOrderNumber(tempRow.get("WO_NO").toString());
						roiMQWorkorderVO.setVoc(tempRow.get("VOC").toString());
						roiMQWorkorderVO.setModifiedDate(tempRow.get("MODIFIED_DATE").toString());
					
						log.info("BLRListResult " + "End extract.......");
						
						FrDeviceVo device = new FrDeviceVo();
						device.setFxName(roiMQWorkorderVO.getFxName());

						String cxIp = roiMQWorkorderVO.getCxIp();
						if (cxIp != null && !cxIp.isEmpty() && cxIp.contains("/")) {
							try {
								String[] ipAndPort = cxIp.split("/");

								if (ipAndPort != null && ipAndPort.length > 0)
									device.setCxIp(ipAndPort[0]);

								if (ipAndPort != null && ipAndPort.length > 1)
									device.setCxPort(Integer.valueOf(ipAndPort[1]));
							} catch (NumberFormatException e) {
								device.setCxPort(0);
							}

						} else {
							device.setCxIp(cxIp);
							device.setCxPort(0);
						}
						log.info("BLRListResult " + "Device constructed .......");
						roiMQWorkorderVO.setDeviceData(device);
						String fullyQualifiedName = roiMQWorkorderVO.getCityId() + "_" + roiMQWorkorderVO.getBranchId()
								+ "_" + roiMQWorkorderVO.getAreaId() + "_" + roiMQWorkorderVO.getAssignedTo();
						roiMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);

						log.info("BLRListResult " + "Original content for ROI FR Mapper is city " + tempRow.get("city")
								+ " branch " + tempRow.get("branch") + " and area " + tempRow.get("area") + " for WO "
								+ tempRow.get("WO_NO") + ", MQ " + tempRow.get("MQ_ID") + " Created on  "
								+ tempRow.get("REQ_DATE") + " modified on  " + tempRow.get("MODIFIED_DATE"));

						log.info("BLRListResult FR data ###### Test " + roiMQWorkorderVO.toString());

						log.info("BLRListResult Before adding for city " + cityName + " size is  "
								+ roiMqOpenTickets.size());
						roiMqOpenTickets.add(roiMQWorkorderVO);
						log.info("BLRListResult After adding for city " + cityName + " size is  "
								+ roiMqOpenTickets.size());
						// Thread.sleep(10);
					} catch (Exception e) {
						e.printStackTrace();

						log.error("Error in Mapper main thread " + e.getMessage(), e);
						continue;
					}

					}
				}

				log.info("Total Fr Ticket found in ROI : city " + cityName + " . " + roiMqOpenTickets.size());

				if (!roiMqOpenTickets.isEmpty())
				{
					lastTimeFrDataFetched = roiMqOpenTickets.get(0).getReqDate() + ":00";
				}
				log.info("Total Fr Ticket found in ROI : city " + cityName + " . " + roiMqOpenTickets.size() + " at " + lastTimeFrDataFetched);

				cityBasedLastFetchedTime.put(cityName, lastTimeFrDataFetched);

				return roiMqOpenTickets;

			} else {

				log.info("*********** Fetching FR Ticket from ROI *************** " + cityName + " and branch "
						+ branchName);

				lastTimeFrDataFetched = branchBasedLastFetchedTime.containsKey(branchName)
						? branchBasedLastFetchedTime.get(branchName)
						: null;

				getAllOpenFrTicket = definitionCoreService
						.getClassificationQuery(DefinitionCoreService.MQ_FR_ROI_QUERY);

				timeToMinusInString = definitionCoreService.getClassificationQuery(
						DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON_FR);

				daysToMinusInString = definitionCoreService
						.getClassificationQuery(DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME_FR);

				log.info("lastTimeFrDataFetched " + lastTimeFrDataFetched + " timeToMinusInString "
						+ timeToMinusInString + " roi city " + cityName + " and branch " + branchName);

				if (lastTimeFrDataFetched == null)
					lastTimeFrDataFetched = CommonUtil
							.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
									: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);

				else
					lastTimeFrDataFetched = CommonUtil.getPriviousDateByMinutes(lastTimeFrDataFetched,
							timeToMinusInString != null ? Integer.valueOf(timeToMinusInString)
									: FWMPConstant.MINUS_MINUTES_FROM_CURRENT_TIME);

				if (getAllOpenFrTicket.contains("where CITY = ? and BRANCH = ?"))
					getAllOpenFrTicket = getAllOpenFrTicket.replace("where CITY = ? and BRANCH = ?",
							"where CITY = ? and BRANCH = ? ");

				else
					getAllOpenFrTicket = getAllOpenFrTicket.replace("where CITY = ?", "where CITY = ? and BRANCH = ? ");

				log.info("lastTimeFrDataFetched : " + lastTimeFrDataFetched + " for " + cityName + " and branch "
						+ branchName + " , query = " + getAllOpenFrTicket);

				List<MQWorkorderVO> roiMqOpenTickets1 = mqROIjdbcTemplate.query(getAllOpenFrTicket,
						new Object[] { cityName, branchName, lastTimeFrDataFetched, lastTimeFrDataFetched },
						"BANGALORE".equalsIgnoreCase(cityName) ? new BLRROIMqFrMapper() : new ROIMqFrMapper());

				log.info("Total Fr Ticket found in ROI : city " + cityName + " and branch " + branchName + " is "
						+ roiMqOpenTickets1.size());

				if (!roiMqOpenTickets1.isEmpty())
					lastTimeFrDataFetched = roiMqOpenTickets1.get(0).getReqDate() + ":00";

				branchBasedLastFetchedTime.put(branchName, lastTimeFrDataFetched);

				return roiMqOpenTickets1;
			}

		} catch (Exception e) {
			log.error("exception in getting FR Tickets for MQ ROI  city " + cityName + e.getMessage(), e);

			e.printStackTrace();
		}
		
		return roiMqOpenTickets;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.integ.mq.roi.RoiMqDAO#getAllOpenFrTicketForROI(java.lang.
	 * String)
	 */
	@Override
	public List<MQWorkorderVO> getAllOpenFrTicketForROI(String branchName) {

		List<MQWorkorderVO> roiMqOpenTickets = new ArrayList<>();
		  String lastTimeFrDataFetched = null;
		log.info("*********** Fetching FR Ticket from ROI ***************  branch " + branchName);

		try {

			log.info("*********** Fetching FR Ticket from ROI *************** branch " + branchName);

			lastTimeFrDataFetched = branchBasedLastFetchedTime.containsKey(branchName)
					? branchBasedLastFetchedTime.get(branchName)
					: null;

			getAllOpenFrTicket = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_FR_ROI_QUERY_BRANCH);

			timeToMinusInString = definitionCoreService.getClassificationQuery(
					DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON_FR);

			daysToMinusInString = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME_FR);

			log.info("lastTimeFrDataFetched " + lastTimeFrDataFetched + " timeToMinusInString " + timeToMinusInString
					+ " roi  branch " + branchName);

			if (lastTimeFrDataFetched == null)
				lastTimeFrDataFetched = CommonUtil
						.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
								: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);

			else
				lastTimeFrDataFetched = CommonUtil.getPriviousDateByMinutes(lastTimeFrDataFetched,
						timeToMinusInString != null ? Integer.valueOf(timeToMinusInString)
								: FWMPConstant.MINUS_MINUTES_FROM_CURRENT_TIME);

			log.info("lastTimeFrDataFetched : " + lastTimeFrDataFetched + " for  branch " + branchName + " , query = "
					+ getAllOpenFrTicket);

			// roiMqOpenTickets = mqROIjdbcTemplate.query(getAllOpenFrTicket,
			// new Object[] { branchName, lastTimeFrDataFetched, lastTimeFrDataFetched },
			// new ROIMqFrMapper());
			//
			List<Map<String, Object>> result = mqROIjdbcTemplate.queryForList(getAllOpenFrTicket,
					new Object[] { branchName, lastTimeFrDataFetched, lastTimeFrDataFetched });

			log.info("lastTimeFrDataFetched : " + lastTimeFrDataFetched + " for  branch " + branchName + " , query = "
					+ getAllOpenFrTicket + " and complete result is " + result.size());

			int rowNo = 0;
			for (Map<String, Object> row : result) {

				MQWorkorderVO roiMQWorkorderVO = new MQWorkorderVO();

				log.info("BLRROIMqFrMapper " + rowNo + "Entry  ");
				if (rowNo == 0) {
					log.info("BLRROIMqFrMapper " + rowNo + " First time stamp " + lastTimeFrDataFetched);
					lastTimeFrDataFetched = row.get("REQ_DATE") + ":00";
					cityBasedLastFetchedTime.put(row.get("city").toString().toUpperCase(), lastTimeFrDataFetched);
				}
				rowNo++;

				try {

					log.info("BLRROIMqFrMapper  Getting area " + row.get("area") + " for " + row.get("WO_NO"));
					Long areaId = areaDAO.getIdByCode(row.get("area").toString());
					log.info("Got area for " + row.get("area") + " for " + row.get("WO_NO"));

					Long branchId = null;
					log.info("BLRROIMqFrMapper " + "Getting branch " + row.get("branch") + " for " + row.get("WO_NO"));
					if (row.get("branch") != null) {

						log.info("\"BLRROIMqFrMapper \"+ row.get(\"branch\") != null");
						if (row.get("branch").toString().equalsIgnoreCase(FWMPConstant.REALTY)) {
							log.info("BLRROIMqFrMapper " + "row.get(\"branch\") == FWMPConstant.REALTY");
							branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
									row.get("branch").toString().toUpperCase().trim(),
									row.get("city").toString().toUpperCase());
						} else {

							branchId = branchDAO.getBranchIdByName(row.get("branch").toString().toUpperCase());
						}

					}

					log.info("BLRROIMqFrMapper " + branchId + " Got branch " + row.get("branch") + " for "
							+ row.get("WO_NO"));

					log.info("BLRROIMqFrMapper " + "Getting city " + row.get("city") + " for " + row.get("WO_NO"));
					Long cityId = cityDAO.getCityIdByName(row.get("city").toString().toUpperCase());
					log.info("BLRROIMqFrMapper " + cityId + " Got city" + row.get("city") + " for " + row.get("WO_NO"));

					if (areaId != null)
						roiMQWorkorderVO.setAreaId(areaId + "");

					if (branchId != null)
						roiMQWorkorderVO.setBranchId(branchId + "");

					if (cityId != null)
						roiMQWorkorderVO.setCityId(cityId + "");
					log.info("BLRROIMqFrMapper " + "Start extract.......");
					roiMQWorkorderVO.setComment(row.get("COMMENTS").toString());
					roiMQWorkorderVO.setCommentCode(row.get("COMMENT_CODE").toString());
					roiMQWorkorderVO.setCustomerAddress(row.get("CUST_ADDRESS").toString());
					roiMQWorkorderVO.setCustomerMobile(row.get("PHONE").toString());
					roiMQWorkorderVO.setCustomerName(row.get("CUST_NAME").toString());
					roiMQWorkorderVO.setCxIp(row.get("CX_IP").toString());
					roiMQWorkorderVO.setFxName(row.get("FX_NAME").toString());
					roiMQWorkorderVO.setMqId(row.get("MQ_ID").toString());
					roiMQWorkorderVO.setProspectNumber(row.get("PROSPECT_NO").toString());
					roiMQWorkorderVO.setReqDate(row.get("REQ_DATE").toString());
					roiMQWorkorderVO.setResponseStatus(row.get("RESP_STATUS").toString());
					roiMQWorkorderVO.setSymptom(row.get("SYMP_NAME").toString());
					roiMQWorkorderVO.setTicketCategory(row.get("CAT_NAME").toString());
					roiMQWorkorderVO.setWorkOrderNumber(row.get("WO_NO").toString());
					roiMQWorkorderVO.setVoc(row.get("VOC").toString());
					roiMQWorkorderVO.setModifiedDate(row.get("MODIFIED_DATE").toString());
					
					log.info("BLRROIMqFrMapper " + "End extract.......");
					FrDeviceVo device = new FrDeviceVo();
					device.setFxName(roiMQWorkorderVO.getFxName());

					String cxIp = roiMQWorkorderVO.getCxIp();
					if (cxIp != null && !cxIp.isEmpty() && cxIp.contains("/")) {
						try {
							String[] ipAndPort = cxIp.split("/");

							if (ipAndPort != null && ipAndPort.length > 0)
								device.setCxIp(ipAndPort[0]);

							if (ipAndPort != null && ipAndPort.length > 1)
								device.setCxPort(Integer.valueOf(ipAndPort[1]));
						} catch (NumberFormatException e) {
							device.setCxPort(0);
						}

					} else {
						device.setCxIp(cxIp);
						device.setCxPort(0);
					}
					log.info("BLRROIMqFrMapper " + "Device constructed .......");
					roiMQWorkorderVO.setDeviceData(device);
					String fullyQualifiedName = roiMQWorkorderVO.getCityId() + "_" + roiMQWorkorderVO.getBranchId()
							+ "_" + roiMQWorkorderVO.getAreaId() + "_" + roiMQWorkorderVO.getAssignedTo();
					roiMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);

					log.info("BLRROIMqFrMapper " + "Original content for ROI FR Mapper is city " + row.get("city")
							+ " branch " + row.get("branch") + " and area " + row.get("area") + " for WO "
							+ row.get("WO_NO") + ", MQ " + row.get("MQ_ID") + " Created on  " + row.get("REQ_DATE")
							+ " modified on " + row.get("MODIFIED_DATE"));
					log.info("FR data ###### Test " + roiMQWorkorderVO.toString());

				} catch (Exception e) {
					log.error("BLRROIMqFrMapper " + "Error in fr row mapper " + e.getMessage());
					e.printStackTrace();
					continue;
				}

				roiMqOpenTickets.add(roiMQWorkorderVO);

			}

			log.info("Total Fr Ticket found in ROI : branch " + branchName + " is " + roiMqOpenTickets.size());

			if (!roiMqOpenTickets.isEmpty())
				lastTimeFrDataFetched = roiMqOpenTickets.get(0).getReqDate() + ":00";

			branchBasedLastFetchedTime.put(branchName, lastTimeFrDataFetched);

			return roiMqOpenTickets;

		} catch (Exception e) {
			log.error("exception in getting FR Tickets for MQ ROI  branch " + branchName + e.getMessage(), e);

			e.printStackTrace();
		}

		return roiMqOpenTickets;

	}

	class BLRROIMqFrMapper implements RowMapper<MQWorkorderVO> {

		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo) throws SQLException {
			MQWorkorderVO roiMQWorkorderVO = new MQWorkorderVO();
			  String lastTimeFrDataFetched = null;
			log.info("BLRROIMqFrMapper " + rowNo + "Entry  ");
			if (rowNo == 0) {
				log.info("BLRROIMqFrMapper " + rowNo + " First time stamp " + lastTimeFrDataFetched);
				lastTimeFrDataFetched = resultSet.getString("REQ_DATE") + ":00";
				cityBasedLastFetchedTime.put(resultSet.getString("city").toUpperCase(), lastTimeFrDataFetched);
			}

			try {

				log.info("BLRROIMqFrMapper " + rowNo + " Getting area " + resultSet.getString("area") + " for "
						+ resultSet.getString("WO_NO"));
				Long areaId = areaDAO.getIdByCode(resultSet.getString("area"));
				log.info("Got area for " + resultSet.getString("area") + " for " + resultSet.getString("WO_NO"));

				Long branchId = null;
				log.info("BLRROIMqFrMapper " + "Getting branch " + resultSet.getString("branch") + " for "
						+ resultSet.getString("WO_NO"));
				if (resultSet.getString("branch") != null) {

					log.info("\"BLRROIMqFrMapper \"+ resultSet.getString(\"branch\") != null");
					if (resultSet.getString("branch").equalsIgnoreCase(FWMPConstant.REALTY)) {
						branchId = null;
						log.info("BLRROIMqFrMapper " + "resultSet.getString(\"branch\") == FWMPConstant.REALTY");
						branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
								resultSet.getString("branch").toUpperCase().trim(),
								resultSet.getString("city").toUpperCase());
					} else {

						branchId = branchDAO.getBranchIdByName(resultSet.getString("branch").toUpperCase());
					}

				}

				log.info("BLRROIMqFrMapper " + branchId + " Got branch " + resultSet.getString("branch") + " for "
						+ resultSet.getString("WO_NO"));

				log.info("BLRROIMqFrMapper " + "Getting city " + resultSet.getString("city") + " for "
						+ resultSet.getString("WO_NO"));
				Long cityId = cityDAO.getCityIdByName(resultSet.getString("city").toUpperCase());
				log.info("BLRROIMqFrMapper " + cityId + " Got city" + resultSet.getString("city") + " for "
						+ resultSet.getString("WO_NO"));

				if (areaId != null)
					roiMQWorkorderVO.setAreaId(areaId + "");

				if (branchId != null)
					roiMQWorkorderVO.setBranchId(branchId + "");

				if (cityId != null)
					roiMQWorkorderVO.setCityId(cityId + "");
				log.info("BLRROIMqFrMapper " + "Start extract.......");
				roiMQWorkorderVO.setComment(resultSet.getString("COMMENTS"));
				roiMQWorkorderVO.setCommentCode(resultSet.getString("COMMENT_CODE"));
				roiMQWorkorderVO.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));
				roiMQWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));
				roiMQWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));
				roiMQWorkorderVO.setCxIp(resultSet.getString("CX_IP"));
				roiMQWorkorderVO.setFxName(resultSet.getString("FX_NAME"));
				roiMQWorkorderVO.setMqId(resultSet.getString("MQ_ID"));
				roiMQWorkorderVO.setProspectNumber(resultSet.getString("PROSPECT_NO"));
				roiMQWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));
				roiMQWorkorderVO.setResponseStatus(resultSet.getString("RESP_STATUS"));
				roiMQWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));
				roiMQWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));
				roiMQWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));
				roiMQWorkorderVO.setVoc(resultSet.getString("VOC"));
				roiMQWorkorderVO.setModifiedDate(resultSet.getString("MODIFIED_DATE"));
				log.info("BLRROIMqFrMapper " + "End extract.......");
				FrDeviceVo device = new FrDeviceVo();
				device.setFxName(roiMQWorkorderVO.getFxName());

				String cxIp = roiMQWorkorderVO.getCxIp();
				if (cxIp != null && !cxIp.isEmpty() && cxIp.contains("/")) {
					try {
						String[] ipAndPort = cxIp.split("/");

						if (ipAndPort != null && ipAndPort.length > 0)
							device.setCxIp(ipAndPort[0]);

						if (ipAndPort != null && ipAndPort.length > 1)
							device.setCxPort(Integer.valueOf(ipAndPort[1]));
					} catch (NumberFormatException e) {
						device.setCxPort(0);
					}

				} else {
					device.setCxIp(cxIp);
					device.setCxPort(0);
				}
				log.info("BLRROIMqFrMapper " + "Device constructed .......");
				roiMQWorkorderVO.setDeviceData(device);
				String fullyQualifiedName = roiMQWorkorderVO.getCityId() + "_" + roiMQWorkorderVO.getBranchId() + "_"
						+ roiMQWorkorderVO.getAreaId() + "_" + roiMQWorkorderVO.getAssignedTo();
				roiMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);

				log.info("BLRROIMqFrMapper " + "Original content for ROI FR Mapper is city "
						+ resultSet.getString("city") + " branch " + resultSet.getString("branch") + " and area "
						+ resultSet.getString("area") + " for WO " + resultSet.getString("WO_NO") + ", MQ "
						+ resultSet.getString("MQ_ID") + " Created on  " + resultSet.getString("REQ_DATE")
						+ " modified on " + resultSet.getString("MODIFIED_DATE"));
				log.info("FR data ###### Test " + roiMQWorkorderVO.toString());

			} catch (Exception e) {
				log.error("BLRROIMqFrMapper " + "Error in fr row mapper " + e.getMessage());
				e.printStackTrace();
			}

			return roiMQWorkorderVO;
		}
	}

	class ROIMqFrMapper implements RowMapper<MQWorkorderVO> {
		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo) throws SQLException {
			MQWorkorderVO roiMQWorkorderVO = new MQWorkorderVO();
			roiMQWorkorderVO.setLogMessage(" Rowmapper " + new Date());

			if (rowNo == 0) {
				cityBasedLastFetchedTime.put(resultSet.getString("city").toUpperCase(), resultSet.getString("REQ_DATE") + ":00");
			}

			try {

				log.info(rowNo + " Getting area " + resultSet.getString("area") + " for "
						+ resultSet.getString("WO_NO"));
				Long areaId = areaDAO.getIdByCode(resultSet.getString("area"));
				log.info("Got area for " + resultSet.getString("area") + " for " + resultSet.getString("WO_NO"));

				Long branchId = null;
				log.info("Getting branch " + resultSet.getString("branch") + " for " + resultSet.getString("WO_NO"));
				if (resultSet.getString("branch") != null) {
					if (resultSet.getString("branch").equalsIgnoreCase(FWMPConstant.REALTY)) {
						branchId = null;
						log.info("Getting realty branch  " + "resultSet.getString(\"branch\") == FWMPConstant.REALTY");
						branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
								resultSet.getString("branch").toUpperCase().trim(),
								resultSet.getString("city").toUpperCase());
					} else {

						branchId = branchDAO.getBranchIdByName(resultSet.getString("branch").toUpperCase());
					}

				}

				log.info("Got branch " + resultSet.getString("branch") + " for " + resultSet.getString("WO_NO"));

				log.info("Getting city " + resultSet.getString("city") + " for " + resultSet.getString("WO_NO"));
				Long cityId = cityDAO.getCityIdByName(resultSet.getString("city").toUpperCase());
				log.info("Got city" + resultSet.getString("city") + " for " + resultSet.getString("WO_NO"));

				if (areaId != null)
					roiMQWorkorderVO.setAreaId(areaId + "");

				if (branchId != null)
					roiMQWorkorderVO.setBranchId(branchId + "");

				if (cityId != null)
					roiMQWorkorderVO.setCityId(cityId + "");

				roiMQWorkorderVO.setComment(resultSet.getString("COMMENTS"));
				roiMQWorkorderVO.setCommentCode(resultSet.getString("COMMENT_CODE"));
				roiMQWorkorderVO.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));
				roiMQWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));
				roiMQWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));
				roiMQWorkorderVO.setCxIp(resultSet.getString("CX_IP"));
				roiMQWorkorderVO.setFxName(resultSet.getString("FX_NAME"));
				roiMQWorkorderVO.setMqId(resultSet.getString("MQ_ID"));
				roiMQWorkorderVO.setProspectNumber(resultSet.getString("PROSPECT_NO"));
				roiMQWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));
				roiMQWorkorderVO.setResponseStatus(resultSet.getString("RESP_STATUS"));
				roiMQWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));
				roiMQWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));
				roiMQWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));
				roiMQWorkorderVO.setVoc(resultSet.getString("VOC"));
				roiMQWorkorderVO.setModifiedDate(resultSet.getString("MODIFIED_DATE"));
				
				FrDeviceVo device = new FrDeviceVo();
				device.setFxName(roiMQWorkorderVO.getFxName());
				roiMQWorkorderVO.setLogMessage(" wo " + roiMQWorkorderVO.getWorkOrderNumber());

				String cxIp = roiMQWorkorderVO.getCxIp();
				if (cxIp != null && !cxIp.isEmpty() && cxIp.contains("/")) {
					try {
						String[] ipAndPort = cxIp.split("/");

						if (ipAndPort != null && ipAndPort.length > 0)
							device.setCxIp(ipAndPort[0]);

						if (ipAndPort != null && ipAndPort.length > 1)
							device.setCxPort(Integer.valueOf(ipAndPort[1]));
					} catch (NumberFormatException e) {
						device.setCxPort(0);
					}

				} else {
					device.setCxIp(cxIp);
					device.setCxPort(0);
				}

				roiMQWorkorderVO.setDeviceData(device);
				String fullyQualifiedName = roiMQWorkorderVO.getCityId() + "_" + roiMQWorkorderVO.getBranchId() + "_"
						+ roiMQWorkorderVO.getAreaId() + "_" + roiMQWorkorderVO.getAssignedTo();

				roiMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);

				log.info("Original content for ROI FR Mapper is city " + resultSet.getString("city") + " branch "
						+ resultSet.getString("branch") + " and area " + resultSet.getString("area") + " for WO "
						+ resultSet.getString("WO_NO") + ", MQ " + resultSet.getString("MQ_ID") + " Created on  "
						+ resultSet.getString("REQ_DATE") + " modified on " + resultSet.getString("MODIFIED_DATE"));
				log.info("FR data ###### Test " + roiMQWorkorderVO.toString());

			} catch (Exception e) {
				log.error("Error in fr row mapper " + e.getMessage());
				e.printStackTrace();
			}

			return roiMQWorkorderVO;
		}
	}

}
