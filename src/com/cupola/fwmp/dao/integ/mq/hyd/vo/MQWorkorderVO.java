/**
 * 
 */
package com.cupola.fwmp.dao.integ.mq.hyd.vo;

import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;

/**
 * @author aditya
 * 
 */
public class MQWorkorderVO {
	private String workOrderNumber;
	private String prospectNumber;
	private String mqId;

	private String customerName;
	private String customerAddress;
	private String customerMobile;

	private String ticketCategory;
	private String responseStatus;
	private String symptom;
	private String reqDate;
	private String comment;
	private String commentCode;
	private String cxIp;
	private String fxName;

	private Long ticketId;

	private String areaId;
	private String branchId;
	private String cityId;
	private String modifiedDate;
	private Long assignedTo;

	private Long innitialWorkStage;
	private String currentWorkStage;

	private String fullyQualifiedName;

	private String voc;
	private boolean locked;
	private String ticketSource;
	private boolean blocked;
	private String altMobileNumber;
	private FrDeviceVo deviceData;

	private StringBuilder logMessage = new StringBuilder("");

	/**
	 * 
	 */
	public MQWorkorderVO() {

	}

	/**
	 * @return the logMessage
	 */
	public StringBuilder getLogMessage() {
		return logMessage;
	}

	/**
	 * @param logMessage
	 *            the logMessage to set
	 */
	public void setLogMessage(String logMessage) {

		this.logMessage.append(logMessage);
	}

	public synchronized String getWorkOrderNumber() {
		return workOrderNumber;
	}

	public synchronized void setWorkOrderNumber(String workOrderNumber) {
		this.workOrderNumber = workOrderNumber;
	}

	public synchronized String getProspectNumber() {
		return prospectNumber;
	}

	public synchronized void setProspectNumber(String prospectNumber) {
		this.prospectNumber = prospectNumber;
	}

	public synchronized String getMqId() {
		return mqId;
	}

	public synchronized void setMqId(String mqId) {
		this.mqId = mqId;
	}

	public synchronized String getCustomerName() {
		return customerName;
	}

	public synchronized void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public synchronized String getCustomerAddress() {
		return customerAddress;
	}

	public synchronized void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public synchronized String getCustomerMobile() {
		return customerMobile;
	}

	public synchronized void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public synchronized String getTicketCategory() {
		return ticketCategory;
	}

	public synchronized void setTicketCategory(String ticketCategory) {
		this.ticketCategory = ticketCategory;
	}

	public synchronized String getResponseStatus() {
		return responseStatus;
	}

	public synchronized void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public synchronized String getSymptom() {
		return symptom;
	}

	public synchronized void setSymptom(String symptom) {
		this.symptom = symptom;
	}

	public synchronized String getReqDate() {
		return reqDate;
	}

	public synchronized void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}

	public synchronized String getComment() {
		return comment;
	}

	public synchronized void setComment(String comment) {
		this.comment = comment;
	}

	public synchronized String getCommentCode() {
		return commentCode;
	}

	public synchronized void setCommentCode(String commentCode) {
		this.commentCode = commentCode;
	}

	public synchronized String getCxIp() {
		return cxIp;
	}

	public synchronized void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}

	public synchronized String getFxName() {
		return fxName;
	}

	public synchronized void setFxName(String fxName) {
		this.fxName = fxName;
	}

	public synchronized Long getTicketId() {
		return ticketId;
	}

	public synchronized void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public synchronized String getAreaId() {
		return areaId;
	}

	public synchronized void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public synchronized String getBranchId() {
		return branchId;
	}

	public synchronized void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public synchronized String getCityId() {
		return cityId;
	}

	public synchronized void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public synchronized Long getAssignedTo() {
		return assignedTo;
	}

	public synchronized void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}

	public synchronized Long getInnitialWorkStage() {
		return innitialWorkStage;
	}

	public synchronized void setInnitialWorkStage(Long innitialWorkStage) {
		this.innitialWorkStage = innitialWorkStage;
	}

	public synchronized String getCurrentWorkStage() {
		return currentWorkStage;
	}

	public synchronized void setCurrentWorkStage(String currentWorkStage) {
		this.currentWorkStage = currentWorkStage;
	}

	public synchronized String getFullyQualifiedName() {
		return fullyQualifiedName;
	}

	public synchronized void setFullyQualifiedName(String fullyQualifiedName) {
		this.fullyQualifiedName = fullyQualifiedName;
	}

	public synchronized String getVoc() {
		return voc;
	}

	public synchronized void setVoc(String voc) {
		this.voc = voc;
	}

	public synchronized boolean isLocked() {
		return locked;
	}

	public synchronized void setLocked(boolean locked) {
		this.locked = locked;
	}

	public synchronized String getTicketSource() {
		return ticketSource;
	}

	public synchronized void setTicketSource(String ticketSource) {
		this.ticketSource = ticketSource;

	}

	public synchronized boolean isBlocked() {
		return blocked;
	}

	public synchronized void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public synchronized FrDeviceVo getDeviceData() {
		return deviceData;
	}

	public synchronized void setDeviceData(FrDeviceVo deviceData) {
		this.deviceData = deviceData;
	}

	/**
	 * @return the modifiedDate
	 */
	public synchronized String getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            the modifiedDate to set
	 */
	public synchronized void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @param logMessage
	 *            the logMessage to set
	 */
	public synchronized void setLogMessage(StringBuilder logMessage) {
		this.logMessage = logMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MQWorkorderVO [workOrderNumber=" + workOrderNumber + ", prospectNumber=" + prospectNumber + ", mqId="
				+ mqId + ", customerName=" + customerName + ", customerAddress=" + customerAddress + ", customerMobile="
				+ customerMobile + ", ticketCategory=" + ticketCategory + ", responseStatus=" + responseStatus
				+ ", symptom=" + symptom + ", reqDate=" + reqDate + ", comment=" + comment + ", commentCode="
				+ commentCode + ", cxIp=" + cxIp + ", fxName=" + fxName + ", ticketId=" + ticketId + ", areaId="
				+ areaId + ", branchId=" + branchId + ", cityId=" + cityId + ", modifiedDate=" + modifiedDate
				+ ", assignedTo=" + assignedTo + ", innitialWorkStage=" + innitialWorkStage + ", currentWorkStage="
				+ currentWorkStage + ", fullyQualifiedName=" + fullyQualifiedName + ", voc=" + voc + ", locked="
				+ locked + ", ticketSource=" + ticketSource + ", blocked=" + blocked + ", deviceData=" + deviceData
				+ ", logMessage=" + logMessage + "]";
	}

	/**
	 * @return the altMobileNumber
	 */
	public String getAltMobileNumber() {
		return altMobileNumber;
	}

	/**
	 * @param altMobileNumber the altMobileNumber to set
	 */
	public void setAltMobileNumber(String altMobileNumber) {
		this.altMobileNumber = altMobileNumber;
				
	}
}
