package com.cupola.fwmp.dao.integ.mq.hyd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.integ.mq.hyd.HydMqDAOImpl.HydMqDAOImplMapper;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.CommonUtil;

public class HydMqDAOImpl implements HydMqDAO
{

	private static Logger log = LogManager
			.getLogger(HydMqDAOImpl.class.getName());

	// @Resource(name="mqHydjdbcTemplate")
	@Autowired
	@Qualifier("mqHydjdbcTemplate")
	JdbcTemplate mqHydjdbcTemplate;

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	UserDao userDao;

	@Autowired
	DefinitionCoreService definitionCoreService;

	private String getAllOpenTicket;

	String lastDataFetchedTimeForHYD = null;
	String lastDataFetchedTimeManualForHYD = null;
	Set<String> previousWOhyd = null;
	
	private String getAllOpenFrTicket;
	private String lastTimeFrDataFetched = null;
	
	private String lastTimeFrDataFetchedEveryHours = null;

	private String timeToMinusInString =  null;
	private String daysToMinusInString =  null;

	public void setGetAllOpenTicket(String getAllOpenTicket)
	{
		this.getAllOpenTicket = getAllOpenTicket;
	}



	@Override
	public List<MQWorkorderVO> getAllOpenTicketForHYD()
	{
		List<MQWorkorderVO> hydMqOpenTickets = null;
		try
		{
			log.info("################ TEST ############ Get all the open ticket for HYD");
		
			getAllOpenTicket = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_HYD_QUERY);

			log.debug("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD);
			
			String timeToMinusInString = definitionCoreService.getClassificationQuery(
					DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON);
		
			String daysToMinusInString = definitionCoreService.getClassificationQuery(
					DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME);


			log.info("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD + " timeToMinusInString " + timeToMinusInString);
			
			if (lastDataFetchedTimeForHYD == null)
			{
				log.info("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD + " daysToMinusInString "
						+ daysToMinusInString);
				
				lastDataFetchedTimeForHYD = CommonUtil
						.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
								: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);

			} else
			{
				lastDataFetchedTimeForHYD = CommonUtil.getPriviousDateByMinutes(lastDataFetchedTimeForHYD,
						timeToMinusInString != null ? Integer.valueOf(timeToMinusInString)
								: FWMPConstant.MINUS_MINUTES_FROM_CURRENT_TIME);
			}

		
			log.debug("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD);
			

			if (previousWOhyd == null)
			{
				previousWOhyd = new LinkedHashSet<String>();
				previousWOhyd.add("-1");

			} else if (previousWOhyd.isEmpty())
			{
				previousWOhyd.add("-1");
			}

//			String previousWOs = StringUtils.join(previousWOhyd, ",");

			hydMqOpenTickets = mqHydjdbcTemplate
					.query(getAllOpenTicket, new Object[] {
							lastDataFetchedTimeForHYD }, new HydMqDAOImplMapper());

			log.info("################################################# test HYD "
					+ hydMqOpenTickets.size());

			if (previousWOhyd != null)
				previousWOhyd.clear();

			if (!hydMqOpenTickets.isEmpty())
				lastDataFetchedTimeForHYD = hydMqOpenTickets.get(0).getReqDate()
						+ ":00";

			log.info("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD);
			List<MQWorkorderVO> mqOpenTickets = new ArrayList<MQWorkorderVO>();
			MQWorkorderVO mqWorkorderVO = null;
			for (Iterator<MQWorkorderVO> iterator = hydMqOpenTickets
					.iterator(); iterator.hasNext();)
			{
				mqWorkorderVO = (MQWorkorderVO) iterator.next();
				try 
				{
					if (mqWorkorderVO.getCityId() != null
							&& !"".equals(mqWorkorderVO.getCityId()))
						mqOpenTickets.add(mqWorkorderVO);
					
					log.info("HYD WO " + mqWorkorderVO.getWorkOrderNumber() 
					+ " MQ " + mqWorkorderVO.getMqId() + " PROSPECT " 
					+ mqWorkorderVO.getProspectNumber() + " MQ CREATED TIME "
					+ mqWorkorderVO.getReqDate());

//					log.info("HYD WO Complete Object is " + mqWorkorderVO);
					
					previousWOhyd.add(mqWorkorderVO.getWorkOrderNumber());
					
				} catch (Exception e) 
				{
					log.error("HYD Exception in processing data for " + mqWorkorderVO);
					e.printStackTrace();
					continue;
				}
			}

			return mqOpenTickets;
			
		} catch (Exception e)
		{
			log.error("Exception in getting Tickets from CRM HYD "
					+ e.getMessage(), e);
			e.printStackTrace();
			
		}
		
		return hydMqOpenTickets;
	}
	
	@Override
	public List<MQWorkorderVO> getAllOpenTicketForHYDSpecial()
	{
		try
		{
			log.info("################ TEST ############ Get all the open ticket for HYD Special cron ###########################");

			getAllOpenTicket = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_HYD_QUERY);

			String timeToMinusInString = definitionCoreService.getClassificationQuery(
					DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_SPECIAL_CRON);
		
			String daysToMinusInString = definitionCoreService.getClassificationQuery(
					DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME);

			log.info("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD + " timeToMinusInString " + timeToMinusInString);
			
			if (lastDataFetchedTimeManualForHYD == null)
			{
				log.info("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD + " daysToMinusInString "
						+ daysToMinusInString);
				
				lastDataFetchedTimeForHYD = CommonUtil
						.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
								: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);

			} else
			{
				lastDataFetchedTimeManualForHYD = CommonUtil.getPriviousDateByMinutes(lastDataFetchedTimeManualForHYD,
						timeToMinusInString != null ? Integer.valueOf(timeToMinusInString)
								: 200);
			}

			log.info("lastDataFetchedTimeManualForHYD " + lastDataFetchedTimeManualForHYD);
			
//			 String previousWOs = StringUtils.join(previousWOHYD, ",");

			List<MQWorkorderVO> HYDMqOpenTickets = mqHydjdbcTemplate
					.query(getAllOpenTicket, new Object[] {
							lastDataFetchedTimeManualForHYD }, new HydMqDAOImplMapper());

			log.info("################################################# test HYD Special cron $############################### "
					+ HYDMqOpenTickets.size());

			if (!HYDMqOpenTickets.isEmpty())
				lastDataFetchedTimeManualForHYD = HYDMqOpenTickets.get(0).getReqDate()
						+ ":00";

			List<MQWorkorderVO> mqOpenTickets = new ArrayList<MQWorkorderVO>();
			MQWorkorderVO mqWorkorderVO = null;
			
			for (Iterator<MQWorkorderVO> iterator = HYDMqOpenTickets
					.iterator(); iterator.hasNext();)
			{
				mqWorkorderVO = (MQWorkorderVO) iterator.next();
				try {
					
					if (mqWorkorderVO.getCityId() != null
							&& !"".equals(mqWorkorderVO.getCityId()))
						mqOpenTickets.add(mqWorkorderVO);
					
					log.info("Special cron HYD WO " + mqWorkorderVO.getWorkOrderNumber() + " MQ "
							+ mqWorkorderVO.getMqId() + " PROSPECT " + mqWorkorderVO.getProspectNumber()
							+ " MQ CREATED TIME " + mqWorkorderVO.getReqDate());
					
//					log.info("Special cron HYD WO Complete Object is " + mqWorkorderVO);
					
				} catch (Exception e) 
				{
					log.error("Special cron HYD Exception in processing data for " + mqWorkorderVO);
					e.printStackTrace();
					continue;
				}
			}

			return mqOpenTickets;
		} catch (Exception e)
		{
			log.error("exception in getting tickets for MQ HYD  while running special cron "
					+ e.getMessage(), e);
			
			e.printStackTrace();
		}
		return null;
	}

	class HydMqDAOImplMapper implements RowMapper<MQWorkorderVO>
	{
		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo)
				throws SQLException
		{
			MQWorkorderVO hydMQWorkorderVO = new MQWorkorderVO();


			try {
				Long areaId = null;
				if(resultSet.getString("area") != null)
					areaId = areaDAO.getIdByCode(resultSet.getString("area"));
				
				Long branchId = null;
				if(resultSet.getString("branch") != null)
				{
					if (resultSet.getString("branch").equalsIgnoreCase(FWMPConstant.REALTY)) {
					
						branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
								resultSet.getString("branch").toUpperCase().trim(), resultSet.getString("city").toUpperCase());
					} else {
						
						branchId = branchDAO.getBranchIdByName(resultSet.getString("branch").toUpperCase());
					}

				}

				Long cityId = null;
				if(resultSet.getString("city") != null)
				 cityId = cityDAO
						.getCityIdByName(resultSet.getString("city").toUpperCase());

				if (areaId != null)
					hydMQWorkorderVO.setAreaId(areaId + "");

				if (branchId != null)
					hydMQWorkorderVO.setBranchId(branchId + "");

				if (cityId != null)
					hydMQWorkorderVO.setCityId(cityId + "");

				if(resultSet.getString("COMMENTS") != null)
					hydMQWorkorderVO.setComment(resultSet.getString("COMMENTS"));
				
				if(resultSet.getString("COMMENT_CODE") != null)
					hydMQWorkorderVO
						.setCommentCode(resultSet.getString("COMMENT_CODE"));
				
				if(resultSet.getString("CUST_ADDRESS") != null)
					hydMQWorkorderVO
						.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));
				
				if(resultSet.getString("PHONE") != null)
					hydMQWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));
				
				if(resultSet.getString("CUST_NAME") != null)
					hydMQWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));
				
				if(resultSet.getString("CX_IP") != null)
					hydMQWorkorderVO.setCxIp(resultSet.getString("CX_IP"));
				
				if(resultSet.getString("FX_NAME") != null)
					hydMQWorkorderVO.setFxName(resultSet.getString("FX_NAME"));
				
				if(resultSet.getString("MQ_ID") != null)
					hydMQWorkorderVO.setMqId(resultSet.getString("MQ_ID"));
				
				if(resultSet.getString("PROSPECT_NO") != null)
					hydMQWorkorderVO
						.setProspectNumber(resultSet.getString("PROSPECT_NO"));
				
				if(resultSet.getString("REQ_DATE") != null)
					hydMQWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));
				
				if(resultSet.getString("RESP_STATUS") != null)
					hydMQWorkorderVO
						.setResponseStatus(resultSet.getString("RESP_STATUS"));
				
				if(resultSet.getString("SYMP_NAME") != null)
					hydMQWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));
				
				if(resultSet.getString("CAT_NAME") != null)
					hydMQWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));
				
				if(resultSet.getString("WO_NO") != null)
					hydMQWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));
				
				if(resultSet.getString("VOC") != null)
					hydMQWorkorderVO.setVoc(resultSet.getString("VOC"));
				
				hydMQWorkorderVO.setModifiedDate(resultSet.getString("MODIFIED_DATE"));
				
				Long assignedTo = null;
				
				if(hydMQWorkorderVO.getFxName() !=null)
					assignedTo = userDao
						.getUserIdByDevcieName(hydMQWorkorderVO.getFxName());
					hydMQWorkorderVO.setAssignedTo(assignedTo);

				
				
				String fullyQualifiedName = hydMQWorkorderVO.getCityId() + "_"
						+ hydMQWorkorderVO.getBranchId() + "_"
						+ hydMQWorkorderVO.getAreaId() + "_"
						+ hydMQWorkorderVO.getAssignedTo();

				hydMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);
				
				hydMQWorkorderVO.setTicketSource("OUTSIDE FWMP");		
				
				log.info("Original content for HYD is city " + resultSet.getString("city") + " branch " + resultSet.getString("branch") 
				+" and area " +resultSet.getString("area") + " for WO " + resultSet.getString("WO_NO") 
				+ ", MQ " + resultSet.getString("MQ_ID") + " modified date " + resultSet.getString("MODIFIED_DATE"));
				
				
			} catch (Exception e) 
			{
				log.error("Error while preparing data from result set while iterating HYD " + e.getMessage());
				e.printStackTrace();
			}
			return hydMQWorkorderVO;
		}
	}
	Map<String, String> branchBasedLastFetchedTime = new HashMap<>();
	@Override
	public List<MQWorkorderVO> getAllOpenFrTicketForHYD(String branchName)
	{
		List<MQWorkorderVO> hydMqOpenTickets = null;
		try
		{
			lastTimeFrDataFetched = branchBasedLastFetchedTime.containsKey(branchName) ? branchBasedLastFetchedTime.get(branchName) : null;
			
			log.info("*********** Fetching FR Ticket from HYD " + branchName
					+ "************ MQ CALL TIME FROM FWMP****** for " + new Date());

			getAllOpenFrTicket = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_FR_HYD_QUERY);
			log.info("lastDataFetchedTimeForHYD " + lastDataFetchedTimeForHYD);
			
			timeToMinusInString = definitionCoreService.getClassificationQuery(
					DefinitionCoreService.SUBSTRACT_MINUTES_TO_CURRENT_TIME_FOR_REGULAR_CRON_FR);
		
			daysToMinusInString = definitionCoreService.getClassificationQuery(
					DefinitionCoreService.SUBSTRACT_DAYS_TO_CURRENT_TIME_FR);

			if (lastTimeFrDataFetched == null)
				lastTimeFrDataFetched = CommonUtil.getTodayMidnight(daysToMinusInString != null ? Integer.valueOf(daysToMinusInString)
						: FWMPConstant.MINUS_DAYS_FROM_CURRENT_TIME);
			
			else 
				lastTimeFrDataFetched = CommonUtil.getPriviousDateByMinutes( lastTimeFrDataFetched ,
						timeToMinusInString != null ? Integer.valueOf(timeToMinusInString)
								: FWMPConstant.MINUS_MINUTES_FROM_CURRENT_TIME);
			log.info(" FR GOING TO FETCH TICKETS FROM MQ  " + lastTimeFrDataFetched + " branchName " + branchName);
			
			hydMqOpenTickets = mqHydjdbcTemplate
					.query(getAllOpenFrTicket, new Object[] {branchName,
							lastTimeFrDataFetched,lastTimeFrDataFetched }, new HydMqFrMapper());

			log.info("Total Fr Ticket found in HYD : "+ hydMqOpenTickets.size() + " for branchName " + branchName);
			
			for (Iterator<MQWorkorderVO> iterator = hydMqOpenTickets.iterator(); iterator.hasNext();) {
				MQWorkorderVO mqWorkorderVO = (MQWorkorderVO) iterator.next();
				log.info("HYD WO ######### " + mqWorkorderVO );
			}
			
			if (!hydMqOpenTickets.isEmpty())
				lastTimeFrDataFetched = hydMqOpenTickets.get(0).getReqDate()
						+ ":00";
			branchBasedLastFetchedTime.put(branchName, lastTimeFrDataFetched);
			return hydMqOpenTickets;
			
		} 
		catch (Exception e)
		{
			log.error("Exception in getting FR Tickets from CRM HYD " + branchName
					+ e.getMessage());
			e.printStackTrace();
			
		}
		return hydMqOpenTickets;
	}
	
	class HydMqFrMapper implements RowMapper<MQWorkorderVO>
	{
		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo)
				throws SQLException
		{
			log.debug("result set  :"+resultSet.getString("branch").toUpperCase().trim());
			MQWorkorderVO hydMQWorkorderVO = new MQWorkorderVO();

			log.info("Getting area "+resultSet.getString("area")+" for " + resultSet.getString("WO_NO"));
			Long areaId = areaDAO.getIdByCode(resultSet.getString("area"));
			log.info("Got area for " +resultSet.getString("area")+" for "+ resultSet.getString("WO_NO"));

			Long branchId = null;
			log.info("Getting branch "+resultSet.getString("branch")+" for " + resultSet.getString("WO_NO"));
			if (resultSet.getString("branch") != null) {
				if (resultSet.getString("branch").equalsIgnoreCase(FWMPConstant.REALTY)) {

					branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
							resultSet.getString("branch").toUpperCase().trim(),
							resultSet.getString("city").toUpperCase());
				} else {

					branchId = branchDAO.getBranchIdByName(resultSet.getString("branch").toUpperCase());
				}

			}
			
			log.info("Got branch "+resultSet.getString("branch")+" for " + resultSet.getString("WO_NO"));
			
			log.info("Getting city " + resultSet.getString("city") + " for " + resultSet.getString("WO_NO"));
			Long cityId = cityDAO.getCityIdByName(resultSet.getString("city").toUpperCase());
			log.info("Got city"+resultSet.getString("city")+" for " + resultSet.getString("WO_NO"));
			
			if (areaId != null)
				hydMQWorkorderVO.setAreaId(areaId + "");

			if (branchId != null)
				hydMQWorkorderVO.setBranchId(branchId + "");

			if (cityId != null)
				hydMQWorkorderVO.setCityId(cityId + "");

			if(resultSet.getString("COMMENTS") != null)
				hydMQWorkorderVO.setComment(resultSet.getString("COMMENTS"));
			
			if(resultSet.getString("COMMENT_CODE") != null)
				hydMQWorkorderVO
					.setCommentCode(resultSet.getString("COMMENT_CODE"));
			
			if(resultSet.getString("CUST_ADDRESS") != null)
				hydMQWorkorderVO
					.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));
			
			if(resultSet.getString("PHONE") != null)
				hydMQWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));
			
			if(resultSet.getString("CUST_NAME") != null)
				hydMQWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));
			
			if(resultSet.getString("CX_IP") != null)
				hydMQWorkorderVO.setCxIp(resultSet.getString("CX_IP"));
			
			if(resultSet.getString("FX_NAME") != null)
				hydMQWorkorderVO.setFxName(resultSet.getString("FX_NAME"));
			
			if(resultSet.getString("MQ_ID") != null)
				hydMQWorkorderVO.setMqId(resultSet.getString("MQ_ID"));
			
			if(resultSet.getString("PROSPECT_NO") != null)
				hydMQWorkorderVO
					.setProspectNumber(resultSet.getString("PROSPECT_NO"));
			
			if(resultSet.getString("REQ_DATE") != null)
				hydMQWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));
			
			if(resultSet.getString("RESP_STATUS") != null)
				hydMQWorkorderVO
					.setResponseStatus(resultSet.getString("RESP_STATUS"));

			hydMQWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));
			hydMQWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));
			hydMQWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));
			hydMQWorkorderVO.setVoc(resultSet.getString("VOC"));
			hydMQWorkorderVO.setModifiedDate(resultSet.getString("MODIFIED_DATE"));
			FrDeviceVo device = new FrDeviceVo();
			device.setFxName(hydMQWorkorderVO.getFxName());
			
			String cxIp = hydMQWorkorderVO.getCxIp();
			if( cxIp != null && !cxIp.isEmpty() && cxIp.contains("/"))
			{
				try
				{
					String[] ipAndPort = cxIp.split("/");
					
					if ( ipAndPort != null && ipAndPort.length > 0 )
						device.setCxIp(ipAndPort[0]);
				
					if( ipAndPort != null && ipAndPort.length > 1)
						device.setCxPort(Integer.valueOf(ipAndPort[1]));
				} 
				catch (NumberFormatException e)
				{
					device.setCxPort(0);
				}
				
			}
			else
			{
				device.setCxIp(cxIp);
				device.setCxPort(0);
			}
			
			hydMQWorkorderVO.setDeviceData(device);

			String fullyQualifiedName = hydMQWorkorderVO.getCityId() + "_"
					+ hydMQWorkorderVO.getBranchId() + "_"
					+ hydMQWorkorderVO.getAreaId() + "_"
					+ hydMQWorkorderVO.getAssignedTo();

			hydMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);
		
			log.info(" FR Details WO HYD " + hydMQWorkorderVO.getWorkOrderNumber() + " "
					+ " roiMQWorkorderVO.getComment " + hydMQWorkorderVO.getComment() + " CommentCode "
					+ hydMQWorkorderVO.getCommentCode() + " Symptom " + hydMQWorkorderVO.getSymptom()
					+ " TicketCategory " + hydMQWorkorderVO.getTicketCategory() + " created on "
					+ hydMQWorkorderVO.getReqDate());

			
			log.info("Original content for ROI FR Mapper is city " + resultSet.getString("city") + " branch "
					+ resultSet.getString("branch") + " and area " + resultSet.getString("area") + " for WO "
					+ resultSet.getString("WO_NO") + ", MQ " + resultSet.getString("MQ_ID") + " Created on  "
					+ resultSet.getString("REQ_DATE") + " modified date " + resultSet.getString("MODIFIED_DATE"));
	
			return hydMQWorkorderVO;
		}
	}

	@Override
	public List<MQWorkorderVO> getAllFrOpenWorkOrderPrivious15Minute(String branchName)
	{

		List<MQWorkorderVO> hydMqOpenTickets = null;
		try
		{
			log.info("Fetching FR Ticket from HYD Every Hour ******* MQ CALL TIME FROM FWMP******"+new Date());
			
			getAllOpenFrTicket = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.MQ_FR_HYD_QUERY);

			lastTimeFrDataFetchedEveryHours = CommonUtil.getPriviousDateByHours( lastTimeFrDataFetchedEveryHours );
			
			log.info(" **********FR GOING TO FETCH TICKETS FROM MQ BY 60 " + lastTimeFrDataFetchedEveryHours );
			
			hydMqOpenTickets = mqHydjdbcTemplate
					.query(getAllOpenFrTicket, new Object[] {branchName,
							lastTimeFrDataFetchedEveryHours,lastTimeFrDataFetchedEveryHours }, new HydMqFrLast15Mapper());

			log.info("Total Fr Ticket found in HYD Every Hours : "+ hydMqOpenTickets.size());
			
			if (!hydMqOpenTickets.isEmpty())
				lastTimeFrDataFetchedEveryHours = hydMqOpenTickets.get(0).getReqDate()
						+ ":00";

			return hydMqOpenTickets;
			
		} 
		catch (Exception e)
		{
			log.error("Exception in getting FR Tickets from CRM HYD "
					+ e.getMessage(), e);
			e.printStackTrace();
			
		}
		return hydMqOpenTickets;
	
	}
	
	class HydMqFrLast15Mapper implements RowMapper<MQWorkorderVO>
	{
		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo)
				throws SQLException
		{
			log.debug("result set  :"+resultSet.getString("branch").toUpperCase().trim());
			MQWorkorderVO hydMQWorkorderVO = new MQWorkorderVO();

			Long areaId = areaDAO.getIdByCode(resultSet.getString("area"));
		/*	Long branchId = branchDAO
					.getIdByCode(resultSet.getString("branch").toUpperCase().trim());*/
			Long branchId = null;
			if(resultSet.getString("branch") != null)
			{
				if (resultSet.getString("branch").equalsIgnoreCase(FWMPConstant.REALTY)) {
				
					branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
							resultSet.getString("branch").toUpperCase().trim(), resultSet.getString("city").toUpperCase());
				} else {
					
					branchId = branchDAO.getBranchIdByName(resultSet.getString("branch").toUpperCase());
				}

			}


			Long cityId = cityDAO.getCityIdByName(resultSet.getString("city"));
			if (areaId != null)
				hydMQWorkorderVO.setAreaId(areaId + "");

			if (branchId != null)
				hydMQWorkorderVO.setBranchId(branchId + "");

			if (cityId != null)
				hydMQWorkorderVO.setCityId(cityId + "");

			hydMQWorkorderVO.setComment(resultSet.getString("COMMENTS"));
			hydMQWorkorderVO
					.setCommentCode(resultSet.getString("COMMENT_CODE"));
			hydMQWorkorderVO
					.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));
			hydMQWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));
			hydMQWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));
			hydMQWorkorderVO.setCxIp(resultSet.getString("CX_IP"));
			hydMQWorkorderVO.setFxName(resultSet.getString("FX_NAME"));
			hydMQWorkorderVO.setMqId(resultSet.getString("MQ_ID"));
			hydMQWorkorderVO
					.setProspectNumber(resultSet.getString("PROSPECT_NO"));
			hydMQWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));
			hydMQWorkorderVO
					.setResponseStatus(resultSet.getString("RESP_STATUS"));
			hydMQWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));
			hydMQWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));
			hydMQWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));
			hydMQWorkorderVO.setVoc(resultSet.getString("VOC"));
			hydMQWorkorderVO.setModifiedDate(resultSet.getString("MODIFIED_DATE"));
			FrDeviceVo device = new FrDeviceVo();
			device.setFxName(hydMQWorkorderVO.getFxName());
			
			String cxIp = hydMQWorkorderVO.getCxIp();
			if( cxIp != null && !cxIp.isEmpty() && cxIp.contains("/"))
			{
				try
				{
					String[] ipAndPort = cxIp.split("/");
					
					if ( ipAndPort != null && ipAndPort.length > 0 )
						device.setCxIp(ipAndPort[0]);
				
					if( ipAndPort != null && ipAndPort.length > 1)
						device.setCxPort(Integer.valueOf(ipAndPort[1]));
				} 
				catch (NumberFormatException e)
				{
					device.setCxPort(0);
				}
				
			}
			else
			{
				device.setCxIp(cxIp);
				device.setCxPort(0);
			}
			
			hydMQWorkorderVO.setDeviceData(device);

			String fullyQualifiedName = hydMQWorkorderVO.getCityId() + "_"
					+ hydMQWorkorderVO.getBranchId() + "_"
					+ hydMQWorkorderVO.getAreaId() + "_"
					+ hydMQWorkorderVO.getAssignedTo();

			hydMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);
			
			log.info("Original content for ROI FR Mapper is city " + resultSet.getString("city") + " branch "
					+ resultSet.getString("branch") + " and area " + resultSet.getString("area") + " for WO "
					+ resultSet.getString("WO_NO") + ", MQ " + resultSet.getString("MQ_ID") + " Created on  "
					+ resultSet.getString("REQ_DATE") + " modified date " + resultSet.getString("MODIFIED_DATE"));
			return hydMQWorkorderVO;
		}
	}
}
