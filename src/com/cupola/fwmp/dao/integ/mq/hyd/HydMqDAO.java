package com.cupola.fwmp.dao.integ.mq.hyd;

import java.util.List;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;

public interface HydMqDAO 
{
	List<MQWorkorderVO> getAllOpenTicketForHYD();

	/**
	 * @author aditya List<MQWorkorderVO>
	 * @param lastDataFetchedTimeManualForHYD
	 * @return
	 */
	List<MQWorkorderVO> getAllOpenTicketForHYDSpecial();

	/**@author aditya
	 * List<MQWorkorderVO>
	 * @param branchName
	 * @return
	 */
	List<MQWorkorderVO> getAllOpenFrTicketForHYD(String branchName);

	/**@author aditya
	 * List<MQWorkorderVO>
	 * @param branchName
	 * @return
	 */
	List<MQWorkorderVO> getAllFrOpenWorkOrderPrivious15Minute(String branchName);
}
