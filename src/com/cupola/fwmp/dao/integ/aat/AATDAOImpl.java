package com.cupola.fwmp.dao.integ.aat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.FWMPConstant.AAT;
import com.cupola.fwmp.vo.TabletInfoVo;
import com.cupola.fwmp.vo.aat.AATEligibilityInput;
import com.cupola.fwmp.vo.aat.AATEligibilityOutput;

public class AATDAOImpl implements AATDAO
{

	private static Logger log = LogManager
			.getLogger(AATDAOImpl.class.getName());

	final static private String GET_USER_BY_MQID = "select tab.registrationId,t.currentAssignedTo,c.mqId,t.id from  Ticket t inner join "
			+ " Customer c on c.id = t.customerId  left join Tablet tab on tab.currentUser=t.currentAssignedTo "
			+ " where t.customerId=c.id and  c.mqId=?";

	JdbcTemplate jdbcTemplate;
	HibernateTemplate hibernateTemplate;

	public JdbcTemplate getJdbcTemplate()
	{
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	public HibernateTemplate getHibernateTemplate()
	{
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public List<TabletInfoVo> getAATDetailsforCustomer(String mqId)
	{

		log.info("get registration and ticketId  as mqId:" + mqId);

		List<TabletInfoVo> tabletInfoVo = null;
		if(mqId == null || mqId.isEmpty())
			return tabletInfoVo;

		try
		{
			tabletInfoVo = (List<TabletInfoVo>) jdbcTemplate
					.query(GET_USER_BY_MQID, new Object[] {
							mqId }, new AATMapper());
		} catch (Exception e)
		{
			log.error("exception while getAATDetailsforCustomer ::" + e);
			return null;
		}
		log.debug("Exit From get registration and ticketId  as mqId:" + mqId);

		return tabletInfoVo;

	}

	@Override
	public AATEligibilityOutput isAllFWMPActivityDone(
			AATEligibilityInput aatEligibilityInput)
	{
//		String query = "select distinct c.firstName as customerName,c.mqId as mqId,t.prospectNo as prospectNo,wo.workOrderNumber as workOrder,"
//				+ "ws.workStageName as connectionType,tal.activityId as activity from Customer c join Ticket t on c.id=t.customerId Join "
//				+ "WorkOrder wo on wo.ticketId=t.id Join TicketActivityLog tal on tal.ticketId=t.id Join WorkStage ws on "
//				+ "ws.id=wo.currentWorkStage where c.mqId=?";
		
		String query = "select distinct c.firstName as customerName,c.mqId as mqId,t.prospectNo as prospectNo,"
				+ "wo.workOrderNumber as workOrder, ws.workStageName as connectionType,tal.activityId as activity "
				+ "from Customer c join Ticket t on c.id=t.customerId Join WorkOrder wo on wo.ticketId=t.id "
				+ "left outer Join TicketActivityLog tal on tal.ticketId=t.id Left outer Join WorkStage ws on ws.id=wo.currentWorkStage "
				+ "where c.mqId=?";

		// + " and c.cityId=?";

		log.info("Checking eligibility of mqId " + aatEligibilityInput.getMqId()
				+ " for AAT, city " + aatEligibilityInput.getCityCode()
				+ " and query is " + query);

		AATEligibilityOutput aatEligibilityOutput = new AATEligibilityOutput();

		List<AATEligibilityVO> activities = jdbcTemplate
				.query(query, new Object[] { aatEligibilityInput.getMqId()
				/*
				 * ,aatEligibilityInput .getCityCode()
				 */ }, new AATEligibilityMapper());

		if (activities != null && !activities.isEmpty())
		{
			log.info("Total row fatched for mqId " + aatEligibilityInput.getMqId() + " is " + activities.size());
			
			if (activities.get(0).getCurrentWorkStage() != null && activities.get(0).getCurrentWorkStage()
					.equalsIgnoreCase(aatEligibilityInput.getConnectionType()))
			{
				Set<Long> activityIds = new LinkedHashSet<Long>();

				for (Iterator<AATEligibilityVO> iterator = activities
						.iterator(); iterator.hasNext();)
				{
					AATEligibilityVO aatEligibilityVO = (AATEligibilityVO) iterator
							.next();

					activityIds.add(aatEligibilityVO.getActivityId());

				}

				if (activityIds.containsAll(AAT.FIBER_ACTIVITIES))
				{
					aatEligibilityOutput.setCode(AAT.AAT_ELIGIBILE_ID);
					aatEligibilityOutput.setMessage(AAT.ALL_ACTIVITY_COMPLETED);
					aatEligibilityOutput.setStatus(AAT.ELIGIBILE);

				} else if (activityIds.containsAll(AAT.COPPER_ACTIVITIES))
				{
					aatEligibilityOutput.setCode(AAT.AAT_ELIGIBILE_ID);
					aatEligibilityOutput.setMessage(AAT.ALL_ACTIVITY_COMPLETED);
					aatEligibilityOutput.setStatus(AAT.ELIGIBILE);

				} else
				{
					aatEligibilityOutput.setCode(AAT.AAT_NON_ELIGIBILE_ID);
					aatEligibilityOutput
							.setMessage(AAT.ALL_ACTIVITY_NOT_COMPLETED);
					aatEligibilityOutput.setStatus(AAT.NON_ELIGIBILE);
				}

			} else
			{
				aatEligibilityOutput.setCode(AAT.CONNECTION_TYPE_MISMATCH_ID);
				aatEligibilityOutput
						.setMessage(AAT.CONNECTION_TYPE_MISMATCH_MESSAGE);
				aatEligibilityOutput.setStatus(AAT.NON_ELIGIBILE);

			}

		} else
		{
//			aatEligibilityOutput.setCode(AAT.AAT_NON_ELIGIBILE_ID);
//			aatEligibilityOutput.setMessage(AAT.ALL_ACTIVITY_NOT_COMPLETED);
//			aatEligibilityOutput.setStatus(AAT.NON_ELIGIBILE);
			
			aatEligibilityOutput.setCode(AAT.AAT_ELIGIBILE_ID);
			aatEligibilityOutput.setMessage(AAT.ALL_ACTIVITY_COMPLETED);
			aatEligibilityOutput.setStatus(AAT.ELIGIBILE);
		}

		return aatEligibilityOutput;

	}

	/*
	 * @Override public List<CustomerClosuresVo> getDemo() { return
	 * (List<CustomerClosuresVo>) hibernateTemplate .find(
	 * "from CustomerClosuresVo"); }
	 */
}

class AATMapper implements RowMapper<TabletInfoVo>
{
	@Override
	public TabletInfoVo mapRow(ResultSet rs, int arg1) throws SQLException
	{

		TabletInfoVo info = new TabletInfoVo();

		info.setTicketId(rs.getLong("Id"));
		info.setRegistrationId(rs.getString("registrationId"));
		info.setUserId(rs.getLong("currentAssignedTo"));

		return info;
	}
}

class AATEligibilityMapper implements RowMapper<AATEligibilityVO>
{

	@Override
	public AATEligibilityVO mapRow(ResultSet rs, int arg1) throws SQLException
	{
//		"select c.firstName as customerName,c.mqId as mqId,t.prospectNo as prospectNo,wo.workOrderNumber as workOrder,"
//				+ "ws.workStageName as connectionType,tal.activityId as activity 
		AATEligibilityVO  aatEligibilityVO = new AATEligibilityVO();
		aatEligibilityVO.setActivityId(rs.getLong("activity"));
		aatEligibilityVO.setCurrentWorkStage(rs.getString("connectionType"));
		aatEligibilityVO.setFirstName(rs.getString("customerName"));
		aatEligibilityVO.setMqId(rs.getString("mqId"));
		aatEligibilityVO.setProspectNo(rs.getString("prospectNo"));
		aatEligibilityVO.setWorkOrderNumber(rs.getString("workOrder"));
		return aatEligibilityVO;
	}

}
