/**
 * 
 */
package com.cupola.fwmp.dao.integ.aat;

/**
 * @author aditya
 *
 */
public class AATEligibilityVO
{
	private String firstName;
	private String mqId;
	private String prospectNo;
	private String workOrderNumber;
	private String currentWorkStage;
	private Long activityId;

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMqId()
	{
		return mqId;
	}

	public void setMqId(String mqId)
	{
		this.mqId = mqId;
	}

	public String getProspectNo()
	{
		return prospectNo;
	}

	public void setProspectNo(String prospectNo)
	{
		this.prospectNo = prospectNo;
	}

	public String getWorkOrderNumber()
	{
		return workOrderNumber;
	}

	public void setWorkOrderNumber(String workOrderNumber)
	{
		this.workOrderNumber = workOrderNumber;
	}

	public String getCurrentWorkStage()
	{
		return currentWorkStage;
	}

	public void setCurrentWorkStage(String currentWorkStage)
	{
		this.currentWorkStage = currentWorkStage;
	}

	public Long getActivityId()
	{
		return activityId;
	}

	public void setActivityId(Long activityId)
	{
		this.activityId = activityId;
	}

	@Override
	public String toString()
	{
		return "AATEligibilityVO [firstName=" + firstName + ", mqId=" + mqId
				+ ", prospectNo=" + prospectNo + ", workOrderNumber="
				+ workOrderNumber + ", currentWorkStage=" + currentWorkStage
				+ ", activityId=" + activityId + "]";
	}

}
