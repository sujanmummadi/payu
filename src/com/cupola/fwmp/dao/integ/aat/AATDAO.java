package com.cupola.fwmp.dao.integ.aat;

import java.util.List;

import com.cupola.fwmp.vo.TabletInfoVo;
import com.cupola.fwmp.vo.aat.AATEligibilityInput;
import com.cupola.fwmp.vo.aat.AATEligibilityOutput;

public interface AATDAO
{

	public List<TabletInfoVo> getAATDetailsforCustomer(String mqId);

	AATEligibilityOutput isAllFWMPActivityDone(AATEligibilityInput aatEligibilityInput);

	//List<CustomerClosuresVo> getDemo();

}
