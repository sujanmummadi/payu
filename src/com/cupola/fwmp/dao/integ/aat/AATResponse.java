package com.cupola.fwmp.dao.integ.aat;

public class AATResponse {

	private String ipAddress;
	private Integer portNo;
	private String mqId;

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getPortNo() {
		return portNo;
	}

	public void setPortNo(Integer portNo) {
		this.portNo = portNo;
	}

	public String getMqId() {
		return mqId;
	}

	public void setMqId(String mqId) {
		this.mqId = mqId;
	}

	@Override
	public String toString() {
		return "AATResponse [ipAddress=" + ipAddress + ", portNo=" + portNo
				+ ", mqId=" + mqId + "]";
	}

	


}
