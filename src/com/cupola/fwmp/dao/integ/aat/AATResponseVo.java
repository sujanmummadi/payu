package com.cupola.fwmp.dao.integ.aat;

public class AATResponseVo
{

	private String deviceIp;
	private String portNumber;
	private String ticketId;

	public String getDeviceIp()
	{
		return deviceIp;
	}

	public void setDeviceIp(String deviceIp)
	{
		this.deviceIp = deviceIp;
	}

	public String getPortNumber()
	{
		return portNumber;
	}

	public void setPortNumber(String portNumber)
	{
		this.portNumber = portNumber;
	}

	public String getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(String ticketId)
	{
		this.ticketId = ticketId;
	}

	@Override
	public String toString()
	{
		return "AATResponseVo [deviceIp=" + deviceIp + ", portNumber="
				+ portNumber + ", ticketId=" + ticketId + "]";
	}

}
