/**
 * 
 */
package com.cupola.fwmp.dao.integ.gis;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

/**
 * @author aditya
 */
public interface GisDAO
{
	Long insertGISInfo(GISPojo gisPojo);

	GISPojo getGisInfoByTicketId(Long ticketId);

	List<Gis> getGisInfo4Tickets(List<Long> ticketIds);

	List<GISPojo> getAllGisInfoByTicketId(Long ticketId);

	Map<Long, GISPojo> getGisTypeAndDetailsByTicketId(Long ticketId);

	int updateSalesFeasibilityWithNeFeasibility(
			UpdateProspectVO updateProspectVO);

	Map<Long, Long> insertGISInfoInBulk(List<MQWorkorderVO> mqWorkorderVOList,
			Map<String, Long> prospectNoMqIdMap);

}
