/**
 * 
 */
package com.cupola.fwmp.dao.integ.gis;

import java.util.Date;
import java.util.Map;

/**
 * @author aditya
 * 
 */
public class GISPojo
{
	private long id;
	private Long ticketNo;
	private String city;
	private String branch;
	private String branchName;
	private String area;
	private String transactionNo;
	private String errorNo;
	private String message;
	private String clusterName;
	private String connectionType;
	private String fxName;
	private String fxIpAddress;
	private String fxMacAddress;
	private String fxPorts;
	private String cxName;
	private String cxIpAddress;
	private String cxMacAddress;
	private String cxPorts;
	private String longitude;
	private String lattitude;
	private Integer workstageType;
	private Long feasibilityType;
	private String remarks;
	private Long addedBy;
	private Date addedOn;
	private Long modifiedBy;
	private Date modifiedOn;
	private Integer status;
	private boolean fiberToCopper;
	private boolean feasibilityTicket;
	private boolean permissionFeasibilityStatus;
	private boolean feasibilityRejectedDuringDeployment;
	private boolean workOrderGenerated;
	private String neComment;
	
	private Map<String, String> subActivityDetail;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Long getTicketNo()
	{
		return ticketNo;
	}

	public void setTicketNo(Long ticketNo)
	{
		this.ticketNo = ticketNo;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getBranch()
	{
		return branch;
	}

	public void setBranch(String branch)
	{
		this.branch = branch;
	}

	public String getArea()
	{
		return area;
	}

	public void setArea(String area)
	{
		this.area = area;
	}

	public String getTransactionNo()
	{
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo)
	{
		this.transactionNo = transactionNo;
	}

	public String getErrorNo()
	{
		return errorNo;
	}

	public void setErrorNo(String errorNo)
	{
		this.errorNo = errorNo;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getClusterName()
	{
		return clusterName;
	}

	public void setClusterName(String clusterName)
	{
		this.clusterName = clusterName;
	}

	public String getConnectionType()
	{
		return connectionType;
	}

	public void setConnectionType(String connectionType)
	{
		this.connectionType = connectionType;
	}

	public String getFxName()
	{
		return fxName;
	}

	public void setFxName(String fxName)
	{
		this.fxName = fxName;
	}

	public String getFxIpAddress()
	{
		return fxIpAddress;
	}

	public void setFxIpAddress(String fxIpAddress)
	{
		this.fxIpAddress = fxIpAddress;
	}

	public String getFxMacAddress()
	{
		return fxMacAddress;
	}

	public void setFxMacAddress(String fxMacAddress)
	{
		this.fxMacAddress = fxMacAddress;
	}

	public String getFxPorts()
	{
		return fxPorts;
	}

	public void setFxPorts(String fxPorts)
	{
		this.fxPorts = fxPorts;
	}

	public String getCxName()
	{
		return cxName;
	}

	public void setCxName(String cxName)
	{
		this.cxName = cxName;
	}

	public String getCxIpAddress()
	{
		return cxIpAddress;
	}

	public void setCxIpAddress(String cxIpAddress)
	{
		this.cxIpAddress = cxIpAddress;
	}

	public String getCxMacAddress()
	{
		return cxMacAddress;
	}

	public void setCxMacAddress(String cxMacAddress)
	{
		this.cxMacAddress = cxMacAddress;
	}

	public String getCxPorts()
	{
		return cxPorts;
	}

	public void setCxPorts(String cxPorts)
	{
		this.cxPorts = cxPorts;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getLattitude()
	{
		return lattitude;
	}

	public void setLattitude(String lattitude)
	{
		this.lattitude = lattitude;
	}

	public Integer getWorkstageType()
	{
		return workstageType;
	}

	public void setWorkstageType(Integer workstageType)
	{
		this.workstageType = workstageType;
	}

	public Long getFeasibilityType()
	{
		return feasibilityType;
	}

	public void setFeasibilityType(Long feasibilityType)
	{
		this.feasibilityType = feasibilityType;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public Long getAddedBy()
	{
		return addedBy;
	}

	public void setAddedBy(Long addedBy)
	{
		this.addedBy = addedBy;
	}

	public Date getAddedOn()
	{
		return addedOn;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public boolean isFiberToCopper()
	{
		return fiberToCopper;
	}

	public void setFiberToCopper(boolean fiberToCopper)
	{
		this.fiberToCopper = fiberToCopper;
	}

	public boolean isFeasibilityTicket()
	{
		return feasibilityTicket;
	}

	public void setFeasibilityTicket(boolean feasibilityTicket)
	{
		this.feasibilityTicket = feasibilityTicket;
	}

	public Map<String, String> getSubActivityDetail()
	{
		return subActivityDetail;
	}

	public void setSubActivityDetail(Map<String, String> subActivityDetail)
	{
		this.subActivityDetail = subActivityDetail;
	}
	
	public boolean isPermissionFeasibilityStatus()
	{
		return permissionFeasibilityStatus;
	}

	public void setPermissionFeasibilityStatus(boolean permissionFeasibilityStatus)
	{
		this.permissionFeasibilityStatus = permissionFeasibilityStatus;
	}

	public boolean isFeasibilityRejectedDuringDeployment()
	{
		return feasibilityRejectedDuringDeployment;
	}

	public void setFeasibilityRejectedDuringDeployment(
			boolean feasibilityRejectedDuringDeployment)
	{
		this.feasibilityRejectedDuringDeployment = feasibilityRejectedDuringDeployment;
	}

	public String getNeComment()
	{
		return neComment;
	}

	public void setNeComment(String neComment)
	{
		this.neComment = neComment;
	}

	@Override
	public String toString()
	{
		return "GISPojo [id=" + id + ", ticketNo=" + ticketNo + ", city=" + city
				+ ", branch=" + branch + ", area=" + area + ", transactionNo="
				+ transactionNo + ", errorNo=" + errorNo + ", message="
				+ message + ", clusterName=" + clusterName + ", connectionType="
				+ connectionType + ", fxName=" + fxName + ", fxIpAddress="
				+ fxIpAddress + ", fxMacAddress=" + fxMacAddress + ", fxPorts="
				+ fxPorts + ", cxName=" + cxName + ", cxIpAddress="
				+ cxIpAddress + ", cxMacAddress=" + cxMacAddress + ", cxPorts="
				+ cxPorts + ", longitude=" + longitude + ", lattitude="
				+ lattitude + ", workstageType=" + workstageType
				+ ", feasibilityType=" + feasibilityType + ", remarks="
				+ remarks + ", addedBy=" + addedBy + ", addedOn=" + addedOn
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn
				+ ", status=" + status + ", fiberToCopper=" + fiberToCopper
				+ ", feasibilityTicket=" + feasibilityTicket
				+ ", permissionFeasibilityStatus=" + permissionFeasibilityStatus
				+ ", feasibilityRejectedDuringDeployment="
				+ feasibilityRejectedDuringDeployment + ", neComment="
				+ neComment + ", subActivityDetail=" + subActivityDetail + "]";
	}

	public boolean isWorkOrderGenerated()
	{
		return workOrderGenerated;
	}

	public void setWorkOrderGenerated(boolean workOrderGenerated)
	{
		this.workOrderGenerated = workOrderGenerated;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	
}
