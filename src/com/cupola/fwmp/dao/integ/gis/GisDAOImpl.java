/**
 * 
 */
package com.cupola.fwmp.dao.integ.gis;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

/**
 * @author aditya
 * 
 */
@Transactional
public class GisDAOImpl implements GisDAO
{

	private static Logger log = LogManager
			.getLogger(GisDAOImpl.class.getName());

	// private static AppUser userDetails = (AppUser)
	// SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	//
	// private static Long userId = AuthUtils.getCurrentUserId();
	//
	// private static String loginName = AuthUtils.getCurrentUserLoginId();
	//
	// private static int roleid = AuthUtils.getCurrentUserRole();
	//

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	HibernateTemplate hibernateTemplate;

	private static String GET_RECENT_GIS_DETAILS_FROM_DB_FOR_A_TICKET = "select * from Gis where TicketNo=:ticketId and feasibilityType !=1 order by addedOn Desc limit 1";

	private static String GET_ALL_GIS_DETAILS_FROM_DB_FOR_A_TICKET = "select * from Gis where TicketNo=:ticketId order by addedOn";

	private static String UPDATE_SALES_FEASIBILITY = "UPDATE Gis SET connectionType=?, fxName=?, fxIpAddress=?, fxMacAddress=?, fxPorts=?, cxName=?, cxIpAddress=?, cxMacAddress=?, cxPorts=? WHERE TicketNo=? and feasibilityType=?";

	@Override
	public Long insertGISInfo(GISPojo gisPojo)
	{
		if(gisPojo == null)
			return 0L;
		
		try
		{
			log.debug("GIS info to add against prospect are " + gisPojo);

			Gis gis = new Gis();

			BeanUtils.copyProperties(putCommonValues(gisPojo), gis);

			gis.setId(StrictMicroSecondTimeBasedGuid.newGuid());

			log.debug("After putting common values GIS info to add against prospect are "
					+ gisPojo + "vales in GIS entities are " + gis);

			Long saveId = (Long) hibernateTemplate.save(gis);

			log.debug("GIS info to added against prospect are " + gisPojo
					+ " with key " + saveId);

			if (saveId >= 0)
				return saveId;

		} catch (DataAccessException e)
		{
			log.error("Error while inserting data in GIS info to add against prospect "
					+ gisPojo.getTicketNo() + ". Error message "
					+ e.getMessage());

			e.printStackTrace();

		}
		return 0L;
	}

	@Override
	public Map<Long, Long> insertGISInfoInBulk(
			List<MQWorkorderVO> mqWorkorderVOList,
			Map<String, Long> prospectNoMqIdMap)
	{

		Map<Long, Long> ticketGisMap = new ConcurrentHashMap<Long, Long>();
		try {
			log.info("Adding insertGISInfoInBulk " + mqWorkorderVOList.size());
			MQWorkorderVO mqWorkorderVO = null;
			GISPojo gisPojo = null;
			Gis gis = null;


		for (Iterator<MQWorkorderVO> iterator = mqWorkorderVOList
				.iterator(); iterator.hasNext();)
		{
			mqWorkorderVO = null;
			mqWorkorderVO = (MQWorkorderVO) iterator.next();
				
			try
			{

				if (prospectNoMqIdMap.containsKey(mqWorkorderVO.getProspectNumber()))
				{
					Long ticketId = prospectNoMqIdMap.get(mqWorkorderVO.getProspectNumber());

					gisPojo = null;
					gisPojo = xformToGISPojo(mqWorkorderVO, ticketId);

					try {
						log.debug("GIS info to add against prospect are " + gisPojo);

						gis = new Gis();

						log.debug("After putting common values GIS info to add against prospect are " + gisPojo
								+ "vales in GIS entities are " + gis);
						BeanUtils.copyProperties(putCommonValues(gisPojo), gis);

						gis.setId(StrictMicroSecondTimeBasedGuid.newGuid());

						log.debug("After putting common values GIS info to add against prospect are " + gisPojo
								+ "vales in GIS entities are " + gis);
						
						Long saveId = (Long) hibernateTemplate.save(gis);

						log.debug("GIS info to added against prospect are " + gisPojo + " with key " + saveId);


						ticketGisMap.put(saveId, ticketId);

					} catch (DataAccessException e) {
						log.error("Error while inserting data in GIS info to add against prospect "
							+ gisPojo.getTicketNo() + ". Error message " + e.getMessage());
						e.printStackTrace();
						continue;
					}
				}
			} catch (Exception e) 
			{
				log.error("Error while adding GIS data for workorder number " + mqWorkorderVO.getWorkOrderNumber() + " "
					+ e.getMessage());
				e.printStackTrace();
				continue;
			}
		}
		} catch (Exception e) 
		{
			log.error("Error while adding GIS data  "+ e.getMessage());
			e.printStackTrace();
		}
		return ticketGisMap;

	
	}

	public GISPojo xformToGISPojo(MQWorkorderVO mqWorkorderVO, long ticketId)
	{
		GISPojo gisPojo = new GISPojo();

		if (mqWorkorderVO == null)
			return gisPojo;
		
		log.debug(" xformToGISPojo ....." +mqWorkorderVO.getAreaId() + " "  +ticketId);
		
		gisPojo.setId(StrictMicroSecondTimeBasedGuid.newGuid());

		gisPojo.setAddedBy(FWMPConstant.SYSTEM_ENGINE);

		gisPojo.setAddedOn(new Date());

		if (mqWorkorderVO.getAreaId() != null)
			gisPojo.setArea(mqWorkorderVO.getAreaId());

		if (mqWorkorderVO.getBranchId() != null)
			gisPojo.setBranch(mqWorkorderVO.getBranchId());

		if (mqWorkorderVO.getCityId() != null)
			gisPojo.setCity(mqWorkorderVO.getCityId());

		gisPojo.setCxIpAddress(mqWorkorderVO.getCxIp());

		gisPojo.setFxName(mqWorkorderVO.getFxName());

		gisPojo.setStatus(1);

		gisPojo.setTicketNo(ticketId);

		gisPojo.setFeasibilityType(FeasibilityType.GIS_CLASSIFICATION);

		return gisPojo;
	}

	@Override
	public GISPojo getGisInfoByTicketId(Long ticketId)
	{
		log.debug(" geting Gis InfoByTicketId....." +ticketId );

		GISPojo gisPojo = null;
		if (ticketId == null || ticketId <= 0)
			return gisPojo;
		Session session = hibernateTemplate.getSessionFactory().openSession();

		try
		{
			List<Gis> gislist = session
					.createSQLQuery(GET_RECENT_GIS_DETAILS_FROM_DB_FOR_A_TICKET)
					.addEntity(Gis.class).setParameter("ticketId", ticketId)
					.list();

			if (gislist != null && !gislist.isEmpty())
			{
				gisPojo = new GISPojo();
				Gis gis = gislist.get(0);
				BeanUtils.copyProperties(gis, gisPojo);
			}
		} catch (BeansException e)
		{
			log.error("Unable to parse beans for ticket " + ticketId
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e)
		{
			log.error("Error while getting getGisInfoByTicketId for ticket "
					+ ticketId + e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null )
				session.close();
		}
		log.debug(" Exit From geting Gis InfoByTicketId....." +ticketId );
		return gisPojo;
	}

	@Override
	public List<GISPojo> getAllGisInfoByTicketId(Long ticketId)
	{
		List<GISPojo> gisPojoList = new ArrayList<GISPojo>();

		if (ticketId == null || ticketId <= 0)
			return gisPojoList;
		
		log.debug(" geting AllGisInfo By TicketId  ...." +ticketId );
		
		Session session = hibernateTemplate.getSessionFactory().openSession();

		try
		{
			List<Gis> gislist = session
					.createSQLQuery(GET_RECENT_GIS_DETAILS_FROM_DB_FOR_A_TICKET)
					.addEntity(Gis.class).setParameter("ticketId", ticketId)
					.list();

			if (gislist != null && !gislist.isEmpty())
			{
				for (Gis gis : gislist)
				{
					GISPojo gisPojo = new GISPojo();
					BeanUtils.copyProperties(gis, gisPojo);
					gisPojoList.add(gisPojo);
				}

			}
		} catch (BeansException e)
		{
			log.error("Unable to parse beans  for ticket getAllGisInfoByTicketId "
					+ ticketId + e.getMessage());
			e.printStackTrace();
		} catch (Exception e)
		{
			log.error("Error while getting getAllGisInfoByTicketId for ticket "
					+ ticketId + e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}

		return gisPojoList;

	}

	@Override
	public Map<Long, GISPojo> getGisTypeAndDetailsByTicketId(Long ticketId)
	{
		Map<Long, GISPojo> feasibilityTypeDetails = new LinkedHashMap<>();

		if (ticketId == null || ticketId <= 0)
			return feasibilityTypeDetails;

		log.debug(" geting Gis Type And Details By TicketId...." +ticketId );
		
		Session session = hibernateTemplate.getSessionFactory().openSession();

		try
		{
			List<Gis> gislist = session
					.createSQLQuery(GET_ALL_GIS_DETAILS_FROM_DB_FOR_A_TICKET)
					.addEntity(Gis.class).setParameter("ticketId", ticketId)
					.list();

			if (gislist != null && !gislist.isEmpty())
			{
				for (Gis gis : gislist)
				{
					GISPojo gisPojo = new GISPojo();
					BeanUtils.copyProperties(gis, gisPojo);
					feasibilityTypeDetails
							.put(gisPojo.getFeasibilityType(), gisPojo);
				}

			}

			log.debug("ticketId " + ticketId
					+ " ########## getGisTypeAndDetailsByTicketId "
					+ feasibilityTypeDetails);
		} catch (BeansException e)
		{
			log.error("Unable to parse beans getGisTypeAndDetailsByTicketId for ticket "
					+ ticketId + e.getMessage());
			e.printStackTrace();
		} catch (Exception e)
		{
			log.error("Error while getting getGisTypeAndDetailsByTicketId for ticket "
					+ ticketId + e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}

		log.debug(" Exit From geting Gis Type And Details By TicketId...." +ticketId );
		
		return feasibilityTypeDetails;

	}

	private GISPojo putCommonValues(GISPojo gisPojo)
	{
	   if(gisPojo == null)
	   return null;
		log.debug(" put GISPojo CommonValues ....." +gisPojo.getCxMacAddress() );

		gisPojo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		gisPojo.setAddedOn(new Date());
		gisPojo.setAddedBy(AuthUtils.getCurrentUserId());

		return gisPojo;
	}

	@Override
	public List<Gis> getGisInfo4Tickets(List<Long> ticketIds)
	{
		if (ticketIds == null ||ticketIds.isEmpty())
			return new ArrayList<Gis>();
		
		log.debug(" geting GisInfo4Tickets  ...." + ticketIds.size() );
		
		Session session = null;
		try
		{
			session = hibernateTemplate.getSessionFactory().openSession();
			if (ticketIds.isEmpty())
				return new ArrayList<Gis>();

			List<Gis> gisList = session
					.createSQLQuery("select g.* from Gis g where g.TicketNo in (:ticketIds) and feasibilityType=:feasibilityType")
					.addEntity(Gis.class)
					.setParameterList("ticketIds", ticketIds)
					.setParameter("feasibilityType", FeasibilityType.GIS_SALES)
					.list();

			if (gisList != null && !gisList.isEmpty())
				return gisList;
			else
				return new ArrayList<Gis>();
		} catch (Exception e)
		{
			log.error("Error While getting gis Info : " + e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		
		log.debug("Exit From geting GisInfo4Tickets  ...." + ticketIds.size() );
		
		return new ArrayList<Gis>();
	}

	@Override
	public int updateSalesFeasibilityWithNeFeasibility(
			UpdateProspectVO updateProspectVO)
	{
		try
		{
			return jdbcTemplate.update(UPDATE_SALES_FEASIBILITY, new Object[] {
					updateProspectVO.getConnectionType(),
					updateProspectVO.getFxName(),
					updateProspectVO.getFxIpAddress(),
					updateProspectVO.getFxMacAddress(),
					updateProspectVO.getFxPorts() != null ? StringUtils.join(updateProspectVO.getFxPorts(), ",") : null,
					updateProspectVO.getCxName(),
					updateProspectVO.getCxIpAddress(),
					updateProspectVO.getCxMacAddress(),
					updateProspectVO.getCxPorts() != null ? StringUtils.join(updateProspectVO.getCxPorts(), ",") : null,					
					updateProspectVO.getTicketId(),
					FeasibilityType.GIS_SALES });
		} catch (Exception e)
		{
			log.error("Error while  updateSalesFeasibilityWithNeFeasibility for ticket "
					+ updateProspectVO.getTicketId());
			e.printStackTrace();
		}
		return 0;
	}

}
