package com.cupola.fwmp.dao.integ.customerAppNotification;

import java.util.List;

public interface CustomerAppDAO {
	
	 List<CustomerActivationVO> customerActivation(CustomerActivationVO customerActivation); 

}
