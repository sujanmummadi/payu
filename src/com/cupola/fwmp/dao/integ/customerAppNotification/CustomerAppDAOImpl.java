package com.cupola.fwmp.dao.integ.customerAppNotification;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;


public class CustomerAppDAOImpl implements CustomerAppDAO{
	
	
	static final Logger log = Logger.getLogger(CustomerAppDAOImpl.class);
	
	
	private HibernateTemplate hibernateTemplate;

	 private JdbcTemplate jdbcTemplate;


	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	private static String GET_REGISTRATION_ID_ON_MQID = "select registrationId from Tablet where currentUser in (select currentAssignedTo from Ticket where id in (select t.id from Ticket t  where customerId= (select id from Customer where mqId=?)))";
    private static String GET_TICKET_ID="select t.id from Ticket t  where customerId in (select id from Customer where mqId=?)";
	
    
    
	@Override
	public List<CustomerActivationVO> customerActivation(CustomerActivationVO customerActivation)
	{
		
		List<CustomerActivationVO> customeractivationvolist=new ArrayList<CustomerActivationVO>();
         
		if(customerActivation == null)
			return customeractivationvolist;
		log.debug(" inside customerActivation "  +customerActivation.getMqId());
		CustomerActivationVO customerActivationvo ;
		
		List<String> regisId = null;
		List<Long> ticketId = null;

		try
		{
			regisId = jdbcTemplate
					.queryForList(GET_REGISTRATION_ID_ON_MQID, new Object[] { customerActivation
							.getMqId() }, String.class);

			ticketId = jdbcTemplate
					.queryForList(GET_TICKET_ID, new Object[] { customerActivation
							.getMqId() }, Long.class);

		} catch (Exception e)
		{
			log.error("Error While getting regisId or ticketId on customerActivation : "+e.getMessage());
			e.printStackTrace();

		}
		if (regisId != null && ticketId != null)
		{

			for (int i = 0; i < regisId.size(); i++)
			{
				customerActivationvo = new CustomerActivationVO(); 
				
				if ((regisId.get(i) != null && !regisId.get(i).isEmpty()))
				{

					customerActivationvo.setRegistrationId(regisId.get(i));
					customerActivationvo.setTicketId(ticketId.get(0));
					
					
					customeractivationvolist.add(customerActivationvo);
					
					log.debug("adding in  list :::"+customeractivationvolist);
				}

				
			}
			log.debug("retruning list :::"+customeractivationvolist.size());
			
			return customeractivationvolist;

		}else{
			return null;
		}

	}

}
