package com.cupola.fwmp.dao.integ.customerAppNotification;

public class CustomerActivationVO {

	
	private long mqId;
	
	private String status;
	
	private String registrationId;
	
	private Long ticketId;
	
	
	
	public Long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}
	public String getRegistrationId() {
		return registrationId;
	}
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}
	public long getMqId() {
		return mqId;
	}
	public void setMqId(long mqId) {
		this.mqId = mqId;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	@Override
	public String toString()
	{
		return "CustomerActivationVO [mqId=" + mqId + ", status=" + status
				+ ", registrationId=" + registrationId + ", ticketId="
				+ ticketId + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}
