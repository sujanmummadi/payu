package com.cupola.fwmp.dao.integ.app;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.tariff.TariffDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAOImpl;
import com.cupola.fwmp.persistance.entities.Payment;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.service.integ.app.CustomerAppProspectHandler;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.integ.app.ProspectVO;
import com.cupola.fwmp.vo.sales.TariffPlanVo;

/**
 * @author aditya
 */

public class AppPropspectDAOImpl implements AppPropspectDAO
{

	private static Logger log = LogManager
			.getLogger(AppPropspectDAOImpl.class.getName());

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CustomerAppProspectHandler appProspectHandler;

	@Autowired
	TariffDAO tariffDAO;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	TicketActivityLogDAOImpl ticketActivityLogDAOImpl;
	@Autowired
	TicketDAO tickectDao;

	@Override
	public void addPropspectInDB(ProspectVO prospectVo)
	{
		if(prospectVo == null)
			return;
		
		log.debug(" adding Propspect InDB "   + prospectVo.getCityCode());
		
		prospectVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		prospectVo.setAddedOn(new Date());

		log.info("Adding prospect to mongo DB");

		mongoTemplate.insert(prospectVo);

		appProspectHandler.add2CreateProspectByAppQueue(prospectVo.getId());

		log.info("Added prospect to mongoDb " + prospectVo);
	}

	@Override
	public ProspectVO getPropspectById(Long id)
	{
		log.info("In app prospect DAO getting data for the id " + id);

		Query query = new Query(Criteria.where("_id").is(id));

		ProspectVO prospectVO = mongoTemplate.findOne(query, ProspectVO.class);

		log.info("Prospect vo ################# " + prospectVO);

		return prospectVO;

	}

	@Override
	public void updatePropspectInDB(ProspectVO prospect)
	{
		log.info("Update Propspect In DB " + prospect.getMobileNumber());

		Query query = new Query();

		query.addCriteria(Criteria.where("prospectNumber")
				.is(prospect.getProspectNumber()));

		mongoTemplate.findAndRemove(query, ProspectVO.class);
		prospect.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		mongoTemplate.insert(prospect);

		appProspectHandler.add2UpdateProspectByAppQueue(prospect);

		log.info("Added prospect to mongoDb for update action" + prospect.getMobileNumber());

	}

	@Override
	@Transactional
	public int updateTariff(ProspectVO prospectVO)
	{
		if (prospectVO == null)
			return 0;

		List<TariffPlanVo> tariffPlan = tariffDAO
				.getTariffByPlanCode(prospectVO.getPlanCode());

		Long planId = (tariffPlan != null && !tariffPlan.isEmpty())
				? Long.valueOf(tariffPlan.get(0).getId()) : null;

		Long ticke = ticketDAO
				.getTicketIdByProspectNumber(prospectVO.getProspectNumber());

		Long ticketId = ticke != null ? ticke : null;
		
		if(ticketId == null)
			return 0;
		
		try
		{
			log.info("################# updateTariff from Customer App "
					+ prospectVO.getMobileNumber());
			/**
			 * setRouterFreeWithPlanModel This flag is required to specify is
			 * router required?
			 */

			if (prospectVO.getRouterFreeWithPlanModel() != null && prospectVO
					.getRouterFreeWithPlanModel().equalsIgnoreCase("Y"))
				prospectVO.setRouterFreeWithPlanModel(1 + "");
			else
			{
				if (prospectVO.getExternalRouterRequired() != null && prospectVO
						.getExternalRouterRequired().equalsIgnoreCase("Y"))
				{
					prospectVO.setExternalRouterRequired(1 + "");
					prospectVO.setRouterFreeWithPlanModel(1 + "");

				} else
					prospectVO.setExternalRouterRequired(0 + "");

				prospectVO.setRouterFreeWithPlanModel(0 + "");
			}

			log.info("########## test  @######## Tariff plan update DAO from Customer App "
					+ planId + " "
					+ Integer.valueOf(prospectVO.getRouterFreeWithPlanModel())
					+ " " + prospectVO.getRouterModel() + " "
					+ prospectVO.getTariffPlanType() + " "
					+ prospectVO.getExternalRouterRequired() + " "
					+ prospectVO.getRouterModel() + " "
					+ prospectVO.getExternalRouterAmount() + " " + ticketId
					+ " tariffPlan " + tariffPlan);

			int row = jdbcTemplate
					.update(UPDATE_TARIFF_DETAILS, new Object[] { planId,
							Integer.valueOf(prospectVO
									.getRouterFreeWithPlanModel()),
							prospectVO.getRouterModel(),
							prospectVO.getTariffPlanType(),
							prospectVO.getExternalRouterRequired(),
							prospectVO.getRouterModel(),
							prospectVO.getExternalRouterAmount(), ticketId });
			//
			log.info("Total updated row " + row);

			if (row > 0)
			{
				log.info("Ticket details during tariff update by customer App is "
						+ ticketId);

				TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();

				ticketActivityLogVo
						.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				ticketActivityLogVo.setSubActivityId(Activity.TARIFF_UPDATE);

				ticketActivityLogVo.setModifiedOn(new Date());
				ticketActivityLogVo.setAddedOn(new Date());
				ticketActivityLogVo.setTicketId(ticketId);
				ticketActivityLogVo.setActivityId(Activity.TARIFF_UPDATE);

				ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);

				ticketActivityLogDAOImpl
						.addTicketActivityLog(ticketActivityLogVo);
				tickectDao
						.updateTicketStatus(ticketId, TicketStatus.TARIFF_PLAN_UPDATED);
				
				Query query = new Query();

				query.addCriteria(Criteria.where("prospectNumber")
						.is(prospectVO.getProspectNumber()));

				mongoTemplate.findAndRemove(query, ProspectVO.class);


			}

			return row;

		} catch (Exception e)
		{
			log.error("Error while updating tariff plan for  " + ticketId
					+ " came from Customer App " + e.getMessage());
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	@Transactional
	public int updatePayment(ProspectVO prospectVO)
	{
		Long ticke = ticketDAO
				.getTicketIdByProspectNumber(prospectVO.getProspectNumber());

		Long ticketId = ticke != null ? ticke : null;
		
		if(ticketId == null)
			return 0;
		
		log.info("Updating payment for ticketID " + ticketId);

		try
		{
			Payment payment = new Payment();

			payment.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			payment.setPaidAmount(prospectVO.getAmountPaid());
			payment.setPaymentMode(prospectVO.getPaymentMode());
			payment.setReferenceNo(prospectVO.getReferenceNo());
			payment.setInstallationCharges(prospectVO.getInstallationCharges());
			payment.setPaidConnection(prospectVO.getPaidConnection());

			hibernateTemplate.save(payment);
			
			log.info("Payment is saved in db for via customer app " + ticketId);

			Ticket ticket = hibernateTemplate.get(Ticket.class, ticketId);

			if (ticket != null)
			{
				log.info("Updating customer app payment details for ticket "
						+ ticket.getProspectNo());
				ticket.setCafNumber(prospectVO.getCafNo());
				ticket.setPayment(payment);
				ticket.setReferenceNo(prospectVO.getReferenceNo());
				ticket.setPaymentStatus(TicketStatus.PAYMENT_RECEIVED);
				ticket.setStatus(TicketStatus.PAYMENT_RECEIVED);
				hibernateTemplate.update(ticket);
			}

			String prospectNo =  ticket !=null && ticket.getProspectNo() !=null ? ticket.getProspectNo() : null;
			
			log.info("updated payment details from Customer App for ticket "
					+ prospectNo);
			
			TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();

			ticketActivityLogVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			ticketActivityLogVo.setSubActivityId(Activity.PAYMENT_UPDATE);

			ticketActivityLogVo.setModifiedOn(new Date());
			ticketActivityLogVo.setAddedOn(new Date());
			ticketActivityLogVo.setTicketId(ticketId);
			ticketActivityLogVo.setActivityId(Activity.PAYMENT_UPDATE);

			ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);

			ticketActivityLogDAOImpl.addTicketActivityLog(ticketActivityLogVo);
			
			log.debug(" $$$$$$$$$$$$$$$$$ " + prospectNo);
			
			tickectDao
					.updateTicketStatus(ticketId, TicketStatus.PAYMENT_RECEIVED);
			
			Query query = new Query();

			query.addCriteria(Criteria.where("prospectNumber")
					.is(prospectVO.getProspectNumber()));

			mongoTemplate.findAndRemove(query, ProspectVO.class);

			return 1;

		} catch (Exception e)
		{
			log.error("Error while updating Payment info for ticket " + ticketId
					+ ". " + e.getMessage());

			e.printStackTrace();

			return 0;
		}
	}

	@Override
	@Transactional
	public int uploadDoucuments(ProspectVO prospectVO)
	{
		int result = 0;

		try
		{
			Long ticke = ticketDAO.getTicketIdByProspectNumber(prospectVO
					.getProspectNumber());
			
			Long ticketId = ticke != null ? ticke : null;

			if(ticketId == null)
				return 0;
			
			String docPath = prospectVO.getDocPath() != null
					? StringUtils.join(prospectVO.getDocPath(), ",") : "";

			result = ticketDAO.updateDocpath(ticketId, docPath,0,null,null);

			if (result > 0)
			{

				TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();

				ticketActivityLogVo
						.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				ticketActivityLogVo.setSubActivityId(Activity.DOCUMENT_UPDATE);

				ticketActivityLogVo.setModifiedOn(new Date());
				ticketActivityLogVo.setAddedOn(new Date());
				ticketActivityLogVo.setTicketId(ticketId);
				ticketActivityLogVo.setActivityId(Activity.DOCUMENT_UPDATE);

				ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);

				ticketActivityLogDAOImpl
						.addTicketActivityLog(ticketActivityLogVo);

				tickectDao
						.updateTicketStatus(ticketId, TicketStatus.POI_DOCUMENTS_UPLOADED);
				
				Query query = new Query();

				query.addCriteria(Criteria.where("prospectNumber")
						.is(prospectVO.getProspectNumber()));

				mongoTemplate.findAndRemove(query, ProspectVO.class);

			}
			log.info("Document Upload for prospect "
					+ prospectVO.getProspectNumber()
					+ " which came from Customer App");

		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error while Document Upload for prospect "
					+ prospectVO.getProspectNumber()
					+ " which came from Customer App" + e.getMessage());

		}

		return result;
	}

	@Override
	public Long getDefaultExecutiveMappingByBranchForApp(Long branchId)
	{
		String sqlQuery = "select  userId from  DefaultExecutiveMappingForApp where branchId="
				+ branchId + " limit 1";
		
		log.info(" getDefaultExecutiveMappingForApp " + sqlQuery);
		
		return jdbcTemplate.queryForLong(sqlQuery);
	}
	

}