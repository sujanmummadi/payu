package com.cupola.fwmp.dao.integ.app;

import com.cupola.fwmp.vo.integ.app.ProspectVO;

public interface AppPropspectDAO
{
	String UPDATE_TARIFF_DETAILS = "update Customer cu, Ticket t set cu.tariffId=?, cu.routerRequired=?, "
			+ "cu.routerDescription=?, cu.CustomerType=?, cu.externalRouterRequired=?, cu.externalRouterDescription=?, "
			+ "cu.externalRouterAmount=? where t.customerId=cu.id and t.id=? ";
	
	

	public void addPropspectInDB(ProspectVO prospectVo);

	public ProspectVO getPropspectById(Long id);

	public void updatePropspectInDB(ProspectVO prospectVO);

	int updateTariff(ProspectVO prospectVO);

	int updatePayment(ProspectVO prospectVO);

	int uploadDoucuments(ProspectVO prospectVO);
	
	Long getDefaultExecutiveMappingByBranchForApp(Long branchId);

}
