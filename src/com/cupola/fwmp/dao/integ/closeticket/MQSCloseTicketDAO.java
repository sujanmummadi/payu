package com.cupola.fwmp.dao.integ.closeticket;

import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;

public interface MQSCloseTicketDAO
{
	String closeTicket(TicketClosureDataVo closeTicketVo);

	String closeFrTicket(TicketClosureDataVo closeTicketVo);

	String modifyTicket(TicketModifyInMQVo vo);
}
