package com.cupola.fwmp.dao.integ.closeticket;

import java.net.InetAddress;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.axis.client.Stub;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.closeticketuri.ServiceLocator;
import org.closeticketuri.ServiceSoap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.FRConstant.MQRequestType;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectResponse;
import com.cupola.fwmp.dao.integ.prospect.pojo.MqProspectStatus;
import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.notification.EmailNotiFicationHelper;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CrmUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.fr.TicketModifyInMQVo;
import com.cupola.fwmp.vo.integ.app.FrTicketMqModifyLog;
import com.cupola.fwmp.vo.integ.app.MqTicketCloseLog;
import com.thoughtworks.xstream.XStream;

public class MQSCloseTicketDAOImpl implements MQSCloseTicketDAO
{

	private static final Logger LOGGER = LogManager
			.getLogger(MQSCloseTicketDAOImpl.class);

	@Autowired
	CrmUtil crmUtil;

	private String hydURL;

	private String roiURL;

	private String hydResolutionCode;

	private String roiResolutionCode;
	private String failedMqSupportEmailId;
	private String fwmpServerIp;

	@Autowired
	EmailNotiFicationHelper emailNotiFicationHelper;
	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	MongoTemplate mongoTemplate;

	public void setHydResolutionCode(String hydResolutionCode)
	{
		this.hydResolutionCode = hydResolutionCode;
	}

	public void setRoiResolutionCode(String roiResolutionCode)
	{
		this.roiResolutionCode = roiResolutionCode;
	}

	public void setHydURL(String hydURL)
	{
		this.hydURL = hydURL;
	}

	public void setRoiURL(String roiURL)
	{
		this.roiURL = roiURL;
	}

	public String getFailedMqSupportEmailId()
	{
		return failedMqSupportEmailId;
	}

	public void setFailedMqSupportEmailId(String failedMqSupportEmailId)
	{
		this.failedMqSupportEmailId = failedMqSupportEmailId;
	}

	public String closeTicket(TicketClosureDataVo closeTicketVo)
	{

		MqTicketCloseLog mqTicketCloseLog = getMqCloseInput();

		String serviceArea = null;
		
		String closeTicketString = null;
		
		String mqResponse = null;
		
		int errorCode =  9564878;

		LOGGER.info("close ticket uri " + closeTicketVo.getMobileNo() );

		ServiceLocator mq = null;

		String url_for_log = null;

		String transactionNo = StrictMicroSecondTimeBasedGuid.newGuid()
				.toString();

		try
		{
			if (closeTicketVo.getTicketResolutionCode()
					.contains(hydResolutionCode))
			{

				LOGGER.info("hitting hyd with URL==========" + hydURL + " "
						+ hydResolutionCode);
				url_for_log = hydURL;
				mq = new ServiceLocator(hydURL);
				serviceArea = CityName.HYDERABAD_CITY;

			}

			else
			{
				LOGGER.info("hitting roi with URL==========" + roiURL + " "
						+ roiResolutionCode);
				url_for_log = roiURL;
				mq = new ServiceLocator(roiURL);
				serviceArea = "ROI";

			}
			// String url = "http://tempuri.org/";

			ServiceSoap service = mq.getServiceSoap();

			// SOAPHeaderElement authentication = new SOAPHeaderElement(url,
			// "MQUserNameToken");
			//
			// SOAPHeaderElement user = new SOAPHeaderElement(url, "User_id",
			// "AFP");
			//
			// SOAPHeaderElement password = new SOAPHeaderElement(url,
			// "Password", "AFP@123");
			//
			// authentication.addChild(user);
			//
			// authentication.addChild(password);
			//
			// ((Stub) service).setHeader(authentication);

			((Stub) service).setHeader(crmUtil.createMqAuth());

			closeTicketString = "<REQUESTINFO><CLOSETICKET><TICKETNO>"
					+ closeTicketVo.getWorkOrderNo()
					+ "</TICKETNO><TICKETRESCODE>"
					+ closeTicketVo.getTicketResolutionCode()
					+ "</TICKETRESCODE><TICKETRESNOTES>"
					+ closeTicketVo.getTicketResolutionNotes()
					+ "</TICKETRESNOTES></CLOSETICKET></REQUESTINFO>";

			LOGGER.info("MQ request while ticket closure :: "
					+ closeTicketString);

			mqTicketCloseLog.setRequestString(closeTicketString);
			mqTicketCloseLog
					.setMqUrl(url_for_log + " transactionNo " + transactionNo);

			mqTicketCloseLog.setResponseTime(new Date().toString());

			Object ob = service.closeTicket(closeTicketString, transactionNo);

			mqResponse = ob.toString();

			LOGGER.info("MQ Response while ticket closure :: "
					+ " for Work order " + closeTicketVo.getWorkOrderNo() + " "
					+ mqResponse);

			if (mqResponse == null)
				mqTicketCloseLog.setResponseString("No response from MQ");
			else
				mqTicketCloseLog.setResponseString(mqResponse);
			
			mongoTemplate.insert(mqTicketCloseLog);

			if (mqResponse != null)
			{

				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream
						.fromXML(mqResponse);

				 errorCode = mqProspectResponse.getStatus().getErrorNo();

				LOGGER.info("Error number " + errorCode + " for Work order "
						+ closeTicketVo.getWorkOrderNo());

				// String error = errorNo.toString();
				//
				// int errorCode = Integer.valueOf(error);

				if (errorCode > 0 && errorCode != 99070)
				{
					handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse, closeTicketVo
							.getWorkOrderNo(), transactionNo, errorCode, serviceArea);
				}  

			} else
			{
				handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse, closeTicketVo
						.getWorkOrderNo(), transactionNo, 9000000, serviceArea);
			}

			String str = "[&]";

			Pattern regex = Pattern.compile(str);
			// matcher to find if there is any special character in string
			java.util.regex.Matcher matcher = regex.matcher(mqResponse);

			if (matcher.find())
			{
				mqResponse = mqResponse.replaceAll(str, " AND ");
				LOGGER.info("After removing the & symbol MQ Response :: "
						+ mqResponse);
				mqTicketCloseLog.setResponseString(mqResponse);
			}

			return mqResponse;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			mongoTemplate.insert(mqTicketCloseLog);
			handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse, closeTicketVo
					.getWorkOrderNo(), transactionNo, errorCode, serviceArea);
			LOGGER.error(" Error while closing ticket in MQ " + e.getMessage());
			return null;
		}
	}

	private MqTicketCloseLog getMqCloseInput()
	{
		MqTicketCloseLog mqLogs = new MqTicketCloseLog();

		mqLogs.setRequestTime(new Date().toString());
		mqLogs.setAddedBy(AuthUtils.getCurrentUserId());
		mqLogs.setMqLogId(StrictMicroSecondTimeBasedGuid.newGuid());
		return mqLogs;

	}

	private void handleWorkOrderClosureFailed(String url, String request,
			String response, String workOrderNo, String transactionNo,
			int errorCode, String serviceArea)
	{
		try {
			
			InetAddress inetAddress = InetAddress.getLocalHost();
			
			String serverIp = null;
			
		StringBuilder text = new StringBuilder();
		EMailVO eMail = new EMailVO();
		eMail.setSubject("Workorder closure Failed in MQ for WorkOrderNo : "
				+ workOrderNo

				+ " Error Code " + errorCode);

		text.append("Mq URL : " + url).append("\n\n");
		text.append("Mq Service Area : " + serviceArea).append("\n\n");
		text.append("FWMP Server IP : " + fwmpServerIp).append("\n\n");
		text.append("Mq TransactionNo : " + transactionNo).append("\n\n");
		text.append("ErrorCode : " + errorCode).append("\n\n");
		text.append("Error Message : " + definitionCoreService
				.getMqErrorMessageById(Long.valueOf(errorCode + "")))
				.append("\n\n");

		text.append("Mq Input XML : " + request).append("\n\n");
		text.append("Mq Response  : " + response).append("\n\n");
		eMail.setMessage(text.toString());
		eMail.setTo(failedMqSupportEmailId);
		emailNotiFicationHelper.addNewMailToQueue(eMail);
			
		} catch ( Exception e) {
			e.printStackTrace();
	}
	}

	public String getFwmpServerIp()
	{
		return fwmpServerIp;
	}

	public void setFwmpServerIp(String fwmpServerIp)
	{
		this.fwmpServerIp = fwmpServerIp;
	}
	
	@Override
	public String closeFrTicket( TicketClosureDataVo closeTicketVo )
	{
		
//		MqTicketCloseLog mqTicketCloseLog = getMqCloseInput();
		
		FrTicketMqModifyLog mqTicketCloseLog = getFrMqCloseInput();
		mqTicketCloseLog.setWorkOrderNumber(closeTicketVo.getWorkOrderNo());
		mqTicketCloseLog.setRequestType(MQRequestType.TICKET_CLOSE+"");

		String serviceArea = null;
		ServiceLocator mq = null;
		String url_for_log = null;
		String mqResponse = null;
		String closeTicketString = null;

		String transactionNo = StrictMicroSecondTimeBasedGuid.newGuid()
				.toString();

		try
		{
			if (closeTicketVo.getCityName() != null 
					&& CityName.HYDERABAD_CITY.equalsIgnoreCase(closeTicketVo.getCityName()) )
			{
				
				url_for_log = getMQEndPoint( closeTicketVo.getCityName() );
				
				LOGGER.info("hitting hyd with URL==========" + url_for_log + " "
						+ closeTicketVo.getTicketResolutionCode());
				
				mq = new ServiceLocator(url_for_log);
				serviceArea = CityName.HYDERABAD_CITY;

			}

			else
			{
				
				url_for_log = getMQEndPoint( closeTicketVo.getCityName() );
				
				LOGGER.info("hitting roi with URL==========" + url_for_log + " "
						+ closeTicketVo.getTicketResolutionCode());
						
				mq = new ServiceLocator(url_for_log);
				serviceArea = "ROI";

			}

			ServiceSoap service = mq.getServiceSoap();

			((Stub) service).setHeader(crmUtil.createMqAuth());

			closeTicketString = getMqRequestString( closeTicketVo );

			LOGGER.info("MQ request while ticket closure :: "
					+ closeTicketString);

			mqTicketCloseLog.setRequestString(closeTicketString);
			mqTicketCloseLog
					.setMqUrl(url_for_log + " transactionNo " + transactionNo);

			mqTicketCloseLog.setResponseTime(new Date().toString());

			Object ob = service.modifyTicket(closeTicketString, transactionNo);
			
			mqResponse = ob.toString();

			LOGGER.info("MQ Response while ticket closure :: "
					+ " for Work order " + closeTicketVo.getWorkOrderNo() + " "
					+ mqResponse);

			mqTicketCloseLog.setResponseString(mqResponse);
			mongoTemplate.insert(mqTicketCloseLog);

			if (mqResponse != null)
			{

				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream
						.fromXML(mqResponse);

				int errorCode = mqProspectResponse.getStatus().getErrorNo();

				LOGGER.info("Error number " + errorCode + " for Work order "
						+ closeTicketVo.getWorkOrderNo());

				if (errorCode > 0 && errorCode != 99070)
				{
					handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse, closeTicketVo
							.getWorkOrderNo(), transactionNo, errorCode, serviceArea);
				} 

			}
			else
			{
				handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse, closeTicketVo
						.getWorkOrderNo(), transactionNo, 9000000, serviceArea);
			}

			String str = "[&]";

			Pattern regex = Pattern.compile(str);
			java.util.regex.Matcher matcher = regex.matcher(mqResponse);

			if (matcher.find())
			{
				mqResponse = mqResponse.replaceAll(str, " AND ");
				LOGGER.info("After removing the & symbol MQ Response :: "
						+ mqResponse);
				mqTicketCloseLog.setResponseString(mqResponse);
			}

			return mqResponse;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			mongoTemplate.insert(mqTicketCloseLog);
			handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse, closeTicketVo
					.getWorkOrderNo(), transactionNo, 9000000, serviceArea);
		
			LOGGER.error(" Error while closing ticket in MQ " + e.getMessage());
			return null;
		}
	}
	
	private String getMqRequestString(  TicketClosureDataVo closeTicketVo )
	{
		String request = "<REQUESTINFO><MODIFYTICKET><TICKETNO>"+closeTicketVo.getWorkOrderNo()+"</TICKETNO>"+
	   " <TICKETCATEGORY></TICKETCATEGORY>	 <TICKETSHORTDESCRIPTION></TICKETSHORTDESCRIPTION>"+
	   " <TICKETDESCRIPTION></TICKETDESCRIPTION>"+
	   " <TICKETNATURE></TICKETNATURE>"+
	   " <TICKETSTATUS>CLOSED</TICKETSTATUS>"+
	   " <TICKETPRIORITY></TICKETPRIORITY>"+
	   " <PREFERREDDATE></PREFERREDDATE>"+
	   " <SCHEDULEDDATE></SCHEDULEDDATE>"+
	   " <SERVICETEAM></SERVICETEAM>"+
	   " <ASSIGNEDTO></ASSIGNEDTO>"+
	   " <NEXTCALLON></NEXTCALLON>"+
	   " <CALLTYPE></CALLTYPE>"+
	   " <COMMENT>DEFAULT</COMMENT>"+
	   " <COMMENTNOTES>CLOSED BY TAB</COMMENTNOTES>"+
	   " <SMSTOEMP></SMSTOEMP>"+
	   " <SMSTOCUST></SMSTOCUST>"+
	   " <AREA></AREA>	"+
	   " <TYPE></TYPE>"+
	   " <TICKETRESCODE>"+closeTicketVo.getTicketResolutionCode()+"</TICKETRESCODE>"+
	   " <TICKETRESNOTES>"+closeTicketVo.getTicketResolutionNotes()+"</TICKETRESNOTES>"+
     " </MODIFYTICKET>"+
     " <FLEX-ATTRIBUTE-INFO>"+
	 "  <ATTRIBUTE1></ATTRIBUTE1>"+
	 "  <ATTRIBUTE2></ATTRIBUTE2>"+
	 "  <ATTRIBUTE3></ATTRIBUTE3>"+
	 "  <ATTRIBUTE4></ATTRIBUTE4>"+
	 "  <ATTRIBUTE5></ATTRIBUTE5>"+
	 "  <ATTRIBUTE6></ATTRIBUTE6>"+
	 "  <ATTRIBUTE7></ATTRIBUTE7>"+
	 "  <ATTRIBUTE8></ATTRIBUTE8>"+
	 "  <ATTRIBUTE9></ATTRIBUTE9>"+
	 "   <ATTRIBUTE10></ATTRIBUTE10>"+
   " </FLEX-ATTRIBUTE-INFO>"+

   " <PROBLEM-INFO>"+
	"<PROBLEM>"+
	"</PROBLEM>"+
   " </PROBLEM-INFO>"+
   "</REQUESTINFO>";
		
		return request;
	}
	
	private String getMQEndPoint( String city )
	{
		if( city != null && city.equalsIgnoreCase(CityName.HYDERABAD_CITY) )
			return definitionCoreService
					.getActForceUrl(DefinitionCoreService.HYD_MQ_API_KEY);
		else
			return definitionCoreService
					.getActForceUrl(DefinitionCoreService.ROI_MQ_API_KEY);
	}
	
	@Override
	public String modifyTicket( TicketModifyInMQVo vo )
	{

		if( vo == null || vo.getWorkOrderNumber() == null 
				|| vo.getRequestParam() == null || vo.getRequestParam().isEmpty())
			
			return null;
//		MqTicketCloseLog mqTicketCloseLog = getMqCloseInput();
		
		FrTicketMqModifyLog mqTicketCloseLog = getFrMqCloseInput();
		mqTicketCloseLog.setWorkOrderNumber(vo.getWorkOrderNumber());
		mqTicketCloseLog.setRequestType(vo.getMqModifyRequestType()+"");

		String serviceArea = null;
		ServiceLocator mq = null;
		String url_for_log = null;
		String closeTicketString = null;
		String mqResponse = null;
		String transactionNo = StrictMicroSecondTimeBasedGuid.newGuid()
				.toString();

		try
		{
			if (vo.getCityName() != null 
					&& CityName.HYDERABAD_CITY.equalsIgnoreCase(vo.getCityName()) )
			{
				
				url_for_log = getMQEndPoint( vo.getCityName() );
				
				LOGGER.info("hitting hyd with URL==========" + url_for_log + " FOR MODIFY" );
				
				mq = new ServiceLocator(url_for_log);
				serviceArea = CityName.HYDERABAD_CITY;

			}

			else
			{
				
				url_for_log = getMQEndPoint( vo.getCityName() );
				
				LOGGER.info("hitting roi with URL==========" + url_for_log + " FOR MODIFY " );
						
				mq = new ServiceLocator(url_for_log);
				serviceArea = "ROI";

			}

			ServiceSoap service = mq.getServiceSoap();
			((Stub) service).setHeader(crmUtil.createMqAuth());
			closeTicketString = vo.getRequestParam();

			LOGGER.info("MQ request while ticket closure :: "
					+ closeTicketString);

			mqTicketCloseLog.setRequestString(closeTicketString);
			mqTicketCloseLog
					.setMqUrl(url_for_log + " transactionNo " + transactionNo);

			mqTicketCloseLog.setResponseTime(new Date().toString());

			Object ob = service.modifyTicket(closeTicketString, transactionNo);
			mqResponse = ob.toString();

			LOGGER.info("MQ Response while ticket closure :: "
					+ " for Work order " + vo.getWorkOrderNumber() + " "
					+ mqResponse);

			mqTicketCloseLog.setResponseString(mqResponse);
			mongoTemplate.insert(mqTicketCloseLog);

			if (mqResponse != null)
			{

				XStream xstream = new XStream();

				xstream.alias("RESPONSEINFO", MqProspectResponse.class);
				xstream.aliasField("STATUS", MqProspectResponse.class, "status");
				xstream.aliasField("TRANSACTIONNO", MqProspectStatus.class, "transactionNo");
				xstream.aliasField("ERRORNO", MqProspectStatus.class, "errorNo");
				xstream.aliasField("MESSAGE", MqProspectStatus.class, "message");

				MqProspectResponse mqProspectResponse = (MqProspectResponse) xstream
						.fromXML(mqResponse);

				int errorCode = mqProspectResponse.getStatus().getErrorNo();

				LOGGER.info("Error number " + errorCode + " for Work order "
						+ vo.getWorkOrderNumber());

				if (errorCode > 0 && errorCode != 99070)
				{
					handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse,
							vo.getWorkOrderNumber() , transactionNo, errorCode, serviceArea);
				} 

			}
			else
			{
				handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse, 
						vo.getWorkOrderNumber(), transactionNo, 9000000, serviceArea);
			}

			String str = "[&]";

			Pattern regex = Pattern.compile(str);
			java.util.regex.Matcher matcher = regex.matcher(mqResponse);

			if (matcher.find())
			{
				mqResponse = mqResponse.replaceAll(str, " AND ");
				LOGGER.info("After removing the & symbol MQ Response :: "
						+ mqResponse);
				mqTicketCloseLog.setResponseString(mqResponse);
			}
			return mqResponse;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			mongoTemplate.insert(mqTicketCloseLog);
			handleWorkOrderClosureFailed(url_for_log, closeTicketString, mqResponse,
			vo.getWorkOrderNumber() , transactionNo, 021212120, serviceArea);
			LOGGER.error(" Error while closing ticket in MQ " + e.getMessage());
			return null;
		}
	}
	
	private FrTicketMqModifyLog getFrMqCloseInput()
	{
		FrTicketMqModifyLog mqLogs = new FrTicketMqModifyLog();

		mqLogs.setRequestTime(new Date().toString());
		mqLogs.setAddedBy(AuthUtils.getCurrentUserId());
		mqLogs.setMqLogId(StrictMicroSecondTimeBasedGuid.newGuid());
		return mqLogs;

	}
	
}
