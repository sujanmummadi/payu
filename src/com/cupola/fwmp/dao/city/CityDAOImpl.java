 package com.cupola.fwmp.dao.city;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.Status;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CityVO;

@Transactional
public class CityDAOImpl implements CityDAO
{

	private Logger log = Logger.getLogger(CityDAOImpl.class.getName());

	HibernateTemplate hibernetTemplate;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernetTemplate = hibernetTemplate;

	}

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	private static final String GET_ALL_CITIES = "select * from City";
	private static final String UPDATE_CITIES_FLOW_SWITCH = "update City set flowSwitch = :flowValue where id = :cityId";

	public static Map<Long, CityVO> cityIdCityDetails = new ConcurrentHashMap<Long, CityVO>();

	public static Map<Long, String> cityIdAndCityNameMap = new ConcurrentHashMap<Long, String>();

	public static Map<String, String> cityNameAndCityCodeMap = new ConcurrentHashMap<String, String>();

	public static Map<String, String> cityNameAndStateMap = new ConcurrentHashMap<String, String>();

	public static Map<String, Long> cityNameAndCityIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<Long, String> cityIdAndCityCodeMap = new ConcurrentHashMap<Long, String>();

       public static Map<String, Long> cityCodeAndCityIdMap = 
			new CaseInsensitiveMap(new ConcurrentHashMap<String, Long>());

	// public static Map<String, Long> cityCodeAndCityIdMap = new
	// ConcurrentHashMap<String, Long>();

	public static Map<Long, String> cityIdAndStateMap = new ConcurrentHashMap<Long, String>();

	public static Map<Long, String> cityIdAndCityNameCorrespondingStateMap = new ConcurrentHashMap<Long, String>();

	public static Map<String, City> cityNameAndCityMap = 
			new ConcurrentHashMap<String, City>();

	private void init()
	{
		log.info("City init method called.. in CityDAOImpl");
		getCities();
		initializeStatusConstants();
		
		log.info("City init method executed.");

	}

	private void initializeStatusConstants()
	{
		log.info(" Inside initializeStatusConstants " );
		
		try {
			List<Status> allStatus = (List<Status>) hibernetTemplate
					.find("from Status");
			if (allStatus != null && !allStatus.isEmpty())
			{
				for (Status status : allStatus)
				{
					TicketStatus.statusCodeValue
							.put(status.getId(), status.getStatus());
					TicketStatus.statusDisplayValue
							.put(status.getId(), status.getDescription());
				}
			}
		} catch ( Exception e) {
			log.error("  error while initializeStatusConstants " ,e );
			 
		}

	}

	@Override
	public CityVO addCity(City city)
	{
		CityVO cityVO = new CityVO();
		
		if(city == null)
			return cityVO;
		
		log.info(" Inside addCity method " + city.getCityName() );
		try
		{

			List<CityVO> cities = new ArrayList<CityVO>(cityIdCityDetails
					.values());

			for (CityVO city1 : cities)
			{
				if (city1.getCityCode().equalsIgnoreCase(city.getCityCode()))
				{

					if (city1.getCityName()
							.equalsIgnoreCase(city.getCityName()))
					{
						BeanUtils.copyProperties(city ,cityVO);
						cityVO.setRemarks(ResponseUtil.createDuplicateResponse().getStatusMessage());
						return cityVO;
					}
				}
			}

			city = addCommonInitialValues(city);

			hibernetTemplate.save(city);

			BeanUtils.copyProperties(city, cityVO);
			addToCache(cityVO);
			addCityToCache(city);
			log.debug("Added city with id: " + city.getId() + "data sucessfuly");

			log.debug("city vo after adding ====== " + cityVO);

		} catch ( Exception e)
		{

			log.error("Error while adding City " + city.getCityName()
					+ ", Message " + e.getMessage());

			e.printStackTrace();
		}
		BeanUtils.copyProperties(city ,cityVO);
		cityVO.setRemarks(ResponseUtil.createSuccessResponse().getStatusMessage());
		
		log.debug(" Exit From addCity method " + city.getCityName() );
		
		return cityVO;
	}

	@Override
	public City updateCity(City city)
	{
		if (city == null || city.getId() <= 0)
			return null;
		
		log.info(" Inside updateCity method  " +city.getCityName());
		try {
			city.setModifiedOn(new Date());
			hibernetTemplate.update(city);
		} catch (Exception e) {
			log.error("Error occured while updating branch...",e);
		}
		
		log.debug(" Exit From updateCity method  " +city.getCityName());
		
		return city;
	}

	@Override
	public City deleteCity(Long cityId)
	{
		City city = null;
		
		if (cityId == null || cityId.longValue() <= 0)
			return city;
		
		log.info(" Inside  deleteCity method  " +cityId);
		try {
			city = (City) hibernetTemplate.load(City.class, cityId);

			hibernetTemplate.delete(city);
		} catch ( Exception e) {
			 log.error( "error while deleting city  ",e);
		}
		
		log.debug(" Exit From  deleteCity method  " +cityId);
		return city;
	}

	@Override
	public CityVO getCityById(Long cityId)
	{
		if (cityId == null || cityId.longValue() <= 0)
			return null;
		
		log.debug(" Inside getting city by id : "+cityId);
		
		if (cityIdCityDetails != null && cityIdCityDetails.containsKey(cityId))
		{

			return cityIdCityDetails.get(cityId);

		} else
			return null;
	}

	@Override
	public Map<Long, String> getAllCity()
	{
		log.debug(" Inside getting AllCity ");
		/*
		 * Map<Long, String> cityIdAndNameMap = new TreeMap<Long, String>();
		 * 
		 * List<City> cityList = (List<City>) hibernetTemplate.find("from City"
		 * );
		 * 
		 * if(cityList != null){
		 * 
		 * for (City city : cityList) {
		 * 
		 * cityIdAndNameMap.put(city.getId(), city.getCityName()); } } return
		 * cityIdAndNameMap;
		 */

		if (cityIdAndCityNameMap.size() == 0)

			getCities();

		log.debug("+++++++++++++++" + cityIdAndCityNameMap);
		return cityIdAndCityNameMap;
	}

	@Override
	public Map<Long, String> citySearchSuggestion(String cityQuery)
	{
		Map<Long, String> cityNames = new TreeMap<Long, String>();

       if(cityQuery == null || cityQuery.isEmpty())
			return cityNames;
			
		log.info(" citysearch suggestion : "+cityQuery);
		try {
		if (cityQuery != null)
		{

			List<City> cities = (List<City>) hibernetTemplate
					.find("from City where cityName like?", "%" + cityQuery
							+ "%");

			if (cities != null && !cities.isEmpty()) 
			{

				log.debug("list of cities size in cities suggestion : "
						+ cities.size());

				for (City city : cities)
				{

					cityNames.put(city.getId(), city.getCityName());

				}
			}
		}
		} catch ( Exception e) {
			log.error("Error occured while city search :" ,e);
		}
		log.debug(" Exit From citysearch suggestion : "+cityQuery);
		return cityNames;


	}

	private void getCities()
	{
		log.debug("########## getCities START jdbcTemplate ##########"
				+ new Date());
		List<CityVO> cityList = jdbcTemplate
				.query(GET_ALL_CITIES, new BeanPropertyRowMapper(CityVO.class));
		log.debug("########## getCities END jdbcTemplate ##########");

		/*
		 * log.info("########## getCities START hibernetTemplate ##########"+new
		 * Date()); List<City> cityList = (List<City>) hibernetTemplate.find(
		 * "from City"); log.info(
		 * "########## getCities START hibernetTemplate ##########"+new Date());
		 */

		if (cityList == null || cityList.isEmpty())
		{

			log.info("City details are not available");
			return;

		} else
		{
			log.debug("########## add city cahe START ##########" + new Date());
			for (CityVO cityVo : cityList)
			{
				City city = new City();
				addToCache(cityVo);
				BeanUtils.copyProperties(cityVo, city);
				addCityToCache(city);

			}
			log.debug("########## add city cahe END ##########" + new Date());
		}

	}

	private void addToCache(CityVO city)
	{
		log.debug(" Inside add City ToCache " +city.getCityName());
		
		cityIdCityDetails.put(city.getId(), city);

		String cityName = city.getCityName();

		cityIdAndCityNameMap.put(city.getId(), cityName);

		cityNameAndCityCodeMap.put(cityName, city.getCityCode());

		cityNameAndStateMap.put(cityName, city.getState());

		cityIdAndCityCodeMap.put(city.getId(), city.getCityCode());

		cityIdAndStateMap.put(city.getId(), city.getState());

		cityNameAndCityIdMap.put(cityName.toUpperCase(), city.getId());

		cityCodeAndCityIdMap.put(city.getCityCode(), city.getId());
		
		log.debug("########## addTo Cache END ##########"  +city.getCityName());
	}

	private void addCityToCache(City city)
	{
		log.debug(" Inside add City ToCache " +city.getCityName());
		
		cityNameAndCityMap.put(city.getCityName().trim().toLowerCase(), city);
		
		log.debug("########## add City ToCache END ##########"  +city.getCityName());
	}

	@Override
	public City getCityByCityName(String cityName)
	{
		if(cityName == null || cityName.isEmpty())
			return null;
		
		log.debug(" Inside getting all cities...by city NAME "+cityName);
		
		if (cityNameAndCityMap != null
				&& cityNameAndCityMap.containsKey(cityName.toLowerCase()))
		{
			return cityNameAndCityMap.get(cityName.toLowerCase());

		} else
			return null;
	}

	private void removeFromCache(Long cityId)
	{
		if (cityId == null || cityId.longValue() <= 0)
			return;
		
		log.debug(" Inside removing from cache ...: " +cityId);

		if (cityIdCityDetails != null && cityIdCityDetails.containsKey(cityId))
		{

			cityIdCityDetails.remove(cityId);
		}

	}

	public City addCommonInitialValues(City city)
	{
		log.debug(" Inside adding city CommonInitialValues " +city.getCityName() );

		city.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		city.setAddedOn(new Date());
		city.setModifiedOn(new Date());
		city.setAddedBy(AuthUtils.getCurrentUserId());
		city.setModifiedBy(AuthUtils.getCurrentUserId());
		city.setStatus(FWMPConstant.Status.ACTIVE);
		
		log.debug(" Exit From adding city CommonInitialValues " +city.getCityName() );
		
		return city;

	}

	@Override
	public City getCityByCityCode(String cityCode)
	{
		if(cityCode == null || cityCode.isEmpty())
			return null;
		
		log.debug(" Inside getting city by citycode : "+cityCode);
		try {
		List<City> cities = hibernetTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("select * from City where cityCode = :cityCode")
				.addEntity(City.class).setParameter("cityCode", cityCode)
				.list();
		// List<City> cities = (List<City>)
		// hibernetTemplate.find("from City where cityCode = ?", cityCode);

		if (cities != null && !(cities.isEmpty()))
		{
			return cities.get(0);
		}
		} catch ( Exception e) {
			log.error("Error occured while getting city by city code :"+cityCode,e);
		}

		log.debug(" Exit From getting city by citycode : "+cityCode);
			return null;

		/*
		 * if(cityIdCityDetails.size() == 0) getCities();
		 * 
		 * List<City> cityList = new
		 * ArrayList<City>(cityIdCityDetails.values());
		 * 
		 * for (City city : cityList) {
		 * if(city.getCityCode().equalsIgnoreCase(cityCode)){
		 * 
		 * return city; } }
		 * 
		 * return null;
		 */
	}

	@Override
	public String getCityCodeByName(String cityName)
	{
		if (cityIdAndCityNameMap != null
				&& cityIdAndCityNameMap.containsKey(cityName))
		{

			return cityIdAndCityNameMap.get(cityName);

		} else
			return null;
	}

	@Override
	public String getCityNameById(Long id)
	{
		if (id == null || id.longValue() <= 0)
			return "";
		
		log.debug(" Inside getting city name by id : "+id);

		if (cityIdAndCityNameMap != null
				&& cityIdAndCityNameMap.containsKey(id))
		{

			return cityIdAndCityNameMap.get(id);

		} else
			return "";
	}

	@Override
	public String getStateNameByName(String cityName)
	{
		log.debug("getting state name by name : "+cityName);
		
		if(cityName == null || cityName.isEmpty())
			return "";
		if (cityNameAndStateMap != null
				&& cityNameAndStateMap.containsKey(cityName))
		{

			return cityNameAndStateMap.get(cityName);

		} else
			return "";
	}

	@Override
	public String getCityCodeById(Long id)
	{
		log.debug("getting city code by id : "+id);
		
		if (id == null || id.longValue() <= 0)
			return "";
		
		if ((cityIdAndCityCodeMap != null && !cityIdAndCityCodeMap.isEmpty())
				&& cityIdAndCityCodeMap.containsKey(id))
		{

			return cityIdAndCityCodeMap.get(id);

		} else
			return "";
	}

	@Override
	public String getStateNameByCityId(Long id)
	{
		log.debug("getting state name by CityId : "+id);
		
		if (id == null || id.longValue() <= 0)
			return "";
		if (cityIdAndStateMap != null && cityIdAndStateMap.containsKey(id))
		{

			return cityIdAndStateMap.get(id);

		} else
			return "";
	}

	@Override
	public Long getCityIdByName(String cityName)
	{
		if (cityName == null || cityName.isEmpty())
			return null;
		
		log.debug("getting city id by name : "+cityName);
		
		if (cityNameAndCityIdMap != null
				&& cityNameAndCityIdMap.containsKey(cityName))
		{

			return cityNameAndCityIdMap.get(cityName);

		} else
			return null;
	}

	@Override
	public Long getIdByCode(String code)
	{
		if(code == null || code.isEmpty())
			return null;
		
		log.debug(" Inside getting Id By Code : "+code);
		
		if (cityCodeAndCityIdMap != null
				&& cityCodeAndCityIdMap.containsKey(code))
		{
			return cityCodeAndCityIdMap.get(code);

		} else
			return null;
	}

	@Override
	public Map<Long, String> getAllCitiesCorrespondingToState(String stateName)
	{
		// TODO Auto-generated method stub
		if (stateName == null || stateName.isEmpty())
			return null;
		
		log.debug(" Inside getAllCitiesCorrespondingToState "+stateName);

		try {
		List<City> cities = (List<City>) hibernetTemplate
				.find("from City where state =?", stateName);
			if (cities != null && !cities.isEmpty())
		{

			log.debug("list of cities size in cities suggestion : "
					+ cities.size());

			for (City city : cities)
			{

				cityIdAndCityNameCorrespondingStateMap
						.put(city.getId(), city.getCityName());

			}
		}
		} catch ( Exception e) {
			log.error("Error occured while getting all cities corresponding to state :"+stateName,e);
		}
		
		log.debug(" Exit From getAllCitiesCorrespondingToState "+stateName);

		return cityIdAndCityNameCorrespondingStateMap;
	}

	@Override
	public List<Long> getAllCityIds()
	{
		log.debug(" Inside getting all cityids : " );

		List<Long> cityIdList = null;
		try {
			cityIdList = (List<Long>) hibernetTemplate
				.find("select c.id from City c");

			if (cityIdList == null || cityIdList.isEmpty())
				return null;
			
		} catch ( Exception e) {
			log.error("Error in getting all cityids");
			e.printStackTrace();
		}
		
		log.debug(" Exit From getting all cityids : " );
		
		return cityIdList;

	}

	@Override
	public String getCityNameByTicketId(Long id)
	{
		List<String> cityNames = null;
		
		if(id == null || id <= 0 )
			return  null;
		
		log.info(" Inside getting CityNameByTicketId : " +id  );
		try {
			cityNames = jdbcTemplate
				.query(GET_CITY_NAME_BY_TICKET_ID, new Object[] {
						id }, new CityNameRowMapper());

		if (cityNames == null || cityNames.isEmpty())
		{
			return null;
		} else
		{
			return cityNames.get(0);
		}
		} catch (Exception e) {
			log.error("Error occured while getting  city name by ticketid :"+id,e);
	}
       log.debug(" Exit From getting CityNameByTicketId : " +id  );
		
		 return null;
		 }
	@Override
	public Long updateCityFlowSwitch(Long cityId, String flowSwitch)
	{
		Long id = 0l;
		if (cityId == null || cityId <= 0)
			return id;
		
		log.debug(" Inside updateCityFlowSwitch : " +cityId  );
		try
		{
			Query query = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(UPDATE_CITIES_FLOW_SWITCH)
					.setParameter("flowValue", flowSwitch)
					.setParameter("cityId", cityId);

			int result = query.executeUpdate();

			if (result > 0)
			{
				CityVO city = cityIdCityDetails.get(cityId);
				if (city != null)
				{
					city.setFlowSwitch(flowSwitch);
					cityIdCityDetails.put(cityId, city);
					id = cityId;
				} else
					getCities();
			}
		} catch (Exception e)
		{
			log.error("Error while upating City flow switch status :" + e);
		}
		log.debug(" Inside updateCityFlowSwitch : " +cityId  );
		return id;
	}

	class CityNameRowMapper implements RowMapper<String>
	{
		@Override
		public String mapRow(ResultSet resultSet, int arg1) throws SQLException
		{
			return resultSet.getString("c.cityName");
		}

	}

	@Override
	public void updateCityCache(City city)
	{
		if (city != null)
		{
			CityVO cityVO = new CityVO();
			BeanUtils.copyProperties(city, cityVO);
			addToCache(cityVO);
			addCityToCache(city);
		}
	}

	
}
