package com.cupola.fwmp.dao.city;


import java.util.List;
import java.util.Map;
import com.cupola.fwmp.vo.CityVO;
import com.cupola.fwmp.persistance.entities.City;

public interface CityDAO {
	
	String GET_CITY_NAME_BY_TICKET_ID = "select c.cityName from City c inner join Customer cu on cu.cityId=c.id inner join Ticket t on cu.id=t.customerId and t.id=?";
	
	public CityVO addCity(City city);
	public CityVO getCityById(Long id);
	public Map<Long, String> getAllCity();
	public City deleteCity(Long id);
	public City updateCity(City city);
	public List<Long> getAllCityIds();
	
	Map<Long, String> citySearchSuggestion(String cityQuery);
	
	City getCityByCityCode(String cityCode);
	
	String getCityCodeByName(String cityName);
	
	String getCityNameById(Long id);
	
	String getCityCodeById(Long id);
	
	String getStateNameByCityId(Long id);
	
	String getStateNameByName(String cityName);
	
	Long getCityIdByName(String cityName);

	Long getIdByCode(String code);
	
	Map<Long, String> getAllCitiesCorrespondingToState(String stateName);
	String getCityNameByTicketId(Long id);
	City getCityByCityName(String cityName);
	
	public Long updateCityFlowSwitch(Long cityId,String flowSwitch);
	/**@author aditya
	 * void
	 * @param city
	 */
	void updateCityCache(City city);
}
