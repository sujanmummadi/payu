package com.cupola.fwmp.dao.branch;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.vo.BranchDetailVo;
import com.cupola.fwmp.vo.BranchVO;

public interface BranchDAO {
	
	public BranchVO addBranch(Branch branch);

	public BranchVO getBranchById(Long id);

	public List<BranchVO> getAllBranch();

	public Branch deleteBranch(Long id);

	public Branch updateBranch(Branch branch);

	public Map<Long, String> getBranchesByCityId(String cityId);

	Map<Long, String> getBranchesByCityName(String cityName);
	
	Branch getBranchByBranchName(String branchName);
	
	Branch getBranchByBranchNameAndCityCode(String branchName, String cityCode);
	
	String getBranchCodeByName(String branchName);
	
	String getBranchCodeById(Long id);
	
	String getBranchNameById(Long id);

	Long getBranchCodeById(String name);
	
	Branch getBranchByBranchCode(String branchCode);
	
	Long getIdByCode(String code);
	
	Long getBranchIdByName(String branchName);
	
	public Long updateBranchFlowSwitchById(Long branchId, String flowSwitch);

	public List<BranchDetailVo> getAllBranchDetail( Long cityIdOrBranchId );

	/**@author aditya
	 * void
	 * @param branch
	 */
	void updateBranchCache(Branch branch);

	/**@author kiran
	 * Long
	 * @param branchName
	 * @param cityName
	 * @return
	 */
	Long getRealtyBranchByBranchNameAndCityName(String branchName, String cityName);
	
}

