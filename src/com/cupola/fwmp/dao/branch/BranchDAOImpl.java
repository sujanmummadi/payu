 package com.cupola.fwmp.dao.branch;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.BranchDetailVo;
import com.cupola.fwmp.vo.BranchVO;
import com.cupola.fwmp.vo.TypeAheadVo;

@Transactional
public class BranchDAOImpl implements BranchDAO
{

	private Logger log = Logger.getLogger(BranchDAOImpl.class.getName());

	HibernateTemplate hibernetTemplate;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernetTemplate = hibernetTemplate;

	}

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	CityDAO cityDAO;

	private static final String UPDATE_BRANCH_MODE_SQL = "UPDATE Branch SET flowSwitch = :switchValue ";
	
	private static final String GET_ALL_BRANCHES = "select id,branchName,branchCode,cityId,flowSwitch from Branch";

	public static Map<Long, BranchVO> branchIdBranchDetails = new ConcurrentHashMap<Long, BranchVO>();

	public static Map<Long, String> branchIdAndBranchNameMap = new ConcurrentHashMap<Long, String>();

	public static Map<String, String> branchNameAndBranchCodeMap = new ConcurrentHashMap<String, String>();

	public static Map<String, Long> branchNameAndBranchIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<Long, String> branchIdAndBranchCodeMap = new ConcurrentHashMap<Long, String>();

	public static Map<String, Long> branchCodeAndIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<String, Long> branchNameAndIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<String, String> branchCodeAndCityCodeMap = new ConcurrentHashMap<String, String>();
	public static Map<String, Branch> branchCodeAndBranchMap = new ConcurrentHashMap<String, Branch>();

	private void init()
	{
		log.info("Branch init method called..");
		getBranches();
		log.info("Branch init method executed.");
	}

	@Transactional
	public BranchVO addBranch(Branch branch)
	{
		BranchVO branchVO = new BranchVO();

		if(branch == null)
			return branchVO;
		
		log.info(" Inside adding Branch Details " + branch.getBranchName());
		
		try
		{

			List<BranchVO> branches = new ArrayList<BranchVO>(branchIdBranchDetails
					.values());

			for (BranchVO branch2 : branches)
			{
				if (branch2.getBranchName()
						.equalsIgnoreCase(branch.getBranchName()))
				{

					if (branch2.getBranchCode()
							.equalsIgnoreCase(branch.getBranchCode()))
					{

						if (branch2.getCityId() == (branch.getCity().getId()))
						{
							BeanUtils.copyProperties(branch, branchVO);
							branchVO.setRemarks(ResponseUtil
									.createDuplicateResponse()
									.getStatusMessage());
							
							log.debug("returning from addBranch having duplicate city for branch...");
							return branchVO;
						}
					}

				}
			}

			branch = addCommonInitialValues(branch);

			hibernetTemplate.save(branch);

			log.debug("branch with id " + branch.getId()
					+ " is added successfully");

			BeanUtils.copyProperties(branch, branchVO);

			City city = branch.getCity();
			if (city != null)
			{
				branchVO.setCityId(city.getId());
				branchVO.setCityCode(city.getCityCode());
				branchVO.setCityName(city.getCityName());

				addToCache(branchVO);

				log.debug("branch vo after adding ====== " + branchVO);
			}
		} catch (Exception e)
		{

			log.error("Error while adding Branch " + branch + ", Message "
					+ e.getMessage());

			e.printStackTrace();

		}
		BeanUtils.copyProperties(branch, branchVO);
		branchVO.setRemarks(ResponseUtil.createSuccessResponse()
				.getStatusMessage());
		
		log.debug(" Exit From adding Branch Details " + branch.getBranchName());
		
		return branchVO;
	}
	
	public Branch updateBranch(Branch branch)
	{
		if (branch == null || branch.getId() <= 0)
			return branch;
		
		log.debug(" Inside update Branch method "  +branch.getBranchName());

		try {
			branch.setModifiedOn(new Date());
			hibernetTemplate.update(branch);
		} catch (Exception e) {
			log.error("Error occured while updating branch...",e);
		}
		log.debug(" Exit From update Branch method "  +branch.getBranchName());
		
		return branch;
	}

	public Branch deleteBranch(Long branchId)
	{
		if( branchId == null || branchId.longValue() <= 0 )
			return null;
		
		log.info(" Inside delete Branch "  +branchId);
		
		Branch branch = (Branch) hibernetTemplate.get(Branch.class, branchId);

		hibernetTemplate.delete(branch);
		
		log.debug(" Exit From delete Branch "  +branchId);
		
		return branch;
	}

	public BranchVO getBranchById(Long branchId)
	{
		if( branchId == null || branchId.longValue() <= 0 )
			return null;
		
		log.debug(" Inside getting branch by branch id : "+branchId);
		
		if (branchIdBranchDetails != null
				&& branchIdBranchDetails.containsKey(branchId))
		{

			return branchIdBranchDetails.get(branchId);

		} else
		{

			return null;
		}
	}

	@Override
	public Branch getBranchByBranchName(String branchName) {
		if( branchName == null || branchName.isEmpty())
			return null ;
		
		log.debug(" Inside getting branch by branchName : "+branchName);

		try {
			List<Branch> branches = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select * from Branch where branchName = :branchName")
					.addEntity(Branch.class).setParameter("branchName", branchName)
					.list();
			if (branches != null && !(branches.isEmpty()))
			{

				return branches.get(0);
			}
		} catch (Exception e) {
			log.error("Error occured while getting branchName by branchName :"+branchName,e);
		}
		
		log.debug(" Exit From getting branch by branchName : "+branchName);
		
		return null;

		/*
		 * if(branchIdBranchDetails.size() == 0) getBranches();
		 * 
		 * List<Branch> branchList = new
		 * ArrayList<Branch>(branchIdBranchDetails.values());
		 * 
		 * for (Branch branch : branchList) {
		 * 
		 * if(branch.getBranchName().equalsIgnoreCase(branchName)){ log.debug(
		 * "branch found "+branch.getBranchName()); return branch; } else
		 * log.debug("branch not found "+branchName); } return null;
		 */
	}

	@Override
	public Branch getBranchByBranchNameAndCityCode(String branchName,
			String cityCode)
	{

		if( branchName == null || branchName.isEmpty() 
				|| cityCode == null || cityCode.isEmpty())
			return null;
		
		log.debug(" Inside getting branch by branchName"+branchName+" & city code: "+cityCode);
	 
		if (branchIdBranchDetails.size() == 0)
			getBranches();

		List<BranchVO> branchList = new ArrayList<BranchVO>(branchIdBranchDetails
				.values());

		for (BranchVO branch : branchList)
		{

			if (branch.getBranchName().equalsIgnoreCase(branchName))
			{
				if (branch.getCityCode().equalsIgnoreCase(cityCode))
				{
					log.debug("branch found " + branch.getBranchName()
							+ cityCode);
					return getBranchByBranchName(branchName);
				}
			} else
				log.info("branch not found " + branchName);
		}
		
		log.debug(" Exit From getting branch by branchName"+branchName+" & city code: "+cityCode);
		
		return null;
	}

	public List<BranchVO> getAllBranch()
	{
		log.debug(" Inside getting all branches... ");
		
		if (branchIdBranchDetails == null || branchIdBranchDetails.isEmpty())
			getBranches();

		List<BranchVO> branchList = null;
		if(branchIdBranchDetails != null)
		{
			branchList = new ArrayList<BranchVO>(branchIdBranchDetails
					.values());
			
			log.debug(" Exit From getting all branches... ");
		}
		
		
		return branchList;
	}

	@Override
	public Map<Long, String> getBranchesByCityName(String cityName)
	{
		Map<Long, String> branchNames = new TreeMap<>();
		
		if(cityName == null || cityName.isEmpty())
			return branchNames;
		
		log.debug(" Inside getting all branches by city name "+cityName);
		
		List<BranchVO> branchVOs = new ArrayList<BranchVO>(branchIdBranchDetails
				.values());

		for (BranchVO branchVO : branchVOs)
		{
			if (branchVO.getCityName().equalsIgnoreCase(cityName))
			{
				branchNames.put(branchVO.getId(), branchVO.getBranchName());
			}

		}

		/*
		 * if(cityName != null){
		 * 
		 * List<City> cities = (List<City>) hibernetTemplate.find(
		 * "from City where cityName like?", "%"+cityName+"%");
		 * 
		 * if(cities!=null){
		 * 
		 * log.debug("list of cities size in cities suggestion : "
		 * +cities.size());
		 * 
		 * City city = cities.get(0);
		 * 
		 * Set<Branch> branches = city.getBranches();
		 * 
		 * for (Branch branch : branches) {
		 * 
		 * branchNames.put(branch.getId(), branch.getBranchName()); }
		 * 
		 * }
		 * 
		 * log.debug("branches :"+branchNames+" for city:"+cityName);
		 * 
		 * }
		 */
		log.debug(" Exit From getting all branches by city name "+cityName);
		
		return branchNames;

	}

	private void getBranches()
	{

		log.debug("########## getBranches START jdbcTemplate ##########"
				+ new Date());
		List<BranchVO> branchList = jdbcTemplate
				.query(GET_ALL_BRANCHES, new BeanPropertyRowMapper(BranchVO.class));
		
		log.debug("########## getBranches END jdbcTemplate ##########");

		/*
		 * log.debug("########## getBranches START ##########"+new Date());
		 * List<Branch> branchList = (List<Branch>) hibernetTemplate .find(
		 * "from Branch"); log.debug("########## getBranches END ##########"+new
		 * Date());
		 */

		if (branchList == null || branchList.isEmpty())
		{
			log.info("Branch details are not available");
			return;

		} else
		{
			log.debug("########## add branch cahe START ##########"
					+ new Date());
			for (BranchVO branchVO : branchList)
			{
				branchVO.setCityCode(cityDAO
						.getCityCodeById(branchVO.getCityId()));
				branchVO.setCityName(cityDAO
						.getCityNameById(branchVO.getCityId()));
				addToCache(branchVO);
			}
			log.debug("########## add branch cahe START ##########"
					+ new Date());
		}

	}

	private void addToCache(BranchVO branch)
	{
		if(branch == null)
			return;
		
		log.debug("Adding Branch Details to cache... : "+branch.getBranchName());
		
		branchIdBranchDetails.put(branch.getId(), branch);

		String branchName = branch.getBranchName();

		branchIdAndBranchNameMap.put(branch.getId(), branchName);

		branchNameAndBranchCodeMap.put(branchName, branch.getBranchCode());

		branchIdAndBranchCodeMap.put(branch.getId(), branch.getBranchCode());

		branchCodeAndIdMap.put(branch.getBranchCode(), branch.getId());

		branchNameAndIdMap
				.put(branch.getBranchName().toUpperCase().trim(), branch.getId());

		branchNameAndBranchIdMap.put(branchName, branch.getId());

		branchCodeAndCityCodeMap
				.put(branch.getBranchCode(), branch.getCityCode());

	}

	private void removeFromCache(Long branchId)
	{
		if(branchId == null || branchId.longValue() <= 0)
			return;
		
		log.debug("removing from cache ...: " +branchId);
		
		if (branchIdBranchDetails != null
				&& branchIdBranchDetails.containsKey(branchId))
		{

			branchIdBranchDetails.remove(branchId);
		}

	}

	public Branch addCommonInitialValues(Branch branch)
	{
		log.debug(" Inside adding Branch CommonInitialValues "   +branch.getBranchName());

		branch.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		branch.setAddedOn(new Date());
		branch.setModifiedOn(new Date());
		branch.setAddedBy(AuthUtils.getCurrentUserId());
		branch.setModifiedBy(AuthUtils.getCurrentUserId());
		branch.setStatus(FWMPConstant.Status.ACTIVE);
		
		log.debug(" Exit From adding Branch CommonInitialValues "   +branch.getBranchName());
		
		return branch;

	}

	@SuppressWarnings("unchecked")
	public Map<Long, String> getBranchesByCityId(String cityId)
	{
		log.debug(" Inside getting Branches by city Id : "+cityId);

		Map<Long, String> branchNames = new TreeMap<>();

       try {
		if (cityId == null || cityId.equals("null") || cityId.equals("0"))
		{
		    log.info("get all branches");
		    
			List<Object[]> branches = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT id , branchName FROM Branch ")
					.list();
			if (branches != null && !branches.isEmpty())
			{
				for (Object[] obj : branches)
				{
					branchNames.put(((BigInteger) obj[0])
							.longValue(), ((String) obj[1]));

				}
			}

			else
				log.info("No branch found");
		}

		else
		{

			Long id = Long.valueOf(cityId);

			List<Object[]> branches = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT id , branchName FROM Branch where cityId =:id")
					.setParameter("id", id).list();
			if (branches != null && !branches.isEmpty())
			{
				for (Object[] obj : branches)
				{
					branchNames.put(((BigInteger) obj[0])
							.longValue(), ((String) obj[1]));

				}
			}

			log.debug("branches :" + branchNames + " for city:" + id);

		}
		}catch (Exception e) {
			log.error("Error occured while getting braches by cityid :" ,e);
		}  
		log.debug(" Exit From getting Branches by city Id : "+cityId);
		
		return branchNames;

	}

	@Override
	public String getBranchCodeByName(String branchName)
	{
		if(branchName == null || branchName.isEmpty())
			return "";
		
		log.debug(" Inside getting Branch Code by Name: "+branchName);
		
		if (branchNameAndBranchCodeMap != null
				&& branchNameAndBranchCodeMap.containsKey(branchName))
		{
			return branchNameAndBranchCodeMap.get(branchName);

		} else
			return "";
	}

	@Override
	public String getBranchNameById(Long id)
	{
		if(id == null || id.longValue() <= 0)
			return "";
		
		log.debug(" Inside getting BranchNameById : "+id);
		
		if (branchIdAndBranchNameMap != null
				&& branchIdAndBranchNameMap.containsKey(id))
		{
			return branchIdAndBranchNameMap.get(id);

		} else
			return "";
	}

	@Override
	public String getBranchCodeById(Long id)
	{
		log.debug("getting Branch Code by id: "+id);
		
		if(id == null || id.longValue() <= 0)
			return "";
		if (branchIdAndBranchCodeMap != null
				&& branchIdAndBranchCodeMap.containsKey(id))
		{
			return branchIdAndBranchCodeMap.get(id);

		} else
			return "";
	}

	@Override
	public Long getBranchCodeById(String name)
	{
		log.debug("getting Branch Code by id: "+name);
		
		if(name == null || name.isEmpty())
			return null;
		if (branchNameAndBranchIdMap != null
				&& branchNameAndBranchIdMap.containsKey(name))
		{
			return branchNameAndBranchIdMap.get(name);

		} else
			return null;
	}

	@Override
	public Branch getBranchByBranchCode(String branchCode)
	{
		if(branchCode == null || branchCode.isEmpty())
			return null;
		
		log.info("Inside getting Branches by BranchCode : "+branchCode);
		
		if(branchCodeAndBranchMap.containsKey(branchCode))
		{
			return branchCodeAndBranchMap.get(branchCode);
		}
		
		try
		{
			List<Branch> branches = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select * from Branch where branchCode = :branchCode")
					.addEntity(Branch.class)
					.setParameter("branchCode", branchCode).list();
			
			if (branches != null && !(branches.isEmpty()))
			{
				branchCodeAndBranchMap.put(branchCode, branches.get(0));
				return branches.get(0);
			}

			else
				return null;
		} catch (Exception e)
		{
			log.error("Error while getting  getBranchByBranchCode : "
					+ e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public Long getIdByCode(String code)
	{
		if(code == null || code.isEmpty())
			return null;
		
		log.debug(" Inside getting id by code: "+code);

		if (branchCodeAndIdMap != null && branchCodeAndIdMap.containsKey(code))
		{
			return branchCodeAndIdMap.get(code);

		} else
			return null;
	}

	@Override
	public Long getBranchIdByName(String branchName)
	{
		if(branchName == null || branchName.isEmpty())
			return null;
		
		log.debug("Inside getting Branch Id by Name: "+branchName);
		
		if (branchNameAndIdMap != null
				&& branchNameAndIdMap.containsKey(branchName))
		{
			return branchNameAndIdMap.get(branchName);

		} else
			return null;
	}

	@Override
	public Long updateBranchFlowSwitchById(Long branchId, String flowSwitch)
	{
		Long id = 0l;
		if (branchId == null || branchId <= 0 || flowSwitch == null
				|| flowSwitch.isEmpty())

			return id;
		
		log.info("Inside updateBranchFlowSwitchById : "+branchId);
		try
		{

			StringBuilder queryString = new StringBuilder(UPDATE_BRANCH_MODE_SQL);

			TypeAheadVo cityVO = AuthUtils.getCurrentUserCity();
			Long cityId = null;
			if (cityVO != null)
			{
				if (branchId.equals(cityVO.getId()))
				{
					cityId = cityVO.getId();
					queryString.append(" WHERE cityId = :cityIds ");
				} else
				{
					queryString.append(" WHERE id = :branchId ");
				}
			} else
				queryString.append(" WHERE id = :branchId ");

			Query query = hibernetTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(queryString.toString());

			if (cityVO != null && branchId.equals(cityVO.getId()))

				query.setParameter("switchValue", flowSwitch)
						.setParameter("cityIds", cityId);
			else
				query.setParameter("switchValue", flowSwitch)
						.setParameter("branchId", branchId);

			int result = query.executeUpdate();

			if (result > 0)
			{
				BranchVO branch = branchIdBranchDetails.get(branchId);
				if (branch != null)
				{
					branch.setFlowSwitch(flowSwitch);
					branchIdBranchDetails.put(branchId, branch);
					id = branchId;
				}

				else if (cityId != null)
				{
					getBranches();
					id = cityId;
				}
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		log.debug(" Exit from updateBranchFlowSwitchById : "+branchId);
		return id;
	}

	@Override
	public List<BranchDetailVo> getAllBranchDetail(Long cityIdOrBranchId)
	{
		List<BranchDetailVo> list = new ArrayList<BranchDetailVo>();
		if (cityIdOrBranchId == null || cityIdOrBranchId <= 0)
			return list;
		
		log.debug("Inside getAllBranchDetail : "+cityIdOrBranchId);
		
		BranchVO branch = branchIdBranchDetails.get(cityIdOrBranchId);
		BranchDetailVo detail = null;
		String managerName = null;

		if (branch != null)
		{
			detail = xformVo(branch);
			detail.setBranchManager(managerName);
			list.add(detail);
		} else
		{
			List<BranchVO> branchVos = getAllBranch();
			for (BranchVO vo : branchVos)
			{
				if (vo.getCityId() != null
						&& vo.getCityId().longValue() == cityIdOrBranchId)
				{
					detail = xformVo(vo);
					detail.setBranchManager(managerName);
					list.add(detail);
				}
			}
		}
		log.debug("Exit from getAllBranchDetail : "+cityIdOrBranchId);
		return list;
	}

	private BranchDetailVo xformVo(BranchVO branch)
	{
		BranchDetailVo vo = new BranchDetailVo();
		
		if(branch == null )
			return vo;
		
		log.info("Inside xformVo : " + branch.getBranchName());
		
		vo.setBranchCode(branch.getBranchCode());
		vo.setCityCode(branch.getCityCode());
		vo.setCityName(branch.getCityName());
		vo.setCityId(branch.getCityId());
		vo.setBranchName(branch.getBranchName());
		vo.setFlowSwitch(branch.getFlowSwitch());
		vo.setId(branch.getId());
		
		log.debug("Exit from xformVo : " + branch.getBranchName());
		return vo;
	}
	@Override
	public void updateBranchCache(Branch branch)
	{
		if (branch != null)
		{
			BranchVO branchVO = new BranchVO();
			BeanUtils.copyProperties(branch, branchVO);

			City city = branch.getCity();
			if (city != null)
			{
				branchVO.setCityId(city.getId());
				branchVO.setCityCode(city.getCityCode());
				branchVO.setCityName(city.getCityName());

				addToCache(branchVO);

			}

		}
	}
	
	@Override
	public Long getRealtyBranchByBranchNameAndCityName(String branchName , String cityName)
	{
		if( branchName == null || branchName.isEmpty())
			return null ;
		
		if( cityName == null || cityName.isEmpty())
			return null;
		
		if( cityDAO.getCityIdByName(cityName.toUpperCase()) == null)
			return null;
		String key = cityName+"_"+branchName;
		
		if(branchNameAndIdMap.containsKey(key))
		{
			return branchNameAndIdMap.get(key);
		}
		
		log.debug(" Inside getting branch by branchName : "+branchName);

		try {
			List<Branch> branches = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select * from Branch where branchName = :branchName and cityId = :cityId")
					.addEntity(Branch.class).setParameter("branchName", branchName)
					.setParameter("cityId", cityDAO
							.getCityIdByName(cityName.toUpperCase()))
					.list();
			if (branches != null && !(branches.isEmpty()))
			{
				Long id = branches.get(0).getId();
				branchNameAndIdMap.put(key, id);
				return id;
			}
		} catch (Exception e) {
			log.error("Error occured while getting branchName by branchName :"+branchName,e);
		}
		
		log.debug(" Exit From getting branch by branchName : "+branchName);
		
		return null;

		/*
		 * if(branchIdBranchDetails.size() == 0) getBranches();
		 * 
		 * List<Branch> branchList = new
		 * ArrayList<Branch>(branchIdBranchDetails.values());
		 * 
		 * for (Branch branch : branchList) {
		 * 
		 * if(branch.getBranchName().equalsIgnoreCase(branchName)){ log.debug(
		 * "branch found "+branch.getBranchName()); return branch; } else
		 * log.debug("branch not found "+branchName); } return null;
		 */
	}
}
