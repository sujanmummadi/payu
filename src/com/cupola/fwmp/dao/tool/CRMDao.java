/**
 * 
 */
package com.cupola.fwmp.dao.tool;

import java.util.List;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.tools.TransactionHistoryLogger;
import com.cupola.fwmp.vo.tools.siebel.to.vo.FaultRepairActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.CreateUpdateFaultRepairRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.CreateUpdateWorkOrderRequestInSiebel;

/**
 * @author aditya
 *
 */
public interface CRMDao {
	
//	String PROSPECT_QUERY_FOR_SIEBEL_FORMAT = "select " + "c.adhaarId as aadharNumber,"
//			+ "t.cafNumber as  actCAFSerialNumber," + "c.existingISP as  existingISP,"
//			+ "t.CxPermission as actcxPermission," +
//			// "t. as actDescription, " +
//			"t.GxPermission as actGXPermission," + "//t. as actionName,"
//			+ "c.alternativeMobileNo as alternateMobileNumber," + "c.mobileNumber as mobileNumber," +
//			// "t. as companyName, " +
//			"gis.cxIpAddress as cxIP," + "gis.cxPorts as cxPort," + "c.emailId as emailId,"
//			+ "c.alternativeEmailId as alternateEmailId, " + "c.firstName as firstName," + "gis.fxName as fxName,"
//			+ "gis.fxPorts as fxPort," + "c.alternativeMobileNo as altMobileNumber," + "t.source as source ,"
//			+ "c.middleName as middleName," + "c.remarks as notes," + "tar.planCode as plancode," +
//			// "t. as planName," +
//			"t.preferedDate as preferredCallDate," +
//			// "t. as productType," +
//			"t.currentAssignedTo as currentAssignedTO," + "t.prospectNo as propsectNo," + "t.reason as reason,"
//			+ "c.referralCustomerAccountNo as crpAccountNo," + "c.referralCustomerMobileNo as crpMobileNo,"
//			+ "//t. as routerModelCode," + "pay.schemeCode as schemeCode," +
//			// "t. as sourceType," +
//			"t.status as status, " + "subarea.subAreaName as subArea," + "t.subAttribute as subAttribute," +
//			// "t. as subSource," +
//			"c.title as title," +
//			// "t. as transactionId," +
//			"t.uin as UIN, " + "t.uinType as UINType, " +
//			// "c. as wifiRequired," +
//			"c.currentAddress as currentAddress," + "c.communicationAdderss as communicationAddress,"
//			+ "c.permanentAddress as permanentAddress "
//			+ "from Ticket t  join Customer c on t.customerId=c.id inner join TariffPlan tar on c.tariffId=tar.id inner join Payment pay on t.idPayment=pay.id inner join Gis gis  on gis.TicketNo=t.id inner join SubArea subarea on subarea.id=t.subAreaId where t.id = ?";

// changes done on query by manjuprasad for getting known source
/*	String PROSPECT_QUERY_FOR_SIEBEL_FORMAT = "select c.adhaarId as aadharNumber,t.cafNumber as  actCAFSerialNumber,tal.activityId as MilestoneStage,"
			+ "c.existingISP as  existingISP,c.status as ProspectType,c.nationality as Nationality,t.CxPermission as actcxPermission,t.GxPermission as actGXPermission, "
			+ "c.alternativeMobileNo as alternateMobileNumber,  c.mobileNumber as mobileNumber, gis.cxIpAddress as cxIP,gis.connectionType as ConnectionType, gis.cxPorts as cxPort, gis.lattitude as Latitude ,gis.longitude as Longitude,  "
			+ "c.emailId as emailId,c.alternativeEmailId as alternateEmailId,   c.firstName as firstName,c.lastName as LastName,c.cityId as cityId,c.branchId as branchId,c.areaId as areaId,gis.fxName as fxName,gis.fxPorts as fxPort,"
			+ "c.alternativeMobileNo as altMobileNumber,t.source as source ,c.middleName as middleName,  c.remarks as notes,c.CustomerProfession as profession,c.externalRouterRequired as externalRouterRequired,  tar.planCode as "
			+ "plancode, tar.packageName as planName, t.preferedDate as preferredCallDate,t.currentAssignedTo as currentAssignedTO,  t.prospectNo as propsectNo,"
			+ "t.reason as reason,c.referralCustomerAccountNo as crpAccountNo,  c.referralCustomerMobileNo as crpMobileNo,pay.schemeCode as schemeCode,"
			+ "t.status as status,   subarea.subAreaName as subArea,  t.subAttribute as subAttribute,c.title as title,t.uin as UIN,t.uinType as UINType,"
			+ "c.currentAddress as currentAddress,c.communicationAdderss as communicationAddress,c.permanentAddress as permanentAddress,c.knownSource,t.paymentStatus ,t.poiStatus,t.poaStatus "
			+ "from Ticket t inner join Customer c on t.customerId=c.id left join TariffPlan tar on c.tariffId=tar.id "
			+ "left outer join Payment pay on t.idPayment=pay.id left outer join Gis gis  on gis.TicketNo=t.id left outer join "
			+ "SubArea subarea on subarea.id=t.subAreaId left outer join TicketActivityLog tal on t.id=tal.ticketId where t.prospectNo=? order by tal.modifiedOn";*/

	String PROSPECT_QUERY_FOR_SIEBEL_FORMAT = "select c.adhaarId as aadharNumber,t.cafNumber as  actCAFSerialNumber,tal.activityId as MilestoneStage,"
			+ "c.existingISP as  existingISP,c.status as ProspectType,c.nationality as Nationality,t.CxPermission as actcxPermission,t.GxPermission as actGXPermission, "
			+ "c.alternativeMobileNo as alternateMobileNumber,  c.mobileNumber as mobileNumber, gis.cxIpAddress as cxIP,gis.connectionType as ConnectionType, gis.cxPorts as cxPort, gis.lattitude as Latitude ,gis.longitude as Longitude,  "
			+ "c.emailId as emailId,c.alternativeEmailId as alternateEmailId,   c.firstName as firstName,c.lastName as LastName,c.cityId as cityId,c.branchId as branchId,c.areaId as areaId,gis.fxName as fxName,gis.fxPorts as fxPort,"
			+ "c.alternativeMobileNo as altMobileNumber,t.source as source ,c.middleName as middleName,  c.remarks as notes,c.CustomerProfession as profession,c.externalRouterRequired as externalRouterRequired,  tar.planCode as "
			+ "plancode, tar.packageName as planName, t.preferedDate as preferredCallDate,t.currentAssignedTo as currentAssignedTO,  t.prospectNo as propsectNo,"
			+ "t.reason as reason,c.referralCustomerAccountNo as crpAccountNo,  c.referralCustomerMobileNo as crpMobileNo,pay.schemeCode as schemeCode,"
			+ "t.status as status,   subarea.subAreaName as subArea,  t.subAttribute as subAttribute,c.title as title,t.uin as UIN,t.uinType as UINType,"
			+ "c.currentAddress as currentAddress,c.communicationAdderss as communicationAddress,c.permanentAddress as permanentAddress,c.knownSource,t.paymentStatus ,t.poiStatus,t.poaStatus "
			+ "from Ticket t inner join Customer c on t.customerId=c.id left join TariffPlan tar on c.tariffId=tar.id "
			+ "left outer join Payment pay on t.idPayment=pay.id left outer join Gis gis  on gis.TicketNo=t.id and gis.id =(select gi.id from Gis gi where t.id = gi.TicketNo and connectionType is not null order by addedOn desc limit 1)"
			+ " left outer join "
			+ "SubArea subarea on subarea.id=t.subAreaId left outer join TicketActivityLog tal on t.id=tal.ticketId where t.prospectNo=? order by tal.modifiedOn";	
	
	String WORK_ORDER_QUERY_FOR_SIEBEL_FORMAT = "SELECT " + 
			"    w.workOrderNumber," + 
			"    t.currentAssignedTo AS AssignedTo," + 
			"    t.communicationETR AS ACTETR," + 
			"    p.escalationType AS PrioritySource," + 
			"    p.value AS PriorityValue," + 
			"    g.cxMacAddress AS ACTCxMACId," + 
			"    g.fxMacAddress AS ACTFxMACId," + 
			"    g.cxName AS ACTCxName," + 
			"	 t.status As Status,"+
			"    g.fxName AS ACTFxName," + 
			"    g.fxPorts AS ACTFxPortNumber," + 
			"    g.cxPorts AS ACTCxPortNumber," + 
			"    g.fxIpAddress AS ACTFxIP," + 
			"	 tal.activityId as ActivityType,"+
			"    g.cxIpAddress AS ACTCxIP," + 
			"    g.remarks AS CancellationReason," + 
			"    g.connectionType AS ACTConnectionType," + 
			"    (select tm.serialNo from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1025) as ACTGxId," + 
			"	(select tm.startPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1001) as ACTCATFIVEStartReading," + 
			"	(select tm.endPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1001) as ACTCATFIVEEndReading," + 
			"	(select tm.startPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1002) as ACTFiberCableStartReading," + 
			"	(select tm.endPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1002) as ACTFiberCableEndReading " + 
			"FROM " + 
			"    WorkOrder w " + 
			"        JOIN " + 
			"    Ticket t ON t.id = w.ticketId " + 
			"        LEFT OUTER JOIN " + 
			"    TicketPriority p ON p.id = t.priorityId " + 
			"        JOIN " + 
			"    Gis g ON g.TicketNo = t.id " + 
			"    inner join TicketActivityLog tal on tal.ticketId=t.id "+
			"    where " + 
			"    w.workOrderNumber = ? order by tal.addedOn" ;

	//modified by manjuprasad for copper fiber isssue.
/*	String CREATE_UPDATE_SIEBEL_WORKORDER_QUERY="SELECT  w.workOrderNumber,t.currentAssignedTo AS AssignedTo,"
			+ "t.currentEtr AS ACTETR,"
			+ "t.status as Status,p.escalationType "
			+ "AS PrioritySource,p.value AS PriorityValue,tal.activityId as ActivityType,"
			+ "g.cxMacAddress AS ACTCxMACId,g.fxMacAddress AS "
			+ "ACTFxMACId,g.cxName AS ACTCxName,g.fxName AS"
			+ " ACTFxName,g.fxPorts AS ACTFxPortNumber,g.cxPorts "
			+ "AS ACTCxPortNumber,g.fxIpAddress AS ACTFxIP,g.cxIpAddress"
			+ " AS ACTCxIP,g.remarks AS CancellationReason,(select gi.connectionType from Gis gi where gi.TicketNo=t.id order by addedOn desc limit 1) "
			+ "AS ACTConnectionType,(select tm.serialNo from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1025) as ACTGxId,(select tm.startPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1001) as ACTCATFIVEStartReading,(select tm.endPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1001) as ACTCATFIVEEndReading,(select tm.startPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1002) as ACTFiberCableStartReading,(select tm.endPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1002) as ACTFiberCableEndReading ,"
			+ "(select tm.MACId from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1018) as ACTRouterMacId "
			+ "FROM WorkOrder w JOIN Ticket t ON t.id = w.ticketId LEFT OUTER JOIN TicketPriority "
			+ "p ON p.id = t.priorityId LEFT OUTER join Gis g ON g.TicketNo = t.id LEFT OUTER join TicketActivityLog "
			+ "tal on tal.ticketId=t.id where w.workOrderNumber=? order by tal.addedOn";*/
	
	String CREATE_UPDATE_SIEBEL_WORKORDER_QUERY="SELECT  w.workOrderNumber,t.currentAssignedTo AS AssignedTo,"
			+ "t.currentEtr AS ACTETR,"
			+ "t.status as Status,p.escalationType "
			+ "AS PrioritySource,p.value AS PriorityValue,tal.activityId as ActivityType,"
			+ "g.cxMacAddress AS ACTCxMACId,g.fxMacAddress AS "
			+ "ACTFxMACId,g.cxName AS ACTCxName,g.fxName AS"
			+ " ACTFxName,g.fxPorts AS ACTFxPortNumber,g.cxPorts "
			+ "AS ACTCxPortNumber,g.fxIpAddress AS ACTFxIP,g.cxIpAddress"
			+ " AS ACTCxIP,g.remarks AS CancellationReason,g.connectionType, "
			+ "AS ACTConnectionType,(select tm.serialNo from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1025) as ACTGxId,(select tm.startPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1001) as ACTCATFIVEStartReading,(select tm.endPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1001) as ACTCATFIVEEndReading,(select tm.startPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1002) as ACTFiberCableStartReading,(select tm.endPoint from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1002) as ACTFiberCableEndReading ,"
			+ "(select tm.MACId from TicketMaterialMapping tm where tm.ticketId = t.id and tm.materialId = 1018) as ACTRouterMacId "
			+ "FROM WorkOrder w JOIN Ticket t ON t.id = w.ticketId LEFT OUTER JOIN TicketPriority "
			+ "p ON p.id = t.priorityId LEFT OUTER join Gis g ON g.TicketNo = t.id and g.id=(select gi.id from Gis gi where t.id = gi.TicketNo and connectionType is not null order by addedOn desc limit 1) LEFT OUTER join TicketActivityLog "
			+ "tal on tal.ticketId=t.id where w.workOrderNumber=? order by tal.addedOn";
	
	//modified by manjuprasad for TicketDescription and voc issue	
	String FAULT_REPAIR_QUERY_FOR_SIEBEL_FORMAT = "select c.mqId as customerNumber,t.workOrderNumber as "
			+ "SrNumber,pr.escalationType as PrioritySource ,t.currentAssignedTo as Owner , "
			+ "t.source as TicketSource ,d.voc as VOC ,d.subCategory as Nature,d.natureCode as NatureCode , "
			+ "pr.value as Priority , t.status as Status  ,etr.communicationETR as ETR ,d.category "
			+ "as Category , t.defectCode as ResolutionCode , t.subDefectCode as SubResolutionCode,t.TicketDescriptionVoc from FrTicket t join TicketPriority pr on pr.id = t.priorityId join "
			+ "FrDetail d on d.ticketId = t.id left join FrTicketEtr etr on etr.ticketId = t.id inner join Customer c on c.id=t.customerId "
			+ "where t.id =?";
	
//	String FAULT_REPAIR_QUERY_FOR_SIEBEL_FORMAT = "select pr.escalationType as PrioritySource , t.currentAssignedTo as Owner , t.source as TicketSource , t.TicketDescriptionVoc as VOC , d.natureCode as Nature , pr.value as Priority , t.status as Status ,d.category as Category from FrTicket t join TicketPriority pr on pr.id = t.priorityId join FrDetail d on d.ticketId = t.id  where t.id =?" ;
	
	List<MQWorkorderVO> getAllOpenTicketFormCRM(String query, String cityName);

	CreateUpdateProspectRequestInSiebel getSalesSiebelFormat(SalesActivityUpdateInCRMVo prospect);

	CreateUpdateWorkOrderRequestInSiebel getWorkOrderSiebelFormat(WorkOrderActivityUpdateInCRMVo workOrder);

	CreateUpdateFaultRepairRequestInSiebel getFaultRepairsSiebelFormat(FaultRepairActivityUpdateInCRMVo faultRepair);

	/**
	 * @author aditya
	 * boolean
	 * @param transactionId
	 * @return
	 */
	boolean getTransactionById(String transactionId);

	/**@author aditya
	 * boolean
	 * @param transactionHistoryLogger
	 * @return
	 */
	boolean getTransactionById(TransactionHistoryLogger transactionHistoryLogger);
	/**@author aditya
	 * List<MQWorkorderVO>
	 * @param string
	 * @param cityName
	 * @return
	 */
	List<MQWorkorderVO> getAllOpenTicketFormCRMForFr(String string, String cityName);

}
