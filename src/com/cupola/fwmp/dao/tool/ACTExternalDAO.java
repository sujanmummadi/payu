 /**
 * @Author aditya  28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.tool;

import java.util.List;

import com.cupola.fwmp.vo.tools.dashboard.PerformaceStasInput;
import com.cupola.fwmp.vo.tools.dashboard.deployment.DeplPerformaceStasOutput;
import com.cupola.fwmp.vo.tools.dashboard.fr.FrPerformaceStasOutput;
import com.cupola.fwmp.vo.tools.dashboard.sales.SalesPerformaceStasOutput;

/**
 * @Author aditya  28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface ACTExternalDAO {
	

	List<FrPerformaceStasOutput> getAllFrPerformaceStats(PerformaceStasInput performaceStasInput);

	/**@author aditya
	 * List<PerformaceStasOutput>
	 * @param performaceStasInput
	 * @return
	 */
	List<SalesPerformaceStasOutput> getAllSalesPerformaceStats(PerformaceStasInput performaceStasInput);

	/**@author aditya
	 * List<PerformaceStasOutput>
	 * @param performaceStasInput
	 * @return
	 */
	List<DeplPerformaceStasOutput> getAllSdPerformaceStats(PerformaceStasInput performaceStasInput);
}
