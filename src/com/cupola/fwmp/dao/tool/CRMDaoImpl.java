/**
 * 
 */
package com.cupola.fwmp.dao.tool;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.FWMPConstant.CustomerType;
import com.cupola.fwmp.FWMPConstant.DocumentSubType;
import com.cupola.fwmp.FWMPConstant.FWMP_Flows.MileStoneStatus;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.mongo.documents.CustomerDocument;
import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.tools.CommonToolUtil;
import com.cupola.fwmp.vo.tools.TransactionHistoryLogger;
import com.cupola.fwmp.vo.tools.siebel.to.vo.FaultRepairActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.ConsolidatedFrVoForSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.faultRepair.CreateUpdateFaultRepairRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.ConsolidatedProspectVoForSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.prospect.CreateUpdateProspectRequestInSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.ConsolidatedWorkOrderVoForSiebel;
import com.cupola.fwmp.vo.tools.siebel.to.vo.workOrder.CreateUpdateWorkOrderRequestInSiebel;

/**
 * @author aditya
 *
 */
public class CRMDaoImpl implements CRMDao {

	private static Logger log = LogManager.getLogger(CRMDaoImpl.class.getName());

	// @Resource(name="mqHydjdbcTemplate")
	@Autowired
	@Qualifier("mqHydjdbcTemplate")
	JdbcTemplate mqHydjdbcTemplate;

	@Autowired
	@Qualifier("mqROIjdbcTemplate")
	JdbcTemplate mqROIjdbcTemplate;

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	UserDao userDao;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	TicketDAO ticketDao;
	
	@Autowired
	DBUtil dbUtil;
	
	//for static issue by manjuprasad
	@Autowired
	CommonToolUtil commonToolUtil;

	@Override
	public List<MQWorkorderVO> getAllOpenTicketFormCRM(String query, String cityName) {
		List<MQWorkorderVO> crmOpenTickets = null;

		if (query == null || query.isEmpty())
			return crmOpenTickets;

		log.info(" Get all the open ticket On demand for " + cityName + " using " + query);
		try {

			if (CityName.HYDERABAD_CITY.equalsIgnoreCase(cityName)) {
				log.info("########### Calling CRM HYD ########");

				crmOpenTickets = mqHydjdbcTemplate.query(query, new MqDAOImplMapper());

			} else {

				log.info("########### Calling CRM ROI ########");
				crmOpenTickets = mqROIjdbcTemplate.query(query, new MqDAOImplMapper());
			}

			log.info(" Got all the open ticket On demand for " + cityName
					+ " using " + query + " data is " + crmOpenTickets);

			List<MQWorkorderVO> mqOpenTickets = new ArrayList<MQWorkorderVO>();
			MQWorkorderVO mqWorkorderVO = null;

			for (Iterator<MQWorkorderVO> iterator = crmOpenTickets
					.iterator(); iterator.hasNext();)
			{
				mqWorkorderVO = (MQWorkorderVO) iterator.next();
				
				try 
				{
				if (mqWorkorderVO.getCityId() != null
						&& !"".equals(mqWorkorderVO.getCityId()))
					mqOpenTickets.add(mqWorkorderVO);
				
					log.info("Manual WO " + mqWorkorderVO.getWorkOrderNumber() + " MQ "
							+ mqWorkorderVO.getMqId() + " PROSPECT " + mqWorkorderVO.getProspectNumber()
							+ " MQ CREATED TIME " + mqWorkorderVO.getReqDate());

//					log.info("Manual WO Complete Object is " + mqWorkorderVO);
					
				} catch (Exception e) 
				{
					e.printStackTrace();
					log.error("Manual data Exception in processing data for " + mqWorkorderVO);
					continue;
				}
			}

			return mqOpenTickets;

		} catch (Exception e) {
			log.error("Exception in getting all the open ticket On demand for " + cityName + " using " + query + " "
					+ e.getMessage());
			e.printStackTrace();

		}
		log.debug(" exit from  Get all the open ticket On demand for " + cityName
				+ " using " + query);

		return crmOpenTickets;
	}

	class MqDAOImplMapper implements RowMapper<MQWorkorderVO> {
		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo) throws SQLException {
			MQWorkorderVO mqWorkorderVO = new MQWorkorderVO();

			try {
				Long areaId = null;
				if(resultSet.getString("area") != null)
					areaId = areaDAO.getIdByCode(resultSet.getString("area"));
				Long branchId = null;
				if(resultSet.getString("branch") != null)
					branchId = branchDAO.getBranchIdByName(resultSet
						.getString("branch").toUpperCase());

				Long cityId = null;
				if(resultSet.getString("city") != null)
				 cityId = cityDAO
						.getCityIdByName(resultSet.getString("city").toUpperCase());

				if (areaId != null)
					mqWorkorderVO.setAreaId(areaId + "");

				if (branchId != null)
					mqWorkorderVO.setBranchId(branchId + "");

				if (cityId != null)
					mqWorkorderVO.setCityId(cityId + "");

				if(resultSet.getString("COMMENTS") != null)
					mqWorkorderVO.setComment(resultSet.getString("COMMENTS"));
				
				if(resultSet.getString("COMMENT_CODE") != null)
					mqWorkorderVO
						.setCommentCode(resultSet.getString("COMMENT_CODE"));
				
				if(resultSet.getString("CUST_ADDRESS") != null)
					mqWorkorderVO
						.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));
				
				if(resultSet.getString("PHONE") != null)
					mqWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));
				
				if(resultSet.getString("CUST_NAME") != null)
					mqWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));
				
				if(resultSet.getString("CX_IP") != null)
					mqWorkorderVO.setCxIp(resultSet.getString("CX_IP"));
				
				if(resultSet.getString("FX_NAME") != null)
					mqWorkorderVO.setFxName(resultSet.getString("FX_NAME"));
				
				if(resultSet.getString("MQ_ID") != null)
					mqWorkorderVO.setMqId(resultSet.getString("MQ_ID"));
				
				if(resultSet.getString("PROSPECT_NO") != null)
					mqWorkorderVO
						.setProspectNumber(resultSet.getString("PROSPECT_NO"));
				
				if(resultSet.getString("REQ_DATE") != null)
					mqWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));
				
				if(resultSet.getString("RESP_STATUS") != null)
					mqWorkorderVO
						.setResponseStatus(resultSet.getString("RESP_STATUS"));
				
				if(resultSet.getString("SYMP_NAME") != null)
					mqWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));
				
				if(resultSet.getString("CAT_NAME") != null)
					mqWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));
				
				if(resultSet.getString("WO_NO") != null)
					mqWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));
				
				if(resultSet.getString("VOC") != null)
					mqWorkorderVO.setVoc(resultSet.getString("VOC"));
				
				Long assignedTo = null;
				
				if(mqWorkorderVO.getFxName() !=null)
					assignedTo = userDao
						.getUserIdByDevcieName(mqWorkorderVO.getFxName());
					mqWorkorderVO.setAssignedTo(assignedTo);

				
				String fullyQualifiedName = mqWorkorderVO.getCityId() + "_"
						+ mqWorkorderVO.getBranchId() + "_"
						+ mqWorkorderVO.getAreaId() + "_"
						+ mqWorkorderVO.getAssignedTo();

				mqWorkorderVO.setFullyQualifiedName(fullyQualifiedName);
				
				mqWorkorderVO.setTicketSource("OUTSIDE FWMP");
				
				log.info("Original content for Manual is city " + resultSet.getString("city") + " branch " + resultSet.getString("branch") 
				+" and area " +resultSet.getString("area") + " for WO " + resultSet.getString("WO_NO") 
				+ ", MQ " + resultSet.getString("MQ_ID"));
			} catch (Exception e) 
			{
				log.error("Error while preparing data from result set while iterating manual puller " + e.getMessage());
				e.printStackTrace();
			}
			
			return mqWorkorderVO;
			
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.tool.CRMDao#getSalesSiebelFormat(com.cupola.fwmp.vo.
	 * tools .siebel.to.vo.SalesActivityUpdateInCRMVo)
	 */
	@Override
	public CreateUpdateProspectRequestInSiebel getSalesSiebelFormat(SalesActivityUpdateInCRMVo prospect) {

		CreateUpdateProspectRequestInSiebel createUpdateProspectInSiebel = null;
		List<ConsolidatedProspectVoForSiebel> consolidatedProspectVoForSiebelList = null;

		try {
			log.info("prospect.getProspectNumber() in crmdaoimpl for consolidated value "
					+ prospect.getProspectNumber());
			String prospctNumber = null;
			if( prospect.getProspectNumber() == null || prospect.getProspectNumber().isEmpty()) {
				prospctNumber = ticketDao.getProspectNumberForTicket(prospect.getTicketId());
				log.info("Getting prospect number from DB :"+prospctNumber);
				prospect.setProspectNumber(prospctNumber);
			}

			consolidatedProspectVoForSiebelList = jdbcTemplate.query(PROSPECT_QUERY_FOR_SIEBEL_FORMAT,
					new Object[] { prospect.getProspectNumber() }, new SalesSiebelFormatMapper());

			log.info("consolidatedProspectVoForSiebelList in crm dao ::" + consolidatedProspectVoForSiebelList);
			if (consolidatedProspectVoForSiebelList != null && !consolidatedProspectVoForSiebelList.isEmpty()) {

				if (consolidatedProspectVoForSiebelList.size() > 0) {

					log.info("setting value in from consolidated data createUpdateProspectInSiebel "
							+ consolidatedProspectVoForSiebelList.get(consolidatedProspectVoForSiebelList.size()-1));
					createUpdateProspectInSiebel = getProspectRequestforSiebelFromConsolidatedObj(
							consolidatedProspectVoForSiebelList.get(consolidatedProspectVoForSiebelList.size()-1));
				}

			}

		} catch (Exception e) {

			log.error("Error while converting ticket " + prospect.getTicketId() + " and customer "
					+ prospect.getCustomerId() + " for branch " + prospect.getBranchId() + " of city "
					+ prospect.getCityId() + "to Siebel format " + e.getMessage());

			e.printStackTrace();
		}
		return createUpdateProspectInSiebel;
	}

	/**
	 * @author aditya CreateUpdateProspectRequestInSiebel
	 * @param consolidatedProspectVoForSiebel
	 * @return
	 */
	private CreateUpdateProspectRequestInSiebel getProspectRequestforSiebelFromConsolidatedObj(
			ConsolidatedProspectVoForSiebel consolidatedProspectVoForSiebel) {

		CreateUpdateProspectRequestInSiebel createUpdateProspectRequestInSiebel = commonToolUtil
				.convertConsolidatedProspectVo2ProspectRequestInSiebel(consolidatedProspectVoForSiebel);

		return createUpdateProspectRequestInSiebel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.tool.CRMDao#getWorkOrderSiebelFormat(com.cupola.fwmp.
	 * vo. tools.siebel.to.vo.WorkOrderActivityUpdateInCRMVo)
	 */
	@Override
	public CreateUpdateWorkOrderRequestInSiebel getWorkOrderSiebelFormat(WorkOrderActivityUpdateInCRMVo workOrder) {

		CreateUpdateWorkOrderRequestInSiebel createUpdateWorkOrderRequestInSiebel = null;

		List<ConsolidatedWorkOrderVoForSiebel> consolidatedWorkOrderVoForSiebellList = null;

		// **************

		try {
			consolidatedWorkOrderVoForSiebellList = jdbcTemplate.query(CREATE_UPDATE_SIEBEL_WORKORDER_QUERY,
					new Object[] { workOrder.getWorkOrderNumber() }, new WorkOrderSiebelFormatMapper());

			if (consolidatedWorkOrderVoForSiebellList != null && !consolidatedWorkOrderVoForSiebellList.isEmpty()) {

				log.debug("in CRMDaoImpl consolidatedProspectVoForSiebelList" + consolidatedWorkOrderVoForSiebellList.size());
				if (consolidatedWorkOrderVoForSiebellList.size() > 0) {
					log.info("passing consolidatedWorkorderVoForSiebelList for converting into siebel formate is ::"
							+ (consolidatedWorkOrderVoForSiebellList.size() - 1));
					createUpdateWorkOrderRequestInSiebel = getWorkOrderRequestforSiebelFromConsolidatedObj(
							consolidatedWorkOrderVoForSiebellList.get(consolidatedWorkOrderVoForSiebellList.size()-1));
				}

			}

		} catch (Exception e) {

			log.error("Error while converting ticket " + workOrder.getTicketId() + " workorder  Number "
					+ workOrder.getWorkOrderNumber() + " and customer " + workOrder.getCustomerId() + " for branch "
					+ workOrder.getBranchId() + " of city " + workOrder.getCityId() + "to Siebel format "
					+ e.getMessage());

			e.printStackTrace();
		}
		return createUpdateWorkOrderRequestInSiebel;
	}

	/**
	 * @author aditya CreateUpdateWorkOrderRequestInSiebel
	 * @param consolidatedWorkOrderVoForSiebel
	 * @return
	 */
	private CreateUpdateWorkOrderRequestInSiebel getWorkOrderRequestforSiebelFromConsolidatedObj(
			ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel) {

		CreateUpdateWorkOrderRequestInSiebel createUpdateWorkOrderRequestInSiebel = new CreateUpdateWorkOrderRequestInSiebel();

		createUpdateWorkOrderRequestInSiebel.setUpdateWorkOrder_Input(CommonToolUtil
				.convertConsolidatedWorkOrderVoForSiebel2UpdateWorkOrder_Input(consolidatedWorkOrderVoForSiebel));

		return createUpdateWorkOrderRequestInSiebel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.tool.CRMDao#getFaultRepairsSiebelFormat(com.cupola.
	 * fwmp. vo.tools.siebel.to.vo.SalesActivityUpdateInCRMVo)
	 */
	@Override
	public CreateUpdateFaultRepairRequestInSiebel getFaultRepairsSiebelFormat(
			FaultRepairActivityUpdateInCRMVo faultRepair) {
		log.info("Getting fr details from DB for work order " + faultRepair.getTicketId());
		log.info("Getting fr details from Database for work order for siebel data:: " + faultRepair);
		
		CreateUpdateFaultRepairRequestInSiebel createUpdateFaultRepairRequestInSiebel = null;

		List<ConsolidatedFrVoForSiebel> consolidatedFrVoForSiebelList = null;

		try {
			
			if(faultRepair.getWorkOrderNumber() != null && faultRepair.getTicketId() <= 0 )
				faultRepair.setTicketId(dbUtil.getFrTicketIdByWorkOrder(faultRepair.getWorkOrderNumber()));
				
			consolidatedFrVoForSiebelList = jdbcTemplate.query(FAULT_REPAIR_QUERY_FOR_SIEBEL_FORMAT,
					new Object[] { faultRepair.getTicketId() }, new FaultRepairSiebelFormatMapper());
			
			log.info("Fr details comes from database for convertion into siebel format is :"+consolidatedFrVoForSiebelList);
			
			log.info("Got fr details from DB for work order " + faultRepair.getTicketId() + " details "
					+ consolidatedFrVoForSiebelList.size());

			if (consolidatedFrVoForSiebelList != null && !consolidatedFrVoForSiebelList.isEmpty()) {

				if (consolidatedFrVoForSiebelList.size() > 0) {
					log.info("Converting data in Siebel format for work order " + faultRepair.getTicketId()
							+ " details " + consolidatedFrVoForSiebelList.get(consolidatedFrVoForSiebelList.size()-1));

					createUpdateFaultRepairRequestInSiebel = getFaultRepairRequestforSiebelFromConsolidatedObj(
							consolidatedFrVoForSiebelList.get(consolidatedFrVoForSiebelList.size()-1));

					log.info("Converted data in Siebel format for work order " + faultRepair.getTicketId() + " details "
							+ createUpdateFaultRepairRequestInSiebel);

				}

			}
		} catch (Exception e) {

			log.error("Error while converting ticket " + faultRepair.getTicketId() + " workorder  Number "
					+ faultRepair.getWorkOrderNumber() + " and customer " + faultRepair.getCustomerId() + " for branch "
					+ faultRepair.getBranchId() + " of city " + faultRepair.getCityId() + "to Siebel format "
					+ e.getMessage());

			e.printStackTrace();
		}
		return createUpdateFaultRepairRequestInSiebel;
	}

	/**
	 * @author aditya CreateUpdateFaultRepairRequestInSiebel
	 * @param consolidatedFrVoForSiebel
	 * @return
	 */

	private CreateUpdateFaultRepairRequestInSiebel getFaultRepairRequestforSiebelFromConsolidatedObj(
			ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel) {

		log.info("getFaultRepairRequestforSiebelFromConsolidatedObj req has been called in order to convert into siebel format:"+consolidatedFrVoForSiebel);
		
		CreateUpdateFaultRepairRequestInSiebel createUpdateFaultRepairRequestInSiebel = CommonToolUtil
				.convertConsolidatedFrVoForSiebel2CreateUpdateFaultRepairRequestInSiebel(consolidatedFrVoForSiebel);
		return createUpdateFaultRepairRequestInSiebel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.dao.tool.CRMDao#getTransactionById(java.lang.String)
	 */
	@Override
	public boolean getTransactionById(String transactionId) {

		boolean result = false;

		try {

			Long trnId = Long.valueOf(transactionId);
			String getTransactionQuery = "SELECT true FROM TransactionHistory WHERE transactionId = " + trnId;

			List<Boolean> sizeResult = jdbcTemplate.queryForList(getTransactionQuery, Boolean.class);

			if (sizeResult != null && !sizeResult.isEmpty())
				return true;
			else {
				String updateTransactionQuery = "insert into TransactionHistory values( " + trnId + ", "
						+ FWMPConstant.SYSTEM_ENGINE + ", now())";
				try {
					int insertResult = jdbcTemplate.update(updateTransactionQuery);
					if (insertResult > 0)
						return false;
					else
						return true;
				} catch (Exception e) {
					log.error("DB population failed for transactionId " + transactionId + " . " + e.getMessage());
					e.printStackTrace();
				}

			}

		} catch (Exception e) {
			log.error("Fetching record about transaction id " + transactionId + " failed. " + e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.tool.CRMDao#getTransactionById(com.cupola.fwmp.vo.
	 * tools.TransactionHistoryLogger)
	 */
	@Override
	public boolean getTransactionById(TransactionHistoryLogger transactionHistoryLogger) {

		boolean result = false;

		try {

			Query query = new Query();

			query.addCriteria(Criteria.where("transactionId").is(transactionHistoryLogger.getTransactionId()));

			TransactionHistoryLogger logger = mongoTemplate.findOne(query, TransactionHistoryLogger.class);
			if (logger != null)
				return true;
			else {
				try {
					mongoTemplate.save(transactionHistoryLogger);

					return false;

				} catch (Exception e) {
					log.error("DB population failed(Mongo DB) for transactionId "
							+ transactionHistoryLogger.getTransactionId() + " . " + e.getMessage());
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			log.error("Fetching record about transaction id " + transactionHistoryLogger.getTransactionId()
					+ " failed(Mongo DB). " + e.getMessage());
			e.printStackTrace();
		}

		return result;
	}


class SalesSiebelFormatMapper implements RowMapper<ConsolidatedProspectVoForSiebel> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */

	@Override
	public ConsolidatedProspectVoForSiebel mapRow(ResultSet resultSet, int arg1) throws SQLException {

		Log.info("SalesSiebelFormatMapper mapper method called for ConsolidatedProspectVoForSiebel");

		ConsolidatedProspectVoForSiebel consolidatedProspectVoForSiebel = new ConsolidatedProspectVoForSiebel();

		consolidatedProspectVoForSiebel.setAadharNumber(resultSet.getString("aadharNumber"));
		consolidatedProspectVoForSiebel.setACTCAFSerialNumber(resultSet.getString("actCAFSerialNumber"));
		consolidatedProspectVoForSiebel.setACTCurrentISP(resultSet.getString("existingISP"));
		if( resultSet.getString("actcxPermission") != null)
			consolidatedProspectVoForSiebel.setACTCxPermission(resultSet.getString("actcxPermission").equalsIgnoreCase( "true" )  ? "Yes" : "No" );
		// consolidatedProspectVoForSiebel.setActDescription(resultSet.getString("actDescription"));
		if( resultSet.getString("actGXPermission") != null)
			consolidatedProspectVoForSiebel.setACTGxRequired(resultSet.getString("actGXPermission").equalsIgnoreCase( "true" )  ? "Yes" : "No" );
		// consolidatedProspectVoForSiebel.setActionName(resultSet.getString("actionName"));
		consolidatedProspectVoForSiebel.setAlternateMobileNumber(resultSet.getString("alternateMobileNumber"));
		consolidatedProspectVoForSiebel.setCellularPhone(resultSet.getString("mobileNumber"));
		// consolidatedProspectVoForSiebel.setCompanyName("companyName");
		consolidatedProspectVoForSiebel.setCountry(FWMPConstant.Country_Name.INDIA);
		consolidatedProspectVoForSiebel.setCXIP(resultSet.getString("cxIP"));
		consolidatedProspectVoForSiebel.setCXPort(resultSet.getString("cxPort"));
		consolidatedProspectVoForSiebel.setEmailAddress(resultSet.getString("emailId"));
		consolidatedProspectVoForSiebel.setEmailAddress2(resultSet.getString("alternateEmailId"));
		consolidatedProspectVoForSiebel.setFirstName(resultSet.getString("firstName"));
		consolidatedProspectVoForSiebel.setLastName(resultSet.getString("LastName"));
		consolidatedProspectVoForSiebel.setFXName(resultSet.getString("fxName"));
		consolidatedProspectVoForSiebel.setFXPort(resultSet.getString("fxPort"));
		consolidatedProspectVoForSiebel.setHOMEPHNUM(resultSet.getString("altMobileNumber"));
		System.out.println("city:"+resultSet.getString("cityId"));
		System.out.println("KnownSoure "+resultSet.getString("knownSource"));
		// commented by manjuprasad for known source issue
//		consolidatedProspectVoForSiebel.setHowdidgettoknowaboutACT(resultSet.getString("source"));
//added by manjuprasad for resolving knownsource issue	
		String howdidyouknowaboutact="";
		String knownSourceValue=resultSet.getString("knownSource");
		System.out.println("KnownSoureValue "+knownSourceValue);
	    if(knownSourceValue.equalsIgnoreCase("null")) {
	    	howdidyouknowaboutact= definitionCoreService.getSourceById(Long.parseLong("434"), Long.parseLong(resultSet.getString("cityId")));
	    }else {
	    	howdidyouknowaboutact= definitionCoreService.getSourceById(Long.parseLong(knownSourceValue), Long.parseLong(resultSet.getString("cityId")));

	    }
	    
		consolidatedProspectVoForSiebel.setHowdidgettoknowaboutACT(howdidyouknowaboutact);
		
		consolidatedProspectVoForSiebel.setLineofBusiness(FWMPConstant.LineOfBusiness.INDIVIDUAL);
		consolidatedProspectVoForSiebel.setMiddleName(resultSet.getString("middleName"));
		consolidatedProspectVoForSiebel.setNotes(resultSet.getString("notes"));
		consolidatedProspectVoForSiebel.setPlanCode(resultSet.getString("plancode"));
		
		consolidatedProspectVoForSiebel.setPlanName(resultSet.getString("planName"));
		consolidatedProspectVoForSiebel.setConnectionType(resultSet.getString("ConnectionType"));
		consolidatedProspectVoForSiebel.setPreferredCallDateTime(resultSet.getString("preferredCallDate"));
		// consolidatedProspectVoForSiebel.setProductType(resultSet.getString("productType"));
		consolidatedProspectVoForSiebel.setProspectAssignedTo(resultSet.getString("currentAssignedTO"));
		consolidatedProspectVoForSiebel.setProspectNumber(resultSet.getString("propsectNo"));
		consolidatedProspectVoForSiebel.setProspectRejectionReason(resultSet.getString("reason"));
		consolidatedProspectVoForSiebel.setRefereeAccountNumber(resultSet.getString("crpAccountNo"));
		consolidatedProspectVoForSiebel.setRefereeMobileNumber(resultSet.getString("crpMobileNo"));
		// consolidatedProspectVoForSiebel.setRouterModelCode(resultSet.getString("routerModelCode"));
		consolidatedProspectVoForSiebel.setSchemeCode(resultSet.getString("schemeCode"));
		//AS per disscussed with Mohith
		// changing source by manjuprasad
//		consolidatedProspectVoForSiebel.setSourceType(FWMPConstant.TicketSource.DOOR_KNOCK);
		consolidatedProspectVoForSiebel.setSourceType(resultSet.getString("source"));
		consolidatedProspectVoForSiebel.setStatus(resultSet.getString("ProspectType"));
		consolidatedProspectVoForSiebel.setSubArea(resultSet.getString("subArea"));// take
																					// from
																					// Locationcache
		consolidatedProspectVoForSiebel.setSubAttribute(resultSet.getString("subAttribute"));
		// consolidatedProspectVoForSiebel.setSubSource(resultSet.getString("subSource"));
		
		consolidatedProspectVoForSiebel.setCity(resultSet.getString("cityId"));
		consolidatedProspectVoForSiebel.setArea(resultSet.getString("areaId"));
		consolidatedProspectVoForSiebel.setOperationalEntity(resultSet.getString("branchId"));
		
		consolidatedProspectVoForSiebel.setTitle(resultSet.getString("title"));
		// consolidatedProspectVoForSiebel.setTransation_spcId(resultSet.getString("transactionId"));
		consolidatedProspectVoForSiebel.setUIN(resultSet.getString("UIN"));
		consolidatedProspectVoForSiebel.setUINType(resultSet.getString("UINType"));
		// consolidatedProspectVoForSiebel.setWifiRequired(resultSet.getString("wifiRequired"));
		consolidatedProspectVoForSiebel.setCurrentAddress(resultSet.getString("currentAddress"));
		consolidatedProspectVoForSiebel.setCommunicationAddress(resultSet.getString("communicationAddress"));
		consolidatedProspectVoForSiebel.setPermanentAddress(resultSet.getString("permanentAddress"));
		consolidatedProspectVoForSiebel.setNationality(resultSet.getString("Nationality"));
		consolidatedProspectVoForSiebel.setMilestoneStage(resultSet.getString("MilestoneStage"));
		consolidatedProspectVoForSiebel.setCustomerProfile(resultSet.getString("profession"));
		consolidatedProspectVoForSiebel.setWifiRequired(resultSet.getString("externalRouterRequired"));
		
			if (consolidatedProspectVoForSiebel.getMilestoneStage() != null) {
				if (Long.valueOf(consolidatedProspectVoForSiebel.getMilestoneStage()) == Activity.PAYMENT_UPDATE) {
					int status = resultSet.getInt("paymentStatus");

					if (status == TicketStatus.PAYMENT_APPROVED) {
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.APPROVED);
					} else if (status == TicketStatus.PAYMENT_REJECTED) {
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.REJECTED);
					} else if ( status == TicketStatus.PAYMENT_RECEIVED){
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.UPLOADED);
					}
				}

				else if (Long.valueOf(consolidatedProspectVoForSiebel.getMilestoneStage()) == Activity.POA_DOCUMENT_UPDATE) {
					
					int status = resultSet.getInt("poaStatus");

					if (status == TicketStatus.POA_DOCUMENTS_APPROVED) {
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.APPROVED);
					} else if (status == TicketStatus.POA_DOCUMENTS_REJECTED) {
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.REJECTED);
					} else if(status == TicketStatus.POA_DOCUMENTS_UPLOADED){
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.UPLOADED);
					}
					try {
						long ticketId = ticketDao.getTicketIdByProspectNumber(consolidatedProspectVoForSiebel.getProspectNumber());
						List<CustomerDocument> documents = mongoTemplate.find(new Query().addCriteria(Criteria.where("ticketId")
						.is(ticketId)).with(new Sort(new Order(Direction.DESC,"modifiedOn"))),CustomerDocument.class);
						if( documents != null && documents.size() > 0) {
							String uinType = DocumentSubType.DOCUMENT_SUBTYPE_VALUE_STRING.get(documents.get(0).getDocSubType());
							consolidatedProspectVoForSiebel.setUINType(uinType);
						}
					} catch (Exception e) {
						log.info("Exception while setting uinType in POA");
					}
				}

				else if (Long.valueOf(consolidatedProspectVoForSiebel.getMilestoneStage()) == Activity.POI_DOCUMENT_UPDATE) {
					
					int status = resultSet.getInt("poiStatus");

					if (status == TicketStatus.POI_DOCUMENTS_APPROVED) {
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.APPROVED);

					} else if (status == TicketStatus.POI_DOCUMENTS_REJECTED) {
						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.REJECTED);

					} else if( status == TicketStatus.POI_DOCUMENTS_UPLOADED ){

						consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.UPLOADED);
					}
				} else {

					consolidatedProspectVoForSiebel.setMileStoneStatus(MileStoneStatus.COMPLETED);
				}

			}
			
			String branchName = LocationCache.getBrancheNameById(Long.valueOf(consolidatedProspectVoForSiebel.getOperationalEntity()));
			if(branchName != null)
			{
				if(branchName.equalsIgnoreCase(FWMPConstant.REALTY))
					consolidatedProspectVoForSiebel.setCustomerType(CustomerType.COMMUNITY);
				else
					consolidatedProspectVoForSiebel.setCustomerType(CustomerType.RETAIL);
			}
			
			consolidatedProspectVoForSiebel.setLatitude(resultSet.getString("Latitude"));
			consolidatedProspectVoForSiebel.setLongitude(resultSet.getString("Longitude"));
		
		Log.info("Returning value to consolidatedProspectVoForSiebelList of consolidatedProspectVoForSiebel"
				+ consolidatedProspectVoForSiebel);
		return consolidatedProspectVoForSiebel;
	}
}

class WorkOrderSiebelFormatMapper implements RowMapper<ConsolidatedWorkOrderVoForSiebel> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */

	@Override
	public ConsolidatedWorkOrderVoForSiebel mapRow(ResultSet resultSet, int arg1) throws SQLException {

		ConsolidatedWorkOrderVoForSiebel consolidatedWorkOrderVoForSiebel = new ConsolidatedWorkOrderVoForSiebel();

		consolidatedWorkOrderVoForSiebel.setWorkOrderNumber(resultSet.getString("workOrderNumber"));
		consolidatedWorkOrderVoForSiebel.setAssignedTo(resultSet.getString("AssignedTo"));
		consolidatedWorkOrderVoForSiebel.setACTETR(resultSet.getString("ACTETR"));
		consolidatedWorkOrderVoForSiebel.setPrioritySource(resultSet.getString("PrioritySource"));
		consolidatedWorkOrderVoForSiebel.setPriorityValue(resultSet.getString("PriorityValue"));
		consolidatedWorkOrderVoForSiebel.setACTCxMACId(resultSet.getString("ACTCxMACId"));
		consolidatedWorkOrderVoForSiebel.setACTFxMACId(resultSet.getString("ACTFxMACId"));
		consolidatedWorkOrderVoForSiebel.setACTCxName(resultSet.getString("ACTCxName"));
		consolidatedWorkOrderVoForSiebel.setACTFxName(resultSet.getString("ACTFxName"));
		consolidatedWorkOrderVoForSiebel.setACTFxPortNumber(resultSet.getString("ACTFxPortNumber"));
		consolidatedWorkOrderVoForSiebel.setACTCxPortNumber(resultSet.getString("ACTCxPortNumber"));
		consolidatedWorkOrderVoForSiebel.setACTFxIP(resultSet.getString("ACTFxIP"));
		consolidatedWorkOrderVoForSiebel.setACTCxIP(resultSet.getString("ACTCxIP"));
		consolidatedWorkOrderVoForSiebel.setCancellationReason(resultSet.getString("CancellationReason"));
		consolidatedWorkOrderVoForSiebel.setACTConnectionType(resultSet.getString("ACTConnectionType"));
		consolidatedWorkOrderVoForSiebel.setACTGxId(resultSet.getString("ACTGxId"));
		consolidatedWorkOrderVoForSiebel.setACTCATFIVEStartReading(resultSet.getString("ACTCATFIVEStartReading"));
		consolidatedWorkOrderVoForSiebel.setACTCATFIVEEndReading(resultSet.getString("ACTCATFIVEEndReading"));
		consolidatedWorkOrderVoForSiebel.setACTFiberCableStartReading(resultSet.getString("ACTFiberCableStartReading"));
		consolidatedWorkOrderVoForSiebel.setACTFiberCableEndReading(resultSet.getString("ACTFiberCableEndReading"));
		consolidatedWorkOrderVoForSiebel.setStatus(resultSet.getString("Status"));
		consolidatedWorkOrderVoForSiebel.setActivityType(resultSet.getString("ActivityType"));
		consolidatedWorkOrderVoForSiebel.setACTRouterMacId(resultSet.getString("ACTRouterMacId"));
		return consolidatedWorkOrderVoForSiebel;
	}

}

class FaultRepairSiebelFormatMapper implements RowMapper<ConsolidatedFrVoForSiebel> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 * int)
	 */

	@Override
	public ConsolidatedFrVoForSiebel mapRow(ResultSet resultSet, int arg1) throws SQLException {

		ConsolidatedFrVoForSiebel consolidatedFrVoForSiebel = new ConsolidatedFrVoForSiebel();

		consolidatedFrVoForSiebel.setPrioritySource(resultSet.getString("PrioritySource"));
		consolidatedFrVoForSiebel.setOwner(resultSet.getString("Owner"));
		consolidatedFrVoForSiebel.setTicketSource(resultSet.getString("TicketSource"));
		consolidatedFrVoForSiebel.setVOC(resultSet.getString("VOC"));
		consolidatedFrVoForSiebel.setNature(resultSet.getString("Nature"));
		consolidatedFrVoForSiebel.setPriority(resultSet.getString("Priority"));
		consolidatedFrVoForSiebel.setStatus(resultSet.getString("Status"));
		consolidatedFrVoForSiebel.setETR(resultSet.getString("ETR"));
		consolidatedFrVoForSiebel.setCategory(resultSet.getString("Category"));
		consolidatedFrVoForSiebel.setCustomerNumber(resultSet.getString("customerNumber"));
		consolidatedFrVoForSiebel.setSRNumber(resultSet.getString("SrNumber"));
		consolidatedFrVoForSiebel.setResolutionCode(resultSet.getString("ResolutionCode"));
		consolidatedFrVoForSiebel.setSubResolutionCode(resultSet.getString("SubResolutionCode"));
		//added by manjuprasad for TicketDescription and voc issue
		consolidatedFrVoForSiebel.setTicketDescription(resultSet.getString("TicketDescriptionVoc"));
		return consolidatedFrVoForSiebel;
	}

}

class ROIMqFrMapper implements RowMapper<MQWorkorderVO> {
	@Override
	public MQWorkorderVO mapRow(ResultSet resultSet, int rowNo) throws SQLException {
		MQWorkorderVO roiMQWorkorderVO = new MQWorkorderVO();

		try {
			log.info("Getting area " + resultSet.getString("area") + " for " + resultSet.getString("WO_NO"));
			Long areaId = areaDAO.getIdByCode(resultSet.getString("area"));
			log.info("Got area for " + resultSet.getString("area") + " for " + resultSet.getString("WO_NO"));

			Long branchId = null;
			log.info("Getting branch " + resultSet.getString("branch") + " for " + resultSet.getString("WO_NO"));
			if (resultSet.getString("branch") != null) {
				if (resultSet.getString("branch").equalsIgnoreCase(FWMPConstant.REALTY)) {

					branchId = branchDAO.getRealtyBranchByBranchNameAndCityName(
							resultSet.getString("branch").toUpperCase().trim(),
							resultSet.getString("city").toUpperCase());
				} else {

					branchId = branchDAO.getBranchIdByName(resultSet.getString("branch").toUpperCase());
				}

			}

			log.info("Got branch " + resultSet.getString("branch") + " for " + resultSet.getString("WO_NO"));

			log.info("Getting city " + resultSet.getString("city") + " for " + resultSet.getString("WO_NO"));
			Long cityId = cityDAO.getCityIdByName(resultSet.getString("city").toUpperCase());
			log.info("Got city" + resultSet.getString("city") + " for " + resultSet.getString("WO_NO"));

			if (areaId != null)
				roiMQWorkorderVO.setAreaId(areaId + "");

			if (branchId != null)
				roiMQWorkorderVO.setBranchId(branchId + "");

			if (cityId != null)
				roiMQWorkorderVO.setCityId(cityId + "");

			roiMQWorkorderVO.setComment(resultSet.getString("COMMENTS"));
			roiMQWorkorderVO.setCommentCode(resultSet.getString("COMMENT_CODE"));
			roiMQWorkorderVO.setCustomerAddress(resultSet.getString("CUST_ADDRESS"));
			roiMQWorkorderVO.setCustomerMobile(resultSet.getString("PHONE"));
			roiMQWorkorderVO.setCustomerName(resultSet.getString("CUST_NAME"));
			roiMQWorkorderVO.setCxIp(resultSet.getString("CX_IP"));
			roiMQWorkorderVO.setFxName(resultSet.getString("FX_NAME"));
			roiMQWorkorderVO.setMqId(resultSet.getString("MQ_ID"));
			roiMQWorkorderVO.setProspectNumber(resultSet.getString("PROSPECT_NO"));
			roiMQWorkorderVO.setReqDate(resultSet.getString("REQ_DATE"));
			roiMQWorkorderVO.setResponseStatus(resultSet.getString("RESP_STATUS"));
			roiMQWorkorderVO.setSymptom(resultSet.getString("SYMP_NAME"));
			roiMQWorkorderVO.setTicketCategory(resultSet.getString("CAT_NAME"));
			roiMQWorkorderVO.setWorkOrderNumber(resultSet.getString("WO_NO"));
			roiMQWorkorderVO.setVoc(resultSet.getString("VOC"));
			roiMQWorkorderVO.setModifiedDate(resultSet.getString("MODIFIED_DATE"));
			
			FrDeviceVo device = new FrDeviceVo();
			device.setFxName(roiMQWorkorderVO.getFxName());

			String cxIp = roiMQWorkorderVO.getCxIp();
			if (cxIp != null && !cxIp.isEmpty() && cxIp.contains("/")) {
				try {
					String[] ipAndPort = cxIp.split("/");

					if (ipAndPort != null && ipAndPort.length > 0)
						device.setCxIp(ipAndPort[0]);

					if (ipAndPort != null && ipAndPort.length > 1)
						device.setCxPort(Integer.valueOf(ipAndPort[1]));
				} catch (NumberFormatException e) {
					device.setCxPort(0);
				}

			} else {
				device.setCxIp(cxIp);
				device.setCxPort(0);
			}

			roiMQWorkorderVO.setDeviceData(device);
			String fullyQualifiedName = roiMQWorkorderVO.getCityId() + "_" + roiMQWorkorderVO.getBranchId() + "_"
					+ roiMQWorkorderVO.getAreaId() + "_" + roiMQWorkorderVO.getAssignedTo();

			roiMQWorkorderVO.setFullyQualifiedName(fullyQualifiedName);

			log.info("Original content for ROI Manually pulled Mapper is city " + resultSet.getString("city") + " branch "
					+ resultSet.getString("branch") + " and area " + resultSet.getString("area") + " for WO "
					+ resultSet.getString("WO_NO") + ", MQ " + resultSet.getString("MQ_ID") + " Created on  "
					+ resultSet.getString("REQ_DATE"));
			log.info("FR data ###### Test " + roiMQWorkorderVO.toString());

		} catch (Exception e) {
			log.error("Error in fr row mapper " + e.getMessage());
			e.printStackTrace();
		}

		return roiMQWorkorderVO;
	}

}


@Override
public List<MQWorkorderVO> getAllOpenTicketFormCRMForFr(String query, String cityName) {


	List<MQWorkorderVO> crmOpenTickets = null;
	
	if(query == null || query.isEmpty() )
		return crmOpenTickets;
	
		log.info(" Get all the open ticket On demand for FR " + cityName
				+ " using " + query);
	try
	{

		if (CityName.HYDERABAD_CITY.equalsIgnoreCase(cityName))
		{
			log.info("########### Calling CRM HYD For FR ########");

			crmOpenTickets = mqHydjdbcTemplate
					.query(query, new ROIMqFrMapper());

		} else
		{

			log.info("########### Calling CRM ROI For FR########");
			crmOpenTickets = mqROIjdbcTemplate
					.query(query, new ROIMqFrMapper());
		}

		log.info(" Got all the open ticket On demand for fr " + cityName
				+ " using " + query + " data is " + crmOpenTickets);

		List<MQWorkorderVO> mqOpenTickets = new ArrayList<MQWorkorderVO>();
		MQWorkorderVO mqWorkorderVO = null;

		for (Iterator<MQWorkorderVO> iterator = crmOpenTickets
				.iterator(); iterator.hasNext();)
		{
			mqWorkorderVO = (MQWorkorderVO) iterator.next();
			
			try 
			{
			if (mqWorkorderVO.getCityId() != null
					&& !"".equals(mqWorkorderVO.getCityId()))
				mqOpenTickets.add(mqWorkorderVO);
			
				log.info("Manual FR WO " + mqWorkorderVO.getWorkOrderNumber() + " MQ "
						+ mqWorkorderVO.getMqId() + " PROSPECT " + mqWorkorderVO.getProspectNumber()
						+ " MQ CREATED TIME " + mqWorkorderVO.getReqDate());

//				log.info("Manual WO Complete Object is " + mqWorkorderVO);
				
			} catch (Exception e) 
			{
				e.printStackTrace();
				log.error("Manual data Exception in processing data for Fr " + mqWorkorderVO);
				continue;
			}
		}

		return mqOpenTickets;

	} catch (Exception e)
	{
		log.error("Exception in getting all the open ticket On demand for FR "
				+ cityName + " using " + query + " " + e.getMessage());
		e.printStackTrace();

	}
	log.debug(" exit from  Get all the open ticket On demand for FR " + cityName
			+ " using " + query);
	
	return crmOpenTickets;

}
}
