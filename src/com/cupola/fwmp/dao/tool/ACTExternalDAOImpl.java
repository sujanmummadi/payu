/**
* @Author aditya  28-Dec-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.dao.tool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.user.UserDaoImpl;
import com.cupola.fwmp.persistance.entities.DeplDashboard;
import com.cupola.fwmp.persistance.entities.FrDashboard;
import com.cupola.fwmp.persistance.entities.SalesDashboard;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.tools.dashboard.PerformaceStasInput;
import com.cupola.fwmp.vo.tools.dashboard.deployment.DeplPerformaceStasOutput;
import com.cupola.fwmp.vo.tools.dashboard.fr.FrPerformaceStasOutput;
import com.cupola.fwmp.vo.tools.dashboard.sales.SalesPerformaceStasOutput;

/**
 * @Author aditya 28-Dec-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class ACTExternalDAOImpl implements ACTExternalDAO {

	private static Logger log = LogManager.getLogger(ACTExternalDAOImpl.class.getName());

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.tool.ACTExternalDAO#getAllPerformaceStas(com.cupola.fwmp.
	 * vo.tools.PerformaceStasInput)
	 */
	@Override
	@Transactional
	public List<FrPerformaceStasOutput> getAllFrPerformaceStats(PerformaceStasInput performaceStasInput) {
		List<FrPerformaceStasOutput> performaceStatsOutputs = new ArrayList<>();

		try {
			log.info(" Getting performance stats for " + performaceStasInput.getLoginId());

			Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			Criteria criteria = session.createCriteria(FrDashboard.class);

			if (performaceStasInput.getCurrentRole() == null) {
				if (AuthUtils.getIfExecutiveRole() == UserRole.FR_CM)
					criteria.add(Restrictions.eq("cmId", performaceStasInput.getLoginId()));

				else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_BM)
					criteria.add(Restrictions.eq("bmId", performaceStasInput.getLoginId()));

				else if (AuthUtils.getIfExecutiveRole() == UserRole.FR_AM)
					criteria.add(Restrictions.eq("amId", performaceStasInput.getLoginId()));

				else if (AuthUtils.isFRTLUser())
					criteria.add(Restrictions.eq("tlId", performaceStasInput.getLoginId()));
				else
					criteria.add(Restrictions.eq("loginid", performaceStasInput.getLoginId()));
			} else {
				if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_CM) {
					log.info(" inside CM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("cmId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_BM) {
					log.info(" inside BM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("bmId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_AM) {
					log.info(" inside AM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("amId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_TL) {
					log.info(" inside TL role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("tlId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.FR_NE) {
					log.info(" inside NE role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("loginid", performaceStasInput.getLoginId()));
				}
			}
			List<FrDashboard> frDashboards = criteria.list();

			log.info(" Got performance stats form DB " + performaceStasInput.getLoginId() + " performace stats from db "
					+ frDashboards.size());

			for (Iterator<FrDashboard> iterator = frDashboards.iterator(); iterator.hasNext();) {

				FrDashboard frDashboard = (FrDashboard) iterator.next();

				FrPerformaceStasOutput performaceStasOutput = new FrPerformaceStasOutput();

				BeanUtils.copyProperties(frDashboard, performaceStasOutput);
				
				
				performaceStatsOutputs.add(performaceStasOutput);

			}

			log.info(" Got performance stats for " + performaceStasInput.getLoginId() + " performace Stats Outputs "
					+ performaceStatsOutputs.size());

		} catch (Exception e) {
			log.error(" Got exception, while getting performance stats for " + performaceStasInput.getLoginId());
			e.printStackTrace();
		}

		return performaceStatsOutputs;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.tool.ACTExternalDAO#getAllSalesPerformaceStats(com.cupola
	 * .fwmp.vo.tools.PerformaceStasInput)
	 */
	@Override
	@Transactional
	public List<SalesPerformaceStasOutput> getAllSalesPerformaceStats(PerformaceStasInput performaceStasInput) {
		List<SalesPerformaceStasOutput> performaceStatsOutputs = new ArrayList<>();

		try {
			log.info(AuthUtils.getIfExecutiveRole() + " Getting performance stats sales for "
					+ performaceStasInput.getLoginId());

			Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			Criteria criteria = session.createCriteria(SalesDashboard.class);
			if (performaceStasInput.getCurrentRole() == null) {
				if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_CM)
					criteria.add(Restrictions.eq("cmId", performaceStasInput.getLoginId()));

				else if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_BM)
					criteria.add(Restrictions.eq("bmId", performaceStasInput.getLoginId()));

				else if (AuthUtils.getIfExecutiveRole() == UserRole.SALES_AM)
					criteria.add(Restrictions.eq("amId", performaceStasInput.getLoginId()));

				else if (AuthUtils.isSalesTlUser())
					criteria.add(Restrictions.eq("tlId", performaceStasInput.getLoginId()));
				else
					criteria.add(Restrictions.eq("loginid", performaceStasInput.getLoginId()));
			} else {
				if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_CM) {
					log.info(" inside SALES_CM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("cmId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_BM) {
					log.info(" inside SALES_BM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("bmId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_AM) {
					log.info(" inside SALES_AM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("amId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_TL) {
					log.info(" inside SALES_TL role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("tlId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.SALES_EX) {
					log.info(" inside SALES_EX role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("loginid", performaceStasInput.getLoginId()));
				}
			}
			List<SalesDashboard> salesDashboards = criteria.list();

			log.info(" Got performance stats sales form DB " + performaceStasInput.getLoginId()
					+ " performace stats from db " + salesDashboards.size());

			for (Iterator<SalesDashboard> iterator = salesDashboards.iterator(); iterator.hasNext();) {

				SalesDashboard salesDashboard = (SalesDashboard) iterator.next();

				SalesPerformaceStasOutput performaceStasOutput = new SalesPerformaceStasOutput();

				BeanUtils.copyProperties(salesDashboard, performaceStasOutput);

				
				performaceStatsOutputs.add(performaceStasOutput);

			}

			log.info(" Got performance stats sales for " + performaceStasInput.getLoginId()
					+ " performace Stats Outputs " + performaceStatsOutputs.size());

		} catch (Exception e) {
			log.error(" Got exception, while getting performance stats sales for " + performaceStasInput.getLoginId());
			e.printStackTrace();
		}

		return performaceStatsOutputs;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.tool.ACTExternalDAO#getAllSdPerformaceStats(com.cupola.
	 * fwmp.vo.tools.PerformaceStasInput)
	 */
	@Override
	@Transactional
	public List<DeplPerformaceStasOutput> getAllSdPerformaceStats(PerformaceStasInput performaceStasInput) {

		List<DeplPerformaceStasOutput> performaceStatsOutputs = new ArrayList<>();

		try {

			log.info(AuthUtils.getIfExecutiveRole() + " Getting performance stats for delp "
					+ performaceStasInput.getLoginId());

			Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			Criteria criteria = session.createCriteria(DeplDashboard.class);
			if (performaceStasInput.getCurrentRole() == null) {
				if (AuthUtils.getIfExecutiveRole() == UserRole.NI_CM)
					criteria.add(Restrictions.eq("cmId", performaceStasInput.getLoginId()));

				else if (AuthUtils.getIfExecutiveRole() == UserRole.NI_BM)
					criteria.add(Restrictions.eq("bmId", performaceStasInput.getLoginId()));

				else if (AuthUtils.getIfExecutiveRole() == UserRole.NI_AM)
					criteria.add(Restrictions.eq("amId", performaceStasInput.getLoginId()));

				else if (AuthUtils.isNITLUser())
					criteria.add(Restrictions.eq("tlId", performaceStasInput.getLoginId()));
				else
					criteria.add(Restrictions.eq("loginid", performaceStasInput.getLoginId()));
			} else {
				if (performaceStasInput.getCurrentRole() != null && !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_CM) {
					log.info(" inside NI_CM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("cmId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_BM) {
					log.info(" inside NI_BM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("bmId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_AM) {
					log.info(" inside NI_AM role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("amId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_TL) {
					log.info(" inside NI_TL role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("tlId", performaceStasInput.getLoginId()));

				} else if (performaceStasInput.getCurrentRole() != null
						&& !performaceStasInput.getCurrentRole().isEmpty()
						&& Integer.valueOf(performaceStasInput.getCurrentRole()) == UserRole.NI_NE) {
					log.info(" inside NI_NE role " + performaceStasInput.getCurrentRole());
					criteria.add(Restrictions.eq("loginid", performaceStasInput.getLoginId()));
				}
			}
			List<DeplDashboard> sdDashboards = criteria.list();

			log.info(" Got performance stats depl form DB " + performaceStasInput.getLoginId()
					+ " performace stats from db " + sdDashboards.size());

			for (Iterator<DeplDashboard> iterator = sdDashboards.iterator(); iterator.hasNext();) {

				DeplDashboard deplDashboard = (DeplDashboard) iterator.next();

				DeplPerformaceStasOutput performaceStasOutput = new DeplPerformaceStasOutput();

				BeanUtils.copyProperties(deplDashboard, performaceStasOutput);

				

				performaceStatsOutputs.add(performaceStasOutput);

			}

			log.info(" Got performance stats depl for " + performaceStasInput.getLoginId()
					+ " performace Stats Outputs " + performaceStatsOutputs.size());

		} catch (Exception e) {
			log.error(" Got exception, while getting performance depl stats for " + performaceStasInput.getLoginId());
			e.printStackTrace();
		}

		return performaceStatsOutputs;

	}

}