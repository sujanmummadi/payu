package com.cupola.fwmp.dao.workStage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.persistance.entities.WorkOrderType;
import com.cupola.fwmp.persistance.entities.WorkStage;
import com.cupola.fwmp.vo.WorkStageVo;

@Transactional
public class WorkStageDAOImpl implements WorkStageDAO {

	private static final Logger logger = LogManager.getLogger(WorkStageDAOImpl.class.getName());
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	private String WORKSTAGES_BY_TYPE_QUERY = "SELECT ws.* FROM workstage ws  INNER JOIN workordertypeworkstagemapping wtwsmapp on wtwsmapp.workSatgeId=ws.id"
			+ " INNER JOIN workordertype as wt on wtwsmapp.workOrderTypeId = wt.id"
			+ " where wtwsmapp.workOrderTypeId=:id order by wtwsmapp.sequence";
	

	@Override
	public List<WorkStageVo> getWorkStages() 
	{
	    logger.debug(" inside getWorkStages method  ");
		Map<Long, WorkStageVo> localCache = new HashMap<Long, WorkStageVo>();
		if(localCache.size() > 0)
			return new ArrayList<>(localCache.values());
		
		List<WorkStage> workStages = (List<WorkStage>) hibernateTemplate.find("from WorkStage");
		if(workStages == null)
			return new ArrayList<>(localCache.values());;
			WorkStageVo vo = null;
			for(WorkStage stage : workStages)
			{
				vo = new WorkStageVo();
				vo.setId(stage.getId());
				vo.setWorkStageName(stage.getWorkStageName());
				localCache.put(stage.getId(), vo);
			}
		logger.debug(" Exitting from  getWorkStages  ");
		return new ArrayList<>(localCache.values());
	}
	
	@Override
	public List<WorkStageVo> getWorkStagesForOrderTypeByTypeId(Long id) 
	{
		List <WorkStageVo> voList = new ArrayList<WorkStageVo>();
		
		if(id == null )
			return voList;
		
		logger.debug(" inside getWorkStagesForOrderTypeByTypeId  "+id);
		
		try
		{
			WorkOrderType type = hibernateTemplate.load(WorkOrderType.class, id);
			
			if(type == null || type.getWorkStages() == null)
			{
				return voList;
			}
			Set<WorkStage>  stages = type.getWorkStages();
		 
			WorkStageVo vo = null;
			for(WorkStage stage : stages)
			{
				vo = new WorkStageVo();
				BeanUtils.copyProperties(stage, vo);
				voList.add(vo);
			}
		} catch ( Exception e) {
			logger.error("Error occured while getWorkStagesForOrderTypeByTypeId ",e);
			e.printStackTrace();
		} 
		 
		logger.debug(" Exitting from getWorkStagesForOrderTypeByTypeId  "+id);
		 
		return voList;
	}
	
}
