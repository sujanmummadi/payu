package com.cupola.fwmp.dao.workStage;

import java.util.List;

import com.cupola.fwmp.vo.WorkStageVo;

public interface WorkStageDAO {
	
	List<WorkStageVo> getWorkStagesForOrderTypeByTypeId(Long id);

	List<WorkStageVo> getWorkStages();

}
