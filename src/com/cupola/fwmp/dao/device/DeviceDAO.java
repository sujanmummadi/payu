package com.cupola.fwmp.dao.device;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.tools.RenameFXVo;

public interface DeviceDAO {

	public Device addDevice(Device device);

	public DeviceVO getDeviceById(Long id);

	public List<Device> getAllDevice();

	public Device deleteDevice(Long id);

	public Device updateDevice(Device device);

	public Map<Long, String> getDevicesByBranchId(String branchId);
	
	long getDeviceIdByDeviceName(String deviceName);

	Map<Long, String> getDevicesByAreaId(String areaId);
	
	String getDeviceMacByDeviceName(String deviceName);
	
	public List<String> getAllDeviceNames();

	void addBulkDevice(List<Device> devices);

	public void addDevices(List<DeviceVO> devices);

	public Map<Long, String> getDevices(Long cityId, Long branchId, String key);

	public Map<Long, String> getDevicesForUser(Long userId);

	void mapDevicesForUser(
			List< TypeAheadVo> devices, Long userId);
	
	APIResponse renameDevice(List<RenameFXVo> renameFXVos);

	public List<String> getAllDeviceForExecutive(long currentUserId);
	
	/**
	 * @Author : Sharan.Shastri
	 */
	public String mapDeviceForUserInBulk(String loginId, String deviceName);
}
