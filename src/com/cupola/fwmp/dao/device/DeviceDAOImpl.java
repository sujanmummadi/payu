package com.cupola.fwmp.dao.device;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.DefaultThereadPoolConfig;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.DeviceVO;
import com.cupola.fwmp.vo.MapUserDeviceVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.tools.RenameFXVo;

@Transactional
public class DeviceDAOImpl implements DeviceDAO
{

	private Logger log = Logger.getLogger(DeviceDAOImpl.class.getName());

	HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	CityDAO cityDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	UserDao userDao;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	DefinitionCoreService definitionCoreService;

	private static final String GET_ALL_DEVICES = "select id,deviceName,ipAddress,macAddress,cityId,branchId,status from Device";
	private static final String GET_DEVICES = "select d.id, d.deviceName from Device d where d.cityId=:cityId and d.branchId =:branchId ";
	private static final String GET_DEVICES_FOR_USER = "SELECT  ud.deviceId,d.deviceName FROM  UserDeviceMapping ud"
			+ " inner join Device d on d.id=ud.deviceId where ud.userId=:userId";
	private static final String DELETE_PREVIOUS_MAPPING = "DELETE FROM UserDeviceMapping where userId=?";

	private static String QUERY_SAVE = " insert into  Device (id, deviceName,cityId,branchId,addedBy,addedOn,modifiedOn,"
			+ " deviceType ,availablePort,totalPort,usedPort,status)"
			+ " VALUES (?, ?, ?,?, ?, ?,?,2,5,8,3,1)";
	private static String QUERY_SAVE_MAPPING = "Insert into UserDeviceMapping (userId,deviceId) values(?,?) ";

	public static Map<Long, DeviceVO> deviceIdDeviceDetails = new ConcurrentHashMap<Long, DeviceVO>();

	public static Map<String, Long> deviceNameAndDeviceIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<String, String> deviceNameAndDeviceMacMap = new ConcurrentHashMap<String, String>();

	private void init()
	{
		log.info("Device init method called..!");
		
		getDevices();
		
		log.info("Device init method executed..");
	}

	private void getDevices()
	{
		log.info("########## getDevices START jdbcTemplate ##########"
				+ new Date());
		List<DeviceVO> deviceList = jdbcTemplate
				.query(GET_ALL_DEVICES, new BeanPropertyRowMapper(DeviceVO.class));
		log.debug("########## getting device END jdbcTemplate ##########"
				+ new Date());
		/*
		 * log.info("########## getDevices START hibernateTemplate##########"
		 * +new Date()); List<Device> deviceList = (List<Device>)
		 * hibernateTemplate.find("from Device"); log.info(
		 * "########## getBranches END hibernateTemplate##########"+new Date());
		 */
		if (deviceList==null || deviceList.isEmpty())
		{
			log.info("Device details are not available");
			return;

		} else
		{
			int totalSize = deviceList.size();
			log.info("Adding device to cache loop started.. total device to cache are " + totalSize);
			
			
			
			List<List<DeviceVO>> chunckedDeviceList = new ArrayList<List<DeviceVO>>();
			
			int maxLimit = 1000;

			for (int i = 0; i < deviceList.size(); i += maxLimit)
			{
				chunckedDeviceList.add(deviceList.subList(i, Math.min(i
						+ maxLimit, deviceList.size())));
			}
			
			log.info("Total chunks for total devices " + totalSize + " are " + chunckedDeviceList.size());
			
			
//			deviceList.forEach(device);
			
			String threadPoolSizeString = definitionCoreService
					.getThreadsPoolsInfo(DefinitionCoreService.FWMP_DEVICE_CACHE_THREAD_POOL_SIZE);

			ExecutorService executor = Executors
					.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
							? Integer.valueOf(threadPoolSizeString)
							: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);

			for (Iterator iterator = chunckedDeviceList.iterator(); iterator.hasNext();) 
			{
				final List<DeviceVO> list = (List<DeviceVO>) iterator.next();
				
			try
			{

				Runnable worker = new Runnable()
				{
					@Override
					public void run()
					{
						int listSize = list != null ? list.size() : 0;
						Thread.currentThread().setName("chunckedDeviceList no "  + Math.random());
						try
						{
							// Start processing from here
							if (list != null
									&& !list.isEmpty())
							{
								for (DeviceVO device : list)
								{
									device.setCityName(cityDAO.getCityNameById(device.getAreaId()));
									device.setBranchName(branchDAO
											.getBranchNameById(device.getBranchId()));
									// device.setAreaName(areaDAO.getAreaNameById(device.getAreaId()));
									log.debug("Remaing device to cache are " + -- listSize + " for thread " + Thread.currentThread().getName());
									addToCache(device);
								}

							}

						} catch (Exception e)
						{
							e.printStackTrace();
							log.error("Error in device thread " + Thread.currentThread().getName()
									+ " " + list.size() + ""
									+ e.getMessage() , e);
						}

					}
				};
				
				executor.execute(worker);
				// Thread.sleep(10);
			} catch (Exception e)
			{
				e.printStackTrace();

				log.error("Error in devcie main thread "
						+ e.getMessage(), e);
				continue;
			}
		}
//			System.exit(0);
//			for (DeviceVO device : deviceList)
//			{
//				device.setCityName(cityDAO.getCityNameById(device.getAreaId()));
//				device.setBranchName(branchDAO
//						.getBranchNameById(device.getBranchId()));
//				// device.setAreaName(areaDAO.getAreaNameById(device.getAreaId()));
//				log.info("Remaing device to cache are " + --totalSize);
//				addToCache(device);
//			}
			
			log.info("Adding device to cache loop ended.. ");
		}

		// System.exit(0);
	}

	private void addToCache(DeviceVO device)
	{
		log.debug(" inside addToCache " +device.getDeviceName());

		deviceIdDeviceDetails.put(device.getId(), device);

		deviceNameAndDeviceIdMap.put(device.getDeviceName(), device.getId());
		
		if(device.getMacAddress()!=null && !device.getMacAddress().isEmpty()){
			
			deviceNameAndDeviceMacMap.put(device.getDeviceName(), device
					.getMacAddress());
		}else{
			device.setMacAddress("00:00:00:00:00:00");
			deviceNameAndDeviceMacMap.put(device.getDeviceName(),device.getMacAddress());
		}

		deviceNameAndDeviceMacMap
				.put(device.getDeviceName(), device.getMacAddress());
	}

	@Override
	public Device addDevice(Device device)
	{

		if (device == null)

			return null;

		else
		{
			try {
			device.setMacAddress(device.getMacAddress().trim().toUpperCase());
			device.setDeviceName(device.getDeviceName().trim().toUpperCase());

			if (deviceNameAndDeviceIdMap!=null && deviceNameAndDeviceIdMap.containsKey(device.getDeviceName()))
				return device;

			device = addCommonInitialValues(device);

			hibernateTemplate.save(device);

			log.info("device with id:" + device.getId()
					+ " is saved into database successfully");

			DeviceVO deviceVo = new DeviceVO();
			BeanUtils.copyProperties(device, deviceVo);

			deviceVo.setCityName(cityDAO.getCityNameById(deviceVo.getAreaId()));
			deviceVo.setBranchName(branchDAO.getBranchNameById(deviceVo
					.getBranchId()));
			deviceVo.setAreaName(areaDAO.getAreaNameById(deviceVo.getAreaId()));

			addToCache(deviceVo);

				
			} catch (DataAccessException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			log.debug("Exit From addDevice ..." +device.getDeviceName());
			
			return device;
		}
	}

	@Override
	public DeviceVO getDeviceById(Long id)
	{
		if (id == null || id.longValue() <= 0)
			return null;

		log.debug("inside getDeviceById " + id);
		
		if (deviceIdDeviceDetails==null || deviceIdDeviceDetails.size() == 0)
			getDevices();
          
		if(deviceIdDeviceDetails!=null && !deviceIdDeviceDetails.isEmpty() ){
        	  
        	  return deviceIdDeviceDetails.get(id);
          }else{
        	  return null;
          }
		
	}

	@Override
	public List<Device> getAllDevice()
	{
		log.debug("Getting all the devices");
		
		return hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery("select * from Device").addEntity(Device.class)
				.list();

	}

	@Override
	public Device deleteDevice(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Device updateDevice(Device device)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Long, String> getDevicesByBranchId(String branchId)
	{
		Map<Long, String> deviceTypeAheadList = new TreeMap<>();
		if(branchId == null || branchId.isEmpty())
			return deviceTypeAheadList;
		
		log.info(" inside getDevicesByBranchId " + branchId);

		List<DeviceVO> deviceVOs = new ArrayList<DeviceVO>(deviceIdDeviceDetails
				.values());

		for (DeviceVO deviceVO : deviceVOs)
		{
			if (deviceVO.getBranchId() == Long.valueOf(branchId))
			{
				deviceTypeAheadList.put(deviceVO.getId(), deviceVO
						.getDeviceName());
			}

		}

		/*
		 * log.info("branch name::"+branchId);
		 * 
		 * if(branchId != null){
		 * 
		 * long id = Long.valueOf(branchId); Branch branch =
		 * hibernateTemplate.get(Branch.class, id);
		 * 
		 * if(branch != null){
		 * 
		 * Set<Device> devices = branch.getDevices();
		 * 
		 * for (Device device : devices) {
		 * deviceTypeAheadList.put(device.getId(), device.getDeviceName()); } }
		 * 
		 * log.info("Device list :: "+deviceTypeAheadList+" for branch: "+id); }
		 * 
		 * else
		 * 
		 * log.info("Branch Id is NULL");
		 */

		log.debug(" Exit From getDevicesByBranchId " + branchId);
		
		return deviceTypeAheadList;

	}

	@Override
	public Map<Long, String> getDevicesByAreaId(String areaId)
	{

		Map<Long, String> deviceTypeAheadList = new TreeMap<>();

		if (areaId == null || areaId.isEmpty())
			return deviceTypeAheadList;
		
		log.debug(" inside getDevicesByAreaId " + areaId);

		List<DeviceVO> deviceVOs = new ArrayList<DeviceVO>(deviceIdDeviceDetails
				.values());

		for (DeviceVO deviceVO : deviceVOs)
		{
			if (deviceVO.getAreaId() == Long.valueOf(areaId))
			{
				deviceTypeAheadList.put(deviceVO.getId(), deviceVO
						.getDeviceName());
			}

		}

		/*
		 * if(areaId != null){
		 * 
		 * long id = Long.valueOf(areaId); Area area =
		 * hibernateTemplate.get(Area.class, id);
		 * 
		 * if(area != null){
		 * 
		 * Set<Device> devices = area.getDevices();
		 * 
		 * for (Device device : devices) {
		 * deviceTypeAheadList.put(device.getId(), device.getDeviceName()); } }
		 * 
		 * log.info("Device list :: "+deviceTypeAheadList+" for branch: "+id); }
		 * 
		 * else
		 * 
		 * log.info("Area Id is NULL");
		 */
		log.debug(" Exit From getDevicesByAreaId " + areaId);

		return deviceTypeAheadList;

	}

	@Override
	public long getDeviceIdByDeviceName(String deviceName)
	{
		if(deviceName == null || deviceName.isEmpty())
			return 0;
		
		log.info(" getDeviceIdByDeviceName " +deviceName);
		
		if (deviceNameAndDeviceIdMap.size() == 0)
			getDevices();

		if (deviceNameAndDeviceIdMap.get(deviceName) != null)
			return deviceNameAndDeviceIdMap.get(deviceName);

		else
			return 0;
	}

	@Override
	public String getDeviceMacByDeviceName(String deviceName)
	{
		if(deviceName == null ||  deviceName.isEmpty())
			return null;
		
		log.info(" getDeviceMacByDeviceName " +deviceName);
		
		if (deviceNameAndDeviceMacMap.size() == 0)
			getDevices();

		if (deviceNameAndDeviceMacMap.get(deviceName) != null)
			return deviceNameAndDeviceMacMap.get(deviceName);

		else
			return null;
	}

	public Device addCommonInitialValues(Device device)
	{
		log.debug(" adding Device CommonInitialValues "  + device.getDeviceName());

		device.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		device.setAddedOn(new Date());
		device.setModifiedOn(new Date());
		device.setAddedBy(100l);
		device.setModifiedBy(100l);

		log.debug("Exit From adding Device CommonInitialValues "  + device.getDeviceName());
		return device;

	}

	@Override
	public List<String> getAllDeviceNames()
	{
		log.debug(" Inside  getAllDeviceNames " );
		
        if(deviceNameAndDeviceMacMap!=null && !deviceNameAndDeviceMacMap.isEmpty()){
        	
        	return new ArrayList<String>(deviceNameAndDeviceMacMap.keySet());
        }else{
        	return new ArrayList<>();
        }
		
	}

	@Override
	public void addBulkDevice(List<Device> devices)
	{
		int totalDevcieToAdd = devices != null ? devices.size() : 0;
 

		log.info("Total device to add are " +totalDevcieToAdd);

		Session session = null;
		Transaction tx = null;

		try
		{
			if (devices == null || devices.isEmpty())
			{
				return;

			} else
			{
				session = hibernateTemplate.getSessionFactory().openSession();
				tx = session.beginTransaction();

				for (Device device : devices)
				{
					log.debug("Remaing Device to add "
							+ totalDevcieToAdd--);

					session.save(device);
				}

				tx.commit();
			}
		} catch (Exception e)
		{
			if (tx != null)
				tx.rollback();

			e.printStackTrace();
			log.error("Error in adding device " + e.getMessage());
		} finally
		{
			if (session != null)
				session.close();

		}

	}

	@Override
	public void addDevices(final List<DeviceVO> devices)
	{
		if (devices == null || devices.isEmpty())
			return;
		
		log.debug(" adding Devices " + devices.size());

		try
		{
			final java.sql.Date currentDate = new java.sql.Date(System.currentTimeMillis());
			final int batchSize = 500;

			for (int j = 0; j < devices.size(); j += batchSize)
			{

				final List<DeviceVO> batchList = devices.subList(j, j
						+ batchSize > devices.size() ? devices.size() : j
						+ batchSize);

				this.jdbcTemplate
						.batchUpdate(QUERY_SAVE, new BatchPreparedStatementSetter()
						{

							@Override
							public int getBatchSize()
							{
								return batchList.size();
							}

							@Override
							public void setValues(PreparedStatement ps, int i)
									throws SQLException
							{
								DeviceVO device = batchList.get(i);
								ps.setLong(1, StrictMicroSecondTimeBasedGuid
										.newGuid());
								ps.setString(2, device.getDeviceName()
										.toUpperCase());
								ps.setLong(3, device.getCityId());
								ps.setLong(4, device.getBranchId());
								ps.setLong(5, AuthUtils.getCurrentUserId());
								ps.setDate(6, currentDate);
								ps.setDate(7, currentDate);
							}
						});

			}
		} catch ( Exception e)
		{
			 log.error(" error while adding Devices " ,e);
		}
	}

	@Override
	public Map<Long, String> getDevices(Long cityId, Long branchId, String key)
	{
		Map<Long, String> devices = new HashMap<Long, String>();
		
		if(cityId == null || cityId.longValue() <= 0 || branchId == null || branchId.longValue() <= 0 )
			return devices;
		
		log.debug("inside getDevices " + cityId + " And " +branchId);
		
		try
		{
			StringBuilder qury = new StringBuilder(GET_DEVICES) ;
			if(key != null && key.length() > 5)
			{
				qury.append(" and d.deviceName like ('"+key+"%') ");
			}
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(qury.toString());
			query.setLong("cityId", cityId);
			query.setLong("branchId", branchId);

			log.debug("Query " + query.getQueryString());
			List<Object[]> result = query.list();

			if (result == null || result.isEmpty())
				return null;

			for (Object[] obj : result)
			{
				devices.put(objectToLong(obj[0]), (obj[1]).toString());
			}

		} catch ( Exception e)
		{
			log.error(" Error while getting Devices " ,e);
		}

		log.debug(" Exit from  getDevices " + cityId + " And " +branchId);
		
		return devices;
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	@Override
	public Map<Long, String> getDevicesForUser(Long userId)
	{

		Map<Long, String> devices = new HashMap<Long, String>();
		
		if(userId == null || userId.longValue() <= 0)
			return devices;
		
		log.debug(" geting Devices For User " +userId);
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(GET_DEVICES_FOR_USER);
			query.setLong("userId", userId);

			log.debug("getDevicesForUser Query " + query.getQueryString());
			List<Object[]> result = query.list();

			if (result == null || result.isEmpty())
				return null;

			for (Object[] obj : result)
			{
				devices.put(objectToLong(obj[0]), (obj[1]).toString());
			}

		} catch ( Exception e)
		{
			log.error("Error in geting Devices For User");
			e.printStackTrace();
		}
		log.debug(" Exit From  geting Devices For User " +userId);

		return devices;

	}

	@Override
	public void mapDevicesForUser(List<TypeAheadVo> devices, final Long userId)
	{
		if(devices == null || devices.isEmpty() || userId == null || userId.longValue() <= 0  )
			return;
		
		log.debug(" inside  mapDevicesForUser " + devices.size() +" For User "  + userId);
		
		try
		{
			final int batchSize = 500;

			this.jdbcTemplate.update(DELETE_PREVIOUS_MAPPING, new Object[] { userId });
			
			for (int j = 0; j < devices.size(); j += batchSize)
			{

				final List<TypeAheadVo> batchList = devices.subList(j, j
						+ batchSize > devices.size() ? devices.size() : j
						+ batchSize);

				this.jdbcTemplate
						.batchUpdate(QUERY_SAVE_MAPPING, new BatchPreparedStatementSetter()
						{

							@Override
							public int getBatchSize()
							{
								return batchList.size();
							}

							@Override
							public void setValues(PreparedStatement ps, int i)
									throws SQLException
							{

								ps.setLong(1, userId);
								ps.setLong(2, batchList.get(i).getId());

							}
						});

			}
			UserVo user = userDao.getUserById(userId);
			user.setDevices(new HashSet<TypeAheadVo>(devices));
		} catch ( Exception e)
		{
			log.error(" Error while   mapping Devices ForUser " ,e  );
		}

	}

	@Override
	public APIResponse renameDevice(List<RenameFXVo> renameFXVos)
	{
		if(renameFXVos == null || renameFXVos.isEmpty())
			return null;
		
		log.info(" inside rename Device "  + renameFXVos.size());
		
		Session session = hibernateTemplate.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		if (renameFXVos != null && !renameFXVos.isEmpty())
		{
			try
			{
				for (Iterator<RenameFXVo> iterator = renameFXVos
						.iterator(); iterator.hasNext();)
				{
					RenameFXVo renameFXVo = (RenameFXVo) iterator.next();

					String query = "select d.* from Device d where deviceName ='"
							+ renameFXVo.getOldFxName() + "'";
					
					List<Device> getDevice = session
							.createSQLQuery(query).addEntity(Device.class)
							.list();
					
					int size = getDevice != null ? getDevice.size() : 0;
					
					log.debug("Total device against " + query + " " + size);

					if (getDevice != null && !getDevice.isEmpty())
					{
						log.debug("Total device against " + query + getDevice.size());
						
						for (Iterator<Device> iterator2 = getDevice
								.iterator(); iterator2.hasNext();)
						{
							try
							{
								log.debug("Renaming " + renameFXVo);
								Device device = (Device) iterator2.next();
								device.setDeviceName(renameFXVo.getNewFxName());

								session.update(device);

							} catch (Exception e)
							{
								log.error("Error in rename device");
								e.printStackTrace();
								continue;
							}
						}
					} else
						continue;
				}
				transaction.commit();

			} catch (Exception e)
			{
				log.error("Error in rename device");
				e.printStackTrace();
			} finally
			{
				session.close();
			}

		}
		
		log.debug(" Exit From rename Device "  + renameFXVos.size());
		
		return null;
	}

	@Override
	public List<String> getAllDeviceForExecutive(long currentUserId)
	{
		if(currentUserId <= 0)
			return null;
		
		log.debug(" getting All DeviceForExecutive " +currentUserId  );
		
		List<DeviceVO> devices = jdbcTemplate
				.query("select d.deviceName as deviceName from Device d "
						+ "join UserDeviceMapping udm on d.id=udm.deviceId join User u "
						+ "on u.id=udm.userId where u.id=?", new Object[] {
								currentUserId }, new DeviceForExecutiveMapper());
	
		List<String> deviceList = null;
		
		if (devices != null && !devices.isEmpty())
		{
			deviceList = new ArrayList<>();

			for (Iterator<DeviceVO> iterator = devices.iterator(); iterator
					.hasNext();)
			{
				DeviceVO deviceVO = (DeviceVO) iterator.next();
				deviceList.add(deviceVO.getDeviceName());

			}
			return deviceList;
		}
		
		log.debug("Exit From  getting All DeviceForExecutive " +currentUserId  );
		
		return deviceList;
	}

	private class DeviceForExecutiveMapper implements RowMapper<DeviceVO>
	{

		@Override
		public DeviceVO mapRow(ResultSet arg0, int arg1) throws SQLException
		{
			DeviceVO deviceVO = new DeviceVO();
			deviceVO.setDeviceName(arg0.getString("deviceName"));
			return deviceVO;
		}

	}
	
	@Override
	public String mapDeviceForUserInBulk(String loginId, String deviceName) {

		MapUserDeviceVO deviceVO1 = new MapUserDeviceVO();
		MapUserDeviceVO deviceVO2 = new MapUserDeviceVO();

		try {
			deviceVO1 = (MapUserDeviceVO) jdbcTemplate.queryForObject(
					"select u.id, u.status, ugum.idUserGroup, ubm.idBranch from User u join UserGroupUserMapping ugum on ugum.idUser=u.id join UserBranchMapping ubm on ubm.idUser=u.id where u.loginId=?",
					new Object[] { loginId }, new CheckUser());
			if (deviceVO1.getIsActive() == 0)
				return "User is InActive";
		} catch (EmptyResultDataAccessException e) {
			return "User not found";
		}

		try {
			deviceVO2 = (MapUserDeviceVO) jdbcTemplate.queryForObject(
					"select  id, branchId from Device where deviceName=?", new Object[] { deviceName },
					new CheckDevice());
		} catch (EmptyResultDataAccessException e) {
			return "Device not found";
		}

		if (deviceVO1.getUserBranchId().equals(deviceVO2.getDeviceBranchId())) {
			try {

				jdbcTemplate.update(
						"delete from UserDeviceMapping where userId in (select x.userId from(select udm.userId from UserDeviceMapping udm join User u on udm.userId = u.id join UserGroupUserMapping ugum on ugum.idUser = u.id where ugum.idUserGroup = ? and udm.deviceId = ?) as x) and deviceId = ?",
						new Object[] { deviceVO1.getUserGroupId(), deviceVO2.getDeviceId(), deviceVO2.getDeviceId() });

				jdbcTemplate.update("insert UserDeviceMapping values(?,?)", deviceVO1.getUserId(),
						deviceVO2.getDeviceId());
			} catch (Exception e) {
				System.out.println(e);
			}
			return "success";
		} else {
			return "User and Device are not from same branch";
		}

	}

	public class CheckUser implements RowMapper<MapUserDeviceVO> {
		public MapUserDeviceVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			MapUserDeviceVO mapUserDeviceVO = new MapUserDeviceVO();
			mapUserDeviceVO.setUserId(rs.getLong("u.id"));
			mapUserDeviceVO.setIsActive(rs.getInt("u.status"));
			mapUserDeviceVO.setUserGroupId(rs.getLong("ugum.idUserGroup"));
			mapUserDeviceVO.setUserBranchId(rs.getLong("ubm.idBranch"));
			return mapUserDeviceVO;
		}

	}

	public class CheckDevice implements RowMapper<MapUserDeviceVO> {
		public MapUserDeviceVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			MapUserDeviceVO mapUserDeviceVO = new MapUserDeviceVO();
			mapUserDeviceVO.setDeviceId(rs.getLong("id"));
			mapUserDeviceVO.setDeviceBranchId(rs.getLong("branchId"));
			return mapUserDeviceVO;
		}

	}
}
