/**
 * 
 */
package com.cupola.fwmp.dao.ticketgc;

import java.util.List;

import com.cupola.fwmp.vo.TicketsForGC;

/**
 * @author aditya
 * 
 */
public interface TicketsForGCDAO
{
	List<TicketsForGC> getSalesTicketsForGC();

	List<TicketsForGC> getDeploymentTicketsForGC();

	List<TicketsForGC> getFrTicketsForGC();
}
