/**
 * 
 */
package com.cupola.fwmp.dao.ticketgc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.vo.TicketsForGC;

/**
 * @author aditya
 * 
 */
@Transactional
public class TicketsForGCDAOImpl implements TicketsForGCDAO
{
	private Logger log = Logger.getLogger(TicketsForGCDAOImpl.class.getName());

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	DefinitionCoreService definitionCoreService;

	private String salesGarbageEligibleTickets;

	private String deploymentGarbageEligibleTickets;
	
	private String frTicketEligibleForGC;

	public void setSalesGarbageEligibleTickets(
			String salesGarbageEligibleTickets)
	{
		this.salesGarbageEligibleTickets = salesGarbageEligibleTickets;
	}

	public void setDeploymentGarbageEligibleTickets(
			String deploymentGarbageEligibleTickets)
	{
		this.deploymentGarbageEligibleTickets = deploymentGarbageEligibleTickets;
	}
	
	public void setFrTicketEligibleForGC(String frTicketEligibleForGC) 
	{
		this.frTicketEligibleForGC = frTicketEligibleForGC;
	}

	@Override
	public List<TicketsForGC> getSalesTicketsForGC()
	{
		log.info("fwmp.get.garbage.eligible.ticket.for.sales :: "
				+ salesGarbageEligibleTickets);

		salesGarbageEligibleTickets = definitionCoreService
				.getClassificationQuery(DefinitionCoreService.REMOVE_SALES_COMPLETED_TICKET);

		List<TicketsForGC> ticketsForGCs = jdbcTemplate
				.query(salesGarbageEligibleTickets, new TicketsForGCMapperForSales());

		

		if (ticketsForGCs != null && !ticketsForGCs.isEmpty())
		{
			log.debug("total tickets For GCs in sales are " + ticketsForGCs.size());
			log.debug("Total Sales Tickets For GC " + ticketsForGCs.size());

			return ticketsForGCs;

		} else
		{
			log.info("No Sales Tickets For GC " + ticketsForGCs);

			return new ArrayList<TicketsForGC>();
		}
	}

	@Override
	public List<TicketsForGC> getDeploymentTicketsForGC()
	{
		log.info("fwmp.get.garbage.eligible.ticket.for.deployment :: "
				+ deploymentGarbageEligibleTickets);
		
		deploymentGarbageEligibleTickets = definitionCoreService
				.getClassificationQuery(DefinitionCoreService.REMOVE_DEPLOYMENT_COMPLETED_TICKET);

		List<TicketsForGC> ticketsForGCs = jdbcTemplate
				.query(deploymentGarbageEligibleTickets, new TicketsForGCMapperForDeployment());

		

		if (ticketsForGCs != null && !ticketsForGCs.isEmpty())
		{
			log.debug("total tickets For GCs in deployment are "
					+ ticketsForGCs.size());
			log.debug("Total Deployment Tickets For GC " + ticketsForGCs.size());

			return ticketsForGCs;

		} else
		{
			log.info("No Deployment Tickets For GC " + ticketsForGCs);

			return new ArrayList<TicketsForGC>();
		}

	}
	
	@Override
	public List<TicketsForGC> getFrTicketsForGC()
	{
		try
		{
			frTicketEligibleForGC = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.REMOVE_FR_COMPLETED_TICKET);
			
			log.info("Fr GC is invoking "+ frTicketEligibleForGC);

			List<TicketsForGC> ticketsForGCs = jdbcTemplate
					.query(frTicketEligibleForGC, new FrTicketsMapperForGC());


			if (ticketsForGCs != null && !ticketsForGCs.isEmpty())
			{

				log.info("total FR tickets For GCs are "
						+ ticketsForGCs.size());
				return ticketsForGCs;
			}
			else
			{
				log.debug("No FR Tickets For GC " + ticketsForGCs);
				return new ArrayList<TicketsForGC>();
			}
		} 
		catch (Exception e) 
		{
			log.debug("No FR Tickets For GC " + e);
			return new ArrayList<TicketsForGC>();
		}
	}

}

class FrTicketsMapperForGC implements RowMapper<TicketsForGC>
{

	@Override
	public TicketsForGC mapRow(ResultSet resultSet, int rowNumber) throws SQLException
	{
		
		TicketsForGC ticketsForGC = new TicketsForGC();

		ticketsForGC.setTicketId(resultSet.getLong("ticketId"));
		ticketsForGC.setTicketStatus(resultSet.getInt("ticketStatus"));
		ticketsForGC.setCurrentAssignedTo(resultSet.getLong("currentAssignedTo"));

		return ticketsForGC;
	}
	
}

class TicketsForGCMapperForSales implements RowMapper<TicketsForGC>
{
	@Override
	public TicketsForGC mapRow(ResultSet resultSet, int rowNumber)
			throws SQLException
	{
		TicketsForGC ticketsForGC = new TicketsForGC();
		if(resultSet == null)
			return ticketsForGC;

		ticketsForGC
				.setCurrentAssignedTo(resultSet.getLong("currentAssignedTo"));
		ticketsForGC.setDocumentStatus(resultSet.getInt("documentStatus"));
		ticketsForGC.setJoiningTableStatus(resultSet.getInt("customerStatus"));
		ticketsForGC.setPaymentStatus(resultSet.getInt("paymentStatus"));
		ticketsForGC.setTicketId(resultSet.getLong("ticketId"));
		ticketsForGC.setTicketStatus(resultSet.getInt("ticketStatus"));

		return ticketsForGC;
	}
}

class TicketsForGCMapperForDeployment implements RowMapper<TicketsForGC>
{
	@Override
	public TicketsForGC mapRow(ResultSet resultSet, int rowNumber)
			throws SQLException
	{
		TicketsForGC ticketsForGC = new TicketsForGC();
		if(resultSet == null)
			return ticketsForGC;
		ticketsForGC
				.setCurrentAssignedTo(resultSet.getLong("currentAssignedTo"));
		ticketsForGC.setDocumentStatus(resultSet.getInt("documentStatus"));
		ticketsForGC.setJoiningTableStatus(resultSet.getInt("workorderStatus"));
		ticketsForGC.setPaymentStatus(resultSet.getInt("paymentStatus"));
		ticketsForGC.setTicketId(resultSet.getLong("ticketId"));
		ticketsForGC.setTicketStatus(resultSet.getInt("ticketStatus"));

		return ticketsForGC;
	}
}
