package com.cupola.fwmp.dao.materials;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.collection.MongoCollections;
import com.cupola.fwmp.dao.mongo.material.MaterialRequest;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.persistance.entities.FrMaterialDetail;
import com.cupola.fwmp.persistance.entities.Materials;
import com.cupola.fwmp.persistance.entities.TicketMaterialMapping;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.MaterialRequestVO;
import com.cupola.fwmp.vo.MaterialsVO;
import com.cupola.fwmp.vo.TicketMaterialMappingVO;

public class MaterialsDAOImpl implements MaterialsDAO
{

	final static Logger log = Logger.getLogger(MaterialsDAOImpl.class);

	private HibernateTemplate hibernateTemplate;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	MongoOperations mongoOperations;

	@Autowired
	TicketActivityLogDAO ticketActivityLogDAO;
	
	public static Map<Integer, String> cachedMaterialsById =new ConcurrentHashMap<Integer, String>();

	
	private static final String GET_USER_ID_USING_TICKETID = "select currentAssignedTo from FrTicket where id=?";

	private static final String GET_USER_ID__USING_TICKETID__FROM_TicketTable_QUERY = "select currentAssignedTo from Ticket where id=?";
	

	private static final String GET_ID_USING_TICKETID = "select id from TicketMaterialMapping where ticketId=? and materialId=?";

	private static final String GET_MAPPING_ID_USING_TICKETID_MATERIALID = "select id from TicketMaterialMapping where ticketId=? and materialId=?";

	private static final String GET_ALL_MATERIALS = "select * from Materials";

	private static final String GET_ADDEDON = "select addedOn from TicketMaterialMapping where id=?";

	private static final String GET_ADDEDBY = "select addedBy from TicketMaterialMapping where id=?";
	
	private static final String GET_MATERIAL_COSUPSTION_DETAILS = "select * from TicketMaterialMapping where ticketId = :ticketId and materialId in (:materialIds)";
	
	private static final String GET_MATERIAL_COSUPSTION = "select * from FrMaterialDetail where ticketId = ? ";
	

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	TicketMaterialMapping ticketMaterialMapping = new TicketMaterialMapping();
	
	private void init()
	{
		try
		{
			resetCachedMaterials();
		}catch (Exception e) {
			log.error("Error while getting Materials");
			e.printStackTrace();
		}
	}

	private void resetCachedMaterials()
	{
		try
		{
			String query = "from Materials";
			List<Materials> materials = (List<Materials>) hibernateTemplate
					.find(query);
			if (materials != null)
			{

				for (Materials material : materials)
				{
					addToLocalCache(material);
				}
			}
		} catch (Exception e)
		{
			log.error("Exception in resetCachedMaterials");
			e.printStackTrace();
		}
		
	}

	private void addToLocalCache(Materials material)
	{
		cachedMaterialsById.put(material.getId().intValue(), material.getName());
		
	}

	@Override
	public Materials addMaterials(Materials materials)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Materials getMaterialsById(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MaterialsVO> getAllMaterials()
	{

		List<MaterialsVO> listMaterials = new ArrayList<MaterialsVO>();

		log.info("Getting all materials");

		try
		{

			listMaterials = jdbcTemplate
					.query(GET_ALL_MATERIALS, new BeanPropertyRowMapper(MaterialsVO.class));

			log.debug("Material vo List: " + listMaterials);

		} catch (Exception e)
		{

			log.error("Exception on reading materials from db and its error message is "
					+ e.getMessage());

		}
		return listMaterials;

	}

	@Override
	public APIResponse updateMultipleMaterialConsumtion(
			TicketMaterialMappingVO ticketMaterialMappingVO)
	{
			log.info("Inside UpdateMultipleMaterialConsumtion of MaterialsDAOImpl with TicketMaterialMappingVO -->"
					+ ticketMaterialMappingVO.getTicketId());

		List<Long> ticketMaterialIdList = null;

		TicketMaterialMapping ticketMaterialMapping = new TicketMaterialMapping();

		try
		{

			if (ticketMaterialMappingVO == null)
			{

				log.info("TicketMaterialMappingVO is null");

				return ResponseUtil.createNullParameterResponse();

			}

			ticketMaterialMapping.setTicketId(ticketMaterialMappingVO
					.getTicketId());

			ticketMaterialMapping.setModifiedBy(ticketMaterialMappingVO
					.getModifiedBy());

			ticketMaterialMapping.setModifiedOn(ticketMaterialMappingVO
					.getModifiedOn());

			ticketMaterialMapping.setTicketId(ticketMaterialMappingVO
					.getTicketId());

			ticketMaterialMapping.setMaterialId(ticketMaterialMappingVO
					.getMaterialId());

			ticketMaterialMapping
					.setTotalConsumption(ticketMaterialMappingVO
							.getTotalConsumption());

			ticketMaterialMapping.setMacid(ticketMaterialMappingVO
					.getMacid());

			ticketMaterialMapping.setSerialNo(ticketMaterialMappingVO
					.getSerialNo());

			ticketMaterialMapping.setStartPoint(ticketMaterialMappingVO
					.getStartPoint());

			ticketMaterialMapping.setEndPoint(ticketMaterialMappingVO
					.getEndPoint());

			ticketMaterialMapping.setDrumNo(ticketMaterialMappingVO
					.getDrumNo());

			ticketMaterialMapping.setFiberType(ticketMaterialMappingVO
					.getFiberType());

			ticketMaterialMapping.setUoMid(ticketMaterialMappingVO
					.getUoMid());

			ticketMaterialMapping.setStatus(ticketMaterialMappingVO
					.getStatus());

			ticketMaterialIdList = jdbcTemplate
					.queryForList(GET_MAPPING_ID_USING_TICKETID_MATERIALID, new Object[] {
							ticketMaterialMappingVO.getTicketId(),
							ticketMaterialMappingVO.getMaterialId() }, Long.class);

			if (ticketMaterialIdList == null)
			{

				log.info("some thing went wrong in db operation. query-->"
						+ GET_MAPPING_ID_USING_TICKETID_MATERIALID);

				return ResponseUtil.createDBExceptionResponse();

			}

			if (!ticketMaterialIdList.isEmpty())
			{

				log.info("ticket id and material id combination is present in TicketMaterialMapping. Need to update row");

				Date addedon = (Date) jdbcTemplate
						.queryForObject(GET_ADDEDON, Date.class, new Object[] { ticketMaterialIdList
								.get(0) });

				Long addedby = (Long) jdbcTemplate
						.queryForObject(GET_ADDEDBY, Long.class, new Object[] { ticketMaterialIdList
								.get(0) });

				ticketMaterialMapping.setAddedBy(addedby);

				ticketMaterialMapping.setAddedOn(addedon);

				ticketMaterialMapping.setId(ticketMaterialIdList.get(0));

				hibernateTemplate.update(ticketMaterialMapping);

			} else
			{

				log.info("ticket id and material id combination is not present in TicketMaterialMapping. Need to add new row");

				ticketMaterialMapping.setAddedBy(ticketMaterialMappingVO
						.getAddedBy());

				ticketMaterialMapping.setId(StrictMicroSecondTimeBasedGuid
						.newGuid());

				ticketMaterialMapping.setAddedOn(ticketMaterialMappingVO
						.getAddedOn());

				ticketMaterialMapping.setModifiedOn(ticketMaterialMappingVO
						.getModifiedOn());

				hibernateTemplate.save(ticketMaterialMapping);

			}

		} catch (Exception e)
		{

			log.error("error occures . not able to add into db. msg-->"
					+ e.getMessage());

			return ResponseUtil.createFailureResponse();

		}

		return ResponseUtil.createSaveSuccessResponse();

	}

	@Override
	public Materials deleteMaterials(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Materials updateMaterials(Materials materials)
	{
		// TODO Auto-generated method stub
		return null;
	}

	// UpdateMaterialConsumption services work as insert or update . if ticket
	// id and material id is not present this will insert otherwise it will
	// update for Materials consumption ,this services is related to android
	// app.

	@Override
	@Transactional
	public String UpdateMaterialConsumption(
			TicketMaterialMappingVO ticketMaterialMappingvo)
	{
		log.info("ticketMaterialMappingvo detailss information:::"
				+ ticketMaterialMappingvo );

		long id = 0;
		String message = "failure";

		/*
		 * TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();
		 * 
		 * TicketAcitivityDetailsVO ticketAcitivityDetailsVO=new
		 * TicketAcitivityDetailsVO();
		 * 
		 * ticketAcitivityDetailsVO.setActivityId(ticketMaterialMappingvo
		 * .getActivityId());
		 * 
		 * ticketAcitivityDetailsVO.setTicketId(String.valueOf(
		 * ticketMaterialMappingvo.getTicketId()));
		 * 
		 * ticketAcitivityDetailsVO.setActivityStatus(ticketMaterialMappingvo
		 * .getActivityStatus());
		 */

		// ticketAcitivityDetailsVO.setSubActivityDetail(ticketMaterialMappingvo.get););

		Long ticketId = Long.valueOf(ticketMaterialMappingvo.getTicketId());

		int materialId = ticketMaterialMappingvo.getMaterialId();

		log.debug("materialIdmaterialId::::" + materialId);

		List<Long> IdList = (List<Long>) jdbcTemplate
				.queryForList(GET_ID_USING_TICKETID, new Object[] { ticketId,
						materialId }, Long.class);

		log.debug("getting unique id using ticket id from ticketmaterialmapping "
				+ IdList);

		if (IdList !=null && !IdList.isEmpty())
		{

			TicketMaterialMapping ticketMaterialMapp = new TicketMaterialMapping();

			ticketMaterialMapp = commonData(ticketMaterialMappingvo);


			if (ticketMaterialMapp != null)
			{

				Date addedon = (Date) jdbcTemplate
						.queryForObject("select addedOn from TicketMaterialMapping where id=?", Date.class, new Object[] { IdList
								.get(0) });

				ticketMaterialMapp.setAddedOn(addedon);

				ticketMaterialMapp.setModifiedOn(ticketMaterialMappingvo
						.getModifiedOn());

				ticketMaterialMapp.setId(IdList.get(0));

				hibernateTemplate.update(ticketMaterialMapp);

				// log.info("ticketActivityLogVo details:::" +
				// ticketActivityLogVo);

				/*
				 * ticketActivityLogDAO
				 * .updateWorkOrderActivity(ticketActivityLogVo);
				 */

				message = "success";
			}
		} else
		{

			TicketMaterialMapping ticketMaterialMap = new TicketMaterialMapping();

			ticketMaterialMap = commonData(ticketMaterialMappingvo);


			if (ticketMaterialMap != null)
			{

				ticketMaterialMap.setId(StrictMicroSecondTimeBasedGuid
						.newGuid());

				ticketMaterialMap.setAddedOn(ticketMaterialMappingvo
						.getAddedOn());

				ticketMaterialMap.setModifiedOn(ticketMaterialMappingvo
						.getModifiedOn());

				id = (Long) hibernateTemplate.save(ticketMaterialMap);

				message = "success";
			}
		}
		if (message.equals("success") || id > 0)
		{

			return message;
		} else
		{
			return message;
		}

	}

	private TicketMaterialMapping commonData(
			TicketMaterialMappingVO ticketMaterialMappingvo)
	{
		
		TicketMaterialMapping ticketMaterialMapping = new TicketMaterialMapping();
		if (ticketMaterialMappingvo == null)
			return ticketMaterialMapping;
		
		log.info(" TicketMaterialMapping commonData " +ticketMaterialMappingvo.getActivityId()  );

		Long ticketId = ticketMaterialMappingvo.getTicketId();
		

		ticketMaterialMapping
				.setTicketId(ticketMaterialMappingvo.getTicketId());
		
		
       List<Long> userIdList = null;
        if ( AuthUtils.isFRUser() )
        {
        	userIdList = jdbcTemplate
        			.queryForObject(GET_USER_ID_USING_TICKETID, 
        					new Object[] { ticketId }, new WorkOrderMaterialMapper());
        }
        else
        {
        	userIdList = jdbcTemplate
        			.queryForObject(GET_USER_ID__USING_TICKETID__FROM_TicketTable_QUERY, 
        					new Object[] { ticketId }, new WorkOrderMaterialMapper());
        }
        
		if (userIdList != null && !userIdList.isEmpty())
		{

			ticketMaterialMapping.setAddedBy(userIdList.get(0));

			ticketMaterialMapping.setModifiedBy(userIdList.get(0));

			ticketMaterialMapping.setTicketId(ticketMaterialMappingvo
					.getTicketId());

			ticketMaterialMapping.setMaterialId(ticketMaterialMappingvo
					.getMaterialId());

			ticketMaterialMapping.setTotalConsumption(ticketMaterialMappingvo
					.getTotalConsumption());

			ticketMaterialMapping.setMacid(ticketMaterialMappingvo.getMacid());

			ticketMaterialMapping.setSerialNo(ticketMaterialMappingvo
					.getSerialNo());

			ticketMaterialMapping.setStartPoint(ticketMaterialMappingvo
					.getStartPoint());

			ticketMaterialMapping.setEndPoint(ticketMaterialMappingvo
					.getEndPoint());

			ticketMaterialMapping
					.setDrumNo(ticketMaterialMappingvo.getDrumNo());

			ticketMaterialMapping.setFiberType(ticketMaterialMappingvo
					.getFiberType());

			ticketMaterialMapping.setUoMid(ticketMaterialMappingvo.getUoMid());

			ticketMaterialMapping
					.setStatus(ticketMaterialMappingvo.getStatus());

			return ticketMaterialMapping;

		} else
		{
			log.info("corrosponding addeddto is not found ");

			return null;
		}

	}

	@Override
	public String addIntoMaterialConsumption(
			TicketMaterialMappingVO ticketMaterialMappingvo)
	{
		// TODO Auto-generated method stub

		// jdbcTemplate.queryForObject(sql, requiredType, args)
		return null;
	}

	@Override
	public APIResponse raiseMaterialRequest(
			List<MaterialRequestVO> materialRequestList)
	{

		MaterialRequest material;

		log.info("Inside raiseMaterialRequest() of MaterialsDAOImpl");

		if (materialRequestList == null)
		{

			log.info("Not able to raise material request input is null");

			return ResponseUtil.createNullParameterResponse();

		}

		try
		{

			for (int i = 0; i < materialRequestList.size(); i++)
			{

				Query query = new Query();

				query.addCriteria(Criteria.where("ticketId")
						.is(materialRequestList.get(i).getTicketId())
						.and("materialId")
						.is(materialRequestList.get(i).getMaterialId()));

				/*
				 * material = mongoOperations .findOne(query,
				 * MaterialRequest.class);
				 */

				long count = mongoOperations
						.count(query, MaterialRequest.class);

				if (count <= 0)
				{

					mongoOperations
							.save(materialRequestList.get(i), MongoCollections.MATERIAL_REQUEST_COLLECTION);

				} else
				{

					Update update = new Update();

					update.set("quantity", materialRequestList.get(i)
							.getQuantity());

					update.set("usrId", materialRequestList.get(i).getUsrId());

					update.set("modifiedON", materialRequestList.get(i)
							.getModifiedON());

					update.set("modifiedBy", materialRequestList.get(i)
							.getModifiedBy());

					mongoOperations
							.updateFirst(query, update, MaterialRequest.class);
				}
			}

		}

		catch (Exception e)
		{

			log.info("Not able to raise material request because of :"
					+ e.getMessage());

			return ResponseUtil.createSaveFailedResponse();

		}

		log.info("Material request info added successfully");

		return ResponseUtil.createSaveSuccessResponse();

	}

	@Override
	public List<TicketMaterialMapping> getMaterialsByTicketIDAndMaterailIds(
			Long ticketId, List<Integer> materialIds)
	{
		if(ticketId != null && ticketId > 0 && !materialIds.isEmpty())
		{
			try
			{
				List<TicketMaterialMapping> consumedMaterialDetails =	(List<TicketMaterialMapping>) hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery(GET_MATERIAL_COSUPSTION_DETAILS)
						.addEntity(TicketMaterialMapping.class)
						.setParameter("ticketId", ticketId)
						.setParameterList("materialIds", materialIds).list();
						
						return consumedMaterialDetails;
			}
			catch (Exception e) {
				log.info("Error while getting material details :"+ e.getMessage());
				return null;
			}
			
		}
		return null;
	
	}
	
	@Transactional
	@Override
	public void updateFrMaterialStatus( Long ticketId )
	{
		if( ticketId == null || ticketId.longValue() <= 0 )
			return;
		
		try{
			
			String UPDATE_FR_NOTIFIED_STATUS = "UPDATE FrMaterialDetail SET notifiedToBOR ="+FRStatus.MODEFIED
					+" WHERE ticketId = "+ ticketId ;
			
			int result = jdbcTemplate.update(UPDATE_FR_NOTIFIED_STATUS);
			if( result > 0 )
				log.info("material notification status updated successfully...");
				
		}catch(Exception e){
			log.error("Error while update fr material notified "
					+ "to BOR status :"+ e.getMessage());
		}
	}
	
	@Override
	public List<FrMaterialDetail> getMaterialsDetailByFrTicket(
			Long ticketId, String ...isNotifiedToBOR )
	{
		if( ticketId == null || ticketId.longValue() <= 0)
			return null;
		try
		{
			List<FrMaterialDetail> listMaterials = null;
			StringBuilder QUERY = new StringBuilder(GET_MATERIAL_COSUPSTION);
			
			if( isNotifiedToBOR != null && isNotifiedToBOR.length == 1 )
			{
				QUERY.append(" AND notifiedToBOR = ? ");
				
				listMaterials = jdbcTemplate.query(QUERY.toString(),
						new Object[] { ticketId, isNotifiedToBOR[0] },
						new BeanPropertyRowMapper<>(FrMaterialDetail.class));
			}
			else
			{
				listMaterials = jdbcTemplate.query(QUERY.toString(),
						new Object[] { ticketId },
						new BeanPropertyRowMapper<>(FrMaterialDetail.class));
			}
			return listMaterials;
		}
		catch (Exception e) {
			log.error("Error while getting material details :"+ e.getMessage());
			return null;
		}
	}

	@Override
	@Transactional
	public Integer addFrMaterialRequest( List<FrMaterialDetail> materialRequestVoList )
	{
		int updatedCount = 0;
		if( materialRequestVoList == null || materialRequestVoList.isEmpty())
			return updatedCount;
		try{
			for( FrMaterialDetail dbvo : materialRequestVoList)
			{
				hibernateTemplate.saveOrUpdate(dbvo);
				updatedCount++;
			}
		}catch(Exception e){
			log.info("Error while adding FR material request  :",e);
		}
		return updatedCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Integer addFrMaterialConsumption(List<FrMaterialDetail> materialConsume,
			List<Integer> materialIds)
	{
		int updatedCount = 0;
		if( materialConsume == null|| materialConsume.isEmpty() || materialIds == null)
			return updatedCount;
		try{
					
			List<FrMaterialDetail> dbvo = null;
			FrMaterialDetail perDbvo = null;
			for( FrMaterialDetail material : materialConsume)
			{
				dbvo = (List<FrMaterialDetail>)hibernateTemplate
						.find("from FrMaterialDetail fm where fm.ticket=? and fm.materialId=?"
								, material.getTicket(),material.getMaterialId());
				
				if( dbvo != null && dbvo.size() > 0 )
				{
					perDbvo = dbvo.get(0);
					perDbvo.setMaterialStartEndPoint(material.getMaterialStartEndPoint());
					perDbvo.setMaterialUniqNumber(material.getMaterialUniqNumber());
					
					perDbvo.setMaterialFiberType(material.getMaterialFiberType());
					perDbvo.setMaterialDrumNumber(material.getMaterialDrumNumber());
					
					perDbvo.setMaterialConsumedQty(material.getMaterialConsumedQty());
					perDbvo.setConsumptionAddedOn(material.getConsumptionAddedOn());
					
					perDbvo.setConsumptionAddedBy(material.getConsumptionAddedBy());
					perDbvo.setConsumptionModifiedOn(material.getConsumptionModifiedOn());
					perDbvo.setConsumptionModifiedBy(material.getConsumptionModifiedBy());
					perDbvo.setNotifiedToBOR(material.getNotifiedToBOR());
					perDbvo.setConsumedMaterial(material.getConsumedMaterial());
					
					hibernateTemplate.update(perDbvo);
					updatedCount++;
				}
				else
				{
					material.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					hibernateTemplate.save(material);
					updatedCount++;
				}
			}
			
			
		}catch(Exception e){
			log.info("Error while adding FR material comsumption  :",e);
		}
		return updatedCount;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.materials.MaterialsDAO#updateFrMaterialRequest(java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Integer updateFrMaterialRequest(
			List<FrMaterialDetail> materialRequestVoList)
	{
		int updatedCount = 0;
		if( materialRequestVoList == null || materialRequestVoList.isEmpty())
			return updatedCount;
		try{
			
			List<FrMaterialDetail> dbvo = null;
			FrMaterialDetail perDbvo = null;
			for( FrMaterialDetail material : materialRequestVoList)
			{
				dbvo = (List<FrMaterialDetail>)hibernateTemplate
						.find("from FrMaterialDetail fm where fm.ticket=? and fm.materialId=?"
								, material.getTicket(),material.getMaterialId());
				
				if( dbvo != null && dbvo.size() > 0 )
				{
					perDbvo = dbvo.get(0);
					
					perDbvo.setMaterialRequested(material.getMaterialRequested());
					perDbvo.setMaterialRequestQty(material.getMaterialRequestQty());
					perDbvo.setMaterialUnit(material.getMaterialUnit());
					
					perDbvo.setModifiedOn(new Date());
					perDbvo.setModifiedBy(AuthUtils.getCurrentUserLoginId()+"");
					perDbvo.setNotifiedToBOR(material.getNotifiedToBOR());
					
					hibernateTemplate.update(perDbvo);
					updatedCount++;
				}
				else
				{
					hibernateTemplate.save(material);
					updatedCount++;
				}
				
			}
				
		}catch(Exception e){
			log.info("Error while updating FR material request  :",e);
		}
		return updatedCount;
	}

}

class WorkOrderMaterialMapper implements RowMapper<List<Long>>
{

	@Override
	public List<Long> mapRow(ResultSet resultSet, int value)
			throws SQLException
	{

		List<Long> list = new ArrayList<>();

		list.add(Long.valueOf(resultSet.getString(1)));

		return list;

	}

}
