package com.cupola.fwmp.dao.materials;

import java.util.List;

import com.cupola.fwmp.persistance.entities.FrMaterialDetail;
import com.cupola.fwmp.persistance.entities.Materials;
import com.cupola.fwmp.persistance.entities.TicketMaterialMapping;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.MaterialRequestVO;
import com.cupola.fwmp.vo.MaterialsVO;
import com.cupola.fwmp.vo.TicketMaterialMappingVO;

public interface MaterialsDAO
{

	public Materials addMaterials(Materials materials);

	public Materials getMaterialsById(Long id);

	public List<MaterialsVO> getAllMaterials();

	public Materials deleteMaterials(Long id);

	public Materials updateMaterials(Materials materials);

	String UpdateMaterialConsumption(
			TicketMaterialMappingVO workOrderMaterialMappingVO);

	String addIntoMaterialConsumption(
			TicketMaterialMappingVO workOrderMaterialMappingVO);

	public APIResponse updateMultipleMaterialConsumtion(
			TicketMaterialMappingVO ticketMaterialMappingVO);

	public APIResponse raiseMaterialRequest(
			List<MaterialRequestVO> materialRequestList);
	
	List<TicketMaterialMapping> getMaterialsByTicketIDAndMaterailIds(Long ticketId, List<Integer> materialIds);

	public List<FrMaterialDetail> getMaterialsDetailByFrTicket(Long ticketId, String ...isNotifiedToBOR );
	
	
	public Integer addFrMaterialRequest(  List<FrMaterialDetail> materialRequestVoList );
	public Integer addFrMaterialConsumption( List<FrMaterialDetail> materialConsume,List<Integer> materialIds );

	/**
	 * @author ashraf
	 * 
	 * @param materialRequestVoList
	 * @return
	 */
	public Integer updateFrMaterialRequest(
			List<FrMaterialDetail> materialRequestVoList);

	/**@author ashraf
	 * void
	 * @param ticketId
	 */
	void updateFrMaterialStatus(Long ticketId);
}