package com.cupola.fwmp.dao.mongo.fr;

import java.util.List;
import java.util.Set;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.vo.fr.FlowActionData;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;

public interface FrActionDao
{

	public void submitFrActionData(FlowActionData actionData);

	public void dumpSymptomDetail(FrTicketEtrDataVo etrDataVo);

	public void dumpSymptomDetailInBulk( Set<FrTicketEtrDataVo> etrResultSet );

	public void dumpFailedWODetailInBulk( List<MQWorkorderVO> workOrder  );

}