package com.cupola.fwmp.dao.mongo.fr;

import java.util.Date;

public class FlowRequestData
{
	private String RequestType;
	private Date RequestTime;
	private Long raisedBy;
	private String status;
	
	public FlowRequestData(){
	}
	
	public FlowRequestData(String requestType, Date requestTime, Long raisedBy,
			String status) {
		super();
		RequestType = requestType;
		RequestTime = requestTime;
		this.raisedBy = raisedBy;
		this.status = status;
	}
	
	public String getRequestType() {
		return RequestType;
	}
	public void setRequestType(String requestType) {
		RequestType = requestType;
	}
	public Date getRequestTime() {
		return RequestTime;
	}
	public void setRequestTime(Date requestTime) {
		RequestTime = requestTime;
	}
	public Long getRaisedBy() {
		return raisedBy;
	}
	public void setRaisedBy(Long raisedBy) {
		this.raisedBy = raisedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
