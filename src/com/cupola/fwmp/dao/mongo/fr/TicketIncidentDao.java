package com.cupola.fwmp.dao.mongo.fr;

import java.util.List;

import com.cupola.fwmp.dao.mongo.vo.fr.AssetsVo;
import com.cupola.fwmp.dao.mongo.vo.fr.FlowRequestDataVo;
import com.cupola.fwmp.dao.mongo.vo.fr.TicketIncidentVo;

public interface TicketIncidentDao 
{
	public void createIncident(TicketIncidentVo dbvo);
	public void saveOrupdateIncident(TicketIncidentVo dbvo);
	public void updateIncident(Long ticketId);
	public List<?> getAllIncident();
	
	public void saveOrUpdateFlowRequest(FlowRequestDataVo dbvo);
//	public List<?> getAllFlowRequest();  
	
	public void createAssets(AssetsVo asset);
	public List<AssetsVo> getAllAssetsByAssignedUser(List<Long> userId);
	public List<AssetsVo> getAllAssets();
	void assignAssets(List<AssetsVo> assets);
}
