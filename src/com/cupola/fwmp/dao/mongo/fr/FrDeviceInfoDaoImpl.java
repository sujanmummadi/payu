package com.cupola.fwmp.dao.mongo.fr;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;

public class FrDeviceInfoDaoImpl implements FrDeviceInfoDao
{
	private Logger LOGGER = Logger.getLogger(FrDeviceInfoDaoImpl.class);
	
	@Autowired
	private MongoOperations mongoOperation;
	
	@Override
	public Integer createDevice(FrDeviceVo device) 
	{
		int result = 0;
		if( device == null || device.getId() == null || device.getId() < 0 )
		{
			LOGGER.info("Can't create device info for ticket : "+device.getFxName());
			return result;
		}
		
		mongoOperation.save(device);
		result++;
		return result;
	}

	@Override
	public FrDeviceVo getDeviceInfoByTicket(Long ticketId)
	{
		LOGGER.debug("  inside  getDeviceInfoByTicket method : " +ticketId);
		if ( ticketId == null || ticketId <= 0 )
		{
			LOGGER.info("Can't fetch device info for ticket : "+ticketId);
			return null;
		}
		FrDeviceVo deviceInfo = null;
		try 
		{
			Query query = new Query();
			query.addCriteria(Criteria.where("id").is(ticketId));
			
			deviceInfo = mongoOperation.findOne(query, FrDeviceVo.class);
			return deviceInfo;
		} 
		catch (Exception e)
		{
			LOGGER.info("Error occure while fetch device info for ticket : "+ticketId);
		}
		return deviceInfo;
	}

}
