package com.cupola.fwmp.dao.mongo.fr;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "FrFlowRequestQueue")
public class FrFlowRequestQueue 
{
	@Id
	private Long id;
	
	private Long ticketId;
	private Long engineerId;
	
	private List<FlowRequestData> frFlowRequestData;
	
	public FrFlowRequestQueue(){
	}

	public FrFlowRequestQueue(Long id, Long ticketId,Long engineerId,
			List<FlowRequestData> frFlowRequestData) {
		super();
		this.id = id;
		this.ticketId = ticketId;
		this.frFlowRequestData = frFlowRequestData;
		this.engineerId = engineerId;
	}

	public Long getEngineerId() {
		return engineerId;
	}

	public void setEngineerId(Long engineerId) {
		this.engineerId = engineerId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public List<FlowRequestData> getFrFlowRequestData() {
		return frFlowRequestData;
	}

	public void setFrFlowRequestData(List<FlowRequestData> frFlowRequestData) {
		this.frFlowRequestData = frFlowRequestData;
	}
	
}
