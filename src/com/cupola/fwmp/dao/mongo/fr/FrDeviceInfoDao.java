package com.cupola.fwmp.dao.mongo.fr;

import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;

public interface FrDeviceInfoDao
{
	public Integer createDevice(FrDeviceVo device);
	public FrDeviceVo getDeviceInfoByTicket(Long ticketId);
}
