package com.cupola.fwmp.dao.mongo.fr;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.fr.FlowActionData;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;

public class FrActionDaoImpl implements FrActionDao
{

	private Logger LOGGER = Logger.getLogger(FrActionDaoImpl.class);

	@Autowired
	MongoOperations mongoOperations;

	public void submitFrActionData(FlowActionData actionData)
	{
		LOGGER.info("Adding FlowActionData with data " + actionData);
		if (actionData == null)
			return;
		actionData.setUserId(AuthUtils.getCurrentUserId());
		actionData.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		mongoOperations.insert(actionData);
	}

	@Override
	public void dumpSymptomDetail( FrTicketEtrDataVo etrDataVo )
	{
		if( etrDataVo == null )
			return;
		LOGGER.debug("inside dumpSymptomDetail " + etrDataVo.getCityCode());
		mongoOperations.insert( etrDataVo );
	}
	
	@Override
	public void dumpSymptomDetailInBulk( Set<FrTicketEtrDataVo> etrResultSet  )
	{
		if( etrResultSet == null || etrResultSet.isEmpty() )
			return;
		mongoOperations.insertAll( etrResultSet );
	}

	@Override
	public void dumpFailedWODetailInBulk( List<MQWorkorderVO> workOrder  )
	{
		if( workOrder == null || workOrder.isEmpty() )
			return;
		mongoOperations.insertAll( workOrder );
	}

}
