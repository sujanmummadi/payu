package com.cupola.fwmp.dao.mongo.fr;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TicketIncidentInfo")
public class TicketIncidentInfo
{
	@Id
	private Long id;
	
	private String deviceType;
	private String macAddress;
	private String ipAddress;
	private String remarks;
	private String serialNumber;
	private String incidentType;
	private String incidentDesc;
	private Long ticketId;
	private Long engineerId;
	private Long lat;
	private Long lon;
	
	
	public TicketIncidentInfo(Long id, String deviceType, Long ticketId,Long engineerId) {
		super();
		this.id = id;
		this.deviceType = deviceType;
		this.ticketId = ticketId;
		this.engineerId = engineerId;
	}
	
	public TicketIncidentInfo(Long id, String deviceType, String macAddress,
			String ipAddress, String remarks, String serialNumber,
			String incidentType, String incidentDesc, Long ticketId,Long engineerId,
			Long lat,Long lon) {
		super();
		this.id = id;
		this.deviceType = deviceType;
		this.macAddress = macAddress;
		this.ipAddress = ipAddress;
		this.remarks = remarks;
		this.serialNumber = serialNumber;
		this.incidentType = incidentType;
		this.incidentDesc = incidentDesc;
		this.ticketId = ticketId;
		this.engineerId = engineerId;
		this.lat = lat;
		this.lon = lon;
	}
	
	public Long getEngineerId() {
		return engineerId;
	}

	public void setEngineerId(Long engineerId) {
		this.engineerId = engineerId;
	}

	public TicketIncidentInfo(){}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getIncidentType() {
		return incidentType;
	}
	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}
	public String getIncidentDesc() {
		return incidentDesc;
	}
	public void setIncidentDesc(String incidentDesc) {
		this.incidentDesc = incidentDesc;
	}
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getLat() {
		return lat;
	}

	public void setLat(Long lat) {
		this.lat = lat;
	}

	public Long getLon() {
		return lon;
	}

	public void setLon(Long lon) {
		this.lon = lon;
	}
	
}
