package com.cupola.fwmp.dao.mongo.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.fr.EmailCorporateDAO;
import com.cupola.fwmp.dao.fr.FrTicketDetailsDao;
import com.cupola.fwmp.dao.mongo.documents.CustomerDocument;
import com.cupola.fwmp.dao.mongo.vo.fr.AssetsVo;
import com.cupola.fwmp.dao.mongo.vo.fr.FlowRequestDataVo;
import com.cupola.fwmp.dao.mongo.vo.fr.TicketIncidentVo;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.fr.EmailCorporateVo;
import com.cupola.fwmp.vo.fr.FrTicketDetailsVO;

public class TicketIncidentDaoImpl implements TicketIncidentDao
{
 
	private Logger LOGGER = Logger.getLogger(TicketIncidentDaoImpl.class);
	
	@Autowired 
	FrTicketDetailsDao frTicketDetailsDao;
	
	@Autowired
	MongoOperations mongoOperations; 
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	EmailCorporateDAO emailCorporateDAO;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	DefinitionCoreServiceImpl definitionCoreService;
	
	@Autowired
	private UserDao userDao;
	
	@Value("${fwmp.legal.team.subject}")
	private String sub;
	
	public final static String EMAIL_LEGAL_KEY = "legal.dept.emailIds.";
	
	@Override
	public void createIncident(TicketIncidentVo dbvo)
	{
		if( dbvo == null)
			return;
		
		LOGGER.debug(" createIncident "  + dbvo.getMacAddress());
		TicketIncidentInfo info = new TicketIncidentInfo();
		BeanUtils.copyProperties(dbvo, info);
		info.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		mongoOperations.insert(info);
		try
		{
			sendMailToLegalTeam(dbvo);
		} catch (Exception e)
		{
			LOGGER.error("mail not  sent successfully "+e.getMessage());
		}
	}

	private void sendMailToLegalTeam(TicketIncidentVo dbvo) {
		//String sub = "New Legal Team Request";
          if( dbvo == null)
			return;
		
		LOGGER.debug(" inside sendMailToLegalTeam "  +dbvo.getMacAddress());
		Map cc = null;
		Map bcc = null;
		String templateName = "legal_team_incident_report.vm";
		
		TicketFilter filter = new TicketFilter();
		
		filter.setTicketId(dbvo.getTicketId());
		List<FrTicketDetailsVO> ticket = frTicketDetailsDao.getFrTicketWithDetails(filter);
		if(ticket == null || ticket.isEmpty())
		{
			LOGGER.info("No ticket found for " + dbvo.getTicketId());
			return;
		}
		FrTicketDetailsVO vo = ticket.get(0);
		String cityCode =  vo.getCity();
		String to = DefinitionCoreServiceImpl.emailCorporateDef
				.get(EMAIL_LEGAL_KEY + cityCode.toLowerCase());
		
		LOGGER.info("Sending mail to :"+to);
	
		LOGGER.info("Sending mail  with ticket id :"+vo.getTicketId());
		mailService
				.sendMiMeMail(sub, to, cc, bcc, templateName, vo);
		LOGGER.info("mail send successfully with ticket id :"+vo.getTicketId());

	/*	EmailCorporateVo emailCorporateVo = new EmailCorporateVo();
		emailCorporateVo.setTicketId(dbvo.getTicketId());
		emailCorporateVo.setEmailType(FWMPConstant.EmailType.LEGAL);
		emailCorporateVo.setCityId(vo.getCityId());
		emailCorporateVo.setStatus(FWMPConstant.EmailStatus.SENT);

		emailCorporateDAO.add(emailCorporateVo);*/

	}

	@Override
	public void saveOrupdateIncident(TicketIncidentVo dbvo)
	{
		if( dbvo == null )
			return;
		
		LOGGER.debug(" saveOrupdateIncident "+dbvo.getMacAddress()  );
		if( dbvo.getId() == null || dbvo.getId() <= 0)
			this.createIncident(dbvo);
		else
		{
			TicketIncidentInfo info = new TicketIncidentInfo();
			BeanUtils.copyProperties(dbvo, info);
		/*	
			Query query = new Query().
					addCriteria(Criteria.where("deviceType").
							is(info.getDeviceType()));
			
			TicketIncidentInfo dbObject = mongoOperations.findOne(query,TicketIncidentInfo .class);*/
			mongoOperations.save(info);
		}
	}

	@Override
	public List<?> getAllIncident() {
		
		LOGGER.debug(" saveOrupdateIncident "  );
		return null;
	}

	@Override
	public void updateIncident(Long ticketId) {
		
		
	}

	@Override
	public void saveOrUpdateFlowRequest(FlowRequestDataVo dbvo)
	{
		if( dbvo == null)
			return;
		
		LOGGER.debug(" inside saveOrUpdateFlowRequest " +dbvo.getTicketId() );
		FrFlowRequestQueue queue = new FrFlowRequestQueue();
		FlowRequestData data = new FlowRequestData(dbvo.getRequestType(), 
				dbvo.getRequestTime(), dbvo.getRaisedBy(), dbvo.getStatus());
		
		queue.setFrFlowRequestData(Arrays.asList(data));
		queue.setTicketId(dbvo.getTicketId());
		if( dbvo.getId() == null || dbvo.getId() <= 0)
		{
			queue.setId(StrictMicroSecondTimeBasedGuid.newGuid()); 
			mongoOperations.insert(queue);
			LOGGER.info("Flow request successfully submitted...");
		}
		else
		{
			Query query = new Query().
					addCriteria(Criteria.where("ticketId").
							is(dbvo.getTicketId()));
			
			FrFlowRequestQueue queue2 = mongoOperations.findOne(query, FrFlowRequestQueue.class);
			List<FlowRequestData> flowData = queue2.getFrFlowRequestData();
			flowData.add(data);
			queue2.setFrFlowRequestData(flowData);
			Update update = new Update();
			update.set("frFlowRequestData", flowData);
			mongoOperations.updateFirst(query, update, FrFlowRequestQueue.class);
			
			LOGGER.info("Flow request successfully updated...");
		}
	}
	
	@Override
	public void createAssets(AssetsVo assets)
	{
		
		if( assets != null )
		{
			LOGGER.debug(" inside createAssets "  +assets.getAssignedBy());
			assets.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			mongoOperations.insert(assets);
		}
	}
	
	@Override
	public void assignAssets(List<AssetsVo> assets)
	{
		
		if( assets != null )
		{
			LOGGER.debug(" inside assignAssets " + assets.size());
			
			mongoOperations
			.remove (new Query().addCriteria(Criteria
					.where("assignedBy")
					.is(assets.get(0).getAssignedBy())
					 
					 ), AssetsVo.class);
			
			for (Iterator iterator = assets.iterator(); iterator.hasNext();)
			{
				AssetsVo assetsVo = (AssetsVo) iterator.next();
				assetsVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			}
			mongoOperations.insertAll(assets);
		
		}
	}
	
	@Override
	public List<AssetsVo> getAllAssetsByAssignedUser(List<Long> userId)
	{
	  if(userId == null || userId .isEmpty())
			return null;
		
		LOGGER.debug(" inside getAllAssetsByAssignedUser "   );
		if( userId != null)
		{
			Query query = new Query().
					addCriteria(Criteria.where("assignedBy").
							in(userId));
			List<AssetsVo> assetVo = mongoOperations.find(query, AssetsVo.class);
			return getAllNenotHavingAssets(assetVo,userId);
		}
		return null;
	}
	
	@Override
	public List<AssetsVo> getAllAssets()
	{
		LOGGER.debug(" inside getAllAssets "  );
		return mongoOperations.findAll(AssetsVo.class);
	}
	
	private  List<AssetsVo> getAllNenotHavingAssets(List<AssetsVo> assets,List<Long> userIds)
	{
		if( userIds == null || assets == null)
			return null;
		LOGGER.debug(" inside getAllNenotHavingAssets "   );
		for (Long userId : userIds)
		{
			Map<Long,String> neUsers = userDao.getNestedReportingUsers(userId, null, null, null);
			
			for(AssetsVo assetData : assets )
			{
					neUsers.remove(assetData.getAssignedTo());
			}
			for( Map.Entry< Long, String> entry : neUsers.entrySet())
			{
				if (AuthUtils.isFrCxDownNEUser(entry.getKey())
						|| AuthUtils.isFrCxUpNEUser(entry.getKey()))
					assets.add(new AssetsVo(entry.getKey(), entry.getValue(), 0, 0, 0,
						userId));
			}
		LOGGER.debug(" Exit From getAllNenotHavingAssets "   );
	}
	return  assets;
}
}
