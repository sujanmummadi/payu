package com.cupola.fwmp.dao.mongo.logger;

import java.util.List;

import com.cupola.fwmp.service.domain.logger.TicketLog;

public interface LoggerDao
{
	public void insertLog(TicketLog ticketLog);

	List<TicketLog> getLog(Long ticketId);
	
}
