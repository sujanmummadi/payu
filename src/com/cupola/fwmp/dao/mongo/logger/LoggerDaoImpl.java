package com.cupola.fwmp.dao.mongo.logger;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.UserVo;

public class LoggerDaoImpl implements LoggerDao
{
	private static Logger LOGGER = Logger.getLogger(LoggerDaoImpl.class.getName());
	private static String MESSAGE_FORMAT = "[_DATETIME_][_USERID_][_CATEGORY_]";
	
	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	UserDao userDao;

	@Override
	public void insertLog(TicketLog ticketLog)
	{
		LOGGER.debug("Checked in the log " + ticketLog);
		mongoOperations.save(ticketLog);
		
	}
	
	@Override
	public List<TicketLog> getLog(Long ticketId)
	{
		LOGGER.debug("getLog " + ticketId);
		Query q = new Query().with(new Sort(Sort.Direction.DESC, "addedOn"));
		q.limit(50);
		
		if (ticketId == null || ticketId <= 0)
			return xformToLoggerView(mongoOperations.find(q,TicketLog.class));

		List<TicketLog> logs = mongoOperations
				.find(q.addCriteria(Criteria.where("ticketId")
						.is(ticketId)), TicketLog.class);
		if (logs != null && !logs.isEmpty() )
			{
			LOGGER.debug(logs.size() + " logs Found for ticket " + ticketId);
			
			}
		else
		{
			LOGGER.info("No logs Found for ticket   " + ticketId);
		}
		LOGGER.debug(" Exit From getLog " + ticketId);
		return xformToLoggerView(logs);

	}
	
	private List<TicketLog>  xformToLoggerView(List<TicketLog> logs)
	{

		if(logs == null)
			return null;
		
		LOGGER.debug("xformToLoggerView " +logs.size() );
		for (Iterator iterator = logs.iterator(); iterator.hasNext();)
		{
			TicketLog ticketLog = (TicketLog) iterator.next();
			ticketLog.setMessage(createDisplayMessage(ticketLog));
		}
		
		LOGGER.debug(" Exit From xformToLoggerView " +logs.size() );
		return logs;
	}
	
	private String createDisplayMessage(TicketLog ticketLog)
	{
		StringBuilder message = new StringBuilder("");
		
		if(ticketLog == null)
			return "";
		
		LOGGER.debug(" inside createDisplayMessage "+ ticketLog.getActivityName()  );
		message.append("[");
		message.append(GenericUtil.convertToUiDateFormat( ticketLog.getAddedOn()));
		message.append("] ");
		
		message.append("[");
//		String userName = userDao.getUserNameByUserId(ticketLog.getAddedBy());
		UserVo vo = null;
		String userName = null;
		if (ticketLog.getAddedBy() == FWMPConstant.SYSTEM_ENGINE)
		{
			userName = "SYSTEM";
		} else
		{
			vo = userDao.getUserById(ticketLog.getAddedBy());
			if(vo != null)
				userName = vo.getFirstName();
		}
		
		
		if(userName == null)
			userName = "User Not Available";
		message.append(userName);
		message.append("] ");
		
		message.append(ticketLog.getCategory());
		
		if(ticketLog.getAdditionalAttributeMap() != null && 
				ticketLog.getAdditionalAttributeMap().containsKey(TicketUpdateConstant.ASSIGNED_TO_KEY) )
		{
				 vo = userDao.getUserById(
						 Long.parseLong(ticketLog.getAdditionalAttributeMap().
								 get(TicketUpdateConstant.ASSIGNED_TO_KEY)));
				 userName = vo.getFirstName();
				 if(userName == null)
					userName = "User Not Available";
				 message.append(" [");
				 message.append(userName);
				 message.append("] ");
			
		}
	 
		
		
		if(ticketLog.getActivityName() != null)
		{
			message.append(" :: "+ticketLog.getActivityName());
			
		}
		
		if(ticketLog.getActivityValue() != null)
		{
			message.append(" :: ");
			if (ticketLog.getActivityId() > 0) {
				int statusId = Integer.valueOf(ticketLog.getActivityValue());
				message.append(TicketStatus.statusDisplayValue.get(statusId));
			}
			else
				message.append(ticketLog.getActivityValue());
		}
		if(ticketLog.getAdditionalAttributeMap() != null && 
				ticketLog.getAdditionalAttributeMap().containsKey(TicketUpdateConstant.NOTE_KEY) )
		{
				 message.append(" [");
				 message.append(ticketLog.getAdditionalAttributeMap().get(TicketUpdateConstant.NOTE_KEY));
				 message.append("] ");
			
		}
		LOGGER.debug(" Exit From createDisplayMessage "+ ticketLog.getActivityName()  );
		return message.toString();
	}
}
