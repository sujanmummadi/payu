package com.cupola.fwmp.dao.mongo.logger;

import com.cupola.fwmp.vo.UserLoginStatusVo;

public interface LoginStatusDao
{

	void insertLoginStatus(UserLoginStatusVo userLoginStatusVo);

}
