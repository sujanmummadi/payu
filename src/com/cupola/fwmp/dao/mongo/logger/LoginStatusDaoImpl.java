package com.cupola.fwmp.dao.mongo.logger;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import com.cupola.fwmp.dao.mongo.vo.LoginStatusVO;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.UserLoginStatusVo;

public class LoginStatusDaoImpl implements LoginStatusDao
{
	private static Logger LOGGER = Logger.getLogger(LoginStatusDaoImpl.class.getName());
	
	@Autowired
	MongoOperations mongoOperations;

	@Override
	public void insertLoginStatus(UserLoginStatusVo userLoginStatusVo)
	{
		LOGGER.debug(" inside insertLoginStatus "  +userLoginStatusVo.getUserId());
		
		LoginStatusVO vo = new LoginStatusVO();
		BeanUtils.copyProperties(userLoginStatusVo, vo);
		vo.setUpdateTime(System.currentTimeMillis());
		vo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		mongoOperations.save(vo);
		
	}
	
	 
}
