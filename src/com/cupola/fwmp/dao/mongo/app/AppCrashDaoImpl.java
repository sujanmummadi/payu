/**
 * 
 */
package com.cupola.fwmp.dao.mongo.app;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.tools.AppCrashVo;

/**
 * @author aditya
 *
 */
public class AppCrashDaoImpl implements AppCrashDao
{
	private static Logger LOGGER = Logger.getLogger(AppCrashDaoImpl.class
			.getName());
	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public void recordAppCrashServcie(AppCrashVo appCrashVo)
	{
		if(appCrashVo == null)
			return;
		
		LOGGER.info(" recordAppCrashServcie " + appCrashVo.getProspectNo());
		appCrashVo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		appCrashVo.setAddedOn(new Date());
		
		mongoTemplate.insert(appCrashVo);
	}

	@Override
	public void sendAppCrashNotification(AppCrashVo appCrashVo)
	{
		// TODO Auto-generated method stub
		
	}

}
