/**
 * 
 */
package com.cupola.fwmp.dao.mongo.app;

import com.cupola.fwmp.vo.tools.AppCrashVo;

/**
 * @author aditya
 *
 */
public interface AppCrashDao
{
	void recordAppCrashServcie(AppCrashVo appCrashVo);

	void sendAppCrashNotification(AppCrashVo appCrashVo);
}
