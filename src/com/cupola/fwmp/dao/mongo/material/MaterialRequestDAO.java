package com.cupola.fwmp.dao.mongo.material;

import java.util.List;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.fr.MaterialRequestVo;



public interface MaterialRequestDAO
{
 public void addMaterialRequest(List<MaterialRequestVo> materialRequestVoList);
 
 public List<MaterialRequestVo> getMaterialRequestDetails(Long ticketId);
 
 List<MaterialRequestVo> getMaterialRequestDetailsByTicketIdWithNotModefied(Long ticketId);
 
 void  updateMaterialRequestDetailsByTicketIdWithtModefiedStatus(Long ticketId , List<Integer> materialIds);
 
 //public APIResponse getMaterialRequestDetailsByTicketId(Long ticketId);
 
}
