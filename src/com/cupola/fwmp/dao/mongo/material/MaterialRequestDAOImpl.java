package com.cupola.fwmp.dao.mongo.material;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.vo.fr.MaterialRequestVo;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;

public class MaterialRequestDAOImpl implements MaterialRequestDAO
{

	/*
	 * @Autowired MongoOperations MongoOperations;
	 */

	@Autowired
	MongoTemplate mongoTemplate;

	private static Logger LOGGER = Logger
			.getLogger(MaterialRequestDAOImpl.class.getName());

	// addMaterialRequest() takes the material request as a input and save into
	// the mongo table.

	@Override
	public void addMaterialRequest(List<MaterialRequestVo> materialRequestVoList)

	{
		
		
		LOGGER.info("In MaterialRequestDAOImpl::List of materialRequestVoList "+materialRequestVoList);
		
		if (materialRequestVoList != null && !materialRequestVoList.isEmpty())
		{
			
			LOGGER.info("In MaterialRequestDAOImpl::materialRequestvo for mongo DB is :::::"
					+materialRequestVoList);

			mongoTemplate.insertAll(materialRequestVoList);
		}

	}

	// getMaterialRequestDetails() this services takes the ticketId as a input
	// and display the all material
	// request details of that ticket.If not provide ticketId ,it display all
	// materials details.

	@Override
	public List<MaterialRequestVo> getMaterialRequestDetails(Long ticketId)
	{
		LOGGER.debug("In MaterialRequestDAOImpl::getMaterialRequestDetails "+ticketId);
		
		Query q = new Query().with(new Sort(Sort.Direction.DESC, "materialRequestOn"));
		q.limit(25);

		Query materialRequestSearchByTicketId = new Query(Criteria
				.where("ticketId").is(ticketId));

		if (ticketId == null || ticketId <= 0)
		{
         LOGGER.info("Calling if conditsn for ticket id in DAO impl ticketId ::::"+ticketId);
			List<MaterialRequestVo> allMaterialList = mongoTemplate
					.findAll(MaterialRequestVo.class);

			return allMaterialList;

		} else
		{
			 LOGGER.info("Calling else conditsn for ticket id in DAO impl ticketId ::::"+ticketId);
			List<MaterialRequestVo> allMaterialReqOnTicketId = mongoTemplate
					.find(materialRequestSearchByTicketId, MaterialRequestVo.class);

			LOGGER.info("getMaterialDetails() show all material requested data as::"
					+ allMaterialReqOnTicketId);

			return allMaterialReqOnTicketId;
		}
	}

	@Override
	public List<MaterialRequestVo> getMaterialRequestDetailsByTicketIdWithNotModefied(
			Long ticketId)
	{
		LOGGER.debug("In MaterialRequestDAOImpl::getMaterialRequestDetailsByTicketIdWithNotModefied "+ticketId);
		if (ticketId != null && ticketId <= 0)
			return null;
		Query materialRequestWithNotModefied = new Query(Criteria
				.where("ticketId").is(ticketId).and("materialRequestValue")
				.is(true).and("status").is(FRStatus.NOT_MODEFIED));
		List<MaterialRequestVo> allMaterialRequestWithNotModefied = mongoTemplate
				.find(materialRequestWithNotModefied, MaterialRequestVo.class);
		LOGGER.debug("Exit from MaterialRequestDAOImpl::getMaterialRequestDetailsByTicketIdWithNotModefied "+ticketId);
		return allMaterialRequestWithNotModefied;

	}

	@Override
	public  void  updateMaterialRequestDetailsByTicketIdWithtModefiedStatus(Long ticketId , List<Integer> materialIds)
	{
		LOGGER.debug("In MaterialRequestDAOImpl::updateMaterialRequestDetailsByTicketIdWithtModefiedStatus "+ticketId);
		if(ticketId != null)
		{
			Query query = new Query(Criteria.where("ticketId").is(ticketId).and("materialRequestId").in(materialIds));
			Update update = new Update();
			update.set("status",FRStatus.MODEFIED );
			mongoTemplate.updateMulti(query, update, MaterialRequestVo.class);
		}
	}

}
