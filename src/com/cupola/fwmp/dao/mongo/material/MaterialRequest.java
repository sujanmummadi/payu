package com.cupola.fwmp.dao.mongo.material;

import java.math.BigInteger;

/**
 * 
 * @author Mahesh Chouhan
 * 
 * */

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MaterialRequest")
public class MaterialRequest {
	@Id
	private BigInteger materialRequestId;
	private Long ticketId;
	private Long materialId;
	private int quantity;
	private Long usrId;
	private Date addedON;
	private Long addedBy;
	private Date modifiedON;
	private Long modifiedBy;
	public BigInteger getMaterialRequestId() {
		return materialRequestId;
	}
	public void setMaterialRequestId(BigInteger materialRequestId) {
		this.materialRequestId = materialRequestId;
	}
	public Long getTicketId() {
		return ticketId;
	}
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	public Long getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Long getUsrId() {
		return usrId;
	}
	public void setUsrId(Long usrId) {
		this.usrId = usrId;
	}
	public Date getAddedON() {
		return addedON;
	}
	public void setAddedON(Date addedON) {
		this.addedON = addedON;
	}
	public Long getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(Long addedBy) {
		this.addedBy = addedBy;
	}
	public Date getModifiedON() {
		return modifiedON;
	}
	public void setModifiedON(Date modifiedON) {
		this.modifiedON = modifiedON;
	}
	public Long getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
