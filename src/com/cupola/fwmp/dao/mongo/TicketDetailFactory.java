package com.cupola.fwmp.dao.mongo;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cupola.fwmp.dao.mongo.vo.histroy.ActivityData;
import com.cupola.fwmp.dao.mongo.vo.histroy.MetaData;
import com.cupola.fwmp.dao.mongo.vo.histroy.SubActivityData;
import com.cupola.fwmp.dao.mongo.vo.histroy.TicketDetailsDbvo;
import com.cupola.fwmp.dao.mongo.vo.histroy.WorkStageData;
import com.cupola.fwmp.dao.subActivity.SubActivityDAO;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.SubActivityVo;
import com.cupola.fwmp.vo.WorkStageVo;

@Component
public class TicketDetailFactory
{
	private static final Logger logger = LogManager
			.getLogger(TicketDetailFactory.class.getName());

	public TicketDetailsDbvo createNewHistoryData(Long workOrderTypeId,
			Long ticketId, Long workOrderId)
	{
		TicketDetailsDbvo data = new TicketDetailsDbvo();
		data.setMetaData(createMetaData(ticketId, workOrderId + ""));
		data.setWorkStages(createWorkStageList(workOrderTypeId));
		return data;

	}

	private List<WorkStageData> createWorkStageList(Long workOrderTypeId)
	{
		List<WorkStageData> workStages = new LinkedList<WorkStageData>();
		WorkStageData stage = null;
		MetaData metaData = null;
		List<WorkStageVo> stages = WorkOrderDefinitionCache
				.getWorkStagesForOrderTypeByTypeId(workOrderTypeId);
		for (Iterator iterator = stages.iterator(); iterator.hasNext();)
		{
			stage = new WorkStageData();
			WorkStageVo workStageVo = (WorkStageVo) iterator.next();
			metaData = new MetaData();
			updateWorkStageMetaData(metaData, workStageVo);
			stage.setActivities(createActivityList(workStageVo.getId()));
			stage.setMetaData(metaData);
			workStages.add(stage);
		}

		
		return workStages;
	}

	private  List<ActivityData> createActivityList(Long workStageId)
	{
		
		logger.info("Getting activities for ws "+workStageId);
		List<ActivityData> dataList = new LinkedList<ActivityData>();
		
		List<ActivityVo> activitiesVo = WorkOrderDefinitionCache.getActivitiesForWorkStage(workStageId);
		logger.info("Found  activities "+activitiesVo);
		ActivityData data = null;
		MetaData metaData = null;
		for (Iterator iterator = activitiesVo.iterator(); iterator.hasNext();)
		{
			data = new ActivityData();
			ActivityVo vo = (ActivityVo) iterator.next();
			metaData  = new MetaData();
			updateActivityMetaData(metaData, vo);
			data.setSubActivities(createSubActivityList(vo.getId()));
			data.setMetaData(metaData);
			dataList.add(data);
		}
		return dataList;
	}

	private List<SubActivityData> createSubActivityList(Long activityId)
	{
		
		logger.info("Getting sub activities for activity "+activityId);
		List<SubActivityData> dataList = new LinkedList<SubActivityData>();
		List<SubActivityVo> voList = WorkOrderDefinitionCache
				.getSubActivitiesForActivity(activityId);
		SubActivityData data = null;
		MetaData metaData = null;
		for (Iterator iterator = voList.iterator(); iterator.hasNext();)
		{
			data = new SubActivityData();
			SubActivityVo vo = (SubActivityVo) iterator.next();
			metaData = new MetaData();
			updateSubActivityMetaData(metaData, vo);
			data.setMetaData(metaData);
			dataList.add(data);
		}
		return dataList;
	}

	private MetaData createMetaData(Long id, String name)
	{
		MetaData metaData = new MetaData();
		metaData.setId(id);
		metaData.setName(name);
		metaData.setCompletedOn(new Date());
		return metaData;
	}

	private void updateWorkStageMetaData(MetaData metaData,
			WorkStageVo workStageVo)
	{
		metaData.setCompletedOn(workStageVo.getModifiedOn());
		metaData.setId(workStageVo.getId());
		metaData.setName(workStageVo.getWorkStageName());
		metaData.setLastUpdatedOn(workStageVo.getModifiedOn());
		metaData.setStartedOn(workStageVo.getAddedOn());
		metaData.setStatus(workStageVo.getStatus());
	}

	private void updateActivityMetaData(MetaData metaData, ActivityVo vo)
	{
		metaData.setCompletedOn(vo.getModifiedOn());
		metaData.setId(vo.getId());
		metaData.setName(vo.getActivityName());
		metaData.setLastUpdatedOn(vo.getModifiedOn());
		metaData.setStartedOn(vo.getAddedOn());
		metaData.setStatus(vo.getStatus());
	}

	private void updateSubActivityMetaData(MetaData metaData, SubActivityVo vo)
	{
		metaData.setCompletedOn(vo.getModifiedOn());
		metaData.setId(vo.getId());
		metaData.setName(vo.getSubactivityName());
		metaData.setLastUpdatedOn(vo.getModifiedOn());
		metaData.setStartedOn(vo.getAddedOn());
		metaData.setStatus(vo.getStatus());
	}
	
	

}
