package com.cupola.fwmp.dao.mongo.documents;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;

public class DocumentDaoImpl implements DocumentDao 
{
	@Autowired
	MongoOperations mongoOperations;
	
	private static Logger LOGGER = Logger.getLogger(DocumentDaoImpl.class.getName());
	
	public void storeCustomeDocument(List<CustomerDocument> documents)
	{
		if(documents == null)
			return;
		
		LOGGER.debug(" StoreCustomeDocument "  + documents.size());
		// Removing previous data can comment later on on app bug fix
		if (documents != null && documents.size() > 0)
		{
			Long ticketId = documents.get(0).getTicketId();

			for (Iterator iterator = documents.iterator(); iterator.hasNext();)
			{
				CustomerDocument customerDocument = (CustomerDocument) iterator
						.next();
				mongoOperations
						.remove(new Query().addCriteria(Criteria.where("ticketId").is(ticketId).and("documentName")
								.regex(customerDocument.getDocumentName().substring(0,
										customerDocument.getDocumentName().lastIndexOf("_")))
								.and("activityId").is(customerDocument.getActivityId())), CustomerDocument.class);

			}
		}
		mongoOperations.insertAll(documents);
	}
	
	@Override
	public List<CustomerDocument>  getCustomeDocument(long ticketId)
	{
		LOGGER.debug(" getting CustomeDocument " + ticketId);
		List<CustomerDocument> documents = mongoOperations.find(new Query().addCriteria(Criteria.where("ticketId")
				.is(ticketId)),CustomerDocument.class);
		
		LOGGER.info("Document for ticket "+documents);
		return documents;
	}
	
	@Override
	public List<CustomerDocument>  renameCustomeDocuments(long ticketId,String oldProspect,String newProspect)
	{
	
		List<CustomerDocument> documents = mongoOperations.find(new Query().addCriteria(Criteria.where("ticketId")
				.is(ticketId)),CustomerDocument.class);
		
		if(documents != null && !documents.isEmpty())
		{

			Update update = new Update();
			update.set("modifiedOn", GenericUtil
					.convertToUiDateFormat(new Date()));
			for (CustomerDocument doc : documents)
			{

				update.set("documentName", doc.getDocumentName()
						.replace(oldProspect, newProspect));
				mongoOperations
						.updateFirst(new Query().addCriteria(Criteria
								.where("_id").is(doc.getDocumentId())), update, CustomerDocument.class);
			}
		}
		LOGGER.info("Document for ticket "+documents);
		return documents;
	}
	
	
	@Override
	public boolean updateCustomerDocuments(List<CustomerDocument> documents)
	{
		LOGGER.debug(" update CustomerDocuments " +documents.size() );
		for (Iterator iterator = documents.iterator(); iterator.hasNext();)
		{
			CustomerDocument customerDocument = (CustomerDocument) iterator
					.next();
			customerDocument.setModifiedBy(AuthUtils.getCurrentUserId());
			mongoOperations.save(customerDocument);
		}
		
		return true;
	}
	
	@Override
	public boolean updateCustomerDocumentsStatus(long ticketId,long status,long updatedBy)
	{
		LOGGER.debug(" update CustomerDocumentsStatus " +  ticketId);
		Update update = new Update();
		update.set("docApprovalStatus", status);
		update.set("modifiedBy", updatedBy);
		update.set("modifiedOn", GenericUtil.convertToUiDateFormat(new Date()));
		
		mongoOperations.findAndModify (new Query().addCriteria(Criteria.where("ticketId")
				.is(ticketId)),update, CustomerDocument.class);
		return true;
	}
	
	
}
