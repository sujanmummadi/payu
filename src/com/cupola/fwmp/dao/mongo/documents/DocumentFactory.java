package com.cupola.fwmp.dao.mongo.documents;

import java.util.Date;

import com.cupola.fwmp.FWMPConstant.DocumentSubType;
import com.cupola.fwmp.FWMPConstant.DocumentType;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;

public class DocumentFactory
{

	public static CustomerDocument createAddressProof(Long ticketId, Long customerId,int docSubType, Long modifiedBy)
	{
		CustomerDocument doc = new CustomerDocument(ticketId, customerId, DocumentType.ADDRESS_PROOF, 
				docSubType, modifiedBy);
		doc.setAddedBy(modifiedBy);
		doc.setAddedOn(GenericUtil.convertToUiDateFormat(new Date()));
		doc.setDisplayName(DocumentSubType.DOCUMENT_SUBTYPE_VALUE_STRING.get(docSubType));
		doc.setDocumentId(StrictMicroSecondTimeBasedGuid.newGuid());
		return doc;
	}
	
	public static CustomerDocument createIDProof(Long ticketId, Long customerId,int docSubType, Long modifiedBy)
	{
		CustomerDocument doc = new CustomerDocument(ticketId, customerId, DocumentType.ID_PROOF, 
				docSubType, modifiedBy);
		doc.setAddedBy(modifiedBy);
		doc.setAddedOn(GenericUtil.convertToUiDateFormat(new Date()));
		doc.setDisplayName(DocumentSubType.DOCUMENT_SUBTYPE_VALUE_STRING.get(docSubType));
		doc.setDocumentId(StrictMicroSecondTimeBasedGuid.newGuid());
		return doc;
	}
	
	public static CustomerDocument createPermissionDoc(Long ticketId, Long customerId,int docSubType, Long modifiedBy)
	{
		CustomerDocument doc = new CustomerDocument(ticketId, customerId, DocumentType.PERMISSION, 
				docSubType, modifiedBy);
		doc.setAddedBy(modifiedBy);
		doc.setAddedOn(GenericUtil.convertToUiDateFormat(new Date()));
		doc.setDisplayName(DocumentSubType.DOCUMENT_SUBTYPE_VALUE_STRING.get(docSubType));
		doc.setDocumentId(StrictMicroSecondTimeBasedGuid.newGuid());
		return doc;
	}
	
	public static CustomerDocument createAadharDoc(Long ticketId, Long customerId,int docSubType, Long modifiedBy)
	{
		CustomerDocument doc = new CustomerDocument(ticketId, customerId, DocumentType.AADHAR, 
				docSubType, modifiedBy);
		doc.setAddedBy(modifiedBy);
		doc.setAddedOn(GenericUtil.convertToUiDateFormat(new Date()));
		doc.setDisplayName(DocumentSubType.DOCUMENT_SUBTYPE_VALUE_STRING.get(docSubType));
		doc.setDocumentId(StrictMicroSecondTimeBasedGuid.newGuid());
		return doc;
	}
}
