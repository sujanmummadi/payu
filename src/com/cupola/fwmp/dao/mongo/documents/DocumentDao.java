package com.cupola.fwmp.dao.mongo.documents;

import java.util.List;

public interface DocumentDao
{
	public void storeCustomeDocument(List<CustomerDocument> documents);


	List<CustomerDocument> getCustomeDocument(long ticketId);


	boolean updateCustomerDocuments(List<CustomerDocument> documents);

	boolean updateCustomerDocumentsStatus(long ticketId, long status,
			long updatedBy);


	List<CustomerDocument> renameCustomeDocuments(long ticketId,
			String oldProspect, String newProspect);
}
