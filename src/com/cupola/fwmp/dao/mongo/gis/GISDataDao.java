package com.cupola.fwmp.dao.mongo.gis;

import com.cupola.fwmp.dao.mongo.vo.fr.GISData;

public interface GISDataDao
{

	public void storeGISData(GISData gisData);

}