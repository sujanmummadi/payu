package com.cupola.fwmp.dao.mongo.gis;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.cupola.fwmp.dao.mongo.vo.fr.GISData;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;

public class GISDataDaoImpl implements GISDataDao

{
	@Autowired
	MongoOperations mongoOperations;
	
	private static Logger LOGGER = Logger.getLogger(GISDataDaoImpl.class.getName());
	
	public void storeGISData(GISData gisData)
	{
	   if(gisData == null)
	    return;
		
		LOGGER.debug(" storeGISData " + gisData.getTicketId());
		// Removing previous data can comment later on on app bug fix
		if (gisData != null)
		{
			Long ticketId = gisData.getTicketId();

			mongoOperations.remove(new Query().addCriteria(Criteria
					.where("ticketId").is(ticketId)), GISData.class);
			gisData.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			mongoOperations.insert(gisData);
			LOGGER.info("Added Gis Data in Db for ticket "+ ticketId);
		}
		
	}
}
