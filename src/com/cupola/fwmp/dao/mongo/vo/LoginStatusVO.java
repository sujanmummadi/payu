package com.cupola.fwmp.dao.mongo.vo;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LoginStatusVO")
public class LoginStatusVO
{
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public long getUserId()
	{
		return userId;
	}
	public void setUserId(long userId)
	{
		this.userId = userId;
	}
	public long getUpdateTime()
	{
		return updateTime;
	}
	public void setUpdateTime(long updateTime)
	{
		this.updateTime = updateTime;
	}
	public Integer getStatus()
	{
		return status;
	}
	public void setStatus(Integer status)
	{
		this.status = status;
	}
		private long id;
		private long userId;
		private long updateTime;
		private Integer status;
}
