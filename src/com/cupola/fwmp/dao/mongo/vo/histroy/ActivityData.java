package com.cupola.fwmp.dao.mongo.vo.histroy;

import java.util.List;
public class ActivityData
{
	private List<SubActivityData> subActivities;
	private MetaData metaData;

	public MetaData getMetaData()
	{
		return metaData;
	}

	public void setMetaData(MetaData metaData)
	{
		this.metaData = metaData;
	}

	public List<SubActivityData> getSubActivities()
	{
		return subActivities;
	}

	public void setSubActivities(List<SubActivityData> subActivities)
	{
		this.subActivities = subActivities;
	}

	@Override
	public String toString()
	{
		return "ActivityData [subActivities=" + subActivities + ", metaData="
				+ metaData + "]";
	}
}
