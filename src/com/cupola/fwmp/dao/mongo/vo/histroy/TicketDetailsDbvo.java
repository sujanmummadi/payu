package com.cupola.fwmp.dao.mongo.vo.histroy;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class TicketDetailsDbvo
{

	@Id
	private String id;
	private Long  ticketId;
	private Long  workOrderId;
	private Long  workOrderType;
	
	private List<WorkStageData> workStages;
	private MetaData metaData;

	public MetaData getMetaData()
	{
		return metaData;
	}

	public void setMetaData(MetaData metaData)
	{
		this.metaData = metaData;
	}

	public List<WorkStageData> getWorkStages()
	{
		return workStages;
	}

	public void setWorkStages(List<WorkStageData> workStages)
	{
		this.workStages = workStages;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public Long getWorkOrderId()
	{
		return workOrderId;
	}

	public void setWorkOrderId(Long workOrderId)
	{
		this.workOrderId = workOrderId;
	}

	public Long getWorkOrderType()
	{
		return workOrderType;
	}

	public void setWorkOrderType(Long workOrderType)
	{
		this.workOrderType = workOrderType;
	}

}
