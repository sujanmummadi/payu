package com.cupola.fwmp.dao.mongo.vo.histroy;

public class SubActivityData
{
	private MetaData metaData;

	public MetaData getMetaData()
	{
		return metaData;
	}

	public void setMetaData(MetaData metaData)
	{
		this.metaData = metaData;
	}

	@Override
	public String toString()
	{
		return "SubActivityData [metaData=" + metaData + "]";
	}
}
