package com.cupola.fwmp.dao.mongo.vo.histroy;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class MetaData
{
	@Id
	private Long metaDataId;
	
	private Long id;
	private String name;
	private String description;
	private Date startedOn;
	private Date completedOn;
	private Date lastUpdatedOn;
	private Long updatedBy;
	private Integer status;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Date getStartedOn()
	{
		return startedOn;
	}

	public void setStartedOn(Date startedOn)
	{
		this.startedOn = startedOn;
	}

	public Date getCompletedOn()
	{
		return completedOn;
	}

	public void setCompletedOn(Date completedOn)
	{
		this.completedOn = completedOn;
	}

	public Date getLastUpdatedOn()
	{
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn)
	{
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public Long getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	@Override
	public String toString()
	{
		return "MetaData [id=" + id + ", name=" + name + "]";
	}
}
