package com.cupola.fwmp.dao.mongo.vo.histroy;

import java.util.List;

public class WorkStageData
{
	private List<ActivityData> activities;
	private MetaData metaData;

	public MetaData getMetaData()
	{
		return metaData;
	}

	public void setMetaData(MetaData metaData)
	{
		this.metaData = metaData;
	}

	public List<ActivityData> getActivities()
	{
		return activities;
	}

	public void setActivities(List<ActivityData> activities)
	{
		this.activities = activities;
	}

	@Override
	public String toString()
	{
		return "WorkStageData [activities=" + activities + ", metaData="
				+ metaData + "]";
	}
}
