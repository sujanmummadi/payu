package com.cupola.fwmp.dao.mongo.vo.fr;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "FrDeviceInfo")
public class FrDeviceVo 
{
	@Id
	private Long id; // this is a ticket id
	
//	private Long ticketId;
	
	private String fxName;
	private String fxIp;
	private String fxMax;
	private Integer fxPort;
	
	private String cxName;
	private String cxIp;
	private String cxMax;
	private Integer cxPort;
	
	public FrDeviceVo(){
	}
	
	public FrDeviceVo(String fxName, String fxIp, String fxMax,
			Integer fxPort, String cxName, String cxIp, String cxMax,
			Integer cxPort) {
		super();
		this.fxName = fxName;
		this.fxIp = fxIp;
		this.fxMax = fxMax;
		this.fxPort = fxPort;
		this.cxName = cxName;
		this.cxIp = cxIp;
		this.cxMax = cxMax;
		this.cxPort = cxPort;
	}

	public FrDeviceVo(String fxName,String cxIp, Integer cxPort)
	{
		super();
		this.fxName = fxName;
		this.cxIp = cxIp;
		this.cxPort = cxPort;
	}
	
	public FrDeviceVo(String fxName,String cxIp )
	{
		super();
		this.fxName = fxName;
		this.cxIp = cxIp;
	}

	public synchronized Long getId() {
		return id;
	}

	public synchronized void setId(Long id) {
		this.id = id;
	}

	public synchronized String getFxName() {
		return fxName;
	}

	public synchronized void setFxName(String fxName) {
		this.fxName = fxName;
	}

	public synchronized String getFxIp() {
		return fxIp;
	}

	public synchronized void setFxIp(String fxIp) {
		this.fxIp = fxIp;
	}

	public synchronized String getFxMax() {
		return fxMax;
	}

	public synchronized void setFxMax(String fxMax) {
		this.fxMax = fxMax;
	}

	public synchronized Integer getFxPort() {
		return fxPort;
	}

	public synchronized void setFxPort(Integer fxPort) {
		this.fxPort = fxPort;
	}

	public synchronized String getCxName() {
		return cxName;
	}

	public synchronized void setCxName(String cxName) {
		this.cxName = cxName;
	}

	public synchronized String getCxIp() {
		return cxIp;
	}

	public synchronized void setCxIp(String cxIp) {
		this.cxIp = cxIp;
	}

	public synchronized String getCxMax() {
		return cxMax;
	}

	public synchronized void setCxMax(String cxMax) {
		this.cxMax = cxMax;
	}

	public synchronized Integer getCxPort() {
		return cxPort;
	}

	public synchronized void setCxPort(Integer cxPort) {
		this.cxPort = cxPort;
	}
	
	@Override
	public String toString() {
		return "DeviceVo [id=" + id + ", fxName=" + fxName + ", fxIp=" + fxIp
				+ ", fxMax=" + fxMax + ", fxPort=" + fxPort + ", cxName="
				+ cxName + ", cxIp=" + cxIp + ", cxMax=" + cxMax + ", cxPort="
				+ cxPort + "]";
	}
}
