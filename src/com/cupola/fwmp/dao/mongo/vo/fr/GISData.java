package com.cupola.fwmp.dao.mongo.vo.fr;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "GISData")
public class GISData
{
	@Id
	private Long id;
	private Long ticketId;
	private Map<String, String> deviceAndReasonMap;

	public Long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}

	public Map<String, String> getDeviceAndReasonMap()
	{
		return deviceAndReasonMap;
	}

	public void setDeviceAndReasonMap(Map<String, String> deviceAndReasonMap)
	{
		this.deviceAndReasonMap = deviceAndReasonMap;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return "GISData [id=" + id + ", ticketId=" + ticketId
				+ ", deviceAndReasonMap=" + deviceAndReasonMap + "]";
	}
	
}
