package com.cupola.fwmp.dao.mongo.vo.fr;

import java.util.Date;

public class FlowRequestDataVo
{
	private Long id;
	private String requestType;
	private Date requestTime;
	private Long raisedBy;
	private String status;
	private Long ticketId;
	private Long engineerId;
	
	public FlowRequestDataVo(){
	}
	
	public FlowRequestDataVo(String requestType, Date requestTime, Long raisedBy,
			String status,Long engineerId,Long ticketId) {
		super();
		this.requestType = requestType;
		this.requestTime = requestTime;
		this.raisedBy = raisedBy;
		this.status = status;
		this.engineerId = engineerId;
		this.ticketId = ticketId;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getEngineerId() {
		return engineerId;
	}

	public void setEngineerId(Long engineerId) {
		this.engineerId = engineerId;
	}

	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public Date getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}
	public Long getRaisedBy() {
		return raisedBy;
	}
	public void setRaisedBy(Long raisedBy) {
		this.raisedBy = raisedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
