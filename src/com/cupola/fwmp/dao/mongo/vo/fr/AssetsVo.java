package com.cupola.fwmp.dao.mongo.vo.fr;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Assets")
public class AssetsVo
{
	@Id
	private long id;
	private long assignedTo;
	private String userDisplayName;
	private int otdrCount;
	private int splicingMachineCount;
	private int laserLightCount;
	private Date creationDate;
	private long assignedBy;
	
	public AssetsVo(){
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public long getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(long assignedTo)
	{
		this.assignedTo = assignedTo;
	}

	public String getUserDisplayName() {
		return userDisplayName;
	}

	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}

	public int getOtdrCount()
	{
		return otdrCount;
	}

	public void setOtdrCount(int otdrCount)
	{
		this.otdrCount = otdrCount;
	}

	public int getSplicingMachineCount()
	{
		return splicingMachineCount;
	}

	public void setSplicingMachineCount(int splicingMachineCount)
	{
		this.splicingMachineCount = splicingMachineCount;
	}

	public int getLaserLightCount()
	{
		return laserLightCount;
	}

	public void setLaserLightCount(int laserLightCount)
	{
		this.laserLightCount = laserLightCount;
	}

	public Date getCreationDate()
	{
		return creationDate;
	}

	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	public long getAssignedBy()
	{
		return assignedBy;
	}

	public void setAssignedBy(long assignedBy)
	{
		this.assignedBy = assignedBy;
	}

	public AssetsVo(long assignedTo, String assetName, int otdrCount,
			int splicingMachineCount, int laserLightCount, long assignedBy) {
		super();
		this.assignedTo = assignedTo;
		this.userDisplayName = assetName;
		this.otdrCount = otdrCount;
		this.splicingMachineCount = splicingMachineCount;
		this.laserLightCount = laserLightCount;
		this.assignedBy = assignedBy;
	}
	
}
