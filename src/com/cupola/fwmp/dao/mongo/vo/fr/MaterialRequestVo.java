package com.cupola.fwmp.dao.mongo.vo.fr;

import java.util.Date;

public class MaterialRequestVo
{
	private Long materialId;
	private String materialType;
	private Long requestedBy;
	private Long materialQty;
	private Date requestedOn;
	private String materialRequestStatus;
	private Long ticketId;
	private String reasonCode;
	private String remarks;
	
	public MaterialRequestVo(){
	}

	public MaterialRequestVo(String materialType, Long requestedBy,
			Long materialQty, Date requestedOn, String materialRequestStatus,
			Long ticketId, String reasonCode, String remarks, Long materialId) {
		super();
		this.materialId = materialId;
		this.materialType = materialType;
		this.requestedBy = requestedBy;
		this.materialQty = materialQty;
		this.requestedOn = requestedOn;
		this.materialRequestStatus = materialRequestStatus;
		this.ticketId = ticketId;
		this.reasonCode = reasonCode;
		this.remarks = remarks;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public Long getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(Long requestedBy) {
		this.requestedBy = requestedBy;
	}

	public Long getMaterialQty() {
		return materialQty;
	}

	public void setMaterialQty(Long materialQty) {
		this.materialQty = materialQty;
	}

	public Date getRequestedOn() {
		return requestedOn;
	}

	public void setRequestedOn(Date requestedOn) {
		this.requestedOn = requestedOn;
	}

	public String getMaterialRequestStatus() {
		return materialRequestStatus;
	}

	public void setMaterialRequestStatus(String materialRequestStatus) {
		this.materialRequestStatus = materialRequestStatus;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
