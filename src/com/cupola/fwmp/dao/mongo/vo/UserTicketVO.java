package com.cupola.fwmp.dao.mongo.vo;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class UserTicketVO
{
	private long userId;
	private long ticketId;
	
	private long categoryId ; // NI,FR,REACTI,SHIFTING
	private int currentPriority;
	private int initialPriority;  // at the time of addition it will be the same as currentPriority
	
	private long assignedBy;

	private long initialTicketTypeId;  // fiber/copper/cxup/cxdown //flow ids
	private long endTicketTypeId; // same as above or can change
	
	
	private long startStatusId; // feasibility(305/308/309) ticket type can be determined 
	private Date startDate;

	private long endStatusId;
	private Date endDate;

	private String remarks;


	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public long getTicketId()
	{
		return ticketId;
	}

	public void setTicketId(long ticketId)
	{
		this.ticketId = ticketId;
	}

	public long getStartStatusId()
	{
		return startStatusId;
	}

	public void setStartStatusId(long startStatusId)
	{
		this.startStatusId = startStatusId;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public long getEndStatusId()
	{
		return endStatusId;
	}

	public void setEndStatusId(long endStatusId)
	{
		this.endStatusId = endStatusId;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}


	public long getAssignedBy()
	{
		return assignedBy;
	}

	public void setAssignedBy(long assignedBy)
	{
		this.assignedBy = assignedBy;
	}

	public long getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(long categoryId)
	{
		this.categoryId = categoryId;
	}

	public int getCurrentPriority()
	{
		return currentPriority;
	}

	public void setCurrentPriority(int currentPriority)
	{
		this.currentPriority = currentPriority;
	}

	public int getInitialPriority()
	{
		return initialPriority;
	}

	public void setInitialPriority(int initialPriority)
	{
		this.initialPriority = initialPriority;
	}

	public long getInitialTicketTypeId()
	{
		return initialTicketTypeId;
	}

	public void setInitialTicketTypeId(long initialTicketTypeId)
	{
		this.initialTicketTypeId = initialTicketTypeId;
	}

	public long getEndTicketTypeId()
	{
		return endTicketTypeId;
	}

	public void setEndTicketTypeId(long endTicketTypeId)
	{
		this.endTicketTypeId = endTicketTypeId;
	}

}
