
package com.cupola.fwmp.dao.mongo.ticket;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.cupola.fwmp.vo.ProspectCoreVO;

/**
 * @author aditya
 * @created 10:57:26 AM Apr 7, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */
public class MongoTicketDAOImpl implements MongoTicketDAO
{
	private static Logger log = LogManager
			.getLogger(MongoTicketDAOImpl.class.getName());

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public List<ProspectCoreVO> getAllPendingProspects()
	{
		log.info("Getting all the open prospect which were not create because of some issues");

		List<ProspectCoreVO> allPendingProspects = new ArrayList<>();

		try
		{
			allPendingProspects = mongoTemplate.findAll(ProspectCoreVO.class);

			log.info("Total open prospect which were not create because of some issues are "
					+ allPendingProspects.size());

		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error while getting  all the open prospect which were not create because of some issues "
					+ e.getMessage());
		}

		return allPendingProspects;
	}
}
