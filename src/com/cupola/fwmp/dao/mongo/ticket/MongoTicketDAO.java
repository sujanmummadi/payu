
package com.cupola.fwmp.dao.mongo.ticket;

import java.util.List;

import com.cupola.fwmp.vo.ProspectCoreVO;

/**
 * @author aditya
 * @created 10:56:50 AM Apr 7, 2017
 * @copyright Cupola Technology Pvt. Ltd. 2017
 */
public interface MongoTicketDAO
{
	List<ProspectCoreVO> getAllPendingProspects();

}
