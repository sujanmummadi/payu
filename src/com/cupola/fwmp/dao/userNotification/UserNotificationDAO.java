package com.cupola.fwmp.dao.userNotification;

import java.util.List;

public interface UserNotificationDAO
{
	public List<String> getRegistrationIdByUserId(List<Long> userId);

	List<Long> getUserByTicketNumber(long ticketNo);
}
