package com.cupola.fwmp.dao.userNotification;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.userCaf.UserCafDAOImpl;

public class UserNotificationDAOImpl implements UserNotificationDAO
{
	private Logger log = Logger.getLogger(UserCafDAOImpl.class.getName());

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	HibernateTemplate hibernateTemplate;

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<String> getRegistrationIdByUserId(List<Long> userId)
	{
		List<String> listOfRegistrationId = null;

		if (userId == null)
			return listOfRegistrationId;
		
		log.debug("getRegistrationId ByUSerId as userId" + userId);
		try
		{
			Query query = hibernateTemplate
					.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select registrationId from Tablet where currentUser in(:registrationId)");
			query.setParameterList("registrationId", userId);
			listOfRegistrationId = query.list();
			if (listOfRegistrationId.isEmpty())
			{
				log.info("not found RegistrationId as user" + userId);
			}
		} catch (Exception e)
		{
			log.error("Error While getting RegistrationIdByUserId :"+e.getMessage());
			e.printStackTrace();
		}
		log.debug("Exitting from getRegistrationId ByUSerId as userId" + userId);

		return listOfRegistrationId;

	}

	@Override
	@Transactional
	public List<Long> getUserByTicketNumber(long ticketNo)
	{
		log.info("get UserId as ticketNo:" + ticketNo);
		List<Long> listOfUserId = null;
		try
		{
			listOfUserId = new ArrayList<Long>();

			Query query = hibernateTemplate
					.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select  t.currentAssignedTo,u.reportTo from Ticket t ,User u where u.id=t.currentAssignedTo and t.id=:ticketNo");
			query.setLong("ticketNo", ticketNo);

			List<Object[]> status = query.list();

			for (Object[] report : status)
			{
				listOfUserId.add(Long.valueOf(report[0].toString()));
				listOfUserId.add(Long.valueOf(report[1].toString()));

			}

			log.info("found userId as:" + listOfUserId + "as ticketNo:"
					+ ticketNo);
		} catch (Exception e)
		{
			log.error("Error While getting UserByTicketNumber :"+e.getMessage());
			e.printStackTrace();
		}

		log.debug("Exitting from get UserId as ticketNo:" + ticketNo);
		
		return listOfUserId;

	}

}
