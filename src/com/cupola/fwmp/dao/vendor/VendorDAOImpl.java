package com.cupola.fwmp.dao.vendor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.FRConstant.UserType;
import com.cupola.fwmp.filters.VendorFilter;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.Skill;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.persistance.entities.UserVendorMapping;
import com.cupola.fwmp.persistance.entities.Vendor;
import com.cupola.fwmp.persistance.entities.VendorDetails;
import com.cupola.fwmp.persistance.entities.VendorSkillMapping;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.VendorDetailSummaryVO;
import com.cupola.fwmp.vo.VendorQueueData;
import com.cupola.fwmp.vo.VendorSummaryVO;
import com.cupola.fwmp.vo.VendorVO;
import com.cupola.fwmp.vo.VendorsRecordVO;

@Transactional
public class VendorDAOImpl implements VendorDAO
{

	private static Logger log = Logger.getLogger(VendorDAOImpl.class);

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	private static final String GET_VENDOR_WITH_SKILL_DETAILS_ON_USER_ID = "select VendorId,SkillId, s.skillName,v.name, count from UserVendorMapping  inner join Skill s inner join Vendor v where s.id=UserVendorMapping.SkillId and VendorId=v.id and UserId in(?)";

	private static final String INSERT_INTO_USER_VENDOR_MAPPING_FOR_USER_SQL_QUERY = "insert into UserVendorMapping value(?,?,?,?,?,?,?,?,?,?)";

	private static final String GET_COUNT_OF_VENDORS = "select count(*) from Vendor";

	private static final String INSERT_INTO_VENDOR_QUERY = "insert into Vendor value(?,?,?,?,?,?,?,?,?,?)";

	private static final String INSERT_INTO_VENDOR_DETAILS_QUERY = "insert into VendorDetails value(?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String INSERT_INTO_VENDOR_SKILL_MAPPING_QUERY = "insert into VendorSkillMapping value(?,?,?,?)";

	private static final String UPDATE_VENDOR_CONT_ON_GIVEN_DAY = "update VendorDetails set count=? where vendorId=? and areaId=? and skillId=?";// and
																																					// date=DATE(now())

	private static final String GET_All_VENDOR_DATA_FROM_VENDOR = "select * from Vendor";

	private static final String SELECT_VENDOR = "select cityId,branchId,areaId from Vendor where id=?";

	private static final String UPDATE_SKILL_COUNT_IN_USER_VENDOR_MAPPING = "update UserVendorMapping set count=?,modifiedOn=? where UserId=? and VendorId=? and SkillId=?";

	private static final String QUERY_FOR_UPDATE_USER_VENDOR_MAPPING = "select id from UserVendorMapping where UserId=? and SkillId=? and VendorId=?";

	private static final String SELECT_VENDOR_FOR_USER = "select id, name, cityId, branchId, areaId , status from Vendor";

	private static final String SELECT_VENDOR_ON_USERID = "(select distinct(VendorId) from UserVendorMapping uv";

	private static final String GET_All_VENDOR_SKILL_FROM_VENDOR_SKILL_TABLE = "select * from VendorSkillMapping";
	
	/*private String SQL_QUERY_VENDOR_SKILL =  "SELECT fr.workOrderNumber,fr.ticketId,usr.id, "
			+ " usr.firstName,usr.lastName,sk.skillName ,fr.currentFlowId,vtick.estimatedStartTime,vtick.estimatedEndTime "
			+ " FROM User usr left Outer JOIN  SubTickets vtick ON vtick.userId = usr.id "
			+ " left JOIN FrDetail fr ON vtick.ticketId = fr.ticketId  "
			+ " left JOIN UserSkillMapping um ON um.idUser = usr.id  "
			+ " inner JOIN Skill sk ON um.idSkill = sk.id  "
			+ "	WHERE usr.reportTo in( :reportToId)  and vtick.userType=" + UserType.VENDOR +"  and vtick.status not in(:status)";*/
	private String SQL_QUERY_VENDOR_SKILL = "SELECT " + 
			"    fr.workOrderNumber," + 
			"    fr.ticketId," + 
			"    usr.id," + 
			"    usr.firstName," + 
			"    usr.lastName," + 
			"    vtick.skillId," + 
			"    fr.currentFlowId," + 
			"    vtick.estimatedStartTime," + 
			"    vtick.estimatedEndTime " + 
			" FROM " + 
			"    User usr " + 
			"        INNER JOIN " + 
			"    SubTickets vtick ON vtick.userId = usr.id " + 
			"        INNER JOIN " + 
			"    FrDetail fr ON vtick.ticketId = fr.ticketId "+
			"	WHERE usr.reportTo in ( :reportToId)  and vtick.userType=" + UserType.VENDOR +"  and vtick.status not in(:status) ";

	Branch branch = new Branch();
	Skill skill = new Skill();
	Vendor vendor = new Vendor();
	Area area = new Area();
	City city = new City();

	VendorVO vto = new VendorVO();

	Map vendorIdskillIdMap = new HashMap();

	static int MAX_ITEM_PER_PAGE = 50;
	static int MIN_ITEM_PER_PAGE = 5;

	Map<Long, VendorSkillMapping> VendorIdskillIdCountMap = new HashMap<Long, VendorSkillMapping>();

	Map<String, Long> vendorNameAndIdCache = new TreeMap<String, Long>();

	public static Map<Long, String> skillIdAndNameCache = new HashMap<Long, String>();

	public static Map<Long, VendorVO> vendorIdVendorDetails = new ConcurrentHashMap<Long, VendorVO>();

	Map<String, Long> vendorNameMap = new HashMap<String, Long>();

	// First time when you want to add the record of vendor into the vendor
	// databases.

	private void init()
	{
		log.info("VendorDAOImpl init method called..");
		getVendors();
		skillDetails();
		log.info("VendorDAOImpl init method executed.");
	}

	private void getVendors()
	{
		log.debug(" inside getVendors  ");

		try {
		List<VendorVO> vendorList = jdbcTemplate
				.query(SELECT_VENDOR_FOR_USER, new BeanPropertyRowMapper(VendorVO.class));
		// List<Vendor> vendorList = (List<Vendor>)
		// hibernateTemplate.find("from Vendor");

			if (vendorList == null || vendorList.isEmpty())
		{

			log.info("Area details are not available");
			return;

		} else
		{

			for (VendorVO vendor : vendorList)
			{

				add2Cache(vendor);
			}

		}
		} catch (Exception e) {
			log.error("Error occured while getVendors",e);
			e.printStackTrace();
	}

		log.debug(" Exitting from  getVendors  ");
	}
	@Override
	public void addToCache(VendorVO vendor)
	{
		add2Cache(vendor);
	}
	private void add2Cache(VendorVO vendor)
	{
		if( vendor == null)
			return;
		log.debug(" inside add2Cache  ");
		
		vendorNameAndIdCache.put(vendor.getName(), vendor.getId());

		vendorIdVendorDetails.put(vendor.getId(), vendor);
		
		log.debug(" Exitting from  addToCache  ");
	}

	private void skillDetails()
	{
		log.debug(" inside skillDetails  ");

		try {
		List<VendorSummaryVO> vendorList = jdbcTemplate
				.query("select id,skillName from Skill", new SkillRowMapper());

			if (vendorList == null || vendorList.isEmpty())
		{

			log.info("Area details are not available");
			return;

		} else
		{

			for (VendorSummaryVO skill : vendorList)
			{

				skillListAddToCache(skill);
			}

		}
		} catch (Exception e) {
			log.error("Error occured while skillDetails ",e);
			e.printStackTrace();
		}
		log.debug(" Exitting from  skillDetails  ");
	}

	private void skillListAddToCache(VendorSummaryVO skill)
	{
		log.debug(" inside skillListAddToCache  ");
		
		if( skill == null )
			return;

		skillIdAndNameCache.put(Long.valueOf(skill.getSkillId()), skill
				.getSkillName());
		
		log.debug(" Exitting from  skillListAddToCache  ");
	}

	private void getAllSkillCount()
	{
		log.debug(" inside getAllSkillCount  ");

		try {
		List<VendorSkillMapping> skillList = (List<VendorSkillMapping>) hibernateTemplate
				.find("from VendorSkillMapping");

		log.debug("getAllSkillCount:::" + skillList);

			if (skillList != null && !skillList.isEmpty())
		{

			for (VendorSkillMapping vendorSkillMapping : skillList)
			{

				addToCache(vendorSkillMapping);
			}
		} else
		{
			log.info("vendor skill mapping table may be  empty");
		}
		} catch (Exception e) {
			log.error("Error occured while getAllSkillCount ",e);
			e.printStackTrace();
	}
		log.debug(" Exitting from  getAllSkillCount  ");
	}

	private Map<Long, VendorSkillMapping> addToCache(
			VendorSkillMapping vendorSkillMapping)
	{
		log.debug(" inside addToCache  ");
		
		if (vendorSkillMapping != null)
		{
			VendorIdskillIdCountMap
					.put(vendorSkillMapping.getVendor().getId(), vendorSkillMapping);

			return VendorIdskillIdCountMap;
		} else
		{
			return null;
		}

	}

	public Long addVendor(VendorVO vendorvo)
	{
		int result = 0;
		if( vendorvo == null )
			return 0l;
		
		log.debug(" inside addVendor  "+vendorvo.getAreaName());

		if (vendorvo.getCityId() > 0 && vendorvo.getBranchId() > 0
				&& vendorvo.getAreaId() > 0)
		{

			List<VendorsRecordVO> vendorList = getAllVendors();

			if (vendorList != null)
			{

				for (VendorsRecordVO venvo : vendorList)
				{

					vendorNameMap.put(venvo.getName().toLowerCase(), venvo
							.getId());
				}
			}
			log.debug("vendor details ::::" + vendorNameMap);

			try
			{

				if (vendorNameMap != null
						&& vendorNameMap.containsKey(vendorvo.getName()
								.toLowerCase()))
				{

					log.debug("Vendor Name all ready Exist");

					return Long.valueOf(result);

				} else
				{

					vendorvo.setId(StrictMicroSecondTimeBasedGuid.newGuid());

					result = jdbcTemplate
							.update(INSERT_INTO_VENDOR_QUERY, new Object[] {
									vendorvo.getId(), vendorvo.getName(),
									vendorvo.getCityId(),
									vendorvo.getBranchId(),
									vendorvo.getAreaId(),
									vendorvo.getAddedBy(),
									vendorvo.getAddedOn(),
									vendorvo.getModifiedBy(),
									vendorvo.getModifiedOn(),
									vendorvo.getStatus() });
					
					if( result > 0 )
					{
						addIntoVendorSkillMapping( vendorvo );
						addToCache(vendorvo);
					}
				}

			} catch (Exception e)
			{
				log.error("Error While adding vendor :"+e.getMessage());
				e.printStackTrace();
			}
		}
		log.debug(" Exitting addVendor  ");
		
		return Long.valueOf(result);

	}

	// ******* Updating the vendor records by passing information which you want
	// to change *******

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public String updateVendor(VendorVO vto)
	{

		String updateValue = "failure";
		if( vto == null )
			return updateValue;
		
		log.debug(" inside updateVendor  "+vto.getAreaName());

		Vendor vendor = new Vendor();

		if (vto.getId() > 0)
		{

			vendor = hibernateTemplate.get(Vendor.class, vto.getId());

		}

		// long vid = vendor.getId();

		// vendor.setId(vid);

		if (vendor != null && vendor.getId() != null && vendor.getId() > 0)
		{
			vendor.setId(vendor.getId());

			if (vto.getBranchId() > 0)
			{

				long bid = vto.getBranchId();

				branch.setId(bid);

				vendor.setBranch(branch);
			}

			if (vto.getCityId() > 0)
			{

				long cid = vto.getCityId();

				city.setId(cid);

				vendor.setCity(city);
			}

			if (vto.getName() != null)
			{

				vendor.setName(vto.getName().toLowerCase());
			}
			if (vto.getEnable() != null)
			{

				vendor.setStatus(vto.getStatus());
			}
			if (vto.getAreaId() > 0)
			{

				long aid = vto.getAreaId();

				area.setId(aid);

				vendor.setArea(area);
			}

			vendor.setModifiedOn(vto.getModifiedOn());
			vendor.setModifiedBy(vto.getModifiedBy());
			hibernateTemplate.update(vendor);

			updateValue = "success";

		}
		if (updateValue != null && updateValue.equalsIgnoreCase("success"))
		{
			log.debug(" Exitting from updateVendor  ");
			return updateValue;
		} else
		{
			log.debug(" Exitting from updateVendor  ");
			return updateValue;

		}

	}

	// ******* Deleting the vendor records by their id *******
	@Override
	@Transactional
	public String deleteVendor(Long id)
	{
		log.debug(" inside deleteVendor  " +id);
		if (id > 0)
		{

			long vendorId = id;

			log.debug("vendor id for delete :" + vendorId);

			vendor = hibernateTemplate.get(Vendor.class, vendorId);

		}

		if (vendor != null)
		{

			log.debug("vendor::" + vendor);

			hibernateTemplate.delete(vendor);

			log.debug(" Exitting from deleteVendor  ");
			return "Success";

		} else
		{
			log.debug(" Exitting from deleteVendor  ");
			return "Failure";
		}

	}

	// insert the records into vendorSkillMapping by providing vendorId and map
	// are providing for skillId and their counts

	@Override
	@Transactional
	public void addIntoVendorSkillMapping(VendorVO vendorvo)
	{
		if( vendorvo == null )
			return ;
		
		log.debug(" inside addIntoVendorSkillMapping  " +vendorvo.getAreaName());

		Map<String, String> vendorSkillCountMap = vendorvo
				.getVendorSkillIdAndCountMap();

		Long vendorId = vendorvo.getId();

		VendorSkillMapping vendorSkillMapping = null;

		for (Map.Entry<String, String> vendorSkillCount : vendorSkillCountMap
				.entrySet())
		{

			vendorSkillMapping = new VendorSkillMapping();

			String skillId = vendorSkillCount.getKey();

			vendorvo.setSkillId(Long.valueOf(skillId));

			String count = vendorSkillCount.getValue();

			vendorvo.setCount(Integer.valueOf(count));

			vendorSkillMapping.setId(StrictMicroSecondTimeBasedGuid.newGuid());

			try
			{

				jdbcTemplate
						.update(INSERT_INTO_VENDOR_SKILL_MAPPING_QUERY, new Object[] {
								vendorSkillMapping.getId(), vendorId,
								vendorvo.getSkillId(), vendorvo.getCount() });

			} catch (HibernateException he)
			{
				log.error("Error While executing Query :"+he.getMessage());

				he.printStackTrace();
			}

		}
		log.debug(" Exitting from  addIntoVendorSkillMapping  "+vendorvo.getAreaName());
	}

	// display how many vendor are there in databases as a number

	@Override
	public int getCountAllVendor()
	{
		log.debug(" inside getCountAllVendor  ");

		return jdbcTemplate.queryForInt(GET_COUNT_OF_VENDORS);

	}

	// display how many vendor are there of the specific skills in databases as
	// a number

	@Override
	@SuppressWarnings("unchecked")
	public int getCountAllVendorBySkill(Long id)
	{
		log.debug(" inside getCountAllVendorBySkill  "+id);
		
		int x = 0;
		long vid = id;

		List<VendorSkillMapping> vsmlist = (List<VendorSkillMapping>) hibernateTemplate
				.find("from VendorSkillMapping vsm where vsm.skill.id =?", vid);

		if (vsmlist != null && !vsmlist.isEmpty())
		{

			x = vsmlist.size();

			return x;
		} else
		{
			return 0;
		}

	}

	// insert the records into vendorDetails on daily basis by providing
	// required data of vendor and as map are providing for skillId and their
	// counts

	@Override
	public void addVendorSkillAndCount(VendorVO vendorvo)
	{
		if(vendorvo == null)
			return;
		
		log.debug("vendor id info:::" + vendorvo.getId());

		VendorsRecordVO vendorsRecordObject = jdbcTemplate
				.queryForObject(SELECT_VENDOR, new Object[] { vendorvo.getId() }, new VendorDailyaddedMappeer());

		log.debug("addVendorSkillAndCount::::::" + vendorsRecordObject);

		if (vendorsRecordObject != null)
		{

			long areaid = vendorsRecordObject.getAreaId();
			long cityid = vendorsRecordObject.getCityId();
			long branchid = vendorsRecordObject.getBranchId();

			vendorvo.setAreaId(areaid);
			vendorvo.setBranchId(branchid);
			vendorvo.setCityId(cityid);

		} else
		{
			log.info("cityid areaid ,branchid not updated");
		}

		if (vendorvo.getVendorSkillIdAndCountMap() != null)
		{

			Map<String, String> vendorSkillAndCountMap = vendorvo
					.getVendorSkillIdAndCountMap();

			int count;

			for (Map.Entry<String, String> entry : vendorSkillAndCountMap
					.entrySet())
			{

				VendorDetails vendorDetails = new VendorDetails();

				vendorDetails.setId(StrictMicroSecondTimeBasedGuid.newGuid());

				String skillId = (String) entry.getKey();

				vendorvo.setSkillId(Long.valueOf(skillId));

				vendorvo.setDate(new Date());

				if (entry.getValue().equals("") || entry.getValue().isEmpty()
						|| entry.getValue() == null)
				{

					count = 0;

					vendorvo.setCount(count);

				} else
				{

					count = Integer.valueOf((String) entry.getValue());

					vendorvo.setCount(count);
				}

				int vendorDetailsResult = jdbcTemplate
						.update(INSERT_INTO_VENDOR_DETAILS_QUERY, new Object[] {
								vendorDetails.getId(), vendorvo.getVendorId(),
								vendorvo.getBranchId(), vendorvo.getSkillId(),
								vendorvo.getDate(), vendorvo.getCount(),
								vendorvo.getAddedBy(), vendorvo.getAddedOn(),
								vendorvo.getModifiedBy(),
								vendorvo.getModifiedOn(), vendorvo.getAreaId(),
								vendorvo.getCityId() });
			}

		}

	}

	private class VendorDailyaddedMappeer implements RowMapper<VendorsRecordVO>
	{

		@Override
		public VendorsRecordVO mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{

			VendorsRecordVO vendorsRecordVO = new VendorsRecordVO();

			vendorsRecordVO.setCityId(resultSet.getLong("cityId"));

			vendorsRecordVO.setBranchId(resultSet.getLong("branchId"));

			vendorsRecordVO.setAreaId(resultSet.getLong("areaId"));

			return vendorsRecordVO;
		}
	}

	// Vendor Service related to Android app as well as Web and accesses by TL
	// this updated the count of skill for the vendorDetails

	@Override
	public String updateCountSkill(VendorVO vendorvo)
	{
        if( vendorvo == null )
			return "failed";
		
		log.debug("vendor id info:::" + vendorvo.getId());
		long vid = vendorvo.getVendorId();

		String GET_SKILLID = "select SkillId from UserVendorMapping where UserId=? and VendorId=?";

		List<Long> skillIdList = jdbcTemplate
				.queryForList(GET_SKILLID, new Object[] { vendorvo.getUserId(),
						vendorvo.getVendorId() }, Long.class);

		log.debug("skill list for givend vendor" + skillIdList);

		log.debug("veendorvo details for inserting :::::" + vendorvo);

		Integer value = null;

		Map<String, String> updateSkillCount = vendorvo
				.getVendorSkillIdAndCountMap();

		log.debug("updateSkillCount### updateSkillCount::" + updateSkillCount);

		for (Map.Entry<String, String> skillCountentry : updateSkillCount
				.entrySet())
		{

			Long skillId = Long.valueOf((String) skillCountentry.getKey());

			int count = Integer.valueOf((String) skillCountentry.getValue());

			vendorvo.setCount(count);

			vendorvo.setSkillId(skillId);

			log.debug("vendorAssignmentByTLForNE skillId   inside for loop ::"
					+ skillId);

			log.debug("vendorAssignmentByTLForNE vendorList id    inside for loop ::"
					+ vendorvo.getVendorId());

			log.debug("vendorAssignmentByTLForNE vendorList id    inside for loop ::"
					+ skillIdList);

			if (skillIdList.contains(skillId))
			{

				log.debug("updation under skilll id" + vendorvo.getSkillId());

				int vendorUpdateResult = jdbcTemplate
						.update(UPDATE_SKILL_COUNT_IN_USER_VENDOR_MAPPING, new Object[] {
								vendorvo.getCount(), vendorvo.getModifiedOn(),
								vendorvo.getUserId(), vendorvo.getVendorId(),
								vendorvo.getSkillId() });

			} else
			{

				UserVendorMapping userVendorMapping = new UserVendorMapping();

				userVendorMapping.setId(StrictMicroSecondTimeBasedGuid
						.newGuid());

				value = jdbcTemplate
						.update(INSERT_INTO_USER_VENDOR_MAPPING_FOR_USER_SQL_QUERY, new Object[] {
								userVendorMapping.getId(),
								vendorvo.getUserId(), vendorvo.getVendorId(),
								skillId, count, vendorvo.getAddedBy(),
								vendorvo.getAddedOn(),
								vendorvo.getModifiedBy(),
								vendorvo.getModifiedOn(), vendorvo.getStatus() });

			}
			updateSkillCount.remove(skillId.toString());
		}

		log.debug(" exit from vendor id info:::" + vendorvo.getId());
		
		return "Success";

		// return null;

	}

	/*
	 * @Override public List<VendorSummaryVO> vendorAssignmentForTL(VendorVO
	 * vendorvo) {
	 * 
	 * List<VendorSummaryVO> vendorAssignForTL = null;
	 * 
	 * long userid=vendorvo.getUserId();
	 * 
	 * vendorAssignForTL = (List<VendorSummaryVO>) jdbcTemplate
	 * .query(GET_VENDOR_DETAILS_FOR_TL, new Object[] { userid }, new
	 * VendorAssignmentForTLMapper());
	 * 
	 * if (vendorAssignForTL != null) { return vendorAssignForTL; } else {
	 * return null; }
	 * 
	 * }
	 */
	/*
	 * class VendorDaoMapper implements RowMapper<VendorVO> {
	 * 
	 * @Override public VendorVO mapRow(ResultSet resultSet, int value) throws
	 * SQLException {
	 * 
	 * VendorVO vendorvo = new VendorVO();
	 * 
	 * vendorvo.setVendorId(resultSet.getLong("vendorId"));
	 * 
	 * vendorvo.setSkillId(resultSet.getLong("skillId"));
	 * 
	 * vendorvo.setAreaId(resultSet.getLong("areaId"));
	 * 
	 * vendorvo.setCount(resultSet.getInt("count"));
	 * 
	 * return vendorvo; }
	 * 
	 * }
	 */

	/*
	 * class VendorAssignmentForTLMapper implements RowMapper<VendorSummaryVO> {
	 * 
	 * @Override public VendorSummaryVO mapRow(ResultSet resultSet, int value)
	 * throws SQLException {
	 * 
	 * VendorSummaryVO vendorAssignForTLResponseObject = new VendorSummaryVO();
	 * 
	 * vendorAssignForTLResponseObject.setVendorId(resultSet
	 * .getLong("VendorId"));
	 * 
	 * vendorAssignForTLResponseObject.setVendorName(resultSet
	 * .getString("VendorName"));
	 * 
	 * //vendorAssignForTLResponseObject.setSkillId(resultSet
	 * //.getInt("SkillId"));
	 * 
	 * //vendorAssignForTLResponseObject.setSkillName(resultSet //
	 * .getString("SkillName"));
	 * 
	 * //vendorAssignForTLResponseObject.setCount(resultSet //
	 * .getInt("CountOnDay"));
	 * 
	 * return vendorAssignForTLResponseObject; }
	 * 
	 * }
	 */

	/*
	 * class UserIdMapper implements RowMapper<User> {
	 * 
	 * @Override public User mapRow(ResultSet resultSet, int value) throws
	 * SQLException { User user = new User();
	 * 
	 * user.setId(Long.valueOf(resultSet.getString("idUser")));
	 * 
	 * return user; }
	 * 
	 * }
	 */

	// insert the records into the userVendorMapping table for keeping the
	// mapping between them by providing the userId and VendorId

	@Override
	@Transactional
	public Integer addUserVendorMapping(VendorVO vendorvo)
	{
		if( vendorvo == null )
			return 0;

		log.debug("addUserVendorMapping:::" + vendorvo.getId());
		
		UserVendorMapping userVendorMapping = new UserVendorMapping();

		Map<String, String> skillAndCountMap = vendorvo
				.getVendorSkillIdAndCountMap();

		Integer value = null;

		Long skillId;
		int count;
		if (skillAndCountMap != null && !skillAndCountMap.isEmpty())
		{
			for (Map.Entry<String, String> skillAndCountValue : skillAndCountMap
					.entrySet())
			{

				if ((skillAndCountValue.getValue().equals(""))
						&& skillAndCountValue.getValue().isEmpty())
				{

					skillId = Long
							.valueOf((String) skillAndCountValue.getKey());

					count = 0;

				} else
				{
					skillId = Long
							.valueOf((String) skillAndCountValue.getKey());
					count = Integer.valueOf((String) skillAndCountValue
							.getValue());
				}

				List<Long> id = (List<Long>) jdbcTemplate
						.queryForList(QUERY_FOR_UPDATE_USER_VENDOR_MAPPING, new Object[] {
								vendorvo.getUserId(), skillId,
								vendorvo.getVendorId() }, Long.class);

				// List listValue=(List)hibernateTemplate.find(queryString,
				// values);

				log.debug("existing uservendormapping id::" + id);

				if (id != null && !id.isEmpty())
				{

					UserVendorMapping userVendor = new UserVendorMapping();

					userVendor.setId(id.get(0));

					Vendor ven = new Vendor();
					ven.setId(vendorvo.getVendorId());
					User usr = new User();
					Skill skill = new Skill();
					skill.setId(skillId);

					usr.setId(vendorvo.getUserId());

					userVendor.setCount(count);
					userVendor.setSkill(skill);
					userVendor.setUser(usr);
					userVendor.setVendor(ven);
					userVendor.setStatus(vendorvo.getStatus());
					userVendor.setAddedBy(vendorvo.getAddedBy());
					userVendor.setModifiedBy(vendorvo.getModifiedBy());
					userVendor.setAddedOn(vendorvo.getAddedOn());
					userVendor.setModifiedOn(vendorvo.getModifiedOn());

					hibernateTemplate.update(userVendor);

					value = 1;

				} else
				{

					userVendorMapping.setId(StrictMicroSecondTimeBasedGuid
							.newGuid());

					value = jdbcTemplate
							.update(INSERT_INTO_USER_VENDOR_MAPPING_FOR_USER_SQL_QUERY, new Object[] {
									userVendorMapping.getId(),
									vendorvo.getUserId(),
									vendorvo.getVendorId(), skillId, count,
									vendorvo.getAddedBy(),
									vendorvo.getAddedOn(),
									vendorvo.getModifiedBy(),
									vendorvo.getModifiedOn(),
									vendorvo.getStatus() });

				}
			}
		}
		if (value != null && value > 0)
		{
			return value;
		} else
		{
			return null;
		}
	}

	// vendorAssignmentByTLForNE services related to android

	@Override
	public String vendorAssignmentByTLForNE(VendorVO vendorvo)

	{
	      if( vendorvo == null )
			return "failed";
		
		log.debug("vendorAssignmentByTLForNE:::" + vendorvo.getId());
	/*	VendorDetails vendorDetail = new VendorDetails();

		long vid = vendorvo.getVendorId();*/

		String GET_SKILLID = "select SkillId from UserVendorMapping where UserId=? and VendorId=?";

		List<Long> skillIdList = jdbcTemplate
				.queryForList(GET_SKILLID, new Object[] {
						vendorvo.getNeUserId(), vendorvo.getVendorId() }, Long.class);

		log.debug("skill list for givend vendor" + skillIdList);

		log.debug("veendorvo details for inserting :::::" + vendorvo);

		Integer value = null;

		Map<String, String> updateSkillCount = vendorvo
				.getVendorSkillIdAndCountMap();

		List<String> removeableIds = new ArrayList<String>();
		log.debug("updateSkillCount### updateSkillCount::" + updateSkillCount);
		
		

		for (Map.Entry<String, String> skillCountentry : updateSkillCount
				.entrySet())
		{

			Long skillId = Long.valueOf((String) skillCountentry.getKey());

			int count = Integer.valueOf((String) skillCountentry.getValue());

			vendorvo.setCount(count);

			vendorvo.setSkillId(skillId);

			log.debug("vendorAssignmentByTLForNE skillId   inside for loop ::"
					+ skillId);

			log.debug("vendorAssignmentByTLForNE vendorList id    inside for loop ::"
					+ vendorvo.getVendorId());

			log.debug("vendorAssignmentByTLForNE vendorList id    inside for loop ::"
					+ skillIdList);

			if (skillIdList.contains(skillId))
			{

				log.debug("updation under skilll id" + vendorvo.getSkillId());

				int vendorUpdateResult = jdbcTemplate
						.update(UPDATE_SKILL_COUNT_IN_USER_VENDOR_MAPPING, new Object[] {
								vendorvo.getCount(), vendorvo.getModifiedOn(),
								vendorvo.getNeUserId(), vendorvo.getVendorId(),
								vendorvo.getSkillId() });

			} else
			{

				UserVendorMapping userVendorMapping = new UserVendorMapping();

				userVendorMapping.setId(StrictMicroSecondTimeBasedGuid
						.newGuid());

				value = jdbcTemplate
						.update(INSERT_INTO_USER_VENDOR_MAPPING_FOR_USER_SQL_QUERY, new Object[] {
								userVendorMapping.getId(),
								vendorvo.getNeUserId(), vendorvo.getVendorId(),
								skillId, count, vendorvo.getAddedBy(),
								vendorvo.getAddedOn(),
								vendorvo.getModifiedBy(),
								vendorvo.getModifiedOn(), vendorvo.getStatus() });

				/*
				 * int vendorInsertResult = jdbcTemplate.update(
				 * INSERT_INTO_VENDOR_DETAILS_QUERY, new Object[] {
				 * vendorDetails.getId(), vendorvo.getVendorId(),
				 * vendorvo.getBranchId(), vendorvo.getSkillId(),
				 * vendorvo.getDate(), vendorvo.getCount(),
				 * vendorvo.getAddedBy(), vendorvo.getAddedOn(),
				 * vendorvo.getModifiedBy(), vendorvo.getModifiedOn(),
				 * vendorvo.getAreaId(), vendorvo.getCityId() });
				 */

			}
			removeableIds.add(skillId.toString());
			
		}
		
		if(!removeableIds.isEmpty()){
			for (Iterator iterator = removeableIds.iterator(); iterator.hasNext();) {
				String string = (String) iterator.next();
				updateSkillCount.remove(string);
			}
		}
		
		log.debug(" exit from vendorAssignmentByTLForNE:::" + vendorvo.getId());
		
		return "Success";

		// return null;
	}

	private class VendorIdAndSkillIdMappeer implements
			RowMapper<VendorSummaryVO>
	{

		@Override
		public VendorSummaryVO mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{

			VendorSummaryVO vsvo = new VendorSummaryVO();

			vsvo.setVendorId(resultSet.getLong("vendorId"));
			vsvo.setSkillId(resultSet.getInt("skillId"));

			return vsvo;
		}

	}

	// Display the all records of vendor on the basis of their skill by using
	// the userId from the vendorDetails tables.

	@Override
	public List<VendorSummaryVO> getVendorDetails(Long userId)
	{
		if(userId == null || userId <= 0)
			return null;

		log.debug(" inside getVendorDetails  " +userId);
		
		List<VendorSummaryVO> vendorSummaryVO = (List<VendorSummaryVO>) jdbcTemplate
				.query(GET_VENDOR_WITH_SKILL_DETAILS_ON_USER_ID, new Object[] { userId }, new VendorDetailsMapper());

		log.debug("getVendorWithSkillByNEUser" + vendorSummaryVO);

		log.debug("getVendorDetails::databases" + vendorSummaryVO);

		if (vendorSummaryVO != null && !vendorSummaryVO.isEmpty())
		{

			return vendorSummaryVO;
		} else
		{
			return null;
		}

	}

	class VendorDetailsMapper implements RowMapper<VendorSummaryVO>
	{

		@Override
		public VendorSummaryVO mapRow(ResultSet resultSet, int value)
				throws SQLException
		{

			VendorSummaryVO vendorSummaryVO = new VendorSummaryVO();

			vendorSummaryVO.setVendorId(Long.valueOf(resultSet
					.getString("vendorId")));

			vendorSummaryVO.setVendorName(resultSet.getString("name"));

			vendorSummaryVO.setSkillName(resultSet.getString("skillName"));

			vendorSummaryVO.setSkillId(resultSet.getInt("skillId"));

			vendorSummaryVO.setCount(resultSet.getInt("count"));

			// skillIdCountMap.put(vendorSummaryVO.getSkillId(),
			// vendorSummaryVO.getCount());

			return vendorSummaryVO;
		}

	}

	// Display all the records of vendor from the vendor tables by providing
	// nothing.

	public List<VendorsRecordVO> getAllVendors()
	{
		log.debug(" inside getAllVendors  ");

		return jdbcTemplate
				.query(GET_All_VENDOR_DATA_FROM_VENDOR, new AllVendorRecordMappeer());

	}

	class AllVendorRecordMappeer implements RowMapper<VendorsRecordVO>
	{

		@Override
		public VendorsRecordVO mapRow(ResultSet resultSet, int in)
				throws SQLException
		{

			VendorsRecordVO vendorvo = new VendorsRecordVO();

			vendorvo.setId(Long.valueOf(resultSet.getString("id")));
			vendorvo.setName(resultSet.getString("name"));
			vendorvo.setCityId(Long.valueOf(resultSet.getString("cityId")));
			vendorvo.setCityName(LocationCache.getCityNameById(vendorvo
					.getCityId()));
			vendorvo.setBranchId(Long.valueOf(resultSet.getString("branchId")));
			vendorvo.setBranchName(LocationCache.getBrancheNameById(vendorvo
					.getBranchId()));
			vendorvo.setAreaId(Long.valueOf(resultSet.getString("areaId")));
			vendorvo.setAreaName(LocationCache.getAreaNameById(vendorvo
					.getAreaId()));
			vendorvo.setStatus(Integer.valueOf(resultSet.getString("status")));

			/*
			 * Map<Long,VendorSkillMapping> skillidcount =getAllSkillCount();
			 * 
			 * log.info("Map values is::::"+skillidcount);
			 * 
			 * VendorSkillMapping
			 * vendorSkillMapping=skillidcount.get(vendorvo.getId());
			 * 
			 * log.info("map value:::::"+vendorSkillMapping);
			 * 
			 * //vendorvo.setSkillId(vendorSkillMapping.getSkill().getId().intValue
			 * ());
			 * 
			 * vendorvo.setCount(vendorSkillMapping.getCount());
			 */

			return vendorvo;
		}

	}

	// Display the vendor records on the basis of filtered data from the vendor
	// tables .
	// if you are not provide anything, display all the records of vendor;

	@Override
	@Transactional
	public List<VendorsRecordVO> getVendorsOnFilter(VendorFilter vf)
	{
		if(vf == null)
			return null;

		log.debug(" inside getVendorsOnFilter  " +vf.getUserId());
		
		Criteria criteria = hibernateTemplate.getSessionFactory()
				.getCurrentSession().createCriteria(Vendor.class, "vendor");

		log.info("criteria:::::::::" + criteria);

		StringBuilder queryStr = new StringBuilder(GET_All_VENDOR_DATA_FROM_VENDOR);

		StringBuilder WHERE_CLAUSE = new StringBuilder(" where ");

		if (vf != null)
		{

			if (vf.getPageSize() > 0)
				;
			else
			{
				vf.setStartOffset(0);

				vf.setPageSize(MIN_ITEM_PER_PAGE);

			}

			if (vf.getBranchId() != null && vf.getBranchId() > 0)

			{
				WHERE_CLAUSE.append(" branchId=" + vf.getBranchId());

			}
			if (vf.getCityId() != null && vf.getCityId() > 0)
			{

				if (WHERE_CLAUSE.indexOf("=") > 0)
					WHERE_CLAUSE.append(" and ");

				WHERE_CLAUSE.append(" cityId=" + vf.getCityId());

			}

			if (vf.getAreaId() != null && vf.getAreaId() > 0)
			{

				if (WHERE_CLAUSE.indexOf("=") > 0)
					WHERE_CLAUSE.append(" and ");

				WHERE_CLAUSE.append(" areaId=" + vf.getAreaId());

			}

			if ((vf.getBranchId() != null && vf.getBranchId() > 0)
					|| (vf.getCityId() != null && vf.getCityId() > 0)
					|| (vf.getAreaId() != null && vf.getAreaId() > 0)
					|| vf.getFromDate() != null)
			{

				queryStr.append(WHERE_CLAUSE);

				queryStr.append(" limit " + vf.getStartOffset() + " , "
						+ vf.getPageSize());

				log.info("areaid and with pagination " + queryStr.toString());

				return jdbcTemplate
						.query(queryStr.toString(), new AllVendorRecordMappeer());

			} else
			{
				queryStr.append(" limit " + vf.getStartOffset() + " , "
						+ vf.getPageSize());

				log.info("#######@@@@@@@@@@@" + queryStr.toString());

				return jdbcTemplate
						.query(queryStr.toString(), new AllVendorRecordMappeer());

			}

		} else
		{
			return new ArrayList();
		}

	}

	private List<VendorsRecordVO> xformToVoList(List<Vendor> vendors)
	{
		List<VendorsRecordVO> listvend = new ArrayList<VendorsRecordVO>();

		if( vendors == null )
			return listvend;

		log.debug(" inside xformToVoList  " +vendors.size());
		
		VendorsRecordVO vrvo;
		for (Vendor vendor : vendors)
		{

			vrvo = new VendorsRecordVO();
			vrvo.setId(vendor.getId());
			vrvo.setName(vendor.getName());

			Area area = vendor.getArea();
			vrvo.setAreaId(area.getId());
			vrvo.setAreaName(area.getAreaName());

			Branch branch = vendor.getBranch();
			vrvo.setBranchId(branch.getId());
			vrvo.setBranchName(branch.getBranchName());

			City city = vendor.getCity();
			vrvo.setCityId(city.getId());
			vrvo.setCityName(city.getCityName());

			vrvo.setStatus(vendor.getStatus());

			listvend.add(vrvo);

		}
		log.debug(" exit from xformToVoList  " +vendors.size());
		
		return listvend;
	}

	public List<VendorsRecordVO> getVendorsOnUserAndLocationFilter(
			VendorFilter vf)
	{
		if(vf == null)
			return null;
		
		log.debug(" inside getVendorsOnUserAndLocationFilter  "+vf.getBranchId());

		StringBuilder queryStr = new StringBuilder(SELECT_VENDOR_FOR_USER);

		StringBuilder WHERE_CLAUSE = new StringBuilder(" where id in ");

		StringBuilder userIdQuery = new StringBuilder(SELECT_VENDOR_ON_USERID);

		StringBuilder WHERE_CLAUSE_FOR_USER = new StringBuilder(" where uv.");

		StringBuilder WHERE_CLAUSE_AND = new StringBuilder(" and ");

		StringBuilder limitquery = new StringBuilder(" LIMIT ");

		int totalPageSize = 5;

		int startpageNo = 0;

		List<VendorsRecordVO> venRecordvoList = null;

		if (vf != null)
		{

			if (vf.getUserId() != null && vf.getUserId() > 0)

			{

				WHERE_CLAUSE_FOR_USER
						.append(" userId=" + vf.getUserId() + " )");

				userIdQuery.append(WHERE_CLAUSE_FOR_USER);

				log.info("@@@@@@@" + userIdQuery.toString());

			}
			if (vf.getCityId() != null && vf.getCityId() > 0)
			{

				if (WHERE_CLAUSE_AND.indexOf("=") > 0)

					WHERE_CLAUSE_AND.append(" and ");

				WHERE_CLAUSE_AND.append(" cityId=" + vf.getCityId());

			}
			if (vf.getAreaId() != null && vf.getAreaId() > 0)
			{

				if (WHERE_CLAUSE_AND.indexOf("=") > 0)
					WHERE_CLAUSE_AND.append(" and ");

				WHERE_CLAUSE_AND.append(" areaId=" + vf.getAreaId());

			}

			if (vf.getBranchId() != null && vf.getBranchId() > 0)
			{

				if (WHERE_CLAUSE_AND.indexOf("=") > 0)

					WHERE_CLAUSE_AND.append(" and ");

				WHERE_CLAUSE_AND.append(" branchId=" + vf.getBranchId());

			}

		}

		if (vf != null && vf.getPageSize() >= 0)
		{
			vf.setPageSize(MIN_ITEM_PER_PAGE);

			startpageNo = vf.getStartOffset();

			if (startpageNo == 0)
			{

			} else
			{
				// startpageNo = (startpageNo - 1) * totalPageSize + 1;
				startpageNo = (startpageNo) * totalPageSize;
			}

			limitquery.append(" " + (startpageNo) + "," + totalPageSize);

			// queryStr.append(limitquery);

			log.debug("limit page size::::" + queryStr.toString());
		}

		if (vf != null && vf.getUserId() != null && vf.getUserId() > 0)

		{

			WHERE_CLAUSE.append(userIdQuery);

			if ((vf.getBranchId() != null && vf.getBranchId() > 0)
					|| (vf.getCityId() != null && vf.getCityId() > 0)
					|| (vf.getAreaId() != null && vf.getAreaId() > 0))
			{
				WHERE_CLAUSE.append(WHERE_CLAUSE_AND);
			}
			if (vf.getStartOffset() >= 1 || vf.getPageSize() >= 1)
			{

				queryStr.append(WHERE_CLAUSE).append(limitquery);

			} else
			{
				queryStr.append(WHERE_CLAUSE).append("  limit  " + startpageNo
						+ "," + totalPageSize);
			}

		}

		log.debug(queryStr.toString());

		List<VendorsRecordVO> vendorlist = (List) jdbcTemplate
				.queryForList(queryStr.toString());

		log.debug("hibernateTemplate  Query  object ::" + vendorlist);

		return vendorlist;

	}

	@Override
	public long getVendorIdByVendorName(String vendorName)
	{
		log.debug(" inside getVendorIdByVendorName  "+vendorName);

		if (vendorNameAndIdCache.size() == 0)
			getVendors();

		if (vendorNameAndIdCache.containsKey(vendorName))

			return vendorNameAndIdCache.get(vendorName);

		else
			return 0;
	}

	@Override
	public Map<Long, String> getVendorsByBranchId(TypeAheadVo aheadVo)
	{

		Map<Long, String> vendorNames = new TreeMap<>();

		if( aheadVo == null )
			return vendorNames;
		
		log.debug(" inside getVendorsByBranchId  " + aheadVo.getId());
		
		if(vendorIdVendorDetails!=null && !vendorIdVendorDetails.isEmpty()){
		
		List<VendorVO> vendorVOs = new ArrayList<VendorVO>(vendorIdVendorDetails
				.values());

		for (VendorVO vendorVO : vendorVOs)
		{
			if (vendorVO.getBranchId() == aheadVo.getId())
			{
				vendorNames.put(vendorVO.getId(), vendorVO.getName());
			}

		}

		log.debug(" Exitting from  getVendorsByBranchId  " );
		
		return vendorNames;
		}else{
			log.debug(" Exitting from  getVendorsByBranchId  " );
			return null;
		}
	}

	@Override
	public List<VendorSummaryVO> skillList()
	{
		log.debug(" inside skillList  ");

		List<VendorSummaryVO> skill = jdbcTemplate
				.query("select id,skillName from Skill", new SkillRowMapper());

		for (VendorSummaryVO vendorSummaryVO : skill)
		{

			skillIdAndNameCache
					.put(Long.valueOf(vendorSummaryVO.getSkillId()), vendorSummaryVO
							.getSkillName());
		}

		log.debug(" Exitting from  skillList  ");
		
		return skill;
	}

	class SkillRowMapper implements RowMapper<VendorSummaryVO>
	{

		@Override
		public VendorSummaryVO mapRow(ResultSet rs, int i) throws SQLException
		{
			VendorSummaryVO vendorSummaryVO = new VendorSummaryVO();
			vendorSummaryVO.setSkillId(rs.getInt(1));
			vendorSummaryVO.setSkillName(rs.getString(2));

			return vendorSummaryVO;
		}
	}

	@Override
	public List<VendorDetailSummaryVO> getVendorSkillDetailByTl(List<Long> reportToId)
	{
		if( reportToId == null )
			return null;
		
		log.debug(" inside getVendorSkillDetailByTl  "+reportToId);
		
		List<Object[]> list = null;
		try{
			list = hibernateTemplate.getSessionFactory().
					getCurrentSession().createSQLQuery(SQL_QUERY_VENDOR_SKILL).
					setParameterList("reportToId", reportToId).
					setParameterList("status", ActionTypeAttributeDefinitions.VENDOR_TASK_DONE_STATUS).list();
			
			log.debug(" Exitting from getVendorSkillDetailByTl  "+reportToId);
			return xformObject(list);
		}catch(Exception exception){
			log.error("Error occure while fetching vendor skill detail of : "
					+reportToId,exception);
		}
		
		log.debug(" Exitting from getVendorSkillDetailByTl  "+reportToId);
		
		return xformObject(list);
	}

	private List<VendorDetailSummaryVO> xformObject(List<Object[]> list)
	{
		if( list == null)
			return null;
		
		log.debug(" inside xformObject  "+ list.size());
		
		List<VendorDetailSummaryVO> voObject = new ArrayList<VendorDetailSummaryVO>();
		VendorDetailSummaryVO vendorObject = null;
		
		Map<Long,List<VendorQueueData>> vendorQmap = new HashMap<Long, List<VendorQueueData>>();
		List<Object[]> anotherList = this.getMapObject(list,vendorQmap);
		for (Object[] vendorDetail : anotherList) 
		{
			vendorObject = new VendorDetailSummaryVO();
			vendorObject.setVendorId(FrTicketUtil.objectToLong(vendorDetail[2]));
			vendorObject.setVendorName(FrTicketUtil.objectToString(vendorDetail[3]) +" "+
					FrTicketUtil.objectToString(vendorDetail[4]));
			vendorObject.setSkillName(ActionTypeAttributeDefinitions.SKILL_DEFINITION
					.get(FrTicketUtil.objectToInt(vendorDetail[5])));
			Long key = vendorObject.getVendorId();
			if( key != null)
				{
				
				vendorObject.setVendorQueueData(vendorQmap.get(key));
				if(vendorObject.getVendorQueueData() != null && !vendorObject.getVendorQueueData().isEmpty())
					vendorObject.setNextJobInQueue(vendorObject.getVendorQueueData().get(0));
				}
			voObject.add(vendorObject);
		}
		
		log.debug(" Exitting xformObject  "+ list.size());
		
		return voObject;
	}

	private List<Object[]> getMapObject(List<Object[]> list,Map<Long,List<VendorQueueData>> vendorQmap)
	{
		if( list == null)
			return null;
		
		log.debug(" inside getMapObject  " +list.size());
		
		VendorQueueData vendQ = null;
		List<Object[]> anotherList = new ArrayList<Object[]>(); 
		for(Object[] vendorDetail : list)
		{
			List<VendorQueueData> qData = null;
			if ( vendorQmap.containsKey(FrTicketUtil.objectToLong(vendorDetail[2])))
			{
				qData = vendorQmap.get(FrTicketUtil.objectToLong(vendorDetail[2]));
				if(qData == null) continue;
				vendQ = new VendorQueueData();
				vendQ.setWorkOrderNumber(FrTicketUtil.objectToString(vendorDetail[0]));
				vendQ.setTicketId(FrTicketUtil.objectToLong(vendorDetail[1]));
				vendQ.setEstimatedStartTime(GenericUtil.convertToUiDateFormat(vendorDetail[7]));
				vendQ.setEstimatedEndTime(GenericUtil.convertToUiDateFormat(vendorDetail[8]));
				qData.add(vendQ);
				vendorQmap.put(FrTicketUtil.objectToLong(vendorDetail[2]), qData);
				
			}else{
			
				if(FrTicketUtil.objectToLong(vendorDetail[1])  > 0)
				{	
					qData = new ArrayList<VendorQueueData>();
					vendQ = new VendorQueueData();
					vendQ.setWorkOrderNumber(FrTicketUtil.objectToString(vendorDetail[0]));
					vendQ.setTicketId(FrTicketUtil.objectToLong(vendorDetail[1]));
					vendQ.setEstimatedStartTime(GenericUtil.convertToUiDateFormat(vendorDetail[7]));
					vendQ.setEstimatedEndTime(GenericUtil.convertToUiDateFormat(vendorDetail[8]));
					qData.add(vendQ);
				}
				vendorQmap.put(FrTicketUtil.objectToLong(vendorDetail[2]), qData);
				anotherList.add(vendorDetail);
			}
		}
		
		log.debug(" Exitting from  getMapObject  " +list.size());
		
		return anotherList;
	}
}
