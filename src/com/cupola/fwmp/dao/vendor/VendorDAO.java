package com.cupola.fwmp.dao.vendor;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.filters.VendorFilter;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.VendorDetailSummaryVO;
import com.cupola.fwmp.vo.VendorSummaryVO;
import com.cupola.fwmp.vo.VendorVO;
import com.cupola.fwmp.vo.VendorsRecordVO;

public interface VendorDAO {

	Long addVendor(VendorVO vendor);
	
	List<VendorsRecordVO> getAllVendors();

	String updateVendor(VendorVO vendor);
	
	String deleteVendor(Long id);
	
	List<VendorsRecordVO> getVendorsOnFilter(VendorFilter vendorFilter);
	
	List<VendorsRecordVO> getVendorsOnUserAndLocationFilter(VendorFilter vendorFilter);
	
	/*List<VendorsRecordVO>getAllVendorsByBranchId(VendorFilter vendorFilter);
	
	List<VendorsRecordVO>getAllVendorsByDate(VendorFilter vendorFilter);*/
	
	//List<VendorsRecordVO> getVendors(VendorFilter vendorFilter);

	void addIntoVendorSkillMapping(VendorVO vendor);

	int getCountAllVendor();

	void addVendorSkillAndCount(VendorVO vendorvo);

	String updateCountSkill(VendorVO vendorvo);

	int getCountAllVendorBySkill(Long id);
	
    //List<VendorSummaryVO>vendorAssignmentForTL(VendorVO vendorvo);
    
    List<VendorSummaryVO>  getVendorDetails(Long userId);
    
    Integer addUserVendorMapping(VendorVO vendorvo);
    
    String vendorAssignmentByTLForNE(VendorVO vendorvo);
    
    long getVendorIdByVendorName(String vendorName);
    
    Map<Long, String> getVendorsByBranchId(TypeAheadVo aheadVo);
    
    public List<VendorSummaryVO> skillList();

	List<VendorDetailSummaryVO> getVendorSkillDetailByTl(List<Long> reportToId);

	void addToCache(VendorVO vendor);
}
