/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.customernotification;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.fr.EmailCorporateDAOImpl;
import com.cupola.fwmp.persistance.entities.CustomerNotification;
import com.cupola.fwmp.persistance.entities.EmailCorporate;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CustomerNotificationVO;

@Transactional
public class CustomerNotificationDAOImpl implements CustomerNotificationDAO{

	private static Logger logger = Logger.getLogger(CustomerNotificationDAOImpl.class
			.getName());
	
	private static final String UPDATE_CustomerNotification_Status = "update CustomerNotification cn set cn.status=:status where cn.ticketId = :id";
	
	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	public CustomerNotification add(
			CustomerNotificationVO customerNotificationVO) {
		CustomerNotification newCustomerNotification = new CustomerNotification();
		logger.debug("inside add CustomerNotification  "  );
		if(customerNotificationVO != null)
		{
			BeanUtils.copyProperties(customerNotificationVO, newCustomerNotification);
			newCustomerNotification.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			hibernateTemplate.save(newCustomerNotification);
			return newCustomerNotification;
		}
		logger.debug("Exit From add CustomerNotification  "  );
		return null;
	}

	@Override
	public List<CustomerNotification> getAllTicketsToSendCustomerNotification() {
		
		logger.debug("inside getAllTicketsToSendCustomerNotification ");
		
		String query = "from CustomerNotification where status=102";
		List<CustomerNotification> customerNotifications  = (List<CustomerNotification>) hibernateTemplate
				.find(query);
		if(customerNotifications != null)
		{
			return customerNotifications;
		}
		
		logger.debug("Exit From getAllTicketsToSendCustomerNotification ");
		return null;
	}

	@Override
	public int updateCustomerNotificationStatus(long ticketId) {
		int row = 0;
		if(ticketId <= 0)
			return row;
		
		logger.debug("inside updateCustomerNotificationStatus " + ticketId);
		try {
			row = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(UPDATE_CustomerNotification_Status)
					.setParameter("status", TicketStatus.COMPLETED)
					.setParameter("id", ticketId).executeUpdate();

			logger.debug("Updated CustomerNotificationStatus for ticket " + ticketId
					+ " with status " + TicketStatus.COMPLETED);
		} catch (Exception e) {
			logger.error("Error in Updating CustomerNotificationStatus " + e.getMessage());
		}

		logger.debug("Exit From updateCustomerNotificationStatus " + ticketId);
		return row;
	}

}
