/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.customernotification;

import java.util.List;

import com.cupola.fwmp.persistance.entities.CustomerNotification;
import com.cupola.fwmp.vo.CustomerNotificationVO;

public interface CustomerNotificationDAO {
	CustomerNotification add(CustomerNotificationVO customerNotificationVO);
	List<CustomerNotification> getAllTicketsToSendCustomerNotification();
	int updateCustomerNotificationStatus(long ticketId);

}
