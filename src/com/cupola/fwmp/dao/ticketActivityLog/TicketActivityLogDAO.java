package com.cupola.fwmp.dao.ticketActivityLog;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.TicketActivityLog;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.WorkOrderVo;

public interface TicketActivityLogDAO {

	public WorkOrderVo getWorkOrderByTicketId(Long ticketId);

	public Integer addTicketActivityLog(TicketActivityLogVo ticketLog);

	public List<ActivityVo> getActivitiesByWoNo(Long ticketId);

	public Integer updateWorkOrderActivity(TicketActivityLogVo wolog);

	public Integer updateTicketActivityLog(TicketActivityLogVo woVo);

	public List<TicketActivityLog> getTicketByTicketId(Long ticketId);

	Integer updateTicketActivityLogByTicketId(
			TicketActivityLogVo ticketActivityLogVo);

	Integer updateTicketActivityLogRemarks(TicketActivityLogVo ticketLog);

	boolean isActivityClosed(Long ticketId, Long activityId);

	Integer updateTicketActivityLogStatus(TicketActivityLogVo ticketLog);

	boolean isActivityRejected(Long ticketId, Long activityId);

	int deleteTicketActivities(Long ticketId, Long workstageId);

	void updateTicketStatus(TicketActivityLogVo ticketLog);

	int deleteFromTicketActivityLog(Long ticketId, Long activityId);

	Map<Long, List<ActivityVo>> getActivitiesByTicketIds(List<Long> ticketIds);

	public Integer updateTicketActivityLogRemarksForClosedActivity(TicketActivityLogVo ticketLog);

}
