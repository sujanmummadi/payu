package com.cupola.fwmp.dao.ticketActivityLog;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.persistance.entities.Activity;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.TicketActivityLog;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.SubActivityVo;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.WorkOrderVo;

public class TicketActivityLogDAOImpl implements TicketActivityLogDAO
{

	private Logger log = Logger
			.getLogger(TicketActivityLogDAOImpl.class.getName());

	private final String INSERT_VALUES_TICKETACTIVITYLOG = "INSERT INTO  TicketActivityLog  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static String SELECT_WORKORDERACTIVITYLOG_QUERY = "SELECT u.* FROM TicketActivityLog u ";

	private static String Get_TICKET_BY_TICKETID = "select * from Ticket where id=? ";

	private static String JOIN_ACTIVITY = " inner join Activity ugm on u.activityId = ugm.id";
	/*
	 * private final String GET_WORKORDER_BY_ID =
	 * "select w.ticketId, w.assignedTo,w.workOrderTypeId,w.initialWorkStage, s.activityId,s.subActivityName, s.id as subactivityId from WorkOrder w, Activity a, SubActivity s  where w.currentActivity=a.id and s.activityId=a.id and w.workOrderNumber=?"
	 * ;
	 */

	private final String Get_WORKORDER_TYPE = "select workOrderNumber,workOrderTypeId from WorkOrder where ticketId=?";

	private final String UPDATE_TICKETACTIVITYLOG = "UPDATE TicketActivityLog SET subactivityId=?,vendorId=?,value=?,modifiedBy=?,modifiedOn=?,status=? where ticketId=? and activityId=? ";

	private final String UPDATE_TICKETACTIVITYLOG_BY_TICKETID = "UPDATE TicketActivityLog SET vendorId=?,value=?,modifiedBy=?,modifiedOn=?,status=? where ticketId=? and activityId=?";

	private final String UPDATE_ACTIVITY_WORKORDER = "UPDATE WorkOrder SET currentActivity=?,currentProgress=?,modifiedBy=?,modifiedOn=?,status=? where ticketId=?";

	private final String IS_ACTIVITY_CLOSED = "select * from TicketActivityLog where ticketId=? and activityId=? and status=101";

	private final String IS_ACTIVITY_REJECTED = "select * from TicketActivityLog where ticketId=? and activityId=? and status=110";

	private final String DELETE_TICKET_ACTIVITIES = "delete from TicketActivityLog where activityId in (select idActivity from WorkStageActivityMapping where idWorkStage = ?) and ticketId = ?";
	// private final String GET_WORKORDERACTIVITYLOG_BY_ID =
	// "select id from TicketActivityLog where workOrderNumber=? and
	// subactivityId=?";

	private final String UPDATE_TICKET_ACTIVITY_LOG_REMARKS = "update TicketActivityLog set Remarks = ?,modifiedOn=? where ticketId = ? and activityId = ?";

	private final String  UPDATE_TICKET_ACTIVITY_LOG_REMARKS_FOR_CLOSED_ACTIVITY = "update TicketActivityLog set Remarks = ?,modifiedOn=? where ticketId = ? and activityId = ? and status= 101";

	private final String UPDATE_TICKET_ACTIVITY_LOG_STATUS = "update TicketActivityLog set status = ?,modifiedOn=? where ticketId = ? and activityId = ?";

	private final String DELETE_TICKET_ACTIVITY_LOG_STATUS = "delete from TicketActivityLog where ticketId = ? and activityId = ?";

	
	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Override
	public WorkOrderVo getWorkOrderByTicketId(Long ticketId)
	{
		log.debug(" inside  getWorkOrderByTicketId  "  +ticketId);
		
		WorkOrderVo workOrder = null;

		try
		{
			workOrder = (WorkOrderVo) jdbcTemplate
					.queryForObject(Get_WORKORDER_TYPE, new Object[] {
							ticketId }, new WorkOrderMapper());
		} catch (Exception e)
		{
			log.error("error while getting ticketId from WorkOrder::"
					+ e.getMessage());
		}
		log.debug(" exit from  getWorkOrderByTicketId  "  +ticketId);

		return workOrder;

	}

	@Override
	public List<TicketActivityLog> getTicketByTicketId(Long ticketId)
	{
		log.debug(" inside  getTicketByTicketId  "  +ticketId);

		List<TicketActivityLog> ticket = (List<TicketActivityLog>) jdbcTemplate
				.query(Get_TICKET_BY_TICKETID, new Object[] {
						ticketId }, new WorkOrderActivityLogMapper());
		
		log.debug(" exit from  getTicketByTicketId  "  +ticketId);
		
		return ticket;

	}

	@Override
	public Integer updateTicketActivityLog(
			TicketActivityLogVo ticketActivityLogVo)
	{

		Integer rows = 0;
		if(ticketActivityLogVo == null)
			return rows;
		
		log.debug(" inside  updateTicketActivityLog  "  +ticketActivityLogVo.getTicketId() );
		try
		{
			Object[] params = { ticketActivityLogVo.getSubActivityId(),
					ticketActivityLogVo.getVendorId(),
					ticketActivityLogVo.getValue(),
					ticketActivityLogVo.getModifiedBy(),
					ticketActivityLogVo.getModifiedOn(),
					ticketActivityLogVo.getStatus(),
					ticketActivityLogVo.getTicketId(),
					ticketActivityLogVo.getActivityId() };

			int[] types = { Types.BIGINT, Types.BIGINT, Types.VARCHAR,
					Types.BIGINT, Types.TIMESTAMP, Types.INTEGER, Types.BIGINT,
					Types.BIGINT };

			rows = jdbcTemplate.update(UPDATE_TICKETACTIVITYLOG, params, types);
			if (ticketActivityLogVo.getSubActivityId() == null
					|| ticketActivityLogVo.getSubActivityId().equals(" "))
			{

			}
			if (rows > 0)
				log.info("TicketActivityLog  updated sucessfully as ticketId:"
						+ ticketActivityLogVo.getTicketId());

		} catch (Exception e)
		{
			log.error("error updateTicketActivityLog :" + e);
			return -1;

		}
		;
		log.debug(" exit from  updateTicketActivityLog  "  +ticketActivityLogVo.getTicketId() );
		return rows;

	}

	@Override
	public Integer updateTicketActivityLogByTicketId(
			TicketActivityLogVo ticketActivityLogVo)
	{

		Integer rows = 0;
		if(ticketActivityLogVo == null)
			return rows;
		
		log.debug(" inside  updateTicketActivityLogByTicketId  "  +ticketActivityLogVo.getActivityId());
		try
		{
			Object[] params = { ticketActivityLogVo.getVendorId(),
					ticketActivityLogVo.getValue(),
					ticketActivityLogVo.getModifiedBy(),
					ticketActivityLogVo.getModifiedOn(),
					ticketActivityLogVo.getStatus(),
					ticketActivityLogVo.getTicketId(),
					ticketActivityLogVo.getActivityId() };

			int[] types = { Types.BIGINT, Types.VARCHAR, Types.BIGINT,
					Types.TIMESTAMP, Types.INTEGER, Types.BIGINT,
					Types.BIGINT };

			rows = jdbcTemplate
					.update(UPDATE_TICKETACTIVITYLOG_BY_TICKETID, params, types);
			if (ticketActivityLogVo.getSubActivityId() == null
					|| ticketActivityLogVo.getSubActivityId().equals(" "))
			{

			}
			if (rows > 0)
				log.info("TicketActivityLog  updated sucessfully as ticketId:"
						+ ticketActivityLogVo.getTicketId());

		} catch (Exception e)
		{
			log.error("error updateTicketActivityLog :" + e);
			return -1;

		}
		;
		log.debug(" exit from  updateTicketActivityLogByTicketId  "  +ticketActivityLogVo.getActivityId());
		
		return rows;

	}

	@Override
	public Integer updateWorkOrderActivity(
			TicketActivityLogVo ticketActivityLogVo)
	{
		log.info("Update workOrder as values:" + ticketActivityLogVo.getTicketId());
		Integer rows = 1;
		
		if(ticketActivityLogVo == null)
			return rows;
		log.debug("inside Update workOrder activity .."+ ticketActivityLogVo.getActivityId());
		try
		{
			Object[] params = { ticketActivityLogVo.getActivityId(),
					ticketActivityLogVo.getCurrentProgress(),
					ticketActivityLogVo.getModifiedBy(),
					ticketActivityLogVo.getModifiedOn(),
					ticketActivityLogVo.getStatus(),
					ticketActivityLogVo.getTicketId() };

			int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT,
					Types.TIMESTAMP, Types.INTEGER, Types.BIGINT };

			jdbcTemplate.update(UPDATE_ACTIVITY_WORKORDER, params, types);

			if (rows <= 0)
			{
				log.info("WorkOrder currentActivity not updated as ticketId:"
						+ ticketActivityLogVo.getTicketId());
				return rows;
			}
			log.info("WorkOrder currentActivity updated as "
					+ ticketActivityLogVo.getActivityId()
					+ "successful for ticketId:"
					+ ticketActivityLogVo.getTicketId());
		} catch (Exception e)
		{
			log.error("error updateWorkOrderActivity:" + e);
			return -1;
		}
		log.debug("exit from Update workOrder activity .."+ ticketActivityLogVo.getActivityId());

		return rows;
	}

	@Override
	public Integer addTicketActivityLog(TicketActivityLogVo ticketLog)
	{
		int rowNum = 0;
		if(ticketLog == null)
			return rowNum;

		log.debug("Data to insert in ticket log table is " + ticketLog.getTicketId());
		try
		{
			rowNum = jdbcTemplate
					.update(INSERT_VALUES_TICKETACTIVITYLOG, new Object[] {
							ticketLog.getId(), ticketLog.getTicketId(),
							ticketLog.getWorkOrderNumber(),
							ticketLog.getActivityId(),
							ticketLog.getSubActivityId(),
							ticketLog.getVendorId(),
							ticketLog.getVendorDetails(), ticketLog.getValue(),
							ticketLog.getAssignedTo(), ticketLog.getAddedOn(),
							ticketLog.getAddedBy(), ticketLog.getModifiedOn(),
							ticketLog.getStatus(), ticketLog.getModifiedBy(),
							ticketLog.getRemarks() });

		} catch (Exception e)
		{
			log.error("Exception added data in ActivityLog :" + e);
			e.printStackTrace();
			return -1;
		}
		if (rowNum <= 0)
			log.info("data addded  in TicketActivityLog failed as ticketId:"
					+ ticketLog.getTicketId());
		else
		{
			log.info("data added in TicketActivityLog successful as ticketId:"
					+ ticketLog.getTicketId());
		}
		log.debug(" exit from Data to insert in ticket log table is " + ticketLog);
		return rowNum;

	}

	@Override
	public void updateTicketStatus(TicketActivityLogVo ticketLog)
	{
		if (ticketLog == null)
			return ;

		log.info("Updating Ticket status after populating TicketActivityLog "
				+ ticketLog.getTicketId());

		Session session = null;
		Transaction transaction = null;
		try
		{
			session = hibernateTemplate.getSessionFactory().openSession();

			Ticket ticket = (Ticket) session
					.get(Ticket.class, ticketLog.getTicketId());

			if (ticket != null)
			{

				transaction = session.beginTransaction();

				if (ticketLog
						.getActivityId() == FWMPConstant.Activity.CX_RACK_FIXING)
					ticket.setStatus(FWMPConstant.TicketStatus.CX_RACK_FIXING_DONE);
				else if (ticketLog
						.getActivityId() == FWMPConstant.Activity.COPPER_LAYING)
					ticket.setStatus(FWMPConstant.TicketStatus.COPPER_LAYING_DONE);
				else if (ticketLog
						.getActivityId() == FWMPConstant.Activity.FIBER_LAYING)
					ticket.setStatus(FWMPConstant.TicketStatus.FIBER_LAYING_DONE);
				else if (ticketLog
						.getActivityId() == FWMPConstant.Activity.SPLICING)
					ticket.setStatus(FWMPConstant.TicketStatus.SPLICING_DONE);
				else if (ticketLog
						.getActivityId() == FWMPConstant.Activity.MATERIAL_CONSUMPTION)
					ticket.setStatus(FWMPConstant.TicketStatus.MATERIAL_CONSUMPTION_UPDATED);
				else if (ticketLog
						.getActivityId() == FWMPConstant.Activity.PORT_ACTIVATION)
					ticket.setStatus(FWMPConstant.TicketStatus.PORT_ACTIVATION_DONE);
				else if (ticketLog
						.getActivityId() == FWMPConstant.Activity.CUSTOMER_ACCOUNT_ACTIVATION)
				{
					if (ticketLog
							.getStatus() == TicketStatus.ACTIVATION_PENDING)
						ticket.setStatus(FWMPConstant.TicketStatus.PORT_ACTIVATION_SUCCESSFULL);// if
																								// replied
																								// no
					else
						ticket.setStatus(FWMPConstant.TicketStatus.CUSTOMER_ACCOUNT_ACTIVATION_DONE);
				} else if (ticketLog
						.getActivityId() == FWMPConstant.Activity.PHYSICAL_FEASIBILITY)
					ticket.setStatus(FWMPConstant.TicketStatus.PHYSICAL_FEASIBILITY_DONE);

				session.update(ticket);

				transaction.commit();

			}
		} catch ( Exception e)
		{
			if (transaction != null)
				transaction.rollback();

			log.error("Error while updating ticket status for ticket "
					+ ticketLog.getTicketId() + " . Message " + e.getMessage());
			e.printStackTrace();

		} finally
		{

			if (session != null)
				session.close();
		}
	}

	@Override
	public Integer updateTicketActivityLogRemarks(TicketActivityLogVo ticketLog)
	{
		int rowNum = 0;
		if (ticketLog == null)
			return rowNum;

		log.info("updateTicketActivityLogRemarks" + ticketLog.getTicketId());
		try
		{
			rowNum = jdbcTemplate
					.update(UPDATE_TICKET_ACTIVITY_LOG_REMARKS, new Object[] {
							ticketLog.getRemarks(), new Date(),
							ticketLog.getTicketId(),
							ticketLog.getActivityId() });

		} catch (Exception e)
		{
			log.error("Exception updateTicketActivityLogRemarks  for ticket id "
					+ ticketLog.getTicketId() + " and Activity Id"
					+ ticketLog.getActivityId() + " " + e.getMessage());
			e.printStackTrace();
			return -1;
		}
		log.debug(" exit from updateTicketActivityLogRemarks" + ticketLog.getTicketId());

		return rowNum;

	}
	
	@Override
	public Integer updateTicketActivityLogRemarksForClosedActivity( TicketActivityLogVo ticketLog )
	{

		log.info("updateTicketActivityLogRemarks" + ticketLog);

		int rowNum = 0;
		try
		{
			rowNum = jdbcTemplate
					.update(UPDATE_TICKET_ACTIVITY_LOG_REMARKS_FOR_CLOSED_ACTIVITY, new Object[] {
							ticketLog.getRemarks(), new Date(),
							ticketLog.getTicketId(),
							ticketLog.getActivityId(),
							});

		} catch (Exception e)
		{
			log.error("Exception updateTicketActivityLogRemarks  for ticket id "
					+ ticketLog.getTicketId() + " and Activity Id"
					+ ticketLog.getActivityId() + " " + e.getMessage());
			e.printStackTrace();
			return -1;
		}

			return rowNum;
		
	}


	@Override
	public Integer updateTicketActivityLogStatus(TicketActivityLogVo ticketLog)
	{

		log.info("updateTicketActivityLogStatus " + ticketLog);

		int rowNum = 0;
		try
		{
			// "update TicketActivityLog set status = ?,modifiedOn=? where
			// ticketId = ? and activityId = ?";

			rowNum = jdbcTemplate
					.update(UPDATE_TICKET_ACTIVITY_LOG_STATUS, new Object[] {
							ticketLog.getStatus(), new Date(),
							ticketLog.getTicketId(),
							ticketLog.getActivityId() });

		} catch (Exception e)
		{
			log.error("Exception updateTicketActivityLogRemarks  for ticket id "
					+ ticketLog.getTicketId() + " and Activity Id"
					+ ticketLog.getActivityId() + " " + e.getMessage());
			e.printStackTrace();
			return -1;
		}
		log.debug(" exit from updateTicketActivityLogStatus " + ticketLog);
		return rowNum;

	}

	@Override
	public List<ActivityVo> getActivitiesByWoNo(Long ticketId)
	{
		Session session = null;
		List<ActivityVo> activityVos = new ArrayList<>();
		if (ticketId == null || ticketId <= 0)
			return activityVos;
		
		log.debug("getActivitiesByWoNo for " + ticketId);
		try
		{
			session = hibernateTemplate.getSessionFactory().openSession();
			StringBuilder queryStr = new StringBuilder(SELECT_WORKORDERACTIVITYLOG_QUERY);
			queryStr.append(JOIN_ACTIVITY);

			queryStr.append(" where u.ticketId=" + ticketId
					+ " order by addedOn  Desc");
			Query query = session.createSQLQuery(queryStr.toString())
					.addEntity(TicketActivityLog.class);
			List<TicketActivityLog> activityLogs = query.list();

			Map<Long, ActivityVo> activityVosList = new HashMap<Long, ActivityVo>();

			if (activityLogs != null)
			{

				for (TicketActivityLog ticketLog : activityLogs)
				{
					Activity activity = ticketLog.getActivity();

					log.debug(" Activity for ticket no in  Ticket Activity log is ");

					ActivityVo activityVo = new ActivityVo();
					BeanUtils.copyProperties(activity, activityVo);
					activityVo.setStatus(ticketLog.getStatus());
					activityVo.setVendorDetails(ticketLog.getVendorDetails());
					activityVo.setVendorId(ticketLog.getVendorId());
					activityVo.setRemarks(ticketLog.getRemarks());
					activityVo.setAddedBy(ticketLog.getAddedBy());
					activityVo.setAddedOn(ticketLog.getAddedOn());
					activityVo.setModifiedOn(ticketLog.getModifiedOn());
					// activityVo.setActivityName(activity.getActivityName());
					Set<SubActivityVo> subActivitiesVo = new HashSet<SubActivityVo>();
					SubActivityVo vo = new SubActivityVo();

					if (ticketLog.getSubActivity() != null)
					{
						SubActivityVo voDef = WorkOrderDefinitionCache
								.getSubActivityById(ticketLog.getSubActivity()
										.getId());

						if (voDef == null)
							continue;
						BeanUtils.copyProperties(voDef, vo);

						if (ticketLog.getValue() != null)
							vo.setValue(ticketLog.getValue());

						subActivitiesVo.add(vo);
					}

					if (activityVosList.get(activityVo.getId()) != null)

					{
						activityVosList.get(activityVo.getId())
								.getSubActivityList().addAll(subActivitiesVo);
					} else
					{
						activityVo.setSubActivityList(subActivitiesVo);
						activityVosList.put(activityVo.getId(), activityVo);
					}

					log.debug(" subActivitiesVo for ticket no " + ticketId
							+ " in  Ticket Activity log is " + subActivitiesVo);

				}
			}

			return new ArrayList<ActivityVo>(activityVosList.values());

		} catch (BeansException e)
		{
			log.error("Bean exception in getActivitiesByWoNo "
					+ e.getMessage());
			e.printStackTrace();
		} catch ( Exception e)
		{
			log.error("HibernateException in getActivitiesByWoNo "
					+ e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		log.debug(" exit from getActivitiesByWoNo for " + ticketId);
		return activityVos;
	}
	@Override
	public Map< Long ,List<ActivityVo>> getActivitiesByTicketIds(List<Long> ticketIds)
	{
		log.debug("getActivitiesByWoNo for " + ticketIds);
		Session session = null;
		
		Map< Long ,List<ActivityVo>> ticketActivityVos = new HashMap< Long ,List<ActivityVo>>();
		
		try
		{
			session = hibernateTemplate.getSessionFactory().openSession();
			StringBuilder queryStr = new StringBuilder(SELECT_WORKORDERACTIVITYLOG_QUERY);
			queryStr.append(JOIN_ACTIVITY);

			queryStr.append(" where u.ticketId in (:ticketIds)  order by addedOn  Desc");
			Query query = session.createSQLQuery(queryStr.toString())
					.addEntity(TicketActivityLog.class);
			query.setParameterList("ticketIds",ticketIds);
			List<TicketActivityLog> activityLogs = query.list();

			
			Map<Long,	Map<Long, ActivityVo> > activityVosListAll =
					new HashMap<Long, Map<Long, ActivityVo> >();
			
			if (activityLogs != null)
			{

				for (TicketActivityLog ticketLog : activityLogs)
				{
					Map<Long, ActivityVo> activityVosList = null;
					if(activityVosListAll.containsKey(ticketLog.getTicketId()))
							 activityVosList = activityVosListAll.get(ticketLog.getTicketId());
					else
						{
						
						activityVosList =  new HashMap<Long, ActivityVo>();
						activityVosListAll.put(ticketLog.getTicketId(), activityVosList);
						}
					
					Activity activity = ticketLog.getActivity();

					log.debug(" Activity for ticket no in  Ticket Activity log is " + ticketLog.getStatus());

					ActivityVo activityVo = new ActivityVo();
					BeanUtils.copyProperties(activity, activityVo);
					activityVo.setStatus(ticketLog.getStatus());
					activityVo.setVendorDetails(ticketLog.getVendorDetails());
					activityVo.setVendorId(ticketLog.getVendorId());
					activityVo.setRemarks(ticketLog.getRemarks());
					activityVo.setAddedBy(ticketLog.getAddedBy());
					activityVo.setAddedOn(ticketLog.getAddedOn());
					activityVo.setModifiedOn(ticketLog.getModifiedOn());
					// activityVo.setActivityName(activity.getActivityName());
					Set<SubActivityVo> subActivitiesVo = new HashSet<SubActivityVo>();
					SubActivityVo vo = new SubActivityVo();

					if (ticketLog.getSubActivity() != null)
					{
						SubActivityVo voDef = WorkOrderDefinitionCache
								.getSubActivityById(ticketLog.getSubActivity()
										.getId());

						if (voDef == null)
							continue;
						BeanUtils.copyProperties(voDef, vo);

						if (ticketLog.getValue() != null)
							vo.setValue(ticketLog.getValue());

						subActivitiesVo.add(vo);
					}

					if (activityVosList.get(activityVo.getId()) != null)

					{
						activityVosList. get(activityVo.getId())
								.getSubActivityList().addAll(subActivitiesVo);
					} else
					{
						activityVo.setSubActivityList(subActivitiesVo);
						activityVosList.put(activityVo.getId(), activityVo);
					}

					log.debug(" subActivitiesVo for ticket no " + ticketIds
							+ " in  Ticket Activity log is " + subActivitiesVo);

				}
				
				for( Entry<Long, Map<Long, ActivityVo>> entry : activityVosListAll.entrySet())
				{
					if(ticketActivityVos.containsKey(entry.getKey()))
					{
						ticketActivityVos.get(entry.getKey()).addAll(entry.getValue().values());
					}
					else
					{
						ticketActivityVos.put(entry.getKey(), new ArrayList<ActivityVo>());
						ticketActivityVos.get(entry.getKey()).addAll(entry.getValue().values());
					}
				}
			}
			 
			log.debug("getActivitiesByWoNo ticketActivityVos " + ticketActivityVos.size());
			
			return ticketActivityVos;

		} catch (BeansException e)
		{
			log.error("Bean exception in getActivitiesByWoNo "
					+ e.getMessage());
			e.printStackTrace();
		} catch ( Exception e)
		{
			log.error("HibernateException in getActivitiesByWoNo "
					+ e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		log.debug(" exit from getActivitiesByWoNo for " + ticketIds);
		return ticketActivityVos;
	}
	@Override
	public boolean isActivityClosed(Long ticketId, Long activityId)
	{
		log.debug(" inside  isActivityClosed " + ticketId);
		
		List activityList = jdbcTemplate
				.queryForList(IS_ACTIVITY_CLOSED, new Object[] { ticketId,
						activityId });

		

		if (activityList != null && activityList.size() > 0
				&& !activityList.isEmpty())
		{
			log.debug("Returing Activity Closed for ticket id " + ticketId + " and "
					+ activityId + " is " + activityList.size());
			return true;
		}
			
		else
			return false;
	}

	@Override
	public boolean isActivityRejected(Long ticketId, Long activityId)
	{
		log.debug(" inside  isActivityRejected "+ticketId);
		List activityList = jdbcTemplate
				.queryForList(IS_ACTIVITY_REJECTED, new Object[] { ticketId,
						activityId });

		
		if (activityList != null && activityList.size() > 0
				&& !activityList.isEmpty())
		{
			log.debug("Returing Activity Closed for ticket id " + ticketId + " and "
					+ activityId + " is " + activityList.size());
			
			return true;

		}
			
		else
			return false;
	}

	@Override
	public int deleteTicketActivities(Long ticketId, Long workstageId)
	{
		log.debug(" inside  deleteTicketActivities " + ticketId);
		int deletedRows = jdbcTemplate
				.update(DELETE_TICKET_ACTIVITIES, new Object[] { workstageId,
						ticketId });

		log.info("Deleting Activity for ticket id " + ticketId + " and "
				+ workstageId + " is " + deletedRows);

		return deletedRows;
	}
	
	@Override
	public int deleteFromTicketActivityLog(Long ticketId, Long activityId)
	{
		log.debug(" inside  deleteFromTicketActivityLog " +ticketId );
		return  jdbcTemplate
				.update(DELETE_TICKET_ACTIVITY_LOG_STATUS, new Object[] { ticketId,
						activityId });
	}

}

class WorkOrderMapper implements RowMapper<WorkOrderVo>
{

	@Override
	public WorkOrderVo mapRow(ResultSet rs, int arg1) throws SQLException
	{

		WorkOrderVo workVo = new WorkOrderVo();

		workVo.setWorkOrderNo(rs.getString("workOrderNumber"));
		workVo.setWorkOrderTypeId(rs.getLong("workOrderTypeId"));

		return workVo;
	}

}

class WorkOrderActivityLogMapper implements RowMapper<TicketActivityLog>
{

	@Override
	public TicketActivityLog mapRow(ResultSet rs, int arg1) throws SQLException
	{
		TicketActivityLog ticketLog = new TicketActivityLog();
		ticketLog.setId(rs.getLong("id"));
		return ticketLog;
	}
	
	
	

}

/**
 * class WorkOrderMapper implements RowMapper<WorkOrder> {
 * 
 * @Override public WorkOrder mapRow(ResultSet rs, int arg1) throws SQLException
 *           { WorkOrder work = new WorkOrder(); Activity act = new Activity();
 *           act.setId(rs.getLong("currentActivity")); WorkOrderType wt = new
 *           WorkOrderType(); wt.setId(rs.getLong("workOrderTypeId"));
 *           work.setActivity(act); work.setWorkOrderType(wt);
 *           work.setWorkOrderNumber("workOrderNumber"); //
 *           work.setAssignedTo(rs.getLong("assignedTo")); return work; }
 * 
 *           }
 */
