/**
* @Author aditya  25-Aug-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.dao.ticketReassignmentLog;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.persistance.entities.TicketReassignmentLog;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;

/**
 * @Author aditya 25-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public class TicketReassignmentLogDAOImpl implements TicketReassignmentLogDAO {

	private Logger log = LogManager.getLogger(TicketReassignmentLogDAOImpl.class.getName());

	@Autowired
	HibernateTemplate hibernateTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cupola.fwmp.dao.ticketReassignmentLog.TicketReassignmentLogDAO#
	 * add2TicketReassignmentLog(com.cupola.fwmp.vo.TicketReassignmentLogVo)
	 */
	@Override
	@Transactional
	public TicketReassignmentLogVo add2TicketReassignmentLog(TicketReassignmentLogVo ticketReassignmentLogVo) {

		if (ticketReassignmentLogVo == null) {

			log.info("TicketReassignmentLog is null ");

			return ticketReassignmentLogVo;
		}
		try {

			log.info("Adding to TicketReassignmentLog for Ticket id " + ticketReassignmentLogVo.getTicketId());

			TicketReassignmentLog ticketReassignmentLog = new TicketReassignmentLog();

			BeanUtils.copyProperties(ticketReassignmentLogVo, ticketReassignmentLog);

			ticketReassignmentLog.setId(StrictMicroSecondTimeBasedGuid.newGuid());

			ticketReassignmentLogVo.setId((Long) hibernateTemplate.save(ticketReassignmentLog));

			log.info("Added to TicketReassignmentLog for Ticket id " + ticketReassignmentLogVo.getTicketId()
					+ " with id " + ticketReassignmentLogVo.getId());

		} catch (BeansException e) {
			log.error("Error while copying bean for ticket id " + ticketReassignmentLogVo.getTicketId() + " "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			log.error(
					"Error while processing ticket id " + ticketReassignmentLogVo.getTicketId() + " " + e.getMessage());
			e.printStackTrace();
		}

		return ticketReassignmentLogVo;
	}

}
