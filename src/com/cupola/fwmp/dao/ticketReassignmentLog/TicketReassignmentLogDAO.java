/**
* @Author aditya  25-Aug-2017
* @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
*/
package com.cupola.fwmp.dao.ticketReassignmentLog;

import com.cupola.fwmp.vo.TicketReassignmentLogVo;

/**
 * @Author aditya 25-Aug-2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface TicketReassignmentLogDAO {

	TicketReassignmentLogVo add2TicketReassignmentLog(TicketReassignmentLogVo ticketReassignmentLogVo);
}
