package com.cupola.fwmp.dao.workOrderType;

import java.util.Map;

import com.cupola.fwmp.vo.WorkOrderTypeVO;

public interface WorkOrderTypeDAO {

	
	Map<Long, WorkOrderTypeVO> getAllWorkOrderTypes();
	
}
