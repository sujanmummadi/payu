package com.cupola.fwmp.dao.workOrderType;



import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.persistance.entities.WorkOrderType;
import com.cupola.fwmp.vo.WorkOrderTypeVO;

@Transactional
public class WorkOrderTypeDAOImpl  implements WorkOrderTypeDAO{

	private static final Logger logger = LogManager.getLogger(WorkOrderTypeDAOImpl.class.getName());
	private HibernateTemplate hibernateTemplate;
	
	@Override
	public Map<Long, WorkOrderTypeVO> getAllWorkOrderTypes() 
	{
		logger.debug(" inside getAllWorkOrderTypes method  ");
		
		Map<Long, WorkOrderTypeVO> localCache = new TreeMap<Long, WorkOrderTypeVO>();
		try {
		List<WorkOrderType> types =
				(List<WorkOrderType>) hibernateTemplate.find("from WorkOrderType a where a.status = 1");
		
		if(types == null)
			return localCache;
		WorkOrderTypeVO vo = null;
		
		for(WorkOrderType type : types)
		{
			vo = new WorkOrderTypeVO();
			BeanUtils.copyProperties(type, vo);
			localCache.put(type.getId(), vo);
		}
		logger.debug("Found work order types :  "+localCache.size());
		} catch (Exception e) {
			logger.error("Error occured while getAllWorkOrderTypes ",e);
			e.printStackTrace();
		} 
		
		logger.debug(" Exitting from  getAllWorkOrderTypes  ");
		
		return localCache;
		
	}
	public HibernateTemplate getHibernateTemplate()
	{
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}
}
