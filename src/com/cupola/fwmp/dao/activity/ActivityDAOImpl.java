package com.cupola.fwmp.dao.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.persistance.entities.Activity;
import com.cupola.fwmp.persistance.entities.WorkStage;
import com.cupola.fwmp.vo.ActivityVo;

public class ActivityDAOImpl  implements ActivityDAO{

	private static final Logger logger = LogManager.getLogger(ActivityDAOImpl.class.getName());
	@Autowired
	HibernateTemplate hibernateTemplate;

	private static long cacheAgeInMillis =  24 * 60 *60 *1000;
	private static long cacheResetTimestamp = 0;
	static List<ActivityVo> activitiesVo =  new ArrayList<ActivityVo>(); 
	@Override
	public List<ActivityVo> getActivitiesForWorkStage(Long workStageId)
	{
		List<ActivityVo> activitiesVo = new ArrayList<ActivityVo>();
		
		if( workStageId == null || workStageId.longValue() < 0 )
			return activitiesVo;
		
		logger.debug("Getting Activites for WorkStage Id :"+workStageId);
		
		try
		{	
			WorkStage stage = hibernateTemplate.get(WorkStage.class, workStageId);
		
			if (stage == null || stage.getActivities() == null )
				return activitiesVo;

	
			Set<Activity> activities = stage.getActivities();
			ActivityVo vo = null;
			for (Iterator iterator = activities.iterator(); iterator.hasNext();)
			{
				Activity activity = (Activity) iterator.next();
				vo = new ActivityVo();
				BeanUtils.copyProperties(activity, vo);
				activitiesVo.add(vo);
			}

			logger.debug("Found activities " + activitiesVo.size()
					+ " for workstage " + workStageId);
			return activitiesVo;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			logger.error("Error while Getting Activites for WorkStage Id : " +workStageId,e);
		}
		 
		logger.debug(" Exit From Getting Activites for WorkStage Id :"+workStageId);
		
		return activitiesVo;
	}
	
	@Override
	public List<ActivityVo> getAllActivities( )
	{
		logger.debug("Inside getting All Activities : " );
		
		resetCacheIfOld();
		
		logger.debug(" Exit From  gettingAllActivities : " );
		
		return populateAllActivities();
	}
	
	private List<ActivityVo> populateAllActivities( )
	{
		logger.debug(" Inside populating all activities : ");
		
		if(activitiesVo.size() != 0)
			return activitiesVo;
		
		try {
			List<Activity> activities = (List<Activity>) hibernateTemplate.find("from Activity a where a.status = 1");
			ActivityVo vo = null;
			
			if( activities == null || (activities != null && activities.isEmpty()))
				return activitiesVo;
			
			for (Iterator iterator = activities.iterator(); iterator.hasNext();)
			{
				Activity activity = (Activity) iterator.next();
				vo = new ActivityVo();
				BeanUtils.copyProperties(activity, vo);
				activitiesVo.add(vo);
			}

			logger.debug("Found All activities " + activitiesVo.size());
			
		} catch (Exception e) {
			 
			e.printStackTrace();
			logger.error("Error occured while Populating All Activites "  ,e);
		}  
		logger.debug(" Exit from  populating all activities : ");
		return activitiesVo;

	}



	private void resetCacheIfOld()
	{
		logger.debug(" inside resetCacheIfOld : ");
		if ((System.currentTimeMillis() - cacheResetTimestamp) > cacheAgeInMillis)
		{
			resetCache();
		}
		logger.debug(" Exit from resetCacheIfOld : ");
	}
	

	public void resetCache()
	{
		logger.debug(" inside resetCache : ");
		
		activitiesVo.clear();
		cacheResetTimestamp = System.currentTimeMillis();
		
		logger.debug(" Exit from resetCache : ");
	}
}
