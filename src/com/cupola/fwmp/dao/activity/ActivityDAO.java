package com.cupola.fwmp.dao.activity;

import java.util.List;

import com.cupola.fwmp.vo.ActivityVo;

public interface ActivityDAO
{
	List<ActivityVo> getActivitiesForWorkStage(Long workStageId);

	List<ActivityVo> getAllActivities();

}
