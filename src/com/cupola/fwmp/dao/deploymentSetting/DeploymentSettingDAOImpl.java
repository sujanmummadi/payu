package com.cupola.fwmp.dao.deploymentSetting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cupola.fwmp.persistance.entities.DeploymentSetting;
import com.cupola.fwmp.vo.DeploymentSettingVo;

public class DeploymentSettingDAOImpl implements DeploymentSettingDAO {

	private static Logger log = LogManager
			.getLogger(DeploymentSettingDAOImpl.class.getName());

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static String GET_ALL_DEPLOYMENT_SETTINGS_QUERY = "select * from DeploymentSetting";

	@Override
	public DeploymentSetting addDeploymentSetting(
			DeploymentSetting deploymentSetting) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentSetting getDeploymentSettingById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DeploymentSettingVo> getAllDeploymentSetting(){
		
		log.debug(" Inside Getting All deployment setting ");
		
		try
		{ 
			return jdbcTemplate.query(GET_ALL_DEPLOYMENT_SETTINGS_QUERY,
				new DeploymentSettingMapper());
		} catch(Exception e)
		{
			log.error("Error in getting deployment setting " + e.getMessage());
			e.printStackTrace();
			return new ArrayList<DeploymentSettingVo>();
		}
	}

	@Override
	public DeploymentSetting deleteDeploymentSetting(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DeploymentSetting updateDeploymentSetting(
			DeploymentSetting deploymentSetting) {
		// TODO Auto-generated method stub
		return null;
	}

	class DeploymentSettingMapper implements RowMapper<DeploymentSettingVo> {

		@Override
		public DeploymentSettingVo mapRow(ResultSet resultSet, int rowNumber)
				throws SQLException {

			DeploymentSettingVo deploymentSettingVo = new DeploymentSettingVo();
		
			deploymentSettingVo.setName(resultSet.getString("name"));
			deploymentSettingVo.setValue(resultSet.getString("value"));

			return deploymentSettingVo;
		}

	}

}
