package com.cupola.fwmp.dao.deploymentSetting;

import java.util.List;

import com.cupola.fwmp.persistance.entities.DeploymentSetting;
import com.cupola.fwmp.vo.DeploymentSettingVo;

public interface DeploymentSettingDAO {

	public DeploymentSetting addDeploymentSetting(DeploymentSetting deploymentSetting);

	public DeploymentSetting getDeploymentSettingById(Long id);

	public List<DeploymentSettingVo> getAllDeploymentSetting();

	public DeploymentSetting deleteDeploymentSetting(Long id);

	public DeploymentSetting updateDeploymentSetting(DeploymentSetting deploymentSetting);
	
}
