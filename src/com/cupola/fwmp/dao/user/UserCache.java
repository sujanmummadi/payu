/**
 * 
 */
package com.cupola.fwmp.dao.user;

import java.util.Set;

import com.cupola.fwmp.FWMPConstant.DBActiveStatus;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;

/**
 * @author aditya
 *
 */
public interface UserCache
{
	String userAreaCacheQuery = "select u.id as userId,u.loginId as loginId, u.empId as empId,"
			+ " a.id as areaId,a.areaName as areaName,a.areaCode as areaCode from Area a "
			+ "join UserAreaMapping uam on a.id=uam.areaId join User u on u.id=uam.userId where u.status = "
			+ DBActiveStatus.ACTIVE + " order by u.id";

	String userBranchCacheQuery = "select u.id as userId,u.loginId as loginId, u.empId as empId,"
			+ " b.id as branchId,b.branchName as branchName,b.branchCode as branchCode from Branch b "
			+ "join UserBranchMapping ubm on b.id=ubm.idBranch join User u on u.id=ubm.idUser where u.status = "
			+ DBActiveStatus.ACTIVE + " order by u.id";

	String userCityCacheQuery = "select u.id as userId,u.loginId as loginId, u.empId as empId, "
			+ "c.id as cityId,c.cityName as cityName,c.cityCode as cityCode from City c "
			+ "join UserCityMapping ubm on c.id=ubm.idCity join User u on u.id=ubm.idUser where u.status = "
			+ DBActiveStatus.ACTIVE + " order by u.id";

	String userDeviceCacheQuery = "select u.id as userId,u.loginId as loginId, u.empId as empId,"
			+ " d.id as deviceId, d.deviceName as deviceName from Device d join UserDeviceMapping udm "
			+ "on udm.deviceId = d.id join User u on u.id=udm.userId where u.status = "
			+ DBActiveStatus.ACTIVE + " order by u.id";

	String userTabletCacheQuery = "select u.id as userId,u.loginId as loginId, u.empId as empId, "
			+ "tab.id as tabletId,tab.macAddress as tabletMac from Tablet tab Join User u on u.id=tab.assignedTo where u.status = "
			+ DBActiveStatus.ACTIVE;

	String userSkillCacheQuery = "select u.id as userId,u.loginId as loginId, u.empId as empId, "
			+ "s.id as skillId,s.skillName as skillName from Skill s Join UserSkillMapping usm on usm.idSkill=s.id "
			+ "Join User u on u.id=usm.idUser where u.status = "
			+ DBActiveStatus.ACTIVE;

	String userUserGroupCacheQuery = "select u.id as userId,u.loginId as loginId, u.empId as empId, "
			+ "ug.id as userGroupId,ug.userGroupcode as userGroupcode, ug.userGroupName as userGroupName "
			+ "from UserGroup ug join UserGroupUserMapping ugum on ugum.idUserGroup=ug.id join User u on u.id=ugum.idUser where u.status = "
			+ DBActiveStatus.ACTIVE;

	String userEmployeerCacheQuery = "";

	String tabletCacheQuery = "select id as tabId,macAddress as tabMac from Tablet";

	String deviceCacheQuery = "select id as deviceId,deviceName as deviceName from Device";

	int userAreaCache(String loginId);

	int userBranchCache(String loginId);

	int userCityCache(String loginId);

	int userDeviceCache(String loginId);

	int tabletCache();

	int userTabletCache(String loginId);

	int userSkillCache(String loginId);

	int userEmployeerCache(String loginId);

	int deviceCache();

	int userUserGroupCache(String loginId);

	Set<TypeAheadVo> getUserAreaByLoginId(String loginId);

	Set<TypeAheadVo> getUserBranchByLoginId(String loginId);

	Set<TypeAheadVo> getUserCityByLoginId(String loginId);

	Set<TypeAheadVo> getUserDeviceByLoginId(String loginId);

	Set<TypeAheadVo> getUserTabletByLoginId(String loginId);

	Set<TypeAheadVo> getUserSkillByLoginId(String loginId);

	Set<TypeAheadVo> getUserEmployeerByLoginId(String loginId);

	void init();

	Set<TypeAheadVo> getUserUserGroupByLoginId(String loginId);

	/**
	 * @author aditya 3:53:33 PM Apr 10, 2017 boolean
	 * @param loginId
	 * @return
	 */
	boolean getFxByName(String loginId);

	void setIncrementalCache(UserVo userVo);

	/**
	 * @author aditya 5:23:57 PM Apr 17, 2017 void
	 * @param loginId
	 */
	void resetCacheBId(String loginId);

}
