/**
 * 
 */
package com.cupola.fwmp.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.user.UserVoModified;

/**
 * @author aditya
 *
 */
public class UserCacheImpl implements UserCache
{
	private static Logger log = LogManager.getLogger(UserCacheImpl.class);

	private Map<String, Set<TypeAheadVo>> userAreaCache = new ConcurrentHashMap<String, Set<TypeAheadVo>>();

	private Map<String, Set<TypeAheadVo>> userBranchCache = new ConcurrentHashMap<String, Set<TypeAheadVo>>();

	private Map<String, Set<TypeAheadVo>> userCityCache = new ConcurrentHashMap<String, Set<TypeAheadVo>>();

	private Map<String, Set<TypeAheadVo>> userDeviceCache = new ConcurrentHashMap<String, Set<TypeAheadVo>>();

	private Map<String, Set<TypeAheadVo>> userTabletCache = new ConcurrentHashMap<String, Set<TypeAheadVo>>();

	private Map<String, Set<TypeAheadVo>> userSkillCache = new ConcurrentHashMap<String, Set<TypeAheadVo>>();

	private Map<String, Set<TypeAheadVo>> userUserGroupCache = new ConcurrentHashMap<String, Set<TypeAheadVo>>();

	private Map<String, Set<Long>> userEmployeerCache = new ConcurrentHashMap<String, Set<Long>>();

	private Map<Long, String> tabletCache = new ConcurrentHashMap<Long, String>();

	private Map<Long, String> deviceCache = new ConcurrentHashMap<Long, String>();

	private Map<String, String> deviceNameIdCache = new ConcurrentHashMap<String, String>();

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	AreaDAO areaDAO;

	@Autowired
	BranchDAO branchDAO;

	@Autowired
	CityDAO cityDAO;

	@Override
	public void init()
	{
		log.debug(" initializing user cache .." );
		
		deviceCache();

		tabletCache();

		userAreaCache(null);

		userBranchCache(null);

		userCityCache(null);

		userDeviceCache(null);

		userTabletCache(null);

		userSkillCache(null);

		// userEmployeerCache();

		userUserGroupCache(null);
		
		log.debug("exit from  initializing user cache .." );
	}

	@Override
	public int userAreaCache(String loginId)
	{
		log.info("User Area Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			if (loginId != null)
			{
				String tempUserAreaCacheQuery = userAreaCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");
				log.info("User Area Cache Called for user " + loginId
						+ " tempUserAreaCacheQuery " + tempUserAreaCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(userAreaCacheQuery, new UserAreaCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userAreaCacheQuery, new UserAreaCacheMapper());
			}

			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User Area Cache are " + userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					String userLoginId = userVoModified.getLoginId();

					log.debug("User id " + userLoginId);

					if (userAreaCache.containsKey(userLoginId))
					{
						Set<TypeAheadVo> userAreas = userAreaCache
								.get(userLoginId);

						Long areaId = userVoModified.getAreaId() != null
								? Long.valueOf(userVoModified.getAreaId()) : 0;

						String areaName = areaDAO.getAreaNameById(areaId);

						log.debug("User id " + userLoginId + " contains areas "
								+ userAreas + " brfore adding " + areaName);

						userAreas.add(new TypeAheadVo(areaId, areaName));

						userAreaCache.put(userLoginId, userAreas);

						log.debug("User id " + userLoginId + " contains areas "
								+ userAreas + " After adding " + areaName);

					} else
					{

						Set<TypeAheadVo> userAreas = new LinkedHashSet<>();

						Long areaId = userVoModified.getAreaId() != null
								? Long.valueOf(userVoModified.getAreaId()) : 0;

						String areaName = areaDAO.getAreaNameById(areaId);

						userAreas.add(new TypeAheadVo(areaId, areaName));

						log.debug("User id 1st time adding " + userLoginId
								+ " contains areas " + userAreas
								+ " After adding " + areaName);

						userAreaCache.put(userLoginId, userAreas);

					}

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user Area Cache "
					+ e.getMessage());
			e.printStackTrace();
		}

		log.info("userAreaCache " + userAreaCache.size());
		return size;
	}

	@Override
	public int userBranchCache(String loginId)
	{
		log.info("User Branch Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			if (loginId != null)
			{
				String tempUserBranchCacheQuery = userBranchCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");

				log.info("User Branch Cache Called for user " + loginId
						+ " tempUserBranchCacheQuery "
						+ tempUserBranchCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(userBranchCacheQuery, new UserBranchCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userBranchCacheQuery, new UserBranchCacheMapper());
			}

			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User Branch Cache are "
						+ userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					String userLoginId = userVoModified.getLoginId();

					if (userBranchCache.containsKey(userLoginId))
					{
						Set<TypeAheadVo> userBranches = userBranchCache
								.get(userLoginId);

						Long branchId = userVoModified.getBranchId() != null
								? Long.valueOf(userVoModified.getBranchId())
								: 0;

						String branchName = branchDAO
								.getBranchNameById(branchId);

						userBranches.add(new TypeAheadVo(branchId, branchName));

						userBranchCache.put(userLoginId, userBranches);

					} else
					{
						Set<TypeAheadVo> userBranches = new LinkedHashSet<>();

						Long branchId = userVoModified.getBranchId() != null
								? Long.valueOf(userVoModified.getBranchId())
								: 0;

						String branchName = branchDAO
								.getBranchNameById(branchId);

						userBranches.add(new TypeAheadVo(branchId, branchName));

						userBranchCache.put(userLoginId, userBranches);

					}

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user Branch Cache "
					+ e.getMessage());
			e.printStackTrace();
		}
		log.info("userBranchCache " + userBranchCache.size());

		return size;
	}

	@Override
	public int userCityCache(String loginId)
	{
		log.info("User City Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			if (loginId != null)
			{
				String tempUserCityCacheQuery = userCityCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");

				log.info("User City Cache Called for user " + loginId
						+ " tempUserCityCacheQuery " + tempUserCityCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(userCityCacheQuery, new UserCityCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userCityCacheQuery, new UserCityCacheMapper());
			}
			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User City Cache are " + userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					String userLoginId = userVoModified.getLoginId();

					if (userCityCache.containsKey(userLoginId))
					{
						Set<TypeAheadVo> userCity = userCityCache
								.get(userLoginId);

						Long cityId = userVoModified.getCityId() != null
								? Long.valueOf(userVoModified.getCityId()) : 0;

						String cityName = cityDAO.getCityNameById(cityId);

						userCity.add(new TypeAheadVo(cityId, cityName));

						userCityCache.put(userLoginId, userCity);

					} else
					{

						Set<TypeAheadVo> userCity = new LinkedHashSet<>();

						Long cityId = userVoModified.getCityId() != null
								? Long.valueOf(userVoModified.getCityId()) : 0;
						String cityName = cityDAO.getCityNameById(cityId);

						userCity.add(new TypeAheadVo(cityId, cityName));

						userCityCache.put(userLoginId, userCity);

					}

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user City Cache "
					+ e.getMessage());
			e.printStackTrace();
		}

		log.info("userCityCache " + userCityCache.size());

		return size;
	}

	@Override
	public int userDeviceCache(String loginId)
	{
		log.info("User Device Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			if (loginId != null)
			{
				String tempUserDeviceCacheQuery = userDeviceCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");

				log.info("User Device Cache Called for user " + loginId
						+ " tempUserDeviceCacheQuery "
						+ tempUserDeviceCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(tempUserDeviceCacheQuery, new UserDeviceCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userDeviceCacheQuery, new UserDeviceCacheMapper());
			}
			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User Device Cache are "
						+ userVoModifieds.size());

				if (deviceCache.isEmpty())
					deviceCache();

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					String userLoginId = userVoModified.getLoginId();

					deviceNameIdCache.put(userVoModified
							.getDeviceName(), userVoModified.getDeviceId());

					if (userDeviceCache.containsKey(userLoginId))
					{
						Set<TypeAheadVo> userDevice = userDeviceCache
								.get(userLoginId);

						Long devcieId = userVoModified.getDeviceId() != null
								? Long.valueOf(userVoModified.getDeviceId())
								: 0;

						userDevice.add(new TypeAheadVo(devcieId, deviceCache
								.get(devcieId)));

						userDeviceCache.put(userLoginId, userDevice);

					} else
					{

						Set<TypeAheadVo> userDevice = new LinkedHashSet<>();

						Long devcieId = userVoModified.getDeviceId() != null
								? Long.valueOf(userVoModified.getDeviceId())
								: 0;

						userDevice.add(new TypeAheadVo(devcieId, deviceCache
								.get(devcieId)));

						userDeviceCache.put(userLoginId, userDevice);

					}

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user Device Cache "
					+ e.getMessage());
			e.printStackTrace();
		}

		log.info("userDeviceCache " + userDeviceCache.size());

		return size;
	}

	@Override
	public int userTabletCache(String loginId)
	{
		log.info("User Tablet Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			if (loginId != null)
			{
				String tempUserTabletCacheQuery = userTabletCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");

				log.info("User Tablet Cache Called for user " + loginId
						+ " tempUserTabletCacheQuery "
						+ tempUserTabletCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(tempUserTabletCacheQuery, new UserTabletCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userTabletCacheQuery, new UserTabletCacheMapper());
			}
			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User Tablet Cache are "
						+ userVoModifieds.size());

				if (tabletCache.isEmpty())
					tabletCache();

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					String userLoginId = userVoModified.getLoginId();

					if (userTabletCache.containsKey(userLoginId))
					{
						Set<TypeAheadVo> userTablet = userTabletCache
								.get(userLoginId);

						Long tabletId = userVoModified.getTabletId() != null
								? Long.valueOf(userVoModified.getTabletId())
								: 0;

						userTablet.add(new TypeAheadVo(tabletId, tabletCache
								.get(tabletId)));

						userTabletCache.put(userLoginId, userTablet);

					} else
					{

						Set<TypeAheadVo> userTablet = new LinkedHashSet<>();

						Long tabletId = userVoModified.getTabletId() != null
								? Long.valueOf(userVoModified.getTabletId())
								: 0;

						userTablet.add(new TypeAheadVo(tabletId, tabletCache
								.get(tabletId)));

						userTabletCache.put(userLoginId, userTablet);

					}

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user Tablet cache "
					+ e.getMessage());
			e.printStackTrace();
		}

		log.info("userTabletCache" + userTabletCache.size());

		return size;
	}

	@Override
	public int userSkillCache(String loginId)
	{
		log.info("User Skill Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			if (loginId != null)
			{
				String tempUserSkillCacheQuery = userSkillCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");

				log.info("User Skill Cache Called for user " + loginId
						+ " tempUserSkillCacheQuery "
						+ tempUserSkillCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(tempUserSkillCacheQuery, new UserSkillCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userSkillCacheQuery, new UserSkillCacheMapper());
			}
			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User Skill Cache are "
						+ userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user Skill Cache "
					+ e.getMessage());
			e.printStackTrace();
		}
		log.debug(" exit from vUser Skill Cache Called " + loginId);

		return size;

	}

	@Override
	public int userEmployeerCache(String loginId)
	{
		log.info("User Employeer Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			if (loginId != null)
			{
				String tempUserEmpoyeeCacheQuery = userEmployeerCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");

				log.info("User Device Cache Called for user " + loginId
						+ " tempUserEmpoyeeCacheQuery "
						+ tempUserEmpoyeeCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(tempUserEmpoyeeCacheQuery, new UserEmployeerCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userEmployeerCacheQuery, new UserEmployeerCacheMapper());
			}
			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User Employeer Cache are "
						+ userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user Employeer Cache "
					+ e.getMessage());
			e.printStackTrace();
		}
		log.debug(" exit from User Employeer Cache Called " +loginId );
		return size;
	}

	@Override
	public int tabletCache()
	{
		log.info("User Tablet Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			userVoModifieds = jdbcTemplate
					.query(tabletCacheQuery, new TabletCacheMapper());

			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total Tablet Cache are " + userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					Long tabletId = userVoModified.getTabletId() != null
							? Long.valueOf(userVoModified.getTabletId()) : 0;

					tabletCache.put(tabletId, userVoModified.getTabMac());

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating Tablet Cache " + e.getMessage());
			e.printStackTrace();
		}

		log.info("tabletCache" + tabletCache.size());

		return size;
	}

	@Override
	public int deviceCache()
	{
		log.info("User Device Cache Called ");

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		try
		{
			userVoModifieds = jdbcTemplate
					.query(deviceCacheQuery, new DeviceCacheMapper());

			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total Device Cache are " + userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					Long deviceId = userVoModified.getDeviceId() != null
							? Long.valueOf(userVoModified.getDeviceId()) : 0;

					deviceCache.put(deviceId, userVoModified.getDeviceName());

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating Device Cache " + e.getMessage());
			e.printStackTrace();
		}

		log.info("deviceCache" + deviceCache.size());

		return size;
	}

	@Override
	public int userUserGroupCache(String loginId)
	{

		List<UserVoModified> userVoModifieds = new ArrayList<UserVoModified>();
		int size = 0;

		log.debug(" inside  userUserGroupCache " + loginId);
		try
		{
			if (loginId != null)
			{
				String tempUserGroupCacheQuery = userUserGroupCacheQuery
						.replace("order", " where u.loginId= '" + loginId
								+ "' order ");

				log.debug("User and User group Cache Called for user " + loginId
						+ " tempUserGroupCacheQuery "
						+ tempUserGroupCacheQuery);

				userVoModifieds = jdbcTemplate
						.query(tempUserGroupCacheQuery, new userUserGroupCacheMapper());

			} else
			{
				userVoModifieds = jdbcTemplate
						.query(userUserGroupCacheQuery, new userUserGroupCacheMapper());
			}
			if (userVoModifieds != null && !userVoModifieds.isEmpty())
			{
				size = userVoModifieds.size();

				log.info("Total User, User Group Cache are "
						+ userVoModifieds.size());

				for (Iterator<UserVoModified> iterator = userVoModifieds
						.iterator(); iterator.hasNext();)
				{
					UserVoModified userVoModified = (UserVoModified) iterator
							.next();

					String userLoginId = userVoModified.getLoginId();

					if (userUserGroupCache.containsKey(userLoginId))
					{
						Set<TypeAheadVo> userGroups = userUserGroupCache
								.get(userLoginId);

						Long groupId = userVoModified.getUserGroupId() != null
								? Long.valueOf(userVoModified.getUserGroupId())
								: 0;

						String groupName = userVoModified.getUserGroup();

						userGroups.add(new TypeAheadVo(groupId, groupName));

						userUserGroupCache.put(userLoginId, userGroups);

					} else
					{

						Set<TypeAheadVo> userGroups = new LinkedHashSet<>();

						Long groupId = userVoModified.getUserGroupId() != null
								? Long.valueOf(userVoModified.getUserGroupId())
								: 0;

						String groupName = userVoModified.getUserGroup();

						userGroups.add(new TypeAheadVo(groupId, groupName));

						userUserGroupCache.put(userLoginId, userGroups);

					}

				}

			}

		} catch (Exception e)
		{
			log.error("Error while populating user City Cache "
					+ e.getMessage());
			e.printStackTrace();
		}
		log.info("userUserGroupCache " + userUserGroupCache.size());
		return size;
	}

	private class UserAreaCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			
			// " a.id as areaId,a.areaName as areaName,a.areaCode as areaCode
			// from Area a "
			UserVoModified userVoModified = new UserVoModified();

			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			userVoModified.setAreaId(resultSet.getString("areaId"));
			userVoModified.setAreaName(resultSet.getString("areaName"));
			userVoModified.setAreaCode(resultSet.getString("areaCode"));

			return userVoModified;
		}
	}

	private class UserBranchCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			// + " b.id as branchId,b.branchName as branchName,b.branchCode as
			// branchCode from Branch b "

			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			userVoModified.setBranchId(resultSet.getString("branchId"));
			userVoModified.setBranchName(resultSet.getString("branchName"));
			userVoModified.setBranchCode(resultSet.getString("branchCode"));

			return userVoModified;
		}
	}

	private class UserCityCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			// + "c.id as cityId,c.cityName as cityName,c.cityCode as cityCode
			// from City a "
			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			userVoModified.setCityId(resultSet.getString("cityId"));
			userVoModified.setCityName(resultSet.getString("cityName"));
			userVoModified.setCityCode(resultSet.getString("cityCode"));

			return userVoModified;
		}
	}

	private class UserDeviceCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			// + " d.id as deviceId, d.deviceName as deviceName from Device d
			// join UserDeviceMapping udm "

			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			userVoModified.setDeviceId(resultSet.getString("deviceId"));
			userVoModified.setDeviceName(resultSet.getString("deviceName"));

			return userVoModified;
		}
	}

	private class UserTabletCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			// + "tab.id as tabletId,tab.macAddress as tabletMac from Tablet tab
			// Join User u on u.id=tab.assignedTo";

			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			userVoModified.setTabletId(resultSet.getString("tabletId"));
			userVoModified.setTabMac(resultSet.getString("tabletMac"));

			return userVoModified;
		}
	}

	private class userUserGroupCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			// ug.id as userGroupId,ug.userGroupcode as userGroupcode,
			// ug.userGroupName as userGroupName

			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			userVoModified.setUserGroupId(resultSet.getString("userGroupId"));
			userVoModified.setUserGroup(resultSet.getString("userGroupName"));

			return userVoModified;
		}
	}

	private class UserSkillCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			// + "s.id as skillId,s.skillName as skillName from Skill s Join
			// UserSkillMapping usm on usm.idSkill=s.id "
			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			return userVoModified;
		}
	}

	private class UserEmployeerCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{

			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setUserId(resultSet.getString("userId"));
			userVoModified.setLoginId(resultSet.getString("loginId"));
			userVoModified.setEmpId(resultSet.getString("empId"));

			return userVoModified;
		}
	}

	private class TabletCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setTabletId(resultSet.getString("tabId"));
			userVoModified.setTabMac(resultSet.getString("tabMac"));
			return userVoModified;
		}
	}

	private class DeviceCacheMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			UserVoModified userVoModified = new UserVoModified();
			userVoModified.setDeviceId(resultSet.getString("deviceId"));
			userVoModified.setDeviceName(resultSet.getString("deviceName"));
			return userVoModified;
		}
	}

	@Override
	public Set<TypeAheadVo> getUserAreaByLoginId(String loginId)
	{
		log.debug(" inside  getUserAreaByLoginId " +loginId);
		return userAreaCache.get(loginId);
	}

	@Override
	public Set<TypeAheadVo> getUserBranchByLoginId(String loginId)
	{
		log.debug(" inside  getUserBranchByLoginId " +loginId);
		return userBranchCache.get(loginId);
	}

	@Override
	public Set<TypeAheadVo> getUserCityByLoginId(String loginId)
	{
		log.debug(" inside  getUserCityByLoginId " +loginId);
		return userCityCache.get(loginId);
	}

	@Override
	public Set<TypeAheadVo> getUserDeviceByLoginId(String loginId)
	{
		log.debug(" inside  getUserDeviceByLoginId " +loginId);
		return userDeviceCache.get(loginId);
	}

	@Override
	public Set<TypeAheadVo> getUserTabletByLoginId(String loginId)
	{
		log.debug(" inside  getUserTabletByLoginId " +loginId);
		return userTabletCache.get(loginId);
	}

	@Override
	public Set<TypeAheadVo> getUserSkillByLoginId(String loginId)
	{
		log.debug(" inside  getUserSkillByLoginId " +loginId);
		return userSkillCache.get(loginId);
	}

	@Override
	public Set<TypeAheadVo> getUserEmployeerByLoginId(String loginId)
	{
		return null;
	}

	@Override
	public Set<TypeAheadVo> getUserUserGroupByLoginId(String loginId)
	{
		log.debug(" inside  getUserUserGroupByLoginId " +loginId);
		return userUserGroupCache.get(loginId);
	}

	@Override
	public boolean getFxByName(String loginId)
	{
		log.debug(" inside  getFxByName " +loginId);
		return deviceNameIdCache.containsKey(loginId);
	}

	@Override
	public void setIncrementalCache(UserVo userVo)
	{
		if(userVo == null)
			return;
		
		log.debug(" inside  setIncrementalCache "+  userVo.getEmpId());
		try
		{
			String userLoginId = userVo.getLoginId();
			if (userVo.getArea() != null)
				userAreaCache.put(userLoginId, userVo.getArea());

			if (userVo.getBranch() != null)
			{
				Long branchId = userVo.getBranch() != null
						? Long.valueOf(userVo.getBranch().getId()) : 0;

				String branchName = branchDAO.getBranchNameById(branchId);

				Set<TypeAheadVo> userBranches = new LinkedHashSet<>();

				userBranches.add(new TypeAheadVo(branchId, branchName));

				userBranchCache.put(userLoginId, userBranches);
			}
			if (userVo.getCity() != null)
			{
				Long cityId = userVo.getCity() != null
						? Long.valueOf(userVo.getCity().getId()) : 0;

				String cityName = cityDAO.getCityNameById(cityId);

				Set<TypeAheadVo> userCity = new LinkedHashSet<>();

				userCity.add(new TypeAheadVo(cityId, cityName));

				userCityCache.put(userLoginId, userCity);
			}
			if (userVo.getDevices() != null)
				userDeviceCache.put(userLoginId, userVo.getDevices());

			if (userVo.getSkills() != null)
				userSkillCache.put(userLoginId, userVo.getSkills());

			userUserGroupCache.put(userLoginId, userVo.getUserGroups());

			log.info("Incremental user cache done for user " + userVo);
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error in incremental user cache " + e.getMessage());
		}

	}

	@Override
	public void resetCacheBId(String loginId)
	{
		log.debug(" inside  resetCacheBId " + loginId );
		
		deviceCache();

		tabletCache();

		userAreaCache(loginId);

		userBranchCache(loginId);

		userCityCache(loginId);

		userDeviceCache(loginId);

		userTabletCache(loginId);

		userSkillCache(loginId);

		// userEmployeerCache();

		userUserGroupCache(loginId);

	}
}
