package com.cupola.fwmp.dao.user;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Workbook;

import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.persistance.entities.DefaultUserAreaMapping;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.service.user.ResetUserVo;
import com.cupola.fwmp.util.PagedResultSet;
import com.cupola.fwmp.vo.LoginPojo;
import com.cupola.fwmp.vo.LoginResponseVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;

public interface UserDao
{

	User addUser(UserVo user);

	User findByUserName(String username);

	LoginResponseVo loginForApp(LoginPojo loginPojo);

	String resetPassword(String userName, String oldPassword,
			String newPassword);

	List<UserVo> getAllUsers();

	User getUserByUserName(String name);

	List<String> getAllElementMacs();

	User updateUser(UserVo user);

	UserVo deleteUser(UserVo user);

	// APIResponse getUserListByPageIndex(int index,int itemPerPage);

	/*
	 * APIResponse addSkills(List<Skill> skills);
	 * 
	 * APIResponse addCity(List<City> cities);
	 * 
	 * APIResponse addBranch(String cityName, List<Branch> branches);
	 * 
	 * APIResponse addArea(String branchName, List<Area> areas);
	 */

	// APIResponse addElement(List<Element> elements);

	Map<Long, String> userSearchSuggestion(String userQuery);

	Map<Long, String> elementSearchSuggestion(String elementQuery);

	// Map<Long, String> getAreasByBranchName(String branchName);
	//
	// Map<Long, String> citySearchSuggestion(String cityQuery);
	//
	// Map<Long, String> getBranchesByCityName(String cityName);

	Map<Long, String> getVendorsByBranchName(TypeAheadVo aheadVo);

	// List<String> getVendorTypesByVendorName(String vendorName);

	Map<Long, String> getSkillsByVendorName(TypeAheadVo aheadVo);

	// Map<Long, String> getSubAreasByAreaName(String areaName);

	String getUserNameByUserId(Long userId);

	UserVo getUserById(Long id);

	PagedResultSet getUsers(UserFilter filter);

	List<UserVo> getUsers(long reportToUserId, long groupId, long cityId,
			long branchId, long areaId, long subAreaId, int startOffset,
			int pageSize);

	long getReportToUser(Long userId);

	UserVo getUserByEmpId(long empId);

	long getEmpIdByUserId(long userId);

	public String getKeyForQueueByUserId(Long userId);

	public Long getUserIdByDeviceName(String macAddress);

	public Long getUserIdByDevcieName(String devcieName);

	List<UserVo> getUsersAssosiatedToDevice(Long userId, String sourceRole,
			String destinationRole);

	List<Long> getUsersForAreasWithRole(List<Long> areaId, String sourceRole);

	String reAssignedDeviceToUser(long userId, String macAddress);

	TypeAheadVo getDeviceDetailsByMacAddr(String macAddr);

	UserVo getUsersByDeviceName(String fxName, String roleSales);

	String createNewPassword(Long userId, String newPassword);

	UserVo getUsersByDeviceName(String deviceName, List<String> roles);

	List<UserVo> getAssociatedUsersByUserId(Long userId, List<String> roles);

	List<UserVo> getReportee(Long userId);

	Map<Long, String> getAllUserNames(Long userType);

	Map<Long, String> getReportingUsers(long userId);

	public List<String> getAllRolesByUserId(long userId);

//	UserVo getDefaultSalesUserForArea(Long areaId, String role);

	List<TypeAheadVo> getAllFrUserNames();

	// public void resetCachedUsers();

	Map<Long, String> getAssociatedUsers(long userId, Long userGroupId);

	Map<Long, String> getUserForAreas(List<Long> areaIds, List<Long> branchIds,
			List<String> userRoles, Long userGroupId);

	public List<UserVo> getCurrentCityTeams(String groupCode, long cityId);
	
	Map<Long, String> getUserByGroupForAreas(List<Long> areaIds,
			Long userGroupId);

	Map<Long, String> getUserForAreasWithRoles(List<Long> areaIds,
			List<String> roles, boolean matchAllRoles);

	public UserVo getUserByLoginId(String loginId);

	void addDefaultSalesUserForAreas(List<DefaultUserAreaMapping> mappings);

	List<UserVo> getUserDetailForAreasWithRoles(List<Long> areaIds,
			List<String> userRoles, boolean matchAllRoles);

	Map<Long, String> getNestedReportingUsers(long userId, Long branchId,
			Long areaId, Long userGroupId);

	UserVo findByUserNameNew(String username);

	public void mapAllAreaForUser(List<TypeAheadVo> areaIds, Long userId);

	public void mapDefaultAreaForUser(List<TypeAheadVo> areaIds, Long userId);

	public Set<TypeAheadVo> getAllAreasForUser(Long userId);

	public Map<Long, String> getDefaultAreasForUser(Long userId);

	void resetCachedUsersByUserId(Long userId);

	int restUserById(ResetUserVo resetUserVo);

	void modifiedResetUser();

	/**
	 * @author aditya 3:22:19 PM Apr 6, 2017 void
	 * @param userVo
	 * @return
	 */
	int addToCache(UserVo userVo);

	/**
	 * @author aditya 11:15:13 AM Apr 14, 2017 UserVo
	 * @param areaId
	 * @param branchId
	 * @param cityId
	 * @param role
	 * @return
	 */
	UserVo getDefaultSalesUserForArea(Long areaId, Long branchId, Long cityId,
			String role);
	

	public TypeAheadVo getUserGroupName(Long userId);
	Map<Long, String> getNestedReportingUsersWithRoles(long userId,
			List<String> roles, boolean matchaesAnyRole);
	List<BigInteger> getReportToByBranchId(Long branchId, Long areaId);

	public Set<TypeAheadVo> getFxDeviceByTl(Long userId);

	/**
	 * @author ashraf
	 * String
	 * @param userId
	 * @return
	 */
	public TypeAheadVo getCityNameByUserId(Long userId);

	UserVo getDefaultSalesUserForSubArea(Long subAreaId, Long areaId, Long branchId, Long cityId, String roleEx);
	
	/*
	 * 
	 * @author manjuprasad.nidlady
	 */
	
	public List<String> getAreaMasterMappingDetails(Workbook workbook, UserVo login_user);
	public String mapDefaultAreaForUserBulk(List<TypeAheadVo> areaIds, Long userId);

}