package com.cupola.fwmp.dao.user;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.DBActiveStatus;
import com.cupola.fwmp.FWMPConstant.UserGroupCodes;
import com.cupola.fwmp.auth.AppUser;
import com.cupola.fwmp.dao.device.DeviceDAO;
import com.cupola.fwmp.dao.userGroup.UserGroupDAO;
import com.cupola.fwmp.filters.UserFilter;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.DefaultUserAreaMapping;
import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.persistance.entities.Role;
import com.cupola.fwmp.persistance.entities.Skill;
import com.cupola.fwmp.persistance.entities.SubArea;
import com.cupola.fwmp.persistance.entities.Tablet;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.persistance.entities.UserGroup;
import com.cupola.fwmp.persistance.entities.Vendor;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.user.ResetUserVo;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.PagedResultSet;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.util.StatusMessages;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.LoginPojo;
import com.cupola.fwmp.vo.LoginResponseVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.user.UserVoModified;

@Transactional
@Repository
public class UserDaoImpl implements UserDao
{

	private static final Logger log = LogManager
			.getLogger(UserDaoImpl.class.getName());

	private static Integer DEFAULT_PERMISSION = 1;

	// private static String SELECT_USER_QUERY = "SELECT u.* FROM User u ";

	String SELECT_USER_QUERY = "select u.id as userId,u.empId as empId, u.loginId as loginId,u.password as password,  "
			+ "u.firstName as firstName, u.lastName as lastName,u.emailId as emailId, u.mobileNo as mobileNo,  "
			+ "u1.id as reportToId, u1.loginId reportToLoginId,u.status as userStatus "
			+ "from User u join User u1 on u.reportTo=u1.id " ;

	private static String JOIN_GROUP = " inner join UserGroupUserMapping ugm on u.id = ugm.idUser";

	private static String JOIN_SUB_AREA = " inner join UserSubAreaMapping usm on u.id = usm.idUser";

	private static String JOIN_AREA = " inner join UserAreaMapping uam on u.id = uam.userId";

	private static String JOIN_BRANCH = " inner join UserBranchMapping ubm on u.id = ubm.idUser";
	private static String JOIN_CITY = " inner join UserCityMapping ucm on u.id = ucm.idUser ";
	private final static String INSERT_VALUES_USER_DEVICE_MAPPING = "insert into UserDeviceMapping values(?,?)";
	private static final String DELETE_PREVIOUS_MAPPING = "DELETE FROM UserAreaMapping where userId=?";
	private final static String GET_USER_BY_DEVICENAME = "SELECT u.* FROM User u inner join UserGroupUserMapping ugm on u.id = ugm.idUser"
			+ " inner join UserGroupRoleMapping ugrm on ugrm.idUserGroup=ugm.idUserGroup"
			+ " inner join Role r on ugrm.idRole = r.id"
			+ " inner join UserDeviceMapping udm on u.id = udm.userId"
			+ " inner join Device d on d.id=udm.deviceId "
			+ " where d.deviceName=:deviceName and u.status="+DBActiveStatus.ACTIVE +" and r.roleCode in(:roleList)";

	// private final static String GET_DEFAULT_USER_BY_AREA = "SELECT u.* FROM
	// User u inner join UserGroupUserMapping ugm on u.id = ugm.idUser"
	// + " inner join UserGroupRoleMapping ugrm on
	// ugrm.idUserGroup=ugm.idUserGroup"
	// + " inner join Role r on ugrm.idRole = r.id"
	// + " inner join UserAreaMapping uam on uam.userId = u.id "
	// + " inner join UserDeviceMapping udm on u.id = udm.userId"
	// + " inner join Device d on d.id=udm.deviceId "
	// + " where u.status=1 and r.roleCode in(:roleList) and uam.areaId =
	// :areaId and u.permissions = "
	// + DEFAULT_PERMISSION;
	// private final static String GET_DEFAULT_USER_BY_AREA = "SELECT u.* FROM
	// User u inner join DefaultUserAreaMapping ugm on u.id = ugm.userId"
	// + " where u.status=1 and ugm.areaId = :areaId ";
	private final static String GET_DEFAULT_USER_BY_AREA = "SELECT u.id FROM User u join DefaultUserAreaMapping "
			+ "ugm on u.id = ugm.userId Join Area a on a.id=ugm.areaId Join Branch b on b.id=a.branchId "
			+ "Join City c on c.id=b.cityId where u.status="+DBActiveStatus.ACTIVE +" and  a.id=:areaId and b.id=:branchId and c.id=:cityId";
	/*
	 * private final static String GET_ASSOCIATED_USERS_BY_USER_ID =
	 * "select distinct u.* from UserDeviceMapping udm,User u, " +
	 * "Role r  where r.roleCode in (:roleIds) and u.id=udm.userId and u.status = 1 and udm.deviceId in  "
	 * +
	 * "(select udm1.deviceId from UserDeviceMapping udm1 where udm1.userId=:userId) and udm.userId!=:userId"
	 * ;
	 */

	private final static String GET_ASSOCIATED_USERS_BY_USER_ID = "SELECT distinct u.* FROM User u "
			+ "inner join UserGroupUserMapping ugm on u.id = ugm.idUser "
			+ "inner join UserGroupRoleMapping ugrm on ugrm.idUserGroup=ugm.idUserGroup "
			+ "inner join Role r on ugrm.idRole = r.id inner join UserDeviceMapping udm on u.id = udm.userId "
			+ "inner join Device d on d.id=udm.deviceId "
			+ "where udm.deviceId in (select udm1.deviceId from UserDeviceMapping udm1 where udm1.userId=?) "
			+ "and udm.userId !=? and r.roleCode in( ? ) and u.status = "+DBActiveStatus.ACTIVE +" ";

	private final static String GET_REPORTEE = "select u.* from User u where u.reportTo=:userId";

	private static String USER_GROUP = " inner join UserGroupUserMapping ugm on ugm.idUser=u.id "
			+ "inner join UserGroup ug on ugm.idUserGroup=ug.id inner join UserGroupRoleMapping ugr on ug.id=ugr.idUserGroup inner join Role r "
			+ "on ugr.idRole=r.id";
	private static String GET_CITY_TEAM_USER = "SELECT usr.* FROM User usr "
			+ "INNER JOIN UserGroupUserMapping ugum ON usr.id = ugum.idUser "
			+ "INNER JOIN UserGroup ug ON ugum.idUserGroup = ug.id "
			+ "INNER JOIN UserCityMapping ucm ON usr.id = ucm.idUser "
			+ "INNER JOIN City c ON ucm.idCity = c.id "
			+ "WHERE ug.userGroupCode = :usrGourpCode AND usr.status = "+DBActiveStatus.ACTIVE +" AND c.id = :cityId ";

	private static final String USER_AREA_ALLOCATION = "INSERT INTO "

			+ "UserAreaMapping " + "(userId, areaId ) "

			+ " VALUES " + "(?, ?)";
	
	private static final String DELETE_PREVIOUS_DEFAULT_AREA_MAPPING = "DELETE FROM DefaultUserAreaMapping where userId=?";

	private static final String USER_DEFAULT_AREA_ALLOCATION = "INSERT INTO "

			+ "DefaultUserAreaMapping " + "(id,userId, areaId ) "

			+ " VALUES " + "(?,?, ?)";

	private static final String USER_DEFAULT_AREA_GET = "SELECT dam.areaId,a.areaName FROM  "
			+ " DefaultUserAreaMapping  dam Inner join Area a on "
			+ " a.id = dam.areaId where dam.userId =:userId";

	
	private final static String GET_DEFAULT_USER_BY_SUB_AREA = " SELECT u1.id FROM User u1 join DefaultUserSubAreaMapping  dugm on u1.id = dugm.userId join SubArea sa on sa.id = dugm.subAreaId "
			+ "Join Area a on a.id=sa.areaId Join Branch b on b.id=a.branchId Join City c on c.id=b.cityId "
			+ "where u1.status="+DBActiveStatus.ACTIVE +" and  sa.id =:subAreaId and  a.id=:areaId and b.id=:branchId and c.id=:cityId";

	private static String GET_USER_GROUP_CODE = "select ug.* "
			+ "FROM User usr INNER JOIN UserGroupUserMapping ugm ON usr.id = ugm.idUser "
			+ "INNER JOIN UserGroup ug ON ugm.idUserGroup = ug.id "
			+ "WHERE usr.id = :userId";

	private static String GET_AM_BY_BRANCHID = " SELECT usr.id FROM"
			+ "_FROM_PLACE_"
			+ "  WHERE usr.status = "+DBActiveStatus.ACTIVE +" and ug.userGroupcode = 'FR_AM' and ubm.idBranch in (:branches)";
	
	private static String IFBRANCH = " User usr INNER JOIN  UserGroupUserMapping ugm on ugm.idUser = usr.id  INNER JOIN UserGroup ug on ug.id = ugm.idUserGroup INNER JOIN UserBranchMapping ubm on ubm.idUser = usr.id  ";
	private static String IFAREA = " User usr INNER JOIN  UserGroupUserMapping ugm on ugm.idUser = usr.id  INNER JOIN UserGroup ug on ug.id = ugm.idUserGroup INNER JOIN UserBranchMapping ubm on ubm.idUser = usr.id INNER JOIN UserAreaMapping uam on uam.userId = usr.id ";

	private static String GET_DEVICE_FOR_TL = " select d.id,d.deviceName from User usr inner join "
			+ "UserDeviceMapping udm on usr.id = udm.userId "
			+ "inner join Device d on d.id = udm.deviceId "
			+ "where usr.reportTo = :reportTo ";
	private static String GET_VENDOR_SKILLS = "select vsm.skillId , s.skillName from VendorSkillMapping vsm inner join Skill s on s.id = vsm.skillId where vsm.vendorId=:vendorId";
	
	private static String GET_CITY_NAME_BY_USER_ID = "select city.id,city.cityName from User usr "
			+" inner join UserCityMapping ucm on usr.id = ucm.idUser "
			+" inner join City city on city.id = ucm.idCity where usr.id = :userId" ;
	
	private static final String GET_AREA_DETAILS = " SELECT a.areaName from Area a  "
			+ "JOIN Branch b ON b.id = a.branchId "
			+ "JOIN City c ON c.id = b.cityId  "
			+ " WHERE b.id = :branchid and c.id = :cityid ";
	
	private static final String VERIFY_AREA_WITH_BRANCH_AND_CITY = " SELECT a.id FROM Area a "
			+ " join Branch b on b.id=a.branchId "
			+ "join UserBranchMapping ub on ub.idBranch=b.id "
			+ "join User u on u.id=ub.idUser "
			+ "WHERE a.id in (:areaId) and u.id = :userId and u.status = "+DBActiveStatus.ACTIVE +"";
	
	/*private static final String UPSERT_TO_DEFAULT_AREA_MAPPING = " INSERT INTO DefaultUserAreaMapping (id,userId,areaId) "
			+ "VALUES (:id,:userid,:areaid ) "
			+ "ON DUPLICATE KEY UPDATE userId = :userid ";*/
	
	private static final String DELETE_USER_AREA_MAPPING = "delete from DefaultUserAreaMapping where userId in "
			+ " (select x.userId from(select udm.userId from DefaultUserAreaMapping udm join User u on udm.userId = u.id join UserGroupUserMapping ugum on ugum.idUser = u.id where ugum.idUserGroup = (select idUserGroup from UserGroupUserMapping where idUser = :userid ) and udm.areaId = :areaid ) as x) "
			+ " and areaId = :areaid ";
	private static final String INSERT_USER_AREA_MAPPING = " INSERT INTO DefaultUserAreaMapping (id,userId,areaId) "
			+ "VALUES (:id,:userid,:areaid ) ";

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	UserCache userCache;
	
	@Autowired
	DefinitionCoreService definitionCoreService;
	
	
	@Autowired
	UserHelper userHelper;

	private int MIN_RESULT_PER_PAGE = 10;

	Map<Integer, List<UserVo>> userRecords = new HashMap<Integer, List<UserVo>>();

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	@Autowired
	UserGroupDAO userGroupDAO;
	@Autowired
	DeviceDAO deviceDao;
	public List<String> elementMacs = new ArrayList<>();

	public Map<Long, String> userNames = new TreeMap<>();
	public static Map<Long, UserVo> cachedUsersById = new ConcurrentHashMap<Long, UserVo>();

	public static Map<String, UserVo> cachedUsersByLoginId = new ConcurrentHashMap<String, UserVo>();

	public static Map<Long, Long> userAndReportToMapping = new ConcurrentHashMap<Long, Long>();
	public static Map<Long, Long> userIdAndEmpIdMapping = new ConcurrentHashMap<Long, Long>();
	public static Map<Long, Long> empIdAndUserMapping = new ConcurrentHashMap<Long, Long>();
	public static Map<Long, List<Long>> userAndReportingMapping = new ConcurrentHashMap<Long, List<Long>>();
	public static Map<Long, List<Long>> userAndQueueKeyMapping = new ConcurrentHashMap<Long, List<Long>>();
	public static Map<String, Long> deviceNameUserIdCache = new ConcurrentHashMap<String, Long>();

	public static Map<Long, List<Long>> userAndNestedReportingMapping = new ConcurrentHashMap<Long, List<Long>>();
	public static Map<String, Set<Long>> userAndDeviceMapping = new ConcurrentHashMap<String, Set<Long>>();

	
	private String resetUserUrl;
	private String servers;

	private void init()
	{

		log.info("User init method called..");
		// resetCachedUsers();
		userCache.init();
		modifiedResetUser();
		log.info("User init method executed.");
	}

	@Override
	public UserVo findByUserNameNew(String username)
	{
		log.debug("findByUserNameNew   "  + username);
		
		UserVo userVo = null;
		try
		{
			log.info("User trying to login with username : " + username);

//			Query query = hibernateTemplate.getSessionFactory()
//					.getCurrentSession()
//					.createSQLQuery("select id,password from User where loginId='"
//							+ username + "'");
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select id,password from User where loginId='"
							+ username + "' and status = " + DBActiveStatus.ACTIVE);


			List<Object[]> id = query.list();
			if (id != null && !id.isEmpty())
			{
				log.info("User   found with login id:"
						+ objectToLong(id.get(0)[0]));

				userVo = getUserById(objectToLong(id.get(0)[0]));
				if (userVo == null)
				{
					StringBuilder queryStr = new StringBuilder(SELECT_USER_QUERY);
					StringBuilder WHERE_CLAUSE = new StringBuilder(" where ");
					WHERE_CLAUSE.append("u.loginId='" + username + "'");
					queryStr.append(WHERE_CLAUSE);
					log.debug("$$$$$$$$$$$$$$ queryStr " + queryStr.toString());

					List<UserVoModified> users = jdbcTemplate.query(queryStr
							.toString(), new ModifiedUserMapper());

					// List<UserVo> userVos = xformToVoList(users);
					List<UserVo> userVos = xformToVoListModified(users);
					log.debug("$$$$$$$$$$$$$$ userVos " + userVos);

					if (userVos != null)
					{
						userVo = (UserVo) userVos.get(0);

						Set<TypeAheadVo> groupAheadVos = userCache
								.getUserUserGroupByLoginId(username);

						Set<TypeAheadVo> cities = userCache
								.getUserCityByLoginId(username);

						Set<TypeAheadVo> branches = userCache
								.getUserBranchByLoginId(username);

						Set<TypeAheadVo> areas = userCache
								.getUserAreaByLoginId(username);

						if (groupAheadVos != null && !groupAheadVos.isEmpty())
							userVo.setUserGroups(groupAheadVos);
						else
						{
							userCache.resetCacheBId(userVo.getLoginId());
							groupAheadVos = userCache
									.getUserUserGroupByLoginId(username);

							cities = userCache.getUserCityByLoginId(username);

							branches = userCache
									.getUserBranchByLoginId(username);

							areas = userCache.getUserAreaByLoginId(username);

						}

						userVo.setUserGroups(groupAheadVos);

						if (cities != null && !cities.isEmpty())
							userVo.setCity(cities.iterator().next());

						if (branches != null && !branches.isEmpty())
							userVo.setCity(branches.iterator().next());

						userVo.setArea(areas);

						log.debug("groupAheadVos %%%%%%%%%%%%%%%%%%%%%%5 "
								+ groupAheadVos);

						Set<TypeAheadVo> roleAheadVos = new HashSet<>();
						if (groupAheadVos != null && !groupAheadVos.isEmpty())
						{
							for (Iterator<TypeAheadVo> iterator2 = groupAheadVos
									.iterator(); iterator2.hasNext();)
							{
								TypeAheadVo typeAheadVo = (TypeAheadVo) iterator2
										.next();

								roleAheadVos.addAll(userGroupDAO
										.getuseGroupAndRolesTAByGroupId(typeAheadVo
												.getId()));

							}

							userVo.setUserRoles(roleAheadVos);
						}

						cachedUsersById.put(userVo.getId(), userVo);
					}
					
				}
				userVo.setPassword((String) id.get(0)[1]);
				log.debug("Returning user vo " + userVo);
				
				return userVo;
			} else

			{
				log.info("User Not found with login id:" + username);
				return null;
			}
		} catch (Exception e)
		{
			log.error("User getting exception " + username);
			e.printStackTrace();
		}
		log.debug(" exit from findByUserNameNew   "  + username);
		return null;
	}

	/* For Spring security login */
	public User findByUserName(String username)
	{
		if(username == null || username.isEmpty())
			return null;

		log.info("User trying to login with username : " + username);
		List<User> users = (List<User>) hibernateTemplate
				.find("from User where loginId = ?", username);

		/*
		 * if (users.size() > 0) { return users.get(0); } else { return null; }
		 */

		if (users != null && users.size() > 0)
		{

			log.info("User '" + users.get(0).getFirstName()
					+ "' found with login id:" + username);

			return users.get(0);
		} else

		{
			log.info("User Not found with login id:" + username);
			return null;
		}

	}

	public String resetPassword(String userName, String oldPassword,
			String newPassword)
	{
		log.debug(" inside resetPassword   "+ userName);
		try
		{
			if (oldPassword.equalsIgnoreCase(newPassword))
			{

				return StatusMessages.DUPLICATE_RECORD;
			}

			List<User> users = (List<User>) hibernateTemplate
					.find("from User where loginId = ?", userName);

			if (users != null)
			{

				User olduser = users.get(0);

				log.info("old user::" + olduser);

				BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

				String encryptedPassword = encoder.encode(newPassword);

				if (encoder.matches(oldPassword, olduser.getPassword()))
				{
					log.debug("password matches....");
					olduser.setPassword(encryptedPassword);

					hibernateTemplate.update(olduser);

					// resetCachedUsersByUserId(olduser.getId());
					// ResetUserVo resetUserVo = new ResetUserVo();
					// resetUserVo.setUserId(olduser.getId());
					// resetUserVo.setNewPassword(olduser.getPassword());
					// restUserById(resetUserVo);

					return StatusMessages.UPDATED_SUCCESSFULLY;
				} else
				{

					return StatusMessages.BAD_CREDENTIALS;
				}
			}

			else
			{

				log.info("no user with username:" + userName);

				return StatusMessages.NO_RECORD_FOUND;
			}

		} catch ( Exception e)
		{
			log.error("Error While executing Query :" + e.getMessage());

			e.printStackTrace();

			return StatusMessages.MONGO_CONNECTION_FAILED;

		}
	}

	@Override
	public String createNewPassword(Long userId, String newPassword)
	{
		log.debug(" inside createNewPassword   " +userId);

		int result = 0;

		if (userId != null && newPassword != null)
		{

			try
			{
				BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

				String encryptedPassword = encoder.encode(newPassword);

				String query = "update User set password='" + encryptedPassword
						+ "' where id = '" + userId + "'";

				log.info("Update query for user " + userId + " and "
						+ newPassword + " is " + query);
				result = jdbcTemplate.update(query);

			} catch (Exception e)
			{
				log.error("Error while updating password for user " + userId
						+ " . " + e.getMessage());
			}

		}

		if (result == 1)
			return StatusMessages.UPDATED_SUCCESSFULLY;
		else
			return StatusMessages.UPDATE_FAILED;

	}

	@Override
	public User addUser(UserVo userVo)
	{
		User newUser = new User();
		if(userVo == null)
			return newUser;

		log.info("add user......." + userVo.getEmpId());

		try
		{

			String loginId = userVo.getLoginId();

			if (cachedUsersByLoginId != null
					&& cachedUsersByLoginId.containsKey(loginId.toUpperCase()))
			{

				log.info("User Already Exists in DB");

				newUser.setStatus(StatusCodes.DUPLICATE_RECORD);

				return newUser;

			}

			else
			{
				log.info("User does not Exists ,Hence Adding the Details with userId:"
						+ loginId);

				BeanUtils.copyProperties(userVo, newUser);

				newUser.setId(StrictMicroSecondTimeBasedGuid.newGuid());

				if (userVo.getMobileNo() > 0)
					newUser.setAlternativeMobileNo(userVo.getMobileNo());
				// if (user.getEnabled().equalsIgnoreCase("enabled"))
				newUser.setStatus(1);

				// else
				// {
				//
				// newUser.setStatus(0);
				// }
				if (userVo.getPassword() != null)
				{
					newUser.setPassword(new BCryptPasswordEncoder()
							.encode(userVo.getPassword()));
				}
				/******************/
				// uncomment later

				/*
				 * Tablet tablet = new Tablet();
				 * tablet.setTabletId((Long)tabletIdAndMacMap
				 * .getKey(user.getTabletName()));
				 * 
				 * log.info("tablet id::"+(Long)tabletIdAndMacMap.getKey(user.
				 * getTabletName())); newUser.setTablet(tablet);
				 */
				if (userVo.getEmployer() != null)
				{
					Vendor vendor = new Vendor();
					vendor.setId(userVo.getEmployer().getId());

					newUser.setVendor(vendor);
				}
				User user2 = new User();
				user2.setId(userVo.getReportTo().getId());

				newUser.setUser(user2);
				newUser.setAddedOn(new Date());
				newUser.setLastLoginTime(new Date());
				newUser.setModifiedBy(100l);
				newUser.setModifiedOn(new Date());
				newUser.setAddedBy(100l);

				userVo.setId(newUser.getId());

				if (userVo.getUserGroups() != null)
				{
					Set<UserGroup> userGroups = new HashSet<UserGroup>();
					Set<TypeAheadVo> groupVos = userVo.getUserGroups();
					for (TypeAheadVo typeAheadVo : groupVos)
					{

						UserGroup group = new UserGroup();
						group.setId(typeAheadVo.getId());

						userGroups.add(group);
					}
					newUser.setUserGroups(userGroups);
				}

				if (userVo.getSkills() != null)
				{

					Set<Skill> skills = new HashSet<Skill>();
					Set<TypeAheadVo> skillVos = userVo.getSkills();
					for (TypeAheadVo typeAheadVo : skillVos)
					{
						if (typeAheadVo == null)
							continue;

						Skill skill = new Skill();
						skill.setId(typeAheadVo.getId());

						skills.add(skill);
					}
					newUser.setSkills(skills);
				}

				if (userVo.getCity() != null)
				{

					Set<City> cities = new HashSet<City>();

					TypeAheadVo typeAheadVo = userVo.getCity();

					City city = new City();
					city.setId(typeAheadVo.getId());

					cities.add(city);
					newUser.setCities(cities);
				}

				if (userVo.getBranch() != null)
				{

					Set<Branch> branches = new HashSet<Branch>();

					TypeAheadVo typeAheadVo = userVo.getBranch();

					Branch branch = new Branch();
					branch.setId(typeAheadVo.getId());

					branches.add(branch);
					newUser.setBranches(branches);
				}

				if (userVo.getArea() != null)
				{

					Set<Area> areas = new HashSet<Area>();
					Set<TypeAheadVo> typeAheadVo = userVo.getArea();
					for (TypeAheadVo areaVO : typeAheadVo)
					{
						if (areaVO == null)
							continue;
						Area area = hibernateTemplate
								.load(Area.class, areaVO.getId());
						areas.add(area);
					}
					newUser.setAreas(areas);
				}

			}
			if (userVo.getSubAreas() != null)
			{

				Set<SubArea> subAreas = new HashSet<SubArea>();
				Set<TypeAheadVo> subAreaVos = userVo.getSubAreas();

				for (TypeAheadVo typeAheadVo : subAreaVos)
				{

					SubArea subArea = new SubArea();
					subArea.setId(typeAheadVo.getId());

					subAreas.add(subArea);
				}
				newUser.setSubAreas(subAreas);

			}

			if (userVo.getTablet() != null)
			{

				Set<Tablet> tablets = new HashSet<Tablet>();

				TypeAheadVo typeAheadVo = userVo.getTablet();

				Tablet tab = new Tablet();
				tab.setId(typeAheadVo.getId());

				tablets.add(tab);
				newUser.setTabletsForAssignedTo(tablets);
				newUser.setTabletsForCurrentUser(tablets);
			}

			if (userVo.getDevices() != null)
			{

				Set<Device> devices = new HashSet<Device>();
				Set<TypeAheadVo> deviceVos = userVo.getDevices();

				for (TypeAheadVo typeAheadVo : deviceVos)
				{
					if (typeAheadVo == null)
						continue;

					Device device = new Device();
					device.setId(typeAheadVo.getId());

					devices.add(device);
				}
				newUser.setDevices(devices);

			}
			Long serial = (Long) hibernateTemplate.save(newUser);

			if (serial != null && serial.longValue() > 0)
			{
				UserVo userNewVo = new UserVo();

				BeanUtils.copyProperties(newUser, userNewVo);
				userNewVo.setUserGroups(userVo.getUserGroups());
				userNewVo.setUserRoles(userVo.getUserRoles());
				userNewVo.setDevices(userVo.getDevices());

				Set<TypeAheadVo> groupAheadVos = userVo.getUserGroups();

				Set<TypeAheadVo> roleAheadVos = new HashSet<>();
				if (groupAheadVos != null && !groupAheadVos.isEmpty())
				{
					for (Iterator<TypeAheadVo> iterator2 = groupAheadVos
							.iterator(); iterator2.hasNext();)
					{
						TypeAheadVo typeAheadVo = (TypeAheadVo) iterator2
								.next();

						roleAheadVos.addAll(userGroupDAO
								.getuseGroupAndRolesTAByGroupId(typeAheadVo
										.getId()));

					}

					userNewVo.setUserRoles(roleAheadVos);
				}

				userNewVo.setCity(userVo.getCity());
				userNewVo.setBranch(userVo.getBranch());
				userNewVo.setArea(userVo.getArea());

				userCache.setIncrementalCache(userNewVo);
				userNames.put(newUser.getId(), loginId);

				addToLocalCache(userNewVo);

				log.debug("Added new user locally... ");
			}

			// userVo.setAddedOn(new Date());
			// userVo.setLastLoginTime(new Date());
			// userVo.setModifiedBy(100l);
			// userVo.setModifiedOn(new Date());
			// userVo.setAddedBy(100l);
			//
			// userNames.put(newUser.getId(), loginId);
			//
			// User u = hibernateTemplate.get(User.class, userVo.getId());
			//
			// addToLocalCache(xformToVoList(Collections.singletonList(u)).get(0));
			log.debug("user id" + newUser.getId());
			log.info("User Successfully Added into Database ");

			return newUser;

		} catch (Exception e)
		{

			log.error("Error in Inserting New User Into DataBase for user Name "

					+ e.getMessage() + " " + e.getStackTrace().toString());
			e.printStackTrace();

			return null;
		}
	}

	/*
	 * public APIResponse addUserElements(User user, List<String> elementMacs) {
	 * 
	 * log.info("adding user Element......."+elementMacs);
	 * 
	 * try {
	 * 
	 * for (String mac : elementMacs) {
	 * 
	 * log.info("element id::"+elementIdAndMacMap.getKey(mac));
	 * 
	 * Element element = new Element();
	 * element.setElementId((Long)elementIdAndMacMap.getKey(mac));
	 * 
	 * UserElementMapping mapping = new UserElementMapping(element,user, new
	 * Date(), user.getUserGuid(), new Date(), user.getUserGuid(), 1);
	 * 
	 * hibernateTemplate.save(mapping);
	 * 
	 * }
	 * 
	 * log.info("User element Successfully Added into Database ");
	 * 
	 * return ResponseUtil.createSaveSuccessResponse();
	 * 
	 * } catch (Exception e) {
	 * 
	 * log.info("Error in Inserting New elements of user Into DataBase"
	 * 
	 * + e.getMessage() + " " + e.getStackTrace().toString());
	 * 
	 * log.info("Hibernate Error"); e.printStackTrace();
	 * 
	 * return ResponseUtil.createDBExceptionResponse(); } }
	 */

	@Override
	public User updateUser(UserVo user)
	{

		if (user == null)
			return null;

		log.info("update user......." + user.getEmpId());

		try
		{

			long userGuid = user.getId();

			User oldUser = (User) hibernateTemplate.get(User.class, userGuid);

			if (oldUser != null)
			{

				BeanUtils.copyProperties(user, oldUser);
				oldUser.setModifiedOn(new Date());
				oldUser.setModifiedBy(userGuid);
				oldUser.setLastLoginTime(new Date());

				/*
				 * Tablet tablet = new Tablet();
				 * tablet.setTabletId((Long)tabletIdAndMacMap
				 * .getKey(user.getTabletName()));
				 * 
				 * oldUser.setTablet(tablet);
				 */
				
				// if (user.getEnabled().equalsIgnoreCase("enabled"))
				oldUser.setStatus(1);
				
				User user2 = new User();
				user2.setId(user.getReportTo().getId());

				oldUser.setUser(user2);

				if (user.getEmployer() != null)
				{
					Vendor vendor = new Vendor();
					vendor.setId(user.getEmployer().getId());
					oldUser.setVendor(vendor);
				}

				Set<UserGroup> userGroups = new HashSet<UserGroup>();

				if (user.getUserGroups() != null)
				{
					Set<TypeAheadVo> groupVos = user.getUserGroups();
					for (TypeAheadVo typeAheadVo : groupVos)
					{

						UserGroup group = new UserGroup();
						group.setId(typeAheadVo.getId());

						userGroups.add(group);
					}
				}

				oldUser.setUserGroups(userGroups);

				Set<Skill> skills = new HashSet<Skill>();

				if (user.getSkills() != null)
				{

					Set<TypeAheadVo> skillVos = user.getSkills();
					for (TypeAheadVo typeAheadVo : skillVos)
					{

						Skill skill = new Skill();
						skill.setId(typeAheadVo.getId());

						skills.add(skill);
					}
				}

				oldUser.setSkills(skills);

				Set<City> cities = new HashSet<City>();

				if (user.getCity() != null)
				{
					TypeAheadVo typeAheadVo = user.getCity();

					City city = new City();
					city.setId(typeAheadVo.getId());

					cities.add(city);
				}

				oldUser.setCities(cities);

				Set<Branch> branches = new HashSet<Branch>();

				if (user.getBranch() != null)
				{
					TypeAheadVo typeAheadVo = user.getBranch();
					log.info("user branch == " + typeAheadVo.getName());
					Branch branch = new Branch();
					branch.setId(typeAheadVo.getId());

					branches.add(branch);
				}

				oldUser.setBranches(branches);

				Set<Area> areas = new HashSet<Area>();

				if (user.getArea() != null)
				{

					Set<TypeAheadVo> typeAheadVo = user.getArea();
					for (TypeAheadVo areaVO : typeAheadVo)
					{
						Area area = new Area();
						area.setId(areaVO.getId());
						areas.add(area);
					}
				}
				oldUser.setAreas(areas);

				Set<SubArea> subAreas = new HashSet<SubArea>();

				if (user.getSubAreas() != null)
				{

					Set<TypeAheadVo> subAreaVos = user.getSubAreas();

					for (TypeAheadVo typeAheadVo : subAreaVos)
					{

						SubArea subArea = new SubArea();
						subArea.setId(typeAheadVo.getId());

						subAreas.add(subArea);
					}
				}
				oldUser.setSubAreas(subAreas);

				Set<Tablet> tablets = new HashSet<Tablet>();

				if (user.getTablet() != null)
				{
					TypeAheadVo typeAheadVo = user.getTablet();

					Tablet tab = new Tablet();
					tab.setId(typeAheadVo.getId());

					tablets.add(tab);

					// oldUser.setTabletsForCurrentUser(tablets);
				}
				oldUser.setTabletsForAssignedTo(tablets);

				Set<Device> devices = new HashSet<Device>();

				if (user.getDevices() != null)
				{
					Set<TypeAheadVo> deviceVos = user.getDevices();

					for (TypeAheadVo typeAheadVo : deviceVos)
					{

						Device device = new Device();
						device.setId(typeAheadVo.getId());

						devices.add(device);
					}
				}
				oldUser.setDevices(devices);

				log.debug("old user ========= " + oldUser);
				hibernateTemplate.update(oldUser);
				
				log.info("User Successfully Updated into Database ");
				
				if (0 == user.getStatus()){
					userNames.remove(oldUser.getId());
					if (cachedUsersByLoginId != null
							&& cachedUsersByLoginId.get(user.getLoginId()) != null)
						cachedUsersByLoginId.remove(user.getLoginId());

					if (cachedUsersById != null
							&& cachedUsersById.get(user.getId()) != null)
						cachedUsersById.remove(user.getId());
				}
				else{
					
			    log.info(" inside else ");
					
				userCache.setIncrementalCache(user);
				addToLocalCache(user);

				user.setLastLoginTime(new Date());
				cachedUsersByLoginId.put(user.getLoginId(), user);
				cachedUsersById.put(user.getId(), user);

				// Added here
				if (user.getReportTo() != null
						&& user.getReportTo().getId() != null)
				{
					userAndReportToMapping
							.put(user.getId(), user.getReportTo().getId());
					if (userAndReportingMapping
							.get(user.getReportTo().getId()) != null)
						userAndReportingMapping.get(user.getReportTo().getId())
								.add(user.getId());
					else
					{
						List<Long> list = new ArrayList<Long>();
						list.add(user.getId());
						userAndReportingMapping
								.put(user.getReportTo().getId(), list);
					}
				}
				
				}
				
				return oldUser;
			} else
			{

				log.info("User doesn't Exists in DB");

				User usr = new User();
				usr.setStatus(StatusCodes.NO_RECORD_FOUND);

				return usr;
			}
		} catch (Exception e)
		{

			log.error("Error in Inserting New User Into DataBase for user Name "

					+ e.getMessage() );

			log.error("Hibernate Error");
			e.printStackTrace();

			return null;
		}
	
	}

	@Override
	public UserVo deleteUser(UserVo user)
	{

		log.info("delete user......." + user);

		try
		{

		//	long userGuid = user.getId();

	//	User oldUser = (User) hibernateTemplate.get(User.class, userGuid);

			if (user != null)
			{

				user.setStatus(0);
			//	hibernateTemplate.update(oldUser);
				String query = "update User set status='" + 0
						+ "' where id = '" + user.getId() + "'";

			
		int		result = jdbcTemplate.update(query);

				// hibernateTemplate.delete(oldUser);

				userNames.remove(user.getId());
				if (cachedUsersByLoginId != null
						&& cachedUsersByLoginId.get(user.getLoginId().toUpperCase()) != null)
					cachedUsersByLoginId.remove(user.getLoginId().toUpperCase());

				if (cachedUsersById != null
						&& cachedUsersById.get(user.getId()) != null)
					cachedUsersById.remove(user.getId());

				log.info("User Successfully Deleted From Database ");

				return user;

			} else
			{

				log.info("User doesn't Exists in DB");

				UserVo usr = new UserVo();
				usr.setStatus(StatusCodes.DELETE_FAILED);
				return usr;
			}
		} catch (Exception e)
		{

			log.error("Error in Deleting User From DataBase for user Name "

					+ e.getMessage());

			log.error("Hibernate Error");
			e.printStackTrace();

			return null;
		}
	}

	@Override
	public List<UserVo> getUsers(long reportToUserId, long groupId, long cityId,
			long branchId, long areaId, long subAreaId, int startOffset,
			int pageSize)
	{
		log.debug(" inside getUsers   "+ cityId);
		Session session = null;
		List<UserVo> userVos = new ArrayList<>();
		try
		{
			session = hibernateTemplate.getSessionFactory().openSession();
			StringBuilder queryStr = new StringBuilder(SELECT_USER_QUERY);
			StringBuilder WHERE_CLAUSE = new StringBuilder(" where ");

			// SELECT u.* FROM User u inner join UserGroupUserMapping ugm on
			// u.id =
			// ugm.idUser
			//
			// inner join UserSubAreaMapping usm on u.id = usm.idUser
			//
			// inner join UserAreaMapping uam on u.id = uam.userId
			//
			// inner join UserBranchMapping ubm on u.id = ubm.idUser
			// inner join UserCityMapping ucm on u.id = ucm.idUser
			if (subAreaId > 0)
			{
				queryStr.append(JOIN_SUB_AREA);
				WHERE_CLAUSE.append(" usm.IdSubArea=" + subAreaId);
			}
			if (areaId > 0)
			{
				queryStr.append(JOIN_AREA);
				if (WHERE_CLAUSE.indexOf(".") > 0)
					WHERE_CLAUSE.append(" and");

				WHERE_CLAUSE.append(" uam.areaId=" + areaId);
			}
			if (branchId > 0)
			{
				queryStr.append(JOIN_BRANCH);
				if (WHERE_CLAUSE.indexOf(".") > 0)
					WHERE_CLAUSE.append(" and");
				WHERE_CLAUSE.append(" ubm.idBranch=" + branchId);
			}
			if (cityId > 0)
			{
				queryStr.append(JOIN_CITY);
				if (WHERE_CLAUSE.indexOf(".") > 0)
					WHERE_CLAUSE.append(" and");
				WHERE_CLAUSE.append(" ucm.idCity=" + cityId);
			}

			if (groupId > 0)
			{
				queryStr.append(JOIN_GROUP);
				if (WHERE_CLAUSE.indexOf(".") > 0)
					WHERE_CLAUSE.append(" and");
				WHERE_CLAUSE.append(" ugm.idUserGroup=" + groupId);
			}

			if (reportToUserId > 0)
			{
				if (WHERE_CLAUSE.indexOf(".") > 0)
					WHERE_CLAUSE.append(" and");
				WHERE_CLAUSE.append(" u.reportTo=" + reportToUserId);
			}

			if (pageSize == 0)
				pageSize = MIN_RESULT_PER_PAGE;

			WHERE_CLAUSE.append(" limit " + startOffset + " , " + pageSize);

			if (reportToUserId > 0 || groupId > 0 || cityId > 0 || branchId > 0
					|| areaId > 0 || subAreaId > 0)
				queryStr.append(WHERE_CLAUSE);

			log.debug(queryStr.toString());
			Query query = session.createSQLQuery(queryStr.toString())
					.addEntity(User.class);
			List<User> users = query.list();

			if (users == null)
				return userVos;
			UserVo vo = null;
			for (User user : users)
			{
				vo = new UserVo();
				BeanUtils.copyProperties(user, vo);
				userVos.add(vo);
			}

			return userVos;
		} catch (BeansException e)
		{
			log.error("Error in copy bean:" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch ( Exception e)
		{
			log.error("Error While executing Query :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		log.debug(" exit from getUsers   "+ cityId);
		return userVos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserVo> getAllUsers()
	{
		log.debug(" inside getAllUsers   " );
		if (cachedUsersById == null || cachedUsersById.size() == 0)
			modifiedResetUser();
		if (cachedUsersById != null && !cachedUsersById.isEmpty())
		{
			return new ArrayList<UserVo>(cachedUsersById.values());
		} else
		{
			return new ArrayList<>();
		}
	}

	/*
	 * public String getVendorTypeBySkillId(Skill skill){
	 * 
	 * log.info("skill id:"+skill.getSkillId());
	 * 
	 * List<VendorTypesSkillMapping> vendorTypesSkillMappings =
	 * (List<VendorTypesSkillMapping>) hibernateTemplate.find(
	 * "from VendorTypesSkillMapping where skillId=?", skill.getSkillId());
	 * 
	 * String vendorType = null;
	 * 
	 * if(vendorTypesSkillMappings!=null){
	 * 
	 * VendorTypesSkillMapping vendorTypesSkillMapping =
	 * vendorTypesSkillMappings.get(0);
	 * 
	 * vendorType =
	 * vendorTypesSkillMapping.getVendorTypes().getVendorTypeName();
	 * 
	 * }
	 * 
	 * return vendorType;
	 * 
	 * }
	 */

	/*
	 * @SuppressWarnings("unchecked") public String getUserTablet(long userId) {
	 * 
	 * try {
	 * 
	 * List<UserTabletMapping> tabList = (List<UserTabletMapping>)
	 * hibernateTemplate .find("from UserTabletMapping where userGuid=?",
	 * userId);
	 * 
	 * if (tabList != null) {
	 * 
	 * UserTabletMapping mapping = tabList.get(0);
	 * 
	 * return mapping.getTablet().getMacAddress(); }
	 * 
	 * return null;
	 * 
	 * } catch (Exception e) {
	 * 
	 * e.printStackTrace();
	 * 
	 * return null; } }
	 */

	// @SuppressWarnings("unchecked")
	// public String getUserElement(long userId) {
	//
	// try {
	//
	// List<UserElementMapping> elements = (List<UserElementMapping>)
	// hibernateTemplate
	// .find("from UserElementMapping where userGuid=?",
	// userId);
	//
	// if (elements != null) {
	//
	// UserElementMapping mapping = elements.get(0);
	//
	// return mapping.getElement().getMacAddress();
	// }
	//
	// return null;
	//
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	//
	// return null;
	// }
	// }

	@SuppressWarnings("unchecked")
	@Override
	public User getUserByUserName(String name)
	{

		log.debug(" inside getUserByUserName "+name);

		try
		{

			List<User> users = (List<User>) hibernateTemplate
					.find("from User where loginId=?", name);

			if (users != null && users.size() > 0)
			{

				log.info("size::::" + users.size());

				return users.get(0);

			} else

				return null;
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Failed to get users. Reason:" + e.getMessage());
			return null;
		}

	}

	@Override
	public UserVo getUserById(Long id)
	{
		log.debug(" inside getUserById   " + id );
		if (cachedUsersById.containsKey(id))
		{
			return cachedUsersById.get(id);

		} else
		{
			getAllUsers();
			if (cachedUsersById.containsKey(id))
				return cachedUsersById.get(id);
		}
		log.debug(" exit from getUserById   " + id );
		return null;

	}

	/*
	 * @Override public APIResponse addUserRole(User user, List<String>
	 * userRoles) {
	 * 
	 * log.info("add userrole.......");
	 * 
	 * try {
	 * 
	 * for (int i = 0; i < userRoles.size(); i++) {
	 * 
	 * UserRole role = new UserRole(user, userRoles.get(i));
	 * 
	 * session.save(role);
	 * 
	 * if( i % 50 == 0 ) { session.flush(); session.clear(); }
	 * 
	 * session.flush(); }
	 * 
	 * UserRole role = new UserRole(user, role) session.save(testUser);
	 * 
	 * 
	 * tx.commit();
	 * 
	 * log.info("User roles Successfully Added into Database "); return
	 * ResponseUtil.createSaveSuccessResponse(); } catch (Exception e) {
	 * 
	 * System.out .println(
	 * "Error in Inserting New User Into DataBase for user Name "
	 * 
	 * + e.getMessage() + " " + e.getStackTrace().toString());
	 * 
	 * tx.rollback();
	 * 
	 * log.info("Hibernate Error"); e.printStackTrace();
	 * 
	 * return ResponseUtil.createDBExceptionResponse(); } finally {
	 * 
	 * session.close(); }
	 * 
	 * return null; }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public LoginResponseVo loginForApp(LoginPojo loginPojo) {
	 * 
	 * BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(); String
	 * encryptedPassword = encoder.encode(loginPojo.getPassword());
	 * 
	 * List<User> associatedUsers = new ArrayList<>(); Set<String> roles = new
	 * HashSet<>(); Map<String, Set<String>> userNameAndRolesMap = new
	 * HashMap<>(); Map<Long, String> subAreaMap = new HashMap<>();
	 * 
	 * log.info("credentials:"+loginPojo.getUsername()+"  "+encryptedPassword);
	 * 
	 * try{
	 * 
	 * String loginId = loginPojo.getUsername();
	 * 
	 * if (cachedusers.containsKey(loginId)) {
	 * 
	 * UserVo user = cachedusers.get(loginId);
	 * 
	 * LoginResponseVo responseVo = new LoginResponseVo();
	 * 
	 * if(encoder.matches(loginPojo.getPassword(),user.getPassword())){
	 * 
	 * log.info("password matches");
	 * 
	 * String fullName = user.getFirstName()+" "+user.getLastName();
	 * 
	 * responseVo.setUserId(user.getId());
	 * 
	 * responseVo.setDisplayName(fullName);
	 * 
	 * responseVo.setStatus(200);
	 * 
	 * TypeAheadVo areaVo = user.getArea();
	 * 
	 * if(areaVo != null){
	 * 
	 * responseVo.setAreaId(areaVo.getId());
	 * responseVo.setAreaName(areaVo.getName()); }
	 * 
	 * else{
	 * 
	 * log.info("area is null"); }
	 * 
	 * Set<TypeAheadVo> subAreaVos = user.getSubAreas();
	 * 
	 * log.info("userSubAreaMappings@@@@@"+ subAreaVos);
	 * 
	 * if(subAreaVos != null){
	 * 
	 * for (TypeAheadVo typeAheadVo : subAreaVos) {
	 * 
	 * subAreaMap.put(typeAheadVo.getId(), typeAheadVo.getName()); }
	 * 
	 * responseVo.setSubAreaMap(subAreaMap); }
	 * 
	 * 
	 * else{
	 * 
	 * log.info("sub area null");
	 * 
	 * }
	 * 
	 * Set<TypeAheadVo> userGroupUserMapping = user.getUserGroups();
	 * 
	 * log.info("user groups:"+userGroupUserMapping);
	 * 
	 * if(userGroupUserMapping!= null){
	 * 
	 * for (TypeAheadVo userRole : userGroupUserMapping) {
	 * 
	 * roles.addAll(getUserRoles(userRole.getId()));
	 * 
	 * }
	 * 
	 * log.info("roles of user ::"+roles);
	 *//*****************************
		 * fetching associated user details START
		 **************/
	/*
	 * 
	 * if(roles.contains("ROLE_TEAM_LEAD")){
	 * 
	 * associatedUsers = (List<User>) hibernateTemplate.find(
	 * "from User where user.id = ?", user.getId());
	 * 
	 * log.info("associatedUsers size ::"+associatedUsers.size());
	 * 
	 * for (User associatedUser : associatedUsers) {
	 * 
	 * Set<String> associatedUserRoles = new HashSet<>();
	 * 
	 * Set<UserGroupUserMapping> userGroupUserMapping1 =
	 * associatedUser.getUserGroupUserMappings();
	 * 
	 * //log.info("user groups:"+userGroupUserMapping1 + "for user:"
	 * +associatedUser.getLoginId());
	 * 
	 * for (UserGroupUserMapping userRole : userGroupUserMapping1) {
	 * 
	 * associatedUserRoles.addAll(getUserRoles(userRole.getUserGroup().getId()))
	 * ;
	 * 
	 * }
	 * 
	 * log.info("roles of associated user ::"+associatedUserRoles +
	 * "for user id:"+associatedUser.getLoginId());
	 * 
	 * userNameAndRolesMap.put(associatedUser.getLoginId(),
	 * associatedUserRoles); }
	 * 
	 * responseVo.setRoles(roles);
	 * responseVo.setAssociatedUsernameAndRoleMap(userNameAndRolesMap);
	 * 
	 * return responseVo;
	 * 
	 * }
	 * 
	 * else{
	 * 
	 * responseVo.setRoles(roles);
	 * 
	 * return responseVo; }
	 *//*****************************
		 * fetching associated user details END
		 **************/
	/*
	 * }
	 * 
	 * } else{ responseVo.setStatus(StatusCodes.BAD_CREDENTIALS); return
	 * responseVo; } } else { LoginResponseVo responseVo = new
	 * LoginResponseVo(); responseVo.setStatus(StatusCodes.NO_RECORD_FOUND);
	 * return responseVo; }
	 * 
	 * 
	 * } catch(Exception e){ e.printStackTrace(); return null; } return null; }
	 */

	/*
	 * public LoginResponseVo loginForApp(LoginPojo loginPojo) {
	 * 
	 * BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(); String
	 * encryptedPassword = encoder.encode(loginPojo.getPassword());
	 * 
	 * List<User> users = new ArrayList<User>(); List<User> associatedUsers =
	 * new ArrayList<>(); Set<String> roles = new HashSet<>(); Map<String,
	 * Set<String>> userNameAndRolesMap = new HashMap<>(); Map<Long, String>
	 * branchMap = new HashMap<>(); Map<Long, String> cityMap = new HashMap<>();
	 * Map<Long, String> areaMap = new HashMap<>(); Map<Long, String> subAreaMap
	 * = new HashMap<>();
	 * 
	 * log.info("credentials:"+loginPojo.getUsername()+"  "+encryptedPassword);
	 * 
	 * try{
	 * 
	 * users = (List<User>) hibernateTemplate.find("from User where loginId =?",
	 * loginPojo.getUsername());
	 * 
	 * log.info("users::"+users.size());
	 * 
	 * if (users.size() > 0) {
	 * 
	 * User user = users.get(0);
	 * 
	 * LoginResponseVo responseVo = new LoginResponseVo();
	 * 
	 * if(encoder.matches(loginPojo.getPassword(),user.getPassword())){
	 * 
	 * //tabDao.setCurrentUser(loginPojo.getDeviceMac(), user);
	 * 
	 * log.info("password matches"+ user.getId());
	 * 
	 * String fullName = user.getFirstName()+" "+user.getLastName();
	 * 
	 * responseVo.setUserId(user.getId());
	 * 
	 * responseVo.setDisplayName(fullName);
	 * 
	 * responseVo.setStatus(200);
	 * 
	 * Set<City> userCities = user.getCities();
	 * 
	 * if(!(userCities.isEmpty())){
	 * 
	 * log.info("User cities not null");
	 * 
	 * for (City city : userCities) {
	 * 
	 * cityMap.put(city.getId(), city.getCityName());
	 * 
	 * } }
	 * 
	 * responseVo.setCityMap(cityMap);
	 * 
	 * Set<Branch> userBranches = user.getBranches();
	 * 
	 * if(!(userBranches.isEmpty())){
	 * 
	 * log.info("User branches not null");
	 * 
	 * for (Branch branch : userBranches) {
	 * 
	 * branchMap.put(branch.getId(), branch.getBranchName()); } }
	 * 
	 * responseVo.setBranchMap(branchMap);
	 * 
	 * Set<Area> userAreas = user.getAreas();
	 * 
	 * if(!(userAreas.isEmpty())){
	 * 
	 * log.info("User areas not null");
	 * 
	 * for (Area area : userAreas) {
	 * 
	 * areaMap.put(area.getId(), area.getAreaName()); } }
	 * 
	 * responseVo.setAreaMap(areaMap);
	 * 
	 * Set<SubArea> userSubAreas = user.getSubAreas();
	 * 
	 * if(!(userSubAreas.isEmpty())){
	 * 
	 * log.info("User sub areas not null");
	 * 
	 * for (SubArea subArea : userSubAreas) {
	 * 
	 * subAreaMap.put(subArea.getId(), subArea.getSubAreaName()); } }
	 * 
	 * responseVo.setSubAreaMap(subAreaMap);
	 * 
	 * 
	 * Set<UserGroup> userGroups = user.getUserGroups();
	 * 
	 * if(!(userGroups.isEmpty())){
	 * 
	 * log.info("user groups not null");
	 * 
	 * for (UserGroup userGroup : userGroups) {
	 * 
	 * Set<Role> userRoles = userGroup.getRoles();
	 * 
	 * for (Role role : userRoles) {
	 * 
	 * roles.add(role.getRoleCode()); } }
	 * 
	 * log.info("roles of user ::"+roles); }
	 * 
	 * responseVo.setRoles(roles);
	 *//*****************************
		 * fetching associated user details START
		 **************/
	/*
	 * 
	 * associatedUsers = (List<User>) hibernateTemplate.find(
	 * "from User where user.id = ?", user.getId());
	 * 
	 * log.info("associatedUsers size ::"+associatedUsers.size());
	 * 
	 * if(associatedUsers.size() > 0){
	 * 
	 * for (User associatedUser : associatedUsers) {
	 * 
	 * Set<String> associatedUserRoles = new HashSet<>();
	 * 
	 * Set<UserGroup> associatedUserGroups = associatedUser.getUserGroups();
	 * 
	 * for (UserGroup userGroup : associatedUserGroups) {
	 * 
	 * Set<Role> associatedRoles = userGroup.getRoles();
	 * 
	 * for (Role role : associatedRoles) {
	 * 
	 * associatedUserRoles.add(role.getRoleCode()); } }
	 * 
	 * log.info("roles of associated user ::"+associatedUserRoles +
	 * "for user id:"+associatedUser.getLoginId());
	 * 
	 * userNameAndRolesMap.put(associatedUser.getLoginId(),
	 * associatedUserRoles); }
	 * 
	 * responseVo.setAssociatedUsernameAndRoleMap(userNameAndRolesMap);
	 * 
	 * return responseVo;
	 * 
	 * }
	 * 
	 * else{
	 * 
	 * log.info("User is not associated with any users");
	 * 
	 * return responseVo; }
	 * 
	 * //***************************** fetching associated user details
	 * END*************
	 */
	/*
	 * 
	 * } else{ responseVo.setStatus(StatusCodes.BAD_CREDENTIALS); return
	 * responseVo; } } else { LoginResponseVo responseVo = new
	 * LoginResponseVo(); responseVo.setStatus(StatusCodes.NO_RECORD_FOUND);
	 * return responseVo; }
	 * 
	 * 
	 * } catch(Exception e){ e.printStackTrace(); return null; } }
	 */

	public LoginResponseVo loginForApp(LoginPojo loginPojo)
	{

		LoginResponseVo userResponseVo = new LoginResponseVo();

		log.debug(" inside loginForApp   " );

		AppUser userDetails = (AppUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		userResponseVo.setUserId(userDetails.getId());

		userResponseVo.setDisplayName(userDetails.getUserDisplayName());

		userResponseVo.setStatus(200);

		userResponseVo.setBranches(Collections
				.singletonList(AuthUtils.getCurrentUserBranch()));

		userResponseVo.setUserGroups(Collections
				.singletonList(AuthUtils.getCurrentUserGroup()));

		userResponseVo.setCityId(AuthUtils.getCurrentUserCity());

		userResponseVo.setRoles(userDetails.getRoles());

		log.debug(" exit from loginForApp   " );
		
		return userResponseVo;
	}

	/*
	 * public String checkCurrentUser(LoginPojo loginPojo) {
	 * 
	 * String result = tabDao.checkCurrentUser(loginPojo);
	 * 
	 * return result; }
	 */

	/*
	 * public void getAllVendorTypes() {
	 * 
	 * log.info("Fetching All VendorTypes..");
	 * 
	 * try {
	 * 
	 * List<VendorTypes> vendorTypes = (List<VendorTypes>)
	 * hibernateTemplate.find("from VendorTypes");
	 * 
	 * if (vendorTypes != null) {
	 * 
	 * for (VendorTypes vendor : vendorTypes) {
	 * 
	 * vendorTypeIdAndVendorNameMap.put(vendor.getVendorTypeId(),
	 * vendor.getVendorTypeName()); }
	 * 
	 * log.info(
	 * "Fetching All VendorTypes from Database has been Successfully Done" );
	 * 
	 * } else {
	 * 
	 * log.info("Failed to fetch");
	 * 
	 * }
	 * 
	 * } catch (Exception e) {
	 * 
	 * log.error(
	 * "Error in Fetching All VendorTypes to which the user has to be assigned : Error Message is "
	 * + e.getMessage() + " " + e.getStackTrace().toString());
	 * 
	 * e.printStackTrace();
	 * 
	 * } }
	 */

	/*
	 * @Override public List<ViewObject> getAllGroup() {
	 * 
	 * log.info("Fetching All Groups to which the user has to be assigned");
	 * 
	 * Session session = null;
	 * 
	 * List<ViewObject> viewObjects = new ArrayList<>();
	 * 
	 * try {
	 * 
	 * session = getSessionFactory().openSession();
	 * 
	 * Query updatequery = session .createQuery("from UserGroup");
	 * 
	 * List<UserGroup> groupList = updatequery.list();
	 * 
	 * if (groupList != null) {
	 * 
	 * for (UserGroup userGroup : groupList) {
	 * 
	 * ViewObject object = new ViewObject(userGroup.getUserGroupGuid(),
	 * userGroup.getGroupId());
	 * 
	 * viewObjects.add(object); }
	 * 
	 * log.info("Fetching All Groups from Database has been Successfully Done");
	 * 
	 * } else {
	 * 
	 * log.info("User has not been assigned to any Group" + groupList.size());
	 * 
	 * }
	 * 
	 * } catch (Exception e) {
	 * 
	 * log.error(
	 * "Error in Fetching All Groups to which the user has to be assigned : Error Message is "
	 * + e.getMessage() + " " + e.getStackTrace().toString());
	 * 
	 * e.printStackTrace();
	 * 
	 * 
	 * } finally {
	 * 
	 * session.close(); }
	 * 
	 * return viewObjects; }
	 */

	/*
	 * public APIResponse addUserTablets(User user,String tabletName) {
	 * 
	 * log.info("adding user tablets......."+tabletName);
	 * 
	 * try {
	 * 
	 * Tablet tablet = new Tablet();
	 * 
	 * log.info("tab id:"+tabletIdAndMacMap.getKey(tabletName));
	 * 
	 * tablet.setTabletId((Long)tabletIdAndMacMap.getKey(tabletName));
	 * 
	 * UserTabletMapping mapping = new UserTabletMapping(user, tablet, new
	 * Date(), user.getUserGuid(), new Date(), user.getUserGuid(), 1);
	 * 
	 * hibernateTemplate.save(mapping);
	 * 
	 * log.info("User tablet Successfully Added into Database ");
	 * 
	 * return ResponseUtil.createSaveSuccessResponse();
	 * 
	 * } catch (Exception e) {
	 * 
	 * log.info("Error in Inserting New Tablet of user Into DataBase"
	 * 
	 * + e.getMessage() + " " + e.getStackTrace().toString());
	 * 
	 * log.info("Hibernate Error"); e.printStackTrace();
	 * 
	 * return ResponseUtil.createDBExceptionResponse(); } }
	 */

	/*
	 * @Override public APIResponse getUserListByPageIndex(int index,int
	 * itemPerPage) { userRecords.clear();
	 * 
	 * List<UserVo> userVos = new ArrayList<>();
	 * 
	 * int i = 0;
	 * 
	 * int pageIndex = index;
	 * 
	 * if (pageIndex == 0) { pageIndex = 1; }
	 * 
	 * int totalNoOfrecords = cachedusers.size(); //
	 * log.debug(totalNoOfrecords); int noofRecordPerPage = itemPerPage;
	 * 
	 * int noOfPage = totalNoOfrecords / noofRecordPerPage;
	 * 
	 * if (totalNoOfrecords > noOfPage * pageIndex) { noOfPage++; }
	 * 
	 * if (noOfPage < pageIndex) {
	 * 
	 * log.error("Page not found :");
	 * 
	 * return ResponseUtil.createRecordNotFoundResponse(); }
	 * 
	 * int setFirstResult = noofRecordPerPage * pageIndex - noofRecordPerPage;
	 * 
	 * Query query = new Query();
	 * 
	 * query.skip(setFirstResult);
	 * 
	 * query.limit(noofRecordPerPage);
	 * 
	 * for (Map.Entry<String, UserVo> entry : cachedusers.entrySet()) {
	 * 
	 * if(i == itemPerPage-1) break;
	 * 
	 * userVos.add(entry.getValue());
	 * 
	 * } userRecords.put(totalNoOfrecords,userVos);
	 * 
	 * 
	 * if (userVos.isEmpty())
	 * 
	 * { log.error("Failed to get Users as this not found"); return
	 * ResponseUtil.createRecordNotFoundResponse();
	 * 
	 * }
	 * 
	 * else {
	 * 
	 * log.info("Found users with id :" + userVos); return
	 * ResponseUtil.createSuccessResponse() .setData(userRecords); } }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String> userSearchSuggestion(String userQuery)
	{
		log.debug(" inside userSearchSuggestion   " +userQuery);
		// Object[] params = {123 , "ann"};

		List<User> users = (List<User>) hibernateTemplate
				.find("from User u where u.loginId like? and u.status <> 0", "%"
						+ userQuery + "%");

		Map<Long, String> userNames = new TreeMap<>();

		if (users != null)
		{

			log.info("list of users size inuser suggestion : " + users.size());

			for (User user : users)
			{

				userNames.put(user.getId(), user.getLoginId());

			}

		}
		log.debug(" exit from userSearchSuggestion   " +userQuery);
		
		return userNames;
	}

	@Override
	public Map<Long, String> elementSearchSuggestion(String elementQuery)
	{
		log.debug(" inside elementSearchSuggestion   "+ elementQuery);

		List<Device> elements = (List<Device>) hibernateTemplate
				.find("from Device where macAddress like?", "%" + elementQuery
						+ "%");

		Map<Long, String> elementNames = new TreeMap<>();

		if (elements != null)
		{

			log.info("list of users size inuser suggestion : "
					+ elements.size());

			for (Device element : elements)
			{

				elementNames.put(element.getId(), element.getMacAddress());

			}

		}
		log.debug(" exit from  elementSearchSuggestion   "+ elementQuery);
		return elementNames;

	}

	@Override
	public Map<Long, String> getVendorsByBranchName(TypeAheadVo aheadVo)
	{

		Map<Long, String> vendorNames = new TreeMap<>();

		log.debug(" inside getVendorsByBranchName   "+aheadVo );
		/*
		 * if(branchName == null || branchName.equals("null")){
		 * 
		 * log.info("banch name:"+branchName); }
		 */
		if (aheadVo != null)
		{

			log.info("branch id is not null...");

			List<Vendor> vendors = (List<Vendor>) hibernateTemplate
					.find("from Vendor where branch.id = ?", aheadVo.getId());

			if (vendors != null)
			{

				log.info("vendorBranchMappings size:" + vendors.size());

				for (Vendor vendor : vendors)
				{

					vendorNames.put(vendor.getId(), vendor.getName());
				}

				log.info("vendorNames :" + vendorNames + " for branch:"
						+ aheadVo.getName());

			}

		}

		else
		{

			log.info("No branch specified");
		}
		log.debug(" exit from getVendorsByBranchName   ");
		
		return vendorNames;
	}

	/*
	 * @Override public List<String> getVendorTypesByVendorName(String
	 * vendorName) {
	 * 
	 * List<String> vendorTypes = new ArrayList<>();
	 * 
	 * if(vendorName != null){
	 * 
	 * long vendorId = (long) vendorIdAndVendorNameMap.getKey(vendorName);
	 * 
	 * List<VendorTypesVendorMapping> vendorTypesVendorMappings =
	 * (List<VendorTypesVendorMapping>) hibernateTemplate.find(
	 * "from VendorTypesVendorMapping where vendorId = ?" , vendorId);
	 * 
	 * if(vendorTypesVendorMappings!=null){
	 * 
	 * log.info("vendorTypesVendorMappings size:"
	 * +vendorTypesVendorMappings.size( ));
	 * 
	 * for (VendorTypesVendorMapping vendorTypesVendorMapping :
	 * vendorTypesVendorMappings) {
	 * 
	 * vendorTypes.add(vendorTypesVendorMapping.getVendorTypes().
	 * getVendorTypeName ()); }
	 * 
	 * log.info("vendorNames :"+vendorTypes+" for branch:"+vendorName);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * return vendorTypes;
	 * 
	 * }
	 */

	@Override
	public Map<Long, String> getSkillsByVendorName(TypeAheadVo aheadVo)
	{

		Map<Long, String> skills = new TreeMap<>();

		if (aheadVo == null)
			return skills;
		
		log.debug(" inside getSkillsByVendorName   " +aheadVo.getId());
		/*
		 * if(vendorName == null || vendorName.equals("null")){
		 * 
		 * log.info("vendorName::"+vendorName); } else{
		 */

		if (aheadVo != null)
		{

			log.debug("vendorName is not null...");

		/*	List<VendorSkillMapping> vendorSkillMappings = (List<VendorSkillMapping>) hibernateTemplate
					.find("from VendorSkillMapping where vendor.id = ?", aheadVo
							.getId());*/
			List<Object[]> vendorSkillMappings  =	hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery(GET_VENDOR_SKILLS).setParameter("vendorId", aheadVo
							.getId()).list();

			if (vendorSkillMappings != null && !vendorSkillMappings.isEmpty() )
			{

				log.info("vendorTypesSkillMappings size:"
						+ vendorSkillMappings.size());
				for (Object[]  object : vendorSkillMappings)
				{
					skills.put(objectToLong(object[0]), (String)object[1]);
				}


				log.info("skills :" + skills + " for vendor:"
						+ aheadVo.getName());

			}
		}
		log.debug(" exit from getSkillsByVendorName   " +aheadVo.getId());
		
		return skills;
	}

	@Override
	public Map<Long, String> getAllUserNames(Long userType)
	{
		log.debug(" inside getAllUserNames   " +userType);

		if (cachedUsersById.size() == 0)
			resetCachedUserNames();

		Map<Long, String> userNames = new TreeMap<>();

		List<String> roles = new ArrayList<String>();

		for (Map.Entry<Long, UserVo> entry : cachedUsersById.entrySet())
		{
			UserVo user = entry.getValue();

			if (AuthUtils.isAdmin())
				;

			else if (user.getCity() == null || !AuthUtils.getCurrentUserCity()
					.getId().equals(user.getCity().getId()))
				continue;

			if (userType != null && userType >= 0
					&& user.getUserRoles() != null)
			{
				roles.clear();
				for (Iterator iterator = user.getUserRoles()
						.iterator(); iterator.hasNext();)
				{

					TypeAheadVo type = (TypeAheadVo) iterator.next();
					if (userType == AuthUtils.VENDOR_ELIGIBLE_USER_TYPE)
					{
						roles.add(type.getName());
					} else if (userType == AuthUtils.TAB_ELIGIBLE_USER_TYPE)
					{
						if (AuthUtils.TAB_ELIGIBLE_ROLES
								.contains(type.getName()))
						{
							userNames.put(entry.getKey(), GenericUtil
									.buildUserSelectOption(user));
						}
					}

				}

				if (userType == AuthUtils.VENDOR_ELIGIBLE_USER_TYPE)
				{
					if (roles.containsAll(AuthUtils.VENDOR_NI_ELIGIBLE_ROLES))
					{
						userNames.put(entry.getKey(), GenericUtil
								.buildUserSelectOption(user));
					}

				}

			} else
			{
				userNames.put(entry.getKey(), GenericUtil
						.buildUserSelectOption(user));
			}
		}
		log.debug(" exit from getAllUserNames   " +userType);
		return userNames;
	}

	@Override
	public List<String> getAllElementMacs()
	{
		log.debug(" inside getAllElementMacs   " );

		if (elementMacs.size() == 0)
			resetCachedElements();
		
		log.debug(" exit getAllElementMacs   " );
		return elementMacs;
	}

	@Override
	public String getUserNameByUserId(Long userId)
	{
		log.debug(" inside getUserNameByUserId   " +userId);

		if (userNames.size() == 0)
			resetCachedUserNames();

		log.debug(" exit getUserNameByUserId   " +userId);
		return userNames.get(userId);
	}

	// @Override
	// public void resetCachedUsers()
	// {
	//
	// log.info("Getting All user for cache.... ");
	// if (cachedUsersByLoginId != null)
	// cachedUsersByLoginId.clear();
	// if (cachedUsersById != null)
	// cachedUsersById.clear();
	// userNames.clear();
	// if (userAndDeviceMapping != null)
	// userAndDeviceMapping.clear();
	// log.info("All user cache cleared.... ");
	//
	// try
	// {
	//
	// String query = "from User u where u.status <> 0";
	//
	// List<User> users = (List<User>) hibernateTemplate.find(query);
	//
	// log.info("All user detailed fatched.... ");
	//
	// if (users != null && !users.isEmpty())
	// {
	// int size = users.size();
	//
	// log.info("Total User to cache are " + size);
	//
	// for (Iterator iterator = users.iterator(); iterator.hasNext();)
	// {
	// final User user = (User) iterator.next();
	//
	// try
	// {
	//
	// UserVo userVo = new UserVo();
	//
	// BeanUtils.copyProperties(user, userVo);
	//
	// if (user.getStatus() != null && user.getStatus() == 1)
	// userVo.setEnabled("enabled");
	//
	// else
	// userVo.setEnabled("disabled");
	//
	// Set<City> userCitySet = user.getCities();
	//
	// if (!(userCitySet.isEmpty()))
	// {
	//
	// for (City city : userCitySet)
	// {
	//
	// userVo.setCity(new TypeAheadVo(city
	// .getId(), city.getCityName()));
	// }
	//
	// }
	//
	// Set<Branch> userBranchSet = user.getBranches();
	//
	// if (!(userBranchSet.isEmpty()))
	// {
	//
	// for (Branch branch : userBranchSet)
	// {
	//
	// userVo.setBranch(new TypeAheadVo(branch
	// .getId(), branch.getBranchName()));
	// }
	//
	// }
	// Set<Area> userAreaSet = user.getAreas();
	//
	// if (!(userAreaSet.isEmpty()))
	// {
	// Set<TypeAheadVo> typeAheadVo = new HashSet<TypeAheadVo>();
	// for (Area area : userAreaSet)
	// {
	//
	// typeAheadVo.add(new TypeAheadVo(area
	// .getId(), area.getAreaName()));
	// }
	// userVo.setArea(typeAheadVo);
	// }
	//
	// Set<SubArea> usersubAreaSet = user.getSubAreas();
	//
	// if (usersubAreaSet != null)
	// {
	//
	// Set<TypeAheadVo> aheadVos = new HashSet<>();
	//
	// for (SubArea subArea : usersubAreaSet)
	// {
	//
	// TypeAheadVo aheadVo = new TypeAheadVo();
	// aheadVo.setId(subArea.getId());
	// aheadVo.setName(subArea.getSubAreaName());
	// aheadVos.add(aheadVo);
	//
	// }
	// userVo.setSubAreas(aheadVos);
	// }
	//
	// Set<Tablet> userTabs = user.getTabletsForAssignedTo();
	//
	// if (userTabs != null)
	// {
	//
	// for (Tablet tablet : userTabs)
	// {
	//
	// if (tablet != null)
	// {
	// userVo.setTablet(new TypeAheadVo(tablet
	// .getId(), tablet.getMacAddress()));
	// }
	// }
	//
	// }
	//
	// Set<Device> userDeviceSet = user.getDevices();
	//
	// if (userDeviceSet != null)
	// {
	//
	// Set<TypeAheadVo> aheadVos = new HashSet<>();
	//
	// for (Device device : userDeviceSet)
	// {
	// deviceNameUserIdCache.put(device
	// .getDeviceName(), user.getId());
	// TypeAheadVo aheadVo = new TypeAheadVo();
	// aheadVo.setId(device.getId());
	// aheadVo.setName(device.getDeviceName());
	// aheadVos.add(aheadVo);
	//
	// }
	// userVo.setDevices(aheadVos);
	//
	// log.debug("User " + user.getId()
	// + " is associated with devices "
	// + userVo.getDevices());
	//
	// }
	//
	// Set<Skill> userSkillSet = user.getSkills();
	//
	// if (userSkillSet != null)
	// {
	// Set<TypeAheadVo> aheadVos = new HashSet<>();
	//
	// for (Skill skill : userSkillSet)
	// {
	//
	// aheadVos.add(new TypeAheadVo(skill
	// .getId(), skill.getSkillName()));
	//
	// }
	//
	// userVo.setSkills(aheadVos);
	//
	// }
	//
	// Set<UserGroup> userGroupUserSet = user.getUserGroups();
	//
	// if (userGroupUserSet != null)
	//
	// {
	//
	// Set<TypeAheadVo> groupAheadVos = new HashSet<>();
	//
	// Set<TypeAheadVo> roleAheadVos = new HashSet<>();
	//
	// for (UserGroup userGroup : userGroupUserSet)
	// {
	//
	// TypeAheadVo groupTypeAheadVo = new TypeAheadVo(userGroup
	// .getId(), userGroup.getUserGroupName());
	// groupAheadVos.add(groupTypeAheadVo);
	//
	// Set<Role> roles = userGroup.getRoles();
	//
	// for (Role role : roles)
	// {
	// TypeAheadVo roleTypeAheadVo = new TypeAheadVo(role
	// .getId(), role.getRoleCode());
	// roleAheadVos.add(roleTypeAheadVo);
	// }
	// }
	// userVo.setUserRoles(roleAheadVos);
	// userVo.setUserGroups(groupAheadVos);
	// }
	//
	// Vendor vendor = user.getVendor();
	//
	// if (vendor != null)
	// {
	// log.debug("vendor name === " + vendor.getName());
	// userVo.setEmployer(new TypeAheadVo(vendor
	// .getId(), vendor.getName()));
	// }
	//
	// User reportToUser = user.getUser();
	//
	// if (reportToUser != null)
	// userVo.setReportTo(new TypeAheadVo(reportToUser
	// .getId(), reportToUser.getLoginId()));
	//
	// log.debug("User ############################# "
	// + userVo);
	//
	// addToLocalCache(userVo);
	//
	// log.info("Remaing User to cache are " + size--);
	//
	// } catch (Exception e)
	// {
	// log.error("Error while caching " + user.getLoginId()
	// + " " + e.getMessage());
	// e.printStackTrace();
	// continue;
	// }
	//
	// }
	// }
	//
	// populateNestedReportingUsers();
	// // System.exit(0);
	// log.info("All user data cached.... ");
	// } catch (Exception e)
	// {
	// e.printStackTrace();
	// log.error("Failed to get users. Reason:" + e.getMessage());
	// }
	//
	// }

	@Override
	public void modifiedResetUser()
	{
		log.info("Reseting initial records..");
		
		userCache.init();
		
		log.info("Initial record Reseted..");
		
		log.info("Getting All user for cache.... ");

		cachedUsersByLoginId.clear();
		cachedUsersById.clear();
		userNames.clear();
		userAndDeviceMapping.clear();
		log.info("All user cache cleared.... ");

		try
		{

			String modifiedQuery = "select u.id as userId,u.empId as empId, u.loginId as loginId,u.password as password,  "
					+ "u.firstName as firstName, u.lastName as lastName,u.emailId as emailId, u.mobileNo as mobileNo,  "
					+ "u1.id as reportToId, u1.loginId reportToLoginId,u.status as userStatus "
					+ "from User u join User u1 on u.reportTo=u1.id";

			List<UserVoModified> users = jdbcTemplate
					.query(modifiedQuery, new ModifiedUserMapper());

			log.info("All user detailed fatched.... ");

			if (users != null && !users.isEmpty()) {
				 
//				int totalSize = users.size();
//
//				log.info("########## addToCache START ##########" + new Date() + " for total User " + totalSize);
//
//				List<List<UserVoModified>> chunckedAreaList = new ArrayList<List<UserVoModified>>();
//
//				int maxLimit = 100;
//
//				for (int i = 0; i < users.size(); i += maxLimit) {
//					
//					chunckedAreaList.add(users.subList(i, Math.min(i + maxLimit, users.size())));
//				}
//
//				log.info("Total chunks for total Area " + totalSize + " are " + chunckedAreaList.size());
//
//				String threadPoolSizeString = definitionCoreService
//						.getThreadsPoolsInfo(DefinitionCoreService.FWMP_USER_CACHE_THREAD_POOL_SIZE);
//
//				ExecutorService executor = Executors
//						.newFixedThreadPool(threadPoolSizeString != null && !threadPoolSizeString.isEmpty()
//								? Integer.valueOf(threadPoolSizeString)
//								: DefaultThereadPoolConfig.DEFAULT_THEREAD_POOL_SIZE);
//
//				
//				
//				for (Iterator<List<UserVoModified>> iterator = chunckedAreaList.iterator(); iterator.hasNext();) {
//
//					final List<UserVoModified> list = (List<UserVoModified>) iterator.next();
//
//					try {
//
//						Runnable worker = new Runnable() {
//							@Override
//							public void run() {
//								
//								int listSize = list != null ? list.size() : 0;
//								Thread.currentThread().setName("chuncked User List no " + Math.random());
//								
//								try {
//									// Start processing from here
//									if (list != null && !list.isEmpty()) {
//										
//										for (UserVoModified area : list) {
//
//
//											log.info("Total User to cache are " + listSize);
//
//											for (Iterator<UserVoModified> iterator = list.iterator(); iterator.hasNext();) {
//												UserVoModified user = (UserVoModified) iterator.next();
//
//												try {
//
//													UserVo userVo = new UserVo();
//
//													userVo.setId(Long.valueOf(user.getUserId()));
//													userVo.setEmpId(Long.valueOf(user.getEmpId()));
//													userVo.setLoginId(user.getLoginId());
//													userVo.setPassword(user.getPassword());
//													userVo.setFirstName(user.getFirstName());
//													userVo.setLastName(user.getLastName());
//													userVo.setEmailId(user.getEmailId());
//													userVo.setMobileNo(
//															user.getMobileNo() != null ? Long.valueOf(user.getMobileNo()) : null);
//													// BeanUtils.copyProperties(user, userVo);
//
//													if (user.getEnabled() != null && Integer.valueOf(user.getEnabled()) == 1)
//														userVo.setEnabled("enabled");
//
//													else
//														userVo.setEnabled("disabled");
//
//													Set<TypeAheadVo> cityAheadVos = userCache.getUserCityByLoginId(userVo.getLoginId());
//
//													TypeAheadVo city = cityAheadVos != null ? cityAheadVos.iterator().next() : null;
//
//													userVo.setCity(city);
//
//													Set<TypeAheadVo> branchAheadVos = userCache.getUserBranchByLoginId(userVo.getLoginId());
//
//													TypeAheadVo branch = branchAheadVos != null ? branchAheadVos.iterator().next() : null;
//
//													userVo.setBranch(branch);
//
//													userVo.setArea(userCache.getUserAreaByLoginId(userVo.getLoginId()));
//
//													Set<TypeAheadVo> tabletAheadVos = userCache.getUserTabletByLoginId(userVo.getLoginId());
//
//													TypeAheadVo tablet = tabletAheadVos != null ? tabletAheadVos.iterator().next() : null;
//
//													userVo.setTablet(tablet);
//
//													userVo.setDevices(userCache.getUserDeviceByLoginId(userVo.getLoginId()));
//
//													log.debug("User " + user.getLoginId() + " is associated with devices "
//															+ userVo.getDevices());
//
//													Set<TypeAheadVo> groupAheadVos = userCache
//															.getUserUserGroupByLoginId(userVo.getLoginId());
//													userVo.setUserGroups(groupAheadVos);
//													log.debug("groupAheadVos " + groupAheadVos);
//													Set<TypeAheadVo> roleAheadVos = new HashSet<>();
//													if (groupAheadVos != null && !groupAheadVos.isEmpty()) {
//														for (Iterator<TypeAheadVo> iterator2 = groupAheadVos.iterator(); iterator2
//																.hasNext();) {
//															TypeAheadVo typeAheadVo = (TypeAheadVo) iterator2.next();
//
//															roleAheadVos.addAll(
//																	userGroupDAO.getuseGroupAndRolesTAByGroupId(typeAheadVo.getId()));
//
//														}
//
//														userVo.setUserRoles(roleAheadVos);
//													}
//
//													userVo.setReportTo(
//															new TypeAheadVo(Long.valueOf(user.getReportToId()), user.getReportToLoginId()));
//													
//													log.debug("User ############################# Thread.currentThread().getName()" + userVo);
//													
//													// System.exit(0);
//													addToLocalCache(userVo);
//
//													log.info("Remaing User to cache are " + --listSize + " for thread "
//															+ Thread.currentThread().getName());
//
//												} catch (Exception e) {
//													log.error("Error while caching " + user.getLoginId() + " " + e.getMessage());
//													e.printStackTrace();
//													continue;
//												}
//
//											}
//										}
//
//
//									}
//
//								} catch (Exception e) {
//									e.printStackTrace();
//									log.error("Error in area thread " + Thread.currentThread().getName() + " " + list.size()
//											+ "" + e.getMessage(), e);
//								}
//
//							}
//						};
//
//						executor.execute(worker);
//						// Thread.sleep(10);
//					} catch (Exception e) {
//						e.printStackTrace();
//
//						log.error("Error in area main thread " + e.getMessage(), e);
//						continue;
//					}
//
//				}
//				
////					{
//
						int size = users.size();

						log.info("Total User to cache are " + size);

						for (Iterator<UserVoModified> iterator = users.iterator(); iterator.hasNext();) {
							UserVoModified user = (UserVoModified) iterator.next();

							try {

								UserVo userVo = new UserVo();

								userVo.setId(Long.valueOf(user.getUserId()));
								userVo.setEmpId(Long.valueOf(user.getEmpId()));
								userVo.setLoginId(user.getLoginId());
								userVo.setPassword(user.getPassword());
								userVo.setFirstName(user.getFirstName());
								userVo.setLastName(user.getLastName());
								userVo.setEmailId(user.getEmailId());
								userVo.setMobileNo(
										user.getMobileNo() != null ? Long.valueOf(user.getMobileNo()) : null);
								// BeanUtils.copyProperties(user, userVo);

								if (user.getEnabled() != null && Integer.valueOf(user.getEnabled()) == 1)
									userVo.setEnabled("enabled");

								else
									userVo.setEnabled("disabled");

								Set<TypeAheadVo> cityAheadVos = userCache.getUserCityByLoginId(userVo.getLoginId());

								TypeAheadVo city = cityAheadVos != null ? cityAheadVos.iterator().next() : null;

								userVo.setCity(city);

								Set<TypeAheadVo> branchAheadVos = userCache.getUserBranchByLoginId(userVo.getLoginId());

								TypeAheadVo branch = branchAheadVos != null ? branchAheadVos.iterator().next() : null;

								userVo.setBranch(branch);

								userVo.setArea(userCache.getUserAreaByLoginId(userVo.getLoginId()));

								Set<TypeAheadVo> tabletAheadVos = userCache.getUserTabletByLoginId(userVo.getLoginId());

								TypeAheadVo tablet = tabletAheadVos != null ? tabletAheadVos.iterator().next() : null;

								userVo.setTablet(tablet);

								userVo.setDevices(userCache.getUserDeviceByLoginId(userVo.getLoginId()));

								log.debug("User " + user.getLoginId() + " is associated with devices "
										+ userVo.getDevices());

								Set<TypeAheadVo> groupAheadVos = userCache
										.getUserUserGroupByLoginId(userVo.getLoginId());
								userVo.setUserGroups(groupAheadVos);
								log.debug("groupAheadVos " + groupAheadVos);
								Set<TypeAheadVo> roleAheadVos = new HashSet<>();
								if (groupAheadVos != null && !groupAheadVos.isEmpty()) {
									for (Iterator<TypeAheadVo> iterator2 = groupAheadVos.iterator(); iterator2
											.hasNext();) {
										TypeAheadVo typeAheadVo = (TypeAheadVo) iterator2.next();

										roleAheadVos.addAll(
												userGroupDAO.getuseGroupAndRolesTAByGroupId(typeAheadVo.getId()));

									}

									userVo.setUserRoles(roleAheadVos);
								}

								userVo.setReportTo(
										new TypeAheadVo(Long.valueOf(user.getReportToId()), user.getReportToLoginId()));
								log.debug("User ############################# " + userVo);
								// System.exit(0);
								addToLocalCache(userVo);

								log.debug("Remaing User to cache are " + --size);

							} catch (Exception e) {
								log.error("Error while caching " + user.getLoginId() + " " + e.getMessage());
								e.printStackTrace();
								continue;
							}

						}
//					} 
				 
			}

			populateNestedReportingUsers();

			log.info("All user data cached.... ");
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Failed to get users. Reason:" + e.getMessage());
		}
		// System.exit(0);
	}

	@Override
	public void resetCachedUsersByUserId(Long userId)
	{

		log.info("Reseting user cache for user " + userId);

		try
		{
			String query = "from User u where u.status <> 0 and id =" + userId;

			List<User> users = (List<User>) hibernateTemplate.find(query);

			if (users != null)
			{

				for (User user : users)
				{

					UserVo userVo = new UserVo();

					BeanUtils.copyProperties(user, userVo);

					if (user.getStatus() != null && user.getStatus() == 1)
						userVo.setEnabled("enabled");

					else
						userVo.setEnabled("disabled");

					Set<City> userCitySet = user.getCities();

					if (!(userCitySet.isEmpty()))
					{

						for (City city : userCitySet)
						{

							userVo.setCity(new TypeAheadVo(city.getId(), city
									.getCityName()));
						}

					}

					Set<Branch> userBranchSet = user.getBranches();

					if (!(userBranchSet.isEmpty()))
					{

						for (Branch branch : userBranchSet)
						{

							userVo.setBranch(new TypeAheadVo(branch
									.getId(), branch.getBranchName()));
						}

					}
					Set<Area> userAreaSet = user.getAreas();

					if (!(userAreaSet.isEmpty()))
					{
						Set<TypeAheadVo> typeAheadVo = new HashSet<TypeAheadVo>();
						for (Area area : userAreaSet)
						{

							typeAheadVo.add(new TypeAheadVo(area.getId(), area
									.getAreaName()));
						}
						userVo.setArea(typeAheadVo);
					}

					Set<SubArea> usersubAreaSet = user.getSubAreas();

					if (usersubAreaSet != null)
					{

						Set<TypeAheadVo> aheadVos = new HashSet<>();

						for (SubArea subArea : usersubAreaSet)
						{

							TypeAheadVo aheadVo = new TypeAheadVo();
							aheadVo.setId(subArea.getId());
							aheadVo.setName(subArea.getSubAreaName());
							aheadVos.add(aheadVo);

						}
						userVo.setSubAreas(aheadVos);
					}

					Set<Tablet> userTabs = user.getTabletsForAssignedTo();

					if (userTabs != null)
					{

						for (Tablet tablet : userTabs)
						{

							if (tablet != null)
							{
								userVo.setTablet(new TypeAheadVo(tablet
										.getId(), tablet.getMacAddress()));
							}
						}

					}

					Set<Device> userDeviceSet = user.getDevices();

					if (userDeviceSet != null)
					{

						Set<TypeAheadVo> aheadVos = new HashSet<>();

						for (Device device : userDeviceSet)
						{
							deviceNameUserIdCache
									.put(device.getDeviceName(), user.getId());
							TypeAheadVo aheadVo = new TypeAheadVo();
							aheadVo.setId(device.getId());
							aheadVo.setName(device.getDeviceName());
							aheadVos.add(aheadVo);

						}
						userVo.setDevices(aheadVos);

						log.debug("User " + user.getId()
								+ " is associated with devices "
								+ userVo.getDevices());

					}

					Set<Skill> userSkillSet = user.getSkills();

					if (userSkillSet != null)
					{
						Set<TypeAheadVo> aheadVos = new HashSet<>();

						for (Skill skill : userSkillSet)
						{

							aheadVos.add(new TypeAheadVo(skill.getId(), skill
									.getSkillName()));

						}

						userVo.setSkills(aheadVos);

					}

					Set<UserGroup> userGroupUserSet = user.getUserGroups();

					if (userGroupUserSet != null)

					{

						Set<TypeAheadVo> groupAheadVos = new HashSet<>();

						Set<TypeAheadVo> roleAheadVos = new HashSet<>();

						for (UserGroup userGroup : userGroupUserSet)
						{

							TypeAheadVo groupTypeAheadVo = new TypeAheadVo(userGroup
									.getId(), userGroup.getUserGroupName());
							groupAheadVos.add(groupTypeAheadVo);

							Set<Role> roles = userGroup.getRoles();

							for (Role role : roles)
							{
								TypeAheadVo roleTypeAheadVo = new TypeAheadVo(role
										.getId(), role.getRoleCode());
								roleAheadVos.add(roleTypeAheadVo);
							}
						}
						userVo.setUserRoles(roleAheadVos);
						userVo.setUserGroups(groupAheadVos);
					}

					Vendor vendor = user.getVendor();

					if (vendor != null)
					{
						log.debug("vendor name === " + vendor.getName());
						userVo.setEmployer(new TypeAheadVo(vendor
								.getId(), vendor.getName()));
					}

					User reportToUser = user.getUser();

					if (reportToUser != null)
						userVo.setReportTo(new TypeAheadVo(reportToUser
								.getId(), reportToUser.getLoginId()));

					addToLocalCache(userVo);

				}
			}

			populateNestedReportingUsers();

			log.info("All user data cached.... ");
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Failed to get users. Reason:" + e.getMessage());
		}

	}

	private void populateNestedReportingUsers()
	{
		log.info("Populating Nested report to cache started..");
		for (Entry<Long, List<Long>> entry : userAndReportingMapping.entrySet())
		{
			if (entry.getKey() == -1)
				continue;
			Set<Long> reportingUser = new HashSet<Long>();
			populateReportingUser(entry.getKey(), reportingUser);
			userAndNestedReportingMapping
					.put(entry.getKey(), new ArrayList<Long>(reportingUser));

		}
		log.info("Populating Nested report to cache completed ");

	}

	private static void populateReportingUser(Long id, Set<Long> reportingUser)
	{
		if (userAndReportingMapping.get(id) == null)
			return;

		log.debug(" inside populateReportingUser   " +id);

		for (Iterator iterator = userAndReportingMapping.get(id)
				.iterator(); iterator.hasNext();)
		{
			Long long1 = (Long) iterator.next();
			reportingUser.addAll(userAndReportingMapping.get(id));
			populateReportingUser(long1, reportingUser);
		}

	}

	@Override
	public Map<Long, String> getUserByGroupForAreas(List<Long> areaIds,
			Long userGroupId)
	{
		log.debug(" inside getUserByGroupForAreas   "+ userGroupId);
		
		Map<Long, String> users = new HashMap<Long, String>();

		Collection<UserVo> allUsers = cachedUsersById.values();

		for (Iterator<UserVo> iterator = allUsers.iterator(); iterator
				.hasNext();)
		{
			UserVo userVo = iterator.next();
			if (isUserContainsGroup(userVo, userGroupId) && areaIds != null
					&& areaIds.size() > 0
					&& isUserBelongsToArea(areaIds, userVo, null))
			{
				users.put(userVo.getId(), GenericUtil
						.buildUserSelectOption(userVo));
			}
		}

		return users;

	}

	public Map<Long, String> getUserForAreasWithRoles(List<Long> areaIds,
			List<String> userRoles, boolean matchAllRoles)
	{
		log.debug(" inside getUserForAreasWithRoles   "+ areaIds );
		Map<Long, String> users = new HashMap<Long, String>();

		Collection<UserVo> allUsers = cachedUsersById.values();

		for (Iterator<UserVo> iterator = allUsers.iterator(); iterator
				.hasNext();)
		{
			UserVo userVo = iterator.next();

			if (userVo.getArea() != null && matchAllRoles
					&& isUserContainsAllRole(userVo, userRoles)
					&& isUserBelongsToArea(areaIds, userVo, null))
			{
				users.put(userVo.getId(), GenericUtil
						.buildUserSelectOption(userVo));
			} else if (userVo.getArea() != null && !matchAllRoles
					&& isUserContainsAnyRole(userVo, userRoles)
					&& isUserBelongsToArea(areaIds, userVo, null))
			{
				users.put(userVo.getId(), GenericUtil
						.buildUserSelectOption(userVo));
			}
		}
		log.debug(" exit from getUserForAreasWithRoles   "+ areaIds);
		return users;

	}

	@Override
	public List<UserVo> getUserDetailForAreasWithRoles(List<Long> areaIds,
			List<String> userRoles, boolean matchAllRoles)
	{
		log.debug(" inside getUserDetailForAreasWithRoles   " +areaIds);

		List<UserVo> users = new ArrayList<UserVo>();

		Collection<UserVo> allUsers = cachedUsersById.values();

		for (Iterator<UserVo> iterator = allUsers.iterator(); iterator
				.hasNext();)
		{
			UserVo userVo = iterator.next();

			if (userVo.getArea() != null && matchAllRoles
					&& isUserContainsAllRole(userVo, userRoles)
					&& isUserBelongsToArea(areaIds, userVo, null))
			{
				users.add(userVo);
			} else if (userVo.getArea() != null && !matchAllRoles
					&& isUserContainsAnyRole(userVo, userRoles)
					&& isUserBelongsToArea(areaIds, userVo, null))
			{
				users.add(userVo);
			}
		}
		log.debug(" exit from getUserDetailForAreasWithRoles   " +areaIds);
		return users;

	}

	@Override
	public Map<Long, String> getUserForAreas(List<Long> areaIds,
			List<Long> branchIds, List<String> userRoles, Long userGroupId)
	{
		log.debug(" inside getUserForAreas   " +areaIds);
		
		Map<Long, String> users = new HashMap<Long, String>();

		Collection<UserVo> allUsers = cachedUsersById.values();

		for (Iterator<UserVo> iterator = allUsers.iterator(); iterator
				.hasNext();)
		{
			UserVo userVo = iterator.next();
			if (areaIds != null && areaIds.size() > 0
					&& userVo.getArea() != null)
			{
				if (userGroupId == null
						&& isUserContainsAnyRole(userVo, userRoles)
						&& isUserBelongsToArea(areaIds, userVo, branchIds))
				{
					users.put(userVo.getId(), GenericUtil
							.buildUserSelectOption(userVo));
				} else if (isUserContainsAllRole(userVo, userRoles)
						&& isUserBelongsToArea(areaIds, userVo, branchIds))
				{
					users.put(userVo.getId(), GenericUtil
							.buildUserSelectOption(userVo));
				}

			}

			else if (userGroupId == null && branchIds != null
					&& branchIds.size() > 0 && userVo.getBranch() != null
					&& isUserContainsAnyRole(userVo, userRoles)
					&& branchIds.contains(userVo.getBranch().getId()))
			{
				users.put(userVo.getId(), GenericUtil
						.buildUserSelectOption(userVo));
			}

			else if (branchIds != null && branchIds.size() > 0
					&& userVo.getBranch() != null
					&& isUserContainsAllRole(userVo, userRoles)
					&& branchIds.contains(userVo.getBranch().getId()))
			{
				users.put(userVo.getId(), GenericUtil
						.buildUserSelectOption(userVo));
			} else if (userGroupId == null && userVo.getArea() != null
					&& isUserContainsAnyRole(userVo, userRoles)
					&& isUserBelongsToArea(areaIds, userVo, branchIds))
			{
				users.put(userVo.getId(), GenericUtil
						.buildUserSelectOption(userVo));
			} else if (userVo.getArea() != null
					&& isUserContainsAllRole(userVo, userRoles)
					&& isUserBelongsToArea(areaIds, userVo, branchIds))
			{
				users.put(userVo.getId(), GenericUtil
						.buildUserSelectOption(userVo));
			}

			log.debug(" exit from getUserForAreas   " + areaIds != null ? areaIds.size() : 0);
		}
		
		return users;
	}

	private boolean isUserBelongsToArea(List<Long> areaIds, UserVo userVo,
			List<Long> brancheIds)
	{
		log.debug(" inside isUserBelongsToArea   " +areaIds);
		if (userVo == null || userVo.getCity() == null || !userVo.getCity()
				.getId().equals(AuthUtils.getCurrentUserCity().getId()))
			return false;

		if (AuthUtils.isManager() && brancheIds != null
				&& !brancheIds.isEmpty())
		{

			if (userVo.getBranch() != null
					&& brancheIds.contains(userVo.getBranch().getId()))
				return true;
		}

		if (userVo.getArea() != null)
			for (TypeAheadVo area : userVo.getArea())
			{
				if (areaIds.contains(area.getId()))
					return true;
			}
		log.debug(" exit from isUserBelongsToArea   " +areaIds);
		
		return false;

	}

	private void addToDeviceMapping(UserVo userVo)
	{
		if(userVo == null)
			return;
		
		log.debug(" inside addToDeviceMapping   " +userVo.getLoginId());
		
		Set<TypeAheadVo> userDeviceSet = userVo.getDevices();

		if (userDeviceSet != null && !userDeviceSet.isEmpty())
		{

			for (Iterator<TypeAheadVo> iterator = userDeviceSet
					.iterator(); iterator.hasNext();)
			{
				TypeAheadVo device = (TypeAheadVo) iterator.next();

				if (userAndDeviceMapping.containsKey(device.getName()))
				{
					userAndDeviceMapping.get(device.getName())
							.add(userVo.getId());
				} else
				{
					Set<Long> users = new HashSet<Long>();
					users.add(userVo.getId());
					userAndDeviceMapping.put(device.getName(), users);
				}

			}

		}
	}

	@Override
	public int addToCache(UserVo userVo)
	{
		if(userVo == null)
			return 0;
		
		log.debug(" inside addToCache   "+ userVo.getEmpId());
		
		userCache.setIncrementalCache(userVo);
		addToLocalCache(userVo);

		if (userNames.containsKey(userVo.getId()))
			return 1;
		else
			return 0;
	}

	private void addToLocalCache(UserVo userVo)
	{
		if(userVo == null)
			return;
		
		log.debug("user =========" + userVo.getId() + " is adding in CACHE");
		cachedUsersByLoginId.put(userVo.getLoginId().toUpperCase(), userVo );
		cachedUsersById.put(userVo.getId(), userVo);
		userNames.put(userVo.getId(), userVo.getLoginId());
		addToDeviceMapping(userVo);

		if (userVo.getEmpId() > 0)
		{
			empIdAndUserMapping.put(userVo.getEmpId(), userVo.getId());
			userIdAndEmpIdMapping.put(userVo.getId(), userVo.getEmpId());

		}

		if (userVo.getReportTo() != null
				&& userVo.getReportTo().getId() != null)
		{
			userAndReportToMapping
					.put(userVo.getId(), userVo.getReportTo().getId());
			if (userAndReportingMapping
					.get(userVo.getReportTo().getId()) != null)
				userAndReportingMapping.get(userVo.getReportTo().getId())
						.add(userVo.getId());
			else
			{
				List<Long> list = new ArrayList<Long>();
				list.add(userVo.getId());
				userAndReportingMapping.put(userVo.getReportTo().getId(), list);
			}
		}
	}

	private void resetCachedUserNames()
	{

		log.info("Fetching All user names and their ids..");

		try
		{

			List<User> userList = (List<User>) hibernateTemplate
					.find("from User");

			if (userList != null)
			{

				for (User user : userList)
				{

					userNames.put(user.getId(), user.getLoginId());

				}
				log.info("Fetching All usernames from Database has been Successfully Done"
						+ userNames);

			} else
			{

				log.info("No User Found");

			}

		} catch (Exception e)
		{

			log.error("Error in Fetching All usernames : Error Message is "
					+ e.getMessage() );

			e.printStackTrace();

		}
	}

	private void resetCachedElements()
	{

		log.info("Fetching All Devices..");

		try
		{

			List<Device> devices = (List<Device>) hibernateTemplate
					.find("from Device");

			if (devices != null)
			{

				for (Device device : devices)
				{

					elementMacs.add(device.getMacAddress());
				}

				log.info("Fetching All Areas from Database has been Successfully Done");

			} else
			{

				log.info("User has not been assigned to any Group");

			}

		} catch (Exception e)
		{

			log.error("Error in Fetching All Groups to which the user has to be assigned : Error Message is "
					+ e.getMessage() );

			e.printStackTrace();

		}
	}

	/*
	 * private void deleteUserGroup(Session session, UserVo userVo){
	 * 
	 * try{
	 * 
	 * Query query = session.createQuery(
	 * "from UserGroupUserMapping where userLoginId = :ID");
	 * 
	 * List<UserGroupUserMapping> groupUserMappings = query.list();
	 * 
	 * if(groupUserMappings!=null){
	 * 
	 * for (UserGroupUserMapping userGroupUserMapping : groupUserMappings) {
	 * 
	 * userGroupUserMapping.setStatus(2); } } } }
	 */
	@Override
	public PagedResultSet getUsers(UserFilter filter)
	{
		if(filter == null)
			return null;

		log.debug(" inside getUsers   " +filter.getBranchId());
		
		Session session = null;
		try
		{
			session = hibernateTemplate.getSessionFactory().openSession();
			log.info("User Filter ============= " + filter);
			/*
			 * StringBuilder completeQuery = new StringBuilder(
			 * "FROM Area where UserAreaMapping.areaId=" +filter.getAreaId());
			 * 
			 * completeQuery.append(" limit "+filter.getStartOffset() +" , "
			 * +filter.getPageSize());
			 * 
			 * Query query = hibernateTemplate.getSessionFactory()
			 * .getCurrentSession().createQuery(completeQuery.toString());
			 * 
			 * List<User> userList = query.list();
			 * 
			 * log.info("user list=========="+userList);
			 */

			String firstName = filter.getFirstName();
			long reportToUserId = filter.getReportToUserId();
			long groupId = filter.getUserGroupId();

			long areaId = filter.getAreaId();
			long branchId = filter.getBranchId();
			long cityId = filter.getCityId();
			long subAreaId = filter.getSubAreaId();
			int pageSize = filter.getPageSize();
			int startOffset = filter.getStartOffset();

			String loginId = filter.getLoginId();
			
			String employeeId = filter.getEmployeeId();
			
			StringBuilder queryStr = new StringBuilder(SELECT_USER_QUERY);
			StringBuilder WHERE_CLAUSE = new StringBuilder(" where ");
			StringBuilder STATUS_CLAUSE = new StringBuilder(" u.status = " + DBActiveStatus.ACTIVE );
			StringBuilder LIMIT_CLAUSE = new StringBuilder(" limit "
					+ startOffset + " , " + pageSize);

			String roleCode = null;
			StringBuilder usrRole = new StringBuilder("");
			StringBuilder name = new StringBuilder("");

			// SELECT u.* FROM User u inner join UserGroupUserMapping ugm on
			// u.id =
			// ugm.idUser
			//
			// inner join UserSubAreaMapping usm on u.id = usm.idUser
			//
			// inner join UserAreaMapping uam on u.id = uam.userId
			//
			// inner join UserBranchMapping ubm on u.id = ubm.idUser
			// inner join UserCityMapping ucm on u.id = ucm.idUser

			if (AuthUtils.isAdmin())
			{
				if (subAreaId > 0)

				{
					queryStr.append(JOIN_SUB_AREA);
					WHERE_CLAUSE
							.append(" usm.IdSubArea=" + filter.getSubAreaId());
				}
				 
				if (firstName != null && !firstName.isEmpty())
				{

					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");

					WHERE_CLAUSE.append(" u.firstName like'" + "%" + firstName
							+ "%" + "'");
				}

				if (areaId > 0)

				{
					queryStr.append(JOIN_AREA);
					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");

					WHERE_CLAUSE.append(" uam.areaId=" + filter.getAreaId());
				}

				if (branchId > 0)

				{
					queryStr.append(JOIN_BRANCH);
					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");
					WHERE_CLAUSE
							.append(" ubm.idBranch=" + filter.getBranchId());
				}

				if (cityId > 0)
				{
					queryStr.append(JOIN_CITY);
					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");
					WHERE_CLAUSE.append(" ucm.idCity=" + filter.getCityId());
				}

				if (groupId > 0)
				{
					queryStr.append(JOIN_GROUP);
					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");
					WHERE_CLAUSE.append(" ugm.idUserGroup="
							+ filter.getUserGroupId());
				}

				if (reportToUserId > 0)
				{
					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");
					WHERE_CLAUSE.append(" u.reportTo="
							+ filter.getReportToUserId());
				}

				if (loginId != null && !loginId.isEmpty())
				{
					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");
					WHERE_CLAUSE.append(" u.loginId='" + loginId + "' ");
				}
				if (employeeId != null && !employeeId.isEmpty())
				{
					if (WHERE_CLAUSE.indexOf(".") > 0)
						WHERE_CLAUSE.append(" and");
					WHERE_CLAUSE.append(" u.empId='" + employeeId + "' ");
				}

				if (pageSize == 0)
					pageSize = MIN_RESULT_PER_PAGE;

				if (WHERE_CLAUSE.indexOf(".") > 0)
					WHERE_CLAUSE.append(" and");

				WHERE_CLAUSE.append(STATUS_CLAUSE);
				WHERE_CLAUSE.append(LIMIT_CLAUSE);

				/*
				 * if(reportToUserId > 0 || groupId > 0 || cityId > 0 ||
				 * branchId > 0 || areaId > 0 || subAreaId > 0)
				 * queryStr.append(WHERE_CLAUSE);
				 * 
				 * else
				 */

				queryStr.append(WHERE_CLAUSE);
				

				log.info(queryStr.toString());
			}

			else if (AuthUtils.isSalesUser())
			{

				roleCode = "ROLE_SALES";

				if (AuthUtils.isBranchManager())
				{

					usrRole.append(" and ug.userGroupCode <> '"
							+ UserGroupCodes.SALES_CM + "'");
				}

				if (AuthUtils.isAreaManager())
				{

					usrRole.append(" and ug.userGroupCode not in ('"
							+ UserGroupCodes.SALES_CM + "','"
							+ UserGroupCodes.SALES_BM + "')");
				}

			}

			else if (AuthUtils.isNIUser())
			{

				roleCode = "ROLE_NI";

				if (AuthUtils.isBranchManager())
				{

					usrRole.append(" and ug.userGroupCode <> '"
							+ UserGroupCodes.NI_CM + "'");
				}

				if (AuthUtils.isAreaManager())
				{

					usrRole.append(" and ug.userGroupCode not in ('"
							+ UserGroupCodes.NI_CM + "','"
							+ UserGroupCodes.NI_BM + "')");
				}
			}

			else if (AuthUtils.isNEUser())
			{

				roleCode = "ROLE_NE";

			}

			else if (AuthUtils.isFRUser())
			{

				roleCode = "ROLE_FR";

			}

			if (!AuthUtils.isAdmin())
			{

				queryStr.append(USER_GROUP);

				if (firstName != null && !firstName.isEmpty())
				{
					name.append(" and u.firstName like '" + "%" + firstName
							+ "%" + "'");
				}

				if (areaId > 0)

				{
					queryStr.append(JOIN_AREA + " and uam.areaId="
							+ filter.getAreaId());

				}

				if (branchId > 0)

				{
					queryStr.append(JOIN_BRANCH + " and ubm.idBranch="
							+ filter.getBranchId());

				}

				if (cityId > 0)
				{
					queryStr.append(JOIN_CITY + " and ucm.idCity="
							+ filter.getCityId());

				} else
				{
					queryStr.append(JOIN_CITY + " and ucm.idCity="
							+ AuthUtils.getCurrentUserCity().getId());

				}
				queryStr.append(" where r.roleCode= '" + roleCode + "' "
						+ usrRole + " " + name);

				if (loginId != null && !loginId.isEmpty())
				{
					queryStr.append(" and u.loginId='" + loginId + "' ");
				}
				
				if (groupId > 0)
				{
					queryStr.append(" and ugm.idUserGroup='" + filter.getUserGroupId() + "' ");
				}
				
				queryStr.append(" and " +STATUS_CLAUSE);
				queryStr.append(LIMIT_CLAUSE);
				log.info(queryStr.toString());
			}

			// Query query = session.createSQLQuery(queryStr.toString())
			// .addEntity(User.class);
			//
			// List<User> users = query.list();

			
			List<UserVoModified> users = jdbcTemplate
					.query(queryStr.toString(), new ModifiedUserMapper());

			// List<UserVo> userVos = xformToVoList(users);
			List<UserVo> userVos = xformToVoListModified(users);

			return CommonUtil.createPagedResultSet(userVos);
			/*
			 * List<UserVo> userVos = new ArrayList<>(); if(users == null)
			 * return userVos; UserVo vo = null; for(User user: users) { vo =
			 * new UserVo(); BeanUtils.copyProperties(user, vo);
			 * userVos.add(vo); }
			 * 
			 * return userVos;
			 */

			/*
			 * Criteria criteria = hibernateTemplate.getSessionFactory()
			 * .getCurrentSession().createCriteria(User.class,"user");
			 * 
			 * if (filter != null) {
			 * 
			 * if (filter.getUserGroupId() != null && filter.getUserGroupId() >
			 * 0) {
			 * 
			 * criteria.add(Restrictions.eq("id", filter.getUserGroupId()));
			 * 
			 * }
			 * 
			 * if (filter.getPageSize() > 0 || filter.getPageSize() > 0) { int
			 * pageIndex = filter.getStartOffset(); int pageSize =
			 * filter.getPageSize();
			 * 
			 * if (pageIndex == 0 || pageSize == 0) { pageIndex = 1; pageSize =
			 * 5; } criteria.setFirstResult(pageIndex);
			 * criteria.setMaxResults(pageSize); }
			 * 
			 * if (filter.getAreaId() != null && filter.getAreaId() > 0) {
			 * log.debug(filter.getAreaId());
			 * 
			 * Area area = new Area(); area.setId(filter.getAreaId());
			 * criteria.add(Restrictions.eq("area", area));
			 * 
			 * } }
			 * 
			 * return xformToVoList((List<User>)criteria.list(), filter);
			 */
		} catch ( Exception e)
		{
			log.error("Error While executing Query :" + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		log.debug(" exit from getUsers   " +filter.getBranchId());
		
		return null;

	}
	// @Override
	// public PagedResultSet getUsers(UserFilter filter)
	// {
	//
	// Session session = null;
	// try
	// {
	// session = hibernateTemplate.getSessionFactory().openSession();
	// log.info("User Filter ============= " + filter);
	// /*
	// * StringBuilder completeQuery = new StringBuilder(
	// * "FROM Area where UserAreaMapping.areaId=" +filter.getAreaId());
	// *
	// * completeQuery.append(" limit "+filter.getStartOffset() +" , "
	// * +filter.getPageSize());
	// *
	// * Query query = hibernateTemplate.getSessionFactory()
	// * .getCurrentSession().createQuery(completeQuery.toString());
	// *
	// * List<User> userList = query.list();
	// *
	// * log.info("user list=========="+userList);
	// */
	//
	// String firstName = filter.getFirstName();
	// long reportToUserId = filter.getReportToUserId();
	// long groupId = filter.getUserGroupId();
	//
	// long areaId = filter.getAreaId();
	// long branchId = filter.getBranchId();
	// long cityId = filter.getCityId();
	// long subAreaId = filter.getSubAreaId();
	// int pageSize = filter.getPageSize();
	// int startOffset = filter.getStartOffset();
	//
	// String loginId = filter.getLoginId();
	//
	// StringBuilder queryStr = new StringBuilder(SELECT_USER_QUERY);
	// StringBuilder WHERE_CLAUSE = new StringBuilder(" where ");
	// StringBuilder STATUS_CLAUSE = new StringBuilder(" status <> 0");
	// StringBuilder LIMIT_CLAUSE = new StringBuilder(" limit "
	// + startOffset + " , " + pageSize);
	//
	// String roleCode = null;
	// StringBuilder usrRole = new StringBuilder("");
	// StringBuilder name = new StringBuilder("");
	//
	// // SELECT u.* FROM User u inner join UserGroupUserMapping ugm on
	// // u.id =
	// // ugm.idUser
	// //
	// // inner join UserSubAreaMapping usm on u.id = usm.idUser
	// //
	// // inner join UserAreaMapping uam on u.id = uam.userId
	// //
	// // inner join UserBranchMapping ubm on u.id = ubm.idUser
	// // inner join UserCityMapping ucm on u.id = ucm.idUser
	//
	// if (AuthUtils.isAdmin())
	// {
	// if (subAreaId > 0)
	//
	// {
	// queryStr.append(JOIN_SUB_AREA);
	// WHERE_CLAUSE
	// .append(" usm.IdSubArea=" + filter.getSubAreaId());
	// }
	//
	// if (firstName != null && firstName != "")
	// {
	//
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	//
	// WHERE_CLAUSE.append(" u.firstName like'" + "%" + firstName
	// + "%" + "'");
	// }
	//
	// if (areaId > 0)
	//
	// {
	// queryStr.append(JOIN_AREA);
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	//
	// WHERE_CLAUSE.append(" uam.areaId=" + filter.getAreaId());
	// }
	//
	// if (branchId > 0)
	//
	// {
	// queryStr.append(JOIN_BRANCH);
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	// WHERE_CLAUSE
	// .append(" ubm.idBranch=" + filter.getBranchId());
	// }
	//
	// if (cityId > 0)
	// {
	// queryStr.append(JOIN_CITY);
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	// WHERE_CLAUSE.append(" ucm.idCity=" + filter.getCityId());
	// }
	//
	// if (groupId > 0)
	// {
	// queryStr.append(JOIN_GROUP);
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	// WHERE_CLAUSE.append(" ugm.idUserGroup="
	// + filter.getUserGroupId());
	// }
	//
	// if (reportToUserId > 0)
	// {
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	// WHERE_CLAUSE.append(" u.reportTo="
	// + filter.getReportToUserId());
	// }
	//
	// if (loginId != null && !loginId.isEmpty())
	// {
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	// WHERE_CLAUSE.append(" u.loginId='" + loginId + "' ");
	// }
	//
	// if (pageSize == 0)
	// pageSize = MIN_RESULT_PER_PAGE;
	//
	// if (WHERE_CLAUSE.indexOf(".") > 0)
	// WHERE_CLAUSE.append(" and");
	//
	// WHERE_CLAUSE.append(STATUS_CLAUSE);
	// WHERE_CLAUSE.append(LIMIT_CLAUSE);
	//
	// /*
	// * if(reportToUserId > 0 || groupId > 0 || cityId > 0 ||
	// * branchId > 0 || areaId > 0 || subAreaId > 0)
	// * queryStr.append(WHERE_CLAUSE);
	// *
	// * else
	// */
	//
	// queryStr.append(WHERE_CLAUSE);
	//
	// log.info(queryStr.toString());
	// }
	//
	// else if (AuthUtils.isSalesUser())
	// {
	//
	// roleCode = "ROLE_SALES";
	//
	// if (AuthUtils.isBranchManager())
	// {
	//
	// usrRole.append(" and ug.userGroupCode <> '"
	// + UserGroupCodes.SALES_CM + "'");
	// }
	//
	// if (AuthUtils.isAreaManager())
	// {
	//
	// usrRole.append(" and ug.userGroupCode not in ('"
	// + UserGroupCodes.SALES_CM + "','"
	// + UserGroupCodes.SALES_BM + "')");
	// }
	//
	// }
	//
	// else if (AuthUtils.isNIUser())
	// {
	//
	// roleCode = "ROLE_NI";
	//
	// if (AuthUtils.isBranchManager())
	// {
	//
	// usrRole.append(" and ug.userGroupCode <> '"
	// + UserGroupCodes.NI_CM + "'");
	// }
	//
	// if (AuthUtils.isAreaManager())
	// {
	//
	// usrRole.append(" and ug.userGroupCode not in ('"
	// + UserGroupCodes.NI_CM + "','"
	// + UserGroupCodes.NI_BM + "')");
	// }
	// }
	//
	// else if (AuthUtils.isNEUser())
	// {
	//
	// roleCode = "ROLE_NE";
	//
	// }
	//
	// else if (AuthUtils.isFRUser())
	// {
	//
	// roleCode = "ROLE_FR";
	//
	// }
	//
	// if (!AuthUtils.isAdmin())
	// {
	//
	// queryStr.append(USER_GROUP);
	//
	// if (firstName != null && firstName != "")
	// {
	// name.append(" and u.firstName like '" + "%" + firstName
	// + "%" + "'");
	// }
	//
	// if (areaId > 0)
	//
	// {
	// queryStr.append(JOIN_AREA + " and uam.areaId="
	// + filter.getAreaId());
	//
	// }
	//
	// if (branchId > 0)
	//
	// {
	// queryStr.append(JOIN_BRANCH + " and ubm.idBranch="
	// + filter.getBranchId());
	//
	// }
	//
	// if (cityId > 0)
	// {
	// queryStr.append(JOIN_CITY + " and ucm.idCity="
	// + filter.getCityId());
	//
	// } else
	// {
	// queryStr.append(JOIN_CITY + " and ucm.idCity="
	// + AuthUtils.getCurrentUserCity().getId());
	//
	// }
	// queryStr.append(" where r.roleCode= '" + roleCode + "' "
	// + usrRole + " " + name);
	//
	// if (loginId != null && !loginId.isEmpty())
	// {
	// queryStr.append(" and u.loginId='" + loginId + "' ");
	// }
	//
	// queryStr.append(LIMIT_CLAUSE);
	// log.info(queryStr.toString());
	// }
	//
	// Query query = session.createSQLQuery(queryStr.toString())
	// .addEntity(User.class);
	// List<User> users = query.list();
	//
	// List<UserVo> userVos = xformToVoList(users);
	//
	// return CommonUtil.createPagedResultSet(userVos);
	// /*
	// * List<UserVo> userVos = new ArrayList<>(); if(users == null)
	// * return userVos; UserVo vo = null; for(User user: users) { vo =
	// * new UserVo(); BeanUtils.copyProperties(user, vo);
	// * userVos.add(vo); }
	// *
	// * return userVos;
	// */
	//
	// /*
	// * Criteria criteria = hibernateTemplate.getSessionFactory()
	// * .getCurrentSession().createCriteria(User.class,"user");
	// *
	// * if (filter != null) {
	// *
	// * if (filter.getUserGroupId() != null && filter.getUserGroupId() >
	// * 0) {
	// *
	// * criteria.add(Restrictions.eq("id", filter.getUserGroupId()));
	// *
	// * }
	// *
	// * if (filter.getPageSize() > 0 || filter.getPageSize() > 0) { int
	// * pageIndex = filter.getStartOffset(); int pageSize =
	// * filter.getPageSize();
	// *
	// * if (pageIndex == 0 || pageSize == 0) { pageIndex = 1; pageSize =
	// * 5; } criteria.setFirstResult(pageIndex);
	// * criteria.setMaxResults(pageSize); }
	// *
	// * if (filter.getAreaId() != null && filter.getAreaId() > 0) {
	// * log.debug(filter.getAreaId());
	// *
	// * Area area = new Area(); area.setId(filter.getAreaId());
	// * criteria.add(Restrictions.eq("area", area));
	// *
	// * } }
	// *
	// * return xformToVoList((List<User>)criteria.list(), filter);
	// */
	// } catch (HibernateException e)
	// {
	// log.error("Error While executing Query :" + e.getMessage());
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } finally
	// {
	// if (session != null && session.isOpen())
	// session.close();
	// }
	// return null;
	// }

	private List<UserVo> xformToVoListModified(List<UserVoModified> users)
	{
		List<UserVo> userVos = new ArrayList<>();

		if (users == null)
			return userVos;
		
		log.debug("user list size:::: " + users.size());
		if (users != null && !users.isEmpty())
		{

			int size = users.size();

			log.info("Total User to cache are " + size);

			for (Iterator<UserVoModified> iterator = users.iterator(); iterator
					.hasNext();)
			{
				UserVoModified user = (UserVoModified) iterator.next();

				try
				{

					UserVo userVo = new UserVo();

					userVo.setId(Long.valueOf(user.getUserId()));
					userVo.setEmpId(Long.valueOf(user.getEmpId()));
					userVo.setLoginId(user.getLoginId());
					userVo.setPassword(user.getPassword());
					userVo.setFirstName(user.getFirstName());
					userVo.setLastName(user.getLastName());
					userVo.setEmailId(user.getEmailId());
					userVo.setMobileNo(user.getMobileNo() != null
							? Long.valueOf(user.getMobileNo()) : null);

					// BeanUtils.copyProperties(user, userVo);

					if (user.getEnabled() != null
							&& Integer.valueOf(user.getEnabled()) == 1)
						userVo.setEnabled("enabled");

					else
						userVo.setEnabled("disabled");

					Set<TypeAheadVo> cityAheadVos = userCache
							.getUserCityByLoginId(userVo.getLoginId());

					TypeAheadVo city = cityAheadVos != null
							? cityAheadVos.iterator().next() : null;

					userVo.setCity(city);

					Set<TypeAheadVo> branchAheadVos = userCache
							.getUserBranchByLoginId(userVo.getLoginId());

					TypeAheadVo branch = branchAheadVos != null
							? branchAheadVos.iterator().next() : null;

					userVo.setBranch(branch);

					userVo.setArea(userCache
							.getUserAreaByLoginId(userVo.getLoginId()));

					Set<TypeAheadVo> tabletAheadVos = userCache
							.getUserTabletByLoginId(userVo.getLoginId());

					TypeAheadVo tablet = tabletAheadVos != null
							? tabletAheadVos.iterator().next() : null;

					userVo.setTablet(tablet);

					userVo.setDevices(userCache
							.getUserDeviceByLoginId(userVo.getLoginId()));

					log.debug("User " + user.getLoginId()
							+ " is associated with devices "
							+ userVo.getDevices());

					Set<TypeAheadVo> groupAheadVos = userCache
							.getUserUserGroupByLoginId(userVo.getLoginId());
					userVo.setUserGroups(groupAheadVos);
					log.debug("groupAheadVos " + groupAheadVos);
					Set<TypeAheadVo> roleAheadVos = new HashSet<>();
					if (groupAheadVos != null && !groupAheadVos.isEmpty())
					{
						for (Iterator<TypeAheadVo> iterator2 = groupAheadVos
								.iterator(); iterator2.hasNext();)
						{
							TypeAheadVo typeAheadVo = (TypeAheadVo) iterator2
									.next();

							roleAheadVos.addAll(userGroupDAO
									.getuseGroupAndRolesTAByGroupId(typeAheadVo
											.getId()));

						}

						userVo.setUserRoles(roleAheadVos);
					}

					userVo.setReportTo(new TypeAheadVo(Long.valueOf(user
							.getReportToId()), user.getReportToLoginId()));
					log.debug("User ############################# " + userVo);
					// System.exit(0);
					addToLocalCache(userVo);
					userVos.add(userVo);

					log.info("Remaing User to cache are " + size--);

				} catch (Exception e)
				{
					log.error("Error while caching " + user.getLoginId() + " "
							+ e.getMessage());
					e.printStackTrace();
					continue;
				}

			}

		}
		log.debug(" exit from xformToVoListModified   "+ users.size());
		return userVos;
	}

	private List<UserVo> xformToVoList(List<User> users)
	{
		List<UserVo> userVos = new ArrayList<>();

		if (users == null)
			return userVos;

		log.info("user list size:::: " + users.size());

		for (User user : users)
		{
			UserVo userVo = new UserVo();

			BeanUtils.copyProperties(user, userVo);

			if (user.getStatus() == 1)
				userVo.setEnabled("enabled");

			else
				userVo.setEnabled("disabled");

			Set<City> userCitySet = user.getCities();

			if (!(userCitySet.isEmpty()))
			{

				for (City city : userCitySet)
				{

					userVo.setCity(new TypeAheadVo(city.getId(), city
							.getCityName()));
				}

			}

			Set<Branch> userBranchSet = user.getBranches();

			if (!(userBranchSet.isEmpty()))
			{

				for (Branch branch : userBranchSet)
				{

					userVo.setBranch(new TypeAheadVo(branch.getId(), branch
							.getBranchName()));
				}

			}
			Set<Area> userAreaSet = user.getAreas();

			if (!(userAreaSet.isEmpty()))
			{
				Set<TypeAheadVo> typeAheadVo = new HashSet<TypeAheadVo>();
				for (Area area : userAreaSet)
				{

					typeAheadVo.add(new TypeAheadVo(area.getId(), area
							.getAreaName()));

				}
				userVo.setArea(typeAheadVo);
			}

			Set<SubArea> usersubAreaSet = user.getSubAreas();

			if (usersubAreaSet != null)
			{

				Set<TypeAheadVo> aheadVos = new HashSet<>();

				for (SubArea subArea : usersubAreaSet)
				{

					TypeAheadVo aheadVo = new TypeAheadVo();
					aheadVo.setId(subArea.getId());
					aheadVo.setName(subArea.getSubAreaName());
					aheadVos.add(aheadVo);

				}
				userVo.setSubAreas(aheadVos);
			}

			Set<Tablet> userTabs = user.getTabletsForAssignedTo();

			if (userTabs != null)
			{

				for (Tablet tablet : userTabs)
				{

					if (tablet != null)
					{
						userVo.setTablet(new TypeAheadVo(tablet.getId(), tablet
								.getMacAddress()));
					}
				}
			}

			Set<Device> userDeviceSet = user.getDevices();

			if (userDeviceSet != null)
			{

				Set<TypeAheadVo> aheadVos = new HashSet<>();

				for (Device device : userDeviceSet)
				{

					TypeAheadVo aheadVo = new TypeAheadVo();
					aheadVo.setId(device.getId());
					aheadVo.setName(device.getDeviceName());
					aheadVos.add(aheadVo);

				}
				userVo.setDevices(aheadVos);

				log.debug("User " + user.getId()
						+ " is associated with devices " + userVo.getDevices());
			}

			Set<Skill> userSkillSet = user.getSkills();

			if (userSkillSet != null)
			{
				Set<TypeAheadVo> aheadVos = new HashSet<>();

				for (Skill skill : userSkillSet)
				{

					aheadVos.add(new TypeAheadVo(skill.getId(), skill
							.getSkillName()));

				}

				userVo.setSkills(aheadVos);

			}

			Set<UserGroup> userGroupUserSet = user.getUserGroups();

			if (userGroupUserSet != null)

			{

				Set<TypeAheadVo> groupAheadVos = new HashSet<>();

				Set<TypeAheadVo> roleAheadVos = new HashSet<>();

				for (UserGroup userGroup : userGroupUserSet)
				{

					TypeAheadVo groupTypeAheadVo = new TypeAheadVo(userGroup
							.getId(), userGroup.getUserGroupName());
					groupAheadVos.add(groupTypeAheadVo);

					Set<Role> roles = userGroup.getRoles();

					for (Role role : roles)
					{
						TypeAheadVo roleTypeAheadVo = new TypeAheadVo(role
								.getId(), role.getRoleCode());
						roleAheadVos.add(roleTypeAheadVo);
					}
				}
				userVo.setUserRoles(roleAheadVos);
				userVo.setUserGroups(groupAheadVos);
			}

			Vendor vendor = user.getVendor();

			if (vendor != null)
				userVo.setEmployer(new TypeAheadVo(vendor.getId(), vendor
						.getName()));

			User reportToUser = user.getUser();

			if (reportToUser != null)
				userVo.setReportTo(new TypeAheadVo(reportToUser
						.getId(), reportToUser.getLoginId()));

			addToLocalCache(userVo);
			userVos.add(userVo);
		}
		log.debug(" exit from xformToVoList   " +users.size());
		return userVos;
	}

	@Override
	public Map<Long, String> getAssociatedUsers(long userId, Long userGroupId)
	{
		List<String> roles = null;
		if (userGroupId != null)
			roles = userGroupDAO.getRolesByUserGroupId(userGroupId);

		Map<Long, String> users = new HashMap<Long, String>();
		log.debug(" inside getAssociatedUsers   " +userId);
		
		List<Long> userIds = userAndReportingMapping.get(userId);
		if (userIds != null)
		{
			for (Long id : userIds)
			{
				UserVo vo = cachedUsersById.get(id);

				if (vo != null && userGroupId != null
						&& isUserContainsAllRole(vo, roles))
					users.put(vo.getId(), GenericUtil
							.buildUserSelectOption(vo));

				else if (vo != null)
				{
					users.put(vo.getId(), GenericUtil
							.buildUserSelectOption(vo));
				}
			}
			return users;
		}

		List<User> associatedUsers = (List<User>) hibernateTemplate
				.find("from User where user.id = ?", userId);
		if (associatedUsers == null)
			return new HashMap<Long, String>();

		for (User user : associatedUsers)
		{
			UserVo vo = cachedUsersById.get(user.getId());
			users.put(vo.getId(), GenericUtil.buildUserSelectOption(vo));
		}
		log.debug(" exit from getAssociatedUsers   " +userId);

		return users;
	}

	private boolean isUserContainsAnyRole(UserVo vo, List<String> roles)
	{
		if (vo == null || roles == null)
			return false;

		log.debug(" inside isUserContainsAnyRole   "+ roles.size());
		List<String> userRoles = new ArrayList<String>();

		if (vo.getUserRoles() != null)
		{
			for (TypeAheadVo tvo : vo.getUserRoles())
			{
				userRoles.add(tvo.getName());
			}
		}
		userRoles.retainAll(roles);
		log.debug(" exit from isUserContainsAnyRole   "+ roles.size());
		return !userRoles.isEmpty();

	}
	private boolean isUserContainsStrictRole(UserVo vo, List<String> roles)
	{
		if (vo == null || roles == null)
			return false;

		List<String> userRoles = new ArrayList<String>();

		if (vo.getUserRoles() != null)
		{
			for (TypeAheadVo tvo : vo.getUserRoles())
			{
				userRoles.add(tvo.getName());
			}
		}

		if (userRoles.contains(UserRole.ROLE_NI) && roles.containsAll(AuthUtils.NE_NI_DEF_STRICT))
		{
			log.debug("CAll is for strict user check for user Id " + vo.getId());
			if (!userRoles.contains(UserRole.ROLE_NE_COPPER))
				userRoles.add(UserRole.ROLE_NE_COPPER);
			
			if (!userRoles.contains(UserRole.ROLE_NE_FIBER))
				userRoles.add(UserRole.ROLE_NE_FIBER);
		}
 
		Boolean result = userRoles.containsAll(roles);
		
		log.debug("Returing roles for id " + vo + ", and result is " + result + " and roles" + roles.size());

		return result;
	}

	private boolean isUserContainsAllRole(UserVo vo, List<String> roles)
	{
		if (vo == null || roles == null)
			return false;
		log.debug(" inside isUserContainsAllRole   " +roles.size());
		List<String> userRoles = new ArrayList<String>();

		if (vo.getUserRoles() != null)
		{
			for (TypeAheadVo tvo : vo.getUserRoles())
			{

				userRoles.add(tvo.getName());
			}
		}
		log.debug(" exit from isUserContainsAllRole   " +roles.size());
		return roles.containsAll(userRoles) && roles.size() == userRoles.size();

	}

	private boolean isUserContainsGroup(UserVo vo, long groupId)
	{
		if (vo == null || groupId <= 0)
			return false;
		log.debug(" inside isUserContainsGroup   " +groupId);

		log.debug("$$$$$$$$$$$$ ^^^^^^^^^^ " + vo);

		List<String> userRoles = new ArrayList<String>();
		if (vo.getUserGroups() != null)
		{
			for (TypeAheadVo tvo : vo.getUserGroups())
			{
				if (tvo.getId() == groupId)
					return true;
				userRoles.add(tvo.getName());
			}
		}
		log.debug(" exit from isUserContainsGroup   " +groupId);
		return false;

	}

	@Override
	public Map<Long, String> getReportingUsers(long userId)
	{
		log.debug(" inside getReportingUsers   " +userId );
		Map<Long, String> users = new HashMap<Long, String>();
		List<Long> userIds = userAndReportingMapping.get(userId);
		if (userIds != null)
		{
			for (Long id : userIds)
			{
				UserVo vo = cachedUsersById.get(id);
				if (vo != null)
				{
					users.put(vo.getId(), vo.getFirstName());
				}
			}
			return users;
		}

		List<User> associatedUsers = (List<User>) hibernateTemplate
				.find("from User where user.id = ?", userId);
		if (associatedUsers == null)
			return new HashMap<Long, String>();

		for (User user : associatedUsers)
		{
			UserVo vo = cachedUsersById.get(user.getId());
			users.put(vo.getId(), vo.getFirstName());
		}
		log.debug(" exit from getReportingUsers   " +userId );
		return users;
	}

	// @Override
	// public UserVo getUsersByDeviceMac(String macAddress, String role)
	// {
	// if (cachedUsersById.size() == 0)
	// resetCachedUsers();
	//
	// if (macAddress != null)
	// macAddress = macAddress.trim().toUpperCase();
	// List<UserVo> userVos = new ArrayList<UserVo>(cachedUsersById.values());
	//
	// for (UserVo userVo : userVos)
	// {
	//
	// Set<TypeAheadVo> userDevices = userVo.getDevices();
	//
	// if (!(userDevices.isEmpty()))
	// {
	//
	// for (TypeAheadVo typeAheadVo : userDevices)
	// {
	//
	// if(typeAheadVo.getName() == null)
	// continue;
	//
	// if (typeAheadVo.getName().equalsIgnoreCase(macAddress))
	// {
	//
	// Set<TypeAheadVo> userRoles = userVo.getUserRoles();
	//
	// if (!(userRoles.isEmpty()))
	// {
	//
	// for (TypeAheadVo typeAheadVo2 : userRoles)
	// {
	// if (typeAheadVo2.getName()
	// .equalsIgnoreCase(role))
	// {
	//
	// return userVo;
	// }
	// }
	// }
	// }
	// }
	// }
	// }
	// return null;
	// }

	@Override
	public UserVo getUsersByDeviceName(String deviceName, String role)
	{
		log.debug(" inside getUsersByDeviceName   " +deviceName);
		Query query = hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery(GET_USER_BY_DEVICENAME).addEntity(User.class);
		query.setParameterList("roleList", Collections.singleton(role));
		query.setParameter("deviceName", deviceName);
		List<User> result = query.list();

		if (result != null && result.size() > 0)
		{
			User user = result.get(0);
			UserVo vo = new UserVo();
			BeanUtils.copyProperties(user, vo);
			return vo;
		}
		log.debug(" exit from getUsersByDeviceName   " +deviceName);
		return null;
	}

	@Override
	public UserVo getDefaultSalesUserForArea(Long areaId, Long branchId,
			Long cityId, String role)
	{
		log.debug(" inside getDefaultSalesUserForArea   "+ areaId);
		Query query = hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery(GET_DEFAULT_USER_BY_AREA);
		query.setParameter("areaId", areaId);
		query.setParameter("branchId", branchId);
		query.setParameter("cityId", cityId);

		List<Object> result = query.list();

		if (result != null && result.size() > 0)
		{
			Long userId = result.get(0) != null
					? Long.valueOf(((BigInteger) result.get(0)).toString())
					: null;

			UserVo vo = cachedUsersById.get(userId);
			return vo;
		}
		return null;
	}
	@Override
	public UserVo getDefaultSalesUserForSubArea(Long subAreaId ,Long areaId, Long branchId,
			Long cityId, String role)
	{
		log.debug(" inside getDefaultSalesUserForArea   "+ areaId);
		Query query = hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery(GET_DEFAULT_USER_BY_SUB_AREA);
		query.setParameter("subAreaId", subAreaId);
		query.setParameter("areaId", areaId);
		query.setParameter("branchId", branchId);
		query.setParameter("cityId", cityId);

		List<Object> result = query.list();

		if (result != null && result.size() > 0)
		{
			Long userId = result.get(0) != null
					? Long.valueOf(((BigInteger) result.get(0)).toString())
					: null;

			UserVo vo = cachedUsersById.get(userId);
			return vo;
		}
		return null;
	}

	@Override
	public void addDefaultSalesUserForAreas(
			List<DefaultUserAreaMapping> mappings)
	{
		log.info("Updating DefaultUserAreaMapping with data " + mappings);

		List<Long> areaList = new ArrayList<Long>();
		for (Iterator iterator = mappings.iterator(); iterator.hasNext();)
		{
			DefaultUserAreaMapping defaultUserAreaMapping = (DefaultUserAreaMapping) iterator
					.next();
			areaList.add(defaultUserAreaMapping.getAreaId());
		}

		if (areaList.size() > 0)
		{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(" Delete from DefaultUserAreaMapping where id >  0 and areaId in (:areaList) ");
			query.setParameterList("areaList", areaList);
			int row = query.executeUpdate();
			log.info("Deleted previous mapping " + row);
		}

		for (Iterator iterator = mappings.iterator(); iterator.hasNext();)
		{
			DefaultUserAreaMapping defaultUserAreaMapping = (DefaultUserAreaMapping) iterator
					.next();
			defaultUserAreaMapping
					.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			log.info(" defaultUserAreaMapping " + defaultUserAreaMapping);
			hibernateTemplate.save(defaultUserAreaMapping);

		}
	}

	@Override
	public UserVo getUsersByDeviceName(String deviceName, List<String> roles)
	{

		if (deviceName == null || roles == null || roles.isEmpty())
			return null;
		log.debug("query.getQueryString() " + " roles are " + roles
				+ " device name " + deviceName);

		if (userAndDeviceMapping.containsKey(deviceName))
		{
			for (Long longId : userAndDeviceMapping.get(deviceName))
			{

				UserVo vo = cachedUsersById.get(longId);
				
				log.debug("Fetching User from cachedUsersById :: device name :" + deviceName + ", and roles " + roles
						+ " deetails of user is" + vo);

				if(isUserContainsStrictRole(vo, roles))
					return vo;
//				else if (isUserContainsAnyRole(vo, roles))
//					return vo;
				
			}
			log.debug("After fail Fetching User from cachedUsersById :: device name :"+deviceName + ", mapping Ids :"+userAndDeviceMapping.get(deviceName));
			log.debug("userAndDeviceMapping : "+ userAndDeviceMapping + "\n cachedUsersById : "+ cachedUsersById);
			return null;
		} else {
			log.info("Device Name :"+ deviceName + " is not present in userAndDeviceMapping");
			return null;
		}

		/*
		 * 
		 * Query query =
		 * hibernateTemplate.getSessionFactory().getCurrentSession()
		 * .createSQLQuery(GET_USER_BY_DEVICENAME).addEntity(User.class);
		 * query.setParameterList("roleList", roles);
		 * query.setParameter("deviceName", deviceName);
		 * 
		 * log.debug("query.getQueryString() " + query.getQueryString() +
		 * " roles are " + roles + " device name " + deviceName);
		 * 
		 * List<User> result = query.list();
		 * 
		 * if (result != null && result.size() > 0) { for (Iterator iterator1 =
		 * result.iterator(); iterator1.hasNext();) { User user = (User)
		 * iterator1.next(); Set<TypeAheadVo> vo =
		 * getUserById(user.getId()).getUserRoles();
		 * 
		 * if (vo == null) continue; Set<String> allRoles = new
		 * HashSet<String>();
		 * 
		 * for (Iterator iterator = vo.iterator(); iterator.hasNext();) {
		 * TypeAheadVo typeAheadVo = (TypeAheadVo) iterator.next();
		 * allRoles.add(typeAheadVo.getName());
		 * 
		 * } if (roles.containsAll(allRoles)) { UserVo userVo = new UserVo();
		 * BeanUtils.copyProperties(user, userVo); return userVo; }
		 * 
		 * } } return null;
		 */
	} /*
		 * @Override public UserVo getUsersByDeviceName(String deviceName,
		 * String role) { System.out.println("deviceName "+deviceName +" role "
		 * +role); if (cachedUsersById.size() == 0) resetCachedUsers();
		 * 
		 * if (deviceName != null) deviceName = deviceName.trim().toUpperCase();
		 * List<UserVo> userVos = new
		 * ArrayList<UserVo>(cachedUsersById.values());
		 * 
		 * for (UserVo userVo : userVos) {
		 * 
		 * Set<TypeAheadVo> userDevices = userVo.getDevices();
		 * 
		 * if (!(userDevices.isEmpty())) {
		 * 
		 * for (TypeAheadVo typeAheadVo : userDevices) { if
		 * (deviceDao.getDeviceById(
		 * typeAheadVo.getId()).getDeviceName().equalsIgnoreCase(deviceName)) {
		 * 
		 * Set<TypeAheadVo> userRoles = userVo.getUserRoles();
		 * 
		 * log.info("######### User Roles " + userRoles);
		 * 
		 * if (!(userRoles.isEmpty())) {
		 * 
		 * for (TypeAheadVo typeAheadVo2 : userRoles) { if
		 * (typeAheadVo2.getName() .equalsIgnoreCase(role)) {
		 * 
		 * return userVo; } } } } } } } return null; }
		 */

	@Override
	public List<Long> getUsersForAreasWithRole(List<Long> areaIds,
			String sourceRole)
	{
		return null;
	}

	@Override
	public List<UserVo> getUsersAssosiatedToDevice(Long userId,
			String sourceRole, String destinationRole)
	{
		log.debug(" inside getUsersAssosiatedToDevice   "+ userId);
		if (cachedUsersById.size() == 0)
		{
			// resetCachedUsers();
			modifiedResetUser();
		}
		// List<UserVo> userVos = new
		// ArrayList<UserVo>(cachedUsersById.values());

		List<UserVo> userAssosiation = new ArrayList<>();
		// List<String> deviceNames = new ArrayList<>();
		//
		if (cachedUsersById.containsKey(userId))
		{
			UserVo userVo = cachedUsersById.get(userId);
			Set<TypeAheadVo> userDevices = userVo.getDevices();

			if (!(userDevices.isEmpty()))
			{

				for (TypeAheadVo typeAheadVo : userDevices)
				{

					Set<TypeAheadVo> userRoles = userVo.getUserRoles();

					if (userRoles == null)
						continue;
					if (!(userRoles.isEmpty()))
					{

						for (TypeAheadVo typeAheadVo2 : userRoles)
						{
							if (typeAheadVo2.getName()
									.equalsIgnoreCase(sourceRole))
							{
								// deviceNames.add(typeAheadVo.getName());

								UserVo vo = getUsersByDeviceName(typeAheadVo
										.getName(), destinationRole);

								if (vo != null)
									userAssosiation.add(vo);
							}
						}
					}

				}

				// for (String devcieName : deviceNames)
				// {
				//
				//
				// }

				// log.info("Device assosiated with user " + userId + " are "
				// + deviceNames);

			} else
			{
				log.info("No device Assosiated with user " + userId);
			}

			log.info("Total  assosiated user with " + userId + " are "
					+ userAssosiation);

			return userAssosiation;

		} else
		{
			return userAssosiation;
		}
	}

	@Override
	public long getReportToUser(Long userId)
	{
		log.debug(" inside getReportToUser   " +userId);
		Long reportToId = userAndReportToMapping.get(userId);

		if (reportToId != null)
		{
			return reportToId;
		}
		UserVo userVo = getUserById(userId);

		if (userVo != null && userVo.getReportTo() != null)
		{
			userAndReportToMapping.put(userId, userVo.getReportTo().getId());
			userVo.getReportTo().getId();
		}
		log.debug(" exit from getReportToUser   " +userId);
		return 0;

	}

	@Override
	public UserVo getUserByEmpId(long empId)
	{
		log.debug(" inside getUserByEmpId   "+ empId);
		if (empIdAndUserMapping.size() == 0)
		{
			// resetCachedUsers();
			modifiedResetUser();
		}
		if (empIdAndUserMapping.containsKey(empId)
				&& cachedUsersById.containsKey(empIdAndUserMapping.get(empId)))
			return cachedUsersById.get(empIdAndUserMapping.get(empId));
		else
			return null;
	}

	@Override
	public long getEmpIdByUserId(long userId)
	{
		log.debug(" inside getEmpIdByUserId   " +userId);
		if (userIdAndEmpIdMapping.size() == 0)
		{
			// resetCachedUsers();
			modifiedResetUser();
		}
		if (userIdAndEmpIdMapping.containsKey(userId))
			return userIdAndEmpIdMapping.get(userId);

		else
			return 0;
	}

	@Override
	public String getKeyForQueueByUserId(Long userId)
	{

		return null;
	}

	@Override
	public Long getUserIdByDeviceName(String name)
	{
		log.debug(" inside getUserIdByDeviceName   " +name);
		if (deviceNameUserIdCache.containsKey(name))
		{

			return deviceNameUserIdCache.get(name);

		} else

			return null;
	}

	@Override
	public Long getUserIdByDevcieName(String devcieName)
	{
		log.debug(" inside getUserIdByDevcieName   "+ devcieName);
		if (deviceNameUserIdCache.containsKey(devcieName))
		{

			return deviceNameUserIdCache.get(devcieName);

		} else

			return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public TypeAheadVo getDeviceDetailsByMacAddr(String fxName)
	{
		log.debug(" inside getDeviceDetailsByMacAddr   " +fxName);

		TypeAheadVo typeAheadVo = null;
		try
		{
			String stringQuery = "select id,deviceName,status from  Device where deviceName=:fxName";
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(stringQuery);
			query.setString("fxName", fxName);
			List<Object[]> status = query.list();
			for (Object[] report : status)
			{
				typeAheadVo = new TypeAheadVo();
				typeAheadVo.setId(Long.valueOf(report[0].toString()));
				typeAheadVo.setName((report[1].toString()));
				typeAheadVo.setStatus(Integer.valueOf(report[2].toString()));

			}

		} catch (Exception e)
		{
			log.error("stringQuery" + e);
		}
		return typeAheadVo;

	}

	@Override
	public String reAssignedDeviceToUser(long userId, String fxName)
	{
		log.debug(" inside reAssignedDeviceToUser   "+ fxName);
		String roleName = null;
		UserVo userVo = null;

		Long deviceId = null;
		Integer result = null;
		UserVo vo = getUserById(userId);
		Set<TypeAheadVo> roles = vo.getUserRoles();

		for (Iterator<TypeAheadVo> iterator = roles.iterator(); iterator
				.hasNext();)
		{
			TypeAheadVo typeAheadVo = iterator.next();
			if (typeAheadVo.getName().equalsIgnoreCase("ROLE_SALES"))
			{
				roleName = "ROLE_SALES";
				break;
			} else if (typeAheadVo.getName().equalsIgnoreCase("ROLE_NE"))
			{
				roleName = "ROLE_NE";
			}

		}

		if (roleName != null)
		{
			userVo = getUsersByDeviceName(fxName, roleName);

			if (userVo == null)
			{
				TypeAheadVo typeAheadVo = getDeviceDetailsByMacAddr(fxName);

				try
				{
					String stringQuery = "INSERT INTO UserDeviceMapping(userId,deviceId) "
							+ "VALUES(" + userId + "," + typeAheadVo.getId()
							+ ")";

					log.info("stringQuery:" + stringQuery);

					Query query = hibernateTemplate.getSessionFactory()
							.getCurrentSession().createSQLQuery(stringQuery);

					result = query.executeUpdate();

					if (result > 0)
					{
						log.info("failed  to insert in UserDeviceMapping as SqlQuery"
								+ stringQuery);
						return StatusMessages.SAVED_SUCCESSFULLY;

					} else
					{
						log.info("failed  to insert in UserDeviceMapping as SqlQuery"
								+ stringQuery);
						return StatusMessages.SAVED_FAILED;
					}

				} catch (Exception e)
				{
					log.error("stringQuery:" + e);
					return StatusMessages.DB_EXCEPTION;
				}
			} else
			{
				log.info("device assoiciated with othersUser as userId"
						+ userVo.getId());
				return StatusMessages.DEVICE_ASSOCIATED_WITH_OTHER_USER;
			}

		} else
		{
			log.info("not found role as NE OR  Sales as userId:" + userId);
			return StatusMessages.FAILURE;
		}

	}

	@Override
	public List<UserVo> getAssociatedUsersByUserId(Long userId,
			List<String> roles)
	{
		log.info(" inside getAssociatedUsersByUserId   "+userId );
		
		List<UserVo> result = jdbcTemplate.query(GET_ASSOCIATED_USERS_BY_USER_ID,new Object[] {
			userId ,userId,	StringUtils.join(roles, ',')
		}, new UserMapper());
		
		log.info("getAssociatedUsersByUserId  " 
				+ " userId " + userId + " roles " + roles);

		if (result != null && result.size() > 0)
						return result;

		log.info("Returing users for " + userId + " in roles " + roles);

		return new ArrayList<UserVo>();
	}

	@Override
	public List<UserVo> getReportee(Long userId)
	{
		log.debug(" inside getReportee   " +userId);
		Query query = hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery(GET_REPORTEE).addEntity(User.class);

		query.setParameter("userId", userId);

		log.debug("getReportee query " + query.getQueryString() + " userId "
				+ userId);

		List<User> result = query.list();

		log.debug("Results getReportee are " + result);

		if (result != null && result.size() > 0)
		{
			List<UserVo> userVoList = new ArrayList<UserVo>();

			for (Iterator<User> iterator = result.iterator(); iterator
					.hasNext();)
			{
				User user = (User) iterator.next();
				UserVo vo = new UserVo();
				BeanUtils.copyProperties(user, vo);

				userVoList.add(vo);
			}

			log.debug("Users result is not null and Returing Reportee for "
					+ userId + "are " + userVoList.size());

			return userVoList;
		}

		log.debug("Returing getReportee for " + userId);

		return new ArrayList<UserVo>();
	}

	@Override
	public List<String> getAllRolesByUserId(long userId)
	{
		log.debug(" inside getAllRolesByUserId   " +userId);

		if (cachedUsersById.size() == 0)
		{
			// resetCachedUsers();
			modifiedResetUser();
		}

		List<String> roles = new ArrayList<>();
		if (cachedUsersById.containsKey(userId))
		{
			UserVo userVo = cachedUsersById.get(userId);
			Set<TypeAheadVo> userRoles = userVo.getUserRoles();
			if( userRoles == null && "enabled".equalsIgnoreCase(userVo.getEnabled()))
			{
				userRoles =  getUserRolesFromDb( userId );
				userVo.setUserRoles(userRoles);
			}
			else if ( userRoles == null && "disabled".equalsIgnoreCase(userVo.getEnabled()))
				return roles;
				
			for (TypeAheadVo role : userRoles)
				roles.add(role.getName());
		}
		return roles;
	}

	private Set<TypeAheadVo> getUserRolesFromDb( Long userId )
	{
		Set<TypeAheadVo> userRoles = new HashSet<>();
		if( userId == null || userId.longValue() <= 0 )
			return userRoles;
		
		try
		{
			final String ROLE_QUERY = "select r.id ,r.roleCode from User u join UserGroupUserMapping ugum "
					+ " on ugum.idUser = u.id join UserGroup ug on ugum.idUserGroup = ug.id "
					+ " join UserGroupRoleMapping ugrm on ugrm.idUserGroup = ug.id join Role r on r.id = ugrm.idRole "
					+ " where u.id = :userId ";
			
			Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery(ROLE_QUERY)
					.setParameter("userId", userId);
			
			List<Object[]> roles = query.list();
			
			if( roles != null && !roles.isEmpty() )
			{
				TypeAheadVo oneRole = null;
				for( Object[] role : roles )
				{
					oneRole = new TypeAheadVo();
					oneRole.setId(FrTicketUtil.objectToLong(role[0]));
					oneRole.setName(FrTicketUtil.objectToString(role[1]));
					userRoles.add(oneRole);
				}
			}
			
		}catch (Exception e) {
			log.error("Error occured while getching roles from db for user : "+userId,e);
		}
		return userRoles;
	}

	@Override
	public List<TypeAheadVo> getAllFrUserNames()
	{
		log.debug(" inside getAllFrUserNames   " );
		if (cachedUsersById.size() == 0)
		{
			// resetCachedUsers();
			modifiedResetUser();
		}

		Collection<UserVo> userVo = cachedUsersById.values();
		TypeAheadVo user = null;
		List<TypeAheadVo> frNeNames = new ArrayList<TypeAheadVo>();
		if (userVo != null)
		{
			for (UserVo vo : userVo)
			{
				if (AuthUtils.isFrCxDownNEUser(vo.getId())
						|| AuthUtils.isFrCxUpNEUser(vo.getId()))
				{
					user = new TypeAheadVo();
					user.setId(vo.getId());
					user.setName(vo.getFirstName());
					frNeNames.add(user);
				}
			}
		}
		return frNeNames;
	}

	public UserVo getUserByLoginId(String loginId)
	{
		log.debug(" inside getUserByLoginId   " +loginId);
		if (cachedUsersByLoginId.size() == 0)
		{
			// resetCachedUsers();
			modifiedResetUser();
		}
		if (cachedUsersByLoginId.containsKey(loginId.toUpperCase()) && cachedUsersById
				.containsKey(cachedUsersByLoginId.get(loginId.toUpperCase())))
			return cachedUsersById.get(cachedUsersByLoginId.get(loginId.toUpperCase()));

		else
			return null;
	}

	@Override
	public Map<Long, String> getNestedReportingUsers(long userId, Long branchId,
			Long areaId, Long userGroupId)
	{
		log.debug(" inside getNestedReportingUsers   " +userId);
		List<String> roles = null;
		Map<Long, String> users = new HashMap<Long, String>();

		if (userGroupId != null)
		{
			long reportingUser = getReportToUser(AuthUtils.getCurrentUserId());

			roles = userGroupDAO.getRolesByUserGroupId(userGroupId);

			log.debug(reportingUser + " $$$$$$$$$$$$$ TTTTTTTTTTTT     "
					+ roles);
			if (AuthUtils.isAdmin() || AuthUtils.isUserAdmin())
				reportingUser = FWMPConstant.SYSTEM_ENGINE;
			
			UserVo vo = cachedUsersById.get(reportingUser);

			log.debug(" #################3 $$$$$$$$$$$$$ TTTTTTTTTTTT     "
					+ vo);

			if (vo != null && userGroupId != null
					&& isUserContainsAllRole(vo, roles))
				users.put(vo.getId(), GenericUtil.buildUserSelectOption(vo));
		} else
		{

			if (AuthUtils.isNIUser())
			{
				roles = AuthUtils.NI_DEF;
			} else if (AuthUtils.isSalesUser())
			{
				roles = AuthUtils.SALES_DEF;
			} else if (AuthUtils.isFRUser())
			{
				roles = AuthUtils.FR_DEF;
			}

		}

		List<Long> userIds = userAndNestedReportingMapping.get(userId);

		log.debug("Testttttttttttttttttttttt " + userIds);

		if (userIds != null && !userIds.isEmpty())
		{
			for (Long id : userIds)
			{

				log.debug("Testttttttttttttttttttttt id " + id);

				UserVo vo = cachedUsersById.get(id);

				if (vo != null && branchId != null && branchId > 0
						&& vo.getBranch() != null)
				{
					if (!branchId.equals(vo.getBranch().getId()))
						continue;
				}

				if (vo != null && userGroupId != null
						&& isUserContainsAllRole(vo, roles))
					users.put(vo.getId(), GenericUtil
							.buildUserSelectOption(vo));

				else if (vo != null && userGroupId == null
						&& isUserContainsAnyRole(vo, roles))
					users.put(vo.getId(), GenericUtil
							.buildUserSelectOption(vo));

				log.debug("Testttttttttttttttttttttt vo " + vo);

			}

			log.debug("Testttttttttttttttttttttt users " + users);

			return users;
		}

		return users;
	}

	@Override
	public List<UserVo> getCurrentCityTeams(String groupCode, long cityId)
	{
		List<UserVo> users = new ArrayList<UserVo>();
		if (groupCode == null || cityId <= 0)
			return users;

		try
		{

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(GET_CITY_TEAM_USER)
					.addEntity(User.class)
					.setParameter("usrGourpCode", groupCode)
					.setParameter("cityId", cityId);

			List<User> result = query.list();
			if (result != null && result.size() > 0)
			{
				UserVo vo = null;
				for (Iterator<User> iterator = result.iterator(); iterator
						.hasNext();)
				{
					User user = (User) iterator.next();
					vo = new UserVo();
					BeanUtils.copyProperties(user, vo);
					users.add(vo);
				}
			}

		} catch (Exception ex)
		{
			log.error("Error occure while getting current city teams : "
					+ ex.getMessage());
		}
		return users;
	}

	public void mapAllAreaForUser(final List<TypeAheadVo> areaIds,
			final Long userId)
	{
		log.debug(" inside mapAllAreaForUser   "+userId );
		try
		{
			this.jdbcTemplate.update(DELETE_PREVIOUS_MAPPING, new Object[] { userId });
			
			UserVo user = getUserById(userId);
			if (user != null && user.getArea() != null)
			{
				areaIds.removeAll(user.getArea());
			}
			
			jdbcTemplate
					.batchUpdate(USER_AREA_ALLOCATION, new BatchPreparedStatementSetter()
					{

						@Override
						public int getBatchSize()
						{
							return areaIds.size();
						}

						@Override
						public void setValues(java.sql.PreparedStatement ps,
								int arg1) throws SQLException
						{
							long areaId = areaIds.get(arg1).getId();
							ps.setLong(1, userId);
							ps.setLong(2, areaId);

						}
					});

			Set<TypeAheadVo> areas = new HashSet<TypeAheadVo>();
			for (TypeAheadVo area : areaIds)
			{
				areas.add(new TypeAheadVo(area.getId(), LocationCache
						.getAreaNameById(area.getId())));
			}

			if (user != null && user.getArea() != null)
			{
				user.getArea().addAll(areas);
			} else if (user != null)
			{
				user.setArea(areas);
			}
		} catch (Exception e)
		{
			log.error("Error While Associate User to Areas::" + e.getMessage());

		}

	}

	public void mapDefaultAreaForUser(final List<TypeAheadVo> areaIds,
			final Long userId)
	{
		log.debug(" inside mapDefaultAreaForUser   " +userId);
		try
		{
			jdbcTemplate.update(DELETE_PREVIOUS_DEFAULT_AREA_MAPPING, new Object[] { userId });

			jdbcTemplate
					.batchUpdate(USER_DEFAULT_AREA_ALLOCATION, new BatchPreparedStatementSetter()
					{

						@Override
						public int getBatchSize()
						{
							return areaIds.size();
						}

						@Override
						public void setValues(java.sql.PreparedStatement ps,
								int arg1) throws SQLException
						{
							long areaId = areaIds.get(arg1).getId();
							ps.setLong(1, StrictMicroSecondTimeBasedGuid
									.newGuid());
							ps.setLong(2, userId);
							ps.setLong(3, areaId);

						}
					});

		} catch ( Exception e)
		{
			log.error("Error While Associate User to Default Areas::"
					+ e.getMessage());

		}
	}

	public String mapDefaultAreaForUserBulk(final List<TypeAheadVo> areaIds,
			final Long userId)
	{
		
		long id = StrictMicroSecondTimeBasedGuid.newGuid();

		List<Long> areaids = new ArrayList<Long>();

		
		//getting the branch and city for the user is calling this method.
		for (TypeAheadVo areaId : areaIds) {
			if(areaId.getId() != null) {
				areaids.add(areaId.getId());
			}	
		}
		
		try
		{

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(VERIFY_AREA_WITH_BRANCH_AND_CITY)
			        .setParameterList("areaId", areaids)
			        .setParameter("userId",userId );
			
			List<BigInteger> result = query.list();
			
			if (! result.isEmpty()) {
				
			int outcome=0;
			for (BigInteger area : result)
			{

				
				try 
				{	
					Query queryforupsert = hibernateTemplate.getSessionFactory()
							.getCurrentSession().createSQLQuery(DELETE_USER_AREA_MAPPING)
					        .setParameter("userid", userId)
							.setParameter("areaid", area)
					        .setParameter("areaid",area);
							
					outcome = queryforupsert.executeUpdate();
					log.info("Deleted user set for area: "+area);
					

				} catch (Exception e)
				{
					log.error("Error while upserting data");
					return "FAILED TO DELETE FROM DATABASE DUE TO SOME EXCEPTION";
				}
				
				try {
					Query queryforinsert = hibernateTemplate.getSessionFactory()
							.getCurrentSession().createSQLQuery(INSERT_USER_AREA_MAPPING)
					        .setParameter("id", id)
							.setParameter("userid", userId)
					        .setParameter("areaid",area);
							
					outcome = queryforinsert.executeUpdate();
					log.info("Default user set for area: "+area);
				}catch(Exception e) {
					log.error("Error while upserting data");
					return "FAILED TO ADD TO DATABASE DUE TO SOME EXCEPTION";
				}
			}
			if(outcome == 1) {
				
				return "SUCCESSFULLY ADDED THE AREA TO THE USER";
			}
			return "FAILED UPSERT TO DEFAULT USER AREA MAPPING";
			}
			else {
				return "AREA,BRANCH AND CITY DOESN'T MATCH Or User is Not Active";
			}

		} catch (Exception e)
		{
			log.error("Exception in Verifying areas with branch and city ");
			return "FAILED TO VERIFY THE ENTERED AREA WITH BRANCH AND CITY DUE TO SOME EXCEPTION";
		}
			
	}
	public Set<TypeAheadVo> getAllAreasForUser(Long userId)
	{
		log.debug(" inside getAllAreasForUser   "+userId );
		UserVo user = getUserById(userId);
		if (user != null)
		{
			return user.getArea();
		} else
			return new HashSet<TypeAheadVo>();
	}

	public Map<Long, String> getDefaultAreasForUser(Long userId)
	{
		log.debug(" inside getDefaultAreasForUser   " +userId);
		Map<Long, String> areas = new HashMap<Long, String>();
		try
		{

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(USER_DEFAULT_AREA_GET);
			query.setParameter("userId", userId);
			List<Object[]> result = query.list();
			if (result == null)
				return areas;

			for (Object[] obj : result)
			{
				areas.put(FrTicketUtil.objectToLong(obj[0]), (String) obj[1]);
			}

		} catch (Exception e)
		{
			log.error("Exception in getDefaultAreasForUser ");
		}
		return areas;
	}

	@Override
	public int restUserById(ResetUserVo resetUserVo)
	{
		int result = 0;
		if(resetUserVo == null)
			return result;
		log.debug(" inside restUserById   " +resetUserVo.getLoginId());
		if (resetUserVo != null)
		{
			if (resetUserVo.getLoginId() != null
					&& resetUserVo.getNewPassword() != null)
			{

				try
				{
					BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

					String encryptedPassword = encoder
							.encode(resetUserVo.getNewPassword());

					String query = "update User set password='"
							+ encryptedPassword + "' where loginId = '"
							+ resetUserVo.getLoginId() + "'";

					log.info("Update query for user " + resetUserVo.getLoginId()
							+ " and " + resetUserVo.getNewPassword() + " is "
							+ query);
					result = jdbcTemplate.update(query);

				} catch (Exception e)
				{
					log.error("Error while updating password for user "
							+ resetUserVo.getLoginId() + " . "
							+ e.getMessage());
				}

			}

		}

		return result;
	}

	public String getResetUserUrl()
	{
		log.debug(" inside getResetUserUrl   " );
		return resetUserUrl;
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	public void setResetUserUrl(String resetUserUrl)
	{
		this.resetUserUrl = resetUserUrl;
	}

	public String getServers()
	{
		return servers;
	}

	public void setServers(String servers)
	{
		this.servers = servers;
	}

	private class ModifiedUserMapper implements RowMapper<UserVoModified>
	{

		@Override
		public UserVoModified mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			UserVoModified userVoModified = new UserVoModified();

			userVoModified.setUserId(resultSet.getString("userId"));

			userVoModified.setEmpId(resultSet.getString("empId"));

			userVoModified.setLoginId(resultSet.getString("loginId"));

			userVoModified.setPassword(resultSet.getString("password"));

			userVoModified.setFirstName(resultSet.getString("firstName"));

			userVoModified.setLastName(resultSet.getString("lastName"));

			userVoModified.setEmailId(resultSet.getString("emailId"));

			userVoModified.setMobileNo(resultSet.getString("mobileNo"));

			userVoModified.setReportToId(resultSet.getString("reportToId"));

			userVoModified.setEnabled(resultSet.getString("userStatus"));
			userVoModified
					.setReportToLoginId(resultSet.getString("reportToLoginId"));

			return userVoModified;
		}

	}

	@Override
	public Set<TypeAheadVo> getFxDeviceByTl(Long userId)
	{
		Set<TypeAheadVo> deviceDetailsToUi = new HashSet<>();
		if (userId == null || userId <= 0)
			return deviceDetailsToUi;
		
		if (userId == null || userId <= 0)
			return deviceDetailsToUi;
		
			Map<Long, String> nestedReportingUsers = getNestedReportingUsers(userId, null, null, null);
			if(nestedReportingUsers != null && nestedReportingUsers.size() > 0)
			{
				log.info("size :"+nestedReportingUsers.size());
				for(Map.Entry<Long ,String> entry : nestedReportingUsers.entrySet())
				{
					if(userHelper.isFRNEUser(entry.getKey()))
					{
						Set<TypeAheadVo> userDeviceList = userCache.getUserDeviceByLoginId(getUserNameByUserId(entry.getKey()));
						if(userDeviceList != null && userDeviceList.size() > 0)
						for (TypeAheadVo typeAheadVo : userDeviceList) {
							deviceDetailsToUi.add(typeAheadVo);
						}
					}
						
				}
			}
			
			return deviceDetailsToUi;


	/*	try
		{
			List<Object[]> deviceList = null;
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(GET_DEVICE_FOR_TL)
					.setParameter("reportTo", userId);

			deviceList = query.list();
			if (deviceList != null && deviceList.size() > 0)
			{

				TypeAheadVo vo = null;
				for (Object[] obj : deviceList)
				{
					if (obj[0] == null || obj[1] == null)
						continue;

					vo = new TypeAheadVo();
					vo.setId(Long.valueOf(obj[0].toString().trim()));
					vo.setName(obj[1].toString().trim());
					device.add(vo);
				}
			}

			return device;

		} catch (Exception e)
		{
			log.error("Error occure while getting device for Tl : " + userId);
		}

		return device; */
	}

	@Override
	public TypeAheadVo getUserGroupName(Long userId)
	{
		if (userId == null || userId <= 0)
			return null;

		try
		{

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(GET_USER_GROUP_CODE)
					.addEntity(UserGroup.class).setParameter("userId", userId);

			List<UserGroup> userGroup = query.list();

			if (userGroup != null && !userGroup.isEmpty())
				return new TypeAheadVo(userGroup.get(0).getId(), userGroup
						.get(0).getUserGroupName());
		} catch (Exception e)
		{
			log.error("Error occure while getting group name of User : "
					+ userId, e);
		}
		return null;
	}

	@Override
	public Map<Long, String> getNestedReportingUsersWithRoles(long userId,
			List<String> roles, boolean matchaesAnyRole)
	{
		Map<Long, String> users = new HashMap<Long, String>();

		if (roles == null || roles.isEmpty())
		{
			return users;
		}

		List<Long> userIds = userAndNestedReportingMapping.get(userId);
		if (userIds != null)
		{
			for (Long id : userIds)
			{
				UserVo vo = cachedUsersById.get(id);

				if (vo != null && !matchaesAnyRole
						&& isUserContainsAllRole(vo, roles))
					users.put(vo.getId(), GenericUtil
							.buildUserSelectOption(vo));

				else if (vo != null && matchaesAnyRole
						&& isUserContainsAnyRole(vo, roles))
					users.put(vo.getId(), GenericUtil
							.buildUserSelectOption(vo));
			}
		}
		return users;
	}

	@Override
	public List<BigInteger> getReportToByBranchId(Long branchId, Long areaId)
	{
		if (branchId == null || branchId <= 0)
			return null;
		try
		{
			StringBuilder completeQuery = new StringBuilder(GET_AM_BY_BRANCHID);
			String queryString = completeQuery.toString();
			if (AuthUtils.isBranchManager() || AuthUtils.isAreaManager())
			{
				queryString = queryString.replace("_FROM_PLACE_", IFAREA);
			}
			if (AuthUtils.isCityManager())
				queryString = queryString.replace("_FROM_PLACE_", IFBRANCH);

			completeQuery = new StringBuilder(queryString);

			if (AuthUtils.isBranchManager() || AuthUtils.isAreaManager())
			{
				completeQuery.append(" and uam.areaId in (:areas)");
			}
		
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(completeQuery.toString())
					.setParameterList("branches", Collections
							.singletonList(branchId));
			
			if (AuthUtils.isBranchManager() || AuthUtils.isAreaManager())
				query.setParameter("areas", areaId);
			
			log.info("get report of am " + query.toString());
			
			return query.list();

			/*
			 * if (reportTo != null && !reportTo.isEmpty() && reportTo.get(0) !=
			 * null) { return reportTo.get(0).longValue();
			 * 
			 * } return null;
			 */

		} catch (Exception ex)
		{
			log.error("Error occure while getting report to  by branchId : "
					+ ex.getMessage());
		}
		return null;
	}

	@Override
	public TypeAheadVo getCityNameByUserId(Long userId)
	{
		if( userId == null || userId.longValue() <= 0 )
			return null;
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(GET_CITY_NAME_BY_USER_ID)
					.setParameter("userId", userId);
			
			List<Object[]> dbvo = query.list();
			TypeAheadVo cityVO = null;
			if( dbvo != null && !dbvo.isEmpty() )
			{
				Object[] obj = dbvo.get(0);
				cityVO = new TypeAheadVo();
				
				cityVO.setId(objectToLong(obj[0]));
				cityVO.setName( obj[1] != null ? obj[1].toString() : "");
			}
			return cityVO;
		} 
		catch (Exception ex)
		{
			log.error("Error occure while getting city name for User id : "+userId, ex);
		}
		return null;
	}
	
	class UserMapper implements RowMapper<UserVo>
	{

		/* (non-Javadoc)
		 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
		 */
		@Override
		public UserVo mapRow(ResultSet resultSet, int rowNo) throws SQLException {
			
			UserVo userVo = new UserVo();
			
			userVo.setId(resultSet.getLong("id"));
			userVo.setFirstName(resultSet.getString("firstName"));
			userVo.setLastName(resultSet.getString("lastName"));
			userVo.setLoginId(resultSet.getString("loginId"));
			
			return userVo;
		}

		
		
	}
	
	@Override
	public List<String> getAreaMasterMappingDetails(Workbook workbook, UserVo login_user) {
		
		long branchid = 0L;
		long cityid = 0L;

		
		//getting the branch and city for the user is calling this method.
		
			
			if (login_user != null)
			{
				branchid = login_user.getBranch().getId();
				cityid = login_user.getCity().getId();
				
			} else {
				log.error("Error While getting the details of user who has logged in:");
			}
			
			try
			{

				Query query = hibernateTemplate.getSessionFactory()
						.getCurrentSession().createSQLQuery(GET_AREA_DETAILS)
				        .setParameter("branchid",branchid )
				        .setParameter("cityid", cityid);
				
				List<String> result = query.list();	
				
				return result;

			} catch (Exception e)
			{
				log.error("Error getting the data ");
			}
		return null;
	}
}