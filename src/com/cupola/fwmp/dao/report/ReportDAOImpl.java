package com.cupola.fwmp.dao.report;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.AuthUtils;

public class ReportDAOImpl implements ReportDAO
{

	private Logger log = Logger.getLogger(ReportDAOImpl.class.getName());

	HibernateTemplate hibernateTemplate;
	JdbcTemplate jdbcTemplate;

	private final static String GET_COUNT_BY_USER_ID = 
			" select t.id as tId  ,t.status, wo.currentWorkStage from Ticket t "
			+ " Left outer JOIN WorkOrder wo ON wo.ticketId = t.id inner JOIN User u ON  t.currentAssignedTo=u.id "
			+ "and (u.reportTo in (:userId) or  u.id in(:userId))   ";

	private final static String GET_COUNT_BY_AREA_ID = 
			"select t.id as tId  ,t.status, wo.currentWorkStage from Ticket t  Left outer JOIN WorkOrder wo ON wo.ticketId = t.id where t.areaId in (:ids)";
	
	private final static String GET_TICKET_STATUS__BY_AREA_ID =
	
			"select t.status,count(t.status)   from Ticket t  where  areaId in (:ids) group by t.status";

	private final static String GET_TICKET_STATUS__BY_USER_ID = 
			"select t.status,count(t.status)   from Ticket t jOIN  User u ON   t.currentAssignedTo=u.id  and  (u.reportTo in (:userId) or  u.id in(:userId))  group by t.status";

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	@Transactional
	@Override
	public Map<Long, Integer> getCountWorkStageType(Long userId)
	{
		Set<Long> areasId = new HashSet<Long>();
		List<Object[]> status = null;
		Map<Long, Integer> ticketCount = new HashMap<Long, Integer>();

		if(userId == null || userId <= 0)
			return ticketCount;
		
		log.debug(" getCountWorkStageType found userid::" + userId);
		
		try
		{
			if (AuthUtils.isAreaManager())
			{
				areasId.add(AuthUtils.getCurrentUserArea().getId());

			}
			if (AuthUtils.isBranchManager())
			{
				areasId = LocationCache
						.getAreasByBranchId(
								String.valueOf(AuthUtils.getCurrentUserBranch()
										.getId())).keySet();
			}

			if (AuthUtils.isCityManager())
			{
				areasId = LocationCache.getAreasByCityId(
						String.valueOf(AuthUtils.getCurrentUserCity().getId()))
						.keySet();
			}
			Session session = hibernateTemplate.getSessionFactory()
					.getCurrentSession();

			if (areasId != null && !areasId.isEmpty())
			{
				log.debug("found areaIds::" + areasId);

				Query query = session.createSQLQuery(GET_COUNT_BY_AREA_ID);

				query.setParameterList("ids", areasId);
				status = query.list();

			}

			else
			{
				log.debug("count Ticket subCategeory as userId::" + userId);

				Query query = session.createSQLQuery(GET_COUNT_BY_USER_ID);
				query.setLong("userId", userId);
				query.setLong("userId", userId);
				status = query.list();
			}
			Integer fiberCount = 0;
			Integer copperCount = 0;
			Integer feasibilityCount = 0;
			for (Object[] report : status)
			{
				if(objectToLong(report[1]) == TicketStatus.FEASIBILITY_PENDING_BY_NE.longValue())
				{
					feasibilityCount ++;
				}
				if(objectToLong(report[2]) == WorkStageType.COPPER)
				{
					copperCount ++;
				}
				else if(objectToLong(report[2]) == WorkStageType.FIBER)
					fiberCount ++;
				

			}
			
			 
				ticketCount.put(TicketSubCategory.FIBER_ID,fiberCount);
				ticketCount.put(TicketSubCategory.COPPER_ID,copperCount);
				ticketCount.put(TicketSubCategory.FEASIBILITY,feasibilityCount);
				
				
		} catch (Exception e)
		{
			log.error("error to count TicketSubCategeory  while executing getCountWorkStageType::"
					+ e);
		}

		log.debug("found Ticket subCategeory count::" + ticketCount);
		return ticketCount;
	}
	
	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}


	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Integer> getStatusReport(Long userId)
	{
		Map<Integer, Integer> statusCount = new HashMap<Integer, Integer>();

		if (userId == null || userId <= 0)
			return statusCount;
		
		log.debug("status report as userId:" + userId);

		Set<Long> areasId = new HashSet<Long>();
		List<Object[]> status = null;

		try
		{
			if (AuthUtils.isAreaManager())
			{
				areasId.add(AuthUtils.getCurrentUserArea().getId());

			}
			if (AuthUtils.isBranchManager())
			{
				areasId = LocationCache
						.getAreasByBranchId(
								String.valueOf(AuthUtils.getCurrentUserBranch()
										.getId())).keySet();
			}

			if (AuthUtils.isCityManager())
			{
				areasId = LocationCache.getAreasByCityId(
						String.valueOf(AuthUtils.getCurrentUserCity().getId()))
						.keySet();
			}
			Session session = hibernateTemplate.getSessionFactory()
					.getCurrentSession();

			if (areasId != null && !areasId.isEmpty())
			{
				log.debug("found areaIds::" + areasId);

				Query query = session
						.createSQLQuery(GET_TICKET_STATUS__BY_AREA_ID);

				query.setParameterList("ids", areasId);
				status = query.list();

			} else
			{
				log.debug("count Ticket status as userId::" + userId);
				Query query = session
						.createSQLQuery(GET_TICKET_STATUS__BY_USER_ID);

				query.setLong("userId", userId);
				query.setLong("userId", userId);
				status = query.list();
			}

			for (Object[] report : status)
			{
				if (report[0] != null && report[0] != "")
					statusCount.put((Integer) report[0],
							Integer.valueOf(report[1].toString()));

			}
		} catch (Exception e)
		{
			log.error("error to count TicketStatus  while executing getCountWorkStageType::"
					+ e);
		}
		log.debug("found Ticket status  count::" + statusCount);
		return statusCount;

	}

}
