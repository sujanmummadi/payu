package com.cupola.fwmp.dao.report;

import java.util.Map;

public interface ReportDAO
{

	public Map<Long, Integer> getCountWorkStageType(Long userId);

	public Map<Integer, Integer> getStatusReport(Long userId);

}
