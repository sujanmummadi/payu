package com.cupola.fwmp.dao.tariff;

import java.util.List;

import com.cupola.fwmp.filters.TariffFilter;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanVo;

public interface TariffDAO
{

	public Integer addTariff(TariffPlan tariff);

	public TariffPlanVo getTariffById(Long id);

	public List<TariffPlan> getAllTariff();

	public TariffPlan deleteTariff(TariffPlan tariff);

	public TariffPlan updateTariff(TariffPlan tariff);

	public List<TariffPlanVo> getTariffByCity(TariffFilter filter);

	public Integer UpdateCustomerTariff(TariffPlanUpdateVO tariffVo);

	String getTariffNameById(Long id);

	List<TariffPlanVo> getTariffByCity(Long cityId);

	int bulkTariffPlanUpload(List<TariffPlan> tariffPlans);

	/**@author aditya 5:07:17 PM Apr 7, 2017
	 * List<TariffPlanVo>
	 * @param filter
	 * @return
	 */
	List<TariffPlanVo> getPlanCategoeryByCity(TariffFilter filter);

	/**@author aditya 7:33:41 PM Apr 10, 2017
	 * List<TariffPlanVo>
	 * @param packageName
	 * @return
	 */
	List<TariffPlanVo> getTariffByPackageName(String packageName);

	/**@author aditya 7:38:43 PM Apr 10, 2017
	 * List<TariffPlanVo>
	 * @param planCode
	 * @return
	 */
	List<TariffPlanVo> getTariffByPlanCode(String planCode);

}
