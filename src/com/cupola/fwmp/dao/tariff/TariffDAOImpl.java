package com.cupola.fwmp.dao.tariff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.filters.TariffFilter;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanVo;

@Transactional
public class TariffDAOImpl implements TariffDAO
{

	private Logger log = Logger.getLogger(TariffDAOImpl.class.getName());

	HibernateTemplate hibernateTemplate;
	JdbcTemplate jdbcTemplate;

	private final static String GET_TARIFF_PLAN_BY_CITY = "select t.*  from TariffPlan t where id > "
			+ Status.ACTIVE;

	private final static String GET_PLAN_CATEGOERY_BY_CITY = "select distinct t.planCategoery  from TariffPlan t where status="
			+ Status.ACTIVE;

	private final static String GET_TARIFF_PLAN_BY_CITY_1 = "select t.*  from TariffPlan t where status="
			+ Status.ACTIVE + " and t.cityId=?";

	private final static String GET_TARIFF_PLAN_BY_PLAN_CODE = "select t.*  from TariffPlan t where status="
			+ Status.ACTIVE + " and t.planCode=?";

	private final static String GET_TARIFF_PLAN_BY_PACKAGE_NAME = "select t.*  from TariffPlan t where status="
			+ Status.ACTIVE + " and t.packageName=?";

	// private final static String GET_TariffPlan_BY_CITY =
	// "select t.* from TariffPlan t where status=1";
	private final static String UPDATE_CUSTOMER_TARIFF = "update Customer c, Ticket t set c.tariffId=? where c.id=t.customerId and t.id=?";
	private final static int MIN_ITEM_PER_PAGE = 5;

	private static Map<Long, TariffPlanVo> allTarrifs = new ConcurrentHashMap<Long, TariffPlanVo>();

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	@Autowired
	CityDAO cityDAO;

	@Override
	public Integer addTariff(TariffPlan tariff)
	{
		if( tariff == null )
			return 0;

		log.debug(" Inside addTariff ..." +tariff.getCityName());
		tariff.setId(StrictMicroSecondTimeBasedGuid.newGuid());

		// if(tariff.getCityCode() != null && tariff.getPlanType() != null &&
		// tariff.getCityCode() != "" && tariff.getPlanType() != "" ){
		if (tariff.getCityId() != null && tariff.getPlanCode() != null)
		{

			try
			{

				hibernateTemplate.save(tariff);
				TariffPlanVo vo = new TariffPlanVo();
				BeanUtils.copyProperties(tariff, vo);
				vo.setId(tariff.getId() + "");
				allTarrifs.put(tariff.getId(), vo);
				log.info("tariff added successfully");
				return 1;
			} catch (Exception e)
			{
				log.error("Error in adding tariff plan :: " + e.getMessage());
				return 0;
			}
		} else
			return 0;
	}

	@Override
	public TariffPlanVo getTariffById(Long id)
	{
		log.debug(" Inside getTariffById ..." +id);
		if (allTarrifs == null || allTarrifs.size() == 0)
			getAllTariff();
		if (allTarrifs != null && !allTarrifs.isEmpty())
		{
			if (allTarrifs.containsKey(id))
			{
				return allTarrifs.get(id);
			} else
			{
				getAllTariff();
				if (allTarrifs.containsKey(id))
				{
					return allTarrifs.get(id);
				} else
					return null;
			}

		} else
		{
			return null;
		}

	}

	@Override
	public String getTariffNameById(Long id)
	{
		log.debug(" Inside getTariffNameById ..."+id);
		TariffPlan plan = hibernateTemplate.get(TariffPlan.class, id);
		log.debug(" Exitting getTariffNameById ...");
		if (plan != null)
			return plan.getPackageName();
		else
			return "";
	}

	@Override
	public List<TariffPlan> getAllTariff()
	{
//		List<TariffPlan> plans = (List<TariffPlan>) hibernateTemplate
//				.find("from TariffPlan ");
		List<TariffPlanVo> plans=	(List<TariffPlanVo>) jdbcTemplate
		.query(GET_TARIFF_PLAN_BY_CITY, new TariffPlanMapper());

		for (Iterator<TariffPlanVo> iterator = plans.iterator(); iterator.hasNext();)
		{
//			TariffPlan tariffPlan = (TariffPlan) iterator.next();
//			TariffPlanVo vo = new TariffPlanVo();
			TariffPlanVo vo = (TariffPlanVo) iterator.next();
//			BeanUtils.copyProperties(tariffPlan, vo);
//			vo.setId(tariffPlan.getId() + "");
			allTarrifs.put(Long.valueOf(vo.getId()), vo);

		}

		log.debug("allTarrifs " + allTarrifs.size());
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TariffPlan updateTariff(TariffPlan tariff)
	{

		if(tariff != null)
		{
			hibernateTemplate.update(tariff);
			if(allTarrifs.containsKey(tariff.getId()))
			{
				TariffPlanVo vo = new TariffPlanVo();
				BeanUtils.copyProperties(tariff, vo);
				vo.setId(tariff.getId() + "");
				allTarrifs.put(tariff.getId(), vo);
			}
		
		}
		
		return tariff;
	}

	@Override
	public List<TariffPlanVo> getTariffByCity(TariffFilter filter)
	{
		if(filter == null )
			return new ArrayList<TariffPlanVo>();
		
		List<TariffPlanVo> tariffPlan = null;

		StringBuilder completeQuery = new StringBuilder(GET_TARIFF_PLAN_BY_CITY);

		long cityId = filter.getCityId();
		log.debug("Getting tariff details for City : " + cityId);

		if (filter.getPageSize() > 0)
			filter.setPageSize(filter.getPageSize());

		if (filter.getStartOffset() > 0)
			filter.setStartOffset(filter.getStartOffset());

		if (filter.getCityId() > 0)
		{
			completeQuery.append(" and t.cityId=" + filter.getCityId());
		}

		if (filter.getRegxName() != null && !filter.getRegxName().isEmpty())
		{
			completeQuery.append(" and t.planCategoery like '%"
					+ filter.getRegxName() + "%'");
		}

		try
		{

			completeQuery.append(" limit " + filter.getStartOffset() + " , "
					+ filter.getPageSize());

			log.info("Complete query for tariff is "
					+ completeQuery.toString());

			tariffPlan = (List<TariffPlanVo>) jdbcTemplate
					.query(completeQuery.toString(), new TariffPlanMapper());

			if (tariffPlan == null ||tariffPlan.isEmpty())
			{
				log.info("No TariffPlan found as User login Id:" + cityId);
				return tariffPlan;
			}

			log.debug("Tariff paln Found as User login Id:" + cityId);

		} catch (Exception e)
		{
			log.error("Error  getting TariffPlanByCity:" + e);

		}
		
		log.debug("Exitting getTariffByCity : " +filter.getCityId());
		return tariffPlan;

	}

	@Override
	public List<TariffPlanVo> getPlanCategoeryByCity(TariffFilter filter)
	{
		
		if( filter == null )
			return new ArrayList<>();
			
		List<TariffPlanVo> tariffPlan = null;

		StringBuilder completeQuery = new StringBuilder(GET_PLAN_CATEGOERY_BY_CITY);

		long cityId = filter.getCityId();
		log.debug("Getting tariff details for City : " + cityId);

		if (filter.getPageSize() > 0)
			filter.setPageSize(filter.getPageSize());

		if (filter.getStartOffset() > 0)
			filter.setStartOffset(filter.getStartOffset());

		log.info("filter GET_PLAN_CATEGOERY_BY_CITY" + filter);

		if (filter.getCityId() > 0)
		{
			completeQuery.append(" and t.cityId=" + filter.getCityId());
		}

		try
		{

			tariffPlan = (List<TariffPlanVo>) jdbcTemplate.query(completeQuery
					.toString(), new TariffPlanCategoeryMapper());

			if (tariffPlan.isEmpty())
			{
				log.info("No TariffPlan found as User login Id:" + cityId);
				return tariffPlan;
			}

			log.debug("Tariff paln Found as User login Id:" + cityId);

		} catch (Exception e)
		{
			log.error("Error  getting TariffPlanByCity:" + e);

		}
		return tariffPlan;

	}

	
	@Override
	public List<TariffPlanVo> getTariffByCity(Long cityId)
	{
		List<TariffPlanVo> tariffPlan = null;

		log.debug("Getting tariff details for City : " + cityId + " query "
				+ GET_TARIFF_PLAN_BY_CITY_1);

		try
		{
			tariffPlan = (List<TariffPlanVo>) jdbcTemplate
					.query(GET_TARIFF_PLAN_BY_CITY_1, new Object[] {
							cityId }, new TariffPlanMapper());

			if (tariffPlan.isEmpty())
			{
				log.info("No TariffPlan found as User login Id:" + cityId);
				return tariffPlan;
			}

		} catch (Exception e)
		{
			log.error("Error  getting TariffPlanByCity:" + e);

		}
		log.debug("Total Tariff paln size :"+tariffPlan.size() +", User city Id:" + cityId);
		return tariffPlan;

	}

	@Override
	public Integer UpdateCustomerTariff(TariffPlanUpdateVO tariffVo)
	{

		int rows = -1;
		if(tariffVo == null)
			return rows;
		
		log.debug("inside Upating CustomerTariffPlan:" +tariffVo.getProspectNo());
		try
		{
			Object[] params = { tariffVo.getTariffPlanId(),
					tariffVo.getTicketId() };

			int[] types = { Types.BIGINT, Types.BIGINT };

			rows = jdbcTemplate.update(UPDATE_CUSTOMER_TARIFF, params, types);
			if (rows <= 0)
			{
				log.info(" CustomerTariffPlan updated failed");
				return rows;
			}
			log.info("CustomerTariffPlan Updated successfuly");

		} catch (Exception e)
		{

			log.error("Error While Upating CustomerTariffPlan:" + e);
		}
		log.debug("exitting from Upating CustomerTariffPlan:" +tariffVo.getProspectNo());
		return rows;
	}

	@Override
	public TariffPlan deleteTariff(TariffPlan tariff)
	{
		try
		{
			long userGuid = tariff.getId();
			TariffPlan oldTariff = (TariffPlan) hibernateTemplate.get(TariffPlan.class, userGuid);
			if (oldTariff != null)
			{
				oldTariff.setStatus(0l);
				hibernateTemplate.update(oldTariff);
				allTarrifs.remove(oldTariff.getId());
				log.info("Tariff Successfully Deleted From Database ");
				return oldTariff;
			}
		}
		catch (Exception e)
		{

			log.error("Error in Deleting Tariff From DataBase  "

					+ e.getMessage() + " " + e.getStackTrace().toString());

			log.error("Hibernate Error");
			return null;
		}

			return null;
		}
		
	@Override
	public int bulkTariffPlanUpload(List<TariffPlan> tariffPlans)
	{

		if( tariffPlans == null || tariffPlans.isEmpty())
			return 0;
		
		

		int count = 0;
		if (tariffPlans != null && !tariffPlans.isEmpty())
		{
			log.debug("Inserting total " + tariffPlans.size()
			+ " rows in tariff plan table");
			for (Iterator<TariffPlan> iterator = tariffPlans
					.iterator(); iterator.hasNext();)
			{
				try
				{
					TariffPlan tariffPlan = (TariffPlan) iterator.next();
					tariffPlan.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					Long result = (Long) hibernateTemplate.save(tariffPlan);
					hibernateTemplate.flush();

					if (result != null)
					{
						count++;
					}

				} catch (DataAccessException e)
				{
					log.error("Error while inserting data in tariff plan table "
							+ e.getMessage());
					e.printStackTrace();
				}

			}

			log.info("Total success " + count + " in tariff plan table out of "
					+ tariffPlans.size());

			return count;
		} else
		{
			log.info("Total success " + count + " in tariff plan table out of "
					+ tariffPlans.size());
			return count;
		}

	}

	@Override
	public List<TariffPlanVo> getTariffByPlanCode(String planCode)
	{
		List<TariffPlanVo> tariffPlan = null;

		log.debug("Getting tariff details for package : " + planCode + " query "
				+ GET_TARIFF_PLAN_BY_PLAN_CODE);

		try
		{
			tariffPlan = (List<TariffPlanVo>) jdbcTemplate
					.query(GET_TARIFF_PLAN_BY_PLAN_CODE, new Object[] {
							planCode }, new TariffPlanMapper());

			if (tariffPlan.isEmpty())
			{
				log.info("No TariffPlan found as for planCode" + planCode);
				return tariffPlan;
			}

			log.info("Tariff paln Found as planCode " + planCode);

		} catch (Exception e)
		{
			log.error("Error  getting TariffPlanByCity:" + e);

		}
		log.debug("Tariff paln Found as planCode " + planCode);
		return tariffPlan;

	}
	
	@Override
	public List<TariffPlanVo> getTariffByPackageName(String packageName)
	{
		List<TariffPlanVo> tariffPlan = null;

		log.debug("Getting tariff details for package : " + packageName + " query "
				+ GET_TARIFF_PLAN_BY_PACKAGE_NAME);

		try
		{
			tariffPlan = (List<TariffPlanVo>) jdbcTemplate
					.query(GET_TARIFF_PLAN_BY_PACKAGE_NAME, new Object[] {
							packageName }, new TariffPlanMapper());

			if (tariffPlan.isEmpty())
			{
				log.info("No TariffPlan found as for planCode" + packageName);
				return tariffPlan;
			}

			log.info("Tariff paln Found as packageName " + packageName);

		} catch (Exception e)
		{
			log.error("Error  getting TariffPlanByCity:" + e);

		}
		log.debug("Tariff paln Found as packageName " + packageName);
		return tariffPlan;

	}



	private class TariffPlanMapper implements RowMapper<TariffPlanVo>
	{
		@Override
		public TariffPlanVo mapRow(ResultSet rs, int arg1) throws SQLException
		{
			TariffPlanVo plan = new TariffPlanVo();

			plan.setId(rs.getString("id"));
			plan.setPlanType(rs.getString("planType"));
			plan.setPlanCategoery(rs.getString("planCategoery"));
			plan.setPlanCode(rs.getString("planCode"));
			plan.setPackageName(rs.getString("packageName"));
			plan.setNetopCode(rs.getString("netopCode"));
			plan.setDescription(rs.getString("description"));
			plan.setTenure(String.valueOf(rs.getLong("tenure")));
			plan.setOriginalPriceWithoutTax(rs
					.getString("OriginalPriceWithoutTax"));
			plan.setOfferOriginalPrice(rs.getString("OfferOriginalPrice"));
			plan.setPriceWithTax(rs.getString("priceWithTax"));
			plan.setOfferPriceWithTax(rs.getString("offerPriceWithTax"));
			plan.setInstallCharges(rs.getString("installCharges"));
			plan.setInstallChargesWithTax(rs
					.getString("installChargesWithTax"));
			plan.setPlanQuota(rs.getString("planQuota"));
			plan.setUsageCategory(rs.getString("usageCategory"));
			plan.setCurrentSpeed(rs.getString("currentSpeed"));
			plan.setCurrentSpeedUnit(rs.getString("currentSpeedUnit"));
			plan.setSpeedBreakerSpeed(rs.getString("speedBreakerSpeed"));
			plan.setIsRouterFree(rs.getString("isRouterFree"));
			plan.setStatus(rs.getString("status"));
			plan.setCityName(rs.getString("cityName"));
			plan.setCityCode(rs.getString("cityCode"));
			plan.setCityId(rs.getString("cityId"));
			plan.setStartDate(rs.getString("startDate"));
			plan.setEndDate(rs.getString("endDate"));
			plan.setStaticIpPriceWithoutTax(rs.getString("staticIpPriceWithoutTax"));
			plan.setStaticIpPriceWithTax(rs.getString("staticIpPriceWithTax"));

			return plan;
		}

	}

	private class TariffPlanCategoeryMapper implements RowMapper<TariffPlanVo>
	{
		@Override
		public TariffPlanVo mapRow(ResultSet rs, int arg1) throws SQLException
		{
			TariffPlanVo plan = new TariffPlanVo();

			// plan.setId(rs.getString("id"));
			// plan.setPlanType(rs.getString("planType"));
			plan.setPlanCategoery(rs.getString("planCategoery"));
			// plan.setPlanCode(rs.getString("planCode"));
			// plan.setPackageName(rs.getString("packageName"));
			// plan.setNetopCode(rs.getString("netopCode"));
			// plan.setDescription(rs.getString("description"));
			// plan.setTenure(rs.getString("tenure"));
			// plan.setOriginalPriceWithoutTax(rs
			// .getString("OriginalPriceWithoutTax"));
			// plan.setOfferOriginalPrice(rs.getString("OfferOriginalPrice"));
			// plan.setPriceWithTax(rs.getString("priceWithTax"));
			// plan.setOfferPriceWithTax(rs.getString("offerPriceWithTax"));
			// plan.setInstallCharges(rs.getString("installCharges"));
			// plan.setInstallChargesWithTax(rs.getString("installChargesWithTax"));
			// plan.setPlanQuota(rs.getString("planQuota"));
			// plan.setUsageCategory(rs.getString("usageCategory"));
			// plan.setCurrentSpeed(rs.getString("currentSpeed"));
			// plan.setCurrentSpeedUnit(rs.getString("currentSpeedUnit"));
			// plan.setSpeedBreakerSpeed(rs.getString("speedBreakerSpeed"));
			// plan.setIsRouterFree(rs.getString("isRouterFree"));
			// plan.setStatus(rs.getString("status"));
			// plan.setCityName(rs.getString("cityName"));
			// plan.setCityCode(rs.getString("cityCode"));
			// plan.setCityId(rs.getString("cityId"));
			// plan.setStartDate(rs.getString("startDate"));
			// plan.setEndDate(rs.getString("endDate"));

			return plan;
		}
	}

}