package com.cupola.fwmp.dao.priorityWeightage;

import java.util.List;

import com.cupola.fwmp.persistance.entities.PriorityWeightage;

public interface PriorityWeightageDAO {
	
	public PriorityWeightage addPriorityWeightage(PriorityWeightage PriorityWeightage);

	public PriorityWeightage getPriorityWeightageById(Long id);

	public List<PriorityWeightage> getAllPriorityWeightage();

	public PriorityWeightage deletePriorityWeightage(Long id);

	public PriorityWeightage updatePriorityWeightage(PriorityWeightage PriorityWeightage);

}
