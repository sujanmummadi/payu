package com.cupola.fwmp.dao.priorityWeightage;

import com.cupola.fwmp.persistance.entities.TicketPriority;
import com.cupola.fwmp.vo.TicketPriorityVo;

public interface TicketPriorityDao
{
	public static String UPDATE_ESCALTED_PRIORITY = "UPDATE TicketPriority SET escalationType = :escalationType ,"
			+ " escalatedValue = :providedValue , updatedCount = updatedCount + 1,"
			+ " modifiedOn = :time , sliderEscalatedValue = :sliderEscalatedValue WHERE id = :id";
	
	public static String UPDATE_BASE_PRIORITY = "UPDATE TicketPriority SET value = value + :baseValue  ,sliderEscalatedValue = :sliderEscalatedValue "
			+ "WHERE id = :id";
	
	public static String GET_BASE_PRIORITY_FOR_TICKET = "SELECT p.value FROM Ticket tick "
			+ "LEFT JOIN TicketPriority p ON tick.priorityId = p.id "
			+ "WHERE tick.id = :ticketId OR p.id = :priorityId";
	 
	public static String GET_ESCALATED_PRIORITY_FOR_TICKET = "SELECT p.escalatedValue FROM Ticket tick "
			+ "LEFT JOIN TicketPriority p ON tick.priorityId = p.id "
			+ "WHERE tick.id = :ticketId OR p.id = :priorityId";
	
	public static String RESET_SLIDER_ESCALATED_VALUE ="UPDATE TicketPriority SET sliderEscalatedValue = 0 where id > 0";
	
	public static String GET_BASE_PRIORITY_FOR_FR_TICKET = "SELECT p.value FROM FrTicket tick "
			+ "LEFT JOIN TicketPriority p ON tick.priorityId = p.id "
			+ "WHERE tick.id = :ticketId OR p.id = :priorityId";
	 
	public static String GET_ESCALATED_PRIORITY_FOR_FR_TICKET = "SELECT p.escalatedValue FROM FrTicket tick "
			+ "LEFT JOIN TicketPriority p ON tick.priorityId = p.id "
			+ "WHERE tick.id = :ticketId OR p.id = :priorityId";
	
//	public Integer createPriorityForTicket(TicketPriorityVo priority);
	public void updatePriorityForTicket(TicketPriorityVo priority);

	public void updateEscalatedPriority(Long escalationType, Integer escalatedValue,
			Long priorityId);

	public void updateBasePriorityForTicket(Integer value, Long priorityId);

	public Integer getBasePriorityByTicketIdOrPriorityId(Long argument);
	
	public Integer getEscalatedValueByTicketIdOrPriorityId(Long argument);

	public void resetSliderEsclatedValue();

	public Integer getBasePriorityOfFrTicketByTicketIdOrProrityId(Long argument);

	public Integer getEscalatedValueOfFrTicketByTicketIdOrPriorityId(Long argument);

	public Integer updateFrTicketPriority(TicketPriority priority);
}
