package com.cupola.fwmp.dao.priorityWeightage;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.persistance.entities.TicketPriority;
import com.cupola.fwmp.vo.TicketPriorityVo;

/**
 * 
 * @author G Ashraf
 * 
 * */

@Transactional
public class TicketPriorityDaoImpl implements TicketPriorityDao   
{
	
	private static Logger LOGGER = Logger.getLogger(TicketPriorityDaoImpl.class);
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	/*@Override
	public Integer createPriorityForTicket(TicketPriorityVo priority)
	{
		if( priority == null )
			return null;
		
		Integer res = null;
		try{
			TicketPriority dbvo = new TicketPriority();
			dbvo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			dbvo.setValue(priority.getPriority());
			dbvo.setEscalationType(priority.getEscalationType());
			dbvo.setEscalatedValue(priority.getEscalatedValue());
			dbvo.setModifiedOn(priority.getModifiedOn());
			dbvo.setUser(null set ModifiedBy value);
			dbvo.setUpdatedCount(priority.getUpdatedCount());
			
			res = (Integer)hibernateTemplate.save(priority);
			return res;
		}catch(Exception e){
			LOGGER.error("Error occure while assigning priority...", e);
		}
		return res;
	}*/

	@Override
	public void updatePriorityForTicket(TicketPriorityVo priority)
	{
		if( priority == null )
			return;
		
		LOGGER.debug("Inside updatePriorityForTicket  " +priority.getWorkorderNumber());
		
		try{
			TicketPriority dbvo = new TicketPriority();
			dbvo.setId(priority.getPriorityId());
			dbvo.setValue(priority.getPriorityValue());
			dbvo.setEscalationType(priority.getEscalationType().intValue());
			dbvo.setEscalatedValue(priority.getEscalatedValue());
			dbvo.setModifiedOn(priority.getPriorityModifiedOn());
			dbvo.setModifiedBy(priority.getPriorityModifiedBy());
			dbvo.setUpdatedCount(priority.getUpdatedCount());
			
			hibernateTemplate.update(dbvo);
			LOGGER.debug("Priority updated successfully...");
			
		}catch(Exception e){
			LOGGER.error("Error occure while updating priority...", e);
		}
		
	}
	
	@Override
	public void updateEscalatedPriority(Long escalationType, Integer escalatedValue,Long priorityId)
	{
		if(priorityId == null || priorityId <= 0)
			return;
		
		LOGGER.debug("Inside updateEscalatedPriority  escalationType  "+ escalationType +" escalatedValue " + escalatedValue + " priorityId "  +priorityId);
		StringBuilder queryString = new StringBuilder(TicketPriorityDao.UPDATE_ESCALTED_PRIORITY);
		
		try{
			int result = hibernateTemplate.getSessionFactory()
							.getCurrentSession()
							.createSQLQuery(queryString.toString())
							.setParameter("escalationType", escalationType)
							.setParameter("providedValue", escalatedValue)
							.setParameter("time", new Date())
							.setParameter("sliderEscalatedValue", 0)
							.setParameter("id", priorityId)
							.executeUpdate();
			if( result > 0)
				LOGGER.debug("priority updated successfully with escalated value : "+escalatedValue);
			
		}catch(Exception e){
			LOGGER.error("Can't update escalated value by escalation type :  "+escalationType,e);
		}
	}

	@Override
	public void updateBasePriorityForTicket(Integer value , Long priorityId)
	{
		if( value == null || priorityId == null || priorityId <= 0)
			value = 0;
		
		LOGGER.debug("Inside updateBasePriorityForTicket  value is" + value + " priorityId is " + priorityId);
		try{
			int result = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(TicketPriorityDao.UPDATE_BASE_PRIORITY)
					.setParameter("baseValue", value)
					.setParameter("sliderEscalatedValue",0)
					.setParameter("id", priorityId)
					.executeUpdate();
			if( result > 0)
				LOGGER.debug("priority updated successfully with... "+value + " for priorityId " + priorityId );
			
		}catch(Exception e){
			LOGGER.error("Can't update base priority for ticket   ",e);
		}
	}
	
	@Override
	public Integer getBasePriorityByTicketIdOrPriorityId(Long argument)
	{
		if( argument == null)
			return 0;
		
		LOGGER.debug("Inside getBasePriorityByTicketIdOrPriorityId  " + argument);
		StringBuilder queryString = new StringBuilder(TicketPriorityDao
				.GET_BASE_PRIORITY_FOR_TICKET);
		return getPriorityFromDb(queryString.toString(), argument);
	}

	@Override
	public Integer getEscalatedValueByTicketIdOrPriorityId(Long argument)
	{
		
		if( argument == null)
			return 0;
		
		LOGGER.debug("Inside getEscalatedValueByTicketIdOrPriorityId  " +argument);
		StringBuilder queryString = new StringBuilder(TicketPriorityDao
				.GET_ESCALATED_PRIORITY_FOR_TICKET);
		return getPriorityFromDb(queryString.toString(), argument);
	}
	
	private Integer getPriorityFromDb(String Query,Long argument)
	{
		if( argument == null)
			return 0;
		
		LOGGER.debug("Inside getPriorityFromDb  " +argument);
		
		try{
			List value = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(Query)
					.setParameter("ticketId", argument)
					.setParameter("priorityId", argument)
					.list();
			
			if( value != null && value.size() > 0)
				return (Integer)value.get(0);
			
			return null;
			
		}catch(Exception e){
			LOGGER.error("Can't fetch base priority for ticket "
					+ "having priority id or ticket id : "+argument,e);
		}
		LOGGER.debug("Exit From getPriorityFromDb  " +argument);
		return null;
	}

	@Override
	public void resetSliderEsclatedValue() 
	{
		LOGGER.debug("Inside resetSliderEsclatedValue  ");
		try{
			hibernateTemplate.getSessionFactory()
			 .getCurrentSession()
			 	.createSQLQuery(TicketPriorityDao
					 .RESET_SLIDER_ESCALATED_VALUE)
					  .executeUpdate();
		}catch(Exception exception){
			LOGGER.error("Error occure while resetting the slider escalated value ",exception);
		}
	}
	
	@Override
	public Integer getBasePriorityOfFrTicketByTicketIdOrProrityId( Long argument )
	{
		if( argument == null || argument <= 0)
			return 0;
		LOGGER.debug("Inside getBasePriorityOfFrTicketByTicketIdOrProrityId  " +argument);
		StringBuilder queryString = new StringBuilder(TicketPriorityDao
				.GET_BASE_PRIORITY_FOR_FR_TICKET);
		return getPriorityFromDb(queryString.toString(), argument);
	}

	@Override
	public Integer getEscalatedValueOfFrTicketByTicketIdOrPriorityId( Long argument )
	{
		
		if( argument == null || argument <= 0)
			return 0;
		LOGGER.debug("Inside getBasePriorityOfFrTicketByTicketIdOrProrityId  " +argument);
		StringBuilder queryString = new StringBuilder(TicketPriorityDao
				.GET_ESCALATED_PRIORITY_FOR_FR_TICKET);
		return getPriorityFromDb(queryString.toString(), argument);
	}
	
	
	@Override
	public Integer updateFrTicketPriority( TicketPriority priority )
	{
		LOGGER.debug("Inside updateFrTicketPriority  " +priority);
		try {
			if( priority == null || priority.getId() <= 0 )
				return 0;
			else
			{
				hibernateTemplate.update(priority);
				int row = 0;
				return ++row;
			}
		} catch (Exception e) {
			LOGGER.error("Error while updating FrTicket priority"+e);
			return 0;
		}
	}
}
