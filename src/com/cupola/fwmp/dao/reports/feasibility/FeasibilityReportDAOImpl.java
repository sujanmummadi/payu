package com.cupola.fwmp.dao.reports.feasibility;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.mongo.vo.fr.GISData;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.reports.vo.FeasibilityCheck;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.ProspectCoreVO;

@Transactional
public class FeasibilityReportDAOImpl implements FeasibilityReportDAO
{

	HibernateTemplate hibernateTemplate;
	@Autowired
	GisDAO gisDAO;
	@Autowired
	UserDao userDao;

	@Autowired
	MongoTemplate mongoTemplate;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernateTemplate = hibernetTemplate;

	}

	private static Logger log = Logger
			.getLogger(FeasibilityReportDAOImpl.class.getName());

	public String FEASIBILITY_REPORT_QUERY_WITHOUT_DATES = "SELECT distinct g.ticketNo, t.prospectNo,c.mobileNumber,c.alternativeMobileNo,c.officeNumber ,a.areaName, t.TicketCreationDate "
			+ "as ticket_added_on,t.commitedETR as days,s.status as feasibilitystatus ,  b.branchName,ct.cityName,t.id,"
			+ "(select g.addedBy from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2  order by g.addedOn desc limit 1) as gissalesSeName,"
			+ "(select g.fxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2 order by g.addedOn  desc limit 1) as gissalesfxname,"
			+ "(select g.cxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2  order by g.addedOn  desc limit 1) as gissalescxname,"
			+ "(select g.fxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2 order by g.addedOn  desc limit 1) as gissalesfxPorts,"
			+ "(select g.cxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2 order by g.addedOn  desc limit 1) as gissalescxPorts,"
			+ "(select g.connectionType from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2    order by g.addedOn  desc limit 1) as gisconnectionType,"
			+ "(select g.addedOn from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2 order by addedOn desc limit 1) as gissalesFeasibilityDate,"

			+ "(select g.addedBy from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3  order by g.addedOn desc limit 1) as physicalSeName,"
			+ "(select g.fxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3 order by g.addedOn  desc limit 1) as physicalfxname,"
			+ "(select g.cxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3  order by g.addedOn  desc limit 1) as physicalcxname,"
			+ "(select g.fxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3 order by g.addedOn  desc limit 1) as physicalfxPorts,"
			+ "(select g.cxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3 order by g.addedOn  desc limit 1) as physicalcxPorts,"
			+ "(select g.connectionType from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3    order by g.addedOn  desc limit 1) as physicalconnectionType,"
			+ "(select g.addedOn from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3 order by addedOn desc limit 1) as physicalFeasibilityDate,"

			+ "(select g.fxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4 order by g.addedOn  desc limit 1) as deploymentfxname,"
			+ "(select g.cxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4  order by g.addedOn  desc limit 1) as deploymentcxname,"
			+ "(select g.fxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4 order by g.addedOn  desc limit 1) as deploymentfxPorts,"
			+ "(select g.cxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4 order by g.addedOn  desc limit 1) as deploymentcxPorts,"
			+ "(select g.connectionType from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4    order by g.addedOn  desc limit 1) as deploymentconnectionType,"
			+ "(select g.addedOn from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4 order by addedOn desc limit 1) as deploymentFeasibilityDate"

			+ " FROM Ticket t inner JOIN Customer c  on t.customerId=c.id inner join Gis g on g.ticketNo=t.id "
			+ " LEFT JOIN Area a on a.id=c.AreaId LEFT JOIN Branch b  on b.id=a.branchId LEFT JOIN City ct on ct.id=b.cityId"
			+ " LEFT JOIN WorkOrder w on w.ticketId=t.id  "
			+ " LEFT JOIN Status s on s.id=t.status  where    t.source in ('Call Centre','Door Knock','Tool') ";

	public List<FeasibilityCheck> getFeasibilityReports(String fromDate,
			String toDate, String branchId, String areaId, String cityId)
	{
		log.debug(" inside  getFeasibilityReports "   + cityId);

		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;

		try
		{

			org.hibernate.Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate))
			{
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try
				{
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e)
				{
					log.error("Error While parsing date formate :"
							+ e.getMessage());
					e.printStackTrace();
				}
			}

			StringBuffer queryString = new StringBuffer();

			queryString.append(FEASIBILITY_REPORT_QUERY_WITHOUT_DATES);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate)))
			{

				datesPresent = true;

				queryString
						.append(" and t.TicketCreationDate>=:fromDate and t.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId))
			{
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId))
			{
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityId))
			{
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent)
			{
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			log.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent)
			{

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent)
			{

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent)
			{
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent)
			{
				Long city = Long.parseLong(cityId);
				query.setLong("cityId", city);
			}

			if (!cityPresent)
			{
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			log.info("query : " + query);

			list = query.list();

			log.info("list size: " + list.size());

		} catch (Exception e)
		{
			log.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and
			// workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		log.info("TIme Taken for db call and getting results in milliseconds"
				+ timeDiff);

		List<FeasibilityCheck> feasibilityList = getFeasibilityList(list);

		/*
		 * try { JasperManager.exportData(jsonCartList,NamingConstants.
		 * FEASIBILITYREPORTJRXML
		 * ,PropLoader.getPropertyForReport("feasibilityreport.xls.output.path"
		 * )); } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		log.debug(" Exit From  getFeasibilityReports "   + cityId);
		return feasibilityList;

	}

	private List<FeasibilityCheck> getFeasibilityList(List<Object[]> list)
	{

		if (list == null)
			return null;
		log.debug(" inside  getFeasibilityList "   + list.size());

		List<FeasibilityCheck> feasibilityList = new ArrayList<FeasibilityCheck>();

		FeasibilityCheck feasibilityReport = null;

		for (Object[] feasibility : list)
		{

			try
			{
				feasibilityReport = new FeasibilityCheck();

				// 0 g.ticketNo,
				// 8 s.status AS feasibilitystatus,
				// 11 t.id

				feasibilityReport.setDcrNO(objectToString(feasibility[1])); // prospectNo
																			// from
																			// Ticket
																			// table
				feasibilityReport
						.setCurrentPhoneMob(objectToString(feasibility[2])); // mobileNumber
																				// from
																				// Customer
																				// table
				feasibilityReport
						.setCurrentPhoneRes(objectToString(feasibility[3]));// alternativeMobileNo
																			// from
																			// Customer
																			// table
				feasibilityReport
						.setCurrentPhoneOff(objectToString(feasibility[4])); // officeNumber
																				// from
																				// Customer
																				// table

				feasibilityReport.setAreaName(objectToString(feasibility[5]));
				feasibilityReport.setBranchName(objectToString(feasibility[9]));
				feasibilityReport.setCityName(objectToString(feasibility[10]));

				String ticketCreationDate = objectToString(feasibility[6]);
				String committedETRDate = objectToString(feasibility[7]);
				if (ticketCreationDate != null
						&& ticketCreationDate.trim().length() > 0
						&& committedETRDate != null
						&& committedETRDate.trim().length() > 0)
				{

					Long time = getDaysDifference(committedETRDate, ticketCreationDate);

					feasibilityReport.setCommitmentDays(String.valueOf(time));

				}
				// String ticket =
				// objectToString(objectToString(feasibility[11]));

				if (feasibility[12] != null)
				{

					feasibilityReport
							.setSeName(GenericUtil.completeUserName(userDao
									.getUserById(objectToLong(feasibility[12]))));

				}
				if (feasibility[13] != null)
				{

					feasibilityReport
							.setFxNameByse(objectToString(feasibility[13]));
				}

				if (feasibility[14] != null)
				{
					feasibilityReport
							.setCxNameByse(objectToString(feasibility[14]));

				}

				if (feasibility[15] != null)
				{
					feasibilityReport
							.setFxPortByse(objectToString(feasibility[15]));

				}

				if (feasibility[16] != null)
				{
					feasibilityReport
							.setCxPortByse(objectToString(feasibility[16]));

				}

				if (feasibility[17] != null)
				{
					feasibilityReport
							.setConnectionTypeByse(objectToString(feasibility[17]));
				}

				if (feasibility[18] != null)
				{
					feasibilityReport.setFeasibilityDateByse(GenericUtil
							.convertToUiDateFormat(feasibility[18]));

				}

				if (feasibility[19] != null)
				{

					feasibilityReport
							.setNeName(GenericUtil.completeUserName(userDao
									.getUserById(objectToLong(feasibility[19]))));

				}
				if (feasibility[20] != null)
				{

					feasibilityReport
							.setFxNameByne(objectToString(feasibility[20]));
				}

				if (feasibility[21] != null)
				{
					feasibilityReport
							.setCxNameByne(objectToString(feasibility[21]));

				}

				if (feasibility[22] != null)
				{
					feasibilityReport
							.setFxPortByne(objectToString(feasibility[22]));

				}

				if (feasibility[23] != null)
				{
					feasibilityReport
							.setCxPortByne(objectToString(feasibility[23]));

				}

				if (feasibility[24] != null)
				{
					feasibilityReport
							.setConnectionTypeByne(objectToString(feasibility[24]));
				}

				if (feasibility[25] != null)
				{
					feasibilityReport.setFeasibilityDateByne(GenericUtil
							.convertToUiDateFormat(feasibility[25]));

				}
				if (feasibility[26] != null)
				{

					feasibilityReport
							.setFxNameByaat(objectToString(feasibility[26]));
				}

				if (feasibility[27] != null)
				{
					feasibilityReport
							.setCxNameByaat(objectToString(feasibility[27]));

				}

				if (feasibility[28] != null)
				{
					feasibilityReport
							.setFxPortByaat(objectToString(feasibility[28]));

				}

				if (feasibility[29] != null)
				{
					feasibilityReport
							.setCxPortByaat(objectToString(feasibility[29]));

				}

				if (feasibility[30] != null)
				{
					feasibilityReport
							.setConaatctionTypeByaat(objectToString(feasibility[30]));
				}

				if (feasibility[31] != null)
				{
					feasibilityReport.setFeasibilityDateByaat(GenericUtil
							.convertToUiDateFormat(feasibility[31]));

				}

				// Long ticketId = null;
				/*
				 * if (ticket != null) { ticketId = Long.parseLong(ticket); }
				 */
				/*
				 * Map<Long, GISPojo> gisDatavalue = gisDAO
				 * .getGisTypeAndDetailsByTicketId(ticketId); if (gisDatavalue
				 * != null) { for (Iterator iterator =
				 * gisDatavalue.values().iterator(); iterator .hasNext();) {
				 * GISPojo gisPojo = (GISPojo) iterator.next();
				 * 
				 * if (gisPojo.getFeasibilityType() != null) { if
				 * (FeasibilityType.GIS_SALES.equals(gisPojo
				 * .getFeasibilityType())) { feasibilityReport
				 * .setSeName(GenericUtil .completeUserName(userDao
				 * .getUserById(gisPojo .getAddedBy())));
				 * 
				 * feasibilityReport .setFxNameByse(gisPojo.getFxName());
				 * feasibilityReport .setCxNameByse(gisPojo.getCxName());
				 * feasibilityReport.setFxPortByse(gisPojo .getFxPorts());
				 * feasibilityReport.setCxPortByse(gisPojo .getCxPorts());
				 * feasibilityReport.setConnectionTypeByse(gisPojo
				 * .getConnectionType()); feasibilityReport
				 * .setFeasibilityDateByse(GenericUtil
				 * .convertToUiDateFormat(gisPojo .getAddedOn()));
				 * 
				 * }
				 * 
				 * else if
				 * (FeasibilityType.PHYSICAL_FEASIBILITY_BY_NE.equals(gisPojo
				 * .getFeasibilityType())) { feasibilityReport
				 * .setNeName(GenericUtil .completeUserName(userDao
				 * .getUserById(gisPojo .getAddedBy())));
				 * 
				 * feasibilityReport .setFxNameByne(gisPojo.getFxName());
				 * feasibilityReport .setCxNameByne(gisPojo.getCxName());
				 * feasibilityReport.setFxPortByne(gisPojo .getFxPorts());
				 * feasibilityReport.setCxPortByne(gisPojo .getCxPorts());
				 * feasibilityReport.setConnectionTypeByne(gisPojo
				 * .getConnectionType()); feasibilityReport
				 * .setFeasibilityDateByne(GenericUtil
				 * .convertToUiDateFormat(gisPojo .getAddedOn()));
				 * 
				 * } else if (FeasibilityType.DEPLOYMENT_AAT.equals(gisPojo
				 * .getFeasibilityType())) {
				 * feasibilityReport.setFxNameByaat(gisPojo .getFxName());
				 * feasibilityReport.setCxNameByaat(gisPojo .getCxName());
				 * feasibilityReport.setFxPortByaat(gisPojo .getFxPorts());
				 * feasibilityReport.setCxPortByaat(gisPojo .getCxPorts());
				 * feasibilityReport.setConaatctionTypeByaat(gisPojo
				 * .getConnectionType()); feasibilityReport
				 * .setFeasibilityDateByaat(GenericUtil
				 * .convertToUiDateFormat(gisPojo .getAddedOn()));
				 * 
				 * } } } }
				 */

				// feasibilityReport.setFxPortNO(objectToString(feasibility[14]));
				// // fxPorts form Gis table
				// feasibilityReport.setCxPortNO(objectToString(feasibility[15]));
				// //cxPorts form Gis table
				//
				log.debug("feasibilityReport " + feasibilityReport);
				log.debug("feasibilityList.add(feasibilityReport); size before adding "
						+ feasibilityList.size());

				feasibilityList.add(feasibilityReport);

				log.debug("feasibilityList.add(feasibilityReport); size after adding "
						+ feasibilityList.size());

				Long ticketNo = Long
						.valueOf(((BigInteger) feasibility[0]).toString());

				Query query = new Query();

				query.addCriteria(Criteria.where("ticketId").is(ticketNo));

				// Update update = new Update();
				// update.set("status", "closed prospectNo " +
				// vo.getProspectNo());
				//
				// mongoTemplate.findAndModify(query, update,
				// ProspectCoreVO.class)
				// ;

				GISData gisData = mongoTemplate.findOne(query, GISData.class);

				log.debug("GIS Multiple data for ticket Id " + ticketNo
						+ " is :: " + gisData);

				if (gisData != null && gisData.getDeviceAndReasonMap() != null
						&& !gisData.getDeviceAndReasonMap().isEmpty())
				{
					Map<String, String> deviceAndReasonMap = gisData
							.getDeviceAndReasonMap();

					log.debug("deviceAndReasonMap " + deviceAndReasonMap
							+ " for ticket " + ticketNo);

					for (Map.Entry<String, String> entry : deviceAndReasonMap
							.entrySet())
					{
						FeasibilityCheck feasibilityReportTemp = new FeasibilityCheck();
						BeanUtils
								.copyProperties(feasibilityReport, feasibilityReportTemp);
						;
						String deviceName = entry.getKey();
						String deviceComment = entry.getValue();

						log.debug(" deviceName " + deviceName + " "
								+ deviceComment);

						if (feasibilityReport.getConnectionTypeByne() != null
								&& feasibilityReport.getConnectionTypeByne()
										.equalsIgnoreCase(TicketSubCategory.COPPER))
						{
							String[] devices = deviceName.split("___");

							String fxName = devices[0];
							String cxName = null;

							if (null != devices[1])
								cxName = devices[1];

							log.debug("CxName " + cxName + " fxName " + fxName
									+ " " + deviceComment);

							feasibilityReportTemp
									.setFxNameByne(fxName !=null ? fxName.replace("__", ".") : fxName);
							feasibilityReportTemp
									.setCxNameByne(cxName !=null ? cxName.replace("__", ".") : cxName);
							feasibilityReportTemp.setRemarks(deviceComment);

						} else
						{
							feasibilityReportTemp.setFxNameByne(deviceName
									.replace("__", "."));
							feasibilityReportTemp.setRemarks(deviceComment);
						}
						log.debug("feasibilityReportTemp size before adding "
								+ feasibilityList.size());

						feasibilityList.add(feasibilityReportTemp);

						log.debug("feasibilityReportTemp size after adding "
								+ feasibilityList.size());

						log.debug("feasibilityReportTemp "
								+ feasibilityReportTemp);
					}

				} else
				{
					log.info("No Multiple GIS comment available for TicketId "
							+ ticketNo);
				}
			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("Exception  " + e.getMessage());
				continue;
			}
		}
		log.debug(" Exit From  getFeasibilityList "   + list.size());
		return feasibilityList;
	}

	private Long getDaysDifference(String committedETRDate,
			String ticketCreationDate)
	{
		log.debug(" inside  getDaysDifference "  );
		DateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

		// DateFormat df = new SimpleDateFormat("dd-MMM-yy");

		Date commitDate = null;
		try
		{
			commitDate = df.parse(committedETRDate);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date ticketDate = null;
		try
		{
			ticketDate = df.parse(ticketCreationDate);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long diff = 0;
		if(commitDate !=null && commitDate.getTime() >0 && ticketDate !=null && ticketDate.getTime() >0)
		 diff = commitDate.getTime() - ticketDate.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

	}

	private String objectToString(Object obj)
	{
		if (obj == null)
			return null;
		return obj.toString();
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

}
