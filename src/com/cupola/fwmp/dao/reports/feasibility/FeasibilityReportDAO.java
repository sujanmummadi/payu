package com.cupola.fwmp.dao.reports.feasibility;

import java.util.Date;
import java.util.List;

import com.cupola.fwmp.reports.vo.FeasibilityCheck;
import com.cupola.fwmp.reports.vo.WorkOrderReport;

public interface FeasibilityReportDAO {
	
	public List<FeasibilityCheck> getFeasibilityReports(String fromDate , String toDate,String branchId,String areaId, String cityId);
}

 
