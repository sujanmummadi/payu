package com.cupola.fwmp.dao.reports.sales;

import java.util.Date;
import java.util.List;

import com.cupola.fwmp.reports.vo.SalesDcrDetails;
 
public interface SalesReportDAO {
	
	public List<SalesDcrDetails> getSalesDcrReports(String fromDate , String toDate,String branchId,String areaId,String cityId);

}
