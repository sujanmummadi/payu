package com.cupola.fwmp.dao.reports.sales;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.FWMPConstant.ProspectType;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.reports.vo.SalesDcrDetails;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.UserVo;

@Transactional
public class SalesDcrReportDAOImpl implements SalesReportDAO
{

	HibernateTemplate hibernateTemplate;

	MongoTemplate mongoTemplate;
	
	
	@Autowired
	TicketActivityLogDAO activityLogDAO;
	
	@Autowired
	GisDAO gisDAO;
	
	@Autowired 
	TicketDAO ticketDAO;
	@Autowired
	UserDao userDao;
	@Autowired
	DefinitionCoreService definitionCoreService;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernateTemplate = hibernetTemplate;

	}

	public void setMongoTemplate(MongoTemplate mongoTemplate)
	{
		this.mongoTemplate = mongoTemplate;
	}

	private static Logger log = Logger.getLogger(SalesDcrReportDAOImpl.class
			.getName());
	public String SALES_REPORT_QUERY_WITHOUT_DATES = " SELECT t.prospectNo,c.firstName,c.currentAddress,s.status,t.TicketCreationDate as ticket_added_on, "
			+ "'gis_added_on',c.mobileNumber,t.TicketCreationDate,t.addedOn as caf_login_time,tp.planType,c.emailId,"
			+ "'ticketlog_added_on',a.areaName,t.ticketCreationDate, t.commitedETR as days,c.remarks,"
			+ "t.cafNumber as cafNo,t.id ,c.status as custStatus,c.middleName,c.lastName"
			+ " , b.branchName,ct.cityName , u.id as userId, t.source,c.enquiry,t.preferedDate,t.modifiedBy,t.modifiedOn,TIMESTAMPDIFF(DAY, t.TicketCreationDate, t.commitedETR) as commitmentDays , "
			+ "(select tal.addedOn from TicketActivityLog tal where  tal.ticketId = t.id   and tal.activityId = 14  order by tal.addedOn desc limit 1) as doc,"
             +" (select tal.addedBy from TicketActivityLog tal where  tal.ticketId = t.id   and tal.activityId = 14 order by tal.addedOn desc limit 1) as seName,"
             +"(select tal.addedOn from TicketActivityLog tal where  tal.ticketId = t.id   and tal.activityId = 2  order by tal.addedOn desc limit 1) as custAcAct ,"
              +"(select tal.addedOn from TicketActivityLog tal where  tal.ticketId = t.id   and tal.activityId = 13  order by tal.addedOn desc limit 1) as cafLoginTime ,"
            +" (select g.addedOn from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2 order by g.addedOn desc limit 1) as feasibilityRaise,"
              +"(select g.connectionType from Gis g where  g.TicketNo = t.id   and g.feasibilityType =2  order by g.addedOn desc limit 1) as connectionType"
			+ " FROM Ticket t INNER JOIN  Customer c  on t.customerId=c.id LEFT JOIN "
			+ "Area a on a.id=c.areaId "
			+" LEFT JOIN Branch b on b.id=a.branchId LEFT JOIN City ct on ct.id=b.cityId "
			+ " LEFT JOIN User u on u.id=t.currentAssignedTo " 
			+ " LEFT JOIN Status s on s.id=t.status  "
			+ "LEFT JOIN TariffPlan tp on tp.id=c.tariffId "
			+ " where    t.source in ('Call Centre','Door Knock','Tool') ";

	public List<SalesDcrDetails> getSalesDcrReports(String fromDate,
			String toDate, String branchId, String areaId,String cityId)
	{
		log.debug(" inside  getSalesDcrReports "   + cityId);

		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try
		{

			Query query = null;
		
			{

				Date from = null;

				Date to = null;

				boolean datesPresent = false;

				boolean areaPresent = false;

				boolean branchPresent = false;
				boolean cityPresent = false;

				if (GenericUtil.isStringNotNull(fromDate)
						&& GenericUtil.isStringNotNull(toDate))
				{
 
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

					try
					{
						from = df.parse(fromDate);
						to = df.parse(toDate);

					} catch (ParseException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				StringBuffer queryString = new StringBuffer();

				queryString.append(SALES_REPORT_QUERY_WITHOUT_DATES);


				if (from != null && to != null
						&& (GenericUtil.isStringNotNull(fromDate))
						&& (GenericUtil.isStringNotNull(toDate)))
				{

					datesPresent = true;

					queryString
							.append(" and t.TicketCreationDate>=:fromDate and t.TicketCreationDate<=:toDate ");
				}

				if (GenericUtil.isStringNotNull(branchId))
				{
					queryString.append(" and b.id =:branchId ");
					branchPresent = true;
				}

				if (GenericUtil.isStringNotNull(areaId))
				{
					areaPresent = true;
					queryString.append(" and a.id =:areaId ");
				}

				if(GenericUtil.isStringNotNull(cityId))
				{
					cityPresent = true;
					queryString.append(" and ct.id =:cityId ");
				}
				if(!cityPresent)
				{
					if(!AuthUtils.isAdmin())
						queryString.append(" and ct.id =:cityId ");
				}
				
				
				
				log.info("query string: " + queryString);

				query = hibernateTemplate.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery(queryString.toString());

				if (datesPresent)
				{

					query.setDate("fromDate", from);
					query.setDate("toDate", to);
				}

				if (branchPresent)
				{

					Long branch = Long.parseLong(branchId);
					query.setLong("branchId", branch);
				}

				if (areaPresent)
				{
					Long area = Long.parseLong(areaId);
					query.setLong("areaId", area);

				}
				


				if(cityPresent)
				{
					Long city = Long.parseLong(cityId);
					query.setLong("cityId", city);
				}

				if(!cityPresent)
				{
					if(!AuthUtils.isAdmin())
						query.setLong("cityId", AuthUtils.getCurrentUserCity().getId());
				}

			}
			log.info("query : " + query);

			list = query.list();

			log.info("list size: " + list.size());

		} catch (Exception e)
		{
			log.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		log.info("TIme Taken for db call and getting results in milliseconds"
				+ timeDiff);

		/*
		 * try {
		 * JasperManager.exportData(jsonCartList,NamingConstants.SALESREPORTJRXML
		 * ,PropLoader.getPropertyForReport("salesreport.xls.output.path")); }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		log.debug(" Exit From  getSalesDcrReports "   + cityId);
		return  getSalesDcrList(list);

	}

	private List<SalesDcrDetails> getSalesDcrList(List<Object[]> list)
	{
		Long timestart = System.currentTimeMillis();

		if (list == null)
			return null;
		
		log.debug(" inside  getSalesDcrList "   + list.size());
		List<SalesDcrDetails> salesDcrList = new ArrayList<SalesDcrDetails>();

		SalesDcrDetails salesDcrReport = null;

		for (Object[] sales : list)
		{
			try
			{
				salesDcrReport = new SalesDcrDetails();

				salesDcrReport.setDcrNo(objectToString(sales[0])); // prospectNo
																	// from Ticket
																	// table
 
				salesDcrReport.setCustomerName(objectToString(sales[1])); // firstName
											

				if(objectToString(sales[19]) != null)
					salesDcrReport.setCustomerName(salesDcrReport.getCustomerName().concat(" "+objectToString(sales[19])));
				else	if(objectToString(sales[20]) != null)
					salesDcrReport.setCustomerName(salesDcrReport.getCustomerName().concat(" "+objectToString(sales[20])));
				// Customer
																			// table
				salesDcrReport.setCompleteAddress(objectToString(sales[2])); // currentAddress
																				// from
																				// Customer
																				// table
				salesDcrReport.setCurrentStatus(objectToString(sales[3])); // status
				salesDcrReport.setContactNumber(objectToString(sales[6]));														// from
				salesDcrReport.setEspTariff(objectToString(sales[9]));
				salesDcrReport.setEmailId(objectToString(sales[10])); // emailId
				salesDcrReport.setAreaName(objectToString(sales[12])); // areaName
				salesDcrReport.setDcrCreactionDate(objectToString(sales[13])); // ticketCreationDate
				
				if (objectToString(sales[15]) != null)
				{
					if (definitionCoreService
							.getCustomerRemarksByKey(objectToString(sales[15])) != null)
					{
						salesDcrReport.setRemarks(definitionCoreService
								.getCustomerRemarksByKey(objectToString(sales[15])));

					} else
					{
						salesDcrReport.setRemarks((objectToString(sales[15])));
					}
				} else
				{
					salesDcrReport.setRemarks((objectToString(sales[15])));
				}
				salesDcrReport.setCafNo(objectToString(sales[16])); // cafNumber
				
 
				salesDcrReport.setBranch(objectToString(sales[21] ));
				salesDcrReport.setCity(objectToString(sales[22] ));

				if(objectToString(sales[23] ) != null)
				{
					long userId = Long.parseLong(objectToString(sales[23]));
					UserVo vo = userDao.getUserById(userId);
				
					if(vo != null && GenericUtil.isSEUser(vo))
						salesDcrReport.setSeName(GenericUtil.completeUserName(vo));

				}
				salesDcrReport.setSource(objectToString(sales[24] ));
				
				
//			c.enquiry,t.preferedDate,t.modifiedBy,t.modifiedOn"
				if(sales[25] != null )
					{
					String enq = definitionCoreService.getEnqueryById(objectToLong(sales[25]));
					salesDcrReport.setEnquiry(enq != null ? enq : "N/A");
					}
				
				salesDcrReport.setPrefferedCallDate(GenericUtil.convertToUiDateFormat(sales[26]));
				
				salesDcrReport.setModifiedBy(GenericUtil.completeUserName(userDao.getUserById(objectToLong(sales[27]))));
				
				salesDcrReport.setModifiedOn( ( objectToString(sales[28])));

				
				
				String ticket = objectToString(sales[17]);
				Long ticketId = null;
				if (ticket != null)
				{
					ticketId = Long.parseLong(ticket);
				}
				
				if(objectToString(sales[18]) != null)
				{
					String type = objectToString(sales[18]) ;
					if((ProspectType.HOT_ID +"").equals(type))
						salesDcrReport.setCustomerType(ProspectType.HOT);
					else if((ProspectType.MEDIUM_ID +"").equals(type))
						salesDcrReport.setCustomerType(ProspectType.MEDIUM);
					else if((ProspectType.COLD_ID +"").equals(type))
						salesDcrReport.setCustomerType(ProspectType.COLD);
				}
				
/*			if(ticketId != null)
				
				{
					log.info("sales report activity start");
				List<ActivityVo> activities =	activityLogDAO.getActivitiesByWoNo(ticketId);
				
				if(activities != null)
				{
					for (Iterator iterator = activities.iterator(); iterator
							.hasNext();)
					{
						ActivityVo activityVo = (ActivityVo) iterator.next();
						if (activityVo.getId() == Activity.DOCUMENT_UPDATE)
						{
							salesDcrReport
									.setDocCollectionTime(GenericUtil
											.convertToUiDateFormat(activityVo
													.getAddedOn()));
							
						UserVo vo =	userDao.getUserById(activityVo.getAddedBy());
						if(vo != null)
							salesDcrReport.setSeName(GenericUtil.completeUserName(vo));

						} else if (activityVo.getId() == Activity.CUSTOMER_ACCOUNT_ACTIVATION)
						{
							salesDcrReport
									.setAccountActivationTime(GenericUtil
											.convertToUiDateFormat(activityVo
													.getAddedOn()));
						}
						else if (activityVo.getId() == Activity.PAYMENT_UPDATE)
						{
							salesDcrReport
									.setCafLoginTime(GenericUtil
											.convertToUiDateFormat(activityVo
													.getAddedOn()));
						}
					}

				}
				log.info("sales report activity stop");
				log.info("sales report gis start");
				Map<Long, GISPojo>  gisDatavalue = gisDAO.getGisTypeAndDetailsByTicketId(ticketId);
				if(gisDatavalue != null)
				{
					
					
					for (Iterator iterator = gisDatavalue.values().iterator(); iterator.hasNext();)
					{
						GISPojo gisPojo = (GISPojo) iterator.next();
						if(gisPojo.getFeasibilityType()!= null && FeasibilityType.GIS_SALES .equals(gisPojo.getFeasibilityType()))
						{
							salesDcrReport.setFeasibilityRaisedTime(GenericUtil
									.convertToUiDateFormat(gisPojo
											.getAddedOn()));  
							salesDcrReport.setFeasibilityGivenTime(GenericUtil
									.convertToUiDateFormat(gisPojo
											.getAddedOn()));  
							salesDcrReport.setConnectionType(gisPojo.getConnectionType());
							
						}
						
						
					}
				}
				log.info("sales report gis stop");
				
				
				
				
				}*/

//	String ticketCreationDate = objectToString(sales[4]);

//	String committedETRDate = objectToString(sales[14]);

				/*if (ticketCreationDate != null
						&& ticketCreationDate.trim().length() > 0
						&& committedETRDate != null
						&& committedETRDate.trim().length() > 0)
				{

					Long time = getDaysDifference(committedETRDate, ticketCreationDate);

					salesDcrReport.setCommitmentDays(String.valueOf(time));  

				}*/
				
				if(objectToString(sales[29]) != null)
				{
					salesDcrReport.setCommitmentDays(objectToString(sales[29]));  
				}
				if(objectToString(sales[30]) != null)
				{
					salesDcrReport
					.setDocCollectionTime(GenericUtil
							.convertToUiDateFormat(sales[30]));
				}
				if(objectToString(sales[31]) != null)
				{
					UserVo vo =	userDao.getUserById(objectToLong(sales[31]));
					if(vo != null)
						salesDcrReport.setSeName(GenericUtil.completeUserName(vo));
				}
				if(objectToString(sales[32]) != null)
				{
					salesDcrReport
					.setAccountActivationTime(GenericUtil
							.convertToUiDateFormat(sales[32]));
				}
				if(objectToString(sales[33]) != null)
				{
					salesDcrReport
					.setCafLoginTime(GenericUtil
							.convertToUiDateFormat(sales[33]));
				}
				if(objectToString(sales[34]) != null)
				{
					salesDcrReport.setFeasibilityRaisedTime(GenericUtil
							.convertToUiDateFormat(sales[34]));  
					salesDcrReport.setFeasibilityGivenTime(GenericUtil
							.convertToUiDateFormat(sales[34]));  
					
				}
				if(objectToString(sales[35]) != null)
				{
					salesDcrReport.setConnectionType(objectToString(sales[35]));
				}
				
				
				salesDcrList.add(salesDcrReport);
			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("Exception  " + e.getMessage());
				continue;
			}

			 
		}
		log.info("end method");
		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		log.info("Time Taken for preparing results in milliseconds"
				+ timeDiff);
		return salesDcrList;
	}

	private Long getDaysDifference(String committedETRDate,
			String ticketCreationDate)
	{

		DateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

		// DateFormat df = new SimpleDateFormat("dd-MMM-yy");

		Date commitDate = null;
		try
		{
			commitDate = df.parse(committedETRDate);
		} catch (ParseException e)
		{
			log.error("Error while parsing dateformate :" + e.getMessage());
			e.printStackTrace();
		}
		Date ticketDate = null;
		try
		{
			ticketDate = df.parse(ticketCreationDate);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long diff = commitDate.getTime() - ticketDate.getTime();

		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

	}
	private Long objectToLong(Object obj)
	{
		if (obj == null || obj.equals(null) || obj == "null"
				|| obj.equals("null"))
			return 0l;
		return Long.valueOf(obj + "");

	}

	private String objectToString(Object obj)
	{
		if (obj == null)
			return null;
		return obj.toString();
	}

}
