 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.reopen;

import java.util.List;

import com.cupola.fwmp.reports.vo.FrReOpenHistoryReport;
 

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface ReOpenReportDAO {
	public  List<FrReOpenHistoryReport> getFrReopenHistoryReport(String fromDate , String toDate,String branchId,String areaId,String cityID);
	

}
