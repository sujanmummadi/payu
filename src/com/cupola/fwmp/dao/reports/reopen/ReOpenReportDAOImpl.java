 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.reopen;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.reports.fr.FrReportDao;
import com.cupola.fwmp.dao.reports.reassignment.ReAssignmentReportDAOImpl;
import com.cupola.fwmp.reports.vo.FrReOpenHistoryReport;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Transactional
public class ReOpenReportDAOImpl implements ReOpenReportDAO {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	private static Logger LOGGER = Logger.getLogger(ReOpenReportDAOImpl.class.getName());
	
	
	public String SQL_ACT_REOPEN_HISTORY ="SELECT c.mqId,ftr.workOrderNumber,ftr.TicketCreationDate,s.status,ftr.TicketCategory,fd.currentSymptomName,ftr.reopenDateTime,u.empId as EmpCode,u.firstName as EmpName,u.empId as UserId,"
			+ "u.firstName as UserName,ftr.TicketClosedTime,ftr.reopenCount "
			+ "FROM  FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join Status s on ftr.status=s.id "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
			+ "join Area a on a.id=c.areaId "
			+ "join Branch b on b.id=a.branchId "
			+ "join City ct on ct.id=b.cityId "
			+ "join FrDetail fd on fd.ticketId=ftr.id "
			+ "join User u on ftr.currentAssignedTo=u.id where reopenCount > 0";
	

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.reopen.ReOpenReportDAO#getFrReopenHistoryReport(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<FrReOpenHistoryReport> getFrReopenHistoryReport(
			String fromDate, String toDate, String branchId, String areaId,
			String cityID) {
		// TODO Auto-generated method stub
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(SQL_ACT_REOPEN_HISTORY);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		return getReopenHistoryReport(list);
	}


	/**@author leela
	 * List<FrReOpenHistoryReport>
	 * @param list
	 * @return
	 */
	private List<FrReOpenHistoryReport> getReopenHistoryReport(
			List<Object[]> list) {
		// TODO Auto-generated method stub
		if (list == null)
			return new ArrayList<FrReOpenHistoryReport>();

		FrReOpenHistoryReport reopenReport = null;
		List<FrReOpenHistoryReport> reopenreportObject = new ArrayList<FrReOpenHistoryReport>();

		for (Object[] db : list) {
			
			reopenReport = new FrReOpenHistoryReport();

			reopenReport.setMqId(FrTicketUtil.objectToString(db[0]));
			reopenReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[1]));
			reopenReport.setTicketCreationDate(FrTicketUtil.objectToDbDate(db[2]));
			reopenReport.setStatus(FrTicketUtil.objectToString(db[3]));
			reopenReport.setTicketCategory(FrTicketUtil.objectToString(db[4]));
			reopenReport.setCurrentSymptomName(FrTicketUtil.objectToString(db[5]));
			reopenReport.setReopenDateTime(FrTicketUtil.objectToDbDate(db[6]));
			reopenReport.setAssignedEmpId(FrTicketUtil.objectToString(db[7]));
			reopenReport.setAssignedEmpName(FrTicketUtil.objectToString(db[8]));
			reopenReport.setClosedByUId(FrTicketUtil.objectToString(db[9]));
			reopenReport.setClosedByUName(FrTicketUtil.objectToString(db[10]));
			reopenReport.setTicketClosedTime(FrTicketUtil.objectToDbDate(db[11]));
			reopenReport.setReopenCount(FrTicketUtil.objectToString(db[12]));
			
			 
			reopenreportObject.add(reopenReport);
		}

		return reopenreportObject;
	}

}
