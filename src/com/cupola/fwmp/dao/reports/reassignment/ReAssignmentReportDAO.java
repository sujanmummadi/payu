 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.reassignment;

import java.util.List;

import com.cupola.fwmp.reports.vo.FrReAssignmentHistoryReport;
import com.cupola.fwmp.reports.vo.FrTicketReAssignmentReport;
 

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface ReAssignmentReportDAO {
	public  List<FrReAssignmentHistoryReport> getFrReassignmentHistoryReport(String fromDate , String toDate,String branchId,String areaId,String cityID);
	public  List<FrTicketReAssignmentReport> getFrTicketReassignmentHistoryReport(String fromDate , String toDate,String branchId,String areaId,String cityID);
	

}
