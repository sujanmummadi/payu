 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.reassignment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.reports.fr.FrReportDao;
import com.cupola.fwmp.reports.vo.FrReAssignmentHistoryReport;
import com.cupola.fwmp.reports.vo.FrTicketReAssignmentReport;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Transactional
public class ReAssignmentReportDAOImpl implements ReAssignmentReportDAO {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	 
	
	private static Logger LOGGER = Logger.getLogger(ReAssignmentReportDAOImpl.class.getName());
	
	public String SQL_ACT_REASSIGNMENT_HISTORY ="SELECT c.mqId,ftr.workOrderNumber,fd.currentSymptomName,ftr.TicketCreationDate,etr.communicationETR,c.mobileNumber "
			+ "FROM FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
			+ "join Area a on a.id=c.areaId "
			+ "join Branch b on b.id=a.branchId "
			+ "join City ct on ct.id=b.cityId "
			+ "join FrDetail fd on fd.ticketId=ftr.id " ;
	
	
	public String SQL_ACT_TICKET_REASSIGNMENT_HISTORY ="SELECT ftr.workOrderNumber, "
			+ "ct.cityName as cityName, b.branchName as branchName, a.areaName as areaName, ftr.TicketCategory,ftr.TicketNature, "
			+ "(select concat(COALESCE(u.firstName,''),',',COALESCE(u.loginId,'')) from User u where u.id = (select tr.assignedFrom order by tr.addedOn desc limit 1)) as assignedFrom, "
			+ "(select concat(COALESCE(u.firstName,''),',',COALESCE(u.loginId,'')) from User u where u.id = (select tr.assignedTo   order by tr.addedOn desc limit 1)) as assignedTo, "
			+ "(select concat(COALESCE(u.firstName,''),',',COALESCE(u.loginId,'')) from User u where u.id = (select tr.assignedBy   order by tr.addedOn desc limit 1)) as assignedBy, "
			+ "tr.addedOn "
			+ "FROM FrTicket ftr join Customer c on ftr.customerId=c.id "
			+ "LEFT JOIN Area a on a.id=c.areaId "
			+ "LEFT JOIN Branch b  on b.id=c.branchId "
			+ "LEFT JOIN City ct on ct.id=c.cityId "
			+ "JOIN TicketReassignmentLog tr on tr.ticketId=ftr.id ";
            
            

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.reassignment.ReAssignmentReportDAO#getFrReassignmentHistoryReport(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<FrReAssignmentHistoryReport> getFrReassignmentHistoryReport(
			String fromDate, String toDate, String branchId, String areaId,
			String cityID) {
		// TODO Auto-generated method stub
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(SQL_ACT_REASSIGNMENT_HISTORY);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		return getReassignmentHistoryReport(list);
	}


	/**@author leela
	 * List<FrReAssignmentHistoryReport>
	 * @param list
	 * @return
	 */
	private List<FrReAssignmentHistoryReport> getReassignmentHistoryReport(
			List<Object[]> list) {
		// TODO Auto-generated method stub
		if (list == null)
			return new ArrayList<FrReAssignmentHistoryReport>();

		FrReAssignmentHistoryReport reAssignmentReport = null;
		List<FrReAssignmentHistoryReport> reAssignmentreportObject = new ArrayList<FrReAssignmentHistoryReport>();

		for (Object[] db : list) {
			
			reAssignmentReport = new FrReAssignmentHistoryReport();

			reAssignmentReport.setMqId(FrTicketUtil.objectToString(db[0]));
			reAssignmentReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[1]));
			reAssignmentReport.setCurrentSymptomName(FrTicketUtil.objectToString(db[2]));
			reAssignmentReport.setTicketCreationDate(FrTicketUtil.objectToDbDate(db[3]));
			reAssignmentReport.setCommunicationETR(FrTicketUtil.objectToDbDate(db[4]));
			reAssignmentReport.setMobileNumber(FrTicketUtil.objectToString(db[5]));
			
			 
			 
			reAssignmentreportObject.add(reAssignmentReport);
		}

		return reAssignmentreportObject;
	}


	@Override
	public List<FrTicketReAssignmentReport> getFrTicketReassignmentHistoryReport(
			String fromDate, String toDate, String branchId, String areaId,
			String cityID) {
		
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(SQL_ACT_TICKET_REASSIGNMENT_HISTORY);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and tr.addedOn>=:fromDate and tr.addedOn<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		
		return getTicketReassignmentHistoryReport(list);
	}

	
	private List<FrTicketReAssignmentReport> getTicketReassignmentHistoryReport(
			List<Object[]> list) {
		// TODO Auto-generated method stub
		if (list == null)
			return new ArrayList<FrTicketReAssignmentReport>();

		FrTicketReAssignmentReport reAssignmentReport = null;
		List<FrTicketReAssignmentReport> reAssignmentreportObject = new ArrayList<FrTicketReAssignmentReport>();

		for (Object[] db : list) {
			
			reAssignmentReport = new FrTicketReAssignmentReport();
			
			List<String> reAssignmentLogList = null;
			
			reAssignmentReport.setTicketNo(FrTicketUtil.objectToString(db[0]));
			reAssignmentReport.setCity(FrTicketUtil.objectToString(db[1]));
			reAssignmentReport.setBranch(FrTicketUtil.objectToString(db[2]));
			reAssignmentReport.setArea(FrTicketUtil.objectToString(db[3]));
			reAssignmentReport.setTicketCategory(FrTicketUtil.objectToString(db[4]));
			reAssignmentReport.setTicketNature(FrTicketUtil.objectToString(db[5]));
			
			reAssignmentLogList=convertCSVToList(FrTicketUtil.objectToString(db[6]));
			
			if(reAssignmentLogList != null && !reAssignmentLogList.isEmpty())
		     {
				reAssignmentReport.setAssignedFromName(reAssignmentLogList.get(0));
				reAssignmentReport.setAssignedFromUid(reAssignmentLogList.get(1));
		     }
			
			reAssignmentLogList=convertCSVToList(FrTicketUtil.objectToString(db[7]));
			
			if(reAssignmentLogList != null && !reAssignmentLogList.isEmpty())
		     {
				reAssignmentReport.setAssignedToName(reAssignmentLogList.get(0));
				reAssignmentReport.setAssignedToUid(reAssignmentLogList.get(1));
		     }
			
			reAssignmentLogList=convertCSVToList(FrTicketUtil.objectToString(db[8]));
			
			if(reAssignmentLogList != null && !reAssignmentLogList.isEmpty())
		     {
				reAssignmentReport.setAssignedByName(reAssignmentLogList.get(0));
				reAssignmentReport.setAssignedByUid(reAssignmentLogList.get(1));
		     }
			
			reAssignmentReport.setAddedOn(FrTicketUtil.objectToDbDate(db[9]));
			
			reAssignmentreportObject.add(reAssignmentReport);
		}

		return reAssignmentreportObject;
	}
	
	public static List<String> convertCSVToList(String value) {
		List<String> valueList = new ArrayList<String>();
		if (value == null || value.length() == 0) {
			return valueList;
		}
		 
		valueList = Arrays.asList(value.split("\\s*,\\s*")); 
		return valueList;
	}
	
}
