 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.lightweight;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

 





import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.reports.closedcomplaints.ClosedComplaintsReportDAOImpl;
import com.cupola.fwmp.dao.reports.fr.FrReportDao;
import com.cupola.fwmp.reports.vo.FrNewLightWeight;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Transactional
public class LightWeightReportDAOImpl implements LightWeightReportDAO {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	 
	private static Logger LOGGER = Logger.getLogger(LightWeightReportDAOImpl.class.getName());
	
	public String SQL_ACT_NET_LIGHTWEIGHT ="SELECT ftr.TicketCreationDate as ValueDateTime,ftr.modifiedOn as LastUpdateDateTime,ftr.workOrderNumber as ComplaintNo,c.mqId,a.areaName, ftr.fxName as FX, ftr.cxIp as CX, c.communicationAdderss,"
			+ "s.status,u.firstName as Empname, c.firstName as Custname,c.mobileNumber, b.branchName ,ftr.TicketCategory as ComplaintDesc,  ftr.TicketNature as ProblemDesc,TIMESTAMPDIFF(HOUR,ftr.TicketCreationDate,ftr.TicketClosedTime) as Duration,DATEDIFF(NOW(),ftr.TicketCreationDate) as Aging ,fd.voc,"
			+ "ftr.reopenCount as Reopencount,etr.communicationETR "
			+ "FROM FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join User u on ftr.currentAssignedTo=u.id "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId"
			+ " join Area a on a.id=c.areaId "
			+ "join Branch b on b.id=a.branchId "
			+ "join City ct on ct.id=b.cityId "
			+ "join FrDetail fd on fd.ticketId=ftr.id "
			+ "join Status s on ftr.status=s.id" ;

	
	
	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.lightweight.LightWeightReportDAO#getFrNewLightWeightReport(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<FrNewLightWeight> getFrNewLightWeightReport(String fromDate,
			String toDate, String branchId, String areaId, String cityID) {
		// TODO Auto-generated method stub
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(SQL_ACT_NET_LIGHTWEIGHT);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		return getNetLightWeightReport(list);
	}



	/**@author leela
	 * List<FrNewLightWeight>
	 * @param list
	 * @return
	 */
	private List<FrNewLightWeight> getNetLightWeightReport(List<Object[]> list) {
		// TODO Auto-generated method stub
		if (list == null)
			return new ArrayList<FrNewLightWeight>();

		FrNewLightWeight netLightWeightReport = null;
		List<FrNewLightWeight> netLightWeightreportObject = new ArrayList<FrNewLightWeight>();

		for (Object[] db : list) {
			netLightWeightReport = new FrNewLightWeight();

			
			netLightWeightReport.setValueDate(FrTicketUtil.objectToDbDate(db[0]));
			netLightWeightReport.setModifiedOn(FrTicketUtil.objectToDbDate(db[1])); 
			netLightWeightReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[2]));
			netLightWeightReport.setMqId(FrTicketUtil.objectToString(db[3]));
			netLightWeightReport.setAreaName(FrTicketUtil.objectToString(db[4]));
			netLightWeightReport.setFxName(FrTicketUtil.objectToString(db[5]));
			netLightWeightReport.setCxName(FrTicketUtil.objectToString(db[6]));
			netLightWeightReport.setCommunicationAdderss(FrTicketUtil.objectToString(db[7]));
			netLightWeightReport.setStatus(FrTicketUtil.objectToString(db[8]));
			netLightWeightReport.setEmpName(FrTicketUtil.objectToString(db[9]));
			netLightWeightReport.setCustName(FrTicketUtil.objectToString(db[10]));
			netLightWeightReport.setMobileNumber(FrTicketUtil.objectToString(db[11]));
			netLightWeightReport.setBranchName(FrTicketUtil.objectToString(db[12]));
			netLightWeightReport.setComplaintDesc(FrTicketUtil.objectToString(db[13]));
			netLightWeightReport.setProblemDesc(FrTicketUtil.objectToString(db[14]));
			netLightWeightReport.setDuration(FrTicketUtil.objectToString(db[15]));
			netLightWeightReport.setAging(FrTicketUtil.objectToString(db[16]));
			netLightWeightReport.setVoc(FrTicketUtil.objectToString(db[17]));
			netLightWeightReport.setReopenCount(FrTicketUtil.objectToString(db[18]));
			netLightWeightReport.setCommunicationETR(FrTicketUtil.objectToDbDate(db[19]));
			 
			netLightWeightreportObject.add(netLightWeightReport);
		}

		return netLightWeightreportObject;
	}

}
