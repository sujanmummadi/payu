 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.lightweight;

import java.util.List;

import com.cupola.fwmp.reports.vo.FrNewLightWeight;

 

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface LightWeightReportDAO {
	public  List<FrNewLightWeight> getFrNewLightWeightReport(String fromDate , String toDate,String branchId,String areaId,String cityID);

}
