 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.closedcomplaints;

import java.util.List;

import com.cupola.fwmp.reports.vo.FrClosedComplaintsReport;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface ClosedComplaintsReportDAO {
	
	public  List<FrClosedComplaintsReport> getFrClosedComplaintsReport(String fromDate , String toDate,String branchId,String areaId,String cityID);
	public  List<FrClosedComplaintsReport> getFrClosedComplaints30MinsReport(String fromDate , String toDate,String branchId,String areaId,String cityID);

}
