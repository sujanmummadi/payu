 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.reports.closedcomplaints;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.reports.fr.FrReportDao;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.reports.vo.FrClosedComplaintsReport;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;

 /**
 * @Author leela  Jun 13, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
@Transactional
public class ClosedComplaintsReportDAOImpl implements ClosedComplaintsReportDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;
	
private static Logger LOGGER = Logger.getLogger(ClosedComplaintsReportDAOImpl.class.getName());

public String SQL_ACT_CLOSED_COMPLAINTS ="SELECT ftr.TicketCreationDate as ValueDateTime,ftr.modifiedOn as LastUpdateDateTime,ftr.workOrderNumber as ComplaintNo,c.mqId,a.areaName, ftr.fxName as FX, ftr.cxIp as CX, c.communicationAdderss,"
		+ "u.firstName as Empname, c.firstName as Custname, b.branchName as EntityName,ftr.TicketCategory as ComplaintDesc, ftr.mqClosureCode,ftr.TicketNature as ProblemDesc,TIMESTAMPDIFF(HOUR,ftr.TicketCreationDate,ftr.TicketClosedTime) as Duration, DATEDIFF(NOW(),ftr.TicketCreationDate) as Aging,fd.voc,ftr.closedBy,"
		+ "ftr.reopenCount as Reopencount,etr.communicationETR, ftr.TicketClosedTime as resolveddatetime,ftr.defectCode,ftr.subDefectCode  "
		+ "FROM FrTicket ftr join Customer c on ftr.customerId=c.id "
		+ "join User u on ftr.currentAssignedTo=u.id "
		+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
		+ "join Area a on a.id=c.areaId "
		+ "join Branch b on b.id=a.branchId "
		+ "join City ct on ct.id=b.cityId "
		+ "join FrDetail fd on fd.ticketId=ftr.id";

public String SQL_ACT_CLOSED_30MINS_COMPLAINTS ="SELECT ftr.TicketCreationDate as ValueDateTime,ftr.modifiedOn as LastUpdateDateTime,ftr.workOrderNumber as ComplaintNo,c.mqId,a.areaName, ftr.fxName as FX, ftr.cxIp as CX, c.communicationAdderss,"
		+ "u.firstName as Empname, c.firstName as Custname, b.branchName as EntityName,ftr.TicketCategory as ComplaintDesc, ftr.mqClosureCode,ftr.TicketNature as ProblemDesc,TIMESTAMPDIFF(HOUR,ftr.TicketCreationDate,ftr.TicketClosedTime) as Duration, DATEDIFF(NOW(),ftr.TicketCreationDate) as Aging,fd.voc,ftr.closedBy,"
		+ "ftr.reopenCount as Reopencount,etr.communicationETR, ftr.TicketClosedTime as resolveddatetime,ftr.defectCode,ftr.subDefectCode  "
		+ "FROM FrTicket ftr join Customer c on c.id=ftr.customerId "
		+ "join User u on ftr.currentAssignedTo=u.id "
		+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
		+ "join Area a on a.id=c.areaId "
		+ "join Branch b on b.id=c.branchId "
		+ "join City ct on ct.id=c.cityId "
		+ "join FrDetail fd on fd.ticketId=ftr.id where (ftr.addedOn > (now() - interval 30 minute))";


	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.closedcomplaints.ClosedComplaintsReportDAO#getFrClosedComplaintsReport(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<FrClosedComplaintsReport> getFrClosedComplaintsReport(
			String fromDate, String toDate, String branchId, String areaId,
			String cityID) {
		// TODO Auto-generated method stub
		LOGGER.debug(" inside  getCafReports "   + cityID);
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(SQL_ACT_CLOSED_COMPLAINTS);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query closed complaints@ : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		
		return getClosedComplaintsList(list);
	}



	/**@author leela
	 * List<FrClosedComplaintsReport>
	 * @param list
	 * @return
	 */
	private List<FrClosedComplaintsReport> getClosedComplaintsList(
			List<Object[]> list) {
		// TODO Auto-generated method stub
		if (list == null)
			return new ArrayList<FrClosedComplaintsReport>();
		LOGGER.debug(" inside  getClosedComplaintsList "   + list.size());
		FrClosedComplaintsReport closedComplaintsReport = null;
		List<FrClosedComplaintsReport> closedComplaintsreportObject = new ArrayList<FrClosedComplaintsReport>();

		for (Object[] db : list) {
			closedComplaintsReport = new FrClosedComplaintsReport();

			closedComplaintsReport.setValueDate(FrTicketUtil.objectToDbDate(db[0]));
			closedComplaintsReport.setModifiedOn(FrTicketUtil.objectToDbDate(db[1])); 
			closedComplaintsReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[2]));
			closedComplaintsReport.setMqId(FrTicketUtil.objectToString(db[3]));
			closedComplaintsReport.setAreaName(FrTicketUtil.objectToString(db[4]));
			closedComplaintsReport.setFxName(FrTicketUtil.objectToString(db[5]));
			closedComplaintsReport.setCxIp(FrTicketUtil.objectToString(db[6]));
			closedComplaintsReport.setCommunicationAdderss(FrTicketUtil.objectToString(db[7]));
			closedComplaintsReport.setEmpName(FrTicketUtil.objectToString(db[8]));
			closedComplaintsReport.setCustName(FrTicketUtil.objectToString(db[9]));
			closedComplaintsReport.setBranchName(FrTicketUtil.objectToString(db[10]));
			closedComplaintsReport.setTicketCategory(FrTicketUtil.objectToString(db[11]));
			closedComplaintsReport.setResCode(FrTicketUtil.objectToString(db[12]));
			closedComplaintsReport.setProblemDesc(FrTicketUtil.objectToString(db[13]));
			closedComplaintsReport.setDuration(FrTicketUtil.objectToString(db[14]));
			closedComplaintsReport.setAging(FrTicketUtil.objectToString(db[15]));
			closedComplaintsReport.setVoc(FrTicketUtil.objectToString(db[16]));
			closedComplaintsReport.setClosedBy(FrTicketUtil.objectToString(db[17]));
			closedComplaintsReport.setReopenCount(FrTicketUtil.objectToString(db[18]));
			closedComplaintsReport.setCommunicationETR(FrTicketUtil.objectToDbDate(db[19]));
			closedComplaintsReport.setTicketClosedTime(FrTicketUtil.objectToDbDate(db[20]));
			closedComplaintsReport.setDefectCode(FrTicketUtil.objectToString(db[21]));
			closedComplaintsReport.setSubDefectCode(FrTicketUtil.objectToString(db[22]));
			
			closedComplaintsreportObject.add(closedComplaintsReport);
		}
		LOGGER.debug(" Exit from  getClosedComplaintsList "   + list.size());
		return closedComplaintsreportObject;
	}



	@Override
	public List<FrClosedComplaintsReport> getFrClosedComplaints30MinsReport(
			String fromDate, String toDate, String branchId, String areaId,
			String cityID) {
		// TODO Auto-generated method stub
		LOGGER.debug(" inside  getFrClosedComplaints30MinsReport "   + cityID);
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(SQL_ACT_CLOSED_30MINS_COMPLAINTS);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		
		LOGGER.debug(" Exit from  getFrClosedComplaints30MinsReport "   + cityID);
		return getClosedComplaintsList(list);
	}

}
