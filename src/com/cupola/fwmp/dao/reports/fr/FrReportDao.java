package com.cupola.fwmp.dao.reports.fr;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.service.reports.fr.FrAllocationReport;
import com.cupola.fwmp.service.reports.fr.FrClosedComplaintsReport;
import com.cupola.fwmp.service.reports.fr.FrEtrReport;
import com.cupola.fwmp.service.reports.fr.FrNewLightWeight;
import com.cupola.fwmp.service.reports.fr.FrProductivityReport;
import com.cupola.fwmp.service.reports.fr.FrReAssignmentHistoryReport;
import com.cupola.fwmp.service.reports.fr.FrReOpenHistoryReport;
import com.cupola.fwmp.service.reports.fr.FrReport;
import com.cupola.fwmp.vo.fr.FrAllocationReportVo;
import com.cupola.fwmp.vo.fr.FrEtrReportVo;

public interface FrReportDao
{
	 
	public String SQL_TL_PRODUCTIVITY_REPORT_NEW = "SELECT usr.firstName, usr.lastName,usr.id AS userId,"
		    +"(select count(t.id) from FrTicket t where t.currentAssignedTo = usr.id and DATE_FORMAT(t.addedOn,'%Y-%m-%d')  >= DATE_SUB(CURRENT_DATE, INTERVAL DAYOFMONTH(CURRENT_DATE)-1 DAY) and DATE_FORMAT(t.addedOn,'%Y-%m-%d') <= CURRENT_DATE and t.status = 103) as MTD,"
		    +"(select count(t.id) from FrTicket t where t.currentAssignedTo = usr.id and  DATE_FORMAT(t.addedOn,'%Y-%m-%d')  =  CURRENT_DATE and t.status = 103) as FTD," 
		    +"(select count(t.id) from FrTicket t where  t.currentAssignedTo = usr.id and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00') and t.status = 103) as before11am,"
		    +"(select count(t.id) from FrTicket t where  t.currentAssignedTo = usr.id and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00') and t.status = 103) as 11amto1pm,"
		    +"(select count(t.id) from FrTicket t where  t.currentAssignedTo = usr.id and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00') and t.status = 103) as 1pmto3pm,"
		    +"(select count(t.id) from FrTicket t where  t.currentAssignedTo = usr.id and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00') and t.status = 103) as 3pmto5pm,"
		    +"(select count(t.id) from FrTicket t where  t.currentAssignedTo = usr.id and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00') and t.status = 103) as 5pmto7pm,"
		    +"(select count(t.id) from FrTicket t where  t.currentAssignedTo = usr.id and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 23:59:59') and t.status = 103) as 7pmonwards "
		    +"FROM User usr LEFT JOIN FrTicket tick ON tick.currentAssignedTo = usr.id "
		    +"LEFT JOIN FrDetail fr ON fr.ticketId = tick.id "
		    +"INNER JOIN UserGroupUserMapping ugm ON ugm.idUser = usr.id "
		    +"INNER JOIN UserGroup ug ON ug.id = ugm.idUserGroup "
		    +"WHERE usr.status = 1 AND ug.userGroupcode <> 'VENDOR' "
		    +"AND usr.reportTo = :reportTo OR tick.currentAssignedTo = :assignedTo AND tick.status <> :status GROUP by usr.id";

	public String SQL_AM_PRODUCTIVITY_REPORT_NEW = "SELECT u.firstName as tlname,u.id AS userId,"
			+ "(select count(u.id) from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1) as numofnesonfield,"
			+ "(select count(u.id) from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode = 'VENDOR' and u.reportTo = u.id and u.status = 1) as numofvendors,"
			+ "(select count(t.id) from FrTicket t where t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN UserGroup ug ON ug.id = ugm.idUserGroup where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and DATE_FORMAT(t.addedOn,'%Y-%m-%d')  >= DATE_SUB(CURRENT_DATE, INTERVAL DAYOFMONTH(CURRENT_DATE)-1 DAY) and DATE_FORMAT(t.addedOn,'%Y-%m-%d') <= CURRENT_DATE and t.status = 103) as MTD,"
			+ "(select count(t.id) from FrTicket t where t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN UserGroup ug ON ug.id = ugm.idUserGroup where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and  DATE_FORMAT(t.addedOn,'%Y-%m-%d')  =  CURRENT_DATE and t.status = 103) as FTD ,"
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN     UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 00:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00') and t.status = 103) as before11am,"
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN     UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 11:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00') and t.status = 103) as 11amto1pm,"
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN     UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 13:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00') and t.status = 103) as 1pmto3pm,"
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN     UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 15:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00') and t.status = 103) as 3pmto5pm,"
			+ " (select count(t.id) from FrTicket t where  t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN     UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00') and t.status = 103) as 5pmto7pm,"
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo in ( select u.id from User u inner join UserGroupUserMapping ugm ON ugm.idUser = u.id INNER JOIN     UserGroup ug ON ug.id = ugm.idUserGroup  where ug.userGroupcode <> 'VENDOR' and u.reportTo = u.id and u.status = 1 ) and  t.addedOn  >= DATE_FORMAT(NOW(),'%Y-%m-%d 19:00:00') and t.addedOn < DATE_FORMAT(NOW(),'%Y-%m-%d 23:59:59') and t.status = 103) as 7pmonwards "
			+ "FROM User u LEFT JOIN FrTicket tick ON tick.currentAssignedTo = u.id "
			+ "LEFT JOIN  FrDetail fr ON fr.ticketId = tick.id "
			+ "INNER JOIN UserGroupUserMapping ugm ON ugm.idUser = u.id "
			+ "INNER JOIN UserGroup ug ON ug.id = ugm.idUserGroup "
			+ "WHERE u.status = 1 AND ug.userGroupcode <> 'VENDOR' "
			+ "OR tick.currentAssignedTo = :assignedTo AND tick.status  <> :status GROUP by u.id";     
								
	public String SQL_TL_ETR_REPORT_NEW =  "SELECT u.firstName,u.lastName,u.id AS userId,"
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo= u.id) as OpenTicket, t.etrElapsedCount, "
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo = u.id and t.etrElapsedCount=1 ) as oneTime, "
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo = u.id and t.etrElapsedCount=2 ) as twoTime, "
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo = u.id and t.etrElapsedCount=3 ) as threeTime, "
			+ "(select count(t.id) from FrTicket t where  t.currentAssignedTo = u.id and t.etrElapsedCount>3 ) as morethanthreeTime "
			+ "FROM FWMPFR.FrTicket t join User u on t.currentAssignedTo=u.id "
			+ "join FrTicketEtr ft on ft.ticketId=t.id where currentAssignedTo= u.id AND t.status  <> :status group by u.id";	        
	        
	// Requirement still not clear for Allocation TL & AM 
	
	public String SQL_TL_ALLOCATION_REPORT = "" ;
	
	public String SQL_AM_ALLOCATION_REPORT = "" ;
	
	
	/*public String SQL_ACT_CLOSED_COMPLAINTS ="SELECT ftr.TicketCreationDate as ValueDateTime,ftr.modifiedOn as LastUpdateDateTime,ftr.workOrderNumber as ComplaintNo,c.mqId,a.areaName, ftr.fxName as FX, ftr.cxName as CX, c.communicationAdderss,"
			+ "u.firstName as Empname, c.firstName as Custname, b.branchName as EntityName,ftr.TicketCategory as ComplaintDesc, ftr.resCode,ftr.TicketCategory as ProblemDesc,ftr.TicketCreationDate as Duration,ftr.TicketCreationDate as Aging,fd.voc,ftr.closedBy,"
			+ "ftr.reopenCount as Reopencount,etr.communicationETR, ftr.TicketClosedTime as resolveddatetime,ftr.defectCode,ftr.subDefectCode  "
			+ "FROM FWMPFR.FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join User u on ftr.currentAssignedTo=u.id "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
			+ "join Branch b on b.id=c.branchId "
			+ "join Area a on ftr.areaId=a.id "
			+ "join FrDetail fd on fd.ticketId=ftr.id";*/
	
	/*public String SQL_ACT_CLOSED_COMPLAINTS ="SELECT ftr.TicketCreationDate as ValueDateTime,ftr.modifiedOn as LastUpdateDateTime,ftr.workOrderNumber as ComplaintNo,c.mqId,a.areaName, ftr.fxName as FX, ftr.cxName as CX, c.communicationAdderss,"
			+ "u.firstName as Empname, c.firstName as Custname, b.branchName as EntityName,ftr.TicketCategory as ComplaintDesc, ftr.resCode,ftr.TicketCategory as ProblemDesc,ftr.TicketCreationDate as Duration,ftr.TicketCreationDate as Aging,fd.voc,ftr.closedBy,"
			+ "ftr.reopenCount as Reopencount,etr.communicationETR, ftr.TicketClosedTime as resolveddatetime,ftr.defectCode,ftr.subDefectCode  "
			+ "FROM FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join User u on ftr.currentAssignedTo=u.id "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
			+ "join Area a on a.id=ftr.areaId "
			+ "join Branch b on b.id=a.branchId "
			+ "join City ct on ct.id=b.cityId "
			+ "join FrDetail fd on fd.ticketId=ftr.id";
	
	public String SQL_ACT_NET_LIGHTWEIGHT ="SELECT ftr.TicketCreationDate as ValueDateTime,ftr.modifiedOn as LastUpdateDateTime,ftr.workOrderNumber as ComplaintNo,c.mqId,a.areaName, ftr.fxName as FX, ftr.cxName as CX, c.communicationAdderss,"
			+ "ftr.status,u.firstName as Empname, c.firstName as Custname,c.mobileNumber, b.branchName ,ftr.TicketCategory as ComplaintDesc,  ftr.TicketCategory as ProblemDesc,ftr.TicketCreationDate as Duration,ftr.TicketCreationDate as Aging,fd.voc,"
			+ "ftr.reopenCount as Reopencount,etr.communicationETR "
			+ "FROM FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join User u on ftr.currentAssignedTo=u.id "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId"
			+ " join Area a on ftr.areaId=a.id "
			+ "join Branch b on b.id=a.branchId "
			+ "join City ct on ct.id=b.cityId "
			+ "join FrDetail fd on fd.ticketId=ftr.id "
			+ "join Status s on ftr.status=s.id" ;

	
	public String SQL_ACT_REASSIGNMENT_HISTORY ="SELECT c.mqId,ftr.workOrderNumber,fd.currentSymptomName,ftr.TicketCreationDate,etr.communicationETR,c.mobileNumber "
			+ "FROM FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
			+ "join Area a on ftr.areaId=a.id "
			+ "join Branch b on b.id=a.branchId "
			+ "join City ct on ct.id=b.cityId "
			+ "join FrDetail fd on fd.ticketId=ftr.id" ; 
	
	
	public String SQL_ACT_REOPEN_HISTORY ="SELECT c.mqId,ftr.workOrderNumber,ftr.TicketCreationDate,ftr.status,ftr.TicketCategory,fd.currentSymptomName,ftr.reopenDateTime,u.empId as EmpCode,u.firstName as EmpName,u.empId as UserId,"
			+ "u.firstName as UserName,ftr.TicketClosedTime,ftr.reopenCount "
			+ "FROM  FrTicket ftr join Customer c on c.id=ftr.customerId "
			+ "join Status s on ftr.status=s.id "
			+ "join FrTicketEtr etr on ftr.id=etr.ticketId "
			+ "join Area a on ftr.areaId=a.id "
			+ "join Branch b on b.id=a.branchId "
			+ "join City ct on ct.id=b.cityId "
			+ "join FrDetail fd on fd.ticketId=ftr.id "
			+ "join User u on ftr.currentAssignedTo=u.id";*/
	
	
	public List<FrProductivityReport> getFrProductivityReportByTlNew(Long tlId);
	public List<FrProductivityReport> getFrProductivityReportByAmNew(Long amId);
	
	public  List<FrEtrReport> getFrEtrReportByTlNew(Long tlId);
	public  List<FrEtrReport> getFrEtrReportByAMNew(Long amId);
	
	// Requirement still not clear for Allocation TL & AM 
	
	public List<FrAllocationReport> getFrAllocationReportByTl(Long tlId);
	public List<FrAllocationReport> getFrAllocationReportByAM(Long amId);
	
	
	 
	/*public  List<FrClosedComplaintsReport> getFrClosedComplaintsReport(String fromDate , String toDate,String branchId,String areaId,String cityID);
	public  List<FrNewLightWeight> getFrNewLightWeightReport(String fromDate , String toDate,String branchId,String areaId,String cityID);
	public  List<FrReAssignmentHistoryReport> getFrReassignmentHistoryReport(String fromDate , String toDate,String branchId,String areaId,String cityID);
	public  List<FrReOpenHistoryReport> getFrReopenHistoryReport(String fromDate , String toDate,String branchId,String areaId,String cityID);*/
	
	public List<?> getDownTimeReportOnOpenTicket();
}
