package com.cupola.fwmp.dao.reports.fr;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.FrReportConstant;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.reports.fr.FrAllocationReport;
import com.cupola.fwmp.service.reports.fr.FrEtrReport;
import com.cupola.fwmp.service.reports.fr.FrProductivityReport;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FrTicketUtil;
 

@Transactional
public class FrReportDaoImpl implements FrReportDao {
	private static Logger LOGGER = Logger.getLogger(FrReportDaoImpl.class
			.getName());

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Autowired
	private UserDao userDao;
	@Autowired
	DBUtil dBUtil;
	@Autowired
	private GlobalActivities globalActivities;

	@Override
	public List<?> getDownTimeReportOnOpenTicket() {

		return null;
	}

	public List<FrProductivityReport> getFrProductivityReportByTlNew(Long tlId) {
		if (tlId == null)
			return new ArrayList<FrProductivityReport>();

		List<Object[]> listObject = null;
		try {

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrReportDao.SQL_TL_PRODUCTIVITY_REPORT_NEW)
					.setParameter("reportTo", tlId)
					.setParameter("assignedTo", tlId)
					.setParameter("status", FRStatus.FR_UNASSIGNEDT_TICKET);

			LOGGER.info("SQL QUERY FOR PRODUCTIVITY TL REPORT : "
					+ query.getQueryString());

			listObject = query.list();

		} catch (Exception exception) {
			LOGGER.error("Error occure while getting productivity report by TL..."
					+ exception);
		}
		return xformToFrProductivityReportTLNew(listObject);
		// return xformToFrAllocationReport(listObject);
	}

	@Override
	public List<FrProductivityReport> getFrProductivityReportByAmNew(Long amId) {

		if (amId == null)
			return new ArrayList<FrProductivityReport>();

		List<Object[]> listObject = null;
		try {

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrReportDao.SQL_AM_PRODUCTIVITY_REPORT_NEW)
					// .setParameter("id", amId)
					.setParameter("assignedTo", amId)
					.setParameter("status", FRStatus.FR_UNASSIGNEDT_TICKET);

			LOGGER.info("SQL QUERY FOR  PRODUCTIVITY AM REPORT : "
					+ query.getQueryString());

			listObject = query.list();

		} catch (Exception exception) {
			LOGGER.error("Error occure while getting productivity report by AM..."
					+ exception);
		}
		return xformToFrProductivityReportNewAm(listObject);
	}

	@Override
	public List<FrEtrReport> getFrEtrReportByTlNew(Long tlId) {

		if (tlId == null)
			return new ArrayList<FrEtrReport>();

		List<Object[]> listObject = null;
		try {

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrReportDao.SQL_TL_ETR_REPORT_NEW)
					// .setParameter("assignedTo", tlId);
					.setParameter("status", FRStatus.FR_UNASSIGNEDT_TICKET);

			LOGGER.info("SQL QUERY FOR ETRTEMPLATE TL REPORT : "
					+ query.getQueryString());

			listObject = query.list();

		} catch (Exception exception) {
			LOGGER.error("Error occure while getting ETR report by TL..."
					+ exception);
		}
		return xformToFrEtrReportNew(listObject);

	}

	@Override
	public List<FrEtrReport> getFrEtrReportByAMNew(Long amId) {
		if (amId == null)
			return new ArrayList<FrEtrReport>();

		List<Object[]> listObject = null;
		try {

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrReportDao.SQL_TL_ETR_REPORT_NEW)
					// .setParameter("assignedTo", tlId);
					.setParameter("status", FRStatus.FR_UNASSIGNEDT_TICKET);

			LOGGER.info("SQL QUERY FOR ETRTEMPLATE AM REPORT : "
					+ query.getQueryString());

			listObject = query.list();

		} catch (Exception exception) {
			LOGGER.error("Error occure while getting ETR report by AM..."
					+ exception);
		}
		return xformToFrEtrReportAMNew(listObject);

	}

	@Override
	public List<FrAllocationReport> getFrAllocationReportByTl(Long tlId) {

		if (tlId == null)
			return new ArrayList<FrAllocationReport>();

		List<Object[]> listObject = null;
		try {

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrReportDao.SQL_TL_ALLOCATION_REPORT)
			/*
			 * .setParameter("reportTo", tlId) .setParameter("assignedTo", tlId)
			 * .setParameter("status", FRStatus.FR_UNASSIGNEDT_TICKET)
			 */;

			LOGGER.info("SQL QUERY FOR ALLOCATION TL REPORT : "
					+ query.getQueryString());

			listObject = query.list();

		} catch (Exception exception) {
			LOGGER.error("Error occure while getting allocation report by TL..."
					+ exception);
		}
		return xformToFrAllocationReportTL(listObject);

	}

	@Override
	public List<FrAllocationReport> getFrAllocationReportByAM(Long amId) {
		if (amId == null)
			return new ArrayList<FrAllocationReport>();

		List<Object[]> listObject = null;
		try {

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrReportDao.SQL_AM_ALLOCATION_REPORT)
					// .setParameter("assignedTo", tlId);
					.setParameter("status", FRStatus.FR_UNASSIGNEDT_TICKET);

			LOGGER.info("SQL QUERY FOR ALLOCATETEMPLATE AM REPORT : "
					+ query.getQueryString());

			listObject = query.list();

		} catch (Exception exception) {
			LOGGER.error("Error occure while getting ETR report by AM..."
					+ exception);
		}
		return xformToFrAllocationReportAM(listObject);
	}

	private List<FrProductivityReport> xformToFrProductivityReportTLNew(
			List<Object[]> listObject) {
		if (listObject == null)
			return new ArrayList<FrProductivityReport>();

		FrProductivityReport productivityReport = null;
		List<FrProductivityReport> reportObject = new ArrayList<FrProductivityReport>();
		List<FrProductivityReport> cxDownTlReport = new ArrayList<FrProductivityReport>();
		List<FrProductivityReport> cxUpTlReport = new ArrayList<FrProductivityReport>();

		for (Object[] db : listObject) {
			productivityReport = new FrProductivityReport();

			productivityReport.setUserName(FrTicketUtil.objectToString(db[0])
					+ " " + FrTicketUtil.objectToString(db[1]));

			productivityReport.setUserId(FrTicketUtil.objectToLong(db[2]));
			productivityReport.setMtd(FrTicketUtil.objectToInteger(db[3]));
			productivityReport.setTotalFtd(FrTicketUtil.objectToInteger(db[4]));
			productivityReport.setBefore11Am(FrTicketUtil
					.objectToInteger(db[5]));
			productivityReport.setBetween11AmTo1Pm(FrTicketUtil
					.objectToInteger(db[6]));
			productivityReport.setBetween1PmTo3Pm(FrTicketUtil
					.objectToInteger(db[7]));
			productivityReport.setBetween3PmTo5Pm(FrTicketUtil
					.objectToInteger(db[8]));
			productivityReport.setBetween5PmTo7Pm(FrTicketUtil
					.objectToInteger(db[9]));
			productivityReport
					.setAfter7Pm(FrTicketUtil.objectToInteger(db[10]));

			if (AuthUtils.isFRTLUser()) {
				if (AuthUtils.isFrCxDownNEUser(productivityReport.getUserId())) {
					// allocationReport.setType("");
					productivityReport.setType(FrReportConstant.CX_DOWN_KEY);
					cxDownTlReport.add(productivityReport);
				} else if (AuthUtils.isFrCxUpNEUser(productivityReport
						.getUserId())) {
					productivityReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(productivityReport);
				}
			} else {
				if (AuthUtils.isFrCxDownNEUser(productivityReport.getUserId())) {
					productivityReport.setType(FrReportConstant.CX_DOWN_KEY);
					// allocationReport.setType("");
					cxDownTlReport.add(productivityReport);
				} else if (AuthUtils.isFrCxUpNEUser(productivityReport
						.getUserId())) {
					productivityReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(productivityReport);
				}
			}

		}

		reportObject.addAll(cxUpTlReport);
		if (!reportObject.isEmpty()) {
			reportObject.get(0).setType(FrReportConstant.CX_UP_KEY);
		}
		if (!(cxDownTlReport.isEmpty())) {
			cxDownTlReport.get(0).setType(FrReportConstant.CX_DOWN_KEY);
		}
		reportObject.addAll(cxDownTlReport);

		return reportObject;
	}

	private List<FrProductivityReport> xformToFrProductivityReportNewAm(
			List<Object[]> listObject) {
		if (listObject == null)
			return new ArrayList<FrProductivityReport>();

		FrProductivityReport productivityReport = null;
		List<FrProductivityReport> reportObject = new ArrayList<FrProductivityReport>();
		List<FrProductivityReport> cxDownTlReport = new ArrayList<FrProductivityReport>();
		List<FrProductivityReport> cxUpTlReport = new ArrayList<FrProductivityReport>();

		for (Object[] db : listObject) {
			productivityReport = new FrProductivityReport();

			productivityReport.setUserName(FrTicketUtil.objectToString(db[0]));
			productivityReport.setUserId(FrTicketUtil.objectToLong(db[1]));
			productivityReport.setNumberOfNeonField(FrTicketUtil
					.objectToInteger(db[2]));
			productivityReport.setNumberOfVendors(FrTicketUtil
					.objectToInteger(db[3]));
			productivityReport.setMtd(FrTicketUtil.objectToInteger(db[4]));
			productivityReport.setTotalFtd(FrTicketUtil.objectToInteger(db[5]));
			productivityReport.setBefore11Am(FrTicketUtil
					.objectToInteger(db[6]));
			productivityReport.setBetween11AmTo1Pm(FrTicketUtil
					.objectToInteger(db[7]));
			productivityReport.setBetween1PmTo3Pm(FrTicketUtil
					.objectToInteger(db[8]));
			productivityReport.setBetween3PmTo5Pm(FrTicketUtil
					.objectToInteger(db[9]));
			productivityReport.setBetween5PmTo7Pm(FrTicketUtil
					.objectToInteger(db[10]));
			productivityReport
					.setAfter7Pm(FrTicketUtil.objectToInteger(db[11]));

			if (AuthUtils.isAreaManager()) {
				if (AuthUtils.isFrCxDownNEUser(productivityReport.getUserId())) {
					// allocationReport.setType("");
					productivityReport.setType(FrReportConstant.CX_DOWN_KEY);
					cxDownTlReport.add(productivityReport);
				} else if (AuthUtils.isFrCxUpNEUser(productivityReport
						.getUserId())) {
					productivityReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(productivityReport);
				}
			} else {
				if (AuthUtils.isFrCxDownNEUser(productivityReport.getUserId())) {
					productivityReport.setType(FrReportConstant.CX_DOWN_KEY);
					// allocationReport.setType("");
					cxDownTlReport.add(productivityReport);
				} else if (AuthUtils.isFrCxUpNEUser(productivityReport
						.getUserId())) {
					productivityReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(productivityReport);
				}
			}

		}

		reportObject.addAll(cxUpTlReport);
		if (!reportObject.isEmpty()) {
			reportObject.get(0).setType(FrReportConstant.CX_UP_KEY);
		}
		if (!(cxDownTlReport.isEmpty())) {
			cxDownTlReport.get(0).setType(FrReportConstant.CX_DOWN_KEY);
		}
		reportObject.addAll(cxDownTlReport);

		return reportObject;
	}

	private List<FrEtrReport> xformToFrEtrReportNew(List<Object[]> listObject) {
		if (listObject == null)
			return new ArrayList<FrEtrReport>();

		FrEtrReport etrReport = null;
		List<FrEtrReport> reportObject = new ArrayList<FrEtrReport>();
		List<FrEtrReport> cxDownTlReport = new ArrayList<FrEtrReport>();
		List<FrEtrReport> cxUpTlReport = new ArrayList<FrEtrReport>();

		for (Object[] db : listObject) {
			etrReport = new FrEtrReport();

			etrReport.setUserName(FrTicketUtil.objectToString(db[0]) + " "
					+ FrTicketUtil.objectToString(db[1]));

			etrReport.setUserId(FrTicketUtil.objectToLong(db[2]));
			etrReport.setTotalOpenTicket(FrTicketUtil.objectToInteger(db[3]));
			etrReport.setEtrExpired(FrTicketUtil.objectToInteger(db[4]));
			etrReport.setOneTimes(FrTicketUtil.objectToInteger(db[5]));
			etrReport.setTwoTimes(FrTicketUtil.objectToInteger(db[6]));
			etrReport.setThreeTimes(FrTicketUtil.objectToInteger(db[7]));
			etrReport.setMoreThan3Times(FrTicketUtil.objectToInteger(db[8]));

			if (AuthUtils.isFRTLUser()) {
				if (AuthUtils.isFrCxDownNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_DOWN_KEY);
					cxDownTlReport.add(etrReport);
				} else if (AuthUtils.isFrCxUpNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(etrReport);
				}
			} else {
				if (AuthUtils.isFrCxDownNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_DOWN_KEY);
					// allocationReport.setType("");
					cxDownTlReport.add(etrReport);
				} else if (AuthUtils.isFrCxUpNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(etrReport);
				}
			}

		}

		reportObject.addAll(cxUpTlReport);
		if (!reportObject.isEmpty()) {
			reportObject.get(0).setType(FrReportConstant.CX_UP_KEY);
		}
		if (!(cxDownTlReport.isEmpty())) {
			cxDownTlReport.get(0).setType(FrReportConstant.CX_DOWN_KEY);
		}
		reportObject.addAll(cxDownTlReport);

		return reportObject;
	}

	private List<FrEtrReport> xformToFrEtrReportAMNew(List<Object[]> listObject) {
		if (listObject == null)
			return new ArrayList<FrEtrReport>();

		FrEtrReport etrReport = null;
		List<FrEtrReport> reportObject = new ArrayList<FrEtrReport>();
		List<FrEtrReport> cxDownTlReport = new ArrayList<FrEtrReport>();
		List<FrEtrReport> cxUpTlReport = new ArrayList<FrEtrReport>();

		for (Object[] db : listObject) {
			etrReport = new FrEtrReport();

			etrReport.setUserName(FrTicketUtil.objectToString(db[0]) + " "
					+ FrTicketUtil.objectToString(db[1]));

			etrReport.setUserId(FrTicketUtil.objectToLong(db[2]));
			etrReport.setTotalOpenTicket(FrTicketUtil.objectToInteger(db[3]));
			etrReport.setEtrExpired(FrTicketUtil.objectToInteger(db[4]));
			etrReport.setOneTimes(FrTicketUtil.objectToInteger(db[5]));
			etrReport.setTwoTimes(FrTicketUtil.objectToInteger(db[6]));
			etrReport.setThreeTimes(FrTicketUtil.objectToInteger(db[7]));
			etrReport.setMoreThan3Times(FrTicketUtil.objectToInteger(db[8]));

			if (AuthUtils.isAreaManager()) {
				if (AuthUtils.isFrCxDownNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_DOWN_KEY);
					cxDownTlReport.add(etrReport);
				} else if (AuthUtils.isFrCxUpNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(etrReport);
				}
			} else {
				if (AuthUtils.isFrCxDownNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_DOWN_KEY);
					// allocationReport.setType("");
					cxDownTlReport.add(etrReport);
				} else if (AuthUtils.isFrCxUpNEUser(etrReport.getUserId())) {
					etrReport.setType(FrReportConstant.CX_UP_KEY);
					// allocationReport.setType("");
					cxUpTlReport.add(etrReport);
				}
			}

		}

		reportObject.addAll(cxUpTlReport);
		if (!reportObject.isEmpty()) {
			reportObject.get(0).setType(FrReportConstant.CX_UP_KEY);
		}
		if (!(cxDownTlReport.isEmpty())) {
			cxDownTlReport.get(0).setType(FrReportConstant.CX_DOWN_KEY);
		}
		reportObject.addAll(cxDownTlReport);

		return reportObject;
	}

	private List<FrAllocationReport> xformToFrAllocationReportTL(
			List<Object[]> listObject) {
		if (listObject == null)
			return new ArrayList<FrAllocationReport>();

		FrAllocationReport allocationReport = null;
		List<FrAllocationReport> reportObject = new ArrayList<FrAllocationReport>();
		List<FrAllocationReport> cxDownTlReport = new ArrayList<FrAllocationReport>();
		List<FrAllocationReport> cxUpTlReport = new ArrayList<FrAllocationReport>();

		for (Object[] db : listObject) { 
		}
		 

		return reportObject;
	}

	private List<FrAllocationReport> xformToFrAllocationReportAM(
			List<Object[]> listObject) {
		if (listObject == null)
			return new ArrayList<FrAllocationReport>();

		FrAllocationReport allocationReport = null;
		List<FrAllocationReport> reportObject = new ArrayList<FrAllocationReport>();
		List<FrAllocationReport> cxDownTlReport = new ArrayList<FrAllocationReport>();
		List<FrAllocationReport> cxUpTlReport = new ArrayList<FrAllocationReport>();

		for (Object[] db : listObject) { 
		}

		return reportObject;
	}


	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.fr.FrReportDao#getFrClosedComplaintsReportByTl(java.lang.Long)
	 */
	/*@Override
	public List<FrClosedComplaintsReport> getFrClosedComplaintsReport(String fromDate , String toDate,String branchId ,String areaId,String cityID) {
		// TODO Auto-generated method stub
		
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(FrReportDao.SQL_ACT_CLOSED_COMPLAINTS);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		
		
		return xformToFrActClosedComplaintsReport(list);
	}
	
	private List<FrClosedComplaintsReport> xformToFrActClosedComplaintsReport(
			List<Object[]> list) {
		if (list == null)
			return new ArrayList<FrClosedComplaintsReport>();

		FrClosedComplaintsReport closedComplaintsReport = null;
		List<FrClosedComplaintsReport> closedComplaintsreportObject = new ArrayList<FrClosedComplaintsReport>();

		for (Object[] db : list) {
			closedComplaintsReport = new FrClosedComplaintsReport();

			
			closedComplaintsReport.setTicketCreationDate(FrTicketUtil.objectToString(db[0]));
			closedComplaintsReport.setModifiedOn(FrTicketUtil.objectToString(db[1])); 
			closedComplaintsReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[2]));
			closedComplaintsReport.setMqId(FrTicketUtil.objectToString(db[3]));
			closedComplaintsReport.setAreaName(FrTicketUtil.objectToString(db[4]));
			closedComplaintsReport.setFxName(FrTicketUtil.objectToString(db[5]));
			closedComplaintsReport.setCxName(FrTicketUtil.objectToString(db[6]));
			closedComplaintsReport.setCommunicationAdderss(FrTicketUtil.objectToString(db[7]));
			closedComplaintsReport.setFirstName(FrTicketUtil.objectToString(db[8]));
			closedComplaintsReport.setFirstName(FrTicketUtil.objectToString(db[9]));
			closedComplaintsReport.setBranchName(FrTicketUtil.objectToString(db[10]));
			closedComplaintsReport.setTicketCategory(FrTicketUtil.objectToString(db[11]));
			closedComplaintsReport.setResCode(FrTicketUtil.objectToString(db[12]));
			closedComplaintsReport.setTicketCategory(FrTicketUtil.objectToString(db[13]));
			closedComplaintsReport.setTicketCreationDate(FrTicketUtil.objectToString(db[14]));
			closedComplaintsReport.setTicketCreationDate(FrTicketUtil.objectToString(db[15]));
			closedComplaintsReport.setVoc(FrTicketUtil.objectToString(db[16]));
			closedComplaintsReport.setClosedBy(FrTicketUtil.objectToString(db[17]));
			closedComplaintsReport.setReopenCount(FrTicketUtil.objectToString(db[18]));
			closedComplaintsReport.setCommunicationETR(FrTicketUtil.objectToString(db[19]));
			closedComplaintsReport.setTicketClosedTime(FrTicketUtil.objectToString(db[20]));
			closedComplaintsReport.setDefectCode(FrTicketUtil.objectToString(db[21]));
			closedComplaintsReport.setSubDefectCode(FrTicketUtil.objectToString(db[22]));
			
			closedComplaintsreportObject.add(closedComplaintsReport);
		}

		return closedComplaintsreportObject;
	}

	 (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.fr.FrReportDao#getFrNewLightWeightReportByTl(java.lang.Long)
	 
	@Override
	public List<FrNewLightWeight> getFrNewLightWeightReport(String fromDate , String toDate,String branchId ,String areaId,String cityID) {
		// TODO Auto-generated method stub

		 
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(FrReportDao.SQL_ACT_NET_LIGHTWEIGHT);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		return xformToFrActNetLightWeightReport(list);
	}

	private List<FrNewLightWeight> xformToFrActNetLightWeightReport(
			List<Object[]> list) {
		if (list == null)
			return new ArrayList<FrNewLightWeight>();

		FrNewLightWeight netLightWeightReport = null;
		List<FrNewLightWeight> netLightWeightreportObject = new ArrayList<FrNewLightWeight>();

		for (Object[] db : list) {
			netLightWeightReport = new FrNewLightWeight();

			
			netLightWeightReport.setTicketCreationDate(FrTicketUtil.objectToString(db[0]));
			netLightWeightReport.setModifiedOn(FrTicketUtil.objectToString(db[1])); 
			netLightWeightReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[2]));
			netLightWeightReport.setMqId(FrTicketUtil.objectToString(db[3]));
			netLightWeightReport.setAreaName(FrTicketUtil.objectToString(db[4]));
			netLightWeightReport.setFxName(FrTicketUtil.objectToString(db[5]));
			netLightWeightReport.setCxName(FrTicketUtil.objectToString(db[6]));
			netLightWeightReport.setCommunicationAdderss(FrTicketUtil.objectToString(db[7]));
			netLightWeightReport.setStatus(FrTicketUtil.objectToString(db[8]));
			netLightWeightReport.setFirstName(FrTicketUtil.objectToString(db[9]));
			netLightWeightReport.setFirstName(FrTicketUtil.objectToString(db[10]));
			netLightWeightReport.setMobileNumber(FrTicketUtil.objectToString(db[11]));
			netLightWeightReport.setBranchName(FrTicketUtil.objectToString(db[12]));
			netLightWeightReport.setTicketCategory(FrTicketUtil.objectToString(db[13]));
			netLightWeightReport.setTicketCategory(FrTicketUtil.objectToString(db[14]));
			netLightWeightReport.setTicketCreationDate(FrTicketUtil.objectToString(db[15]));
			netLightWeightReport.setTicketCreationDate(FrTicketUtil.objectToString(db[16]));
			netLightWeightReport.setVoc(FrTicketUtil.objectToString(db[17]));
			netLightWeightReport.setReopenCount(FrTicketUtil.objectToString(db[18]));
			netLightWeightReport.setCommunicationETR(FrTicketUtil.objectToString(db[19]));
			 
			netLightWeightreportObject.add(netLightWeightReport);
		}

		return netLightWeightreportObject;
	}

	 (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.fr.FrReportDao#getFrReassignmentHistoryReport(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 
	@Override
	public List<FrReAssignmentHistoryReport> getFrReassignmentHistoryReport(
			String fromDate, String toDate, String branchId, String areaId,
			String cityID) {
		// TODO Auto-generated method stub
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(FrReportDao.SQL_ACT_REASSIGNMENT_HISTORY);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		return xformToFrActReassignmentHistoryReport(list);
	}

	*//**@author leela
	 * List<FrReAssignmentHistoryReport>
	 * @param list
	 * @return
	 *//*
	private List<FrReAssignmentHistoryReport> xformToFrActReassignmentHistoryReport(
			List<Object[]> list) {
		// TODO Auto-generated method stub
		 
		if (list == null)
			return new ArrayList<FrReAssignmentHistoryReport>();

		FrReAssignmentHistoryReport reAssignmentReport = null;
		List<FrReAssignmentHistoryReport> reAssignmentreportObject = new ArrayList<FrReAssignmentHistoryReport>();

		for (Object[] db : list) {
			
			reAssignmentReport = new FrReAssignmentHistoryReport();

			reAssignmentReport.setMqId(FrTicketUtil.objectToString(db[0]));
			reAssignmentReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[1]));
			reAssignmentReport.setCurrentSymptomName(FrTicketUtil.objectToString(db[2]));
			reAssignmentReport.setTicketCreationDate(FrTicketUtil.objectToString(db[3]));
			reAssignmentReport.setCommunicationETR(FrTicketUtil.objectToString(db[4]));
			reAssignmentReport.setMobileNumber(FrTicketUtil.objectToString(db[5]));
			
			 
			 
			reAssignmentreportObject.add(reAssignmentReport);
		}

		return reAssignmentreportObject;
		
		
	}

	 (non-Javadoc)
	 * @see com.cupola.fwmp.dao.reports.fr.FrReportDao#getFrReopenHistoryReport(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 
	@Override
	public List<FrReOpenHistoryReport> getFrReopenHistoryReport(
			String fromDate, String toDate, String branchId, String areaId,
			String cityID) {
		// TODO Auto-generated method stub
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate)) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try {
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e) {
					LOGGER.error("Error While parsing date :" + e.getMessage());
					e.printStackTrace();
				}
			}
			StringBuffer queryString = new StringBuffer();

			queryString.append(FrReportDao.SQL_ACT_REOPEN_HISTORY);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate))) {

				datesPresent = true;

				queryString
						.append(" and ftr.TicketCreationDate>=:fromDate and ftr.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId)) {
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId)) {
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityID)) {
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			LOGGER.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent) {

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent) {

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent) {
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent) {
				Long city = Long.parseLong(cityID);
				query.setLong("cityId", city);
			}

			if (!cityPresent) {
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			LOGGER.info("query : " + query);

			list = query.list();

			LOGGER.info("list size: " + list.size());

		} catch (Exception e) {
			LOGGER.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		LOGGER.info("TIme Taken for db call and getting results in milliseconds "
				+ timeDiff);
		return xformToFrActReopenHistoryReport(list);
	}

	*//**@author leela
	 * List<FrReOpenHistoryReport>
	 * @param list
	 * @return
	 *//*
	private List<FrReOpenHistoryReport> xformToFrActReopenHistoryReport(
			List<Object[]> list) {
		// TODO Auto-generated method stub
		if (list == null)
			return new ArrayList<FrReOpenHistoryReport>();

		FrReOpenHistoryReport reopenReport = null;
		List<FrReOpenHistoryReport> reopenreportObject = new ArrayList<FrReOpenHistoryReport>();

		for (Object[] db : list) {
			
			reopenReport = new FrReOpenHistoryReport();

			reopenReport.setMqId(FrTicketUtil.objectToString(db[0]));
			reopenReport.setWorkOrderNumber(FrTicketUtil.objectToString(db[1]));
			reopenReport.setTicketCreationDate(FrTicketUtil.objectToString(db[2]));
			reopenReport.setStatus(FrTicketUtil.objectToString(db[3]));
			reopenReport.setTicketCategory(FrTicketUtil.objectToString(db[4]));
			reopenReport.setCurrentSymptomName(FrTicketUtil.objectToString(db[5]));
			reopenReport.setReopenDateTime(FrTicketUtil.objectToString(db[6]));
			reopenReport.setEmpId(FrTicketUtil.objectToString(db[7]));
			reopenReport.setFirstName(FrTicketUtil.objectToString(db[8]));
			reopenReport.setEmpId(FrTicketUtil.objectToString(db[9]));
			reopenReport.setFirstName(FrTicketUtil.objectToString(db[10]));
			reopenReport.setTicketClosedTime(FrTicketUtil.objectToString(db[11]));
			reopenReport.setReopenCount(FrTicketUtil.objectToString(db[12]));
			
			
			
			 
			reopenreportObject.add(reopenReport);
		}

		return reopenreportObject;
	}
*/
	 

}
