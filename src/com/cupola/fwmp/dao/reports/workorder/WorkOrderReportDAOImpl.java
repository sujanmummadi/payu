package com.cupola.fwmp.dao.reports.workorder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.reports.vo.WorkOrderReport;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.SubActivityVo;
import com.cupola.fwmp.vo.UserVo;
import com.google.gson.Gson;

@Transactional
public class WorkOrderReportDAOImpl implements WorkOrderReportDAO
{

	HibernateTemplate hibernateTemplate;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernateTemplate = hibernetTemplate;

	}

	@Autowired
	TicketActivityLogDAO activityLogDAO;

	@Autowired
	GisDAO gisDAO;

	@Autowired
	TicketDAO ticketDAO;
	@Autowired
	UserDao userDao;

	private static Logger log = Logger
			.getLogger(WorkOrderReportDAOImpl.class.getName());

	public String WORKORDER_REPORT_QUERY_WITHOUT_DATES = "SELECT t.prospectNo as prospectnumber,c.firstName as firstNam,c.currentAddress as currentAdd,c.mobileNumber  "
			+ "as mobile,a.areaName as area,s.status as sta,w.workOrderNumber as workored,c.mqId as mid,t.TicketCreationDate "
			+ "as tcreationdate,  w.addedOn as work_added_on,ws.workStageName,w.addedOn as wclosedon,  c.remarks, u.id,t.id AS TicketId  "
			+ " , b.branchName,ct.cityName , c.middleName,c.lastName ,"
			+ "(select tal.addedBy from TicketActivityLog tal inner join Activity ugm on tal.activityId = ugm.id where  tal.ticketId = t.id   and tal.activityId = 3 Group By tal.ticketId order by tal.addedOn  desc) as cxRackFixing,"
			+ "(select tal.addedBy from TicketActivityLog tal inner join Activity ugm on tal.activityId = ugm.id where  tal.ticketId = t.id   and tal.activityId = 8 Group By tal.ticketId order by tal.addedOn  desc) as cxRackMat,"
			+ "(select tmm.serialNo from TicketMaterialMapping tmm where tmm.ticketId = t.id and tmm.materialId = 1003 ) as batteryDetails,"
			+ "(select tal.addedOn from TicketActivityLog tal  where  tal.ticketId = t.id   and tal.activityId = 2 order by tal.addedOn desc limit 1) as woClosedDate,"
			+ "(select tal.addedBy from TicketActivityLog tal inner join Activity ugm on tal.activityId = ugm.id where  tal.ticketId = t.id   and tal.activityId = 9 Group By tal.ticketId order by tal.addedOn  desc) as feasibilityGiven,"
			+ "(select tal.addedBy from TicketActivityLog tal inner join Activity ugm on tal.activityId = ugm.id where  tal.ticketId = t.id   and tal.activityId = 6  order by tal.addedOn  desc limit 1) as fiberDone,"
			+ "(select tal.addedBy from TicketActivityLog tal inner join Activity ugm on tal.activityId = ugm.id where  tal.ticketId = t.id   and tal.activityId = 7  order by tal.addedOn  desc limit 1) as spliciingDone,"
			+ "(select tal.value from TicketActivityLog tal inner join Activity ugm on tal.activityId = ugm.id where  tal.ticketId = t.id   and tal.activityId = 7 and tal.subactivityId = 16  order by tal.addedOn  desc limit 1) as numOfSplicing,"
			+ "(select g.fxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4   order by g.addedOn desc  limit 1) as fxName,"
			+ "(select g.cxName from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4   order by g.addedOn desc  limit 1) as cxName,"
			+ "(select g.fxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4   order by g.addedOn desc  limit 1) as fxPorts,"
			+ "(select g.cxPorts from Gis g where  g.TicketNo = t.id   and g.feasibilityType =4   order by g.addedOn desc  limit 1) as cxPorts,"
			+ "(select g.addedOn from Gis g where  g.TicketNo = t.id   and g.feasibilityType =3   order by g.addedOn desc  limit 1) as feasibilityDate "

			+ "FROM Ticket t  inner JOIN WorkOrder w on  w.ticketId=t.id inner JOIN Customer c on t.customerId=c.id  "
			+ "LEFT JOIN User u on u.id = t.currentAssignedTo LEFT JOIN Area a on a.id=c.AreaId"
			+ " LEFT JOIN Branch b on b.id=a.branchId LEFT JOIN City ct on ct.id=b.cityId "
			+ " LEFT JOIN Status s on s.id=t.status LEFT JOIN WorkStage ws on ws.id = w.currentWorkStage  where t.source in ('Call Centre','Door Knock','Tool') ";

	public Map<String, Map<String, String>> ticketActivityVendorMap = new HashMap<String, Map<String, String>>();

	public List<WorkOrderReport> getWorkOrderReports(String fromDate,
			String toDate, String branchId, String areaId, String cityId)
	{
		log.debug(" inside  getWorkOrderReports "   + cityId);
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try
		{

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;


			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate))
			{
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try
				{
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e)
				{
					log.error("Error While parsing dateformate :"
							+ e.getMessage());
					e.printStackTrace();
				}
			}

			StringBuffer queryString = new StringBuffer();

			queryString.append(WORKORDER_REPORT_QUERY_WITHOUT_DATES);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate)))
			{

				datesPresent = true;

				queryString
						.append(" and w.addedOn >=:fromDate and w.addedOn <=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId))
			{
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId))
			{
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityId))
			{
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent)
			{
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			log.info("query string: " + queryString);

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());

			if (datesPresent)
			{

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent)
			{

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent)
			{
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent)
			{
				Long city = Long.parseLong(cityId);
				query.setLong("cityId", city);
			}

			if (!cityPresent)
			{
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			list = query.list();

			log.info("list size: " + list.size());

		} catch (Exception e)
		{
			log.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and
			// workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		log.info("TIme Taken for db call and getting results in milliseconds"
				+ timeDiff);

		log.debug(" Exit From  getWorkOrderReports "   + cityId);
		return getWorkOrderList(list);

	}

	private List<WorkOrderReport> getWorkOrderList(List<Object[]> list)
	{

		if (list == null)
			return null;
		log.debug(" inside  getWorkOrderList "   + list.size());
		List<WorkOrderReport> workOrderList = new ArrayList<WorkOrderReport>();

		WorkOrderReport workOrderReport = null;

		log.info("Work order list size ++++ " + list.size());

		for (Object[] workOrder : list)
		{
			try
			{
				workOrderReport = new WorkOrderReport();
				// t.prospectNo as prospectnumber,c.firstName as
				// firstNam,c.currentAddress as currentAdd,c.mobileNumber
				// as mobile,a.areaName as area,s.status as
				// sta,w.workOrderNumber as
				// workored,c.mqId as mid,t.TicketCreationDate
				// as tcreationdate, w.addedOn as work_added_on,w.addedOn as
				// wclosedon, t.remarks, t.id AS TicketId


				workOrderReport.setDcrNo(objectToString(workOrder[0])); // prospectNo
																		// from
																		// Ticket
																		// 17
																		// 18table
				workOrderReport.setCustomerName(objectToString(workOrder[1])); // firstName
																				// from
																				// Customer
																				// table
				if (objectToString(workOrder[17]) != null)
					workOrderReport.setCustomerName(workOrderReport
							.getCustomerName()
							.concat(" " + objectToString(workOrder[17])));
				else if (objectToString(workOrder[18]) != null)
					workOrderReport.setCustomerName(workOrderReport
							.getCustomerName()
							.concat(" " + objectToString(workOrder[18])));

				workOrderReport
						.setCustomerAddress(objectToString(workOrder[2])); // currentAddress
																			// from
																			// Customer
																			// table
				workOrderReport.setPhoneMobile(objectToString(workOrder[3])); // mobileNumber
																				// from
																				// Customer
																				// table
				workOrderReport.setArea(objectToString(workOrder[4])); // areaName
																		// from
																		// Area
																		// table
				workOrderReport.setSubstatusId(objectToString(workOrder[5]));// status
																				// from
																				// Status
																				// table
				workOrderReport.setWorkOrderNo(objectToString(workOrder[6])); // workOrderNumber
																				// from
																				// WorkOrder
																				// table
				workOrderReport.setMqId(objectToString(workOrder[7])); // mqId
																		// from
																		// Customer
																		// table
				workOrderReport.setApproachDate(objectToString(workOrder[8]));// TicketCreationDate
																				// from
																				// Ticket
																				// table
				workOrderReport
						.setWoGeneratedDate(objectToString(workOrder[9])); // addedOn
																			// from
																			// WorkOrder
																			// table
				workOrderReport.setWoType(objectToString(workOrder[10])); // workOrderType
																			// from
																			// workOrderType
																			// table
				workOrderReport.setRemarks(objectToString(workOrder[12]));// remarks
																			// from
																			// Ticket
																			// table

				if (objectToString(workOrder[13]) != null)
				{
					UserVo vo = userDao.getUserById(Long
							.parseLong(objectToString(workOrder[13])));
					if (vo != null)
					{
						workOrderReport.setWoAssignedTo(vo.getFirstName());// firstname
																			// from
																			// User
																			// table
						UserVo vo1 = userDao.getUserById(userDao
								.getReportToUser(vo.getId()));
						if (vo1 != null)
						{
							workOrderReport.setAlName(vo1.getFirstName());// firstname
																			// from
																			// User
																			// table
						}
					}

				}

				workOrderReport.setBranch(objectToString(workOrder[15]));
				workOrderReport.setCity(objectToString(workOrder[16]));

				if (workOrder[19] != null)
				{
					String userCompleted = GenericUtil.completeUserName(userDao
							.getUserById(objectToLong(workOrder[19])));
					workOrderReport.setRackFixedBy(userCompleted);
				}

				if (workOrder[20] != null)
				{
					String userCompleted = GenericUtil.completeUserName(userDao
							.getUserById(objectToLong(workOrder[20])));
					workOrderReport.setRackFixedBy(userCompleted);
				}
				if (workOrder[21] != null)
				{
					workOrderReport.setBatteryInstalled("YES");
					workOrderReport
							.setBatteryOneDetails(objectToString(workOrder[21]));
				}
				if (workOrder[22] != null)
				{
					workOrderReport.setWoClosedDate(GenericUtil
							.convertToUiDateFormat(workOrder[22])); // addedOn
																	// from
																	// WorkOrder
																	// table
					workOrderReport.setAccountActivation("COMPLETED");
				}
				if (workOrder[23] != null)
				{
					String userCompleted = GenericUtil.completeUserName(userDao
							.getUserById(objectToLong(workOrder[23])));
					workOrderReport.setFeasibilityGivenBy(userCompleted);// firstname
					// from
					// User
				}
				if (workOrder[24] != null)
				{
					String userCompleted = GenericUtil.completeUserName(userDao
							.getUserById(objectToLong(workOrder[24])));
					workOrderReport.setFiberDoneBy(userCompleted);// firstname
					// from
					// User
					// table
				}
				if (workOrder[25] != null)
				{
					String userCompleted = GenericUtil.completeUserName(userDao
							.getUserById(objectToLong(workOrder[25])));
					workOrderReport.setSplicingDoneBy(userCompleted);
				}
				if (workOrder[26] != null)
				{
					workOrderReport
							.setNoOfSplicing(objectToString(workOrder[26]));
				}
				if (workOrder[27] != null)
				{
					workOrderReport.setFxName(objectToString(workOrder[27])); // fxName
					// from
					// Gis
					// table

				}
				if (workOrder[28] != null)
				{
					workOrderReport.setCxName(objectToString(workOrder[28])); // cxName
					// from
					// Gis
					// table
				}
				if (workOrder[29] != null)
				{
					workOrderReport
							.setFxPortNumber(objectToString(workOrder[29])); // fxName
					// from
					// Gis
					// table
				}

				if (workOrder[30] != null)
				{
					workOrderReport
							.setCxPortNumber(objectToString(workOrder[30])); // cxName
					// from
					// Gis
					// table
				}
				if (workOrder[31] != null)
				{
					workOrderReport.setFeasibilityDate(GenericUtil
							.convertToUiDateFormat(workOrder[31]));// addedOn
					// from
					// Gis
					// table
				}

				/*
				 * String ticket =
				 * objectToString(objectToString(workOrder[14]));
				 */
				/*
				 * Long ticketId = null; if (ticket != null) { ticketId =
				 * Long.parseLong(ticket); }
				 */

				/*
				 * List<ActivityVo> activities =
				 * activityLogDAO.getActivitiesByWoNo(ticketId);
				 * 
				 * if (activities != null) { for (Iterator iterator =
				 * activities.iterator(); iterator.hasNext();) { ActivityVo
				 * activityVo = (ActivityVo) iterator.next(); String
				 * userCompleted =
				 * GenericUtil.completeUserName(userDao.getUserById(activityVo.
				 * getAddedBy())); if (activityVo.getId() ==
				 * Activity.CX_RACK_FIXING) {
				 * 
				 * workOrderReport.setRackFixedBy(userCompleted);
				 * 
				 * } else if (activityVo.getId() ==
				 * Activity.MATERIAL_CONSUMPTION) {
				 * workOrderReport.setRackFixedBy(userCompleted);
				 * Set<SubActivityVo> subAct = activityVo.getSubActivityList();
				 * if (subAct != null) { for (Iterator iterator2 =
				 * subAct.iterator(); iterator2.hasNext();) { SubActivityVo
				 * subActivityVo = (SubActivityVo) iterator2.next(); if
				 * (subActivityVo.getId() == 1003) {
				 * workOrderReport.setBatteryInstalled("YES");
				 * workOrderReport.setBatteryOneDetails(subActivityVo.getValue()
				 * ); }
				 * 
				 * }
				 * 
				 * } }
				 * 
				 * else if (activityVo.getId() ==
				 * Activity.CUSTOMER_ACCOUNT_ACTIVATION) {
				 * workOrderReport.setWoClosedDate(GenericUtil.
				 * convertToUiDateFormat (activityVo.getAddedOn())); // addedOn
				 * // from // WorkOrder // table
				 * workOrderReport.setAccountActivation("COMPLETED"); } else if
				 * (activityVo.getId() == Activity.PHYSICAL_FEASIBILITY) {
				 * workOrderReport.setFeasibilityGivenBy(userCompleted);//
				 * firstname // from // User // table } else if
				 * (activityVo.getId() == Activity.FIBER_LAYING) {
				 * workOrderReport.setFiberDoneBy(userCompleted);// firstname //
				 * from // User // table } else if (activityVo.getId() ==
				 * Activity.SPLICING) { Set<SubActivityVo> subAct =
				 * activityVo.getSubActivityList();
				 * 
				 * workOrderReport.setSplicingDoneBy(userCompleted); if (subAct
				 * != null) { for (Iterator iterator2 = subAct.iterator();
				 * iterator2.hasNext();) { SubActivityVo subActivityVo =
				 * (SubActivityVo) iterator2.next(); if (subActivityVo.getId()
				 * == 16) {
				 * 
				 * workOrderReport.setNoOfSplicing(subActivityVo.getValue()); }
				 * 
				 * }
				 * 
				 * } }
				 * 
				 * } }
				 */

				/*
				 * Map<Long, GISPojo> gisDatavalue =
				 * gisDAO.getGisTypeAndDetailsByTicketId(ticketId); if
				 * (gisDatavalue != null) { for (Iterator iterator =
				 * gisDatavalue.values().iterator(); iterator.hasNext();) {
				 * GISPojo gisPojo = (GISPojo) iterator.next(); if
				 * (gisPojo.getFeasibilityType() ==
				 * FeasibilityType.DEPLOYMENT_AAT) {
				 * workOrderReport.setFxName(gisPojo.getFxName()); // fxName //
				 * from // Gis // table
				 * workOrderReport.setCxName(gisPojo.getCxName()); // cxName //
				 * from // Gis // table
				 * workOrderReport.setFxPortNumber(gisPojo.getFxPorts()); //
				 * fxName // from // Gis // table
				 * workOrderReport.setCxPortNumber(gisPojo.getCxPorts()); //
				 * cxName // from // Gis // table } if
				 * (gisPojo.getFeasibilityType() ==
				 * FeasibilityType.PHYSICAL_FEASIBILITY_BY_NE) {
				 * 
				 * workOrderReport.setFeasibilityDate(GenericUtil.
				 * convertToUiDateFormat(gisPojo.getAddedOn()));// addedOn //
				 * from // Gis // table } } }
				 */

				workOrderList.add(workOrderReport);
			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("Exception  " + e.getMessage());
				continue;
			}

		}
		log.debug(" Exit From  getWorkOrderList "   + list.size());
		return workOrderList;
	}

	private String objectToString(Object obj)
	{
		if (obj == null)
			return null;
		return obj.toString();
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null || obj.equals(null) || obj == "null"
				|| obj.equals("null"))
			return 0l;
		return Long.valueOf(obj + "");

	}

}
