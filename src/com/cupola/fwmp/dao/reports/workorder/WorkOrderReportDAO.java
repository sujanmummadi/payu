package com.cupola.fwmp.dao.reports.workorder;

import java.util.List;

import com.cupola.fwmp.reports.vo.WorkOrderReport;

public interface WorkOrderReportDAO {

	public List<WorkOrderReport> getWorkOrderReports(String fromDate,String toDate,String branchId,String areaId,String cityId);
}
