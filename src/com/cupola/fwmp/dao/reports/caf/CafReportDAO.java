package com.cupola.fwmp.dao.reports.caf;

import java.util.Date;
import java.util.List;

import com.cupola.fwmp.reports.vo.CafVerficationReport;

public interface CafReportDAO {
	
	public List<CafVerficationReport> getCafReports(String fromDate , String toDate,String branchId,String areaId,String cityID);
	

}
