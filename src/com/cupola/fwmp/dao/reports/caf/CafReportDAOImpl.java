package com.cupola.fwmp.dao.reports.caf;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.reports.vo.CafVerficationReport;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.UserVo;
import com.google.gson.Gson;


@Transactional
public class CafReportDAOImpl implements CafReportDAO {
	
	HibernateTemplate hibernateTemplate;
    MongoTemplate mongoTemplate;
    @Autowired
	TicketActivityLogDAO activityLogDAO;
    @Autowired
	UserDao userDao;
	public void sethibernetTemplate(HibernateTemplate hibernetTemplate) {

		this.hibernateTemplate = hibernetTemplate;

	}
	 
	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}


	private static Logger log = Logger.getLogger(CafReportDAOImpl.class
			.getName());

 

	public String CAF_REPORT_QUERY_WITHOUT_DATES = " SELECT c.mqId,t.prospectNo,c.firstName,c.middleName,c.lastName, c.mobileNumber,a.areaName,b.branchName, "
			+ " ct.cityName ,s.status,t.cafNumber,t.TicketCreationDate,tp.PackageName,t.amountPaid,t.id as ticketId , "
			+ " w.addedOn,t.TicketCreationDate as cafsatusdate ,p.paidAmount, t.currentAssignedTo as assigned ,"
			+" (select tal.addedOn from TicketActivityLog tal where  tal.ticketId = t.id   and tal.activityId = 14 Group By tal.ticketId) as docColl,"
			+"(select tal.modifiedOn from TicketActivityLog tal where  tal.ticketId = t.id   and tal.activityId = 14 Group By tal.ticketId) as docReject,"
	        +"(select tal.addedBy from TicketActivityLog tal where  tal.ticketId = t.id   and tal.activityId = 14 Group By tal.ticketId) as seName"

			+ " FROM Ticket t  Inner JOIN Customer c on t.customerId=c.id "
			+ "  LEFT JOIN Payment p on p.id=t.idPayment "
			+ "  LEFT JOIN WorkOrder w on w.ticketId=t.id "
			+ "  LEFT JOIN Area a on a.id=c.AreaId  "
			+ "  LEFT JOIN Branch b  "
			+ "  on b.id=a.branchId LEFT JOIN City ct on ct.id=b.cityId "
			+ "  LEFT JOIN Status s on  "
			+ " s.id=t.status LEFT JOIN TariffPlan tp on c.tariffId=tp.id  "
			+ " where t.source in ('Call Centre','Door Knock','Tool') and t.cafNumber is not null ";

	
	

	public List<CafVerficationReport> getCafReports(String fromDate , String toDate,String branchId ,String areaId,String cityID) {

		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;
		try {

			 
			Query query = null;
			 
				Date from = null;

				Date to = null;
				
				boolean datesPresent = false;

				boolean areaPresent = false;

				boolean branchPresent = false;
				
				boolean cityPresent = false;

				if (GenericUtil.isStringNotNull(fromDate)
						&& GenericUtil.isStringNotNull(toDate))
				{
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
					
					try {
						from = df.parse(fromDate);
						to = df.parse(toDate);
						
					} catch (ParseException e) {
						log.error("Error While parsing date :" +e.getMessage());
						e.printStackTrace();
					}
					}
	            StringBuffer queryString = new StringBuffer();
				
				queryString.append(CAF_REPORT_QUERY_WITHOUT_DATES);
				
				

				if (from != null && to != null
						&& (GenericUtil.isStringNotNull(fromDate))
						&& (GenericUtil.isStringNotNull(toDate)))
				{

					datesPresent = true;

					queryString
							.append(" and t.TicketCreationDate>=:fromDate and t.TicketCreationDate<=:toDate ");
				}
				
				if (GenericUtil.isStringNotNull(branchId))
				{
					queryString.append(" and b.id =:branchId ");
					branchPresent = true;
				}

				if (GenericUtil.isStringNotNull(areaId))
				{
					areaPresent = true;
					queryString.append(" and a.id =:areaId ");
				}
				
				if(GenericUtil.isStringNotNull(cityID))
				{
					cityPresent = true;
					queryString.append(" and ct.id =:cityId ");
				}
				if(!cityPresent)
				{
					if(!AuthUtils.isAdmin())
						queryString.append(" and ct.id =:cityId ");
				}
					
				
				log.info("query string: " + queryString);
				
				query = hibernateTemplate.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery(queryString.toString());

				if (datesPresent)
				{

					query.setDate("fromDate", from);
					query.setDate("toDate", to);
				}

				if (branchPresent)
				{

					Long branch = Long.parseLong(branchId);
					query.setLong("branchId", branch);
				}

				if (areaPresent)
				{
					Long area = Long.parseLong(areaId);
					query.setLong("areaId", area);

				}
				
				if(cityPresent)
				{
					Long city = Long.parseLong(cityID);
					query.setLong("cityId", city);
				}

				if(!cityPresent)
				{
					if(!AuthUtils.isAdmin())
						query.setLong("cityId", AuthUtils.getCurrentUserCity().getId());
				}
					
				 
			
			log.info("query : "+query);
			
			list = query.list();
			
			log.info("list size: "+list.size());

		} catch (Exception e) {
			log.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		} 

		Long timeEnd = System.currentTimeMillis();
		
		long timeDiff = timeEnd - timestart;
		
		log.info("TIme Taken for db call and getting results in milliseconds "+ timeDiff);
		
		log.debug(" Exit From  getCafReports "   + cityID);
		 
		return getCafList(list);


	}

	private List<CafVerficationReport> getCafList(List<Object[]> list)
	{

		if (list == null)
			return null;

		log.debug("  getCafList "   + list.size());
		List<CafVerficationReport> cafList = new ArrayList<CafVerficationReport>();

		CafVerficationReport cafReport = null;

		for (Object[] caf : list)
		{

			cafReport = new CafVerficationReport();

			cafReport.setMqId(objectToString(caf[0])); // mqId from Customer
														// table
			cafReport.setDcrNo(objectToString(caf[1])); // prospectNo from
														// Ticket table

			cafReport.setCustomerName(objectToString(caf[2])); // firstName
																// Customer
																// table
			if (objectToString(caf[3]) != null)
				cafReport.setCustomerName(cafReport.getCustomerName()
						.concat(" " + objectToString(caf[3])));
			else if (objectToString(caf[4]) != null)
				cafReport.setCustomerName(cafReport.getCustomerName()
						.concat(" " + objectToString(caf[4])));
			cafReport.setPhoneMobile(objectToString(caf[5]));// mobileNumber
																// Customer
																// table
			cafReport.setArea(objectToString(caf[6])); // areaName from area
														// table
			cafReport.setBranch(objectToString(caf[7]));
			cafReport.setCity(objectToString(caf[8]));
			cafReport.setSubStatusId(objectToString(caf[9]));// status from
																// Status table
			cafReport.setCafNumber(objectToString(caf[10])); // cafNumber from
																// Ticket table
			cafReport.setApproachDate(objectToString(caf[11])); // TicketCreationDate
																// from Ticket
																// table

			cafReport.setTariffPlanName(objectToString(caf[12])); // PackageName
																	// from
																	// TariffPlan
																	// table
			cafReport.setTotalSubcPaid(objectToString(caf[17])); // amountPaid
																	// from
																	// Ticket
																	// table
			if (objectToString(caf[15]) != null)
				cafReport.setCafStatusDate(objectToString(caf[15])); // TicketCreationDate
																		// from
			if(caf[19] != null)
			{
				cafReport.setDocCollectedDate(GenericUtil
						.convertToUiDateFormat(caf[19]));
			}
			if(caf[20] != null)
			{
				cafReport.setDocAccRejectDate(GenericUtil
						.convertToUiDateFormat(caf[20]));
			}
			if(caf[21] != null)
			{
				UserVo user = userDao.getUserById(objectToLong(caf[21]));


				if (user != null)
					cafReport.setSeName(GenericUtil
							.completeUserName(user));
			}
			
			if (cafReport.getSeName() == null
					&& cafReport.getCafStatusDate() == null
					&& objectToString(caf[18]) != null)
			{
				String user = objectToString(caf[18]);
				Long userId = null;
				if (user != null)
				{
					userId = Long.parseLong(user);
					UserVo userVo = userDao.getUserById(userId);
					if (userVo != null)
						cafReport.setSeName(GenericUtil
								.completeUserName(userVo));
				}

			}


			/*String ticket = objectToString(caf[14]);
			Long ticketId = null;
			if (ticket != null)
			{
				ticketId = Long.parseLong(ticket);
			}*/

			/*if (ticketId != null)
			{
				List<ActivityVo> activities = activityLogDAO
						.getActivitiesByWoNo(ticketId);

				if (activities != null)
				{
					for (Iterator iterator = activities.iterator(); iterator
							.hasNext();)
					{
						ActivityVo activityVo = (ActivityVo) iterator.next();

						if (activityVo.getId() == Activity.DOCUMENT_UPDATE)
						{
							cafReport.setDocCollectedDate(GenericUtil
									.convertToUiDateFormat(activityVo
											.getAddedOn()));
							cafReport.setDocAccRejectDate(GenericUtil
									.convertToUiDateFormat(activityVo
											.getModifiedOn()));
							UserVo user = userDao.getUserById(activityVo
									.getAddedBy());

							if (user != null)
								cafReport.setSeName(GenericUtil
										.completeUserName(user));
						}
					}
				}
				if (cafReport.getSeName() == null
						&& cafReport.getCafStatusDate() == null
						&& objectToString(caf[18]) != null)
				{
					String user = objectToString(caf[18]);
					Long userId = null;
					if (user != null)
					{
						userId = Long.parseLong(user);
						UserVo userVo = userDao.getUserById(userId);
						if (userVo != null)
							cafReport.setSeName(GenericUtil
									.completeUserName(userVo));
					}

				}

				
			}*/
			cafList.add(cafReport);
		}
		log.debug(" Exit From  getCafList "   + list.size());
		return cafList;
	}

	private String objectToString(Object obj) {
		if (obj == null)
			return null;
		return obj.toString();
	}
	
	private Long objectToLong(Object obj)
	{
		if (obj == null || obj.equals(null) || obj == "null"
				|| obj.equals("null"))
			return 0l;
		return Long.valueOf(obj + "");

	}




}
