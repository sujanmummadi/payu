package com.cupola.fwmp.dao.reports.material;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.reports.vo.MaterialReport;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;

@Transactional
public class MaterialReportDAOImpl implements MaterialReportDAO
{

	HibernateTemplate hibernateTemplate;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernateTemplate = hibernetTemplate;

	}

	private static Logger log = Logger
			.getLogger(MaterialReportDAOImpl.class.getName());

	public String MATERIAL_REPORT_QUERY_WITHOUT_DATES = "SELECT c.firstName,c.middleName, c.lastName, a.areaName as area, b.branchName, ct.cityName,ws.workStageName, "
			+ " c.currentAddress,c.mobileNumber,a.areaName,s.status,t.TicketCreationDate,w.workOrderNumber, "
			+ " c.mqId, t.cafNumber,tp.packageName, w.addedOn,t.id as ticket_id, "
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1001 order by tal.addedOn desc limit 1) as CopperCablePulledFrom,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1002 order by tal.addedOn desc limit 1) as Fibre,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1003 order by tal.addedOn desc limit 1) as BatteryFixing,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1004 order by tal.addedOn desc limit 1) as Cx,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1005 order by tal.addedOn desc limit 1) as CoreElectricWire,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1006 order by tal.addedOn desc limit 1 ) as FemaleSocket,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1007 order by tal.addedOn desc limit 1 ) as ImsCabinet,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1008 order by tal.addedOn desc limit 1) as FlexiblePipe,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1010 order by tal.addedOn desc limit 1) as PatchChord,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1011 order by tal.addedOn desc limit 1) as Sfp,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1012 order by tal.addedOn desc limit 1) as PadLock,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1013 order by tal.addedOn desc limit 1) as Rj45Boots,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1014 order by tal.addedOn desc limit 1) as Rj45Connectors,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1015 order by tal.addedOn desc limit 1) as FiberType,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1016 order by tal.addedOn desc limit 1) as Drum,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1017 order by tal.addedOn desc limit 1) as CxRack,"
			+ "(SELECT tal.value FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId where tal.ticketId=t.id and tal.activityId=8 and sa.id=1018 order by tal.addedOn desc limit 1) as Router"

			+ " FROM Ticket t Inner JOIN WorkOrder w on w.ticketId=t.id  "
			+ " Inner JOIN Customer c  on t.customerId=c.id  "
			+ " inner JOIN Status s on s.id=t.status "
			+ " LEFT JOIN Area a on a.id=c.areaId  "
			+ " LEFT JOIN TariffPlan tp on tp.id=c.tariffId  "
			+ " left join WorkStage ws on ws.id =w.currentWorkStage "
			+ " LEFT JOIN Branch b  on b.id=a.branchId LEFT JOIN City ct on ct.id=b.cityId "
			+ " where    t.source in ('Call Centre','Door Knock','Tool') ";

	public String TICKET_QUERY1 = " SELECT sa.id ,sa.subActivityName ,tal.value "
			+ " FROM TicketActivityLog tal inner Join SubActivity sa on sa.id = tal.subactivityId "
			+ "  where ticketId=:ticketId and tal.activityId=8 ";

	public List<MaterialReport> getMaterialReports(String fromDate,
			String toDate, String branchId, String areaId, String cityId)
	{

		log.debug(" getMaterialReports " +cityId );
		Long timestart = System.currentTimeMillis();

		List<Object[]> list = null;

		try
		{

			Query query = null;

			Date from = null;

			Date to = null;

			boolean datesPresent = false;

			boolean areaPresent = false;

			boolean branchPresent = false;

			boolean cityPresent = false;

			if (GenericUtil.isStringNotNull(fromDate)
					&& GenericUtil.isStringNotNull(toDate))
			{
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				try
				{
					from = df.parse(fromDate);
					to = df.parse(toDate);

				} catch (ParseException e)
				{
					log.error("Error while parsing DateFormate :"
							+ e.getMessage());
					e.printStackTrace();
				}
			}

			StringBuffer queryString = new StringBuffer();

			queryString.append(MATERIAL_REPORT_QUERY_WITHOUT_DATES);

			if (from != null && to != null
					&& (GenericUtil.isStringNotNull(fromDate))
					&& (GenericUtil.isStringNotNull(toDate)))
			{

				datesPresent = true;

				queryString
						.append(" and t.TicketCreationDate>=:fromDate and t.TicketCreationDate<=:toDate ");
			}

			if (GenericUtil.isStringNotNull(branchId))
			{
				queryString.append(" and b.id =:branchId ");
				branchPresent = true;
			}

			if (GenericUtil.isStringNotNull(areaId))
			{
				areaPresent = true;
				queryString.append(" and a.id =:areaId ");
			}

			if (GenericUtil.isStringNotNull(cityId))
			{
				cityPresent = true;
				queryString.append(" and ct.id =:cityId ");
			}
			if (!cityPresent)
			{
				if (!AuthUtils.isAdmin())
					queryString.append(" and ct.id =:cityId ");
			}

			query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(queryString.toString());
			if (datesPresent)
			{

				query.setDate("fromDate", from);
				query.setDate("toDate", to);
			}

			if (branchPresent)
			{

				Long branch = Long.parseLong(branchId);
				query.setLong("branchId", branch);
			}

			if (areaPresent)
			{
				Long area = Long.parseLong(areaId);
				query.setLong("areaId", area);

			}

			if (cityPresent)
			{
				Long city = Long.parseLong(cityId);
				query.setLong("cityId", city);
			}

			if (!cityPresent)
			{
				if (!AuthUtils.isAdmin())
					query.setLong("cityId", AuthUtils.getCurrentUserCity()
							.getId());
			}

			log.info("query : " + query.getQueryString());

			list = query.list();

			log.info("list size: " + list.size());

		} catch (Exception e)
		{
			log.error("error: " + e);
			e.printStackTrace();
			// logger.info("Failed to fetch Tickets with userId :"+userId+" and
			// workOrderTypeId "+workOrderTypeId,e);
		}

		Long timeEnd = System.currentTimeMillis();

		long timeDiff = timeEnd - timestart;

		log.info("TIme Taken for db call and getting results in milliseconds"
				+ timeDiff);
		log.debug("Exit From getMaterialReports " +cityId );
		return getMaterialList(list);

	}

	public List<MaterialData> getMateActivities(long ticketId)
	{
		log.debug("  getMateActivities " +ticketId );

		List<Object[]> list = null;
		try
		{

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(TICKET_QUERY1);
			query.setLong("ticketId", ticketId);
			list = query.list();

			log.debug("list size: " + list.size());

		} catch (Exception e)
		{
			log.error("error: " + e);
			e.printStackTrace();

		}
		log.debug("  Exit From getMateActivities " +ticketId );
		return getMateActivityList(list);
	}

	private List<MaterialData> getMateActivityList(List<Object[]> list)
	{

		if (list == null)
			return null;
		log.debug("  getMateActivityList " +list.size() );
		List<MaterialData> listData = new ArrayList<MaterialData>();

		for (Object[] ticketMateActivity : list)
		{

			listData.add(new MaterialData(objectToLong(ticketMateActivity[0]), objectToString(ticketMateActivity[1]), objectToString(ticketMateActivity[2])));
		}
		return listData;
	}

	private List<MaterialReport> getMaterialList(List<Object[]> list)
	{

		String cellValue = "Y";

		if (list == null)
			return null;

		log.debug("   getMaterialList " +list.size() );
		List<MaterialReport> materialList = new ArrayList<MaterialReport>();

		MaterialReport materialReport = null;

		for (Object[] material : list)
		{
			try
			{
				materialReport = new MaterialReport();

				materialReport.setCustomerName(objectToString(material[0]));
				if (objectToString(material[1]) != null)
					materialReport
							.setCustomerName(materialReport.getCustomerName()
									.concat(" " + objectToString(material[1])));
				else if (objectToString(material[2]) != null)
					materialReport
							.setCustomerName(materialReport.getCustomerName()
									.concat(" " + objectToString(material[2])));
				materialReport.setArea(objectToString(material[3]));
				materialReport.setBranchName(objectToString(material[4]));
				materialReport.setCityName(objectToString(material[5]));
				materialReport.setWoType(objectToString(material[6]));

				materialReport.setCustomerAddress(objectToString(material[7]));
				materialReport.setPhoneMobile(objectToString(material[8]));
				materialReport.setSubStatusId(objectToString(material[10]));
				materialReport.setApproachDate(objectToString(material[11]));
				materialReport.setWorkOrderNo(objectToString(material[12]));
				materialReport.setMqId(objectToString(material[13]));
				materialReport.setCaf(objectToString(material[14]));
				materialReport.setTariffPlan(objectToString(material[15]));
				materialReport.setWoDate(objectToString(material[16])); // addedOn
																		// from
																		// WorkOrder
																		// table

				materialReport.setNe(objectToString(material[9])); // firstname
																	// from
																	// User
																	// table
				// String ticket = objectToString(objectToString(material[17]));
				if (material[18] != null)
				{
					materialReport
							.setCopperCablePulledFromCxtoCPE(objectToString(material[18]));
				}
				if (material[19] != null)
				{
					materialReport.setFibre(objectToString(material[19]));
				}

				if (material[20] != null)
				{
					materialReport
							.setBatteryFixing(objectToString(material[20]));
				}

				if (material[21] != null)
				{
					materialReport.setCx(objectToString(material[21]));
				}

				if (material[22] != null)
				{
					materialReport
							.setCoreElectricWire(objectToString(material[22]));
				}

				if (material[23] != null)
				{
					materialReport
							.setFemaleSocket(objectToString(material[23]));
				}

				if (material[24] != null)
				{
					materialReport.setImsCabinet(objectToString(material[24]));
				}

				if (material[25] != null)
				{
					materialReport
							.setFlexiblePipe(objectToString(material[25]));
				}

				if (material[26] != null)
				{
					materialReport.setPatchChord(objectToString(material[26]));
				}
				if (material[27] != null)
				{
					materialReport.setSfp(objectToString(material[27]));
				}
				if (material[28] != null)
				{
					materialReport.setPadLock(objectToString(material[28]));
				}
				if (material[29] != null)
				{
					materialReport.setRj45Boots(objectToString(material[29]));
				}
				if (material[30] != null)
				{
					materialReport
							.setRj45Connectors(objectToString(material[30]));
				}
				if (material[31] != null)
				{
					materialReport.setFiberType(objectToString(material[31]));
				}
				if (material[32] != null)
				{
					materialReport.setDrum(objectToString(material[32]));
				}
				if (material[33] != null)
				{
					materialReport.setCxRack(objectToString(material[33]));
				}
				if (material[34] != null)
				{
					materialReport.setRouter(objectToString(material[34]));
				}

				/*
				 * Long ticketId = null; if (ticket != null) { ticketId =
				 * Long.parseLong(ticket); List<MaterialData> data =
				 * getMateActivities(ticketId); if (data != null) { for
				 * (Iterator iterator = data.iterator(); iterator .hasNext();) {
				 * MaterialData materialData = (MaterialData) iterator .next();
				 * if (materialData.getValue() == null) continue; cellValue =
				 * materialData.getValue(); switch
				 * (materialData.getSubActivityId() + "") { case "1001":
				 * materialReport.setCopperCablePulledFromCxtoCPE(cellValue);
				 * break; case "1002": materialReport.setFibre(cellValue);
				 * break; case "1003":
				 * materialReport.setBatteryFixing(cellValue); break; case
				 * "1004": materialReport.setCx(cellValue); break; case "1005":
				 * materialReport.setCoreElectricWire(cellValue); break; case
				 * "1006": materialReport.setFemaleSocket(cellValue); break;
				 * case "1007": materialReport.setImsCabinet(cellValue); break;
				 * case "1008": materialReport.setFlexiblePipe(cellValue);
				 * break;
				 * 
				 * case "1010": materialReport.setPatchChord(cellValue); break;
				 * case "1011": materialReport.setSfp(cellValue); break; case
				 * "1012": materialReport.setPadLock(cellValue); break; case
				 * "1013": materialReport.setRj45Boots(cellValue); break; case
				 * "1014": materialReport.setRj45Connectors(cellValue); break;
				 * case "1015": materialReport.setFiberType(cellValue); break;
				 * case "1016": materialReport.setDrum(cellValue); break; case
				 * "1017": materialReport.setCxRack(cellValue); break; case
				 * "1018": materialReport.setRouter(cellValue); break; default:
				 * break; }
				 * 
				 * } }
				 * 
				 * }
				 */

				materialList.add(materialReport);
			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("Exception  " + e.getMessage());
				continue;
			}
		}
		log.debug(" Exit From getMaterialList " +list.size() );
		return materialList;
	}

	private String objectToString(Object obj)
	{
		if (obj == null)
			return null;
		return obj.toString();
	}

	class MaterialData
	{
		Long subActivityId;
		String subActivityName;
		String value;

		public Long getSubActivityId()
		{
			return subActivityId;
		}

		public void setSubActivityId(Long subActivityId)
		{
			this.subActivityId = subActivityId;
		}

		public String getSubActivityName()
		{
			return subActivityName;
		}

		public void setSubActivityName(String subActivityName)
		{
			this.subActivityName = subActivityName;
		}

		public String getValue()
		{
			return value;
		}

		public void setValue(String value)
		{
			this.value = value;
		}

		public MaterialData(Long subActivityId, String subActivityName,
				String value)
		{
			super();
			this.subActivityId = subActivityId;
			this.subActivityName = subActivityName;
			this.value = value;
		}
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

}
