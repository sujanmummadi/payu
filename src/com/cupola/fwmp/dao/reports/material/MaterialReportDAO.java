package com.cupola.fwmp.dao.reports.material;

import java.util.Date;
import java.util.List;

import com.cupola.fwmp.reports.vo.MaterialReport;

public interface MaterialReportDAO {
	
	public List<MaterialReport> getMaterialReports(String fromDate , String toDate,String branchId,String areaId,String cityId);

}
