package com.cupola.fwmp.dao.userCaf;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.FWMPConstant.DBActiveStatus;
import com.cupola.fwmp.FWMPConstant.UserCaf;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.CafDetailsVo;
import com.cupola.fwmp.vo.ReassignCAF;
import com.cupola.fwmp.vo.UserCafResponseVo;
import com.cupola.fwmp.vo.UserCafVo;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserCafDAOImpl implements UserCafDAO
{

	private Logger log = Logger.getLogger(UserCafDAOImpl.class.getName());

	private final String INSERT_VALUES_USERCAF = "INSERT INTO  UserCAF  VALUES (?,?,?,?,?,?,?,?,?)";

	private final String UPDATE_USERCAF = "UPDATE UserCAF caf SET caf.userId=?,caf.range=?,caf.modifiedBy=?,caf.modifiedOn=?,caf.status=? "
			+ "where caf.userId=?";

	private final String UPDATE_CAFSTATUS = "update UserCAF u set u.modifiedOn=?,u.modifiedBy=?,u.status=? where u.range=? and u.cityId=?";

	private static String DELETE_USERCAF = "delete from UserCAF where userId=?";

	private static String GET_ALL_DETAILS_BY_USERID = "select * from UserCAF caf where caf.userId=?";

	private static String GET_CAF_BY_USERID = "select range from UserCAF caf where caf.userId=?";

	private static String GET_USERCAF = "select * from UserCAF caf";

	private static String UPDATE_CAF = "update UserCAF caf set caf.userId=?, caf.modifiedOn=now() where caf.range=? and caf.userId=?";

	private static String GET_CAF_BY_CAF_NUMBER = "select * from UserCAF caf  where caf.range=? and status=?";

	HibernateTemplate hibernateTemplate;
	JdbcTemplate jdbcTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<String> addCafDetails(List<CafDetailsVo> userCafVo,
			Long assignedUser)
	{
		List<String> list = new ArrayList<>();
		if( userCafVo == null || userCafVo.isEmpty() )
			return list;
		
		log.debug(" inside addCafDetails  "+userCafVo.size());
		Integer rowNum = 0;


		Long cityId = AuthUtils.getCurrentUserCity().getId();
		try
		{
			for (Iterator<CafDetailsVo> iterator = userCafVo
					.iterator(); iterator.hasNext();)
			{
				CafDetailsVo cafDetailsVo = (CafDetailsVo) iterator.next();
				try
				{

					log.debug("adding data in UserCaf: " + userCafVo
							+ "and cityId" + cityId);

					rowNum = jdbcTemplate
							.update(INSERT_VALUES_USERCAF, new Object[] {
									StrictMicroSecondTimeBasedGuid.newGuid(),
									cityId, cafDetailsVo.getUserId(),
									cafDetailsVo.getCafNumber(), new Date(),
									assignedUser, new Date(), assignedUser,
									cafDetailsVo.getStatus() });

					if (rowNum > 0)
					{
						rowNum = 0;

					} else
					{
						list.add(cafDetailsVo.getCafNumber());
					}
				} catch (Exception e)
				{

					e.printStackTrace();
					log.error("Error while adding caf for user "
							+ cafDetailsVo.getUserId() + " for caf "
							+ cafDetailsVo.getCafNumber());
					list.add(cafDetailsVo.getCafNumber());
					continue;
				}
			}

		} catch (Exception e)
		{
			log.error("Error while adding caf for user " + assignedUser
					+ ". Message " + e.getMessage());

			e.printStackTrace();
		}

		log.debug(" Exitting from  addCafDetails  "+userCafVo.size());
		return list;
	}

	@Override
	public List<String> getCafDetailsByUserId(Long userId)
	{
		log.debug("get CafListNumber as userId:" + userId);
		List<String> listOfCafNumber = null;

		if (userId == null || userId <= 0)
			return listOfCafNumber;

		try
		{
			listOfCafNumber = (List<String>) jdbcTemplate
					.queryForList("select UserCAF.range from UserCAF where userId=? and status=?", new Object[] {
							userId, UserCaf.UN_USED }, String.class);

			log.debug("List of caf for user " + userId + " is "
					+ listOfCafNumber.size());

		} catch (Exception e)
		{
			log.error("Error in getting caf details for user " + userId);
			e.printStackTrace();
		}
		
		log.debug("Exitting from get CafListNumber as userId:" + userId);
		
		return listOfCafNumber;

	}

	@Override
	public Integer updateUserCafByUserId(CafDetailsVo vo)
	{
		log.debug("updating  data in UserCaf:" + vo + "userId"
				+ vo.getUserId());
		Integer rows = 0;

		if(vo == null)
			return rows;
		
		log.debug("updating  data in UserCaf:" + vo + "userId"
				+ vo.getUserId());
		try
		{
			Long cityId = AuthUtils.getCurrentUserCity().getId();

			Object[] params = { new Date(), vo.getUserId(), vo.getStatus(),
					vo.getCafNumber(), cityId };

			int[] types = { Types.TIMESTAMP, Types.BIGINT, Types.INTEGER,
					Types.VARCHAR, Types.BIGINT };

			log.debug("updating  data in UserCaf:" + vo + "userId"
					+ vo.getUserId());
			rows = jdbcTemplate.update(UPDATE_CAFSTATUS, params, types);

		} catch (Exception e)
		{
			log.error("Error While updating UserCafByUserId :"
					+ e.getMessage());
			e.printStackTrace();
		}
		log.debug("Exitting from updating  data in UserCaf:" + vo + "userId"
				+ vo.getUserId());
		return rows;

	}

	@Override
	public int addUserCAF(UserCafVo userCafVo, Long userId)
	{

		int rowNum = 0;

		if ( userCafVo == null )
			return rowNum;
		
		log.debug("adding  data in userCaf :" + userCafVo.getUserId());
		try
		{

			ObjectMapper mapper = new ObjectMapper();

			String newList = mapper
					.writeValueAsString(convertRange2List(userCafVo
							.getCafDetails()));

			// String existingRange = jdbcTemplate
			// .queryForObject(GET_CAF_BY_USERID, new Object[] { userCafVo
			// .getUserId() }, new RangeMapper());

			String completeList = newList;

			// if (existingRange != null)
			// completeList = completeList + existingRange;

			rowNum = jdbcTemplate.update(INSERT_VALUES_USERCAF, new Object[] {
					StrictMicroSecondTimeBasedGuid.newGuid(),
					userCafVo.getUserId(), completeList, new Date(), userId,
					new Date(), userId, DBActiveStatus.ACTIVE });

		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("error adding data in UserCAF:" + e);
			return -1;
		}
		if (rowNum <= 0)
			log.info("data adeed in TicketActivityLog failed as ticketId:");
		else
			log.info("data adeed in TicketActivityLog successful as ticketId:");
		
		log.debug("Exitting from adding  data in userCaf :" + userCafVo.getUserId());
		
		return rowNum;

	}

	@Override
	public int removeCafEntryFromUser(String cafNumber, Long userId)
	{
		log.debug(" inside removeCafEntryFromUser  "+cafNumber);
		{
			int rows = 0;

			if(cafNumber == null ||cafNumber.isEmpty() ||userId == null || userId <= 0)
				return rows;
			try
			{
				UserCafResponseVo userCafResponseVo = getUserCAFByUserId(userId);

				if (userCafResponseVo == null)
				{
					return 0;
				}
				ObjectMapper mapper = new ObjectMapper();
				String cafDetails = userCafResponseVo.getRange();

				List<Integer> mapperList = mapper
						.readValue(cafDetails, new TypeReference<List<Integer>>()
						{
						});

				mapperList.remove(Integer.valueOf(cafNumber));
				cafDetails = mapper.writeValueAsString(mapperList);

				Object[] params = { userId, cafDetails, userId, new Date(),
						DBActiveStatus.ACTIVE, userId };

				int[] types = { Types.BIGINT, Types.VARCHAR, Types.BIGINT,
						Types.TIMESTAMP, Types.INTEGER, Types.BIGINT };

				rows = jdbcTemplate.update(UPDATE_USERCAF, params, types);

				if (rows > 0)
					log.info("TicketCaf updated sucessfully as ticketId:");

			} catch (Exception e)
			{
				e.printStackTrace();
				log.error("error updateTicketActivityLog :" + e);
				return 0;

			}
			log.debug(" Exitting from  removeCafEntryFromUser  "+cafNumber);
			return rows;

		}
	}

	@Override
	public int updateUserCAF(UserCafVo userCafVo, Long userId)
	{
		log.debug(" inside updateUserCAF for User : "+userId);
		int rows = 0;

		if(userCafVo == null ||userId == null || userId <= 0)
			return rows;
		try
		{
			String cafDetails = userCafVo.getCafDetails();
			UserCafResponseVo userCafResponseVo = getUserCAFByUserId(userId);

			if (userCafResponseVo == null)
			{
				return 0;
			}
			ObjectMapper mapper = new ObjectMapper();

			String newList = mapper
					.writeValueAsString(convertRange2List(userCafVo
							.getCafDetails()));

			cafDetails = userCafResponseVo.getRange() + " , " + newList;

			Object[] params = { userCafVo.getUserId(), cafDetails, userId,
					new Date(), DBActiveStatus.ACTIVE, userCafVo.getUserId() };

			int[] types = { Types.BIGINT, Types.VARCHAR, Types.BIGINT,
					Types.TIMESTAMP, Types.INTEGER, Types.BIGINT };

			rows = jdbcTemplate.update(UPDATE_USERCAF, params, types);

			log.debug("**************" + userCafVo);

			if (rows > 0)
				log.info("TicketActivityLog  updated sucessfully as ticketId:");

		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("error updateTicketActivityLog :" + e);
			return 0;

		}
		log.debug(" Exitting from  updateUserCAF for User : "+userId);
		return rows;

	}

	@Override
	public int deleteUserCAF(UserCafVo userCafVo)
	{

		int row = 0;
		if(userCafVo == null )
			return row;
		log.debug(" inside deleteUserCAF  " +userCafVo.getUserId());
		try
		{
			row = jdbcTemplate.update(DELETE_USERCAF, userCafVo.getUserId());
			if (row > 0)
			{
				log.info("data deleted from UserCaf Successfuly as userId:"
						+ userCafVo.getUserId());
			}

			if (row <= 0)
			{
				log.info("No record  in UserCaf  as userId:"
						+ userCafVo.getUserId());
			}
		} catch (Exception e)
		{
			log.error("error deleting UserCaf:" + e);
		}
		
		log.debug(" Exitting from  deleteUserCAF  "+userCafVo.getUserId());
		return row;

	}

	@Override
	public List<Integer> getUserCAFNumberByUserId(UserCafVo userCafVo)
	{

		UserCafResponseVo userCaf = null;

		List<Integer> list = new ArrayList<Integer>();
		if(userCafVo == null )
			return list;

		log.debug(" inside getUserCAFNumberByUserId  "+userCafVo.getUserId());
		try
		{
			List<UserCafResponseVo> caf = (List<UserCafResponseVo>) jdbcTemplate
					.query(GET_ALL_DETAILS_BY_USERID, new Object[] {
							userCafVo.getUserId() }, new UserCafMapper());

			log.info("Data received from the userCaf table for "
					+ userCafVo.getUserId() + " is " + caf);

			if (caf != null && !caf.isEmpty() )
			{

				log.info("found  data from UserCaf  as userId:"
						+ userCafVo.getUserId() + "" + caf);
				userCaf = caf.get(0);

				ObjectMapper mapper = new ObjectMapper();

				List<Integer> mapperList = mapper.readValue(userCaf
						.getRange(), new TypeReference<List<Integer>>()
						{
						});

				log.debug("mapper List " + mapperList);

				list.addAll(mapperList);
			}

			if (caf == null || caf.isEmpty())
			{
				log.info("No record  in UserCaf  as userId:"
						+ userCafVo.getUserId());
				return null;
			}
		} catch (Exception e)
		{
			log.error("Error in UserCAFNumber:" + e);
			return null;
		}

		log.debug(" exittring from getUserCAFNumberByUserId  "+userCafVo.getUserId());
		return list;

	}

	@Override
	public UserCafResponseVo getUserCAFByUserId(Long userId)
	{
		if( userId == null || userId <= 0 )
			return null;
		
		log.debug(" inside getUserCAFByUserId  "+userId);

		List<UserCafResponseVo> userCaf = null;
		try
		{
			userCaf = (List<UserCafResponseVo>) jdbcTemplate
					.query(GET_ALL_DETAILS_BY_USERID, new Object[] {
							userId }, new UserCafMapper());

			if (userCaf != null)
			{
				log.info("found  data from UserCaf  as userId:" + userId);
			}

			if (userCaf == null)
			{
				log.info("No record  in UserCaf  as userId:" + userId);
				return null;
			}
		} catch (Exception e)
		{
			log.error("Error getUserCAFById:" + e);
			return null;
		}

		if (userCaf != null && userCaf.size() > 0)
			return userCaf.get(0);
		else
			return null;
	}

	@Override
	public List<UserCafResponseVo> getListUserCAF()
	{
		log.debug(" inside getListUserCAF  ");

		List<UserCafResponseVo> userCaf = null;
		try
		{
			userCaf = (List<UserCafResponseVo>) jdbcTemplate
					.query(GET_USERCAF, new UserCafMapper());

			if (!userCaf.isEmpty())
			{
				log.info("found  data from UserCaf");
			}

			if (userCaf.isEmpty())
			{
				log.info("No record found  in UserCaf");
			}
		}

		catch (Exception e)
		{
			log.error("Error in get ListUserCAF:" + e);
		}
		
		log.debug(" Exitting from  getListUserCAF  ");
		
		return userCaf;

	}

	private Set<Integer> convertRange2List(String range)
	{

		log.debug("Getting list for range " + range);

		Set<Integer> list = new LinkedHashSet<>();

		try
		{
			JSONObject jsonObject = new JSONObject(range);

			log.debug("jsonObject.get(\"start\") " + jsonObject.get("start"));

			Integer startIndex = Integer.valueOf(jsonObject.getInt("start"));
			Integer lastIndex = Integer.valueOf(jsonObject.getInt("end"));

			for (int index = startIndex; index <= lastIndex; index++)
			{
				list.add(index);
			}

		} catch (JSONException e)
		{
			log.error("Error While converting json to list :" + e.getMessage());
			e.printStackTrace();
		}
		log.debug("Exitting form Getting list for range " + range);

		return list;

	}

	@Override
	public UserCafResponseVo searchCAF(String cafNo)
	{
		log.info("Getting caf " + cafNo + " details from DB");

		List<UserCafResponseVo> caf = (List<UserCafResponseVo>) jdbcTemplate
				.query(GET_CAF_BY_CAF_NUMBER, new Object[] { cafNo,
						UserCaf.UN_USED }, new UserCafMapper());


		if (caf != null && !caf.isEmpty())
		{
			log.debug("Total caf for caf no " + cafNo + " is  " + caf.size());
			if (caf.size() == 1)
			{
				UserCafResponseVo cafResponseVo = caf.get(0);

				log.debug(" cafResponseVo " + cafResponseVo);

				return cafResponseVo;
			}

		} else
		{
			return null;
		}
		
		log.debug("Exitting from Getting caf " + cafNo + " details from DB");
		
		return null;

	}

	@Override
	public Integer cafReassignment(ReassignCAF reassignCAF)
	{
		if( reassignCAF == null )
			return 0;
		
		log.debug(" inside cafReassignment  "+reassignCAF.getSourceUserId());
		
		int result = jdbcTemplate.update(UPDATE_CAF, new Object[] {
				reassignCAF.getDestinationUserId(), reassignCAF.getCafNumber(),
				reassignCAF.getSourceUserId() });
		
		log.debug(" Exitting from  cafReassignment  " +reassignCAF.getSourceUserId());
		return result;
	}

}

class UserCafMapper implements RowMapper<UserCafResponseVo>
{

	@Override
	public UserCafResponseVo mapRow(ResultSet rs, int arg1) throws SQLException
	{

		UserCafResponseVo userCaf = new UserCafResponseVo();
		userCaf.setId(rs.getLong("id"));
		User user = new User();
		user.setId(rs.getLong("id"));
		userCaf.setRange(rs.getString("range"));
		userCaf.setAddedBy(rs.getLong("addedBy"));
		userCaf.setAddedOn(rs.getTimestamp("addedOn"));
		userCaf.setModifiedOn(rs.getTimestamp("modifiedOn"));
		userCaf.setModifiedBy(rs.getLong("modifiedBy"));
		userCaf.setUserId(rs.getLong("userId"));
		userCaf.setStatus(rs.getInt("status"));

		return userCaf;
	}

}

class RangeMapper implements RowMapper<String>
{

	@Override
	public String mapRow(ResultSet rowMapper, int arg1) throws SQLException
	{
		return rowMapper.getString("range");
	}

}
