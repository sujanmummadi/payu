package com.cupola.fwmp.dao.userCaf;

import java.util.List;

import com.cupola.fwmp.vo.CafDetailsVo;
import com.cupola.fwmp.vo.ReassignCAF;
import com.cupola.fwmp.vo.UserCafResponseVo;
import com.cupola.fwmp.vo.UserCafVo;

public interface UserCafDAO
{

	public int addUserCAF(UserCafVo userCafVo, Long userId);

	public int updateUserCAF(UserCafVo userCafVo, Long userId);

	public int deleteUserCAF(UserCafVo userCafVo);

	public List<UserCafResponseVo> getListUserCAF();

	public List<Integer> getUserCAFNumberByUserId(UserCafVo userCafVo);

	int removeCafEntryFromUser(String cafNumber, Long userId);

	UserCafResponseVo getUserCAFByUserId(Long userId);

	List<String> getCafDetailsByUserId(Long userId);

	Integer updateUserCafByUserId(CafDetailsVo vo);

	List<String> addCafDetails(List<CafDetailsVo> userCafVo, Long assignedUser);

	public UserCafResponseVo searchCAF(String cafNo);

	public Integer cafReassignment(ReassignCAF reassignCAF);

}
