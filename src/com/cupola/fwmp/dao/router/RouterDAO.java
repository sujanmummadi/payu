/**
 * 
 */
package com.cupola.fwmp.dao.router;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Router;
import com.cupola.fwmp.vo.sales.RouterVo;

/**
 * @author aditya
 * 
 */
public interface RouterDAO
{
	String GET_ALL_ROUTER_DETAILS = "select r.id,r.name,r.model,r.cost,r.status,r.cityId from "
			+ "Router r join City c on c.id=r.cityId where r.cityId=?";

	int bulkRouterUpload(List<Router> routers);

	List<RouterVo> getRouter(Long cityId);
}
