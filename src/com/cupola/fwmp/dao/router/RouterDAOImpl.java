/**
 * 
 */
package com.cupola.fwmp.dao.router;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.persistance.entities.Router;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.sales.RouterVo;

/**
 * @author aditya
 * 
 */
@Transactional
public class RouterDAOImpl implements RouterDAO
{
	private static Logger log = LogManager.getLogger(RouterDAOImpl.class);

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	public List<RouterVo> getRouter(Long cityId)
	{
		log.debug(" inside  getRouter "   + cityId);
		return jdbcTemplate.query(GET_ALL_ROUTER_DETAILS,new Object[]{cityId}, new RouterMapper());
	}

	class RouterMapper implements RowMapper<RouterVo>
	{
		@Override
		public RouterVo mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			RouterVo routerVo = new RouterVo();

			routerVo.setRouterId(resultSet.getLong("r.id"));
			routerVo.setRouterName(resultSet.getString("r.name"));
			routerVo.setRouterModel(resultSet.getString("r.model"));
			routerVo.setRouterCost(resultSet.getString("r.cost"));
			routerVo.setStatus(resultSet.getString("r.status"));
			routerVo.setCityId(resultSet.getLong("r.cityId"));
			return routerVo;
		}

	}

	@Override
	public int bulkRouterUpload(List<Router> routers)
	{
		int count = 0;

		
		int totalRow = routers != null ? routers.size() : 0;
		log.info("Inserting total " + totalRow 
				+ " rows in routers table");
		
		if (routers != null && !routers.isEmpty())
		{
			for (Iterator<Router> iterator = routers.iterator(); iterator
					.hasNext();)
			{
				try
				{
					Router router = (Router) iterator.next();
					router.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					Long result = (Long) hibernateTemplate.save(router);

					if (result != null)
					{
						count++;
					}
				} catch (DataAccessException e)
				{
					log.error("Error while inserting data in routers table "
							+ e.getMessage());
					e.printStackTrace();
				}

			}
			log.info("Total success " + count + " in routers table out of "
					+ routers.size());

			return count;
		} else
		{
			log.info("Total success " + count + " in routers table out of "
					+ totalRow);
			return count;
		}

	}

}
