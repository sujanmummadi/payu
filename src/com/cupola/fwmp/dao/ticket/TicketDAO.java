package com.cupola.fwmp.dao.ticket;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.CafVo;
import com.cupola.fwmp.vo.CurrentUserVO;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.GxCxPermission;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.RemarksVO;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.TicketCustomerDetailsVO;
import com.cupola.fwmp.vo.TicketEtrVo;
import com.cupola.fwmp.vo.TicketPriorityVo;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;
import com.cupola.fwmp.vo.TicketVo;
import com.cupola.fwmp.vo.WorkOrderVo;
import com.cupola.fwmp.vo.counts.TickectCountsVo;
import com.cupola.fwmp.vo.counts.dashboard.DashBoardSummaryVO;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.tools.CRMFrLogger;
import com.cupola.fwmp.vo.tools.CRMProspectLogger;
import com.cupola.fwmp.vo.tools.CRMWorkOrderLogger;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckResponse;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckVO;

public interface TicketDAO {

	public Ticket getTicketById(Long id);

	public List<Ticket> getAllTicket();

	public Ticket deleteTicket(Long id);

	public List<TicketVo> getTickets(TicketFilter filter);

	public Long createNewInstallationProspect(ProspectCoreVO prospect);

	public Integer updateEtr(TicketEtrVo etr);

	public Integer updateTicketEtr(TicketEtrVo etr);

	Gis getGisInfoTicket(Long ticketId);

	// public void updateCurrentAssignedTo(long ticketId, Long userId);

	public void assignTickets(AssignVO assignVO, Long areaId);

	public WorkOrderVo getWorkOrderUserId(Long valueOf);

	public void updateProspectNumberInTicket(Long ticketId, String prospectNo);

	TickectCountsVo getCounts(Map<Integer, String> statusList, Map<Long, String> initialWorkStages,
			Map<Long, String> tTypes, Map<Long, String> activities);

	public Integer addTicketEtr(TicketEtrVo etr);

	public Integer updateTicketCaf(CafVo cafVo, Long userId);

	Long addTicket(TicketVo ticketVo, ProspectCoreVO prospect);

	public int updateRemarks(RemarksVO remark);

	public int updateGxCXPermission(GxCxPermission gxCxPermission);

	int reassignTicket(ReassignTicketVo reassignTicketVo, Long areaId);

	Ticket getTicketByProspectNumber(String prospectNo);

	int updateCustomerMq(Long ticketId, String mqId);

	public Map<Long, String> getAllETRPEndingEntries();

	List<BigInteger> getCurrentAssignedForTickets(List<Long> ticketIds);

	Long addNewTicket(TicketVo ticketVo);

	Map<String, Long> getTicketNosFromProspects(List<String> prospectList, String location);

	TicketClosureDataVo getTicketByCustomerMobileNo(String mobileNo);

	List<TicketCustomerDetailsVO> getTicketsForCustomerAutoNotification();

	List<TicketCustomerDetailsVO> getTicketsForCustomerAutoNotificationOnETRUpdate();

	Long ticketClosureStatusUpdate(TicketClosureDataVo remark);

	List<TicketPriorityVo> getTicketPriorityVO(Set<Long> listOfTicketId);

	void updateCurrentPriority(BigInteger workOrderID, Integer priority);

	void updateReassignment(Long ticketId, Long recipientUserId, int status);

	CurrentUserVO ticketCurrentUser(Long ticketId);

	DashBoardSummaryVO calculateDashBoardStatus(Long ticketType);

	public Integer getCountEtr(Long ticketId);

	int updatePriority(long ticketNo, Integer escalatedValue, boolean modificationType);

	int updateTicketSubCategory(Long ticketId, String subCategoryName, Long subCategoryId);

	List<BigInteger> getTicketIdsForCustomerNotification(Set<BigInteger> ticketIds);

	List<Ticket> getTicketDetailsWithTicketList(List<BigInteger> ticketIds);

	public int updateDefectCode(long ticketId, long defectCode, long subDefectCode, long status, String remarks);

	boolean isEligibleForRefund(Long ticketId);

	String getProspectNumberForTicket(Long ticketId);

	void updateAadharPermanentAddress(long ticketId, String permanentAddress);

	public void updateProspectRetryCountForFailed(long ticketId);

	public Integer getTicketStatusById(Long ticketId);

	TicketClosureDataVo getTicketByCustomerMobileNoWithProspect(String mobileNo, String prospectNo);

	int updateTicketStatus(Long ticketId, int statusCode);

	void updateCurrentAssignedTo(long ticketId, Long userIdAssignedTo, Long userIdAssignedBy, int status);

	List<Ticket> getAllNotCompletedDeployedTickets();

	int updateCommunicationETR(Long ticketId, Date communicationEtr);

	boolean isPaymentApproved(Long ticketId);

//	boolean isDocumentApproved(Long ticketId);

	TicketEtrVo getEtrVo(Long ticketId);

	public void updateFrCurrentAssignedTo(long ticketId, Long userId, Long assignedBy);

	public void updateFrCurrentAssignedToViaCrm(long ticketId, Long userIdAssignedTo, Long userIdAssignedBy,
			int status);

	Map<String, Long> addNewTicketInBulk(List<MQWorkorderVO> mqWorkorderVOList,
			Map<String, CustomerVO> mqIdCustomerMap);

	public int updateFrTicketClosingCode(FRDefectSubDefectsVO resolutionCode);

	public void updateReopneTicket(Ticket ticket);

	public Map<String, Long> addNewFrTicketInBulk(List<MQWorkorderVO> mqWorkorderVOList,
			Map<String, CustomerVO> mqIdCustomerMap);

	public FrTicket getFrTicketById(Long id);

	public List<FrTicket> getAllOpenFrTicket();

	/**
	 * @author aditya 8:01:47 PM Apr 10, 2017 Long
	 * @param prospectNo
	 * @return
	 */
	Long getTicketIdByProspectNumber(String prospectNo);

	int updateTicketStatusAndAssignedBy(Long ticketId, int statusCode, Long assignedTo);

	/**
	 * @author aditya void
	 * @param valueOf
	 * @param filePath
	 * @param activityId
	 */
	int updateAadharPermanentAddressAndCustomerFlag(long ticketId, String permanentAddress,
			boolean isCustomerViaAadhaar);

	/**
	 * @author aditya void
	 * @param valueOf
	 * @param filePath
	 * @param activityId
	 */
	public int updateDocpath(Long valueOf, String filePath, long activityId, String uinType, String uin);

	/**
	 * @author aditya Map<String,Long>
	 * @param prospects
	 * @return
	 */
	Map<String, Long> getCompleteProspectStatusMap(Set<String> prospects);

	/**
	 * @author Ashraf FrTicket
	 * @param ticket
	 * @return
	 */
	public Integer updateFrTicket(FrTicket ticket);

	/**
	 * @author aditya List<TicketReassignmentLogVo>
	 * @param ticketIds
	 * @return
	 */
	List<TicketReassignmentLogVo> getCurrentAssignedForTicketsWithIds(List<Long> ticketIds);

	boolean isPOADocumentApproved(Long ticketId);

	boolean isPOIDocumentApproved(Long ticketId);

	void prospectLoggerFromSiebel(CRMProspectLogger crmProspectLogger);

	/**
	 * @author aditya WOEligibilityCheckResponse
	 * @param woEligibilityCheckVO
	 * @param cityId
	 * @return
	 */
	WOEligibilityCheckResponse isEligibleForWOCreation(WOEligibilityCheckVO woEligibilityCheckVO, Long cityId);

	/**
	 * @author kiran void
	 * @param createUpdateWorkOrderVo
	 */
	void workOrderLoggerFromSiebel(CRMWorkOrderLogger crmWorkOrderLogger);

	/**
	 * @author kiran void
	 * @param createUpdateFrVo
	 */
	void frLoggerFromSiebel(CRMFrLogger crmFrLogger);

	/**
	 * @author aditya boolean
	 * @param prospectNumber
	 * @return
	 */
	boolean isProspectPresentInDB(String prospectNumber);
	/**@author aditya
	 * int
	 * @param cafNumber
	 * @param sentTime
	 * @return
	 */
	int updateEmailSentTime(String cafNumber, Date sentTime);

	long getFrTicketIdByWorkOrder(String workOrderNumber);
	
}
