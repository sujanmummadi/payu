package com.cupola.fwmp.dao.ticket;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.DashBoardStatusDefinition;
import com.cupola.fwmp.FWMPConstant.DeDupFlag;
import com.cupola.fwmp.FWMPConstant.MQStatus;
import com.cupola.fwmp.FWMPConstant.PriorityFilter;
import com.cupola.fwmp.FWMPConstant.ProspectType;
import com.cupola.fwmp.FWMPConstant.RemarkType;
import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.FWMPConstant.TicketCategory;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.WOConversionCheck;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.area.AreaDAO;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.mongo.documents.DocumentDao;
import com.cupola.fwmp.dao.mongo.fr.FrDeviceInfoDao;
import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;
import com.cupola.fwmp.dao.ticket.app.TicketActivityData;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.persistance.entities.SubArea;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.TicketEtr;
import com.cupola.fwmp.persistance.entities.TicketPriority;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.service.customer.CustomerCoreService;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.PriorityValue;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.handlers.FrWorkOrderHandler;
import com.cupola.fwmp.service.domain.engines.classification.jobs.PriortyUpdateHelper;
import com.cupola.fwmp.service.domain.engines.doc.service.DocumentationCoreEngine;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.CafVo;
import com.cupola.fwmp.vo.CurrentUserVO;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.GxCxPermission;
import com.cupola.fwmp.vo.ProspectCoreVO;
import com.cupola.fwmp.vo.ReassignTicketVo;
import com.cupola.fwmp.vo.RemarksVO;
import com.cupola.fwmp.vo.TicketClosureDataVo;
import com.cupola.fwmp.vo.TicketCustomerDetailsVO;
import com.cupola.fwmp.vo.TicketEtrVo;
import com.cupola.fwmp.vo.TicketPriorityVo;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;
import com.cupola.fwmp.vo.TicketVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.WorkOrderVo;
import com.cupola.fwmp.vo.counts.ActivityBasedCountVo;
import com.cupola.fwmp.vo.counts.StatusBasedCountVo;
import com.cupola.fwmp.vo.counts.TickectCountsVo;
import com.cupola.fwmp.vo.counts.TypeBasedCountVo;
import com.cupola.fwmp.vo.counts.WSBasedCountVo;
import com.cupola.fwmp.vo.counts.dashboard.DashBoardSummaryVO;
import com.cupola.fwmp.vo.counts.dashboard.SummaryVO;
import com.cupola.fwmp.vo.counts.dashboard.UserSummaryVO;
import com.cupola.fwmp.vo.counts.dashboard.WiPSummaryVO;
import com.cupola.fwmp.vo.fr.FRDefectSubDefectsVO;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;
import com.cupola.fwmp.vo.tools.CRMFrLogger;
import com.cupola.fwmp.vo.tools.CRMProspectLogger;
import com.cupola.fwmp.vo.tools.CRMWorkOrderLogger;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckResponse;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityCheckVO;
import com.cupola.fwmp.vo.tools.siebel.from.vo.WOEligibilityVO;

@Transactional
public class TicketDAOImpl implements TicketDAO
{
	private Logger log = Logger.getLogger(TicketDAOImpl.class.getName());

	HibernateTemplate hibernateTemplate;
	JdbcTemplate jdbcTemplate;
	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	UserDao userDao;
	@Autowired
	DBUtil dbUtil;

	@Autowired
	DocumentationCoreEngine documentationService;

	@Autowired
	DocumentDao documentDao;
	
	@Autowired
	AreaDAO areaDao;

	@Autowired
	FrTicketService frService;

	@Autowired
	private FrDeviceInfoDao deviceDao;
	
	@Autowired
	BranchDAO branchDAO;
	
	@Autowired
	private FrWorkOrderHandler etrHandler;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	CityDAO cityDAO;
	
	@Autowired
	CustomerCoreService customerCoreService;

	private final static String GET_TICKET_ETR = "select count  from TicketETR where ticketId=:ticketId";
	private final static String UPDATE_TICKET_ETR_COUNT = "update TicketETR set approvalValue=?,count=? where ticketId=?";
	private final static String UPDATE_ETR = "update Ticket set currentEtr=?,etrUpdateMode=?,modifiedBy=?,modifiedOn=? where id=?";
	private final static String INSERT_IN_TICKTETR = "insert into TicketETR values(?,?,?,?,?,?,?,?,?,?,?,?)";
	private final static String GET_USER_FROM_WO = "select w.assignedTo,w.assignedBy,w.workOrderNumber,t.currentEtr from  WorkOrder w,Ticket t where t.id=? and  w.ticketId=?";
	private final static String UPDATE_TICKET_CAF = "UPDATE Ticket  SET cafNumber=?,modifiedBy=?,modifiedOn=? WHERE id=1465371497952019";
	private final static String UPDATE_PROSPECT_NO_TO_TICKET = "update Ticket set prospectNo = ?, mqStatus = ? where id = ?";
	private final static String UPDATE_TICKET_DEFECT_CODE = "update Ticket set defectCode=:defectCode ,subDefectCode=:subDefectCode ,status=:status, remarks = :remarks where id=:id";
	 private final static String UPDATE_CUSTOMER_PERMANENT_ADDRESS_WITH_AADHAR = "update Customer c join Ticket t on t.customerId=c.id "
				+ "set c.isCustomerViaAadhaar=? where t.id=?";
	 
	 private final static String UPDATE_TICKET_ECAF_MAIL_SENT_TIME =  "Update Ticket t set t.emailSentOn = ? where t.cafNumber=?";
	 
	// private final static String GET_COUNT_BY_STATUS =
	// "select t.status,count(t.id) from Ticket t where t.paymentStatus in
	// (:pstatusList) "
	// +
	// " OR t.documentStatus in(:dstatusList) and t.id in (:tids) group by
	// t.status";

	private final static String GET_COUNT_BY_STATUS = "select t.paymentStatus,t.documentStatus,t.status, t.id from Ticket t where t.id in (:tids) and ( t.paymentStatus in (:pstatusList) "
			+ " OR t.documentStatus in(:dstatusList) OR t.status in(:statusList) )  ";
	// "select t.status,count(t.id) from Ticket t where t.status in(:statuslist)
	// and t.id in (:tids) group by t.status";
	private final static String GET_COUNT_BY_INITIALWS = "SELECT wo.currentWorkStage , count(wo.id) FROM WorkOrder wo Left outer Join Ticket t ON t.id = wo.ticketId where wo.currentWorkStage in (:wstages) ";
	private final static String GET_COUNT_BY_TYPE = "SELECT WorkOrderTypeId , count(id) FROM WorkOrder where WorkOrderTypeId in (:types) group by WorkOrderTypeId";

	// private final static String REASSIGN_TICKET =
	// "update Ticket t, WorkOrder wo set wo.assignedTo=?,
	// t.currentAssignedTo=?, t.reason=? where t.id=wo.ticketId and t.id=?";
	private final static String REASSIGN_TICKET = "update Ticket t set t.currentAssignedTo=:currentAssignedTo, t.assignedBy=:assignedBy, t.reason=:reason where t.id in (:id)";

	private final static String REASSIGN_TICKET_EXECUTIVE = "update Ticket t set t.currentAssignedTo=:currentAssignedTo, t.assignedBy=:assignedBy, t.areaId=:areaId, t.reason=:reason where t.id in (:id)";

	private final static String UPDATE_GX_CX_USING_TICKETID_FROM_TICKET = "update Ticket set GxPermission=? ,CxPermission=? where id=?";

	private final static String UPDATE_DOC_PATH_FOR_TICKET = "update Ticket set documentPath=? where id=?";

	private final static String ACTIVITIES_COUNT_BASE = "SELECT a.id,count(a.id ) FROM WorkOrder w "
			+ " Left outer Join Ticket t ON t.id = w.ticketId "
			+ " Left outer Join WorkStage ws on w.currentWorkStage= ws.id"
			+ " Left outer Join WorkStageActivityMapping wsa on wsa.idWorkStage= ws.id"
			+ " Left outer Join  Activity a on a.id = wsa.idActivity  where t.id > 0 ";

	private final static String ACTIVITIES_LOG_COUNT_BASE = "SELECT  tal.activityId  , count(tal.activityId) From TicketActivityLog  tal "
			+ "  Left outer Join Ticket t ON t.id = tal.ticketId "
			+ "  where tal.status =" + TicketStatus.COMPLETED + " ";

	private static String TICKET_DETAIL_CUSTOMER_DETAIL_QUERY = "SELECT wo.workOrderNumber,t.id as ticketId,"
			+ "t.modifiedOn,t.status as statusId," + "c.mqId,"
			+ "c.id as customerId," + "c.mobileNumber as mobileNumber,"
			+ "c.emailId as customerEmail " + " FROM  "
			+ "Ticket t INNER JOIN Customer c on t.customerId = c.id INNER JOIN WorkOrder wo ON t.id = wo.ticketId where  c.id > 0 and t.status =? and TIMESTAMPDIFF(HOUR,t.modifiedOn,NOW()) <=?";

	private static String ETR_UPDATE_QUERY = "SELECT t.id as ticketId, t.currentEtr as currentEtr, t.commitedETR,"
			+ "t.modifiedOn,t.status as statusId," + "c.mqId,"
			+ "c.id as customerId," + "c.mobileNumber as mobileNumber,"
			+ "c.emailId as customerEmail " + " FROM  "
			+ "Ticket t INNER JOIN Customer c on t.customerId = c.id where  c.id > 0 and t.currentEtr > t.commitedETR and DATEDIFF(t.commitedETR,NOW()) = 0";

	private static String PRIORITY_INFO_QUERY = "select wo.id as workorderId, wo.workOrderNumber as workOrderNumber, wo.ticketId as ticketId, wo.addedOn as createDate,  wo.assignedTo as assignedTo, wo.totalPriority as priority  from WorkOrder wo where wo.ticketId in (:tids)";

	private static String TICKET_USER_INFO_QUERY = "select id, assignedBy, currentAssignedTo, assignedTo from Ticket where id = ?";

	private static final String PROSPECT_NUMBER_BY_TICKET = "select prospectNo from Ticket where id = :ticketId";

	/*
	 * private final static String WIP_COUNT =
	 * "SELECT  t.id as tId , a.id as aId, w.initialWorkStage," +
	 * " t.currentAssignedTo ,tpr.value , u.firstName FROM User u " +
	 * "   LEFT OUTER JOIN  Ticket t  on t.currentAssignedTo= u.id and (u.reportTo in (:userId) or  u.id in(:userId))  "
	 * + "   Left outer Join TicketPriority tpr on tpr.id = t.priorityId " +
	 * " LEFT OUTER JOIN WorkOrder w on  t.id = w.ticketId " +
	 * "   LEFT OUTER JOIN TicketActivityLog tal on tal.ticketId= t.id"
	 * 
	 * 
	 * +
	 * "   Left outer Join  Activity a on a.id = tal.activityId where t.id > 0 "
	 * ;
	 */

	private final static String WIP_COUNT = "SELECT t.id as tId , a.id as aId, w.currentWorkStage, u.id,tpr.value ,u.firstName, ur.id as urId,ur.firstName as reporttouser"
			+ " FROM User u    LEFT OUTER JOIN  Ticket t  on t.currentAssignedTo= u.id "
			+ " LEFT JOIN User ur   on  ur.id  = u.reportTo "
			+ " Left outer Join TicketPriority tpr on tpr.id = t.priorityId "
			+ " LEFT OUTER JOIN WorkOrder w on  t.id = w.ticketId  "
			+ " LEFT OUTER JOIN TicketActivityLog tal on tal.ticketId= t.id   "
			+ " Left outer Join  Activity a on a.id = tal.activityId  where t.id > 0 ";

	private static final Long NI_TL_GROUP = 10l;

	@Value("${tickets.for.customer.notification}")
	private String TICKETS_FOR_CUSTOMER_NOTIFICATION;

	private static String GET_ALL_NOT_COMPLETED_TICKETS = "SELECT t.*  FROM Ticket t left Outer JOIN WorkOrder wo ON t.id = wo.ticketId where t.status not in (:status) and DATE_FORMAT(t.communicationETR, '%Y-%m-%d') = CURDATE()";

	private static String UPDATE_COMMUNICATION_ETR = "update Ticket t set t.communicationETR=:communicationETR , t.currentEtr=:currentEtr where t.id = :id";

	// select * from Ticket t where t.currentEtr > t.commitedETR and
	// DATEDIFF(t.commitedETR,NOW()) = 0
	// private final static String
	// GET_TICKETS_BASED_ON_DIFFERENCE_IN_MODIFIED_ON_TIME =
	// "select * from Ticket t where t.status =? and
	// TIMESTAMPDIFF(HOUR,t.modifiedOn,NOW())=?) ";

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{

		this.hibernateTemplate = hibernateTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{

		this.jdbcTemplate = jdbcTemplate;

	}

	public HibernateTemplate getHibernateTemplate()
	{

		return this.hibernateTemplate;
	}

	@Override
	public void updateCurrentPriority(BigInteger workOrderID, Integer priority)
	{
		log.debug(" inside  updateCurrentPriority  " + workOrderID);
		
		String query = "update WorkOrder w set w.totalPriority=" + priority
				+ " where w.id=" + workOrderID;
		getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createSQLQuery(query).executeUpdate();

	}

	@Override
	public TickectCountsVo getCounts(Map<Integer, String> statusList,
			Map<Long, String> initialWorkStages, Map<Long, String> tTypes,
			Map<Long, String> activities)
	{
		log.debug(" inside  getCounts method  "  );
		
		List<Integer> status = new ArrayList<Integer>(statusList.keySet());
		List<Long> wstages = new ArrayList<Long>(initialWorkStages.keySet());
		List<Long> types = new ArrayList<Long>(tTypes.keySet());
		TickectCountsVo counts = new TickectCountsVo();

		String key = dbUtil
				.getKeyForQueueByUserId(AuthUtils.getCurrentUserId());
		Set<Long> ticketListForWorkingQueue = globalActivities
				.getAllTicketFromMainQueueByKey(key);

		if (ticketListForWorkingQueue == null
				|| ticketListForWorkingQueue.size() <= 0)
		{
			ticketListForWorkingQueue = new HashSet<Long>(1);
			ticketListForWorkingQueue.add(-1l);
		}

		Session session = this.getHibernateTemplate().getSessionFactory()
				.getCurrentSession();
		try
		{

			if (activities != null && activities.size() > 0)
			{
				calculateActivitiesStatus(counts, activities, ticketListForWorkingQueue);
			}
			// p,d
			List<Long> statusCounts = new ArrayList<Long>();

			if (status != null && status.size() > 0)
			{

				Query q = session.createSQLQuery(GET_COUNT_BY_STATUS);

				q.setParameterList("tids", ticketListForWorkingQueue);
				q.setParameterList("pstatusList", status);
				q.setParameterList("dstatusList", status);
				List<Integer> statuses = new ArrayList<Integer>();
				if (status.contains(TicketStatus.OPEN))
					statuses.add(TicketStatus.OPEN);
				if (status.contains(TicketStatus.WO_UNASSIGNED_TICKET) || status
						.contains(TicketStatus.SALES_UNASSIGNED_TICKET))
					statuses.add(TicketStatus.SALES_UNASSIGNED_TICKET);

				if (statuses.size() == 0)
				{
					statuses.add(-1);
				}

				q.setParameterList("statusList", statuses);
				List statusBasedCount = q.list();

				List<StatusBasedCountVo> voList = new ArrayList<StatusBasedCountVo>();
				List<Integer> processedStatus = new ArrayList<Integer>();
				log.debug(" statusBasedCount size " + statusBasedCount.size());
				

				if (ticketListForWorkingQueue != null
						&& ticketListForWorkingQueue.size() > 0
						&& statusBasedCount != null
						&& statusBasedCount.size() > 0)
				{
					
					log.debug(" statusBasedCount size " +statusBasedCount.size());

					log.debug(" statusBasedCount size " + statusBasedCount.size());
					for (Iterator iterator = statusBasedCount
							.iterator(); iterator.hasNext();)
					{
						Object[] ob = (Object[]) iterator.next();

						if (ob[0] != null || ob[1] != null)

						{
							if (ob[0] != null)
								statusCounts.add(objectToLong(ob[0]));
							if (ob[1] != null)
								statusCounts.add(objectToLong(ob[1]));
						} else if (ob[2] != null)
							statusCounts.add(objectToLong(ob[2]));

					}
					Set<Long> uniqueStatus = new HashSet<Long>(statusCounts);

					for (Iterator iterator = uniqueStatus.iterator(); iterator
							.hasNext();)
					{
						Long st = (Long) iterator.next();

						Integer id = st.intValue();
						processedStatus.add(id);
						StatusBasedCountVo vo = new StatusBasedCountVo(id
								.longValue(), statusList.get(id), statusList
										.get(id), Collections
												.frequency(statusCounts, st));
						voList.add(vo);

					}
					log.debug(uniqueStatus.size() + " " + statusCounts.size());
					log.debug(ticketListForWorkingQueue + " "
							+ q.getQueryString());
					log.debug(status);
					log.debug(voList.size() + " status count result " + voList);

				}

				status.removeAll(processedStatus);
				for (Iterator iterator = status.iterator(); iterator.hasNext();)
				{
					Integer integer = (Integer) iterator.next();

					StatusBasedCountVo vo = new StatusBasedCountVo(integer
							.longValue(), statusList
									.get(integer), statusList.get(integer), 0);
					voList.add(vo);

				}

				log.debug(voList.size() + " last status count result "
						+ voList);

				counts.setStatusCounts(voList);
			}

			if (wstages != null && wstages.size() > 0)
			{
				StringBuilder completeQuery = new StringBuilder(GET_COUNT_BY_INITIALWS);
				if (ticketListForWorkingQueue.size() > 0)
					completeQuery.append(" and t.id in (:ticketIds) ");

				completeQuery.append(" group by wo.currentWorkStage ");

				log.debug("------Count query ws type  ---------");
				log.debug("ticketIds " + ticketListForWorkingQueue);
				log.debug("wstages " + wstages);

				log.debug(completeQuery);
				log.debug("------ ---------");

				Query query = session.createSQLQuery(completeQuery.toString());
				if (ticketListForWorkingQueue.size() > 0)
					query.setParameterList("ticketIds", ticketListForWorkingQueue);

				List wsBasedCount = query.setParameterList("wstages", wstages)

						.list();

				List<WSBasedCountVo> voList = new ArrayList<WSBasedCountVo>();

				if (wsBasedCount != null && wsBasedCount.size() > 0)
				{
					List<Long> processed = new ArrayList<Long>();
					for (Iterator iterator = wsBasedCount.iterator(); iterator
							.hasNext();)
					{
						Object[] ob = (Object[]) iterator.next();
						Long id = objectToLong(ob[0]);
						processed.add(id);

						WSBasedCountVo vo = new WSBasedCountVo(id, initialWorkStages
								.get(id), initialWorkStages
										.get(id), objectToInt(ob[1]));
						voList.add(vo);
					}
					wstages.removeAll(processed);
					for (Iterator iterator = wstages.iterator(); iterator
							.hasNext();)
					{
						Long value = (Long) iterator.next();

						WSBasedCountVo vo = new WSBasedCountVo(value, initialWorkStages
								.get(value), initialWorkStages.get(value), 0);
						voList.add(vo);

					}
					counts.setWsCounts(voList);

				}
			}
			if (types != null && types.size() > 0)
			{
				List typeBasedCount = session.createSQLQuery(GET_COUNT_BY_TYPE)
						.setParameterList("types", types).list();
				List<TypeBasedCountVo> voList = new ArrayList<TypeBasedCountVo>();
				if (typeBasedCount != null && typeBasedCount.size() > 0)
				{
					List<Long> processed = new ArrayList<Long>();
					for (Iterator iterator = typeBasedCount.iterator(); iterator
							.hasNext();)
					{
						Object[] ob = (Object[]) iterator.next();
						Long id = objectToLong(ob[0]);
						processed.add(id);
						TypeBasedCountVo vo = new TypeBasedCountVo(id, tTypes
								.get(id), tTypes.get(id), objectToInt(ob[1]));
						voList.add(vo);
					}

					types.removeAll(processed);
					for (Iterator iterator = types.iterator(); iterator
							.hasNext();)
					{
						Long value = (Long) iterator.next();

						TypeBasedCountVo vo = new TypeBasedCountVo(value, tTypes
								.get(value), tTypes.get(value), 0);
						voList.add(vo);

					}
					counts.setTypeCounts(voList);
				}

			}
		} catch (Exception e)
		{
			log.error("Erroe While Getting ticket counts :" + e.getMessage());
			e.printStackTrace();
		} finally
		{
			// session.close();
		}
		log.debug(" exit from  getCounts method  "  );
		return counts;

	}

	private void calculateActivitiesStatus(TickectCountsVo counts,
			Map<Long, String> activities, Set<Long> ticketListForWorkingQueue)
	{
		if(activities == null)
			return;
		
		log.debug(" inside  calculateActivitiesStatus  " +activities.size());
		StringBuilder completeQuery = new StringBuilder(ACTIVITIES_COUNT_BASE);
		if (AuthUtils.isNIUser())

		{
			completeQuery.append(" and a.id in (:activities)");

			if (ticketListForWorkingQueue.size() > 0)
				completeQuery.append(" and t.id in (:ticketIds) ");

			completeQuery.append(" group by a.id ");
			log.debug("------Count query activity based ---------");
			log.debug("ticketIds " + ticketListForWorkingQueue);
			log.debug("activities " + activities.keySet());

			log.debug(completeQuery);
			log.debug("------ ---------");

			Session s = getHibernateTemplate().getSessionFactory()
					.getCurrentSession();
			Query query = s.createSQLQuery(completeQuery.toString());

			if (ticketListForWorkingQueue.size() > 0)
				query.setParameterList("ticketIds", ticketListForWorkingQueue);

			query.setParameterList("activities", activities.keySet());

			List<Object[]> result = query.list();

			List<ActivityBasedCountVo> activitiesCounts = new ArrayList<ActivityBasedCountVo>();
			if (result == null || result.size() == 0)
			{
				for (Iterator iterator = activities.keySet()
						.iterator(); iterator.hasNext();)
				{
					Long activity = (Long) iterator.next();
					activitiesCounts
							.add(new ActivityBasedCountVo(activity, activities
									.get(objectToLong(activity)), activities
											.get(activity), 0));

				}
			} else
			{

				for (Object[] obj : result)
				{
					activitiesCounts
							.add(new ActivityBasedCountVo(objectToLong(obj[0]), activities
									.get(objectToLong(obj[0])), activities
											.get(objectToLong(obj[0])), objectToInt(obj[1])));
				}

				completeQuery = new StringBuilder(ACTIVITIES_LOG_COUNT_BASE);
				if (ticketListForWorkingQueue.size() > 0)
					completeQuery.append("and t.id in (:ticketIds) ");

				completeQuery.append(" group by tal.activityId ");

				log.debug("------Count status activity based ---------"
						+ completeQuery);

				query = s.createSQLQuery(completeQuery.toString());
				if (ticketListForWorkingQueue.size() > 0)
					query.setParameterList("ticketIds", ticketListForWorkingQueue);
				result = query.list();
				Map<Long, Integer> activityCountMap = new HashMap<Long, Integer>();

				if (result != null)
				{
					for (Object[] obj : result)
					{

						activityCountMap
								.put(objectToLong(obj[0]), objectToInt(obj[1]));
					}

					log.debug("------ ---------" + activityCountMap);
					for (Iterator iterator = activitiesCounts
							.iterator(); iterator.hasNext();)
					{
						ActivityBasedCountVo activityBasedCountVo = (ActivityBasedCountVo) iterator
								.next();
						if (activityCountMap
								.containsKey(activityBasedCountVo.getId()))
						{
							activityBasedCountVo
									.setPendingCount(activityBasedCountVo
											.getCount()
											- activityCountMap
													.get(activityBasedCountVo
															.getId()));
						} else
						{
							activityBasedCountVo
									.setPendingCount(activityBasedCountVo
											.getCount());
						}
					}
				}

			}

			counts.setActivitiesCounts(activitiesCounts);
		}

	}

	@Override
	public Long addTicket(TicketVo ticketVo, ProspectCoreVO prospect)
	{
		Long id = 0l;
		if (ticketVo == null || prospect == null)
			return id;
		
		log.debug("%%%%%%%%%% " + ticketVo + " %%%%%%%%%%% " + prospect);
		
		log.debug(" inside  addTicket  " + ticketVo.getAreaId());
		try
		{
			Ticket ticket = new Ticket();

			if (ticketVo.getStatus() != null && ticketVo.getStatus()
					.equalsIgnoreCase(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED
							+ ""))
				ticket.setStatus(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);
			else
				ticket.setStatus(TicketStatus.OPEN);

			if (prospect.getTicket().getStatus() != null)
				ticket.setStatus(Integer
						.valueOf(prospect.getTicket().getStatus()));
			ticket.setCustomer(getHibernateTemplate()
					.load(Customer.class, ticketVo.getCustomerId()));

			if (prospect.getTicket().getTicketSubCategory() != null)
				ticket.setTicketSubCategory(prospect.getTicket()
						.getTicketSubCategory());
			
//			log.debug("prospect.getCustomer().getSubAreaId() " + prospect.getCustomer().getSubAreaId());
			
			if (prospect.getCustomer().getSubAreaId() != null && prospect.getCustomer().getSubAreaId() > 0)
				ticket.setSubArea(getHibernateTemplate().load(SubArea.class, prospect.getCustomer().getSubAreaId()));
			
			ticket.setTicketCategory(TicketCategory.NEW_INSTALLATION);
			ticket.setTicketCategoryId(GenericUtil
					.getCategoryIdByString(ticket.getTicketCategory()));
			ticket.setTicketCreationDate(new Date());
			ticket.setTicketCreationMode(prospect.getCreationSource());
			ticket.setSource(prospect.getCreationSource());
			ticket.setSubAttribute(ticketVo.getSubAttribute());

			if (ticketVo.getMqStatus() !=null && 
					MQStatus.FAILED.equals(Integer.valueOf(ticketVo.getMqStatus())))
			{
				ticket.setMqStatus(MQStatus.FAILED);
			} else
				ticket.setMqStatus(MQStatus.SUCCESS);

			
			log.debug("ticketVo.getMqStatus() " + ticketVo.getMqStatus());
			
			if (ticketVo.getMqStatus() != null
					&& ticketVo.getMqStatus().equalsIgnoreCase(DeDupFlag.deDupFlagSuccess + ""))
				ticket.setMqStatus(DeDupFlag.deDupFlagSuccess);
			
			if (ticketVo.getMqStatus() != null
					&& ticketVo.getMqStatus().equalsIgnoreCase(DeDupFlag.deDupFlagFailure + ""))
				ticket.setMqStatus(DeDupFlag.deDupFlagFailure);
			
			ticket.setAddedOn(new Date());
			ticket.setModifiedOn(new Date());
			ticket.setStatus(TicketStatus.OPEN);
			// ticket.setModifiedBy(prospect.getTicket().getAssignTo());

			if (prospect.getCustomer() != null)
			{

				ticket.setAddedBy(prospect.getCustomer().getAddedBy());
				ticket.setModifiedBy(prospect.getCustomer().getAddedBy());
				ticket.setStatus(TicketStatus.OPEN);

				if (prospect.getCustomer() != null)
				{

					ticket.setPreferedDate(prospect.getCustomer()
							.getPrefferedCallDate());
				}
				// ticket.setStatus(prospect.getTicket().getStatus());
			}

			ticket.setProspectNo(prospect.getTicket().getProspectNo());
			ticket.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());
			ticket.setPreferedDate(prospect.getTicket().getPreferedDate());

			if (ticketVo != null)
			{
				if (ticketVo.getAreaId() > 0)
					ticket.setArea(getHibernateTemplate()
							.load(Area.class, ticketVo.getAreaId()));
				if (ticketVo.getAssignedBy() > 0)
					ticket.setUserByAssignedBy(hibernateTemplate
							.load(User.class, ticketVo.getAssignedBy()));
				if(ticketVo.getSubAreaId() > 0)
					ticket.setSubArea(getHibernateTemplate()
							.load(SubArea.class, ticketVo.getSubAreaId()));
			}

			if (prospect.getTicket().getCommitedEtr() != null)
			{
				ticket.setCommitedEtr(GenericUtil
						.convertFromUiDateFormat(prospect.getTicket()
								.getCommitedEtr()));
				ticket.setCurrentEtr(GenericUtil
						.convertFromUiDateFormat(prospect.getTicket()
								.getCommitedEtr()));

				ticket.setCommunicationETR(GenericUtil
						.convertFromUiDateFormat(prospect.getTicket()
								.getCommitedEtr()));
			}

			log.debug(" prospect.getTicket().getAssignTo() "
					+ prospect.getTicket().getAssignTo());

			if (prospect.getTicket().getAssignTo() > 0)
			{
				User user = getHibernateTemplate()
						.load(User.class, prospect.getTicket().getAssignTo());
				if (user != null)
				{
					ticket.setUserByAssignedTo(user);
					ticket.setUserByCurrentAssignedTo(user);
					ticket.setUserBySalesDoneBy(user);

				}
				if (prospect.getTicket().getAssignedBy() > 0)
				{
					User userAssignedBy = hibernateTemplate
							.load(User.class, prospect.getTicket()
									.getAssignedBy());
					if (user != null)
					{
						ticket.setUserByAssignedBy(userAssignedBy);
					}
				}

			}

			ticket.setPreferedDate(prospect.getTicket().getPreferedDate());

			ticketVo = xformToVo(ticket);
			ticket.setProspectNo(prospect.getTicket().getProspectNo());
			TicketPriority p = new TicketPriority();
			p.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			p.setModifiedOn(new Date());
			p.setValue(0);
			p.setUpdatedCount(0);
			p.setModifiedBy(ticket.getAddedBy());

			ticket.setTicketPriority(p);

			id = (Long) getHibernateTemplate().save(ticket);

			ticket.setId(id);

		} catch (Exception e)
		{
			log.error("Error While adding Ticket :" + e.getMessage());
			e.printStackTrace();
		}
		log.debug(" exit from  addTicket  " + ticketVo.getAreaId());
		return id;
	}

	/**
	 * Only by Clasiification Engine
	 */
	@Override
	public Long addNewTicket(TicketVo ticketVo)
	{
		Long id = 0l;
		if (ticketVo == null)
			return id;

		log.debug(" inside  addNewTicket  " +ticketVo.getAreaCode());
		try
		{
			Ticket ticket = new Ticket();
			if (ticketVo !=null && ticketVo.getStatus()
					.equalsIgnoreCase(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED
							+ ""))
				ticket.setStatus(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);
			else
				ticket.setStatus(TicketStatus.OPEN);

			if (ticketVo !=null && ticketVo.getStatus() != null)
				ticket.setStatus(Integer.valueOf(ticketVo.getStatus()));
			ticket.setCustomer(getHibernateTemplate()
					.load(Customer.class, ticketVo.getCustomerId()));

			if (ticketVo.getTicketSubCategory() != null)
				ticket.setTicketSubCategory(ticketVo.getTicketSubCategory());

			ticket.setTicketCategory(TicketCategory.NEW_INSTALLATION);
			if (ticketVo.getTicketCategory() != null)
				ticket.setTicketCategory(ticketVo.getTicketCategory());

			ticket.setTicketCategoryId(GenericUtil
					.getCategoryIdByString(ticket.getTicketCategory()));

			ticket.setTicketCreationDate(new Date());
			ticket.setTicketCreationMode(ticketVo.getSource());
			ticket.setSource(ticketVo.getSource());

			ticket.setAddedOn(new Date());
			ticket.setModifiedOn(new Date());
			// ticket.setStatus(TicketStatus.OPEN);

			ticket.setAddedBy(ticketVo.getUpdatedBy());
			ticket.setModifiedBy(ticketVo.getUpdatedBy());
			// ticket.setStatus(TicketStatus.OPEN);

			ticket.setProspectNo(ticketVo.getProspectNo());
			ticket.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());

			if (ticketVo != null && ticketVo.getAreaId() > 0)
			{
				ticket.setArea(hibernateTemplate
						.load(Area.class, ticketVo.getAreaId()));
				
				log.info("Got area for prospect " + ticketVo.getProspectNo());
				
			} else
				
			{
				ticket.setArea(null);
				ticket.setSource("OUTSIDE FWMP");

			}
			 
			if (ticketVo.getAssignedBy() > 0)
				ticket.setUserByAssignedBy(hibernateTemplate
							.load(User.class, ticketVo
									.getAssignedBy()));
			

			if (ticketVo.getCommitedEtr() != null)
			{
				ticket.setCommitedEtr(GenericUtil
						.convertStringToDateFormat(ticketVo.getCommitedEtr()));
				ticket.setCurrentEtr(GenericUtil
						.convertStringToDateFormat(ticketVo.getCommitedEtr()));
			}


			if (ticketVo.getAssignTo() > 0)
			{
				User user = getHibernateTemplate()
						.load(User.class, ticketVo.getAssignTo());
				if (user != null)
				{
					ticket.setUserByAssignedTo(user);
					ticket.setUserByCurrentAssignedTo(user);

				}
			}

			if (ticketVo.getFxName() != null)
				ticket.setFxName(ticketVo.getFxName());

			ticket.setPreferedDate(ticketVo.getPreferedDate());

			TicketPriority p = new TicketPriority();
			p.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			p.setModifiedOn(new Date());
			p.setValue(0);
			p.setUpdatedCount(0);
			p.setModifiedBy(ticket.getAddedBy());
			p.setEscalatedValue(0);
			p.setSliderEscalatedValue(0);

			ticket.setTicketPriority(p);

			ticketVo = xformToVo(ticket);
			ticket.setProspectNo(ticketVo.getProspectNo());
			id = (Long) getHibernateTemplate().save(ticket);

			ticket.setId(id);

			if (ticket.getTicketCategoryId() != null
					&& ticket.getTicketCategoryId().equals(TicketCategory.FR))
			{
				FrTicketEtrDataVo etrDataVo = new FrTicketEtrDataVo();
				etrDataVo.setSymptomName(ticketVo.getSymptomCode());
				etrDataVo.setTicketId(id);
				etrDataVo.setCityCode(LocationCache.getCityCodeById(ticket.getCustomer().getCity().getId()));
				etrDataVo.setBranchCode(branchDAO.getBranchCodeById(ticket.getCustomer().getBranch().getId()));
				frService.createFrTicketEtr(etrDataVo);
			}

		} catch (Exception e)
		{
			log.error("Error While Adding new ticket :" + e.getMessage());
			e.printStackTrace();
		}
		log.debug(" exit from  addNewTicket  " +ticketVo.getAreaCode());
		return id;
	}

	private Area getAreaById( Long areaId )
	{
		log.info("Trying to load Area by area ID : "+areaId);
		if( areaId == null || areaId.longValue() <= 0 )
			return null;
		try
		{
			return hibernateTemplate
					.load( Area.class, areaId );
		}
		catch (Exception e) {
			log.error("Error occured while loading Area by area ID : "+areaId,e);
			return null;
		}
	}

	@Override
	public Map<String, Long> addNewFrTicketInBulk(
			List<MQWorkorderVO> mqWorkorderVOList,
			Map<String, CustomerVO> mqIdCustomerMap)
	{

		log.info(" Inserting Fr Ticket...... " + mqWorkorderVOList.size());
		Long id = 0l;

		Map<String, Long> prospectNoTicketIdMap = null;
		int totalTicketSize = 0;
		List<FrTicketEtrDataVo> etrTobeCreate = null;
		;
		try {
			prospectNoTicketIdMap = new ConcurrentHashMap<String, Long>();

			totalTicketSize = mqWorkorderVOList.size();
			etrTobeCreate = new ArrayList<FrTicketEtrDataVo>();

			for (Iterator<MQWorkorderVO> iterator = mqWorkorderVOList.iterator(); iterator.hasNext();) {
				log.info("inside loop..........remaining object : ");
				try {
					MQWorkorderVO mqWorkorderVO = (MQWorkorderVO) iterator.next();

					if (mqIdCustomerMap.containsKey(mqWorkorderVO.getMqId())) {
						CustomerVO customerVo = mqIdCustomerMap.get(mqWorkorderVO.getMqId());

						log.info("inside loop.if.contains FR MQ " + mqWorkorderVO.getMqId() + " and customerId is "
								+ customerVo.getId() + " wo " + mqWorkorderVO.getWorkOrderNumber());

						TicketVo ticketVo = xformToTicket(mqWorkorderVO, customerVo);

						log.info("Ticket transformation completed for " + "mqWorkorderVO.getWorkOrderNumber() "
								+ mqWorkorderVO.getWorkOrderNumber() + " " + ticketVo + "");
						try {
							FrTicket ticket = new FrTicket();
							ticket.setStatus(TicketStatus.OPEN);

							if (ticketVo.getStatus() != null)
								ticket.setStatus(Integer.valueOf(ticketVo.getStatus()));

							log.info("Setting Customer details for " + mqWorkorderVO.getWorkOrderNumber());

							ticket.setCustomer(getHibernateTemplate().load(Customer.class, ticketVo.getCustomerId()));

							log.info("Setted Customer details for " + mqWorkorderVO.getWorkOrderNumber());

							if (ticketVo.getTicketSubCategory() != null)
								ticket.setTicketSubCategory(ticketVo.getTicketSubCategory());

							log.debug("ticket.getTicketSubCategory " + ticket.getTicketSubCategory());

							ticket.setTicketCategory(TicketCategory.NEW_INSTALLATION);

							log.debug("ticket.setTicketCategory " + ticket.getTicketCategory());

							if (ticketVo.getTicketCategory() != null)
								ticket.setTicketCategory(ticketVo.getTicketCategory());

							log.debug("before setTicketCategoryId loop.if.........remaining object : " + ticketVo);

							ticket.setTicketCategoryId(GenericUtil.getCategoryIdByString(ticket.getTicketCategory()));

							log.debug("ticket.getTicketCategory() " + ticket.getTicketCategory());

							ticket.setTicketCreationDate(new Date());
							ticket.setTicketCreationMode(ticketVo.getSource());

							log.debug("ticketVo.getSource() " + ticketVo.getSource());

							ticket.setSource(ticketVo.getSource());

							log.debug("ticketVo.getSource() " + ticketVo.getSource());

							ticket.setAddedOn(new Date());
							ticket.setModifiedOn(new Date());
							// ticket.setStatus(TicketStatus.OPEN);
							ticket.setCrmCreatedOn(ticketVo.getCrmCreatedOn());
							ticket.setAddedBy(ticketVo.getUpdatedBy());
							ticket.setModifiedBy(ticketVo.getUpdatedBy());
							// ticket.setStatus(TicketStatus.OPEN);

							ticket.setProspectNo(ticketVo.getProspectNo());

							log.debug("ticketVo.getProspectNo() " + ticketVo.getProspectNo());
							
							//added by manjuprasad for ticket description voc issue
							ticket.setTicketDescriptionVoc(ticketVo.getTicketDescriptionVoc());

							ticket.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());

							ticket.setPushBackCount(0);

							log.debug("after pushback loop.if.........remaining object : " + ticketVo.getAreaId());

							/*
							 * ticket.setArea(hibernateTemplate .load(Area.class, ticketVo.getAreaId()));
							 */
							log.info("Setting Area details for " + mqWorkorderVO.getWorkOrderNumber());
							ticket.setArea(getAreaById(ticketVo.getAreaId()));

							log.info("Setted Area details for " + mqWorkorderVO.getWorkOrderNumber());

							if (ticketVo.getAssignedBy() > 0)
								ticket.setUserByAssignedBy(
										hibernateTemplate.load(User.class, ticketVo.getAssignedBy()));
							log.info("Setted User for " + mqWorkorderVO.getWorkOrderNumber());

							/*
							 * if (ticketVo.getCommitedEtr() != null) { ticket.setCommitedEtr(GenericUtil
							 * .convertStringToDateFormat(ticketVo .getCommitedEtr()));
							 * ticket.setCurrentEtr(GenericUtil .convertStringToDateFormat(ticketVo
							 * .getCommitedEtr())); }
							 */
							log.info("Setting Assigned by details for " + mqWorkorderVO.getWorkOrderNumber());
							if (ticketVo.getAssignTo() > 0) {
								User user = getHibernateTemplate().load(User.class, ticketVo.getAssignTo());
								log.debug("user " + user);

								if (user != null) {
									ticket.setUserByAssignedTo(user);
									ticket.setUserByCurrentAssignedTo(user);

								}

								log.debug("Exit " + user);
							}
							log.info("Setted Assigned by details for " + mqWorkorderVO.getWorkOrderNumber());
							if (ticketVo.getFxName() != null)
								ticket.setFxName(ticketVo.getFxName());

							FrDeviceVo device = mqWorkorderVO.getDeviceData();
							log.debug("Device " + device);

							ticket.setFxMac(device.getFxMax());
							ticket.setFxIp(device.getFxIp());
							ticket.setFxPort(device.getFxPort() != null ? device.getFxPort() + "" : null);

							ticket.setCxName(device.getCxName());
							ticket.setCxMac(device.getCxMax());
							ticket.setCxIp(device.getCxIp());
							ticket.setCxPort(device.getCxPort() != null ? device.getCxPort() + "" : null);
							log.info("Setted Device details for " + mqWorkorderVO.getWorkOrderNumber());
							ticket.setPreferedDate(ticketVo.getPreferedDate());

							if (mqWorkorderVO.getWorkOrderNumber() != null)
								ticket.setWorkOrderNumber(mqWorkorderVO.getWorkOrderNumber());

							log.info("Getting...Vicinity.....priority : " + mqWorkorderVO.getWorkOrderNumber());
							int priority = frService.getVicinityTicketPriority(mqWorkorderVO);
							log.info("Done...Vicinity.....priority : " + priority + " "
									+ mqWorkorderVO.getWorkOrderNumber());

							TicketPriority p = new TicketPriority();
							p.setId(StrictMicroSecondTimeBasedGuid.newGuid());
							p.setModifiedOn(new Date());
							p.setValue(priority);
							p.setUpdatedCount(0);
							p.setModifiedBy(ticket.getAddedBy());
							p.setEscalatedValue(0);
							p.setSliderEscalatedValue(0);
							p.setEscalationType(0);

							log.info("Setted Priority details for " + mqWorkorderVO.getWorkOrderNumber());
							ticket.setTicketPriority(p);

							log.info("setEscalationType Value From FrTicket:"+ ticket.getTicketPriority().getEscalatedValue());
							

							ticketVo = xformToVo(ticket);

							log.debug("Ticket transformed " + ticketVo);

							ticket.setProspectNo(ticketVo.getProspectNo());
							id = (Long) getHibernateTemplate().save(ticket);
							hibernateTemplate.flush();
							log.info("Ticket saved " + id + " for work order " + mqWorkorderVO.getWorkOrderNumber()
									+ " crm created on " + mqWorkorderVO.getReqDate());

							ticket.setId(id);

							/*
							 * if (ticket.getTicketCategoryId() != null && ticket
							 * .getTicketCategoryId().equals(TicketCategory.FR)) {
							 */
							log.debug("creating object********** FrTicketEtrDataVo");
							FrTicketEtrDataVo etrDataVo = new FrTicketEtrDataVo();
							etrDataVo.setSymptomName(mqWorkorderVO.getSymptom());
							etrDataVo.setTicketId(id);
							etrDataVo.setWorkOrderNumber(mqWorkorderVO.getWorkOrderNumber());
							etrDataVo.setMobileNumber(customerVo.getMobileNumber());
							etrDataVo.setMqId(mqWorkorderVO.getMqId());
							etrDataVo.setCityCode(LocationCache.getCityCodeById(customerVo.getCityId()));
							if (customerVo.getBranchId() != null)
								etrDataVo.setBranchCode(branchDAO.getBranchCodeById(customerVo.getBranchId()));
	//changed by manjuprasad for FRticket etr table data insertion						
//							frService.createFrTicketEtr( etrDataVo );
							etrTobeCreate.add(etrDataVo);

							log.debug("Done object********** FrTicketEtrDataVo remaing are " + --totalTicketSize);
							/* } */

							prospectNoTicketIdMap.put(ticket.getWorkOrderNumber(), ticket.getId());

						} catch (Exception e) {
							log.error("Error While Adding new Fr ticket :" + mqWorkorderVO.getWorkOrderNumber()
									+ e.getMessage());
							e.printStackTrace();
							continue;
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
					log.error("Error for wo " + e.getMessage());
					continue;
				}

			}

			log.info("Started Flush ");

			hibernateTemplate.flush();

			log.info("Started etr calculation ");

			etrHandler.calculateFrTicketEtr(etrTobeCreate);

			log.info("All Fr ticket added ...... " + prospectNoTicketIdMap.size());

		} catch (Exception e) {

			log.error("error while adding fr ticket :: " + e.getMessage());
			e.printStackTrace();

		} finally {
			
			log.info("in finally block after etr calculation ");
			// hibernateTemplate.flush();
			// hibernateTemplate.getSessionFactory().getCurrentSession().getTransaction().commit();
		}

		return prospectNoTicketIdMap;
	
	}

	@Override
	public Map<String, Long> addNewTicketInBulk(
			List<MQWorkorderVO> mqWorkorderVOList,
			Map<String, CustomerVO> mqIdCustomerMap)
	{

		Map<String, Long> prospectNoTicketIdMap = new ConcurrentHashMap<String, Long>();
		try {
			Long id = 0l;

			int totalTicketSize = mqWorkorderVOList.size();
		
			MQWorkorderVO mqWorkorderVO = null;
			CustomerVO customerVo = null;
			Ticket ticket = null;
			
			for (Iterator<MQWorkorderVO> iterator = mqWorkorderVOList
					.iterator(); iterator.hasNext();)
			{
				customerVo = null;
				mqWorkorderVO = null;
				
				mqWorkorderVO = (MQWorkorderVO) iterator.next();
				
				try {

					if (mqIdCustomerMap.containsKey(mqWorkorderVO.getMqId()))
					{
						customerVo = mqIdCustomerMap
								.get(mqWorkorderVO.getMqId());
						
//						log.info("Got Customer for prospect " + mqWorkorderVO);
						
						log.info("Got Customer for prospect " + mqWorkorderVO.getProspectNumber());
						
						TicketVo ticketVo = xformToTicket(mqWorkorderVO, customerVo);
						
						log.info("Converted mqData to Ticket vo for prospect No " + mqWorkorderVO.getProspectNumber());
						
						try
						{
							ticket = new Ticket();
							ticket.setStatus(TicketStatus.OPEN);
							ticket.setSource(mqWorkorderVO.getTicketSource());
							
							if (ticketVo.getStatus() != null)
								ticket.setStatus(Integer.valueOf(ticketVo.getStatus()));
							ticket.setCustomer(getHibernateTemplate()
									.load(Customer.class, ticketVo.getCustomerId()));

							if (ticketVo.getTicketSubCategory() != null)
								ticket.setTicketSubCategory(ticketVo
										.getTicketSubCategory());

							ticket.setTicketCategory(TicketCategory.NEW_INSTALLATION);
							if (ticketVo.getTicketCategory() != null)
								ticket.setTicketCategory(ticketVo.getTicketCategory());

							ticket.setTicketCategoryId(GenericUtil
									.getCategoryIdByString(ticket.getTicketCategory()));

							ticket.setTicketCreationDate(new Date());
							ticket.setTicketCreationMode(ticketVo.getSource());
							ticket.setSource(ticketVo.getSource());

							ticket.setAddedOn(new Date());
							ticket.setModifiedOn(new Date());
							// ticket.setStatus(TicketStatus.OPEN);

							ticket.setAddedBy(ticketVo.getUpdatedBy());
							ticket.setModifiedBy(ticketVo.getUpdatedBy());
							// ticket.setStatus(TicketStatus.OPEN);

							ticket.setProspectNo(ticketVo.getProspectNo());
							ticket.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());
						
							if (ticketVo != null && ticketVo.getAreaId() > 0)
							{
								ticket.setArea(hibernateTemplate
										.load(Area.class, ticketVo.getAreaId()));
								
								log.debug("Got area for prospect " + mqWorkorderVO.getProspectNumber());
								
							} else
								
							{
								ticket.setArea(null);
								ticket.setSource("OUTSIDE FWMP");
							}
						
							if (ticketVo !=null && ticketVo.getAssignedBy() > 0)
							{	
								ticket.setUserByAssignedBy(hibernateTemplate
										.load(User.class, ticketVo
												.getAssignedBy()));
							
							}
						
							if (ticketVo.getCommitedEtr() != null)
							{
								ticket.setCommitedEtr(GenericUtil
										.convertStringToDateFormat(ticketVo
												.getCommitedEtr()));
								ticket.setCurrentEtr(GenericUtil
										.convertStringToDateFormat(ticketVo
												.getCommitedEtr()));
							}
						
							if (ticketVo.getAssignTo() > 0)
							{
								User user = getHibernateTemplate()
										.load(User.class, ticketVo.getAssignTo());
								if (user != null)
								{
									ticket.setUserByAssignedTo(user);
									ticket.setUserByCurrentAssignedTo(user);

								}
							}

							ticket.setPreferedDate(ticketVo.getPreferedDate());
							
							TicketPriority p = new TicketPriority();
							p.setId(StrictMicroSecondTimeBasedGuid.newGuid());
							p.setModifiedOn(new Date());
							p.setValue(0);
							p.setUpdatedCount(0);
							p.setModifiedBy(ticket.getAddedBy());
							p.setEscalatedValue(0);
							p.setSliderEscalatedValue(0);

							ticket.setTicketPriority(p);
						
							ticketVo = xformToVo(ticket);
							ticket.setProspectNo(ticketVo.getProspectNo());
							
							id = (Long) hibernateTemplate.save(ticket);

							ticket.setId(id);
						
							prospectNoTicketIdMap
									.put(ticket.getProspectNo(), ticket.getId());

						} catch (Exception e)
						{
							log.error("Error While Adding new ticket :"
									+ e.getMessage());
							e.printStackTrace();
							continue;
						}
					}
				} catch (Exception e) 
				{
					log.error("Error while adding Ticket for workorder number " + mqWorkorderVO.getWorkOrderNumber() + " "
							+ e.getMessage());
					e.printStackTrace();
					continue;
				}

			}
//			hibernateTemplate.flush();
			log.info("All ticket added ...... " + totalTicketSize);
		} catch (Exception e) 
		{
			log.error("Error while adding Tickets "
					+ e.getMessage());
			e.printStackTrace();
			
		} finally
		{
				hibernateTemplate.flush();
				//hibernateTemplate.getSessionFactory().getCurrentSession().getTransaction().commit();
				
		}
		return prospectNoTicketIdMap;
	
	}

	private TicketVo xformToTicket(MQWorkorderVO mqWorkorderVO,
			CustomerVO customerVo)
	{
		TicketVo ticketVo = new TicketVo();
		log.debug("Converting data mq object " + mqWorkorderVO + " \n customerVo " + customerVo);
		try {
			
		ticketVo.setCustomerId(customerVo.getId());
			log.debug("Area Dao Object : " +areaDao +"Custoemr id while converting " + customerVo.getId());
		try
		{
			if (mqWorkorderVO.getAreaId() != null)
				ticketVo.setAreaId(Long.valueOf(mqWorkorderVO.getAreaId()));
			else
				log.debug("customerVo.getCityId()******"+customerVo.getCityId());
				ticketVo.setAreaId(areaDao
						.getDefaultAreaByCity(customerVo.getCityId()));
				log.debug("Area " + ticketVo.getAreaId());
		} catch (Exception e)
		{
				log.error("Error occured while getting Area by Area id or default area by city id :"+e.getMessage());
				e.printStackTrace();
		}
		
		//added by manjuprasad for ticket description voc issue
		ticketVo.setTicketDescriptionVoc(mqWorkorderVO.getComment());
		

		if (mqWorkorderVO.getAssignedTo() != null)
			ticketVo.setAssignTo(mqWorkorderVO.getAssignedTo());

			log.debug("ticketVo.getAssignTo " + ticketVo.getAssignTo());
			
		if (mqWorkorderVO.getFxName() != null)
			ticketVo.setFxName(mqWorkorderVO.getFxName());

			log.debug("ticketVo.getFxNAme " + ticketVo.getFxName());
			
		ticketVo.setSymptomCode(mqWorkorderVO.getSymptom());
			
			log.debug("Sysmtom code " + ticketVo.getSymptomCode());
			
		ticketVo.setTicketCategory(mqWorkorderVO.getTicketCategory());
			log.debug("setTicketCategory code " + ticketVo.getTicketCategory());
			
			
		if (definitionCoreService
				.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_CATEG)
				.contains(mqWorkorderVO.getTicketCategory())
				|| definitionCoreService
						.getClassificationCategory(DefinitionCoreService.NEW_INSTALLATION_SYMPTOM)
						.contains(mqWorkorderVO.getSymptom()))
		{
				log.debug("Inside if ");
			ticketVo.setTicketCategory(TicketCategory.NEW_INSTALLATION);
		}
		ticketVo.setProspectNo(mqWorkorderVO.getProspectNumber());
			log.debug("Converted data " + mqWorkorderVO);
			

			String currentCRM = dbUtil.getCurrentCrmName(null, null);

			if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_SIEBEL)) {
				ticketVo.setCrmModifiedOn(new Date());
				ticketVo.setCrmCreatedOn(GenericUtil.convertToFWMPDateFormatFromSiebel(mqWorkorderVO.getReqDate()));
			}
			else if (currentCRM.equalsIgnoreCase(CurrentCrmNames.CRM_MQ)) {
				ticketVo.setCrmModifiedOn(GenericUtil.converDateFromMQDateString(mqWorkorderVO.getModifiedDate()));
				ticketVo.setCrmCreatedOn(GenericUtil.converDateFromMQDateString(mqWorkorderVO.getReqDate()));
			}
			log.debug("CRM Date " + ticketVo.getCrmCreatedOn());
			log.debug("Converted data " + mqWorkorderVO);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in Converting data " + mqWorkorderVO + e.getMessage());
		}
		
		log.debug("Final return value "  +ticketVo);
		return ticketVo;
	}

	@Override
	public Ticket getTicketById(Long id)
	{
		try {

			return getHibernateTemplate().get(Ticket.class, id);

		} catch (Exception e) {
			log.error("Error occured while loading Ticket for id : " + id);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public FrTicket getFrTicketById(Long id)
	{
		if (id == null || id.longValue() <= 0)
			return null;

		try {
			return getHibernateTemplate().load(FrTicket.class, id);
		} catch (DataAccessException e) {
			log.error("Error occured while loading FrTicket for id : "+id);
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	public Integer updateFrTicket(FrTicket ticket)
	{
		try 
		{
			if (ticket != null && ticket.getId() >  0)
			{
				getHibernateTemplate().update(ticket);
				return 1;
			}
			
		} catch (DataAccessException e) {
			log.error("Error occured while updating FrTicket for id : ");
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public Gis getGisInfoTicket(Long ticketId)
	{
		// TODO Auto-generated method stub

		List<Gis> gis = (List<Gis>) hibernateTemplate
				.find("from Gis s where s.ticketNo=" + ticketId
						+ " and feasibilityType !=1 and connectionType is not null ORDER BY s.addedOn DESC");
		if (gis != null && gis.size() > 0)
			return gis.get(0);
		return null;
	}

	@Override
	public void updateCurrentAssignedTo(long ticketId, Long userIdAssignedTo,
			Long userIdAssignedBy, int status)
	{
		try {
			log.info("Updating updateCurrentAssignedTo for icketId " + ticketId
					+ " userId " + userIdAssignedTo + " userIdAssignedBy "
					+ userIdAssignedBy + " status " + status);

			String userIdAssignedToString = "";

			if (userIdAssignedTo == null || userIdAssignedTo <= 0)
			{
				userIdAssignedToString = "null";
			} else
			{
				userIdAssignedToString = userIdAssignedTo + "";
			}

			String userIdAssignedByString = "";
			if (userIdAssignedBy == null || userIdAssignedBy <= 0)
			{
				userIdAssignedByString = customerCoreService
				.getTicketIdAndSaelsExecutiveMap(Arrays.asList(ticketId)).get(ticketId)+ "";
			} else
			{
				userIdAssignedByString = userIdAssignedBy + "";
			}

			String query = "update Ticket t, WorkOrder wo set t.currentAssignedTo="
					+ userIdAssignedToString + ",wo.assignedTo="
					+ userIdAssignedToString + ", t.assignedBy = "
					+ userIdAssignedByString + ", t.status = " + status
					+ " where t.id=" + ticketId + " and t.id=wo.ticketId";

			int result = jdbcTemplate.update(query);

			log.info("Total updated row are " + result + " query " + query);
		} catch (Exception e) {
			log.error("Error While updating updateCurrentAssignedTo after creating workorder :: "+e.getMessage());
		}
	}

	@Override
	public void updateFrCurrentAssignedToViaCrm(long ticketId,
			Long userIdAssignedTo, Long userIdAssignedBy, int status)
	{
		log.info("Updating current assigned to for fr ticket : " + ticketId);

		if (ticketId <= 0)
			return;
		try
		{

			String userIdString = "";
			if (userIdAssignedTo == null || userIdAssignedTo <= 0)
				userIdString = "null";
			else
				userIdString = userIdAssignedTo + "";

			String userIdAssignedByString = "";
			if (userIdAssignedBy == null || userIdAssignedBy <= 0)
				userIdAssignedByString = "null";
			else
				userIdAssignedByString = userIdAssignedBy + "";

			String UPDATE_FR_CURRENT_ASSIGNEDTO = "UPDATE FrTicket tick INNER JOIN FrDetail fr "
					+ "ON tick.id = fr.ticketId "
					+ "SET tick.currentAssignedTo = " + userIdString
					+ ", tick.assignedTo = " + userIdString
					+ ", tick.assignedBy = " + userIdAssignedByString
					+ ", tick.modifiedBy = " + userIdAssignedByString
					+ ", tick.status = " + status + " WHERE tick.id = "
					+ ticketId;
			
			if( status != 0 && FRStatus.FR_PUSH_BACK_TICKET.equals(status))
				UPDATE_FR_CURRENT_ASSIGNEDTO = "UPDATE FrTicket tick INNER JOIN FrDetail fr "
						+ "ON tick.id = fr.ticketId "
						+ "SET tick.currentAssignedTo = " + userIdString
						+ ", tick.assignedBy = " + userIdAssignedByString
						+ ", tick.status = " + status + " WHERE tick.id = "
						+ ticketId;

			int result = jdbcTemplate.update(UPDATE_FR_CURRENT_ASSIGNEDTO);

			log.info("Total updated row are " + result + " query "
					+ UPDATE_FR_CURRENT_ASSIGNEDTO);

		} catch (Exception exception)
		{
			log.error("Error occure while updating current assigned to for fr ticket : "
					+ ticketId + " :: " + exception.getMessage());
		}

	}

	@Override
	public void updateFrCurrentAssignedTo(long ticketId, Long userId,
			Long assignedBy)
	{

		if (ticketId <= 0)
			return;

		log.info("Updating current assignedTo for Ticket id :" + ticketId
				+ "to  user id : " + userId);
		try
		{
			String userIdString = "";
			if (userId == null || userId <= 0)
				userIdString = "null";
			else
				userIdString = userId + "";

			String assignedByString = "";
			if (assignedBy == null || assignedBy <= 0)
				assignedByString = "null";
			else
				assignedByString = assignedBy + "";

			final String UPDATE_FR_CURRENT_ASSIGNEDTO = "UPDATE FrTicket tick INNER JOIN FrDetail fr "
					+ " ON tick.id = fr.ticketId "
					+ " SET tick.currentAssignedTo= " + userIdString
					+ ", tick.assignedBy = " + assignedByString
					+ " WHERE tick.id= " + ticketId;

			int result = jdbcTemplate.update(UPDATE_FR_CURRENT_ASSIGNEDTO);

			log.info("Total updated row are " + result + " query "
					+ UPDATE_FR_CURRENT_ASSIGNEDTO);

		} catch (Exception exception)
		{
			log.error("Error occure while updating current assigned to for fr ticket : "
					+ ticketId + " :: " + exception.getMessage());
		}

	}

	@Transactional
	@Override
	public List<TicketVo> getTickets(TicketFilter filter)
	{

		Criteria criteria = getHibernateTemplate().getSessionFactory()
				.getCurrentSession().createCriteria(Ticket.class, "ticket");

		if (filter != null)
		{

			if (filter.getTicketId() != null && filter.getTicketId() > 0)
			{

				criteria.add(Restrictions.eq("id", filter.getTicketId()));

			}

			if (filter.getAssignedUserId() != null
					&& filter.getAssignedUserId() > 0)
			{

				criteria.add(Restrictions
						.eq("ticket.user.id", filter.getAssignedUserId()));

			}

			if (filter.getPageSize() > 0 || filter.getStartOffset() > 0)
			{
				int pageIndex = filter.getStartOffset();
				int pageSize = filter.getPageSize();

				if (pageIndex == 0 || pageSize == 0)
				{
					pageIndex = 1;
					pageSize = 5;
				}
				criteria.setFirstResult(pageIndex);
				criteria.setMaxResults(pageSize);
			}

			if (filter.getAreaId() != null && filter.getAreaId() > 0)
			{
				log.debug(filter.getAreaId());

				Area area = new Area();
				area.setId(filter.getAreaId());
				criteria.add(Restrictions.eq("area", area));

			}

			if (filter.getToDate() != null || filter.getFromDate() != null)
			{
				criteria.add(Restrictions.between("fromDate", filter
						.getFromDate(), filter.getToDate()));
			}
		}

		return xformToVoList((List<Ticket>) criteria.list());
	}

	@Override
	public List<Ticket> getAllTicket()
	{
		@SuppressWarnings("unchecked")
		// List<Ticket> ticket = (List<Ticket>) hibernateTemplate
		// .find("from Ticket");

		Query query = hibernateTemplate.getSessionFactory().openSession()
				.createQuery("from Ticket t where t.source IN (:sources)");
		query.setParameterList("sources", Arrays
				.asList(new String[] { "Call Centre", "Door Knock", "Tool" }));

		List<Ticket> ticket = query.list();
		return ticket;
	}

	@Override
	public List<FrTicket> getAllOpenFrTicket()
	{
		List<FrTicket> ticket = null;

		try
		{
			Query query = hibernateTemplate.getSessionFactory().openSession()
					.createSQLQuery("select * from FrTicket t where t.status not in (:statusList)")
					.addEntity(FrTicket.class);

			query.setParameterList("statusList", TicketStatus.COMPLETED_DEF);

			ticket = query.list();
		} catch (Exception ex)
		{

		}

		return ticket;
	}

	@Override
	public Ticket deleteTicket(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public void reportTo(Long id)
	{

	}

	@Override
	public Integer updateTicketEtr(TicketEtrVo etr)

	{

		log.info("update TicketEtr ::" + etr);
		int rows = 0;

		try
		{
			Object[] params = { etr.getNewETR(), etr.getCount(),
					etr.getTicketId() };

			int[] types = { Types.TIMESTAMP, Types.INTEGER, Types.BIGINT };

			rows = jdbcTemplate.update(UPDATE_TICKET_ETR_COUNT, params, types);
		} catch (Exception e)
		{
			log.error("Exception while updating TicketEtr:: "
					+ etr.getTicketId() + " Message " + e);
		}

		return rows;

	}

	@Override
	public Integer updateEtr(TicketEtrVo etr)
	{
		log.info("updaing Ticket Etr as ticketId as:" + etr.getTicketId());

		int rows = 0;
		try
		{

			Object[] params = { etr.getNewETR(), etr.getEtrMode(),
					etr.getUserId(), new Date(), etr.getTicketId() };

			int[] types = { Types.TIMESTAMP, Types.INTEGER, Types.BIGINT,
					Types.TIMESTAMP, Types.BIGINT };

			rows = jdbcTemplate.update(UPDATE_ETR, params, types);
			if (rows <= 0)
			{
				log.info("Ticket Etr is not updated" + etr.getTicketId());
				// start
				// send etr update customer notification code
				List<BigInteger> ticketIdsInBigInteger = new ArrayList<BigInteger>();
				ticketIdsInBigInteger
						.add(BigInteger.valueOf(etr.getTicketId()));
				List<BigInteger> tickets = ticketIdsInBigInteger;
				// etrUpdateCustomerNotificationJob.sendCustomerNotification(tickets);
				// end
				return rows;
			}

			etr.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			addTicketEtr(etr);
			log.info("Ticket Etr is  updated as ticketId:" + etr.getTicketId());

		} catch (Exception e)
		{
			log.error("exception while UpdateEtr::" + etr.getTicketId()
					+ "Message " + e);

		}

		return rows;
	}

	@Override
	public WorkOrderVo getWorkOrderUserId(Long ticketId)
	{
		WorkOrderVo WorkOrderVo = null; 

		try {
			WorkOrderVo = (WorkOrderVo) jdbcTemplate
					.queryForObject(GET_USER_FROM_WO, new Object[] {
							ticketId }, new WorkOrderVoMapper());
		} catch (Exception e) {
			log.error("Error while getting WO for ticketNo " + ticketId);
			e.printStackTrace();
			return WorkOrderVo;
		}

		return WorkOrderVo;
	}

	@SuppressWarnings("unused")
	@Override
	public Integer addTicketEtr(TicketEtrVo etr)
	{
		log.info("add ticketEtr::" + etr);
		Integer countEtr = 0;
		Integer row = 0;

		int role = AuthUtils.getCurrentUserRole();

		try
		{

			row = jdbcTemplate.update(INSERT_IN_TICKTETR, new Object[] {
					etr.getId(), etr.getTicketId(), etr.getNewETR(),
					etr.getApprovalStatus(), etr.getReasonCode(),
					etr.getRemarks(), etr.getUserId(), new Date(),
					etr.getUserId(), new Date(), etr.getCount(),
					etr.getStatus() });

			if (row <= 0)
			{
				log.info("Insertion failed in TicketEtr as ticketId::"
						+ etr.getTicketId());
				return row;
			}
			log.info("added sucessfully in TicketEtr as ticketId"
					+ etr.getId());
		} catch (Exception e)
		{
			log.error("Error while adding TicketEtr for ticketId"
					+ etr.getTicketId() + " " + e.getMessage());
		}

		return row;

	}

	@Override
	public Map<Long, String> getAllETRPEndingEntries()
	{
		Map<Long, String> etrUpdateCache = new HashMap<Long, String>();
		try
		{
			if (hibernateTemplate == null)
				return null;
			List<TicketEtr> pendingEtrs = (List<TicketEtr>) hibernateTemplate
					.find("from TicketEtr t where t.approvalStatus=0");

			if (pendingEtrs != null)
			{
				for (Iterator iterator = pendingEtrs.iterator(); iterator
						.hasNext();)
				{

					TicketEtr ticketEtr = (TicketEtr) iterator.next();
					etrUpdateCache.put(ticketEtr.getTicketId(), GenericUtil
							.convertToDateFormat(ticketEtr.getApprovalValue()));

				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error While getting all ETRPEndingEntries :"
					+ e.getMessage());
		}

		return etrUpdateCache;

	}

	@Override
	@Transactional
	public Integer getCountEtr(Long ticketId)
	{
		log.debug("get etr count for ticket as id:: " + ticketId);
		Integer count = 0;

		try
		{
			Session session = hibernateTemplate.getSessionFactory()
					.getCurrentSession();

			Query query = session.createSQLQuery(GET_TICKET_ETR);

			query.setLong("ticketId", ticketId);
			count = (Integer) query.uniqueResult();
			if (count == null)
				count = 0;
			log.info("count ticket etr updated by NE:" + count);
		} catch (Exception e)
		{
			log.error("exception to countEtr :: " + e);
		}

		return count;

	}

	@Override
	@Transactional
	public TicketEtrVo getEtrVo(Long ticketId)
	{
		log.debug("get etr count for ticket as id:: " + ticketId);

		try
		{

			List<TicketEtr> etr = (List<TicketEtr>) hibernateTemplate
					.find("from TicketEtr  t where t.ticketId=? order by t.addedOn Desc ", ticketId);

			if (etr != null && !etr.isEmpty())
			{
				TicketEtrVo etrVo = new TicketEtrVo();
				BeanUtils.copyProperties(etr.get(0), etrVo);
				return etrVo;
			}

		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("exception to countEtr :: " + e);
		}

		return null;

	}

	private List<TicketVo> xformToVoList(List<Ticket> tickets)
	{
		if (tickets == null)
			return null;

		List<TicketVo> ticketVos = new ArrayList<TicketVo>();

		TicketVo vo = null;
		for (Ticket ticket : tickets)
		{
			vo = new TicketVo();
			BeanUtils.copyProperties(ticket, vo);
			if (ticket.getArea() != null)
			{
				vo.setAreaCode(ticket.getArea().getAreaName());
				vo.setAreaId(ticket.getArea().getId());
			}
			if (ticket.getSubArea() != null)
			{
				vo.setAreaId(ticket.getSubArea().getId());

			}

			ticketVos.add(vo);
		}
		return ticketVos;

	}

	private TicketVo xformToVo(FrTicket ticket)
	{

		TicketVo vo = new TicketVo();
		BeanUtils.copyProperties(ticket, vo);

		return vo;
	}

	private TicketVo xformToVo(Ticket ticket)
	{

		TicketVo vo = new TicketVo();
		BeanUtils.copyProperties(ticket, vo);

		return vo;
	}

	@Override
	public Long createNewInstallationProspect(ProspectCoreVO prospect)
	{
		return addTicket(prospect.getTicket(), prospect);
	}

	@Override
	public void assignTickets(AssignVO assignVO, Long areaId)
	{
		if (assignVO != null)
		{

			try
			 {
			List<Long> tickets = assignVO.getTicketId();

				if (tickets != null)
				{

					log.debug("ticket ids===============" + tickets.size());
					for (Long id : tickets)
					{

						Ticket ticket = getHibernateTemplate()
							.get(Ticket.class, id);

						if (ticket.getStatus() != null
							&& TicketStatus.SALES_UNASSIGNED_TICKET == ticket
									.getStatus().intValue())
						{
							if (AuthUtils.isNIUser())
								ticket.setStatus(TicketStatus.WO_GENERATED);
							else
								ticket.setStatus(TicketStatus.OPEN);
						}

						else if (ticket.getStatus() != null
							&& TicketStatus.WO_UNASSIGNED_TICKET == ticket
									.getStatus().intValue())
						{
							ticket.setStatus(TicketStatus.WO_GENERATED);
						}
						// ticket.setUserByAssignedBy(hibernateTemplate
						// .load(User.class, assignVO.getAssignedById()));
						ticket.setModifiedBy(assignVO.getAssignedById());
					
						User userByAssignedTo = hibernateTemplate
							.load(User.class, assignVO.getAssignedToId());
				
						if(userByAssignedTo != null)
							ticket.setUserByAssignedTo(userByAssignedTo);
					
					User userCurrentAssignedTo = hibernateTemplate
							.load(User.class, assignVO.getAssignedToId());
					
					if(userCurrentAssignedTo != null)
						ticket.setUserByCurrentAssignedTo(userCurrentAssignedTo);

					log.debug("assignVO.getAssignedToId() "
							+ assignVO.getAssignedToId());

					/*UserVo user = userDao
							.getUserById(assignVO.getAssignedToId());

					log.debug("assignVO.getAssignedToId() "
							+ assignVO.getAssignedToId() + " user " + user);
*/
					/*Customer c = ticket.getCustomer();

					if (user.getBranch() != null
							&& user.getBranch().getId() != null
							&& areaId != null)
					{
						c.setBranch(getHibernateTemplate()
								.load(Branch.class, user.getBranch().getId()));
					}

					if (user.getArea() != null && !user.getArea().isEmpty())
					{
						Long aId = user.getArea().iterator().next().getId();
						log.debug("Area id for User " + user.getLoginId()
								+ user.getFirstName() + " is " + aId);
						Area area = getHibernateTemplate()
								.load(Area.class, aId);
						c.setArea(area);
						ticket.setArea(area);

					} else
					{
						if (user.getBranch() != null
								&& user.getBranch().getId() != null)
						{
							Map<Long, String> areaBranch = LocationCache
									.getAreasByBranchId(user.getBranch().getId()
											+ "");

							log.debug("areaBranch " + areaBranch);

							Long arId = areaBranch.entrySet().iterator().next()
									.getKey();
							log.debug("areaBranch " + areaBranch + " arId "
									+ arId);
							Area area = getHibernateTemplate()
									.load(Area.class, arId);
							c.setArea(area);
							ticket.setArea(area);
						}

					}
					if (user.getBranch() != null
							&& user.getBranch().getId() != null)
					{
						c.setBranch(getHibernateTemplate()
								.load(Branch.class, user.getBranch().getId()));
					}

					if (areaId != null && areaId > 0)
					{
						Area area = getHibernateTemplate()
								.load(Area.class, areaId);
						ticket.setArea(area);
					}
					getHibernateTemplate().update(c);*/
					getHibernateTemplate().update(ticket);
					log.debug("Ticket with id " + id + " is assigned to "
							+ assignVO.getAssignedToId());

				}
			}
			} catch (Exception e) {
				
				log.error("Error while reassigning ticket for ticket id " + assignVO + " " + e.getMessage() );
				e.printStackTrace();
			}
		}

	}

	@Override
	public Integer updateTicketCaf(CafVo cafVo, Long userId)
	{

		int rows = 0;

		log.info("update Ticket CafNumber as ticketId:" + cafVo.getTicketId());
		try
		{
			Object[] params = { cafVo.getCafNumber(), userId, new Date(),
					cafVo.getTicketId() };

			int[] types = { Types.VARCHAR, Types.BIGINT, Types.TIMESTAMP,
					Types.BIGINT };

			rows = jdbcTemplate.update(UPDATE_TICKET_CAF, params, types);
		} catch (Exception e)
		{
			log.error("Query:" + UPDATE_TICKET_CAF + e);
		}

		return rows;

	}

	@Override
	public int updateCustomerMq(Long ticketId, String mqId)
	{
		int result = 0;
		try
		{
			Ticket ticket = getHibernateTemplate().load(Ticket.class, ticketId);
			Customer customer = ticket.getCustomer();
			customer.setMqId(mqId);
			getHibernateTemplate().update(ticket);

		} catch (Exception e)
		{
			log.error("Erroe While updating customer MQ :" + e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public Ticket getTicketByProspectNumber(String prospectNo)
	{

		List<Ticket> ticket = (List<Ticket>) hibernateTemplate
				.find("from Ticket s where s.prospectNo=?",new Object[]{ prospectNo});
		
		log.info("prospect value in Ticket result is ::"+ticket);
		
		if (ticket != null && ticket.size() > 0)
			return ticket.get(0);
		return null;
	}

	@Override
	public Long getTicketIdByProspectNumber(String prospectNo)
	{

		try
		{
			List<Ticket> ticket = (List<Ticket>) hibernateTemplate
					.find("from Ticket s where s.prospectNo='" + prospectNo + "'");
			
			
			
			if (ticket != null && ticket.size() > 0)
			{
				log.info("getTicketIdByProspectNumber for prospect " +prospectNo + " are " + ticket.size());
				return ticket.get(0).getId();
			}
				
			
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error while getting ticket details for prospect " + prospectNo);
		}
		return null;
	}
	
	public String assignTickets(AssignVO assignVO, long tid)
	{
		if (assignVO != null)
		{

			List<Long> tickets = assignVO.getTicketId();

			if (tickets != null)
			{

				log.debug("ticket ids===============" + tickets.size());
				for (Long id : tickets)
				{
					User usr1 = (User) getHibernateTemplate()
							.getSessionFactory().getCurrentSession()
							.load(User.class, assignVO.getAssignedToId());

					User usr2 = (User) getHibernateTemplate()
							.getSessionFactory().getCurrentSession()
							.load(User.class, assignVO.getAssignedById());

					Ticket ticket = (Ticket) hibernateTemplate
							.getSessionFactory().getCurrentSession()
							.load(Ticket.class, id);

					if(usr1 != null)
					{
						ticket.setUserByCurrentAssignedTo(usr1);
						ticket.setUserByAssignedTo(usr1);
					}
					// ticket.setUserByAssignedBy(usr2);
					ticket.setModifiedBy(usr2.getId());
					getHibernateTemplate().update(ticket);

					log.info("Ticket with id " + id + " is assigned to "
							+ assignVO.getAssignedToId());

				}
			}
		}
		return null;

	}

	@Override
	public int updateRemarks(RemarksVO remark)
	{

		Ticket ticket = getHibernateTemplate()
				.get(Ticket.class, remark.getTicketId());
		if(remark.getRemarkType() == RemarkType.POA)
			ticket.setPoaRemarks(GenericUtil.formatRemarks(ticket.getRemarks(), remark
					.getRemark(), remark.getUserName()));
		else if(remark.getRemarkType() == RemarkType.POI)
				ticket.setPoiRemarks(GenericUtil.formatRemarks(ticket.getRemarks(), remark
						.getRemark(), remark.getUserName()));
		else
		ticket.setRemarks(GenericUtil.formatRemarks(ticket.getRemarks(), remark
				.getRemark(), remark.getUserName()));
		getHibernateTemplate().save(ticket);
		return 0;

	}

	@Override
	public void updateProspectNumberInTicket(Long ticketId, String prospectNo)
	{
		try
		{
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);
			if (ticket != null)
			{
				ticket.setOldProspectNo(ticket.getProspectNo());
				ticket.setProspectNo(prospectNo);
				ticket.setMqStatus(MQStatus.SUCCESS);
				ticket.setModifiedOn(new Date());
				hibernateTemplate.update(ticket);
				if (ticket.getOldProspectNo() != null)
				{
					documentationService.renameProspectFileAndDirectory(ticket
							.getOldProspectNo(), prospectNo);
					documentDao.renameCustomeDocuments(ticketId, ticket
							.getOldProspectNo(), prospectNo);
				}
			}
		} catch (DataAccessException e)
		{
			log.error("Erroe While updating ProspectNumberInTicket :"
					+ e.getMessage());
			e.printStackTrace();
		}

	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}

	@Override
	public int reassignTicket(ReassignTicketVo reassignTicketVo, Long areaId)
	{

		Session session = null;
		try
		{
			session = getHibernateTemplate().getSessionFactory().openSession();
			if (areaId != null)
			{
				Query updateReassignQuery = session
						.createSQLQuery(REASSIGN_TICKET_EXECUTIVE);

				updateReassignQuery.setParameter("currentAssignedTo", Long
						.valueOf(reassignTicketVo.getRecipientUserId()));

				updateReassignQuery.setParameter("assignedBy", AuthUtils
						.getCurrentUserId());

				updateReassignQuery
						.setParameter("reason", reassignTicketVo.getReason());

				updateReassignQuery.setParameter("areaId", areaId);

				updateReassignQuery.setParameterList("id", reassignTicketVo
						.getTicketIds());

				return updateReassignQuery.executeUpdate();

			} else
			{
				Query updateReassignQuery = getHibernateTemplate()
						.getSessionFactory().getCurrentSession()
						.createSQLQuery(REASSIGN_TICKET);

				updateReassignQuery.setParameter("currentAssignedTo", Long
						.valueOf(reassignTicketVo.getRecipientUserId()));

				updateReassignQuery.setParameter("assignedBy", AuthUtils
						.getCurrentUserId());

				updateReassignQuery
						.setParameter("reason", reassignTicketVo.getReason());

				updateReassignQuery.setParameterList("id", reassignTicketVo
						.getTicketIds());

				return updateReassignQuery.executeUpdate();
			}
		} catch (NumberFormatException e)
		{
			log.error("Erroe While formating number :" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e)
		{
			log.error("Erroe While executing query :" + e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		return 0;
	}

	@Override
	public int updateGxCXPermission(GxCxPermission gxCxPermission)
	{
		return jdbcTemplate
				.update(UPDATE_GX_CX_USING_TICKETID_FROM_TICKET, new Object[] {
						gxCxPermission.getGxPermission(),
						gxCxPermission.getCxPermission(),
						gxCxPermission.getTicketId() });

	}

	@Override
	public int updateDocpath(Long ticketId, String docPath, long activityId, String uinType ,String uin)
	{
		if (ticketId == null || ticketId.longValue() <= 0)
			return 0;
		try{
			Ticket ticket = getHibernateTemplate().get(Ticket.class, ticketId);
			if (ticket != null) 
			{
				ticket.setDocumentPath(docPath);

				if (Activity.POA_DOCUMENT_UPDATE == activityId) 
				{
					ticket.setStatus(TicketStatus.POA_DOCUMENTS_UPLOADED);
					ticket.setPoaStatus(TicketStatus.POA_DOCUMENTS_UPLOADED);

				} else if (Activity.POI_DOCUMENT_UPDATE == activityId) 
				{
					ticket.setStatus(TicketStatus.POI_DOCUMENTS_UPLOADED);
					ticket.setPoiStatus(TicketStatus.POI_DOCUMENTS_UPLOADED);

				}
				
				if(uinType != null)
					ticket.setUinType(uinType);
				
				if(uin != null)
					ticket.setUin(uin);

				getHibernateTemplate().update(ticket);
				return 1;
			}
		} catch (Exception e) {
			log.error("Error occured while updating Doc path for Ticket id : " + ticketId);
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public Long ticketClosureStatusUpdate(TicketClosureDataVo closureDataVo)
	{
		Long result = 0l;
		try
		{
			Ticket ticket = getHibernateTemplate()
					.load(Ticket.class, closureDataVo.getTicketId());

			// String userName = AuthUtils.getCurrentUserLoginId();

			ticket.setRemarks(GenericUtil.formatRemarks(ticket
					.getRemarks(), closureDataVo.getRemarks(), ""));

			ticket.setStatus(TicketStatus.ACTIVATION_COMPLETED_VERIFIED);
			getHibernateTemplate().update(ticket);
			result = 1l;

			return result;

		} catch (Exception e)
		{
			log.error("Error while closing ticket "
					+ closureDataVo.getTicketId() + " " + e.getMessage());
			e.printStackTrace();
			return result;
		}

	}

	@Override
	public List getCurrentAssignedForTickets(List<Long> ticketIds)
	{
		List currentAssignToLsit = null;
		try
		{
			currentAssignToLsit = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("Select currentAssignedTo from Ticket where id in (:ids)")
					.setParameterList("ids", ticketIds).list();
		} catch (Exception e)
		{
			log.error("currentAssignedForTickets" + e);
			e.printStackTrace();
		}

		return currentAssignToLsit;
	}
	
	@Override
	public List<TicketReassignmentLogVo> getCurrentAssignedForTicketsWithIds(List<Long> ticketIds)
	{
		List<Object[]> currentAssignToLsit = null;
		List<TicketReassignmentLogVo> result = new ArrayList<>();
		try
		{
			currentAssignToLsit = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("Select id,currentAssignedTo from Ticket where id in (:ids)")
					.setParameterList("ids", ticketIds).list();
			
			TicketReassignmentLogVo ticketReassignmentLogVo = null;
			for (Object[] obj : currentAssignToLsit)
			{
				ticketReassignmentLogVo = new TicketReassignmentLogVo();
				ticketReassignmentLogVo.setTicketId(Long.valueOf(obj[0].toString()));
				if(obj[1] != null)
					ticketReassignmentLogVo.setAssignedFrom(Long.valueOf(obj[1].toString()));
				result.add(ticketReassignmentLogVo);
			}
			
		} catch (Exception e)
		{
			log.error("getCurrentAssignedForTicketsWithIds " + e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public Map<String, Long> getTicketNosFromProspects(
			List<String> prospectList, String location)
	{
		Map<String, Long> propspectTicketMap = new ConcurrentHashMap<String, Long>();
		if (prospectList.isEmpty())
			return propspectTicketMap;

		try
		{
			log.info("Prospect List size " + prospectList.size() + " for location " + location);
			
			List<Object[]> branches = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT t.id , t.prospectNo FROM Ticket t where t.prospectNo in (:prospectList)")
					.setParameterList("prospectList", prospectList).list();
			
			if (branches != null)
			{
				log.info("Available Prospect List size " + branches.size() + " for location " + location);
				
				for (Object[] obj : branches)
				{
					propspectTicketMap
							.put(((String) obj[1]), ((BigInteger) obj[0])
									.longValue());

				}
			} else
			{
				log.info("Available Prospect List size " + branches != null ? branches.size() : 0 
						+ " for location " + location + " against " + prospectList.size());
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error while getting getTicketNosFromProspects "
					+ e.getMessage());
		}
		return propspectTicketMap;
		//
		// List<Ticket> tickets = hibernateTemplate
		// .getSessionFactory()
		// .getCurrentSession()
		// .createSQLQuery("select t.* from Ticket t where t.prospectNo in
		// (:prospectList)")
		// .addEntity(Ticket.class)
		// .setParameterList("prospectList", prospectList).list();
		//
		// if (tickets != null)
		// return tickets;
		// else
		// return new ArrayList<Ticket>();
	}

	@Override
	public TicketClosureDataVo getTicketByCustomerMobileNo(String mobileNo)
	{

		try
		{
//
//			String query_sms = "select t.* from Ticket t inner join  WorkOrder wo on wo.ticketId=t.id "
//					+ "inner join Customer c on c.id=t.customerId and  c.status=:customerStatus"
//					+ " and c.mobileNumber=:mobileNumber "
//					+ "inner join TicketActivityLog tact on tact.ticketId=t.id  and tact.activityId = 4 "
//					+ " group by tact.addedOn desc";
			

			String modified_query_sms = "select t.id as ticketId,t.prospectNo as prospectNum,wo.workOrderNumber as workOrderNum,"
					+ "c.mobileNumber as mobileNum,city.cityName as cityName,wo.id workorderId,c.id as customerId,c.mqId mqID,"
					+ "t.currentAssignedTo currentAssignedTo from "
					+ "Ticket t inner join  WorkOrder wo on wo.ticketId=t.id inner join "
					+ "Customer c on c.id=t.customerId and  c.status=? and c.mobileNumber=? inner join "
					+ "City city on c.cityId = city.id inner join TicketActivityLog tact on tact.ticketId=t.id  and tact.activityId = 4  "
					+ " group by tact.addedOn desc";
			
//			List<Ticket> tickets = hibernateTemplate.getSessionFactory()
//					.getCurrentSession().createSQLQuery(query_sms)
//					.addEntity(Ticket.class)
			// .setParameter("mobileNumber", mobileNo)
			// .setParameter("customerStatus", ProspectType.HOT_ID)
			//// .setParameter("ticketstatus",
//					// TicketStatus.PORT_ACTIVATION_SUCCESSFULL)
//					.list();
			
			
//			List<Object[]> tickets = hibernateTemplate.getSessionFactory()
//					.getCurrentSession().createSQLQuery(modified_query_sms)
//					.setParameter("mobileNumber", mobileNo)
//					.setParameter("customerStatus", ProspectType.HOT_ID)
			// .list();

			log.info("Query ################# " + modified_query_sms + " " + mobileNo + " " + ProspectType.HOT_ID );
			
			List<TicketClosureDataVo> tickets = jdbcTemplate.query(modified_query_sms, new Object[] {
					ProspectType.HOT_ID, mobileNo },  new TicketClosureDataMapper() );


			if (tickets != null && !(tickets.isEmpty()))
			{
				TicketClosureDataVo ticketClosureDataVo = null;
				
				for(TicketClosureDataVo obj : tickets)
				{

					Long ticketId = obj.getTicketId();
					String prospectNum = obj.getProspectNo();
					String workOrderNo = obj.getWorkOrderNo();
					String mobileNum = obj.getMobileNo();
					String cityName = obj.getCityName();
					String workorderId = obj.getWorkorderId();
					String customerId = obj.getCustomerId();
					String mqID = obj.getMqID();
					String currentAssignedTo = obj.getCurrentAssignedTo();

					ticketClosureDataVo = new TicketClosureDataVo(workOrderNo, ticketId,
							cityName, prospectNum, mobileNum, workorderId, customerId, mqID, currentAssignedTo);

					log.debug("TicketClosureDataVo [workOrderNo=" + workOrderNo
							+ ", ticketId=" + ticketId + ", cityName="
							+ cityName + ", prospectNum=" + prospectNum
							+ ", mobileNo=" + mobileNo + "]");
				}
				
				return ticketClosureDataVo;
			}

		} catch (Exception e)
		{
			log.error("Erroe While getting  TicketByCustomerMobileNo :"
					+ e.getMessage());
			e.printStackTrace();
			return null;
		}
		return null;
	}

	@Override
	public TicketClosureDataVo getTicketByCustomerMobileNoWithProspect(String mobileNo,
			String prospectNo)
	{

		try
		{
			// String query_web = "from Ticket t where t.customer.mobileNumber =
			// :mobileNumber and t.status=:ticketstatus and
			// t.prospectNo=:prospectNo";
//			String query_web = "from Ticket t where t.customer.mobileNumber = :mobileNumber 
//			and t.customer.status=:customerStatus and t.prospectNo=:prospectNo";
//
//			List<Ticket> tickets = hibernateTemplate.getSessionFactory()
//					.getCurrentSession().createQuery(query_web)
//					.setParameter("mobileNumber", mobileNo)
//					.setParameter("customerStatus", ProspectType.HOT_ID)
//					// .setParameter("ticketstatus",
//					// TicketStatus.PORT_ACTIVATION_SUCCESSFULL)
//					.setParameter("prospectNo", prospectNo).list();
//
//			if (tickets != null && !(tickets.isEmpty()))
//				return tickets.get(0);

			
			String modified_query_sms = "select t.id as ticketId,t.prospectNo as prospectNum,wo.workOrderNumber as workOrderNum,"
					+ "c.mobileNumber as mobileNum,city.cityName as cityName,wo.id workorderId,c.id as customerId,c.mqId mqID,"
					+ "t.currentAssignedTo currentAssignedTo from "
					+ "Ticket t inner join  WorkOrder wo on wo.ticketId=t.id and t.prospectNo=? inner join "
					+ "Customer c on c.id=t.customerId and  c.status=? and c.mobileNumber=? inner join "
					+ "City city on c.cityId = city.id inner join TicketActivityLog tact on tact.ticketId=t.id  and tact.activityId = 4  "
					+ "group by tact.addedOn desc";
			
//			List<Object[]> tickets = hibernateTemplate.getSessionFactory()
//					.getCurrentSession().createSQLQuery(modified_query_sms)
//					.setParameter("mobileNumber", mobileNo)
//					.setParameter("customerStatus", ProspectType.HOT_ID)
//					.setParameter("prospectNo", prospectNo)
//					.list();

//
//			if (tickets != null && !(tickets.isEmpty()))
//			{
//				TicketClosureDataVo ticketClosureDataVo = null;
//				
//				for(Object[] obj : tickets)
//				{
//					Long ticketId = Long.valueOf((obj[0].toString()));
//					String prospectNum = obj[1].toString();
//					String workOrderNo = obj[2].toString();
//					String mobileNum = obj[3].toString();
//					String cityName = obj[4].toString();
//					String workorderId = obj[5].toString();
//					String customerId = obj[6].toString();
//					String mqID = obj[7].toString();
//					String currentAssignedTo = obj[8].toString();
//
//					ticketClosureDataVo = new TicketClosureDataVo(workOrderNo, ticketId,
//							cityName, prospectNum, mobileNum, workorderId, customerId, mqID, currentAssignedTo);
//
//
//					log.info("TicketClosureDataVo [workOrderNo=" + workOrderNo
//							+ ", ticketId=" + ticketId + ", cityName="
//							+ cityName + ", prospectNum=" + prospectNum
//							+ ", mobileNo=" + mobileNo + "]");
//				}
			

			
//			List<Object[]> tickets = hibernateTemplate.getSessionFactory()
//					.getCurrentSession().createSQLQuery(modified_query_sms)
//					.setParameter("mobileNumber", mobileNo)
//					.setParameter("customerStatus", ProspectType.HOT_ID)
//					.list();
			
			log.info("Query ################# " + modified_query_sms + " " + mobileNo + " " + ProspectType.HOT_ID );
			
			List<TicketClosureDataVo> tickets = jdbcTemplate.query(modified_query_sms, new Object[] {
					prospectNo, ProspectType.HOT_ID,mobileNo },  new TicketClosureDataMapper() );


			if (tickets != null && !(tickets.isEmpty()))
			{
				TicketClosureDataVo ticketClosureDataVo = null;
				
				for(TicketClosureDataVo obj : tickets)
				{

					Long ticketId = obj.getTicketId();
					String prospectNum = obj.getProspectNo();
					String workOrderNo = obj.getWorkOrderNo();
					String mobileNum = obj.getMobileNo();
					String cityName = obj.getCityName();
					String workorderId = obj.getWorkorderId();
					String customerId = obj.getCustomerId();
					String mqID = obj.getMqID();
					String currentAssignedTo = obj.getCurrentAssignedTo();

					ticketClosureDataVo = new TicketClosureDataVo(workOrderNo, ticketId,
							cityName, prospectNum, mobileNum, workorderId, customerId, mqID, currentAssignedTo);

					log.debug("TicketClosureDataVo [workOrderNo=" + workOrderNo
							+ ", ticketId=" + ticketId + ", cityName="
							+ cityName + ", prospectNum=" + prospectNum
							+ ", mobileNo=" + mobileNo + "]");
				}
				
				return ticketClosureDataVo;
			}

		} catch (Exception e)
		{
			log.error("Error While getting ticket details :"+e.getMessage());;
			e.printStackTrace();
			return null;
		}
		return null;
	}

	@Override
	public List<TicketCustomerDetailsVO> getTicketsForCustomerAutoNotification()
	{

		Object[] params = { TicketStatus.PORT_ACTIVATION_SUCCESSFULL,
				TicketStatus.DIFF_BETWEEN_MODIFIED_ON_AND_NOW_IN_HOURS };

		return jdbcTemplate
				.query(TICKET_DETAIL_CUSTOMER_DETAIL_QUERY, params, new BeanPropertyRowMapper(TicketCustomerDetailsVO.class));
	}

	@Override
	public List<TicketCustomerDetailsVO> getTicketsForCustomerAutoNotificationOnETRUpdate()
	{

		return jdbcTemplate
				.query(ETR_UPDATE_QUERY, new BeanPropertyRowMapper(TicketCustomerDetailsVO.class));
	}

	@Override
	public void updateReassignment(Long ticketId, Long recipientUserId,
			int status)
	{
		try
		{
			log.info("Updating current assigned to as " + recipientUserId
					+ " for ticket id " + ticketId + " with status " + status);

			Ticket ticket = hibernateTemplate.get(Ticket.class, ticketId);

			if (ticket != null)
			{
				ticket.setUserByAssignedBy(hibernateTemplate
						.load(User.class, AuthUtils.getCurrentUserId()));

				User userByCurrentAssignedTo = hibernateTemplate
						.load(User.class, recipientUserId);
				
				if(userByCurrentAssignedTo != null)
					ticket.setUserByCurrentAssignedTo(userByCurrentAssignedTo);

				ticket.setStatus(status);

				hibernateTemplate.update(ticket);

				log.info("Updated current assigned to as " + recipientUserId
						+ " for ticket id " + ticketId + " with status "
						+ status);

			}
		} catch (DataAccessException e)
		{
			log.info(" Error while Updating current assigned to as "
					+ recipientUserId + " for ticket id " + ticketId
					+ " with status " + status + ". Message " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public CurrentUserVO ticketCurrentUser(Long ticketId)
	{
		return jdbcTemplate
				.queryForObject(TICKET_USER_INFO_QUERY, new Object[] {
						ticketId }, new TicketCurrentUserMapper());

	}

	@Override
	public String getProspectNumberForTicket(Long ticketId)
	{
		List<Object[]> result = hibernateTemplate.getSessionFactory()
				.getCurrentSession().createSQLQuery(PROSPECT_NUMBER_BY_TICKET)
				.setParameter("ticketId", ticketId).list();

		if (result == null || result.isEmpty())
			return null;
		else
		{
			Object obj = result.get(0);
			return (String) obj;
		}
	}

	private long givePriorityCategory(long priorityValue)
	{
		Map<Long, PriorityValue> prioritydef = definitionCoreService
				.getPriorityDefinitions();
		for (Map.Entry<Long, PriorityValue> entry : prioritydef.entrySet())
		{
			if (priorityValue >= entry.getValue().getStart()
					&& priorityValue <= entry.getValue().getEnd())
				return entry.getKey();
		}

		return -1;
	}

	@Override
	public DashBoardSummaryVO calculateDashBoardStatus(Long ticketType)
	{

		DashBoardSummaryVO vo = new DashBoardSummaryVO();
		if (ticketType == null)
			ticketType = 0l;

		String key = dbUtil
				.getKeyForQueueByUserId(AuthUtils.getCurrentUserId());

		Set<Long> ticketListForWorkingQueue = new LinkedHashSet<>();
		ticketListForWorkingQueue.add(-1L);

		ticketListForWorkingQueue = globalActivities
				.getAllTicketFromMainQueueByKey(key);

		List<Long> areaIds = new ArrayList<Long>();
		areaIds.add(-1L);
		StringBuilder completeQuery = new StringBuilder(WIP_COUNT);

		if (AuthUtils.isManager() || AuthUtils.isAdmin())
		{
			if (ticketListForWorkingQueue != null)
				ticketListForWorkingQueue.clear();
			else
				ticketListForWorkingQueue = new HashSet<Long>();
			areaIds = GenericUtil.getUsersSelectedAreaIds();
			Map<Long, String> tlUsers = userDao
					.getUserByGroupForAreas(areaIds, NI_TL_GROUP);

			if (tlUsers != null)
			{
				String userKey = null;
				for (long userId : tlUsers.keySet())
				{
					userKey = dbUtil.getKeyForQueueByUserId(userId);

					if (globalActivities
							.getAllTicketFromMainQueueByKey(userKey) != null)
						ticketListForWorkingQueue.addAll(globalActivities
								.getAllTicketFromMainQueueByKey(userKey));
				}
			}

			completeQuery.append(" and t.areaId in (:areaIds) ");
		}
		if (AuthUtils.isTLUser())
		{
			if (ticketListForWorkingQueue == null
					|| ticketListForWorkingQueue.size() == 0)
			{
				ticketListForWorkingQueue = new HashSet<Long>();
				ticketListForWorkingQueue.add(-1l);
			}
		}

		if (ticketListForWorkingQueue != null
				&& ticketListForWorkingQueue.size() > 0)
			completeQuery.append(" and t.id in (:ticketIds) ");

		else if (AuthUtils.isNEUser() && !AuthUtils.isTLUser())
		{
			UserSummaryVO summaryVo = new UserSummaryVO();
			summaryVo.setUser(new TypeAheadVo(AuthUtils
					.getCurrentUserId(), AuthUtils
							.getCurrentUserDisplayName()));
			populateUserSummaryData(summaryVo);
			vo.setUserWiseSummary(Collections.singletonList(summaryVo));
			return vo;
		}
		
		completeQuery.append(" order by t.TicketCreationDate desc");

		/*
		 * if(AuthUtils.isNITLUser()) { completeQuery.append(
		 * " where  u.reportTo in ("+ AuthUtils.getCurrentUserId()+")"); } else
		 * { completeQuery.append(" where  u.id in ("+
		 * AuthUtils.getCurrentUserId()+")"); }
		 */

		/*
		 * if(ticketType != null && ticketType > 0) { completeQuery.append(
		 * " and w.initialWorkStage = "+ticketType); }
		 */

		Map<Long, TicketActivityData> fiberTypeTicketsAndActivities = new HashMap<Long, TicketActivityData>();
		Map<Long, TicketActivityData> copperTypeTicketsAndActivities = new HashMap<Long, TicketActivityData>();

		Session s = getHibernateTemplate().getSessionFactory()
				.getCurrentSession();
		Query query = s.createSQLQuery(completeQuery.toString());

		if (ticketListForWorkingQueue != null
				&& ticketListForWorkingQueue.size() > 0)
			query.setParameterList("ticketIds", ticketListForWorkingQueue);

		if (AuthUtils.isManager()
				|| AuthUtils.isAdmin() && areaIds != null && !areaIds.isEmpty())
		{
			if (!AuthUtils.isNIManager())
				query.setParameterList("areaIds", areaIds);
			else
			{
				areaIds.add(-1L);

				Long areaId = AuthUtils.getCurrentUserBranch() != null
						? areaDao.getAreasByBranchId(AuthUtils
								.getCurrentUserBranch().getId() + "") != null
										? areaDao
												.getAreasByBranchId(AuthUtils
														.getCurrentUserBranch()
														.getId() + "")
												.entrySet().iterator().next()
												.getKey()
										: -1l
						: AuthUtils.getCurrentUserCity() != null
								? AuthUtils.getCurrentUserBranch() != null
										? areaDao
												.getAreasByBranchId(AuthUtils
														.getCurrentUserBranch()
														.getId() + "")
												.entrySet().iterator().next()
												.getKey()
										: -1L
								: -1l;

				areaIds.add(areaId);

			query.setParameterList("areaIds", areaIds);
		}
		}

		Map<Long, String> userWithNoData = new HashMap<Long, String>();

		List<Object[]> result = query.list();

		log.debug(" Dashboard table WIP_COUNT Complete Query "
				+ query.getQueryString());
		log.debug(areaIds
				+ " Dashboard table WIP_COUNT Complete Query ticketIds "
				+ ticketListForWorkingQueue);
		if (result != null)
		{
			for (Object[] obj : result)
			{
				Long ticketId = objectToLong(obj[0]);
				Long activityId = objectToLong(obj[1]);
				long wsId = objectToLong(obj[2]);
				long assignedToUserId = objectToLong(obj[3]);
				long priorityValue = objectToLong(obj[4]);

				String assignedToDisplayName = (String) (obj[5]);

				if (AuthUtils.isManager() || AuthUtils.isAdmin())
				{
					// assignedToUserId = objectToLong(obj[6]);
					// assignedToDisplayName = (String) (obj[7]);

					assignedToUserId = AuthUtils.getCurrentUserId();
					assignedToDisplayName = AuthUtils
							.getCurrentUserDisplayName();
				}

				userWithNoData.put(assignedToUserId, assignedToDisplayName);
				if (ticketId == null || ticketId == 0)
				{

					continue;
				}

				priorityValue = priorityValue
						+ PriortyUpdateHelper.getGlobalPriority().longValue();
				if (wsId == WorkStageType.COPPER)
				{
					TicketActivityData data = copperTypeTicketsAndActivities
							.get(ticketId);

					if (data == null)
					{
						Set<Long> activities = new HashSet<Long>();
						activities.add(activityId);

						data = new TicketActivityData(activities, ticketId, assignedToUserId, assignedToDisplayName, givePriorityCategory(priorityValue), priorityValue);

						copperTypeTicketsAndActivities.put(ticketId, data);
					} else
					{
						data.getCompletedActivities().add(activityId);
					}

				} else if (wsId == WorkStageType.FIBER)
				{
					TicketActivityData data = fiberTypeTicketsAndActivities
							.get(ticketId);

					if (data == null)
					{
						Set<Long> activities = new HashSet<Long>();
						activities.add(activityId);

						data = new TicketActivityData(activities, ticketId, assignedToUserId, assignedToDisplayName, givePriorityCategory(priorityValue), priorityValue);

						fiberTypeTicketsAndActivities.put(ticketId, data);
					} else
					{
						data.getCompletedActivities().add(activityId);
					}

				}
			}
			List<UserSummaryVO> userWiseSummary = new ArrayList<UserSummaryVO>();
			Map<Long, UserSummaryVO> userWiseSummaryMap = new TreeMap<Long, UserSummaryVO>();

			log.debug("fiberTypeTicketsAndActivities >>>>>>>>> "
					+ fiberTypeTicketsAndActivities.keySet());

			log.debug("copperTypeTicketsAndActivities >>>>>>>>> "
					+ copperTypeTicketsAndActivities.keySet());

			if (ticketType == WorkStageType.FIBER || ticketType <= 0)
				categorizeTickets(fiberTypeTicketsAndActivities, userWiseSummaryMap, WorkStageType.FIBER);

			if (ticketType == WorkStageType.COPPER || ticketType <= 0)
				categorizeTickets(copperTypeTicketsAndActivities, userWiseSummaryMap, WorkStageType.COPPER);

			for (Long id : userWiseSummaryMap.keySet())
			{
				userWithNoData.remove(id);
			}

			if (userWithNoData.size() > 0)
			{
				SummaryVO summaryVO = new SummaryVO();
				Map<Long, WiPSummaryVO> priorityWiseWiPSummary = new TreeMap<Long, WiPSummaryVO>();

				getpriorityWiseWiPSummary(priorityWiseWiPSummary);
				summaryVO.setPriorityWiseWiPSummary(priorityWiseWiPSummary);

				for (Map.Entry<Long, String> userEntry : userWithNoData
						.entrySet())
				{

					userWiseSummary
							.add(new UserSummaryVO(new TypeAheadVo(userEntry
									.getKey(), userEntry
											.getValue()), summaryVO));
				}

			}

			if (!AuthUtils.isNITLUser()
					&& (AuthUtils.isNEUser() || AuthUtils.isManager()))
			{
				userWiseSummary.add(userWiseSummaryMap
						.get(AuthUtils.getCurrentUserId()));
			}

			else if (!AuthUtils.isNITLUser() && !AuthUtils.isManager())
			{
				userWiseSummary.add(userWiseSummaryMap
						.get(AuthUtils.getCurrentUserId()));
			} else
			{
				userWiseSummary.addAll(userWiseSummaryMap.values());
			}

			vo.setUserWiseSummary(userWiseSummary);
			log.debug("userWiseSummary >>>>>>>>> " + userWiseSummary);

		}
		return vo;
	}

	private void getpriorityWiseWiPSummary(
			Map<Long, WiPSummaryVO> priorityWiseWiPSummary)
	{
		for (Map.Entry<Long, String> entry : PriorityFilter.definition
				.entrySet())
		{
			if (entry.getKey() > 0)
			{

				WiPSummaryVO VO = new WiPSummaryVO();
				priorityWiseWiPSummary.put(entry.getKey(), VO);

			}
		}
	}

	private void categorizeTickets(
			Map<Long, TicketActivityData> fiberTypeTicketsAndActivities,

			Map<Long, UserSummaryVO> userWiseSummaryMap, long ticketType)
	{
		for (Entry<Long, TicketActivityData> entry : fiberTypeTicketsAndActivities
				.entrySet())
		{
			if (entry.getValue().getAssignedToUserId() != null)
			{
				if (userWiseSummaryMap
						.get(entry.getValue().getAssignedToUserId()) == null)
				{
					UserSummaryVO vo = new UserSummaryVO();
					vo.setUser(new TypeAheadVo(entry.getValue()
							.getAssignedToUserId(), entry.getValue()
									.getAssignedToDisplayName()));
					userWiseSummaryMap
							.put(entry.getValue().getAssignedToUserId(), vo);

					Map<Long, WiPSummaryVO> priorityWiseWiPSummary = new TreeMap<Long, WiPSummaryVO>();

					initializeAllPriorityData(priorityWiseWiPSummary);

					WiPSummaryVO wipSummary = new WiPSummaryVO();

					populateWiPSummaryVO(wipSummary, entry
							.getValue(), ticketType);

					priorityWiseWiPSummary.put(entry.getValue()
							.getPriorityCategory(), wipSummary);

					SummaryVO summary = new SummaryVO();
					summary.setPriorityWiseWiPSummary(priorityWiseWiPSummary);

					vo.setSummary(summary);

				} else
				{
					UserSummaryVO vo = userWiseSummaryMap
							.get(entry.getValue().getAssignedToUserId());

					WiPSummaryVO wiseWiPSummary = vo.getSummary()
							.getPriorityWiseWiPSummary()
							.get(entry.getValue().getPriorityCategory());

					if (wiseWiPSummary == null)
					{
						wiseWiPSummary = new WiPSummaryVO();

						populateWiPSummaryVO(wiseWiPSummary, entry
								.getValue(), ticketType);

						vo.getSummary().getPriorityWiseWiPSummary()
								.put(entry.getValue()
										.getPriorityCategory(), wiseWiPSummary);

					} else
					{

						populateWiPSummaryVO(wiseWiPSummary, entry
								.getValue(), ticketType);

					}
				}
			}
		}

		populateOtherReportingUsersData(userWiseSummaryMap);
	}

	private void populateOtherReportingUsersData(
			Map<Long, UserSummaryVO> userWiseSummaryMap)
	{
		Map<Long, String> users = userDao
				.getReportingUsers(AuthUtils.getCurrentUserId());

		for (Map.Entry<Long, String> entry : users.entrySet())
		{
			if (!userWiseSummaryMap.containsKey(entry.getKey()))
			{
				UserSummaryVO vo = new UserSummaryVO();
				vo.setUser(new TypeAheadVo(entry.getKey(), entry.getValue()));
				populateUserSummaryData(vo);
				userWiseSummaryMap.put(entry.getKey(), vo);
			}
		}

	}

	/**
	 * @param userWiseSummaryMap
	 * @param entry
	 */
	private void populateUserSummaryData(UserSummaryVO vo)
	{
		Map<Long, WiPSummaryVO> priorityWiseWiPSummary = new TreeMap<Long, WiPSummaryVO>();

		initializeAllPriorityData(priorityWiseWiPSummary);

		SummaryVO summary = new SummaryVO();
		summary.setPriorityWiseWiPSummary(priorityWiseWiPSummary);

		vo.setSummary(summary);
	}

	private void initializeAllPriorityData(
			Map<Long, WiPSummaryVO> priorityWiseWiPSummary)
	{

		for (Long key : PriorityFilter.definition.keySet())
		{
			if (key > 0)
				priorityWiseWiPSummary.put(key, new WiPSummaryVO());

		}

	}

	private void populateWiPSummaryVO(WiPSummaryVO wipSummary,
			TicketActivityData value, long ticketType)
	{
		long ticketId = value.getTicketId();

		if (value.getCompletedActivities() != null)
			value.getCompletedActivities()
					.removeAll(Activity.SALES_ACTIVITIES_LIST);

		if (ticketType == WorkStageType.FIBER && !value.getCompletedActivities()
				.contains(Activity.CUSTOMER_ACCOUNT_ACTIVATION))
		{

			if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.FIBER_TYPE_ACTIVATION_PENDING))

			{

				wipSummary.addToActivationPending();
				wipSummary.addToActivationPendingTicketIds(ticketId);

			} else if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.FIBER_TYPE_COPPER_PENDING))

			{
				wipSummary.addToCoperPending();
				wipSummary.addToCoperPendingTicketIds(ticketId);

			} else if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.FIBER_LAID_RACK_FIXED_SPLICING_ACTIVATION_PENDING))
			{
				wipSummary
						.addToFiberLaidRackFixedSplActiPendingTicketIds(ticketId);

				wipSummary.addToFiberLaidRackFixedSplActiPending();

			} else if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.FIBER_DONE_RACK_SPLICING_PENDING))

			{
				wipSummary.addToFiberDoneRackSplicingPendingTicketIds(ticketId);
				wipSummary.addToFiberDoneRackSplicingPending();

			} else if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.FIBER_TYPE_ONLY_RACK_FIXED))

			{
				wipSummary.addToCxRackFixedOtherPendingTicketIds(ticketId);
				wipSummary.addToCxRackFixedOtherPending();

			} else if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.FIBER_TYPE_FIBER_PENDING)
					|| value.getCompletedActivities() == null
					|| value.getCompletedActivities().size() == 0)

			{
				wipSummary.addToFiberYetToStart();
				wipSummary.addToFiberYetToStartTicketIds(ticketId);
			}

			else if (value.getCompletedActivities().size() == 1
					&& value.getCompletedActivities().contains(0l))

			{
				wipSummary.addToFiberYetToStart();
				wipSummary.addToFiberYetToStartTicketIds(ticketId);
			}

			else
			{
				log.debug(ticketId + " fiber ticket not categorized "
						+ value.getCompletedActivities());
			}

		} else if (ticketType == WorkStageType.COPPER
				&& !value.getCompletedActivities()
						.contains(Activity.CUSTOMER_ACCOUNT_ACTIVATION))
		{

			if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.COPPER_TYPE_ACTIVATION_PENDING))

			{

				wipSummary.addToActivationPending();
				wipSummary.addToActivationPendingTicketIds(ticketId);

			} else if (value.getCompletedActivities()
					.containsAll(DashBoardStatusDefinition.COPPER_TYPE_COPPER_PENDING)
					|| value.getCompletedActivities() == null
					|| value.getCompletedActivities().size() == 0)

			{
				wipSummary.addToCoperPending();
				wipSummary.addToCoperPendingTicketIds(ticketId);
			} else
			{
				log.debug(ticketId + " copper ticket not categorized "
						+ value.getCompletedActivities());
			}
		}

	}

	@Override
	public List<TicketPriorityVo> getTicketPriorityVO(Set<Long> listOfTicketId)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int updatePriority(long ticketNo, Integer escalatedValue,
			boolean isValueToAdd)
	{

		log.info("Updating priority during feasibility for ticket No "
				+ ticketNo + " escalatedValue " + escalatedValue);

		Ticket ticket = hibernateTemplate.get(Ticket.class, ticketNo);

		if (ticket != null && ticket.getPriorityId() != null
				&& ticket.getPriorityId() > 0)
		{
			TicketPriority tp = hibernateTemplate
					.get(TicketPriority.class, ticket.getPriorityId());

			if (tp != null)
			{

				if (isValueToAdd)
				{
					escalatedValue += tp.getValue();
					tp.setValue(escalatedValue);

				} else
				{
					escalatedValue -= tp.getValue();
					tp.setValue(escalatedValue);
				}
				hibernateTemplate.update(tp);
				return 1;
			}

			return 0;

		} else
		{
			return 0;
		}

	}

	@Override
	public int updateTicketSubCategory(Long ticketId, String subCategoryName,
			Long subCategoryId)
	{


		try
		{
			Ticket ticket = hibernateTemplate.get(Ticket.class, ticketId);

			if (ticket != null)
			{
				ticket.setSubCategoryId(subCategoryId);
				ticket.setTicketSubCategory(subCategoryName);
				ticket.setModifiedBy(AuthUtils.getCurrentUserId());
				ticket.setModifiedOn(new Date());
				hibernateTemplate.update(ticket);

				return 1;
			}
		} catch ( Exception e) {
			log.error("Error occured while loading and updating Ticket by Ticket Id : "+ticketId);
			e.printStackTrace();
		}
		
		return 0;
	
	}

	@Override
	public List<BigInteger> getTicketIdsForCustomerNotification(
			Set<BigInteger> ticketIds)
	{
		StringBuilder completeQuery = new StringBuilder(TICKETS_FOR_CUSTOMER_NOTIFICATION);
		if (ticketIds != null && !ticketIds.isEmpty() && ticketIds.size() > 0)
			completeQuery.append(" and t.id not in (:ticketIds) ");
		List<BigInteger> ticketList = null;
		try
		{
			Query getTicketIds = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(completeQuery.toString())
					.setParameterList("status", FWMPConstant.TicketStatus.COMPLETED_DEF);

			if (ticketIds != null && !ticketIds.isEmpty()
					&& ticketIds.size() > 0)
				getTicketIds.setParameterList("ticketIds", ticketIds);

			log.info("Query to getTicketIds " + getTicketIds.getQueryString()
					+ " ticketIds " + ticketIds);

			ticketList = getTicketIds.list();
		} catch (Exception e)
		{
			log.error("getTicketIdForCustomerNotification" + e);

		}

		return ticketList;
	}

	@Override
	public List<Ticket> getTicketDetailsWithTicketList(
			List<BigInteger> ticketIds)
	{
		if (ticketIds.isEmpty())
			return new ArrayList<Ticket>();
		log.info("ticketList " + ticketIds);
		List<Ticket> tickets = hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("select t.* from Ticket t where t.id in (:ticketIds)")
				.addEntity(Ticket.class)
				.setParameterList("ticketIds", ticketIds).list();

		if (tickets != null)
			return tickets;
		else
			return new ArrayList<Ticket>();
	}

	@Override
	public boolean isEligibleForRefund(Long ticketId)
	{
		if( ticketId == null || ticketId.longValue() <= 0 )
			return false;
		
		try 
		{
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			log.info("isEligibleForRefund  ############ " + ticket);

			if (null != ticket)
			{
				if (null != ticket.getDocumentStatus() && null != ticket.getPaymentStatus()
						&& ticket.getPoiStatus() == TicketStatus.POI_DOCUMENTS_APPROVED
						&& ticket.getPoiStatus() == TicketStatus.POA_DOCUMENTS_APPROVED
						&& ticket.getPaymentStatus() == TicketStatus.PAYMENT_APPROVED)
					return true;
				else
					return false;
			}
		} 
		catch ( Exception e) {
			log.error("Error occured while loading and checking eligibility for Refund...for ticket id :"+ticketId);
			e.printStackTrace();
		} 
		return false;
	}

	@Override
	public boolean isPaymentApproved(Long ticketId)
	{

		if( ticketId == null || ticketId.longValue() <= 0 )
			return false;
		
		try 
		{
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			log.info("isEligibleForRefund  ############ " + ticket);

			if (null != ticket)
			{
				if (null != ticket.getDocumentStatus()
						&& null != ticket.getPaymentStatus() && ticket
								.getPaymentStatus() == TicketStatus.PAYMENT_APPROVED)
					return true;
				else
					return false;
			}
		} catch ( Exception e ) {
			log.error("Error occured while loading and checking isPaymentApproved for Ticket id : "+ticketId);
			e.printStackTrace();
		} 
			
		return false;
	}

//	@Override
//	public boolean isDocumentApproved(Long ticketId)
//	{
//
//		try {
//			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);
//
//			log.info("isEligibleForRefund  ############ " + ticket);
//
//			if (null != ticket)
//			{
//				if (null != ticket.getDocumentStatus()
//						&& null != ticket.getPaymentStatus() && ticket
//								.getDocumentStatus() == TicketStatus.DOCUMENTS_APPROVED)
//					return true;
//				else
//					return false;
//			}
//		} catch ( Exception e) {
//			log.error("Error occured while loading and checking isDocumentApproved for Ticket id : "+ticketId );
//			e.printStackTrace();
//		}
//		
// 
//			return false;
//	}

	@Override
	public boolean isPOADocumentApproved(Long ticketId)
	{

		try {
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			log.info("isEligibleForRefund  ############ " + ticket);

			if (null != ticket)
			{
				if (null != ticket.getDocumentStatus()
						&& null != ticket.getPaymentStatus() && ticket
								.getPoaStatus() == TicketStatus.POA_DOCUMENTS_APPROVED)
					return true;
				else
					return false;
			}
		} catch ( Exception e) {
			log.error("Error occured while loading and checking isDocumentApproved for Ticket id : "+ticketId );
			e.printStackTrace();
		}
		
 
			return false;
	}
	
	@Override
	public boolean isPOIDocumentApproved(Long ticketId)
	{

		try {
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			log.info("isEligibleForRefund  ############ " + ticket);

			if (null != ticket)
			{
				if (null != ticket.getDocumentStatus()
						&& null != ticket.getPaymentStatus() && ticket
								.getPoiStatus() == TicketStatus.POI_DOCUMENTS_APPROVED)
					return true;
				else
					return false;
			}
		} catch ( Exception e) {
			log.error("Error occured while loading and checking isDocumentApproved for Ticket id : "+ticketId );
			e.printStackTrace();
		}
		
 
			return false;
	}
	
	// permanentAddress
	@Override
	public void updateAadharPermanentAddress(long ticketId,
			String permanentAddress)
	{
		try
		{
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);
			if (ticket != null)
			{
				Customer c = ticket.getCustomer();
				if (c != null)
				{
					c.setPermanentAddress(permanentAddress);
					c.setIsCustomerViaAadhaar(Status.ACTIVE);
					hibernateTemplate.update(c);
				} else
				{
					log.info("Customer not found for ticket  " + ticketId);
				}
			}
		} catch (Exception e)
		{
			log.error("Erroe While updating  AadharPermanentAddress  :"
					+ e.getMessage());
			e.printStackTrace();
		}
	}

	public void updateProspectRetryCountForFailed(long ticketId)
	{
		try
		{
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);
			if (ticket != null)
			{
				if (ticket.getMqRetryCount() == null)
					ticket.setMqRetryCount(0);

				ticket.setMqRetryCount(ticket.getMqRetryCount() + 1);
				hibernateTemplate.update(ticket);
			}
		} catch (DataAccessException e)
		{
			log.error("Erroe While updating ProspectRetryCountForFailed :"
					+ e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public int updateDefectCode(long ticketId, long defectCode,
			long subDefectCode, long status, String remarks)
	{
		int row = 0;
		try
		{
			row = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(UPDATE_TICKET_DEFECT_CODE)
					.setParameter("defectCode", String.valueOf(defectCode))
					.setParameter("subDefectCode", String
							.valueOf(subDefectCode))
					.setParameter("status", status)
					.setParameter("remarks", remarks)
					.setParameter("id", ticketId).executeUpdate();

			log.info("Updated DefectCode for ticket " + ticketId
					+ " with defectCode " + defectCode + " and subDefect code "
					+ subDefectCode);
		} catch (Exception e)
		{
			log.error("Error in Updating DefectCode " + e.getMessage());
		}

		return row;
	}

	@Override
	public int updateFrTicketClosingCode(FRDefectSubDefectsVO resolutionCode)
	{
		int row = 0;
		try
		{
			FrTicket ticket = hibernateTemplate
					.load(FrTicket.class, resolutionCode.getTicketId());
			if (ticket != null)
			{
				ticket.setDefectCode(resolutionCode.getDefectCode());
				ticket.setSubDefectCode(resolutionCode.getSubDefectCode());
				ticket.setStatus(Integer
						.valueOf(resolutionCode.getStatus() + ""));
				ticket.setRemarks(resolutionCode.getRemarks());
				ticket.setModifiedOn(new Date());
				ticket.setModifiedBy(AuthUtils.getCurrentUserId());

				hibernateTemplate.update(ticket);
				row++;
				log.info("Updated DefectCode sub defect code for" + " ticket "
						+ resolutionCode.getTicketId());
			} else
			{
				log.info("Can't update DefectCode sub defect code for"
						+ " ticket " + resolutionCode.getTicketId());
			}
		} catch (Exception e)
		{
			log.error("Error occure while updating defect sub defect "
					+ "code for ticket : " + resolutionCode.getTicketId()
					+ e.getMessage());
		}

		return row;
	}

	@Override
	public Integer getTicketStatusById(Long ticketId)
	{
		try
		{
			FrTicket ticket = getHibernateTemplate()
					.load(FrTicket.class, ticketId);
			if (ticket != null)
			{
				Set<FrDetail> workOrder = ticket.getFrWorkOrders();
				if (workOrder != null && workOrder.size() > 0)
				{
					FrDetail detail = workOrder.iterator().next();
					if (detail.isBlocked())
						return FRStatus.FLOW_APPROVAL_PENDING;
				}
				return 0;
			}
		} catch (Exception exception)
		{
			log.error("Error occure while getting status by Id : " + ticketId);
		}
		return null;
	}

	@Override
	public Map<String, Long> getCompleteProspectStatusMap(Set<String> prospects)
	{
		log.info("Total prospects size is " + prospects.size()
				+ " and details are " + prospects);

		List<Object[]> results = hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("select prospectNo,status from Ticket where prospectNo in (:prospects)")
				.setParameterList("prospects", prospects).list();

		log.info("Total prospects results is " + results.size());

		Map<String, Long> completeProspectStatusMap = new ConcurrentHashMap<String, Long>();

		if (results != null && !results.isEmpty())
		{
			for (Object[] obj : results)
			{
				log.info("obj[0].toString(), Long.valueOf(obj[1].toString()) "
						+ obj[0].toString() + " "
						+ Long.valueOf(obj[1].toString()));

				completeProspectStatusMap.put(obj[0].toString(), Long
						.valueOf(obj[1].toString()));
			}
		}

		return completeProspectStatusMap;
	}
	
	private int getTicketPendingStatus(Integer status)
	{
		if (definitionCoreService
				.getStatusFilterForRole(DefinitionCoreService.TICKET_PENDING_STATUS)
				.contains(status))
			return FRStatus.FLOW_APPROVAL_PENDING;
		else
			return 0;

	}

	// @Override
	// public void updateCurrentAssignedTo(long ticketId, Long userIdAssignedTo)
	// {
	// log.info("Updating updateCurrentAssignedTo for icketId " + ticketId
	// + " userId " + userIdAssignedTo);
	// String userIdAssignedToString = "";
	// if (userIdAssignedTo == null || userIdAssignedTo <= 0)
	// {
	// userIdAssignedToString = "null";
	// } else
	// {
	// userIdAssignedToString = userIdAssignedTo + "";
	// }
	//
	// String query = "update Ticket t, WorkOrder wo set t.currentAssignedTo="
	// + userIdAssignedToString + ",wo.assignedTo="
	// + userIdAssignedToString + " where t.id=" + ticketId
	// + " and t.id=wo.ticketId";
	//
	// int result = jdbcTemplate.update(query);
	//
	// log.info("Total updated row are " + result + " query " + query);
	// }

	@Override
	public int updateTicketStatus(Long ticketId, int statusCode)
	{
		try
		{
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			if (ticket != null)
			{
				ticket.setStatus(statusCode);
				ticket.setModifiedOn(new Date());
				hibernateTemplate.update(ticket);

				return 1;
			}
		} catch (DataAccessException e)
		{
			log.error("Error while updating ticket status as " + statusCode
					+ " for ticket " + ticketId);
			e.printStackTrace();
			return 0;
		}

		return 0;
	}
	

	@Override
	public int updateTicketStatusAndAssignedBy(Long ticketId, int statusCode, Long assignedTo)
	{
		try
		{
			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			if (ticket != null)
			{
				ticket.setStatus(statusCode);
				ticket.setModifiedOn(new Date());
				ticket.setUserByAssignedBy(hibernateTemplate.load(User.class, assignedTo));
				ticket.setUserByAssignedTo(hibernateTemplate.load(User.class, assignedTo));
			
				hibernateTemplate.update(ticket);

				return 1;
			}
		} catch (DataAccessException e)
		{
			log.error("Error while updating ticket status as " + statusCode
					+ " for ticket " + ticketId);
			e.printStackTrace();
			return 0;
		}

		return 0;
	}

	@Override
	public List<Ticket> getAllNotCompletedDeployedTickets()
	{
		try
		{
			List<Ticket> ticketsList = new ArrayList<Ticket>();
			Query getDeployedTickets = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(GET_ALL_NOT_COMPLETED_TICKETS)
					.addEntity(Ticket.class)
					.setParameterList("status", FWMPConstant.TicketStatus.COMPLETED_DEF);

			log.info("Query to getTicketIds "
					+ getDeployedTickets.getQueryString());

			ticketsList = getDeployedTickets.list();
			if (ticketsList != null)
				return ticketsList;

		} catch (Exception exception)
		{
			log.error("Error While getting NotCompletedDeployedTickets "
					+ exception.getMessage());
		}
		return new ArrayList<Ticket>();

	}

	@Override
	public int updateCommunicationETR(Long ticketId, Date communicationEtr)
	{
		int row = 0;
		try
		{
			row = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(UPDATE_COMMUNICATION_ETR)
					.setParameter("communicationETR", communicationEtr)
					.setParameter("currentEtr", communicationEtr)
					.setParameter("id", ticketId).executeUpdate();

			log.info("Updated communicationETR for ticket " + ticketId
					+ " with communicationETR " + communicationEtr);
		} catch (Exception e)
		{
			log.error("Error in Updating communicationETR " + e.getMessage());
		}

		return row;
	}

	@Override
	public void updateReopneTicket(Ticket ticket)
	{
		if (ticket != null && ticket.getId() > 0)
			hibernateTemplate.update(ticket);

	}
	

	class TicketClosureDataMapper implements RowMapper<TicketClosureDataVo>
	{
		@Override
		public TicketClosureDataVo mapRow(ResultSet rs, int arg1)
				throws SQLException
		{
			TicketClosureDataVo ticketClosureDataVo = new TicketClosureDataVo();
			
			ticketClosureDataVo.setTicketId(rs.getLong("ticketId"));
			ticketClosureDataVo.setProspectNo(rs.getString("prospectNum"));
			ticketClosureDataVo.setWorkOrderNo(rs.getString("workOrderNum"));
			ticketClosureDataVo.setMobileNo(rs.getString("mobileNum"));
			ticketClosureDataVo.setCityName(rs.getString("cityName"));
			ticketClosureDataVo.setWorkorderId(rs.getString("workorderId"));
			ticketClosureDataVo.setCustomerId(rs.getString("customerId"));
			ticketClosureDataVo.setMqID(rs.getString("mqID"));
			ticketClosureDataVo
					.setCurrentAssignedTo(rs.getString("currentAssignedTo"));

			return ticketClosureDataVo;
		}

	}


	@Override
	public int updateAadharPermanentAddressAndCustomerFlag(long ticketId,
			String permanentAddress, boolean isCustomerViaAadhaar)
	{
		log.info("Updating Customer aadhaar details for ticket id " + ticketId + " and customerAadharStatus is "  + isCustomerViaAadhaar );
		
		int result = 0;
		
		try
		{
			if(ticketId >0)
			{
				result = jdbcTemplate.update(UPDATE_CUSTOMER_PERMANENT_ADDRESS_WITH_AADHAR, new Object[] {
						isCustomerViaAadhaar ? Status.ACTIVE : Status.IN_ACTIVE, ticketId });
				
				log.info("Updating Customer aadhaar details for ticket id " + ticketId + " and customerAadharStatus is "
						+ isCustomerViaAadhaar + " Query " + UPDATE_CUSTOMER_PERMANENT_ADDRESS_WITH_AADHAR + " "
						+ (isCustomerViaAadhaar ? Status.ACTIVE : Status.IN_ACTIVE));
			}
			 
		} catch (Exception e)
		{
			log.error("Erroe While updating  AadharPermanentAddress for Ticket  : " + ticketId
					+ e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public int updateEmailSentTime(String cafNumber, Date sentTime)
	{
		log.info("Updating Ticket email sent time for caf number " + cafNumber + " and sent time is "  + sentTime );
		
		int result = 0;
		
		try
		{
			if (cafNumber != null && !cafNumber.isEmpty()) {
				result = jdbcTemplate.update(UPDATE_TICKET_ECAF_MAIL_SENT_TIME, new Object[] { sentTime, cafNumber });

				log.info("Updating Ticket email sent time for caf number " + cafNumber + " and sent time is " + sentTime
						+ " values " + result);
			}
		} catch (Exception e)
		{
			log.error("Erroe While updating  pdating Ticket email sent time for caf number  : " + cafNumber
					+ e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public void prospectLoggerFromSiebel(CRMProspectLogger crmProspectLogger) {
		
		if(crmProspectLogger != null)
			mongoTemplate.insert(crmProspectLogger);
		
	}
	
	

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.ticket.TicketDAO#isEligibleForWOCreation(com.cupola.fwmp.vo.tools.siebel.vo.WOEligibilityCheckVO)
	 */
	@Override
	public WOEligibilityCheckResponse isEligibleForWOCreation(WOEligibilityCheckVO woEligibilityCheckVO, Long cityId) 
	{
		String query = "select distinct c.firstName as customerName,t.prospectNo as prospectNo,"
				+ "tal.activityId as activityId, city.cityName as cityName, city.id as cityId "
				+ "from Customer c join Ticket t on c.id=t.customerId Join TicketActivityLog tal on tal.ticketId=t.id "
				+ "join City city on city.id=c.cityId where t.prospectNo=? and c.cityId = ? and t.poiStatus=1050 and t.poaStatus=1053 and t.paymentStatus=161";

		log.info("Checking eligibility of prospect " + woEligibilityCheckVO.getProspectNo()
				+ " for WO conversion for city " + woEligibilityCheckVO.getCityName() + " " + " and query is " + query);

		WOEligibilityCheckResponse woEligibilityCheckResponse = new WOEligibilityCheckResponse();

		List<WOEligibilityVO> activities = jdbcTemplate.query(query,
				new Object[] { woEligibilityCheckVO.getProspectNo(), cityId }, new WOEligibilityMapper());

		if (activities != null && !activities.isEmpty()) {
			log.info("Total row fatched for mqId " + woEligibilityCheckVO.getProspectNo() + " is " + activities.size());

			Set<Long> activityIds = new LinkedHashSet<Long>();

			for (Iterator<WOEligibilityVO> iterator = activities.iterator(); iterator.hasNext();) {
				WOEligibilityVO aatEligibilityVO = (WOEligibilityVO) iterator.next();

				activityIds.add(aatEligibilityVO.getActivityId());

			}

			if (activityIds.containsAll(WOConversionCheck.SALES_SIEBEL_ACTIVITIES)) {
				woEligibilityCheckResponse.setEligibleForWorkOrderCreation(WOConversionCheck.ELIGIBILE);
				woEligibilityCheckResponse.setCityName(woEligibilityCheckVO.getCityName());
				woEligibilityCheckResponse.setProspectNo(woEligibilityCheckVO.getProspectNo());

			} else {

				woEligibilityCheckResponse.setEligibleForWorkOrderCreation(WOConversionCheck.NON_ELIGIBILE);
				woEligibilityCheckResponse.setCityName(woEligibilityCheckVO.getCityName());
				woEligibilityCheckResponse.setProspectNo(woEligibilityCheckVO.getProspectNo());

			}

		} else {
			woEligibilityCheckResponse.setEligibleForWorkOrderCreation(WOConversionCheck.NON_ELIGIBILE);
			woEligibilityCheckResponse.setCityName(woEligibilityCheckVO.getCityName());
			woEligibilityCheckResponse.setProspectNo(woEligibilityCheckVO.getProspectNo());
		}

		return woEligibilityCheckResponse;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.ticket.TicketDAO#logCreateUpdateWorkOrderSiebelData(com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateWorkOrderVo)
	 */
	@Override
	public void workOrderLoggerFromSiebel(CRMWorkOrderLogger crmWorkOrderLogger) {

		if(crmWorkOrderLogger != null)
			mongoTemplate.insert(crmWorkOrderLogger);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.ticket.TicketDAO#logCreateUpdateFrSiebelData(com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo)
	 */
	@Override
	public void frLoggerFromSiebel(CRMFrLogger crmFrLogger) {
		
		if(crmFrLogger != null)
			mongoTemplate.insert(crmFrLogger);
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.ticket.TicketDAO#isProspectPresentInDB(java.lang.String)
	 */
	@Override
	public boolean isProspectPresentInDB(String prospectNumber) {

		try {
			List<Object> workOrders = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select prospectNo from Ticket where prospectNo= :prospectNo")
					.setParameter("prospectNo", prospectNumber).list();

			if (workOrders != null && !workOrders.isEmpty())
				return true;
		} catch (Exception e) {
			log.error("Error while getting prospectNo details for prospect " + prospectNumber + " " + e.getMessage());
		}

		return false;
	
	}
	
	
	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.ticket.TicketDAO#getFrTicketIdByWorkOrder(java.lang.String)
	 */
	@Override
	public long getFrTicketIdByWorkOrder(String workOrderNumber) {

		long ticketId = 0;
		try {
			ticketId = jdbcTemplate.queryForLong("Select id from FrTicket where workOrderNumber = ?",
					new Object[] { workOrderNumber });
		} catch (Exception e) {
			log.error("Error while getting frTicket by wo " + workOrderNumber + " " + e.getMessage());
			e.printStackTrace();
			
		}
		return ticketId; 

	}
}


class WorkOrderVoMapper implements RowMapper<WorkOrderVo>
{

	@Override
	public WorkOrderVo mapRow(ResultSet rs, int arg1) throws SQLException
	{

		WorkOrderVo wo = new WorkOrderVo();
		wo.setTicketId(rs.getLong("ticketId"));
		wo.setAssignedTo(rs.getLong("assignedTo"));
		wo.setAssignedBy(rs.getLong("assignedBy"));

		return wo;
	}

}

class TicketCurrentUserMapper implements RowMapper<CurrentUserVO>
{
	@Override
	public CurrentUserVO mapRow(ResultSet rs, int arg1) throws SQLException
	{
		CurrentUserVO currentUserVO = new CurrentUserVO();

		currentUserVO.setTicketId(rs.getLong("id"));
		currentUserVO.setAssignedBy(rs.getLong("assignedBy"));
		currentUserVO.setCurrentAssignedTo(rs.getLong("currentAssignedTo"));
		currentUserVO.setAssignedTo(rs.getLong("assignedTo"));

		return currentUserVO;
	}
}


class WOEligibilityMapper implements RowMapper<WOEligibilityVO>
{

	@Override
	public WOEligibilityVO mapRow(ResultSet rs, int arg1) throws SQLException
	{
		// "select distinct c.firstName as customerName,t.prospectNo as prospectNo,"
		// + "tal.activityId as activityId, city.cityName as cityName, city.id as cityId

		WOEligibilityVO woEligibilityVO = new WOEligibilityVO();

		woEligibilityVO.setCustomerName(rs.getString("customerName"));
		woEligibilityVO.setProspectNo(rs.getString("prospectNo"));
		woEligibilityVO.setActivityId(rs.getLong("activityId"));
		woEligibilityVO.setCityName(rs.getString("cityName"));
		woEligibilityVO.setCityId(rs.getLong("cityId"));
		return woEligibilityVO;
	}
}