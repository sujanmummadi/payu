package com.cupola.fwmp.dao.ticket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.BooleanValue;
import com.cupola.fwmp.FWMPConstant.DeDupFlag;
import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.ecaf.EcafDAO;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.subArea.SubAreaDAO;
import com.cupola.fwmp.dao.tariff.TariffDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.workStage.WorkStageDAO;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.Payment;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.service.WorkOrderDefinitionCache;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.PriorityValue;
import com.cupola.fwmp.service.domain.engines.classification.jobs.PriortyUpdateHelper;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.ticket.TicketCoreService;
import com.cupola.fwmp.util.ActivitySequenceComparator;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StatusMessages;
import com.cupola.fwmp.util.TicketPriorityComparator;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.SubActivityVo;
import com.cupola.fwmp.vo.TicketActivityLogVo;
import com.cupola.fwmp.vo.TicketDetailVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.WorkOrderTypeVO;
import com.cupola.fwmp.vo.WorkStageVo;
import com.cupola.fwmp.vo.sales.TariffPlanVo;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;

@Transactional
public class TicketDetailDAOImpl implements TicketDetailDAO
{
	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	SubAreaDAO subAreaDao;
	@Autowired
	CustomerDAO customerDAO;
	@Autowired
	TariffDAO tariffDAO;
	/*
	 * @Autowired AreaDAO areaDao;
	 * 
	 * @Autowired CityDAO cityDao;
	 * 
	 * @Autowired BranchDAO branchDAO;
	 */
	@Autowired
	UserDao userDao;
	@Autowired
	GisDAO gisDAO;
	@Autowired
	WorkStageDAO workStageDAO;
	@Autowired
	TicketCoreService ticketCoreService;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	TicketActivityLogDAO ticketActivityLoggingDAO;
	
	@Autowired
	EcafDAO ecafDAO;

	static int MAX_ITEM_PER_PAGE = 50;
	static int MIN_ITEM_PER_PAGE = 5;

	private static Logger logger = Logger.getLogger(TicketDetailDAOImpl.class
			.getName());
	
	@Value("${tickets.for.customer.notification}")
	private String FWMP_ADDRESS_LIST_RELEASE_DATE;
	
	static int IS_SALES = 1;
	static int IS_NOT_SALES = 2;

	public final int PAYMENT_PAGE = 1;
	public final int DOCUMENT_PAGE = 2;

	static int SEARCH_PAGE = 3;
	static int CC_PAGE = 4;

	@Value("${wait.hours.till.customer.reply.before.close.ticket}")
	private int timeDiffenrenceForCC;

	// private String customerPermanentAddress;
	// private String pincode;
	// private String city;
	// private String branch;
	// id, CustomerName, CustomerType, tariffId, mobileNumber,
	// alternativeMobileNo, officeNumber, emailId,
	// CustomerProfession, alternativeEmailId, subscriptionTypeId, mqId,
	// currentAddress, communicationAdderss,
	// permanentAddress, branchId, areaId, cityId,
	// routerDescription, routerRequired, addedBy, addedOn, modifiedBy,
	// modifiedOn, status, id, id
	//

	private static String TICKET_DETAIL_BASE_QUERY = "SELECT wo.id as woId, t.ticketCategoryId, wo.status as woStatus, wo.totalPriority, "
			+ "wo.currentProgress,"
			+ " t.source, t.currentEtr, c.mqId as mq, c.id as cId ,wo.assignedTo, "
			// + "wo.initialWorkStage,"
			+ "wo.currentWorkStage,"
			+ "t.id,wo.workOrderNumber,c.areaId,t.status,t.cafNumber,t.remarks,"
			+ "c.routerRequired,c.routerDescription,t.currentAssignedTo, t.ticketCreationDate as tcd ,t.prospectNo,"
			+ "t.preferedDate,c.branchId,c.cityId, c.tariffId,c.communicationAdderss,c.pincode,t.TicketSubCategory"
			+ ",c.emailId,tp.packageName , t.subCategoryId,wo.escalatedValue,wo.priorityEscalationType ,c.lattitude,c.longitude"
			+ " , tpr.value, tpr.escalationType, tpr.escalatedValue as tpescalatedValue, tpr.updatedCount , "
			+ "tpr.sliderEscalatedValue as tpeSscalatedValue , t.cxPermission, t.gxPermission ,"
			+ " t.paymentRemark,t.documentRemark,t.mqStatus,t.mqRetryCount, c.neComment , "
			+ "t.commitedEtr as commitedTime, c.remarks as customerRemarks, t.assignedBy,"
		/*	+ "c.poi, c.poa, t.poiStatus, t.poaStatus, t.poaRemarks, t.poiRemarks ,"
			+ "c.referralCustomerAccountNo as ReferralCustomerAccountNo,c.referralCustomerMobileNo as referralCustomerMobileNo,"
			+ "c.isStaticIPRequired as staticIPRequired,c.existingISP as ExistingISP,c.nationality as nationality,t.subAreaId, "*/
			+ "wo.appointmentDate"
			
			// " FROM WorkOrder wo, Ticket t, Customer c, TariffPlan tp  where tp.id=c.tariffId and t.id = wo.ticketId and t.customerId = c.id ";
			// +
			// " FROM WorkOrder wo INNER JOIN Ticket t ON t.id = wo.ticketId INNER JOIN Customer c ON t.customerId = c.id Left Outer JOIN TariffPlan tp ON c.tariffId = tp.id where c.id > 0 ";
			+ " FROM Ticket t  INNER JOIN Customer c ON t.customerId = c.id"
			 
			+ " Left Outer JOIN TariffPlan tp ON c.tariffId = tp.id  "
			+ "left Outer JOIN WorkOrder wo ON t.id = wo.ticketId  Left outer Join TicketPriority tpr on tpr.id = t.priorityId where c.id > 0 ";

	private static String TICKET_DETAIL_PRE_SALE_QUERY = "SELECT t.id,t.prospectNo,"
			+ "t.source,t.currentEtr,"
			+ "c.mqId as mq,"
			+ "c.id as cId,"
			+ "c.firstName,"
			+ "c.mobileNumber,"
			+ "c.communicationAdderss ,t.ticketCreationDate ,c.areaId ,t.status,t.cafNumber,t.remarks, "
			+ "c.routerRequired,c.routerDescription,t.currentAssignedTo , t.ticketCreationDate as tcd ,"
			+ "t.preferedDate,t.ticketCategory,c.branchId,c.cityId, c.tariffId,c.permanentAddress,c.pincode,t.idPayment,t.TicketSubCategory"
			+ ",c.emailId,c.enquiry,c.knownSource, c.remarks as customerRemarks,c.status as customerStatus, t.subCategoryId , "
			+ "t.paymentStatus, t.documentStatus,"
			+ " t.cxPermission, t.gxPermission ,c.alternativeMobileNo,c.officeNumber,c.CustomerProfession,c.alternativeEmailId,"
			+ "c.externalRouterRequired, c.externalRouterDescription, c.externalRouterAmount,c.lattitude,c.longitude,  "
			+ " tp.value, tp.escalationType, tp.escalatedValue, tp.updatedCount , c.currentAddress,  t.paymentRemark,"
			+ "t.documentRemark, t.mqStatus,t.mqRetryCount, c.neComment, t.commitedEtr as commitedTime, "
			+ "t.assignedBy , c.poi, c.poa, t.poiStatus, t.poaStatus, t.poaRemarks, t.poiRemarks , t.poaDocumentRemark ,t.poiDocumentRemark , "
			+ "c.referralCustomerAccountNo as ReferralCustomerAccountNo,c.referralCustomerMobileNo as referralCustomerMobileNo,"
			+ "c.isStaticIPRequired as staticIPRequired,c.existingISP as ExistingISP,c.nationality as nationality,"
			+ "t.subAreaId as subAreaId,t.uinType as UinType,t.uin as Uin, "
			+ "a.UID "
			// "Ticket t, Customer c where   t.customerId = c.id  ";inner join Payment p on p.id=t.idPayment
			+ " FROM  "
			+ " Ticket t  INNER JOIN Customer c on   t.customerId = c.id "
			+ " Left outer Join Adhaar a on a.id = c.adhaarId "
			+ " Left outer Join TicketPriority tp on tp.id = t.priorityId where  c.id > 0 "
			;
	private static String TICKET_DETAIL_CFE_QUERY = "SELECT t.id,t.prospectNo," + "t.source,t.currentEtr,"
			+ "c.mqId as mq," + "c.id as cId," + "c.firstName," + "c.mobileNumber,"
			+ "c.communicationAdderss ,t.ticketCreationDate ,c.areaId ,t.status,t.cafNumber,t.remarks, "
			+ "c.routerRequired,c.routerDescription,t.currentAssignedTo , t.ticketCreationDate as tcd ,"
			+ "t.preferedDate,t.ticketCategory,c.branchId,c.cityId, c.tariffId,c.permanentAddress,c.pincode,t.idPayment,t.TicketSubCategory"
			+ ",c.emailId,c.enquiry,c.knownSource, c.remarks as customerRemarks,c.status as customerStatus, t.subCategoryId , "
			+ "t.paymentStatus, t.documentStatus,"
			+ " t.cxPermission, t.gxPermission ,c.alternativeMobileNo,c.officeNumber,c.CustomerProfession,c.alternativeEmailId,"
			+ "c.externalRouterRequired, c.externalRouterDescription, c.externalRouterAmount,c.lattitude,c.longitude,  "
			+ " tp.value, tp.escalationType, tp.escalatedValue, tp.updatedCount , c.currentAddress,  t.paymentRemark,"
			+ "t.documentRemark, t.mqStatus,t.mqRetryCount, c.neComment, t.commitedEtr as commitedTime,t.assignedBy, "
			+ "a.UID , a.name , a.dob , a.Email , a.gender , a.phone , a.careOf , a.houseno , a.district , a.locality , a.landmark , a.pincode as adhaarPinCode, a.postoffice , a.street ,a.state ,a.vtcname, "
			+ " c.poi, c.poa, t.poiStatus, t.poaStatus, t.poaRemarks, t.poiRemarks, c.finahubTransactionId "
			/*+ "c.referralCustomerAccountNo as ReferralCustomerAccountNo,c.referralCustomerMobileNo as referralCustomerMobileNo,"
			+ "c.isStaticIPRequired as staticIPRequired,c.existingISP as ExistingISP,c.nationality as nationality,t.subAreaId as subAreaId "
			
*/			// "Ticket t, Customer c where t.customerId = c.id ";
			+ " FROM  " + " Ticket t  INNER JOIN Customer c on   t.customerId = c.id "
			+ " Left outer Join Adhaar a on a.id = c.adhaarId"
			+ " Left outer Join TicketPriority tp on tp.id = t.priorityId where  c.id > 0 ";

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	@Override
	public List<TicketDetailVo> searchTicketWithDetails(TicketFilter filter)
	{
		if( filter == null )
			return null;
		
		logger.debug(" inside  searchTicketWithDetails  " + filter.getMqId());
		try
		{

			logger.info("Search tickets for customer Mobl:  "
					+ filter.getCustomerContactNumber() + " cId: "
					+ filter.getCustomerId());

			filter.setPageContextId(SEARCH_PAGE);
			List<Customer> customers = null;
			if (filter.getCustomerContactNumber() != null
					&& filter.getCustomerContactNumber() > 0)
				customers = (List<Customer>) hibernateTemplate
						.find("from Customer c where c.mobileNumber =?", filter
								.getCustomerContactNumber().toString());
			else
			{
				List<Ticket> t = (List<Ticket>) hibernateTemplate
						.find("from Ticket t where t.prospectNo=?", String
								.valueOf(filter.getCustomerId()));
				if (t != null && !t.isEmpty())
				{
					Customer c = t.get(0).getCustomer();
					customers = new ArrayList<Customer>();
					customers.add(c);
				}
			}

			if (customers == null || customers.size() == 0)
			{
				logger.info("Customer not found with details "
						+ filter.getCustomerContactNumber() + " or "
						+ filter.getCustomerId());
				return null;
			} else
			{
				filter.setIncludeCompleteStatus(true);
				List<TicketDetailVo> tickets = new ArrayList<TicketDetailVo>();
				List<Long> presalesTicketIds = new ArrayList<Long>();
				List<Long> othersTicketIds = new ArrayList<Long>();

				for (Iterator iterator = customers.iterator(); iterator
						.hasNext();)
				{
					Customer customer = (Customer) iterator.next();

					if (customer.getTickets() != null
							&& customer.getTickets().size() > 0)
					{
						for (Object obj : customer.getTickets())
						{
							Ticket ticket = (Ticket) obj;

							if (filter.getCustomerId() != null
									&& !filter.getCustomerId().toString()
											.equals(ticket.getProspectNo()))
							{
								continue;
							}

							if (ticket.getWorkOrders() != null
									&& ticket.getWorkOrders().size() > 0)
							{
								othersTicketIds.add(ticket.getId());
							} else
							{
								presalesTicketIds.add(ticket.getId());
							}

						}
					}
				}
				if (othersTicketIds.size() > 0)
				{
					filter.setTicketIds(othersTicketIds);
					tickets.addAll(getTicketWithDetails(filter));
				}
				if (presalesTicketIds.size() > 0)
				{
					filter.setTicketIds(presalesTicketIds);
					tickets.addAll(getPreSaleTicketWithDetails(filter));
				}
				return tickets;
			}

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception in searching ticket for filter " + filter);
		}
		logger.debug(" exit from  searchTicketWithDetails  " + filter.getMqId());
		return new ArrayList<TicketDetailVo>();

	}

	@Override
	public List<TicketDetailVo> getTicketWithDetails(TicketFilter filter)
	{
		if (filter == null)
			filter = new TicketFilter();
		
		logger.debug(" inside  getTicketWithDetails  "  +filter.getMqId());
		try
		{
			StringBuilder completeQuery = new StringBuilder(TICKET_DETAIL_BASE_QUERY);
			

			if (filter.getPageSize() > 0)
				;
			else
			{
				filter.setStartOffset(0);
				filter.setPageSize(MIN_ITEM_PER_PAGE);
			}

			if (filter.getTicketId() != null && filter.getTicketId() > 0)
				completeQuery.append(" and t.id =" + filter.getTicketId());

			if (filter.getTicketIds() != null
					&& filter.getTicketIds().size() > 0)
			{
				completeQuery.append(" and t.id in (:ticketIds)");
				// filter.setAssignedUserId(null); // Shows from queue
			}

			List<Long> areaIds = new ArrayList<Long>();
			if (filter.getAreaId() != null && filter.getAreaId() > 0)
				areaIds.add(filter.getAreaId());
			else if (filter.getAreaIds() != null && filter.getAreaIds().size() > 0)
				areaIds.addAll(filter.getAreaIds());
			/*else if (filter.getBranchId() != null && filter.getBranchId() > 0)
				areaIds.addAll(LocationCache.getAreasByBranchId(filter
						.getBranchId() + "").keySet());

			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				Map<Long, String> areas = LocationCache.getAreasByCityId(filter
						.getCityId() + "");
				areaIds.addAll(areas.keySet());
			}*/
			
			// Remove areas if user is havving ticket id as filter
			if (filter.getTicketIds() != null
					&& filter.getTicketIds().size() > 0)
			{
				areaIds.clear();
				filter.setBranchId(null);
				filter.setCityId(null);
			}
			if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
			
			{
				 
					completeQuery.append(" and c.areaId in (:areaIds)");
			}
			else if(areaIds.size() > 0)
			{
				completeQuery.append(" and (  c.areaId in (:areaIds) or t.currentAssignedTo in(:reportingUsers)) ");
			}
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{	
				completeQuery.append(" and  c.branchId  in (:branchIds)   ");
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				completeQuery.append(" and  c.cityId  in (:cityIds) ");
			}

			{ // ticket b ased
				if (filter.getTicketType() != null
						&& filter.getTicketType() > 0)
					completeQuery.append(" and t.ticketCategoryId ="
							+ filter.getTicketType());

				if (filter.getPageContextId() != null
						&& filter.getPageContextId() == CC_PAGE)
				{
					completeQuery.append(" and t.status ="
							+ TicketStatus.PORT_ACTIVATION_SUCCESSFULL);
					completeQuery
							.append(" and HOUR(timediff(NOW(),t.modifiedOn) ) > "
									+ timeDiffenrenceForCC);
				}

				if (filter.getTicketStatus() != null
						&& filter.getTicketStatus() > 0)
				{
					completeQuery.append(" and t.status ="
							+ filter.getTicketStatus());
				}

			}
			if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
			{
				completeQuery.append(" and t.status in (:statusIds) ");
			}

			if (filter.getPriorityFilterValue() != null
					&& filter.getPriorityFilterValue() > 0)
			{

				PriorityValue priority = definitionCoreService
						.getPriorityDefinitionsFor(filter
								.getPriorityFilterValue());
				completeQuery.append(" and tpr.value >= "
						+ (priority.getStart() - PriortyUpdateHelper
								.getGlobalPriority().intValue()));
				completeQuery.append(" and tpr.value <= "
						+ (priority.getEnd() - PriortyUpdateHelper
								.getGlobalPriority().intValue()));

			}

			{ // Date filter
				if (filter.getFromDate() != null)
					completeQuery.append(" and wo.addedOn >= :fromDate");
				if (filter.getToDate() != null)
				{
					filter.setToDate(GenericUtil.addDays(filter.getToDate(), 1));
					completeQuery.append(" and wo.addedOn <= :toDate");
				}
			}

			{ // user based

				if (filter.getAssignedByUserId() != null)
					completeQuery.append(" and wo.assignedBy = "
							+ filter.getAssignedByUserId());
				if (filter.getAssignedUserId() != null)
					completeQuery.append(" and t.currentAssignedTo = "
							+ filter.getAssignedUserId());
			}
			{// fiber(1) and copper(2) type
				if (filter.getWorkStageType() != null
						&& filter.getWorkStageType() > 0)
					completeQuery.append(" and wo.currentWorkStage = "
							+ filter.getWorkStageType());
			}

			if (filter.getCustomerId() != null)
			{
				completeQuery.append(" and t.prospectNo = "
						+ filter.getCustomerId());
			}

			if (filter.getWorkorderNumber() != null)
			{
				completeQuery.append(" and wo.workOrderNumber = "
						+ "'"+filter.getWorkorderNumber()+ "'");
			}

			if (filter.getCustomerContactNumber() != null)
			{
				completeQuery.append(" and c.mobileNumber = "
						+ filter.getCustomerContactNumber());
			}
			
			if (filter.getMqId() != null)
			{
				completeQuery.append(" and c.mqId = "
						+ filter.getMqId());
			}
			
			appendCachingFilterQuery(filter, completeQuery );
			completeQuery.append(" and t.prospectNo is not null");
			completeQuery.append(" limit " + filter.getStartOffset() + " , "
					+ filter.getPageSize());

			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(completeQuery.toString());

			setCachingFilter(query, filter);
			
			if (filter.getFromDate() != null)
				query.setDate("fromDate", filter.getFromDate());
			if (filter.getToDate() != null)
				query.setDate("toDate", filter.getToDate());

			if (filter.getTicketIds() != null
					&& filter.getTicketIds().size() > 0)
			{
				query.setParameterList("ticketIds", filter.getTicketIds());
				logger.debug(" : ticketIds " + filter.getTicketIds());
			}

			if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
				
			{
				 
				query.setParameterList("areaIds", areaIds);
				logger.debug(" : areaIds " + areaIds);
			}
			else if(areaIds.size() > 0)
			{
				Map<Long, String> reportingUsers = userDao.getNestedReportingUsers(AuthUtils.getCurrentUserId(), null,null,null);
				query.setParameterList("areaIds", areaIds);
				
				if(reportingUsers == null || reportingUsers.size() == 0)
				{
					query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
				}
				else
				{
					query.setParameterList("reportingUsers", reportingUsers.keySet());
				}
			}
			
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{
				query.setParameterList("branchIds", Collections.singletonList(filter.getBranchId()));
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				query.setParameterList("cityIds",Collections.singletonList( filter.getCityId()));
			}
			logger.debug(" : branch  "
					+ Collections.singletonList(filter.getBranchId())
					+ " cityIds "
					+ Collections.singletonList(filter.getCityId()));
				
			if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
			{
				query.setParameterList("statusIds", filter
						.getTicketStatusList());
				logger.debug(" : statusIds " + filter.getTicketStatusList());
			}

			logger.debug(" completeQuery.toString() with out details "
					+ query.getQueryString());
			
			List<Object[]> result = query.list();

			if (result == null || result.isEmpty())
				return null;

			List<TicketDetailVo> tickets = new ArrayList<TicketDetailVo>();
			List<String> ticketIds = new ArrayList<String>();
			TicketDetailVo ticket;
			List<CustomerAddressVO> cavolist;
			ObjectMapper mapper = new ObjectMapper();

			for (Object[] obj : result)
			{
				cavolist = new ArrayList<CustomerAddressVO>();
				ticket=new TicketDetailVo();
				//ticket = new TicketDetailVo(objectToLong(obj[0]), objectToLong(obj[1]), objectToLong(obj[2]), objectToInt(obj[3]), objectToInt(obj[4]));
				ticket.setWorkOrderId(objectToLong(obj[0]));
				ticket.setWorkOrderTypeId(objectToLong(obj[1]));
				ticket.setStatusId(objectToLong(obj[2]));
				ticket.setPriority(objectToInt(obj[3]));
				logger.info("currentProgress for WO  :: "+ticket.getWorkOrderId() +" is :: "+objectToInt(obj[4]));
				
				ticket.setCurrentProgress(objectToInt(obj[4]));
				ticket.setSource((String) obj[5]);
				ticket.setCurrentEtr(GenericUtil.convertToUiDateFormat(obj[6]));

				ticket.setAccountNumber((String) obj[7]);
				ticket.setCustomerId(objectToLong(obj[8]) + "");

				ticket.setInitialWorkStageId(objectToLong(obj[10]));
				ticket.setId(objectToLong(obj[11])+"");

				if (obj[12] != null)
					ticket.setWorkOrderNumber((String) (obj[12]));
				else
					ticket.setWorkOrderNumber("--");

				ticket.setAreaId(objectToLong(obj[13]));
				ticket.setStatusId(objectToLong(obj[14]));
				ticket.setCafNumber((String) (obj[15]));
				ticket.setRemarks((String) (obj[16]));
				if (obj[17] != null)
					ticket.setRouterRequired((Boolean) (obj[17]));
				ticket.setRouterDescription((String) (obj[18]));

				if (obj[19] != null)
				{
					Long userId = objectToLong(obj[19]);
					ticket.setAssignedToId(userId);
					UserVo vo = userDao.getUserById(userId);

					if (vo != null)
					{
						ticket.setAssignedToDisplayName(GenericUtil.completeUserName(vo ));

						if(vo.getUserRoles() != null && !vo.getUserRoles().isEmpty())
						{
							ticket.setUserRoles(vo.getUserRoles());
						}
						
					}
					// ticket.setAssignedToDisplayName(userId+"");
				}

				if (obj[20] != null)
				{

					ticket.setCreatedDate(GenericUtil
							.convertToUiDateFormat(obj[20]));
				}

				if (obj[21] != null)
				{

					ticket.setProspectNo((String) obj[21]);
				}

				if (obj[22] != null)
				{

					ticket.setPreferedDate((GenericUtil
							.convertToDateFormat(obj[22])));
				}

				ticket.setBranchId(objectToLong(obj[23]));
				ticket.setCityId(objectToLong(obj[24]));
				ticket.setPlanId(objectToLong(obj[25]));

				
				ticket.setBranch(LocationCache.getBrancheNameById(ticket
						.getBranchId()));
				ticket.setCity(LocationCache.getCityNameById(ticket.getCityId()));
				// ticket.setPlanName(tariffDAO.getTariffNameById(ticket.getPlanId()));

				if (ticket.getAreaId() > 0)
				{
					ticket.setAreaName(LocationCache.getAreaNameById(ticket
							.getAreaId()));
				}
				
				// ***************Permanent Address **************************** 
				
				if(obj[26]!=null){
					
					String permanentAddress=obj[26].toString();
					
					if (CommonUtil.isValidJson(permanentAddress)) {

						CustomerAddressVO permanentAddressVo = mapper.readValue(permanentAddress,
								CustomerAddressVO.class);
						
						if (permanentAddressVo.getOperationalEntity() == null
								|| permanentAddressVo.getOperationalEntity().isEmpty()) {
							permanentAddressVo.setOperationalEntity(ticket.getBranch());
							permanentAddressVo.setOperationalEntityId(ticket.getBranchId());

						}
						
						if (permanentAddressVo.getCity() == null || permanentAddressVo.getCity().isEmpty()) {
							permanentAddressVo.setCity(ticket.getCity());
						}
						
						if (permanentAddressVo.getArea() == null || permanentAddressVo.getArea().isEmpty()) {
							permanentAddressVo.setArea(ticket.getAreaName());
							permanentAddressVo.setAreaId(ticket.getAreaId());
						}
						
						if (permanentAddressVo.getSubArea() == null
								|| permanentAddressVo.getSubArea().isEmpty() && ticket.getSubAreaId() != null
								&& ticket.getSubAreaId() != null) {
							permanentAddressVo.setSubArea(ticket.getSubAreaName());
							if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
								permanentAddressVo.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
						}
						cavolist.add(permanentAddressVo);

					}else {
						ticket.setPermanentAddress((String) (obj[26]));	
					}
				}

				ticket.setPincode(objectToInt(obj[27]) + "");

				if (obj[28] != null)
				{
					ticket.setTicketSubCategory((String) (obj[28]));
				}
				if (obj[29] != null)
				{
					ticket.setCustomerEmail((String) (obj[29]));
				}
				if (obj[30] != null)
				{
					ticket.setPlanName((String) (obj[30]));
				}

				ticket.setTicketSubCategoryId(objectToLong(obj[31]));
				ticket.setEscalatedValue(objectToInt(obj[32]));
				ticket.setPriorityEscalationType(objectToInt(obj[33]));
				ticket.setLattitude((String) (obj[34]));
				ticket.setLongitude((String) (obj[35]));
				ticket.setPriority(objectToInt(obj[36])
						+ PriortyUpdateHelper.getGlobalPriority().intValue());
				ticket.setPriorityEscalationType(objectToInt(obj[37]));
				ticket.setEscalatedValue(objectToInt(obj[38]));
				ticket.setUpdatedCount(objectToInt(obj[39]));
				ticket.setSliderEscalatedValue(objectToInt(obj[40]));

				// if ((String) (obj[41]) != null
				// && BooleanValue.FALSE_ID
				// .equalsIgnoreCase((String) (obj[41])))
				ticket.setCxPermission((String) (obj[41]));

				// else if ((String) (obj[41]) != null
				// && BooleanValue.TRUE_ID
				// .equalsIgnoreCase((String) (obj[41])))
				// ticket.setCxPermission(BooleanValue.TRUE);

				// if ((String) (obj[42]) != null
				// && BooleanValue.TRUE_ID
				// .equalsIgnoreCase((String) (obj[42])))
				ticket.setGxPermission((String) (obj[42]));
				//
				// else if ((String) (obj[42]) != null
				// && BooleanValue.FALSE_ID
				// .equalsIgnoreCase((String) (obj[42])))
				// ticket.setGxPermission(BooleanValue.FALSE);

				ticket.setPaymentRemark((String) (obj[43]));
				ticket.setDocumentRemark((String) (obj[44]));
				
				ticket.setMqStatus(objectToInt(obj[45]));
				ticket.setMqRetryCount(objectToInt(obj[46]));
				
				ticket.setNeComment((String) (obj[47]));
				ticket.setCommitedEtr(GenericUtil.convertToUiDateFormat(obj[48]));
				ticket.setSeComment((String) (obj[49]));
				
				if((objectToLong(obj[50])) != null && objectToLong(obj[50]) > 0)
				{
					UserVo user = userDao.getUserByEmpId(objectToLong(obj[50]));
					ticket.setAssignedBy(GenericUtil.completeUserName(user));
				}
				
				if(obj[51] != null)
					ticket.setAppointmentDate((String) (obj[51]));
				
			
				GISPojo gisPojo = gisDAO.getGisInfoByTicketId(Long.valueOf(ticket.getId()));

				logger.debug("Gis details are " + gisPojo);

				if (gisPojo != null)
				{
					ticket.setFxName(gisPojo.getFxName());
					ticket.setConnectionType(gisPojo.getConnectionType());
					ticket.setFxIpAddress(gisPojo.getFxIpAddress());
					ticket.setFxMacAddress(gisPojo.getFxMacAddress());
					ticket.setFxPorts(gisPojo.getFxPorts());
					ticket.setCxName(gisPojo.getCxName());
					ticket.setCxIpAddress(gisPojo.getCxIpAddress());
					ticket.setCxMacAddress(gisPojo.getCxMacAddress());
					ticket.setCxPorts(gisPojo.getCxPorts());
				
					if (ticket.getLattitude() != null
							&& gisPojo.getLattitude() != null)
						ticket.setLattitude(gisPojo.getLattitude());

					if (ticket.getLongitude() != null
							&& gisPojo.getLongitude() != null)
						ticket.setLongitude(gisPojo.getLongitude());
					
					if (gisPojo
							.getConnectionType() != null && WorkStageName.FIBER.equalsIgnoreCase(gisPojo
							.getConnectionType()))
					{
						ticket.setFiberFeasible(true);
					}
				}

				ticket.setFeasibilityTypeDetails(gisDAO
						.getGisTypeAndDetailsByTicketId(Long.valueOf(ticket.getId())));

				ticket.setBranch(LocationCache.getBrancheNameById(ticket
						.getBranchId()));
				ticket.setCity(LocationCache.getCityNameById(ticket.getCityId()));
				// ticket.setPlanName(tariffDAO.getTariffNameById(ticket.getPlanId()));

				if (ticket.getAreaId() > 0)
				{
					ticket.setAreaName(LocationCache.getAreaNameById(ticket
							.getAreaId()));
				}

					
				
				tickets.add(ticket);
				ticketIds.add(ticket.getId()+"");
				Long statusId = ticket.getStatusId();
				if (statusId != null)
					ticket.setStatus(TicketStatus.statusDisplayValue
							.get(statusId.intValue()));

				 
				ticket.setEditable(definitionCoreService
						.getEditableStatusFilterForCurrentUser().contains(Integer
								.valueOf(ticket.getStatusId() + "")));
				 
				
				WorkOrderTypeVO woType = WorkOrderDefinitionCache
						.getWorkOrderTypeVO(ticket.getWorkOrderTypeId());
				if (woType != null)
				{

					ticket.setWorkOrderType(woType.getWorkOrderType());

					/*if (filter.isIncludeCompleteStatus())
					{
						if (filter.getPageContextId() != null
								&& filter.getPageContextId() == 3)
							populateActivityLog(ticket, filter.getPageContextId(), filter);
						else
							populateActivityLog(ticket, IS_NOT_SALES, filter);
					}*/

				}
				CustomerVO customer = customerDAO.getCustomerById(Long
						.valueOf(ticket.getCustomerId()));
				ticket.setCustomerName(GenericUtil
						.completeCustomerName(customer));
				
				// *********************Current Address ***************************
				
				if(customer.getCurrentAddress()!=null){
					
					if(CommonUtil.isValidJson(customer.getCurrentAddress())){
						
						CustomerAddressVO currentAddressvo=mapper.readValue(customer.getCurrentAddress(), CustomerAddressVO.class);
						if (currentAddressvo.getOperationalEntity() == null
								|| currentAddressvo.getOperationalEntity().isEmpty()) {
							currentAddressvo.setOperationalEntity(ticket.getBranch());
							currentAddressvo.setOperationalEntityId(ticket.getBranchId());

						}
						
						if (currentAddressvo.getCity() == null || currentAddressvo.getCity().isEmpty()) {
							currentAddressvo.setCity(ticket.getCity());
						}
						
						if (currentAddressvo.getArea() == null || currentAddressvo.getArea().isEmpty()) {
							currentAddressvo.setArea(ticket.getAreaName());
							currentAddressvo.setAreaId(ticket.getAreaId());
						}
						
						if (currentAddressvo.getSubArea() == null || currentAddressvo.getSubArea().isEmpty()
								&& ticket.getSubAreaId() != null) {
							currentAddressvo.setSubArea(ticket.getSubAreaName());
							if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
								currentAddressvo.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
						}
						cavolist.add(currentAddressvo);
					}else{
						ticket.setCurrentAddress(customer.getCurrentAddress());
					}
					
				}
				
				ticket.setMobileNumber(customer.getMobileNumber());
				
				//***************** Communication Address *********************************
				
				if (customer.getCommunicationAdderss() != null) {
					if (CommonUtil.isValidJson(customer.getCommunicationAdderss())) {
						CustomerAddressVO communicationAddressVo = mapper.readValue(customer.getCommunicationAdderss(),
								CustomerAddressVO.class);
						if (communicationAddressVo.getOperationalEntity() == null
								|| communicationAddressVo.getOperationalEntity().isEmpty()) {
							communicationAddressVo.setOperationalEntity(ticket.getBranch());
							communicationAddressVo.setOperationalEntityId(ticket.getBranchId());

						}

						if (communicationAddressVo.getCity() == null || communicationAddressVo.getCity().isEmpty()) {
							communicationAddressVo.setCity(ticket.getCity());
						}

						if (communicationAddressVo.getArea() == null || communicationAddressVo.getArea().isEmpty()) {
							communicationAddressVo.setArea(ticket.getAreaName());
							communicationAddressVo.setAreaId(ticket.getAreaId());
						}

						if (communicationAddressVo.getSubArea() == null
								|| communicationAddressVo.getSubArea().isEmpty() && ticket.getSubAreaId() != null) {
							communicationAddressVo.setSubArea(ticket.getSubAreaName());
							if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
								communicationAddressVo.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
						}

						cavolist.add(communicationAddressVo);
					} else {
						ticket.setCommunicationAddress(customer.getCommunicationAdderss());
					}
				}
				
				ticket.setCustomerAddressesList(cavolist);
				
				if (ticket.getInitialWorkStageId() > 0)
				{
					WorkStageVo workStage = WorkOrderDefinitionCache
							.getWorkStageById(ticket.getInitialWorkStageId());
					if (workStage != null)
						ticket.setInitialWorkStage(workStage.getWorkStageName());
				} else
				{
					ticket.setInitialWorkStage("--");
				}

			}

			if (tickets != null)
				Collections.sort(tickets, new TicketPriorityComparator());
			// logger.info("#######################################result from
			// base query "
			// + tickets);

			/*
			 * for (Iterator iterator = ticket.getActivities().iterator();
			 * iterator .hasNext();) { ActivityVo activityVo = (ActivityVo)
			 * iterator.next(); if(activityVo.getId() == 9)
			 * 
			 * System.out.println(ticket.getId()+" >>>>>>>>>>>>>>>>>>>>>>> \n"
			 * +activityVo);
			 * 
			 * }
			 */
			
			if (filter.isIncludeCompleteStatus()  && !ticketIds.isEmpty())
			{
				List<Long> ticketIdsLong = new ArrayList<Long>();
				for (String str : ticketIds) {
					ticketIdsLong.add(Long.valueOf(str));
				}
				
				if (filter.getPageContextId() != null
						&& filter.getPageContextId() == 3)
				{
					populateActivityLogForTickets(tickets, ticketIdsLong, filter.getPageContextId(), filter) ;
				}
				else
				{
					populateActivityLogForTickets(tickets,ticketIdsLong,  IS_NOT_SALES, filter);
				}
			}
			logger.info("###################results for ni "+tickets.size());
			logger.debug("################### tickets detaisl value is :: "+tickets);
			return tickets;// CommonUtil.createPagedResultSet(tickets);

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception in Getting  tickets for workorder with filter "
					+ filter + " message " + e.getMessage());
		}
		logger.debug(" exit from  getTicketWithDetails  "  +filter.getMqId());
		return new ArrayList<TicketDetailVo>();

	}

	private void appendCachingFilterQuery(TicketFilter filter,StringBuilder query)
	{
		if(filter == null)
			return;
		
		logger.debug(" inside  appendCachingFilterQuery  " + filter.getMqId() );
		if(filter.getRequestKey() != null)
		{
			  query.append(" and (t.status not in (:closedStatus) or t.modifiedOn >=:lastModified )");
//					.append(" and t.modifiedOn >=:lastModified ");
			
		}
		 
	}
	private void setCachingFilter(Query q,TicketFilter filter)
	{
		if(filter == null)
			return;
		
		logger.debug(" inside  setCachingFilter  " +filter.getMqId() );
		if(filter.getRequestKey() != null)
		{
			
			 q.setParameterList("closedStatus",TicketStatus.CLOSED_TICKET_STATUS) ;
			 Date date = GenericUtil.getLastRequestTime(filter.getRequestKey());
			 q.setDate("lastModified", date);
		}
	}
	
	
	private void populateActivityLoga(TicketDetailVo ticket, int queryType,
			TicketFilter filter)
	{
		if(ticket == null || filter == null )
			return;
		
		logger.debug(" inside  populateActivityLoga  "  +ticket.getAreaName());
		Set<ActivityVo> activities = new HashSet<ActivityVo>();
		if (ticket.getWorkOrderTypeId() > 0)
		{

			if (ticket.getInitialWorkStageId() > 0)
			{
				List<ActivityVo> activityVos = WorkOrderDefinitionCache
						.getActivitiesForWorkStage(ticket
								.getInitialWorkStageId());
				if (activityVos != null)
					activities.addAll(activityVos);
			}

		} else if (queryType == IS_SALES
				|| filter.getPageContextId() == SEARCH_PAGE)
		{
			List<ActivityVo> activityVos = WorkOrderDefinitionCache
					.getActivitiesForWorkStage(WorkStageType.SALES);
			if (activityVos != null)
				activities.addAll(activityVos);
			queryType = IS_SALES;
		}

		List<ActivityVo> activityStatusVos = ticketActivityLoggingDAO
				.getActivitiesByWoNo(Long.valueOf(ticket.getId()));

		logger.debug("Activities for WorkOrderNumber "
				+ ticket.getWorkOrderNumber() + " is " + activityStatusVos);

		if (activityStatusVos != null && !activityStatusVos.isEmpty())
		{
			if (FWMPConstant.ANDRIOD_PAGE_CONTEXT.equals(filter
					.getPageContextId()))
			{
				for (Iterator<ActivityVo> iterator = activityStatusVos
						.iterator(); iterator.hasNext();)
				{
					ActivityVo activityVo = (ActivityVo) iterator.next();
					if (activityVo.getId() == Activity.PORT_ACTIVATION) // Port
																		// Activation
					{
						GISPojo gis = gisDAO.getGisInfoByTicketId(Long.valueOf(ticket
								.getId()));
						if (gis != null)
						{
							Set<SubActivityVo> sa = activityVo
									.getSubActivityList();
							if (sa != null)
							{
								for (Iterator iterator2 = sa.iterator(); iterator2
										.hasNext();)
								{
									SubActivityVo subActivityVo = (SubActivityVo) iterator2
											.next();
									if (subActivityVo.getId() == com.cupola.fwmp.FWMPConstant.SubActivity.CX_IP)
										subActivityVo.setValue(gis
												.getCxIpAddress());
									else if (subActivityVo.getId() == com.cupola.fwmp.FWMPConstant.SubActivity.CX_PORT)
										subActivityVo
												.setValue(gis.getCxPorts());

								}
							}

						}
					}

				}

			}

			if (queryType == IS_SALES || queryType == IS_NOT_SALES)
			{
				for (Iterator iterator = activityStatusVos.iterator(); iterator
						.hasNext();)
				{
					ActivityVo activityVo = (ActivityVo) iterator.next();

					if (ticket.getPaymentStatus()  > 0
							&& activityVo.getId() == Activity.PAYMENT_UPDATE)
					{
						activityVo.setStatus(ticket.getPaymentStatus());
						activityVo.setRemarks(ticket.getPaymentRemark());
					}
					if (ticket.getDocumentStatus() > 0
							&& activityVo.getId() == Activity.DOCUMENT_UPDATE)
					{
						activityVo.setStatus(ticket.getDocumentStatus());
						activityVo.setRemarks(ticket.getDocumentRemark());
					}

					if (queryType == IS_SALES
							&& !Activity.SALES_ACTIVITIES_LIST
									.contains(activityVo.getId()))
					{
						iterator.remove();
					} else if (queryType == IS_NOT_SALES
							&& Activity.SALES_ACTIVITIES_LIST
									.contains(activityVo.getId()))
					{
						iterator.remove();
					}
				}
			}
			Set<ActivityVo> c = new HashSet<ActivityVo>(activityStatusVos);
			// c.retainAll(activities);
			activities.removeAll(c);
			activities.addAll(c);
		}

		ticket.setActivities(new ArrayList(activities));

		Collections
				.sort(ticket.getActivities(), new ActivitySequenceComparator());

	}
	
	private void populateActivityLogForTickets(List<TicketDetailVo> tickets,List<Long>  ticketIds, int queryType,
			TicketFilter filter)
	{
		if(tickets == null || filter == null)
			return;
		
		logger.debug(" inside  populateActivityLogForTickets  " + filter.getMqId() );
		
		Map<Long, List<ActivityVo>> activityStatusVosAll = ticketActivityLoggingDAO
				.getActivitiesByTicketIds(   ticketIds);
		
		for(TicketDetailVo ticket : tickets)
		{
			Set<ActivityVo> activities = new HashSet<ActivityVo>();
			if (ticket.getWorkOrderTypeId() > 0)
			{

				if (ticket.getInitialWorkStageId() > 0)
				{
					List<ActivityVo> activityVos = WorkOrderDefinitionCache
							.getActivitiesForWorkStage(ticket
									.getInitialWorkStageId());
					if (activityVos != null)
						activities.addAll(activityVos);
				}

			} else if (queryType == IS_SALES
					|| filter.getPageContextId() == SEARCH_PAGE)
			{
				List<ActivityVo> activityVos = WorkOrderDefinitionCache
						.getActivitiesForWorkStage(WorkStageType.SALES);
				if (activityVos != null)
					activities.addAll(activityVos);
				queryType = IS_SALES;
			}
			
			List<ActivityVo> activityStatusVos = activityStatusVosAll.get(Long.valueOf(ticket.getId()));
			
			logger.debug("Activities for WorkOrderNumber "
					+ ticket.getWorkOrderNumber() + " is " + activityStatusVos);

			if (activityStatusVos != null && !activityStatusVos.isEmpty())
			{
				if (FWMPConstant.ANDRIOD_PAGE_CONTEXT.equals(filter
						.getPageContextId()))
				{
					for (Iterator<ActivityVo> iterator = activityStatusVos
							.iterator(); iterator.hasNext();)
					{
						ActivityVo activityVo = (ActivityVo) iterator.next();
						if (activityVo.getId() == Activity.PORT_ACTIVATION) // Port
																			// Activation
						{
							GISPojo gis = gisDAO.getGisInfoByTicketId(Long.valueOf(ticket
									.getId()));
							if (gis != null)
							{
								Set<SubActivityVo> sa = activityVo
										.getSubActivityList();
								if (sa != null)
								{
									for (Iterator iterator2 = sa.iterator(); iterator2
											.hasNext();)
									{
										SubActivityVo subActivityVo = (SubActivityVo) iterator2
												.next();
										if (subActivityVo.getId() == com.cupola.fwmp.FWMPConstant.SubActivity.CX_IP)
											subActivityVo.setValue(gis
													.getCxIpAddress());
										else if (subActivityVo.getId() == com.cupola.fwmp.FWMPConstant.SubActivity.CX_PORT)
											subActivityVo.setValue(gis
													.getCxPorts());

									}
								}

							}
						}

					}

				}

				if (queryType == IS_SALES || queryType == IS_NOT_SALES)
				{
					for (Iterator iterator = activityStatusVos.iterator(); iterator
							.hasNext();)
					{
						ActivityVo activityVo = (ActivityVo) iterator.next();

						if (ticket.getPaymentStatus() > 0
								&& activityVo.getId() == Activity.PAYMENT_UPDATE)
						{
							activityVo.setStatus(ticket.getPaymentStatus());
							activityVo.setRemarks(ticket.getPaymentRemark());
						}
						if (ticket.getDocumentStatus() > 0
								&& activityVo.getId() == Activity.DOCUMENT_UPDATE)
						{
							activityVo.setStatus(ticket.getDocumentStatus());
							activityVo.setRemarks(ticket.getDocumentRemark());
						}
						
						if (ticket.getPoaStatusId() > 0
								&& activityVo.getId() == Activity.POA_DOCUMENT_UPDATE)
						{
							activityVo.setStatus((int)ticket.getPoaStatusId());
							activityVo.setRemarks(ticket.getPoaDocumentRemark());
						}
						if (ticket.getPoiStatusId() > 0
								&& activityVo.getId() == Activity.POI_DOCUMENT_UPDATE)
						{
							activityVo.setStatus((int)ticket.getPoiStatusId());
							activityVo.setRemarks(ticket.getPoiDocumentRemark());
							logger.debug("poi activity status :: "+activityVo.getStatus());
						}

						if (queryType == IS_SALES
								&& !Activity.SALES_ACTIVITIES_LIST
										.contains(activityVo.getId()))
						{
							iterator.remove();
						} else if (queryType == IS_NOT_SALES
								&& Activity.SALES_ACTIVITIES_LIST
										.contains(activityVo.getId()))
						{
							iterator.remove();
						}
					}
				}
				Set<ActivityVo> c = new HashSet<ActivityVo>(activityStatusVos);
				// c.retainAll(activities);
				activities.removeAll(c);
				activities.addAll(c);
			}

			ticket.setActivities(new ArrayList(activities));

			Collections
					.sort(ticket.getActivities(), new ActivitySequenceComparator());
		}
	}
	@Override
	public List<TicketDetailVo> getPreSaleTicketWithDetails(TicketFilter filter)
	{
		if (filter == null)
			filter = new TicketFilter();

		logger.debug(" inside  getPreSaleTicketWithDetails  " + filter.getMqId());
		try
		{

			StringBuilder completeQuery = new StringBuilder(TICKET_DETAIL_PRE_SALE_QUERY);
			 

			if (filter.getPageSize() > 0)
				;
			else
			{
				filter.setStartOffset(0);
				filter.setPageSize(MIN_ITEM_PER_PAGE);
			}

			if (filter.getTicketId() != null && filter.getTicketId() > 0)
				completeQuery.append(" and t.id =" + filter.getTicketId());

			if (filter.getProspectType() != null
					&& filter.getProspectType() > 0)
				completeQuery.append(" and c.status="
						+ filter.getProspectType());

			if (filter.getTicketIds() != null
					&& !filter.getTicketIds().isEmpty()
					&& filter.getTicketIds().size() > 0)
			{
				completeQuery.append(" and t.id in (:ticketIds)");
				filter.setAssignedUserId(null); // Shows from queue
			}

			if (filter.getAssignedUserId() != null)
				completeQuery.append(" and t.currentAssignedTo = "
						+ filter.getAssignedUserId());

			// Ignore tickets with not payment info

			if (AuthUtils.isCFEUser())
			{
				if (DefinitionCoreService.PAYMENT_RELATED_STATUS.equals(filter
						.getPageContextId())
						|| DefinitionCoreService.REFUND_RELATED_STATUS
								.equals(filter.getPageContextId()))
					completeQuery
							.append(" and t.paymentStatus in (:paymentStatusList) ");
				else if (DefinitionCoreService.DOCUMENT_RELATED_STATUS
						.equals(filter.getPageContextId()))
					completeQuery
							.append(" and t.documentStatus in (:documentStatusList)  ");
				
				else if (DefinitionCoreService.MQ_FAILURE_RELATED_STATUS .equals(filter
						.getPageContextId()))
				{
					completeQuery
					.append(" and t.mqStatus = 0 and t.paymentStatus is not null and t.documentStatus is not null ");
				}
				
			}

			else if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
				completeQuery.append(" and t.status in (:ticketStatusList)");

			if (filter.getTicketType() != null && filter.getTicketType() > 0)
				completeQuery.append(" and t.ticketCategory="
						+ filter.getTicketType());

			{ // Date filter
				if (filter.getFromDate() != null)
					completeQuery.append(" and t.addedOn >= :fromDate");
				if (filter.getToDate() != null)

				{
					filter.setToDate(GenericUtil.addDays(filter.getToDate(), 1));
					completeQuery.append(" and t.addedOn <= :toDate");
				}
			}

			List<Long> areaIds = new ArrayList<Long>();
			
			 if (filter.getAreaId() != null && filter.getAreaId() > 0)
				areaIds.add(filter.getAreaId());
			else if (filter.getAreaIds() != null && filter.getAreaIds().size() > 0)
				areaIds.addAll(filter.getAreaIds());
			 
			
			// Remove areas if user is havving ticket id as filter
			if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0)
				areaIds.clear();
			
			if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
				
			{
					completeQuery.append(" and c.areaId in (:areaIds)");
			}
			else if(areaIds.size() > 0)
			{
				completeQuery.append(" and (  c.areaId in (:areaIds) or t.currentAssignedTo in(:reportingUsers)) ");
			}
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{	
				completeQuery.append(" and  c.branchId  in (:branchIds)   ");
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				completeQuery.append(" and  c.cityId  in (:cityIds) ");
			}
			 

			if (filter.getTicketStatus() != null
					&& filter.getTicketStatus() > 0 && !AuthUtils.isCFEUser())
			{
				completeQuery.append(" and t.status ="
						+ filter.getTicketStatus());
			}

			if (filter.getCustomerId() != null)
			{
				completeQuery.append(" and t.prospectNo = "
						+ filter.getCustomerId());
			}
			if (filter.getCustomerContactNumber() != null)
			{
				completeQuery.append(" and c.mobileNumber = "
						+ filter.getCustomerContactNumber());
			}
			appendCachingFilterQuery(filter, completeQuery );
			completeQuery
					.append(" and t.prospectNo is not null order by t.modifiedOn Desc ");
			completeQuery.append(" limit " + filter.getStartOffset() + " , "
					+ filter.getPageSize());

			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(completeQuery.toString());

			setCachingFilter(query, filter);

			if (filter.getFromDate() != null)
				query.setDate("fromDate", filter.getFromDate());
			if (filter.getToDate() != null)
				query.setDate("toDate", filter.getToDate());

			if (filter.getTicketIds() != null
					&& filter.getTicketIds().size() > 0)
				query.setParameterList("ticketIds", filter.getTicketIds());

			if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
				
			{
				 
				query.setParameterList("areaIds", areaIds);
				logger.debug(" : areaIds " + areaIds);
			}
			else if(areaIds.size() > 0)
			{
				Map<Long, String> reportingUsers = userDao.getNestedReportingUsers(AuthUtils.getCurrentUserId(), null,null,null);
				query.setParameterList("areaIds", areaIds);
				
				if(reportingUsers == null || reportingUsers.size() == 0)
				{
					query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
				}
				else
				{
					query.setParameterList("reportingUsers", reportingUsers.keySet());
				}
			}
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{
				query.setParameterList("branchIds", Collections.singletonList(filter.getBranchId()));
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				query.setParameterList("cityIds",Collections.singletonList( filter.getCityId()));
			}
			

			if (AuthUtils.isCFEUser())
			{
				// filter.getTicketStatus() > 0
				if (DefinitionCoreService.PAYMENT_RELATED_STATUS.equals(filter
						.getPageContextId()))
				{
					if (filter.getTicketStatus() != null
							&& filter.getTicketStatus() > 0)
					{
						query.setParameterList("paymentStatusList", Collections
								.singletonList(filter.getTicketStatus()));

					} else
					{
						query.setParameterList("paymentStatusList", definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER));

					}
				}
				// completeQuery.append(" and t.paymentStatus in (:paymentStatusList) ");
				else if (DefinitionCoreService.DOCUMENT_RELATED_STATUS
						.equals(filter.getPageContextId()))
				{
					if (filter.getTicketStatus() != null
							&& filter.getTicketStatus() > 0)
					{
						query.setParameterList("documentStatusList", Collections
								.singletonList(filter.getTicketStatus()));
					} else
					{
						query.setParameterList("documentStatusList", definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER));

					}

				}

				// completeQuery.append(" and t.paymentStatus in (:paymentStatusList) ");
				else if (DefinitionCoreService.REFUND_RELATED_STATUS
						.equals(filter.getPageContextId()))
				{
					if (filter.getTicketStatus() != null
							&& filter.getTicketStatus() > 0)
					{
						query.setParameterList("paymentStatusList", Collections
								.singletonList(filter.getTicketStatus()));
					} else
					{
						query.setParameterList("paymentStatusList", definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_REFUND_STATUS_FILTER));

					}

				}

			}

			else if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
				query.setParameterList("ticketStatusList", filter
						.getTicketStatusList());

			logger.debug("completeQuery.toString() presale "
					+ query.getQueryString());

			List<Object[]> result = query.list();

			if (result == null)
				return null;

			List<TicketDetailVo> tickets = new ArrayList<TicketDetailVo>();
			List<Long> ticketIds = new ArrayList<Long>();
			TicketDetailVo ticket;

			List<CustomerAddressVO> cavolist;
			ObjectMapper mapper = new ObjectMapper();

			for (Object[] obj : result)
			{
				ticket = new TicketDetailVo();
				cavolist = new ArrayList<CustomerAddressVO>();
				ticket.setId(objectToLong(obj[0])+"");
				ticket.setProspectNo((String) obj[1]);

				ticket.setSource((String) obj[2]);
				// ticket.setCommitedEtr(GenericUtil.convertToDateFormat(obj[3]));
				if (obj[3] != null)
					ticket.setCurrentEtr(GenericUtil.convertToUiDateFormat(obj[3]));
				ticket.setAccountNumber((String) obj[4]);
				ticket.setCustomerId(objectToLong(obj[5]) + "");

				CustomerVO customer = customerDAO.getCustomerById(Long
						.valueOf(ticket.getCustomerId()));
				
				if (customer != null)
				{
					ticket.setCustomerName(GenericUtil
							.completeCustomerName(customer));
					ticket.setTitle(customer.getTitle());
					ticket.setFirstName(customer.getFirstName());
					ticket.setLastName(customer.getLastName());
					ticket.setMiddleName(customer.getMiddleName());
				}
				
				ticket.setMobileNumber((String) obj[7]);
				
				String communicationAddress = null;
				if(obj[8] != null)
				{
					logger.debug("FWMP_ADDRESS_LIST_RELEASE_DATE " + FWMP_ADDRESS_LIST_RELEASE_DATE);
					
					communicationAddress = (String) obj[8];

				}
				ticket.setCreatedDate(GenericUtil.convertToDateFormat(obj[9]));
				ticket.setAreaId(objectToLong(obj[10]));
				ticket.setStatusId(objectToLong(obj[11]));

				if (obj[11] != null)
				{
					int status = (int) obj[11];

					logger.debug("\n\nTicket is having status as " + status);

					// if (status > 0
					// && status == TicketStatus.FEASIBILITY_GRANTED_BY_NE)
					// {
					GISPojo gisPojo = gisDAO.getGisInfoByTicketId(Long.valueOf(ticket
							.getId()));

					logger.debug("###### Gis details are In presale ticket details "
							+ gisPojo);

					ticket.setLattitude((String) (obj[44]));
					ticket.setLongitude((String) (obj[45]));
					
					if (gisPojo != null)
					{
						ticket.setFxName(gisPojo.getFxName());
						ticket.setConnectionType(gisPojo.getConnectionType());
						ticket.setFxIpAddress(gisPojo.getFxIpAddress());
						ticket.setFxMacAddress(gisPojo.getFxMacAddress());
						ticket.setFxPorts(gisPojo.getFxPorts());
						ticket.setCxName(gisPojo.getCxName());
						ticket.setCxIpAddress(gisPojo.getCxIpAddress());
						ticket.setCxMacAddress(gisPojo.getCxMacAddress());
						ticket.setCxPorts(gisPojo.getCxPorts());
					
						if (ticket.getLattitude() != null
								&& gisPojo.getLattitude() != null)
							ticket.setLattitude(gisPojo.getLattitude());

						if (ticket.getLongitude() != null
								&& gisPojo.getLongitude() != null)
							ticket.setLongitude(gisPojo.getLongitude());

						if (gisPojo.getConnectionType() != null && WorkStageName.FIBER
								.equalsIgnoreCase(gisPojo.getConnectionType()))
						{
							ticket.setFiberFeasible(true);
						}
					}
					// } else
					// {
					// logger.debug("\n\nTicket is having status as less than zero"
					// + status);
					// }
				}

				ticket.setFeasibilityTypeDetails(gisDAO
						.getGisTypeAndDetailsByTicketId(Long.valueOf(ticket.getId())));

				ticket.setCafNumber((String) (obj[12]));
				ticket.setRemarks((String) (obj[13]));

				if (obj[14] != null)
					ticket.setRouterRequired((Boolean) (obj[14]));
				ticket.setRouterDescription((String) (obj[15]));

				if (obj[16] != null)
				{
					Long userId = objectToLong(obj[16]);
					ticket.setAssignedToId(userId);
					UserVo vo = userDao.getUserById(userId);
					if (vo != null)
						ticket.setAssignedToDisplayName(GenericUtil.completeUserName(vo ));

				}

				if (obj[17] != null)
				{

					ticket.setCreatedDate(GenericUtil
							.convertToUiDateFormat(obj[17]));
				}

				if (obj[18] != null)
				{

					ticket.setPreferedDate((GenericUtil
							.convertToDateFormat(obj[18])));
				}
				if (obj[19] != null)
				{
					ticket.setWorkOrderType((String) (obj[19]));
				}

				ticket.setBranchId(objectToLong(obj[20]));

				ticket.setCityId(objectToLong(obj[21]));
				ticket.setPlanId(objectToLong(obj[22]));
				String permanentAddress = null;
				if(obj[23] != null)
				{
					permanentAddress = (String) (obj[23]);

				}
				
//				String[] permanentAddArray=currentAddress.split(AddressLinesSeparator.ADDRESS_SEPARATOR);
//				logger.info("permanentAddArray is ::"+permanentAddArray.length);
//				CustomerAddressVO cavo = null;
//				for(int i = 0;i< permanentAddArray.length;i++)
//				{
//					if(i == 0)
//					{
//						cavo=new CustomerAddressVO();
//						cavo.setAddress1(permanentAddArray[0]);
//					} 
//				cavo.setAddress2(permanentAddArray[1]);
//				cavo.setLandmark(permanentAddArray[2]);
//				cavo.setPincode(permanentAddArray[3]);
//				}
//				logger.info("Customer address ::"+cavo);
				
				//ticket.setCustomerPermanentAddress((String) (obj[23]));

				ticket.setPincode(objectToInt(obj[24]) + "");

				Long paymentId = objectToLong(obj[25]);
			
				if (paymentId > 0)
				{
					Payment pay = hibernateTemplate
							.get(Payment.class, paymentId);
					
					ticket.setPaymentRefNumber(pay.getReferenceNo());
					ticket.setPaidAmount(pay.getPaidAmount());
					ticket.setPaymentMode(pay.getPaymentMode());
					ticket.setInstallationCharges(pay.getInstallationCharges());
					
					if (pay.getSchemeCode() != null)
						ticket.setSchemeCode(pay.getSchemeCode());
					
					if(pay.getDiscountAmount() != null)
						ticket.setDiscountAmount(pay.getDiscountAmount());
					
//					System.out.println("pay.getPaidConnection() " + pay.getPaidConnection() + " Ticket " + ticket.getId());

					if (null != pay.getPaidConnection()
							&& !"null".equalsIgnoreCase(pay.getPaidConnection()))
					{
//						System.out.println("Inside ");
						if (BooleanValue.FALSE
								.equalsIgnoreCase(pay.getPaidConnection()))
						{
							ticket.setPaidConnection(BooleanValue.FALSE);
//							System.out.println("Free");
						}
						else
							ticket.setPaidConnection(BooleanValue.TRUE);
					} else
					{
						ticket.setPaidConnection(BooleanValue.TRUE);
					}
					
//					System.out.println("pay.getPaidConnection() " + ticket.getPaidConnection() + " Ticket " + ticket.getId());

					
				} else
				{
					ticket.setPaymentRefNumber("N/A");
					ticket.setPaidConnection(BooleanValue.TRUE);
				}

				if (obj[26] != null)
				{
					ticket.setTicketSubCategory((String) (obj[26]));
				}

				if (obj[27] != null)
				{
					ticket.setCustomerEmail((String) (obj[27]));
				}
				if (obj[28] != null)
				{
					ticket.setEnquiryModeId((String) (obj[28]));
				}
				if (obj[29] != null)
				{
					ticket.setSourceModeId((String) (obj[29]));
				}
				if (obj[30] != null)
				{
					
					ticket.setProspectColdReason((String) (obj[30]));
				}

				if (obj[31] != null)
				{
					if ((int) (obj[31]) == FWMPConstant.ProspectType.COLD_ID)
						ticket.setProspectType(FWMPConstant.ProspectType.COLD);

					if ((int) (obj[31]) == FWMPConstant.ProspectType.HOT_ID)
						ticket.setProspectType(FWMPConstant.ProspectType.HOT);

					if ((int) (obj[31]) == FWMPConstant.ProspectType.MEDIUM_ID)
						ticket.setProspectType(FWMPConstant.ProspectType.MEDIUM);
				}

				ticket.setTicketSubCategoryId(objectToLong(obj[32]));

				if (obj[33] != null)
					ticket.setPaymentStatus(objectToInt(obj[33]));

				if (obj[34] != null)
					ticket.setDocumentStatus(objectToInt(obj[34]));

				ticket.setCxPermission((String) (obj[35]));
				ticket.setGxPermission((String) (obj[36]));

				ticket.setCustomerAltMobile((String) (obj[37]));
				ticket.setOfficeNumber((String) (obj[38]));
				ticket.setCustomerProfile((String) (obj[39]));
				ticket.setCustomerAltEmail((String) (obj[40]));

				if (obj[41] != null && (Byte) (obj[41]) == 1)
					ticket.setExternalRouterRequired(true);
				else
					ticket.setExternalRouterRequired(false);

				ticket.setExternalRouterDescription((String) (obj[42]));
				ticket.setExternalRouterAmount((String) (obj[43]));

				ticket.setPriority(objectToInt(obj[46])
						+ PriortyUpdateHelper.getGlobalPriority().intValue());
				ticket.setPriorityEscalationType(objectToInt(obj[47]));
				ticket.setEscalatedValue(objectToInt(obj[48]));
				ticket.setUpdatedCount(objectToInt(obj[49]));
				

//				ticket.setCustomerAddress((obj[50]).toString());
				String currentAddress = null;
				if(obj[50] != null)
				{
					currentAddress = (obj[50]).toString();
					
				}
				
				ticket.setPaymentRemark((String) (obj[51]));
				ticket.setDocumentRemark((String) (obj[52]));

				ticket.setMqStatus(objectToInt(obj[53]));
				ticket.setMqRetryCount(objectToInt(obj[54]));
				
				ticket.setNeComment((String) (obj[55]));
				ticket.setCommitedEtr(GenericUtil.convertToUiDateFormat(obj[56]));
				

				if((objectToLong(obj[57])) != null && objectToLong(obj[57]) > 0)
				{
					UserVo user = userDao.getUserByEmpId(objectToLong(obj[57]));
					ticket.setAssignedBy(GenericUtil.completeUserName(user));
				}
				
				if((objectToLong(obj[58])) != null && objectToLong(obj[58]) == Long.valueOf(Status.ACTIVE))
				{
					ticket.setAadhaarForPOI(true);
				} else
					ticket.setAadhaarForPOI(false);
				
				if((objectToLong(obj[59])) != null && objectToLong(obj[59]) == Long.valueOf(Status.ACTIVE))
				{
					ticket.setAadhaarForPOA(true);
					
				} else
					ticket.setAadhaarForPOA(false);
				
				if(obj[60] != null )
					ticket.setPoiStatusId(objectToLong(obj[60]));
				
				if(obj[61] != null )
					ticket.setPoaStatusId(objectToLong(obj[61]));
				
				if(obj[62] != null )
					ticket.setPoaRemarks((String)obj[62]);
				
				if(obj[63] != null )
					ticket.setPoiRemarks((String)obj[63]);
				
				if(obj[64] != null )
					ticket.setPoaDocumentRemark((String)obj[64]);
				
				if(obj[65] != null )
					ticket.setPoiDocumentRemark((String)obj[65]);
			 
				if(obj[64] != null )
					ticket.setPoaDocumentRemark((String)obj[64]);
				
				if(obj[65] != null )
					ticket.setPoiDocumentRemark((String)obj[65]);
				
				if (obj[66] != null)
					ticket.setCrpAccountNumber((String) obj[66]);
				
				if (obj[67] != null)
					ticket.setCrpMobileNumber((String) obj[67]);
				
				if(obj[68]!=null && (Byte)(obj[68]) == 1){
					ticket.setIsStaticIPRequired(true);
				}else{
					ticket.setIsStaticIPRequired(false);
				}
				
				if (obj[69] != null)
					ticket.setExistingIsp((String) obj[69]);
				if (obj[70] != null)
					ticket.setNationality((String) obj[70]);
			 
				if(obj[71] != null)
				{
					ticket.setSubAreaId(obj[71].toString());
					ticket.setSubAreaName(LocationCache.getSubAreasBySubAreaId(objectToLong(obj[71])));
			
				}
				if(obj[72]!=null){
					ticket.setUinType(obj[72].toString());
				}
				if(obj[73]!=null){
					ticket.setUin(obj[73].toString());
				}
				
				ticket.setDeDupFlag(ticket.getMqStatus());

//				System.out.println("ticket.getMqStatus() " + ticket.getMqStatus() + "ticket.getDeDupFlag()"
//						+ ticket.getDeDupFlag());

				if (ticket.getMqStatus() > 0
						&& ticket.getMqStatus() == DeDupFlag.deDupFlagSuccess)
				{
					ticket.setDeDupMessage(DeDupFlag.cleanProspectString);
					ticket.setDeDupFlag(DeDupFlag.deDupFlagSuccess);

				}
				
				if (ticket.getMqStatus() > 0
						&& ticket.getMqStatus() == DeDupFlag.deDupFlagFailure)
				{	
					ticket.setDeDupMessage(DeDupFlag.duplicatedProspectString);
					ticket.setDeDupFlag(DeDupFlag.deDupFlagFailure);
				}
				ticket.setBranch(LocationCache.getBrancheNameById(ticket
						.getBranchId()));

				ticket.setCity(LocationCache.getCityNameById(ticket.getCityId()));
				ticket.setPlanName(tariffDAO.getTariffNameById(ticket
						.getPlanId()));

				
				if (FWMPConstant.ANDRIOD_PAGE_CONTEXT.equals(filter
						.getPageContextId()))
				{
					if (ticket.getPlanId() >= 0)
					{
						TariffPlanVo tariffPlanVo = tariffDAO
								.getTariffById(ticket.getPlanId());

						if (tariffPlanVo != null)
						{
							ticket.setPlanName(tariffPlanVo.getPackageName());
							ticket.setPlanId(Long.valueOf(tariffPlanVo.getId()));
							ticket.setPlanType(tariffPlanVo.getPlanType());
							ticket.setPlanCategoery(tariffPlanVo
									.getPlanCategoery());
							ticket.setPlanAmount(tariffPlanVo
									.getOfferPriceWithTax());
							ticket.setStaticIpPriceWithTax(tariffPlanVo.getStaticIpPriceWithTax());
//							ticket.setInstallationCharges(tariffPlanVo
//									.getInstallChargesWithTax());
						}
					}
				}
				if (ticket.getAreaId() > 0)
				{
					ticket.setAreaName(LocationCache.getAreaNameById((ticket
							.getAreaId())));
				}
				/*if (filter.isIncludeCompleteStatus())
				{
					if (filter.getPageContextId() != null
							&& filter.getPageContextId() == SEARCH_PAGE)
						populateActivityLog(ticket, filter.getPageContextId(), filter);
					else
						populateActivityLog(ticket, IS_SALES, filter);
				}*/
				logger.debug("ticket details complete information for app/web:"+ticket);
				
				
				
				
				if (communicationAddress != null) {
					
					logger.debug("communicationAddress " + communicationAddress);
					
					if(CommonUtil.isValidJson(communicationAddress))
					{
						CustomerAddressVO communicationAddresstoSet = mapper.readValue(communicationAddress,
								CustomerAddressVO.class);
						
						if (communicationAddresstoSet.getOperationalEntity() == null
								|| communicationAddresstoSet.getOperationalEntity().isEmpty()) {
							communicationAddresstoSet.setOperationalEntity(ticket.getBranch());
							communicationAddresstoSet.setOperationalEntityId(ticket.getBranchId());

						}

						if (communicationAddresstoSet.getCity() == null || communicationAddresstoSet.getCity().isEmpty()) {
							communicationAddresstoSet.setCity(ticket.getCity());
						}

						if (communicationAddresstoSet.getArea() == null || communicationAddresstoSet.getArea().isEmpty()) {
							communicationAddresstoSet.setArea(ticket.getAreaName());
							communicationAddresstoSet.setAreaId(ticket.getAreaId());
						}

						if (communicationAddresstoSet.getSubArea() == null
								|| communicationAddresstoSet.getSubArea().isEmpty() && ticket.getSubAreaId() != null) {
							communicationAddresstoSet.setSubArea(ticket.getSubAreaName());
							if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty() && ticket.getSubAreaId() != null)
								communicationAddresstoSet.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
						}

						
						cavolist.add(communicationAddresstoSet);
						
					} else
					{
						ticket.setCommunicationAddress(communicationAddress);
					}
					
				}
				
				if (currentAddress != null) {
					logger.debug("currentAddress " + currentAddress);

					if(CommonUtil.isValidJson(currentAddress))
					{
						CustomerAddressVO currentAddressVo = mapper.readValue(currentAddress,
								CustomerAddressVO.class);
						
						if (currentAddressVo.getOperationalEntity() == null
								|| currentAddressVo.getOperationalEntity().isEmpty()) {
							currentAddressVo.setOperationalEntity(ticket.getBranch());
							currentAddressVo.setOperationalEntityId(ticket.getBranchId());

						}

						if (currentAddressVo.getCity() == null || currentAddressVo.getCity().isEmpty()) {
							currentAddressVo.setCity(ticket.getCity());
						}

						if (currentAddressVo.getArea() == null || currentAddressVo.getArea().isEmpty()) {
							currentAddressVo.setArea(ticket.getAreaName());
							currentAddressVo.setAreaId(ticket.getAreaId());
						}

						if (currentAddressVo.getSubArea() == null
								|| currentAddressVo.getSubArea().isEmpty() && ticket.getSubAreaId() != null) {
							currentAddressVo.setSubArea(ticket.getSubAreaName());
							if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
								currentAddressVo.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
						}

						
						cavolist.add(currentAddressVo);
						
					} else
					{
						ticket.setCurrentAddress(currentAddress);
					}
					
				}
				

				if (permanentAddress != null) {
					
					logger.debug("permanentAddress " + permanentAddress);
					
					if(CommonUtil.isValidJson(permanentAddress))
					{
						CustomerAddressVO permanentAddressVo = mapper.readValue(permanentAddress,
								CustomerAddressVO.class);
						
						if (permanentAddressVo.getOperationalEntity() == null
								|| permanentAddressVo.getOperationalEntity().isEmpty()) {
							permanentAddressVo.setOperationalEntity(ticket.getBranch());
							permanentAddressVo.setOperationalEntityId(ticket.getBranchId());

						}

						if (permanentAddressVo.getCity() == null || permanentAddressVo.getCity().isEmpty()) {
							permanentAddressVo.setCity(ticket.getCity());
						}

						if (permanentAddressVo.getArea() == null || permanentAddressVo.getArea().isEmpty()) {
							permanentAddressVo.setArea(ticket.getAreaName());
							permanentAddressVo.setAreaId(ticket.getAreaId());
						}

						if (permanentAddressVo.getSubArea() == null
								|| permanentAddressVo.getSubArea().isEmpty() && ticket.getSubAreaId() != null) {
							permanentAddressVo.setSubArea(ticket.getSubAreaName());
							if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
								permanentAddressVo.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
						}

						cavolist.add(permanentAddressVo);
						
					} else
					{
						ticket.setPermanentAddress(permanentAddress);
					}
				}
				
				ticket.setCustomerAddressesList(cavolist);
				
				logger.debug("Address list for ticket " + ticket.getId() + " is " + cavolist);
				
				tickets.add(ticket);
				ticketIds.add(Long.valueOf(ticket.getId()));
				Long statusId = ticket.getStatusId();
				if (statusId != null)
				{
					ticket.setStatus(TicketStatus.statusDisplayValue
							.get(statusId.intValue()));

					if (AuthUtils.isCFEUser())
					{
						if (ticket.getPaymentStatus() > 0
								&& DefinitionCoreService.PAYMENT_RELATED_STATUS
										.equals(filter.getPageContextId()))
						{
							ticket.setStatus(TicketStatus.statusDisplayValue
									.get(ticket.getPaymentStatus()));
						}
						// completeQuery.append(" and t.paymentStatus in (:paymentStatusList) ");
						else if (ticket.getDocumentStatus() > 0
								&& DefinitionCoreService.DOCUMENT_RELATED_STATUS
										.equals(filter.getPageContextId()))
						{
							ticket.setStatus(TicketStatus.statusDisplayValue
									.get(ticket.getDocumentStatus()));
						}
					}
					else
					{
						ticket.setEditable(definitionCoreService
								.getEditableStatusFilterForCurrentUser().contains(Integer
										.valueOf(ticket.getStatusId() + "")));
					}
				}

			}
			if (filter.isIncludeCompleteStatus() && !ticketIds.isEmpty())
			{
				if (filter.getPageContextId() != null
						&& filter.getPageContextId() == 3)
					populateActivityLogForTickets(tickets, ticketIds, filter.getPageContextId(), filter) ;
				else
					populateActivityLogForTickets(tickets,ticketIds,  IS_SALES, filter);
			}
			// if (tickets != null)
			// Collections.sort(tickets, new TicketPriorityComparator());
			logger.info("############## results for Sales "
					+ tickets.size());
			logger.debug("ticket details arraylist for complete:"+tickets);
			return tickets;// CommonUtil.createPagedResultSet(tickets);

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception in Getting  tickets for Sales with filter "
					+ filter + " message " + e.getMessage());
		}
		logger.debug(" exit from  getPreSaleTicketWithDetails  " + filter.getMqId());
		return new ArrayList<TicketDetailVo>();

	}
	
	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}

	@Override
	public String updateTicketStatus(StatusVO status)
	{
		String statusMsg = null;

		logger.info("StatusVO for updating status " + status);

		try
		{

			if (status.getTicketList() != null
					&& status.getTicketList().size() > 0)
			{
				int result = hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Update  Ticket t set t.status=:status,  t.remarks=:remark, t.reason=:reason ,t.modifiedOn=:modifiedOn where t.id in (:ids)")
						.setParameter("status", status.getStatus())
						.setParameter("reason", status.getReasonCode() + "")
						.setParameter("remark", GenericUtil.formatRemarks(status
								.getRemarks(), status.getRemarks(), status
								.getAddedByUserName()))
						.setParameter("modifiedOn", new Date())
						.setParameterList("ids", status.getTicketList())
						.executeUpdate();

				return StatusMessages.UPDATED_SUCCESSFULLY;

			} else if (status.getPropspectNoList() != null
					&& status.getPropspectNoList().size() > 0)
			{

				int result = hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Update  Ticket t set t.status=:status,  t.remarks=:remark, t.reason=:reason ,t.modifiedOn=:modifiedOn where t.prospectNo in (:prospectNos)")
						.setParameter("status", status.getStatus())
						.setParameter("reason", status.getReasonCode() + "")
						.setParameter("remark", GenericUtil.formatRemarks(status
								.getRemarks(), status.getRemarks(), status
								.getAddedByUserName()))
						.setParameter("modifiedOn", new Date())
						.setParameterList("prospectNos", status.getPropspectNoList())
						.executeUpdate();

				return StatusMessages.UPDATED_SUCCESSFULLY;

			} else if (status.getPropspectNo() != null)
			{

				int result = hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Update  Ticket t set t.status=:status,  t.remarks=:remark, t.reason=:reason where t.prospectNo=:prospectNos")
						.setParameter("status", status.getStatus())
						.setParameter("reason", status.getReasonCode() + "")
						.setParameter("remark", GenericUtil.formatRemarks(status
								.getRemarks(), status.getRemarks(), status
								.getAddedByUserName()))
						.setParameter("prospectNos", status.getPropspectNo())
						.executeUpdate();

				logger.debug("Prospect " + status.getPropspectNo()
						+ " status updated successfully");
				statusMsg = StatusMessages.UPDATED_SUCCESSFULLY;
				return statusMsg;
			} else
			{

				Ticket ticket = hibernateTemplate.get(Ticket.class, status
						.getTicketId());
				if (ticket == null)
					statusMsg = StatusMessages.NO_RECORD_FOUND;

				ticket = addCommonInitialValues(ticket);
				ticket.setStatus(status.getStatus());
				ticket.setRemarks(GenericUtil.formatRemarks(ticket.getRemarks(), status
						.getRemarks(), status.getAddedByUserName()));
				if (status.getReasonCode() > 0)
					ticket.setReason(status.getReasonCode() + "");

				if (definitionCoreService
						.getStatusFilterForRole(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER)
						.contains(status.getStatus())
						|| definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_REFUND_STATUS_FILTER)
								.contains(status.getStatus()))

				{
					String query = "";
					
					if(status.getStatus() == TicketStatus.PAYMENT_APPROVED)
					{
						query = "update Payment p join Ticket t on p.id = t.idPayment set p.approvedBy = :approvedBy where t.id = :ticketId ";
						
						hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery(query)
						.setParameter("approvedBy", AuthUtils.getCurrentUserId())
						.setParameter("ticketId", status
								.getTicketId())
						.executeUpdate();
						
						if(ticket.getPoaStatus() == TicketStatus.POA_DOCUMENTS_APPROVED && ticket.getPoiStatus() == TicketStatus.POI_DOCUMENTS_APPROVED)
						{
							ecafDAO.sendeCAFToCustomer(status.getTicketId());
						}
					}
					
					if(status.getStatus() == TicketStatus.PAYMENT_REJECTED)
					{
						query = "update Payment p join Ticket t on p.id = t.idPayment set p.rejectedBy = :rejectedBy where t.id = :ticketId ";
						
						hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery(query)
						.setParameter("rejectedBy", AuthUtils.getCurrentUserId())
						.setParameter("ticketId", status
								.getTicketId())
						.executeUpdate();
					}
					
					ticket.setPaymentStatus(status.getStatus());
					TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();
					ticketActivityLogVo.setTicketId(ticket.getId());
					ticketActivityLogVo.setActivityId(Activity.PAYMENT_UPDATE);
					ticketActivityLogVo.setModifiedOn(new Date());
					ticketActivityLogVo.setModifiedBy(AuthUtils.getCurrentUserId());
					ticketActivityLogVo.setStatus(TicketStatus.COMPLETED);
					ticketActivityLoggingDAO.updateTicketActivityLogByTicketId(ticketActivityLogVo);
					
					ticket.setPaymentRemark(status.getReasonValue());
					
				} else if (definitionCoreService
						.getStatusFilterForRole(DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER)
						.contains(status.getStatus()))
				{
					ticket.setDocumentStatus(status.getStatus());
					ticket.setDocumentRemark(status.getReasonValue());
					TicketActivityLogVo ticketActivityLogVo = new TicketActivityLogVo();
					ticketActivityLogVo.setActivityId(Activity.DOCUMENT_UPDATE);
					
					if(status.getStatus() == TicketStatus.POA_DOCUMENTS_APPROVED) 
					{
						ticket.setStatus(TicketStatus.POA_DOCUMENTS_APPROVED);
						ticket.setDocumentStatus(TicketStatus.POA_DOCUMENTS_APPROVED);
						ticket.setPoaStatus(TicketStatus.POA_DOCUMENTS_APPROVED);
						ticketActivityLogVo.setActivityId(Activity.POA_DOCUMENT_UPDATE);
						ticket.setPoaDocumentRemark(status.getReasonValue());
						
						if(ticket.getPaymentStatus() != null && ticket.getPaymentStatus() == TicketStatus.PAYMENT_APPROVED && ticket.getPoiStatus() == TicketStatus.POI_DOCUMENTS_APPROVED)
						{
							ecafDAO.sendeCAFToCustomer(status.getTicketId());
						}
						
					} else if (status.getStatus() == TicketStatus.POI_DOCUMENTS_APPROVED)
					{
						ticket.setStatus(TicketStatus.POI_DOCUMENTS_APPROVED);
						ticket.setDocumentStatus(TicketStatus.POI_DOCUMENTS_APPROVED);
						ticket.setPoiStatus(TicketStatus.POI_DOCUMENTS_APPROVED);
						ticketActivityLogVo.setActivityId(Activity.POI_DOCUMENT_UPDATE);
						ticket.setPoiDocumentRemark(status.getReasonValue());
						
						if(ticket.getPaymentStatus() != null && ticket.getPaymentStatus() == TicketStatus.PAYMENT_APPROVED && ticket.getPoaStatus() == TicketStatus.POA_DOCUMENTS_APPROVED)
						{
							ecafDAO.sendeCAFToCustomer(status.getTicketId());
						}
						
					} else if(status.getStatus() == TicketStatus.POA_DOCUMENTS_REJECTED) 
					{
						ticket.setStatus(TicketStatus.POA_DOCUMENTS_REJECTED);
						ticket.setDocumentStatus(TicketStatus.POA_DOCUMENTS_REJECTED);
						ticket.setPoaStatus(TicketStatus.POA_DOCUMENTS_REJECTED);
						ticketActivityLogVo.setActivityId(Activity.POA_DOCUMENT_UPDATE);
						ticket.setPoaDocumentRemark(status.getReasonValue());
						ticket.setPoaReason(status.getReasonCode() + "");
						
					} else if (status.getStatus() == TicketStatus.POI_DOCUMENTS_REJECTED)
					{
						ticket.setStatus(TicketStatus.POI_DOCUMENTS_REJECTED);
						ticket.setDocumentStatus(TicketStatus.POI_DOCUMENTS_REJECTED);
						ticket.setPoiStatus(TicketStatus.POI_DOCUMENTS_REJECTED);
						ticketActivityLogVo.setActivityId(Activity.POI_DOCUMENT_UPDATE);
						ticket.setPoiDocumentRemark(status.getReasonValue());
						ticket.setPoiReason(status.getReasonCode() + "");
						
					}else {
						
						ticket.setStatus(status.getStatus());
						ticket.setDocumentStatus(status.getStatus());
					}
					
					
					
					ticketActivityLogVo.setTicketId(ticket.getId());
					ticketActivityLogVo.setModifiedOn(new Date());
					ticketActivityLogVo.setModifiedBy(AuthUtils.getCurrentUserId());
					
					ticketActivityLoggingDAO.updateTicketActivityLogByTicketId(ticketActivityLogVo);
				}
				hibernateTemplate.update(ticket);

				logger.info("Ticket " + status.getTicketId()
						+ " status updated successfully");
				statusMsg = StatusMessages.UPDATED_SUCCESSFULLY;
				return statusMsg;
			}
		} catch ( Exception e)
		{

			logger.error("Failed to update Ticket Status. Reason "
					+ e.getMessage());

			e.printStackTrace();

			statusMsg = StatusMessages.UPDATE_FAILED;

			return statusMsg;
		}

	}

	private Ticket addCommonInitialValues(Ticket ticket)
	{
		if(ticket == null)
			return ticket;
		
		logger.debug(" inside  addCommonInitialValues  " + ticket.getProspectNo());
		// TODO Auto-generated method stub
		Date currentDate = new Date();

		ticket.setModifiedOn(currentDate);

		logger.debug(" exit from  addCommonInitialValues  " + ticket.getProspectNo());
		return ticket;
	}

	@Override
	public List<TicketDetailVo> getCFETicketWithDetails(TicketFilter filter) {

		if (filter == null)
			filter = new TicketFilter();

		logger.info(" inside  getCFETicketWithDetails  " + AuthUtils.getCurrentUserLoginId());
		try
		{

			StringBuilder completeQuery = new StringBuilder(TICKET_DETAIL_CFE_QUERY);

			if (filter.getPageSize() > 0)
				;
			else
			{
				filter.setStartOffset(0);
				filter.setPageSize(MIN_ITEM_PER_PAGE);
			}

			if (filter.getTicketId() != null && filter.getTicketId() > 0)
				completeQuery.append(" and t.id =" + filter.getTicketId());

			if (filter.getProspectType() != null
					&& filter.getProspectType() > 0)
				completeQuery.append(" and c.status="
						+ filter.getProspectType());

			if (filter.getTicketIds() != null
					&& !filter.getTicketIds().isEmpty()
					&& filter.getTicketIds().size() > 0)
			{
				completeQuery.append(" and t.id in (:ticketIds)");
				filter.setAssignedUserId(null); // Shows from queue
			}

			if (filter.getAssignedUserId() != null)
				completeQuery.append(" and t.currentAssignedTo = "
						+ filter.getAssignedUserId());

			// Ignore tickets with not payment info

			if (AuthUtils.isCFEUser())
			{
				if (DefinitionCoreService.PAYMENT_RELATED_STATUS.equals(filter
						.getPageContextId())
						|| DefinitionCoreService.REFUND_RELATED_STATUS
								.equals(filter.getPageContextId()))
					completeQuery
							.append(" and t.paymentStatus in (:paymentStatusList) ");
				else if (DefinitionCoreService.DOCUMENT_RELATED_STATUS
						.equals(filter.getPageContextId()))
					completeQuery
							.append(" and (t.documentStatus in (:documentStatusList) or t.poiStatus in (:poiStatus) or t.poaStatus in (:poaStatus))  ");
				
				else if (DefinitionCoreService.MQ_FAILURE_RELATED_STATUS .equals(filter
						.getPageContextId()))
				{
					completeQuery
					.append(" and t.mqStatus = 0 and t.paymentStatus is not null and ( t.documentStatus is not null or ( t.poiStatus is not null and t.poaStatus is not null ) ) ");
				}
				
				else if (DefinitionCoreService.ECAF_RELATED_STATUS.equals(filter.getPageContextId()))
				{
					logger.info("ecaf :: " +filter.getPageContextId());
					completeQuery.append(" and t.paymentStatus in (:paymentStatusList) and t.documentStatus in (:documentStatusList) ");
				}
				
			}

			else if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
				completeQuery.append(" and t.status in (:ticketStatusList)");

			if (filter.getTicketType() != null && filter.getTicketType() > 0)
				completeQuery.append(" and t.ticketCategory="
						+ filter.getTicketType());

			{ // Date filter
				if (filter.getFromDate() != null)
					completeQuery.append(" and t.addedOn >= :fromDate");
				if (filter.getToDate() != null)

				{
					filter.setToDate(GenericUtil.addDays(filter.getToDate(), 1));
					completeQuery.append(" and t.addedOn <= :toDate");
				}
			}

			List<Long> areaIds = new ArrayList<Long>();
			
			 if (filter.getAreaId() != null && filter.getAreaId() > 0)
				areaIds.add(filter.getAreaId());
			else if (filter.getAreaIds() != null && filter.getAreaIds().size() > 0)
				areaIds.addAll(filter.getAreaIds());
			 
			
			// Remove areas if user is havving ticket id as filter
			if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0)
				areaIds.clear();
			
			if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
				
			{
					completeQuery.append(" and c.areaId in (:areaIds)");
			}
			else if(areaIds.size() > 0)
			{
				completeQuery.append(" and (  c.areaId in (:areaIds) or t.currentAssignedTo in(:reportingUsers)) ");
			}
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{	
				completeQuery.append(" and  c.branchId  in (:branchIds)   ");
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				completeQuery.append(" and  c.cityId  in (:cityIds) ");
			}
			 

			if (filter.getTicketStatus() != null
					&& filter.getTicketStatus() > 0 && !AuthUtils.isCFEUser())
			{
				completeQuery.append(" and t.status ="
						+ filter.getTicketStatus());
			}

			if (filter.getCustomerId() != null)
			{
				completeQuery.append(" and t.prospectNo = "
						+ filter.getCustomerId());
			}
			if (filter.getCustomerContactNumber() != null)
			{
				completeQuery.append(" and c.mobileNumber = "
						+ filter.getCustomerContactNumber());
			}
			appendCachingFilterQuery(filter, completeQuery );
			
			completeQuery
					.append(" and t.prospectNo is not null order by t.modifiedOn Desc ");
			completeQuery.append(" limit " + filter.getStartOffset() + " , "
					+ filter.getPageSize());

			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(completeQuery.toString());

			setCachingFilter(query, filter);

			if (filter.getFromDate() != null)
				query.setDate("fromDate", filter.getFromDate());
			if (filter.getToDate() != null)
				query.setDate("toDate", filter.getToDate());

			if (filter.getTicketIds() != null
					&& filter.getTicketIds().size() > 0)
				query.setParameterList("ticketIds", filter.getTicketIds());

			if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
				
			{
				 
				query.setParameterList("areaIds", areaIds);
				logger.info(" : areaIds " + areaIds);
			}
			else if(areaIds.size() > 0)
			{
				Map<Long, String> reportingUsers = userDao.getNestedReportingUsers(AuthUtils.getCurrentUserId(), null,null,null);
				query.setParameterList("areaIds", areaIds);
				
				if(reportingUsers == null || reportingUsers.size() == 0)
				{
					query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
				}
				else
				{
					query.setParameterList("reportingUsers", reportingUsers.keySet());
				}
			}
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{
				query.setParameterList("branchIds", Collections.singletonList(filter.getBranchId()));
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				query.setParameterList("cityIds",Collections.singletonList( filter.getCityId()));
			}
			

			if (AuthUtils.isCFEUser())
			{
				// filter.getTicketStatus() > 0
				if (DefinitionCoreService.PAYMENT_RELATED_STATUS.equals(filter
						.getPageContextId()))
				{
					if (filter.getTicketStatus() != null
							&& filter.getTicketStatus() > 0)
					{
						query.setParameterList("paymentStatusList", Collections
								.singletonList(filter.getTicketStatus()));

					} else
					{
						logger.debug(" payment status codes :: "+definitionCoreService.getStatusFilterForRole(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER));
						query.setParameterList("paymentStatusList", definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_PAYMENT_STATUS_FILTER));

					}
				}
				// completeQuery.append(" and t.paymentStatus in (:paymentStatusList) ");
				else if (DefinitionCoreService.DOCUMENT_RELATED_STATUS
						.equals(filter.getPageContextId()))
				{
					Set<Integer> documentStatus = definitionCoreService
							.getStatusFilterForRole(DefinitionCoreService.CFE_AT_DOCUMENT_STATUS_FILTER);
					Set<Integer> poaDocumentStatus = definitionCoreService
							.getStatusFilterForRole(DefinitionCoreService.CFE_AT_POA_DOCUMENT_STATUS_FILTER);
					Set<Integer> poiDocumentStatus = definitionCoreService
							.getStatusFilterForRole(DefinitionCoreService.CFE_AT_POI_DOCUMENT_STATUS_FILTER);
					if (filter.getTicketStatus() != null
							&& filter.getTicketStatus() > 0)
					{
						if (documentStatus.contains(filter.getTicketStatus()))
							query.setParameterList("documentStatusList",
									Collections.singletonList(filter.getTicketStatus()));
						else
							query.setParameterList("documentStatusList", documentStatus);
						
						if (poiDocumentStatus.contains(filter.getTicketStatus()))
							query.setParameterList("poiStatus", Collections.singletonList(filter.getTicketStatus()));
						else
							query.setParameterList("poiStatus", poiDocumentStatus);
						
						if (poaDocumentStatus.contains(filter.getTicketStatus()))
							query.setParameterList("poaStatus", Collections.singletonList(filter.getTicketStatus()));
						else
							query.setParameterList("poaStatus", poaDocumentStatus);

					} else
					{
						query.setParameterList("documentStatusList", documentStatus);
						query.setParameterList("poiStatus", poiDocumentStatus);
						query.setParameterList("poaStatus", poaDocumentStatus);

					}

				}

				// completeQuery.append(" and t.paymentStatus in (:paymentStatusList) ");
				else if (DefinitionCoreService.REFUND_RELATED_STATUS
						.equals(filter.getPageContextId()))
				{
					if (filter.getTicketStatus() != null
							&& filter.getTicketStatus() > 0)
					{
						query.setParameterList("paymentStatusList", Collections
								.singletonList(filter.getTicketStatus()));
					} else
					{
						query.setParameterList("paymentStatusList", definitionCoreService
								.getStatusFilterForRole(DefinitionCoreService.CFE_AT_REFUND_STATUS_FILTER));

					}

				}
				
			}

			else if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
				query.setParameterList("ticketStatusList", filter
						.getTicketStatusList());

			logger.info("completeQuery.toString() presale "
					+ query.getQueryString());

          //  TicketDetailVo ticket;
			
			

			List<Object[]> result = query.list();
			
			logger.info("cfe tickets size :: "+result.size());

			if (result == null)
				return null;

			List<TicketDetailVo> tickets = new ArrayList<TicketDetailVo>();
			List<Long> ticketIds = new ArrayList<Long>();
			TicketDetailVo ticket;

			List<CustomerAddressVO> cavolist;
			ObjectMapper mapper = new ObjectMapper();

			for (Object[] obj : result)
			{
				cavolist=new ArrayList<CustomerAddressVO>();
				ticket = new TicketDetailVo();
				ticket.setId(objectToLong(obj[0])+"");
				ticket.setProspectNo((String) obj[1]);

				ticket.setSource((String) obj[2]);
				// ticket.setCommitedEtr(GenericUtil.convertToDateFormat(obj[3]));
				if (obj[3] != null)
					ticket.setCurrentEtr(GenericUtil.convertToUiDateFormat(obj[3]));
				ticket.setAccountNumber((String) obj[4]);
				ticket.setCustomerId(objectToLong(obj[5]) + "");

				CustomerVO customer = customerDAO.getCustomerById(Long
						.valueOf(ticket.getCustomerId()));
				if (customer != null)
				{
					ticket.setCustomerName(GenericUtil
							.completeCustomerName(customer));
					ticket.setTitle(customer.getTitle());
					ticket.setFirstName(customer.getFirstName());
					ticket.setLastName(customer.getLastName());
					ticket.setMiddleName(customer.getMiddleName());
				}
				ticket.setMobileNumber((String) obj[7]);
				//communication address for C.F.E
				if(obj[8]!=null){
					
					String communicationAddress=obj[8].toString();
					
					if(CommonUtil.isValidJson(communicationAddress)){
							
							CustomerAddressVO communicationAddressVO = mapper.readValue(communicationAddress,
									CustomerAddressVO.class);
							if (communicationAddressVO.getOperationalEntity() == null
									|| communicationAddressVO.getOperationalEntity().isEmpty()) {
								communicationAddressVO.setOperationalEntity(ticket.getBranch());
								communicationAddressVO.setOperationalEntityId(ticket.getBranchId());

							}

							if (communicationAddressVO.getCity() == null || communicationAddressVO.getCity().isEmpty()) {
								communicationAddressVO.setCity(ticket.getCity());
							}

							if (communicationAddressVO.getArea() == null || communicationAddressVO.getArea().isEmpty()) {
								communicationAddressVO.setArea(ticket.getAreaName());
								communicationAddressVO.setAreaId(ticket.getAreaId());
							}

							if (communicationAddressVO.getSubArea() == null
									|| communicationAddressVO.getSubArea().isEmpty() && ticket.getSubAreaId() != null) {
								communicationAddressVO.setSubArea(ticket.getSubAreaName());
								if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
									communicationAddressVO.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
							}

							
							cavolist.add(communicationAddressVO);
						}else{
							
							ticket.setCommunicationAddress((String) obj[8]);
						}
					}
				//ticket.setCommunicationAddress((String) obj[8]);
				ticket.setCreatedDate(GenericUtil.convertToDateFormat(obj[9]));
				ticket.setAreaId(objectToLong(obj[10]));
				ticket.setStatusId(objectToLong(obj[11]));

				if (obj[11] != null)
				{
					int status = (int) obj[11];

					logger.debug("\n\nTicket is having status as " + status);

					// if (status > 0
					// && status == TicketStatus.FEASIBILITY_GRANTED_BY_NE)
					// {
					GISPojo gisPojo = gisDAO.getGisInfoByTicketId(Long.valueOf(ticket
							.getId()));

					logger.debug("###### Gis details are In presale ticket details "
							+ gisPojo);

					ticket.setLattitude((String) (obj[44]));
					ticket.setLongitude((String) (obj[45]));
					
					if (gisPojo != null)
					{
						ticket.setFxName(gisPojo.getFxName());
						ticket.setConnectionType(gisPojo.getConnectionType());
						ticket.setFxIpAddress(gisPojo.getFxIpAddress());
						ticket.setFxMacAddress(gisPojo.getFxMacAddress());
						ticket.setFxPorts(gisPojo.getFxPorts());
						ticket.setCxName(gisPojo.getCxName());
						ticket.setCxIpAddress(gisPojo.getCxIpAddress());
						ticket.setCxMacAddress(gisPojo.getCxMacAddress());
						ticket.setCxPorts(gisPojo.getCxPorts());
					
						if (ticket.getLattitude() != null
								&& gisPojo.getLattitude() != null)
							ticket.setLattitude(gisPojo.getLattitude());

						if (ticket.getLongitude() != null
								&& gisPojo.getLongitude() != null)
							ticket.setLongitude(gisPojo.getLongitude());

						if (WorkStageName.FIBER
								.equalsIgnoreCase(gisPojo.getConnectionType()))
						{
							ticket.setFiberFeasible(true);
						}
					}
					// } else
					// {
					// logger.debug("\n\nTicket is having status as less than zero"
					// + status);
					// }
				}

				ticket.setFeasibilityTypeDetails(gisDAO
						.getGisTypeAndDetailsByTicketId(Long.valueOf(ticket.getId())));

				ticket.setCafNumber((String) (obj[12]));
				ticket.setRemarks((String) (obj[13]));

				if (obj[14] != null)
					ticket.setRouterRequired((Boolean) (obj[14]));
				ticket.setRouterDescription((String) (obj[15]));

				if (obj[16] != null)
				{
					Long userId = objectToLong(obj[16]);
					ticket.setAssignedToId(userId);
					UserVo vo = userDao.getUserById(userId);
					if (vo != null)
						ticket.setAssignedToDisplayName(GenericUtil.completeUserName(vo ));

				}

				if (obj[17] != null)
				{

					ticket.setCreatedDate(GenericUtil
							.convertToUiDateFormat(obj[17]));
				}

				if (obj[18] != null)
				{

					ticket.setPreferedDate((GenericUtil
							.convertToDateFormat(obj[18])));
				}
				if (obj[19] != null)
				{
					ticket.setWorkOrderType((String) (obj[19]));
				}

				ticket.setBranchId(objectToLong(obj[20]));

				ticket.setCityId(objectToLong(obj[21]));
				ticket.setPlanId(objectToLong(obj[22]));

		// ************************ Permanent Address**************************************** 		
				if(obj[23]!=null){
					
					String permanentAddress=obj[23].toString();
					
					if(CommonUtil.isValidJson(permanentAddress)){
						
						CustomerAddressVO permanentAddressVO=mapper.readValue(permanentAddress, CustomerAddressVO.class);
						
						if (permanentAddressVO.getOperationalEntity() == null
								|| permanentAddressVO.getOperationalEntity().isEmpty()) {
							permanentAddressVO.setOperationalEntity(ticket.getBranch());
							permanentAddressVO.setOperationalEntityId(ticket.getBranchId());

						}

						if (permanentAddressVO.getCity() == null || permanentAddressVO.getCity().isEmpty()) {
							permanentAddressVO.setCity(ticket.getCity());
						}

						if (permanentAddressVO.getArea() == null || permanentAddressVO.getArea().isEmpty()) {
							permanentAddressVO.setArea(ticket.getAreaName());
							permanentAddressVO.setAreaId(ticket.getAreaId());
						}

						if (permanentAddressVO.getSubArea() == null
								|| permanentAddressVO.getSubArea().isEmpty() && ticket.getSubAreaId() != null) {
							permanentAddressVO.setSubArea(ticket.getSubAreaName());
							if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
								permanentAddressVO.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
						}
						cavolist.add(permanentAddressVO);
					}else{
						ticket.setPermanentAddress((String) (obj[23]));
					}
				}

				ticket.setPincode(objectToInt(obj[24]) + "");

				Long paymentId = objectToLong(obj[25]);
			
				if (paymentId > 0)
				{
					Payment pay = hibernateTemplate
							.get(Payment.class, paymentId);
					
					ticket.setPaymentRefNumber(pay.getReferenceNo());
					ticket.setPaidAmount(pay.getPaidAmount());
					ticket.setPaymentMode(pay.getPaymentMode());
					ticket.setInstallationCharges(pay.getInstallationCharges());
					
//					System.out.println("pay.getPaidConnection() " + pay.getPaidConnection() + " Ticket " + ticket.getId());

					if (null != pay.getPaidConnection()
							&& !"null".equalsIgnoreCase(pay.getPaidConnection()))
					{
//						System.out.println("Inside ");
						if (BooleanValue.FALSE
								.equalsIgnoreCase(pay.getPaidConnection()))
						{
							ticket.setPaidConnection(BooleanValue.FALSE);
//							System.out.println("Free");
						}
						else
							ticket.setPaidConnection(BooleanValue.TRUE);
					} else
					{
						ticket.setPaidConnection(BooleanValue.TRUE);
					}
					
//					System.out.println("pay.getPaidConnection() " + ticket.getPaidConnection() + " Ticket " + ticket.getId());

					
				} else
				{
					ticket.setPaymentRefNumber("N/A");
					ticket.setPaidConnection(BooleanValue.TRUE);
				}

				if (obj[26] != null)
				{
					ticket.setTicketSubCategory((String) (obj[26]));
				}

				if (obj[27] != null)
				{
					ticket.setCustomerEmail((String) (obj[27]));
				}
				if (obj[28] != null)
				{
					ticket.setEnquiryModeId((String) (obj[28]));
				}
				if (obj[29] != null)
				{
					ticket.setSourceModeId((String) (obj[29]));
				}
				if (obj[30] != null)
				{
					
//					System.out.println("@@@@@@@@@@@@######## (String) (obj[30]) " + (String) (obj[30])+" " +ticket.getId());
					ticket.setProspectColdReason((String) (obj[30]));
				}

				if (obj[31] != null)
				{
					if ((int) (obj[31]) == FWMPConstant.ProspectType.COLD_ID)
						ticket.setProspectType(FWMPConstant.ProspectType.COLD);

					if ((int) (obj[31]) == FWMPConstant.ProspectType.HOT_ID)
						ticket.setProspectType(FWMPConstant.ProspectType.HOT);

					if ((int) (obj[31]) == FWMPConstant.ProspectType.MEDIUM_ID)
						ticket.setProspectType(FWMPConstant.ProspectType.MEDIUM);
				}

				ticket.setTicketSubCategoryId(objectToLong(obj[32]));

				if (obj[33] != null)
					ticket.setPaymentStatus(objectToInt(obj[33]));

				if (obj[34] != null)
					ticket.setDocumentStatus(objectToInt(obj[34]));

				ticket.setCxPermission((String) (obj[35]));
				ticket.setGxPermission((String) (obj[36]));

				ticket.setCustomerAltMobile((String) (obj[37]));
				ticket.setOfficeNumber((String) (obj[38]));
				ticket.setCustomerProfile((String) (obj[39]));
				ticket.setCustomerAltEmail((String) (obj[40]));

				if (obj[41] != null && (Byte) (obj[41]) == 1)
					ticket.setExternalRouterRequired(true);
				else
					ticket.setExternalRouterRequired(false);

				ticket.setExternalRouterDescription((String) (obj[42]));
				ticket.setExternalRouterAmount((String) (obj[43]));

				ticket.setPriority(objectToInt(obj[46])
						+ PriortyUpdateHelper.getGlobalPriority().intValue());
				ticket.setPriorityEscalationType(objectToInt(obj[47]));
				ticket.setEscalatedValue(objectToInt(obj[48]));
				ticket.setUpdatedCount(objectToInt(obj[49]));

				// ***************************Current Address *************************
				
				if(obj[50]!=null){
					
                  String currentAddress = (obj[50]).toString();
                  
                  if (currentAddress != null) {
                	  
						if(CommonUtil.isValidJson(currentAddress))
						{
							CustomerAddressVO currentAddressVo = mapper.readValue(currentAddress,
									CustomerAddressVO.class);
							
							if (currentAddressVo.getOperationalEntity() == null
									|| currentAddressVo.getOperationalEntity().isEmpty()) {
								currentAddressVo.setOperationalEntity(ticket.getBranch());
								currentAddressVo.setOperationalEntityId(ticket.getBranchId());

							}

							if (currentAddressVo.getCity() == null || currentAddressVo.getCity().isEmpty()) {
								currentAddressVo.setCity(ticket.getCity());
							}

							if (currentAddressVo.getArea() == null || currentAddressVo.getArea().isEmpty()) {
								currentAddressVo.setArea(ticket.getAreaName());
								currentAddressVo.setAreaId(ticket.getAreaId());
							}

							if (currentAddressVo.getSubArea() == null
									|| currentAddressVo.getSubArea().isEmpty() && ticket.getSubAreaId() != null) {
								currentAddressVo.setSubArea(ticket.getSubAreaName());
								if (ticket.getSubAreaId() != null && !ticket.getSubAreaId().isEmpty())
									currentAddressVo.setSubAreaId(Long.valueOf(ticket.getSubAreaId()));
							}

							cavolist.add(currentAddressVo);
						}
				        else {
						ticket.setCurrentAddress((obj[50]).toString());
					}
				}
				}
				

				ticket.setPaymentRemark((String) (obj[51]));
				ticket.setDocumentRemark((String) (obj[52]));

				ticket.setMqStatus(objectToInt(obj[53]));
				ticket.setMqRetryCount(objectToInt(obj[54]));
				
				ticket.setNeComment((String) (obj[55]));
				ticket.setCommitedEtr(GenericUtil.convertToUiDateFormat(obj[56]));
				

				if((objectToLong(obj[57])) != null && objectToLong(obj[57]) > 0)
				{
					UserVo user = userDao.getUserByEmpId(objectToLong(obj[57]));
					ticket.setAssignedBy(GenericUtil.completeUserName(user));
				}
				
				String aadharTransactions = customerDAO.getCustomerAadhaarMapping(Long.valueOf(ticket.getCustomerId()), Long.valueOf(ticket.getId()));
				
				if(aadharTransactions != null)
				ticket.setFinahubTransactionId(aadharTransactions);
				
				KycDetails kycDetails = new KycDetails();
				
				
				if(aadharTransactions != null)
				{
					ticket.setFinahubTransactionId(aadharTransactions);
					kycDetails.setUid(aadharTransactions);
				}
				if (obj[59] != null)
					kycDetails.setName((String)obj[59]);
				if (obj[60] != null)
					kycDetails.setDob((String)obj[60]);
				if (obj[61] != null)
					kycDetails.setEmail((String)obj[61]);
				if (obj[62] != null)
					kycDetails.setGender((String)obj[62]);
				if (obj[63] != null)
					kycDetails.setPhone((String)obj[63]);
				if (obj[64] != null)
					kycDetails.setCareof((String)obj[64]);
				if (obj[65] != null)
					kycDetails.setHouseno((String)obj[65]);
				if (obj[66] != null)
					kycDetails.setDistrict((String)obj[66]);
				if (obj[67] != null)
					kycDetails.setLocality((String)obj[67]);
				if (obj[68] != null)
					kycDetails.setLandmark((String)obj[68]);
				if (obj[69] != null)
					kycDetails.setPincode((String)obj[69]);
				if (obj[70] != null)
					kycDetails.setPostoffice((String)obj[70]);
				if (obj[71] != null)
					kycDetails.setStreet((String)obj[71]);
				if (obj[72] != null)
					kycDetails.setState((String)obj[72]);
				if (obj[73] != null)
					kycDetails.setVtcname((String)obj[73]);
				
				ticket.setAdhaarDetails(kycDetails);
				
				if((objectToLong(obj[74])) != null && objectToLong(obj[74]) == Long.valueOf(Status.ACTIVE))
				{
					ticket.setAadhaarForPOI(true);
				} else
					ticket.setAadhaarForPOI(false);
				
				if((objectToLong(obj[75])) != null && objectToLong(obj[75]) == Long.valueOf(Status.ACTIVE))
				{
					ticket.setAadhaarForPOA(true);
					
				} else
					ticket.setAadhaarForPOA(false);
				
				if(obj[76] != null )
					ticket.setPoiStatusId(objectToLong(obj[76]));
				
				if(obj[77] != null )
					ticket.setPoaStatusId(objectToLong(obj[77]));
				
				if(obj[78] != null )
					ticket.setPoaRemarks((String)obj[78]);
				
				if(obj[79] != null )
					ticket.setPoiRemarks((String)obj[79]);
				
				if(obj[80] != null )
					ticket.setFinahubTransactionId((String)obj[80]);
				
			/*	if(obj[81] != null ){
					ticket.setCrpAccountNumber(obj[81].toString());
				}
				if(obj[82]!=null){
					ticket.setCrpMobileNumber(obj[82].toString());
				}
				if(obj[83]!=null)
				{
					byte vIn = (byte)(obj[83]);
					boolean vOut = vIn!=0;
					ticket.setIsStaticIPRequired(vOut);
				}
				if(obj[84]!=null){
					ticket.setExistingIsp(obj[84].toString());
				}
				if(obj[85]!=null){
					ticket.setCouponCode(obj[85].toString());
					
				}
				if(obj[86]!=null){
					ticket.setNationality(obj[86].toString());
					
				}
				if(obj[87]!=null){
					ticket.setSubAreaId(obj[87].toString());
					ticket.setSubAreaName(LocationCache.getSubAreasBySubAreaId(objectToLong(obj[87])));
					
				}*/
				
		
				ticket.setCustomerAddressesList(cavolist);
				
				ticket.setBranch(LocationCache.getBrancheNameById(ticket
						.getBranchId()));

				ticket.setCity(LocationCache.getCityNameById(ticket.getCityId()));
				ticket.setPlanName(tariffDAO.getTariffNameById(ticket
						.getPlanId()));

/*				if (FWMPConstant.ANDRIOD_PAGE_CONTEXT.equals(filter
						.getPageContextId()))
				{
*/					if (ticket.getPlanId() >= 0)
					{
						TariffPlanVo tariffPlanVo = tariffDAO
								.getTariffById(ticket.getPlanId());

						if (tariffPlanVo != null)
						{
							ticket.setPlanName(tariffPlanVo.getPackageName());
							ticket.setPlanId(Long.valueOf(tariffPlanVo.getId()));
							ticket.setPlanType(tariffPlanVo.getPlanType());
							ticket.setPlanCategoery(tariffPlanVo
									.getPlanCategoery());
							ticket.setPlanAmount(tariffPlanVo
									.getOfferPriceWithTax());
//							ticket.setInstallationCharges(tariffPlanVo
//									.getInstallChargesWithTax());
/*						}
*/					}
				}
				if (ticket.getAreaId() > 0)
				{
					ticket.setAreaName(LocationCache.getAreaNameById((ticket
							.getAreaId())));
				}
				/*if (filter.isIncludeCompleteStatus())
				{
					if (filter.getPageContextId() != null
							&& filter.getPageContextId() == SEARCH_PAGE)
						populateActivityLog(ticket, filter.getPageContextId(), filter);
					else
						populateActivityLog(ticket, IS_SALES, filter);
				}*/
				tickets.add(ticket);
				ticketIds.add(Long.valueOf(ticket.getId()));
				Long statusId = ticket.getStatusId();
				if (statusId != null)
				{
					ticket.setStatus(TicketStatus.statusDisplayValue
							.get(statusId.intValue()));

					if (AuthUtils.isCFEUser())
					{
						if (ticket.getPaymentStatus() > 0
								&& DefinitionCoreService.PAYMENT_RELATED_STATUS
										.equals(filter.getPageContextId()))
						{
							ticket.setStatus(TicketStatus.statusDisplayValue
									.get(ticket.getPaymentStatus()));
						}
						// completeQuery.append(" and t.paymentStatus in (:paymentStatusList) ");
						else if (ticket.getDocumentStatus() > 0
								&& DefinitionCoreService.DOCUMENT_RELATED_STATUS
										.equals(filter.getPageContextId()))
						{
							ticket.setStatus(TicketStatus.statusDisplayValue
									.get(ticket.getDocumentStatus()));
						}
						
					}
					else
					{
						ticket.setEditable(definitionCoreService
								.getEditableStatusFilterForCurrentUser().contains(Integer
										.valueOf(ticket.getStatusId() + "")));
					}
				}

			}
			if (filter.isIncludeCompleteStatus() && !ticketIds.isEmpty())
			{
				if (filter.getPageContextId() != null
						&& filter.getPageContextId() == 3)
					populateActivityLogForTickets(tickets, ticketIds, filter.getPageContextId(), filter) ;
				else
					populateActivityLogForTickets(tickets,ticketIds,  IS_SALES, filter);
			}
			// if (tickets != null)
			// Collections.sort(tickets, new TicketPriorityComparator());
			logger.info("############## results for Sales "
					+ tickets.size());
			return tickets;// CommonUtil.createPagedResultSet(tickets);

		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Exception in Getting  tickets for Sales with filter "
					+ filter + " message " + e.getMessage());
		}
		logger.info(" exit from  getPreSaleTicketWithDetails  " + filter.getMqId());
		return new ArrayList<TicketDetailVo>();

	
	}

}
