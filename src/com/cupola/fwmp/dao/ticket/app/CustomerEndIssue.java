package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

public class CustomerEndIssue
{

	private String priorityLevel;
	private Long internalCableIssue;
	private Long customerPcRouterIssue;

	private List<Long> internalCableIssueTicketIds;
	private List<Long> customerPcRouterIssueTicketIds;

	public CustomerEndIssue()
	{
		this.priorityLevel = "0";
		this.internalCableIssue = 0l;
		this.customerPcRouterIssue = 0l;
	}

	public String getPriorityLevel()
	{
		return priorityLevel;
	}

	public void setPriorityLevel(String priorityLevel)
	{
		this.priorityLevel = priorityLevel;
	}

	public Long getInternalCableIssue()
	{
		return internalCableIssue;
	}

	public void setInternalCableIssue(Long internalCableIssue)
	{
		this.internalCableIssue = internalCableIssue;
	}

	public Long getCustomerPcRouterIssue()
	{
		return customerPcRouterIssue;
	}

	public void setCustomerPcRouterIssue(Long customerPcRouterIssue)
	{
		this.customerPcRouterIssue = customerPcRouterIssue;
	}

	public List<Long> getInternalCableIssueTicketIds()
	{
		return internalCableIssueTicketIds;
	}

	public void setInternalCableIssueTicketIds(
			List<Long> internalCableIssueTicketIds)
	{
		this.internalCableIssueTicketIds = internalCableIssueTicketIds;
	}

	public List<Long> getCustomerPcRouterIssueTicketIds()
	{
		return customerPcRouterIssueTicketIds;
	}

	public void setCustomerPcRouterIssueTicketIds(
			List<Long> customerPcRouterIssueTicketIds)
	{
		this.customerPcRouterIssueTicketIds = customerPcRouterIssueTicketIds;
	}

	@Override
	public String toString()
	{
		return "CustomerEndIssue [priorityLevel=" + priorityLevel
				+ ", internalCableIssue=" + internalCableIssue
				+ ", customerPcRouterIssue=" + customerPcRouterIssue
				+ ", internalCableIssueTicketIds="
				+ internalCableIssueTicketIds
				+ ", customerPcRouterIssueTicketIds="
				+ customerPcRouterIssueTicketIds + "]";
	}

}
