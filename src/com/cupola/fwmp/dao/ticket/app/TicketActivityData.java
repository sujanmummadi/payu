package com.cupola.fwmp.dao.ticket.app;

import java.util.List;
import java.util.Set;

public class TicketActivityData
{
	Set<Long> completedActivities ;
	Long ticketId ;
	Long assignedToUserId ;
	String assignedToDisplayName;
	Long priorityCategory;
	Long priorityValue;
	
	
	
	
	public Set<Long> getCompletedActivities()
	{
		return completedActivities;
	}
	public void setCompletedActivities(Set<Long> completedActivities)
	{
		this.completedActivities = completedActivities;
	}
	public Long getAssignedToUserId()
	{
		return assignedToUserId;
	}
	public void setAssignedToUserId(Long assignedToUserId)
	{
		this.assignedToUserId = assignedToUserId;
	}
	public String getAssignedToDisplayName()
	{
		return assignedToDisplayName;
	}
	public void setAssignedToDisplayName(String assignedToDisplayName)
	{
		this.assignedToDisplayName = assignedToDisplayName;
	}
	public Long getPriorityCategory()
	{
		return priorityCategory;
	}
	public void setPriorityCategory(Long priorityCategory)
	{
		this.priorityCategory = priorityCategory;
	}
	public Long getPriorityValue()
	{
		return priorityValue;
	}
	public void setPriorityValue(Long priorityValue)
	{
		this.priorityValue = priorityValue;
	}
	public Long getTicketId()
	{
		return ticketId;
	}
	public void setTicketId(Long ticketId)
	{
		this.ticketId = ticketId;
	}
	public TicketActivityData(Set<Long> completedActivities, Long ticketId,
			Long assignedToUserId, String assignedToDisplayName,
			Long priorityCategory, Long priorityValue)
	{
		super();
		this.completedActivities = completedActivities;
		this.ticketId = ticketId;
		this.assignedToUserId = assignedToUserId;
		this.assignedToDisplayName = assignedToDisplayName;
		this.priorityCategory = priorityCategory;
		this.priorityValue = priorityValue;
	}
	@Override
	public String toString()
	{
		return "TicketActivityData [completedActivities=" + completedActivities
				+ ", ticketId=" + ticketId + ", assignedToUserId="
				+ assignedToUserId + ", assignedToDisplayName="
				+ assignedToDisplayName + ", priorityCategory="
				+ priorityCategory + ", priorityValue=" + priorityValue + "]";
	}
}
