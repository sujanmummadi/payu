package com.cupola.fwmp.dao.ticket.app;

import java.util.Date;

public class TicketSummary {
	
	private Long userId;
	private String workordertype;
	private Long workOrderTypeId;
	private Long workOrderId;
	private Long ticketId;
	private Long currentActivity;
	private Long assignedTo;
	private Date addedOn;
	private Long currentWorkStage;
	private Long subCategoryId;
	private Integer status;
	private Long priorityValue;
	
	
	public Long getPriorityValue() {
		return priorityValue;
	}

	public void setPriorityValue(Long priorityValue) {
		this.priorityValue = priorityValue;
	}

	public Long getSubCategoryId()
	{
		return subCategoryId;
	}

	public void setSubCategoryId(Long subCategoryId)
	{
		this.subCategoryId = subCategoryId;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public TicketSummary(){}
	
	
	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public Long getCurrentWorkStage() {
		return currentWorkStage;
	}

	public void setCurrentWorkStage(Long currentWorkStage) {
		this.currentWorkStage = currentWorkStage;
	}

	public String getWorkordertype() {
		return workordertype;
	}
	
	public Long getWorkOrderId()
	{
		return workOrderId;
	}

	public void setWorkOrderId(Long workOrderId)
	{
		this.workOrderId = workOrderId;
	}

	public void setWorkordertype(String workordertype) {
		this.workordertype = workordertype;
	}
	
	public Long getWorkOrderTypeId() {
		return workOrderTypeId;
	}
	
	public void setWorkOrderTypeId(Long workOrderTypeId) {
		this.workOrderTypeId = workOrderTypeId;
	}
	
	
	public Long getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}
	
	public Long getCurrentActivity() {
		return currentActivity;
	}
	
	public void setCurrentActivity(Long currentActivity) {
		this.currentActivity = currentActivity;
	}
	
	public Long getAssignedTo() {
		return assignedTo;
	}
	
	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}
	
	public Date getAddedOn() {
		return addedOn;
	}
	
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	@Override
	public String toString()
	{
		return "TicketSummary [userId=" + userId + ", workordertype="
				+ workordertype + ", workOrderTypeId=" + workOrderTypeId
				+ ", id=" + workOrderId + ", ticketId=" + ticketId
				+ ", currentActivity=" + currentActivity + ", assignedTo="
				+ assignedTo + ", addedOn=" + addedOn + ", currentWorkStage="
				+ currentWorkStage + ", subCategoryId=" + subCategoryId
				+ ", status=" + status + "]";
	}

}
