package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

public class SalesRelatedIssue {
	
	private String priorityLevel;
	private Long customerRejectingConn;
	private Long permissionIssue;
	
	private List<Long> customerRejectingConnTicketIds;
	private List<Long> permissionIssueTicketIds;
	
	public SalesRelatedIssue()
	{
		this.customerRejectingConn = 0l;
		this.permissionIssue = 0l; 
		this.priorityLevel = "0";
	}

	public String getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(String priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Long getCustomerRejectingConn() {
		return customerRejectingConn;
	}

	public void setCustomerRejectingConn(Long customerRejectingConn) {
		this.customerRejectingConn = customerRejectingConn;
	}

	public Long getPermissionIssue() {
		return permissionIssue;
	}

	public void setPermissionIssue(Long permissionIssue) {
		this.permissionIssue = permissionIssue;
	}

	public  List<Long> getCustomerRejectingConnTicketIds()
	{
		return customerRejectingConnTicketIds;
	}

	public  void setCustomerRejectingConnTicketIds(
			List<Long> customerRejectingConnTicketIds)
	{
		this.customerRejectingConnTicketIds = customerRejectingConnTicketIds;
	}

	public  List<Long> getPermissionIssueTicketIds()
	{
		return permissionIssueTicketIds;
	}

	public  void setPermissionIssueTicketIds(
			List<Long> permissionIssueTicketIds)
	{
		this.permissionIssueTicketIds = permissionIssueTicketIds;
	}


	
}
