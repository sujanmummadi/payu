package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

public class CustomerAppointmentIssue {
	
	private String	priorityLevel;
	private Long dateNotMentioned;
	private Long ftc;
	private Long phoneRingingNoResponse;
	
	private List<Long> dateNotMentionedTicketIds;
	private List<Long> ftcTicketIds;
	private List<Long> phoneRingingNoResponseTicketIds;
	
	
	public CustomerAppointmentIssue()
	{
		this.dateNotMentioned = 0l;
		this.ftc = 0l;
		this.phoneRingingNoResponse = 0l;
		this.priorityLevel = "0";
	}

	public String getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(String priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Long getDateNotMentioned() {
		return dateNotMentioned;
	}

	public void setDateNotMentioned(Long dataToBeMentioned) {
		this.dateNotMentioned = dataToBeMentioned;
	}

	public Long getFtc() {
		return ftc;
	}

	public void setFtc(Long ftc) {
		this.ftc = ftc;
	}

	public Long getPhoneRingingNoResponse() {
		return phoneRingingNoResponse;
	}

	public void setPhoneRingingNoResponse(Long phoneRingingNoResponse) {
		this.phoneRingingNoResponse = phoneRingingNoResponse;
	}

	public  List<Long> getDateNotMentionedTicketIds()
	{
		return dateNotMentionedTicketIds;
	}

	public  void setDateNotMentionedTicketIds(
			List<Long> dataToBeMentionedTicketIds)
	{
		this.dateNotMentionedTicketIds = dataToBeMentionedTicketIds;
	}

	public  List<Long> getFtcTicketIds()
	{
		return ftcTicketIds;
	}

	public  void setFtcTicketIds(List<Long> ftcTicketIds)
	{
		this.ftcTicketIds = ftcTicketIds;
	}

	public  List<Long> getPhoneRingingNoResponseTicketIds()
	{
		return phoneRingingNoResponseTicketIds;
	}

	public  void setPhoneRingingNoResponseTicketIds(
			List<Long> phoneRingingNoResponseTicketIds)
	{
		this.phoneRingingNoResponseTicketIds = phoneRingingNoResponseTicketIds;
	}

	
	
}
