package com.cupola.fwmp.dao.ticket.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.ActivityStatusDefinition;
import com.cupola.fwmp.FWMPConstant.DashBoardStatusDefinition;
import com.cupola.fwmp.FWMPConstant.PriorityFilter;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.userAvailabilityStatus.UserAvailabilityStatusDAO;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.PriorityValue;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.SubActivityVo;
import com.cupola.fwmp.vo.TicketEtrVo;
import com.cupola.fwmp.vo.counts.RibbonCountSummary;
import com.cupola.fwmp.vo.counts.RibbonCountSummaryVO;
import com.cupola.fwmp.vo.counts.TLNEDetails;

/**
 * 
 * @author G Ashraf
 * 
 */

public class AppTicketCountUtil
{

	private static Map<String, Map<String, Object>> ticketCountMap = new HashMap<String, Map<String, Object>>();
	private static boolean isTicketIncludedIn = false;

	private static UserAvailabilityStatusDAO userLoginStatusDao;
	private static DefinitionCoreService definitionCoreService;
	private static UserDao userDao;
	
	private static Integer baseValue;
	
	public static void setBaseValue(Integer baseValue) {
		AppTicketCountUtil.baseValue = baseValue;
	}
	
	public static void setUserDao(UserDao userDao) {
		AppTicketCountUtil.userDao = userDao;
	}

	public static void setDefinitionCoreService(
			DefinitionCoreService definitionCoreService)
	{
		AppTicketCountUtil.definitionCoreService = definitionCoreService;
	}

	public static UserAvailabilityStatusDAO getUserLoginStatusDao()
	{
		return userLoginStatusDao;
	}

	public static void setUserLoginStatusDao(
			UserAvailabilityStatusDAO userLoginStatusDao)
	{
		AppTicketCountUtil.userLoginStatusDao = userLoginStatusDao;
	}
	
	public static Set<String> getDashBoardSymptomCode(String neSuffix)
	{
		Set<String> symptoms = null;
		Set<Long> priority = null;
		if("Priority".equals(neSuffix))
		{
			priority = new HashSet<Long>( PriorityFilter.definition.keySet());
			priority.remove(-1l);
			symptoms = new HashSet<String>();
			for( Long val : priority)
				symptoms.add(val+"");
		}
		else if ("SrElement".equals(neSuffix))
		{
			symptoms = definitionCoreService.
					 getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_SR_SYMPTOMS) ;
		}
		else
		 symptoms = "CxUp".equals(neSuffix) ? definitionCoreService.
				 getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_CX_UP_SYMPTOMS) 
				 : definitionCoreService.
				 getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_CX_DOWN_SYMPTOMS) ;
				 
		return symptoms;
	}
	
	public static Set<String> getDashBoardSymptomCode(String neSuffix, String cityName )
	{
		if( cityName == null|| cityName.isEmpty() 
				|| !cityName.equalsIgnoreCase("HYD"))
			
			return getDashBoardSymptomCode( neSuffix );
					
		Set<String> symptoms = null;
		Set<Long> priority = null;
		if("Priority".equals(neSuffix))
		{
			priority = new HashSet<Long>( PriorityFilter.definition.keySet());
			priority.remove(-1l);
			symptoms = new HashSet<String>();
			for( Long val : priority)
				symptoms.add(val+"");
		}
		else if ("SrElement".equals(neSuffix))
		{
			symptoms = definitionCoreService.
					 getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_SR_SYMPTOMS_HYD) ;
		}
		else
		 symptoms = "CxUp".equals(neSuffix) ? definitionCoreService.
				 getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_CX_UP_SYMPTOMS_HYD) 
				 : definitionCoreService.
				 getClassificationProperty(DefinitionCoreService.DASHBOARD_FR_CX_DOWN_SYMPTOMS_HYD) ;
				 
		return symptoms;
	}

	private static Map<String, Object> initialzeAllObjects(String type)
	{
		Map<String, Object> map = new TreeMap<String, Object>();
		String key = null;
		for (int i = 1; i <= 4; i++)
		{
			key = i + "";
			if (type.equals("WorkInProgress"))
			{
				WorkInProgress obje = new WorkInProgress();
				obje.setPriorityLevel(key);
				map.put(key, obje);
			} else if (type.equals("SalesRelatedIssue"))
			{
				SalesRelatedIssue obje = new SalesRelatedIssue();
				obje.setPriorityLevel(key);
				map.put(key, obje);
			} else if (type.equals("ReachRevert"))
			{
				ReachRevert obje = new ReachRevert();
				obje.setPriorityLevel(key);
				map.put(key, obje);
			} else if (type.equals("LCOIssue"))
			{
				LCOIssue obje = new LCOIssue();
				obje.setPriorityLevel(key);
				map.put(key, obje);
			} else if (type.equals("CustomerEndIssue"))
			{
				CustomerEndIssue obje = new CustomerEndIssue();
				obje.setPriorityLevel(key);
				map.put(key, obje);
			} else if (type.equals("CustomerAppointmentIssue"))
			{
				CustomerAppointmentIssue obje = new CustomerAppointmentIssue();
				obje.setPriorityLevel(key);
				map.put(key, obje);
			}
		}
		return map;
	}

	public static String getPriorityLevel(long priorityValue)
	{
		Map<Long, PriorityValue> prioritydef = definitionCoreService
				.getPriorityDefinitions();

		for (Map.Entry<Long, PriorityValue> priorityRange : prioritydef
				.entrySet())
		{
			if (priorityValue >= priorityRange.getValue().getStart()
					&& priorityValue <= priorityRange.getValue().getEnd())
				return priorityRange.getKey() + "";
		}

		return null;
	}
	
	private static WorkInProgress getWIPCountNew(WorkInProgress progress,
			List<Long> activities, Long currentWorkStage, TicketSummary summary)
	{
		long count = 0;
		
		
		if( currentWorkStage.equals(WorkStageType.FIBER ) && 
				!activities.contains(Activity.CUSTOMER_ACCOUNT_ACTIVATION) )
		{
			if(  activities.containsAll(DashBoardStatusDefinition
					.FIBER_TYPE_ACTIVATION_PENDING) )
			{
				count = progress.getActivationPending();
				count++;
				progress.setActivationPending(count);

				List<Long> ticketIds = progress.getActivationPendingTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setActivationPendingTicketIds(ticketIds);
			}
			else if( activities.containsAll(DashBoardStatusDefinition
					.FIBER_TYPE_COPPER_PENDING))
			{
				count = progress.getCoperPending();
				count++;
				progress.setCoperPending(count);

				List<Long> ticketIds = progress.getCoperPendingTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setCoperPendingTicketIds(ticketIds);
				
			}
			else if( activities.containsAll(
					DashBoardStatusDefinition.FIBER_LAID_RACK_FIXED_SPLICING_ACTIVATION_PENDING))
			{
				count = progress.getFiberLaidRackFixedSplActiPending();
				count++;
				progress.setFiberLaidRackFixedSplActiPending(count);

				List<Long> ticketIds = progress
						.getFiberLaidRackFixedSplActiPendingTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setFiberLaidRackFixedSplActiPendingTicketIds(ticketIds);
			}
			else if (activities.containsAll(
					DashBoardStatusDefinition.FIBER_DONE_RACK_SPLICING_PENDING))
			{
				count = progress.getFiberDoneRackSplicingPending();
				count++;
				progress.setFiberDoneRackSplicingPending(count);

				List<Long> ticketIds = progress
						.getFiberDoneRackSplicingPendingTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setFiberDoneRackSplicingPendingTicketIds(ticketIds);
			}
			
			else if (activities
					.containsAll(DashBoardStatusDefinition.FIBER_TYPE_FIBER_PENDING)
					|| activities.isEmpty()
					|| activities.size() == 0)

			{
				
				count = progress.getFiberYetToStart();
				count++;
				progress.setFiberYetToStart(count);

				List<Long> ticketIds = progress.getFiberYetToStartTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setFiberYetToStartTicketIds(ticketIds);
			}

			else if (activities.size() == 1
					&& activities.contains(0l))

			{
				
				count = progress.getFiberYetToStart();
				count++;
				progress.setFiberYetToStart(count);

				List<Long> ticketIds = progress.getFiberYetToStartTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setFiberYetToStartTicketIds(ticketIds);
			}
			
			else if ( activities.containsAll(DashBoardStatusDefinition.FIBER_TYPE_ONLY_RACK_FIXED) 
					&& !activities.removeAll(Arrays.asList(6l, 7l))) 
			{

				count = progress.getRackFixedFiberSplicingPending();
				count++;
				progress.setRackFixedFiberSplicingPending(count);

				List<Long> ticketIds = progress
						.getRackFixedFiberSplicingPendingTicketIds();
				if (ticketIds == null) {
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setRackFixedFiberSplicingPendingTicketIds(ticketIds);

			}
		}
		
		else if( currentWorkStage.equals(WorkStageType.COPPER) && !activities.contains(Activity.CUSTOMER_ACCOUNT_ACTIVATION))
		{
			if (activities
					.containsAll(DashBoardStatusDefinition.COPPER_TYPE_ACTIVATION_PENDING) )

			{
				count = progress.getActivationPending();
				count++;
				progress.setActivationPending(count);

				List<Long> ticketIds = progress.getActivationPendingTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setActivationPendingTicketIds(ticketIds);
				

			} else if (activities
					.containsAll(DashBoardStatusDefinition.COPPER_TYPE_COPPER_PENDING)
					|| activities.isEmpty()
					|| activities.size() == 0)

			{
				count = progress.getCoperPending();
				count++;
				progress.setCoperPending(count);

				List<Long> ticketIds = progress.getCoperPendingTicketIds();
				if (ticketIds == null)
				{
					ticketIds = new ArrayList<Long>();
				}
				ticketIds.add(summary.getTicketId());
				progress.setCoperPendingTicketIds(ticketIds);
			} 
		}
		
		return progress;
	}

	private static WorkInProgress getWIPCount(WorkInProgress progress,
			List<Long> activities, Long currentWorkStage, TicketSummary summary)
	{
		long count = 0;
		if (activities.isEmpty()
				&& currentWorkStage.equals(WorkStageType.COPPER))
		{
			isTicketIncludedIn = true;
			count = progress.getCoperPending();
			count++;
			progress.setCoperPending(count);

			List<Long> ticketIds = progress.getCoperPendingTicketIds();
			if (ticketIds == null)
			{
				ticketIds = new ArrayList<Long>();
			}
			ticketIds.add(summary.getTicketId());
			progress.setCoperPendingTicketIds(ticketIds);

		} else if (ActivityStatusDefinition.COPPER_PENDING
				.containsAll(activities)
				&& currentWorkStage.equals(WorkStageType.FIBER)
				&& activities.size() == ActivityStatusDefinition.COPPER_PENDING
						.size())
		{
			isTicketIncludedIn = true;
			count = progress.getCoperPending();
			count++;
			progress.setCoperPending(count);

			List<Long> ticketIds = progress.getCoperPendingTicketIds();
			if (ticketIds == null)
			{
				ticketIds = new ArrayList<Long>();
			}
			ticketIds.add(summary.getTicketId());
			progress.setCoperPendingTicketIds(ticketIds);

		} else if (ActivityStatusDefinition.FIBER_DONE_RACK_SPLICING_PENDING
				.containsAll(activities)
				&& currentWorkStage.equals(WorkStageType.FIBER)
				&& activities.size() == ActivityStatusDefinition.FIBER_DONE_RACK_SPLICING_PENDING
						.size())
		{
			isTicketIncludedIn = true;
			count = progress.getFiberDoneRackSplicingPending();
			count++;
			progress.setFiberDoneRackSplicingPending(count);

			List<Long> ticketIds = progress
					.getFiberDoneRackSplicingPendingTicketIds();
			if (ticketIds == null)
			{
				ticketIds = new ArrayList<Long>();
			}
			ticketIds.add(summary.getTicketId());
			progress.setFiberDoneRackSplicingPendingTicketIds(ticketIds);

		} else if (ActivityStatusDefinition.FIBER_LAID_RACK_FIXED_SPLICING_ACTIVATION_PENDING
				.containsAll(activities)
				&& currentWorkStage.equals(WorkStageType.FIBER)
				&& activities.size() == ActivityStatusDefinition.FIBER_LAID_RACK_FIXED_SPLICING_ACTIVATION_PENDING
						.size())
		{
			isTicketIncludedIn = true;
			count = progress.getFiberLaidRackFixedSplActiPending();
			count++;
			progress.setFiberLaidRackFixedSplActiPending(count);

			List<Long> ticketIds = progress
					.getFiberLaidRackFixedSplActiPendingTicketIds();
			if (ticketIds == null)
			{
				ticketIds = new ArrayList<Long>();
			}
			ticketIds.add(summary.getTicketId());
			progress.setFiberLaidRackFixedSplActiPendingTicketIds(ticketIds);
		} else if (activities.isEmpty()
				&& currentWorkStage.equals(WorkStageType.FIBER))
		{
			isTicketIncludedIn = true;
			count = progress.getFiberYetToStart();
			count++;
			progress.setFiberYetToStart(count);

			List<Long> ticketIds = progress.getFiberYetToStartTicketIds();
			if (ticketIds == null)
			{
				ticketIds = new ArrayList<Long>();
			}
			ticketIds.add(summary.getTicketId());
			progress.setFiberYetToStartTicketIds(ticketIds);

		} else
		{
			isTicketIncludedIn = false;
		}
		return progress;
	}

	public static void resetAndCreateCountMap()
	{
		ticketCountMap.clear();
		isTicketIncludedIn = false;
		ticketCountMap
				.put("WORK_IN_PROGRESS", initialzeAllObjects("WorkInProgress"));
		ticketCountMap
				.put("SALE_RELATED_ISSUE", initialzeAllObjects("SalesRelatedIssue"));
		ticketCountMap.put("REACH_REVERT", initialzeAllObjects("ReachRevert"));
		ticketCountMap.put("LCO_ISSUE", initialzeAllObjects("LCOIssue"));
		ticketCountMap
				.put("CUSTOMER_END_ISSUE", initialzeAllObjects("CustomerEndIssue"));
		ticketCountMap
				.put("CUSTOMER_APPOINTMENT_ISSUE", initialzeAllObjects("CustomerAppointmentIssue"));
	}

	public static boolean createWorkInProgressCount(TicketSummary summary,
			List<Long> compAcitvity)
	{
		WorkInProgress progress = null;
		Map<String, Object> progressMap = ticketCountMap
				.get("WORK_IN_PROGRESS");
		
		Set<Long> complActivity = new HashSet<Long>(compAcitvity);
		compAcitvity = new ArrayList<>(complActivity);

		String priorityLevel = getPriorityLevel(summary.getPriorityValue());

		if (priorityLevel != null)
		{
			progress = (WorkInProgress) progressMap.get(priorityLevel);
			progress.setPriorityLevel(priorityLevel);
			progress = getWIPCountNew(progress, compAcitvity, summary.getCurrentWorkStage(), summary);
			progressMap.put(priorityLevel, progress);
			ticketCountMap.put("WORK_IN_PROGRESS", progressMap);
		}

		return isTicketIncludedIn;
	}
	private static ReachRevert getReachRevertIssueCount(
			TicketSummary summary, ReachRevert issue, TicketEtrVo etr)
	{
		long count = 0;
		 if ( etr != null && "5065".equals(etr.getReasonCode()))
		{
			count = issue.getJeInstallationPending();
			count++;
			issue.setJeInstallationPending(count);
			if(issue.getJeInstallationPendingTicketIds() != null)	
			{
				issue.getJeInstallationPendingTicketIds().add(summary.getTicketId());
			}
			else
			{
				issue.setJeInstallationPendingTicketIds(new ArrayList<Long>());
				issue.getJeInstallationPendingTicketIds().add(summary.getTicketId());
			}
				
		 
		}
		return issue;
	}
	private static SalesRelatedIssue getSalesRelatedIssueCount(
			TicketSummary summary, SalesRelatedIssue salesRelated,TicketEtrVo etr)
	{
		long count = 0;
		if (summary.getStatus() != null
				&& summary.getStatus()
						.equals(TicketStatus.PERMISSION_PENDING_BY_SALES))
		{
			count = salesRelated.getPermissionIssue();
			count++;
			salesRelated.setPermissionIssue(count);
		}
		if ( etr != null && "5064".equals(etr.getReasonCode()))
		{
			count = salesRelated.getCustomerRejectingConn();
			count++;
			salesRelated.setCustomerRejectingConn(count);
			if(salesRelated.getCustomerRejectingConnTicketIds() != null)	
			{
				salesRelated.getCustomerRejectingConnTicketIds().add(summary.getTicketId());
			}
			else
			{
				salesRelated.setCustomerRejectingConnTicketIds(new ArrayList<Long>());
				salesRelated.getCustomerRejectingConnTicketIds().add(summary.getTicketId());
			}
				
		}
		
		return salesRelated;
	}
	private static LCOIssue getLCOIssueCount(
			TicketSummary summary, LCOIssue issue ,TicketEtrVo etr )
	{
		long count = 0;
		if ( etr != null && "5051".equals(etr.getReasonCode()))
		{
			count = issue.getLcoIssue();
			count++;
			issue.setLcoIssue(count);
			if(issue.getLcoIssueTicketIds() != null)	
			{
				issue.getLcoIssueTicketIds().add(summary.getTicketId());
			}
			else
			{
				issue.setLcoIssueTicketIds(new ArrayList<Long>());
				issue.getLcoIssueTicketIds().add(summary.getTicketId());
			}
				
		}
		return issue;
	}
	private static CustomerAppointmentIssue getCustomerAppointmentIssueCount(
			TicketSummary summary, CustomerAppointmentIssue issue ,TicketEtrVo etr )
	{
		long count = 0;
		
		if(etr != null )
		{
		if (  "5057".equals(etr.getReasonCode()) ||   "5058".equals(etr.getReasonCode()))
		{
			count = issue.getPhoneRingingNoResponse();
			count++;
			issue.setPhoneRingingNoResponse(count);
			if(issue.getPhoneRingingNoResponseTicketIds() != null)	
			{
				issue.getPhoneRingingNoResponseTicketIds().add(summary.getTicketId());
			}
			else
			{
				issue.setPhoneRingingNoResponseTicketIds(new ArrayList<Long>());
				issue.getPhoneRingingNoResponseTicketIds().add(summary.getTicketId());
			}
				
		}
		else if (  "5062".equals(etr.getReasonCode())  )
		{
			count = issue.getDateNotMentioned();
			count++;
			issue.setDateNotMentioned(count);
			if(issue.getDateNotMentionedTicketIds() != null)	
			{
				issue.getDateNotMentionedTicketIds().add(summary.getTicketId());
			}
			else
			{
				issue.setDateNotMentionedTicketIds(new ArrayList<Long>());
				issue.getDateNotMentionedTicketIds().add(summary.getTicketId());
			}
				
		}
		else if (  "5063".equals(etr.getReasonCode())  )
		{
			count = issue.getFtc();
			count++;
			issue.setFtc(count);
			if(issue.getFtcTicketIds() != null)	
			{
				issue.getFtcTicketIds().add(summary.getTicketId());
			}
			else
			{
				issue.setFtcTicketIds(new ArrayList<Long>());
				issue.getFtcTicketIds().add(summary.getTicketId());
			}
				
		}
		}
		return issue;
	}
	public static void createSalesRelatedIssueCount(TicketSummary summary,TicketEtrVo etr)
	{
		SalesRelatedIssue salesRelated = null;
		Map<String, Object> progressMap = ticketCountMap
				.get("SALE_RELATED_ISSUE");

		String priorityLevel = getPriorityLevel(summary.getPriorityValue());

		if (priorityLevel != null)
		{
			salesRelated = (SalesRelatedIssue) progressMap.get(priorityLevel);
			salesRelated.setPriorityLevel(priorityLevel);
			salesRelated = getSalesRelatedIssueCount(summary, salesRelated,etr);
			progressMap.put(priorityLevel, salesRelated);
			ticketCountMap.put("SALE_RELATED_ISSUE", progressMap);
		}
	}

	public static ReachRevert createReachRevertCount(TicketSummary summary,TicketEtrVo etr)
	{
		ReachRevert revert = new ReachRevert();
		
		
		Map<String, Object> progressMap = ticketCountMap
				.get("REACH_REVERT");

		String priorityLevel = getPriorityLevel(summary.getPriorityValue());

		if (priorityLevel != null)
		{
			revert = (ReachRevert) progressMap.get(priorityLevel);
			revert.setPriorityLevel(priorityLevel);
			revert = getReachRevertIssueCount(summary, revert,etr);
			progressMap.put(priorityLevel, revert);
			ticketCountMap.put("REACH_REVERT", progressMap);
		}
		

		return revert;
	}

	public static LCOIssue createLcoIssueCount(TicketSummary summary, TicketEtrVo ticketEtrVo)
	{
		LCOIssue issue = new LCOIssue();
		if(ticketEtrVo != null)
		{
			Map<String, Object> progressMap = ticketCountMap
					.get("LCO_ISSUE");

			String priorityLevel = getPriorityLevel(summary.getPriorityValue());

			if (priorityLevel != null)
			{
				issue = (LCOIssue) progressMap.get(priorityLevel);
				issue.setPriorityLevel(priorityLevel);
				issue = getLCOIssueCount(summary, issue,ticketEtrVo);
				progressMap.put(priorityLevel, issue);
				ticketCountMap.put("LCO_ISSUE", progressMap);
			}
			
		}
		
		 

		return issue;
	}
	
	public static CustomerEndIssue createCustomerEndIssueCount(
			TicketSummary summary, TicketEtrVo ticketEtrVo)
	{
		
		CustomerEndIssue issue = new CustomerEndIssue();
		if(ticketEtrVo != null)
		{
			Map<String, Object> progressMap = ticketCountMap
					.get("CUSTOMER_END_ISSUE");

			String priorityLevel = getPriorityLevel(summary.getPriorityValue());

			if (priorityLevel != null)
			{
				issue = (CustomerEndIssue) progressMap.get(priorityLevel);
				issue.setPriorityLevel(priorityLevel);
				issue = getCustomerEndIssueCount(summary, issue,ticketEtrVo);
				progressMap.put(priorityLevel, issue);
				ticketCountMap.put("CUSTOMER_END_ISSUE", progressMap);
			}
			
		}
		return issue;
	}

	public static CustomerEndIssue getCustomerEndIssueCount(
			TicketSummary summary, CustomerEndIssue issue ,TicketEtrVo etr )
	{
		
		long count = 0;
		if ( etr != null && "5061".equals(etr.getReasonCode()) )
		{
			count = issue.getInternalCableIssue();
			count++;
			issue.setInternalCableIssue(count);
			if(issue.getInternalCableIssue() != null)	
			{
				issue.getInternalCableIssueTicketIds().add(summary.getTicketId());
			}
			else
			{
				issue.setInternalCableIssueTicketIds(new ArrayList<Long>());
				issue.getInternalCableIssueTicketIds().add(summary.getTicketId());
			}
				
		}
		else if ( etr != null && "5060".equals(etr.getReasonCode()))
		{
			count = issue.getInternalCableIssue();
			count++;
			issue.setCustomerPcRouterIssue(count);
			if(issue.getInternalCableIssue() != null)	
			{
				issue.getCustomerPcRouterIssueTicketIds().add(summary.getTicketId());
			}
			else
			{
				issue.setCustomerPcRouterIssueTicketIds(new ArrayList<Long>());
				issue.getCustomerPcRouterIssueTicketIds().add(summary.getTicketId());
			}
				
		}
		return issue;
		
	}

	public static CustomerAppointmentIssue createCustomerAppointmentIssueCount(
			TicketSummary summary, TicketEtrVo ticketEtrVo)
	{
		
		
		CustomerAppointmentIssue issue = new CustomerAppointmentIssue();
		if(ticketEtrVo != null)
		{
			Map<String, Object> progressMap = ticketCountMap
					.get("CUSTOMER_APPOINTMENT_ISSUE");

			String priorityLevel = getPriorityLevel(summary.getPriorityValue());

			if (priorityLevel != null)
			{
				issue = (CustomerAppointmentIssue) progressMap.get(priorityLevel);
				issue.setPriorityLevel(priorityLevel);
				issue = getCustomerAppointmentIssueCount(summary, issue,ticketEtrVo);
				progressMap.put(priorityLevel, issue);
				ticketCountMap.put("CUSTOMER_APPOINTMENT_ISSUE", progressMap);
			}
			
		}
		
		 

		return issue;
	}

	public static AppSummaryVO getAppSummaryVO()
	{
		Set<String> keys = ticketCountMap.keySet();
		Map<String, Object> progressMap = null;
		List<WorkInProgress> progressList = new ArrayList<WorkInProgress>();
		List<SalesRelatedIssue> salesIssueList = new ArrayList<SalesRelatedIssue>();
		List<CustomerAppointmentIssue> custAppList = new ArrayList<CustomerAppointmentIssue>();
		List<CustomerEndIssue> cusEndList = new ArrayList<CustomerEndIssue>();
		List<LCOIssue> lcoList = new ArrayList<LCOIssue>();
		List<ReachRevert> reachList = new ArrayList<ReachRevert>();

		for (String key : keys)
		{
			progressMap = ticketCountMap.get(key);
			if (progressMap != null && progressMap.size() > 0)
			{
				Collection<Object> list = progressMap.values();
				for (Iterator<Object> itr = list.iterator(); itr.hasNext();)
				{
					Object genericObject = itr.next();
					if (genericObject instanceof WorkInProgress)
					{
						WorkInProgress progress = (WorkInProgress) genericObject;
						progressList.add(progress);
					} else if (genericObject instanceof SalesRelatedIssue)
					{
						SalesRelatedIssue saleIssue = (SalesRelatedIssue) genericObject;
						salesIssueList.add(saleIssue);
					} else if (genericObject instanceof ReachRevert)
					{
						ReachRevert reach = (ReachRevert) genericObject;
						reachList.add(reach);
					} else if (genericObject instanceof LCOIssue)
					{
						LCOIssue lco = (LCOIssue) genericObject;
						lcoList.add(lco);
					} else if (genericObject instanceof CustomerEndIssue)
					{
						CustomerEndIssue cust = (CustomerEndIssue) genericObject;
						cusEndList.add(cust);
					} else if (genericObject instanceof CustomerAppointmentIssue)
					{
						CustomerAppointmentIssue app = (CustomerAppointmentIssue) genericObject;
						custAppList.add(app);
					}
				}
			}
		}
		AppSummaryVO summaryVO = new AppSummaryVO();
		summaryVO.setWiProgress(progressList);
		summaryVO.setSrIssues(salesIssueList);
		summaryVO.setReachReverts(reachList);
		summaryVO.setLcoIssue(lcoList);
		summaryVO.setCustEndIssue(cusEndList);
		summaryVO.setCustAppointIssue(custAppList);
		return summaryVO;
	}

	private static Map<Long, List<RibbonCountSummary>> getTlneDetailsMap(
			List<RibbonCountSummary> list)
	{
		Map<Long, List<RibbonCountSummary>> neTicketMap = new HashMap<Long, List<RibbonCountSummary>>();
		List<RibbonCountSummary> neticketList = null;

		Long userId = null;
		for (RibbonCountSummary summary : list)
		{
			userId = summary.getUserId();
			if (neTicketMap.containsKey(userId))
			{
				List<RibbonCountSummary> oldRibbon = neTicketMap.get(userId);
				oldRibbon.add(summary);
				neTicketMap.put(userId, oldRibbon);
			} else
			{
				neticketList = new ArrayList<RibbonCountSummary>();
				neticketList.add(summary);
				neTicketMap.put(userId, neticketList);
			}
		}
		return neTicketMap;
	}

	public static List<RibbonCountSummaryVO> getRibbonCountSummaryVO(
			List<RibbonCountSummary> list)
	{
		Map<Long, List<RibbonCountSummary>> neTicketMap = getTlneDetailsMap(list);
		return getRibbonSummary(neTicketMap);
	}

	private static List<RibbonCountSummaryVO> getRibbonSummary(
			Map<Long, List<RibbonCountSummary>> neTicketMap)
	{
		List<RibbonCountSummaryVO> countSummaryVO = new ArrayList<RibbonCountSummaryVO>();
		RibbonCountSummaryVO summary = null;

		Set<Long> keys = neTicketMap.keySet();
		for (Long key : keys)
		{
			summary = getRibbonCountSummary(neTicketMap.get(key));
			countSummaryVO.add(summary);
		}
		return countSummaryVO;
	}

	private static RibbonCountSummaryVO getRibbonCountSummary(
			List<RibbonCountSummary> list)
	{
		RibbonCountSummaryVO summary = new RibbonCountSummaryVO();
		long wipCount = 0;
		long sriCount = 0;
		long rriCount = 0;
		long lcoCount = 0;
		long ceiCount = 0;
		long caiCount = 0;

		for (RibbonCountSummary countSummary : list)
		{
			if (ActivityStatusDefinition.INPROGRESS_DEF.contains(countSummary
					.getTicketStatus()))
			{
				wipCount++;
			}

			if (ActivityStatusDefinition.SALES_RELATED_ISSUE_DEF
					.contains(countSummary.getTicketStatus()))
			{
				sriCount++;
			}

			if (ActivityStatusDefinition.LCO_ISSUE_DEF.contains(countSummary
					.getTicketStatus()))
			{
				lcoCount++;
			}

			if (ActivityStatusDefinition.REACH_REVERT_DEF.contains(countSummary
					.getTicketStatus()))
			{
				rriCount++;
			}

			if (ActivityStatusDefinition.CUSTOMER_END_ISSUE_DEF
					.contains(countSummary.getTicketStatus()))
			{
				ceiCount++;
			}

			if (ActivityStatusDefinition.CUSTOMER_APPOINT_ISSUE_DEF
					.contains(countSummary.getTicketStatus()))
			{
				caiCount++;
			}
		}

		summary.setUserId(list.get(0).getUserId());
		summary.setUserName(list.get(0).getFirstName());
		summary.setWorkInProgress(Integer.parseInt(wipCount + ""));
		summary.setSalesIssues(Integer.parseInt(sriCount + ""));
		summary.setReachReverted(Integer.parseInt(rriCount + ""));
		summary.setLcoIssues(Integer.parseInt(lcoCount + ""));
		summary.setCustomerEndIssues(Integer.parseInt(ceiCount + ""));
		summary.setCustomerAppoinmentIssues(Integer.parseInt(caiCount + ""));

		return summary;
	}
	
	private static Map<String,List<Long>> listOfIds = new HashMap<String,List<Long>>();
	
	private static void clearListOfIdsForCategory()
	{
		listOfIds.clear();
		listOfIds.put("FICAT", new ArrayList<Long>());
		listOfIds.put("COCAT", new ArrayList<Long>());
		listOfIds.put("FECAT", new ArrayList<Long>());
		listOfIds.put("PRCAT", new ArrayList<Long>());
		listOfIds.put("TOCAT", new ArrayList<Long>());
	}
	
	private static void addTicketIdsToCategory(Long ticketId, String category)
	{
		if(ticketId != null && ticketId > 0)
		{
			List<Long> ids = listOfIds.get(category);
			ids.add(ticketId);
			listOfIds.put(category, ids);
		}
	}

	private static TLNEDetails getTLNEDetailsCount(
			List<RibbonCountSummary> summary)
	{
		TLNEDetails details = new TLNEDetails();
		long totalTickets = summary.size();
		long totalCopperTickets = 0;
		long totalFiberTickets = 0;
		long priorityTicketsCount = 0;
		long completedTickets = 0;
		
		long feasibilityTickets = 0;
		clearListOfIdsForCategory();
		
		for (RibbonCountSummary ribbon : summary)
		{		
			if(ribbon.getTicketId() == null || ribbon.getTicketId()  == 0)
			{
				totalTickets = 0;
				continue;
			}
			addTicketIdsToCategory(ribbon.getTicketId(), "TOCAT");
			if (ribbon.getTicketStatus() != null
					&& ActivityStatusDefinition.COMPLETED_DEF.contains(ribbon
							.getTicketStatus()))
				completedTickets++;
			
			if (ribbon.getTicketStatus() != null
					&& TicketStatus.FEASIBILITY_STATUS_LIST.contains(ribbon.getTicketStatus()))
			{
				feasibilityTickets++;
				addTicketIdsToCategory(ribbon.getTicketId(), "FECAT");
			}
			else{
				if (ribbon.getInitialWorkStage() != null
						&& ribbon.getInitialWorkStage().equals(WorkStageType.COPPER))
				{
					totalCopperTickets++;
					addTicketIdsToCategory(ribbon.getTicketId(), "COCAT");
				}
				else if (ribbon.getInitialWorkStage() != null
						&& ribbon.getInitialWorkStage().equals(WorkStageType.FIBER))
				{
					totalFiberTickets++;
					addTicketIdsToCategory(ribbon.getTicketId(), "FICAT");
				}
			}

			if (ribbon.getTickePriority() != null
					&& ribbon.getTickePriority() >= baseValue)
			{
				priorityTicketsCount++;
				addTicketIdsToCategory(ribbon.getTicketId(), "PRCAT");
			}
		}

		details.setNeName(summary.get(0).getFirstName());
		details.setNeUserId(summary.get(0).getUserId());
		details.setFiberTicketsCount(totalFiberTickets);
		details.setCopperTicketsCount(totalCopperTickets);
		details.setFeasibilityTicketsCount(feasibilityTickets);
		details.setPriorityTicketsCount(priorityTicketsCount);
		details.setTotalTicketsCount(totalTickets);
		details.setCopperTicketIds(listOfIds.get("COCAT"));
		details.setFiberTicketIds(listOfIds.get("FICAT"));
		details.setFeasibilityTicketIds(listOfIds.get("FECAT"));
		details.setTotalTicketIds(listOfIds.get("TOCAT"));
		details.setPriorityTicketIds(listOfIds.get("PRCAT"));
		int online = userLoginStatusDao
				.isUserOnLine(summary.get(0).getUserId()) ? 1 : 0;
		details.setNeStatus(online);
		
		return details;
	}

	public static List<TLNEDetails> getTlneTicketCounts(
			List<RibbonCountSummary> summary)
	{
		Map<Long, List<RibbonCountSummary>> neTicketMap = getTlneDetailsMap(summary);
		List<TLNEDetails> tlneDetails = new ArrayList<TLNEDetails>();
		Set<Long> keys = neTicketMap.keySet();
		for (Long key : keys)
			tlneDetails.add(getTLNEDetailsCount(neTicketMap.get(key)));
		
		tlneDetails.addAll(getNeForTlnotHavingTicket(keys));
		return tlneDetails;
	}
	
	private static List<TLNEDetails> getNeForTlnotHavingTicket(Set<Long> keys)
	{
		Map<Long, String> users = userDao.getReportingUsers(AuthUtils.getCurrentUserId());
		
		if(users !=null)
			users.keySet().removeAll(keys);
	
		List<TLNEDetails> tlneNotHavingTickets = new ArrayList<TLNEDetails>();
		TLNEDetails neDetails = null;
		if( users != null && !users.isEmpty())
		{
			Set<Long> tlneNotHavingTicket = users.keySet();
			for(Long neId : tlneNotHavingTicket)
			{
				int online = userLoginStatusDao.isUserOnLine(neId) ? 1 : 0;
				neDetails = new TLNEDetails(users.get(neId),neId, 0l,0l, 0l, 0l,0l, online);
				tlneNotHavingTickets.add(neDetails);
			}
		}
		return tlneNotHavingTickets;
	}
}
