package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

public class LCOIssue {
	private String	priorityLevel;
	private long lcoIssue;
	 
	private List<Long> lcoIssueTicketIds;
	
	public LCOIssue(){
		this.priorityLevel = "0";
	}

	public String getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(String priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public long getLcoIssue()
	{
		return lcoIssue;
	}

	public void setLcoIssue(long lcoIssue)
	{
		this.lcoIssue = lcoIssue;
	}

	public  List<Long> getLcoIssueTicketIds()
	{
		return lcoIssueTicketIds;
	}

	public  void setLcoIssueTicketIds(List<Long> lcoIssueTicketIds)
	{
		this.lcoIssueTicketIds = lcoIssueTicketIds;
	}




}
