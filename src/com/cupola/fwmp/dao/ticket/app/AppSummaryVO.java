package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

public class AppSummaryVO {
	
	private List<WorkInProgress> wiProgress;
	private List<SalesRelatedIssue> srIssues;
	private List<ReachRevert> reachReverts;
	private List<LCOIssue> lcoIssue;
	private List<CustomerEndIssue> custEndIssue;
	private List<CustomerAppointmentIssue> custAppointIssue;
	
	public AppSummaryVO(){}

	public List<WorkInProgress> getWiProgress() {
		return wiProgress;
	}

	public void setWiProgress(List<WorkInProgress> wiProgress) {
		this.wiProgress = wiProgress;
	}

	public List<SalesRelatedIssue> getSrIssues() {
		return srIssues;
	}

	public void setSrIssues(List<SalesRelatedIssue> srIssues) {
		this.srIssues = srIssues;
	}

	public List<ReachRevert> getReachReverts() {
		return reachReverts;
	}

	public void setReachReverts(List<ReachRevert> reachReverts) {
		this.reachReverts = reachReverts;
	}

	public List<LCOIssue> getLcoIssue() {
		return lcoIssue;
	}

	public void setLcoIssue(List<LCOIssue> lcoIssue) {
		this.lcoIssue = lcoIssue;
	}

	public List<CustomerEndIssue> getCustEndIssue() {
		return custEndIssue;
	}

	public void setCustEndIssue(List<CustomerEndIssue> custEndIssue) {
		this.custEndIssue = custEndIssue;
	}

	public List<CustomerAppointmentIssue> getCustAppointIssue() {
		return custAppointIssue;
	}

	public void setCustAppointIssue(List<CustomerAppointmentIssue> custAppointIssue) {
		this.custAppointIssue = custAppointIssue;
	}
}
