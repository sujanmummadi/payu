package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

public class WorkInProgress {
	
	private String priorityLevel;
	private long cat5FromNewCX;
	private Long coperPending;
	private Long fiberDoneRackSplicingPending;
	private Long fiberLaidRackFixedSplActiPending;
	private Long fiberYetToStart;
	private long activationPending;
	private long	rackFixedFiberSplicingPending;
	
	private List<Long> coperPendingTicketIds;
	private List<Long> fiberDoneRackSplicingPendingTicketIds;
	private List<Long> fiberLaidRackFixedSplActiPendingTicketIds;
	private List<Long> fiberYetToStartTicketIds;
	private List<Long> othersTicketIds;
	private List<Long> activationPendingTicketIds;
	private List<Long>	rackFixedFiberSplicingPendingTicketIds;
	
	public long getCat5FromNewCX()
	{
		return cat5FromNewCX;
	}

	public void setCat5FromNewCX(long cat5FromNewCX)
	{
		this.cat5FromNewCX = cat5FromNewCX;
	}

	public long getActivationPending()
	{
		return activationPending;
	}

	public void setActivationPending(long activationPending)
	{
		this.activationPending = activationPending;
	}

	public WorkInProgress(){
		this.coperPending = 0l;
		this.fiberDoneRackSplicingPending = 0l;
		this.fiberLaidRackFixedSplActiPending = 0l;
		this.fiberYetToStart = 0l;
		this.priorityLevel = "0";
	}

	public String getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(String priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Long getCoperPending() {
		return coperPending;
	}

	public void setCoperPending(Long coperPending) {
		this.coperPending = coperPending;
	}

	public Long getFiberDoneRackSplicingPending() {
		return fiberDoneRackSplicingPending;
	}

	public void setFiberDoneRackSplicingPending(Long fiberDoneRackSplicingPending) {
		this.fiberDoneRackSplicingPending = fiberDoneRackSplicingPending;
	}

	public Long getFiberLaidRackFixedSplActiPending() {
		return fiberLaidRackFixedSplActiPending;
	}

	public void setFiberLaidRackFixedSplActiPending(
			Long fiberLaidRackFixedSplActiPending) {
		this.fiberLaidRackFixedSplActiPending = fiberLaidRackFixedSplActiPending;
	}

	public Long getFiberYetToStart() {
		return fiberYetToStart;
	}

	public void setFiberYetToStart(Long fiberYetToStart) {
		this.fiberYetToStart = fiberYetToStart;
	}


	@Override
	public String toString() {
		return "WorkInProgress [priorityLevel=" + priorityLevel + ", coperPending="
				+ coperPending + ", fiberDoneRackSplicingPending="
				+ fiberDoneRackSplicingPending
				+ ", fiberLaidRackFixedSplActiPending="
				+ fiberLaidRackFixedSplActiPending + ", fiberYetToStart="
				+ fiberYetToStart  + "]";
	}

	public List<Long> getCoperPendingTicketIds()
	{
		return coperPendingTicketIds;
	}

	public void setCoperPendingTicketIds(List<Long> coperPendingTicketIds)
	{
		this.coperPendingTicketIds = coperPendingTicketIds;
	}

	public List<Long> getFiberDoneRackSplicingPendingTicketIds()
	{
		return fiberDoneRackSplicingPendingTicketIds;
	}

	public void setFiberDoneRackSplicingPendingTicketIds(
			List<Long> fiberDoneRackSplicingPendingTicketIds)
	{
		this.fiberDoneRackSplicingPendingTicketIds = fiberDoneRackSplicingPendingTicketIds;
	}

	public List<Long> getFiberLaidRackFixedSplActiPendingTicketIds()
	{
		return fiberLaidRackFixedSplActiPendingTicketIds;
	}

	public void setFiberLaidRackFixedSplActiPendingTicketIds(
			List<Long> fiberLaidRackFixedSplActiPendingTicketIds)
	{
		this.fiberLaidRackFixedSplActiPendingTicketIds = fiberLaidRackFixedSplActiPendingTicketIds;
	}

	public List<Long> getFiberYetToStartTicketIds()
	{
		return fiberYetToStartTicketIds;
	}

	public void setFiberYetToStartTicketIds(List<Long> fiberYetToStartTicketIds)
	{
		this.fiberYetToStartTicketIds = fiberYetToStartTicketIds;
	}

	public List<Long> getOthersTicketIds()
	{
		return othersTicketIds;
	}

	public void setOthersTicketIds(List<Long> othersTicketIds)
	{
		this.othersTicketIds = othersTicketIds;
	}

	public List<Long> getActivationPendingTicketIds() {
		return activationPendingTicketIds;
	}

	public void setActivationPendingTicketIds(List<Long> activationPendingTicketIds) {
		this.activationPendingTicketIds = activationPendingTicketIds;
	}

	public long getRackFixedFiberSplicingPending() {
		return rackFixedFiberSplicingPending;
	}

	public void setRackFixedFiberSplicingPending(long rackFixedFiberSplicingPending) {
		this.rackFixedFiberSplicingPending = rackFixedFiberSplicingPending;
	}

	public List<Long> getRackFixedFiberSplicingPendingTicketIds() {
		return rackFixedFiberSplicingPendingTicketIds;
	}

	public void setRackFixedFiberSplicingPendingTicketIds(
			List<Long> rackFixedFiberSplicingPendingTicketIds) {
		this.rackFixedFiberSplicingPendingTicketIds = rackFixedFiberSplicingPendingTicketIds;
	}
	
	
	
}
