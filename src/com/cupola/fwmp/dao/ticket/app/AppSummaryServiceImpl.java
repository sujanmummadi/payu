package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;

/**
 * 
 * @author ashraf
 *
 */

public class AppSummaryServiceImpl implements AppSummaryService
{

	@Autowired
	AppTicketSummaryDAO appDao;
	@Autowired
	UserDao userDao;
	public AppSummaryVO getTicketSummaryCount(Long userId,Long workOrderTypeId)
	{
		if(workOrderTypeId == null)
			return null;
		
		return AppTicketSummaryCache.getCountSummary(userId,workOrderTypeId);
	}

	@Override
	public List<Long> getCompletedTicketActivityIds(Long ticketId) {
		
		return appDao.getCompletedTicketActivityIds(ticketId);
	}

	@Override
	public APIResponse getNeTicketCount()
	{
		return ResponseUtil.createSuccessResponse()
				.setData(AppTicketSummaryCache.
						 getTlneDetails(AuthUtils.getCurrentUserId()));
	}

}
