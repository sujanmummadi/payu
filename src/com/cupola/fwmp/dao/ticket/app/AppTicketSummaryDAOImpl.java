package com.cupola.fwmp.dao.ticket.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.classification.jobs.PriortyUpdateHelper;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.counts.RibbonCountSummary;

/**
 * 
 * @author G Ashraf
 *
 */

@Transactional
public class AppTicketSummaryDAOImpl implements AppTicketSummaryDAO
{
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	GlobalActivities globalActivities;
	@Autowired
	DBUtil dbUtil;
	
	private static Logger logger = Logger.getLogger(AppTicketSummaryDAOImpl.class);
	
	@Override
	public List<TicketSummary> getTicketSummaryByUserId(Long userId ,Long workOrderTypeId){
	
		List<Object[]> list = null;
		if( userId == null || workOrderTypeId == null )
			return null;
		
		logger.debug("getTicketSummaryByUserId for user : "+userId);
		try{
			Set<Long> ticketIds = globalActivities.getAllTicketFromMainQueueByKey(dbUtil.getKeyForQueueByUserId(userId));
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(AppTicketSummaryDAO.FIND_COUNT_FOR_TL_QUERY)
					.setParameter("wotid", workOrderTypeId)
					.setParameterList("tlassigned", ticketIds);
			
			list = query.list();
		}catch(Exception e){
			logger.info("Failed to fetch Tickets .....");
		}
		
		logger.debug("exitting from getTicketSummaryByUserId for user : "+userId);
		return this.getTicketSummaryDbvo(list);
	}
	
	@Override
	public List<TicketSummary> getTicketSummaryByWorkOrderTypeId(Long userId ,Long workOrderTypeId)
	{
		logger.debug("getTicketSummaryByWorkOrderTypeId for user : "+userId);
		
		StringBuilder queryString = null;
		if(AuthUtils.isNITLUser())
			queryString = new StringBuilder(AppTicketSummaryDAO.FIND_COUNT_FOR_USER_QUERY_TL);
		else
			queryString = new StringBuilder(AppTicketSummaryDAO.FIND_COUNT_FOR_USER_QUERY_NE);
		
		String userKey = dbUtil.getKeyForQueueByUserId(userId);
		Set<Long> tids = globalActivities.getAllTicketFromMainQueueByKey(userKey);
		
		if(tids == null || tids.isEmpty())
			return null;
		
		List<Object[]> list = null;
		
		if(workOrderTypeId != null && workOrderTypeId >= 0 )
			queryString.append( " AND wo.workOrderTypeId =:wotid ");
				
				
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(queryString.toString())
					.setParameterList("ticketIds", tids);
			
			if(workOrderTypeId != null && workOrderTypeId >= 0 )
				query.setParameter("wotid", workOrderTypeId);
				
			logger.info("Ticket summary query is : "+query.getQueryString());
			list = query.list();
		}catch(Exception e){
			logger.info("Failed to fetch Tickets with userId :"+userId+" and workOrderTypeId "+workOrderTypeId,e);
		}
		logger.debug("exitting from getTicketSummaryByWorkOrderTypeId for user : "+userId);	
		return this.getTicketSummaryDbvo(list);
	}
	
	@Override
	public List<Long> getCompletedTicketActivityIds(Long ticketId)
	{
		List<Long> ids = new ArrayList<Long>();
		if( ticketId == null || ticketId.longValue() <= 0 )
			return ids;
		
		logger.debug("getTicketSummaryByWorkOrderTypeId for user : "+ticketId);
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(AppTicketSummaryDAO.FIND_ALL_COMPLETED_ACTIVITY)
					.setParameter("ticket", ticketId);
			
			List<Object[]> list = query.list();
			for(Object id : list){
				ids.add(Long.parseLong(id.toString()));
			}
		}catch(Exception e){
			logger.info("Failed to fetch Completed Activity for Ticket : "+ticketId,e);
		}
		logger.debug(" exit from getTicketSummaryByWorkOrderTypeId for user : "+ticketId);
		return ids;
	}

	@Override
	public List<RibbonCountSummary> getCountSummaryGropedByNE(Long initialWorkStage , Long userId)
	{
		logger.debug("getCountSummaryGropedByNE   : "+userId);
		StringBuilder completeQuery  = new StringBuilder(AppTicketSummaryDAO.GET_COUNT_SUMMARY_GROPEDBY_NE_TL);
		
		
		

	 	String key = dbUtil.getKeyForQueueByUserId(AuthUtils
				.getCurrentUserId());
		Set<Long> ticketListForWorkingQueue = globalActivities
				.getAllTicketFromMainQueueByKey(key); 
		
		if(AuthUtils.isManager() || AuthUtils.isAdmin())
		{
			completeQuery.append(" AND tick.areaId in(:areaIds) " );
		}
		else{
		
		 if (ticketListForWorkingQueue != null
					&& ticketListForWorkingQueue.size() > 0)
				;
			else
			{
				ticketListForWorkingQueue = new HashSet<Long>();
				ticketListForWorkingQueue.add(-1l);
			} 
		 completeQuery.append(" and tick.id in (:ticketIds) ");
		}
		if(initialWorkStage != null && !(initialWorkStage == 0))
			completeQuery.append(" AND wo.currentWorkStage = "+initialWorkStage);
		
		List<Object[]> list = null;
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(completeQuery.toString())
					 ;
			
			 if (ticketListForWorkingQueue != null
						&& ticketListForWorkingQueue.size() > 0)
					query.setParameterList("ticketIds", ticketListForWorkingQueue); 
			 if(AuthUtils.isManager() || AuthUtils.isAdmin())
				{
					query.setParameterList("areaIds", GenericUtil.getUsersSelectedAreaIds());
				}

			 
			logger.debug("Dashboard GET_COUNT_SUMMARY_GROPEDBY_NE_TL "+query.getQueryString());
			logger.debug("Dashboard GET_COUNT_SUMMARY_GROPEDBY_NE_TL ticketIds "+ticketListForWorkingQueue);
			
			list = query.list();
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Failed to fetching count summary grouped by TL :"+e.getMessage());
		}
		logger.debug(" exit from getCountSummaryGropedByNE   : "+userId);
		return getRibboCountSummaryDbvo(list);
	}
	
	private List<RibbonCountSummary> getRibboCountSummaryDbvo(List<Object[]> list)
	{
		if(list == null)
			return null;
		
		logger.debug("getRibboCountSummaryDbvo   : "+list.size());
		List<RibbonCountSummary> ribbonCountSummary = new ArrayList<RibbonCountSummary>();
	
		RibbonCountSummary  summary = null;
		for(Object[] object : list)
		{
			summary = new RibbonCountSummary();
			summary.setUserId(objectToLong(object[0]));
			summary.setFirstName(objectToString(object[1]));
			summary.setLastName(objectToString(object[2]));
			summary.setTicketId(objectToLong(object[3]));
			summary.setInitialWorkStage(objectToLong(object[4]));
			summary.setAssignedTo(objectToLong(object[5]));
			summary.setTicketStatus(objectToInt(object[6]));
			summary.setTicketSubCategory(objectToString(object[7]));
			summary.setPriorityId(objectToLong(object[8]));
			summary.setTickePriority(objectToInt(object[9]));
			
			ribbonCountSummary.add(summary);
		}
		logger.debug(" exit from getRibboCountSummaryDbvo   : "+list.size());
		return ribbonCountSummary;
	}
	
	private List<TicketSummary> getTicketSummaryDbvo(List<Object[]> list)
	{
		if(list == null)
			return null;
		
		logger.debug("getTicketSummaryDbvo   : "+list.size());
		
		List<TicketSummary> summaries = new ArrayList<TicketSummary>();
		TicketSummary summary = null;
		
		Long priority = PriortyUpdateHelper.getGlobalPriority().longValue();
		
		for(Object[] summa : list)
		{
			summary = new TicketSummary();
			summary.setWorkordertype(objectToString(summa[0]));
			summary.setWorkOrderTypeId(objectToLong(summa[1]));
			summary.setWorkOrderId(objectToLong(summa[2]));
			summary.setTicketId(objectToLong(summa[3]));
			summary.setCurrentActivity(objectToLong(summa[4]));
			summary.setAssignedTo(objectToLong(summa[5]));
			summary.setAddedOn(summa[6] == null ? null : (Date)summa[6]);
			summary.setCurrentWorkStage(objectToLong(summa[7]));
			summary.setUserId(objectToLong(summa[8]));
			summary.setStatus(objectToInt(summa[9]));
			summary.setSubCategoryId(objectToLong(summa[10]));
			summary.setPriorityValue(objectToLong(summa[11]) + priority);
			
			summaries.add(summary);
		}
		logger.debug(" exit from getTicketSummaryDbvo   : "+list.size());
		return summaries;
	}
	
	private Long objectToLong(Object obj)
	{
		if(obj == null)
			return 0l;
		return Long.valueOf(obj.toString());
	}
	
	private String objectToString(Object obj)
	{
		if(obj == null)
			return null;
		return obj.toString();
	}
	
	private Integer objectToInt(Object obj)
	{
		if(obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}
}
