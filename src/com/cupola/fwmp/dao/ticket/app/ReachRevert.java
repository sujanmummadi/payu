package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

public class ReachRevert
{

	private String priorityLevel;
	private long jeInstallationPending;

	private List<Long> jeInstallationPendingTicketIds;

	public ReachRevert()
	{
		this.jeInstallationPending = 0l;
		this.priorityLevel = "0";

	}

	public String getPriorityLevel()
	{
		return priorityLevel;
	}

	public void setPriorityLevel(String priorityLevel)
	{
		this.priorityLevel = priorityLevel;
	}

	public long getJeInstallationPending()
	{
		return jeInstallationPending;
	}

	public void setJeInstallationPending(Long jeInstallationPending)
	{
		this.jeInstallationPending = jeInstallationPending;
	}

	public  List<Long> getJeInstallationPendingTicketIds()
	{
		return jeInstallationPendingTicketIds;
	}

	public  void setJeInstallationPendingTicketIds(
			List<Long> jeInstallationPendingTicketIds)
	{
		this.jeInstallationPendingTicketIds = jeInstallationPendingTicketIds;
	}

}
