package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

import com.cupola.fwmp.vo.counts.RibbonCountSummary;

/**
 * 
 * @author G Ashraf
 * 
 * */

public interface AppTicketSummaryDAO {
	
	public static String FIND_COUNT_FOR_USER_QUERY_TL = "SELECT wot.workorderType,"
			   +"wo.workOrderTypeId,wo.id,wo.ticketId,wo.currentActivity"
			   +",tick.currentAssignedTo,wo.addedOn,wo.currentWorkStage,usr.id AS userID,"
			   + "tick.status,tick.subCategoryId,priority.value "
			   +"FROM WorkOrderType wot "
		       +"INNER JOIN WorkOrder wo ON wot.id = wo.workOrderTypeId "
//		       +"INNER JOIN Activity act ON act.id = wo.currentActivity "
		       +"INNER JOIN Ticket tick ON tick.id = wo.ticketId "
		       +"INNER JOIN TicketPriority priority ON tick.priorityId = priority.id "
		       +"INNER JOIN User usr ON usr.id = tick.currentAssignedTo "
		       +"WHERE  tick.id in (:ticketIds) "; 
	
	public static String FIND_COUNT_FOR_USER_QUERY_NE = "SELECT wot.workorderType,"
			   +"wo.workOrderTypeId,wo.id,wo.ticketId,wo.currentActivity"
			   +",tick.currentAssignedTo,wo.addedOn,wo.currentWorkStage,usr.id AS userID,"
			   + "tick.status,tick.subCategoryId,priority.value "
			   +"FROM WorkOrderType wot "
		       +"INNER JOIN WorkOrder wo ON wot.id = wo.workOrderTypeId "
//		       +"INNER JOIN Activity act ON act.id = wo.currentActivity "
		       +"INNER JOIN Ticket tick ON tick.id = wo.ticketId "
		       +"INNER JOIN TicketPriority priority ON tick.priorityId = priority.id "
		       +"INNER JOIN User usr ON usr.id = tick.currentAssignedTo "
		       +"WHERE  tick.id in (:ticketIds) ";
	
	public static String FIND_COUNT_FOR_TL_QUERY = "SELECT wot.workordertype,wo.workOrderTypeId,"
			   +"wo.id,wo.ticketId,wo.currentActivity"
			   +",wo.assignedTo,wo.addedOn,wo.currentWorkStage "
			   +"FROM WorkOrderType wot "
		       +"INNER JOIN WorkOrder wo ON wot.id = wo.workOrderTypeId "
		       +"INNER JOIN Activity act ON act.id = wo.currentActivity "
		       +"WHERE wo.workOrderTypeId =:wotid AND wo.ticketId in (:tlassigned)"; 
	

	public static String FIND_ALL_COMPLETED_ACTIVITY = "SELECT tal.activityId FROM TicketActivityLog tal "
			+ "WHERE tal.ticketId =:ticket";
	
	/*public static String GET_COUNT_SUMMARY_GROPEDBY_NE_TL = "SELECT usr.id,usr.firstName,usr.lastName,"
			   +" tick.id as tId ,wo.currentWorkStage,tick.currentAssignedTo, "
			   + " tick.status,tick.TicketSubCategory FROM User usr "
			   +" Left outer JOIN Ticket tick ON tick.currentAssignedTo = usr.id "
			   +" Left outer JOIN WorkOrder wo ON wo.ticketId = tick.id " 
			   +" WHERE usr.reportTo = :currentUser ";*/

	public static String GET_COUNT_SUMMARY_GROPEDBY_NE_TL = "SELECT usr.id,usr.firstName,usr.lastName,"
			  +"tick.id as tId ,wo.currentWorkStage,tick.currentAssignedTo,"
			  +"tick.status,tick.TicketSubCategory,tick.priorityId,tp.value "
			  +"FROM User usr  LEFT JOIN Ticket tick ON tick.currentAssignedTo = usr.id "
			  +"LEFT JOIN TicketPriority tp ON tick.priorityId = tp.id "
			  +"LEFT JOIN WorkOrder wo ON wo.ticketId = tick.id "
			  +"WHERE usr.id > 0 ";
			
			
	public	List<TicketSummary> getTicketSummaryByUserId(Long userId, Long workOrderTypeId);

	public List<TicketSummary> getTicketSummaryByWorkOrderTypeId(Long userId, Long workOrderTypeId);

	public List<Long> getCompletedTicketActivityIds(Long ticketId);

	public List<RibbonCountSummary> getCountSummaryGropedByNE(Long initialWorkStage, Long userId);
}
