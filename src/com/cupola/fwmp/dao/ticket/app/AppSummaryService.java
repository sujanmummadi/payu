package com.cupola.fwmp.dao.ticket.app;

import java.util.List;

import com.cupola.fwmp.response.APIResponse;

public interface AppSummaryService {
	
	
	public AppSummaryVO getTicketSummaryCount(Long userId, Long workOrderTypeId);
	public List<Long> getCompletedTicketActivityIds(Long ticketId);
	
	APIResponse getNeTicketCount();
}

