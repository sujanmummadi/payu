package com.cupola.fwmp.dao.ticket.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.vo.ActivityVo;
import com.cupola.fwmp.vo.TicketEtrVo;
import com.cupola.fwmp.vo.counts.RibbonCountSummary;
import com.cupola.fwmp.vo.counts.RibbonCountSummaryVO;
import com.cupola.fwmp.vo.counts.TLNEDetails;

/**
 * 
 * @author ashraf
 *
 */

public class AppTicketSummaryCache
{
	
	private static AppTicketSummaryDAO appDao;
	private static TicketDAO  ticketDAO;
	private static  TicketActivityLogDAO ticketActivityLoggingDAO;
	
	private static long cacheAgeInMillis =  2 * 60 * 1000;
	private static long cacheResetTimestamp =0;
	
	static final Logger LOGGER  = Logger.getLogger(AppTicketSummaryCache.class);
	
	private static Map<String, AppSummaryVO> userTicketCacheForApp = new HashMap<String, AppSummaryVO>();
	private static Map<String, List<RibbonCountSummaryVO>> neTicketCountCacheForApp = new HashMap<String, List<RibbonCountSummaryVO>>();
	
	private static void resetCacheIfOld()
	{
		/*if ((System.currentTimeMillis() - cacheResetTimestamp) > cacheAgeInMillis)
		{
			resetAllCache();
		}*/
		resetAllCache();
	}
	
	private static  void resetAllCache()
	{
		userTicketCacheForApp.clear();
		neTicketCountCacheForApp.clear();
		
		cacheResetTimestamp = System.currentTimeMillis();
		LOGGER.info("App Ticket Count Summary cache reset done");
	}
	
	private static  void resetUserAppTicketCache(Long userId)
	{
		userTicketCacheForApp.remove(userId.toString());
		updateUserAppTicketCache(userId,null);
	}
	
	private static List<Long> getCompletedTicketActivityIds(Long ticketId) {
		
		return appDao.getCompletedTicketActivityIds(ticketId);
	}
	
	private static AppSummaryVO pupolateAppSummaryVo(Long userId,Long workOrderTypeId)
	{
		List<TicketSummary> summaries = appDao.getTicketSummaryByWorkOrderTypeId(userId, workOrderTypeId);
	
		if(summaries == null)
			return null;
		
		AppTicketCountUtil.resetAndCreateCountMap();
		
		for(TicketSummary summary : summaries)
		{
			TicketEtrVo etr =  ticketDAO.getEtrVo(summary.getTicketId());
			AppTicketCountUtil.createSalesRelatedIssueCount(summary,etr);
			AppTicketCountUtil.createReachRevertCount(summary,etr);
			AppTicketCountUtil.createLcoIssueCount(summary,etr);
			AppTicketCountUtil.createCustomerAppointmentIssueCount(summary,etr);
			AppTicketCountUtil.createCustomerEndIssueCount(summary,etr);
			
			List<Long> completedActivitys = null;//getCompletedTicketActivityIds(summary.getTicketId());
			
			List<ActivityVo> activities = ticketActivityLoggingDAO
					.getActivitiesByWoNo(summary.getTicketId());
			
			if (activities != null && activities.size() > 0)
			{
				completedActivitys = new ArrayList<Long>();
				for( ActivityVo activity : activities )
				{
					completedActivitys.add(activity.getId());
				}
			}
			if(completedActivitys !=null)
				AppTicketCountUtil.createWorkInProgressCount(summary,completedActivitys);
		}
		return AppTicketCountUtil.getAppSummaryVO();
	}
	
	private static void updateUserAppTicketCache(Long userId,Long workOrderTypeId)
	{
		AppSummaryVO appSummary = pupolateAppSummaryVo(userId,workOrderTypeId);
		userTicketCacheForApp.put(getCachedObjectKey(userId, workOrderTypeId), appSummary);
		
		LOGGER.info("App Ticket Count Summary cache updated for userId : "+userId);
	}
	
	public static  AppSummaryVO getCountSummary(Long userId,Long workOrderTypeId)
	{
		resetCacheIfOld();
		AppSummaryVO vo = userTicketCacheForApp.get(getCachedObjectKey(userId, workOrderTypeId));
		if(vo == null)
			updateUserAppTicketCache(userId,workOrderTypeId);
		
		return userTicketCacheForApp.get(getCachedObjectKey(userId,workOrderTypeId));
	}
	
	private static List<RibbonCountSummaryVO> populateRibbonCountSummaryVO(long initialWorkStagetype)
	{
		List<RibbonCountSummary> list = appDao.getCountSummaryGropedByNE(initialWorkStagetype, AuthUtils.getCurrentUserId());
		if(list == null)
			return null;
		
		return AppTicketCountUtil.getRibbonCountSummaryVO(list);
	}
	
	private static void  updateNETicketCountCacheForApp(long initialWorkStagetype)
	{
		List<RibbonCountSummaryVO> listVo = populateRibbonCountSummaryVO(initialWorkStagetype);
		neTicketCountCacheForApp.put(getRibbonMapKey(initialWorkStagetype), listVo);
	}
	
	public static List<RibbonCountSummaryVO> getCountSummaryGropedByNE(long initialWorkStagetype)
	{
		resetCacheIfOld();
		List<RibbonCountSummaryVO> listVo = neTicketCountCacheForApp.get(getRibbonMapKey(initialWorkStagetype));
		if(listVo == null)
			updateNETicketCountCacheForApp(initialWorkStagetype);
		
		return neTicketCountCacheForApp.get(getRibbonMapKey(initialWorkStagetype));
		
	}
	
	public static List<TLNEDetails> getTlneDetails(Long tlUserId)
	{
		List<RibbonCountSummary> summary = appDao.getCountSummaryGropedByNE(null, AuthUtils.getCurrentUserId());
		
		if(summary == null)
			return null;
		
		return AppTicketCountUtil.getTlneTicketCounts(summary);
	}

	public static AppTicketSummaryDAO getAppDao()
	{
		return appDao;
	}

	public static void setAppDao(AppTicketSummaryDAO appDao)
	{
		AppTicketSummaryCache.appDao = appDao;
	}
	
	private static String getCachedObjectKey(Long userId , Long workOrderTypeId)
	{
		return +userId+"_"+workOrderTypeId;
	}
	
	private static String getRibbonMapKey(long workStage)
	{
		return workStage+"_WORKSTAGE";
	}
	
	private static String getNeDetailsKey(Long userId){
		return userId+"_TLUSER";
	}

	public static  TicketActivityLogDAO getTicketActivityLoggingDAO()
	{
		return ticketActivityLoggingDAO;
	}

	public static  void setTicketActivityLoggingDAO(
			TicketActivityLogDAO ticketActivityLoggingDAO)
	{
		AppTicketSummaryCache.ticketActivityLoggingDAO = ticketActivityLoggingDAO;
	}

	public static  TicketDAO getTicketDAO()
	{
		return ticketDAO;
	}

	public static  void setTicketDAO(TicketDAO ticketDAO)
	{
		AppTicketSummaryCache.ticketDAO = ticketDAO;
	}

}
