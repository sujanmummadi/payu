package com.cupola.fwmp.dao.ticket;

import java.util.List;

import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TicketDetailVo;

public interface TicketDetailDAO {
	
	public List<TicketDetailVo> getTicketWithDetails(TicketFilter filter);

	List<TicketDetailVo> getPreSaleTicketWithDetails(TicketFilter filter);

	String updateTicketStatus(StatusVO status);

	public List<TicketDetailVo> searchTicketWithDetails(TicketFilter filter);

	List<TicketDetailVo> getCFETicketWithDetails(TicketFilter filter);

}
