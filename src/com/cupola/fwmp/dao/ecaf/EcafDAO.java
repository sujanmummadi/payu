 /**
 * @Author kiran  Sep 1, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.ecaf;

import com.cupola.fwmp.vo.EcafVo;

/**
 * @Author kiran  Sep 1, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
public interface EcafDAO {

	/**
	 * @author kiran
	 * EcafVo
	 * @param ticketId
	 * @return
	 */
	EcafVo geteCAFDetails(Long ticketId);
	
	void sendeCAFToCustomer(Long ticketId);
}
