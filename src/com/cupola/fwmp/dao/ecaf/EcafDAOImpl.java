 /**
 * @Author kiran  Sep 1, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */
package com.cupola.fwmp.dao.ecaf;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.AadhaarTransactionType;
import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.integ.gis.GISPojo;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.mongo.documents.CustomerDocument;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.mail.EMailVO;
import com.cupola.fwmp.mail.MailService;
import com.cupola.fwmp.persistance.entities.Adhaar;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.persistance.entities.Payment;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.ekyc.KycDetails;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.notification.EcafNotificationHelper;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.GeneratePDFUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.EcafVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.sales.TariffPlanVo;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;
import com.cupola.fwmp.vo.tools.PaymentVO;



/**
 * @Author kiran  Sep 1, 2017
 * @Copyright (c) 2017 HappiestMinds Tech Pvt LTD. All rights reserved.
 */

@Transactional
public class EcafDAOImpl implements EcafDAO {
	
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	MongoOperations mongoOperations;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	CustomerDAO customerDAO;
	
	@Autowired
	GeneratePDFUtil generatePDFUtil;
	
	@Autowired
	GisDAO gisDAO;
	
	@Autowired
	DefinitionCoreService definitionCoreService;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	EcafNotificationHelper ecafHelper;
	
	@Autowired
	TicketDAO ticketDAO;
	

	private static String GET_ECAF_DETAILS = " select t.id , t.cafNumber , t.addedOn , c.firstName , c.middleName , c.lastName , c.cityId , c.customerPhoto ,"
			                                 + " c.customerSign , c.adhaarId , c.CustomerProfession  , c.currentAddress , c.communicationAdderss , c.permanentAddress ,"
			                                 + " c.officeNumber , c.mobileNumber , c.alternativeMobileNo, c.pincode , c.tariffId , c.emailId ,t.cxPermission,c.externalRouterRequired,"
			                                 + "t.idPayment ,c.isCustomerViaAadhaar , c.id as customerId ,t.id as ticketId from Ticket t inner join Customer c"
			                                 + " on c.id = t.customerId where t.id = ? ";

	@Override
	@Transactional
	public EcafVo geteCAFDetails(Long ticketId) {
		
		if(ticketId > 0)
		{
			List<EcafVo> eCAFDetails = jdbcTemplate.query(GET_ECAF_DETAILS, new Object[] {
							ticketId },new ECAFMapper());
			
			
			return eCAFDetails.get(0);
				
		}
		
		return new EcafVo();
	}
	
	class ECAFMapper implements RowMapper<EcafVo>
	{
		@Override
		public EcafVo mapRow(ResultSet resultSet, int rowNo)
				throws SQLException
		{
			EcafVo ecafVo = new EcafVo();
			ObjectMapper mapper = new ObjectMapper();

			ecafVo.setCafNo(resultSet.getString("cafNumber"));
			ecafVo.setCreatedDate(resultSet.getString("addedOn"));
			ecafVo.setCustomerName(resultSet.getString("firstName") +" " + resultSet.getString("middleName") + " " + resultSet.getString("lastName"));
			ecafVo.setPlace(LocationCache.getCityNameById(resultSet.getLong("cityId")));
			ecafVo.setCustomerPhoto(resultSet.getString("customerPhoto"));
			ecafVo.setCustomerSign(resultSet.getString("customerSign"));
			
			Long adhaarId = resultSet.getLong("adhaarId");
			
			if(adhaarId != null)
			{
				Adhaar adhaar = hibernateTemplate.get(Adhaar.class, adhaarId);
				
				KycDetails eKYCDetails = new KycDetails();
				
				if (adhaar != null)
					BeanUtils.copyProperties(adhaar, eKYCDetails);
				
				ecafVo.setAdhaar(eKYCDetails);
			}
			
			ecafVo.setCustomerProfession(resultSet.getString("CustomerProfession"));
			ecafVo.setState(LocationCache.getCityDAO().getStateNameByCityId(resultSet.getLong("cityId")));
			ecafVo.setCurrentAddress(resultSet.getString("currentAddress"));
			ecafVo.setCommunicationAdderss(resultSet.getString("communicationAdderss"));
			ecafVo.setPermanentAddress(resultSet.getString("permanentAddress"));
			ecafVo.setMobileNumber(resultSet.getString("mobileNumber"));
			ecafVo.setOfficeNumber(resultSet.getString("officeNumber"));
			ecafVo.setPincode(resultSet.getString("pincode"));
			
			Long tariffId = resultSet.getLong("tariffId");
			
			if(tariffId != null )
			{
				TariffPlan tariffPlan = hibernateTemplate.get(TariffPlan.class, tariffId);
				
				TariffPlanVo tariffPlanVo = new TariffPlanVo();
				
				if (tariffPlan != null)
					BeanUtils.copyProperties(tariffPlan, tariffPlanVo);
				
				ecafVo.setTariffPlanVo(tariffPlanVo);
			}
			
			List<CustomerDocument> documents = mongoOperations.find(new Query().addCriteria(Criteria.where("ticketId")
					.is(resultSet.getLong("id"))),CustomerDocument.class);
			
			if(documents != null)
			{
				ecafVo.setDocuments(documents);
			}
			
			ecafVo.setEmailId(resultSet.getString("emailId"));
			ecafVo.setCxPermission(resultSet.getString("cxPermission"));
			ecafVo.setExternalRouterRequired(resultSet.getString("externalRouterRequired"));
			
			Long paymentId = resultSet.getLong("idPayment");
			if(paymentId != null)
			{
				Payment payment = hibernateTemplate.get(Payment.class, paymentId);
				PaymentVO paymentVO = new PaymentVO();
				if (payment != null) {
					BeanUtils.copyProperties(payment, paymentVO);
					if (payment.getUploadedBy() != null) {
						UserVo vo = userDao.getUserById(payment.getUploadedBy());
						paymentVO.setSeName(vo != null ? vo.getFirstName() + " - SE" : "");
					}
					
					if (payment.getApprovedBy() != null) {
						UserVo vo = userDao.getUserById(payment.getApprovedBy());
						paymentVO.setCfeName(vo != null ? vo.getFirstName() + " - CFE" : "");
					}

				}
					
				ecafVo.setPaymentVO(paymentVO);
			}
			
			if(resultSet.getInt("isCustomerViaAadhaar") == Long.valueOf(Status.ACTIVE))
				ecafVo.setCustomerViaAadhaar(true);
			 else
				ecafVo.setCustomerViaAadhaar(false);
			
			CustomerAadhaarMapping customerAadhaarMappingPrimary =  customerDAO.getCustomerAadhaarMappingByLable(resultSet.getLong("customerId"), resultSet.getLong("ticketId"), AadhaarTransactionType.PRIMARY);
			 
			if(customerAadhaarMappingPrimary != null)
			{
				ecafVo.setPrimaryTransactionId(customerAadhaarMappingPrimary.getAdhaarTrnId());
				
				ecafVo.setPrimaryDate(GenericUtil.convertToUiDateFormat(customerAadhaarMappingPrimary.getAddedOn()));
			}
			 
			 
			 CustomerAadhaarMapping customerAadhaarMappingSecondary = customerDAO.getCustomerAadhaarMappingByLable(resultSet.getLong("customerId"), resultSet.getLong("ticketId"), AadhaarTransactionType.SECONDARY);
			
			 if(customerAadhaarMappingSecondary != null)
			 {
				 ecafVo.setSecoundryTransactionId(customerAadhaarMappingSecondary.getAdhaarTrnId());
				 
				 ecafVo.setSecondaryDate(GenericUtil.convertToUiDateFormat(customerAadhaarMappingSecondary.getAddedOn()) );
			 }
			 
			 GISPojo gisPojo = gisDAO.getGisInfoByTicketId(resultSet.getLong("ticketId"));
			 
			 if(gisPojo != null)
				 ecafVo.setGisPojo(gisPojo);
			 
			 try {
				if (ecafVo.getCurrentAddress() != null) {
						if (CommonUtil.isValidJson(ecafVo.getCurrentAddress())) {
							CustomerAddressVO currentAddressVo = mapper.readValue(ecafVo.getCurrentAddress(),
									CustomerAddressVO.class);
							ecafVo.setCurrent(currentAddressVo);

						} 
					}
				if (ecafVo.getPermanentAddress() != null) {
					if (CommonUtil.isValidJson(ecafVo.getPermanentAddress())) {
						CustomerAddressVO permanentAddressVo = mapper.readValue(ecafVo.getPermanentAddress(),
								CustomerAddressVO.class);
						ecafVo.setPerminent(permanentAddressVo);

					} 
				}
				
				if (ecafVo.getCommunicationAdderss() != null) {
					if (CommonUtil.isValidJson(ecafVo.getCommunicationAdderss())) {
						CustomerAddressVO communication = mapper.readValue(ecafVo.getCommunicationAdderss(),
								CustomerAddressVO.class);
						ecafVo.setPerminent(communication);

					} 
				}

			}  catch (Exception e) {
				e.printStackTrace();
			}
				 
			return ecafVo;
		}
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.ecaf.EcafDAO#sendeCAFToCustomer(java.lang.Long)
	 */
	@Override
	public void sendeCAFToCustomer(Long ticketId) {

		try {

			if (ticketId != null) {
				
				EcafVo ecafVo = geteCAFDetails(ticketId);

				if (ecafVo != null) {
					
					String src = definitionCoreService.getIntegrationFilePropertiesByKey(DefinitionCoreService.SOURCE_KEY);
					String dest_path = definitionCoreService.getIntegrationFilePropertiesByKey(DefinitionCoreService.DESTINATION_KEY)
							+ File.separator + ecafVo.getCafNo() + File.separator;

					String docName =   ecafVo.getCafNo() + "_" + "eCAF" + ".pdf";
					String docPath = createFolder(dest_path);

					String dest = docPath + docName;

					FileUtils.copyFile(new File(src), new File(dest));

					generatePDFUtil.manipulateECAFPdf(src, dest, ecafVo);
					
					EMailVO mailVo = new EMailVO();
					
					mailVo.setTo(ecafVo.getEmailId());
					mailVo.setMessage(definitionCoreService.getMailConfig(DefinitionCoreService.BODY_KEY));
					mailVo.setSubject(definitionCoreService.getMailConfig(DefinitionCoreService.SUBJECT_KEY));
					mailVo.setDocName(docName);
					mailVo.setAbsolutePath(dest);
					
					ticketDAO.updateEmailSentTime(ecafVo.getCafNo(), new Date());
					
					ecafHelper.addNewMailToQueue(mailVo);
					
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**@author kiran
	 * void
	 * @param dest
	 */
	private String createFolder(String dest) {
		

		File filePath = new File(dest);

		if (!filePath.exists())
		{

			filePath.mkdir();


			return dest;

		} else
		{

			return dest;

		}

	
		
	}
}
