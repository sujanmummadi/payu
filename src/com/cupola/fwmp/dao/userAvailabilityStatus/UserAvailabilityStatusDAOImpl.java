package com.cupola.fwmp.dao.userAvailabilityStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.UserAvailabilityStatus;
import com.cupola.fwmp.dao.mongo.logger.LoginStatusDao;
import com.cupola.fwmp.persistance.entities.UserLoginStatus;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.UserLoginStatusVo;

@Transactional
public class UserAvailabilityStatusDAOImpl implements UserAvailabilityStatusDAO
{

	private Logger log = Logger.getLogger(UserAvailabilityStatusDAOImpl.class
			.getName());

	private final static String INSERT_VALUES_USER_LOGIN_STATUS = "INSERT INTO UserLoginStatus  VALUES(?,?,?,?,?) ";
	private final String UPDATE_USER_STATUS = "UPDATE UserLoginStatus SET status=?,loginTime=? where userId=?";
	private final String UPDATE_USER_LOGIN_STATUS = "UPDATE UserLoginStatus SET status=?,logoutTime=? where userId=? ORDER BY loginTime desc LIMIT  1";
	private final String GET_USER_BY_USER_ID = "select * from UserLoginStatus where userId=?  Order  by loginTime desc limit 1";

	private final String IS_USER_ONLINE_OFFLINE = "SELECT * FROM UserLoginStatus WHERE userId =:userId AND status = 1 AND logoutTime IS NULL AND loginTime = :time ORDER BY loginTime desc LIMIT 1";
	
	HibernateTemplate hibernateTemplate;
	JdbcTemplate jdbcTemplate;
	@Autowired
	LoginStatusDao loginStatusDao;
	private static Map<Long,Date> lastTimeLogin = new ConcurrentHashMap<Long, Date>();

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public int addOrUpdateuserAvailabilityStatus(Long userId, int status)
	{
		int rowNum = 0;

		if(userId == null || userId <= 0)
			return rowNum;
		
		log.debug("addorupdateuserAvailabilityStatus as user Id:" + userId);
		
		UserLoginStatusVo userLoginStatusVo = getuserAvailabilityStatusById(userId);
		
		if (userLoginStatusVo == null)
		{
			log.debug("found new user as userId:" + userId);
			rowNum = adduserAvailabilityStatus(userId, status);

			return rowNum;
		}
		else 
		{
			loginStatusDao.insertLoginStatus(userLoginStatusVo);
			rowNum = updateuserAvailabilityStatus(userId, status);
			log.debug("found new user as userId:" + userId);
			return rowNum;
		}
	}

	@Override
	public String getDayByUserStatus(Date date, Long userId)
	{
		log.debug(" inside getDayByUserStatus  "+userId);
		int rowNum = 0;

		List<UserLoginStatusVo> userLoginStatusVo = getuserLoginStatusById(userId);
		if (userLoginStatusVo == null || userLoginStatusVo.isEmpty())
		{
			return null;
		}
		for (UserLoginStatusVo userLogin : userLoginStatusVo)
		{
			if (userLogin.getLoginTime() != null
					&& userLogin.getLogoutTime() != null)
			{
				log.info(userLogin.getLoginTime().getSeconds());
			}
		}
		log.debug(" inside getDayByUserStatus  "+userId);
		return GET_USER_BY_USER_ID;

	}

	@Override
	public int adduserAvailabilityStatus(Long userId, int status)
	{
		int rowNum = 0;
		Date timeOut = null;
		if(userId == null || userId <= 0)
			return rowNum;
		
		log.debug("adding userLogin status as userId:" + userId);
		
		if (status == UserAvailabilityStatus.USER_UNAVAILABILITY_STATUS)
		{
			timeOut = new Date();
		}
		try
		{
			Date lastLogin = new Date();
			lastTimeLogin.put(userId,lastLogin );
			
			rowNum = jdbcTemplate
					.update(INSERT_VALUES_USER_LOGIN_STATUS, new Object[] {
							StrictMicroSecondTimeBasedGuid.newGuid(), userId,
							lastLogin, timeOut, status });

		} catch (Exception e)
		{
			log.error("Exception adding data in UserAvailabilityDAOImpl :" + e);
		}
		if (rowNum <= 0)
			log.debug("data adeed in UserAvailability failed as userId:"
					+ userId);
		else
			log.debug("data adeed in UserAvailability successful as userId:"
					+ userId);
		
		log.debug("exitting from adding userLogin status as userId:" + userId);
		return rowNum;

	}

	@Override
	public int updateuserAvailabilityStatus(Long userId, int status)
	{
		log.debug("update UserAvailabilityDAOImpl as userId:" + userId);
		Integer rows = 0;
		if(userId == null || userId <= 0)
			return rows;
		Object[] params = { status, new Date(), userId };

		int[] types = { Types.INTEGER, Types.TIMESTAMP, Types.BIGINT };


		try
		{
			rows = jdbcTemplate.update(UPDATE_USER_LOGIN_STATUS, params, types);
			if (rows > 0)
				log.debug("UserAvailabilityDAOImpl  updated sucessfully as userId:"
						+ userId + "and status:" + status);

		} catch (Exception e)
		{
			log.error("error updateTicketActivityLog :" + e);

		}
		log.debug("exitting from update UserAvailabilityDAOImpl as userId:" + userId);
		return rows;
	}

	@Override
	public UserLoginStatusVo getuserAvailabilityStatusById(Long userId)
	{

		log.debug("status report as userId:" + userId);

		List<UserLoginStatusVo> userLoginStatusVo = null;

		try
		{
			userLoginStatusVo = (List<UserLoginStatusVo>) jdbcTemplate
					.query(GET_USER_BY_USER_ID, new Object[] { userId }, new UserLoginStatusMapper());

			if (userLoginStatusVo == null || userLoginStatusVo.isEmpty())
			{
				return null;
			}

		} catch (Exception e)
		{
			log.error("Error While getting userAvailabilityStatusById :"+e.getMessage());
			e.printStackTrace();
		}
		log.debug("exitting from status report as userId:" + userId);
		return userLoginStatusVo.get(0);

	}

	@Override
	public List<UserLoginStatusVo> getuserLoginStatusById(Long userId)
	{

		log.debug("status report as userId:" + userId);

		List<UserLoginStatusVo> userLoginStatusVo = null;

		try
		{
			userLoginStatusVo = (List<UserLoginStatusVo>) jdbcTemplate
					.query(GET_USER_BY_USER_ID, new Object[] { userId }, new UserLoginStatusMapper());

			if (userLoginStatusVo == null || userLoginStatusVo.isEmpty())
			{
				return null;
			}

		} catch (Exception e)
		{
			log.error("Error While getting userLoginStatusById :"+e.getMessage());
			e.printStackTrace();
		}
		log.debug("exitting from getuserLoginStatusById for userId:" + userId);

		return userLoginStatusVo;

	}

	@Override
	public boolean isUserOnLine(Long userId)
	{
		log.debug("User : "+userId+" trying to login ");
		
		boolean online = false;
		if (userId == null)
			return online;
		try
		{
			Date time = lastTimeLogin == null ? null : lastTimeLogin.get(userId);
			log.debug("User : "+userId+" last login time was : "+time);
			
			if(time == null)
				return online;
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String string = format.format(time.getTime());
			
			Query query = hibernateTemplate.getSessionFactory()
			 			.getCurrentSession()
			 			.createSQLQuery(IS_USER_ONLINE_OFFLINE)
						.setParameter("userId",userId )
						.setParameter("time",string);
			
			log.debug("Trying Query to fetch info : "+query.getQueryString());	
			List<UserLoginStatus> list = (List<UserLoginStatus>)query.list();
			if (list != null && list.size() > 0)
				online = true;
			else
				online = false;

		} catch (Exception e){
			log.error("Error occure while checking user online or not ",e);
		}
		log.debug(" Exitting from User : "+userId+" trying to login ");
		return online;
	}
}

class UserLoginStatusMapper implements RowMapper<UserLoginStatusVo>
{

	@Override
	public UserLoginStatusVo mapRow(ResultSet rs, int arg1) throws SQLException
	{
		UserLoginStatusVo userLoginStatus = new UserLoginStatusVo();
		userLoginStatus.setId(rs.getLong("id"));
		userLoginStatus.setUserId(rs.getLong("userId"));
		userLoginStatus.setLoginTime(rs.getTimestamp("loginTime"));
		userLoginStatus.setLogoutTime(rs.getTimestamp("logoutTime"));
		userLoginStatus.setStatus(rs.getInt("status"));
		return userLoginStatus;
	}

}
