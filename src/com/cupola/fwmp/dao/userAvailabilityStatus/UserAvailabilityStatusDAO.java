package com.cupola.fwmp.dao.userAvailabilityStatus;

import java.util.Date;
import java.util.List;

import com.cupola.fwmp.vo.UserLoginStatusVo;

public interface UserAvailabilityStatusDAO
{

	int adduserAvailabilityStatus(Long userId, int status);

	int updateuserAvailabilityStatus(Long userId, int status);

	UserLoginStatusVo getuserAvailabilityStatusById(Long userId);

	int addOrUpdateuserAvailabilityStatus(Long userId, int status);

	List<UserLoginStatusVo> getuserLoginStatusById(Long userId);

	String getDayByUserStatus(Date date, Long userId);

	public boolean isUserOnLine(Long userId);

}
