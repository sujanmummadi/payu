package com.cupola.fwmp.dao.workOrder;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.user.UserDaoImpl;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.TicketPriority;
import com.cupola.fwmp.persistance.entities.WorkOrder;
import com.cupola.fwmp.persistance.entities.WorkOrderType;
import com.cupola.fwmp.persistance.entities.WorkStage;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StatusMessages;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
//import com.cupola.fwmp.vo.AppointmentVO;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.TicketCountTlneVo;
import com.cupola.fwmp.vo.WorkOrderVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateWorkOrderVo;

@Transactional
public class WorkOrderDAOImpl implements WorkOrderDAO
{

	@Autowired
	HibernateTemplate hibernateTemplate;
	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	DBUtil dbUtil;
	@Autowired
	EtrCalculator etrCalculator;
	@Autowired
	NotificationService notificationService;
	
	@Autowired
	DefinitionCoreService definitionCoreService;

	private static final Logger log = LogManager
			.getLogger(WorkOrderDAOImpl.class.getName());

	private String getAllOpenTicketQuery;

	

	public void setGetAllOpenTicketQuery(String getAllOpenTicketQuery)
	{
		this.getAllOpenTicketQuery = getAllOpenTicketQuery;
	}

	@Override
	public WorkOrder addWorkOrder(WorkOrderVo workOrder)
	{
		if(workOrder == null)
			return null;

		log.info("WO addWorkOrder preparation for ticketId "
				+ workOrder.getTicketId() + " starterd at " + new Date());
		
		WorkOrder wo = null;
		
		try
		{
			wo = new WorkOrder();

			wo.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());

			wo.setAddedBy(workOrder.getAddedBy());
			wo.setModifiedBy(workOrder.getAddedBy());
			wo.setAddedOn(new Date());
			wo.setTotalPriority(0);
			wo.setModifiedOn(new Date());
			wo.setWorkOrderNumber(workOrder.getWorkOrderNo());
			wo.setTicket(hibernateTemplate
					.load(Ticket.class, workOrder.getTicketId()));
			wo.setWorkOrderType(hibernateTemplate
					.load(WorkOrderType.class, workOrder.getWorkOrderTypeId()));
			wo.setStatus(workOrder.getStatus());
			wo.setAssignedBy(workOrder.getAssignedBy());
			wo.setAssignedTo(workOrder.getAssignedTo());
			if (workOrder.getStatus() == null || workOrder.getStatus() == 0)
				wo.setStatus(TicketStatus.WO_GENERATED);
			
			
			if (workOrder.getInitialWorkStageId() != null
					&& workOrder.getInitialWorkStageId() > 0)
			{
				WorkStage ws = hibernateTemplate.load(WorkStage.class, workOrder
						.getInitialWorkStageId());
				if (ws != null)
				{
					log.info("Getting ticket details for WO " 
							+ workOrder.getWorkOrderNo() + " And Ticket id is  " + workOrder.getTicketId()+ " start time " + new Date());
					
					Ticket t = hibernateTemplate
							.load(Ticket.class, workOrder.getTicketId());
					
					t.setSubCategoryId(workOrder.getInitialWorkStageId());
					
					Date etr = etrCalculator.getETRforWorkStageType(workOrder
							.getInitialWorkStageId());
					
					if (etr != null)
					{
						t.setCommitedEtr(etr);
						t.setCurrentEtr(etr);
						t.setCommunicationETR(etr);
					}
					
					hibernateTemplate.update(t);
					
					log.info("Got ticket details for WO " 
							+ workOrder.getWorkOrderNo() + " And Ticket id is  " + workOrder.getTicketId() + " end time " + new Date());
					
					wo.setWorkStageByCurrentWorkStage(ws);
					wo.setWorkStageByInitialWorkStage(ws);
				}
			}

			if (workOrder.getGisId() != null && workOrder.getGisId() > 0)
				wo.setGis(hibernateTemplate
						.load(Gis.class, workOrder.getGisId()));

			 hibernateTemplate.save(wo);

//			hibernateTemplate.flush();
			
			TicketLog ticketLog = new TicketLogImpl(workOrder.getTicketId(),
					TicketUpdateConstant.WO_GENERATED, FWMPConstant.SYSTEM_ENGINE);
			ticketLog.setStatusId(TicketStatus.WO_GENERATED);
			ticketLog.setWoNumber(wo.getWorkOrderNumber());
			TicketUpdateGateway.logTicket(ticketLog);
			
			log.info("WorkOrder added with id   " + wo.getWorkOrderNumber() + " for ticket id " + workOrder.getTicketId() + " and ended at " + new Date());

		} catch (Exception e)
		{
			log.error("Duplicate key entry " + e.getMessage());
			e.printStackTrace();
			wo = null;
			return wo;
		}
		
		log.debug("Exitting from setGetAllOpenTicketQuery   " );
		
		return wo;
	}

	@Override
	public List<TicketCountTlneVo> getAllWorkOrder(Long workOrderTypeId,
			Long workStage, Long userId)
	{
		log.debug(" inside getAllWorkOrder  "+workOrderTypeId);

		Set<Long> tickets = globalActivities
				.getAllTicketFromMainQueueByKey(dbUtil
						.getKeyForQueueByUserId(userId));
		StringBuilder completeQuery = new StringBuilder(WorkOrderDAO.GET_TICKET_COUNT_BASED_ON_USER);

		if (AuthUtils.isManager() || AuthUtils.isAdmin())
		{
			completeQuery.append(" AND tick.areaId in(:areaIds) ");
		} else
		{
			if (tickets == null || tickets.size() <= 0 || tickets.isEmpty())
				return null;
		}

		if (tickets != null && tickets.size() > 0)
			completeQuery.append(" AND tick.id in(:ticketIds) ");

		if (workOrderTypeId != null)
			completeQuery
					.append(" AND wo.workOrderTypeId = " + workOrderTypeId);
		if (workStage != null)
			completeQuery.append(" AND wo.currentWorkStage = " + workStage);

		completeQuery.append(" order by tick.TicketCreationDate desc");
		
		List<Object[]> list = null;
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(completeQuery.toString());
			log.debug("Dashboard GET_TICKET_COUNT_BASED_ON_USER "
					+ query.getQueryString());
			log.debug("Dashboard GET_TICKET_COUNT_BASED_ON_USER tickets "
					+ tickets);

			if (tickets != null && tickets.size() > 0)
				query.setParameterList("ticketIds", tickets);

			if (AuthUtils.isManager() || AuthUtils.isAdmin())
			{
				query.setParameterList("areaIds", GenericUtil
						.getUsersSelectedAreaIds());
			}
			log.debug("Dashboard GET_TICKET_COUNT_BASED_ON_USER tickets GenericUtil.getUsersSelectedAreaIds() "
					+ GenericUtil.getUsersSelectedAreaIds());

			list = query.list();

		} catch (Exception e)
		{
			log.error("Failed to fetch workorder of user :" + userId, e);
		}
		
		log.debug(" Exitting from  getAllWorkOrder  "+workOrderTypeId);
		
		return xformVoObject(list);
	}

	private List<TicketCountTlneVo> xformVoObject(List<Object[]> list)
	{
		if (list == null)
			return null;

		log.debug(" inside xformVoObject  "+list.size());
		
		List<TicketCountTlneVo> tlneTicket = new ArrayList<TicketCountTlneVo>();
		TicketCountTlneVo tlneVo = null;
		for (Object[] dbvo : list)
		{
			tlneVo = new TicketCountTlneVo();
			tlneVo.setTicketId(objectToLong(dbvo[0]));
			tlneVo.setTicketStatus(objectToInt(dbvo[1]));
			tlneVo.setSubCategoryId(objectToLong(dbvo[2]));
			tlneVo.setCurrentAssignedTo(objectToLong(dbvo[3]));
			tlneVo.setTicketCategory(objectToString(dbvo[4]));
			tlneVo.setTicketSubCategory(objectToString(dbvo[5]));
			tlneVo.setPriorityId(objectToLong(dbvo[6]));
			tlneVo.setPriorityValue(objectToInt(dbvo[7]));
			tlneVo.setCurrentWorkStage(objectToInt(dbvo[8]));
			tlneVo.setWorkOrderTypeId(objectToLong(dbvo[9]));
			tlneVo.setWorkStageName(objectToString(dbvo[10]));
			tlneVo.setWorkOrderType(objectToString(dbvo[11]));

			tlneTicket.add(tlneVo);
		}
		log.debug(" Exitting from  xformVoObject  "+list.size());

		return tlneTicket;
	}

	@Override
	public WorkOrder deleteWorkOrder(Long id)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WorkOrder updateWorkOrder(WorkOrder workOrder)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateAssignedTo(AssignVO assignVO)
	{
		if(assignVO == null)
			return null;

		log.debug(" inside updateAssignedTo  " + assignVO.getWorkOrderNo());
		try {
		if (assignVO != null)
		{

			List<String> workOrderNos = assignVO.getWorkOrderNo();

			if (workOrderNos != null)
			{

				for (String orderNo : workOrderNos)
				{

					String query = "update WorkOrder w set w.assignedTo="
							+ assignVO.getAssignedToId() + ", w.assignedBy="
							+ assignVO.getAssignedById() + " where w.workOrderNumber='"
							+ orderNo+"'";
					hibernateTemplate.getSessionFactory().getCurrentSession()
							.createSQLQuery(query).executeUpdate();

					log.info("workOrder with id " + orderNo + " is assigned to "
							+ assignVO.getAssignedToId());
				}

				return StatusMessages.UPDATED_SUCCESSFULLY;
			}
		}
		} catch (Exception e) {
			log.error("Error occured while updateAssignedTo ",e);
			e.printStackTrace();
		}
		log.debug(" exit from updateAssignedTo  " + assignVO.getWorkOrderNo());
		return null;
	}

	@Override
	public void updatePriority(String workOrderNo, int priority,
			Integer escalationType, Integer escalatedValue)
	{

		// String query = "update WorkOrder w set w.totalPriority=" + priority
		// + " where w.workOrderNumber=" + workOrderNo;

		log.info("Updating priority for wo " + workOrderNo + " priority:"
				+ priority + " escalationType : " + escalationType
				+ " escalatedValue" + escalatedValue);
		String query = "from WorkOrder  where workOrderNumber= ?";

		List<WorkOrder> workOrders = (List<WorkOrder>) hibernateTemplate
				.find(query, workOrderNo + "");
		if (workOrders != null && !workOrders.isEmpty())
		{
			WorkOrder workOrder = workOrders.get(0);
			TicketPriority tp = workOrder.getTicket().getTicketPriority();
			if (tp != null)
			{

				if (escalationType == null || escalationType <= 0)
					tp.setSliderEscalatedValue(escalatedValue);
				else
				{
					tp.setEscalatedValue(escalatedValue);
					tp.setEscalationType(escalationType);
				}
				tp.setModifiedOn(new Date());
				tp.setValue(priority);
				int uc = tp.getUpdatedCount();
				uc++;
				tp.setUpdatedCount(uc);
				tp.setModifiedBy(AuthUtils.getCurrentUserId());
				hibernateTemplate.update(tp);
			}
			/*
			 * if (escalationType != null)
			 * workOrder.setPriorityEscalationType(escalationType); if
			 * (escalatedValue != null)
			 * workOrder.setEscalatedValue(escalatedValue);
			 * 
			 * workOrder.setTotalPriority(priority);
			 */

		}

		// hibernateTemplate.getSessionFactory().getCurrentSession()
		// .createSQLQuery(query).executeUpdate();

	}

	@Override
	public List<MQWorkorderVO> getAllOpenTicket()
	{
		log.debug(" inside getAllOpenTicket method ");
		
		List<MQWorkorderVO> mqWorkorderVOs = null;

		try
		{
			getAllOpenTicketQuery = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.PREPARE_DEPLOYMENT_QUEUE);
			
			if(getAllOpenTicketQuery == null)
				getAllOpenTicketQuery = "select wo.ticketId as workOrderNo, t.prospectNo as prosppectNo,a.id as areaId,b.id as branchId, c.id as cityId, cu.id as customerId,  wo.totalPriority,wo.initialWorkStage as initialWorkorder ,t.currentAssignedTo as assignedTo from WorkOrder wo inner join Ticket t on t.id=wo.ticketId inner join Customer cu  on cu.id=t.CustomerId inner join Area a on a.id = t.areaId inner join Branch b on a.branchId=b.id inner join  City c on b.cityId=c.id where t.status in (140,250,251,252,253,246,247,248,268,304,306,307,305,308,309,102,313,314,315,316,317,318,320,321,323)";

			mqWorkorderVOs = jdbcTemplate
					.query(getAllOpenTicketQuery, new WorkOrderClassificationMapper());
			
			return mqWorkorderVOs;
			
		} catch (Exception e)
		{
			log.error("Error while getting work order for queue " + e.getMessage());
			e.printStackTrace();
		}
		
		log.debug(" Exitting from  getAllOpenTicket  ");
		
		return mqWorkorderVOs;
	}

	class WorkOrderClassificationMapper implements RowMapper<MQWorkorderVO>
	{

		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			MQWorkorderVO mqWorkorderVO = new MQWorkorderVO();
			mqWorkorderVO.setAreaId(resultSet.getString("areaId"));
			mqWorkorderVO.setAssignedTo(resultSet.getLong("assignedTo"));
			mqWorkorderVO.setBranchId(resultSet.getString("branchId"));
			mqWorkorderVO.setCityId(resultSet.getString("cityId"));
			mqWorkorderVO
					.setWorkOrderNumber(resultSet.getString("workOrderNo"));

			mqWorkorderVO.setProspectNumber(resultSet.getString("prosppectNo"));

			mqWorkorderVO.setInnitialWorkStage(resultSet
					.getLong("initialWorkorder"));

			return mqWorkorderVO;
		}

	}

	
	private String frWorkOrderSql;

	@Override
	public List<MQWorkorderVO> getAllOpenFrTicket()
	{
		log.debug(" inside getAllOpenFrTicket  ");
		
		List<MQWorkorderVO> mqWorkorderVOs = null;

		try
		{
			frWorkOrderSql = definitionCoreService
					.getClassificationQuery(DefinitionCoreService.FR_QUEUE_QUERY_KEY);
			if(frWorkOrderSql == null)
				frWorkOrderSql = "SELECT wo.ticketId as workOrderNo,t.workOrderNumber as prosppectNo,a.id as areaId,b.id as branchId, c.id as cityId, cu.id as customerId, t.currentAssignedTo as assignedTo,wo.nonLock as lockValue,wo.nonBlocked AS blocked FROM FrDetail wo INNER JOIN FrTicket t ON wo.ticketId = t.id INNER JOIN Area a ON t.areaId = a.id INNER JOIN Branch b ON a.branchId = b.id INNER JOIN City c ON b.cityId = c.id INNER JOIN Customer cu ON t.customerId = cu.id where date(t.addedOn) >= DATE_SUB(NOW(), INTERVAL 300 DAY)";

			mqWorkorderVOs = jdbcTemplate
					.query(frWorkOrderSql, new FrWorkOrderMapper());
			
			return mqWorkorderVOs;
			
		} catch (Exception e)
		{
			log.error("Error while getting work order for fr queue " + e.getMessage());
			e.printStackTrace();
		}
		return mqWorkorderVOs;

	}

	class FrWorkOrderMapper implements RowMapper<MQWorkorderVO>
	{

		@Override
		public MQWorkorderVO mapRow(ResultSet resultSet, int arg1)
				throws SQLException
		{
			MQWorkorderVO mqWorkorderVO = new MQWorkorderVO();
			mqWorkorderVO.setAreaId(resultSet.getString("areaId"));
			mqWorkorderVO.setAssignedTo(resultSet.getLong("assignedTo"));
			mqWorkorderVO.setBranchId(resultSet.getString("branchId"));
			mqWorkorderVO.setCityId(resultSet.getString("cityId"));
			mqWorkorderVO
					.setWorkOrderNumber(resultSet.getString("workOrderNo"));

			mqWorkorderVO.setProspectNumber(resultSet.getString("prosppectNo"));
			mqWorkorderVO.setLocked(resultSet.getInt("lockValue") == 1 ? true
					: false);
			
			mqWorkorderVO.setBlocked(resultSet.getInt("blocked") == 1 ? true
					: false);
			return mqWorkorderVO;
		}

	}

	@Override
	public WorkOrder getWorkOrderByWoNo(String workOrderNumber)
	{
		log.debug(" inside getWorkOrderByWoNo  "+workOrderNumber);
		
		try {
		List<WorkOrder> workOrders = hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("select * from WorkOrder where workOrderNumber= :workOrderNumber")
				.addEntity(WorkOrder.class)
				.setParameter("workOrderNumber", workOrderNumber).list();

			if (workOrders != null && !workOrders.isEmpty())
			return workOrders.get(0);
		} catch ( Exception e) {
			
			 log.error("error while getting WorkOrderByWoNo " ,e);
		}

		log.debug(" Exitting from  getWorkOrderByWoNo  "+workOrderNumber);
		
		return null;
	}
	
	@Override
	public boolean isWOPresentInDB(String workOrderNumber)
	{
		boolean result = false;
		
		try {
		List<Object> workOrders = hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("select workOrderNumber from WorkOrder where workOrderNumber= :workOrderNumber")
				.setParameter("workOrderNumber", workOrderNumber).list();


		if (workOrders != null && !workOrders.isEmpty())
			result = true;
		else
			result = false;	

		} catch (Exception e) {
			e.printStackTrace();
			result = false;	
		}
		
		return result;
	}

	@Override
	public String getWoNumberForTicket(Long ticketId)
	{
		log.debug(" inside getWoNumberForTicket  ");

		String result = null;
		
		 try {
			 result = (String)jdbcTemplate
					.queryForObject("Select w.workOrderNumber from WorkOrder w  where w.ticketId=?", new Object[] {
							ticketId }, String.class);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while getting wo for ticket " + ticketId + " " + e.getMessage());
		}
		 
		 return result;
	}

	@Override
	public int updateFiberToCopper(long ticketId, long typeId)
	{
		log.debug(" inside updateFiberToCopper  " +ticketId);
		
		return jdbcTemplate
				.update("update WorkOrder wo set wo.currentWorkStage=? where wo.ticketId=?", new Object[] {
						typeId, ticketId });
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());
	}

	private String objectToString(Object obj)
	{
		if (obj == null)
			return null;
		return obj.toString();
	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}

	@Override
	public List<WorkOrder> getAllUnassignedWorkOrder()
	{
		log.debug(" inside getAllUnassignedWorkOrder  ");
		
		String unassignedWorkOrder = "select * from WorkOrder where assignedTo is null";

		return hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery(unassignedWorkOrder).addEntity(WorkOrder.class)
				.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.workOrder.WorkOrderDAO#updateWOByCRM(com.cupola.fwmp.vo.
	 * tools.siebel.from.vo.CreateUpdateWorkOrderVo)
	 */
	@Override
	public int updateWOByCRM(CreateUpdateWorkOrderVo workOrderVo) {

		log.info("Update WO by Siebel CRM " + workOrderVo);
		int result = 0;
		Long userId = UserDaoImpl.cachedUsersByLoginId.get(workOrderVo.getAssignedTo().toUpperCase()).getId();

		Long prioritySource = workOrderVo.getListOfPrioritySource().getPriority().getPriorityType() != null
				&& !workOrderVo.getListOfPrioritySource().getPriority().getPriorityType().isEmpty()
						? definitionCoreService.getEscalationTypeByValues(
								workOrderVo.getListOfPrioritySource().getPriority().getPriorityType())
						: null;

		log.info("Update WO by Siebel CRM " + workOrderVo.getWorkOrderNumber() + " prioritySource" + prioritySource);
		try {

			result = jdbcTemplate.update(UPDATE_WO_BY_CRM,
					new Object[] { workOrderVo.getPriorityValue(), userId, prioritySource,
							workOrderVo.getDefectCode() != null && !workOrderVo.getDefectCode().isEmpty()
									? TicketStatus.CUSTOMER_ACCOUNT_ACTIVATION_DONE
									: workOrderVo.getStatus(),
							workOrderVo.getWorkOrderNumber() });

		} catch (Exception e) {
			log.error("Error while updating workorder details by Siebel for workorder "
					+ workOrderVo.getWorkOrderNumber() + " . " + e.getMessage(), e);
			e.printStackTrace();
		}

		return result;
	}

//	@Override
//	public int updateAppointmentDate(AppointmentVO appointmentVO) {
//
//		try {
//
//			WorkOrder wo = hibernateTemplate.load(WorkOrder.class, appointmentVO.getWorkOrderNo());
//
//			if (wo != null) {
//				log.info("Appointment date updation has been successful for workorder:"
//						+ appointmentVO.getWorkOrderNo());
//
//				int updateAppointmentValue=jdbcTemplate.update(APPOINTMENT_UPDATE_QUERY,
//						new Object[] { appointmentVO.getAppointmentDate(), appointmentVO.getWorkOrderNo() });
//				
//				return updateAppointmentValue;
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.info("Appointment date updation has been failed for workorder:" + appointmentVO.getWorkOrderNo());
//		}
//       return 0;
//	}


	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.workOrder.WorkOrderDAO#closeWOinBulk(java.util.Set)
	 */
	@Override
	public int closeWOinBulk(Set<String> workOrders) {
		try {
			Query query = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(WO_MANUAL_CLOSURE_QUERY).setParameterList("workOrders", workOrders);

			int result = query.executeUpdate();

			log.info("WorkOrder ticket manual closure QUERY : " + query.getQueryString() + " and work orders are "
					+ workOrders + " and result is " + result);

			if (result == 0)
				return 90000001;
			else
				return result;
		} catch (Exception e) {
			e.printStackTrace();
			return 90000000;
		}
	}

}
