package com.cupola.fwmp.dao.workOrder;

import java.util.List;
import java.util.Set;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.persistance.entities.WorkOrder;
//import com.cupola.fwmp.vo.AppointmentVO;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.TicketCountTlneVo;
import com.cupola.fwmp.vo.WorkOrderVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateWorkOrderVo;

public interface WorkOrderDAO
{

	/*
	 * public static String GET_TICKET_COUNT_BASED_ON_USERID = "SELECT * "
	 * +"FROM WorkOrder wo " +"WHERE wo.ticketId in (:ticketIds)";
	 */

	public static String GET_TICKET_COUNT_BASED_ON_USER = "SELECT tick.id,tick.status,tick.subCategoryId, "
			+ "tick.currentAssignedTo, "
			+ "tick.TicketCategory,tick.TicketSubCategory,tick.priorityId,priority.value, "
			+ "wo.currentWorkStage,wo.workOrderTypeId,ws.workStageName,wot.workOrderType "
			+ "FROM Ticket tick LEFT JOIN TicketPriority priority ON tick.priorityId = priority.id "
			+ "LEFT JOIN WorkOrder wo ON wo.ticketId = tick.id "
			+ "LEFT JOIN WorkStage ws ON wo.currentWorkStage = ws.id "
			+ "LEFT JOIN WorkOrderType wot ON wo.workOrderTypeId = wot.id "
			+ "WHERE tick.id > 0 ";

	String WO_MANUAL_CLOSURE_QUERY="Update Ticket t join WorkOrder wo on wo.ticketId=t.id set t.status = 249 where workOrderNumber in (:workOrders)";

	String UPDATE_WO_BY_CRM = "update WorkOrder wo join Ticket t on t.id=wo.ticketId join "
			+ "TicketPriority tp on t.priorityId=tp.id set "
			+ "tp.value = ?, t.currentAssignedTo = ?, tp.escalationType = ?, t.status = ? where wo.workOrderNumber = ?";
	
	
//	public static String APPOINTMENT_UPDATE_QUERY="update WorkOrder set appointmentDate=? "
//			+ "where workOrederNumber=?";

	public WorkOrder addWorkOrder(WorkOrderVo wo);

	public WorkOrder getWorkOrderByWoNo(String workOrderNumber);

	public WorkOrder deleteWorkOrder(Long id);

	public WorkOrder updateWorkOrder(WorkOrder workOrder);

	String updateAssignedTo(AssignVO assignVO);

	void updatePriority(String workOrderNo, int priority, Integer escalationType,
			Integer escalatedValue);

	public List<TicketCountTlneVo> getAllWorkOrder(Long workOrderType,
			Long workStage, Long userId);

	public List<MQWorkorderVO> getAllOpenTicket();

	String getWoNumberForTicket(Long ticketId);

	int updateFiberToCopper(long ticketId, long typeId);

	List<MQWorkorderVO> getAllOpenFrTicket();
	
	List<WorkOrder> getAllUnassignedWorkOrder();

	/**@author aditya
	 * boolean
	 * @param workOrderNumber
	 * @return
	 */
	boolean isWOPresentInDB(String workOrderNumber);

	/**@author aditya
	 * int
<<<<<<< HEAD
<<<<<<< HEAD
	 * @param createUpdateWorkOrderVo
	 * @return
	 */
	public int updateWOByCRM(CreateUpdateWorkOrderVo createUpdateWorkOrderVo);
	
//	 public int updateAppointmentDate(AppointmentVO appointmentVO);

	/**@author aditya
	 * int
=======
>>>>>>> 9e6859edb... Code Merging from 2-0 to 2-1
=======
>>>>>>> fwmp_server_rel_ver_2_1_support
	 * @param workOrders
	 * @return
	 */
	public int closeWOinBulk(Set<String> workOrders);
}
