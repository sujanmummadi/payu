package com.cupola.fwmp.dao.skill;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.persistance.entities.Skill;
import com.cupola.fwmp.vo.VendorSummaryVO;

public class SkillDAOImpl  implements SkillDAO{
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;
	
	Map<String,Long> skillNameAndIdMap = new TreeMap<String,Long>();
	
	private static Logger log = LogManager.getLogger(SkillDAOImpl.class.getName());
	
	private void init()
	{
		log.info("SkillDAOImpl init method called..");
		getSkills();
		log.info("SkillDAOImpl init method executed.");
	}
	@Override
	public Skill addSkill(Skill skill) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Skill getSkillById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Skill> getAllSkill() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Skill deleteSkill(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Skill updateSkill(Skill skill) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getSkillIdBySkillName(String skillName)
	{
		if(skillName == null || skillName.isEmpty())
			return 0;
		
		log.debug("inside getSkillIdBySkillName " +skillName );
		if(skillNameAndIdMap.size() == 0)
			getSkills();
		
		if(skillNameAndIdMap.get(skillName) != null)
				return skillNameAndIdMap.get(skillName);
		
		else
			return 0;
	}


	private void getSkills(){
		
		List<VendorSummaryVO> vendorList = jdbcTemplate
				.query("select id,skillName from Skill", new SkillRowMapper());
		
		if (vendorList == null || vendorList.isEmpty())
		{

			log.debug("Skills are not available");
			return;

		} else
		{

			for (VendorSummaryVO skill : vendorList)
			{
				addToCache(skill);
			}

		}

	}
	
	class SkillRowMapper implements RowMapper<VendorSummaryVO>
	{

		@Override
		public VendorSummaryVO mapRow(ResultSet rs, int i) throws SQLException
		{
			VendorSummaryVO vendorSummaryVO = new VendorSummaryVO();
			vendorSummaryVO.setSkillId(rs.getInt(1));
			vendorSummaryVO.setSkillName(rs.getString(2));

			return vendorSummaryVO;
		}
	}
	private void addToCache(VendorSummaryVO skill) {

		skillNameAndIdMap.put(skill.getSkillName(), Long.valueOf(skill.getSkillId()));
	}
}
