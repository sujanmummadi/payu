package com.cupola.fwmp.dao.skill;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Skill;

public interface SkillDAO {

	public Skill addSkill(Skill skill);

	public Skill getSkillById(Long id);

	public List<Skill> getAllSkill();

	public Skill deleteSkill(Long id);

	public Skill updateSkill(Skill skill);

	long getSkillIdBySkillName(String skillName);
}
