package com.cupola.fwmp.dao.tablet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TabFilter;
import com.cupola.fwmp.persistance.entities.Tablet;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.LoginPojo;
import com.cupola.fwmp.vo.TabDetailsVo;
import com.cupola.fwmp.vo.TabletFilterVO;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserAndRegIdDetails;

//import com.cupola.fwmp.filters;

public class TabletDAOImpl implements TabletDAO
{

	final static Logger logger = Logger.getLogger(TabletDAOImpl.class);

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	UserDao userDao;

	private static String tabletBaseQuery = "Select t.* ,u.firstName as assignedToC ,u1.firstName as assignedToCur  from Tablet t LEFT OUTER JOIN User u on t.assignedTo= u.id  LEFT OUTER JOIN User u1 on t.currentUser = u1.id   where t.status=1";

	private static String PAGINATIONQUERY = "select t.tabName,t.macAddress, t.brandName, (select firstName from User where id=t.currentUser) as CurrentUser, (select firstName from User where id=t.assignedTo) as AssignedUser,t.status , (select Id from User where id=t.currentUser) as CurrentUserId , (select Id from User where id=t.assignedTo) as AssignedUserId from Tablet t where status =1";

	private static final int MIN_ITEM_PER_PAGE = 5;

	// private static String tabletBaseQuery = "Select t.* ,u.firstName as
	// assignedToC ,u1.firstName as assignedToCur from Tablet t LEFT OUTER JOIN
	// User u on t.assignedTo= u.id LEFT OUTER JOIN User u1 on t.currentUser =
	// u1.id ";

	static Map<String, Tablet> userNameAndTabMap = new ConcurrentHashMap<String, Tablet>();

	public Map<String, Long> tabMacAndIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<Long, String> tabIdAndTabMacCache = new ConcurrentHashMap<Long, String>();

	private void init()
	{
		logger.debug("initializing Tablets ");
		getTablets();
		logger.debug(" exit from initializing Tablets ");
	}

	private void getTablets()
	{
		logger.debug("getTablets ");

       try {
		List<Tablet> tabList = (List<Tablet>) hibernateTemplate
				.find("from Tablet");

			if (tabList == null || tabList.isEmpty())
		{

			return;

		} else
		{

			for (Tablet tablet : tabList)
			{

				addToCache(tablet);
			}

		}
	}catch ( Exception e) {
		logger.error(" error while getting Tablets",e );
		}
	}

	private void addToCache(Tablet tablet)
	{
		if (tablet == null)
			return;
		
		logger.debug(" addToCache " +tablet.getTabName() );

		tabMacAndIdMap.put(tablet.getMacAddress(), tablet.getId());

		tabIdAndTabMacCache.put(tablet.getId(), tablet.getMacAddress());
	}

	@Override
	public String setCurrentUser(LoginPojo loginPojo, Long userId)
	{

		if (loginPojo == null  )
			return "";
		
		logger.debug(" setCurrentUser " + loginPojo.getUsername()+ " "   + userId);
		try
		{

			String loginId = loginPojo.getUsername();

			if (userNameAndTabMap!=null && userNameAndTabMap.containsKey(loginId))
			{

				Tablet currentTab = userNameAndTabMap.get(loginId);

				logger.info("User: " + loginId
						+ " is currently associated with the tab : "
						+ currentTab.getMacAddress()
						+ ".Disabling this tablet.. ");

				currentTab.setStatus(0);

				hibernateTemplate.save(currentTab);
			} else
			{

				logger.info("User is not currently associated with any tab.");
			}

			List<Tablet> tabs = (List<Tablet>) hibernateTemplate
					.find("from Tablet where macAddress = ?", loginPojo
							.getDeviceId());

			if (tabs != null && tabs.size() > 0)
			{

				Tablet tab = tabs.get(0);

				User currentUser = new User();

				currentUser.setId(userId);

				tab.setUserByCurrentUser(currentUser);

				hibernateTemplate.saveOrUpdate(tab);

			}
			return "success";

		} catch (Exception e)
		{
			logger.error("Error while setting current user :"+e.getMessage());
			e.printStackTrace();

			return "failed";
		}
	}

	@Override
	public String checkCurrentUser(LoginPojo loginPojo)
	{

		if(loginPojo == null)
			return null;
		
		logger.debug(" checkCurrentUser " +loginPojo.getUsername());

		try 
		{
			List<User> users = (List<User>) hibernateTemplate
				.find("from User where loginId = ?", loginPojo.getUsername());

			if (users != null && !users.isEmpty())
			{

				User user = users.get(0);

				List<Tablet> tablets = (List<Tablet>) hibernateTemplate
					.find("from Tablet where userByCurrentUser.id = ?", user
							.getId());

				if (tablets == null || tablets.isEmpty())
				{

					return "associated false";
				}

				else
				{

					Tablet tablet = tablets.get(0);

					userNameAndTabMap.put(user.getLoginId(), tablet);

					return "associated true";
				}
			}
		} catch ( Exception e) {
			logger.error( "error while checking  CurrentUser",e);
		}
		logger.info(" Exit From checkCurrentUser " +loginPojo.getUsername());
		
		return null;
	}

	@Override
	public List<TypeAheadVo> tabSearchSuggestion(String tabQuery)
	{
		List<TypeAheadVo> tabMacs = new ArrayList<>();
		
		if(tabQuery == null || tabQuery.isEmpty())
			return tabMacs;
		
		logger.debug( " inside tabSearchSuggestion" + tabQuery);
       
       try 
       {
    	   List<Tablet> tabs = (List<Tablet>) hibernateTemplate
				.find("from Tablet where macAddress like?", "%" + tabQuery
						+ "%");
    	   
    	   if( tabs == null || tabs.isEmpty())
    		   return tabMacs;

			for (Tablet tablet : tabs)
			{

				TypeAheadVo aheadVo = new TypeAheadVo(tablet.getId(), tablet.getMacAddress());
				tabMacs.add(aheadVo);
			}

		 
		} catch ( Exception e) {
			logger.error( "error while tabSearchSuggestion ",e);
		}
       	logger.debug( " Exit From tabSearchSuggestion" + tabQuery);
		return tabMacs;
	}

	@Override
	@Transactional
	public APIResponse updateRegistrationId(TabDetailsVo tabDetailsVo)
	{
		if (tabDetailsVo == null)
			return null;

		logger.info("Updating registration id for tab : " + tabDetailsVo);

		Session session = hibernateTemplate.getSessionFactory().openSession();

		Transaction tx = session.beginTransaction();

		List<Tablet> tabs = session
				.createSQLQuery("select * from Tablet where macAddress =:macAddress")
				.addEntity(Tablet.class)
				.setParameter("macAddress", tabDetailsVo.getDeviceId()).list();

		logger.debug("Data details from Tablet table for registration id for tab : "
				+ tabs);

		if (tabs != null && !tabs.isEmpty())
		{
			Tablet tab = tabs.get(0);

			logger.debug("tab =========== " + tab.getMacAddress());

			tab.setRegistrationId(tabDetailsVo.getRegistrationId());

			try
			{
				session.saveOrUpdate(tab);
				tx.commit();

			} catch (Exception e)
			{
				logger.error("Error while updating RegistrationId : "+e.getMessage());
				
				e.printStackTrace();
			} finally
			{
				session.close();
			}

			return ResponseUtil.createUpdateSuccessResponse();

		}
		logger.debug("Exit From updateRegistrationId " + tabDetailsVo.getDeviceId());
		return null;
	}

	@Transactional
	@Override
	public Tablet addTablet(TabletVO tabletVo)
	{
		Tablet tablet = null;
		
		if(tabletVo == null)
			return tablet;
		
		logger.debug(" inside addTablet ..." +tabletVo.getBrandName());
		try
		{
			if (tabMacAndIdMap!=null && tabMacAndIdMap.containsKey(tabletVo.getMacAddress()))
				return null;

			tablet = new Tablet();
			BeanUtils.copyProperties(tabletVo, tablet);
			tablet = addCommonInitialValues(tablet);

			User user = null;
			if (tabletVo.getUserByAssignedTo() != null)
			{
				user = hibernateTemplate.load(User.class, tabletVo
						.getUserByAssignedTo().getId());
				tablet.setUserByAssignedTo(user);

			}

			if (tabletVo.getUserByCurrentUser() != null)
			{

				tablet.setUserByCurrentUser(hibernateTemplate
						.load(User.class, tabletVo.getUserByCurrentUser()
								.getId()));
			}

			else
			{
				tablet.setUserByCurrentUser(user);
			}

			Long result = (Long) hibernateTemplate.save(tablet);

			logger.info("Added Tablet with id: " + result + "data sucessfuly");

			addToCache(tablet);

			// tablet.setStatus(StatusCodes.SUCCESS);

		} catch ( Exception e)
		{
			logger.error("Error while adding Tablet " + tabletVo + ", Message "
					+ e.getMessage());

			e.printStackTrace();

			tabletVo.setStatus(StatusCodes.FAILURE);
		}
		logger.debug("Exitting from inside addTablet ..." +tabletVo.getBrandName());
		return tablet;
	}

	/*
	 * public APIResponse addTablet(TabletVO tabletVo) {
	 * 
	 * Tablet tablet = new Tablet();
	 * 
	 * BeanUtils.copyProperties(tabletVo, tablet);
	 * 
	 * try { tablet = addCommonInitialValues(tablet);
	 * 
	 * if(tabletVo.getCurrentUserName() == null){
	 * 
	 * User assignedTo = (User) hibernateTemplate.getSessionFactory()
	 * .getCurrentSession() .load(User.class,
	 * tabletVo.getUserByAssignedTo().getId());
	 * 
	 * tablet.setUserByAssignedTo(assignedTo);
	 * 
	 * if (tabletVo.getUserByAssignedTo().getId() == tabletVo
	 * .getUserByCurrentUser().getId()) {
	 * 
	 * logger.info("current user and associated user are same");
	 * 
	 * tablet.setUserByCurrentUser(assignedTo); }
	 * 
	 * else {
	 * 
	 * User currentUser = (User) hibernateTemplate .getSessionFactory()
	 * .getCurrentSession() .load(User.class,
	 * tabletVo.getUserByCurrentUser().getId());
	 * 
	 * tablet.setUserByCurrentUser(currentUser); }
	 * 
	 * } tablet.setMacAddress(tablet.getMacAddress().trim().toUpperCase()); Long
	 * result = (Long) hibernateTemplate.save(tablet);
	 * 
	 * logger.info(" result : " + result);
	 * 
	 * return ResponseUtil.recordAddedSucessfully();
	 * 
	 * } catch (HibernateException e) { logger.error(
	 * "Error while adding Tablet " + tablet + ", Message " + e.getMessage());
	 * 
	 * e.printStackTrace();
	 * 
	 * return ResponseUtil.createSaveFailedResponse(); }
	 * 
	 * }
	 */

	public Tablet addCommonInitialValues(Tablet tablet)
	{
		if(tablet == null)
			return tablet;
		
		logger.debug(" adding Tablet CommonInitialValues " +tablet.getBrandName());
		Date currentDate = new Date();

		tablet.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		tablet.setAddedOn(currentDate);
		tablet.setModifiedOn(currentDate);
		tablet.setAddedBy(1l);
		tablet.setModifiedBy(1l);
		if (tablet.getMacAddress() != null)
			tablet.setMacAddress(tablet.getMacAddress().toUpperCase().trim());
		
		logger.debug(" exitting from adding Tablet CommonInitialValues " +tablet.getBrandName());
		return tablet;

	}

	@Transactional
	@Override
	public APIResponse getTabletById(Long id)
	{
		logger.debug("getTabletById id : " + id);

		if (id < 0)
		{
			logger.info("Tablet details are not available");
			return ResponseUtil.NoRecordFound();

		} else
		{
			Tablet tablet = hibernateTemplate.get(Tablet.class, id);

			logger.info("Got details tablet" + tablet);

			return ResponseUtil.recordFoundSucessFully()
					.setData(xfromTabletToVO(tablet));
		}
	}

	private TabletVO xfromTabletToVO(Tablet tablet)
	{
		TabletVO tabletVO = new TabletVO();
		
		if (tablet == null)
			return tabletVO;

		logger.info(" xfromTabletToVO " +tablet.getBrandName());

		BeanUtils.copyProperties(tablet, tabletVO);

		if (tablet.getUserByAssignedTo() != null)
		{
			User assignedUser = tablet.getUserByAssignedTo();
			TypeAheadVo aheadVo = new TypeAheadVo();
			aheadVo.setId(assignedUser.getId());
			aheadVo.setName(assignedUser.getLoginId());
			tabletVO.setUserByAssignedTo(aheadVo);
			/*
			 * vo.setUserByAssignedTo(tablet.getUserByAssignedTo().getId());
			 * vo.setAssignedToUserName(tablet.getUserByAssignedTo()
			 * .getFirstName());
			 */
		}

		if (tablet.getUserByCurrentUser() != null)
		{
			User currentUser = tablet.getUserByCurrentUser();
			TypeAheadVo aheadVo = new TypeAheadVo();
			aheadVo.setId(currentUser.getId());
			aheadVo.setName(currentUser.getLoginId());
			tabletVO.setUserByCurrentUser(aheadVo);
			/*
			 * vo.setUserByCurrentUser(tablet.getUserByCurrentUser().getId());
			 * vo.setCurrentUserName(tablet.getUserByCurrentUser()
			 * .getFirstName());
			 */
		}

		logger.info("tabletVO : " + tabletVO.getBrandName());
		return tabletVO;
	}

	@Transactional
	@Override
	public APIResponse getAllTablet()
	{
		logger.debug(" inside getAllTablet");
		List<TabletVO> tabletVos = new ArrayList<TabletVO>();
		try
		{

			// tabletList = hibernateTemplate.loadAll(Tablet.class);

			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(tabletBaseQuery);

			List<Object[]> result = query.list();

			if (result == null)
				return null;

			TabletVO vo = null;

			for (Object[] obj : result)
			{
				vo = new TabletVO(objectToLong(obj[0]), (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], objectToLong(obj[7]), GenericUtil
						.convertStringToDateFormat(obj[8]), objectToLong(obj[9]), GenericUtil
						.convertStringToDateFormat(obj[10]), objectToInt(obj[11]));

				TypeAheadVo tvo = new TypeAheadVo(objectToLong(obj[5]), (String) obj[12]);

				vo.setUserByAssignedTo(tvo);
				vo.setUserByCurrentUser(tvo);

				tabletVos.add(vo);

			}

		} catch (Exception e)
		{

			e.printStackTrace();
			logger.error("Exception in getAllTablet method of TabletDAOImpl  "
					+ e.getMessage());

		}
		logger.debug("Exitting from getAllTablet method of TabletDAOImpl  ");

		return ResponseUtil.recordFoundSucessFully().setData(tabletVos);
	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}

	private List<TabletVO> xformToVo(List<Tablet> tabletList)
	{
		if (tabletList == null)
			return null;

		logger.debug(" xformToVo " +tabletList.size());
		
		List<TabletVO> tabletVos = new ArrayList<TabletVO>();

		TabletVO vo = null;

		for (Tablet tablet : tabletList)
		{
			vo = new TabletVO();
			BeanUtils.copyProperties(tablet, vo);

			if (tablet.getUserByAssignedTo() != null)
			{
				User assignedUser = tablet.getUserByAssignedTo();
				TypeAheadVo aheadVo = new TypeAheadVo();
				aheadVo.setId(assignedUser.getId());
				aheadVo.setName(assignedUser.getLoginId());
				vo.setUserByAssignedTo(aheadVo);
				/*
				 * vo.setUserByAssignedTo(tablet.getUserByAssignedTo().getId());
				 * vo.setAssignedToUserName(tablet.getUserByAssignedTo()
				 * .getFirstName());
				 */
			}

			if (tablet.getUserByCurrentUser() != null)
			{
				User currentUser = tablet.getUserByCurrentUser();
				TypeAheadVo aheadVo = new TypeAheadVo();
				aheadVo.setId(currentUser.getId());
				aheadVo.setName(currentUser.getLoginId());
				vo.setUserByCurrentUser(aheadVo);
				/*
				 * vo.setUserByCurrentUser(tablet.getUserByCurrentUser().getId()
				 * ) ; vo.setCurrentUserName(tablet.getUserByCurrentUser()
				 * .getFirstName());
				 */
			}

			logger.info("vo : " + vo);
			tabletVos.add(vo);
		}
		return tabletVos;
	}

	@Override
	public Map<Long, String> getAllTabs()
	{
		logger.debug(" getAllTabs ");

		if (tabIdAndTabMacCache==null || tabIdAndTabMacCache.size() == 0)

			getTablets();
		
		if(tabIdAndTabMacCache!=null && !tabIdAndTabMacCache.isEmpty()){
			return tabIdAndTabMacCache;
		}else{
			return new ConcurrentHashMap<Long, String>();
		}

		
	}

	@Transactional
	@Override
	public APIResponse deleteTablet(Long id)
	{
		logger.debug(" deleteTablet " +id);
		try
		{
			Tablet tablet = hibernateTemplate.load(Tablet.class, id);
			tablet.setStatus(0);
			hibernateTemplate.update(tablet);
		} catch (Exception e)
		{

			e.printStackTrace();
			logger.info("Exception in deleteTablet method of TabletDAOImpl  "
					+ e.getMessage());
			return ResponseUtil.createDeleteFailedResponse();
		}
		
		logger.debug("Exitting from  deleteTablet ");
		return ResponseUtil.createDeleteSuccessResponse();
	}

	@Transactional
	@Override
	public APIResponse updateTablet(TabletVO tabletVo)
	{
		if(tabletVo == null)
			return ResponseUtil.nullArgument();
		
		logger.debug(" updateTablet " +tabletVo.getBrandName());
		if (tabletVo.getBrandName() == null)
		{
			tabletVo.setBrandName("");

		}
		try
		{
			jdbcTemplate.update("update Tablet set tabName = '"
					+ tabletVo.getTabName() + "',brandName='"
					+ tabletVo.getBrandName() + "',assignedTo="
					+ tabletVo.getUserByAssignedTo().getId() + ",currentUser="
					+ tabletVo.getUserByCurrentUser().getId()
					+ " where macAddress='" + tabletVo.getMacAddress() + "'");
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.info("Exception in updateTablet method of TabletDAOImpl "
					+ e.getMessage());
			return ResponseUtil.createUpdateFailedResponse();
		}
		
		logger.debug(" Extting updateTablet " +tabletVo.getBrandName());
		return ResponseUtil.createUpdateSuccessResponse();
	}

	@Transactional
	public List<UserAndRegIdDetails> getUserAndRegIdDetails(Long currentUser)
	{
		logger.debug("inside  getUserAndRegistationDetailsVO of TabletDAOImpl");

		String strQuery = "SELECT user.emailId , user.mobileNo, tab.registrationId FROM User user, Tablet tab  where user.id = tab.currentUser and user.id = :userId";
		StringBuilder completeQuery = new StringBuilder(strQuery);

		Query query = hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery(completeQuery.toString());

		if (currentUser >= 0)
			query.setLong("userId", currentUser);
		logger.info("completeQuery.toString() " + query.getQueryString());

		List<Object[]> result = query.list();

		if (result == null)
			return null;

		List<UserAndRegIdDetails> messageList = new ArrayList<UserAndRegIdDetails>();
		UserAndRegIdDetails message;

		for (Object[] obj : result)
		{
			message = new UserAndRegIdDetails();
			message.setEmailId((String) obj[0]);
			message.setMobileNo(objectToLong(obj[1]));
			message.setRegistrationId((String) obj[2]);
			messageList.add(message);
		}

		logger.debug("Exit from  getUserAndRegistationDetailsVO of TabletDAOImpl messageList "
				+ messageList);
		return messageList;
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());
	}

	@Override
	public long getTabletIdByTabletMac(String macAddress)
	{
		logger.debug(" updateTablet " +macAddress);
		if (tabMacAndIdMap.size() == 0)
			getTablets();
		if (tabMacAndIdMap.get(macAddress) != null)
			return tabMacAndIdMap.get(macAddress);
		else
			return 0;
	}

	@Override
	public List<TabletFilterVO> tabPagination(TabFilter tf)
	{
		logger.debug(" tabPagination " +tf);
		StringBuilder queryStr = new StringBuilder(PAGINATIONQUERY);

		if (tf != null)
		{

			if (tf.getPageSize() > 0)
				;
			else
			{
				tf.setStartOffset(0);

				tf.setPageSize(MIN_ITEM_PER_PAGE);

			}
		}

		if (tf != null && tf.getCurrentUserIds() != null && !tf.getCurrentUserIds().isEmpty())
		{
			queryStr.append(" and t.currentUser  in ("
					+ StringUtils.join(tf.getCurrentUserIds(), ",") + ")");

		}

		if (tf != null && tf.getAssignedUserIds() != null
				&& !tf.getAssignedUserIds().isEmpty())
		{
			queryStr.append(" and t.assignedTo  in ("
					+ StringUtils.join(tf.getAssignedUserIds(), ",") + ")");
		}

		if (tf != null && tf.getMacAddress() != null && !tf.getMacAddress().isEmpty())
		{
			queryStr.append(" and t.macAddress  = '" + tf != null ? tf.getMacAddress() : null + "'");
		}

		queryStr.append(" limit " + tf !=null ? tf.getStartOffset() : null + ","
				+ tf.getPageSize() + ";");

		// "select t.tabName,t.macAddress, t.brandName, (select firstName from
		// User where id=t.currentUser) as CurrentUser, (select firstName from
		// User where id=t.assignedTo) as AssignedUser,t.status , (select Id
		// from User where id=t.currentUser) as CurrentUserId , (select Id from
		// User where id=t.assignedTo) as AssignedUserId from Tablet t";
		if (true)
		{

		}

		logger.info("query string in tablet dao is::::::" + queryStr.toString());

		List<TabletFilterVO> tablist = (List) jdbcTemplate.query(queryStr
				.toString(), new TabletRowMapper());

		logger.debug("Exitting from  tabPagination ");
		if (tablist != null && !tablist.isEmpty())
		{
			return tablist;

		} else
		{
			return new ArrayList<>();
		}

	}

	public class TabletRowMapper implements RowMapper<TabletFilterVO>
	{

		@Override
		public TabletFilterVO mapRow(ResultSet rs, int i) throws SQLException
		{
			TabletFilterVO tab = new TabletFilterVO();

			tab.setMacAddress(rs.getString("t.macAddress"));
			tab.setBrandName(rs.getString("t.brandName"));
			tab.setAssignedTo(rs.getString("AssignedUser"));
			tab.setCurrentUserName(rs.getString("CurrentUser"));
			tab.setTabName(rs.getString("t.tabName"));
			tab.setStatus(rs.getString("t.status"));
			tab.setCurrentUserId(rs.getString("CurrentUserId"));
			tab.setAssignedToId(rs.getString("AssignedUserId"));

			return tab;
		}

	}

	@Override
	@Transactional
	public boolean getTabletByMacAddress(String macAddress)
	{
		if(macAddress == null)
			return false;
		logger.debug(" getTabletByMacAddress " +macAddress);
		try
		{
			@SuppressWarnings("unchecked")
			List<Tablet> tabDetailsList = hibernateTemplate
					.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select * from Tablet where macAddress=:macAddress")
					.addEntity(Tablet.class).setParameter("macAddress", macAddress)
					.list();

			logger.info("Mac Address details  for " + macAddress + " are "
					+ tabDetailsList.size());

			if (tabDetailsList.size() > 0)
				return true;
			else
				return false;
			
		} catch (Exception e)
		{
			logger.error("Error while getting data for mac " + macAddress);
			e.printStackTrace();
			return false;
		}
	}

}
