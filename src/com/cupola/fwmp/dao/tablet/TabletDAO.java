package com.cupola.fwmp.dao.tablet;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.filters.TabFilter;
import com.cupola.fwmp.persistance.entities.Tablet;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.LoginPojo;
import com.cupola.fwmp.vo.TabDetailsVo;
import com.cupola.fwmp.vo.TabletFilterVO;
import com.cupola.fwmp.vo.TabletVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserAndRegIdDetails;

public interface TabletDAO
{

	APIResponse updateRegistrationId(TabDetailsVo tabDetailsVo);

	public APIResponse getTabletById(Long id);

	public APIResponse getAllTablet();

	public APIResponse deleteTablet(Long id);

	public APIResponse updateTablet(TabletVO tabletVo);

	List<TypeAheadVo> tabSearchSuggestion(String tabQuery);

	String setCurrentUser(LoginPojo loginPojo, Long userId);

	String checkCurrentUser(LoginPojo loginPojo);

	List<UserAndRegIdDetails> getUserAndRegIdDetails(Long currentUser);

	long getTabletIdByTabletMac(String macAddress);

	Map<Long, String> getAllTabs();

	public List<TabletFilterVO> tabPagination(TabFilter tf);

	Tablet addTablet(TabletVO tabletVo);
	
	boolean getTabletByMacAddress(String macAddress);

}
