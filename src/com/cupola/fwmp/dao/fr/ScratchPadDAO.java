/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.fr;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.ControlBlock;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.fr.Activity;
import com.cupola.fwmp.vo.fr.AndroidFlowCxDown;
import com.cupola.fwmp.vo.fr.Decision;
import com.cupola.fwmp.vo.fr.DefectCode;
import com.cupola.fwmp.vo.fr.ETRDetails;
import com.cupola.fwmp.vo.fr.ScratchPadLog;
import com.cupola.fwmp.vo.fr.ScratchPadVo;
import com.cupola.fwmp.vo.fr.ScratchVo;
import com.cupola.fwmp.vo.fr.SubDefectCode;

public interface ScratchPadDAO {
	Map<Integer, Decision> getAllDecisions();
	Map<Integer, Activity> getAllActivities();
	ControlBlock spAPI(ScratchVo scratchVo);
	List<ScratchPadLog> getScratchPadByTicketId(long ticketId);
	APIResponse getVersionDetails();
	Map<Integer, DefectCode> getAllDefectCodes();
	Map<Integer, SubDefectCode> getAllSubDefectCodes();
	Map<Integer,ETRDetails> getAllETRDetails();
	ETRDetails getETRDetailsByStepID(int stepID);
	Date calculateCurrentEtr(long ticketId,int stepID);
	int calculateProgressWeightage(long ticketId,int stepID);
	APIResponse resetScratchPadCache();
	Map<Integer, AndroidFlowCxDown> getAllCxDownAndroidFlow();
	Map<Integer, AndroidFlowCxDown> getAllSlowSpeedAndroidFlow();
	Map<Integer, AndroidFlowCxDown> getAllFDAndroidFlow();
	Map<Integer, AndroidFlowCxDown> getAllCxUpAndroidFlow();
	List<ScratchPadVo> getContextRuleByTicketId(long ticketId);
	Map<Integer, AndroidFlowCxDown> getAllSeniorElementAndroidFlow();
	void delectContextDataInScratchPad(long ticketId, int contextType);
	
 
}
