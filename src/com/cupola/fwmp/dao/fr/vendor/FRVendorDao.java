package com.cupola.fwmp.dao.fr.vendor;

import java.util.List;

import com.cupola.fwmp.persistance.entities.VendorAllocation;
import com.cupola.fwmp.persistance.entities.VendorTicket;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FRVendorVO;
import com.cupola.fwmp.vo.fr.VendorNEAssignVO;
import com.cupola.fwmp.vo.fr.VendorScheduleVO;
import com.cupola.fwmp.vo.fr.VendorTicketUpdateVO;

public interface FRVendorDao
{

	public int associateVendorUserToMultipleNE(
			List<VendorNEAssignVO> vendorNEAssignVO);

	public int associateVendorUserToNE(long vendorUserId, Long associateToUserId);

	public int assignTicketToVendor(long vendorUserId, long ticketId);

	public int updateActualTimeToTicketByVendor(VendorTicket ticketData);

	public int updateTicketStatus(long ticketId, int status);

	public  List<VendorScheduleVO>  getVendorUserSchedule(long vendorUserId,
			long status);

	List<TypeAheadVo> getAllSkills();

	List<TypeAheadVo> getVendors(FRVendorFilter filter);

	List<FRVendorVO> getVendorsForTicket(long ticketId);

	int updateEstimatedTimeToTicketByVendor(VendorTicket ticketData);

	List<FRVendorVO> getReportingVendors(FRVendorFilter filter);

	public int deleteAssociateVendorUserToNE(long vendorUserId);

	List<FRVendorVO> assignOrUpdateVendorTicket(VendorTicketUpdateVO vo);

	public List<VendorAllocation> getAllVendorAssociateWithNes();

	List<FRVendorVO> getVendorsWithDetails(FRVendorFilter filter);

}