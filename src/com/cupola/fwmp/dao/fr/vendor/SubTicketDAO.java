/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.fr.vendor;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.VendorAllocation;
import com.cupola.fwmp.persistance.entities.SubTickets;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FRVendorVO;
import com.cupola.fwmp.vo.fr.VendorNEAssignVO;
import com.cupola.fwmp.vo.fr.VendorScheduleVO;
import com.cupola.fwmp.vo.fr.VendorTicketUpdateVO;

public interface SubTicketDAO
{

	public int associateVendorUserToMultipleNE(
			List<VendorNEAssignVO> vendorNEAssignVO);

	public int associateVendorUserToNE(long vendorUserId, Long associateToUserId);

	public int assignTicketToVendor(long vendorUserId, long ticketId);

	public int updateActualTimeToTicketByVendor(SubTickets ticketData);

	public int updateTicketStatus(long ticketId, int status);

	public  List<VendorScheduleVO>  getVendorUserSchedule(long vendorUserId,
			List<Integer> pendingDef);

	List<TypeAheadVo> getAllSkills();

	List<TypeAheadVo> getVendors(FRVendorFilter filter);

	List<FRVendorVO> getVendorsForTicket(long ticketId);

	int updateEstimatedTimeToTicketByVendor(SubTickets ticketData);

	List<FRVendorVO> getReportingVendors(FRVendorFilter filter);

	public int deleteAssociateVendorUserToNE(long vendorUserId);

	List<FRVendorVO> assignOrUpdateVendorTicket(VendorTicketUpdateVO vo);

	public List<VendorAllocation> getAllVendorAssociateWithNes();

	List<FRVendorVO> getVendorsWithDetails(FRVendorFilter filter);

	public Map<Long, List<Long>> getAllAssociateVendorIdsWithTl();
	
	public List<SubTickets> getUserWithType(Integer userType, Long ticketId);

	public Long addSubTicketToVendor(SubTickets ticketData);

	public boolean isSubTicketExistForUserWithSameRequest(SubTickets ticket);

	public int updateTicketStatusByCCNR(long ticketId, int status);
	
	public int assignSubTicketToVendor(SubTickets ticketData);

}