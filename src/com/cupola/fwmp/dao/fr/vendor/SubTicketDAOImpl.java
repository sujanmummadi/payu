/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.fr.vendor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.FRConstant.UserType;
import com.cupola.fwmp.persistance.entities.VendorAllocation;
import com.cupola.fwmp.persistance.entities.SubTickets;
import com.cupola.fwmp.service.fr.vendor.VendorCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FRVendorVO;
import com.cupola.fwmp.vo.fr.VendorNEAssignVO;
import com.cupola.fwmp.vo.fr.VendorScheduleVO;
import com.cupola.fwmp.vo.fr.VendorTicketUpdateVO;

@Transactional
public class SubTicketDAOImpl implements SubTicketDAO
{
	private static Logger LOGGER = Logger
			.getLogger(SubTicketDAOImpl.class.getName());

	private static String RESET_VENDOR_ALLOCATION = "delete from VendorAllocation where userId = :userId ";

	private static String CREATE_VENDOR_ALLOCATION = "insert into VendorAllocation values (:id,:allocatedToUserId,:currentAllocatedToUserId,:userId,:allocatedBy,now(),1);";

	private static List<TypeAheadVo> skills = new ArrayList<TypeAheadVo>();
	private static String UPDATE_ESTIMATED_TIME = "update SubTickets set  estimatedStartTime =  :estimatedStartTime, estimatedEndTime = :estimatedEndTime where userId =:vendorId and ticketID = :ticketId ";
	private static String UPDATE_ACTUAL_TIME = "update SubTickets set  actualStartTime =  :actualStartTime, actualEndTime= :actualEndTime where userId =:vendorId and ticketID = :ticketId ";

	public static String UPDATE_TICKET_STATUS = "update SubTickets  set status=:status,addedOn=now(),outStatus=:outStatus where ticketID=:ticketID";

	// id, userId, ticketID, estimatedStartTime, estimatedEndTime,
	// actualStartTime, actualEndTime,
	// addedBy, addedOn, status, id, id

	private static String VENDOR_SCHEDULE = "SELECT vt.status,vt.estimatedStartTime FROM SubTickets vt where vt.userId = :userId and vt.status in(:status) order by vt.estimatedStartTime and vt.userType="
			+ UserType.VENDOR + " ";
	private static String ASSIGN_TICKET_TO_VENDOR = "insert into SubTickets  (id, userId,userType, ticketID,requestedBy,addedBy, addedOn, status, inStatus) values (:id,:userId,:userType,:ticketID,:requestedBy,:addedBy,now(),1,1);";
	private static String SKILL_LIST = "select id,skillName from Skill";

	private static String VENDOR_ALLOCATED = "Select distinct u.id,u.firstName ,sk.skillName,usm.idSkill from User u left join  VendorAllocation va on va.userId = u.id "
			+ " inner Join UserSkillMapping usm on usm.idUser = u.id inner Join Skill sk on sk.id = usm.idSkill left join SubTickets vt on vt.userId = va.userId  "
			+ " where u.id > 0 ";

	private static String VENDOR_REPORTING = "Select u.id,u.firstName ,sk.skillName,usm.idSkill from User u inner Join "
			+ " UserSkillMapping usm on usm.idUser = u.id inner Join Skill sk on sk.id = usm.idSkill "
			+ " where u.id > 0 ";

	private static String VENDOR_CURRENT_ALLOCATED = "Select va.currentAllocatedToUserId ,u.firstName from  User u inner join  VendorAllocation va on va.currentAllocatedToUserId = u.id where va.userId  = :userId";

	public static String VENDOR_FOR_TICKETS_ALLOCATED = "Select u.id,u.firstName ,vt.estimatedStartTime,vt.status , vt.skillId   from User u "
			+ " inner join SubTickets vt on vt.userId = u.id   "
			+ " where u.id > 0 and vt.skillId in (:skills)";

	public static String VENDOR_ALLOCATION = "INSERT INTO "

			+ "VendorAllocation"
			+ "(id, allocatedToUserId, currentAllocatedToUserId, userId, allocatedBy, allocatedOn, status) "

			+ "VALUES " + "(?, ?, ?, ?, ?, ?, ?)";

	public static String UPDATE_VENDOR_TICKET_DATA = "Update SubTickets vt set vt.userId = :newUserId, vt.estimatedStartTime= :estimatedStartTime, vt.addedBy = :addedBy  where vt.ticketID = :ticketId and vt.userId = :userId";

	public static String SQL_TL_ASSOCIATED_VENDOR_WITH_TL = "SELECT usr.firstName,usr.lastName,usr.id AS userId,usr.reportTo "
			+ " FROM User usr INNER JOIN  UserGroupUserMapping ugm ON ugm.idUser = usr.id  "
			+ " INNER JOIN UserGroup ug ON ug.id = ugm.idUserGroup  "
			+ " WHERE usr.status = 1 AND ug.userGroupcode = 'VENDOR'";

	public static String SUB_TICKET_USERTYPE = "select * from SubTickets ";

	public static String SAME_SUB_TICKET_SQL = "select * from SubTickets "
			+ "where ticketID = :id and requestedBy = :requestby and userId = :userId ";

	private static String ASSIGN_SUB_TICKET_TO_VENDOR = "insert into SubTickets  (id, userId,userType, ticketID,requestedBy,estimatedStartTime,estimatedEndTime,actualStartTime,actualEndTime,addedBy, addedOn, status, inStatus,skillId) values (:id,:userId,:userType,:ticketID,:requestedBy,:estimatedStartTime,:estimatedEndTime,:actualStartTime,:actualEndTime,:addedBy,now(), :status, :inStatus,:skillId)";

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public int associateVendorUserToMultipleNE(
			List<VendorNEAssignVO> vendorNEAssignVO)
	{
		if(vendorNEAssignVO == null)
			return 0;
		
		LOGGER.info(" associateVendorUserToMultipleNE  " + vendorNEAssignVO.size());
		
		List<VendorAllocation> vendorAllocations = new ArrayList<VendorAllocation>();

		VendorAllocation vo = null;
		int deleteRecords = -1;
		long allocatedById = AuthUtils.getCurrentUserId();
		int[] noOfInsertedRecords = null;
		Date allocatedOn = new Date();
		try
		{
			for (VendorNEAssignVO vendorNEAssignVo : vendorNEAssignVO)
			{
				if (vendorNEAssignVo.getVendorId() <= 0)
				{
					LOGGER.debug("found vendorId::"
							+ vendorNEAssignVo.getVendorId()
							+ "and listOfUserIds::"
							+ vendorNEAssignVo.getUserIds());
					continue;
				}
				deleteRecords = deleteAssociateVendorUserToNE(vendorNEAssignVo
						.getVendorId());
				if (deleteRecords > 0)
					LOGGER.debug("vendorId as:" + vendorNEAssignVo.getVendorId()
							+ "deleted successfuly");

				if (vendorNEAssignVo.getUserIds() == null
						|| vendorNEAssignVo.getUserIds().isEmpty())
				{
					continue;
				}

				for (Iterator iterator = vendorNEAssignVo.getUserIds()
						.iterator(); iterator.hasNext();)
				{

					vo = new VendorAllocation();
					vo.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					vo.setAllocatedBy(allocatedById);
					vo.setAllocatedOn(allocatedOn);
					vo.setUserId(vendorNEAssignVo.getVendorId());
					vo.setStatus(1);
					Long neId = (Long) iterator.next();
					vo.setCurrentAllocatedToUserId(neId);
					vo.setAllocatedToUserId(neId);
					vendorAllocations.add(vo);

					LOGGER.debug("Vendor " + vendorNEAssignVo.getVendorId()
							+ " Associated with users " + neId);
				}

			}
			noOfInsertedRecords = associateVendorUserToMultipleNEs(vendorAllocations);
			if (noOfInsertedRecords.length > 0)
				VendorCache.addToVendorCahe();

		} catch (Exception e)
		{
			LOGGER.error("Error While Associate Vendor to MultipleNE::"
					+ e.getMessage());
		}

		LOGGER.debug(" Exit from associateVendorUserToMultipleNE  " + vendorNEAssignVO.size());
		return noOfInsertedRecords == null ? deleteRecords
				: noOfInsertedRecords.length;

	}

	private int[] associateVendorUserToMultipleNEs(
			final List<VendorAllocation> vendorAllocations)
	{
		int[] noOfRecords = null;
		
		if(vendorAllocations == null)
			return noOfRecords;
		LOGGER.debug(" associateVendorUserToMultipleNE  " + vendorAllocations.size());
		
		try
		{

			noOfRecords = jdbcTemplate
					.batchUpdate(VENDOR_ALLOCATION, new BatchPreparedStatementSetter()
					{

						@Override
						public int getBatchSize()
						{
							return vendorAllocations.size();
						}

						@Override
						public void setValues(java.sql.PreparedStatement ps,
								int arg1) throws SQLException
						{
							VendorAllocation va = vendorAllocations.get(arg1);
							ps.setLong(1, va.getId());
							ps.setLong(2, va.getAllocatedToUserId());
							ps.setLong(3, va.getCurrentAllocatedToUserId());
							ps.setLong(4, va.getUserId());
							ps.setLong(5, va.getAllocatedBy());
							ps.setDate(6, new java.sql.Date(va.getAllocatedOn()
									.getTime()));
							ps.setInt(7, va.getStatus());
						}
					});

		} catch (Exception e)
		{
			LOGGER.error("Error While Associate Vendor to MultipleNE::"
					+ e.getMessage());

		}
		LOGGER.debug(" Exit from associateVendorUserToMultipleNE  " + vendorAllocations.size());
		return noOfRecords;
	}

	public int associateVendorUserToNE(long vendorUserId,
			Long associateToUserId)
	{
		LOGGER.debug(" inside  associateVendorUserToNE  " + vendorUserId);
		
		int noOfRecords = 0;
		try
		{
			deleteAssociateVendorUserToNE(vendorUserId);

			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(CREATE_VENDOR_ALLOCATION)
					.setParameter("id", StrictMicroSecondTimeBasedGuid
							.newGuid())
					.setParameter("allocatedToUserId", associateToUserId)
					.setParameter("currentAllocatedToUserId", associateToUserId)
					.setParameter("allocatedBy", AuthUtils.getCurrentUserId())
					.setParameter("userId", vendorUserId).executeUpdate();

			if (noOfRecords > 0)
				VendorCache.addToVendorCahe(associateToUserId, vendorUserId);

		} catch (Exception e)
		{
			LOGGER.error("Error While Associate Vendor to NE::"
					+ e.getMessage());

		}
		LOGGER.debug(" Exit from  associateVendorUserToNE  " + vendorUserId);
		return noOfRecords;

	}

	public int deleteAssociateVendorUserToNE(long vendorUserId)
	{
		LOGGER.debug(" inside  deleteAssociateVendorUserToNE  " + vendorUserId);
		int noOfRecords = 0;
		try
		{

			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(RESET_VENDOR_ALLOCATION)
					.setParameter("userId", vendorUserId).executeUpdate();
			LOGGER.info("delete vendor vendorId::" + vendorUserId);
			if (noOfRecords > 0)
				VendorCache.addToVendorCahe();

		} catch (Exception e)
		{
			LOGGER.error("Error While Deleting Associate Vendor to User::"
					+ e.getMessage());
		}
		LOGGER.debug(" Exit from  deleteAssociateVendorUserToNE  " + vendorUserId);
		return noOfRecords;

	}

	@Override
	public List<FRVendorVO> assignOrUpdateVendorTicket(VendorTicketUpdateVO vo)
	{
		if(vo == null)
			return null;
		
		LOGGER.debug(" inside  assignOrUpdateVendorTicket  " +  vo.getTicketId());
		try
		{
			sessionFactory.getCurrentSession()
					.createSQLQuery(UPDATE_VENDOR_TICKET_DATA)
					.setParameter("newUserId", vo.getVendorUserId())
					.setTimestamp("estimatedStartTime", vo
							.getEstimatedStartTime())
					.setParameter("ticketId", vo.getTicketId())
					.setParameter("addedBy", AuthUtils.getCurrentUserId())
					.setParameter("userId", vo.getOldVendorId())
					.executeUpdate();

			LOGGER.info("New Vendor " + vo.getVendorUserId()
					+ " Associated with Ticket " + vo.getTicketId());

			return getVendorsForTicket(vo.getTicketId());

		} catch (Exception e)
		{

			e.printStackTrace();
			LOGGER.error("Error in New Vendor " + vo.getVendorUserId()
					+ " Associated with Ticket " + vo.getTicketId());

		}
		LOGGER.debug(" Exit from  assignOrUpdateVendorTicket  " +  vo.getTicketId());
		return null;
	}

	public int assignTicketToVendor(long vendorUserId, long ticketId)

	{
		LOGGER.debug(" inside  assignTicketToVendor  " +  ticketId);
		int noOfRecords = 0;

		try
		{
			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(ASSIGN_TICKET_TO_VENDOR)
					.setParameter("id", StrictMicroSecondTimeBasedGuid
							.newGuid())
					.setParameter("ticketID", ticketId)
					.setParameter("userType", UserType.VENDOR)
					.setParameter("requestedBy", AuthUtils.getCurrentUserId())
					.setParameter("addedBy", AuthUtils.getCurrentUserId())
					.setParameter("userId", vendorUserId).executeUpdate();

			LOGGER.info("Vendor " + vendorUserId + " Associated with Ticket "
					+ ticketId);

		} catch (Exception e)
		{
			LOGGER.error("Error While Assiging Ticket to Vendor::"
					+ e.getMessage());
		}
		
		LOGGER.debug(" Exit from  assignTicketToVendor  " +  ticketId);
		return noOfRecords;
	}

	@Override
	public Long addSubTicketToVendor(SubTickets ticketData)
	{
		Long rowsAffected = 0l;
		if (ticketData == null)
			return rowsAffected;
		
		LOGGER.debug(" inside  addSubTicketToVendor  " +  ticketData.getId());
		try
		{

			if (ticketData.getId() <= 0)
				ticketData.setId(StrictMicroSecondTimeBasedGuid.newGuid());

			rowsAffected = (Long) hibernateTemplate.save(ticketData);

		} catch (Exception exception)
		{
			LOGGER.error("Error occure While adding sub Ticket to Vendor::"
					+ exception.getMessage());
		}
		LOGGER.debug(" Exit From  addSubTicketToVendor  " +  ticketData.getId());
		return rowsAffected;
	}

	@Override
	public boolean isSubTicketExistForUserWithSameRequest(SubTickets ticket)
	{
		if(ticket == null)
			return false;
		
		LOGGER.debug(" inside  isSubTicketExistForUserWithSameRequest  " +  ticket.getTicketId());
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(SAME_SUB_TICKET_SQL)
					.addEntity(SubTickets.class)
					.setParameter("id", ticket.getTicketId())
					.setParameter("requestby", ticket.getRequestedBy())
					.setParameter("userId", ticket.getUserId());

			List<SubTickets> sub = query.list();
			if (sub != null && !sub.isEmpty())
				return true;

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		LOGGER.debug(" Exit from isSubTicketExistForUserWithSameRequest  " +  ticket.getTicketId());
		return false;
	}

	@Override
	public int updateEstimatedTimeToTicketByVendor(SubTickets ticketData)
	{
		int noOfRecords = 0;
		
		LOGGER.debug(" inside  updateEstimatedTimeToTicketByVendor  " +  ticketData.getTicketId());
		try
		{
			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(UPDATE_ESTIMATED_TIME)
					.setTimestamp("estimatedStartTime", ticketData
							.getEstimatedStartTime())
					.setTimestamp("estimatedEndTime", ticketData
							.getEstimatedEndTime())
					.setParameter("vendorId", ticketData.getUserId())
					.setParameter("ticketId", ticketData.getTicketId())
					.executeUpdate();

			LOGGER.info("Estimated time updated for ticket "
					+ ticketData.getTicketId() + " Start "
					+ ticketData.getEstimatedStartTime());

		} catch (Exception e)
		{
			LOGGER.error("Error While Updating EstimatedTimeToTicket::"
					+ e.getMessage());
		}
		
		LOGGER.debug(" Exit form updateEstimatedTimeToTicketByVendor  " +  ticketData.getTicketId());
		return noOfRecords;

	}

	public int updateActualTimeToTicketByVendor(SubTickets ticketData)
	{
		int noOfRecords = 0;
		
		if(ticketData == null)
			return noOfRecords;
		
		LOGGER.debug(" inside  updateActualTimeToTicketByVendor  " +  ticketData.getTicketId());
		try
		{
			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(UPDATE_ACTUAL_TIME)
					.setTimestamp("actualStartTime", ticketData
							.getActualStartTime())
					.setTimestamp("actualEndTime", ticketData
							.getActualStartTime())
					.setParameter("vendorId", ticketData.getUserId())
					.setParameter("ticketId", ticketData.getTicketId())
					.executeUpdate();

			LOGGER.info("Actual time updated for ticket "
					+ ticketData.getTicketId() + " Start "
					+ ticketData.getTicketId());

		} catch (Exception e)
		{
			LOGGER.error("Error While Updating ActualTimeToTicket::"
					+ e.getMessage());
		}
		LOGGER.debug(" Exit From  updateActualTimeToTicketByVendor  " +  ticketData.getTicketId());
		return noOfRecords;

	}

	public int updateTicketStatus(long ticketId, int status)
	{

		int noOfRecords = 0;
		
		LOGGER.debug(" inside  updateTicketStatus  " +   ticketId);

		try
		{
			// UPDATE_TICKET_STATUS

			final String VENDOR_COMPLETED_TICKET_SQL = "UPDATE FrDetail fr INNER JOIN SubTickets sub  "
					+ "ON fr.ticketId = sub.ticketID "
					+ "SET sub.status = :status, sub.outStatus = :outStatus , fr.nonBlocked = :blockedFlag "
					+ "WHERE fr.ticketId = :ticketId ";

			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(VENDOR_COMPLETED_TICKET_SQL)
					.setParameter("status", status)
					.setParameter("outStatus", status)
					.setParameter("blockedFlag", false)
					.setParameter("ticketId", ticketId).executeUpdate();

			LOGGER.info("ticketId::" + ticketId + "changed status::" + status);

		} catch (Exception e)
		{
			LOGGER.error("Error While Assiging Ticket to Vendor::"
					+ e.getMessage());
		}
		LOGGER.debug(" Exit from  updateTicketStatus  " +   ticketId);
		return noOfRecords;
	}

	@Override
	public int updateTicketStatusByCCNR(long ticketId, int status)
	{
		int noOfRecords = 0;
		LOGGER.debug(" inside  updateTicketStatusByCCNR  " +   ticketId);

		try
		{
			// UPDATE_TICKET_STATUS

			final String VENDOR_COMPLETED_TICKET_SQL = "UPDATE FrDetail fr INNER JOIN SubTickets sub  "
					+ "ON fr.ticketId = sub.ticketID "
					+ "SET sub.status = :status, sub.outStatus = :outStatus "
					+ "WHERE fr.ticketId = :ticketId ";

			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(VENDOR_COMPLETED_TICKET_SQL)
					.setParameter("status", status)
					.setParameter("outStatus", status)
					.setParameter("ticketId", ticketId).executeUpdate();

			LOGGER.info("ticketId::" + ticketId + "changed status::" + status);

		} catch (Exception e)
		{
			LOGGER.error("Error While Assiging Ticket to Vendor::"
					+ e.getMessage());
		}
		LOGGER.debug(" Exit from  updateTicketStatusByCCNR  " +   ticketId);
		return noOfRecords;
	}

	@SuppressWarnings("unchecked")
	public List<VendorScheduleVO> getVendorUserSchedule(long vendorUserId,
			List<Integer> pendingDef)
	{
		LOGGER.debug(" inside  getVendorUserSchedule  " +   vendorUserId);
		List<VendorScheduleVO> schedules = new ArrayList<VendorScheduleVO>();
		try
		{
			List<Object[]> result = sessionFactory.getCurrentSession()
					.createSQLQuery(VENDOR_SCHEDULE)
					.setParameter("userId", vendorUserId)
					.setParameterList("status", pendingDef).list();

			if (result == null || result.isEmpty())
				return null;
			LOGGER.info("result :" + result);
			VendorScheduleVO vo = null;
			for (Object[] obj : result)
			{
				vo = new VendorScheduleVO();
				vo.setStatus(objectToInt(obj[0]));
				vo.setEstimatedStartTime(GenericUtil
						.convertToUiDateFormat(obj[1]));
				schedules.add(vo);
			}

		} catch ( Exception e)
		{
			LOGGER.error("Exception in getVendorUserSchedule :"+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOGGER.debug(" Exit from  getVendorUserSchedule  " +   vendorUserId);
		return schedules;
	}

	@Override
	public List<FRVendorVO> getVendorsForTicket(long ticketId)
	{
		LOGGER.debug(" inside  getVendorsForTicket  " +   ticketId);
		List<FRVendorVO> vendorList = null;
		try
		{
			vendorList = new ArrayList<FRVendorVO>();
			StringBuilder completeQuery = new StringBuilder(VENDOR_FOR_TICKETS_ALLOCATED);

			if (ticketId > 0)
				completeQuery.append(" and vt.ticketID = ").append(ticketId);

			Query q = sessionFactory.getCurrentSession()
					.createSQLQuery(completeQuery.toString())
					.setParameterList("skills",ActionTypeAttributeDefinitions.SKILL_DEFINITION.keySet() );

			LOGGER.info("COmplete query VENDOR_FOR_TICKETS_ALLOCATED "
					+ q.getQueryString());

			List<Object[]> result = q.list();
			if (result == null)
				return null;

			FRVendorVO vvo = null;
			for (Object[] obj : result)
			{

				vvo = new FRVendorVO();
				vvo.setVendorId(objectToLong(obj[0]));
				vvo.setVendorSkill(ActionTypeAttributeDefinitions.SKILL_DEFINITION
						.get(objectToInt(obj[4])));
				vvo.setEstimatedStartTime(GenericUtil
						.convertToUiDateFormat(obj[2]));
				vvo.setStatus(objectToInt(obj[3]));
				vvo.setVendorSkillId(objectToLong(obj[4]));
				vvo.setVendorName((String) obj[1] );
				
				vvo.setCurrentStatus("");
				if(ActionTypeAttributeDefinitions.FR_VENDOR_PENDING_STATUS
						.contains(Long.valueOf(vvo.getStatus()+"")))
					vvo.setCurrentStatus("0");
				else if( ActionTypeAttributeDefinitions.VENDOR_TASK_DONE_STATUS
						.contains(Long.valueOf(vvo.getStatus()+"")))
					vvo.setCurrentStatus("1");
				
				vendorList.add(vvo);

			}
			return vendorList;
		} catch (Exception e)
		{
			LOGGER.error("Error While getting Vendor for Ticket::"
					+ e.getMessage());
		}
		LOGGER.debug(" Exit from  getVendorsForTicket  " +   ticketId);
		return vendorList;
	}

	@Override
	public List<FRVendorVO> getReportingVendors(FRVendorFilter filter)
	{
		// and usm.idSkill = :skillId and u.reportTo = :reportToId
		List<FRVendorVO> vendorList = null;
		
		if(filter == null)
			return vendorList;
		
		LOGGER.info(" inside  getReportingVendors  " +   filter.getTicketId());
		try
		{
			vendorList = new ArrayList<FRVendorVO>();
			StringBuilder completeQuery = new StringBuilder(VENDOR_REPORTING);

			if (filter.getReportToUserId() > 0)
			{
				completeQuery.append(" and  u.reportTo = ")
						.append(filter.getReportToUserId());
			}

			if (filter.getAssocatedToUserId() > 0)
			{
				completeQuery.append(" and  va.currentAllocatedToUserId =")
						.append(filter.getAssocatedToUserId());
			}

			if (filter.getSkillId() > 0)
				completeQuery.append(" and usm.idSkill = ")
						.append(filter.getSkillId());

			if (filter.getTicketId() > 0)
				completeQuery.append(" and vt.ticketID = ")
						.append(filter.getTicketId());

			completeQuery.append(" order by sk.id ");
			Query q = sessionFactory.getCurrentSession()
					.createSQLQuery(completeQuery.toString());

			LOGGER.info("Complete query getvendors " + q.getQueryString());

			List<Object[]> result = q.list();
			if (result == null)
				return null;

			FRVendorVO vvo = null;
			for (Object[] obj : result)
			{
				vvo = new FRVendorVO();
				vvo.setVendorId(objectToLong(obj[0]));
				vvo.setVendorName((String) obj[1] + "(" + (String) obj[2]
						+ ")");
				vvo.setVendorSkill((String) obj[2]);
				vvo.setVendorSkillId(objectToLong(obj[3]));

				@SuppressWarnings("unchecked")
				List<Object[]> associatedUsers = sessionFactory
						.getCurrentSession()
						.createSQLQuery((VENDOR_CURRENT_ALLOCATED))
						.setParameter("userId", vvo.getVendorId()).list();

				List<TypeAheadVo> associatedUserList = null;
				if (associatedUsers != null)
				{
					associatedUserList = new ArrayList<TypeAheadVo>();
					for (Iterator iterator = associatedUsers
							.iterator(); iterator.hasNext();)
					{
						Object[] rowData = ((Object[]) iterator.next());
						associatedUserList
								.add(new TypeAheadVo(objectToLong(rowData[0]), (String) rowData[1]));
					}
				}

				vvo.setAssociatedUsers(associatedUserList);
				vendorList.add(vvo);
			}

			return vendorList;
		} catch (Exception e)
		{
			LOGGER.error("Error While getting Vendor for reports::"
					+ e.getMessage());
		}
		LOGGER.debug(" Exit from  getReportingVendors  " +   filter.getTicketId());
		return vendorList;
	}

	@Override
	public List<FRVendorVO> getVendorsWithDetails(FRVendorFilter filter)
	{
		// and usm.idSkill = :skillId and u.reportTo = :reportToId
		List<FRVendorVO> vendorList = null;
		
		if(filter == null)
			return vendorList;
		
		LOGGER.info(" inside  getVendorsWithDetails  " +   filter.getTicketId());
		try
		{
			vendorList = new ArrayList<FRVendorVO>();
			StringBuilder completeQuery = new StringBuilder(VENDOR_ALLOCATED);

			if (filter.getReportToUserId() > 0)
			{
				completeQuery.append(" and  u.reportTo = ")
						.append(filter.getReportToUserId());
			}

			if (filter.getAssocatedToUserId() > 0)
			{
				completeQuery.append(" and  va.currentAllocatedToUserId =")
						.append(filter.getAssocatedToUserId());
			}

			if (filter.getSkillId() > 0)
				completeQuery.append(" and usm.idSkill = ")
						.append(filter.getSkillId());

			if (filter.getTicketId() > 0)
				completeQuery.append(" and vt.ticketID = ")
						.append(filter.getTicketId());

			Query q = sessionFactory.getCurrentSession()
					.createSQLQuery(completeQuery.toString());

			LOGGER.info("COmplete query getvendors " + q.getQueryString());

			List<Object[]> result = q.list();
			if (result == null)
				return null;
			for (Object[] obj : result)
			{
				// vendorList
				// .add(new FRVendorVO(objectToLong(obj[0]), (String) obj[1],
				// (String) obj[2], objectToLong(obj[3])));

				vendorList
						.add(new FRVendorVO(objectToLong(obj[0]), getVendorName(obj[1], obj[2]), (String) obj[2], objectToLong(obj[3])));
			}
			return vendorList;
		} catch (Exception e)
		{
			LOGGER.error("Error While getting Vendors" + e.getMessage());

		}
		LOGGER.debug(" Exit from  getVendorsWithDetails  " +   filter.getTicketId());
		return vendorList;
	}

	private String getVendorName(Object vendorName, Object vendorSkill)
	{
		String name = "";
		if (vendorName == null || vendorSkill == null)
			return name;
		LOGGER.debug(" Inside  from  getVendorName  " +   vendorName);
		name = vendorName.toString() + " (" + vendorSkill.toString() + ")";
		return name;
	}

	@Override
	public List<TypeAheadVo> getVendors(FRVendorFilter filter)
	{
		// and usm.idSkill = :skillId and u.reportTo = :reportToId
		List<TypeAheadVo> vendorList = null;
		if(filter == null)
			return vendorList;
		
		LOGGER.debug(" inside  getVendors  " +   filter.getTicketId());
		try
		{
			vendorList = new ArrayList<TypeAheadVo>();
			StringBuilder completeQuery = new StringBuilder(VENDOR_ALLOCATED);

			if (filter.getReportToUserId() > 0)
			{
				completeQuery.append(" and  u.reportTo = ")
						.append(filter.getReportToUserId());
			}

			if (filter.getAssocatedToUserId() > 0)
			{
				completeQuery.append(" and  va.currentAllocatedToUserId =")
						.append(filter.getAssocatedToUserId());
			}

			if (filter.getSkillId() > 0)
				completeQuery.append(" and usm.idSkill = ")
						.append(filter.getSkillId());

			if (filter.getTicketId() > 0)
				completeQuery.append(" and vt.ticketID = ")
						.append(filter.getTicketId());

			Query q = sessionFactory.getCurrentSession()
					.createSQLQuery(completeQuery.toString());

			LOGGER.info("COmplete query getvendors " + q.getQueryString());

			List<Object[]> result = q.list();
			if (result == null)
				return null;
			for (Object[] obj : result)
			{
				vendorList
						.add(new TypeAheadVo(objectToLong(obj[0]), (String) obj[1]
								+ "(" + (String) obj[2] + ")"));

			}
			return vendorList;
		} catch (Exception e)
		{
			LOGGER.error("Error While getting Vendors" + e.getMessage());

		}
		LOGGER.debug(" Exit from  getVendors  " +   filter.getTicketId());
		return vendorList;
	}

	@Override
	public List<VendorAllocation> getAllVendorAssociateWithNes()
	{
		LOGGER.debug(" inside  getAllVendorAssociateWithNes  " );
		try
		{
			return (List<VendorAllocation>) hibernateTemplate
					.find("from VendorAllocation");
		} catch (Exception exception)
		{
			LOGGER.error("Error occure while fetching associated vendor with nes...", exception);
			return null;
		}
	}

	@Override
	public List<TypeAheadVo> getAllSkills()
	{
		LOGGER.debug(" inside  getAllSkills  " );

		try
		{
			if (skills.size() > 0)
				return skills;
			@SuppressWarnings("unchecked")
			List<Object[]> result = sessionFactory.getCurrentSession()
					.createSQLQuery(SKILL_LIST).list();
			if (result == null)
				return null;
			for (Object[] obj : result)
			{
				skills.add(new TypeAheadVo(objectToLong(obj[0]), (String) obj[1]));

			}
			return skills;
		} catch (Exception e)
		{
			LOGGER.error("Error While getting VendorSkills" + e.getMessage());

		}
		LOGGER.debug(" Exit from  getAllSkills  " );
		return null;
	}

	@Override
	public Map<Long, List<Long>> getAllAssociateVendorIdsWithTl()
	{
		LOGGER.debug(" inside  getAllAssociateVendorIdsWithTl  " );
		Map<Long, List<Long>> listOfVendors = new HashMap<Long, List<Long>>();
		try
		{
			List<Object[]> result = sessionFactory.getCurrentSession()
					.createSQLQuery(SQL_TL_ASSOCIATED_VENDOR_WITH_TL).list();

			if (result != null)
			{
				List<Long> vendorIds = null;
				long tlAskey = 0;
				for (Object[] row : result)
				{
					tlAskey = objectToLong(row[3]);

					if (tlAskey == 0)
						continue;
					if (listOfVendors.containsKey(tlAskey))
					{
						vendorIds = listOfVendors.get(tlAskey);
						vendorIds.add(objectToLong(row[2]));
						listOfVendors.put(tlAskey, vendorIds);
					} else
					{
						vendorIds = new ArrayList<Long>();
						vendorIds.add(objectToLong(row[2]));
						listOfVendors.put(tlAskey, vendorIds);
					}
				}
			}

		} catch (Exception exception)
		{
			LOGGER.error("Error While getting Vendors...", exception);
		}
		LOGGER.debug(" Exit from  getAllAssociateVendorIdsWithTl  " );
		return listOfVendors;
	}

	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}

	@Override
	public List<SubTickets> getUserWithType(Integer userType, Long ticketId)
	{
		LOGGER.debug(" inside  getUserWithType  " +ticketId);

		if (userType != null && ticketId != null)
		{
			return null;

		}

		StringBuilder queryStr = new StringBuilder(SUB_TICKET_USERTYPE);

		if (ticketId != null && ticketId > 0)
		{
			queryStr.append(" and ticketId = " + ticketId);

		}
		if (userType != null && userType > 0)
		{
			queryStr.append(" and userType = " + userType);

		}

		try
		{
			return (List<SubTickets>) hibernateTemplate
					.find(queryStr.toString());
		} catch (Exception exception)
		{
			LOGGER.error("Error occure while fetching SubTickets...", exception);
			return null;
		}

	}

	@Override
	public int assignSubTicketToVendor(SubTickets ticketData)
	{
		int noOfRecords = 0;
		if (ticketData == null)
			return noOfRecords;
		LOGGER.debug(" inside  assignSubTicketToVendor  " +ticketData.getId());

		try
		{
			noOfRecords = sessionFactory.getCurrentSession()
					.createSQLQuery(ASSIGN_SUB_TICKET_TO_VENDOR)
					.setParameter("id", ticketData.getId())
					.setParameter("userId", ticketData.getUserId())
					.setParameter("userType", ticketData.getUserType())
					.setParameter("ticketID", ticketData.getTicketId())
					.setParameter("requestedBy", ticketData.getRequestedBy())
					.setParameter("estimatedStartTime", ticketData
							.getEstimatedStartTime())
					.setParameter("estimatedEndTime", ticketData
							.getEstimatedStartTime())
					.setParameter("actualStartTime", ticketData
							.getActualStartTime())
					.setParameter("actualEndTime", ticketData
							.getActualEndTime())
					.setParameter("addedBy", AuthUtils.getCurrentUserId())
					.setParameter("status", ticketData.getStatus())
					.setParameter("inStatus", ticketData.getInStatus())
					.setParameter("skillId", ticketData.getSkillId())
					.executeUpdate();

			LOGGER.info("Vendor " + ticketData.getUserId()
					+ " Associated with Ticket " + ticketData.getTicketId()
					+ " Sub ticket inserted :" + noOfRecords);

		} catch (Exception e)
		{
			LOGGER.error("Error While Assiging Ticket to Vendor::"
					+ e.getMessage());
		}
		LOGGER.debug(" Exit from  assignSubTicketToVendor  " +ticketData.getId());
		return noOfRecords;
	}

}
