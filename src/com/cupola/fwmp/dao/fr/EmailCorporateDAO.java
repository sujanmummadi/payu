package com.cupola.fwmp.dao.fr;

import java.util.List;

import com.cupola.fwmp.filters.EmailFilter;
import com.cupola.fwmp.persistance.entities.EmailCorporate;
import com.cupola.fwmp.vo.fr.EmailCorporateVo;

public interface EmailCorporateDAO {
	
	EmailCorporate add(EmailCorporateVo emailCorporateVo);
	EmailCorporate update(EmailCorporateVo emailCorporateVo);
	List<EmailCorporateVo> getAll();
	List<EmailCorporateVo> getEmailsByEmailFilter(EmailFilter emailFilter);
	

}
