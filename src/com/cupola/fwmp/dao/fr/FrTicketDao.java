/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.fr;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.FrTicketEtr;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.counts.dashboard.FRSummaryCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketCountSummaryVO;
import com.cupola.fwmp.vo.counts.dashboard.FrTicketTlneDetailVo;
import com.cupola.fwmp.vo.fr.ETRExpireVo;
import com.cupola.fwmp.vo.fr.FrTicketPushBackVo;
import com.cupola.fwmp.vo.fr.FrTicketVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo;

public interface FrTicketDao
{
	public static String GET_FRTICKET_BYUSER = "SELECT * FROM FrDetail  where ticketId in (:ticketIds)";
	
	public static String UPDATE_FR_SYMTOM = "UPDATE FrDetail SET "
			+ "currentSymptomName = :symptomName ,currentFlowId = :flowId WHERE ticketId = :ticketId"; 
	
	public static String UPDATE_FR_STATUS_LIST = "UPDATE FrDetail SET statusList = CONCAT(statusList,:status)"
			+ " WHERE ticketId = :tickId";
	
	public static String UPDATE_FR_FLOWID = "UPDATE FrDetail fr INNER JOIN FrTicket tick ON tick.id = fr.ticketId "
			+ " SET fr.currentSymptomName = :symptomName ,fr.currentFlowId = :flowId ,"
			+ " tick.currentAssignedTo = :currentAssigned , tick.status =:status, tick.modifiedBy=:modifiedBY "
			+ " WHERE tick.id = :ticketId ";
	
	
	public static String  FR_TL_DASHBOARD_FROM_QUERY = " User usr LEFT JOIN FrTicket tick ";
	public static String  FR_MANGER_DASHBOARD_FROM_QUERY = " FrTicket tick   LEFT  JOIN User usr  ";
	
	public static String FR_TL_DASHBOARD_COUNT_QUERY = "SELECT usr.firstName AS userName,"
			+ "usr.id AS userId,tick.id AS ticketId,fr.statusList,fr.currentflowId,"
			+ "fr.currentSymptomName,fr.nonLock,tick.currentEtr,tick.commitedEtr,tick.status AS ticketStatus,"
			+ "pr.value AS priorityValue,pr.escalatedValue,pr.updatedCount,fr.id,tick.crmCreatedOn,tick.modifiedOn,"
			+ "tick.reopenCount "
			+ "FROM "
			
			+ " _FROM_PLACE_ "
			
			+ " ON usr.id = tick.currentAssignedTo "
			+ " LEFT JOIN UserBranchMapping ubm on  ubm.idUser =  usr.id "
			+ " LEFT JOIN Customer c ON tick.customerId = c.id "
			+ " LEFT JOIN FrDetail fr ON tick.id = fr.ticketId "
			+ "LEFT JOIN TicketPriority pr ON pr.id = tick.priorityId "
			+ "WHERE  tick.id >0  ";
	
	public static String FR_UNLOCKED_TICKET_COUNT = "SELECT count(fr.nonLock) FROM FrTicket tick "
			+ "INNER JOIN FrDetail fr ON fr.ticketId = tick.id "
			+ "WHERE tick.currentAssignedTo = :userId AND fr.nonLock = 0 AND tick.status not in (:statusList)";
	
	public static String UPDATE_ACTIVATION_FLAG = "UPDATE FrTicket tick "
			+ "INNER JOIN FrDetail fr ON tick.id = fr.ticketId "
			+ "SET fr.nonLock = :lockValue "
			+ "WHERE  tick.id = :ticketId "
			+ "AND tick.currentAssignedTo = :userId ";
	
	public static String FR_ENGINEER_SQL_QUERY = "SELECT distinct usr.id AS userId,usr.firstName,usr.lastName, "
			+ "tick.id AS ticketId ,ug.userGroupcode, "
			+ "fr.currentFlowId,fr.currentSymptomName,tick.status, "
			+ "vtick.requestedBy,vtick.status AS vendorTicketStatus,vtick.ticketId AS vendorTicketId,vtick.userType,vtick.outStatus "
			+ "FROM User usr INNER JOIN  UserGroupUserMapping ugm on ugm.idUser = usr.id "
			+ "INNER JOIN UserGroup ug on ug.id = ugm.idUserGroup  "
			+ "LEFT JOIN FrTicket tick ON  usr.id = tick.currentAssignedTo "
			+ "LEFT JOIN FrDetail fr ON fr.ticketId = tick.id "
			+ "LEFT JOIN SubTickets vtick ON vtick.ticketId = tick.id "
			+ "WHERE ug.userGroupcode <> 'VENDOR' AND usr.status = 1 " ;
	
	public static String FR_VENDOR_COUNT_QUERY = "SELECT t.id,t.status,vt.status as vtStatus from FrTicket t Inner join SubTickets vt on vt.ticketID=t.id  "
			+ " where vt.userId = :vendorId and (TIMESTAMPDIFF(HOUR,vt.addedOn,now()) < :afpVendorClosedTicketInBucketHours OR vt.status not in (:pendingStatusList))";
	
	public	static String UPDATE_TICKET_STATUS = "UPDATE FrTicket t set t.status = :status where t.id = :ticketId and t.status = 901";

	public static final String GET_NEXT_LOCKED_TICKET_ALL = "SELECT t.id FROM FrTicket t "
			+ "INNER JOIN FrDetail fr ON t.id = fr.ticketId "
			+ "INNER JOIN TicketPriority p ON t.priorityId = p.id "
			+ "WHERE t.currentAssignedTo = :userId AND fr.nonLock = 1 AND fr.currentFlowId <> 0 "
			+ "AND t.status not in (:ticketStatusList) " ;
	
	public static final String GET_NEXT_LOCKED_TICKET =  GET_NEXT_LOCKED_TICKET_ALL  
			+ " ORDER BY (p.value + TIMESTAMPDIFF(HOUR,t.TicketCreationDate,NOW()) * 0.5) DESC, t.TicketCreationDate ASC ";
	
	public static final String GET_NEXT_LOCKED_TICKET_WITH_IGNORE = GET_NEXT_LOCKED_TICKET_ALL 
			+ " AND t.id not in (:ticketList) "
			+ " ORDER BY (p.value + TIMESTAMPDIFF(HOUR,t.TicketCreationDate,NOW()) * 0.5) DESC, t.TicketCreationDate ASC ";
	
	public static final String ACTIVATE_N_LOCKED_FR_TICKET = "UPDATE FrDetail fr INNER JOIN FrTicket tick "
			+ "ON fr.ticketId = tick.id SET fr.nonLock = :lockFlag "
			+ "WHERE tick.currentAssignedTo = :userId and tick.id in (:ticketIds)" ;
	
	public static final String GET_TICKET_BY_WORKORDER_DETAIL_SQL = "SELECT * FROM FrDetail WHERE workOrderNumber = :workOrder";
	public static final String GET_ALL_FRTICKET_BY_WORKORDER_SQL = "SELECT * FROM FrTicket WHERE workOrderNumber = :workOrder";
	
	public static final String PUSH_BACK_COUNT = "SELECT pushBackCount FROM FrTicket WHERE id = :ticketId";
	public static final String PUSH_BACK_UPDATE_SQL = "UPDATE FrTicket tick INNER JOIN FrDetail fr "
			+ "ON tick.id = fr.ticketId SET fr.nonLock = :nonLock ,tick.pushBackCount = tick.pushBackCount + 1, "
			+ "tick.reason = :reasonValue, tick.remarks = :remarksValue,tick.modifiedBy = :modifiedBy "
			+ "WHERE tick.id = :ticketId ";
	
	public static final String GET_TOP_N_PRIORITY_TICKET = "SELECT if( (fr.nonLock= 1 AND t.status in(:available)), "
			+ "t.id,fr.nonLock) AS lockedIds "
			+ "FROM FrTicket t INNER JOIN FrDetail fr ON t.id = fr.ticketId "
			+ "INNER JOIN TicketPriority p ON t.priorityId = p.id "
			+ "WHERE t.currentAssignedTo = :userId AND t.status not in ( :completed ) "
			+ "ORDER BY p.value DESC, t.TicketCreationDate ASC limit :noOfIds " ;
	
	public static String GET_ALL_VICINITY_TICKET = "SELECT tick.id FROM FrTicket tick INNER JOIN Customer cust "
			+ "ON tick.customerId = cust.id INNER JOIN FrDetail fr ON fr.ticketId = tick.id "
			+ "WHERE tick.cxIp = :cxIp AND cust.cityId = :cityId AND fr.currentFlowId = :flowId "
			+ "AND tick.status NOT IN (:completed) ";

	public static String GET_CLOSED_FRTICKET_FOR_REOPEN_SQL = "SELECT * FROM FrTicket tick where tick.workOrderNumber=:workOrder "
			+ "AND tick.status in (:statusList)";
	
	
	String GET_ALL_ETR_EXPIRED_TICKETS = "select t.id ,fd.currentFlowId,c.mobileNumber,etr.currentEtr,c.mqId,etr.etrElapsedCount ,c.cityId,c.branchId from FrTicket t "
			+"inner join FrDetail fd on t.id = fd.ticketId "
			+"inner join FrTicketEtr etr on t.id = etr.ticketId "
			+"inner join Customer c on c.id = t.customerId "
			+"where t.status not in (:completed)  "
			+"and TIMESTAMPDIFF(MINUTE,NOW(),etr.communicationEtr) between 0 and :beforeMinutes";
//			+" and TIMESTAMPDIFF(MINUTE,etr.committedEtr,etr.currentEtr) >= 20  ";
	
	public static String GET_FR_TICKET_COMMON_DATA_SQL = "SELECT city.id,city.cityName "
			+"FROM   FrTicket tick INNER JOIN Customer c ON tick.customerId = c.id  "
			+"INNER JOIN City city on city.id = c.cityId "
			+"WHERE  tick.id = :ticketId ";


	public static String GET_FR_TICKET_BY_FXNAME_SQL = "SELECT tick.id,tick.status FROM FrTicket tick INNER JOIN FrDetail fr "
			+ " ON tick.id = fr.ticketId WHERE tick.fxName = :fxName AND fr.currentFlowId = :flowId "
			+ " AND status NOT IN (:completed) LIMIT 0,1";
				
	public static String GET_ALL_OPEN_TICKET_USER_SQL = "SELECT tick.id FROM FrTicket tick where "
			+ " tick.currentAssignedTo = :userId AND tick.status not in ( :statusIds )";
	
	public static final String PUSH_BACK_UPDATE_TLQ_SQL = "UPDATE FrTicket tick INNER JOIN FrDetail fr "
			+ "ON tick.id = fr.ticketId SET fr.nonLock = 1 ,tick.pushBackCount = tick.pushBackCount + 1, "
			+ "tick.reason = :reasonValue, tick.remarks = :remarksValue,tick.modifiedBy = :modifiedBy "
			+ "tick.currentAssignedTo = :assignedTo ,tick.assignedBy = 1, tick.status = :statusId "
			+ "WHERE tick.id = :ticketId" ;
	
	String FR_MANUAL_CLOSURE_QUERY = "Update FrTicket set status = " + FWMPConstant.TicketStatus.CLOSED
			+ " where workOrderNumber in (:workOrders)";
	
	String UPDATE_FR_WO_BY_CRM = "update FrTicket t INNER JOIN FrDetail fr ON fr.ticketId = t.id "
			+ "LEFT JOIN TicketPriority pr ON pr.id = t.priorityId set t.defectCode=?, t.subDefectCode=?,t.currentAssignedTo =?"
			+ ",t.reopenCount=?,fr.voc=?,pr.escalationType=?, pr.value = ?,t.status=? where t.workOrderNumber=?";
	

	public Long createFrTicket(FrTicketVo ticketVo);
	public Integer deleteFrTicket(Long ticketId);
	public List<FrDetail> getAllFrTicket();
	
	public List<FrDetail> getFrTicketByUser(Long userId);
	public void saveOrUpdateFrTicket(FrTicketVo dbvo);
	public boolean isFrTicketAvailabeWithTicketIdAndSymptom(Long ticketId,
			String symptom);
	
	public List<FrDetail> findFrTicketByFlowId(Long flowId);
	public List<FrDetail> findFrTicketByEngineer(Long userId);
	
	public Integer updateTicketLock(boolean isLocked,Long ticketId);
	
	public Integer updateFrTicketSymptom(Long flowId , String symptomName,Long ticketId);
	
	public Integer updateFrTicketStatus(Long ticketId,List< Long> status);
	public Integer deleteCompletedStatus(Long ticketId, List<Long> status);
	
	public List<FRTicketCountSummaryVO> findFrTicketCounts(Long userId);
	
	public Integer updateFlowId(Long ticketId , String symptomName ,Long flowId,Long userId, Integer flowChangedDownToUp);
	public BigInteger getUnlockedTicketCount(Long userId);
	public Integer activateTicket(boolean ativateValue, Long userId, Long ticketId);
	
	public List<FrTicketTlneDetailVo> findFrTicketTlneCounts(Long reportTo);
	public List<FRSummaryCountVO> getVendorTicketSummary();
	
	int updateTicketStatusToInProgress(Long ticketId,Integer status);
	public List<FrDetail> getAllUnassignedFrTicket();
	public Integer updateBlockStatus(long ticketId , boolean isBlocked);
	public Integer getUnlockedTicketWithUnblockedCount(Long userId);
	
	
	public List<Long> getNextNLockedFrTicket(Long userId ,Integer numberOfTicket, List<Long> ignoreTickets ) ;
	public Integer activateNextLockedFrTicket( Long userId ,int noOfTicketToBeActivate,List<Long> ignoreTickets );
	public Integer activateUserFrTickets(Long userId, List<Long> ticketIds);
	
	public FrDetail findFrTicketDetailByWorkOrder( String workOrder );
	public void reopenWorkOrder(FrDetail frTicket);
	
	public List<BigInteger> getCurrentAssignedForTickets(List<Long> ticketIds);
	public void assignFrTickets(AssignVO assignVO, Long areaId);
	public void updateReopneTicket(FrTicket ticket);
	public FrTicket getFrTicketByWorkOrder(String prospectNo);
	public String updateFrTicket(StatusVO status);
	
	public Map<String, Long> getFrTicketByWorkOrder(List<String> workOrders);
	public Integer getCurrentPushBackCount(Long ticketId);
	public Integer updateFrTicketPushBackCount(FrTicketPushBackVo pushBackVo);
	public List<Long> getNTopPriorityLockedFrTicket(Long userId, int noOfIds);
	
	public List<Long> getAllVicinityFrTicketByCxIp(String cxIp,Long flowId );
	
	List<ETRExpireVo> getAllETRExpiredTickets();
	public FrTicket getCloseFrTicketByWorkOrder(String workOrder);
	public boolean isTicketfound();
	public TypeAheadVo getCityNameForTicket(Long ticketId);
	public void updateFrTicketEtr(FrTicketEtr ticketEtr);
	
	public boolean getFrTicketByElementNameAndFlowId(String elementName ,Long flowId );
	
//	public Integer blockedTicketActivateNext(Long userId, Long ticketId);
	
	/** ******************* API FOR MANAGING THE FR TICKET BY NE OR TL****************************/
	
	/*public Integer updateOpenGart(long status ,long ticketId, Date openGart);
	public Integer updateCloseGart(long status,long ticketId,Date closeGart);
	public Integer updateCurrentAssignedTo(long ticketId, long userId);*/
	
	public Map<Long, Boolean> createFrDetailInBulk(List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap ) ;
			
	/**@author aditya
	 * List<TicketReassignmentLogVo>
	 * @param ticketIds
	 * @return
	 */
	List<TicketReassignmentLogVo> getCurrentAssignedForTicketsWithIds(List<Long> ticketIds);
	public List<Long> getAllOpenFrTicketByUser(Long userId);
	/**@author aditya
	 * int
	 * @param workOrders
	 * @return
	 */
	int closeWOinBulk4Fr(Set<String> workOrders);
	/**@author aditya
	 * int
	 * @param prospect
	 * @return
	 */
	public int updateFrWOByCRM(CreateUpdateFrVo faultRepair);
	/**@author aditya
	 * boolean
	 * @param workOrderNumber
	 * @return
	 */
	boolean isFrWOPresentInDB(String workOrderNumber);
	
	Long getFrTicketCurrentSatus(String workOrderNumber);
	
	Integer getFrTicketCurrentPriority(String workOrderNumber);
}
