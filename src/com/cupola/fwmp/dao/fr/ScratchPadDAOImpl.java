/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.fr;

import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.persistance.entities.ControlBlock;
import com.cupola.fwmp.persistance.entities.ScratchPad;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.fr.ScratchPadCache;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.fr.Activity;
import com.cupola.fwmp.vo.fr.AndroidFlowCxDown;
import com.cupola.fwmp.vo.fr.Decision;
import com.cupola.fwmp.vo.fr.DefectCode;
import com.cupola.fwmp.vo.fr.ETRDetails;
import com.cupola.fwmp.vo.fr.ScratchPadLog;
import com.cupola.fwmp.vo.fr.ScratchPadVo;
import com.cupola.fwmp.vo.fr.ScratchVo;
import com.cupola.fwmp.vo.fr.SubDefectCode;
import com.cupola.fwmp.vo.fr.TypeMethod;

@Transactional
public class ScratchPadDAOImpl implements ScratchPadDAO
{
	private static Logger LOGGER = Logger.getLogger(ScratchPadDAOImpl.class
			.getName());
	@Autowired
	MongoOperations mongoOperations;

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	UserDao userDao;

	@Autowired
	FrTicketDao frTicketDao;

//	public static Map<Long, ScratchVo> cachedSPByTicketId =new ConcurrentHashMap<Long, ScratchVo>();

	private final String UPDATE_SCRATCH_PAD = "UPDATE ScratchPad SET selectedOption=? where ticketId=? and type=? and idDetails=?";

	private final String DELETE_CONTEXT_DATA = "DELETE FROM ScratchPad WHERE ticketId = :ticketId and status = :status";

	private void init()
	{
		LOGGER.info("ScratchPadDAOImpl init method called..");
		try
		{
			ScratchPadCache.reFreshCache();
			//resetCachedSp();
		} catch (IOException e)
		{
			LOGGER.error("Error while adding cache FRdef :" + e.getMessage());
		}
		LOGGER.info("ScratchPadDAOImpl init method executed.");
	}



	/*private void addToLocalCache(ScratchVo scratchVo)
	{
		LOGGER.debug("Scratch =========" + scratchVo.getTicketId()
				+ " is adding in CACHE");
		LOGGER.debug("id>>>>>>is" + scratchVo.getTicketId());
		cachedSPByTicketId.put(scratchVo.getTicketId(), scratchVo);
	}*/

	@Override
	public Map<Integer, Decision> getAllDecisions()
	{
		if (ScratchPadCache.decisionData.size() != 0)
			return ScratchPadCache.decisionData;
		return null;
	}

	@Override
	public Map<Integer, Activity> getAllActivities()
	{
		if (ScratchPadCache.activityData.size() != 0)
			return ScratchPadCache.activityData;
		return null;
	}

	@Override
	public ControlBlock spAPI(ScratchVo scratchVo)
	{
		if (scratchVo != null)
		{
			if (scratchVo.getCbStatus() == FWMPConstant.ControlBlockStatus.ADD)
			{
				return addSP(scratchVo);
			} else if (scratchVo.getCbStatus() == FWMPConstant.ControlBlockStatus.UPDATE)
			{
				return updateSP(scratchVo);
			} else if (scratchVo.getCbStatus() == FWMPConstant.ControlBlockStatus.CLEAR)
			{
				return clearSP(scratchVo);
			} else
				LOGGER.info("Unsoported Command........");
		}
        LOGGER.debug(" Exit From spAPI.." + scratchVo != null ? scratchVo.getTicketId() : null);
		return null;

	}

	private ControlBlock clearSP(ScratchVo scratchVo)
	{
		LOGGER.info("Clear ScratchPad......." + scratchVo.getTicketId());
		ControlBlock newScratchPad = new ControlBlock();
		BeanUtils.copyProperties(scratchVo, newScratchPad);
		Object controlBlock = hibernateTemplate
				.load(ControlBlock.class, newScratchPad.getTicketId());
		hibernateTemplate.delete(controlBlock);
		return newScratchPad;
	}

	/*
	 * private ControlBlock deleteSP(ScratchVo scratchVo) { return null; }
	 */

	private ControlBlock updateSP(ScratchVo scratchVo)
	{
		LOGGER.info("Update ScratchPad......." + scratchVo.getTicketId());
		ControlBlock newScratchPad = new ControlBlock();
		BeanUtils.copyProperties(scratchVo, newScratchPad);
		if (scratchVo.getScratchPads() != null)
		{
			Set<ScratchPadVo> scratchPadVos = scratchVo.getScratchPads();
			for (ScratchPadVo scratchPadVo : scratchPadVos)
			{
				LOGGER.debug("selectedOption" + scratchPadVo.getSelectedOption());
				if (scratchPadVo.getSelectedOption() > 0
						&& scratchVo.getTicketId() > 0
						&& scratchPadVo.getType() > 0
						&& scratchPadVo.getIdDetails() >= 0)
				{

					Object[] params = { scratchPadVo.getSelectedOption(),
							scratchVo.getTicketId(), scratchPadVo.getType(),
							scratchPadVo.getIdDetails() };

					int[] types = { Types.INTEGER, Types.BIGINT, Types.INTEGER,
							Types.INTEGER };

					int rows = jdbcTemplate
							.update(UPDATE_SCRATCH_PAD, params, types);
					LOGGER.debug("ScratchPad is updated :" + rows);
					return newScratchPad;
				}
				LOGGER.debug("invalid parameters :"
						+ scratchPadVo.getSelectedOption()
						+ scratchVo.getTicketId() + scratchPadVo.getType()
						+ scratchPadVo.getIdDetails());
			}
		}
		LOGGER.debug(" Exit From Clear ScratchPad......." + scratchVo.getTicketId());
		return null;
	}

	private ControlBlock addSP(ScratchVo scratchVo)
	{
		LOGGER.debug("add ScratchPad......." + scratchVo.getTicketId());
		ControlBlock newScratchPad = new ControlBlock();
		BeanUtils.copyProperties(scratchVo, newScratchPad);
		LOGGER.info("" + newScratchPad.getTicketId());

		//if (cachedSPByTicketId.containsKey(newScratchPad.getTicketId()))
		if(controllBlockExists(newScratchPad.getTicketId()))
		{
			LOGGER.info("inside if block");
			LOGGER.info("ticket id is already there in table");
			/*ScratchVo scratch = cachedSPByTicketId.get(newScratchPad
					.getTicketId());
			LOGGER.info("id " + scratch.getTicketId());
			BeanUtils.copyProperties(scratch, newScratchPad);*/
			//newScratchPad.setTicketId(ticketId);
			newScratchPad.setStatus(1);
			newScratchPad.setAddedOn(new Date());
			newScratchPad.setUpdatedOn(new Date());
			if (scratchVo.getScratchPads() != null)
			{
				Set<com.cupola.fwmp.persistance.entities.ScratchPad> scratchPads = new HashSet<com.cupola.fwmp.persistance.entities.ScratchPad>();
				Set<ScratchPadVo> scratchPadVos = scratchVo.getScratchPads();
				for (ScratchPadVo scratchPadVo : scratchPadVos)
				{
					com.cupola.fwmp.persistance.entities.ScratchPad scratchPad = new com.cupola.fwmp.persistance.entities.ScratchPad();
					scratchPad.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					scratchPad.setControlBlock(newScratchPad);
					scratchPad.setType(scratchPadVo.getType());
					scratchPad.setIdDetails(scratchPadVo.getIdDetails());
					scratchPad.setAddedOn(scratchPadVo.getAddedOn());
					scratchPad.setUpdatedOn(scratchPadVo.getUpdatedOn());
					scratchPad.setStatus(scratchPadVo.getStatus());
					scratchPad.setSelectedOption(scratchPadVo
							.getSelectedOption());
					if(scratchPadVo.getRemarks() != null && !scratchPadVo.getRemarks().isEmpty())
						scratchPad.setRemarks(scratchPadVo.getRemarks());
					scratchPad.setAddedBy(scratchVo.getAddedBy());
					scratchPads.add(scratchPad);
				}
				newScratchPad.setScratchPads(scratchPads);
				hibernateTemplate.saveOrUpdate(newScratchPad);
			}
			// insert data for context status

			if (scratchVo.getContexts() != null)
			{

				//delectContextDataInScratchPad(scratchVo.getTicketId(), FWMPConstant.ControlBlockStatus.CONTEXT_TYPE);

				Set<com.cupola.fwmp.persistance.entities.ScratchPad> scratchPads = new HashSet<com.cupola.fwmp.persistance.entities.ScratchPad>();
				Set<ScratchPadVo> scratchPadVos = scratchVo.getContexts();
				for (ScratchPadVo scratchPadVo : scratchPadVos)
				{
					com.cupola.fwmp.persistance.entities.ScratchPad scratchPad = new com.cupola.fwmp.persistance.entities.ScratchPad();
					scratchPad.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					scratchPad.setControlBlock(newScratchPad);
					scratchPad.setType(scratchPadVo.getType());
					scratchPad.setIdDetails(scratchPadVo.getIdDetails());
					scratchPad.setAddedOn(scratchPadVo.getAddedOn());
					scratchPad.setUpdatedOn(scratchPadVo.getUpdatedOn());
					scratchPad.setStatus(scratchPadVo.getStatus());
					scratchPad.setSelectedOption(scratchPadVo
							.getSelectedOption());
					if(scratchPadVo.getRemarks() != null && !scratchPadVo.getRemarks().isEmpty())
						scratchPad.setRemarks(scratchPadVo.getRemarks());
					scratchPad.setAddedBy(scratchVo.getAddedBy());
					scratchPad.setJumpId(scratchPadVo.getJumpId());
					scratchPads.add(scratchPad);
				}
				newScratchPad.setScratchPads(scratchPads);
				hibernateTemplate.saveOrUpdate(newScratchPad);
			}
            LOGGER.debug("Record inserted successfully......." + newScratchPad);
			return newScratchPad;
		} else
		{

			 frTicketDao.updateTicketStatusToInProgress(scratchVo
					.getTicketId(), TicketStatus.IN_PROGRESS);
		
				if (scratchVo.getScratchPads() != null)
				{
					newScratchPad.setStatus(1);
					newScratchPad.setAddedOn(new Date());
					newScratchPad.setUpdatedOn(new Date());
					Set<com.cupola.fwmp.persistance.entities.ScratchPad> scratchPads = new HashSet<com.cupola.fwmp.persistance.entities.ScratchPad>();
					Set<ScratchPadVo> scratchPadVos = scratchVo
							.getScratchPads();
					for (ScratchPadVo scratchPadVo : scratchPadVos)
					{
						com.cupola.fwmp.persistance.entities.ScratchPad scratchPad = new com.cupola.fwmp.persistance.entities.ScratchPad();
						scratchPad.setId(StrictMicroSecondTimeBasedGuid
								.newGuid());
						scratchPad.setControlBlock(newScratchPad);
						scratchPad.setType(scratchPadVo.getType());
						scratchPad.setIdDetails(scratchPadVo.getIdDetails());
						scratchPad.setAddedOn(scratchPadVo.getAddedOn());
						scratchPad.setUpdatedOn(scratchPadVo.getUpdatedOn());
						scratchPad.setStatus(scratchPadVo.getStatus());
						scratchPad.setSelectedOption(scratchPadVo
								.getSelectedOption());
						if(scratchPadVo.getRemarks() != null && !scratchPadVo.getRemarks().isEmpty())
							scratchPad.setRemarks(scratchPadVo.getRemarks());
						scratchPad.setAddedBy(scratchVo.getAddedBy());
						scratchPads.add(scratchPad);
					}
					newScratchPad.setScratchPads(scratchPads);
				//	cachedSPByTicketId.put(scratchVo.getTicketId(), scratchVo);
					hibernateTemplate.save(newScratchPad);

				}
				// insert data for context status

				if (scratchVo.getContexts() != null)
				{
					newScratchPad.setStatus(1);
					newScratchPad.setAddedOn(new Date());
					newScratchPad.setUpdatedOn(new Date());
					Set<com.cupola.fwmp.persistance.entities.ScratchPad> scratchPads = new HashSet<com.cupola.fwmp.persistance.entities.ScratchPad>();
					Set<ScratchPadVo> scratchPadVos = scratchVo.getContexts();
					for (ScratchPadVo scratchPadVo : scratchPadVos)
					{
						com.cupola.fwmp.persistance.entities.ScratchPad scratchPad = new com.cupola.fwmp.persistance.entities.ScratchPad();
						scratchPad.setId(StrictMicroSecondTimeBasedGuid
								.newGuid());
						scratchPad.setControlBlock(newScratchPad);
						scratchPad.setType(scratchPadVo.getType());
						scratchPad.setIdDetails(scratchPadVo.getIdDetails());
						scratchPad.setAddedOn(scratchPadVo.getAddedOn());
						scratchPad.setUpdatedOn(scratchPadVo.getUpdatedOn());
						scratchPad.setStatus(scratchPadVo.getStatus());
						scratchPad.setSelectedOption(scratchPadVo
								.getSelectedOption());
						scratchPad.setAddedBy(scratchVo.getAddedBy());
						if(scratchPadVo.getRemarks() != null && !scratchPadVo.getRemarks().isEmpty())
							scratchPad.setRemarks(scratchPadVo.getRemarks());
						scratchPad.setJumpId(scratchPadVo.getJumpId());
						scratchPads.add(scratchPad);
					}
					newScratchPad.setScratchPads(scratchPads);
					hibernateTemplate.saveOrUpdate(newScratchPad);
				}

		

			LOGGER.info("Record inserted successfully......." );
			return newScratchPad;
		}
	}

	public void delectContextDataInScratchPad(long ticketId, int contextType)
	{
		Query q = hibernateTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery(DELETE_CONTEXT_DATA)
				.setParameter("ticketId", ticketId)
				.setParameter("status", contextType);
		int result = q.executeUpdate();
		LOGGER.info("delected context data successfully :" + result);
	}

	private ControlBlock getIdByTicketId(long ticketId)
	{

		LOGGER.info("getIdByTicketId");

		try
		{

			List<ControlBlock> controlBlocks = (List<ControlBlock>) hibernateTemplate
					.find("from ControlBlock where ticketId=?", ticketId);

			if (controlBlocks != null)
			{

				LOGGER.info("size::::" + controlBlocks.size());

				return controlBlocks.get(0);

			} else

				return null;
		} catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("Failed to get control Block Reason:" + e.getMessage());
			return null;
		}

	}

	@Override
	public List<ScratchPadLog> getScratchPadByTicketId(long ticketId)
	{
		LOGGER.debug("getScratchPadByTicketId" +ticketId);
		if (ticketId > 0)
		{
			try
			{
				Object[] params = { ticketId,
						FWMPConstant.ControlBlockStatus.SCRATCH_TYPE };

				List<com.cupola.fwmp.persistance.entities.ScratchPad> scratchPads = (List<com.cupola.fwmp.persistance.entities.ScratchPad>) hibernateTemplate
						.find("from ScratchPad where ticketId=? and status=?", params);

				if (scratchPads != null)
				{
					return xformToLoggerView(scratchPads);
				} else

					return null;
			} catch (Exception e)
			{
				e.printStackTrace();
				LOGGER.info("Failed to get ScratchPad Reason:" + e.getMessage());
				return null;
			}
		} else
		{
			List<com.cupola.fwmp.persistance.entities.ScratchPad> scratchPads = (List<com.cupola.fwmp.persistance.entities.ScratchPad>) hibernateTemplate
					.find(" from ScratchPad where status =?", FWMPConstant.ControlBlockStatus.SCRATCH_TYPE);
			return xformToLoggerView(scratchPads);
		}
	}

	private List<ScratchPadLog> xformToLoggerView(List<ScratchPad> scratchPads)
	{
		List<ScratchPadLog> result = new ArrayList<ScratchPadLog>();
		LOGGER.info("size::::" + scratchPads.size());
		for (com.cupola.fwmp.persistance.entities.ScratchPad scratchPad : scratchPads)
		{
			ScratchPadLog scratchPadLog = new ScratchPadLog();
			scratchPadLog.setId(scratchPad.getId());
			scratchPadLog.setTicketId(scratchPad.getControlBlock()
					.getTicketId());
			scratchPadLog.setTypeId(scratchPad.getType());
			scratchPadLog.setTypeName(TypeMethod.getTypeMethod(scratchPad
					.getType()).toString());

			if (scratchPad.getType() == TypeMethod.DECISION.getValue())
			{
				scratchPadLog.setDecisionId(scratchPad.getIdDetails());
				Decision decision = ScratchPadCache.decisionData.get(scratchPad
						.getIdDetails());
				if(decision != null)
				{
					scratchPadLog.setDecisionName(decision.getDecisionName() + "?");
					scratchPadLog.setNoOfOptions(decision.getNoOfOptions());
					if (scratchPad.getSelectedOption() != null)
					{
						scratchPadLog.setSelectedOption(scratchPad
								.getSelectedOption());

						switch (scratchPad.getSelectedOption())
						{
						case 1:
							scratchPadLog.setSelectedOptionName(decision
									.getOption1());

							break;
						case 2:
							scratchPadLog.setSelectedOptionName(decision
									.getOption2());
							break;
						case 3:
							scratchPadLog.setSelectedOptionName(decision
									.getOption3());

							break;
						case 4:
							scratchPadLog.setSelectedOptionName(decision
									.getOption4());

							break;

						default:
							break;
						}
				}
				
				}
			}
			if (scratchPad.getType() == TypeMethod.ACTIVITY.getValue())
			{
				Activity activity = ScratchPadCache.activityData.get(scratchPad
						.getIdDetails());

				scratchPadLog.setActivityId(scratchPad.getIdDetails());
				scratchPadLog.setActivityName(activity.getActivityName());
			}
			
			if (scratchPad.getType() == TypeMethod.DEFECTCODE.getValue())
			{
				scratchPadLog.setReasionName(scratchPad.getRemarks());
			}
			if (scratchPad.getType() == TypeMethod.SUBDEFECTCODE.getValue())
			{
				scratchPadLog.setReasionName(scratchPad.getRemarks());
			}
			scratchPadLog.setStatus(scratchPad.getStatus());
			scratchPadLog.setTimeStamp(scratchPad.getUpdatedOn());
			scratchPadLog.setAddedOn(scratchPad.getAddedOn());
			scratchPadLog.setModifiedOn(scratchPad.getUpdatedOn());
			scratchPadLog.setAddedBy(scratchPad.getAddedBy());
			scratchPadLog.setMessage(createDisplayMessage(scratchPadLog));
			result.add(scratchPadLog);
		}

		Collections.sort(result, new Comparator<ScratchPadLog>()
		{

			@Override
			public int compare(ScratchPadLog o1, ScratchPadLog o2)
			{
				return o1.getTimeStamp().compareTo(o2.getTimeStamp());
			}

		});
		return result;

	}

	private String createDisplayMessage(ScratchPadLog scratchPadLog)
	{
		StringBuilder message = new StringBuilder("");
		message.append("[");
		message.append(GenericUtil.convertToUiDateFormat(scratchPadLog
				.getTimeStamp()));
		message.append("] ");

		message.append("[");
		UserVo vo = userDao.getUserById(scratchPadLog.getAddedBy());
		String userName = vo.getFirstName();

		if (userName == null)
			userName = "User Not Available";
		message.append(userName);
		message.append("] ");

		message.append(" :: " + scratchPadLog.getTypeName());
		if (scratchPadLog.getTypeId() == TypeMethod.DECISION.getValue())
		{
			message.append(" :: ");
			message.append(scratchPadLog.getDecisionName());
			message.append(" :: ");
			message.append(scratchPadLog.getSelectedOptionName());

		}

		if (scratchPadLog.getTypeId() == TypeMethod.ACTIVITY.getValue())
		{
			message.append(" :: ");
			message.append(scratchPadLog.getActivityName());
		}

		if (scratchPadLog.getTypeId() == TypeMethod.COMMAND.getValue())
		{
			message.append(" :: ");
			message.append(scratchPadLog.getCmdName());

		}
		if (scratchPadLog.getTypeId() == TypeMethod.DEFECTCODE.getValue())
		{
			message.append(" :: ");
			message.append(scratchPadLog.getReasionName());
		}
		if (scratchPadLog.getTypeId() == TypeMethod.SUBDEFECTCODE.getValue())
		{
			message.append(" :: ");
			message.append(scratchPadLog.getReasionName());
		}
		return message.toString();
	}

	@Override
	public APIResponse getVersionDetails()
	{
		LOGGER.info("getVersionDetails");
		return definitionCoreService.readVersionDetails();
	}

	@Override
	public Map<Integer, DefectCode> getAllDefectCodes()
	{
		if (ScratchPadCache.defectCodeData.size() != 0)
			return ScratchPadCache.defectCodeData;
		return null;
	}

	@Override
	public Map<Integer, SubDefectCode> getAllSubDefectCodes()
	{
	    LOGGER.debug(" getAllSubDefectCodes ");
		if (ScratchPadCache.subDefectCodeData.size() != 0)
			return ScratchPadCache.subDefectCodeData;
		LOGGER.debug("Exit From getAllSubDefectCodes ");
		return null;
	}

	@Override
	public Map<Integer, ETRDetails> getAllETRDetails()
	{
	    LOGGER.debug(" getAllETRDetails ");
		if (ScratchPadCache.etrData.size() != 0)
			return ScratchPadCache.etrData;
		LOGGER.debug(" Exit From getAllETRDetails ");
		return null;
	}

	@Override
	public ETRDetails getETRDetailsByStepID(int stepID)
	{
	 LOGGER.debug(" getETRDetailsByStepID " +stepID );
		if (stepID > 0)
		{
			try
			{
				return ScratchPadCache.getETRDetailsById(stepID);
			} catch (IOException e)
			{
				LOGGER.error("Error while  getETRDetailsByStepID:"+e.getMessage());
			}
		}
		LOGGER.debug(" Exit From getETRDetailsByStepID " +stepID );
		return null;
	}

	@Override
	public Date calculateCurrentEtr(long ticketId, int stepID)
	{
	  LOGGER.debug(" calculateCurrentEtr " +ticketId + " " +stepID );
		if (stepID > 0 && ticketId > 0)
		{
			try
			{
				ETRDetails etrDetails = ScratchPadCache
						.getETRDetailsById(stepID);
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				c.add(Calendar.MINUTE, etrDetails.getEtr());

				Date date = c.getTime();

				LOGGER.debug("Returing current Etr" + etrDetails.getEtr()
						+ " to date " + date);

				return date;
			} catch (IOException e)
			{
				LOGGER.error("Error while calculateCurrentEtr :"+e.getMessage());
				e.printStackTrace();
			}
		}
       LOGGER.debug("Exit From calculateCurrentEtr " +ticketId + " " +stepID );
		return null;
	}

	@Override
	public int calculateProgressWeightage(long ticketId, int stepID)
	{
	LOGGER.debug(" calculateProgressWeightage " +ticketId + " " +stepID );
		if (stepID > 0 && ticketId > 0)
		{

			try
			{
				ETRDetails etrDetails = ScratchPadCache
						.getETRDetailsById(stepID);
				return etrDetails.getProgressPercentage();
			} catch (IOException e)
			{
				LOGGER.error("Error while calculateProgressWeightage :"+e.getMessage());
				e.printStackTrace();
			}

		}
      LOGGER.debug("Exit From calculateProgressWeightage " +ticketId + " " +stepID );
		return 0;
	}

	@Override
	public APIResponse resetScratchPadCache()
	{
	LOGGER.debug(" resetScratchPadCache "   );
		try
		{
			ScratchPadCache.reFreshCache();
			return ResponseUtil.createSuccessResponse();
		} catch (IOException e)
		{
			LOGGER.error("Error while reset ScratchPad :"+e.getMessage());
		}
		LOGGER.debug("Exit From resetScratchPadCache "   );
		return ResponseUtil.createFailureResponse();
	}

	@Override
	public Map<Integer, AndroidFlowCxDown> getAllCxDownAndroidFlow()
	{
	    LOGGER.debug(" getAllCxDownAndroidFlow "   );
		if (ScratchPadCache.cxDownFlowData.size() != 0)
			return ScratchPadCache.cxDownFlowData;
		LOGGER.debug(" Exit From getAllCxDownAndroidFlow "   );
		return null;
	}

	@Override
	public Map<Integer, AndroidFlowCxDown> getAllSlowSpeedAndroidFlow()
	{
	   LOGGER.debug(" getAllSlowSpeedAndroidFlow "   );
		if (ScratchPadCache.slowSpeedFlowData.size() != 0)
			return ScratchPadCache.slowSpeedFlowData;
		LOGGER.debug("Exit From getAllSlowSpeedAndroidFlow "   );
		return null;
	}

	@Override
	public Map<Integer, AndroidFlowCxDown> getAllFDAndroidFlow()
	{
		LOGGER.debug(" inside getAllFDAndroidFlow "   );
		if (ScratchPadCache.fdFlowData.size() != 0)
			return ScratchPadCache.fdFlowData;
		LOGGER.debug("Exit From getAllFDAndroidFlow "   );
		return null;
	}

	@Override
	public Map<Integer, AndroidFlowCxDown> getAllCxUpAndroidFlow()
	{
	    LOGGER.debug(" getAllFDAndroidFlow "   );
		if (ScratchPadCache.cxUpFlowData.size() != 0)
			return ScratchPadCache.cxUpFlowData;
		LOGGER.debug("Exit From getAllFDAndroidFlow "   );
		return null;
	}

	@Override
	public List<ScratchPadVo> getContextRuleByTicketId(long ticketId)
	{
		LOGGER.debug("getContextRuleByTicketId with ticketId "+ticketId);
		if (ticketId > 0)
		{
			try
			{
				Object[] params = { ticketId,
						FWMPConstant.ControlBlockStatus.CONTEXT_TYPE };

				List<com.cupola.fwmp.persistance.entities.ScratchPad> scratchPads = (List<com.cupola.fwmp.persistance.entities.ScratchPad>) hibernateTemplate
						.find("from ScratchPad where ticketId=? and status=?", params);

				if (scratchPads != null)
				{
					return xformToContextView(scratchPads);
				}
				LOGGER.debug(" Exit from getContextRuleByTicketId with ticketId "+ticketId);
				return null;
			} catch (Exception e)
			{
				LOGGER.error("Failed to get ScratchPad Reason:" + e.getMessage());
				return null;
			}
		}
		LOGGER.debug(" Exit from getContextRuleByTicketId with ticketId "+ticketId);
		return null;
	}

	private List<ScratchPadVo> xformToContextView(List<ScratchPad> scratchPads)
	{
		List<ScratchPadVo> result = new ArrayList<ScratchPadVo>();
		if(scratchPads == null)
			return result;
		
		LOGGER.debug(" xformToContextView   "+scratchPads.size());
		for (ScratchPad contextRule : scratchPads)
		{
			ScratchPadVo contextRuleVo = new ScratchPadVo();
			BeanUtils.copyProperties(contextRule, contextRuleVo);
			contextRuleVo.setTicketId(contextRule.getControlBlock().getTicketId());
			result.add(contextRuleVo);
		}
		Collections.sort(result, new Comparator<ScratchPadVo>()
				{

					@Override
					public int compare(ScratchPadVo o1, ScratchPadVo o2)
					{
						return o1.getUpdatedOn().compareTo(o2.getUpdatedOn());
					}

				});
		LOGGER.debug("context db size with  :"+result.size());
		return result;
	}
	
	@Override
	public Map<Integer, AndroidFlowCxDown> getAllSeniorElementAndroidFlow()
	{
		LOGGER.debug(" inside getAllSeniorElementAndroidFlow   " );
		if (ScratchPadCache.srElementFlowData.size() != 0)
			return ScratchPadCache.srElementFlowData;
		LOGGER.debug(" Exit from getAllSeniorElementAndroidFlow   " );
		return null;
	}
	
	private boolean controllBlockExists(Long ticketId)
	{
		boolean result = false;
		Long id = null;
		try
		{
			id = jdbcTemplate
					.queryForLong("select ticketId from ControlBlock where ticketId = "
							+ ticketId);
		}catch (Exception e) {
			LOGGER.error("error while checking ticket present in controlblock :: "+e.getMessage());
			result = false;
		}
		LOGGER.info("Control Block ticket id is :: "+id);
	
		if(id != null && id > 0)
			result = true;
		
		return result; 

	}


}
