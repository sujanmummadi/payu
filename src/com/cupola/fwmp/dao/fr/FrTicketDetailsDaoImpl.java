package com.cupola.fwmp.dao.fr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.FlowAttribute;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.dao.fr.vendor.SubTicketDAO;
import com.cupola.fwmp.dao.mongo.fr.FrDeviceInfoDao;
import com.cupola.fwmp.dao.mongo.vo.fr.FrDeviceVo;
import com.cupola.fwmp.dao.user.UserCacheImpl;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.definition.PriorityValue;
import com.cupola.fwmp.service.domain.engines.classification.jobs.PriortyUpdateHelper;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.CommonUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.vo.BranchVO;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.fr.FRVendorFilter;
import com.cupola.fwmp.vo.fr.FRVendorVO;
import com.cupola.fwmp.vo.fr.FrTicketDetailsVO;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;

/**
 *
 * @author G Ashraf
 * 
 * */

@Transactional
public class FrTicketDetailsDaoImpl implements FrTicketDetailsDao
{

	@Autowired
	private HibernateTemplate hiberanteTemplate;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	SubTicketDAO frVendorDao;
	
	@Autowired
	DefinitionCoreService definitionCoreService;
	
	@Autowired
	private FrDeviceInfoDao deviceDao;
	
	@Autowired
	private BranchDAO branchDao;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	UserCacheImpl userCache;
	
	
	private static Logger LOGGER = Logger.getLogger(FrTicketDetailsDaoImpl.class);
	static int MAX_ITEM_PER_PAGE = 50;
	static int MIN_ITEM_PER_PAGE = 10;
	static int OFFSET = 0;

	static int TL_VENDOR_PAGE = 8;
	
	@Override
	public List<FrTicketDetailsVO> searchFrTicketWithDetails(TicketFilter filter)
	{
		if( filter == null)
			return null;
			
		LOGGER.info("Searching FR Tickets..." +filter.getMqId());
		List<Object[]> customers = null;
		boolean search = false;
		try{
			StringBuilder completeQuery = new StringBuilder(FrTicketDetailsDao
					.SEARCH_FR_TICKET_WITH_FITLERS);
			
			if(filter.getCustomerContactNumber() != null && 
					filter.getCustomerContactNumber() > 0)
			{
				LOGGER.info("Searching Fr tickets for customer using Mobl."
						+filter.getCustomerContactNumber());
				completeQuery.append(" AND c.mobileNumber = :mobileNo");
				search = true;
			}
			else if(filter.getCustomerId() != null &&
					filter.getCustomerId() > 0)
			{
				LOGGER.info("Searching Fr tickets for customer using customer id"
						+filter.getCustomerId());
				completeQuery.append(" AND t.customerId= :custId");
				search = true;
			}
			else if (filter.getWorkorderNumber() != null &&
					!filter.getWorkorderNumber().isEmpty() )
			{
				LOGGER.info("Searching Fr tickets for customer using work order number : "
						+filter.getWorkorderNumber());
				completeQuery.append(" AND fr.workOrderNumber = :workOrder");
				search = true;
			}
			
			if (search)
			{
				Query query = hiberanteTemplate.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery(completeQuery.toString());
				
				if(filter.getCustomerContactNumber() != null && 
						filter.getCustomerContactNumber() > 0)
					query.setParameter("mobileNo", filter.getCustomerContactNumber()+"");
				
				else if(filter.getCustomerId() != null &&
						filter.getCustomerId() > 0)	
					query.setParameter("custId", filter.getCustomerId());
					
				else if(filter.getWorkorderNumber() != null &&
						!filter.getWorkorderNumber().isEmpty() )
					query.setParameter("workOrder", filter.getWorkorderNumber());
				
				LOGGER.info("SQL Query : " +query.getQueryString());
				customers = query.list();
			}
			
			List<FrTicketDetailsVO>  ticketsVo = xformFrTicketDetailsVo(customers ,null);
			
			if(ticketsVo == null || ticketsVo.isEmpty())
				LOGGER.info("Customer not found with details contact no. :"
					+ filter.getCustomerContactNumber() + " or customer Id :"
					+ filter.getCustomerId()
					+ " or work order number :"
					+ filter.getWorkorderNumber());
			
			return ticketsVo;
			
		}catch(Exception e){
			LOGGER.error("Error occure while searching Fr Ticket", e);
		}
		LOGGER.debug("Exit From Searching FR Tickets..." +filter.getMqId() );
		return null;
	}

	@Override
	public List<FrTicketDetailsVO> getFrTicketWithDetails(TicketFilter filter)
	{
	   LOGGER.debug("Fetching Fr Ticket Details..." +filter.getCustomerId() );
		StringBuilder completeQuery = new StringBuilder(FrTicketDetailsDao
				.FR_TICKET_DETAILS_QUERY);
		
		List<Object[]> listTicket = null;
		try{
			
			if( filter != null && filter.getStatus() != null 
					&& filter.getStatus().equals(900l))
				return getUnassignedTicket(filter);
			
			Session session = hiberanteTemplate.getSessionFactory()
					.getCurrentSession();
				boolean isAreaManager = false;
				if(AuthUtils.isAreaManager())
					isAreaManager = true;

			if( filter == null)
			{
				filter = new TicketFilter();
				filter.setStartOffset(0);
				filter.setPageSize(MIN_ITEM_PER_PAGE);
				completeQuery.append(" ORDER BY pr.value DESC ");
				completeQuery.append(" LIMIT "+filter.getStartOffset()+" , "+filter.getPageSize());
				listTicket = session.createSQLQuery(completeQuery.toString()).list();
				return this.xformFrTicketDetailsVo(listTicket ,null);
			}
			else
			{
				if(filter.getTicketId() != null && filter.getTicketId() > 0)
					completeQuery.append(" AND fr.ticketId = "+filter.getTicketId());
				if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0)
					completeQuery.append(" AND fr.ticketId in (:ticketIds)");
				
				List<Long> areaIds = new ArrayList<Long>();
				if(filter.getAreaId() != null && filter.getAreaId() > 0)
					areaIds.add(filter.getAreaId());
				
				else if (filter.getAreaIds() != null && filter.getAreaIds().size() > 0)
					areaIds.addAll(filter.getAreaIds());
				
			/*	else if(filter.getBranchId() != null && filter.getBranchId() > 0)
					areaIds.addAll(LocationCache
							.getAreasByBranchId(filter
									.getBranchId()+"").keySet());
				
				else if(filter.getCityId() != null && filter.getCityId() > 0)
					areaIds.addAll(LocationCache
							.getAreasByCityId(filter
									.getCityId()+"").keySet());*/
				
				// Remove areas if user is having ticket id as filter
				if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0 )
					areaIds.clear();
				
				if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
				{
					isAreaManager = false;
					if(filter.getAssignedByUserId() != null && filter.getAssignedByUserId() >0)
						completeQuery.append(" and  t.currentAssignedTo in(:reportingUsers) ");
					else
					completeQuery.append(" and ( c.areaId in (:areaIds) ) ");
				}
				else if (filter.getBranchId() != null && filter.getBranchId() > 0)
				{	
					if(filter.getAssignedByUserId() != null && filter.getAssignedByUserId() >0)
					{
						completeQuery.append(" and  t.currentAssignedTo in(:reportingUsers) ");
					}
					else
					completeQuery.append(" and  (c.branchId in (:branchIds) ) ");
				}
				else if (filter.getCityId() != null && filter.getCityId() > 0)
				{
					
					if(filter.getAssignedByUserId() != null && filter.getAssignedByUserId() >0)
					{
						if(userHelper.isFRCityManager(filter.getAssignedByUserId()))
							completeQuery.append(" and  c.cityId in (:cityIds) ");
						if(userHelper.isFRBranchManager(filter.getAssignedByUserId()))
							completeQuery.append(" and ( c.branchId  in (:branchIds) or t.currentAssignedTo in(:reportingUsers))  ");
						if(userHelper.isFRAreaManager(filter.getAssignedByUserId()))
							completeQuery.append(" and t.currentAssignedTo in(:reportingUsers) ");
						if(userHelper.isFRTLUser(filter.getAssignedByUserId()))
							completeQuery.append(" and t.currentAssignedTo in (:reportingUsers) ");
					}
					else
						completeQuery.append(" and  c.cityId in (:cityIds) ");
				
				}
				if(isAreaManager)
				{
					completeQuery.append(" and  t.currentAssignedTo in (:reportingUsers) ");
				}
				
				/*if(areaIds.size() > 0 || !areaIds.isEmpty())
					completeQuery.append(" AND t.areaId in (:areaIds)");*/
				
				if(filter.getAssignedUserId() != null && filter.getAssignedUserId() > 0)
					completeQuery.append(" AND t.currentAssignedTo = "+filter
							.getAssignedUserId());
				
				if(filter.getWorkorderNumber() != null && !filter.getWorkorderNumber().isEmpty())
					completeQuery.append(" AND t.workOrderNumber = "
							+ "'"+filter.getWorkorderNumber()+"'");
				
				if(filter.getCustomerContactNumber() != null)
					completeQuery.append(" AND c.mobileNumber = "
							+ filter.getCustomerContactNumber());
				
				if(filter.getSubCategory() != null)
					completeQuery.append(" AND fr.currentSympotmName = :subCategory");
				
				if(filter.getMqId() != null && filter.getMqId().trim().length() > 0 )
					completeQuery.append(" AND c.mqId = :mqId");
				
				if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					completeQuery.append(" AND t.currentAssignedTo in (:assignedIds)");
				
				if(filter.getTicketeCategory() != null )
					completeQuery.append(" AND fr.category = :cateogry");
				
				if( filter.getSymptomCode() != null && filter.getSymptomCode() > 0)
					completeQuery.append(" AND fr.currentSymptomCode = "+filter
							.getSymptomCode());
				
				if (filter.getTicketStatus() != null
						&& filter.getTicketStatus() > 0)
					completeQuery.append(" AND t.status ="
							+ filter.getTicketStatus());
				
				if ( filter.getStatus() != null && filter.getStatus().equals(902l))
					completeQuery.append(" AND t.reopenCount > 0 ");
				
				if(filter.getWorkorderNumber() == null || filter.getWorkorderNumber().isEmpty())
				if (filter.getTicketStatusList() != null
						&& filter.getTicketStatusList().size() > 0 )
					completeQuery.append(" AND t.status in (:statusIds) ");
				
				if(filter.getDeviceId() != null && filter.getDeviceId() > 0)
					completeQuery.append(" AND fr.deviceId = "+filter.getDeviceId());
				
				if(filter.getFlowId() != null && filter.getFlowId() >= 0)
					completeQuery.append(" AND fr.currentFlowId = "+filter.getFlowId());
				
				if(filter.getNatureCode() != null && filter.getNatureCode().trim().length() > 0)
					completeQuery.append(" AND fr.natureCode = :natCode");
				
				if(filter.getDeviceName() != null && !filter.getDeviceName().isEmpty() )
					completeQuery.append(" AND t.fxName = :deviceName");
				
				if (filter.getPriorityFilterValue() != null
						&& filter.getPriorityFilterValue() > 0)
				{
					PriorityValue priority = definitionCoreService
							.getPriorityDefinitionsFor(filter
									.getPriorityFilterValue());
					completeQuery.append(" AND pr.value >= "
							+ (priority.getStart() - PriortyUpdateHelper
									.getGlobalPriority().intValue()));
					completeQuery.append(" AND pr.value <= "
							+ (priority.getEnd() - PriortyUpdateHelper
									.getGlobalPriority().intValue()));
				}
				
				if (filter.getFromDate() != null)
					completeQuery.append(" AND fr.addedOn >= :fromDate");
				
				if (filter.getToDate() != null)
					completeQuery.append(" AND fr.addedOn <= :toDate");
				
				appendCachingFilterQuery(filter, completeQuery );
				
				completeQuery.append(" ORDER BY pr.value DESC ");
				
				if( filter.getPageSize() != 0)
					completeQuery.append(" LIMIT "+filter.getStartOffset()+" , "+filter.getPageSize());
				else
					completeQuery.append(" LIMIT "+ OFFSET +" , "+MAX_ITEM_PER_PAGE);
				
				Query query = session.createSQLQuery(completeQuery.toString());
				
				setCachingFilter(query, filter);
				
				if(filter.getTicketIds() != null 
						&& filter.getTicketIds().size() > 0)
					query.setParameterList("ticketIds", filter.getTicketIds());
				
				/*if(areaIds.size() > 0)
					query.setParameterList("areaIds", areaIds);*/
				
				if(filter.getAreaId() != null && filter.getAreaId() > 0 && areaIds.size() > 0)
					
				{
					if(filter.getAssignedByUserId() != null && filter.getAssignedByUserId() > 0)
					{
						Map<Long, String> reportingUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(), null, null, null);
						if(reportingUsers != null && !reportingUsers.containsKey(filter.getAssignedByUserId()))
							reportingUsers.put(filter.getAssignedByUserId(), null);
						
						if(reportingUsers == null || reportingUsers.size() == 0)
						{
							query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
						}
						else
						{
							query.setParameterList("reportingUsers", reportingUsers.keySet());
						}
					}
					else
					{
						query.setParameterList("areaIds", areaIds);
					}
					
				
				}
				
				
				else if (filter.getBranchId() != null && filter.getBranchId() > 0)
				{
					if(filter.getAssignedByUserId() != null && filter.getAssignedByUserId() > 0)
					{
						Map<Long, String> reportingUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(), null, null, null);
						
						if(reportingUsers != null  && !reportingUsers.containsKey(filter.getAssignedByUserId()))
							reportingUsers.put(filter.getAssignedByUserId(), null);
						if(reportingUsers == null || reportingUsers.size() == 0)
						{
							query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
						}
						else
						{
							query.setParameterList("reportingUsers", reportingUsers.keySet());
						}
					}
					else
					{
						query.setParameterList("branchIds", Collections.singletonList(filter.getBranchId()));
					}
					
				}
				else if (filter.getCityId() != null && filter.getCityId() > 0)
				{
					
					if(filter.getAssignedByUserId()!= null && filter.getAssignedByUserId() > 0 )
					{
			    		UserVo userVo = userDao.getUserById(filter.getAssignedByUserId());
			    		Map<Long, String> reportingUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(), null, null, null);
			    	
			    		if(filter.getAssignedByUserId() != null)
			    			reportingUsers.put(filter.getAssignedByUserId(), null);
			    		
			    		if(userHelper.isFRCityManager(filter.getAssignedByUserId()))
			    		{
			    			query.setParameterList("cityIds",Collections.singletonList( filter.getCityId()));
			    		}
			    		
			    		if(userHelper.isFRBranchManager(filter.getAssignedByUserId()))
			    		{
			    			
			    			Set<TypeAheadVo> branchTypeHeadVo =	userCache.getUserBranchByLoginId(userVo.getLoginId());		
			    			List<Long> branches = new ArrayList<Long>();
			    			for (TypeAheadVo typeAheadVo : branchTypeHeadVo)
							{
			    				branches.add(typeAheadVo.getId());
							}
			    			LOGGER.debug("branches list size :: " +branches.size());
							query.setParameterList("branchIds",branches);
							
							
							if(reportingUsers == null || reportingUsers.size() == 0)
							{
								query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
							}
							else
							{
								query.setParameterList("reportingUsers", reportingUsers.keySet());
							}
			    		}
						if(userHelper.isFRAreaManager(filter.getAssignedByUserId()))
						{
				
							if(reportingUsers == null || reportingUsers.size() == 0)
							{
								query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
							}
							else
							{
								query.setParameterList("reportingUsers", reportingUsers.keySet());
							}
							
						}
						if(userHelper.isFRTLUser(filter.getAssignedByUserId()))
						{
				    		if(reportingUsers == null || reportingUsers.size() == 0)
							{
								query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
							}
							else
							{
								query.setParameterList("reportingUsers", reportingUsers.keySet());
							}
						}
			    		
					}
					else
					{
						query.setParameterList("cityIds",Collections.singletonList( filter.getCityId()));
					}
				}
				if(isAreaManager)
				{
					Map<Long, String> reportingUsers = null;
					if(filter.getAssignedByUserId() != null && filter.getAssignedByUserId() >0)
					{
						 reportingUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(), null, null, null);
						 reportingUsers.put(filter.getAssignedByUserId(), null);
					}
						
					else
					{
						 reportingUsers = userDao.getNestedReportingUsers(AuthUtils.getCurrentUserId(), null, null, null);
						 reportingUsers.put(AuthUtils.getCurrentUserId(), AuthUtils.getCurrentUserDisplayName());
					}
					
					if(reportingUsers == null || reportingUsers.size() == 0)
					{
						query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
					}
					else
					{
						query.setParameterList("reportingUsers", reportingUsers.keySet());
					}
				
				}
					
				
				if(filter.getSubCategory() != null)
					query.setParameter("subCategory", filter.getSubCategory());
				
				if(filter.getMqId() != null && filter.getMqId().trim().length() > 0 )
					query.setParameter("mqId", filter.getMqId());
				
				if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					query.setParameterList("assignedIds", filter.getAssignedUserIds());
				
				if(filter.getTicketeCategory() != null)
					query.setParameter("cateogry", filter.getTicketeCategory());
				
				if(filter.getWorkorderNumber() == null || filter.getWorkorderNumber().isEmpty())
				if (filter.getTicketStatusList() != null
						&& filter.getTicketStatusList().size() > 0)
					query.setParameterList("statusIds", filter.getTicketStatusList());
				
				if(filter.getNatureCode() != null && filter.getNatureCode().trim().length() > 0)
					query.setParameter("natCode", filter.getNatureCode());
				
				if(filter.getDeviceName() != null && !filter.getDeviceName().isEmpty() )
					query.setParameter("deviceName", filter.getDeviceName());
				
				if (filter.getFromDate() != null)
					query.setDate("fromDate", filter.getFromDate());
				if (filter.getToDate() != null)
					query.setDate("toDate", filter.getToDate());
				
//				LOGGER.info("areaIds "+areaIds);
				LOGGER.debug("filter.getTicketStatusList() :: "+filter.getTicketStatusList());
				LOGGER.info("FR Ticket Details Query is : "+query.getQueryString());
//				LOGGER.debug("filter.getTicketIds() size :: "+filter.getTicketIds().size());
				listTicket = query.list();
				return xformFrTicketDetailsVo(listTicket,null);
			}
		}catch(Exception e){
			LOGGER.error("Error occure while fetching FR Tickets Details...",e);
		}
		LOGGER.debug(" Exit From Fetching Fr Ticket Details..." + filter.getAreaId());
		return null;
	}

	private List<FrTicketDetailsVO> getUnassignedTicket(TicketFilter filter)
	{
		if( filter == null )
			return new ArrayList<FrTicketDetailsVO>();
		
		LOGGER.debug(" inside getUnassignedTicket..." +filter.getAreaId());
		StringBuilder completeQuery = new StringBuilder(FrTicketDetailsDao
				.FR_TICKET_DETAILS_QUERY);
		
		List<Object[]> listTicket = null;
		
		try{
			Session session = hiberanteTemplate.getSessionFactory()
					.getCurrentSession();
			
			boolean isAreaManager = false;
			if(AuthUtils.isAreaManager())
				isAreaManager = true;

			if(filter.getTicketId() != null && filter.getTicketId() > 0)
				completeQuery.append(" AND fr.ticketId = "+filter.getTicketId());
			if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0)
				completeQuery.append(" AND fr.ticketId in (:ticketIds)");
			
			List<Long> areaIds = new ArrayList<Long>();
			if(filter.getAreaId() != null && filter.getAreaId() > 0)
				areaIds.add(filter.getAreaId());
			
			else if (filter.getAreaIds() != null && filter.getAreaIds().size() > 0)
				areaIds.addAll(filter.getAreaIds());
			
			if(filter.getTicketIds() != null && filter.getTicketIds().size() > 0 )
				areaIds.clear();
			
			if( isAreaManager && areaIds.size() > 0)
			{
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
					completeQuery.append(" and c.areaId in (:areaIds) and t.currentAssignedTo in(:reportingUsers) ");
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					completeQuery.append(" and c.areaId in (:areaIds) and t.currentAssignedTo in(:reportingUsers) ");
				else
					completeQuery.append(" and c.areaId in (:areaIds) ");
			}
			if( isAreaManager && areaIds.isEmpty() )
			{
				TypeAheadVo userVo = AuthUtils.getCurrentUserCity();
				long cityId = 0;
				if( userVo != null)
					cityId = userVo.getId();
				
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
					completeQuery.append(" and c.cityId ="+cityId+" and  t.currentAssignedTo in(:reportingUsers)");
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					completeQuery.append(" and c.cityId ="+cityId+" and t.currentAssignedTo in(:reportingUsers)  ");
			}
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
					completeQuery.append(" and c.branchId ="+filter.getBranchId()+" and  t.currentAssignedTo in(:reportingUsers) ");
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					completeQuery.append(" and c.branchId ="+filter.getBranchId()+" and ( t.currentAssignedTo in(:reportingUsers) or t.status="+FRStatus.FR_UNASSIGNEDT_TICKET+") ");
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
					completeQuery.append(" and c.cityId ="+filter.getCityId()+" and  t.currentAssignedTo in(:reportingUsers)");
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					completeQuery.append(" and c.cityId ="+filter.getCityId()+" and ( t.currentAssignedTo in(:reportingUsers) or t.status="+FRStatus.FR_UNASSIGNEDT_TICKET+") ");
			}
			
			if(filter.getAssignedUserId() != null && filter.getAssignedUserId() > 0)
				completeQuery.append(" AND t.currentAssignedTo = "+filter
						.getAssignedUserId());
			
			if(filter.getWorkorderNumber() != null && !filter.getWorkorderNumber().isEmpty())
				completeQuery.append(" AND t.workOrderNumber = "
						+ "'"+filter.getWorkorderNumber()+"'");
			
			if(filter.getCustomerContactNumber() != null)
				completeQuery.append(" AND c.mobileNumber = "
						+ filter.getCustomerContactNumber());
			
			
			if(filter.getMqId() != null && filter.getMqId().trim().length() > 0 )
				completeQuery.append(" AND c.mqId = :mqId");
			
			if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0
					&& !AuthUtils.isFrManager() )
				completeQuery.append(" AND t.currentAssignedTo in(:assignedIds)");
			
			if (filter.getTicketStatus() != null
					&& filter.getTicketStatus() > 0)
				completeQuery.append(" AND t.status ="
						+ filter.getTicketStatus());
			
			if ( filter.getStatus() != null && filter.getStatus().equals(902l))
				completeQuery.append(" AND t.reopenCount > 0 ");
			
			if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
				completeQuery.append(" AND t.status in (:statusIds) ");
			
			if(filter.getFlowId() != null && filter.getFlowId() >= 0)
				completeQuery.append(" AND fr.currentFlowId = "+filter.getFlowId());
			
			if(filter.getDeviceName() != null && !filter.getDeviceName().isEmpty() )
				completeQuery.append(" AND t.fxName = :deviceName");
			
			if (filter.getPriorityFilterValue() != null
					&& filter.getPriorityFilterValue() > 0)
			{
				PriorityValue priority = definitionCoreService
						.getPriorityDefinitionsFor(filter
								.getPriorityFilterValue());
				completeQuery.append(" AND pr.value >= "
						+ (priority.getStart() - PriortyUpdateHelper
								.getGlobalPriority().intValue()));
				completeQuery.append(" AND pr.value <= "
						+ (priority.getEnd() - PriortyUpdateHelper
								.getGlobalPriority().intValue()));
			}
			
			if (filter.getFromDate() != null)
				completeQuery.append(" AND fr.addedOn >= :fromDate");
			
			if (filter.getToDate() != null)
				completeQuery.append(" AND fr.addedOn <= :toDate");
			
			completeQuery.append(" ORDER BY pr.value DESC ");
			
			if( filter.getPageSize() != 0)
				completeQuery.append(" LIMIT "+filter.getStartOffset()+" , "+filter.getPageSize());
			else
				completeQuery.append(" LIMIT "+ OFFSET +" , "+MAX_ITEM_PER_PAGE);
			
			Query query = session.createSQLQuery(completeQuery.toString());
			
			if(filter.getTicketIds() != null 
					&& filter.getTicketIds().size() > 0)
				query.setParameterList("ticketIds", filter.getTicketIds());
			
			
			if( isAreaManager && areaIds.size() > 0)
			{
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
				{
					query.setParameterList("areaIds", areaIds);
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
				}
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
				{
					query.setParameterList("areaIds", areaIds);
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
				}
				else
					query.setParameterList("areaIds", areaIds);
			}
			if( isAreaManager && areaIds.isEmpty() )
			{
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
			}
			else if (filter.getBranchId() != null && filter.getBranchId() > 0)
			{
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
				
			}
			else if (filter.getCityId() != null && filter.getCityId() > 0)
			{
				if( filter.getSymptomCode() != null && filter.getSymptomCode().equals(900))
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
				else if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 )
					query.setParameterList("reportingUsers", filter.getAssignedUserIds());
			}
			
			if(filter.getMqId() != null && filter.getMqId().trim().length() > 0 )
				query.setParameter("mqId", filter.getMqId());
			
			if( filter.getAssignedUserIds() != null && filter.getAssignedUserIds() .size() > 0 
					&& !AuthUtils.isFrManager())
				query.setParameterList("assignedIds", filter.getAssignedUserIds());
			
			
			if (filter.getTicketStatusList() != null
					&& filter.getTicketStatusList().size() > 0)
//				query.setParameterList("statusIds", filter.getTicketStatusList());
			query.setParameterList("statusIds", filter.getTicketStatusList());
			
			if (filter.getFromDate() != null)
				query.setDate("fromDate", filter.getFromDate());
			if (filter.getToDate() != null)
				query.setDate("toDate", filter.getToDate());
			
			LOGGER.info("filter.getTicketStatusList() "+filter.getTicketStatusList());
			
			listTicket = query.list();
			
			LOGGER.debug(" Exiting from FR Ticket Details Query is FOR Unassigned Search: "+query.getQueryString());
			return xformFrTicketDetailsVo(listTicket,null);
			
		}catch(Exception e)
		{
			LOGGER.error("Error in getUnassignedTicket");
			e.printStackTrace();
		}
		LOGGER.debug(" Exit from getUnassignedTicket..." +filter.getAreaId());
		return null;
	}

	private List<FrTicketDetailsVO> xformFrTicketDetailsVo(List<Object[]> listTicket, FRVendorFilter filter) {
		List<FrTicketDetailsVO> ticketDetailsVo = new ArrayList<FrTicketDetailsVO>();
		List<CustomerAddressVO> cavolist;
		ObjectMapper mapper = new ObjectMapper();

		if (listTicket == null)
			return ticketDetailsVo;

		FrTicketDetailsVO frTicketDetailsVo = null;

		for (Object[] row : listTicket) {
			
			try {
				frTicketDetailsVo = new FrTicketDetailsVO();
				cavolist = new ArrayList<CustomerAddressVO>();

				frTicketDetailsVo.setProspectNo(getObjectToString(row[0]));
				

				if (row[2] != null) {
					Long assignedTo = objectToLong(row[2]);
					frTicketDetailsVo.setAssignedToId(assignedTo);
					UserVo engineer = userDao.getUserById(assignedTo);
					if (engineer != null) {
						// frTicketDetailsVo.setAssignedToDisplayName(engineer.getFirstName()+"
						// "+engineer.getLastName());
						frTicketDetailsVo.setAssignedToDisplayName(
								getCustomerFullName(null, engineer.getFirstName(), null, engineer.getLastName()));

						frTicketDetailsVo.setAssignedNEMobileNumber(engineer.getMobileNo() + "");
					}

				}

					
				
			frTicketDetailsVo.setCustomerId(getObjectToString(row[3]));
			frTicketDetailsVo.setMqId(getObjectToString(row[4]));
			
			frTicketDetailsVo.setCustomerName(getCustomerFullName(getObjectToString(row[5])
					, getObjectToString(row[6]),
					getObjectToString(row[7]), getObjectToString(row[8])));
			
			frTicketDetailsVo.setMobileNumber(getObjectToString(row[9]));

			frTicketDetailsVo.setBranchId(objectToLong(row[11]));
			frTicketDetailsVo.setCityId(objectToLong(row[12]));
			frTicketDetailsVo.setAreaId(objectToLong(row[13]));
			frTicketDetailsVo.setAreaName(LocationCache.getAreaNameById(frTicketDetailsVo.getAreaId()));
			frTicketDetailsVo.setCity(LocationCache.getCityNameById(frTicketDetailsVo.getCityId()));
			frTicketDetailsVo.setBranch(LocationCache.getBrancheNameById(frTicketDetailsVo.getBranchId()));
			
			
			frTicketDetailsVo.setPincode(getObjectToString(row[15]));
			frTicketDetailsVo.setCustomerEmail(getObjectToString(row[16]));
			
			
			frTicketDetailsVo.setFrId(objectToLong(row[17]));
			frTicketDetailsVo.setTicketId(getObjectToString(row[18]));
			frTicketDetailsVo.setId(getObjectToString(row[18]));
			
			frTicketDetailsVo.setCategory(getObjectToString(row[19]));
			frTicketDetailsVo.setSubCategory(getObjectToString(row[20]));
			frTicketDetailsVo.setUpdatedSymptomCode(getObjectToString(row[21]));
			frTicketDetailsVo.setNatureCode(getObjectToString(row[22]));
			
//			frTicketDetailsVo.setUserAssignedTo(ticketVo.getAssignedToId());
//			frTicketDetailsVo.setEngineerName(ticketVo.getAssignedToDisplayName());
			
			frTicketDetailsVo.setStatusList(getObjectToString(row[23]));
//changed by manjuprasad for VOC+Description isssue			
//			frTicketDetailsVo.setVoc(getObjectToString(row[24]));

			frTicketDetailsVo.setVoc(getObjectToString(row[24])+"		Description: "+getObjectToString(row[62]));
			frTicketDetailsVo.setAddedOn(GenericUtil.convertToDateFormat(row[25]));
			frTicketDetailsVo.setFlowId(objectToLong(row[26]));
			frTicketDetailsVo.setFxName(getObjectToString(row[27]));
//			frTicketDetailsVo.setDeviceId(objectToLong(row[27]));
			frTicketDetailsVo.setDeviceName(frTicketDetailsVo.getFxName());
			frTicketDetailsVo.setReopenCount(objectToInt(row[28]));
			
			frTicketDetailsVo.setFxIp(getObjectToString(row[29]));
			frTicketDetailsVo.setFxMac(getObjectToString(row[30]));
			frTicketDetailsVo.setFxPorts(getObjectToString(row[31]) != null ? Long.valueOf(getObjectToString(row[31])):0);
			
			frTicketDetailsVo.setCxIp(getObjectToString(row[32]));
			frTicketDetailsVo.setCxMac(getObjectToString(row[33]));
			
			frTicketDetailsVo.setFlowName(FrTicketUtil.
					findSymptomCategoryForFlowId(frTicketDetailsVo.getFlowId()));
			
			frTicketDetailsVo.setWorkOrderNumber(getObjectToString(row[34]));
			frTicketDetailsVo.setCurrentEtr(GenericUtil.convertToUiDateFormat(row[35]));
			frTicketDetailsVo.setCommitedEtr(GenericUtil.convertToUiDateFormat(row[36]));
			frTicketDetailsVo.setLocked(objectToBoolean(row[37]));
			
			frTicketDetailsVo.setStatusId(objectToLong(row[38]));
			frTicketDetailsVo.setCurrentFlowId(objectToLong(row[39]));
			frTicketDetailsVo.setCurrentSymptomName(getObjectToString(row[40]));
			
			frTicketDetailsVo.setCurrentFlowName(FrTicketUtil.
					findSymptomCategoryForFlowId(frTicketDetailsVo.getCurrentFlowId()));
			
		//added by manjuprasad for priority calculation	
			int caculatedPriority=0;
			if (row[1]!=null) {
				Date createdDate = GenericUtil.converDateFromString(GenericUtil.convertToDateFormat(row[1]));
				caculatedPriority = PriortyUpdateHelper.calculatePriorityBasedOnTime(System.currentTimeMillis(),createdDate.getTime()).intValue();
			}
/*			
			frTicketDetailsVo.setPriority(objectToInt(row[41])
					+ PriortyUpdateHelper.getGlobalPriority().intValue());*/
			
			frTicketDetailsVo.setPriority(objectToInt(row[41])
					+ caculatedPriority);
			
			frTicketDetailsVo.setPriorityEscalationType(objectToInt(row[42]));
			frTicketDetailsVo.setEscalatedValue(objectToInt(row[43]));
			frTicketDetailsVo.setSliderEscalatedValue(objectToInt(row[44]));
			frTicketDetailsVo.setUpdatedCount(objectToInt(row[45]));
			
			/*FrDeviceVo device = getDeviceDataByTicket(frTicketDetailsVo.getTicketId());
			if(device != null)
			{
				frTicketDetailsVo.setFxIp(device.getFxIp());
				frTicketDetailsVo.setFxMac(device.getFxMax());
				frTicketDetailsVo.setFxPorts(device.getFxPort() != null
						? device.getFxPort() : 0);

				frTicketDetailsVo.setCxName(device.getCxName());
				frTicketDetailsVo.setCxIp(device.getCxIp());
				frTicketDetailsVo.setCxMac(device.getCxMax());
				frTicketDetailsVo.setCxPorts(device.getCxPort() != null
						? device.getCxPort() : 0);
			}*/
			Date modifiedDate = null;
			
			long modifiedBy = 0;
			
			
			if(filter != null)
			{
					// u.id as vendorId, u.firstName
					// ,sk.skillName,usm.idSkill,vt.status as VtStatus
					LOGGER.debug(" xformFrTicketDetailsVo..." + filter.getTicketId());

					FRVendorVO vendor = new FRVendorVO(objectToLong(row[46]), (String) row[47],
							ActionTypeAttributeDefinitions.SKILL_DEFINITION.get(objectToInt(row[48])),
							objectToLong(row[48]));
					vendor.setEstimatedStartTime(getObjectToString(row[50]));
					frTicketDetailsVo.setVendor(vendor);
					frTicketDetailsVo.setStatusId(objectToLong(row[49]));

					modifiedDate = GenericUtil.converDateFromString(GenericUtil.convertToDateFormat(row[51]));
					frTicketDetailsVo.setBlocked(FrTicketUtil.objectToBoolean(row[52]));

					frTicketDetailsVo.setCxName(getObjectToString(row[53]));
					frTicketDetailsVo.setCxPorts(
							getObjectToString(row[54]) != null ? Long.valueOf(getObjectToString(row[54])) : 0);

					frTicketDetailsVo.setOldCustAccNo(getObjectToString(row[55]));
					frTicketDetailsVo.setNewCustAccNo(getObjectToString(row[56]));
					frTicketDetailsVo.setPushBackCount(objectToInt(row[57]));
					frTicketDetailsVo.setReason(getObjectToString(row[58]));
					frTicketDetailsVo.setRemarks(getObjectToString(row[59]));

					frTicketDetailsVo.setDefectCode(getObjectToString(row[60]));
					frTicketDetailsVo.setSubDefectCode(getObjectToString(row[61]));

					modifiedBy = objectToLong(row[62]);
					frTicketDetailsVo.setCommunicatedEtr(GenericUtil.convertToUiDateFormat(row[63]));
					frTicketDetailsVo.setCurrentProgress(getObjectToString(row[64]));
					frTicketDetailsVo.setCreatedDate(GenericUtil.convertToDateFormat(row[65]));

			}
			else
			{
					modifiedDate = GenericUtil.converDateFromString(GenericUtil.convertToDateFormat(row[46]));
					frTicketDetailsVo.setBlocked(FrTicketUtil.objectToBoolean(row[47]));

					frTicketDetailsVo.setCxName(getObjectToString(row[48]));
					frTicketDetailsVo.setCxPorts(
							getObjectToString(row[49]) != null ? Long.valueOf(getObjectToString(row[49])) : 0);

					frTicketDetailsVo.setOldCustAccNo(getObjectToString(row[50]));
					frTicketDetailsVo.setNewCustAccNo(getObjectToString(row[51]));
					frTicketDetailsVo.setPushBackCount(objectToInt(row[52]));
					frTicketDetailsVo.setReason(getObjectToString(row[53]));
					frTicketDetailsVo.setRemarks(getObjectToString(row[54]));

					frTicketDetailsVo.setDefectCode(getObjectToString(row[55]));
					frTicketDetailsVo.setSubDefectCode(getObjectToString(row[56]));

					modifiedBy = objectToLong(row[57]);
					frTicketDetailsVo.setCommunicatedEtr(GenericUtil.convertToUiDateFormat(row[58]));
					frTicketDetailsVo.setCurrentProgress(getObjectToString(row[59]));
					frTicketDetailsVo.setCreatedDate(GenericUtil.convertToDateFormat(row[60]));
				}
				
				// **** source is setting to frticketdetailsvo **************
				
				//System.out.println("getObjectToString(row[60]) " + getObjectToString(row[60]));
				
				frTicketDetailsVo.setModifiedBy(modifiedBy);
				UserVo engineer = userDao.getUserById(modifiedBy);
				if (engineer != null) {
					if (modifiedBy == 1)
						frTicketDetailsVo.setModifiedDisplayName(FlowAttribute.SYMTEM_ASSIGNED);
					else
						frTicketDetailsVo.setModifiedDisplayName(
								getCustomerFullName(null, engineer.getFirstName(), null, engineer.getLastName()));
				} else
					frTicketDetailsVo.setModifiedDisplayName("--");

				Long statusId = frTicketDetailsVo.getStatusId();
				if (statusId != null) {
					/*
					 * frTicketDetailsVo.setStatus(TicketStatus.
					 * statusDisplayValue .get(statusId.intValue()));
					 */

					frTicketDetailsVo.setStatus(definitionCoreService.getFrStatusStringByStatusId(statusId));

					boolean editable = definitionCoreService.getEditableStatusFilterForCurrentUser()
							.contains(statusId.intValue());

					if (editable)
						frTicketDetailsVo.setEditable(false);
					else
						frTicketDetailsVo.setEditable(true);
				}
			
			if( frTicketDetailsVo.isBlocked() )
				frTicketDetailsVo.setProceedStatus(	FRStatus.FLOW_APPROVAL_PENDING);
			else
				frTicketDetailsVo.setProceedStatus(0);
			
//			if( filter!= null 
//					&& filter.getVendorUserId() <= 0)
//			{
				frTicketDetailsVo.setVendors(frVendorDao.getVendorsForTicket(Long.valueOf(frTicketDetailsVo.getTicketId())));

				// }
				BranchVO branch = branchDao.getBranchById(frTicketDetailsVo.getBranchId());
				if (branch != null)
					frTicketDetailsVo.setFlowSwitch(branch.getFlowSwitch());
				else
					frTicketDetailsVo.setFlowSwitch("1");

				/*
				 * calculate GART for open or closed ticket set parameter ticket
				 * creation date only for open GART set both last modified and
				 * ticket creation date for closed GART
				 * 
				 */

				frTicketDetailsVo.setTicketGART(CommonUtil.getTicketGART(objectToInt(row[38]),
						GenericUtil.converDateFromString(frTicketDetailsVo.getCreatedDate()), modifiedDate));

				
				
				
				
				

				String communicationAddress=getObjectToString(row[10]);
				
				if(communicationAddress != null && CommonUtil.isValidJson(communicationAddress)){
						
						CustomerAddressVO communicationAddressVO = mapper.readValue(communicationAddress,
								CustomerAddressVO.class);
						if (communicationAddressVO.getOperationalEntity() == null
								|| communicationAddressVO.getOperationalEntity().isEmpty()) {
							communicationAddressVO.setOperationalEntity(frTicketDetailsVo.getBranch());
							communicationAddressVO.setOperationalEntityId(frTicketDetailsVo.getBranchId());

						}

						if (communicationAddressVO.getCity() == null || communicationAddressVO.getCity().isEmpty()) {
							communicationAddressVO.setCity(frTicketDetailsVo.getCity());
						}

						if (communicationAddressVO.getArea() == null || communicationAddressVO.getArea().isEmpty()) {
							communicationAddressVO.setArea(frTicketDetailsVo.getAreaName());
							communicationAddressVO.setAreaId(frTicketDetailsVo.getAreaId());
						}

						if (communicationAddressVO.getSubArea() == null
								|| communicationAddressVO.getSubArea().isEmpty()) {
							communicationAddressVO.setSubArea(frTicketDetailsVo.getSubAreaName());
							if (frTicketDetailsVo.getSubAreaId() != null && frTicketDetailsVo.getSubAreaId()>0)
								communicationAddressVO.setSubAreaId(Long.valueOf(frTicketDetailsVo.getSubAreaId()));
						}

						
						cavolist.add(communicationAddressVO);
					}else{
						
						frTicketDetailsVo.setCustomerAddress(getObjectToString(row[10]));
					}
				
				String permanentAddress=getObjectToString(row[14]);
				
				if(permanentAddress != null && CommonUtil.isValidJson(permanentAddress)){
						
						CustomerAddressVO permanentAddressVO = mapper.readValue(permanentAddress,
								CustomerAddressVO.class);
						if (permanentAddressVO.getOperationalEntity() == null
								|| permanentAddressVO.getOperationalEntity().isEmpty()) {
							permanentAddressVO.setOperationalEntity(frTicketDetailsVo.getBranch());
							permanentAddressVO.setOperationalEntityId(frTicketDetailsVo.getBranchId());

						}

						if (permanentAddressVO.getCity() == null || permanentAddressVO.getCity().isEmpty()) {
							permanentAddressVO.setCity(frTicketDetailsVo.getCity());
						}

						if (permanentAddressVO.getArea() == null || permanentAddressVO.getArea().isEmpty()) {
							permanentAddressVO.setArea(frTicketDetailsVo.getAreaName());
							permanentAddressVO.setAreaId(frTicketDetailsVo.getAreaId());
						}

						if (permanentAddressVO.getSubArea() == null
								|| permanentAddressVO.getSubArea().isEmpty()) {
							permanentAddressVO.setSubArea(frTicketDetailsVo.getSubAreaName());
							if (frTicketDetailsVo.getSubAreaId() != null && frTicketDetailsVo.getSubAreaId()>0)
								permanentAddressVO.setSubAreaId(Long.valueOf(frTicketDetailsVo.getSubAreaId()));
						}

						
						cavolist.add(permanentAddressVO);
					}else{
						
						frTicketDetailsVo.setCustomerPermanentAddress(getObjectToString(row[14]));
					}
				
				int currentAddressPlace;
				if(filter != null)
					currentAddressPlace = 66;
				else
					currentAddressPlace = 61;
				
				String currentAddress=getObjectToString(row[currentAddressPlace]);
				
				if(currentAddress != null && CommonUtil.isValidJson(currentAddress)){
						
						CustomerAddressVO currentAddressVO = mapper.readValue(currentAddress,
								CustomerAddressVO.class);
						if (currentAddressVO.getOperationalEntity() == null
								|| currentAddressVO.getOperationalEntity().isEmpty()) {
							currentAddressVO.setOperationalEntity(frTicketDetailsVo.getBranch());
							currentAddressVO.setOperationalEntityId(frTicketDetailsVo.getBranchId());

						}

						if (currentAddressVO.getCity() == null || currentAddressVO.getCity().isEmpty()) {
							currentAddressVO.setCity(frTicketDetailsVo.getCity());
						}

						if (currentAddressVO.getArea() == null || currentAddressVO.getArea().isEmpty()) {
							currentAddressVO.setArea(frTicketDetailsVo.getAreaName());
							currentAddressVO.setAreaId(frTicketDetailsVo.getAreaId());
						}

						if (currentAddressVO.getSubArea() == null
								|| currentAddressVO.getSubArea().isEmpty()) {
							currentAddressVO.setSubArea(frTicketDetailsVo.getSubAreaName());
							if (frTicketDetailsVo.getSubAreaId() != null && frTicketDetailsVo.getSubAreaId()>0)
								currentAddressVO.setSubAreaId(Long.valueOf(frTicketDetailsVo.getSubAreaId()));
						}
						
						cavolist.add(currentAddressVO);
					}else{
						
						frTicketDetailsVo.setCustomerAddress(getObjectToString(row[currentAddressPlace]));
					}
				
				frTicketDetailsVo.setCustomerAddressesList(cavolist);
				
				ticketDetailsVo.add(frTicketDetailsVo);

				LOGGER.debug("Total Fr Ticket founds :" + ticketDetailsVo.size());
				
			} catch (Exception e) {
				LOGGER.info("FrTicket details ::"+ticketDetailsVo);
				e.printStackTrace();
				continue;
			}
		}
		LOGGER.debug("Total Fr Ticket founds :"+ticketDetailsVo.size());
		return ticketDetailsVo;
	}
	
	public List<FrTicketDetailsVO> getFrVendorTickets(FRVendorFilter filter)
	{
        LOGGER.debug("getFrVendorTickets..." + filter.getTicketId() );
		StringBuilder completeQuery = new StringBuilder(FrTicketDetailsDao
				.FR_VENDOR_TICKET_DETAILS_QUERY);
		
		List<Object[]> listTicket = null;
		try{
			Session session = hiberanteTemplate.getSessionFactory()
					.getCurrentSession();
			if( filter == null)
			{
				filter = new FRVendorFilter();
				filter.setStartOffset(0);
				filter.setPageSize(MIN_ITEM_PER_PAGE);
				completeQuery.append(" ORDER BY pr.value DESC,t.TicketCreationDate ASC LIMIT "+filter.getStartOffset()+" , "+filter.getPageSize());
				listTicket = session.createSQLQuery(completeQuery.toString()).list();
				return this.xformFrTicketDetailsVo(listTicket ,filter);
			}
			else
			{
				if(filter.getVendorUserId() > 0)
				{
					completeQuery.append(" AND vt.userId ="+filter.getVendorUserId());
				}
				
				if( filter.getTicketId() > 0)
					completeQuery.append(" AND fr.ticketId = "+filter.getTicketId());
				 
			 
				
				if( filter.getAssocatedToUserId() > 0)
					completeQuery.append(" AND  usm.idUser = "+filter
							.getAssocatedToUserId());
				
			/*	if(filter.getCustomerId() != null && filter.getCustomerId() > 0)
					completeQuery.append(" AND t.prospectNo = "
							+ filter.getCustomerId());
				
				if(filter.getCustomerContactNumber() != null)
					completeQuery.append(" AND c.mobileNumber = "
							+ filter.getCustomerContactNumber());
				
				if(filter.getSubCategory() != null)
					completeQuery.append(" AND fr.currentSympotmName = :subCategory");
				
				if(filter.getTicketeCategory() != null )
					completeQuery.append(" AND fr.category = :cateogry");
				
				if( filter.getSymptomCode() != null && filter.getSymptomCode() > 0)
					completeQuery.append(" AND fr.currentSymptomCode = "+filter
							.getSymptomCode());
				
				if (filter.getTicketStatus() != null
						&& filter.getTicketStatus() > 0)
					completeQuery.append(" AND t.status ="
							+ filter.getTicketStatus());
				
				if (filter.getTicketStatusList() != null
						&& filter.getTicketStatusList().size() > 0)
					completeQuery.append(" AND t.status in (:statusIds) ");
				
				if(filter.getDeviceId() != null && filter.getDeviceId() > 0)
					completeQuery.append(" AND fr.deviceId = "+filter.getDeviceId());
				
				if(filter.getFlowId() != null && filter.getFlowId() >= 0)
					completeQuery.append(" AND fr.currentFlowId = "+filter.getFlowId());
				
				if(filter.getNatureCode() != null && filter.getNatureCode().trim().length() > 0)
					completeQuery.append(" AND fr.natureCode = :natCode");
				
				if (filter.getPriorityFilterValue() != null
						&& filter.getPriorityFilterValue() > 0)
				{
					PriorityValue priority = definitionCoreService
							.getPriorityDefinitionsFor(filter
									.getPriorityFilterValue());
					completeQuery.append(" AND pr.value >= "
							+ (priority.getStart() - PriortyUpdateHelper
									.getGlobalPriority().intValue()));
					completeQuery.append(" AND pr.value <= "
							+ (priority.getEnd() - PriortyUpdateHelper
									.getGlobalPriority().intValue()));
				}
				*/
				if (filter.getFromDate() != null)
					completeQuery.append(" AND fr.addedOn >= :fromDate");
				
				if (filter.getToDate() != null)
					completeQuery.append(" AND fr.addedOn <= :toDate");
				
				if( filter.getPageSize() != 0)
					completeQuery.append(" ORDER BY pr.value DESC,t.TicketCreationDate ASC LIMIT "+filter.getStartOffset()+" , "+filter.getPageSize());
				else
					completeQuery.append(" ORDER BY pr.value DESC,t.TicketCreationDate ASC LIMIT "+ OFFSET +" , "+MAX_ITEM_PER_PAGE);
				
				Query query = session.createSQLQuery(completeQuery.toString());
				 
				
				LOGGER.info("Vendor FR Ticket Details Query is : "+query.getQueryString());
				
				listTicket = query.list();
				return xformFrTicketDetailsVo(listTicket,filter);
			}
		}catch(Exception e){
			LOGGER.error("Error occure while fetching FR Tickets Details...",e);
		}
		LOGGER.debug(" Exit From getFrVendorTickets..." + filter.getTicketId());
		return null;
	
		
	}
	
	@Override
	public FrDeviceVo getDeviceDataByTicket(Long ticketId)
	{
		LOGGER.debug("  inside  getDeviceDataByTicket..." + ticketId);
		if( ticketId == null || ticketId <= 0)
			return null;

		return deviceDao.getDeviceInfoByTicket(ticketId);
	}
	
	private int getTicketPendingStatus(Long inStatus)
	{
	   LOGGER.debug(" getTicketPendingStatus..." + inStatus);
		Integer status = inStatus.intValue();
		if(definitionCoreService.getStatusFilterForRole
				(DefinitionCoreService.TICKET_PENDING_STATUS).contains(status))
			return FRStatus.FLOW_APPROVAL_PENDING;
		else
			return 0;
			
	}
	
	private String getCustomerFullName(String title, String firstName ,
			String middleName, String lastName)
	{
	   LOGGER.debug(" getTicketPendingStatus..." + firstName + " " +lastName);
		String fullName = firstName;
		if(title != null)
			fullName = title +" "+fullName;
		if(middleName != null)
			fullName = fullName+" "+middleName;
		
		if(lastName != null)
			fullName = fullName +" "+lastName;
		LOGGER.debug("Exit From getTicketPendingStatus..." + firstName + " " +lastName);
		return fullName;
	}
	
	private boolean objectToBoolean(Object value)
	{
	   LOGGER.debug(" objectToBoolean..." + value );
		int val = Integer.valueOf(value.toString());
		if( val == 1)
			return true;
		else
			return false;
	}
	
	private String getObjectToString(Object value)
	{
		if(value == null)
			return null;
		else
			return value.toString();
	}
	
	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());
	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}
	
	private void appendCachingFilterQuery(TicketFilter filter,StringBuilder query)
	{
		LOGGER.debug("  inside  appendCachingFilterQuery..." + query);
		if(filter.getRequestKey() != null)
		{
			  query.append(" and (t.status not in (:closedStatus) or t.modifiedOn >=:lastModified )");
//					.append(" and t.modifiedOn >=:lastModified ");
			
		}
		 
	}
	private void setCachingFilter(Query q,TicketFilter filter)
	{
		if(filter == null)
			return;
		
		LOGGER.debug("  inside  setCachingFilter..." + filter.getMqId());
		if(filter.getRequestKey() != null)
		{
			
			 q.setParameterList("closedStatus",TicketStatus.COMPLETED_DEF) ;
			 Date date = GenericUtil.getLastRequestTime(filter.getRequestKey());
			 q.setDate("lastModified", date);
		}
	}

}
