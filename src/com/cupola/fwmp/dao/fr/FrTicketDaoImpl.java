/*******************************************************************************
 * Copyright 2016 (c) Cupola Technology Pvt Ltd. - All Rights Reserved
 *
 * Proprietary and confidential.
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *******************************************************************************/
package com.cupola.fwmp.dao.fr;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant;
import com.cupola.fwmp.FRConstant.ActionTypeAttributeDefinitions;
import com.cupola.fwmp.FRConstant.FRStatus;
import com.cupola.fwmp.FRConstant.UpdateType;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.ControlBlockStatus;
import com.cupola.fwmp.FWMPConstant.FrTicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.dao.user.UserCacheImpl;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.dao.user.UserDaoImpl;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.Device;
import com.cupola.fwmp.persistance.entities.FrDetail;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.FrTicketEtr;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.fr.FrTicketService;
import com.cupola.fwmp.service.user.UserHelper;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.FieldValidatorUtil;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StatusMessages;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.util.tools.CommonToolUtil;
import com.cupola.fwmp.vo.AssignVO;
import com.cupola.fwmp.vo.StatusVO;
import com.cupola.fwmp.vo.TicketReassignmentLogVo;
import com.cupola.fwmp.vo.TypeAheadVo;
import com.cupola.fwmp.vo.UserVo;
import com.cupola.fwmp.vo.counts.dashboard.FRSummaryCountVO;
import com.cupola.fwmp.vo.counts.dashboard.FRTicketCountSummaryVO;
import com.cupola.fwmp.vo.counts.dashboard.FrTicketTlneDetailVo;
import com.cupola.fwmp.vo.fr.ETRExpireVo;
import com.cupola.fwmp.vo.fr.FrTicketPushBackVo;
import com.cupola.fwmp.vo.fr.FrTicketVo;
import com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo;


@Transactional
@Lazy(false)
public class FrTicketDaoImpl implements FrTicketDao
{

	private static Logger LOGGER = Logger.getLogger(FrTicketDaoImpl.class.getName());

	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	UserCacheImpl userCache;
	@Autowired
	DBUtil dbUtil;
	@Autowired
	private GlobalActivities globalActivities;
	
	@Autowired
	ScratchPadDAO ScratchPad;
	
	@Autowired
	DefinitionCoreServiceImpl definitionCoreService;
	
	@Autowired
	private FrTicketService frService;
	
	private final String TOTAL = "Total";
	private final String PENDING = "Pending";
	private final String CLOSED = "Closed";
	private static final String keyOfEtrUpdateTimeInMinutes = "frticket.mq.update.time.in.minutes";
	/** this method creating fr detail with work order from crm */
	@Autowired
	UserDao userDao;
	private int afpVendorClosedTicketInBucketHours;
	
	private Map<Long, Boolean> resultOfWorkOrderCreations = new ConcurrentHashMap<>();
	
	private Map<String, MQWorkorderVO> failedFrDetailsMap = new ConcurrentHashMap<>();
	private Map<String, Long> completeProspectMapRetry = new ConcurrentHashMap<>();
	
	
	@Override
	public Long createFrTicket(FrTicketVo frTicketVo)
	{
		Long addedId = 0l;
		if(frTicketVo == null || frTicketVo.getTicketId() <= 0 )
			return addedId;
		
		LOGGER.debug("creating fr details..." +frTicketVo.getWorkOrderNumber());
		try{
			FrDetail dbvo = new FrDetail();
			dbvo.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());
			
			FrTicket ticket = hibernateTemplate.load(FrTicket.class, 
					frTicketVo.getTicketId());
			
			dbvo.setTicket(ticket);
			dbvo.setCategory(frTicketVo.getCategory());
			dbvo.setSubCategory(frTicketVo.getSubCategory());
			dbvo.setCurrentSymptomName(frTicketVo.getSubCategory());
			
			dbvo.setSymptomCode(FrTicketStatus.SYMPTOM_CODE);
			dbvo.setUpdatedSymptomCode(FrTicketStatus.SYMPTOM_CODE);
			
			dbvo.setNatureCode(frTicketVo.getNatureCode());
			dbvo.setLocked(true); 
			dbvo.setBlocked(false);
			
			dbvo.setAddedOn(new Date());
			dbvo.setModifiedOn(new Date());
			dbvo.setVoc(frTicketVo.getVoc());
			
			dbvo.setAddedBy(frTicketVo.getAddedBy());
			dbvo.setModifiedBy(frTicketVo.getModifiedBy());
			
			Device device = null;
			/*try {
				device = hibernateTemplate.get(Device.class, 
						frTicketVo.getDeviceId());
			} catch (Exception e) {
			}*/
			
			dbvo.setDevice(device);
			dbvo.setFlowId(frTicketVo.getFlowId());
			dbvo.setCurrentFlowId(frTicketVo.getFlowId());
			
			dbvo.setWorkOrderNumber(frTicketVo.getWorkOrderNumber());
			
			addedId = (Long)hibernateTemplate.save(dbvo);
			
			TicketLog ticketLog = new TicketLogImpl(dbvo.getId(),
					TicketUpdateConstant.WO_GENERATED, FWMPConstant.SYSTEM_ENGINE);
			ticketLog.setStatusId(TicketStatus.WO_GENERATED);
			ticketLog.setWoNumber(dbvo.getWorkOrderNumber());
			TicketUpdateGateway.logTicket(ticketLog);
			
		}catch(Exception e){
			LOGGER.error("Error occure while adding the FR Detail..",e);
		}
		LOGGER.debug(" Exit From creating fr details..." +frTicketVo.getWorkOrderNumber());
		return addedId;
	}
	
	public Map<Long, Boolean> createFrDetailInBulk( List<MQWorkorderVO> roiMQWorkorderVOList,
			Map<String, Long> completeProspectMap ) 
	{

		resultOfWorkOrderCreations.clear();
		if( roiMQWorkorderVOList == null || completeProspectMap == null)
			return resultOfWorkOrderCreations;
		
		Long addedId = 0l;
		Long ticketId = null;
		try
		{
			for (MQWorkorderVO workOrder : roiMQWorkorderVOList) {
				try {
					if (completeProspectMap.containsKey(workOrder.getWorkOrderNumber())) {
						if (completeProspectMap.get(workOrder.getWorkOrderNumber()) != null)
							ticketId = completeProspectMap.get(workOrder.getWorkOrderNumber());
						else
							ticketId = dbUtil.getFrTicketIdByWorkOrder(workOrder.getWorkOrderNumber());
					}

					if (ticketId == null) {
						LOGGER.info("completeProspectMap doesnt contain ticketid :: " + workOrder.getWorkOrderNumber());
						
						failedFrDetailsMap.put(workOrder.getWorkOrderNumber(), workOrder);
						completeProspectMapRetry.put(workOrder.getWorkOrderNumber(),completeProspectMap.get(workOrder.getWorkOrderNumber()));
						
						LOGGER.info("Adding failed fr ticket for retry " + failedFrDetailsMap + " and  " + completeProspectMapRetry);
						
						continue;
					}

					String natureCode = definitionCoreService.findFrNatureCodeBySymptomName(workOrder.getSymptom());
					Long flowId = FrTicketUtil.findFlowIdforMatchingSymptomName(workOrder.getSymptom());

					FrDetail dbvo = new FrDetail();
					dbvo.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());

					FrTicket ticket = hibernateTemplate.load(FrTicket.class, ticketId);

					dbvo.setTicket(ticket);

					dbvo.setCategory(workOrder.getTicketCategory());
					dbvo.setSubCategory(workOrder.getSymptom());
					dbvo.setCurrentSymptomName(workOrder.getSymptom());

					dbvo.setSymptomCode(FrTicketStatus.SYMPTOM_CODE);
					dbvo.setUpdatedSymptomCode(FrTicketStatus.SYMPTOM_CODE);

					dbvo.setNatureCode(natureCode);
					dbvo.setLocked(true);
					dbvo.setBlocked(false);

					dbvo.setAddedOn(new Date());
					dbvo.setModifiedOn(new Date());
					dbvo.setVoc(workOrder.getVoc());

					dbvo.setAddedBy(1l);
					dbvo.setModifiedBy(1l);

					Device device = null;
					/*
					 * try { device = hibernateTemplate.get(Device.class, frTicketVo.getDeviceId());
					 * } catch (Exception e) { }
					 */

					dbvo.setDevice(device);
					dbvo.setFlowId(flowId);
					dbvo.setCurrentFlowId(flowId);

					dbvo.setWorkOrderNumber(workOrder.getWorkOrderNumber());

					addedId = (Long) hibernateTemplate.save(dbvo);

					if (addedId > 0) {
						resultOfWorkOrderCreations.put(ticketId, true);
					} else {
						resultOfWorkOrderCreations.put(ticketId, false);
						LOGGER.info("Fr Details creation failed for workOrder : " + workOrder.getWorkOrderNumber());
					}
				} catch (Exception e) {
					LOGGER.error("Error occure while creating fr details for workOrder id : "
							+ workOrder.getWorkOrderNumber(), e);
					continue;
				}
			}
		}
		finally
		{
			hibernateTemplate.flush();
		//	hibernateTemplate.getSessionFactory().getCurrentSession().getTransaction().commit();
			
		}
		return resultOfWorkOrderCreations;
	
	}

	@Override
	public Integer deleteFrTicket(Long ticketId)
	{
		if( ticketId == null || ticketId.longValue() <= 0 )
		{
			LOGGER.info("Can't be delete fr ticket details for ticket id : "
					+ticketId);
			return 0;
		}
		LOGGER.info("Deleting fr ticket details for ticket id : "
				+ticketId);
		try{
			Query query = hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("delete from FrDetail where ticketId = :ticket")
				.setParameter("ticket", ticketId);
			
			int result = query.executeUpdate();
			if(result > 0)
				LOGGER.info("Fr ticket details deleted successfully for ticket id : "
						+ticketId);
			return result;
		}catch(Exception e){
			LOGGER.error("Error occure while deleting fr details for ticket id : "
					+ticketId,e);
			return 0;
		}
	}
	
	@Override
	public Integer updateFrTicketSymptom(Long flowId , String symptomName, Long ticketId)
	{
		LOGGER.info("Updating Fr Ticket Symptom...");
		Integer result = 0;
		if( ticketId == null || symptomName == null)
			return result;
		
		LOGGER.debug(" updateFrTicketSymptom..." +flowId + " " +symptomName + " " + ticketId );
		try{
			if( flowId == null )
				flowId = FrTicketUtil.findFlowIdforMatchingSymptomName(symptomName);
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrTicketDao.UPDATE_FR_SYMTOM)
					.setParameter("symptomName", symptomName)
					.setParameter("flowId", flowId)
					.setParameter("ticketId", ticketId);
			
			result = query.executeUpdate();
			if(result > 0)
				LOGGER.info("Fr symptom updated successfully for ticket... "
						+ticketId);
			return result;
			
		}catch(Exception exception){
			LOGGER.error("Error occure while updating fr details for ticket id : "+ticketId
					,exception);
		}
		LOGGER.debug("Exit From updateFrTicketSymptom..." +flowId + " " +symptomName + " " + ticketId );
		return result;
	}
	
	@Override
	public void saveOrUpdateFrTicket(FrTicketVo frTicketVo)
	{
		if( frTicketVo == null)
			return;
		
			LOGGER.debug(" saveOrUpdateFrTicket..." + frTicketVo.getTicketId());

		if(frTicketVo.getId() == null || frTicketVo.getId() <= 0)
			this.createFrTicket(frTicketVo);
		else{
			try
			{
				LOGGER.info("Updating fr ticket details...");
				
				FrDetail dbvo = new FrDetail();
				dbvo.setId(frTicketVo.getId());
				FrTicket ticket = hibernateTemplate.load(FrTicket.class,
						frTicketVo.getTicketId());
				
				dbvo.setTicket(ticket);
				dbvo.setCategory(frTicketVo.getCategory());
//				dbvo.setSubCategory(frTicketVo.getSubCategory());
				dbvo.setCurrentSymptomName(frTicketVo.getCurrentSymptomName());
				
				dbvo.setSymptomCode(frTicketVo.getSymptomCode());
				dbvo.setUpdatedSymptomCode(frTicketVo.getSymptomCode());
				dbvo.setNatureCode(frTicketVo.getNatureCode());
				
				dbvo.setAddedOn(FrTicketUtil
						.convertStringToDbDate(frTicketVo.getAddedOn()));
				dbvo.setAddedBy(frTicketVo.getAddedBy());
				
				dbvo.setModifiedOn(new Date());
				dbvo.setModifiedBy(frTicketVo.getModifiedBy());
//				dbvo.setStatus(frTicketVo.getStatus());
				dbvo.setSubCode(frTicketVo.getSubCode());
				dbvo.setCurrentProgress(frTicketVo.getCurrentProgress());
				dbvo.setVoc(frTicketVo.getVoc());
				
				Device device = hibernateTemplate.load(Device.class, 
						frTicketVo.getDeviceId());
				
				dbvo.setDevice(device);
//				dbvo.setFlowId(frTicketVo.getFlowId());
				
				dbvo.setCurrentFlowId(frTicketVo.getCurrentFlowId());
				
				
				hibernateTemplate.update(dbvo);
				
			}catch(Exception e){
				LOGGER.error("Error occure while updating Fr tickets...");
			}
		}
	}

	@Override
	public List<FrDetail> getAllFrTicket()
	{
		LOGGER.info("Getting All Fr Tickets...");
		List<FrDetail> list = null;
		try 
		{
			Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			list =(List<FrDetail>)
					session.createSQLQuery("SELECT * FROM FrDetail where id > 0")
					.addEntity(FrDetail.class).list();
			
			return list;
		} 
		catch (Exception e) 
		{
			LOGGER.error("Error occure while get all fr ticket...",e);
		}
		return list;
	}
	
	@Override
	public List<FrDetail> getAllUnassignedFrTicket()
	{
		LOGGER.info("Getting All Unassigned Fr Tickets...");
		final String ALL_FR_UNASSIGNED_TICKET = "SELECT fr.* FROM FrDetail fr INNER JOIN FrTicket tick "
				+ " ON tick.id = fr.ticketId WHERE tick.status = :unassigned ";
		List<FrDetail> list = null;		
		
		try 
		{
			Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			list =(List<FrDetail>)
					session.createSQLQuery(ALL_FR_UNASSIGNED_TICKET)
					.addEntity(FrDetail.class)
					.setParameter("unassigned", FRStatus.FR_UNASSIGNEDT_TICKET ).list();
			
			return list;
		} 
		catch (HibernateException e) 
		{
			
			LOGGER.error("Error occure while get all unassigned fr ticket...",e);
		}
		return list;
	}

	@Override
	public List<FrDetail> getFrTicketByUser(Long userId)
	{
		LOGGER.info("Getting Fr Ticket By User : "+userId);
		if( userId == null || userId.longValue() <= 0 )
			return null;
		
		Set<Long> ids = globalActivities.
				 getAllFrTicketFromMainQueueByKey(dbUtil
						 .getKeyForQueueByUserId(userId));
	
		List<FrDetail> list = null;
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrTicketDao.GET_FRTICKET_BYUSER)
					.addEntity(FrDetail.class)
					.setParameterList("ticketIds",ids);
			
			list = (List<FrDetail>)query.list();
		}catch(Exception e){
			LOGGER.error("Error while fetching FR Ticket for user : "+userId,e);
		}
		LOGGER.debug(" Exit From getFrTicketByUser..." +userId );
		return list;
	}

	@Override
	public boolean isFrTicketAvailabeWithTicketIdAndSymptom(Long ticketId,
			String symptom)
	{
		LOGGER.debug("Checking Fr Ticket available with Ticket id and Symptom...");
		if(symptom == null || ticketId == null 
				|| symptom.isEmpty() || ticketId.longValue() <= 0 )
			
			return false;
		
		try{
			List<FrDetail> frDetail = (List<FrDetail>)hibernateTemplate
					.find("from FrDetail fr where fr.ticket.id = ? and fr.subCategory = ?"
					,ticketId, symptom);
			if( frDetail != null && frDetail.size() > 0)
				return true;
			
		}catch(Exception e){
			LOGGER.error("Error occure while fetching FR Ticket available or not : ",e);
		}
		return false;
	}
	
	@Override
	public Integer updateFrTicketStatus(Long ticketId,List<Long> status)
	{
		LOGGER.info("Updating Fr Ticket status for Ticket..."+ticketId);
		
		Integer result = 0;
		if( ticketId == null || ticketId.longValue() <= 0 )
			return result ;
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT * from FrDetail where ticketId = :tickId")
					.addEntity(FrDetail.class)
					.setParameter("tickId", ticketId);
			
			List<FrDetail> frDetail = (List<FrDetail>)query.list();
			
			if( frDetail != null && frDetail.size() > 0)
			{
				FrDetail fr = frDetail.get(0);
				String statusList = fr.getStatus();
				if(statusList != null && statusList.trim().length() > 0)
				{	List<Long> dbStatus = convertStatusStringToLong(statusList.split(","));
					if(status != null && status.size() > 0)
					{
						statusList = "";
						dbStatus.addAll(status);
						Set<Long> dbStatusValue = new TreeSet<Long>(dbStatus);
						for(Long statusValue : dbStatusValue)
							statusList = statusList + statusValue+",";
				
						result = updateStatusList(statusList,ticketId);
					}
				}
				else
				{
					statusList = "";
					if(status != null && status.size() > 0)
					{
						Collections.sort(status);
						for(Long statusValue : status)
							statusList = statusList + statusValue+",";
						
						result = updateStatusList(statusList,ticketId);
					}
				}
			}
			else 
				LOGGER.info("Fr Ticket not fount with id : "+ticketId);
			
			if( result > 0)	
				LOGGER.info("Fr Ticket status is updated successfully...");
			
		}catch(Exception e){
			LOGGER.error("Error occure while updating fr ticket status...",e);
		}
		return result;
	}
	
	private List<Long> convertStatusStringToLong(String... status)
	{
		if(status != null)
			LOGGER.debug("Convert Status String To Long ..." );
		
		List<Long> values = new ArrayList<Long>();
		try{
			if(status != null)
				for( String value : status)
					if(value.trim().length() > 0)
						values.add(Long.valueOf(value));
		}catch(Exception exception){
			LOGGER.error("Exception occure while converting...",exception);
		}
		LOGGER.debug(" Exit From convertStatusStringToLong ..."  );
		return values;
	}
	
	@Override
	public Integer deleteCompletedStatus(Long ticketId,List<Long> status)
	{

		LOGGER.info("Deleting Status of Completed Ticket..."+ticketId);
		Integer result = 0;
		if( ticketId == null || ticketId.longValue() <= 0 )
			return result ;
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT * from FrDetail where ticketId = :tickId")
					.addEntity(FrDetail.class)
					.setParameter("tickId", ticketId);
			
			List<FrDetail> frDetail = (List<FrDetail>)query.list();
			
			if( frDetail != null && frDetail.size() > 0)
			{
				FrDetail fr = frDetail.get(0);
				String statusList = fr.getStatus();
				if( statusList != null && status != null && status.size() > 0)
				{
					Collections.sort(status);
					for(Long statusValue : status)
						statusList = statusList.replace(statusValue+",", "");
					
					result = updateStatusList(statusList,ticketId);
				}
			}
		}catch(Exception exception){
			LOGGER.error("Error occure while removing completed status...",exception);
		}
		return result;
	}
	
	private Integer updateStatusList(String list,Long ticketId)
	{
		Integer result = 0;

		if( ticketId == null || ticketId.longValue() <= 0 )
			return result;
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("UPDATE FrDetail SET statusList = :listvalue WHERE ticketId = :tickId")
					.setParameter("listvalue", list)
					.setParameter("tickId", ticketId);
	
			LOGGER.debug("Fr ticket update QUERY : "+query.getQueryString());
			result = query.executeUpdate();
			
		}catch(Exception exception){
			LOGGER.error("Error occure while removing status...",exception);
		}
		LOGGER.debug("Exit From  updateStatusList ..."+ list + " " + ticketId);
		return result;	
	}

	@Override
	public List<FrDetail> findFrTicketByFlowId(Long flowId)
	{

		LOGGER.info("Getting Fr Ticket by Flow ID : "+flowId);
		if( flowId == null || flowId < 0)
			return null;
		
		List<FrDetail> list = null;
		try{
			list = (List<FrDetail>)hibernateTemplate
					.find("from FrDetail fr where fr.flowId = ?", flowId);
			
			return list;
		}catch(Exception exception){
			LOGGER.error("Error occure while fetching FR Ticket for flow id : "+flowId,exception);
		}
		LOGGER.debug(" Exit From findFrTicketByFlowId ..."+ flowId  );
		return null;
	}

	@Override
	public List<FrDetail> findFrTicketByEngineer(Long userId)
	{
	   LOGGER.debug("  findFrTicketByEngineer ..."+ userId  );
		return this.getFrTicketByUser(userId);
	}

	@Override
	public Integer updateTicketLock(boolean isLocked,Long ticketId)
	{

		LOGGER.info("Updating Fr Ticket Lock status for Ticket : "+ticketId);
		Integer result = 0;
		if(ticketId == null || ticketId < 0)
			return result;
		
		try{
			
			final String UNLOCK_SQL = "UPDATE FrDetail fr INNER JOIN FrTicket tick ON fr.ticketId = tick.id "
					+ "SET nonLock = :isLocked WHERE ticketId = :tickId ";
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(UNLOCK_SQL)
					.setParameter("isLocked", isLocked)
					.setParameter("tickId", ticketId);
	
			LOGGER.info("Fr ticket update QUERY : "+query.getQueryString());
			result = query.executeUpdate();
			if(result > 0)
				LOGGER.info("Ticket Lock updated successfully...");
			
		}catch(Exception exception){
			LOGGER.error("Error occure while updating ticket lock : "+ticketId,exception);
		}
		LOGGER.debug(" Exit From updateTicketLock ..."+ ticketId  );
		return result;
	}
	
	@Override
	public Integer activateTicket(boolean ativateValue , Long userId, Long ticketId)
	{

		LOGGER.info("Unlocking Fr Ticket : "+ticketId+" for User : "+userId);
		if( userId == null || ticketId == null
				|| userId .longValue() <= 0 || ticketId.longValue() <= 0 )
			
			return 0;
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrTicketDao.UPDATE_ACTIVATION_FLAG)
					.setParameter("lockValue", ativateValue)
					.setParameter("ticketId", ticketId)
					.setParameter("userId", userId);
	
			LOGGER.info("Fr ticket update QUERY : "+query.getQueryString());
			return query.executeUpdate();
			
		}catch(Exception exception){
			LOGGER.error("Error occure while updating ticket lock : "
					+ticketId,exception);
			return 0;
		}
	}

	@Override
	public List<FRTicketCountSummaryVO> findFrTicketCounts(Long userId) 
	{
		LOGGER.info("findFrTicketCounts API called for User : "+userId);
		if( userId == null || userId.longValue() <= 0 )
			return null;
		
		StringBuilder completeQuery  = new StringBuilder(FrTicketDao.FR_TL_DASHBOARD_COUNT_QUERY);
		
		String queryString = completeQuery.toString() ;
		if(userHelper.isFRManager(userId))
		{
			queryString =	 queryString.replace("_FROM_PLACE_",FR_MANGER_DASHBOARD_FROM_QUERY);
		}else
		{
			queryString =  queryString.replace("_FROM_PLACE_",FR_TL_DASHBOARD_FROM_QUERY);
		}
		completeQuery = new StringBuilder(queryString);
		
		List<Object[]> list = null;
		
		try{
			
			String key = dbUtil.getKeyForQueueByUserId(userId);
			Set<Long> ticketListForWorkingQueue = globalActivities
					.getAllFrTicketFromMainQueueByKey(key); 
			
			TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
		
			if(userHelper.isFRNEUser(userId))
			{
				completeQuery.append("AND tick.id in (:ticketIds)");
			}
			if(userHelper.isFRTLUser(userId))
			{
				completeQuery.append("AND tick.id in (:ticketIds)");
			}
			if(userHelper.isFRAreaManager(userId))
			{
				completeQuery.append(" and  tick.currentAssignedTo in(:reportingUsers) ");
			}
			
			if(userHelper.isFRBranchManager(userId))
			{
				completeQuery.append(" and ( c.branchId  in (:branchIds) or tick.currentAssignedTo in(:reportingUsers))  ");
			}

			if(userHelper.isFRCityManager(userId))
			{
				completeQuery.append(" and  c.cityId  in (:cityIds) ");
			}
			
			completeQuery.append(" ORDER BY fr.nonLock asc ,tick.status desc ");
		    Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(completeQuery.toString());
		   
		    LOGGER.info("FR DASHBOARD COUNT QUERY   : "+query.getQueryString());
			LOGGER.debug("FR DASHBOARD NE TL TICKETS IDS : "+ticketListForWorkingQueue);
			
		    if( ticketListForWorkingQueue == null && !AuthUtils.isManager())
		    	return null;
			if(userHelper.isFRNEUser(userId))
			{
				 if (ticketListForWorkingQueue != null && ticketListForWorkingQueue.size() > 0  )
						query.setParameterList("ticketIds", ticketListForWorkingQueue); 
				 else
					 query.setParameterList("ticketIds", Arrays.asList(new Long[]{-1l})); 
			}
			if(userHelper.isFRTLUser(userId))
			{
				 if (ticketListForWorkingQueue != null && ticketListForWorkingQueue.size() > 0  )
						query.setParameterList("ticketIds", ticketListForWorkingQueue); 
				 else
					 query.setParameterList("ticketIds", Arrays.asList(new Long[]{-1l})); 
			}
			if(userHelper.isFRAreaManager(userId))
			{
				Map<Long, String> reportingUsers = null;
				if(filter != null)
				{
					reportingUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(),null,null, null);
					reportingUsers.put(filter.getAssignedByUserId(),null);
				}
				else
				{
					reportingUsers = userDao.getNestedReportingUsers(AuthUtils.getCurrentUserId(),null,null, null);
					reportingUsers.put(AuthUtils.getCurrentUserId(),null);
				}
				if(reportingUsers == null || reportingUsers.size() == 0)
				{
					query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
				}
				else
				{
					reportingUsers.put(AuthUtils.getCurrentUserId(),null);
					query.setParameterList("reportingUsers", reportingUsers.keySet());
				}
			}
			
			if(userHelper.isFRBranchManager(userId))
			{
				Map<Long, String> reportingUsers = null;
				UserVo userVo  = null;
				if(filter != null)
				{
					reportingUsers = userDao.getNestedReportingUsers(filter.getAssignedByUserId(),null,null, null);
					if(filter.getAssignedByUserId() != null)
						reportingUsers.put(filter.getAssignedByUserId(),null);
					userVo = userDao.getUserById(filter.getAssignedByUserId());
				}
				else
				{
					reportingUsers = userDao.getNestedReportingUsers(AuthUtils.getCurrentUserId(),null,null, null);
					reportingUsers.put(AuthUtils.getCurrentUserId(),null);
					userVo = userDao.getUserById(AuthUtils.getCurrentUserId());
				}
				if(AuthUtils.getCurrentUserId() > 0)
					reportingUsers.put(AuthUtils.getCurrentUserId(),null);
				Set<TypeAheadVo> branchTypeHeadVo = userCache
						.getUserBranchByLoginId(userVo.getLoginId());
				List<Long> branches = new ArrayList<Long>();
				for (TypeAheadVo typeAheadVo : branchTypeHeadVo)
				{
					branches.add(typeAheadVo.getId());
				}
				LOGGER.debug("branches list size ::" + branches.size());
				query.setParameterList("branchIds", branches);

				if (reportingUsers == null
						|| reportingUsers.size() == 0)
				{
					query.setParameterList("reportingUsers", Arrays
							.asList(new Long[] { -1l }));
				} else
				{
					query.setParameterList("reportingUsers", reportingUsers
							.keySet());
				}
			}

			if(userHelper.isFRCityManager(userId))
			{
				query.setParameterList("cityIds",Collections.singletonList(AuthUtils.getCurrentUserCity().getId() ));
			}
			list = query.list();
			
		}catch(Exception exception){
			LOGGER.error("Error occure while getting count info for count...",exception);
			return null;
		}
		
		LOGGER.debug("Exitting from findFrTicketCounts for User : "+userId);
		return xformToFrCountSummaryVo(list);
	}
	
	private List<FRTicketCountSummaryVO> xformToFrCountSummaryVo(List<Object[]> list)
	{
		if( list == null || list.isEmpty())
			return null;
			
        LOGGER.debug("  xformToFrCountSummaryVo ..." +list.size()  );
		
		List<FRTicketCountSummaryVO> summaryVoList = new ArrayList<FRTicketCountSummaryVO>();
		FRTicketCountSummaryVO summaryVo = null;
		for(Object[] row : list)
		{
			summaryVo = new FRTicketCountSummaryVO();
			
			summaryVo.setUserName(FrTicketUtil.objectToString(row[0]));
			summaryVo.setUserId(FrTicketUtil.objectToLong(row[1]));
			summaryVo.setTicketId(FrTicketUtil.objectToLong(row[2]));
			summaryVo.setStatusList(FrTicketUtil.
					convertStatusStringToLong(FrTicketUtil.objectToString(row[3])));
			
			summaryVo.setCurrentflowId(FrTicketUtil.objectToLong(row[4]));
			summaryVo.setCurrentSymptomName(FrTicketUtil.objectToString(row[5]));
			summaryVo.setLocked(FrTicketUtil.objectToBoolean(row[6]));
			summaryVo.setCurrentEtr(FrTicketUtil.convertObjectToDate(row[7]));
			
			summaryVo.setCommitedEtr(FrTicketUtil.convertObjectToDate(row[8]));
			summaryVo.setTicketStatus(FrTicketUtil.objectToInt(row[9]));
			summaryVo.setPriorityValue(FrTicketUtil.objectToInt(row[10]));
			summaryVo.setEscalatedValue(FrTicketUtil.objectToInt(row[11]));
			summaryVo.setUpdatedCount(FrTicketUtil.objectToInt(row[12]));
			summaryVo.setTicketCreationDate(FrTicketUtil.convertObjectToDate(row[14]));
			summaryVo.setTicketClosedDate(FrTicketUtil.convertObjectToDate(row[15]));
			summaryVo.setReopenTicket(FrTicketUtil.objectToInt(row[16]));
			
			summaryVoList.add(summaryVo);
		}
		LOGGER.debug("  Exit From xformToFrCountSummaryVo ..." +list.size()  );
		return summaryVoList;
	}

	@Override
	public Integer updateFlowId(Long ticketId , String symptomName , Long flowId, Long userId ,Integer flowChangedDownToUp)
	{
		LOGGER.info("Updating flow Id for Fr Ticket : "+ticketId +" Assigned To user : "+userId);
		Integer result = 0;
		
		if( ticketId == null || flowId == null || userId == null 
				|| userId <= 0 || flowChangedDownToUp == null)
			
			return result;
		
		try
		{

			/*
			 * if( userId == null && symptomName == null) return result; else
			 * if( userId == null && symptomName != null ) return
			 * this.updateFrTicketSymptom(flowId, symptomName, ticketId); else
			 * if( userId != null && symptomName == null) symptomName =
			 * "Not specified";
			 */
		
			LOGGER.info("ticket id " + ticketId + " symptomName " + symptomName
					+ " flowId " + flowId + " userId " + userId +" ,,,, "+flowChangedDownToUp);
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(UPDATE_FR_FLOWID)
					.setParameter("symptomName", symptomName)
					.setParameter("flowId", flowId)
					.setParameter("currentAssigned", userId)
					.setParameter("status", flowChangedDownToUp)
					.setParameter("modifiedBY", AuthUtils.getCurrentUserId())
					.setParameter("ticketId", ticketId);
			
			LOGGER.info("Query "+query.getQueryString());
			result = query.executeUpdate();
			
			if (result > 0)
				LOGGER.info("Fr flow id updated successfully for ticket... "
						+ ticketId);

			return result;

		}catch(Exception exception){
			LOGGER.error("Error occure while updating flow id for ticket : "
					+ticketId,exception);
		}
		LOGGER.debug(" Exit From updateFlowId ..." +ticketId + " " + flowId   );
		return result;
	}
	
	@Override
	public BigInteger getUnlockedTicketCount(Long userId)
	{
		LOGGER.info("Getting count of Unlocked Fr Tickets for User : "+userId);
		if( userId == null || userId.longValue() <= 0 )
		{
			LOGGER.info("Can't get count with user id : "+userId);
			return null;
		}
		try{
			List list = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(FrTicketDao.FR_UNLOCKED_TICKET_COUNT)
					.setParameter("userId",userId)
					.setParameterList("statusList",TicketStatus.COMPLETED_DEF ).list();
			
			if( list != null && list.size() > 0)
				return (BigInteger)list.get(0);
		}catch(Exception exception){
			LOGGER.error("Error occure while getting unlocked ticket count for user : "
					+userId,exception);
		}
		
		LOGGER.debug(" Exit from Getting count of Unlocked Fr Tickets for User : "+userId);
		return null;
	}
	
	@Override
	public Integer getUnlockedTicketWithUnblockedCount( Long userId )
	{
		LOGGER.debug("Getting count of unlocked Ticke and unblocked Ticket for User : "+userId);
		if( userId == null || userId.longValue() <= 0 )
		{
			LOGGER.info("Can't get count with user id : "+userId);
			return null;
		}
		try{
			StringBuilder completeQuery  = new StringBuilder(FrTicketDao.FR_UNLOCKED_TICKET_COUNT);
			completeQuery.append(" AND nonBlocked = 0 ");
			
			List list = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(completeQuery.toString())
					.setParameter("userId",userId)
					.setParameterList("statusList",TicketStatus.COMPLETED_DEF ).list();
			
			if( list != null && list.size() > 0)
				return ((BigInteger)list.get(0)).intValue();
		}catch(Exception exception){
			LOGGER.error("Error occure while getting unlocked with unblocked ticket count for user : "
					+userId,exception);
		}
		LOGGER.debug(" Exit From getUnlockedTicketCount : " + userId);
		return null;
	}

	@Override
	public List<FrTicketTlneDetailVo> findFrTicketTlneCounts(Long reportTo)
	{

		LOGGER.info("Getting count for NE of report To : "+ reportTo);
		if( reportTo == null || reportTo.longValue() <= 0 )
			return null;
		
		List<Object[]> listObject = null;
		StringBuilder completeQuery  = new StringBuilder(FrTicketDao.FR_ENGINEER_SQL_QUERY);
		
		boolean isNeUser = false;
		TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
		try{
			Set<Long> ticketListForWorkingQueue = null;
			if( AuthUtils.isFrCxDownNEUser(reportTo) || AuthUtils.isFrCxUpNEUser(reportTo)
					|| AuthUtils.isSRNEUser(reportTo))
			{
				ticketListForWorkingQueue = globalActivities
						.getAllFrTicketFromMainQueueByKey(dbUtil.getKeyForQueueByUserId(reportTo)); 
				
				completeQuery.append(" AND tick.id in (:ticketIds)");
				
				if( ticketListForWorkingQueue != null && ticketListForWorkingQueue.size() > 0)
					isNeUser = true;
				else
				{
					ticketListForWorkingQueue= new HashSet<>(Arrays.asList(0l));
					isNeUser = true;
				}
			}
			else
			{
				completeQuery.append(" AND usr.reportTo = :reportTo");
				
			}
				
			LOGGER.info("FR NE Details : " +completeQuery.toString());
			Query query = hibernateTemplate.getSessionFactory()
								.getCurrentSession().createSQLQuery(completeQuery.toString());
			if( isNeUser )	
				query.setParameterList("ticketIds", ticketListForWorkingQueue);
			else 
			{
				query.setParameter("reportTo", reportTo);
				
			}
				
			
			listObject = query.list();
			
		}catch(Exception exception){
			LOGGER.error("Error occure while fetching tl ne details for user : "
					+reportTo,exception);
		}
		LOGGER.debug(" Exit From findFrTicketTlneCounts : " + reportTo);
		return xformToValueObject(listObject);
	}
	
	private  List<FrTicketTlneDetailVo> xformToValueObject(List<Object[]> listObject)
	{
		if( listObject == null || listObject.isEmpty())
			return null;
		LOGGER.debug(" xformToValueObject : " + listObject.size());
		 List<FrTicketTlneDetailVo> valueObject = new ArrayList<FrTicketTlneDetailVo>();
		 FrTicketTlneDetailVo neDetailVo = null;
		 
		 for( Object[] dbValue : listObject)
		 {
			 neDetailVo = new FrTicketTlneDetailVo();
			 neDetailVo.setUserId(FrTicketUtil.objectToLong(dbValue[0]));
			 neDetailVo.setUserName(FrTicketUtil.objectToString(dbValue[1]));
			 neDetailVo.setLastName(FrTicketUtil.objectToString(dbValue[2]));
			 
			 neDetailVo.setTicketId(FrTicketUtil.objectToLong(dbValue[3]));
			 neDetailVo.setUserGroupCode(FrTicketUtil.objectToString(dbValue[4]));
			 neDetailVo.setCurrentFlowId(FrTicketUtil.objectToLong(dbValue[5]));
			 neDetailVo.setCurrentSymptomName(FrTicketUtil.objectToString(dbValue[6]));
			 neDetailVo.setTicketStatus(FrTicketUtil.objectToInt(dbValue[7]));
			 neDetailVo.setRequestedBy(FrTicketUtil.objectToLong(dbValue[8]));
			 neDetailVo.setVendorTicketStatus(FrTicketUtil.objectToInt(dbValue[9]));
			 neDetailVo.setVendorTicketId(FrTicketUtil.objectToLong(dbValue[10]));
			 neDetailVo.setUserType(FrTicketUtil.objectToLong(dbValue[11]));
			 
			 neDetailVo.setOutStatus(FrTicketUtil.objectToInt(dbValue[12]));
			 
			 valueObject.add(neDetailVo);
		 }
		LOGGER.debug("Exit From xformToValueObject : " + listObject.size());
		return valueObject;
	}
	
	public List<FRSummaryCountVO> getVendorTicketSummary()
	{
		LOGGER.info("Getting getVendorTicketSummary ...");
		List<Object[]> listObject = null;
		StringBuilder completeQuery  = new StringBuilder(FR_VENDOR_COUNT_QUERY);
		Query query = hibernateTemplate.getSessionFactory()
				.getCurrentSession().createSQLQuery(completeQuery.toString());
		query.setParameter("vendorId", AuthUtils.getCurrentUserId());
		query.setParameterList("pendingStatusList", ActionTypeAttributeDefinitions.VENDOR_TASK_DONE_STATUS );
		query.setParameter("afpVendorClosedTicketInBucketHours", afpVendorClosedTicketInBucketHours);
		
		Map<String, FRSummaryCountVO> mapData = new HashMap<String, FRSummaryCountVO>();
	
		mapData.put( TOTAL , new FRSummaryCountVO( TOTAL , 1l, new ArrayList<Long>(), 0));
		mapData.put( PENDING , new FRSummaryCountVO( PENDING , 2l, new ArrayList<Long>(), 0));
		mapData.put( CLOSED , new FRSummaryCountVO( CLOSED , 3l, new ArrayList<Long>(), 0));
		
		listObject = query.list();
		if(listObject != null)
		{
			
			for(Object[] obj :listObject )
			{
				mapData.get(TOTAL).getTicketIds().add(FrTicketUtil.objectToLong(obj[0]));
				mapData.get(TOTAL).setPendingCounts(mapData.get(TOTAL).getPendingCounts()+1);
				
				if(ActionTypeAttributeDefinitions.VENDOR_TASK_DONE_STATUS.contains(FrTicketUtil.objectToLong(obj[2])))
				{
					mapData.get(CLOSED).getTicketIds().add(FrTicketUtil.objectToLong(obj[0]));
					mapData.get(CLOSED).setPendingCounts(mapData.get(CLOSED).getPendingCounts()+1);
					
				}
				else
				{
					mapData.get(PENDING).getTicketIds().add(FrTicketUtil.objectToLong(obj[0]));
					mapData.get(PENDING).setPendingCounts(mapData.get(PENDING).getPendingCounts()+1);
			
				}
				
				
			}
		}
		LOGGER.debug(" Exit from Getting getVendorTicketSummary ...");
		return new CopyOnWriteArrayList<FRSummaryCountVO>(mapData.values()) ;

	}

	public int getAfpVendorClosedTicketInBucketHours()
	{
		return afpVendorClosedTicketInBucketHours;
	}

	public void setAfpVendorClosedTicketInBucketHours(
			int afpVendorClosedTicketInBucketHours)
	{
		this.afpVendorClosedTicketInBucketHours = afpVendorClosedTicketInBucketHours;
	}

	@Override
	public int updateTicketStatusToInProgress(Long ticketId , Integer status)
	{
		LOGGER.info("Updating In progress status for Ticket : "+ticketId);
		int result = 0;
		if(ticketId > 0 && status > 0)
		{
			try{
				Query query = hibernateTemplate.getSessionFactory()
						.getCurrentSession().createSQLQuery(UPDATE_TICKET_STATUS)
						.setParameter("status", status)
						.setParameter("ticketId", ticketId);
			
				result = query.executeUpdate();
				if(result > 0 )
					LOGGER.info("Ticket status is updated successfully for ticket : "+ticketId);
				return result;
			}catch(Exception exception){
				LOGGER.error("Error occure while updating ticket status  for ticket : "
						+ticketId,exception);
			}
			
				
		}
		return 0;
	}

	@Override
	public Integer updateBlockStatus(long ticketId , boolean isBlocked)
	{
		LOGGER.info("Updating Bock status for Ticket : "+ticketId);
		if( ticketId <= 0 )
			return 0;
		try{
			
			final String UNLOCK_SQL = "UPDATE FrDetail fr INNER JOIN FrTicket tick ON fr.ticketId = tick.id "
					+ "SET nonBlocked = :blocked WHERE ticketId = :tickId ";
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(UNLOCK_SQL)
					.setParameter("blocked",isBlocked)
					.setParameter("tickId", ticketId);
			
			int updated = query.executeUpdate();
			if( updated > 0 )
			{
				LOGGER.info("Fr ticket : "+ticketId+" blocked status updated successfully...");
				return updated;
			}
			
		}catch(Exception exception){
			LOGGER.error("Error occure while updating block status  for ticket : "
					+ticketId+exception.getMessage());
		}
		return 0;
	}

	@Override
	public List<Long> getNextNLockedFrTicket(Long userId , Integer numberOfTicket , List<Long> ignoreTicket) 
	{
		LOGGER.debug("Getting N Locked Ticket for User : "+userId);
		if( userId == null || userId.longValue() <= 0 )
		{
			LOGGER.info("Can't get locked ticket of User id : "+userId);
			return null;
		}
		
		StringBuilder completeQuery  = null;
		if( ignoreTicket != null && ignoreTicket.size() > 0 )
			completeQuery = new StringBuilder(FrTicketDao.GET_NEXT_LOCKED_TICKET_WITH_IGNORE);
		else
			completeQuery = new StringBuilder(FrTicketDao.GET_NEXT_LOCKED_TICKET);
		
		if( numberOfTicket == null || numberOfTicket <= 0)
			numberOfTicket = 1;
		
		completeQuery.append(" LIMIT "+numberOfTicket);
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(completeQuery.toString())
					.setParameter("userId",userId)
					.setParameterList("ticketStatusList", TicketStatus.COMPLETED_DEF);
					
			if(  ignoreTicket != null && ignoreTicket.size() > 0 )
				query.setParameterList("ticketList", ignoreTicket);
			
			LOGGER.debug("NEXT N LOCKED TICKET QUERY :"+query.getQueryString());
			
			return objectToListLong(query.list());
			
		}catch(Exception exception){
			LOGGER.error("Error occure while fetching next locked ticket for User id : "
					+userId,exception);
		}

		LOGGER.debug(" Exit from Getting N Locked Ticket for User : "+userId);

		return null;
	
	}
	
	private List<Long> objectToListLong(List<BigInteger> object)
	{
//		System.out.println("LIST OF TICKE CONVERTIN BIGINT TO LONG :"+object);
		List<Long> list = new ArrayList<Long>();
		if( object == null || object.isEmpty() )
		return list;
		
		for( BigInteger obj : object)
		{
			if( obj.longValue() == 0 )
				continue;
			list.add(obj.longValue());
		}
		return list;
	}
	
	@Override
	public Integer activateUserFrTickets(Long userId, List<Long> ticketIds)
	{
		LOGGER.info("Unlocking List of Tickets : "+ticketIds+" For User : "+userId);
		if( userId == null || ticketIds == null 
				|| userId.longValue() <= 0 || ticketIds.isEmpty() )
		{
			LOGGER.info("Can't activate no of tickets :"+ticketIds+" for User id :"+userId );
			return 0;
		}
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrTicketDao.ACTIVATE_N_LOCKED_FR_TICKET)
					.setParameter("lockFlag", false)
					.setParameter("userId", userId)
					.setParameterList("ticketIds", ticketIds);
	
			LOGGER.info("Fr ticket update QUERY : "+query.getQueryString());
			return query.executeUpdate();
			
		}catch(Exception exception){
			LOGGER.error("Error occure while updating tickets lock : "
					+ticketIds,exception);
			return 0;
		}
	}
	
	@Override
	public Integer activateNextLockedFrTicket( Long userId ,int noOfTicketToBeActivate, List<Long> ignoreTickets )
	{
	   LOGGER.debug("activateNextLockedFrTicket : "+userId);
		List<Long> lockedId = getNextNLockedFrTicket( userId ,noOfTicketToBeActivate,ignoreTickets);
		if( lockedId == null || lockedId.isEmpty() )
		{
			LOGGER.debug("Can't unlocked tickets : "+lockedId +"For User : "+userId);
			return 0;
		}

		
		LOGGER.debug(" Exit from activateNextLockedFrTicket : "+userId);
		
		return activateUserFrTickets(userId, lockedId );
	}

	@Override
	public FrDetail findFrTicketDetailByWorkOrder( String workOrder )
	{

		LOGGER.info("Getting Fr Ticket detail by work order number : "+workOrder);
		if( workOrder == null || workOrder.isEmpty() )
			return null;
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrTicketDao.GET_TICKET_BY_WORKORDER_DETAIL_SQL)
					.addEntity(FrDetail.class)
					.setParameter("workOrder", workOrder);
			
			List<FrDetail> detail = query.list();
			if( detail != null && !detail.isEmpty() )
				return detail.get(0);
			
		}
		catch (Exception exception) 
		{
			LOGGER.error("Error occure while fetching ticket detail by workorder : "
					+exception.getMessage());
		}

		LOGGER.debug(" Exit from Getting Fr Ticket detail by work order number : "+workOrder);
		return null;
	}
	
	@Override
	public FrTicket getCloseFrTicketByWorkOrder( String workOrder )
	{

		LOGGER.info("Getting closed fr Ticke by workOrder number  : "+workOrder);
		if( workOrder == null || workOrder.isEmpty() )
			return null;
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FrTicketDao.GET_CLOSED_FRTICKET_FOR_REOPEN_SQL)
					.addEntity(FrTicket.class)
					.setParameter("workOrder", workOrder)
					.setParameterList("statusList", Arrays.asList(FRStatus.SUCCESSFUL_CLOSED_IN_MQ));
			
			List<FrTicket> detail = query.list();
			if( detail != null && !detail.isEmpty() )
				return detail.get(0);
			
		}
		catch (Exception exception) 
		{
			LOGGER.error("Error occure while fetching ticket detail by workorder : "
					+exception.getMessage());
		}
		LOGGER.debug(" Exit from Getting closed fr Ticke by workOrder number  : "+workOrder);
		return null;
	}
	
	@Override
	public void updateReopneTicket(FrTicket ticket) 
	{
		try {
			if(ticket == null)
				return;
			
			LOGGER.debug(" inside updateReopneTicket  :: " +ticket.getId());
			if ( ticket != null && ticket.getId() > 0)
			{
				hibernateTemplate.update(ticket);
				hibernateTemplate.flush();
			}
			LOGGER.debug(" exit from updateReopneTicket  :: " +ticket.getId());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			hibernateTemplate.flush();
		}
		LOGGER.debug(" exit from updateReopneTicket  method :: " +ticket.getId());
	}
	
	@Override
	public void updateFrTicketEtr(FrTicketEtr ticketEtr) 
	{
		try {
			if(ticketEtr == null)
				return;
			
			LOGGER.debug(" inside updateFrTicketEtr  :: " +ticketEtr.getId());
			if ( ticketEtr != null && ticketEtr.getId() > 0)
			{
				hibernateTemplate.update(ticketEtr);
				hibernateTemplate.flush();
			}
			LOGGER.debug(" exit from updateFrTicketEtr  :: " +ticketEtr.getId());
		} catch (DataAccessException e) {
			e.printStackTrace();
		} finally {
			hibernateTemplate.flush();
		}
		LOGGER.debug(" exit from updateFrTicketEtr  method :: " +ticketEtr.getId());
	}

	@Override
	public void reopenWorkOrder(FrDetail frTicket)
	{
		try {
			if ( frTicket == null || frTicket.getId() <= 0 )
				return ;
			
			LOGGER.info(" inside reopenWorkOrder  :: " +frTicket.getWorkOrderNumber());
			try{
				hibernateTemplate.update(frTicket);
			}catch(Exception exception){
				LOGGER.error("Error occure while reopening workorder : "
						+frTicket.getWorkOrderNumber());
			}
			LOGGER.debug(" exit from reopenWorkOrder  :: " +frTicket.getWorkOrderNumber());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			hibernateTemplate.flush();
		}
		LOGGER.debug(" exit from reopenWorkOrder  :: " +frTicket.getWorkOrderNumber());
	
	}
	
	@Override
	public FrTicket getFrTicketByWorkOrder(String workOrder)
	{
		LOGGER.info("Getting Fr Ticket by work order ..."+	workOrder);
		if( workOrder == null || workOrder.isEmpty())
			return null;

		/*List<FrTicket> ticket = (List<FrTicket>) hibernateTemplate
				.find("from FrTicket fr where fr.workOrderNumber=" + workOrder);
		if (ticket != null && ticket.size() > 0)
			return ticket.get(0);*/
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT fr.* FROM FrTicket fr WHERE fr.workOrderNumber = :workOrder")
					.addEntity(FrTicket.class)
					.setParameter("workOrder", workOrder);
			
			List<FrTicket> ticket = query.list();
			if (ticket != null && ticket.size() > 0)
				return ticket.get(0);
			
		}catch(Exception e){
			LOGGER.error("Error occure while fetching FrTicket by workorder :" +workOrder, e);
		}
		

		LOGGER.debug(" exit from Getting Fr Ticket by work order ..."+	workOrder);

		return null;
	}
	
	/*@Override 
	public Integer blockedTicketActivateNext(Long userId , Long ticketId )
	{
		if( userId == null || ticketId == null )
		{
			LOGGER.info("Can't blocked tickets : "+ticketId);
			return 0;
		}
		
		List<Long> lockedId = getNextNLockedFrTicket( userId ,1);
		return 0;
	}*/
	
	@Override
	public List<BigInteger> getCurrentAssignedForTickets(List<Long> ticketIds)
	{
		LOGGER.info("Getting current assigned user for Tickets : "+ticketIds);
		if( ticketIds == null || ticketIds.isEmpty() )
			return null;
		
		LOGGER.debug("Getting current assigned user for Tickets : "+ticketIds.size());
		List<BigInteger> currentAssignToLsit = null;
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Select currentAssignedTo from FrTicket where id in (:ids)")
						.setParameterList("ids", ticketIds);
			currentAssignToLsit = query.list();
		} 
		catch (Exception e)
		{
			LOGGER.error("currentAssignedForTickets" + e);

		}
		LOGGER.debug("Getting current assigned user for Tickets : "+ticketIds.size());
		return currentAssignToLsit;
	}
	
	@Override
	public List<TicketReassignmentLogVo> getCurrentAssignedForTicketsWithIds(List<Long> ticketIds)
	{
		LOGGER.info("Getting current assigned user for Tickets : "+ticketIds);
		if( ticketIds == null || ticketIds.isEmpty() )
			return null;
		
		List<Object[]> currentAssignToLsit = null;
		List<TicketReassignmentLogVo> result = new ArrayList<>();
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Select id,currentAssignedTo from FrTicket where id in (:ids)")
						.setParameterList("ids", ticketIds);
			currentAssignToLsit = query.list();
			
			TicketReassignmentLogVo ticketReassignmentLogVo = null;
			for (Object[] obj : currentAssignToLsit)
			{
				ticketReassignmentLogVo = new TicketReassignmentLogVo();
				ticketReassignmentLogVo.setTicketId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null)
					ticketReassignmentLogVo.setAssignedFrom(Long.valueOf(obj[1].toString()));
				result.add(ticketReassignmentLogVo);
			}
			
		} 
		catch (Exception e)
		{
			LOGGER.error("currentAssignedForTickets" + e);
			e.printStackTrace();
		}

		return result;
	}
	
	@Override
	public void assignFrTickets(AssignVO assignVO, Long areaId)
	{


		LOGGER.info("Assignment API is called...");
		if ( assignVO == null || assignVO.getTicketId()== null 
				|| assignVO.getTicketId().size() <= 0 )
		{
			LOGGER.info("Can't assigned FR Ticket with data : " +assignVO );
			
			return ;
		}
		try
		{
			List<Long> tickets = assignVO.getTicketId();

			if (tickets != null)
			{

				LOGGER.debug("Fr Ticket ids found : " + tickets.size());
				for (Long id : tickets) 
				{

					FrTicket ticket = hibernateTemplate.get(FrTicket.class, id);

					if (ticket == null)
						continue;

					if (ticket.getStatus() != null
							&& FRStatus.FR_UNASSIGNEDT_TICKET.equals(ticket.getStatus()) )
					{
						ticket.setStatus(FRStatus.FR_WORKORDER_GENERATED);
						ticket.setUserByAssignedTo(hibernateTemplate.load(User.class,
								assignVO.getAssignedToId()));
					}
					else if ( ticket.getStatus() != null && TicketStatus.OPEN == ticket.getStatus() )
					{
						ticket.setStatus(FRStatus.FR_WORKORDER_GENERATED);
					}
					else if( ticket.getStatus() != null && FRStatus.FR_PUSH_BACK_TICKET.equals(ticket.getStatus()) )
					{
						ticket.setStatus(FRStatus.FR_WORKORDER_GENERATED );
						ticket.setReason(null);
						ticket.setRemarks(null);
					}
					else if( ticket.getStatus() != null 
							&& ticket.getStatus().equals(FRStatus.FLOW_CHANGED_DOWN_TO_UP) )
					{
						if( userHelper.isFRNEUser(assignVO.getAssignedToId()))
						{
							ticket.setStatus(FRStatus.FR_WORKORDER_GENERATED);
							ScratchPad.delectContextDataInScratchPad(id, ControlBlockStatus.CONTEXT_TYPE);
						}
					}
					
					ticket.setPushBackCount(0);
					ticket.setModifiedBy(assignVO.getAssignedById());
					ticket.setModifiedOn(new Date());

					ticket.setUserByCurrentAssignedTo(hibernateTemplate.load(
							User.class, assignVO.getAssignedToId()));
					
					ticket.setUserByAssignedBy(hibernateTemplate.load(
							User.class, assignVO.getAssignedById()));

					if (areaId != null && areaId > 0) 
					{
						Area area = hibernateTemplate.get(Area.class, areaId);

						ticket.setArea(area);

						Customer c = ticket.getCustomer();

						c.setBranch(area.getBranch());
						hibernateTemplate.update(c);
					}

					hibernateTemplate.update(ticket);
					LOGGER.info("Fr Ticket with id " + id + " is assigned to "
							+ assignVO.getAssignedToId());

				}
				hibernateTemplate.flush();
			}
		} 
		catch (Exception e) {
			LOGGER.error("Error occured while re-assigning Fr Ticke to User :",e);
			e.printStackTrace();

		}
	
	}

	@Override
	public String updateFrTicket(StatusVO status)
	{
		LOGGER.info("Updating Fr Ticket Status have vo object ...");
		String statusMsg = null;
		if( status == null )
			return statusMsg;
		
		LOGGER.debug("Updating Fr Ticket Status have vo object ..." +status.getPropspectNo());
		try
		{

			if (status.getTicketList() != null
					&& status.getTicketList().size() > 0)
			{
				int result = hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Update  FrTicket t set t.status=:status,  t.remarks=:remark, t.reason=:reason ,t.modifiedOn=:modifiedOn where t.id in (:ids)")
						.setParameter("status", status.getStatus())
						.setParameter("reason", status.getReasonCode() + "")
						.setParameter("remark", GenericUtil.formatRemarks(status
								.getRemarks(), status.getRemarks(), status
								.getAddedByUserName()))
						.setParameter("modifiedOn", new Date())
						.setParameterList("ids", status.getTicketList())
						.executeUpdate();

				return StatusMessages.UPDATED_SUCCESSFULLY;

			} 
			else if (status.getPropspectNoList() != null
					&& status.getPropspectNoList().size() > 0)
			{

				int result = hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Update  FrTicket t set t.status=:status,  t.remarks=:remark, t.reason=:reason ,t.modifiedOn=:modifiedOn where t.prospectNo in (:prospectNos)")
						.setParameter("status", status.getStatus())
						.setParameter("reason", status.getReasonCode() + "")
						.setParameter("remark", GenericUtil.formatRemarks(status
								.getRemarks(), status.getRemarks(), status
								.getAddedByUserName()))
						.setParameter("modifiedOn", new Date())
						.setParameterList("prospectNos", status.getPropspectNoList())
						.executeUpdate();

				return StatusMessages.UPDATED_SUCCESSFULLY;

			} 
			else if ( status.getWorkOrderNumber() != null )
			{

				int result = hibernateTemplate
						.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery("Update  FrTicket t set t.status=:status,  t.remarks=:remark  where t.workOrderNumber=:prospectNos")
						.setParameter("status", status.getStatus())
						.setParameter("remark", GenericUtil.formatRemarks(status
								.getRemarks(), status.getRemarks(), status
								.getAddedByUserName()))
						.setParameter("prospectNos", status.getWorkOrderNumber())
						.executeUpdate();

				statusMsg = StatusMessages.UPDATED_SUCCESSFULLY;
				return statusMsg;
			}
			else
			{

				FrTicket ticket = hibernateTemplate.get(FrTicket.class, status
						.getTicketId());
				if (ticket == null)
				{
					statusMsg = StatusMessages.NO_RECORD_FOUND;
					return statusMsg;
				}

				ticket.setStatus(status.getStatus());
				
				if( status.getRemarks() != null && !status.getRemarks().isEmpty())
					ticket.setRemarks(status.getRemarks());
				
				if( status.getReasonCode() > 0 )
					ticket.setReason(status.getReasonCode()+"");
				
				ticket.setModifiedOn(new Date());
						
				hibernateTemplate.update(ticket);
				
				statusMsg = StatusMessages.UPDATED_SUCCESSFULLY;
				return statusMsg;
			}
		} 
		catch ( Exception e)
		{

			LOGGER.error("Failed to update Ticket Status. Reason "
					+ e.getMessage());

			statusMsg = StatusMessages.UPDATE_FAILED;

			return statusMsg;
		}finally {
			hibernateTemplate.flush();
		}
	}
	
	@Override
	public Map<String, Long> getFrTicketByWorkOrder(List<String> workOrders)
	{
		LOGGER.info("Getting Map of work order by list of workorder ...");
		Map<String, Long> workOrdersTicketMap = new ConcurrentHashMap<String, Long>();
		
		if (workOrders == null || workOrders.isEmpty())
			return workOrdersTicketMap;
		
		LOGGER.debug("Getting Map of work order by list of workorder ..." + workOrders.size());
		try
		{

			List<Object[]> branches =  hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT t.id , t.workOrderNumber FROM FrTicket t where t.workOrderNumber in (:prospectList)")
					.setParameterList("prospectList", workOrders).list();
			
			if (branches != null)
			{
				for (Object[] obj : branches)
				{
					workOrdersTicketMap
					.put( ((String) obj[1]),((BigInteger) obj[0]).longValue());


				}
			}
			LOGGER.debug("Exitting  from getFrTicketByWorkOrder of work order by list of workorder ...");
		} 
		catch (Exception e)
		{
			LOGGER.error("Error while getting getFrTicketNosFromProspects " + e.getMessage());
		}
		LOGGER.debug(" Exit from Getting Map of work order by list of workorder ..." + workOrders.size());
		
		return workOrdersTicketMap;
	}
	
	@Override
	public Integer getCurrentPushBackCount(Long ticketId )
	{
		LOGGER.info("Getting push back count for Ticket ...");
		if( ticketId == null || ticketId <= 0 )
			return -1;
		try
		{
			List list = hibernateTemplate.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery(PUSH_BACK_COUNT)
						.setParameter("ticketId", ticketId).list();
			
			if( list != null && list.size() > 0)
				return (Integer)list.get(0);
		}
		catch (Exception e)
		{
			LOGGER.error("Error occure while "
					+ "getting push back count "+e);
		}
		return -1;
	}

	@Override
	public Integer updateFrTicketPushBackCount(FrTicketPushBackVo pushBackVo)
	{

		LOGGER.info("Updating Push back count value for Ticket ...");
		if( pushBackVo == null || pushBackVo.getTicketId() == null 
				|| pushBackVo.getUpdateType() == null 
				|| pushBackVo.getUpdateType().isEmpty() )
			
			return 0;
		
		LOGGER.debug("Updating Push back count value for Ticket ..." +pushBackVo.getTicketId());
		try  
		{
			StringBuilder completeQuery  = null;
			
			if( UpdateType.NE.equals( pushBackVo.getUpdateType()) )
				completeQuery = new StringBuilder( PUSH_BACK_UPDATE_SQL );
			else if ( UpdateType.TL.equals( pushBackVo.getUpdateType()) )
				completeQuery = new StringBuilder( PUSH_BACK_UPDATE_TLQ_SQL );
			else 
				return 0;
			
			long modifiedBy = AuthUtils.getCurrentUserId();
			
			Session session = hibernateTemplate.getSessionFactory()
					.getCurrentSession();
			
			Query query = null;
			
			if( UpdateType.NE.equals( pushBackVo.getUpdateType()) )
				
				query = session.createSQLQuery( completeQuery.toString() )
					.setParameter("nonLock", pushBackVo.isLocked() )
					.setParameter("reasonValue", pushBackVo.getReason())
					.setParameter("remarksValue", pushBackVo.getRemarks())
					.setParameter("modifiedBy", modifiedBy )
					.setParameter("ticketId", Long.valueOf(pushBackVo.getTicketId()));
			
			else
				query = session.createSQLQuery( completeQuery.toString() )
					.setParameter("reasonValue", pushBackVo.getReason())
					.setParameter("remarksValue", pushBackVo.getRemarks())
					.setParameter("modifiedBy", modifiedBy )
					.setParameter("assignedTo", pushBackVo.getReportTo())
					.setParameter("statusId", FRStatus.FR_PUSH_BACK_TICKET )
					.setParameter("ticketId", Long.valueOf(pushBackVo.getTicketId()));
			
			return query.executeUpdate();
		}
		catch (Exception e)
		{
			LOGGER.error("Error occure while "
					+ "updating push back count "+e);
			
			return 0;
		}
		
	
	}

	@Override
	public List<Long> getNTopPriorityLockedFrTicket( Long userId, int noOfIds )
	{
		LOGGER.info("Getting N Top Priority Locked Fr Ticket for User : "+userId);
		if( noOfIds <= 0 || userId == null 
				|| userId <= 0)
			return new ArrayList<Long>();
		
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
				    .getCurrentSession()
				    .createSQLQuery(FrTicketDao.GET_TOP_N_PRIORITY_TICKET)
				    .setParameter("available",FRStatus.NEW_TICKET_DEF)
				    .setParameter("userId",userId)
				    .setParameterList("completed",TicketStatus.COMPLETED_DEF)
				    .setParameter("noOfIds",noOfIds);
					
			return objectToListLong(query.list());
		}
		catch(Exception e)
		{
			LOGGER.error("Error occure while getting to prority "
					+ "ticket for user : "+userId,e);
		}
		return new ArrayList<Long>();
	}
	
	@Override
	public List<Long> getAllVicinityFrTicketByCxIp( String cxIp ,Long flowId )
	{
		LOGGER.info("Getting Vicinity Tickets for flow and cx IP :"+cxIp);
		if( flowId == null || !FieldValidatorUtil.isValidIp( cxIp ) )
			
			return new ArrayList<Long>();
		
		try
		{
			TypeAheadVo cityVO = AuthUtils.getCurrentUserCity();
			long cityId = 0;
			
			if( cityVO != null )
				cityId = cityVO.getId();
			else
				return new ArrayList<Long>();
			
			Query query = hibernateTemplate.getSessionFactory()
				    .getCurrentSession()
				    .createSQLQuery(FrTicketDao.GET_ALL_VICINITY_TICKET)
				    .setParameter("cxIp", cxIp)
				    .setParameter("cityId", cityId)
				    .setParameter("flowId", flowId)
				    .setParameterList("completed",TicketStatus.COMPLETED_DEF);
			
			return objectToListLong( query.list() );
		}
		catch(Exception e)
		{
			LOGGER.error("Error occure while getting vicinity ticket by cxIp: "
					+cxIp,e);
		}
		return new ArrayList<Long>();
	}

	@Override
	public List<ETRExpireVo> getAllETRExpiredTickets()
	{

		LOGGER.info("Called getAllETRExpiredTickets ...");
		String valueOfEtrUpdateTimeInMinutes = definitionCoreService.getPropertyValue(keyOfEtrUpdateTimeInMinutes);
		List<ETRExpireVo> etrExpiredTicketDetails = new ArrayList<ETRExpireVo>();
		List<Object[]> etrExpiredTickets = null;
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
				    .getCurrentSession()
				    .createSQLQuery(FrTicketDao.GET_ALL_ETR_EXPIRED_TICKETS)
				    .setParameterList("completed",TicketStatus.COMPLETED_DEF)
				    .setParameter("beforeMinutes", Integer.valueOf(valueOfEtrUpdateTimeInMinutes));
			etrExpiredTickets = query.list();
			etrExpiredTicketDetails = 	xFromETRExpired(etrExpiredTickets);
			return etrExpiredTicketDetails;
		}catch (Exception e) {
			LOGGER.info("Error while getting etr expired tickets :"+e.getMessage());
			return null;
		}
	}

	private List<ETRExpireVo> xFromETRExpired(List<Object[]> etrExpiredTickets)
	{
		List<ETRExpireVo> etrExpiredTicketDetails = new ArrayList<ETRExpireVo>();
		
		if(etrExpiredTickets == null)
			return etrExpiredTicketDetails;
		ETRExpireVo etrExpireVo = null;
		for (Object[] row  : etrExpiredTickets)
		{
			etrExpireVo = new ETRExpireVo();
			etrExpireVo.setTicketId(objectToLong(row[0]));
			etrExpireVo.setCurrentFlowId(objectToLong(row[1]));
			etrExpireVo.setMobileNumber(getObjectToString(row[2]));
			etrExpireVo.setCurrentETR(GenericUtil.convertStringToDateFormat(row[3]));
			etrExpireVo.setMqId(objectToLong(row[4]));
			if(row[5] != null )
			{
				etrExpireVo.setEtrElapsedCount(objectToInt(row[5]));
				
			}else {
				etrExpireVo.setEtrElapsedCount(0);
			}
			etrExpireVo.setCityId(objectToLong(row[6]));
			etrExpireVo.setBranchId(objectToLong(row[7]));
			etrExpiredTicketDetails.add(etrExpireVo);
		}
		return etrExpiredTicketDetails;
	}
	

	@Override
	public boolean isTicketfound()
	{
		LOGGER.info("Checking Fr Ticket is there in DB ...");
		try{
			List<FrTicket> tickets = (List<FrTicket>)hibernateTemplate.find("from FrTicket");
			if( tickets == null || tickets.isEmpty())
				return false;
			else
				return true;
			
		}catch(Exception e){
			LOGGER.error("Error while checking ticket availability...");
		}
		return false;
	}
	
	@Override
	public TypeAheadVo getCityNameForTicket( Long ticketId )
	{

		LOGGER.info("Getting City Name for Fr Ticket : "+ticketId);
		if( ticketId == null || ticketId.longValue() <= 0 )
		{
			LOGGER.info("Can't fetch data for ticket : "+ticketId);
			return null;
		}
		
		try{
			Query query = hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery(FrTicketDao.GET_FR_TICKET_COMMON_DATA_SQL)
				.setParameter("ticketId", ticketId );
					
			List<Object[]> listOfData = query.list();
			TypeAheadVo cityVO = null;
			if( listOfData != null && !listOfData.isEmpty() )
			{
				for( Object[] obj : listOfData)
				{
					cityVO = new TypeAheadVo();
					cityVO.setId(objectToLong(obj[0]));
					cityVO.setName(getObjectToString(obj[1]));
				}
			}
			return cityVO;
			
		}catch(Exception e){
			LOGGER.error("Error occure while getting common data for ticket : "+ticketId);
		}
		return null;
	}

	@Override
	public boolean getFrTicketByElementNameAndFlowId( String elementName ,Long flowId )
	{
		LOGGER.info(flowId +"Getting FR Ticket by FxName and flow Id : "+elementName);
		if( elementName == null || elementName.isEmpty() 
				|| flowId == null || flowId.longValue() < 0 )
			
			return false;
		
		try{
			
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery( FrTicketDao.GET_FR_TICKET_BY_FXNAME_SQL )
					.setParameter("fxName", elementName )
					.setParameter("flowId", flowId)
					.setParameterList("completed", TicketStatus.COMPLETED_DEF);
			
			List<Object[]> frTicket = query.list();
			if( frTicket != null && !frTicket.isEmpty())
				return true;
						
		}catch (Exception e) {
			LOGGER.error("Error occure while loading FrTicket by element name : "+e.getMessage(),e);
			e.printStackTrace();
		}
		return false;
	}
	
	private String getObjectToString(Object value)
	{
		if(value == null)
			return null;
		else
			return value.toString();
	}
	
	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());
	}
	
	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}
	
	@Override
	public List<Long> getAllOpenFrTicketByUser( Long userId )
	{
		LOGGER.info("Getting All open Fr Ticket for User id : "+userId);
		
		if( userId == null || userId.longValue() <= 0 )
			return new ArrayList<Long>();
		
		try
		{
			Query query = hibernateTemplate.getSessionFactory()
				    .getCurrentSession()
				    .createSQLQuery(FrTicketDao.GET_ALL_OPEN_TICKET_USER_SQL)
				    .setParameter("userId",userId)
				    .setParameterList("statusIds",TicketStatus.COMPLETED_DEF);
					
			return objectToListLong( query.list() );
		}
		catch (Exception e) 
		{
			LOGGER.info("Error occured while fetching all open Fr"
					+ " Ticket by user id : "+userId,e);
			e.printStackTrace();
			return new ArrayList<Long>();
		}
		
	}


	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrTicketDao#closeWOinBulk4Fr(java.util.Set)
	 */
	@Override
	public int closeWOinBulk4Fr(Set<String> workOrders) {
		
		try {
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(FR_MANUAL_CLOSURE_QUERY)
					.setParameterList("workOrders", workOrders);

			int result = query.executeUpdate();
			
			LOGGER.info("Fr ticket manual closure QUERY : " + query.getQueryString() + " and work orders are "
					+ workOrders + " and result is " + result);
			
				if(result == 0)
					return 90000001;
				else
					return result;
		} catch (Exception e) {
			e.printStackTrace();
			return 90000000;
		}
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrTicketDao#updateFrWOByCRM(com.cupola.fwmp.vo.tools.siebel.from.vo.CreateUpdateFrVo)
	 */
	@Override
	public int updateFrWOByCRM(CreateUpdateFrVo faultRepair) {

		int result = 0;
//added by manjuprasad for Gart issue.		
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(dt);
		
		Long userId = UserDaoImpl.cachedUsersByLoginId
				.get(faultRepair.getAssignedTo().toUpperCase()).getId();
				
		try {
			String UPDATE_FR_WO_BY_CRM = "update FrTicket t INNER JOIN FrDetail fr ON fr.ticketId = t.id "
					+ "LEFT JOIN  FrTicketEtr etr ON etr.ticketId =t.id  "
					+ "LEFT JOIN TicketPriority pr ON pr.id = t.priorityId "
					+ "set t.defectCode=?, t.subDefectCode=?,t.currentAssignedTo =?"
					+ ",t.reopenCount=?,t.modifiedOn=?,fr.voc=?,pr.escalationType=?, pr.value = ?,t.status=?,etr.communicationETR=?,etr.currentEtr=?,etr.committedEtr=? where t.workOrderNumber=?";

			LOGGER.info("faultRepair.getListOfPrioritySource().getPriority().getPriorityType()"
					+ faultRepair.getListOfPrioritySource().getPriority().getPriorityType());

			Long prioritySource = faultRepair.getListOfPrioritySource().getPriority().getPriorityType() != null
					&& !faultRepair.getListOfPrioritySource().getPriority().getPriorityType().isEmpty()
							? definitionCoreService.getEscalationTypeByValues(
									faultRepair.getListOfPrioritySource().getPriority().getPriorityType())
							: 0;
									
			Long status = getFrTicketCurrentSatus(faultRepair.getTicketNo());
			
			if(faultRepair.getPriorityValue() == null || faultRepair.getPriorityValue().isEmpty() )
				faultRepair.setPriorityValue(0+"");
				
			int priorityValue =  Integer.valueOf(faultRepair.getPriorityValue());
			
			if(faultRepair.getStatus().equalsIgnoreCase("REOPEN"))
			{
				LOGGER.info("");
				
				if(TicketStatus.COMPLETED_DEF.contains(status.intValue()))
				{
					MQWorkorderVO mqWorkorderVO = CommonToolUtil.convertFRWorkOrderDataToFWMPFormate(faultRepair);

					LOGGER.info("Reopen mqWorkorderVO ::  " + mqWorkorderVO);

					LOGGER.info("Reopening FR WO for customer for mqWorkorderVO" + mqWorkorderVO);
					frService.reopenFrWorkOrder(mqWorkorderVO);
					return 1;
				}
					
			}

			result = jdbcTemplate.update(UPDATE_FR_WO_BY_CRM,
					new Object[] { faultRepair.getDefectCode() != null ? faultRepair.getDefectCode() : "t.defectCode",
							faultRepair.getSubDefectCode() != null ? faultRepair.getSubDefectCode() : "t.subDefectCode",
							userId, faultRepair.getReopenCount(),currentTime,faultRepair.getVoc(), prioritySource,

							priorityValue,
							faultRepair.getDefectCode() != null && !faultRepair.getDefectCode().isEmpty()
									? TicketStatus.CLOSED
									: status,faultRepair.getETR() != null && !faultRepair.getETR().isEmpty() 
									? GenericUtil.convertToFWMPDateFormatFromSiebel(faultRepair.getETR() ) : null,
									  faultRepair.getETR() != null && !faultRepair.getETR().isEmpty() 
									? GenericUtil.convertToFWMPDateFormatFromSiebel(faultRepair.getETR() ) : null,
									  faultRepair.getETR() != null && !faultRepair.getETR().isEmpty() 
									? GenericUtil.convertToFWMPDateFormatFromSiebel(faultRepair.getETR() ) : null,
							faultRepair.getTicketNo() });

		} catch (Exception e) {
			LOGGER.error("Error while updating Fault Repair details by Siebel for workorder "
					+ faultRepair.getTicketNo() + " . " + e.getMessage(), e);
			e.printStackTrace();
		}

		return result;
	}
	
	@Override
	public boolean isFrWOPresentInDB(String workOrderNumber)
	{
		try {
			List<Object> workOrders = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("select workOrderNumber from FrDetail where workOrderNumber= :workOrderNumber")
					.setParameter("workOrderNumber", workOrderNumber).list();

			if (workOrders != null && !workOrders.isEmpty())
				return true;
			
		} catch (Exception e) {
			LOGGER.error("Error while getting wo details for wo " + workOrderNumber + " " + e.getMessage());
		}

		return false;
	}
	
	@Scheduled(cron = "${fr.details.entry.retry}")
	public void retryFrDetails() {
		
		LOGGER.info("Started special cron for fr details enrty :: " + failedFrDetailsMap != null
				? failedFrDetailsMap.keySet()
				: failedFrDetailsMap.size());

		Map<String, Long> tempMap = new ConcurrentHashMap<>();
		Map<Long, Boolean> result = null;
		
		if(!failedFrDetailsMap.isEmpty())
		{
			for(Map.Entry<String, MQWorkorderVO> entry : failedFrDetailsMap.entrySet())
			{
				tempMap.put(entry.getKey(), completeProspectMapRetry.get(entry.getKey()));
				LOGGER.info("Started special cron for fr details enrty for FR WO :: " + entry.getKey());

				result = createFrDetailInBulk(Collections.singletonList(entry.getValue()), tempMap);

				LOGGER.info(
						"Result of special cron for fr details enrty for FR WO :: " + entry.getKey() + " is " + result);

				failedFrDetailsMap.remove(entry.getKey());
				completeProspectMapRetry.remove(entry.getKey());
			}
		} else
		{
			LOGGER.info("Started special cron for fr Ticket detail is empty");
		}
		LOGGER.info("Ended special cron for fr details enrty :: ");
	}

	@Override
	public Long getFrTicketCurrentSatus(String workOrderNumber) {
		try {
			return jdbcTemplate.queryForObject("select status from FrTicket where workOrderNumber ="+ "'"+workOrderNumber+"'",Long.class);
		} catch (Exception e) {
			Log.error("status not present  :: "+e.getMessage());
			e.printStackTrace();
			return Long.valueOf(FRStatus.FR_WORKORDER_GENERATED);
		}
	}

	@Override
	public Integer getFrTicketCurrentPriority(String workOrderNumber) {
		try {
			return jdbcTemplate.queryForObject("select pr.value from TicketPriority pr join FrTicket t on t.priorityId = pr.id where  t.workOrderNumber ="+ "'"+workOrderNumber+"'",Integer.class);
		} catch (Exception e) {
			Log.error("status not present  :: "+e.getMessage());
			e.printStackTrace();
			return 0;
		}
	}
}
