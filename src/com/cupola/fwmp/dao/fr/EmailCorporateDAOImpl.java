package com.cupola.fwmp.dao.fr;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.dao.city.CityDAO;
import com.cupola.fwmp.filters.EmailFilter;
import com.cupola.fwmp.persistance.entities.EmailCorporate;
import com.cupola.fwmp.service.definition.DefinitionCoreServiceImpl;
import com.cupola.fwmp.util.StatusCodes;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.fr.EmailCorporateVo;

@Transactional
public class EmailCorporateDAOImpl implements EmailCorporateDAO{
	
	private static Logger logger = Logger.getLogger(EmailCorporateDAOImpl.class
			.getName());
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	@Autowired
	CityDAO cityService;
	
	
	
	public static Map<Long, EmailCorporateVo> cachedECByTicketId =new ConcurrentHashMap<Long, EmailCorporateVo>();
	private final String GET_EC_BY_STATUS = "SELECT * from EmailCorporate";
	public final static String EMAIL_LEGAL_KEY = "legal.dept.emailIds.";
	public final static String EMAIL_CORPORATE_KEY = "corparate.dept.emailIds.";
	
	private void init()
	{
		logger.info("EmailCorporateDAOImpl init method called..");
		resetCachedEC();
		logger.info("EmailCorporateDAOImpl init method executed.");
	}

	private void resetCachedEC() {
		 
		logger.debug("getAllEc's..");
		try {
			String query = "from EmailCorporate";
			List<EmailCorporate> emailCorporates = (List<EmailCorporate>) hibernateTemplate
					.find(query);
			if (emailCorporates != null && !emailCorporates.isEmpty()) {

				for (EmailCorporate emailCorporate : emailCorporates) {
					EmailCorporateVo  emailCorporateVo = new EmailCorporateVo();
					BeanUtils.copyProperties(emailCorporate, emailCorporateVo);
					addToLocalCache(emailCorporateVo);
				}
			}
		} catch (Exception e) {
			logger.error("Error while adding cache Email Corporate : " +e.getMessage());

		}
	}

	private void addToLocalCache(EmailCorporateVo emailCorporateVo) {
		// TODO Auto-generated method stub
		logger.debug("EmailCorparate =========" + emailCorporateVo.getTicketId()
				+ " is adding in CACHE");
		cachedECByTicketId.put(emailCorporateVo.getTicketId(), emailCorporateVo);
	}

	@Override
	public List<EmailCorporateVo> getAll() {
		logger.debug("getAll... method in EmailCorporateDAOImpl :: ");
		// TODO Auto-generated method stub
		if(cachedECByTicketId.size() == 0)
			resetCachedEC();
		return new ArrayList<EmailCorporateVo>(cachedECByTicketId.values());
	}

	@Override
	public EmailCorporate add(EmailCorporateVo emailCorporateVo) {
		// TODO Auto-generated method stub
		EmailCorporate newEmailCorporate = new EmailCorporate();
		if(emailCorporateVo == null)
			return newEmailCorporate;
		
		logger.debug("Adding EmailCorporate ..." + emailCorporateVo.getTicketId());
		
		if(emailCorporateVo != null)
		{
			BeanUtils.copyProperties(emailCorporateVo, newEmailCorporate);
			if(cachedECByTicketId.containsKey(newEmailCorporate.getTicketId()))
			{
				logger.info("EmailCorporate Already Exists in DB");

				newEmailCorporate.setStatus(StatusCodes.DUPLICATE_RECORD);

				return newEmailCorporate;
			}
			else {
				logger.info("EmailCorporate does not Exists ,Hence Adding the Details with ticketId:"
						+ emailCorporateVo.getTicketId());

				BeanUtils.copyProperties(emailCorporateVo, newEmailCorporate);
				newEmailCorporate.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				String cityCode = cityService.getCityCodeById(emailCorporateVo.getCityId());
				if(emailCorporateVo.getEmailType() == FWMPConstant.EmailType.LEGAL)
				{
					newEmailCorporate.setMails(DefinitionCoreServiceImpl.emailCorporateDef.get(EMAIL_LEGAL_KEY+cityCode.toLowerCase()));
				}
				if(emailCorporateVo.getEmailType() == FWMPConstant.EmailType.CORPORATE)
				{
					newEmailCorporate.setMails(DefinitionCoreServiceImpl.emailCorporateDef.get(EMAIL_CORPORATE_KEY+cityCode.toLowerCase()));
				}
				
				hibernateTemplate.save(newEmailCorporate);
				emailCorporateVo.setId(newEmailCorporate.getId());
				emailCorporateVo.setAddedOn(new Date());
				emailCorporateVo.setUpdatedOn(new Date());
				emailCorporateVo.setStatus(FWMPConstant.EmailStatus.NOT_YET_SENT);
				addToLocalCache(emailCorporateVo);
				logger.info("EmailCorporate Successfully Added into Database ");
				return newEmailCorporate;
			}
		}
		return null;
	}

	@Override
	public EmailCorporate update(EmailCorporateVo emailCorporateVo) {
		
		if(emailCorporateVo == null )
			return null;
		
		logger.info("update EmailCorporate " +emailCorporateVo.getCityId());
			try
			{
			if(emailCorporateVo != null)
			{
			EmailCorporate oldEmailCorporate = (EmailCorporate) hibernateTemplate.get(EmailCorporate.class, emailCorporateVo.getId());

				if (oldEmailCorporate != null)
				{
					emailCorporateVo.setMails(oldEmailCorporate.getMails());
					BeanUtils.copyProperties(emailCorporateVo, oldEmailCorporate);
					oldEmailCorporate.setAddedOn(new Date());
					oldEmailCorporate.setUpdatedOn(new Date());
					oldEmailCorporate.setStatus(FWMPConstant.EmailStatus.SENT);
					logger.info("old user ========= " + oldEmailCorporate);
					hibernateTemplate.update(oldEmailCorporate);
					emailCorporateVo.setId(oldEmailCorporate.getId());
					emailCorporateVo.setAddedOn(new Date());
					emailCorporateVo.setUpdatedOn(new Date());
					emailCorporateVo.setStatus(FWMPConstant.EmailStatus.NOT_YET_SENT);
					cachedECByTicketId.put(emailCorporateVo.getTicketId(), emailCorporateVo);
					
				logger.debug(" Exit From update EmailCorporate " +emailCorporateVo.getCityId());
					
					return oldEmailCorporate;
					
				}
				
			}
		} catch ( Exception e) {
			logger.error("Exception in email corporate update");
			e.printStackTrace();
		} 
		return null;
	}

	@Override
	public List<EmailCorporateVo> getEmailsByEmailFilter(EmailFilter emailFilter) 
	{
		if(emailFilter == null )
		return null;
		
		logger.debug("getting Emails ByEmailFilter " +emailFilter.getCityId());
		
		if (emailFilter != null) {
			List<EmailCorporateVo> results = new ArrayList<EmailCorporateVo>();
			StringBuilder completeQuery = new StringBuilder(GET_EC_BY_STATUS);
			completeQuery.append(" where ");
			if(emailFilter.getTicketId() >0)
			{
				completeQuery.append(" ticketId = "+emailFilter.getTicketId());
			}
			if(emailFilter.getCityId() >0)
			{
				completeQuery.append(" cityId = "+emailFilter.getCityId());
			}
			if(emailFilter.getEventId() >0)
			{
				completeQuery.append(" eventId = "+emailFilter.getEventId());
			}
			if(emailFilter.getEmailType() >0)
			{
				completeQuery.append(" emailType = "+emailFilter.getEmailType());
			}
			if(emailFilter.getStatus() >0)
			{
				completeQuery.append(" status = "+emailFilter.getStatus());
			}
			if(emailFilter.getOffset() > 0)
			{
				completeQuery.append(" limit "+emailFilter.getOffset());
			}
			Query query = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(completeQuery.toString());
			List<Object[]> result = query.list();
			if(result != null && !result.isEmpty())
			{
				for (Iterator iterator = result.iterator(); iterator.hasNext();) {
					Object[] emailCorporates = (Object[]) iterator.next();
					EmailCorporateVo emailCorporate = new EmailCorporateVo();
					emailCorporate.setId(objectToLong(emailCorporates[0]));
					emailCorporate.setTicketId(objectToLong(emailCorporates[1]));
					emailCorporate.setCityId(objectToLong(emailCorporates[2]));
					emailCorporate.setEventId(objectToInt(emailCorporates[3]));
					emailCorporate.setEmailType(objectToInt(emailCorporates[4]));
					emailCorporate.setAttachments((String)emailCorporates[5]);
					emailCorporate.setMails((String)emailCorporates[6]);
					if(emailCorporates[7] != null)
						emailCorporate.setAddedOn((Date)emailCorporates[7]);
					if(emailCorporates[8] != null)
						emailCorporate.setUpdatedOn((Date)emailCorporates[8]);
					emailCorporate.setStatus(objectToInt(emailCorporates[9]));
					results.add(emailCorporate);
				}
			}
			return results;
		}
		
		logger.debug(" Exit From getting Emails ByEmailFilter " +emailFilter.getCityId());
		return null;
	}
	
	private Long objectToLong(Object obj)
	{
		if (obj == null)
			return 0l;
		return Long.valueOf(obj.toString());

	}

	private Integer objectToInt(Object obj)
	{
		if (obj == null)
			return 0;
		return Integer.valueOf(obj.toString());
	}


}
