package com.cupola.fwmp.dao.fr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant.CityName;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.BranchFlowConfig;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.FlowType;
import com.cupola.fwmp.persistance.entities.FrDeploymentSetting;
import com.cupola.fwmp.persistance.entities.FrSymptomNames;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.BranchFlowConfigVO;
import com.cupola.fwmp.vo.FrSymptomCodesVo;
import com.cupola.fwmp.vo.TypeAheadVo;

@Transactional
public class FrDeploymentSettingDaoImpl implements FrDeploymentSettingDao
{
	private static final String INSERT_INTO_BRANCH_FLOW_QUERY = "insert into BranchFlowConfig value(?,?,?,?,?,?,?,?,?,?)";
	
	private static final String DELETE_PREVIOUS_SYMPTOM_NAMES = "DELETE FROM FrSymptomCodes where flowId=?";
	
	private static final String GET_SYMPTOMNAMES_FOR_FLOWID = "select fn.symptomNames from FrSymptomNames fn inner join FrSymptomCodes fc on fn.id = fc.symptomCodes where fc.flowId = ?";
	
	private static final String GET_ALL_SYMPTOMS = "select f.id, f.flowId, fN.id, fN.symptomNames  from FrSymptomCodes f inner join FrSymptomNames fN on f.symptomCodes = fN.id;";
	
	private static final String GET_SYMPTOMOBJECTS_FOR_FLOWID = "select fn.id, fn.symptomNames from FrSymptomNames fn inner join FrSymptomCodes fc on fn.id = fc.symptomCodes where fc.flowId = ?";
	
	private static String QUERY_SAVE_SYMPTOM_NAMES = "Insert into FrSymptomCodes (flowId,symptomCodes) values(?,?) ";

	private static Logger LOGGER = Logger.getLogger(FrDeploymentSettingDaoImpl
			.class.getName());
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	BranchDAO branchDAO;
	
	public static Map<String, Map<String,Set<String>>> defectAndListSubDefect = 
			new ConcurrentHashMap<String, Map<String,Set<String>>>();

	public static Map<String, String> defectSubDefecAndMqCode = new ConcurrentHashMap<String, String>();
	
	public static Map<String, String> frDefaultETR = new ConcurrentHashMap<>();
	
	public  static Map<String, String> bufferTime = new ConcurrentHashMap<>();
	
	public static Map<Integer, List<String>> frFlowNamesAndFlowIdMap = new ConcurrentHashMap<>();
	
	private FileInputStream inputStream = null;

	@Override
	public void init()
	{
		cacheData();
		getBufferandavarageTimeCache();
		getSymtomNameAndFlowIdsCache();
		LOGGER.info("AavarageTimeCache size:"+frDefaultETR.size());
		LOGGER.info("BufferTime Cache Size:"+bufferTime.size());
		LOGGER.info("SymtomNameAndFlowIdsCache Size:"+frFlowNamesAndFlowIdMap.size());
		LOGGER.info("defectAndListSubDefect Size:"+defectAndListSubDefect.size());
		LOGGER.info("defectSubDefecAndMqCode Size:"+defectSubDefecAndMqCode.size());
	}
	
	Branch branch = new Branch();
	City city = new City();
	
	@Override
	public APIResponse createFrDeploymentEntry( FrDeploymentSetting setting )
	{
		if( setting == null || setting.getCityName()== null
				|| setting.getDefectCode() == null || setting.getSubDefectCode() == null || setting.getMqClosureCode() == null)
			return ResponseUtil.createNullParameterResponse();
		
		LOGGER.info(" inside  createFrDeploymentEntry " +setting.getCityName());
 
		try{
			
			if( !(setting.getCityName().equalsIgnoreCase(CityName.HYDERABAD_CITY)))
				setting.setCityName("REST_OF_INDIA");
			
			String cityName = setting.getCityName().toUpperCase();
			
			String key = getLogicalCodeValue( setting.getDefectCode()
					+"_"+setting.getSubDefectCode()+"_"+cityName);
			
			if( key != null && defectSubDefecAndMqCode.containsKey(key))
				return ResponseUtil.createDuplicateResponse();
			
			setting.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			setting.setDefectSubDefectLogicalCode(key);
			setting.setDefectCodeValue(getLogicalCodeValue(setting.getDefectCode()));
			setting.setSubDefectCodeValue(getLogicalCodeValue(setting.getSubDefectCode()));
			setting.setCityName(cityName);
			
			Long result = (Long) hibernateTemplate.save(setting);
			
			if(result != null && result.longValue() > 0) {
				defectSubDefecAndMqCode.put(key, setting.getMqClosureCode());
				return ResponseUtil.createSuccessResponse();
			}else {
				return ResponseUtil.createFailureResponse();
			}
			
		}
		catch(Exception e){
			LOGGER.error("Error while create fr setting entry"+e);
			return ResponseUtil.createDBExceptionResponse();
		}
	}
	
	private String getLogicalCodeValue( String string )
	{
		if( string == null || string.isEmpty() )
			return null;
		
		byte[] value = string.getBytes(Charset.forName("UTF-8"));

		long sum = 0;
		for( byte vo : value)
			sum = sum + vo;
		
	    int h = 0;
	    for (int i = 0; i < string.length(); i++) 
	    {
	        h = 31 * h + string.charAt(i);
	    }
		
		return sum +"_"+ h;
	}
	
	private void cacheData()
	{
		LOGGER.debug(" inside  cacheData "  );
		List<FrDeploymentSetting> allStatus = (List<FrDeploymentSetting>) hibernateTemplate
				.find("from FrDeploymentSetting");
		
		if( allStatus != null )
		{
			Set<String> subDefect = null;
			Map<String,Set<String>> map = null;
			
			for( FrDeploymentSetting setting : allStatus )
			{
				
				if( defectAndListSubDefect.containsKey(setting.getCityName()))
				{
					map = defectAndListSubDefect.get(setting.getCityName());
					
					if( map != null && map.containsKey(setting.getDefectCode()))
					{
						subDefect = map.get(setting.getDefectCode());
						if( subDefect != null )
							subDefect.add(setting.getSubDefectCode());
						else
						{
							subDefect = new HashSet<String>();
							subDefect.add(setting.getSubDefectCode());
						}
						map.put(setting.getDefectCode(), subDefect);
					}
					else 
					{
						subDefect = new HashSet<String>();
						subDefect.add(setting.getSubDefectCode());
						if( map == null )
							map = new HashMap<>();
						
						map.put(setting.getDefectCode(), subDefect);
					}
					
					defectAndListSubDefect.put(setting.getCityName(), map);
				}
				else
				{
					Set<String> newsubDefect = new HashSet<String>();
					Map<String,Set<String>> newMap = new HashMap<String,Set<String>>();
					
					newsubDefect.add(setting.getSubDefectCode().trim());
					
					newMap.put(setting.getDefectCode().trim(), newsubDefect);
					
					defectAndListSubDefect.put(setting.getCityName(), newMap);
				}
				
				// cache defect sub defect code wrt mq closure code
				if( setting.getDefectSubDefectLogicalCode() != null || setting.getMqClosureCode() != null)
					defectSubDefecAndMqCode.put(setting.getDefectSubDefectLogicalCode()
							, setting.getMqClosureCode());
			}
		}
	}
	
	private void getBufferandavarageTimeCache() {
		
		List<BranchFlowConfigVO> branchFlowConfigVOs = null;
		
		try {
			
			branchFlowConfigVOs =  jdbcTemplate
			.query("select * from BranchFlowConfig", new BeanPropertyRowMapper(BranchFlowConfigVO.class));
			
			for (BranchFlowConfigVO branchFlowConfigVO : branchFlowConfigVOs) {
				
				String map_key = getAvarageandBufferTimeMapKey(branchFlowConfigVO.getCityId(),branchFlowConfigVO.getBranchId(),branchFlowConfigVO.getFlowId());
				
				frDefaultETR.put(map_key, branchFlowConfigVO.getAverageCloseTime().toString());
				
				bufferTime.put(map_key, branchFlowConfigVO.getBufferTime().toString());
				
			}
			
		} catch (Exception e) {
			LOGGER.error("Exception in getBufferandavarageTimeCache"+e.getMessage());
		}
	}
	
	private String getAvarageandBufferTimeMapKey(Long cityId, Long branchId, Long flowId) {
		
		String city_code = LocationCache.getCityCodeById(cityId);
		String branch_code = branchDAO.getBranchCodeById(branchId);
		String key = city_code+"_"+branch_code+"_"+flowId.longValue();
		return key;
	}
	
	private void getSymtomNameAndFlowIdsCache() {
		
		try {

			List<Integer> flowIds = jdbcTemplate.query("select id from FlowType", new RowMapper<Integer>() {
				public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getInt(1);
				}
			});

			for (Integer flowId : flowIds) {
				List<String> symptomNames = jdbcTemplate.query(GET_SYMPTOMNAMES_FOR_FLOWID
						, new Object[] { flowId },
						new RowMapper<String>() {
							public String mapRow(ResultSet rs, int rowNum) throws SQLException {
								return rs.getString(1);
							}
						});
				if( symptomNames != null && symptomNames.size() >0 )
					frFlowNamesAndFlowIdMap.put(flowId, symptomNames);
			}

		} catch (Exception e) {
			LOGGER.error("Exception in getSymtomNameAndFlowIdsCache :" + e.getMessage());
			e.printStackTrace();
		}
		
	}

	@Override
	public Map<String,List<String>> getDefectSubDefectByCityId(String cityId)
	{
		Map<String,List<String>> resutl = new HashMap<String, List<String>>();
		if( cityId == null || cityId .isEmpty() )
			return resutl;
		
		LOGGER.debug(" inside  getDefectSubDefectByCityId " +cityId );
		
		if( defectAndListSubDefect == null || defectAndListSubDefect.isEmpty() )
			cacheData();
		if(defectAndListSubDefect != null )
		{
			Map<String,Set<String>> newMap = defectAndListSubDefect.get(cityId);
			if( newMap != null )
			{
				List<String> newValue = null;
				for( Map.Entry<String,Set<String>> entry : newMap.entrySet())
				{
					newValue = new ArrayList<>(entry.getValue());
					resutl.put(entry.getKey(), newValue);
				}
			}
		}
		
		return resutl;
	}
	
	@Override
	public String getMQClosureCode(String defect, String subDefect ,String city)
	{
		if( defect == null || defect.isEmpty() 
				|| subDefect == null || subDefect.isEmpty() 
				|| city == null || city.isEmpty())
			
			return null;
		
		LOGGER.debug(" inside  getMQClosureCode " +city );
		
		if( defectSubDefecAndMqCode == null || defectSubDefecAndMqCode.isEmpty() )
			cacheData();
		
		String keyCode = defect+"_"+subDefect+"_"+city.toUpperCase();
		LOGGER.debug("GETTING MQ CODE FOR KEY CODE : "+keyCode);
		
		String key = getLogicalCodeValue( defect+"_"+subDefect+"_"+city.toUpperCase());
		LOGGER.debug("GETTING MQ CODE FOR KEY : "+key);
		
		return defectSubDefecAndMqCode != null ? defectSubDefecAndMqCode.get(key) : null;
	}
	
	private void readExcel(String fileName, String SystemPath )
	{
		LOGGER.debug(" inside  readExcel " +fileName );
		XSSFWorkbook workBook = null;
		
		try 
		{
			File path = new File(SystemPath+File.separator+fileName);
			if( !path.exists())
				path.mkdirs();
				
			this.inputStream = new FileInputStream(path);
			workBook = new XSSFWorkbook(this.inputStream);
			
			Sheet spreadsheet = workBook.getSheetAt(0);
			Iterator<Row> iterator = spreadsheet.iterator();

			iterator.next();
			FrDeploymentSetting setting = null;
			while (iterator.hasNext()) 
			{
				setting = new FrDeploymentSetting();
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				while (cellIterator.hasNext())
				{
					Cell cell = cellIterator.next();
					switch (cell.getColumnIndex())
					{
					case 0: 
						setting.setCityName(cell.getStringCellValue());
						break;
					case 1: 
						setting.setDefectCode(cell.getStringCellValue());
						break;
					case 2: 
						setting.setSubDefectCode(cell.getStringCellValue());
						break;
					case 3: 
						setting.setMqClosureCode(cell.getStringCellValue());
						break;
					case 4: 
						setting.setResolutionDescription(cell.getStringCellValue());
						break;
					}
				}
				createFrDeploymentEntry(setting);
			}
		} 
		catch (FileNotFoundException e)	{
			LOGGER.error("defect sub defect mapping file : "+fileName+"are not available in Location : "
					+SystemPath);
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error("defect sub defect mapping file : "+fileName+"are not available in Location : ");
			e.printStackTrace();
		}
		catch (Exception e) {
			LOGGER.error("defect sub defect mapping file : "+fileName+"are not available in Location : ");
			e.printStackTrace();
		}
		finally{
			try{
				if( this.inputStream != null)
					this.inputStream.close();
			}catch (Exception e) {
				LOGGER.error("Error in defect sub defect mapping file , closing stream ");
				e.printStackTrace();
			}
		}
	
	}
	
	@Override
	public Integer refresh( String fileName,String SystemPath )
	{
		if( fileName == null || fileName.isEmpty() 
				|| SystemPath == null || SystemPath.isEmpty())
			return 0;
		LOGGER.debug(" inside  refresh " +fileName );
		try
		{
			final String SQL = "delete from FrDeploymentSetting where id > 0 ";
			
			int result = hibernateTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(SQL).executeUpdate();
			
			
			readExcel(fileName,SystemPath);
			
			Thread.sleep(500l);
			resetCache();
			
			return result;
		} 
		catch (Exception e)
		{
			LOGGER.error("Error while refreshing fr setting entry"+e);
			return 0;
		}
	}
	
	@Override
	public Integer addeNewCityDefectSubDefectCode(String fileName, String SystemPath )
	{
		if( fileName == null || fileName.isEmpty() 
				|| SystemPath == null || SystemPath.isEmpty())
			return 0;
		LOGGER.debug(" inside  addeNewCityDefectSubDefectCode " +fileName );
		try
		{
			int result = 0;
			readExcel(fileName,SystemPath);
			
			Thread.sleep(500l);
			resetCache();
			
			result++;
			
			return result;
		} 
		catch (Exception e)
		{
			LOGGER.error("Error while adding fr setting entry : "+e);
			e.printStackTrace();
			return 0;
		}
	}
	
	
	@Override
	public List<FrDeploymentSetting> getFrDeploymentSetting()
	{
		List<FrDeploymentSetting> list = null;
		try {
			list = (List<FrDeploymentSetting>) hibernateTemplate
						.find("from FrDeploymentSetting where cityName = 'SIEBEL'");
	
		} catch (Exception e) {
			LOGGER.info("Exception while getting fr deployment setting" + e.getMessage());
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public void resetCache()
	{
		LOGGER.debug("  inside resetCache "   );
		if( defectAndListSubDefect != null )
			defectAndListSubDefect.clear();
		if( defectSubDefecAndMqCode != null)
			defectSubDefecAndMqCode.clear();
		
		frDefaultETR.clear();
		bufferTime.clear();
		frFlowNamesAndFlowIdMap.clear();
		
		init();
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#getMQLevelData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FrDeploymentSetting> getMQLevelData(String cityName) {
		
		LOGGER.info("Inside getMQLevelData");
		
		List<FrDeploymentSetting> deploymentSettings = null;

		try {
			if (cityName.equalsIgnoreCase("ALL")) {
				deploymentSettings = (List<FrDeploymentSetting>)hibernateTemplate.find("from FrDeploymentSetting");
			} else if (cityName.equalsIgnoreCase(CityName.HYDERABAD_CITY)
					|| cityName.equalsIgnoreCase("REST_OF_INDIA")) {
				deploymentSettings = (List<FrDeploymentSetting>)hibernateTemplate.find("from FrDeploymentSetting where cityName = ?", cityName);
			}

		} catch (Exception e) {
			LOGGER.error("Exception while Fetching FrDeploymentSetting Details");
			e.printStackTrace();
		}
		
		return deploymentSettings;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#updateFrDeploymentSetting(com.cupola.fwmp.persistance.entities.FrDeploymentSetting)
	 */
	@Override
	public APIResponse updateFrDeploymentSetting(FrDeploymentSetting deploymentSetting) {
		
		LOGGER.info("Inside updateFrDeploymentSetting");
		
		try {
			FrDeploymentSetting oldDeploymentSetting =  (FrDeploymentSetting)hibernateTemplate.get(FrDeploymentSetting.class, deploymentSetting.getId());
			
			if( oldDeploymentSetting != null ) {
				
				if( deploymentSetting.getDefectCode() != null)
					oldDeploymentSetting.setDefectCode(deploymentSetting.getDefectCode());
				if( deploymentSetting.getSubDefectCode() != null)
					oldDeploymentSetting.setSubDefectCode(deploymentSetting.getSubDefectCode());
				if( deploymentSetting.getMqClosureCode() != null )
					oldDeploymentSetting.setMqClosureCode(deploymentSetting.getMqClosureCode());
				
				hibernateTemplate.update(oldDeploymentSetting);
				
				defectSubDefecAndMqCode.put(deploymentSetting.getDefectSubDefectLogicalCode(), deploymentSetting.getMqClosureCode());
			}
			else
				return ResponseUtil.createRecordNotFoundResponse();
			
			return ResponseUtil.createUpdateSuccessResponse();
		} catch (Exception e) {
			LOGGER.error("Exception in updateFrDeploymentSetting :"+e.getMessage());
			e.printStackTrace();
			return ResponseUtil.createDBExceptionResponse();
		}
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#deleteFrDeploymentSetting(com.cupola.fwmp.persistance.entities.FrDeploymentSetting)
	 */
	@Override
	public APIResponse deleteFrDeploymentSetting(FrDeploymentSetting deploymentSetting) {
		
		LOGGER.info("Inside deleteFrDeploymentSetting");
		try {
			FrDeploymentSetting  oldSeploymentSetting = (FrDeploymentSetting) hibernateTemplate
					.get(FrDeploymentSetting.class, deploymentSetting.getId());
			
			if( oldSeploymentSetting != null) {
					hibernateTemplate.delete(oldSeploymentSetting);
					if( deploymentSetting.getDefectSubDefectLogicalCode() != null && defectSubDefecAndMqCode.containsKey(deploymentSetting.getDefectSubDefectLogicalCode()))
						defectSubDefecAndMqCode.remove(deploymentSetting.getDefectSubDefectLogicalCode());
			}
			else
				return ResponseUtil.createRecordNotFoundResponse();
			
			return ResponseUtil.createDeleteSuccessResponse();
		}catch (Exception e) {
			LOGGER.error("Exception in deleteFrDeploymentSetting :"+e.getMessage());
			e.printStackTrace();
			return ResponseUtil.createDBExceptionResponse();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#getAllBranchFlowConfigurations()
	 */
	@Override
	public List<BranchFlowConfigVO> getAllBranchFlowConfigurations() {
		
		LOGGER.info("Inside  getAllBranchFlowConfigurations");
List<BranchFlowConfigVO> branchFlowConfigVOs = null;
		
		try {
			branchFlowConfigVOs = jdbcTemplate
					.query("select * from BranchFlowConfig", new BeanPropertyRowMapper(BranchFlowConfigVO.class));
					
		} catch (Exception e) {
			LOGGER.info("Exception in getBranchFlowConfigurations :"+e.getMessage());
			e.printStackTrace();
		}
		return branchFlowConfigVOs;
	}
	

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#getBranchFlowConfigurations(java.lang.String)
	 */
	@Override
	public List<BranchFlowConfigVO> getBranchFlowConfigurations(String cityId, String branchId) {
		
		LOGGER.info("Inside getBranchFlowConfigurations  :cityId :"+cityId +", branchId: "+branchId);
		List<BranchFlowConfigVO> branchFlowConfigVOs = null;
		
		try {
			
			if( cityId != null && branchId != null && !branchId.isEmpty()) {
				LOGGER.info("Inside branch ");
				branchFlowConfigVOs = jdbcTemplate
					.query("select * from BranchFlowConfig where branchId = '"+branchId+"'", new BeanPropertyRowMapper(BranchFlowConfigVO.class));
			}
			else if( cityId != null) {
				LOGGER.info("Inside only city ");
				branchFlowConfigVOs = jdbcTemplate
				.query("select * from BranchFlowConfig where cityId = '"+cityId+"'", new BeanPropertyRowMapper(BranchFlowConfigVO.class));
			}
		} catch (Exception e) {
			LOGGER.info("Exception in getBranchFlowConfigurations :"+e.getMessage());
			e.printStackTrace();
		}
		return branchFlowConfigVOs;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#addBranchFlowConfiguration(com.cupola.fwmp.vo.BranchFlowConfigVO)
	 */
	@Override
	public APIResponse addBranchFlowConfiguration(BranchFlowConfigVO branchFlowConfigVO) {
		
		LOGGER.info("Inside addBranchFlowConfiguration");
		
		if( branchFlowConfigVO != null && branchFlowConfigVO.getCityId() > 0 && branchFlowConfigVO.getBranchId() > 0 && branchFlowConfigVO.getFlowId() != null) {
			
			String key = getAvarageandBufferTimeMapKey(branchFlowConfigVO.getCityId(), branchFlowConfigVO.getBranchId(), branchFlowConfigVO.getFlowId());
			
			if( bufferTime.containsKey(key) || frDefaultETR.containsKey(key))
				return ResponseUtil.createDuplicateResponse();
			
			try {
				branchFlowConfigVO.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				branchFlowConfigVO.setAddedOn(new Date());
				branchFlowConfigVO.setAddedBy(AuthUtils.getCurrentUserId());

				int result = jdbcTemplate
						.update(INSERT_INTO_BRANCH_FLOW_QUERY, new Object[] {
								branchFlowConfigVO.getId(),
								branchFlowConfigVO.getBufferTime(),
								branchFlowConfigVO.getAverageCloseTime(),
								branchFlowConfigVO.getFlowId(),
								branchFlowConfigVO.getBranchId(),
								branchFlowConfigVO.getCityId(),
								branchFlowConfigVO.getAddedBy(),
								branchFlowConfigVO.getAddedOn(),
								branchFlowConfigVO.getModifiedBy(),
								branchFlowConfigVO.getModifiedOn() });
				if( result > 0) {
					frDefaultETR.put(key, branchFlowConfigVO.getAverageCloseTime().toString());
					bufferTime.put(key, branchFlowConfigVO.getBufferTime().toString());
					return ResponseUtil.createSaveSuccessResponse();
				}
				else
					return ResponseUtil.createSaveFailedResponse();
				
			} catch (Exception e) {
				LOGGER.info("Exception when adding new BranchFlow Configuration");
				e.printStackTrace();
				return ResponseUtil.createDBExceptionResponse();
			}
			
		}
		
		return ResponseUtil.createNullParameterResponse();
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#upDateBranchFlowConfiguration(
	 * com.cupola.fwmp.vo.BranchFlowConfigVO)
	 */
	@Override
	public APIResponse upDateBranchFlowConfiguration(BranchFlowConfigVO branchFlowConfigVO) {

		LOGGER.info("Inside upDateBranchFlowConfiguration");
		
		if (branchFlowConfigVO != null && branchFlowConfigVO.getCityId() > 0 && branchFlowConfigVO.getBranchId() > 0 && branchFlowConfigVO.getFlowId() != null) {

			BranchFlowConfig branchFlow = new BranchFlowConfig();

			try {
				if (branchFlowConfigVO.getId() > 0) {
					branchFlow = hibernateTemplate.get(BranchFlowConfig.class, branchFlowConfigVO.getId());
				}

				if (branchFlow != null && branchFlow.getId() != null && branchFlow.getId() > 0) {
					branchFlow.setId(branchFlow.getId());

					if (branchFlowConfigVO.getBranchId() > 0) {

						long bid = branchFlowConfigVO.getBranchId();

						branch.setId(bid);

						branchFlow.setBranch(branch);
					}

					if (branchFlowConfigVO.getCityId() > 0) {

						long cid = branchFlowConfigVO.getCityId();

						city.setId(cid);

						branchFlow.setCity(city);
					}

					if (branchFlowConfigVO.getBufferTime() != null) {

						branchFlow.setBufferTime(branchFlowConfigVO.getBufferTime());
					}
					if (branchFlowConfigVO.getAverageCloseTime() != null) {

						branchFlow.setAverageCloseTime(branchFlowConfigVO.getAverageCloseTime());
					}
					branchFlow.setModifiedOn(new Date());
					branchFlow.setModifiedBy(AuthUtils.getCurrentUserId());
					hibernateTemplate.update(branchFlow);
					
					String key = getAvarageandBufferTimeMapKey(branchFlowConfigVO.getCityId(), branchFlowConfigVO.getBranchId(), branchFlowConfigVO.getFlowId());
					frDefaultETR.put(key, branchFlowConfigVO.getAverageCloseTime().toString());
					bufferTime.put(key, branchFlowConfigVO.getBufferTime().toString());
					
					return ResponseUtil.createUpdateSuccessResponse();
				}
				
				return ResponseUtil.createRecordNotFoundResponse();
			} catch (Exception e) {
				LOGGER.error("Exception in Update Branchflow :"+e.getMessage());
				return ResponseUtil.createDBExceptionResponse();
			}
		}
		
		return ResponseUtil.createNullParameterResponse();
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#deleteBranchFlowConfiguration(com.cupola.fwmp.vo.BranchFlowConfigVO)
	 */
	@Override
	public APIResponse deleteBranchFlowConfiguration(long branchFlowId) {
		
		LOGGER.info(" inside deleteBranchFlow  " + branchFlowId);
		BranchFlowConfig brFlowConfig = null;

		if (branchFlowId > 0) {

			try {

				brFlowConfig = hibernateTemplate.get(BranchFlowConfig.class, branchFlowId);

				if (brFlowConfig != null) {

					hibernateTemplate.delete(brFlowConfig);
					
					String key = getAvarageandBufferTimeMapKey(brFlowConfig.getCity().getId(), brFlowConfig.getBranch().getId(), (long)brFlowConfig.getFlowType().getId());
					
					if(frDefaultETR.containsKey(key))
						frDefaultETR.remove(key, brFlowConfig.getAverageCloseTime().toString());
					if(bufferTime.containsKey(key))
						bufferTime.remove(key, brFlowConfig.getBufferTime().toString());

					return ResponseUtil.createDeleteSuccessResponse();

				} else {
					return ResponseUtil.createRecordNotFoundResponse();
				}
			} catch (Exception e) {
				LOGGER.error("Exceptoin in Deleting BranchFlow Configuration " + branchFlowId + ", " + e.getMessage());
				return ResponseUtil.createDBExceptionResponse();
			}
		}
		return ResponseUtil.createNullParameterResponse();
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.fr.FrDeploymentSettingDao#getAllFlowTypes()
	 */
	@Override
	public List<FlowType> getAllFlowTypes() {
		
		LOGGER.info("Inside getAllFlowTypes");
		
		List<FlowType> flowTypes = null;
		
		try {
			flowTypes = jdbcTemplate
					.query("select * from FlowType", new BeanPropertyRowMapper(FlowType.class));
		} catch (Exception e) {
			LOGGER.error("Exception in getAllFlowTypes"+ e.getMessage());
		}
		
		return flowTypes;
	}

	@Override
	public List<FrSymptomCodesVo> getAllSymptomTypes() {
		
		LOGGER.info("Inside getAllSymptomTypes");
		
		List <FrSymptomCodesVo> frSymptomCodes = null;
		
		try {
			frSymptomCodes = jdbcTemplate
					.query(GET_ALL_SYMPTOMS, new BeanPropertyRowMapper(FrSymptomCodesVo.class));
		} catch (Exception e) {
			LOGGER.error("Exception in getAllSymptomTypes"+e.getMessage());
			e.printStackTrace();
		}
		
		LOGGER.info("Symptom Data :"+ frSymptomCodes);
		
		return frSymptomCodes;
	}

	@Override
	public List<FrSymptomNames> getSymptomNamesFromFlowId(Long flowId) {
		
		LOGGER.info("Inside getSymptomNamesFromFlowId :"+flowId);
		List<FrSymptomNames> frSymptomNames = null;
		
		try {
			frSymptomNames = jdbcTemplate
					.query(GET_SYMPTOMOBJECTS_FOR_FLOWID, new Object[] { flowId }, new BeanPropertyRowMapper(FrSymptomNames.class));
		} catch (Exception e) {
			LOGGER.error("Exception in getSymptomNamesFromFlowId :"+e.getMessage());
			e.printStackTrace();
		}
		
		LOGGER.info("Return data size:"+ frSymptomNames.size());
		
		return frSymptomNames;
	}
	
	@Override
	public Map<Integer, List<String>> getAllSymptomNamesForFlowIds() {
		
		LOGGER.info("Inside getAllSymptomNamesForFlowIds");
		
		Map<Integer, List<String>> frSymptomNames = new HashMap<Integer, List<String>>();
		
		try {

			Query idQuery = hibernateTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery("select id from FlowType");
			List<Integer> flowIds = idQuery.list();

			for (Integer flowId : flowIds) {
				Query query = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery(
						"select fn.symptomNames from FrSymptomNames fn inner join FrSymptomCodes fc on fn.id = fc.symptomCodes where fc.flowId ="
								+ flowId);
				if( query.list().size() >0 )
					frSymptomNames.put(flowId, query.list());
				
			}

		} catch (Exception e) {
			LOGGER.error("Exception in getSymptomNamesFromFlowId :" + e.getMessage());
			e.printStackTrace();
		}
		
		LOGGER.info("Return data size:"+ frSymptomNames.size());
		
		return frSymptomNames;
	}

	@Override
	public List<FrSymptomNames> getAllSymptomNames() {
		List<FrSymptomNames> frSymptomNames = null;
		
		LOGGER.info("Inside getAllSymptomNames");
		
		try {
			frSymptomNames = jdbcTemplate
					.query("select id, symptomNames from FrSymptomNames", new BeanPropertyRowMapper(FrSymptomNames.class));
		} catch (Exception e) {
			LOGGER.error("Exception in getSymptomNamesFromFlowId :"+e.getMessage());
			e.printStackTrace();
		}
		
		LOGGER.info("Return data size:"+ frSymptomNames.size());
		
		return frSymptomNames;
	}
	
	@Override
	public APIResponse updateSymptomNames(final Integer flowId, List<FrSymptomNames> symptoms) {

		if(flowId == null )
			return ResponseUtil.createNullParameterResponse();
		
		LOGGER.info(" inside  updateSymptomNames " + symptoms.size() +" For FlowId "  + flowId);
		
		try
		{
			final int batchSize = 500;

			this.jdbcTemplate.update(DELETE_PREVIOUS_SYMPTOM_NAMES, new Object[] { flowId });
			
			if(symptoms == null || symptoms.isEmpty() )
				return ResponseUtil.createDeleteSuccessResponse();
			
			List<String> symptomNames = new ArrayList<>();
			
			for (int j = 0; j < symptoms.size(); j += batchSize)
			{

				final List<FrSymptomNames> batchList = symptoms.subList(j, j
						+ batchSize > symptoms.size() ? symptoms.size() : j
						+ batchSize);
				
				
				this.jdbcTemplate
						.batchUpdate(QUERY_SAVE_SYMPTOM_NAMES, new BatchPreparedStatementSetter()
						{

							@Override
							public int getBatchSize()
							{
								return batchList.size();
							}

							@Override
							public void setValues(PreparedStatement ps, int i)
									throws SQLException
							{

								ps.setLong(1, flowId);
								ps.setLong(2, batchList.get(i).getId());
							}
						});
			}
			
			for (FrSymptomNames names : symptoms) {
				symptomNames.add(names.getSymptomNames());
			}
			frFlowNamesAndFlowIdMap.put(flowId, symptomNames);
			
			return ResponseUtil.createSaveSuccessResponse();
		} catch ( Exception e)
		{
			LOGGER.error(" Error while Update Symtom Codes  :" + e.getMessage() );
			return ResponseUtil.createDBExceptionResponse();
		}

	}

	@Override
	public APIResponse addSymptom(String symptomName) {
		
		LOGGER.info("Inside addSymptom");
		
		if( symptomName == null && symptomName.isEmpty())
			return ResponseUtil.createNullParameterResponse();
		
		FrSymptomNames frSymptomNames = new FrSymptomNames();
		
		frSymptomNames.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		frSymptomNames.setSymptomNames(symptomName);
		
		try {
			Long result = (Long) hibernateTemplate.save(frSymptomNames);
			
			if( result != null  && result.longValue() > 0) {
				return ResponseUtil.createSaveSuccessResponse();
			}
			return ResponseUtil.createSaveFailedResponse();
		} catch (Exception e) {
			LOGGER.error("Exception in addSymptom :"+e.getMessage());
			return ResponseUtil.createDBExceptionResponse();
		}
	}
	
	@Override
	public APIResponse getAllFrSymptomNameByFlowId(Long flowId)
	{
		
		LOGGER.info("Inside getAllFrSymptomNameByFlowId");
		List<TypeAheadVo> typeAhead = new ArrayList<TypeAheadVo>();
		if (flowId != null)
		{
			try
			{
				Integer key = flowId.intValue();
				List<String> names = frFlowNamesAndFlowIdMap.get(key);
				TypeAheadVo vo = null;
				for (String value : names)
				{
					vo = new TypeAheadVo();
					vo.setName(value);
					typeAhead.add(vo);
				}
				return ResponseUtil.createSuccessResponse().setData(typeAhead);

			} catch (Exception exception)
			{
				LOGGER.error("Error While executing getAllFrSymptomNameByFlowId   :"
						+ exception.getMessage());
				exception.printStackTrace();
				return ResponseUtil.createSuccessResponse().setData(typeAhead);
			}
		}
		return ResponseUtil.createSuccessResponse().setData(typeAhead);
	}
}
