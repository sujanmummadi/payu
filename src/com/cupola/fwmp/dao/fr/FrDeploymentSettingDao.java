package com.cupola.fwmp.dao.fr;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.FlowType;
import com.cupola.fwmp.persistance.entities.FrDeploymentSetting;
import com.cupola.fwmp.persistance.entities.FrSymptomNames;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.vo.BranchFlowConfigVO;
import com.cupola.fwmp.vo.FrSymptomCodesVo;

public interface FrDeploymentSettingDao
{

	APIResponse createFrDeploymentEntry(FrDeploymentSetting setting);

	Map<String, List<String>> getDefectSubDefectByCityId(String cityId);

	String getMQClosureCode(String defect, String subDefect, String city);

	void init();

	Integer refresh( String fileName, String SystemPath );

	Integer addeNewCityDefectSubDefectCode(String fileName, String SystemPath);
	
	/**
	 * @author snoor
	 * List<FrDeploymentSetting>
	 * @param cityName
	 * @return
	 */
	List<FrDeploymentSetting> getMQLevelData(String cityName);
	
	/**
	 * @author snoor
	 * APIResponse
	 * @param deploymentSetting
	 * @return
	 */
	APIResponse updateFrDeploymentSetting(FrDeploymentSetting deploymentSetting);

	/**@author snoor
	 * APIResponse
	 * @param deploymentSetting
	 * @return
	 */
	APIResponse deleteFrDeploymentSetting(FrDeploymentSetting deploymentSetting);

	/**@author snoor
	 * List<BranchFlowConfigVO>
	 * @param branchId
	 * @return
	 */
	List<BranchFlowConfigVO> getBranchFlowConfigurations(String cityId, String branchId);

	/**@author snoor
	 * APIResponse
	 * @param branchFlowConfigVO
	 * @return
	 */
	APIResponse addBranchFlowConfiguration(BranchFlowConfigVO branchFlowConfigVO);

	/**@author snoor
	 * APIResponse
	 * @param branchFlowConfigVO
	 * @return
	 */
	APIResponse upDateBranchFlowConfiguration(BranchFlowConfigVO branchFlowConfigVO);

	/**@author snoor
	 * APIResponse
	 * @param branchFlowConfigVO
	 * @return
	 */
	APIResponse deleteBranchFlowConfiguration(long branchFlowConfigVO);

	/**@author snoor
	 * List<BranchFlowConfigVO>
	 * @return
	 */
	List<BranchFlowConfigVO> getAllBranchFlowConfigurations();

	/**@author snoor
	 * List<FlowType>
	 * @return
	 */
	List<FlowType> getAllFlowTypes();

	List<FrSymptomCodesVo> getAllSymptomTypes();
	
	List<FrSymptomNames> getSymptomNamesFromFlowId(Long flowId);

	Map<Integer, List<String>> getAllSymptomNamesForFlowIds();

	List<FrSymptomNames> getAllSymptomNames();

	APIResponse updateSymptomNames(Integer flowId, List<FrSymptomNames> symptoms);

	APIResponse addSymptom(String symptomName);

	APIResponse getAllFrSymptomNameByFlowId(Long flowId);

	void resetCache();
	
	/**@author pawan
	 * List<FrDeploymentSetting>
	 * @return
	 */
	List<FrDeploymentSetting> getFrDeploymentSetting();

}
