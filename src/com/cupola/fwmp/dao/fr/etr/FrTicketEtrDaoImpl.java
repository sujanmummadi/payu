package com.cupola.fwmp.dao.fr.etr;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FRConstant.FrTicketEtrStatus;
import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.persistance.entities.FrTicket;
import com.cupola.fwmp.persistance.entities.FrTicketEtr;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.etr.EtrCalculator;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;
import com.cupola.fwmp.vo.fr.FrTicketEtrVo;

@Transactional
public class FrTicketEtrDaoImpl implements FrTicketEtrDao
{
	private static Logger LOGGER = Logger.getLogger(FrTicketEtrDaoImpl.class);
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@Autowired
	private EtrCalculator etrCalculator;
	
	@Autowired
	private DefinitionCoreService definitionCoreService;
	
	private String defaultHours = "";
	private Set<FrTicketEtrDataVo> calculatedEtrValue = new LinkedHashSet<FrTicketEtrDataVo>();
	
	public static String UPDATE_ETR = "UPDATE FrTicketEtr SET "
			+ "currentEtr = :currentEtr ,communicationETR = :communicationETR , etrElapsedCount = :updatedCount WHERE ticketId = :ticketId"; 

	/** added default ETR for newly create FR Ticket from crm   */
	@Override
	public long addFrTicketEtr(FrTicketEtrVo etrVo)
	{
		if( etrVo == null )
		 return 0;
		
		LOGGER.debug(" inside addFrTicketEtr " + etrVo.getTicketId());
		try{
			
			FrTicketEtr etr = new FrTicketEtr();
			
			etr.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			etr.setCurrentEtr( etrVo.getCommittedEtr() );
			etr.setCommittedEtr(etrVo.getCommittedEtr());
			etr.setCommunicatedEtr(etrVo.getCommittedEtr());
			
			FrTicket ticket = hibernateTemplate.
					load(FrTicket.class, etrVo.getTicketId());
			
			etr.setTicket(ticket);
			etr.setUpdatedCount(0);
			etr.setEtrUpdateMode(FrTicketEtrStatus.ETR_UPDATE_MANUAL_MODE);
			
			etr.setStatus(FrTicketEtrStatus.ETR_NEW);
			etr.setModifiedBy(FWMPConstant.SYSTEM_ENGINE);
			etr.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
			
			etr.setModifiedOn(new Date());
			etr.setAddedOn(new Date());
			
			etr.setApprovalEtr( etr.getCurrentEtr() );
			etr.setCurrentProgress("0");
			etr.setEtrElapsedCount(0);
			
//			etr.setResonCode(resonCode);
//			etr.setRemarks(remarks);
			
			return (Long)hibernateTemplate.save(etr);
			
		}catch(Exception e){
			LOGGER.error("Error occure while adding etr for ticket : "
					+etrVo.getTicketId());
		}
		LOGGER.debug(" Exit from  addFrTicketEtr " + etrVo.getTicketId());
		return 0;
	}
	
	@Override
	public Set<FrTicketEtrDataVo> addFrTicketEtrInBulk( List<FrTicketEtrDataVo> listOfObject )
	{
		calculatedEtrValue.clear();
		if( listOfObject == null || listOfObject.isEmpty())
			return calculatedEtrValue;
		
		Long flowId = 6l;
		
		for( FrTicketEtrDataVo etrVo : listOfObject )
		{
			try
			{
				if( etrVo.getSymptomName() != null && !etrVo.getSymptomName().isEmpty() )
				{
					flowId = FrTicketUtil.
							findFlowIdforMatchingSymptomName(etrVo.getSymptomName());
					
					LOGGER.debug("******** Symtom Name : "+etrVo.getSymptomName() +"Flow Id : "+flowId);
				}
				
				//commeneted by Manjuprasad for ETR issue
				
				/*Date committedEtr = etrCalculator
						.calculateEtrForFRTicket(flowId,etrVo.getCityCode(),etrVo.getBranchCode());
				
				if( committedEtr != null )
					etrVo.setCommittedEtr(committedEtr);
				else 
				{
					defaultHours = definitionCoreService
							.getPropertyValue(DefinitionCoreService.ETR_DEFAULT_HOURS);
					int hours = 0;
					
					try{
						hours = Integer.valueOf(defaultHours);
					}catch(Exception ex){
						LOGGER.error("Please provid default etr value in property file : ");
					}
					etrVo.setCommittedEtr(GenericUtil.addHours(hours));
				}*/
				
				if(flowId == null)
					flowId = 0l;
				
				etrVo.setFlowId(flowId);
				etrVo.setCommittedEtr(etrVo.getCommittedEtr());

				FrTicketEtr etr = new FrTicketEtr();
				
				etr.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				etr.setCurrentEtr( etrVo.getCommittedEtr() );
				etr.setCommittedEtr(etrVo.getCommittedEtr());
				etr.setCommunicatedEtr(etrVo.getCommittedEtr());
				
				LOGGER.debug("IN ETR DAO IMPL ------------Creating Etr for Ticket ID :"+etrVo.getTicketId());
				FrTicket ticket = hibernateTemplate.
						load(FrTicket.class, etrVo.getTicketId());
				
				etr.setTicket(ticket);
				etr.setUpdatedCount(0);
				etr.setEtrUpdateMode(FrTicketEtrStatus.ETR_UPDATE_MANUAL_MODE);
				
				etr.setStatus(FrTicketEtrStatus.ETR_NEW);
				etr.setModifiedBy(FWMPConstant.SYSTEM_ENGINE);
				etr.setAddedBy(FWMPConstant.SYSTEM_ENGINE);
				
				etr.setModifiedOn(new Date());
				etr.setAddedOn(new Date());
				
				etr.setApprovalEtr( etr.getCurrentEtr() );
				etr.setCurrentProgress("0");
				etr.setEtrElapsedCount(0);
				
//				etr.setResonCode(resonCode);
//				etr.setRemarks(remarks);
				
				
				Long etrId = (Long) hibernateTemplate.save(etr);
				hibernateTemplate.flush();
				calculatedEtrValue.add(etrVo);
				LOGGER.debug("Saved successfully :: "+etr.getTicket().getId() + " etrId " + etrId);
				
			}catch(Exception e){
				LOGGER.error("Error occure while adding etr for ticket : "
						+etrVo.getTicketId(),e);
				e.printStackTrace();
				continue;
			}
		}
		return calculatedEtrValue;
	}
	
	@Override
	public Integer updateCurrentEtrByNeOrTl( FrTicketEtrVo etrVo )
	{
		int result = 0;
		if( etrVo == null || etrVo.getTicketId() == null 
				|| etrVo.getTicketId().longValue() <= 0 )
			
			return result;
		
		LOGGER.info(" inside updateCurrentEtrByNeOrTl " + etrVo.getTicketId());
		try
		{
			result = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(FrTicketEtrDao.UPDATE_CURRENT_ETR_AND_PROGRESS)
					.setParameter("currEtr", etrVo.getApprovalEtr())
					.setParameter("currProgress", etrVo.getCurrentProgress())
					.setParameter("ticketId", etrVo.getTicketId())
					.executeUpdate();
			
			return result;
		}
		catch(Exception e)
		{
			LOGGER.error("Error occure while updating current etr for ticket : "
					+etrVo.getTicketId(),e);
			
			return result;
		}
	}
	
	@Override
	public Integer updateEtrByNeOrTl(FrTicketEtrVo etrVo)
	{
		Integer result = 0;
		if( etrVo == null )
			return result;
		
		LOGGER.debug(" inside updateEtrByNeOrTl " + etrVo.getTicketId());
		
		try{
			
			List<FrTicketEtr> etr = (List<FrTicketEtr>)hibernateTemplate
					.find("from FrTicketEtr etr where etr.ticket.id = ?",etrVo.getTicketId());
			
			if( etr != null && etr.size() > 0 )
			{
				FrTicketEtr etrDb = etr.get(0);
				
				/*Long committedEtrDb = etrDb.getCommittedEtr() != null ? 
						etrDb.getCommittedEtr().getTime() : null;
						
				Long newApprovalEtr = etrVo.getApprovalEtr() != null ?
						etrVo.getApprovalEtr().getTime() : null;
				
				if( committedEtrDb == null || newApprovalEtr == null 
						|| committedEtrDb <  newApprovalEtr )
					return result;*/
				
				/*
				 * 
				 * if ETR approval features required then we have to
				 * 	update here approvalEtr field in db & not update 
				 *  updateCount field in db till ETR approved with status
				 *  ETR_APPROVAL_PENDING
				 * else 
				 * 	update currentEtr column in db & update updateCount
				 *  field in db with status ETR_APPROVED
				 * 
				 * for now considering no approval features
				 * 
				 * */
				
				etrDb.setCurrentEtr(etrVo.getApprovalEtr());
				etrDb.setUpdatedCount(etrDb.getUpdatedCount() + 1 );
				
				etrDb.setStatus(etrVo.getStatus());
				etrDb.setApprovalEtr(etrVo.getApprovalEtr());
				
				etrDb.setCommunicatedEtr(etrVo.getApprovalEtr());
				
				if( etrVo.getResonCode() != null 
						&& !etrVo.getResonCode().isEmpty())
					etrDb.setResonCode(etrVo.getResonCode());
				
				if( etrVo.getRemarks() != null 
						&& !etrVo.getRemarks().isEmpty())
					etrDb.setRemarks(etrVo.getRemarks());
				
				etrDb.setModifiedBy(etrVo.getModifiedBy());
				etrDb.setModifiedOn(new Date());
				
				if( etrVo.getCurrentProgress() != null 
						&& !etrVo.getCurrentProgress().isEmpty())
					etrDb.setCurrentProgress(etrVo.getCurrentProgress());
				
				etrDb.setEtrUpdateMode(etrVo.getEtrUpdateMode());
				hibernateTemplate.saveOrUpdate(etrDb);
				result++;
			}
			
		}catch(Exception exception){
			LOGGER.error("Error occure while updating etr for ticket : "
					+etrVo.getTicketId(),exception);
		}
		
		LOGGER.debug(" Exit from updateEtrByNeOrTl " + etrVo.getTicketId());
		return result;
	}

	@Override
	public Integer updateETRByJob(Long ticketId,Date updatedEtr, Integer expiryCount)
	{
		LOGGER.debug(" inside updateETRByJob " +  ticketId);
		
		int result = 0;
		if(updatedEtr != null && ticketId != null && ticketId >0)
		{
		
			try
			{
				Query query = hibernateTemplate.getSessionFactory()
						.getCurrentSession()
						.createSQLQuery(UPDATE_ETR)
						.setParameter("currentEtr", updatedEtr)
						.setParameter("communicationETR", updatedEtr)
						.setParameter("updatedCount", expiryCount)
						.setParameter("ticketId", ticketId);
				
				result	 = query.executeUpdate();
				
				return result;
			}catch (Exception e) {
				LOGGER.error("Error while updating fr etr by job :"+e.getMessage());
				e.printStackTrace();
				return result;
			}
		}
		LOGGER.debug(" Exit from updateETRByJob " +  ticketId);
		return result;
	}

}
