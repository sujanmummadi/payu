package com.cupola.fwmp.dao.fr.etr;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.cupola.fwmp.vo.fr.FrTicketEtrDataVo;
import com.cupola.fwmp.vo.fr.FrTicketEtrVo;

public interface FrTicketEtrDao
{
	public static final String UPDATE_CURRENT_ETR_AND_PROGRESS = "UPDATE FrTicketEtr SET currentEtr = :currEtr, "
			+ "currentProgress = :currProgress "
			+ "WHERE ticketId = :ticketId";
	
	public long addFrTicketEtr(FrTicketEtrVo etrVo);
	public Integer updateEtrByNeOrTl(FrTicketEtrVo etrVo);
	
	public Integer updateETRByJob(Long ticketId,Date updatedEtr , Integer expiryCount);
	public Integer updateCurrentEtrByNeOrTl(FrTicketEtrVo etrVo);
	
	public Set<FrTicketEtrDataVo> addFrTicketEtrInBulk( List<FrTicketEtrDataVo> listOfObject );
}
