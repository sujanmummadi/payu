package com.cupola.fwmp.dao.customer;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.vo.AuditLogVo;
import com.cupola.fwmp.vo.CustomerAadhaarMappingVo;
import com.cupola.fwmp.vo.CustomerVO;

public interface CustomerDAO
{
	String GET_CUSTOMER_BY_TICKET_ID ="select c.* from Customer c inner join Ticket t on c.id=t.customerId where t.id=:ticketId";
	
	String UPDATE_AADHAR_DETAILS_IN_CUSTOMER = "update Customer c join Ticket t on t.customerId=c.id set c.adhaarId=? where t.id=? and c.id=? ";

	public static String GET_CUST_DETAIL_BY_TICKET_ID = "select ci.id as cityId,ci.cityCode,t.prospectNo,t.id as ticketId,"
			+ " c.firstName as custFirstName, c.lastName,c.id as customerId,c.mobileNumber,tp.packageName,tp.planCode,tp.planType,"
			+ " tp.planCategoery, py.paymentMode ,py.paidAmount,py.referenceNo ,t.paymentRemark,t.status,t.mqStatus,"
			+ " t.paymentStatus,t.documentRemark,t.documentStatus,t.TicketCreationDate,t.addedOn,ci.cityName,c.emailId,"
			+ " c.mqId, t.communicationETR,t.preferedDate,( select b.branchName from Branch b where b.id = c.branchId ) "
			+ " as branchName, ( select a.areaName from Area a where a.id = c.areaId ) as areaName ,"
			+ " u.firstName as UserName,u.id as currentAssignedToId, u.mobileNo, c.communicationAdderss "
			+ " from Ticket t join Customer c on c.id = t.customerId "
			+ " left join User u on u.id = t.currentAssignedTo "
			+ " join City ci on ci.id = c.cityId "
			+ " left join TariffPlan tp   ON c.tariffId = tp.id  "
			+ " left join Payment py on py.id = t.IdPayment "
			+ " where t.id = :ticketId ";

	public CustomerVO addCustomer(CustomerVO customer);

	public CustomerVO getCustomerById(Long id);

	public long getCustomerByMqId(String mqId);

	public List<CustomerVO> getAllCustomer();

	public void deleteCustomer(Long id);

	public CustomerVO updateCustomer(CustomerVO customer,Long ticketId);

	int updateCustomerMq(Long ticketId, String mqId, Date etr);
	
	CustomerVO getCustomerByTicketId(Long ticketId);
	
	List<Customer> getCustomersByTicketIdList(List<BigInteger> ticketIds);

	int updateCustomerByTicketId(Long ticketId, int status, String remarks);

	public int updateNeComment(Long ticketNo, String neComment);

	Map<String, CustomerVO> addCustomerInBulk(
			List<MQWorkorderVO> mqWorkorderVOList);

	long getCustomerTypeByTicketId(Long ticketId);

	public Map<String, CustomerVO> addFrCustomerInBulk(
			List<MQWorkorderVO> mqWorkorderVOList);
	
	public CustomerVO updateCustomerDetails(CustomerVO customer);
	
	public AuditLogVo getCustomerDetailByTicketId(Long ticketId);
	
	int updatePOIOrPOAWhileActivity(Long ticketId , int poi , int poa);
	
	CustomerAadhaarMappingVo populateCustomerAadhaarMapping(CustomerAadhaarMapping aadhaarMapping);

	/**@author aditya
	 * void
	 * @param customerId
	 * @param id
	 */
	public int updateAadhaarDetails(Long customerId, Long aadharId,Long ticketId);

	/**@author aditya
	 * String
	 * @param customerId
	 * @param ticketId
	 * @return
	 */
	String getCustomerAadhaarMapping(Long customerId, Long ticketId);
	
	/**@author  kiran
	 * String
	 * @param customerId
	 * @param ticketId
	 * @return
	 */
	CustomerAadhaarMapping getCustomerAadhaarMappingByLable(Long customerId, Long ticketId , int lable);
	
}
