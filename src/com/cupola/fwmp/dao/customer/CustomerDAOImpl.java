package com.cupola.fwmp.dao.customer;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.jfree.util.Log;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.AddressType;
import com.cupola.fwmp.FWMPConstant.DBActiveStatus;
import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.dao.integ.mq.hyd.vo.MQWorkorderVO;
import com.cupola.fwmp.persistance.entities.Adhaar;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.persistance.entities.SubscriptionType;
import com.cupola.fwmp.persistance.entities.TariffPlan;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.FrTicketUtil;
import com.cupola.fwmp.util.GenericUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.AuditLogVo;
import com.cupola.fwmp.vo.CustomerAadhaarMappingVo;
import com.cupola.fwmp.vo.CustomerVO;
import com.cupola.fwmp.vo.tools.CustomerAddressVO;

@Transactional
public class CustomerDAOImpl implements CustomerDAO {

	private static Logger LOGGER = Logger.getLogger(CustomerDAOImpl.class);

	@Autowired
	HibernateTemplate hiberTemplate;

	@Autowired
	DefinitionCoreService definitionCoreService;

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Override
	public CustomerVO addCustomer(CustomerVO customerVo) {
		if (customerVo == null)
			return null;

		LOGGER.info("CustomerVO to insert are " + customerVo.getMobileNumber());

		try {
			if (customerVo != null) {

				Customer dbvo = xformToCustomer(customerVo);

				dbvo.setId(customerVo.getId());
				dbvo.setCustomerType(customerVo.getEnquiryModeId() + "");
				dbvo.setAddedOn(new Date());
				dbvo.setRemarks(customerVo.getNotes());
				dbvo.setModifiedOn(new Date());

				if (customerVo.getAdhaarId() != null && customerVo.getAdhaarId() > 0) {

					try {
						
						if( customerVo.getKycDetails().getPhoto() != null && !customerVo.getKycDetails().getPhoto().isEmpty())
							dbvo.setCustomerPhoto(customerVo.getKycDetails().getPhoto());
						
						dbvo.setIsCustomerViaAadhaar(Status.ACTIVE);
						dbvo.setAdhaarId(hiberTemplate.load(Adhaar.class, customerVo.getAdhaarId()));
						dbvo.setFinahubTransactionId(customerVo.getFinahubTransactionId());

						if (customerVo.isAadhaarForPOA())
							dbvo.setPoa(Status.ACTIVE);
						else
							dbvo.setPoa(Status.IN_ACTIVE);

						if (customerVo.isAadhaarForPOI())
							dbvo.setPoi(Status.ACTIVE);
						else
							dbvo.setPoi(Status.IN_ACTIVE);
					} catch (Exception e) {
						LOGGER.error("Error while adding aadhar details for customer mobile " + dbvo.getMobileNumber()
								+ " " + e.getMessage());
						e.printStackTrace();
					}
				}

				if (dbvo.getCommunicationAdderss() == null)
					dbvo.setCommunicationAdderss(dbvo.getCurrentAddress());

				if (customerVo.getSourceModeId() != null)
					dbvo.setKnownSource(customerVo.getSourceModeId() + "");

				if (customerVo.getEnquiryModeId() != null)
					dbvo.setEnquiry(customerVo.getEnquiryModeId() + "");

				if (customerVo.getEnquiry() != null)
					dbvo.setEnquiry(customerVo.getEnquiry() + "");

//commented by manjuprasad for just storing the knownSource to Id				
/*				if (customerVo.getKnownSource() != null)
					dbvo.setKnownSource(customerVo.getKnownSource() + "");*/

				TariffPlan tariffPlan = new TariffPlan();

				if (customerVo.getTariffId() != null) {
					tariffPlan.setId(customerVo.getTariffId());

					dbvo.setTariffPlan(tariffPlan);
				}

				if (customerVo.getMobileNumber() != null && customerVo.getMobileNumber().contains(",")) {
					for (String mob : customerVo.getMobileNumber().split(",")) {
						if (mob.length() > 6) {
							dbvo.setMobileNumber(mob);
							break;
						}
					}
				}
				dbvo.setAddedBy(customerVo.getAddedBy());
				dbvo.setModifiedBy(customerVo.getAddedBy());

				if (customerVo.getStatus() != null && customerVo.getStatus() == FWMPConstant.ProspectType.HOT_ID)
					dbvo.setStatus((FWMPConstant.ProspectType.HOT_ID));
				else
				dbvo.setStatus(FWMPConstant.ProspectType.HOT_ID);
				if (dbvo.getCity() == null)
				{
					try
					{
						dbvo.setCity(hiberTemplate.load(City.class, AuthUtils
								.getCurrentUserCity().getId()));
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				} /*else
				{
					dbvo.setCity(hiberTemplate.load(City.class, AuthUtils.getCurrentUserCity().getId()));
				}
*/				if (customerVo.getCustomerProfile() != null) {
					dbvo.setCustomerProfession(customerVo.getCustomerProfile());
				}
				if (customerVo.getNationality() != null) {
					dbvo.setNationality(customerVo.getNationality());
				}
				if (customerVo.getExistingIsp() != null) {
					dbvo.setExistingISP(customerVo.getExistingIsp());
				}
				if (customerVo.getCrpAccountNo() != null) {
					dbvo.setReferralCustomerAccountNo(customerVo.getCrpAccountNo());
				}

				if (customerVo.getCrpMobileNo() != null) {
					dbvo.setReferralCustomerMobileNo(customerVo.getCrpMobileNo());
				}
				Long value = (Long) hiberTemplate.save(dbvo);
				LOGGER.debug("Value After successfully save into customer table " + value);
				customerVo.setId(value);
			}
		} catch (Exception e) {
			LOGGER.error("Error while adding Customer : " + e.getMessage());

			e.printStackTrace();
			return null;
		}
		LOGGER.debug(" Exit From  add Customer method " + customerVo.getMobileNumber());
		return customerVo;
	}

	private CustomerVO findCustomer(String mqId)
	{
		LOGGER.debug("CustomerVO findCustomer are " + mqId);
		
		final String GET_CUSTOMER_BY_MQ_ID ="select c.* "
				+ "from Customer c where c.mqId=:mQIds"; 


		Query query = hiberTemplate.getSessionFactory().getCurrentSession().createSQLQuery(GET_CUSTOMER_BY_MQ_ID)
				.addEntity(Customer.class).setParameter("mQIds", mqId);

		List<Customer> customers = query.list();

		if (customers != null && !customers.isEmpty()) {
			Customer customer = customers.get(0);
			return getCustomerVO(customer);
		} else {
			return null;
		}

	}

	@Override
	public Map<String, CustomerVO> addFrCustomerInBulk(
			List<MQWorkorderVO> mqWorkorderVOList)
	{

		Map<String, CustomerVO> mqIdCustomerIdMap = new ConcurrentHashMap<String, CustomerVO>();

		if (mqWorkorderVOList == null)
			return mqIdCustomerIdMap;

		LOGGER.info("Fr CustomerVO to insert are " + mqWorkorderVOList.size());

		int size = mqWorkorderVOList.size();

		try
		{
			MQWorkorderVO mqWorkorderVO = null;
			for (Iterator<MQWorkorderVO> iterator = mqWorkorderVOList.iterator(); iterator.hasNext();) {
				try {
					mqWorkorderVO = (MQWorkorderVO) iterator.next();
					LOGGER.info("Setting Customer in customer DAO details for " + mqWorkorderVO.getWorkOrderNumber());
					CustomerVO customerVo = xformToCustomerForBulkUpload(mqWorkorderVO);
					LOGGER.info("Setted Customer in customer DAO details for " + mqWorkorderVO.getWorkOrderNumber());
					/*
					 * if( mqWorkorderVO .getMqId() != null ) { CustomerVO vo =
					 * findCustomer(mqWorkorderVO .getMqId()); if( vo != null ) {
					 * System.out.println(vo.getId()); mqIdCustomerIdMap.put(mqWorkorderVO
					 * .getMqId(), customerVo); continue; }
					 * 
					 * }
					 */

					if (customerVo != null) {
						LOGGER.info("Transforming Customer in customer DAO details for "
								+ mqWorkorderVO.getWorkOrderNumber());

						Customer dbvo = xformToCustomer(customerVo);

						LOGGER.info("Transformed Customer in customer DAO details for "
								+ mqWorkorderVO.getWorkOrderNumber());

						dbvo.setId(customerVo.getId());
						dbvo.setCustomerType(customerVo.getEnquiryModeId() + "");
						dbvo.setAddedOn(new Date());
						dbvo.setRemarks(customerVo.getNotes());
						dbvo.setModifiedOn(new Date());
						if (dbvo.getCommunicationAdderss() == null)
							dbvo.setCommunicationAdderss(dbvo.getCurrentAddress());

						if (customerVo.getSourceModeId() != null)
							dbvo.setKnownSource(customerVo.getSourceModeId() + "");

						if (customerVo.getEnquiryModeId() != null)
							dbvo.setEnquiry(customerVo.getEnquiryModeId() + "");

						if (customerVo.getEnquiry() != null)
							dbvo.setEnquiry(customerVo.getEnquiry() + "");

						if (customerVo.getKnownSource() != null)
							dbvo.setKnownSource(customerVo.getKnownSource() + "");

						TariffPlan tariffPlan = new TariffPlan();

						if (customerVo.getTariffId() != null) {
							tariffPlan.setId(customerVo.getTariffId());

							dbvo.setTariffPlan(tariffPlan);
						}

						if (customerVo.getMobileNumber() != null && customerVo.getMobileNumber().contains(",")) {
							for (String mob : customerVo.getMobileNumber().split(",")) {
								if (mob.length() > 6) {
									dbvo.setMobileNumber(mob);
									break;
								}
							}
						}
						dbvo.setAddedBy(customerVo.getAddedBy());
						dbvo.setModifiedBy(customerVo.getAddedBy());
 
						dbvo.setStatus(FWMPConstant.ProspectType.HOT_ID);
						LOGGER.info("Setted City in customer DAO details for " + mqWorkorderVO.getWorkOrderNumber());
						if (dbvo.getCity() == null) {
							try {
								dbvo.setCity(hiberTemplate.load(City.class, AuthUtils.getCurrentUserCity().getId()));
							} catch (Exception e) {
								LOGGER.error("Error in addFrCustomerInBulk");
							}
						}
						LOGGER.info("Setted City in customer DAO details for " + mqWorkorderVO.getWorkOrderNumber());
						Long value = (Long) hiberTemplate.save(dbvo);
						customerVo.setId(value);

						mqIdCustomerIdMap.put(dbvo.getMqId(), customerVo);
					}

				} catch (Exception e) {

					LOGGER.error("Error for adding FR customer for " + mqWorkorderVO.getWorkOrderNumber() + " "
							+ e.getMessage());
					e.printStackTrace();
					continue;

				} 

				LOGGER.info("Remaing customer to add are " + --size);
			}

			LOGGER.info("Fr CustomerVO to insert done....... ");
		} catch (Exception e)
		{
			LOGGER.error("Error while adding Fr Customer : " + e.getMessage());

			e.printStackTrace();
			
		}finally {
			hiberTemplate.flush();
		}
		LOGGER.debug(" Exit from CustomerVO to insert are " + mqWorkorderVOList.size());
		return mqIdCustomerIdMap;

	
	
	
	}

	@Override
	public Map<String, CustomerVO> addCustomerInBulk(
			List<MQWorkorderVO> mqWorkorderVOList)
	{
		LOGGER.info("CustomerVO to insert are " + mqWorkorderVOList.size());

		Map<String, CustomerVO> mqIdCustomerIdMap = new ConcurrentHashMap<String, CustomerVO>();

		try
		{
			MQWorkorderVO mqWorkorderVO = null;
			CustomerVO customerVo = null;
			Customer dbvo = null;
			
			for (Iterator<MQWorkorderVO> iterator = mqWorkorderVOList
					.iterator(); iterator.hasNext();)
			{
				mqWorkorderVO = null;
				customerVo = null;
				
				mqWorkorderVO = (MQWorkorderVO) iterator.next();
				
				try {
					

					customerVo = xformToCustomerForBulkUpload(mqWorkorderVO);

					if (customerVo != null)
					{
						dbvo = null;
						
						dbvo = xformToCustomer(customerVo);

						dbvo.setId(customerVo.getId());
						dbvo.setCustomerType(customerVo.getEnquiryModeId() + "");
						dbvo.setAddedOn(new Date());
						dbvo.setRemarks(customerVo.getNotes());
						dbvo.setModifiedOn(new Date());
						if (dbvo.getCommunicationAdderss() == null)
							dbvo.setCommunicationAdderss(dbvo.getCurrentAddress());

						if (customerVo.getSourceModeId() != null)
							dbvo.setKnownSource(customerVo.getSourceModeId() + "");

						if (customerVo.getEnquiryModeId() != null)
							dbvo.setEnquiry(customerVo.getEnquiryModeId() + "");

						if (customerVo.getEnquiry() != null)
							dbvo.setEnquiry(customerVo.getEnquiry() + "");

						if (customerVo.getKnownSource() != null)
							dbvo.setKnownSource(customerVo.getKnownSource() + "");

						TariffPlan tariffPlan = new TariffPlan();

						if (customerVo.getTariffId() != null)
						{
							tariffPlan.setId(customerVo.getTariffId());

							dbvo.setTariffPlan(tariffPlan);
						}

						if (customerVo.getMobileNumber() != null
								&& customerVo.getMobileNumber().contains(","))
						{
							for (String mob : customerVo.getMobileNumber()
									.split(","))
							{
								if (mob.length() > 6)
								{
									dbvo.setMobileNumber(mob);
									break;
								}
							}
						}
						dbvo.setAddedBy(customerVo.getAddedBy());
						dbvo.setModifiedBy(customerVo.getAddedBy());
						dbvo.setStatus(FWMPConstant.ProspectType.HOT_ID);
						if (dbvo.getCity() == null)
						{
							try
							{
								dbvo.setCity(hiberTemplate
										.load(City.class, AuthUtils
												.getCurrentUserCity().getId()));
							} catch (Exception e){
								LOGGER.error("Error in addCustomerInBulk");
								e.printStackTrace();
							}
						}
						
						LOGGER.info("CustomerVO to insert are " + customerVo.getMobileNumber() + " " + customerVo.getFirstName());
						
						Long value = (Long) hiberTemplate.save(dbvo);
						customerVo.setId(value);

						mqIdCustomerIdMap.put(dbvo.getMqId(), customerVo);
					}
				} catch (Exception e) 
				{
					LOGGER.error("Error while adding customer for workorder number " + mqWorkorderVO .getWorkOrderNumber() + " " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}

			LOGGER.info("CustomerVO to insert done....... ");
		} catch (Exception e)
		{
			LOGGER.error("Error while adding Customer : " + e.getMessage());

			e.printStackTrace();
		}finally
		{
			hiberTemplate.flush();
			//hiberTemplate.getSessionFactory().getCurrentSession().getTransaction().commit();
		}
		LOGGER.debug(" Exit From CustomerVO to insert are " + mqWorkorderVOList.size());

		return mqIdCustomerIdMap;
	
	}

	private CustomerVO xformToCustomerForBulkUpload(MQWorkorderVO mqWorkorderVO) {
		CustomerVO customerVo = new CustomerVO();

		if (mqWorkorderVO == null)
			return customerVo;

		LOGGER.debug(" Inside xformToCustomerForBulkUpload "  +mqWorkorderVO.getAreaId());
		
		customerVo.setId(StrictMicroSecondTimeBasedGuid.newGuidForTicket());
		customerVo.setAddedOn(new Date());

		if (mqWorkorderVO.getAreaId() != null)
			customerVo.setAreaId(Long.valueOf(mqWorkorderVO.getAreaId()));

		if (mqWorkorderVO.getBranchId() != null && !mqWorkorderVO.getBranchId().isEmpty())
			customerVo.setBranchId(Long.valueOf(mqWorkorderVO.getBranchId()));

		if (mqWorkorderVO.getCityId() != null)
			customerVo.setCityId(Long.valueOf(mqWorkorderVO.getCityId()));
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("communicationAdderss :: "+mqWorkorderVO.getCustomerAddress());
			CustomerAddressVO communicationAddress= mapper.readValue(mqWorkorderVO.getCustomerAddress(), CustomerAddressVO.class )  ;
			communicationAddress.setCustomerAddressType(AddressType.COMMUNICATION_ADDRESS);

			customerVo.setCommunicationAdderss(mapper.writeValueAsString(communicationAddress));
			
			CustomerAddressVO currentAddress= mapper.readValue(mqWorkorderVO.getCustomerAddress(), CustomerAddressVO.class )  ;
			currentAddress.setCustomerAddressType(AddressType.CURRENT_ADDRESS);

			customerVo.setCurrentAddress(mapper.writeValueAsString(currentAddress));
			
			CustomerAddressVO permanentAddress= mapper.readValue(mqWorkorderVO.getCustomerAddress(), CustomerAddressVO.class )  ;
			permanentAddress.setCustomerAddressType(AddressType.PERMANENT_ADDRESS);
			
			customerVo.setPermanentAddress(mapper.writeValueAsString(permanentAddress));
		} catch (Exception e) {
			LOGGER.error("Error While converting customer addreess :: " + e.getMessage());
		
			e.printStackTrace();
		}

		customerVo.setFirstName(mqWorkorderVO.getCustomerName());
		customerVo.setMobileNumber(mqWorkorderVO.getCustomerMobile());
		
		if (mqWorkorderVO.getAltMobileNumber() != null)
			customerVo.setAlternativeMobileNo(mqWorkorderVO.getAltMobileNumber());
		
		customerVo.setMqId(mqWorkorderVO.getMqId());

		LOGGER.debug(" Exit From xformToCustomerForBulkUpload " + mqWorkorderVO.getAreaId());
		return customerVo;
	}

	@Override
	public int updateCustomerMq(Long ticketId, String mqId, Date etr) {
		LOGGER.info("Updating mq and etr for ticket " + ticketId + " mqId " + mqId + " etr " + etr);

		int result = 0;
		if (ticketId == null || ticketId <= 0 || mqId == null || mqId.isEmpty() || etr == null)
			return result;
		try {
			String jdbcQuery = "update Customer c, Ticket t set t.currentEtr=?, t.commitedETR=?, c.mqId=?"
					+ " where t.customerId=c.id and t.id=?";

			result = jdbcTemplate.update(jdbcQuery, new Object[] { etr, etr, mqId, ticketId });

		} catch (Exception e) {
			LOGGER.error("Error in updating mq and etr for ticket " + ticketId + "" + e.getMessage());
			e.printStackTrace();
		}

		LOGGER.debug(" Exit From Updating mq and etr for ticket " + ticketId + " mqId "
				+ mqId + " etr " + etr);
		
		return result;
	}

	public CustomerVO getCustomerById(Long id) {
		Customer customer = null;
		CustomerVO vo = null;
		if (id == null || id.longValue() <= 0)
			return null;

		LOGGER.debug(" getting customer by id .. " + id);
		
		try {
		customer = hiberTemplate.load( Customer.class,id);
		vo = getCustomerVO(customer);
		} catch ( Exception e) {
			LOGGER.error("Error while getting Customer by id : " , e);
		}

		LOGGER.debug(" Exit From getting customer by id .. " + id);
		
		return vo;
	}

	public List<CustomerVO> getAllCustomer()
	{
		LOGGER.debug(" getting all customer data..  ");
		
		List<CustomerVO> custVO = new ArrayList<CustomerVO>();
		try {
			List list = hiberTemplate.find("from Customer");
			for (Iterator<Customer> it = list.iterator(); it.hasNext();) {
				Customer dbvo = (Customer) it.next();
				custVO.add(getCustomerVO(dbvo));
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting all Customers : ", e);
		}
		LOGGER.debug(" Exit From getting all customer data..  ");
		return custVO;
	}

	public void deleteCustomer(Long id) {
		Customer customer = null;
		if (id == null)
			throw new RuntimeException("Customer id should not be null");

		LOGGER.info(" deleting customer... " + id);
		try {
			List list = hiberTemplate.find("from Customer cust where cust.id = ?", id);

			if (list != null && list.size() > 0) {
				customer = (Customer) list.get(0);
				hiberTemplate.delete(customer);
			} else {
				LOGGER.info("Customer is not deleted with Id : " + id);
			}
		} catch (Exception e) {
			LOGGER.error("Error while deleting Customer : " + e.getMessage());
		}
	}

	public CustomerVO updateCustomer(CustomerVO customerVo, Long ticketId) {
		if (customerVo == null)
			return customerVo;

		LOGGER.info("Inside updateCustomer() of CustomerDAOImpl" + customerVo.getMobileNumber());

		try {
			if (customerVo != null && customerVo.getId() > 0) {
				Customer dbvo = hiberTemplate.load(Customer.class, customerVo.getId());

				if (dbvo != null) {

					LOGGER.debug("Retrived information of customer with customer id: " + customerVo.getId());
					dbvo.setTitle(customerVo.getTitle());
					dbvo.setFirstName(customerVo.getFirstName());
					dbvo.setMiddleName(customerVo.getMiddleName());
					dbvo.setLastName(customerVo.getLastName());
					dbvo.setEmailId(customerVo.getEmailId());
					dbvo.setAlternativeEmailId(customerVo.getAlternativeEmailId());
					dbvo.setMobileNumber(customerVo.getMobileNumber());
					dbvo.setAlternativeMobileNo(customerVo.getAlternativeMobileNo());
					dbvo.setCurrentAddress(customerVo.getCurrentAddress());
					dbvo.setRemarks(customerVo.getNotes());
					dbvo.setPincode(customerVo.getPincode());
					dbvo.setModifiedBy(AuthUtils.getCurrentUserId());
					dbvo.setModifiedOn(new Date());
					Ticket ticket = null;

					if (ticketId != null && ticketId > 0)
						ticket = hiberTemplate.load(Ticket.class, ticketId);

					if (ticket != null) {

						if (customerVo.getPrefferedCallDate() != null)
							ticket.setPreferedDate(customerVo.getPrefferedCallDate());

					} else {

						LOGGER.info("Ticket info not available for ticket id: " + ticketId
								+ ". So not able to set prefered date in ticket table");

					}
					Branch branch = (Branch) loadObject(customerVo.getBranchId(), Branch.class);
					dbvo.setBranch(branch == null ? null : branch);
					Area area = (Area) loadObject(customerVo.getAreaId(), Area.class);
					dbvo.setArea(area == null ? null : area);
					City city = (City) loadObject(customerVo.getCityId(), City.class);
					dbvo.setCity(city == null ? null : city);
					if (customerVo.getEnquiryModeId() != null && customerVo.getEnquiryModeId() > 0)
						dbvo.setEnquiry("" + customerVo.getEnquiryModeId());
					if (customerVo.getSourceModeId() != null && customerVo.getSourceModeId() > 0)
						dbvo.setKnownSource("" + customerVo.getSourceModeId());
					// Customer dbvo = xformToCustomer(customerVo);
					hiberTemplate.update(dbvo);
					LOGGER.info("Information of customer with id " + customerVo.getId() + " is updated in db");

				} else {

					LOGGER.info("Not able to retrive information of customer with customer id: " + customerVo.getId());

				}
			} else {

				LOGGER.info("customerVo is null");

			}
		} catch (Exception e) {
			LOGGER.error("Error while updating Customer : " + e.getMessage());
		}

		LOGGER.debug("Exit From updateCustomer() of CustomerDAOImpl" + customerVo.getMobileNumber());
		return customerVo;

	}

	private Customer xformToCustomer(CustomerVO dbvo) {
		Customer customer = new Customer();

		if (dbvo == null)
			return customer;

		LOGGER.info(" Inside xformToCustomer " + dbvo.getMobileNumber() + " for branch " + dbvo.getBranchId()
				+ " of city " + dbvo.getCityId() + ", Area " + dbvo.getAreaId());
		try {

			TariffPlan tariff = (TariffPlan) loadObject(dbvo.getTariffId(), TariffPlan.class);
			customer.setTariffPlan(tariff == null ? null : tariff);

			Branch branch = (Branch) loadObject(dbvo.getBranchId(), Branch.class);
			customer.setBranch(branch == null ? null : branch);

			Area area = (Area) loadObject(dbvo.getAreaId(), Area.class);
			customer.setArea(area == null ? null : area);

			City city = (City) loadObject(dbvo.getCityId(), City.class);
			customer.setCity(city == null ? null : city);

			SubscriptionType subscriptionType = (SubscriptionType) loadObject(dbvo.getSubscriptionTypeId(),
					SubscriptionType.class);

			customer.setSubscriptionType(subscriptionType == null ? null : subscriptionType);

			BeanUtils.copyProperties(dbvo, customer);
			customer.setEnquiry(dbvo.getEnquiryModeId() + "");
			customer.setKnownSource(dbvo.getSourceModeId() + "");
			// customer.setCommunicationAdderss(dbvo.getCurrentAddress());
			customer.setCurrentAddress(dbvo.getCurrentAddress());
			customer.setCommunicationAdderss(dbvo.getCommunicationAdderss());
			customer.setRemarks(dbvo.getNotes());
			customer.setLattitude(dbvo.getLat());
			customer.setLongitude(dbvo.getLon());
			customer.setPincode(dbvo.getPincode());
			customer.setPermanentAddress(dbvo.getPermanentAddress());

		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.debug(" Exit From xformToCustomer " + dbvo.getMobileNumber());
		return customer;
	}

	private String cretateCompleteName(CustomerVO dbvo) {
		if (dbvo == null)
			return null;
		
		LOGGER.info(" inside cretateCompleteName  " + dbvo.getMobileNumber());
		
		return dbvo.getTitle() + " " + dbvo.getFirstName() != null
				? dbvo.getFirstName()
				: "" + " " + dbvo.getMiddleName() != null ? dbvo.getMiddleName()
						: "" + " " + dbvo.getLastName() != null ? dbvo.getLastName() : "";

	}

	private CustomerVO getCustomerVO(Customer customer) {
		CustomerVO custVO = new CustomerVO();

		if (customer == null)
			return custVO;

		LOGGER.debug(" inside getCustomerVO " + customer.getMobileNumber());
		
		try {
			BeanUtils.copyProperties(customer, custVO);

			custVO.setCustomerName(GenericUtil.completeCustomerName(custVO));

			custVO.setTariffId(customer.getTariffPlan() != null ? customer.getTariffPlan().getId() : null);
			custVO.setBranchId(customer.getBranch() != null ? customer.getBranch().getId() : null);
			custVO.setAreaId(customer.getArea() != null ? customer.getArea().getId() : null);
			custVO.setCityId(customer.getCity() != null ? customer.getCity().getId() : null);
			custVO.setSubscriptionTypeId(
					customer.getSubscriptionType() != null ? customer.getSubscriptionType().getId() : null);

			Adhaar adhaar = customer.getAdhaarId();
			if(adhaar != null && adhaar.getUid() != null)
				custVO.setAadhaarNumber(adhaar.getUid());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.debug(" Exit From getCustomerVO " + customer.getMobileNumber());
		return custVO;
	}

	private Object loadObject(Long id, Class clasz) {
		if (id == null || id <= 0)
			return null;

		LOGGER.debug("loadObject " + id);
		try {
			Object object = hiberTemplate.load(clasz, id);
			return object;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("ERROR IN LOADINGG.." + id);
		}

		LOGGER.debug(" Exit From loadObject " + id);
		return null;
	}

	@Override
	public long getCustomerByMqId(String mqId) {
		if (mqId == null || mqId.isEmpty())
			return 0l;
		
		LOGGER.info(" getCustomerByMqId " + mqId);
		
		try {
			return jdbcTemplate.queryForLong("select id from Customer where mqId=?", new Object[] { mqId });
		} catch (Exception e) {
			LOGGER.error("Error in getCustomerByMqId");
			e.printStackTrace();
			return 0l;
		}

	}

	@Override
	public CustomerVO getCustomerByTicketId(Long ticketId) {
		if (ticketId == null || ticketId.longValue() <= 0)
			return null;

		LOGGER.info(" inside getCustomerByTicketId " + ticketId);

		try {
			List<Customer> customers = hiberTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(GET_CUSTOMER_BY_TICKET_ID).addEntity(Customer.class)
					.setParameter("ticketId", ticketId).list();

		if (customers != null && !customers.isEmpty())
		{
			Customer customer = customers.get(0);
			CustomerVO customerVO = getCustomerVO(customer);
			LOGGER.info("Returing Customer Vo " +customerVO ); 
			return customerVO;
		}
		} catch (Exception e) {
			LOGGER.error(" Error while getting customer by ticket id " ,e);
	}
		LOGGER.debug(" Exit From getCustomerByTicketId " + ticketId );
		return null;

	}
	
	@Override
	public AuditLogVo getCustomerDetailByTicketId(Long ticketId)
	{

		if( ticketId == null || ticketId.longValue() <= 0 )
			return new AuditLogVo();
		
		LOGGER.debug(" getCustomerTypeByTicketId " + ticketId);
		try 
		{
			List<Object[]> customers = hiberTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery( GET_CUST_DETAIL_BY_TICKET_ID )
					.setParameter("ticketId", ticketId )
					.list();

			return xformToAuditLogVo( customers ) ;
			
		} catch ( Exception e) {
			LOGGER.error("Error occured while get Audit log data for ticket Id : "+ticketId);
			e.printStackTrace();
			return new AuditLogVo();
		}
	
	}
	

	private AuditLogVo xformToAuditLogVo(List<Object[]> customers)
	{
		AuditLogVo auditVo = new AuditLogVo();
		if( customers == null || customers.isEmpty() )
			return auditVo;
		
		LOGGER.info("Total recoud found : "+ customers.size());
		
		for( Object[] row : customers )
		{
			
			auditVo.setCityId(FrTicketUtil.objectToLong(row[0]));
			auditVo.setCityCode(FrTicketUtil.objectToString(row[1]));
			auditVo.setProspectNo( FrTicketUtil.objectToString(row[2]));
			
			auditVo.setTicketId(FrTicketUtil.objectToLong(row[3]));
			auditVo.setCustomerName(FrTicketUtil.objectToString(row[4]) +" "
					+FrTicketUtil.objectToString(row[5]));
			auditVo.setCustomerId(FrTicketUtil.objectToLong(row[6]));
			
			auditVo.setCustMobileNumber(FrTicketUtil.objectToString(row[7]));
			auditVo.setPackageName(FrTicketUtil.objectToString(row[8]));
			auditVo.setPlanCode(FrTicketUtil.objectToString(row[9]));
						
			auditVo.setPlanType(FrTicketUtil.objectToString(row[10]));
			auditVo.setPlanCategoery(FrTicketUtil.objectToString(row[11]));
			auditVo.setPaymentMode(FrTicketUtil.objectToString(row[12]));
			
			auditVo.setPaidAmount(FrTicketUtil.objectToString(row[13]));
			auditVo.setPaymentReferenceNo(FrTicketUtil.objectToString(row[14]));
			auditVo.setPaymentRemark(FrTicketUtil.objectToString(row[15]));
			
			auditVo.setTicketStatus(FrTicketUtil.objectToInt(row[16]));
			auditVo.setMqStatus(FrTicketUtil.objectToInt(row[17]));
			auditVo.setPaymentStatus(FrTicketUtil.objectToInt(row[18]));
			
			auditVo.setDocumentRemark(FrTicketUtil.objectToString(row[19]));
			auditVo.setDocumentStatus(FrTicketUtil.objectToInt(row[20]));
			auditVo.setTicketCreationDate(GenericUtil.convertToUiDateFormat(row[21]) );
			
			auditVo.setAddedOn(GenericUtil.convertToUiDateFormat(row[22]) );
			auditVo.setCityName(FrTicketUtil.objectToString(row[23]));
			auditVo.setEmailId(FrTicketUtil.objectToString(row[24]));
			
			auditVo.setMqId( FrTicketUtil.objectToString(row[25]));
			auditVo.setCommunicationETR(FrTicketUtil.convertObjectToDate(row[26]));
			auditVo.setPreferedDate(FrTicketUtil.convertObjectToDate(row[27]));

			auditVo.setBranchName(FrTicketUtil.objectToString(row[28]) );
			auditVo.setAreaName( FrTicketUtil.objectToString(row[29]));
			auditVo.setAssignedToName( FrTicketUtil.objectToString(row[30]));
			
			auditVo.setCurrentAssignedToId(FrTicketUtil.objectToLong(row[31]));
			auditVo.setAssignedMobileNumber(FrTicketUtil.objectToString(row[32]));
			auditVo.setCommunicationAdderss(FrTicketUtil.objectToString(row[33]));
			
			break;
		}
		
		return auditVo;
	}

	
	@Override
	public long getCustomerTypeByTicketId(Long ticketId)
	{
		if(ticketId == null || ticketId.longValue() <= 0)
			return 0l;
		
		LOGGER.info(" getCustomerTypeByTicketId " + ticketId );
		
		List<Object[]> result = null;
		try {
			result = hiberTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("select c.status from Customer c inner join Ticket t on c.id=t.customerId where t.id =:ticketId ")
				 .setParameter("ticketId", ticketId)
				.list();
		
		if (result == null || result.isEmpty() || result.get(0) == null)
			return 0;
		else
		{
			return Long.valueOf(result.get(0)+"");
		}
		} catch (Exception e) {
			LOGGER.error("Error while getting CustomerType by ticket id : " + e.getMessage());
		}
		LOGGER.debug("Exit From getCustomerTypeByTicketId " + ticketId );
		
		return 0;

	}

	@Override
	public int updateCustomerByTicketId(Long ticketId, int status, String remarks) {
		int result = 0;

		if (ticketId == null || ticketId.longValue() <= 0)
			return result;

		LOGGER.info("Updating customer prospect type for ticket " + ticketId + " status as  " + status);
		try {
			String jdbcQueryUpdateCustomerByTicketId = "update Customer c join Ticket t on t.customerId=c.id set c.status=?, c.remarks=? where t.id=?";

			result = jdbcTemplate.update(jdbcQueryUpdateCustomerByTicketId, new Object[] { status, remarks, ticketId });

		} catch (Exception e) {
			LOGGER.error("Updating customer prospect type for ticket " + ticketId + " status as  " + status + ""
					+ e.getMessage());
			e.printStackTrace();
		}

		LOGGER.debug(" Exit From Updating customer prospect type for ticket " + ticketId + " status as  " + status);
		return result;
	}

	@Override
	public List<Customer> getCustomersByTicketIdList(List<BigInteger> ticketIds) {
		if (ticketIds == null || ticketIds.isEmpty())
			return new ArrayList<Customer>();

		LOGGER.debug("TickettList " + ticketIds);

		List<Customer> customers = hiberTemplate.getSessionFactory().getCurrentSession()
				.createSQLQuery("select c.* from Customer c, Ticket t where c.id=t.customerId and t.id in (:ticketIds)")
				.addEntity(Customer.class).setParameterList("ticketIds", ticketIds).list();

		/*
		 * List<CustomerVO> customerVOs = new ArrayList<>();
		 * 
		 * for (Iterator<Customer> iterator = customers.iterator();
		 * iterator.hasNext();) { Customer customer = (Customer)
		 * iterator.next(); customerVOs.add(getCustomerVO(customer));
		 * 
		 * }
		 */

		LOGGER.debug(" Exit From getCustomersByTicketIdList " + ticketIds);
		return customers;

	}

	@Override
	@Transactional
	public int updateNeComment(Long ticketNo, String neComment) {
		if (ticketNo == null || ticketNo.longValue() <= 0 || neComment == null || neComment.isEmpty())
			return 0;
		LOGGER.info("  Inside updateNeComment "  +ticketNo + " neComment  " + neComment);
		try
		{
			LOGGER.debug(" updating NE comment for ticket no " + ticketNo
					+ " message " + neComment);

			return jdbcTemplate.update(
					"update Customer c inner join Ticket t on t.customerId=c.id "
							+ "set c.neComment=?, c.modifiedOn=now() where t.id=?",
					new Object[] { neComment, ticketNo });
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error while updating NE comment for ticket no " + ticketNo + " message " + e.getMessage());

			LOGGER.info("  Exit From updateNeComment " + ticketNo + " neComment  " + neComment);
			return 0;
		}
	}

	@Override
	public CustomerVO updateCustomerDetails(CustomerVO customerVo) {

		if (customerVo == null)
			return customerVo;

		LOGGER.info("Inside updateCustomer() of CustomerDAOImpl" + customerVo.getMobileNumber());

		try {
			if (customerVo != null && customerVo.getId() > 0) {
				Customer dbvo = hiberTemplate.load(Customer.class, customerVo.getId());

				if (dbvo != null) {

					LOGGER.info("Retrived information of customer with customer id: " + customerVo.getId());
					if (customerVo.getTitle() != null && !customerVo.getTitle().isEmpty())
						dbvo.setTitle(customerVo.getTitle());
					if (customerVo.getFirstName() != null && !customerVo.getFirstName().isEmpty())
						dbvo.setFirstName(customerVo.getFirstName());
					if (customerVo.getMiddleName() != null && !customerVo.getMiddleName().isEmpty())
						dbvo.setMiddleName(customerVo.getMiddleName());
					if (customerVo.getLastName() != null && !customerVo.getLastName().isEmpty())
						dbvo.setLastName(customerVo.getLastName());
					if (customerVo.getEmailId() != null && !customerVo.getEmailId().isEmpty())
						dbvo.setEmailId(customerVo.getEmailId());
					if (customerVo.getAlternativeEmailId() != null && !customerVo.getAlternativeEmailId().isEmpty())
						dbvo.setAlternativeEmailId(customerVo.getAlternativeEmailId());
					if (customerVo.getMobileNumber() != null && !customerVo.getMobileNumber().isEmpty())
						dbvo.setMobileNumber(customerVo.getMobileNumber());
					if (customerVo.getAlternativeMobileNo() != null && !customerVo.getAlternativeMobileNo().isEmpty())
						dbvo.setAlternativeMobileNo(customerVo.getAlternativeMobileNo());
					if (customerVo.getCurrentAddress() != null && !customerVo.getCurrentAddress().isEmpty())
						dbvo.setCurrentAddress(customerVo.getCurrentAddress());
					if(customerVo.getCommunicationAdderss() != null && !customerVo.getCommunicationAdderss().isEmpty())
						dbvo.setCommunicationAdderss(customerVo.getCommunicationAdderss());
					if (customerVo.getNotes() != null && !customerVo.getNotes().isEmpty())
						dbvo.setRemarks(customerVo.getNotes());
					if (customerVo.getPincode() != null && customerVo.getPincode() > 0)
						dbvo.setPincode(customerVo.getPincode());
					dbvo.setModifiedBy(AuthUtils.getCurrentUserId());
					dbvo.setModifiedOn(new Date());

					hiberTemplate.update(dbvo);
					LOGGER.info("Information of customer with id " + customerVo.getId() + " is updated in db");

				} else {

					LOGGER.info("Not able to retrive information of customer with customer id: " + customerVo.getId());

				}
			} else {

				LOGGER.info("customerVo is null");

			}
		} catch (Exception e) {
			LOGGER.error("Error while updating Customer : " + e.getMessage());
		}

		LOGGER.debug("Exit From updateCustomer() of CustomerDAOImpl" + customerVo.getMobileNumber());

		return customerVo;

	}

	public int updatePOIOrPOAWhileActivity(Long ticketId, int poi, int poa) {
		if (ticketId == null || ticketId < 0)
			return 0;
		if (poi == 0 && poa == 0)
			return 0;

		try {
			LOGGER.info(" updating POI or POI flag for ticket no " + ticketId + " poi " + poi + "poa" + poa);
			StringBuilder completeQuery = new StringBuilder(
					"update Customer c inner join Ticket t on t.customerId=c.id set ");
			if (poa > 0)
				completeQuery.append(" c.poa=? ");
			if (poi > 0)
				completeQuery.append(" c.poi=? ");

			completeQuery.append(" where t.id=? ");

			Object[] input = null;
			if(poa > 0)
				input = new Object[] {
						poa,ticketId };
			if(poi > 0)
				input = new Object[] {
						poi,ticketId };
	
			
			
			return jdbcTemplate
					.update(completeQuery.toString(), input);
		} catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("Error while updating POI or POI flag for ticket no " + ticketId + e.getMessage());

			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.customer.CustomerDAO#populateCustomerAadhaarMapping(com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping)
	 */
	@Override
	public CustomerAadhaarMappingVo populateCustomerAadhaarMapping(CustomerAadhaarMapping aadhaarMapping) {

		CustomerAadhaarMappingVo aadhaarMappingVo = new CustomerAadhaarMappingVo();
		
		LOGGER.info("Adding Customer Aadhar mapping Vo in DB :"+aadhaarMapping);
		
		try {
			
			 hiberTemplate.getSessionFactory().getCurrentSession().saveOrUpdate(aadhaarMapping);
		
			LOGGER.info("Added Customer Aadhar mapping Vo in DB :"+aadhaarMapping );
			 
			BeanUtils.copyProperties(aadhaarMapping, aadhaarMappingVo);
			
		} catch (Exception e) {
			LOGGER.error("Exception in saving CustomerAadhaarMappingVo :"+aadhaarMapping);
			e.printStackTrace();
		}
		return aadhaarMappingVo;
	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.customer.CustomerDAO#updateAadhaarDetails(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public int updateAadhaarDetails(Long customerId, Long aadharId, Long ticketId) {
		int result = 0;

		try {
			result = jdbcTemplate.update(UPDATE_AADHAR_DETAILS_IN_CUSTOMER,
					new Object[] { aadharId, ticketId, customerId });
		} catch (Exception e) {
			LOGGER.error("Error while updating aadhar details for ticket " + ticketId + " customer " + customerId
					+ " and aadhaarId " + aadharId + " " + e.getMessage());
			e.printStackTrace();

		}

		return result;

	}
	
	@Override
	public String getCustomerAadhaarMapping(Long customerId, Long ticketId)
	{

		String result = "";
		String docType = "";

		try {
			List<CustomerAadhaarMapping> customerAadhaarMappings = hiberTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(
							"select * from CustomerAadhaarMapping where customerId=:customerId and ticketId=:ticketId")
					.addEntity(CustomerAadhaarMapping.class).setParameter("customerId", customerId)
					.setParameter("ticketId", ticketId).list();

			if (customerAadhaarMappings != null && !customerAadhaarMappings.isEmpty()) {

				for (CustomerAadhaarMapping customerAadhaarMapping : customerAadhaarMappings) {

					docType = "";
					if (customerAadhaarMapping.getActivityId() != null) {

						if (customerAadhaarMapping.getActivityId() == FWMPConstant.Activity.POA_DOCUMENT_UPDATE)
							docType = FWMPConstant.Activity.POA_STATUS;

						if (customerAadhaarMapping.getActivityId() == FWMPConstant.Activity.POI_DOCUMENT_UPDATE)
							docType = FWMPConstant.Activity.POI_STATUS;

						if (customerAadhaarMapping.getActivityId() == FWMPConstant.Activity.PAYMENT_UPDATE)
							docType = FWMPConstant.Activity.PAYMENT_STATUS;
					}
					if (result.isEmpty()) {

						result = customerAadhaarMapping.getAdhaarTrnId() + "_" + docType;

					} else {

						result = result + ", " + customerAadhaarMapping.getAdhaarTrnId() + "_" + docType;
					}
				}
			}

		} catch (Exception e) {

			LOGGER.error("Error while getting Aadhaar Mapping details for ticket " + ticketId + " customer "
					+ customerId + " " + e.getMessage());
			e.printStackTrace();
		}

		return result;

	}

	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.customer.CustomerDAO#getCustomerAadhaarMappingByLable(java.lang.Long, java.lang.Long, int)
	 */
	@Override
	public CustomerAadhaarMapping getCustomerAadhaarMappingByLable(Long customerId, Long ticketId, int lable) {
		
		try {
			List<CustomerAadhaarMapping> customerAadhaarMappings = hiberTemplate.getSessionFactory().getCurrentSession()
					.createSQLQuery(
							"select * from CustomerAadhaarMapping where customerId=:customerId and ticketId=:ticketId and label = :label")
					.addEntity(CustomerAadhaarMapping.class).setParameter("customerId", customerId)
					.setParameter("ticketId", ticketId)
					.setParameter("label", lable).list();

			if (customerAadhaarMappings != null && !customerAadhaarMappings.isEmpty()) {

			  return customerAadhaarMappings.get(0);
			}

		} catch (Exception e) {

			LOGGER.error("Error while getting Aadhaar Mapping details for ticket " + ticketId + " customer "
					+ customerId + " " + e.getMessage());
			e.printStackTrace();
		}
		return new CustomerAadhaarMapping();


	
	}
	
	
}