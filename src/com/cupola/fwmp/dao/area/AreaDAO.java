package com.cupola.fwmp.dao.area;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.vo.AreaVO;

public interface AreaDAO {

	public AreaVO addArea(Area area);

	public Area getAreaById(Long areaId);

	public Area getAreaByAreaName(String areaName);
	
	public List<AreaVO> getAllArea();

	public Area deleteArea(Long id);

	public Area getUpdateArea(Area area);

	public Map<Long, String> getAreasByBranchId(String branchId);
	
	public List<Long> getAllAreaIds();

	Map<Long, String> getAreasByBranchName(String branchName);

	Map<Long, String> getAreasByBranchIds(List<Long> branchIds);
	
	String getAreaCodeByName(String areaName);
	
	String getAreaNameById(Long id);
	
	String getAreaCodeById(Long id);

	Long getAreaIdByName(String name);
	
	Long getIdByCode(String code);

	Long getDefaultAreaByCity(Long cityId);

	Area getAreaByAreaCode(String areaCode);
	
	int updateUserArea(Long cityId);

	public Map<String, Long> getAreaInfoIdFromTicketId(Long ticketNo);

	/**@author aditya
	 * void
	 * @param area
	 */
	void updateAreaCache(Area area);
	
	//for getting area id if cache has wrong values
		Long getAreaIdForBulk(String areaName,Long userid);
	
	 
}
