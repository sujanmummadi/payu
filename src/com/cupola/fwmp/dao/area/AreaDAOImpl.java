package com.cupola.fwmp.dao.area;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.DBActiveStatus;
import com.cupola.fwmp.dao.branch.BranchDAO;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.AreaVO;
import com.cupola.fwmp.vo.BranchVO;

@Transactional
public class AreaDAOImpl implements AreaDAO
{

	private Logger log = Logger.getLogger(AreaDAOImpl.class.getName());

	HibernateTemplate hibernetTemplate;

	public void sethibernetTemplate(HibernateTemplate hibernetTemplate)
	{

		this.hibernetTemplate = hibernetTemplate;

	}

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	BranchDAO branchDAO;
	@Autowired
	DefinitionCoreService definitionCoreService;
	
	private static final String GET_ALL_AREAS = "select * from Area where status=1 ";

	private static final String GET_DEFAULT_AREA_BY_CITY = "SELECT a.id FROM Area a join Branch b on a.branchId = b.id join City c on c.id = b.cityId where c.id = :cityId and a.areaName = 'DEFAULT'";

	public static Map<Long, AreaVO> areaIdAreaDetails = new ConcurrentHashMap<Long, AreaVO>();

	public static Map<Long, String> areaIdAndAreaNameMap = new ConcurrentHashMap<Long, String>();

	public static Map<String, Long> areaNameAndAreaIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<String, String> areaNameAndAreaCodeMap = new ConcurrentHashMap<String, String>();

	public static Map<Long, String> areaIdAndAreaCodeMap = new ConcurrentHashMap<Long, String>();

	public static Map<String, Long> areaCodeAndAreaIdMap = new ConcurrentHashMap<String, Long>();

	public static Map<String, Area> areaCodeAndAreaMap = new ConcurrentHashMap<String, Area>();

	private static final String QUERY_FOR_AREA_ID = " SELECT a.id FROM Area a "
			+ " join Branch b on b.id=a.branchId "
			+ "join UserBranchMapping ub on ub.idBranch=b.id "
			+ "join User u on u.id=ub.idUser "
			+ "WHERE a.areaName in (:areaName) and u.id = :userId and u.status = "+DBActiveStatus.ACTIVE +"";
	
	
	@SuppressWarnings("unused")
	private void init()
	{

		log.info("Area init method called..");
		getAreas();
		log.info("Area init method executed.");

	}

	@Transactional
	public AreaVO addArea(Area area)
	{
		AreaVO areaVO = new AreaVO();

		if (area == null)
			return null;

		try
		{

			/*
			 * if(areaNameAndAreaIdMap.containsKey(area.getAreaName()))
			 * 
			 * return area;
			 */
			List<AreaVO> areas = new ArrayList<AreaVO>(areaIdAreaDetails
					.values());

			for (AreaVO area1 : areas)
			{
				if (area1.getAreaName().equalsIgnoreCase(area.getAreaName()))
				{

					if (area1.getBranchName()
							.equalsIgnoreCase(area.getBranch().getBranchName()))
					{

						if (area1.getBranchCode().equalsIgnoreCase(area
								.getBranch().getBranchCode()))
						{

							// if (area1.getCityCode().equalsIgnoreCase(area
							// .getBranch().getCity().getCityCode()))

							BeanUtils.copyProperties(area, areaVO);
							areaVO.setRemarks(ResponseUtil
									.createDuplicateResponse()
									.getStatusMessage());

							return areaVO;
						} else
						{
							log.info("City Branch mismatch " + area);
						}
					}

				}
			}

			area = addCommonInitialValues(area);
			area.setAreaDescription(area.getAreaDescription());
			hibernetTemplate.save(area);

			log.info("Area with id: " + area.getId() + " is added sucessfully");

			BeanUtils.copyProperties(area, areaVO);

			Branch branch = area.getBranch();
			areaVO.setBranchId(branch.getId());
			BranchVO branchVO = branchDAO.getBranchById(areaVO.getBranchId());
			areaVO.setBranchCode(branch.getBranchCode());
			areaVO.setBranchName(branch.getBranchName());
			areaVO.setCityCode(branchVO.getCityCode());

			addToCache(areaVO);
			addToAreaCache(area);

			log.debug("area vo after adding ======= " + areaVO.getAreaName());
			
		} catch ( Exception e)
		{

			log.error("Error while adding area " + area.getAreaName()
					+ ", Message " + e.getMessage());

			e.printStackTrace();
		}
		BeanUtils.copyProperties(area, areaVO);
		areaVO.setRemarks(ResponseUtil.createSuccessResponse().getStatusMessage());
		return areaVO;
	}

	public Area updateArea(Area area)
	{
		
		if (area == null || area.getId() <= 0)
			return null;
		
		log.info(" Updating Area data " + area.getAreaName());

		try {
		hibernetTemplate.update(area);
		} catch (Exception e) {
			log.error("Error in update the Area");
			e.printStackTrace();
		}
		log.info(" Exit From Updating Area data " + area.getAreaName());

		return area;
	}

	public Area deleteArea(Long areaId)
	{
		log.info(" Inside delete Area " +areaId);

		if (areaId == null || areaId.longValue() <= 0)
			return null;
		Area area = null;
		try {
			area =  hibernetTemplate.get(Area.class, areaId);

		hibernetTemplate.delete(area);
		} catch ( Exception e) {
			log.error("Error occur while deteting Area...");
			e.printStackTrace();
		}
		
		log.info(" Exit From delete Area " +areaId);
		
		return area;
	}

	@Override
	public Long getDefaultAreaByCity(Long cityId)
	{
		log.info(" Getting default area by city ID" + cityId);

		if(cityId == null || cityId <= 0)
			return null;

		try
		{
			List result = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery(GET_DEFAULT_AREA_BY_CITY)
					.setParameter("cityId", cityId).list();

			if (result == null || result.isEmpty())
				return null;

			if (result != null && result.size() > 0)
				return Long.valueOf(result.get(0).toString());

		} catch (NumberFormatException e)
		{
			log.error("Error while getting default area for city " + cityId
					+ " . " + e.getMessage());
		} catch (Exception e)
		{
			log.error("Error while getting default area for city " + cityId
					+ " . " + e.getMessage());
		}
		log.debug(" Exit From Getting default area by city ID" + cityId);

		return null;
	}

	public Area getAreaById(Long areaId)
	{
		log.debug(" Inside getting Area ById " +areaId);
		
		if (areaId == null || areaId <= 0)
			return null;

		try {
			return hibernetTemplate.load(Area.class, areaId);
		} 
		catch ( Exception e) {
			log.error("Error occured while loading Area by area Id : "+areaId,e);
			e.printStackTrace();
			return null;
		}
		/*
		 * if (!areaIdAreaDetails.containsKey(areaId)) {
		 * 
		 * return null;
		 * 
		 * } else {
		 * 
		 * if (areaIdAreaDetails.containsKey(areaId)) {
		 * 
		 * Area area = areaIdAreaDetails.get(areaId);
		 * 
		 * return area;
		 * 
		 * } else {
		 * 
		 * return null;
		 * 
		 * } }
		 */
	}

	public List<AreaVO> getAllArea()
	{
		log.debug(" Inside Getting all areas...");

		if (areaIdAreaDetails.isEmpty())
			return null;

		List<AreaVO> areaList = new ArrayList<AreaVO>(areaIdAreaDetails
				.values());

		log.debug(" Exit from Getting all areas...");
		return areaList;
	}

	@Override
	public Map<Long, String> getAreasByBranchName(String branchName)
	{
		log.debug("geting AreasByBranchName " +branchName);

		Map<Long, String> areaTypeAheadList = new TreeMap<>();

		if(branchName == null || branchName.isEmpty())
			return areaTypeAheadList;

		log.debug("branch name::" + branchName);

		List<AreaVO> areaVOs = new ArrayList<AreaVO>(areaIdAreaDetails
				.values());

		for (AreaVO areaVO : areaVOs)
		{
			if (areaVO.getBranchName().equalsIgnoreCase(branchName))
			{
				areaTypeAheadList.put(areaVO.getId(), areaVO.getAreaName());
			}

		}

		/*
		 * if(branchName != null){
		 * 
		 * List<Branch> branches = (List<Branch>) hibernetTemplate.find(
		 * "from Branch where branchName like?", "%"+branchName+"%");
		 * 
		 * if(branches!=null){
		 * 
		 * log.info("list of branches size in branch suggestion : "
		 * +branches.size());
		 * 
		 * Branch branch = branches.get(0);
		 * 
		 * Set<Area> areas = branch.getAreas();
		 * 
		 * for (Area area : areas) {
		 * 
		 * areaTypeAheadList.put(area.getId(), area.getAreaName()); }
		 * 
		 * }
		 * 
		 * log.info("areas :"+areaTypeAheadList+" for branch:"+branchName);
		 * 
		 * }
		 */
		log.debug(" Exit From geting AreasByBranchName " +branchName);

		return areaTypeAheadList;

	}

	private void getAreas()
	{

		/*
		 * log.info("########## getAreas START hibernetTemplate ##########"+new Date());
		 * List<Area> areas = (List<Area>) hibernetTemplate.find( "from Area");
		 * log.info( "########## getAreas END hibernetTemplate ##########"+new Date());
		 */

		log.debug("########## getAreas START jdbcTemplate ##########" + new Date());
		List<AreaVO> areaList = jdbcTemplate.query(GET_ALL_AREAS, new BeanPropertyRowMapper(AreaVO.class));
		log.debug("########## getAreas END jdbcTemplate ##########");

		if (areaList == null || areaList.isEmpty()) {

			this.log.info("Area details are not available");
			return;

		} else
		{

			log.debug("########## addToCache START ##########" + new Date());

			for (AreaVO area : areaList) 
			{
				BranchVO branchVO = branchDAO.getBranchById(area.getBranchId());

				if (branchVO != null) {
					area.setBranchCode(branchVO.getBranchCode());
					area.setBranchName(branchVO.getBranchName());
					area.setCityCode(branchVO.getCityCode());
					addToCache(area);
				}
			}

			log.debug("########## addToCache END ##########" + new Date());
		}
		// System.exit(0);
	}

	private void addToCache(AreaVO area)
	{
		if (area == null)
			return;

		log.debug("adding Area ToCache" +area.getAreaName());
		
		areaIdAreaDetails.put(area.getId(), area);

		String areaName = area.getAreaName();
		
		String branchAndAreaName = area.getBranchName()+"_"+area.getAreaName();

		areaIdAndAreaNameMap.put(area.getId(), areaName);

		areaNameAndAreaCodeMap.put(areaName, area.getAreaCode());

		areaIdAndAreaCodeMap.put(area.getId(), area.getAreaCode());

		areaNameAndAreaIdMap.put(branchAndAreaName, area.getId());

		areaCodeAndAreaIdMap.put(area.getAreaCode(), area.getId());

		log.debug("Exit From adding Area ToCache" +area.getAreaName());
	}

	@Override
	public Area getAreaByAreaCode(String areaCode)
	{
		if(areaCode == null || areaCode.isEmpty())
			return null;
		
		log.debug(" inside getArea ByAreaCode " +areaCode);
		
		if (areaCodeAndAreaMap.containsKey(areaCode))
			return areaCodeAndAreaMap.get(areaCode);
		else
			log.info(" Exit From getArea By AreaCode from cache " +areaCode);
			return null;

	}

	private void addToAreaCache(Area area)
	{
		if(area == null)
			return;
		
		log.debug(" Inside addToAreaCache ... "  +area.getAreaName());

		areaCodeAndAreaMap.put(area.getAreaCode(), area);
	}

	private void removeFromCache(Long areaId)
	{
		if(areaId == null || areaId.longValue() <= 0)
			return;
		
		log.debug(" inside removeFromCache "  +  areaId);

		if (areaIdAreaDetails.containsKey(areaId))
		{

			areaIdAreaDetails.remove(areaId);
		}

	}

	public Area addCommonInitialValues(Area area)
	{
		log.debug(" Inside adding Area CommonInitialValues "  + area.getAreaName());

		area.setId(StrictMicroSecondTimeBasedGuid.newGuid());
		area.setAddedOn(new Date());
		area.setModifiedOn(new Date());
		area.setAddedBy(AuthUtils.getCurrentUserId());
		area.setModifiedBy(AuthUtils.getCurrentUserId());
		area.setStatus(FWMPConstant.Status.ACTIVE);
		
		log.debug(" Exit From adding Area CommonInitialValues "  + area.getAreaName());
		return area;

	}

	@SuppressWarnings("unchecked")
	public Map<Long, String> getAreasByBranchId(String branchId)
	{
		log.debug(" Inside getting Areas By BranchId == " +branchId);

		Map<Long, String> areaNames = new TreeMap<>();

		try {
		if (branchId == null || branchId.equals("null") || branchId.equals("0"))
		{
		    log.debug("get all branches");
		 
			List<Object[]> branches = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT id , areaName FROM Area where status=1 ")
					.list();
				if (branches != null && !branches.isEmpty())
			{
				for (Object[] obj : branches)
				{
					areaNames.put(((BigInteger) obj[0])
							.longValue(), ((String) obj[1]));

				}
			}

			else
					log.info("No branch found");
		}

		else
		{

			Long id = Long.valueOf(branchId);

			List<Object[]> areas = hibernetTemplate.getSessionFactory()
					.getCurrentSession()
					.createSQLQuery("SELECT id , areaName FROM Area where branchId =:id and status=1")
					.setParameter("id", id).list();
				if (areas != null && !areas.isEmpty())
			{
				for (Object[] obj : areas)
				{
					areaNames.put(((BigInteger) obj[0])
							.longValue(), ((String) obj[1]));

				}
			}

		}
		}
		catch (Exception e)
		{
			log.error("Error occured while getting all areas by branch Id :"+branchId,e);
		} 
		
		log.debug(" Exit From getting Areas By BranchId == " +branchId);

		return areaNames;

	}

	@Override
	public Map<Long, String> getAreasByBranchIds(List<Long> branchIds)
	{
		Map<Long, String> areaNames = new TreeMap<>();

		if (branchIds == null || branchIds.isEmpty())
			return areaNames;
		
		log.debug(" Inside getting  Areas by branch Ids : "+branchIds);
		
		try {
		List<Object[]> areas = hibernetTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("SELECT id , areaName FROM Area where branchId in (:id) and status=1")
				.setParameterList("id", branchIds).list();
			if (areas != null && !areas.isEmpty())
		{
			for (Object[] obj : areas)
			{
				areaNames.put(((BigInteger) obj[0])
						.longValue(), ((String) obj[1]));

			}
		}

		else
		{

			log.info("No area found for branches: " + branchIds);
		}
		} catch (Exception e) {
			 log.error(" Error occured while getting areas by branch ids : "+branchIds,e);
		}
		log.debug(" Exit From getting  Areas by branch Ids : "+branchIds);

		return areaNames;

	}

	@Override
	public Area getUpdateArea(Area area)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public Area getAreaByAreaName(String areaName)
	{
		if (areaName == null || areaName.isEmpty())
			return null;
		
		log.debug("Inside getting  Areas by areaName : "+areaName);
		
		try {

		List<Area> areas = hibernetTemplate.getSessionFactory()
				.getCurrentSession()
				.createSQLQuery("select * from Area where areaName = :areaName")
				.addEntity(Area.class).setParameter("areaName", areaName)
				.list();
			
			if (areas != null && !areas.isEmpty())
		{
			return areas.get(0);
		}
		} catch (Exception e) {
			log.error("Error occured while getting area by area name :"+areaName,e);
		}

		log.debug("Exit From getting  Areas by areaName : "+areaName);
		return null;

		// if(areaIdAreaDetails.size() == 0)
		// getAreas();
		//
		// List<Area> areaList = new ArrayList<Area>(
		// areaIdAreaDetails.values());
		// for (Area area : areaList)
		// {
		// if(area.getAreaName().equalsIgnoreCase(areaName))
		//
		// return area;
		// }
		// return null;

		/*
		 * List<Area> areas = (List<Area>) hibernetTemplate.find(
		 * "from Area where areaName=?", areaName);
		 * 
		 * if(areas != null)
		 * 
		 * return areas.get(0);
		 */

		// return area;
	}

	@Override
	public String getAreaCodeByName(String areaName)
	{
		if(areaName == null || areaName.isEmpty())
			return "";
		
		log.debug(" Inside getting  Area code by areaName : "+areaName);
		
		if (areaNameAndAreaCodeMap.containsKey(areaName))
		{
			return areaNameAndAreaCodeMap.get(areaName);
		} else
			return "";
	}

	@Override
	public String getAreaNameById(Long id)
	{
		if(id == null || id.longValue()<= 0)
			return "";
		
		log.debug(" Inside getting  Areas name by id : "+id);
		
		if (areaIdAndAreaNameMap != null
				&& areaIdAndAreaNameMap.containsKey(id))
		{

			return areaIdAndAreaNameMap.get(id);

		} else
			return "";
	}

	@Override
	public String getAreaCodeById(Long id)
	{
		if(id == null || id.longValue()<= 0)
			return  "";
		
		log.debug("Inside getting Areas code by id : "+id);
		
		if (areaIdAndAreaCodeMap != null
				&& areaIdAndAreaCodeMap.containsKey(id))
		{
			return areaIdAndAreaCodeMap.get(id);

		} else
			return "";
	}

	@Override
	public Long getAreaIdByName(String name)
	{
		log.debug("get Area id by name : "+name);
		
		if(name == null || name.isEmpty())
			return null;
		if (areaNameAndAreaIdMap != null
				&& areaNameAndAreaIdMap.containsKey(name))
		{
			return areaNameAndAreaIdMap.get(name);

		} else
			return null;
	}

	@Override
	public Long getIdByCode(String code)
	{
		if(code == null || code.isEmpty())
			return null;
		
		log.debug(" Inside getting ID by Code : "+code);
		
		if (areaCodeAndAreaIdMap != null
				&& areaCodeAndAreaIdMap.containsKey(code))
		{
			return areaCodeAndAreaIdMap.get(code);

		} else
			return null;
	}

	@Override
	public List<Long> getAllAreaIds()
	{
		log.debug("getAllAreaIds  "  );
		
		List<Long> areaIdList = (List<Long>) hibernetTemplate
				.find("select a.id from Area a");

		return areaIdList;

	}

	@Override
	public int updateUserArea(Long cityId)
	{
		if(cityId == null || cityId.longValue() <= 0)
			return 0;
		String query = "select * from Area where branchId in (select id from Branch where cityId="
				+ cityId + ")";

		return 0;
	}

	@Override
	public Map<String,Long> getAreaInfoIdFromTicketId(Long ticketId) 
	{
		log.info(" inside getAreaInfoIdFromTicketId  "+  ticketId);
		//Gives areaId of any ticket by receiving ticketId
		Map<String,Long> areaInfo=null;
		Object[] areaBranchCity= (Object[]) hibernetTemplate.getSessionFactory()
		.getCurrentSession()
		.createSQLQuery("select c.areaId,c.branchId,c.cityId from Ticket t join Customer c on t.customerId=c.id where t.id=:ticketId")
		.setParameter("ticketId", ticketId).uniqueResult();
		if(areaBranchCity!=null && areaBranchCity.length>=3){
			areaInfo=new HashMap<String,Long>();
			areaInfo.put("area", Long.valueOf(areaBranchCity[0].toString()));
			areaInfo.put("branch", Long.valueOf(areaBranchCity[1].toString()));
			areaInfo.put("city", Long.valueOf(areaBranchCity[2].toString()));
		}
		
		log.debug(" Exit from getAreaInfoIdFromTicketId  "+  ticketId);
		return areaInfo;
	}
	@Override
	public  void updateAreaCache(Area area)
	{
		if (area == null)
			return  ;

		log.debug(" Inside update Area "  +area.getAreaName());
		
		if(area != null)
		{
			AreaVO areaVO = new AreaVO();
			BeanUtils.copyProperties(area, areaVO);

			Branch branch = area.getBranch();
			areaVO.setBranchId(branch.getId());
			BranchVO branchVO = branchDAO.getBranchById(areaVO.getBranchId());
			areaVO.setBranchCode(branch.getBranchCode());
			areaVO.setBranchName(branch.getBranchName());
			areaVO.setCityCode(branchVO.getCityCode());

			addToCache(areaVO);
			addToAreaCache(area);
		}
		
	}
	
	// for getting areaid from areaname and branch of user
			@Override
			public Long getAreaIdForBulk(String areaName,Long userid) {
				
				try 
				{	
					Query query = hibernetTemplate.getSessionFactory()
							.getCurrentSession().createSQLQuery(QUERY_FOR_AREA_ID)
					        .setParameter("areaName", areaName)
					        .setParameter("userId",userid );
					
					List<Long> result = query.list();
					
					if (! result.isEmpty()) {
						return result.get(0);
					
					}
					

				} catch (Exception e)
				{
					log.error("Error while upserting data");


				}
				return null;
				
			}
}