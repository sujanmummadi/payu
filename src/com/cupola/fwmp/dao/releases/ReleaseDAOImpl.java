package com.cupola.fwmp.dao.releases;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.persistance.entities.ApkRelease;
import com.cupola.fwmp.persistance.entities.BranchApkRelease;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.ResponseUtil;

public class ReleaseDAOImpl implements ReleaseDAO
{

	private Logger log = Logger.getLogger(ReleaseDAOImpl.class.getName());

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Autowired
	JdbcTemplate jdbcTemplate;

	String UPDATE_VERSION = "update ApkRelease set previousVersion = ?,currentVersion = ? where cityId = ?";

	String UPDATE_VERSION_MODIFIED = "update ApkRelease a, BranchApkRelease b  set a.previousVersion = ? ,b.previousVersion = ?"
			+ ",a.currentVersion = ?,b.currentVersion = ? where a.cityId=? and b.branchId=?";

	static Map<Long, String> releaseDetails = new HashMap<Long, String>();

	public void init()
	{
		log.info("Initializing version cache..."); 
		cacheVersionDetails();
		log.info("Initialization version cache done...");
	}
	
	private void cacheVersionDetails()
	{
		try
		{
			List<BranchApkRelease> versionDetail = (List<BranchApkRelease>) hibernateTemplate
					.find("from BranchApkRelease");
			
			if( versionDetail == null )
				return;
			
			for( BranchApkRelease version : versionDetail)
			{
				if( version.getBranchId() == null || version.getBranchId().longValue() <= 0 
						|| version.getCurrentVersion() == null
						|| version.getCurrentVersion().isEmpty() )
					continue;
				
				releaseDetails.put(version.getBranchId(), version.getCurrentVersion());
			}
			
		}catch (Exception e) {
			log.error("Error occured while caching version cache from Db"+e);
		}
	}
	
	private String getVersionStatus( String version )
	{
		log.info("************** Getting Version from cache **************");
		Long branchId = AuthUtils.getCurrentUserBranch() != null ? AuthUtils.getCurrentUserBranch().getId() : 0;
		
		if( releaseDetails.containsKey( branchId) )
		{
			 if( isTargetedVersionIsHigher(releaseDetails.get( branchId ), version))
				return "N/A";
			 else
				return releaseDetails.get( branchId );
		}else
			return "N/A";
	}
	
	@Override
	@Transactional
	public String getReleases(String version)
	{
		log.debug("Inside getReleases  ");

		if( version != null && !version.isEmpty() )
			return getVersionStatus( version );
		
		// Assuming current verion and fr version for the branch will be same
		// where as city current version will be same as installation version
		// and fr version will be same as branch fr version
		
		String newInstallationSqlQuery = "select a.* from ApkRelease a join BranchApkRelease b on "
				+ "a.currentVersion=b.currentVersion where a.cityId="
				+ AuthUtils.getCurrentUserCity().getId() + " and b.branchId="
				+ AuthUtils.getCurrentUserBranch().getId() /*+ " and b.currentVersion= " + "'"+version+"'"*/;

		String frSqlQuery = "select b.* from ApkRelease a join BranchApkRelease b on "
				+ "a.frVersion=b.frVersion where a.cityId="
				+ AuthUtils.getCurrentUserCity().getId() + " and b.branchId="
				+ AuthUtils.getCurrentUserBranch().getId() /*+ " and b.currentVersion= " + "'"+version+"'"*/;

		if (releaseDetails
				.containsKey(AuthUtils.getCurrentUserBranch().getId()))
		{
			if(releaseDetails.get(AuthUtils.getCurrentUserBranch().getId()).equalsIgnoreCase(version))
			{
				return releaseDetails.get(AuthUtils.getCurrentUserBranch().getId());
			}
			else if(isTargetedVersionIsHigher(releaseDetails.get(AuthUtils.getCurrentUserBranch().getId()), version))
				return "N/A";
			else
				return releaseDetails.get(AuthUtils.getCurrentUserBranch().getId());
			
		} else if (releaseDetails.containsKey(AuthUtils.getCurrentUserCity().getId()))
		{
			if(releaseDetails.get(AuthUtils.getCurrentUserCity().getId()).equalsIgnoreCase(version))
				return releaseDetails.get(AuthUtils.getCurrentUserCity().getId());
			else if(isTargetedVersionIsHigher(releaseDetails.get(AuthUtils.getCurrentUserCity().getId()), version))
				return "N/A";
			else
				return releaseDetails.get(AuthUtils.getCurrentUserCity().getId());
		}

		log.info("newInstallationSqlQuery for apk version " + newInstallationSqlQuery + " for branch " + AuthUtils.getCurrentUserBranch().getName());

		@SuppressWarnings("unchecked")
		java.util.List<ApkRelease> newInstallationApkRelease = (java.util.List<ApkRelease>) hibernateTemplate
				.getSessionFactory().getCurrentSession()
				.createSQLQuery(newInstallationSqlQuery)
				.addEntity(ApkRelease.class).list();

		log.info("newInstallationSqlQuery for apk version result "
				+ newInstallationApkRelease.size());
		

		if (newInstallationApkRelease != null
				&& !newInstallationApkRelease.isEmpty())
		{
			log.debug("newInstallationSqlQuery for apk version result "
					+ newInstallationApkRelease.size());
			log.info("New Installation App version "
					+ newInstallationApkRelease.get(0).getCityId() + " "
					+ newInstallationApkRelease.get(0).getCurrentVersion());

			releaseDetails.put(newInstallationApkRelease.get(0)
					.getCityId(), newInstallationApkRelease.get(0)
							.getCurrentVersion());
			if (isTargetedVersionIsHigher(newInstallationApkRelease.get(0).getCurrentVersion(), version))
				return "N/A";

			return releaseDetails.get(AuthUtils.getCurrentUserCity().getId());

		} else
		{
			log.info("frSqlQuery for apk version " + frSqlQuery);

			@SuppressWarnings("unchecked")
			java.util.List<BranchApkRelease> frApkRelease = (java.util.List<BranchApkRelease>) hibernateTemplate
					.getSessionFactory().getCurrentSession()
					.createSQLQuery(frSqlQuery)
					.addEntity(BranchApkRelease.class).list();

			

			if (frApkRelease != null && !frApkRelease.isEmpty())
			{

				log.info("FR App version " + frApkRelease.get(0).getBranchId()
						+ " " + frApkRelease.get(0).getCurrentVersion());

				releaseDetails
						.put(frApkRelease.get(0).getBranchId(), frApkRelease
								.get(0).getCurrentVersion());
				if (isTargetedVersionIsHigher(frApkRelease.get(0).getCurrentVersion(), version))
					return "N/A";

				return releaseDetails
						.get(AuthUtils.getCurrentUserBranch().getId());

			} else
				return "N/A";
		}

	}

	@Override
	public APIResponse updateRelease(Long cityId, String newVersion,
			Long branchId)
	{
		log.debug("Inside updateRelease  " + cityId + " branchId is " +branchId );

		String sqlQuery = "select a.* from ApkRelease a join BranchApkRelease b on "
				+ "a.currentVersion=b.currentVersion where a.cityId=" + cityId
				+ " and b.branchjId=" + branchId;
		
		if (cityId != null && newVersion != null)
		{

			int rows = 0;
			try
			{

				java.util.List<String> list = (java.util.List<String>) hibernateTemplate
						.find(sqlQuery);

				if (list != null && !list.isEmpty())

				{
					Object previous = (String) list.get(0);

					rows = jdbcTemplate
							.update(UPDATE_VERSION_MODIFIED, previous, previous, newVersion, newVersion, cityId, branchId);

				} else
				{

					rows = jdbcTemplate
							.update(UPDATE_VERSION, newVersion, newVersion, cityId);
				}

				if (rows > 0)
				{
					releaseDetails.put(cityId, newVersion);
					return ResponseUtil.createUpdateSuccessResponse();
				} else
				{
					return ResponseUtil.createUpdateFailedResponse();

				}
			} catch (Exception e)
			{
				log.error("Error While Updating Release info : "
						+ e.getMessage());
				e.printStackTrace();
				return ResponseUtil.databaseUpdateException();

			}

		} else
		{

			return ResponseUtil.createRecordNotFoundResponse();
		}

	}

	@Override
	public int resetVersionCache()
	{
		int result = releaseDetails.size();
		releaseDetails.clear();
		init();
		return result;
	}
	
	public  boolean  isTargetedVersionIsHigher(String currentVersion, String targetedVersion) {
		String[] versionchars1 = currentVersion.split("\\.");
		String[] versionchars2 = targetedVersion.split("\\.");
		int length = Math.max(versionchars1.length, versionchars2.length);
		for (int i = 0; i < length; i++) {
			Integer tempV1 = i < versionchars1.length ? getAscii(versionchars1[i]) : 0;
			Integer tempV2 = i < versionchars2.length ? getAscii(versionchars2[i]) : 0;
			int compare = tempV1.compareTo(tempV2);
			if (compare != 0) {
				return compare < 0;
			}
		}
		return false;
	}

	private  Integer getAscii(String input) {
		StringBuffer sbTemp = new StringBuffer();
		char[] inputArr = input.toCharArray();
		for (int i = 0; i < inputArr.length; i++) {
			if (inputArr[i] == '0' && sbTemp.length() == 0) {
				continue;
			}
			int ia = (int) inputArr[i];
			sbTemp.append(ia);
		}
		if (sbTemp.toString().length() == 0) {
			return 0;
		}
		try {
			return Integer.parseInt(sbTemp.toString());
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
}
