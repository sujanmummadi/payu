package com.cupola.fwmp.dao.releases;

import com.cupola.fwmp.response.APIResponse;

public interface ReleaseDAO {
	
	public  String  getReleases(String version);
	
	/**@author aditya 4:35:25 PM Apr 10, 2017
	 * APIResponse
	 * @param cityId
	 * @param newVersion
	 * @param branchId
	 * @return
	 */
	APIResponse updateRelease(Long cityId, String newVersion, Long branchId);

	public int resetVersionCache();


}
