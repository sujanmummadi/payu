package com.cupola.fwmp.dao.rollback;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.ResponseUtil;

public class RollBackDAOImpl implements RollBackDAO
{

	private Logger log = Logger.getLogger(RollBackDAOImpl.class.getName());

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	HibernateTemplate hibernateTemplate;

	@Override
	public APIResponse confirmRollBackActivity(String ticketId1,
			String activityId1)
	{

		long ticketId = Long.parseLong(ticketId1);
		long activityId = Long.parseLong(activityId1);

		long ticketActivityId = 0;

		String CONFIRM_ROLLBACK = "select count(id) from TicketActivityLog where ticketId="
				+ ticketId + " " + "and activityId=" + activityId;

		String REMOVE_WRONG_ENTRY = "delete from TicketActivityLog where ticketId= "
				+ ticketId + " " + "and activityId= " + activityId;

		Long userId = AuthUtils.getCurrentUserId();
		int roleId = AuthUtils.getCurrentUserRole();
     
		log.info("ticketId = " + ticketId + " activityId = " + activityId
				+ "  userid = = =  " + userId + "  id " + roleId);

		if (roleId == UserRole.NE || roleId == UserRole.NE_NI || roleId == UserRole.SALES_EX)
		{

			try
			{
				
				
				
			List<Object[]>	result = hibernateTemplate.getSessionFactory()
					.getCurrentSession().createSQLQuery(CONFIRM_ROLLBACK).list();
		
			if(result != null && result.size() > 0)
			{
				Object[] count = result.get(0);
				ticketActivityId = 	Long.valueOf(count[0].toString());
			}
			
			
//				ticketActivityId = jdbcTemplate.execute(CONFIRM_ROLLBACK);

			} catch (Exception e)
			{

				log.error("exception in getting " + e.getMessage());

			}

			if (ticketActivityId != 0)
			{
				log.info("There is entry in table");

				
				return ResponseUtil.createSuccessResponse().
						setData(ResponseUtil.createFailureRollBack());

			} else
			{
				log.info("There is no entry in table");

				return ResponseUtil.createSuccessResponse()
						.setData(ResponseUtil.createRecordNotFoundResponse());

			}

		}

		if (roleId == UserRole.TL_NI || roleId == UserRole.TL || roleId == UserRole.SALES_TL)
		{
			int deleteRow = 0;
			int updateRow = 0;

			try
			{
				deleteRow = jdbcTemplate.update(REMOVE_WRONG_ENTRY);
				log.info(deleteRow + " rows deleted from TicketActivityLog");

				/*
				 * below code commented because there is no requirement for
				 * right now for updating the WorKOrder table once after delete
				 * the entry from TicketActivityLog
				 * 
				 * if(deleteRow != 0 ){ try{ updateRow =
				 * jdbcTemplate.update(UPDATE_WORKORDER); log.info(updateRow +
				 * " rows updated for WorkOrder"); } catch(Exception e){
				 * log.error("Error in update workorder  "+e.getMessage());
				 * 
				 * } }
				 * 
				 * else{ log.info("There is no entry to delete"); }
				 */

			} catch (Exception e)
			{
				log.error("Error in deletion  " + e.getMessage());

			}

			if (deleteRow != 0)
			{

				return ResponseUtil.createSuccessResponse()
						.setData(ResponseUtil.createSuccessRollBack());
			}

			else
			{

				return ResponseUtil.createSuccessResponse().
						setData(ResponseUtil.createRecordNotFoundResponse());
			}

		}

		else
		{

			log.info("you are not authorized person");

			return ResponseUtil.createBadCredentialsExceptionResponse();
		}

	}

}
