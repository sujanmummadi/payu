package com.cupola.fwmp.dao.rollback;

import com.cupola.fwmp.response.APIResponse;

public interface RollBackDAO {
	
	public APIResponse confirmRollBackActivity(String ticketId,
			String activityId);

}
