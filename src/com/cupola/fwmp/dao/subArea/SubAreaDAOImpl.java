package com.cupola.fwmp.dao.subArea;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.SubArea;
import com.cupola.fwmp.vo.SubAreaVO;

@Transactional
public class SubAreaDAOImpl implements SubAreaDAO {

	private Logger log = Logger.getLogger(SubAreaDAOImpl.class.getName());
	
	private final String GET_ALL_SUB_AREA = "select * from SubArea";
	private final String FIND_SUB_AREA_BY_ID = "select * from SubArea where id=?";
	private final String FIND_SUB_AREA_BY_AREA_ID = "select * from SubArea where areaId=?";
	private final String INSERT_VALUES = "insert into SubArea values (?,?,?,?,?,?,?,?)";
	public static Map<Long, SubAreaVO> subAreaCache = new HashMap<Long, SubAreaVO>();
	public static Map<String, Long> subAreaNameAndSubAreaIdMap = new ConcurrentHashMap<String, Long>();
	
	JdbcTemplate jdbcTemplate;

	HibernateTemplate hibernateTemplate;
	
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
	{
		this.hibernateTemplate = hibernateTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		
		this.jdbcTemplate = jdbcTemplate;
		
	}

	public void init()
	{
		log.info("initializing SubAreaVo "  );
		
		List<SubAreaVO> subAreaVOs = getAllSubArea();
		
		if(subAreaVOs!= null){
			
			for (SubAreaVO subAreaVO : subAreaVOs) {
				
				subAreaCache.put(subAreaVO.getId(), subAreaVO);
				log.info("area.getSubAreaName() :: "+subAreaVO.getAreaId()+"_"+subAreaVO.getSubAreaName());
				subAreaNameAndSubAreaIdMap.put(subAreaVO.getAreaId()+"_"+subAreaVO.getSubAreaName(), subAreaVO.getId());
			}
		}
		log.info(" exit from initializing SubAreaVo "  );
	}
	
	
	
	
	@Override
	public void addSubArea(SubArea subArea)
	{
		if (subArea == null)
			return;

		log.debug(" inside adding SubArea Details" +subArea .getSubAreaName());
		try {
		jdbcTemplate.update(
				INSERT_VALUES,
				new Object[] { subArea.getId(), subArea.getArea().getId(),
						subArea.getSubAreaName(), subArea.getAddedBy(),
						subArea.getAddedOn(), subArea.getModifiedBy(),
						subArea.getModifiedOn(), subArea.getStatus() });
		} catch ( Exception e) {
			 log.error("error while adding SubArea details ",e);
			e.printStackTrace();
	}
    }
	@Override
	public SubAreaVO getSubAreaById(Long id) 
	{
      log.debug("getSubAreaById   " +id );

		if(subAreaCache.containsKey(id))
			return subAreaCache.get(id);
		else
			init();
		
		log.debug(" Exit From getSubAreaById   " +id );
		
		return  subAreaCache.get(id);
	}

	@Override
	public List<SubAreaVO> getAllSubArea() {

		log.debug("getAllSubArea   "  );
		return jdbcTemplate.query(GET_ALL_SUB_AREA, new SubAreaMapper());

	}

	@Override
	public SubArea deleteSubArea(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SubArea updateSubArea(SubArea subArea) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Long, String> getSubAreasByAreaId(Long areaId) 
	{
		
		Map<Long, String> subAreaMap = new TreeMap<Long, String>();
		
		log.debug(" getSubAreasByAreaId  ::"+areaId);
		
		try
		{
			if(areaId > 0)
			{
		 
				List<SubAreaVO> areaVOs = jdbcTemplate.query(FIND_SUB_AREA_BY_AREA_ID,new Object[] { areaId }, new SubAreaMapper());
		
				if(areaVOs != null && !areaVOs.isEmpty())
				{
			
					log.debug("sub area size:"+areaVOs.size());
				
					for (SubAreaVO subAreaVO : areaVOs)
					{
				
						subAreaMap.put(subAreaVO.getId(), subAreaVO.getSubAreaName());
					}
				}
		
				else
				
				log.debug("No sub area found for area=="+areaId);
			}

			else
			{
			
				List<SubAreaVO> subAreaVOs = getAllSubArea();
			
				if(subAreaVOs!= null){
				
					for (SubAreaVO subAreaVO : subAreaVOs) 
					{
					
						subAreaMap.put(subAreaVO.getId(), subAreaVO.getSubAreaName());
					}
				}
			
				else
				{
				
					log.info("No sub areas found");
				}
			}
		} catch ( Exception e) {
			 log.error("error while getting SubAreas By AreaId " ,e);
		}
		log.debug(" Exit From getSubAreasByAreaId ::"+areaId);
		
		return subAreaMap;
		
}

	@Override
	public Map<Long, String> getSubAreasByAreaName(String areaName) 
	{
       log.debug("getSubAreasByAreaName " + areaName);
		Map<Long, String> subAreaNames = new TreeMap<>();
		
		if( areaName == null || areaName.isEmpty())
			return subAreaNames;
		
		try
		{
		
			List<Area> areas = (List<Area>) hibernateTemplate.find("from Area where areaName =?", "%"+areaName+"%");
			if(areas!=null && !areas.isEmpty())
			{
			
				log.debug("list of sub areas size  : "+areas.size());
				
				Area area = areas.get(0);
				Set<SubArea> subAreas = area.getSubAreas();
			
				for (SubArea subArea : subAreas) {
				
					subAreaNames.put(subArea.getId(), subArea.getSubAreaName());
				}
				
				log.info("sub areas::"+subAreaNames+" for area:"+areaName);
		
			}
		} catch ( Exception e) {
			 log.error("error while getting SubAreasByAreaName",e);
		}
		
		log.info(" Exit From getSubAreasByAreaName " + areaName);
		return subAreaNames;
		
	}
	
	@Override
	public Long getSubAreaIdByName(String name)
	{
		log.info("get sub Area id by name : "+name);
		
		if(name == null || name.isEmpty())
			return null;
		if (subAreaNameAndSubAreaIdMap != null
				&& subAreaNameAndSubAreaIdMap.containsKey(name))
		{
			return subAreaNameAndSubAreaIdMap.get(name);

		} else
			return null;
	}

}
class SubAreaMapper implements RowMapper<SubAreaVO> {

	@Override
	public SubAreaVO mapRow(ResultSet resultSet, int rowNumber)
			throws SQLException {

		SubAreaVO subArea = new SubAreaVO();

		subArea.setId(resultSet.getLong("id"));
		subArea.setAreaId(resultSet.getLong("areaId"));
		subArea.setSubAreaName(resultSet.getString("subAreaName"));
		subArea.setAddedBy(resultSet.getLong("addedBy"));
		subArea.setAddedOn(resultSet.getDate("addedOn"));
		subArea.setModifiedBy(resultSet.getLong("modifiedBy"));
		subArea.setModifiedOn(resultSet.getDate("modifiedOn"));
		subArea.setStatus(resultSet.getInt("status"));

		return subArea;
	}

	
}
