package com.cupola.fwmp.dao.subArea;

import java.util.List;
import java.util.Map;

import com.cupola.fwmp.persistance.entities.SubArea;
import com.cupola.fwmp.vo.SubAreaVO;

public interface SubAreaDAO{
	
	public void addSubArea(SubArea subArea);

	public SubAreaVO getSubAreaById(Long id);

	public List<SubAreaVO> getAllSubArea();

	public SubArea deleteSubArea(Long id);

	public SubArea updateSubArea(SubArea subArea);

	public Map<Long, String> getSubAreasByAreaId(Long areaId);

	Map<Long, String> getSubAreasByAreaName(String areaName);
	
	public Long getSubAreaIdByName(String name);
}
