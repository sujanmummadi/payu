/**
 * 
 */
package com.cupola.fwmp.dao.sales;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.cupola.fwmp.FWMPConstant;
import com.cupola.fwmp.FWMPConstant.AadhaarTransactionType;
import com.cupola.fwmp.FWMPConstant.Activity;
import com.cupola.fwmp.FWMPConstant.BooleanValue;
import com.cupola.fwmp.FWMPConstant.CurrentCrmNames;
import com.cupola.fwmp.FWMPConstant.DeDupFlag;
import com.cupola.fwmp.FWMPConstant.FeasibilityType;
import com.cupola.fwmp.FWMPConstant.MQStatus;
import com.cupola.fwmp.FWMPConstant.ProspectType;
import com.cupola.fwmp.FWMPConstant.Status;
import com.cupola.fwmp.FWMPConstant.TicketStatus;
import com.cupola.fwmp.FWMPConstant.TicketSubCategory;
import com.cupola.fwmp.FWMPConstant.WorkStageName;
import com.cupola.fwmp.FWMPConstant.WorkStageType;
import com.cupola.fwmp.dao.adhaar.AdhaarDAO;
import com.cupola.fwmp.dao.customer.CustomerDAO;
import com.cupola.fwmp.dao.integ.gis.GisDAO;
import com.cupola.fwmp.dao.tariff.TariffDAO;
import com.cupola.fwmp.dao.ticket.TicketDAO;
import com.cupola.fwmp.dao.ticketActivityLog.TicketActivityLogDAO;
import com.cupola.fwmp.dao.user.UserCache;
import com.cupola.fwmp.dao.user.UserDao;
import com.cupola.fwmp.filters.TicketFilter;
import com.cupola.fwmp.persistance.entities.Area;
import com.cupola.fwmp.persistance.entities.Branch;
import com.cupola.fwmp.persistance.entities.City;
import com.cupola.fwmp.persistance.entities.Customer;
import com.cupola.fwmp.persistance.entities.CustomerAadhaarMapping;
import com.cupola.fwmp.persistance.entities.Gis;
import com.cupola.fwmp.persistance.entities.Payment;
import com.cupola.fwmp.persistance.entities.SubArea;
import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.persistance.entities.User;
import com.cupola.fwmp.response.APIResponse;
import com.cupola.fwmp.service.definition.DefinitionCoreService;
import com.cupola.fwmp.service.domain.engines.classification.global.GlobalActivities;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateConstant;
import com.cupola.fwmp.service.domain.gateway.TicketUpdateGateway;
import com.cupola.fwmp.service.domain.logger.TicketLog;
import com.cupola.fwmp.service.domain.logger.TicketLogImpl;
import com.cupola.fwmp.service.location.LocationCache;
import com.cupola.fwmp.service.notification.NotificationService;
import com.cupola.fwmp.service.ticketActivityLog.TicketActivityLogCoreService;
import com.cupola.fwmp.util.ApplicationUtils;
import com.cupola.fwmp.util.AuthUtils;
import com.cupola.fwmp.util.AuthUtils.UserRole;
import com.cupola.fwmp.util.DBUtil;
import com.cupola.fwmp.util.StrictMicroSecondTimeBasedGuid;
import com.cupola.fwmp.vo.TicketAcitivityDetailsVO;
import com.cupola.fwmp.vo.counts.SalesTypeCountVO;
import com.cupola.fwmp.vo.sales.ImageInput;
import com.cupola.fwmp.vo.sales.PaymentUpdateVO;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;
import com.cupola.fwmp.ws.sales.DocumentService;

/**
 * @author aditya
 * 
 */
@Transactional("fwmpTransactionManager")
public class SalesDAOImpl implements SalesDAO
{
	private static Logger log = LogManager
			.getLogger(SalesDAOImpl.class.getName());

	@Autowired
	@Qualifier("jdbcTemplate")
	JdbcTemplate jdbcTemplate;

	@Autowired
	AdhaarDAO adhaarDao;
	
	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	TicketDAO ticketDAO;

	@Autowired
	TariffDAO tariffDAO;

	@Autowired
	UserDao userDao;

	@Autowired
	DBUtil dbUtil;

	@Autowired
	GlobalActivities globalActivities;

	@Autowired
	TicketActivityLogCoreService ticketActivityLogCoreService;

	@Autowired
	HibernateTemplate hibernateTemplate;
	@Autowired
	TicketActivityLogDAO ticketActivityLogDAO;

	@Autowired
	GisDAO gisDAO;

	@Autowired
	NotificationService notificationService;
	
	@Autowired
	DocumentService documentService;

	@Override
	public List<SalesTypeCountVO> customerTypeCount(long userId)
	{
		Session session = null;
		List<SalesTypeCountVO> customerList = new ArrayList<SalesTypeCountVO>();
		try
		{
			session = hibernateTemplate.getSessionFactory().openSession();

			String userKey = dbUtil.getKeyForQueueByUserId(userId);

			Set<Long> ticketIds = globalActivities
					.getAllTicketFromMainQueueByKey(userKey);

			log.debug("Tickets in queue for user " + userId + " are "
					+ ticketIds);

			if (AuthUtils.isManager())
				;
			else if (ticketIds == null || ticketIds.isEmpty())
				return null;

			StringBuilder queryString = new StringBuilder(GET_SE_RIBBON_COUNT);
			List<Long> areaIds = null;
			if (AuthUtils.isManager())
			{
				areaIds = new ArrayList<Long>();
				TicketFilter filter = ApplicationUtils.retrieveTicketFilter();
				if (filter != null)
				{
					if (filter.getAreaId() != null && filter.getAreaId() > 0)
						areaIds.add(filter.getAreaId());

					else if (filter.getBranchId() != null
							&& filter.getBranchId() > 0)
						areaIds.addAll(LocationCache
								.getAreasByBranchId(filter.getBranchId() + "")
								.keySet());

					else
						areaIds.addAll(AuthUtils.getAreaIdsForCurrentUser());
				}

			}
			if (areaIds != null && areaIds.size() > 0)
			{
				queryString.append(" and t.areaId in(:areaIds) ");
			}else if( AuthUtils.isAreaManager()){
				queryString.append(" and  t.currentAssignedTo in(:reportingUsers) ");
			} else
			{
				queryString.append(" and t.id in(:ticketIds) ");
			}
			
			queryString.append(" order by t.TicketCreationDate desc");
			
			Query query = session.createSQLQuery(queryString.toString());

			log.info("######## test Query to get GET_SE_RIBBON_COUNT "
					+ query.getQueryString() + " ticketIds " + ticketIds
					+ " ticketStatus "
					+ TicketStatus.ACTIVATION_COMPLETED_VERIFIED);

			if (areaIds != null && areaIds.size() > 0)
				query.setParameterList("areaIds", areaIds);
			else if(AuthUtils.isManager()){
				if(AuthUtils.isAreaManager()){
					Map<Long, String> reportingUsers = null;
					reportingUsers = userDao.getNestedReportingUsers(AuthUtils.getCurrentUserId(),null,null, null);
					reportingUsers.put(AuthUtils.getCurrentUserId(),null);
					if(reportingUsers == null || reportingUsers.size() == 0)
					{
						query.setParameterList("reportingUsers", Arrays.asList(new Long[]{-1l}));
					}
					else
					{
						query.setParameterList("reportingUsers", reportingUsers.keySet());
					}
					log.debug("Sales Reporting users :"+reportingUsers.keySet());
				}
			}
			else
			{
				query.setParameterList("ticketIds", ticketIds);
			}
			query.setParameter("ticketStatus", TicketStatus.ACTIVATION_COMPLETED_VERIFIED);

			List tempCustomerList = query.list();

			log.debug("Got result for " + userId + " tempCustomerList "
					+ tempCustomerList.size());

			for (Iterator iterator = tempCustomerList.iterator(); iterator
					.hasNext();)
			{
				Object[] object = (Object[]) iterator.next();

				SalesTypeCountVO salesTypeCountVO = new SalesTypeCountVO();

				log.debug("object[0].toString() " + object[0]
						+ " object[1].toString() " + object[1]
						+ "object[2].toString()" + object[2]
						+ " object[3].toString() " + object[3]
						+ " object[4].toString() " + object[4]
						+ "object[5].toString() " + object[5]
						+ "object[6].toString() " + object[6]
						+ "object[7].toString() " + object[7]);

				if (object[0] != null)
					salesTypeCountVO
							.setCustomerId(Long.valueOf(object[0].toString()));
				if (object[1] != null)
					salesTypeCountVO.setProspectStatus(Integer
							.valueOf(object[1].toString()));
				if (object[2] != null)
					salesTypeCountVO
							.setTicketId(Long.valueOf(object[2].toString()));
				if (object[3] != null)
					salesTypeCountVO.setTicketStatus(Integer
							.valueOf(object[3].toString()));
				if (object[4] != null)
					salesTypeCountVO.setPaymentStatus(Integer
							.valueOf(object[4].toString()));
				if (object[5] != null)
					salesTypeCountVO.setDocumentStatus(Integer
							.valueOf(object[5].toString()));
				if (object[6] != null)
					salesTypeCountVO
							.setAssignedTo(Long.valueOf(object[6].toString()));
				if (object[7] != null)
					salesTypeCountVO.setTicketSource(object[7].toString());
				if (object[8] != null)
					salesTypeCountVO.setPoiDocumentStatus(Integer
							.valueOf(object[8].toString()));
				if (object[9] != null)
					salesTypeCountVO.setPoaDocumentStatus(Integer
							.valueOf(object[9].toString()));

				log.debug("Prospect details for se " + salesTypeCountVO);

				customerList.add(salesTypeCountVO);
			}

			log.info("Total Prospect details for user " + userId + " are "
					+ " and size is " + customerList.size());

			return customerList;
		} catch (NumberFormatException e)
		{
			log.error("Number format exception in customerTypeCount "
					+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e)
		{
			log.error("HibernateException in customerTypeCount "
					+ e.getMessage());
			e.printStackTrace();
		} finally
		{
			if (session != null && session.isOpen())
				session.close();
		}
		log.debug(" Exit From customerTypeCount ::"  +userId);
		return customerList;
	}

	@Override
	public int updateBasicInfo(UpdateProspectVO updateProspectVO,boolean isValuesFromCRM , Long assignToFromCRM,String deDupFlag)
	{
		if (updateProspectVO == null)
			return 0;
		
		log.info("Update basic info called in sales dao for user " + updateProspectVO.getProspectNo());


		Session session = hibernateTemplate.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();

		try
		{
			if (updateProspectVO.getCustomerId() != null)
			{
				Customer customer = (Customer) session.get(Customer.class, Long
						.valueOf(updateProspectVO.getCustomerId()));

				log.info("Got customer details " + customer.getMobileNumber());

				customer.setModifiedOn(new Date());
				customer.setModifiedBy(AuthUtils.getCurrentUserId());
				customer.setStatus(FWMPConstant.TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);

				if (updateProspectVO.getAlternativeEmailId() != null)
					customer.setAlternativeEmailId(updateProspectVO
							.getAlternativeEmailId());

				if (updateProspectVO.getAlternativeMobileNo() != null)
					customer.setAlternativeMobileNo(updateProspectVO
							.getAlternativeMobileNo());

//				if (updateProspectVO.getAreaId() != null)
//					customer.setArea((Area) session.load(Area.class, Long
//							.valueOf(updateProspectVO.getAreaId())));
//
//				if (updateProspectVO.getBranchId() != null)
//					customer.setBranch((Branch) session.load(Branch.class, Long
//							.valueOf(updateProspectVO.getBranchId())));

				if (updateProspectVO.getAreaId() != null)
				{
					customer.setArea((Area) session.load(Area.class, Long
							.valueOf(updateProspectVO.getAreaId())));
				}
				else if(updateProspectVO.getAreaName() != null)
				{
					Area a = LocationCache.getAreaDAO().getAreaByAreaName(updateProspectVO.getAreaName());
					
					log.debug(updateProspectVO.getAreaName() + " for prospect " + updateProspectVO.getProspectNo() + " is " + a);
				
					if(a != null)
						customer.setArea(a);
				}
				
				if (updateProspectVO.getBranchId() != null)
				{
					customer.setBranch((Branch) session.load(Branch.class, Long

							.valueOf(updateProspectVO.getBranchId())));
				}
				else if(updateProspectVO.getBranchName() != null)
				{
					Branch b = LocationCache.getBranchDAO().getBranchByBranchCode(updateProspectVO.getBranchName());
					
					log.info(updateProspectVO.getBranchName() + " for prospect " + updateProspectVO.getProspectNo() + " is "+ b);
					
					if(b != null)
						customer.setBranch(b);
				}

				if (updateProspectVO.getCityId() != null)
					customer.setCity((City) session.load(City.class, Long
							.valueOf(updateProspectVO.getCityId())));

				if (updateProspectVO.getCommunicationAdderss() != null)
					customer.setCommunicationAdderss(updateProspectVO
							.getCommunicationAdderss());

				if (updateProspectVO.getCurrentAddress() != null)
					customer.setCurrentAddress(updateProspectVO
							.getCurrentAddress());

				if (updateProspectVO.getFirstName() != null)
					customer.setFirstName(updateProspectVO.getFirstName());

				if (updateProspectVO.getMiddleName() != null)
					customer.setMiddleName(updateProspectVO.getMiddleName());

				if (updateProspectVO.getLastName() != null)
					customer.setLastName(updateProspectVO.getLastName());

				if (updateProspectVO.getTitle() != null)
					customer.setTitle(updateProspectVO.getTitle());

				if (updateProspectVO.getCustomerProfile() != null)
					customer.setCustomerProfession(updateProspectVO
							.getCustomerProfile());

				if (updateProspectVO.getEmailId() != null)
					customer.setEmailId(updateProspectVO.getEmailId());

				if (updateProspectVO.getMobileNumber() != null)
					customer.setMobileNumber(updateProspectVO
							.getMobileNumber());

				if (updateProspectVO.getOfficeNumber() != null)
					customer.setOfficeNumber(updateProspectVO
							.getOfficeNumber());

				if (updateProspectVO.getPermanentAddress() != null)
					customer.setPermanentAddress(updateProspectVO
							.getPermanentAddress());

				if (updateProspectVO.getPinCode() != null)
					customer.setPincode(Integer
							.valueOf(updateProspectVO.getPinCode()));

				if (updateProspectVO.getCustomerType() != null)
					customer.setCustomerType(updateProspectVO
							.getCustomerType());

				if (updateProspectVO.getProspectType() != null)
				{
					if (updateProspectVO.getProspectType()
							.equalsIgnoreCase(FWMPConstant.ProspectType.COLD))
					{
						customer.setStatus(FWMPConstant.ProspectType.COLD_ID);
						customer.setRemarks(updateProspectVO
								.getProspectColdReason());
					} else if (updateProspectVO.getProspectType()
							.equalsIgnoreCase(FWMPConstant.ProspectType.HOT))
						customer.setStatus(FWMPConstant.ProspectType.HOT_ID);

					else if (updateProspectVO.getProspectType()
							.equalsIgnoreCase(FWMPConstant.ProspectType.MEDIUM))
						customer.setStatus(FWMPConstant.ProspectType.MEDIUM_ID);
				}

				if (updateProspectVO.getRemarks() != null && !updateProspectVO.getRemarks().isEmpty())
					customer.setRemarks(updateProspectVO.getRemarks());

				if (updateProspectVO.getProspectColdReason() != null && !updateProspectVO.getProspectColdReason().isEmpty())
					customer.setRemarks(updateProspectVO
							.getProspectColdReason());

				if (updateProspectVO.getEnquiryType() != null && !updateProspectVO.getEnquiryType().isEmpty())
					customer.setEnquiry(updateProspectVO.getEnquiryType());

				if (updateProspectVO.getSourceOfEnquiry() != null)
					customer.setKnownSource(updateProspectVO
							.getSourceOfEnquiry());

				if(updateProspectVO.getLattitude() != null && !updateProspectVO.getCrpAccountNumber().isEmpty())
				customer.setLattitude(updateProspectVO.getLattitude());

				if(updateProspectVO.getLongitude() != null && !updateProspectVO.getLongitude().isEmpty())
				customer.setLongitude(updateProspectVO.getLongitude());
				
				if(updateProspectVO.getCustomerPhoto() != null && !updateProspectVO.getCustomerPhoto().isEmpty())
					customer.setCustomerPhoto(updateProspectVO.getCustomerPhoto());
				
				if(updateProspectVO.getCustomerEsign() != null && !updateProspectVO.getCustomerEsign().isEmpty())
					customer.setCustomerSign(updateProspectVO.getCustomerEsign());
				
				if(updateProspectVO.getNationality() != null && !updateProspectVO.getNationality().isEmpty())
				customer.setNationality(updateProspectVO.getNationality());
				
				if(updateProspectVO.getExistingIsp() != null && !updateProspectVO.getExistingIsp().isEmpty())
				customer.setExistingISP(updateProspectVO.getExistingIsp());
				
				//customer.setStaticIp(updateProspectVO.getStaticIp());
				
				if(updateProspectVO.getCrpMobileNumber() != null && !updateProspectVO.getCrpMobileNumber().isEmpty())
				customer.setReferralCustomerMobileNo(updateProspectVO.getCrpMobileNumber());
				
				if(updateProspectVO.getCrpAccountNumber() != null && !updateProspectVO.getCrpAccountNumber().isEmpty())
				customer.setReferralCustomerAccountNo(updateProspectVO.getCrpAccountNumber());
				
				//customer.setCouponCode(updateProspectVO.getCouponCode());
				
				if (updateProspectVO.getKycDetails() != null) {

					if (updateProspectVO.isAadhaarForPOI()) {

						try {

							customer.setPoi(Status.ACTIVE);

							if( updateProspectVO.getKycDetails().getPhoto() != null && !updateProspectVO.getKycDetails().getPhoto().isEmpty())
								customer.setCustomerPhoto(updateProspectVO.getKycDetails().getPhoto());

							customer.setAdhaarId(adhaarDao.saveAdhar(updateProspectVO.getKycDetails()));

							customer.setFinahubTransactionId(updateProspectVO.getFinahubTransactionId());
							customerDAO.populateCustomerAadhaarMapping(getCustomerAadhaarMappingForUpdatePospectVO(updateProspectVO));
							
							
							if (updateProspectVO.isAadhaarForPOA())
								customer.setPoa(Status.ACTIVE);
							else
								customer.setPoa(Status.IN_ACTIVE);

						} catch (Exception e) {

							log.info("Not able to create adhar information with updateProspectVO-->" + updateProspectVO
									+ " " + e.getMessage());
							e.printStackTrace();
							return 0;

						}

					} else
						customer.setPoi(Status.IN_ACTIVE);

				}

				session.update(customer);

				log.info("Customer info updated " + customer.getMobileNumber());

			}

			if (updateProspectVO.getTicketId() != null)
			{
				Ticket ticket = (Ticket) session.get(Ticket.class, Long
						.valueOf(updateProspectVO.getTicketId()));

				log.info("Got Ticket details " + ticket);
				
				if(assignToFromCRM != null && assignToFromCRM > 0)
				{
					ticket.setUserByCurrentAssignedTo((User)session.get(User.class, Long
							.valueOf(assignToFromCRM)));
				}
				
				if(deDupFlag != null && !deDupFlag.isEmpty()
						&& DeDupFlag.cleanProspect == Integer
						.valueOf(deDupFlag))
				{
					ticket.setMqStatus(DeDupFlag.deDupFlagSuccess);
				}
				else if(deDupFlag != null && !deDupFlag.isEmpty()
						&& DeDupFlag.duplicatedProspect == Integer
						.valueOf(deDupFlag))
				{
					ticket.setMqStatus(DeDupFlag.deDupFlagFailure);
				}

				ticket.setModifiedBy(AuthUtils.getCurrentUserId());
				ticket.setModifiedOn(new Date());

				if (updateProspectVO.getCxName() != null)
					ticket.setCxName(updateProspectVO.getCxName());

				if (updateProspectVO.getFxName() != null)
					ticket.setFxName(updateProspectVO.getFxName());

				if (updateProspectVO.getCxPermission() != null)
					ticket.setCxPermission(updateProspectVO.getCxPermission());

				if (updateProspectVO.getGxPermission() != null)
					ticket.setGxPermission(updateProspectVO.getGxPermission());

				if (updateProspectVO.getPrefferedCallDate() != null)
					ticket.setPreferedDate(new Date(updateProspectVO
							.getPrefferedCallDate()));

				if (updateProspectVO.getAreaId() != null)
					ticket.setArea((Area) session.load(Area.class, Long
							.valueOf(updateProspectVO.getAreaId())));
				
				if (updateProspectVO.getSubAreaId() != null)
					ticket.setSubArea((SubArea)session.load(SubArea.class, Long
							.valueOf(updateProspectVO.getSubAreaId())));
				
				if (isValuesFromCRM) {
					
					if (!updateProspectVO.isNonFeasible())
						ticket.setStatus(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);
					

					if (updateProspectVO.getProspectType() != null)
					{
						if (updateProspectVO.getProspectType()
								.equalsIgnoreCase(FWMPConstant.ProspectType.COLD))
						{
							if (!updateProspectVO.isNonFeasible())
								ticket.setStatus(TicketStatus.CUSTOMER_REJECTED_BY_SALES);
						}

						else if (updateProspectVO.getProspectType()
								.equalsIgnoreCase(FWMPConstant.ProspectType.HOT))
						{
							if (!updateProspectVO.isNonFeasible())
								ticket.setStatus(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);
						}

						else if (updateProspectVO.getProspectType()
								.equalsIgnoreCase(FWMPConstant.ProspectType.MEDIUM))
						{
							if (!updateProspectVO.isNonFeasible())
								ticket.setStatus(TicketStatus.CUSTOMER_BASIC_INFO_UPDATED);
						}
					}
				}


				if (updateProspectVO.isNonFeasible())
				{
					ticket.setTicketSubCategory(TicketSubCategory.FISIBLE);
					ticket.setSubCategoryId(TicketSubCategory.FEASIBILITY);
				}

				session.update(ticket);

				log.info("Ticket info updated " + ticket);
				

				if (isValuesFromCRM) {
					
					Gis gis = new Gis();

					gis.setId(StrictMicroSecondTimeBasedGuid.newGuid());
					gis.setAddedBy(AuthUtils.getCurrentUserId());
					gis.setAddedOn(new Date());
					gis.setTicketNo(Long.valueOf(updateProspectVO.getTicketId()));

					gis.setModifiedBy(AuthUtils.getCurrentUserId());
					gis.setModifiedOn(new Date());
					gis.setStatus(FWMPConstant.TicketStatus.FEASIBILITY_UPDATED_DURING_SALE);

					if (AuthUtils.getCurrentUserRole() == UserRole.SALES_EX)
						gis.setStatus(FWMPConstant.TicketStatus.PRE_SALE);

					if (updateProspectVO.getAreaId() != null)
						gis.setArea(updateProspectVO.getAreaId());

					if (updateProspectVO.getBranchId() != null)
						gis.setBranch(updateProspectVO.getBranchId());

					if (updateProspectVO.getCityId() != null)
						gis.setCity(updateProspectVO.getCityId());

					if (updateProspectVO.getClusterName() != null)
						gis.setClusterName(updateProspectVO.getClusterName());

					if (updateProspectVO.getCxIpAddress() != null)
						gis.setCxIpAddress(updateProspectVO.getCxIpAddress());

					if (updateProspectVO.getCxMacAddress() != null)
						gis.setCxMacAddress(updateProspectVO.getCxMacAddress());

					if (updateProspectVO.getCxName() != null)
						gis.setCxName(updateProspectVO.getCxName());

					if (updateProspectVO.getCxPorts() != null)
						gis.setCxPorts(updateProspectVO.getCxPorts() + "");

					if (updateProspectVO.getFxIpAddress() != null)
						gis.setFxIpAddress(updateProspectVO.getFxIpAddress());

					if (updateProspectVO.getFxMacAddress() != null)
						gis.setFxMacAddress(updateProspectVO.getFxMacAddress());

					if (updateProspectVO.getFxName() != null)
						gis.setFxName(updateProspectVO.getFxName());

					if (updateProspectVO.getFxPorts() != null)
						gis.setFxPorts(updateProspectVO.getFxPorts() + "");

					if (updateProspectVO.getConnectionType() != null)
						gis.setConnectionType(updateProspectVO.getConnectionType());

					if (updateProspectVO.getTransactionNo() != null)
						gis.setTransactionNo(updateProspectVO.getTransactionNo());

					if (updateProspectVO.getErrorNo() != null)
						gis.setErrorNo(updateProspectVO.getErrorNo());

					if (updateProspectVO.getMessage() != null)
						gis.setMessage(updateProspectVO.getMessage());

					if (updateProspectVO.getConnectionType() != null)
					{
						gis.setConnectionType(updateProspectVO.getConnectionType());

						if (WorkStageName.FIBER.equalsIgnoreCase(updateProspectVO
								.getConnectionType()))
						{
							gis.setWorkstageType((int) WorkStageType.FIBER);
						} else if (WorkStageName.COPPER
								.equalsIgnoreCase(updateProspectVO
										.getConnectionType()))
						{
							gis.setWorkstageType((int) WorkStageType.COPPER);
						}
					}

					gis.setFeasibilityType(FeasibilityType.GIS_SALES);

					gis.setLattitude(updateProspectVO.getLattitude());
					gis.setLongitude(updateProspectVO.getLongitude());

					session.save(gis);
					
					log.debug("Updated Gis details " + gis);
				}

				

				
			}

			if (!updateProspectVO.isNonFeasible())
			{
				APIResponse response = null;
				/*if (updateProspectVO.getSource() != null && updateProspectVO.getSource()
						.equalsIgnoreCase(CurrentCrmNames.DO_NOT_CALL_PROSPECT_SERVICE_IF_SOURCE_IS_SIEBEL)) {

				} else*/
				if (isValuesFromCRM) {
					response = updateTicketActivityLogVo(Activity.BASIC_INFO_UPDATE + "",
							updateProspectVO.getTicketId(), FWMPConstant.TicketStatus.COMPLETED + "", updateProspectVO);
				}

				log.info("Comming out of Basic update dao and total activity log entry are " + response);
			} else
			{
				TicketLog ticketLog = new TicketLogImpl();
				ticketLog.setTicketId(Long.valueOf(updateProspectVO.getTicketId().trim()));
				ticketLog.setAddedBy(AuthUtils.getCurrentUserId());
				ticketLog.setAddedOn(new Date());

				ticketLog.setActivityId(Activity.BASIC_INFO_UPDATE);
				ticketLog.setActivityValue(TicketStatus.COMPLETED+"");
				ticketLog.setId(StrictMicroSecondTimeBasedGuid.newGuid());
				ticketLog.setCategory(TicketUpdateConstant.ACTIVITY_UPDATED);
				ticketLog.setCurrentUpdateCategory(TicketUpdateConstant.ACTIVITY_UPDATED);
				
				Map<String, String> additionalAttributeMap = new HashMap<String, String>();
				additionalAttributeMap
						.put(TicketUpdateConstant.NOTE_KEY, updateProspectVO.getProspectType());
				
				ticketLog.setAdditionalAttributeMap(additionalAttributeMap);
				TicketUpdateGateway.logTicket(ticketLog);
			}

			tx.commit();

			return 1;

		} catch (Exception e)
		{
			log.error("Error while updating basic info " + e.getMessage());

			if (tx != null)
				tx.rollback();

			e.printStackTrace();

			return 0;

		} finally
		{
			session.close();
		}
	}


	@Override
	public int updateTariff(TariffPlanUpdateVO planUpdateVO)
	{
		
		if (planUpdateVO == null)
			return 0;
		
		log.info(" updateTariff :: "  +planUpdateVO.getTicketId() );
		try
		{

			if (planUpdateVO.getRouterRequired().equalsIgnoreCase("Y"))
				planUpdateVO.setRouterRequired(1 + "");
			else
				planUpdateVO.setRouterRequired(0 + "");
			

			if (planUpdateVO.getStaticIPRequired().equalsIgnoreCase("Y"))
				planUpdateVO.setStaticIPRequired(Status.ACTIVE + "");
			else
				planUpdateVO.setStaticIPRequired(Status.IN_ACTIVE + "");
			
			
			
			log.info("########## test  @######## Tariff plan update DAO "
					+ planUpdateVO);

			int row = jdbcTemplate.update(UPDATE_TARIFF_DETAILS, new Object[] {
					planUpdateVO.getTariffPlanId(),
					Integer.valueOf(planUpdateVO.getRouterRequired()),
					planUpdateVO.getRouterModel(),
					planUpdateVO.getCustomerTypeId(),
					planUpdateVO.getExternalRouterRequired(),
					planUpdateVO.getExternalRouterModel(),
					planUpdateVO.getExternalRouterAmount(),
					planUpdateVO.getStaticIPRequired(),
					planUpdateVO.getTicketId() });

			log.info("Total updated row " + row);

			if (row > 0)
			{
				Ticket ticket = hibernateTemplate
						.load(Ticket.class, planUpdateVO.getTicketId());

				if (ticket != null)
				{
					ticket.setStatus(TicketStatus.TARIFF_PLAN_UPDATED);
					hibernateTemplate.update(ticket);

					APIResponse response = updateTicketActivityLogVo(Activity.TARIFF_UPDATE
							+ "", planUpdateVO.getTicketId()
									+ "", FWMPConstant.TicketStatus.COMPLETED
											+ "", null);

					log.info("Ticket logger updated during tariff Update "
							+ response);

				}
			}

			return row;
		} catch (Exception e)
		{
			log.error("Error while updating tariff plan for "
					+ planUpdateVO.getTicketId() + "  " + e.getMessage());
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int updatePayment(PaymentUpdateVO paymentUpdateVO)
	{
		if (paymentUpdateVO == null)
			return 0;
		
		log.debug("updatePayment ::"  +paymentUpdateVO.getTicketId());
		try
		{
			Payment payment = new Payment();

			payment.setId(StrictMicroSecondTimeBasedGuid.newGuid());
			payment.setPaidAmount(paymentUpdateVO.getPaidAmount());
			payment.setPaymentMode(paymentUpdateVO.getPaymentMode());
			payment.setReferenceNo(paymentUpdateVO.getReferenceNo());
			payment.setInstallationCharges(paymentUpdateVO
					.getInstallationCharges());
			payment.setPaidConnection(paymentUpdateVO.getPaidConnection());
			payment.setUploadedOn(new Date());
			payment.setUploadedBy(AuthUtils.getCurrentUserId());
			payment.setSchemeCode(paymentUpdateVO.getSchemeCode());
			payment.setDiscountAmount(paymentUpdateVO.getDiscountAmount());

			hibernateTemplate.save(payment);

			Ticket ticket = hibernateTemplate
					.load(Ticket.class, paymentUpdateVO.getTicketId());

			if (ticket != null)
			{
				ticket.setCafNumber(paymentUpdateVO.getCafNo());
				ticket.setPayment(payment);
				ticket.setReferenceNo(paymentUpdateVO.getReferenceNo());
				ticket.setPaymentStatus(TicketStatus.PAYMENT_RECEIVED);
				ticket.setStatus(TicketStatus.PAYMENT_RECEIVED);
				hibernateTemplate.update(ticket);
			}

			/*
			 * TicketLog logDetail = new
			 * TicketLogImpl(paymentUpdateVO.getTicketId(),
			 * TicketUpdateConstant.ACTIVITY_UPDATED, AuthUtils
			 * .getCurrentUserId());
			 * logDetail.setActivityName("PAYMENT_RECEIVED");
			 * logDetail.setStatusId(TicketStatus.PAYMENT_RECEIVED);
			 * TicketUpdateGateway.logTicket(logDetail);
			 */

			APIResponse response = updateTicketActivityLogVo(Activity.PAYMENT_UPDATE
					+ "", paymentUpdateVO.getTicketId()
							+ "", FWMPConstant.TicketStatus.COMPLETED
									+ "", null);

			log.info("Ticket logger updated during payment update " + response);
			
			documentService.createFolderForProspect(getImageInputForPaymentUpdate(paymentUpdateVO));

			return 1;

		} catch (Exception e)
		{
			log.error("Error while updating Payment info for ticket "
					+ paymentUpdateVO.getTicketId() + ". " + e.getMessage());

			e.printStackTrace();

			return 0;
		}
	}

	private APIResponse updateTicketActivityLogVo(String activityId,
			String ticketId, String activityStatus,
			UpdateProspectVO updateProspectVO)
	{
		log.debug("updateTicketActivityLogVo ::"  +activityId);

		TicketAcitivityDetailsVO ticketAcitivityDetailsVO = new TicketAcitivityDetailsVO();

		ticketAcitivityDetailsVO.setActivityCompletedTime(new Date());
		ticketAcitivityDetailsVO.setActivityId(activityId + "");
		ticketAcitivityDetailsVO.setActivityStatus(activityStatus);
		ticketAcitivityDetailsVO.setTicketId(ticketId + "");

		if (updateProspectVO == null)
			return ticketActivityLogCoreService
					.addTicketActivityLog(ticketAcitivityDetailsVO);
		else
			return ticketActivityLogCoreService
					.addTicketActivityLogCommon(ticketAcitivityDetailsVO, updateProspectVO);

	}

	@Override
	public void seSummary()
	{

	}

	@Override
	public List<Ticket> propspectTypeCount()
	{
		return null;
	}

	@Override
	public int updatePermission(UpdateProspectVO updateProspectVO)
	{
		if(updateProspectVO == null)
			return 0;
		log.debug("updatePermission ::"  +updateProspectVO.getProspectNo());
		try
		{
			log.info("UpdateProspectVO ################ " + updateProspectVO.getMobileNumber());

			if (updateProspectVO.getTicketId() == null)
				return 0;

			Long ticketId = Long.valueOf(updateProspectVO.getTicketId());

			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			boolean fiberStatus = false;

			if (ticket != null)
			{
				if (AuthUtils.isSalesUser())
				{
					ticket.setCxPermission(updateProspectVO.getCxPermission());
					ticket.setGxPermission(updateProspectVO.getGxPermission());
					ticket.setRemarks(updateProspectVO.getRemarks());

					if (updateProspectVO.getPermissionFeasibilityStatus()
							.equalsIgnoreCase(BooleanValue.TRUE))
					{
						ticket.setStatus(TicketStatus.PERMISSION_GRANTED_BY_SALES);
						fiberStatus = true;

						log.info("Permission for cx or gx is granted by se for ticket "
								+ ticketId);

						ticket.setTicketSubCategory(WorkStageName.FIBER);
						ticket.setSubCategoryId(TicketSubCategory.FIBER_ID);

					} else
					{
						ticket.setStatus(TicketStatus.PERMISSION_REJECTED_BY_SALES);

						log.info("Permission not granted for cx or gx for ticket "
								+ ticketId);

						ticket.setRemarks(updateProspectVO
								.getPermissionRejectionReason());

						if (ticket.getCustomer() != null)
						{
							int status = 0;

							if (updateProspectVO.getProspectType() != null)
							{
								if (updateProspectVO.getProspectType()
										.equalsIgnoreCase(FWMPConstant.ProspectType.COLD))
								{
									status = FWMPConstant.ProspectType.COLD_ID;

									if (ticketDAO.isEligibleForRefund(ticketId))
									{
										ticket.setStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND);
										ticket.setPaymentStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND);
									}

								} else if (updateProspectVO.getProspectType()
										.equalsIgnoreCase(FWMPConstant.ProspectType.HOT))
									status = FWMPConstant.ProspectType.HOT_ID;

								else if (updateProspectVO.getProspectType()
										.equalsIgnoreCase(FWMPConstant.ProspectType.MEDIUM))
									status = FWMPConstant.ProspectType.MEDIUM_ID;

								log.info("Updating customer as cold and closing the ticket "
										+ ticketId + " with details status "
										+ status + " PermissionRejectionReason "
										+ updateProspectVO
												.getPermissionRejectionReason());

								int result = customerDAO
										.updateCustomerByTicketId(ticketId, status, updateProspectVO
												.getPermissionRejectionReason());

								log.info("Customer status updated under ticket jump "
										+ result);
							} else
							{
								log.info("Customer status updated under ticket jump got prospect type as null for ticket id "
										+ ticketId);
								return 0;
							}
						}
					}
				} else if (AuthUtils.isNIUser())
				{
					if (updateProspectVO.getPermissionFeasibilityStatus()
							.equalsIgnoreCase(BooleanValue.TRUE))
					{
						ticket.setStatus(TicketStatus.FEASIBILITY_GRANTED_BY_NE);

						// update fx name in Gis for the Sales feasibility

						int resultCount = gisDAO
								.updateSalesFeasibilityWithNeFeasibility(updateProspectVO);

						log.info("Total updated rows during feasibility update of sales are "
								+ resultCount);

					} else
					{
						if (updateProspectVO
								.isFeasibilityRejectedDuringDeployment())
							ticket.setStatus(TicketStatus.FEASIBILITY_REJECTED_BY_NE_DURING_DEPLOYMENT);
						else
							ticket.setStatus(TicketStatus.FEASIBILITY_REJECTED_BY_NE);
					}
				}

				hibernateTemplate.update(ticket);

				log.info("################## fiberStatus " + fiberStatus);

				if (fiberStatus)
				{
					if (!updateProspectVO
							.isFeasibilityRejectedDuringDeployment())
					{

						int result = jdbcTemplate
								.update(UPDATE_WORK_ORDER, new Object[] {
										TicketSubCategory.FIBER_ID, new Date(),
										AuthUtils.getCurrentUserId(),
										ticketId });

						log.info("################## UPDATE_WORK_ORDER in permission "
								+ result);

						int deletedRows = ticketActivityLogDAO
								.deleteTicketActivities(ticketId, TicketSubCategory.FIBER_ID);

						log.info("################## Total deleted rows are "
								+ deletedRows + " for TicketId " + ticketId);

					}
				}
				fiberStatus = false;
				return 1;
			}
			return 0;
		} catch (Exception e)
		{
			e.printStackTrace();
			log.error("Error while updatePermission for "
					+ updateProspectVO.getTicketId() + " " + e.getMessage());
			return 0;
		}
	}

	@Override
	public int updatePropspectType(UpdateProspectVO updateProspectVO)
	{
		
		if(updateProspectVO == null)
			return 0;
		
		log.info(" updatePropspectType ::"  +updateProspectVO.getProspectNo());
		
		try
		{
			long ticketId = Long.valueOf(updateProspectVO.getTicketId());

			Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

			if (ticket != null)
			{
				Customer customer = hibernateTemplate
						.load(Customer.class, ticket.getCustomer().getId());

				if (customer != null)
				{
					if (customer.getStatus()
							.equals(FWMPConstant.ProspectType.COLD_ID)
							&& updateProspectVO.isFromFwmpApp())
					{
						return ProspectType.COLD_ID;
					}

				}

				if (updateProspectVO.getProspectType() != null)
				{
					if (updateProspectVO.getProspectType()
							.equalsIgnoreCase(FWMPConstant.ProspectType.COLD))
					{
						customer.setStatus(FWMPConstant.ProspectType.COLD_ID);
						customer.setRemarks(updateProspectVO
								.getProspectColdReason());

						if (ticket.getPaymentStatus() != null && ticket
								.getPaymentStatus() == TicketStatus.PAYMENT_APPROVED)
						{
							if (updateProspectVO.isFromFwmpApp())
							{
								ticket.setStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND_APPROVAL);

							} else
							{
								ticket.setStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND);

								ticket.setPaymentStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND);
							}
						} else
						{
							ticket.setStatus(TicketStatus.CUSTOMER_REJECTED_BY_SALES);
						}

					} else if (updateProspectVO.getProspectType()
							.equalsIgnoreCase(FWMPConstant.ProspectType.HOT))
					{
						customer.setStatus(FWMPConstant.ProspectType.HOT_ID);
						ticket.setStatus(TicketStatus.OPEN);
						customer.setRemarks(updateProspectVO
								.getProspectColdReason());

					} else if (updateProspectVO.getProspectType()
							.equalsIgnoreCase(FWMPConstant.ProspectType.MEDIUM))
					{
						if (ticket.getPaymentStatus() != null && ticket
								.getPaymentStatus() == TicketStatus.PAYMENT_APPROVED)
						{
							if (updateProspectVO.isFromFwmpApp())
							{
								ticket.setStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND_APPROVAL);

							} else
							{
								ticket.setStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND);

								// ticket.setPaymentStatus(TicketStatus.WAITINING_FOR_PAYMENT_REFUND);
							}
						} else
							ticket.setStatus(TicketStatus.CUSTOMER_REJECTED_BY_SALES);

						customer.setStatus(FWMPConstant.ProspectType.MEDIUM_ID);

						customer.setRemarks(updateProspectVO
								.getProspectColdReason());
					}
				}

				if (updateProspectVO.getPermissionFeasibilityStatus()
						.equalsIgnoreCase(BooleanValue.FALSE))
				{

					APIResponse response = updateTicketActivityLogVo(Activity.BASIC_INFO_UPDATE
							+ "", updateProspectVO
									.getTicketId(), FWMPConstant.TicketStatus.COMPLETED
											+ "", updateProspectVO);

					log.info("Comming out of Basic update dao and total activity log entry for updatePropspectType are "
							+ response);

				}

				hibernateTemplate.update(customer);
				hibernateTemplate.update(ticket);

				return 1;
			}
			return 0;
			//
			// if (updateProspectVO.getProspectType() != null)
			// {
			// if (updateProspectVO.getProspectType()
			// .equalsIgnoreCase(FWMPConstant.ProspectType.COLD))
			// {
			// customerStatus = FWMPConstant.ProspectType.COLD_ID;
			//
			// if (ticketDAO.isEligibleForRefund(ticketId))
			// {
			// ticketStatus = TicketStatus.WAITINING_FOR_PAYMENT_REFUND;
			//
			// paymentStatus = TicketStatus.WAITINING_FOR_PAYMENT_REFUND;
			//
			// } else
			// {
			// ticketStatus = TicketStatus.CUSTOMER_REJECTED_BY_SALES;
			// }
			//
			// } else if (updateProspectVO.getProspectType()
			// .equalsIgnoreCase(FWMPConstant.ProspectType.HOT))
			// {
			// customerStatus = FWMPConstant.ProspectType.HOT_ID;
			// ticketStatus = TicketStatus.OPEN;
			// } else if (updateProspectVO.getProspectType()
			// .equalsIgnoreCase(FWMPConstant.ProspectType.MEDIUM))
			// {
			// customerStatus = FWMPConstant.ProspectType.MEDIUM_ID;
			// ticketStatus = TicketStatus.CUSTOMER_REJECTED_BY_SALES;
			// }
			// }

			// if (updateProspectVO.getPermissionFeasibilityStatus()
			// .equalsIgnoreCase(BooleanValue.FALSE))
			// {
			// APIResponse response =
			// updateTicketActivityLogVo(Activity.BASIC_INFO_UPDATE
			// + "", updateProspectVO
			// .getTicketId(), FWMPConstant.TicketStatus.COMPLETED
			// + "");
			//
			// log.info("Comming out of Basic update dao and total activity log
			// entry for updatePropspectType are "
			// + response);
			//
			// TicketLog logDetail = new TicketLogImpl(Long
			// .valueOf(updateProspectVO
			// .getTicketId()), TicketUpdateConstant.ACTIVITY_UPDATED, AuthUtils
			// .getCurrentUserId());
			//
			// logDetail.setActivityId(Activity.BASIC_INFO_UPDATE);
			// logDetail
			// .setActivityValue(FWMPConstant.TicketStatus.CUSTOMER_BASIC_INFO_UPDATED
			// + "");
			// TicketUpdateGateway.logTicket(logDetail);
			//
			// // log.info("UPDATE_PROPSPECT_TYPE " + UPDATE_PROPSPECT_TYPE
			// // + " customerStatus " + customerStatus + " ticketStatus "
			// // + ticketStatus + updateProspectVO.getTicketId());
			// //
			// // return jdbcTemplate.update(UPDATE_PROPSPECT_TYPE, new Object[]
			// {
			// // customerStatus, updateProspectVO.getProspectColdReason(),
			// // ticketStatus, paymentStatus, ticketId });
			// } else
			// {
			// // log.info("UPDATE_PROPSPECT_TYPE_DURING_SALES_FEASIBILITY "
			// // + UPDATE_PROPSPECT_TYPE_DURING_SALES_FEASIBILITY
			// // + " customerStatus " + customerStatus + " ticketStatus "
			// // + ticketStatus + " updateProspectVO.getLattitude() "
			// // + updateProspectVO.getLattitude()
			// // + "updateProspectVO.getLongitude() "
			// // + updateProspectVO.getLongitude()
			// // + "updateProspectVO.getTicketId()"
			// // + updateProspectVO.getTicketId());
			// //
			// // return jdbcTemplate
			// // .update(UPDATE_PROPSPECT_TYPE_DURING_SALES_FEASIBILITY, new
			// // Object[] {
			// // customerStatus,
			// // updateProspectVO.getProspectColdReason(),
			// // ticketStatus, updateProspectVO.getLattitude(),
			// // updateProspectVO.getLongitude(), ticketId });
			// }
		} catch (Exception e)
		{
			log.error("Error while updatePropspectType for "
					+ updateProspectVO.getTicketId() + " " + e.getMessage());
			e.printStackTrace();
			return 0;
		}

	}

	@Override
	public void triggerMessageAfterBasicInfoUpdate(Long ticketId,
			boolean isFeasible)
	{
		if(ticketId == null || ticketId <= 0)
			return;
		
		log.info("Sending message to ticket " + ticketId + " isFeasible "
				+ isFeasible);

		Ticket ticket = hibernateTemplate.load(Ticket.class, ticketId);

		if (ticket != null)
		{
			if (MQStatus.SUCCESS.equals(ticket.getMqStatus()))
			{
				if (!isFeasible)
				{
					log.info("Sending sms after basic info update while ticket is feasible "
							+ ticketId);
					notificationService
							.sendFeasibilityNotificationByTicketIds(Collections
									.singletonList(BigInteger
											.valueOf(ticketId)), DefinitionCoreService.PROSPECT_FEASIBLE_MESSAGE_KEY, ticket
													.getProspectNo());
				} else
				{
					log.info("Sending sms after basic info update while ticket is non feasible "
							+ ticketId);
					notificationService
							.sendFeasibilityNotificationByTicketIds(Collections
									.singletonList(BigInteger
											.valueOf(ticketId)), DefinitionCoreService.PROSPECT_NON_FEASIBLE_MESSAGE_KEY, ticket
													.getProspectNo());
				}

			} else
				log.info("Mq status is failed");

		} else
			log.info("Ticket does not exist for ticket id while sending sms after basic info update "
					+ ticketId);
	}
	
	/**@author noor
	 * CustomerAadhaarMapping
	 * @param updateProspectVO
	 * @return
	 */
	private CustomerAadhaarMapping getCustomerAadhaarMappingForUpdatePospectVO(UpdateProspectVO updateProspectVO) {
		
		
		log.info("Update ProspectVo for CustomerAadhaarMapping : "+updateProspectVO);
		CustomerAadhaarMapping aadhaarMapping = new CustomerAadhaarMapping();
		
		aadhaarMapping.setCustomerId(Long.valueOf(updateProspectVO.getCustomerId()));
		aadhaarMapping.setTicketId(Long.valueOf(updateProspectVO.getTicketId()));
		aadhaarMapping.setAddedBy(AuthUtils.getCurrentUserId());
		aadhaarMapping.setAdhaarTrnId(updateProspectVO.getFinahubTransactionId());
	
		aadhaarMapping.setAddedOn(new Date());
		aadhaarMapping.setActivityId(Activity.BASIC_INFO_UPDATE);
		
		if( updateProspectVO.getAadhaarTransactionType().equalsIgnoreCase(AadhaarTransactionType._PRIMARY))
			aadhaarMapping.setLabel(Long.valueOf(AadhaarTransactionType.PRIMARY));
		else if( updateProspectVO.getAadhaarTransactionType().equalsIgnoreCase(AadhaarTransactionType._SECONDARY))
			aadhaarMapping.setLabel(Long.valueOf(AadhaarTransactionType.SECONDARY));
		
		return aadhaarMapping;
	}
	
	/* (non-Javadoc)
	 * @see com.cupola.fwmp.dao.sales.SalesDAO#updatesSalesExecutiveId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public int updatesSalesExecutiveId(Long ticketId, Long userId) {

		log.info("Updating sales executive for ticket " + ticketId + " and sales executive is " + userId);

		int result = 0;

//		try {
//
//			result = jdbcTemplate.update(UPDATE_SALES_DONE_BY, new Object[] { userId, ticketId });
//
//			log.info("Updating sales executive for ticket " + ticketId + " and sales executive is " + userId + " query "
//					+ UPDATE_SALES_DONE_BY);
//
//			log.info("updated sales executive for ticket " + ticketId + " and sales executive is " + userId
//					+ " and result is " + result);
//
//		} catch (Exception e) {
//
//			log.error("Exception while Updating sales executive for ticket " + ticketId + " and sales executive is "
//					+ userId, e);
//			e.printStackTrace();
//		}
		return result;
	}
	
	/**@author noor
	 * ImageInput
	 * @param paymentUpdateVO
	 * @return
	 */
	private ImageInput getImageInputForPaymentUpdate(PaymentUpdateVO paymentUpdateVO) {
		
		ImageInput imageInput = new ImageInput();
		
		imageInput.setTicketId(String.valueOf(paymentUpdateVO.getTicketId()));
		imageInput.setAadhaarTransactionType(paymentUpdateVO.getAadhaarTransactionType());
		imageInput.setKycData(paymentUpdateVO.getKycData());
		imageInput.setTransactionId(paymentUpdateVO.getTransactionId());
		imageInput.setActivityId(Activity.PAYMENT_UPDATE);
		
		return imageInput;
	}
}
