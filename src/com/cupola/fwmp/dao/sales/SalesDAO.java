/**
 * 
 */
package com.cupola.fwmp.dao.sales;

import java.util.List;

import com.cupola.fwmp.persistance.entities.Ticket;
import com.cupola.fwmp.vo.counts.SalesTypeCountVO;
import com.cupola.fwmp.vo.sales.PaymentUpdateVO;
import com.cupola.fwmp.vo.sales.RouterVo;
import com.cupola.fwmp.vo.sales.TariffPlanUpdateVO;
import com.cupola.fwmp.vo.sales.UpdateProspectVO;

/**
 * @author aditya
 * 
 */
public interface SalesDAO
{

//	String GET_SE_RIBBON_COUNT = "select c.id as customerId, c.status as prospectStatus, t.id as ticketId, t.status as ticketStatus,"
//			+ " t.paymentStatus as paymentStatus, t.documentStatus as documentStatus, t.currentAssignedTo as assignedTo from Customer c, Ticket t  "
//			+ "where t.status !=:ticketStatus and t.customerId=c.id and t.id in(:ticketIds) ";
	

	String GET_SE_RIBBON_COUNT = "select c.id as customerId, c.status as prospectStatus, t.id as ticketId, "
			+ "t.status as ticketStatus,t.paymentStatus as paymentStatus, t.documentStatus as documentStatus, "
			+ "t.currentAssignedTo as assignedTo, t.source as ticketSource , t.poiStatus as poiDocumentStatus , t.poaStatus as poaDocumentStatus from Customer c inner join Ticket t on t.customerId=c.id"
			+ " where t.status !=:ticketStatus  ";
	
	

	String GET_TICKET_COUNT = "select t.status,count(t.id) from Ticket t where "
			+ "t.paymentStatus in(:paymentStatus) OR t.documentStatus in (:documentStatus) and   "
			+ "t.id in (:ticketIds) group by t.status";

	String GET_CUSTOMER_COUNT = "select c.status,count(c.id) from Customer c where "
			+ "t.paymentStatus in(:paymentStatus) OR t.documentStatus in (:documentStatus) and   "
			+ "t.id in ((:ticketIds) group by t.status";

	String UPDATE_WORK_ORDER = "update WorkOrder set currentWorkStage=? , "
			+ "modifiedOn = ?, modifiedBy= ? where ticketId=?";

	String UPDATE_SALES_DONE_BY = "update Ticket set salesDoneBy=? where id = ?";
	
	// String GET_SE_RIBBON_COUNT =
	// "select c.* from Customer c, Ticket t  where t.status !=:ticketStatus"
	// +
	// " and t.currentAssignedTo=:userId and t.customerId=c.id and t.id in(:ticketIds) ";

	String UPDATE_TARIFF_DETAILS = "update Customer cu, Ticket t set cu.tariffId=?, cu.routerRequired=?, "
			+ "cu.routerDescription=?, cu.CustomerType=?, cu.externalRouterRequired=?, cu.externalRouterDescription=?, "
			+ "cu.externalRouterAmount=?,cu.isStaticIPRequired=? where t.customerId=cu.id and t.id=? ";

	String UPDATE_PROPSPECT_TYPE = "update Customer cu, Ticket t set cu.status=?, cu.remarks=? , t.status=?,t.paymentStatus=? where t.customerId=cu.id and t.id=? ";
	
	String UPDATE_PROPSPECT_TYPE_DURING_SALES_FEASIBILITY = "update Customer cu, Ticket t set cu.status=?, cu.remarks=?, t.status=?, cu.lattitude=?, cu.longitude=? where t.customerId=cu.id and t.id=? ";

	List<SalesTypeCountVO> customerTypeCount(long userId);

	List<Ticket> propspectTypeCount();

	int updateBasicInfo(UpdateProspectVO updateProspectVO,boolean isValuesFromCRM, Long assignToFromCRM,String deDupFlag);

	int updateTariff(TariffPlanUpdateVO planUpdateVO);

	int updatePayment(PaymentUpdateVO paymentUpdateVO);

	void seSummary();

	int updatePermission(UpdateProspectVO updateProspectVO);

	int updatePropspectType(UpdateProspectVO updateProspectVO);
	
	void triggerMessageAfterBasicInfoUpdate(Long ticketId, boolean isFeasible);
	int updatesSalesExecutiveId(Long ticketId, Long userId);
}
