package com.cupola.fwmp.collection;

public interface MongoCollections
{
	String TAB_COLLECTION = "Tab";
	String USER_COLLECTION_NAME = "User";
	String USERGROUP_COLLECTION_NAME = "UserGroup";
	String ROLE_COLLECTION_NAME = "Role";
	String USER_GROUP_ROLE_MAPPING_COLLECTION_NAME = "UserGroupRoleMapping";
	String USER_GROUP_USER_MAPPING_COLLECTION_NAME = "UserGroupUserMapping";
	String USER_SUBSCRIPTION = "UserSubscription";
	String DEPLOYMENT_GIS_INPUT = "DEPLOYMENT_GIS_INPUT";
	String SALES_GIS_INPUT = "SALES_GIS_INPUT";
	String DEPLOYMENT_GIS_OUTPUT = "DEPLOYMENT_GIS_OUTPUT";
	String SALES_GIS_OUTPUT = "SALES_GIS_OUTPUT";
	
	public static String MATERIAL_REQUEST_COLLECTION="MaterialRequest"; 
}
