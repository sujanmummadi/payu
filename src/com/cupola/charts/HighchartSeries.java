package com.cupola.charts;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mahesh
 *
 */
public class HighchartSeries {
	
	private String name;
	private List data;
	private String color;
	private boolean colorByPoint=false;
	private String stack;
	public String getStack()
	{
		return stack;
	}

	public void setStack(String stack)
	{
		this.stack = stack;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

	public boolean isColorByPoint() {
		return colorByPoint;
	}

	public void setColorByPoint(boolean colorByPoint) {
		this.colorByPoint = colorByPoint;
	}

	public String getColor()
	{
		return color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}
	
	
}
