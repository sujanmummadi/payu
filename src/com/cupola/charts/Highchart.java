package com.cupola.charts;

import java.util.ArrayList;
import java.util.List;


/**
 * @author mahesh
 *
 */
public class Highchart {

	private String chartBackgroundColor="#FFFFFF";
	private String chartype="line";
	//zoomType may be x, y, xy
	private String chartZoomType="xy";

	private String titleText="Chart Title";
	private String titleStyleColor="#333333";
	private String titleStyleFontWeight="bold";
	private String titleStyleFontSize="18px";


	private String subtitleText="sub title";
	private String subtitleStyleColor="#555555";
	private String subtitleStyleFontWeight="normal";
	private String subtitleStyleFontSize="15px";


	//Enable or disable animation of the tooltip. In slow legacy IE browsers the animation is disabled by default. Defaults to true.
	private boolean tooltipAnimation=true;
	//The background color or gradient for the tooltip. Defaults to rgba(255, 255, 255, 0.85).
	private String tooltipBackgroundColor="rgba(255, 255, 255, 0.85)";
	//The color of the tooltip border. When null, the border takes the color of the corresponding series or point. Defaults to null.
	private String tooltipBorderColor;
	//enable or disable tooltip
	private boolean tooltipEnabled=true;
	private boolean tooltipShared=false;
	private String tooltipPointFormat="<span style=\"color:{point.color}\">\u25CF</span> {series.name}: <b>{point.y}</b><br/>.";
	private String tooltipHeaderFormat="<span style=\"font-size: 10px\">{point.key}</span><br/>";
	private String exportingFilename="chart";
	private Boolean exportingButtonsContextButtonEnabled=true;
	private int sizeHeight=0;
	private int sizeWidth=0;
	public String getTooltipHeaderFormat()
	{
		return tooltipHeaderFormat;
	}
	public void setTooltipHeaderFormat(String tooltipHeaderFormat)
	{
		this.tooltipHeaderFormat = tooltipHeaderFormat;
	}
	public String getTooltipPointFormat()
	{
		return tooltipPointFormat;
	}
	public void setTooltipPointFormat(String tooltipPointFormat)
	{
		this.tooltipPointFormat = tooltipPointFormat;
	}
	public boolean isTooltipShared()
	{
		return tooltipShared;
	}
	public void setTooltipShared(boolean tooltipShared)
	{
		this.tooltipShared = tooltipShared;
	}
	private boolean plotOptionsPieDataLabelsEnabled=false;
	private boolean plotOptionsPieAllowPointSelect=true;
	private boolean plotOptionsPieAnimation=true;
	private String plotOptionsPieCursor="pointer";
	private boolean plotOptionsPieShowInLegend=true;
	private String plotOptionsAreaStacking;
	private String plotOptionsAreaLineColor;
	private String plotOptionsColumnStacking;
	private boolean plotOptionsColumnDataLabelsEnabled=false;
	public boolean isPlotOptionsColumnDataLabelsEnabled()
	{
		return plotOptionsColumnDataLabelsEnabled;
	}
	public void setPlotOptionsColumnDataLabelsEnabled(
			boolean plotOptionsColumnDataLabelsEnabled)
	{
		this.plotOptionsColumnDataLabelsEnabled = plotOptionsColumnDataLabelsEnabled;
	}
	public String getPlotOptionsColumnStacking()
	{
		return plotOptionsColumnStacking;
	}
	public void setPlotOptionsColumnStacking(String plotOptionsColumnStacking)
	{
		this.plotOptionsColumnStacking = plotOptionsColumnStacking;
	}
	public String getPlotOptionsBarStacking()
	{
		return plotOptionsBarStacking;
	}
	public void setPlotOptionsBarStacking(String plotOptionsBarStacking)
	{
		this.plotOptionsBarStacking = plotOptionsBarStacking;
	}
	private String plotOptionsBarStacking;
	private List series;	

	private List xaxisCategories;
	private String xaxisType;
	private String xaxisTitleText="x axis title";
	//CSS styles for the title. 
	private String xaxisTitleStyleColor="#707070";
	private String xaxisTitleStyleFontWeight="bold";
	private String xaxisTitleStyleFontSize="12px";


	private String yaxisTitleText="y axis title";
	//CSS styles for the title. 
	private String yaxisTitleStyleColor="#707070";
	private String yaxisTitleStyleFontWeight="bold";
	private String yaxisTitleStyleFontSize="12px";


	//The horizontal alignment of the legend box within the chart area. Valid values are left, center and right.
	private String legendAlign="center";
	private boolean legendEnabled=true;		
    private String legendVerticalAlign= "bottom";
    private String legendLayout="horizontal";
    private int legendX=0;
    private int legendY=0;

	//Highchart by default puts a credits label in the lower right corner of the chart. This can be changed using these options.
	//Whether to show the credits text. Defaults to true.
	private boolean creditsEnabled=false;
	//The URL for the credits label. Defaults to http://www.highcharts.com.
	private String creditsHref="http://www.highcharts.com";
	//The text for the credits label. Defaults to Highcharts.com.
	private String creditsText="Highcharts.com";
	public String getChartBackgroundColor() {
		return chartBackgroundColor;
	}
	public void setChartBackgroundColor(String chartBackgroundColor) {
		this.chartBackgroundColor = chartBackgroundColor;
	}
	public String getChartype() {
		return chartype;
	}
	public void setChartype(String chartype) {
		this.chartype = chartype;
	}
	public String getChartZoomType() {
		return chartZoomType;
	}
	public void setChartZoomType(String chartZoomType) {
		this.chartZoomType = chartZoomType;
	}
	public String getTitleText() {
		return titleText;
	}
	public void setTitleText(String titleText) {
		this.titleText = titleText;
	}
	public String getTitleStyleColor() {
		return titleStyleColor;
	}
	public void setTitleStyleColor(String titleStyleColor) {
		this.titleStyleColor = titleStyleColor;
	}
	public String getTitleStyleFontWeight() {
		return titleStyleFontWeight;
	}
	public void setTitleStyleFontWeight(String titleStyleFontWeight) {
		this.titleStyleFontWeight = titleStyleFontWeight;
	}
	public String getTitleStyleFontSize() {
		return titleStyleFontSize;
	}
	public void setTitleStyleFontSize(String titleStyleFontSize) {
		this.titleStyleFontSize = titleStyleFontSize;
	}
	public String getSubtitleText() {
		return subtitleText;
	}
	public void setSubtitleText(String subtitleText) {
		this.subtitleText = subtitleText;
	}
	public String getSubtitleStyleColor() {
		return subtitleStyleColor;
	}
	public void setSubtitleStyleColor(String subtitleStyleColor) {
		this.subtitleStyleColor = subtitleStyleColor;
	}
	public String getSubtitleStyleFontWeight() {
		return subtitleStyleFontWeight;
	}
	public void setSubtitleStyleFontWeight(String subtitleStyleFontWeight) {
		this.subtitleStyleFontWeight = subtitleStyleFontWeight;
	}
	public String getSubtitleStyleFontSize() {
		return subtitleStyleFontSize;
	}
	public void setSubtitleStyleFontSize(String subtitleStyleFontSize) {
		this.subtitleStyleFontSize = subtitleStyleFontSize;
	}
	public boolean isTooltipAnimation() {
		return tooltipAnimation;
	}
	public void setTooltipAnimation(boolean tooltipAnimation) {
		this.tooltipAnimation = tooltipAnimation;
	}
	public String getTooltipBackgroundColor() {
		return tooltipBackgroundColor;
	}
	public void setTooltipBackgroundColor(String tooltipBackgroundColor) {
		this.tooltipBackgroundColor = tooltipBackgroundColor;
	}
	public String getTooltipBorderColor() {
		return tooltipBorderColor;
	}
	public void setTooltipBorderColor(String tooltipBorderColor) {
		this.tooltipBorderColor = tooltipBorderColor;
	}
	public boolean isTooltipEnabled() {
		return tooltipEnabled;
	}
	public void setTooltipEnabled(boolean tooltipEnabled) {
		this.tooltipEnabled = tooltipEnabled;
	}

	public boolean isPlotOptionsPieDataLabelsEnabled() {
		return plotOptionsPieDataLabelsEnabled;
	}
	public void setPlotOptionsPieDataLabelsEnabled(
			boolean plotOptionsPieDataLabelsEnabled) {
		this.plotOptionsPieDataLabelsEnabled = plotOptionsPieDataLabelsEnabled;
	}
	public boolean isPlotOptionsPieAllowPointSelect() {
		return plotOptionsPieAllowPointSelect;
	}
	public void setPlotOptionsPieAllowPointSelect(
			boolean plotOptionsPieAllowPointSelect) {
		this.plotOptionsPieAllowPointSelect = plotOptionsPieAllowPointSelect;
	}
	public boolean isPlotOptionsPieAnimation() {
		return plotOptionsPieAnimation;
	}
	public void setPlotOptionsPieAnimation(boolean plotOptionsPieAnimation) {
		this.plotOptionsPieAnimation = plotOptionsPieAnimation;
	}
	public String getPlotOptionsPieCursor() {
		return plotOptionsPieCursor;
	}
	public void setPlotOptionsPieCursor(String plotOptionsPieCursor) {
		this.plotOptionsPieCursor = plotOptionsPieCursor;
	}
	public boolean isPlotOptionsPieShowInLegend() {
		return plotOptionsPieShowInLegend;
	}
	public void setPlotOptionsPieShowInLegend(boolean plotOptionsPieShowInLegend) {
		this.plotOptionsPieShowInLegend = plotOptionsPieShowInLegend;
	}
	public List getSeries() {
		return series;
	}
	public void setSeries(List series) {
		this.series = series;
	}
	public List getXaxisCategories() {
		return xaxisCategories;
	}
	public void setXaxisCategories(List xaxisCategories) {
		this.xaxisCategories = xaxisCategories;
	}
	public String getXaxisType() {
		return xaxisType;
	}
	public void setXaxisType(String xaxisType) {
		this.xaxisType = xaxisType;
	}
	public String getXaxisTitleText() {
		return xaxisTitleText;
	}
	public void setXaxisTitleText(String xaxisTitleText) {
		this.xaxisTitleText = xaxisTitleText;
	}
	public String getXaxisTitleStyleColor() {
		return xaxisTitleStyleColor;
	}
	public void setXaxisTitleStyleColor(String xaxisTitleStyleColor) {
		this.xaxisTitleStyleColor = xaxisTitleStyleColor;
	}
	public String getXaxisTitleStyleFontWeight() {
		return xaxisTitleStyleFontWeight;
	}
	public void setXaxisTitleStyleFontWeight(String xaxisTitleStyleFontWeight) {
		this.xaxisTitleStyleFontWeight = xaxisTitleStyleFontWeight;
	}
	public String getXaxisTitleStyleFontSize() {
		return xaxisTitleStyleFontSize;
	}
	public void setXaxisTitleStyleFontSize(String xaxisTitleStyleFontSize) {
		this.xaxisTitleStyleFontSize = xaxisTitleStyleFontSize;
	}
	public String getYaxisTitleText() {
		return yaxisTitleText;
	}
	public void setYaxisTitleText(String yaxisTitleText) {
		this.yaxisTitleText = yaxisTitleText;
	}
	public String getYaxisTitleStyleColor() {
		return yaxisTitleStyleColor;
	}
	public void setYaxisTitleStyleColor(String yaxisTitleStyleColor) {
		this.yaxisTitleStyleColor = yaxisTitleStyleColor;
	}
	public String getYaxisTitleStyleFontWeight() {
		return yaxisTitleStyleFontWeight;
	}
	public void setYaxisTitleStyleFontWeight(String yaxisTitleStyleFontWeight) {
		this.yaxisTitleStyleFontWeight = yaxisTitleStyleFontWeight;
	}
	public String getYaxisTitleStyleFontSize() {
		return yaxisTitleStyleFontSize;
	}
	public void setYaxisTitleStyleFontSize(String yaxisTitleStyleFontSize) {
		this.yaxisTitleStyleFontSize = yaxisTitleStyleFontSize;
	}
	public String getLegendAlign() {
		return legendAlign;
	}
	public void setLegendAlign(String legendAlign) {
		this.legendAlign = legendAlign;
	}
	public boolean isLegendEnabled() {
		return legendEnabled;
	}
	public void setLegendEnabled(boolean legendEnabled) {
		this.legendEnabled = legendEnabled;
	}
	public boolean isCreditsEnabled() {
		return creditsEnabled;
	}
	public void setCreditsEnabled(boolean creditsEnabled) {
		this.creditsEnabled = creditsEnabled;
	}
	public String getCreditsHref() {
		return creditsHref;
	}
	public void setCreditsHref(String creditsHref) {
		this.creditsHref = creditsHref;
	}
	public String getCreditsText() {
		return creditsText;
	}
	public void setCreditsText(String creditsText) {
		this.creditsText = creditsText;
	}
	public String getPlotOptionsAreaStacking()
	{
		return plotOptionsAreaStacking;
	}
	public void setPlotOptionsAreaStacking(String plotOptionsAreaStacking)
	{
		this.plotOptionsAreaStacking = plotOptionsAreaStacking;
	}
	public String getPlotOptionsAreaLineColor()
	{
		return plotOptionsAreaLineColor;
	}
	public void setPlotOptionsAreaLineColor(String plotOptionsAreaLineColor)
	{
		this.plotOptionsAreaLineColor = plotOptionsAreaLineColor;
	}
	public String getExportingFilename()
	{
		return exportingFilename;
	}
	public void setExportingFilename(String exportingFilename)
	{
		this.exportingFilename = exportingFilename;
	}
	public Boolean getExportingButtonsContextButtonEnabled()
	{
		return exportingButtonsContextButtonEnabled;
	}
	public void setExportingButtonsContextButtonEnabled(
			Boolean exportingButtonsContextButtonEnabled)
	{
		this.exportingButtonsContextButtonEnabled = exportingButtonsContextButtonEnabled;
	}
	public int getSizeHeight()
	{
		return sizeHeight;
	}
	public void setSizeHeight(int sizeHeight)
	{
		this.sizeHeight = sizeHeight;
	}
	public int getSizeWidth()
	{
		return sizeWidth;
	}
	public void setSizeWidth(int sizeWidth)
	{
		this.sizeWidth = sizeWidth;
	}
	public String getLegendVerticalAlign() {
		return legendVerticalAlign;
	}
	public void setLegendVerticalAlign(String legendVerticalAlign) {
		this.legendVerticalAlign = legendVerticalAlign;
	}
	public String getLegendLayout() {
		return legendLayout;
	}
	public void setLegendLayout(String legendLayout) {
		this.legendLayout = legendLayout;
	}
	public int getLegendX() {
		return legendX;
	}
	public void setLegendX(int legendX) {
		this.legendX = legendX;
	}
	public int getLegendY() {
		return legendY;
	}
	public void setLegendY(int legendY) {
		this.legendY = legendY;
	}
	
}
