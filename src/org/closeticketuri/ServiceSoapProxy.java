package org.closeticketuri;

public class ServiceSoapProxy implements org.closeticketuri.ServiceSoap {
  private String _endpoint = null;
  private org.closeticketuri.ServiceSoap serviceSoap = null;
  
  public ServiceSoapProxy() {
    _initServiceSoapProxy();
  }
  
  public ServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initServiceSoapProxy();
  }
  
  private void _initServiceSoapProxy() {
    try {
      serviceSoap = (new org.closeticketuri.ServiceLocator()).getServiceSoap();
      if (serviceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)serviceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)serviceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (serviceSoap != null)
      ((javax.xml.rpc.Stub)serviceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.closeticketuri.ServiceSoap getServiceSoap() {
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap;
  }
  
  public java.lang.String processOrder(java.lang.String processORderXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.processOrder(processORderXML, referenceNo);
  }
  
  public java.lang.String purchaseEvent(java.lang.String purchaseEventXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.purchaseEvent(purchaseEventXML, referenceNo);
  }
  
  public java.lang.String getRemainingUnits(java.lang.String getRemainingUnitsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getRemainingUnits(getRemainingUnitsXML, referenceNo);
  }
  
  public java.lang.String rateDataRecord(java.lang.String rateDataRecordXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.rateDataRecord(rateDataRecordXML, referenceNo);
  }
  
  public java.lang.String cancelEvent(java.lang.String cancelEventXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.cancelEvent(cancelEventXML, referenceNo);
  }
  
  public java.lang.String availableUsageLimit(java.lang.String availableUsageLimitXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.availableUsageLimit(availableUsageLimitXML, referenceNo);
  }
  
  public java.lang.String getUsageDetails(java.lang.String getUsageDetailsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getUsageDetails(getUsageDetailsXML, referenceNo);
  }
  
  public java.lang.String makePayment(java.lang.String makePaymentXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.makePayment(makePaymentXML, referenceNo);
  }
  
  public java.lang.String registerTicketForSelfcare(java.lang.String registerTicketForSelfcareXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.registerTicketForSelfcare(registerTicketForSelfcareXML, referenceNo);
  }
  
  public java.lang.String modifyTicket(java.lang.String modifyTicketXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyTicket(modifyTicketXML, referenceNo);
  }
  
  public java.lang.String modifyTicketForSelfcare(java.lang.String modifyTicketForSelfcareXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyTicketForSelfcare(modifyTicketForSelfcareXML, referenceNo);
  }
  
  public java.lang.String createProspect(java.lang.String createProspectXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.createProspect(createProspectXML, referenceNo);
  }
  
  public java.lang.String getSubscriberTickets(java.lang.String getSubscriberTicketsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getSubscriberTickets(getSubscriberTicketsXML, referenceNo);
  }
  
  public java.lang.String getTicketDetails(java.lang.String getTicketDetailsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getTicketDetails(getTicketDetailsXML, referenceNo);
  }
  
  public java.lang.String authenticateUser(java.lang.String authenticateUserXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.authenticateUser(authenticateUserXML, referenceNo);
  }
  
  public java.lang.String attachDocument(java.lang.String attachDocumentXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.attachDocument(attachDocumentXML, referenceNo);
  }
  
  public java.lang.String createUserSelfCare(java.lang.String createUserSelfCareXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.createUserSelfCare(createUserSelfCareXML, referenceNo);
  }
  
  public java.lang.String changePassword(java.lang.String changePasswordXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.changePassword(changePasswordXML, referenceNo);
  }
  
  public java.lang.String getSubscriptionValidity(java.lang.String subscriptionvalidityXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getSubscriptionValidity(subscriptionvalidityXML, referenceNo);
  }
  
  public java.lang.String createCustomer(java.lang.String customerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.createCustomer(customerXML, referenceNo);
  }
  
  public java.lang.String renewContract(java.lang.String renewalContractXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.renewContract(renewalContractXML, referenceNo);
  }
  
  public java.lang.String reconnectContract(java.lang.String reconnectContractXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.reconnectContract(reconnectContractXML, referenceNo);
  }
  
  public java.lang.String disconnectContract(java.lang.String disconnectContractXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.disconnectContract(disconnectContractXML, referenceNo);
  }
  
  public java.lang.String topUp(java.lang.String topupXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.topUp(topupXML, referenceNo);
  }
  
  public java.lang.String getCustomerInfo(java.lang.String customerInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getCustomerInfo(customerInfoXML, referenceNo);
  }
  
  public java.lang.String modifyCustomer(java.lang.String modifyCustomerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyCustomer(modifyCustomerXML, referenceNo);
  }
  
  public java.lang.String addContract(java.lang.String addContractXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.addContract(addContractXML, referenceNo);
  }
  
  public java.lang.String getPlaninfo(java.lang.String planinfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getPlaninfo(planinfoXML, referenceNo);
  }
  
  public java.lang.String getAllPlans(java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getAllPlans(referenceNo);
  }
  
  public java.lang.String getContractsByKey(java.lang.String getContractsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getContractsByKey(getContractsXML, referenceNo);
  }
  
  public java.lang.String registerTicket(java.lang.String registerTicketXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.registerTicket(registerTicketXML, referenceNo);
  }
  
  public java.lang.String getTicketCategory(java.lang.String getTicketCategoryXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getTicketCategory(getTicketCategoryXML, referenceNo);
  }
  
  public java.lang.String getCustomerConsolidatedView(java.lang.String getCustomerConsolidatedViewXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getCustomerConsolidatedView(getCustomerConsolidatedViewXML, referenceNo);
  }
  
  public java.lang.String redeemPPC(java.lang.String redeemPPCXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.redeemPPC(redeemPPCXML, referenceNo);
  }
  
  public java.lang.String getBills(java.lang.String getBillsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getBills(getBillsXML, referenceNo);
  }
  
  public java.lang.String getBillDetails(java.lang.String getBillDetailsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getBillDetails(getBillDetailsXML, referenceNo);
  }
  
  public java.lang.String getPayments(java.lang.String getPaymentsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getPayments(getPaymentsXML, referenceNo);
  }
  
  public java.lang.String usageExhausted(java.lang.String usageExhaustedXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.usageExhausted(usageExhaustedXML, referenceNo);
  }
  
  public java.lang.String getCustomerBalance(java.lang.String getCustomerBalanceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getCustomerBalance(getCustomerBalanceXML, referenceNo);
  }
  
  public java.lang.String reportSuspend(java.lang.String suspendContractXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.reportSuspend(suspendContractXML, referenceNo);
  }
  
  public java.lang.String getDocumentsByCustomer(java.lang.String getDocumentsByCustomerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getDocumentsByCustomer(getDocumentsByCustomerXML, referenceNo);
  }
  
  public java.lang.String getInvoicesByPayment(java.lang.String getInvoicesByPaymentXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getInvoicesByPayment(getInvoicesByPaymentXML, referenceNo);
  }
  
  public java.lang.String getCardDetailsByCustomer(java.lang.String getCardDetailsByCustomerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getCardDetailsByCustomer(getCardDetailsByCustomerXML, referenceNo);
  }
  
  public java.lang.String getDocumentByDocNbr(java.lang.String getDocumentByDocNbrXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getDocumentByDocNbr(getDocumentByDocNbrXML, referenceNo);
  }
  
  public java.lang.String getMostRecentInvoice(java.lang.String getMostRecentInvoiceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getMostRecentInvoice(getMostRecentInvoiceXML, referenceNo);
  }
  
  public java.lang.String getProvisioningRequests(java.lang.String getProvisioningRequestsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getProvisioningRequests(getProvisioningRequestsXML, referenceNo);
  }
  
  public java.lang.String updateProvisioningResponse(java.lang.String updateProvisioningResponseXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.updateProvisioningResponse(updateProvisioningResponseXML, referenceNo);
  }
  
  public java.lang.String sendMailToProvAdmin(java.lang.String sendMailToProvAdminXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.sendMailToProvAdmin(sendMailToProvAdminXML, referenceNo);
  }
  
  public java.lang.String deleteCardDetailsByCustomer(java.lang.String deleteCardDetailsByCustomerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.deleteCardDetailsByCustomer(deleteCardDetailsByCustomerXML, referenceNo);
  }
  
  public java.lang.String setBillingStatusByCustomer(java.lang.String setBillingStatusByCustomerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.setBillingStatusByCustomer(setBillingStatusByCustomerXML, referenceNo);
  }
  
  public java.lang.String getPaymentsByInvoice(java.lang.String getPaymentsbyinvoiceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getPaymentsByInvoice(getPaymentsbyinvoiceXML, referenceNo);
  }
  
  public java.lang.String getRefundsByPayment(java.lang.String getRefundByPaymentXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getRefundsByPayment(getRefundByPaymentXML, referenceNo);
  }
  
  public java.lang.String getCreditNotesByInvoice(java.lang.String getCreditNotesByInvoiceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getCreditNotesByInvoice(getCreditNotesByInvoiceXML, referenceNo);
  }
  
  public java.lang.String getInvoicesByCreditNote(java.lang.String getInvoicesByCreditNoteXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getInvoicesByCreditNote(getInvoicesByCreditNoteXML, referenceNo);
  }
  
  public java.lang.String addExternalId(java.lang.String addExternalIdXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.addExternalId(addExternalIdXML, referenceNo);
  }
  
  public java.lang.String deleteExternalId(java.lang.String deleteExternalIdXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.deleteExternalId(deleteExternalIdXML, referenceNo);
  }
  
  public byte[] getInvoicePDF(java.lang.String getInvoicePDFXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getInvoicePDF(getInvoicePDFXML, referenceNo);
  }
  
  public java.lang.String updateOutboundCall(java.lang.String updateOutboundCallXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.updateOutboundCall(updateOutboundCallXML, referenceNo);
  }
  
  public java.lang.String createPlan(java.lang.String createPlanXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.createPlan(createPlanXML, referenceNo);
  }
  
  public java.lang.String updatePlan(java.lang.String updatePlanXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.updatePlan(updatePlanXML, referenceNo);
  }
  
  public java.lang.String oneTimeSale(java.lang.String onetimesaleXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.oneTimeSale(onetimesaleXML, referenceNo);
  }
  
  public java.lang.String addCustomerBankAccount(java.lang.String addCustomerBankAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.addCustomerBankAccount(addCustomerBankAccountXML, referenceNo);
  }
  
  public java.lang.String deleteCustomerBankAccount(java.lang.String deleteCustomerBankAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.deleteCustomerBankAccount(deleteCustomerBankAccountXML, referenceNo);
  }
  
  public java.lang.String getCustomerBankAccount(java.lang.String getCustomerBankAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getCustomerBankAccount(getCustomerBankAccountXML, referenceNo);
  }
  
  public java.lang.String getAccountSummary(java.lang.String getAccountSummaryXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getAccountSummary(getAccountSummaryXML, referenceNo);
  }
  
  public java.lang.String getBillingStatus(java.lang.String getBillingStatusXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getBillingStatus(getBillingStatusXML, referenceNo);
  }
  
  public java.lang.String modifyContract(java.lang.String modifyContractXml, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyContract(modifyContractXml, referenceNo);
  }
  
  public java.lang.String getDocumentsByDocType(java.lang.String getDocumentsByDocTypeXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getDocumentsByDocType(getDocumentsByDocTypeXML, referenceNo);
  }
  
  public java.lang.String getRemittancesByCustomer(java.lang.String getRemittancesByCustomerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getRemittancesByCustomer(getRemittancesByCustomerXML, referenceNo);
  }
  
  public java.lang.String getRemittanceByRemittanceID(java.lang.String getRemittanceByRemittanceIDXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getRemittanceByRemittanceID(getRemittanceByRemittanceIDXML, referenceNo);
  }
  
  public java.lang.String deleteCustomer(java.lang.String deleteCustomerXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.deleteCustomer(deleteCustomerXML, referenceNo);
  }
  
  public java.lang.String updateProvisioningAccount(java.lang.String updateProvisioningAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.updateProvisioningAccount(updateProvisioningAccountXML, referenceNo);
  }
  
  public java.lang.String getRecordsBySearch(java.lang.String getRecordsBySearchXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getRecordsBySearch(getRecordsBySearchXML, referenceNo);
  }
  
  public java.lang.String updateServiceDowngradeDetails(java.lang.String updateServiceDowngradeDetailsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.updateServiceDowngradeDetails(updateServiceDowngradeDetailsXML, referenceNo);
  }
  
  public java.lang.String closeTicket(java.lang.String closeTicketXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.closeTicket(closeTicketXML, referenceNo);
  }
  
  public java.lang.String addCreditCard(java.lang.String addCreditCardXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.addCreditCard(addCreditCardXML, referenceNo);
  }
  
  public java.lang.String enableDevice(java.lang.String deviceInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.enableDevice(deviceInfoXML, referenceNo);
  }
  
  public java.lang.String disableDevice(java.lang.String deviceInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.disableDevice(deviceInfoXML, referenceNo);
  }
  
  public java.lang.String getPlanPrice(java.lang.String getPlanPriceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getPlanPrice(getPlanPriceXML, referenceNo);
  }
  
  public java.lang.String registerDevice(java.lang.String registerDeviceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.registerDevice(registerDeviceXML, referenceNo);
  }
  
  public java.lang.String modifyDevice(java.lang.String modifyDeviceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyDevice(modifyDeviceXML, referenceNo);
  }
  
  public java.lang.String getDevices(java.lang.String getDevicesXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getDevices(getDevicesXML, referenceNo);
  }
  
  public java.lang.String createProfile(java.lang.String createProfileXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.createProfile(createProfileXML, referenceNo);
  }
  
  public java.lang.String getProfiles(java.lang.String getProfilesXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getProfiles(getProfilesXML, referenceNo);
  }
  
  public java.lang.String modifyProfile(java.lang.String modifyProfileXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyProfile(modifyProfileXML, referenceNo);
  }
  
  public java.lang.String lockAccount(java.lang.String lockAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.lockAccount(lockAccountXML, referenceNo);
  }
  
  public java.lang.String lockUserAccount(java.lang.String lockUserAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.lockUserAccount(lockUserAccountXML, referenceNo);
  }
  
  public java.lang.String unlockAccount(java.lang.String unlockAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.unlockAccount(unlockAccountXML, referenceNo);
  }
  
  public java.lang.String unlockUserAccount(java.lang.String unlockUserAccountXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.unlockUserAccount(unlockUserAccountXML, referenceNo);
  }
  
  public java.lang.String addDevice(java.lang.String addDeviceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.addDevice(addDeviceXML, referenceNo);
  }
  
  public java.lang.String rememberMe(java.lang.String rememberMeXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.rememberMe(rememberMeXML, referenceNo);
  }
  
  public java.lang.String getAccountProfile(java.lang.String accountInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getAccountProfile(accountInfoXML, referenceNo);
  }
  
  public java.lang.String getActiveSubscriptions(java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getActiveSubscriptions(referenceNo);
  }
  
  public java.lang.String getSubscriptionDetails(java.lang.String subscriptionDetailsXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getSubscriptionDetails(subscriptionDetailsXML, referenceNo);
  }
  
  public java.lang.String getAddressByReversePopulation(java.lang.String addressInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getAddressByReversePopulation(addressInfoXML, referenceNo);
  }
  
  public java.lang.String checkUserAvailability(java.lang.String checkUserAvailabilityXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.checkUserAvailability(checkUserAvailabilityXML, referenceNo);
  }
  
  public java.lang.String materialIssue(java.lang.String materialInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.materialIssue(materialInfoXML, referenceNo);
  }
  
  public java.lang.String authenticateAndRegisterDevice(java.lang.String authenticateAndRegisterDeviceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.authenticateAndRegisterDevice(authenticateAndRegisterDeviceXML, referenceNo);
  }
  
  public java.lang.String orderBooking(java.lang.String orderBookingXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.orderBooking(orderBookingXML, referenceNo);
  }
  
  public java.lang.String calculatePrice(java.lang.String calculatePriceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.calculatePrice(calculatePriceXML, referenceNo);
  }
  
  public java.lang.String generateJV(java.lang.String generateJVXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.generateJV(generateJVXML, referenceNo);
  }
  
  public java.lang.String cancelPayment(java.lang.String cancelPaymentXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.cancelPayment(cancelPaymentXML, referenceNo);
  }
  
  public java.lang.String suspendService(java.lang.String suspendServiceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.suspendService(suspendServiceXML, referenceNo);
  }
  
  public java.lang.String reactivateService(java.lang.String reactivateServiceXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.reactivateService(reactivateServiceXML, referenceNo);
  }
  
  public java.lang.String getEntitlementInformation(java.lang.String getEntitlementInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getEntitlementInformation(getEntitlementInfoXML, referenceNo);
  }
  
  public java.lang.String sendOSD(java.lang.String sendOSDXML, java.lang.String refereneceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.sendOSD(sendOSDXML, refereneceNo);
  }
  
  public java.lang.String verifyTopup(java.lang.String verifyTopupXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.verifyTopup(verifyTopupXML, referenceNo);
  }
  
  public java.lang.String processTopup(java.lang.String processTopupXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.processTopup(processTopupXML, referenceNo);
  }
  
  public java.lang.String modifyAssociation(java.lang.String modifyAssociationXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyAssociation(modifyAssociationXML, referenceNo);
  }
  
  public java.lang.String updateProvsionRequestStatus(java.lang.String updateProvsionRequestStatusXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.updateProvsionRequestStatus(updateProvsionRequestStatusXML, referenceNo);
  }
  
  public java.lang.String retrack(java.lang.String retrackXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.retrack(retrackXML, referenceNo);
  }
  
  public java.lang.String addConnection(java.lang.String connectionXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.addConnection(connectionXML, referenceNo);
  }
  
  public java.lang.String removeConnection(java.lang.String connectionInfoXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.removeConnection(connectionInfoXML, referenceNo);
  }
  
  public java.lang.String modifyWorkorder(java.lang.String modifyWorkorderXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyWorkorder(modifyWorkorderXML, referenceNo);
  }
  
  public java.lang.String modifyContractFlexAttributes(java.lang.String modifyContractFlexAttributesXml, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.modifyContractFlexAttributes(modifyContractFlexAttributesXml, referenceNo);
  }
  
  public byte[] getStatementPDF(java.lang.String getStatementPDFXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.getStatementPDF(getStatementPDFXML, referenceNo);
  }
  
  public java.lang.String preActivation(java.lang.String preActivationXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.preActivation(preActivationXML, referenceNo);
  }
  
  public java.lang.String updateProvisioningRequestResponse(java.lang.String updateProvisioningRequestResponseXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.updateProvisioningRequestResponse(updateProvisioningRequestResponseXML, referenceNo);
  }
  
  public java.lang.String addProspectCallStatus(java.lang.String addProspectCallStatusXML, java.lang.String referenceNo) throws java.rmi.RemoteException{
    if (serviceSoap == null)
      _initServiceSoapProxy();
    return serviceSoap.addProspectCallStatus(addProspectCallStatusXML, referenceNo);
  }
  
  
}